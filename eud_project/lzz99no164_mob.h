
#include "lzz99no164_utils.h"
#include "libs/voiceList.h"

void monsterCommonSettings(int mon){
    RetreatLevel(mon,0.0);
    ResumeLevel(mon,1.0);
    AggressionLevel(mon,1.0);
}

int GiantLeechBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1851877703; arr[1] = 1701137524; arr[2] = 26723; arr[17] = 375; arr[19] = 66; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 1; 
		arr[27] = 5; arr[28] = 1113325568; arr[29] = 98; arr[31] = 8; arr[59] = 5542784; 
		arr[60] = 1358; arr[61] = 46895696; 
	pArr = arr;
	return pArr;
}

void GiantLeechSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073574051;		ptr[137] = 1073574051;
	int *hpTable = ptr[139];
	hpTable[0] = 375;	hpTable[1] = 375;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GiantLeechBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobSlime(float x, float y){
    int m=CreateObjectById(OBJ_GIANT_LEECH,x,y);

    GiantLeechSubProcess(m);
    SetUnitVoice(m,MONSTER_VOICE_BomberBlue);
    return m;
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 260; arr[19] = 80; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1107296256; arr[29] = 25; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobGoon(float x, float y){
    int m=CreateObjectById(OBJ_GOON,x,y);

    GoonSubProcess(m);
    SetUnitVoice(m,MONSTER_VOICE__FireKnight1);
    return m;
}

int createMobArcher(float x,float y){
    int m=CreateObjectById(OBJ_ARCHER,x,y);

    SetUnitMaxHealth(m, 128);
    SetUnitVoice(m,MONSTER_VOICE__Maiden1);
    return m;
}

int BearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[17] = 375; arr[19] = 50; arr[21] = 1065353216; arr[24] = 1069547520; 
		arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1114636288; arr[31] = 2; arr[32] = 12; arr[33] = 18; arr[59] = 5542784; 
		arr[60] = 1365; arr[61] = 46903040; 
	pArr = arr;
	return pArr;
}

void BearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = 375;	hpTable[1] = 375;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = BearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobBear(float x,float y){
    int m=CreateObjectById(OBJ_BEAR,x,y);
    BearSubProcess(m);
    return m;
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 275; arr[19] = 90; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 3; arr[28] = 1112014848; arr[29] = 40; arr[30] = 1106247680; 
		arr[31] = 10; arr[32] = 12; arr[33] = 20; arr[34] = 6; arr[35] = 9; 
		arr[36] = 36; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 275;	hpTable[1] = 275;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobBlueOni(float x,float y){
    int m=CreateObjectById(OBJ_WOUNDED_APPRENTICE,x,y);

    WoundedApprenticeSubProcess(m);
    SetUnitVoice(m,MONSTER_VOICE_EmberDemon);
    return m;
}

int SpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684631635; arr[1] = 29285; arr[17] = 225; arr[18] = 20; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1104674816; arr[29] = 10; arr[31] = 10; arr[32] = 9; arr[33] = 12; 
		arr[34] = 50; arr[35] = 5; arr[36] = 40; arr[59] = 5544896; arr[60] = 1353; 
		arr[61] = 46914304; 
	pArr = arr;
	return pArr;
}

void SpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int createMobGopdung(float x,float y){
    int m=CreateObjectById(OBJ_SPIDER,x,y);
    SpiderSubProcess(m);
    return m;
}

void QueryDeathEvent(int *getFn, int setFn){
    int ev;
    if (getFn){
        getFn[0]=ev;
        return;
    }
    ev=setFn;
}

void CreateRandomMonsterLow(int pos){
    short fn[]={createMobArcher,createMobGoon,createMobBear,createMobBlueOni,createMobGopdung,};
    int ixy[]={GetObjectX(pos),GetObjectY(pos),};
    Delete(pos);
    int m=Bind(fn[Random(0,4)], ixy);

    monsterCommonSettings(m);
    int ev;
    QueryDeathEvent(&ev,0);
    if (ev!=0)
        SetCallback(m,5,ev);
}

int MechanicalGolemBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751344461; arr[1] = 1667853921; arr[2] = 1866951777; arr[3] = 7169388; arr[17] = 800; 
		arr[19] = 73; arr[21] = 1065353216; arr[23] = 65537; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[28] = 1112014848; arr[29] = 100; arr[30] = 1120403456; arr[31] = 2; 
		arr[32] = 60; arr[33] = 90; arr[58] = 5545616; arr[59] = 5544288; arr[60] = 1318; 
		arr[61] = 46900992; 
	pArr = arr;
	return pArr;
}

void MechanicalGolemSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074538742;		ptr[137] = 1074538742;
	int *hpTable = ptr[139];
	hpTable[0] = 800;	hpTable[1] = 800;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = MechanicalGolemBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobBillyOni(float x, float y){
	int mon=CreateObjectById(OBJ_MECHANICAL_GOLEM,x,y);
	MechanicalGolemSubProcess(mon);
	return mon;
}
int CarnivorousPlantBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 600; 
		arr[19] = 77; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[27] = 5; 
		arr[28] = 1110704128; arr[29] = 100; arr[31] = 8; arr[59] = 5542784; arr[60] = 1371; 
		arr[61] = 46901760; 
	pArr = arr;
	return pArr;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075042058;		ptr[137] = 1075042058;
	int *hpTable = ptr[139];
	hpTable[0] = 600;	hpTable[1] = 600;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = CarnivorousPlantBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int createMobDeathFlower(float x, float y){
	int mon=CreateObjectById(OBJ_CARNIVOROUS_PLANT,x,y);
	CarnivorousPlantSubProcess(mon);
	return mon;
}

int HecubahWithOrbBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 500; 
		arr[19] = 70; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; arr[40] = 1852140903; 
		arr[41] = 116; arr[53] = 1128792064; arr[55] = 18; arr[56] = 28; arr[60] = 1384; 
		arr[61] = 46914560; 
	pArr = arr;
	return pArr;
}

void HecubahWithOrbSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 500;	hpTable[1] = 500;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = HecubahWithOrbBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int createMobGiantWoman(float x, float y){
	int mon=CreateObjectById(OBJ_HECUBAH_WITH_ORB,x,y);
	HecubahWithOrbSubProcess(mon);
	return mon;
}
int BBearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[17] = 660; arr[18] = 130; arr[19] = 65; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1114636288; arr[29] = 88; arr[30] = 1123024896; arr[31] = 2; arr[59] = 5542784; 
		arr[60] = 1365; arr[61] = 46903040; 
	pArr = arr;
	return pArr;
}

void BBearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073322393;		ptr[137] = 1073322393;
	int *hpTable = ptr[139];
	hpTable[0] = 660;	hpTable[1] = 660;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = BBearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int createMobGiantGopdung(float x, float y){
	int mon=CreateObjectById(OBJ_BLACK_BEAR,x,y);
	BBearSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE_Mimic);
	return mon;
}
int LichLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 485; arr[19] = 82; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1110441984; arr[29] = 50; arr[30] = 1106247680; arr[32] = 9; arr[33] = 12; 
		arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void LichLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075671204;		ptr[137] = 1075671204;
	int *hpTable = ptr[139];
	hpTable[0] = 485;	hpTable[1] = 485;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int createMobLichking(float x, float y){
	int mon=CreateObjectById(OBJ_LICH_LORD,x,y);
	LichLordSubProcess(mon);
	return mon;
}

int AirshipCaptainBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 600; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1113325568; arr[29] = 66; arr[31] = 10; arr[32] = 12; arr[33] = 12; 
		arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328; 
	pArr = arr;
	return pArr;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 600;	hpTable[1] = 600;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = AirshipCaptainBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int createMobSantaClaus(float x, float y){
	int mon=CreateObjectById(OBJ_AIRSHIP_CAPTAIN,x,y);
	AirshipCaptainSubProcess(mon);
	SetUnitVoice(mon, MONSTER_VOICE__FireKnight2);
	return mon;
}

void CreateRandomMonsterHigh(int pos){
    short fn[]={createMobBillyOni,createMobDeathFlower,createMobGiantWoman,createMobGiantGopdung,createMobLichking,createMobSlime,};
    int ixy[]={GetObjectX(pos),GetObjectY(pos),};
    Delete(pos);
    int m=Bind(fn[Random(0,5)], ixy);

    monsterCommonSettings(m);
    int ev;
    QueryDeathEvent(&ev,0);
    if (ev!=0)
        SetCallback(m,5,ev);
}

void CreateRandomMonsterXMas(int pos){
    float x=GetObjectX(pos),y=GetObjectY(pos);
    Delete(pos);
    int m=createMobSantaClaus(x,y);

    monsterCommonSettings(m);
    int ev;
    QueryDeathEvent(&ev,0);
    if (ev!=0)
        SetCallback(m,5,ev);
}
