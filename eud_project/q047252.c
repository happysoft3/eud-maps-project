
#include "libs/define.h"
#include "libs/printutil.h"
#include "libs/format.h"
#include "libs/buff.h"
#include "libs/waypoint.h"
#include "libs/fxeffect.h"
#include "libs/unitstruct.h"
#include "libs/sound_define.h"
#include "libs/coopteam.h"
#include "libs/observer.h"
#include "libs/network.h"
#include "libs/winapi.h"
#include "libs/hash.h"
#include "libs/bind.h"
#include "libs/imageutil.h"
#include "libs/clientside.h"
#include "libs/queueTimer.h"
#include "libs/cmdline.h"

#define SEND_LEFT_SHIFT 1
#define SEND_RIGHT_SHIFT 2
#define SEND_DOWN_SHIFT 3

#define WALLGROUP_startingWalls 0

#define PLAYER_DEATH_FLAG 0x80000000

#define CAT_REVIVE_LOCATION 11

int *m_innerExitRing;
int m_innerExitRingCount;
int *m_outerExitRing;
int m_outerExitRingCount;

int m_creatureHash;
int m_lastCreatedUnit;
int m_unitscanHashInstance;

int m_toggleTeleporting[2];
int m_toggleTeleportKey[2];

int *m_pImgVector;

int m_flameWalls[3];
int m_flameWalls2[5];
int m_flameWalls3[5];
int m_flameWalls4[5];
int m_flameWalls5[5];

int m_player[32];
int m_pFlag[32];
int m_creature[32];

int m_clientNetId;
int *m_clientKeyState;

int m_liftGroupL[31];
int m_liftPitGroupL[31];
int m_liftGroupC[31];
int m_liftPitGroupC[31];
int m_liftGroupR[31];
int m_liftPitGroupR[31];

static void wrapObjectOn(int unit)
{
    if (ToInt(GetObjectX(unit)))
    {
        if (!IsObjectOn(unit))
            ObjectOn(unit);
    }
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

static void RemoveCreature(int cre)
{
    if (MaxHealth(cre))
    {
        HashGet(m_creatureHash, cre, NULLPTR, TRUE);
        Delete(cre);
    }
}

static void ReviveCreature(int cre)
{
    if (!MaxHealth(cre))
        return;

    int plr;

    if (HashGet(m_creatureHash, cre, &plr, FALSE))
    {
        PlayerClassStartGame(plr, m_player[plr], FALSE);
    }
}

static void ReportDeathAll(int user)
{
    char buff[128], *name=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(buff, "방금, %s 님이 조종하던 고양이가 파괴되었습니다", &name, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

static void OnCreatureDeath(int cre)
{
    int plr=-1, owner = GetOwner(cre);

    if (CurrentHealth(owner))
    {
        UniPrint(owner, "당신이 조종하던 유닛이 파괴되었습니다. 잠시 후 다시 부활합니다");
        ReportDeathAll(owner);
    }

    if (HashGet(m_creatureHash, cre, &plr, FALSE))
    {
        float x=GetObjectX(cre), y=GetObjectY(cre);

        RemoveCreature(cre);
        cre = CreateObjectAt("Urchin", x, y);
        HashPushback(m_creatureHash, cre, plr);
        Damage(cre, 0, 999, DAMAGE_TYPE_PLASMA);
        m_creature[plr] = cre;
        SecondTimerWithArg(3, cre, ReviveCreature);
    }
}

void OnCreatureCollide()
{
    if (!GetTrigger())
        return;

    if (HasClass(OTHER, "DANGEROUS"))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        OnCreatureDeath(GetTrigger());
    }
}

static int createPlayerCreature(int user, float xpos, float ypos)
{
    int cre = DummyUnitCreateAt("UrchinShaman", xpos, ypos);

    SetOwner(user, cre);
    SetCallback(cre, 9, OnCreatureCollide);
    Frozen(cre, FALSE);
    return cre;
}

static void PlayerClassStartGame(int plr, int user, int initial)
{
    RemoveCreature(m_creature[plr]);

    m_creature[plr]=createPlayerCreature(user, LocationX(CAT_REVIVE_LOCATION), LocationY(CAT_REVIVE_LOCATION));

    HashPushback(m_creatureHash, m_creature[plr], plr);
    if (initial)
    {
        Enchant(user, EnchantList(ENCHANT_FREEZE), 0.0);
        MoveObject(user, LocationX(12), LocationY(12));
    }
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}

void PlayerClassOnJoin(int plr)
{
    int user = m_player[plr];

    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    // if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
    //     PlayerSetAllBuff(pUnit);
    PlayerClassStartGame(plr, user, TRUE);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(2), LocationY(2));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int plr)
{
    MoveObject(user, LocationX(474), LocationY(474));
    Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    PushTimerQueue(3, plr, PlayerClassOnJoin);
    UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
}

static void imageBrick()
{ }

static void imageCatUp()
{ }
static void imageCatLeft()
{ }
static void imageCatDown()
{ }
static void imageCatRight()
{ }
static void imageCongrate()
{ }
static void KeySettingImage()
{ }

void MapBgmData()
{ }

static void commonServerClientProcedure()
{
    int index=0;
    m_pImgVector=CreateImageVector(32);

    InitializeImageHandlerProcedure(m_pImgVector);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatUp)+4, 117769, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatDown)+4, 117828, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatRight)+4, 117804, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatLeft)+4, 117792, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imageBrick)+4, 125167, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imageCongrate) + 4, 132315, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(KeySettingImage) + 4, 131930, index++);

    ExtractMapBgm("..\\buzzer.mp3", MapBgmData);
}

int KeyboardIOCheckKey(int keyId)
{
    int *keyTable = 0x6950b0;
    int *selkey = keyTable+(keyId*8);
    int key = (selkey[0]>>8)&0xff;

    return key==2;
}

static void serverInputLoop()
{
    int up = KeyboardIOCheckKey(0xc8);
    int down = KeyboardIOCheckKey(0xd0)<<SEND_DOWN_SHIFT;
    int left = KeyboardIOCheckKey(0xcb)<<SEND_LEFT_SHIFT;
    int right = KeyboardIOCheckKey(0xcd)<<SEND_RIGHT_SHIFT;
    int prevState, state = up|left|right|down;

    if (prevState!=state)
    {
        prevState=state;
        m_clientKeyState[31]=state;
    }
    FrameTimer(3, serverInputLoop);
}

static void ClientKeyHandlerRemix()
{
    int up = KeyboardIOCheckKey(0xc8);
    int down = KeyboardIOCheckKey(0xd0)<<SEND_DOWN_SHIFT;
    int left=KeyboardIOCheckKey(0xcb)<<SEND_LEFT_SHIFT;
    int right=KeyboardIOCheckKey(0xcd)<<SEND_RIGHT_SHIFT;
    int prevState, state = up|left|right|down;

    if (prevState!=state)
    {
        prevState=state;

        char testPacket[]={0x3d, 0x00+(m_clientNetId*4),0x10,0x75,0x00, 0, 0,0,0};
        int *p = testPacket + 1;

        p[1] = prevState;
        NetClientSendRaw(31, 0, testPacket, sizeof(testPacket));
    }
}

void ClientClassTimerLoop()
{
    ClientKeyHandlerRemix();
    FrameTimer(1, ClientClassTimerLoop);
}

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    m_clientNetId = GetMemory(0x979720);
    FrameTimer(30, ClientClassTimerLoop);
    commonServerClientProcedure();
}

void PlayerClassOnInit(int plr, int pUnit)
{
    m_player[plr]=pUnit;
    m_pFlag[plr]=1;

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
            commonServerClientProcedure();
        PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    }
    m_clientKeyState[ plr ] = 0;
    ChangeGold(pUnit, -GetGold(pUnit));
    PushTimerQueue(1, plr, PlayerClassOnLoop);

    char buff[128];

    NoxSprintfString(buff, "playeroninit. index-%d", &plr, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void PlayerClassOnEntry(int plrUnit)
{
    while (TRUE)
    {
        if (!CurrentHealth(plrUnit))
            break;
        int plr = GetPlayerIndex(plrUnit);
        char initialUser=FALSE;

        if (!MaxHealth(m_player[plr]))
            PlayerClassOnInit(plr, plrUnit);
        if (plr >= 0)
        {
            if (initialUser)
                OnPlayerDeferredJoin(plrUnit, plr);
            else
                PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(plrUnit);
        break;
    }
}

void PlayerClassOnShutdown(int rep)
{
    if (CurrentHealth(m_player[rep]))
        MoveObject(m_player[rep], LocationX(2), LocationY(2));
    if (m_creature[rep])
    {
        RemoveCreature(m_creature[rep]);
        m_creature[rep]=0;
    }
    m_player[rep]=0;
    m_pFlag[rep]=0;

    char buff[128];

    NoxSprintfString(buff, "playeronshutdown. index-%d", &rep, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void PlayerClassOnDeath(int rep, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

#define KEY_UP_SHIFT 1
#define KEY_LEFT_SHIFT 2
#define KEY_RIGHT_SHIFT 4
#define KEY_DOWN_SHIFT 8

static void serverTaskUserInputHandler(int plr, int pUnit)
{
    int pIndex=GetPlayerIndex(pUnit);
    int keyState=m_clientKeyState[pIndex], dir=0;
    float xVect=0.0, yVect=0.0;

    if (keyState&KEY_UP_SHIFT)
    {
        yVect-=3.0;
        dir=192;
    }
    if (keyState&KEY_DOWN_SHIFT)
    {
        yVect+=3.0;
        dir=64;
    }
    if (keyState&KEY_LEFT_SHIFT)
    {
        xVect-=3.0;
        dir=128;
    }
    if (keyState&KEY_RIGHT_SHIFT)
    {
        xVect+=3.0;
        dir=255;
    }
    if (ToInt(xVect) || ToInt(yVect))
    {
        PushObjectTo(m_creature[plr], xVect, yVect);
        if (GetDirection(m_creature[plr])!=dir)
            LookWithAngle(m_creature[plr], dir);
    }
}

void PlayerClassOnAlive(int plr, int user)
{
    int cre=m_creature[plr];

    if (MaxHealth(cre))
    {
        if (CheckWatchFocus(user))
            PlayerLook(user, cre);
        serverTaskUserInputHandler(plr, user);
    }
}

void PlayerClassOnLoop(int pIndex)
{
    int user = m_player[pIndex];

    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (m_pFlag[pIndex])
        {
            PlayerClassOnShutdown(pIndex);
            return;
        }
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
}

static void initialServerPatch()
{
    int oldProtect;

    WinApiVirtualProtect(0x51cf84, 1024, 0x40, &oldProtect);
    char *p = 0x51cf84;

    p[23]=0x7a;
    SetMemory(0x51d0c8, 0x513f10);
    WinApiVirtualProtect(0x51cf84, 1024, oldProtect, NULLPTR);
    //46 8B 06 8B 4E 04 89 08 83 C6 09 E9 24 8F 00 00

    WinApiVirtualProtect(0x513f10, 256, 0x40, &oldProtect);

    char servcode[]={0x46, 0x8B, 0x06, 0x8B, 0x4E, 0x04, 0x89, 0x08, 0x83, 0xC6, 0x08, 0xE9, 0x24, 0x8F, 0x00, 0x00};

    NoxByteMemCopy(servcode, 0x513f10, sizeof(servcode));
    WinApiVirtualProtect(0x513f10, 256, oldProtect, NULLPTR);

    SetMemory(0x5c31ec, 0x513f30);
}

void OnEndedUnitScan()
{
    deferredInit();
    WallGroupOpen(WALLGROUP_startingWalls);
    UniPrintToAll("q047252.c::OnEndedUnitScan");
}

static void UnitScanProcessProto(int functionId, int posUnit)
{
    Bind(functionId, &functionId + 4);
}

void UnitScanLoop(int cur)
{
    int rep = 30, action, thingId;

    while (rep--)
    {
        if (++cur >= m_lastCreatedUnit)
        {
            OnEndedUnitScan();
            return;
        }
        thingId=GetUnitThingID(cur);
        if (thingId)
        {
            if (HashGet(m_unitscanHashInstance, thingId, &action, FALSE))
                UnitScanProcessProto(action, cur);
        }
    }
    PushTimerQueue(1, cur, UnitScanLoop);
}

static void startUnitScan(int cur)
{
    UnitScanLoop(cur);
}

static void moveReviveLocation()
{
    if (!GetTrigger())
        return;
    if (!IsMonsterUnit(OTHER))
        return;

    int owner=GetOwner(OTHER);

    if (IsPlayerUnit(owner))
    {
        if (CurrentHealth(owner))
        {
            float x=GetObjectX(SELF),y=GetObjectY(SELF);

            TeleportLocation(CAT_REVIVE_LOCATION, x,y);
            PlaySoundAround(OTHER, SOUND_SoulGateTouch);
            Effect("YELLOW_SPARKS",x,y,0.0,0.0);
            Delete(GetTrigger()+1);
            Delete(SELF);
            UniPrintToAll("!-부활터 위치가 변경되었습니다-!");
        }
    }
}

static void createReviveStuff(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
    Delete(posUnit);
    int stuff=DummyUnitCreateAt("Bomber", x,y);
    int sel2Fx=CreateObjectAt("Magic", x,y);
    Frozen(sel2Fx, TRUE);
    SetUnitSubclass(sel2Fx, GetUnitSubclass(sel2Fx) ^ 1);
    SetCallback(stuff, 9, moveReviveLocation);
}

static void createWallBox(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
    Delete(posUnit);
    int wall=DummyUnitCreateAt("CarnivorousPlant", x,y);
    SetUnitMass(wall, 99999.0);
    UnitNoCollide( wall );
    CreateObjectAt("ExtentCylinderLarge", x,y);
}

static void backwardPatrolFlames(int *args)
{
    int rep = 16, *pFlames=args[0], *pLocations=args[1];

    while (--rep>=0)
    {
        Move(pFlames[rep], pLocations[rep]);
    }
    PushTimerQueue(71, args, forwardPatrolFlames);
}

static void forwardPatrolFlames(int *args)
{
    int rep = 16, *pFlames=args[0];

    while (--rep>=0)
    {
        Move(pFlames[rep], 24);
    }
    PushTimerQueue(77, args, backwardPatrolFlames);
}

static void initPatrolFlames()
{
    int flames[12], i;
    int locations[]={25,26,27,28,36,35,34,33,32,31,30,29};
    int args[]={flames, locations};
    char buff[16];

    PushTimerQueue(10, args, forwardPatrolFlames);

    for (i = 1 ; i <= sizeof(flames);++i)
    {
        NoxSprintfString(buff, "patrolF%d", &i, 1);
        flames[i-1]=Object(ReadStringAddressEx(buff));
    }
}

static void placeMovingFlame(int arg)
{
    int right=Random(0,1), f=movingToRight;
    float x=LocationX(63), y= RandomFloat(LocationY(65), LocationY(66));
    if (right)
    {
        x=LocationX(64);
        f=movingToLeft;
    }

    int flame=CreateObjectAt("LargeFlame", x,y);

    PushTimerQueue(1, flame, f);
    LookWithAngle(flame, 157);
}

static void movingToLeft(int flame)
{
    if (ToInt(GetObjectX(flame)))
    {
        if (GetDirection(flame))
        {
            PushTimerQueue(1, flame, movingToLeft);
            LookWithAngle(flame, GetDirection(flame)-1);
            MoveObjectVector(flame, -5.0, 0.0);
            return;
        }
        Delete(flame);
        PushTimerQueue(Random(1, 33),0, placeMovingFlame);
    }
}

static void movingToRight(int flame)
{
    if (ToInt(GetObjectX(flame)))
    {
        if (GetDirection(flame))
        {
            PushTimerQueue(1, flame, movingToRight);
            LookWithAngle(flame, GetDirection(flame)-1);
            MoveObjectVector(flame, 5.0, 0.0);
            return;
        }
        Delete(flame);
        PushTimerQueue(Random(1, 33),0, placeMovingFlame);
    }
}

static void initFlameWalls()
{
    m_flameWalls[0]=CreateObject("LargeFlame", 21);
    m_flameWalls[1]=CreateObject("LargeFlame", 22);
    m_flameWalls[2]=CreateObject("LargeFlame", 23);

    m_flameWalls2[0]=CreateObject("LargeFlame", 73);
    m_flameWalls2[1]=CreateObject("LargeFlame", 74);
    m_flameWalls2[2]=CreateObject("LargeFlame", 75);
    m_flameWalls2[3]=CreateObject("LargeFlame", 76);
    m_flameWalls2[4]=CreateObject("LargeFlame", 77);

    m_flameWalls3[0]=CreateObject("LargeFlame", 92);
    m_flameWalls3[1]=CreateObject("LargeFlame", 93);
    m_flameWalls3[2]=CreateObject("LargeFlame", 94);
    m_flameWalls3[3]=CreateObject("LargeFlame", 95);
    m_flameWalls3[4]=CreateObject("LargeFlame", 96);

    m_flameWalls4[0]=CreateObject("LargeFlame", 110);
    m_flameWalls4[1]=CreateObject("LargeFlame", 111);
    m_flameWalls4[2]=CreateObject("LargeFlame", 112);
    m_flameWalls4[3]=CreateObject("LargeFlame", 113);
    m_flameWalls4[4]=CreateObject("LargeFlame", 114);

    m_flameWalls5[0]=CreateObject("LargeFlame", 105);
    m_flameWalls5[1]=CreateObject("LargeFlame", 106);
    m_flameWalls5[2]=CreateObject("LargeFlame", 107);
    m_flameWalls5[3]=CreateObject("LargeFlame", 108);
    m_flameWalls5[4]=CreateObject("LargeFlame", 109);
}

static void blockTrapHandler(int *pArgs)
{
    int sub=pArgs[0], *pWays = pArgs[1];

    if (ToInt(GetObjectX(sub)))
    {
        int idx=GetDirection(sub);

        if (pWays[idx])
        {
            float x=LocationX(pWays[idx]),y=LocationY(pWays[idx]);

            PushTimerQueue(4, pArgs, blockTrapHandler);
            LookWithAngle(sub, idx+1);
            MoveObject(sub+1, x-23.0, y-23.0);
            MoveObject(sub+2, x+23.0, y-23.0);
            MoveObject(sub+3, x-23.0, y+23.0);
            MoveObject(sub+4, x+23.0, y+23.0);
            PlaySoundAround(sub+1, SOUND_HammerMissing);
            return;
        }
        else
        {
            PushTimerQueue(19, pArgs, blockTrapHandler);
            LookWithAngle(sub, 0);
            return;
        }        
        Delete(sub++);
        Delete(sub++);
        Delete(sub++);
        Delete(sub++);
        Delete(sub++);
    }
}

static void startBlockTraps(int delay)
{
    int ways[]={115, 117, 119,121,123,125,127,129,131,133,135,137,139,141,116,118,120,122,
        124,126,128,130,132,134,136,138,140,142, 0};
    float x=LocationX(ways[0]), y=LocationY(ways[0]);
    int sub=CreateObjectAt("InvisibleLightBlueLow", x,y);
    int blocks[]={CreateObjectAt("SpikeBlock", x,y),CreateObjectAt("SpikeBlock", x,y),CreateObjectAt("SpikeBlock", x,y),CreateObjectAt("SpikeBlock", x,y)};
    int args[]={sub, ways};

    PushTimerQueue(delay, args, blockTrapHandler);
}

void randomPickTeleport(int location)
{
    TeleportLocation(location, RandomFloat(LocationX(160), LocationX(161)), RandomFloat(LocationY(161), LocationY(162)));
}

static void junkyardDogHandler(int sub)
{
    if (IsObjectOn(sub))
    {
        int flame=GetObjectZ(sub), location=GetUnit1C(sub);

        if (ToInt(GetObjectX(flame)))
        {
            PushTimerQueue(Random(28, 90), sub, junkyardDogHandler);
            randomPickTeleport(location);
            Move(flame, location);
            return;
        }
        Delete(sub);
    }
}

static void makeJunkyardDogFlame(int location, int flame)
{
    int sub=CreateObjectAt("InvisibleLightBlueHigh", LocationX(location), LocationY(location));

    PushTimerQueue(1, sub, junkyardDogHandler);
    Raise(sub, flame);
    SetUnit1C(sub, location);
}

static void initJunkyardDog()
{
    makeJunkyardDogFlame(167, Object("junkDog1"));
    makeJunkyardDogFlame(166, Object("junkDog2"));
    makeJunkyardDogFlame(165, Object("junkDog3"));
    makeJunkyardDogFlame(159, Object("junkDog4"));
    makeJunkyardDogFlame(163, Object("junkDog5"));
    makeJunkyardDogFlame(164, Object("junkDog6"));
    makeJunkyardDogFlame(169, Object("junkDog7"));
    makeJunkyardDogFlame(170, Object("junkDog8"));
    makeJunkyardDogFlame(168, Object("junkDog9"));
    makeJunkyardDogFlame(171, Object("junkDogA"));
    makeJunkyardDogFlame(172, Object("junkDogB"));
    makeJunkyardDogFlame(173, Object("junkDogC"));
    makeJunkyardDogFlame(174, Object("junkDogD"));
    // makeJunkyardDogFlame(175, Object("junkDogE"));
    // makeJunkyardDogFlame(176, Object("junkDogF"));
}

static void toggle5Seconds()
{
    ObjectToggle(m_toggleTeleporting[0]);
    ObjectToggle(m_toggleTeleporting[1]);
    ObjectToggle(m_toggleTeleportKey[0]);
    ObjectToggle(m_toggleTeleportKey[1]);
    SecondTimer(5, toggle5Seconds);
}

static void initToggleTeleportingSystem()
{
    m_toggleTeleporting[0]=Object("gotoRed");
    m_toggleTeleporting[1]=Object("gotoBlue");
    m_toggleTeleportKey[0]=Object("redkey");
    m_toggleTeleportKey[1]=Object("blukey");

    ObjectOn(m_toggleTeleporting[0]);
    ObjectOn(m_toggleTeleportKey[0]);
    SecondTimer(5, toggle5Seconds);
}

static void playWav(short soundId)
{
    int rep=sizeof(m_player);

    while(--rep>=0)
    {
        if (CurrentHealth(m_player[rep]))
            PlaySoundAround(m_player[rep], soundId);
    }
}

static void replaceElevatorDestination(int srcelev, int dest)
{
    int *ptr=UnitToPtr(srcelev);

    SetMemory(GetMemory(ptr+0x2ec)+4, UnitToPtr(dest));
}

static void showThisMapInfo()
{
    UniPrintToAll("---고양이 탈출--- 제작. 237");
    UniPrintToAll("방향키를 사용하여 고양이를 움직여 보세요");
    playWav(SOUND_JournalEntryAdd);
}

static void liftComplexOff(int count, int *pLift, int *pLiftpit)
{
    while (--count>=0)
    {
        ObjectOff(pLift[count]);
        ObjectOff(pLiftpit[count]);
    }
}

static void deferredLiftOff(int *pArgs)
{
    liftComplexOff(pArgs[0], pArgs[1], pArgs[2]);
    FreeSmartMemEx(pArgs);
}

static void liftComplexOn(int *pLift, int *pLiftpit)
{
    int count=31, *ptr;

    AllocSmartMemEx(12, &ptr);
    ptr[0]=count;
    ptr[1]=pLift;
    ptr[2]=pLiftpit;
    PushTimerQueue(21, ptr, deferredLiftOff);
    while (--count>=0)
    {
        ObjectOn(pLift[count]);
        ObjectOn(pLiftpit[count]);
    }
}

static void initelevGroup(char codename, int *pLift, int *pLiftPit)
{
    int count=31;
    char buff[64];

    while(--count>=0)
    {
        int args[]={codename, count+1};
        NoxSprintfString(buff, "ev%c%d", args, sizeof(args));
        pLift[count]=Object(ReadStringAddressEx(buff));
        NoxSprintfString(buff, "epit%c%d", args, sizeof(args));
        pLiftPit[count]=Object(ReadStringAddressEx(buff));
        replaceElevatorDestination(pLift[count], pLiftPit[count]);
        replaceElevatorDestination(pLiftPit[count], pLift[count]);
        if (ToInt(GetObjectX(pLift[count])))
            CreateObjectAt("PeriodicSpike", GetObjectX(pLift[count]), GetObjectY(pLift[count]));
        ObjectOff(pLift[count]);
    }
}

static void deferredWalk(int mon)
{
    if (CurrentHealth(mon))
    {
        Move(mon, GetDirection(mon));
    }
}

static void initElevatorTrap(int delay)
{
    initelevGroup('L', m_liftGroupL, m_liftPitGroupL);
    initelevGroup('C', m_liftGroupC, m_liftPitGroupC);
    initelevGroup('R', m_liftGroupR, m_liftPitGroupR);
    int z=CreateObjectAt("VileZombie", LocationX(185), LocationY(185));

    AggressionLevel(z, 0.0);
    LookWithAngle(z, 183);
    PushTimerQueue(delay, z, deferredWalk);
}

#define INNER_RING_COUNT 24
#define OUTER_RING_COUNT 36
#define RING_CENTER_LOCATION 186

static void makeExitInnerRing()
{
    int arr[INNER_RING_COUNT], rep=INNER_RING_COUNT;
    string n="Flame";
    float x=LocationX(RING_CENTER_LOCATION),y=LocationY(RING_CENTER_LOCATION);

    while (--rep>=0)
        arr[rep]=CreateObjectAt(n, x+MathSine(rep*15 +90, 38.0), y+MathSine(rep*15, 38.0));

    m_innerExitRing=arr;
    m_innerExitRingCount=INNER_RING_COUNT;
}

static void makeExitOuterRing()
{
    int arr[OUTER_RING_COUNT], rep=OUTER_RING_COUNT;
    string n="BlueFlame";
    float x=LocationX(RING_CENTER_LOCATION),y=LocationY(RING_CENTER_LOCATION);

    while (--rep>=0)
        arr[rep]=CreateObjectAt(n, x+MathSine(rep*10 +90, 47.0), y+MathSine(rep*10, 47.0));

    m_outerExitRing=arr;
    m_outerExitRingCount=OUTER_RING_COUNT;
}

static void initExitRing()
{ makeExitInnerRing();makeExitOuterRing();}

static void initialPlaceMovingFlame(int count)
{
    while (--count>=0)
        PushTimerQueue(Random(1, 90),0, placeMovingFlame);
}

static void deferredInit()
{
    initPatrolFlames();
    initFlameWalls();
    initialPlaceMovingFlame(16);
    PushTimerQueue(10, 1, startBlockTraps);
    FrameTimer(20, initJunkyardDog);
    initToggleTeleportingSystem();
    SecondTimer(6, showThisMapInfo);
    PushTimerQueue(30, 1, initElevatorTrap);
    FrameTimer(31, initExitRing);
}

static void initReadable()
{
    RegistSignMessage(Object("mpic1"), "고양이 탈출- 제작. 237");
    RegistSignMessage(Object("mpic2"), "게임방법- 방향키를 사용하여 고양이를 조종하세요. 고양이를 조종하여 탈출하세요");
}

void MapInitialize()
{
    MusicEvent();
    m_lastCreatedUnit=CreateObjectAt("SpellBook", 100.0, 100.0);
    m_clientKeyState = 0x751000;

    PushTimerQueue(1, Object("firstscan"),startUnitScan);
    initialServerPatch();
    initReadable();
    initPatrolFlames();
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(60, serverInputLoop);

    HashCreateInstance(&m_unitscanHashInstance);
    HashPushback(m_unitscanHashInstance, 2675, createWallBox);
    HashPushback(m_unitscanHashInstance, 2673, createReviveStuff);

    HashCreateInstance(& m_creatureHash );
}

static int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(m_player[pIndex]))
    {
        return m_player[pIndex]==pUnit;
    }
    return FALSE;
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (checkIsPlayerAlive(GetPlayerIndex(OTHER), GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(2), LocationY(2));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

static void makeCongrat(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int du=GetDirection(sub);

        if (du)
        {
            PushTimerQueue(3, sub, makeCongrat);
            LookWithAngle(sub, du-1);
            CreateObjectAt("OgreBench3", GetObjectX(sub), GetObjectY(sub));
            return;
        }
        Delete(sub);
    }
}

void VictoryEvent()
{
    if (!IsMonsterUnit(OTHER))
        return;

    int owner = GetOwner(OTHER);

    if (CurrentHealth(owner))
    {
        ObjectOff(SELF);

        UniPrint(owner, "축하합니다! - 당신은 우승자 입니다!");
        char buff[192], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(owner));

        NoxSprintfString(buff, "축하합니다! 오늘의 승자는 %s 님입니다. 모든 역경과 고난을 이기고 골인했습니다", &p, 1);
        UniPrintToAll(ReadStringAddressEx(buff));
        CreateObjectAt("ManaBombCharge", LocationX(181), LocationY(181));
        PlaySoundAround(SELF, SOUND_FlagCapture);
        int sub=CreateObjectAt("InvisibleLightBlueLow", LocationX(181), LocationY(181));

        LookWithAngle(sub, 8);
        PushTimerQueue(1, sub, makeCongrat);
    }
}

void RemoveFlames()
{
    ObjectOff(SELF);
    Delete(m_flameWalls[0]);
    Delete(m_flameWalls[1]);
    Delete(m_flameWalls[2]);
}

void RemoveFlames4()
{
    ObjectOff(SELF);
    Delete(m_flameWalls4[0]);
    Delete(m_flameWalls4[1]);
    Delete(m_flameWalls4[2]);
    Delete(m_flameWalls4[3]);
    Delete(m_flameWalls4[4]);
}

void RemoveFlames5()
{
    ObjectOff(SELF);
    Delete(m_flameWalls5[0]);
    Delete(m_flameWalls5[1]);
    Delete(m_flameWalls5[2]);
    Delete(m_flameWalls5[3]);
    Delete(m_flameWalls5[4]);
}

void RemoveFlames3()
{
    ObjectOff(SELF);
    Delete(m_flameWalls3[0]);
    Delete(m_flameWalls3[1]);
    Delete(m_flameWalls3[2]);
    Delete(m_flameWalls3[3]);
    Delete(m_flameWalls3[4]);
}

void RemoveFlames2()
{
    ObjectOff(SELF);
    Delete(m_flameWalls2[0]);
    Delete(m_flameWalls2[1]);
    Delete(m_flameWalls2[2]);
    Delete(m_flameWalls2[3]);
    Delete(m_flameWalls2[4]);
}

static int commonFpsCheck()
{
    int *cFps = 0x84ea04, tmp;

    if (ABS(cFps[0]-tmp) > 48)
    {
        tmp=cFps[0];
        return TRUE;
    }
    return FALSE;
}

int *m_prevLift=0;
int *m_prevPit=0;

void LeftLiftComplexOn()
{
    if (!commonFpsCheck())
        return;
    if (m_prevLift==&m_liftGroupL)
        return;

    if (m_prevLift)
        liftComplexOn(m_prevLift, m_prevPit);

    m_prevLift=&m_liftGroupL;
    m_prevPit=&m_liftPitGroupL;
    liftComplexOn(m_prevLift, m_prevPit);
}

void RightLiftComplexOn()
{
    if (!commonFpsCheck())
        return;
    if (m_prevLift==m_liftGroupR)
        return;

    if (m_prevLift)
        liftComplexOn(m_prevLift, m_prevPit);

    m_prevLift=m_liftGroupR;
    m_prevPit=m_liftPitGroupR;
    liftComplexOn(m_prevLift, m_prevPit);
}

void CenterLiftComplexOn()
{
    if (!commonFpsCheck())
        return;
    if (m_prevLift==m_liftGroupC)
        return;

    if (m_prevLift)
        liftComplexOn(m_prevLift, m_prevPit);

    m_prevLift=m_liftGroupC;
    m_prevPit=m_liftPitGroupC;
    liftComplexOn(m_prevLift, m_prevPit);
}

static void turnOffRing(int *p, int count)
{
    if (!IsObjectOn( p[0]) )
        return;

    while (--count>=0)
        ObjectOff(p[count]);
}

static void turnOnRing(int *p, int count)
{
    if (IsObjectOn(p[0]))
        return;

    while(--count>=0)
        ObjectOn(p[count]);
}

void TurnOffInnerRing()
{
    ObjectOff(SELF);
    PushTimerQueue(82, GetTrigger(), wrapObjectOn);
    turnOffRing(m_innerExitRing, INNER_RING_COUNT);
}

void TurnOffOuterRing()
{
    ObjectOff(SELF);
    PushTimerQueue(82, GetTrigger(), wrapObjectOn);
    turnOffRing(m_outerExitRing, OUTER_RING_COUNT);
}

void TurnOnBoth()
{
    ObjectOff(SELF);
    PushTimerQueue(76, GetTrigger(), wrapObjectOn);
    turnOnRing(m_innerExitRing, INNER_RING_COUNT);
    turnOnRing(m_outerExitRing, OUTER_RING_COUNT);
    PlaySoundAround(SELF, SOUND_FireGrate);
}
