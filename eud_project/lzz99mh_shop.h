
#include "lzz99mh_gvar.h"
#include "lzz99mh_utils.h"
#include "libs/shop.h"
#include "libs/itemproperty.h"
#include "libs/objectIDdefines.h"
#include "libs/waypoint.h"
#include "libs/format.h"
#include "libs/printutil.h"
#include "libs/buff.h"
#include "libs/fixtellstory.h"
#include "libs/gui_window.h"
#include "libs/networkRev.h"
#include "libs/bind.h"

#define SPECIAL_WEAPON_MAX_COUNT 5

void onPopupMessageChanged(int messageId)
{
    GUISetWindowScrollListboxText(GetDialogCtx(), GetPopupMessage(messageId), NULLPTR);
}

void deferredChangeDialogContext(int *pParams) //override
{
    int user=pParams[0];
    int message=pParams[1];

    FreeSmartMemEx(pParams);
    if (!CurrentHealth(user))
        return;

    if (user==GetHost())
    {
        onPopupMessageChanged(message);
        return;
    }
    ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, message);
}

void sendChangeDialogCtx(int user, int param)
{
    int params[]={
        user,
        param,
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
}

void openShopForced()
{
    char openshopcode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 
    0x10, 0xEF, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3, 0x90
};int args[]={UnitToPtr(SELF),UnitToPtr(OTHER)};
int *pExec=0x5c336c;
		int *pOld=pExec[0];
		pExec[0]= openshopcode;
		Unknownb8(args);
		pExec[0]=pOld;
}

int dispositionShop(short thingId, float xpos, float ypos)
{
    int lh=DummyUnitCreateById(thingId, xpos, ypos);
    int ptr=UnitToPtr(lh);

    SetMemory(ptr+12,GetMemory(ptr+12)^8);
    int p=MemAlloc(0x6c0);
    NoxByteMemset(p, 0x6c0, 0);
    SetMemory(ptr+0x2b4, p);
    SetDialog(lh,"NORMAL",openShopForced,openShopForced);
    return lh;
}

int placingArmorShop(short location, int property1, int property2)
{
    int shop= dispositionShop(OBJ_HORRENDOUS, LocationX(location), LocationY(location));

    UnitNoCollide(shop);
    ShopUtilSetTradePrice(shop, 1.9, 0.0);
    ShopUtilAppendItemWithProperties(shop, OBJ_ORNATE_HELM, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_BREASTPLATE, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_PLATE_ARMS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_PLATE_BOOTS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_PLATE_LEGGINGS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_MEDIEVAL_CLOAK, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_MEDIEVAL_PANTS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_MEDIEVAL_SHIRT, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_STEEL_SHIELD, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    LookWithAngle(shop, 92);
    return shop;
}

int placingWeaponShop(short location, int property1, int property2)
{
    int shop=dispositionShop(OBJ_SWORDSMAN,LocationX(location),LocationY(location));
    UnitNoCollide(shop);
    ShopUtilSetTradePrice(shop,1.6,0.0);
    ShopUtilAppendItemWithProperties(shop,OBJ_WAR_HAMMER,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_GREAT_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_ROUND_CHAKRAM,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_BATTLE_AXE,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_WAR_HAMMER,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_GREAT_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_ROUND_CHAKRAM,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_BATTLE_AXE,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    LookWithAngle(shop,92);
    return shop;
}

void specialWeaponDesc()
{
    int pIndex=GetPlayerIndex(OTHER);
    int *p;

    if (!HashGet(GenericHash(), GetTrigger(), &p, FALSE))
        return;

    int cur= p[pIndex];
    sendChangeDialogCtx(GetCaller(), GetSpecialWeaponMessage(cur));
    TellStoryUnitName("AA", "MainBG.wnd:Loading", GetSpecialWeaponName(cur));
}

int CreateSpecialWeaponProto(int functionId, float x,float y)
{
    return Bind(functionId, &functionId + 4) ;
}

void specialWeaponTrade()
{
    int pIndex=GetPlayerIndex(OTHER), dlgRes = GetAnswer(SELF);
    int *p;

    if (!HashGet(GenericHash(), GetTrigger(), &p, FALSE))
        return;
    int cur=p[pIndex];
    if (dlgRes==2)
    {
        UniPrint(OTHER, "'아니오'을 누르셨어요. 다음 항목을 보여드리겠어요");
        p[pIndex]=(cur+1)%SPECIAL_WEAPON_MAX_COUNT;
        specialWeaponDesc();
    }
    else if (dlgRes==1)
    {
        int pay=GetSpecialWeaponPay(cur);
        if (GetGold(OTHER)>=pay)
        {
            ChangeGold(OTHER, -pay);
            CreateSpecialWeaponProto(GetSpecialWeaponFunction(cur), GetObjectX(OTHER), GetObjectY(OTHER));
            UniPrint(OTHER, "거래완료!- 당신 아래에 있어요");
        }
        else
            UniPrint(OTHER, "거래실패!- 잔액이 부족합니다");
    }
}

int PlaceSpecialWeaponShop(int location)
{
    int s=DummyUnitCreateById(OBJ_WIZARD_WHITE, LocationX(location), LocationY(location));

    SetDialog(s, "YESNO", specialWeaponDesc, specialWeaponTrade);
    StoryPic(s,"HorvathPic");
    int *p;

    AllocSmartMemEx(32*4, &p);
    NoxDwordMemset(p, 32, 0);
    HashPushback(GenericHash(), s, p);
    return s;
}

int SetInvulnerabilityItem(int unit)
{
    int count = 0, inv = GetLastItem(unit);

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void tradeInvincibleItem()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=3000)
    {
        ChangeGold(OTHER, -3000);
        int count = SetInvulnerabilityItem(OTHER);
        char buff[128];

        NoxSprintfString(buff, "%d개 아이템이 처리되었습니다", &count,1);
        UniPrint(OTHER,ReadStringAddressEx(buff));
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descInvincibleItem()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "인벤토리 무적화");
}

int placingInvincibleItemShop(short location)
{
    int s=DummyUnitCreateById(OBJ_ARCHER, LocationX(location),LocationY(location));

    SetDialog(s,"YESNO",descInvincibleItem,tradeInvincibleItem);
    StoryPic(s,"WarriorPic");
    return s;
}

void descTeleportToLastPosition()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_MESSAGE_TELEPORT_TO_LAST);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "마지막 위치로\n이동");
}

void tradeTeleportToLastPosition()
{
    if (GetAnswer(SELF)^1)
        return;

    int pIndex=GetPlayerIndex(OTHER);
    int respawn=GetUserRespawnMark(pIndex);
    float x=GetObjectX(respawn),y=GetObjectY(respawn);

    Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER),0.0,0.0);
    MoveObject(OTHER, x,y);
    Effect("TELEPORT", x,y,0.0,0.0);
    PlaySoundAround(OTHER, SOUND_BlindOff);
}

int placingTeleportOption(short location)
{
    int t=DummyUnitCreateById(OBJ_BOMBER,LocationX(location),LocationY(location));

    SetDialog(t,"YESNO",descTeleportToLastPosition,tradeTeleportToLastPosition);
    StoryPic(t,"AldwynPic");
    LookWithAngle(t,64);
    return t;
}

void descTeleportToHome()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_MESSAGE_TELEPORT_TO_HOME);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "마을지역으로\n이동");
}

void tradeTeleportToHome()
{
    if (GetAnswer(SELF)^1)
        return;

    float x= LocationX(86), y=LocationY(86);

    Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER),0.0,0.0);
    MoveObject(OTHER, x,y);
    Effect("TELEPORT", x,y,0.0,0.0);
    PlaySoundAround(OTHER, SOUND_BlindOff);
}

int placingTeleportHome(short location)
{
    int t=DummyUnitCreateById(OBJ_URCHIN_SHAMAN,LocationX(location),LocationY(location));

    SetDialog(t,"YESNO",descTeleportToHome,tradeTeleportToHome);
    StoryPic(t,"AldwynPic");
    LookWithAngle(t,64);
    return t;
}

void InitializeShopSystem()
{
    placingArmorShop(111,ITEM_PROPERTY_LightningProtect4,ITEM_PROPERTY_FireProtect4);
    placingArmorShop(112,ITEM_PROPERTY_Regeneration4,ITEM_PROPERTY_PoisonProtect4);
    placingArmorShop(113,ITEM_PROPERTY_Regeneration4,ITEM_PROPERTY_Speed4);
    placingWeaponShop(114,ITEM_PROPERTY_lightning4,ITEM_PROPERTY_stun4);
    placingWeaponShop(115,ITEM_PROPERTY_vampirism4,ITEM_PROPERTY_fire4);
    placingWeaponShop(116,ITEM_PROPERTY_venom4,ITEM_PROPERTY_vampirism4);
    PlaceSpecialWeaponShop(117);
    placingInvincibleItemShop(118);
    placingTeleportOption(188);
    placingTeleportHome(193);
    placingTeleportHome(194);
    placingTeleportHome(195);
    placingTeleportHome(200);
    placingTeleportHome(201);
    placingTeleportHome(223);
    placingTeleportHome(225);
    placingTeleportHome(228);
    placingTeleportHome(230);
    placingTeleportHome(232);
    placingTeleportHome(289);
    placingTeleportHome(96);
    placingTeleportHome(308);
    placingTeleportHome(334);
    placingTeleportHome(336);
    placingTeleportHome(338);
    placingTeleportHome(396);
}

