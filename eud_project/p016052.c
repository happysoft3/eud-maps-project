
#include "noxscript\builtins.h"

#include "libs\typecast.h"
#include "libs\callmethod.h"
#include "libs\opcodehelper.h"
#include "libs\unitstruct.h"
#include "libs\unitutil.h"
#include "libs\printutil.h"
#include "libs\username.h"
#include "libs\mathlab.h"

#include "libs\waypoint.h"
#include "libs\observer.h"
#include "libs\playerinfo.h"
#include "libs\network.h"
#include "libs\clientside.h"

int LastUnitID = 60;
int ScreenMover, MovingScreen;
float XRange = 270.0;
int StartPtr, PlrCnt, Switch1, SwUnit1, GameEnd = 0;
int ColorR[10], ColorG[10], ColorB[10];
int player[20], PlrCre[10], PlrCam[10];
int FireWay[12];
int StartPointNumber = 4, StartButtonPos = 9, GoToLocation = 11;
//int StartPointNumber = 389, StartButtonPos = 391, GoToLocation = 247;

#define NULLPTR 0

int GetPlayerScrIndex(int pUnit)
{
    int i;

    for (i = 9 ; i >= 0 ; Nop(i --))
    {
        if (!(pUnit ^ player[i]))
            return i;
    }
    return -1;
}

static int NetworkUtilClientMain()
{
    PlayerClassCommonWhenEntry();
}

int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, LastUnitID);
        }
    }
    LastUnitID ++;

    return unitMover;
}

void Nop(int n)
{ }

void MapExit()
{
    MusicEvent();
}

void InitGameScreen()       //Not yet
{
    ScreenMover = Object("MovingScreenMover");
    MovingScreen = Object("ScreenMoving");
}

void GameGuideMessage()
{
    FrameTimer(10, ChatMessageLoop);
    //UniBroadcast("게임방법:\n화면 중심부의 초록 선을 벗어나게 되면 죽게됩니다\n화면 안에서 장애물을 피해 최종 장소에 도착하면 승리합니다");
}

void StartButtonCollide()
{
    if (!GetDirection(self))
    {
        SetCallback(self, 9, PlayerRegist);
        FrameTimer(30, GameGuideMessage);
        LookWithAngle(self, 1);
    }
}

void PutStartButton(int loc)
{
    int unit = CreateObject("CarnivorousPlant", loc);

    SetCallback(unit, 9, StartButtonCollide);
    LookWithAngle(unit, 0);
    Damage(unit, 0, 9999, -1);
}

void PlayerClassFastJoin()
{
    int plr = CheckPlayer();

    if (CurrentHealth(other))
    {
        if (plr + 1)
            MoveObject(other, LocationX(388), LocationY(388));
        else
            MoveObject(other, LocationX(392), LocationY(392));
    }
}

void PutFastJoinButton(int loc)
{
    int unit = CreateObject("CarnivorousPlant", loc);

    SetCallback(unit, 9, PlayerClassFastJoin);
    Damage(unit, 0, 9999, -1);
}

void InitUseMapSetting()
{
    PlacedObstacle(10);
    InitMovingDemonTraps();
    InitWispBridge();           //OK
    FrameTimer(1, ZigegObsticle);
    FrameTimer(1, InitSlideTraps);
    PutUnitBlock1();
    PutUnitBlock2();
    PutUnitBlock3();
    PutUnitBlock4();
    FrameTimer(30, PutSpinTraps);
    FrameTimer(30, RevSpinTraps);   //OK

    MakeRightBlocks(220, 8);
    MakeUpBlocks(222, 9);
    MakeRightBlocks(224, 7);

    FrameTimer(30, PatrolBlocks);
    FrameTimer(30, SquareSpinRots);
    //FrameTimer(31, ArrowTower); //error

    FrameTimer(105, MapDecorations);
    FrameTimerWithArg(130, 0, ControlAvoidBombTraps);
    FrameTimer(1, ObeliskBlockRow);
    FrameTimer(100, MovingObeliskTrap);
    PutStartButton(388);
    PutFastJoinButton(393);
    FrameTimer(30, StrStartArrow);
}

int FistTrap(int wp)
{
    int unit = CreateObject("CarnivorousPlant", wp);

    Enchant(CreateObject("InvisibleLightBlueMed", wp), "ENCHANT_ANCHORED", 0.0);
    SetCallback(unit, 9, DropFist);
    Damage(unit, 0, 999, -1);
    return unit;
}

void AllocationFistTraps()
{
    int count, k;

    if ((count++) < 10)
    {
        for (k = 0 ; k < 6 ; Nop(++k))
        {
            Nop(FistTrap(354 + k));
            TeleportLocationVector(354 + k, 46.0, -46.0);
        }
        FrameTimer(1, AllocationFistTraps);
    }
}

void DropFist()
{
    if (CurrentHealth(other))
    {
        if (IsObjectOn(GetTrigger() + 1))
        {
            TeleportLocation(341, GetObjectX(self), GetObjectY(self));
            EnchantOff(GetTrigger() + 1, "ENCHANT_ANCHORED");
            AudioEvent("PoisonTrapTriggered", 341);
            ObjectOff(GetTrigger() + 1);
            CastSpellObjectObject("SPELL_FIST", GetTrigger() + 1, GetTrigger() + 1);
            FrameTimerWithArg(82, GetTrigger(), ResetFistTrap);
        }
    }
}

void ResetFistTrap(int unit)
{
    Enchant(unit + 1, "ENCHANT_ANCHORED", 0.0);
    ObjectOn(unit + 1);
    TeleportLocation(341, GetObjectX(unit), GetObjectY(unit));
    AudioEvent("PotionPickup", 341);
}

void MovingObeliskTrap()
{
    MovingOb(350, 346);
    MovingOb(351, 348);
    FrameTimer(50, MovingObeliskTrap);
}

int MovingOb(int wp, int dest)
{
    int unit = CreateObject("WizardGenerator", wp);

    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    SetMemory(GetMemory(GetMemory(0x750710) + 0x2ec) + 0x4c, DestGen);
    ObjectOn(CreateMoverFix(unit, dest, 20.0));
    LookWithAngle(unit + 1, 1);
    return unit;
}

void GenStop(int wp)
{
    int unit = CreateObject("Maiden", wp);
    Frozen(unit, 1);
    SetCallback(unit, 9, RemoveGen);
}

void RemoveGen()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        if (GetDirection(GetCaller() + 1))
        {
            Delete(other);
            Delete(GetCaller() + 1);
        }
    }
}

void DestGen()
{
    Effect("EXPLOSION", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        Delete(self);
        Delete(GetTrigger() + 1);
    }
    else if (CurrentHealth(other))
    {
        if (HasEnchant(other, "ENCHANT_CROWN"))
        {
            Damage(other, 0, 999, 14);
        }
    }
}

void ObeliskBlockRow()
{
    int count;

    if ((count++) < 15)
    {
        SpawnObelisk(342);
        SpawnObelisk(343);
        SpawnObelisk(344);
        SpawnObelisk(345);
        TeleportLocationVector(342, 46.0, -46.0);
        TeleportLocationVector(343, 46.0, -46.0);
        TeleportLocationVector(344, 46.0, -46.0);
        TeleportLocationVector(345, 46.0, -46.0);
        FrameTimer(1, ObeliskBlockRow);
    }
    else
        AllocationFistTraps();
}

int SpawnObelisk(int wp)
{
    int unit = CreateObject("MonsterGenerator", wp);

    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    SetMemory(GetMemory(GetMemory(0x750710) + 0x2ec) + 0x4c, GenerateCollideEvent);
    ObjectOff(unit);
    return unit;
}

void ControlAvoidBombTraps(int flag)
{
    int time;
    //(329, 330) (331, 332) (333, 334)
    //(335, 336) (337, 338) (339, 340)
    AvoidBombUp(329 + (flag * 2));
    AvoidBombUp(330 + (flag * 2));
    FrameTimerWithArg(12, 335 + (flag * 2), AvoidBombDown);
    FrameTimerWithArg(12, 336 + (flag * 2), AvoidBombDown);
    if (flag % 3 == 2)
        time = 40;
    else
        time = 24;
    FrameTimerWithArg(time, (flag + 1) % 3, ControlAvoidBombTraps);
}

void AvoidBombDown(int wp)
{
    float x = LocationX(wp), y = LocationY(wp);
    int k, ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1;

    Delete(ptr - 1);
    for (k = 0 ; k < 7 ; Nop(k ++))
    {
        ObjectOff(CreateObject("WizardGenerator", wp));
        DeleteObjectTimer(ptr + (k * 2), 1);
        SetMemory(GetMemory(GetMemory(0x750710) + 0x2ec) + 0x4c, GenerateCollideEvent);
        DeleteObjectTimer(CreateObject("MeteorExplode", wp), 30);
        AudioEvent("GolemHitting", wp);
        TeleportLocationVector(wp, -46.0, 46.0);
    }
    TeleportLocation(wp, x, y);
}

void AvoidBombUp(int wp)
{
    float x = LocationX(wp), y = LocationY(wp);
    int k, ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1;

    Delete(ptr - 1);
    for (k = 0 ; k < 7 ; Nop(k ++))
    {
        ObjectOff(CreateObject("WizardGenerator", wp));
        DeleteObjectTimer(ptr + (k * 2), 1);
        SetMemory(GetMemory(GetMemory(0x750710) + 0x2ec) + 0x4c, GenerateCollideEvent);
        DeleteObjectTimer(CreateObject("MeteorExplode", wp), 30);
        AudioEvent("GolemHitting", wp);
        TeleportLocationVector(wp, 46.0, -46.0);
    }
    MoveWaypoint(wp, x, y);
}

void GenerateCollideEvent()
{
    if (CurrentHealth(other))
    {
        if (HasEnchant(other, "ENCHANT_CROWN"))
        {
            Damage(other, 0, 999, 14);
        }
    }
}

void MapDecorations()
{
    CreateObject("DeathScreenJunk", 313);
    UnitBlockCreate(314, 5, -30.0, -30.0);
    UnitBlockCreate(315, 6, 30.0, 30.0);
    UnitBlockCreate(316, 6, -30.0, -30.0);
    UnitBlockCreate(317, 6, 30.0, 30.0);
    UnitBlockCreate(318, 6, -30.0, -30.0);
    UnitBlockCreate(319, 6, 30.0, 30.0);
    UnitBlockCreate(320, 6, -30.0, -30.0);
    UnitBlockCreate(321, 6, 30.0, 30.0);
    UnitBlockCreate(322, 6, -30.0, -30.0);
    FastAllocPatrolBlock(323);
    FastAllocPatrolBlock(325);
    FrameTimer(1, StrDestLocation);
    FrameTimer(1, StrReviveTeam);
    FrameTimerWithArg(30, 363, SpawnExit);
    SpawnObelisk(347);
    GenStop(347);
    GenStop(349);
    SpawnObelisk(349);
    SpawnObelisk(352);
    SpawnObelisk(353);
}

int RiseFires(int wp)
{
    int k, ptr = CreateObject("RedPotion", wp) + 1;
    float x = LocationX(wp), y = LocationY(wp);

    Delete(ptr - 1);
    for (k = 0 ; k < 17 ; Nop(k ++))
    {
        Enchant(CreateObject("Flame", wp), "ENCHANT_FREEZE", 0.0);
        TeleportLocationVector(wp, 23.0, -23.0);
    }
    TeleportLocation(wp, x, y);
    return ptr;
}

void SetFireways(int count)
{
    if (count < 12)
    {
        FireWay[count] = RiseFires(366 + count);
        FrameTimerWithArg(1, ++count, SetFireways);
    }
}

void ClearFireWay()
{
    int k;
    for (k = 0 ; k < 12 ; Nop(++k))
        ClearFWayByIndex(k);
}

void ClearFWayByIndex(int idx)
{
    int k;

    for (k = 0 ; k < 17 ; Nop(k ++))
    {
        if (IsObjectOn(FireWay[idx] + k))
            Delete(FireWay[idx] + k);
    }
}

void WaterTank(int wp, int target)
{
    int unit = CreateObject("Wizard", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Damage(unit, 0, 999, -1);
    SetCallback(unit, 9, ShootOut);
    FrameTimerWithArg(20, unit, ShootWaterLoop);
}

void ShootOut()
{
    if (CurrentHealth(other))
    {
        if (GetOwner(GetTrigger() + 1) != GetCaller())
        {
            int subsub;

            if (IsObjectOn(subsub))
                Delete(subsub);
            TeleportLocation(341, GetObjectX(other), GetObjectY(other));
            subsub = CreateObject("InvisibleLightBlueHigh", 341);
            SetOwner(self, subsub);
            AudioEvent("SmallGong", 341);
            GreenSparkFx(341);
            SetOwner(other, GetTrigger() + 1);
            FrameTimerWithArg(1, subsub, ShootWaterLoop);
        }
    }
}

void ShootWaterLoop(int ptr)
{
    int unit = GetOwner(ptr), owner, wt;

    if (IsObjectOn(ptr))
    {
        owner = GetOwner(unit + 1);
        if (CurrentHealth(owner))
        {
            MoveWaypoint(341, GetObjectX(unit + 1), GetObjectY(unit + 1));
            wt = CreateObject("WaterBarrel", 341);
            Raise(CreateObject("InvisibleLightBlueHigh", 341), UnitRatioX(unit, owner, 3.0));
            Raise(CreateObject("InvisibleLightBlueHigh", 341), UnitRatioY(unit, owner, 3.0));
            AudioEvent("TowerShoot", 341);
            FrameTimerWithArg(1, wt, MovingWaterBarrel);
        }
        FrameTimerWithArg(20, ptr, ShootWaterLoop);
    }
}

void MovingWaterBarrel(int unit)
{
    int count = GetDirection(unit);

    if (IsObjectOn(unit))
    {
        if (count < 250)
        {
            LookWithAngle(unit, ++count);
            PushObjectTo(unit, GetObjectZ(unit + 1), GetObjectZ(unit + 2));
        }
        else
            Delete(unit);
        FrameTimerWithArg(1, unit, MovingWaterBarrel);
    }
    else
    {
        Delete(unit + 1);
        Delete(unit + 2);
    }
}

void SpawnExit(int wp)
{
    int unit = CreateObject("Hecubah", wp);
    int ptr = GetMemory(0x750710);

    SetCallback(unit, 9, VictoryEvent);
    ObjectOff(unit);
    Damage(unit, 0, 999, 14);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e0, 38817);
}

void VictoryEvent()
{
    if (CurrentHealth(other))
    {
        GameEnd = 1;
        Delete(self);
        MoveWaypoint(313, GetObjectX(self), GetObjectY(self));
        AudioEvent("LevelUp", 313);
        DeleteObjectTimer(CreateObject("LevelUp", 313), 1200);
        UniPrintToAll("축하드립니다_ 맵을 모두 클리어 하셨습니다!");
        Effect("WHITE_FLASH", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
    }
}

int UnitBlockCreate(int wp, int max, float x_vect, float y_vect)
{
    int k, ptr = CreateObject("InvisibleLightBlueLow", wp) + 1;
    Delete(ptr - 1);

    for (k = 0 ; k < max ; Nop(k ++))
    {
        Frozen(CreateObject("AirshipCaptain", wp), 1);
        SetCallback(ptr + k, 9, FireTouch);
        TeleportLocationVector(wp, x_vect, y_vect);
    }
    return ptr;
}

void ArrowTower()
{
    int unit = CreateObject("Maiden", 311);
    CreateObject("ForceOfNatureCharge", 311);
    Frozen(unit, 1);
    SetCallback(unit, 9, FireTouch);
    FrameTimerWithArg(1, unit, CheckUnitAround);
}

void CheckUnitAround(int unit)
{
    if (CurrentHealth(unit))
    {
        DetectEnemy(unit, 500.0, 312);
        FrameTimerWithArg(3, unit, CheckUnitAround);
    }
}

void DetectEnemy(int owner, float range, int wp)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1, k;

    SetOwner(owner, ptr - 1);
    MoveObject(ptr - 1, range, GetObjectX(ptr - 1));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObject("WeirdlingBeast", wp), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, 32 * k);
        SetOwner(ptr - 1, ptr + k);
        SetCallback(ptr + k, 3, Splash);
    }
    DeleteObjectTimer(ptr - 1, 2);
}

void Splash()
{
    if (!HasEnchant(other, "ENCHANT_VILLAIN"))
    {
        if (DistanceUnitToUnit(other, self) <= GetObjectX(GetOwner(self)))
        {
            Enchant(other, "ENCHANT_VILLAIN", 0.1);
            int mis = CreateObjectAt("ArcherBolt", GetObjectX(self) + UnitRatioX(other, self, 17.0), GetObjectY(self) + UnitRatioY(other, self, 17.0));
            Enchant(mis, "ENCHANT_FREEZE", 0.0);
            SetOwner(self, mis);
            PushObjectTo(mis, UnitRatioX(other, self, 23.0), UnitRatioY(other, self, 23.0));
        }
    }
}

void PatrolBlocks()
{
    AllocPatrolBlock(249);
    AllocPatrolBlock(251);
    AllocPatrolBlock(253);
    AllocPatrolBlock(255);
    AllocPatrolBlock(257);
    AllocPatrolBlock(259);
    AllocPatrolBlock(261);
    AllocPatrolBlock(263);
    AllocPatrolBlock(265);
    AllocPatrolBlock(267);
    AllocPatrolBlock(269);
    AllocCrossBlock(271, 272, 55.0);
    AllocCrossBlock(272, 271, 55.0);
    AllocCrossBlock(273, 274, 55.0);
    AllocCrossBlock(274, 273, 55.0);
}

void SquareSpinRots()
{
    AllocCrossBlock(275, 276, 25.0);
    AllocCrossBlock(284, 283, 25.0);
    AllocCrossBlock(308, 307, 25.0);
    AllocCrossBlock(292, 291, 25.0);
    AllocCrossBlock(300, 299, 25.0);
    AllocCrossBlock(303, 304, 25.0);
    AllocCrossBlock(290, 289, 25.0);
    AllocCrossBlock(298, 297, 25.0);
    AllocCrossBlock(280, 281, 25.0);
    AllocCrossBlock(304, 305, 25.0);
}

int AllocCrossBlock(int wp, int dest, float speed)
{
    int unit = CreateObject("Maiden", wp);
    Frozen(unit, 1);
    SetCallback(unit, 9, FireTouch);
    ObjectOn(CreateMoverFix(CreateObject("Magic", wp), dest, speed));
    Frozen(unit + 1, 1);
    ObjectOn(CreateMoverFix(unit, dest, speed));
    return unit;
}

int AllocPatrolBlock(int wp)
{
    int unit = CreateObject("Maiden", wp);
    Frozen(unit, 1);
    SetCallback(unit, 9, FireTouch);
    ObjectOn(CreateMoverFix(CreateObject("ImpShot", wp), wp + 1, 40.0));
    Frozen(unit + 1, 1);
    ObjectOn(CreateMoverFix(unit, wp + 1, 40.0));
    return unit;
}

int FastAllocPatrolBlock(int wp)
{
    int unit = CreateObject("Maiden", wp);
    Frozen(unit, 1);
    SetCallback(unit, 9, FireTouch);
    ObjectOn(CreateMoverFix(CreateObject("MagicMissile", wp), wp + 1, 96.0));
    Frozen(unit + 1, 1);
    ObjectOn(CreateMoverFix(unit, wp + 1, 96.0));
    return unit;
}

void PutSpinTraps()
{
    int k;

    for (k = 0 ; k < 9 ; Nop( k ++))
    {
        SpinTrap(241, 157, k);
        SpinTrap(242, 157, k);
        SpinTrap(243, 157, k);
        TeleportLocationVector(241, -108.0, 108.0);
        TeleportLocationVector(242, -108.0, 108.0);
        TeleportLocationVector(243, -108.0, 108.0);
    }
}

void RevSpinTraps()
{
    int k;

    for (k = 0 ; k < 3 ; Nop(k ++))
    {
        SpinTrap(244, 67, k);
        SpinTrap(245, 67, k);
        SpinTrap(246, 67, k);
        TeleportLocationVector(244, -108.0, 108.0);
        TeleportLocationVector(245, -108.0, 108.0);
        TeleportLocationVector(246, -108.0, 108.0);
    }
}

void PreserveSpin(int unit)
{
    int angle = GetDirection(unit);

    MoveObjectVector(unit, MathSine(angle * 2 + 90, 2.1), MathSine(angle * 2, 2.1));
    LookWithAngle(unit, (angle + 1) % 180);
    FrameTimerWithArg(1, unit, PreserveSpin);
}

void RevSpinPreserve(int unit)
{
    int angle = GetDirection(unit);

    MoveObjectVector(unit, MathSine(angle * 2, 2.1), MathSine(angle * 2 + 90, 2.1));
    LookWithAngle(unit, (angle + 1) % 180);
    FrameTimerWithArg(1, unit, RevSpinPreserve);
}

void SpinTrap(int wp, int angle, int rev)
{
    int unit = CreateObjectAt("Fear", LocationX(wp), LocationY(wp));
    int spinAction[] = {PreserveSpin, RevSpinPreserve};

    Frozen(unit, 1);
    LookWithAngle(unit, angle);
    SetUnitCallbackOnCollide(unit, FireTouch);
    FrameTimerWithArg(1, unit, spinAction[rev & 1]);
}

void PutSwitchs()
{
    if (IsObjectOn(SwUnit1))
    {
        Delete(SwUnit1);
        Delete(SwUnit1 + 1);
        Delete(SwUnit1 + 2);
        Delete(SwUnit1 + 3);
    }
    SwUnit1 = CreateObject("WeirdlingBeast", 239);
    SetUnitMaxHealth(GetMemory(0x750710), 100);
    CreateObject("BlueSummons", 239);
    CreateObject("WeirdlingBeast", 240);
    SetUnitMaxHealth(GetMemory(0x750710), 100);
    CreateObject("BlueSummons", 240);
    Damage(SwUnit1, 0, 999, 14);
    Damage(SwUnit1 + 2, 0, 999, 14);
    SetCallback(SwUnit1, 9, Switch1Touch);
    SetCallback(SwUnit1 + 2, 9, Switch1Touch);
}

void Switch1Touch()
{
    if (CurrentHealth(other))
    {
        Delete(self);
        Delete(GetTrigger() + 1);
        if ((++Switch1) == 2)
        {
            Switch1 = 0;
            FireWalls(0);
            UniChatMessage(other, "불기둥이 사라집니다", 90);
        }
    }
}

void ResetFireWalls(int num)
{
    FireWalls(num);
    if (!num)
    {
        FrameTimerWithArg(1, 1, ResetFireWalls);
    }
}

int FireWalls(int arg)
{
    int ptr;
    
    if (arg)
    {
        ptr = CreateObject("Flame", 384);
        CreateObject("Flame", 385);
        CreateObject("Flame", 386);
        return ptr;
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
        return 0;
    }
}

void InitSlideTraps()
{
    int unit = CreateObject("LargeFlame", 189);
    Enchant(CreateObject("LargeFlame", 210), "ENCHANT_FREEZE", 0.0);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    CreateMoverFix(unit, 0, 75.0);
    CreateMoverFix(unit + 1, 0, 75.0);
    FrameTimerWithArg(1, unit, MovingSlide);
    FrameTimerWithArg(1, 13, PutFireWall);
    FrameTimerWithArg(1, 226, SpawnFires);
    FrameTimerWithArg(2, 230, SpawnFires);
    FrameTimerWithArg(10, 10, SpinTrapStandBy);
}

void SpinTrapStandBy(int count)
{
    if (count >= 0)
    {
        SpawnMovingBooks(234, 20.0);
        SpawnMovingBooks(236, 20.0);
        FrameTimerWithArg(8, count - 1, SpinTrapStandBy);
    }
}

string SpellsBookTypename(int num)
{
    string name[] = {"SpellBook", "ConjurerSpellBook", "CommonSpellBook", "AbilityBook", "BlackBook1", "BookOfOblivion"};

    return name[num];
}

void SpawnMovingBooks(int wp, float speed)
{
    int unit = CreateObject("Maiden", wp);

    ObjectOn(CreateMoverFix(unit, wp + 1, speed));
    ObjectOn(CreateMoverFix(CreateObject(SpellsBookTypename(Random(0, 5)), wp), wp + 1, speed));
    Frozen(unit, 1);
    Frozen(unit + 2, 1);
    SetCallback(unit, 9, FireTouch);
}

void SpawnFires(int wp)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1, k;

    Delete(ptr - 1);
    for (k = 0 ; k < 4 ; Nop(k ++))
    {
        ObjectOn(CreateMoverFix(CreateObject("WillOWisp", k + wp), wp + ((k + 1) % 4), 25.0));
        Frozen(ptr + (k * 2), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
    }
}

void MakeRightBlocks(int wp, int max)
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1;
    Delete(ptr - 1);
    for (k = 0 ; k < max ; Nop( k ++) )
    {
        Frozen(CreateObject("FireSprite", wp), 1);
        Frozen(CreateObject("FireSprite", wp + 1), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
        SetCallback(ptr + (k * 2) + 1, 9, FireTouch);
        TeleportLocationVector(wp, 46.0, 0.0);
        TeleportLocationVector(wp + 1, 46.0, 0.0);
    }
}

void MakeUpBlocks(int wp, int max)
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1;
    Delete(ptr - 1);
    for (k = 0 ; k < max ; Nop( k ++) )
    {
        Frozen(CreateObject("FireSprite", wp), 1);
        Frozen(CreateObject("FireSprite", wp + 1), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
        SetCallback(ptr + (k * 2) + 1, 9, FireTouch);
        TeleportLocationVector(wp, 0.0, -46.0);
        TeleportLocationVector(wp + 1, 0.0, -46.0);
    }
}

void PutUnitBlock1()
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 189) + 1;
    Delete(ptr - 1);
    for (k = 0 ; k < 8 ; Nop( k ++) )
    {
        Frozen(CreateObject("FireSprite", 212), 1);
        Frozen(CreateObject("FireSprite", 213), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
        SetCallback(ptr + (k * 2) + 1, 9, FireTouch);
        TeleportLocationVector(212, 46.0, 0.0);
        TeleportLocationVector(213, 46.0, 0.0);
    }
}

void PutUnitBlock3()
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 216) + 1;
    Delete(ptr - 1);
    for (k = 0 ; k < 8 ; Nop( k ++))
    {
        Frozen(CreateObject("FireSprite", 216), 1);
        Frozen(CreateObject("FireSprite", 217), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
        SetCallback(ptr + (k * 2) + 1, 9, FireTouch);
        TeleportLocationVector(216, 46.0, 0.0);
        TeleportLocationVector(217, 46.0, 0.0);
    }
}

void PutUnitBlock2()
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 214) + 1;
    Delete(ptr - 1);
    for (k = 0 ; k < 8 ; Nop( k ++) )
    {
        Frozen(CreateObject("FireSprite", 214), 1);
        Frozen(CreateObject("FireSprite", 215), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
        SetCallback(ptr + (k * 2) + 1, 9, FireTouch);
        TeleportLocationVector(214, 0.0, -46.0);
        TeleportLocationVector(215, 0.0, -46.0);
    }
}

void PutUnitBlock4()
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 218) + 1;
    Delete(ptr - 1);
    for (k = 0 ; k < 8 ; Nop(k ++))
    {
        Frozen(CreateObject("FireSprite", 218), 1);
        Frozen(CreateObject("FireSprite", 219), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
        SetCallback(ptr + (k * 2) + 1, 9, FireTouch);
        TeleportLocationVector(218, 0.0, -46.0);
        TeleportLocationVector(219, 0.0, -46.0);
    }
}

void PutFireWall(int max)
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 186) + 1;
    
    Delete(ptr - 1);
    for (k = 0 ; k < max ; Nop(k ++))
    {
        Frozen(CreateObject("FireSprite", 186), 1);
        Frozen(CreateObject("FireSprite", 187), 1);
        SetCallback(ptr + (k * 2), 9, FireTouch);
        SetCallback(ptr + (k * 2) + 1, 9, FireTouch);
        TeleportLocationVector(186, 46.0, -46.0);
        TeleportLocationVector(187, 46.0, -46.0);
    }
}

void MovingSlide(int unit)
{
    Move(unit, 188);
    Move(unit + 1, 211);
}

void ZigegObsticle()
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 180) + 1;
    Delete(ptr - 1);

    for (k = 0 ; k < 7 ; Nop(k ++))
    {
        Frozen(CreateObject("FireSprite", 180), 1);
        Frozen(CreateObject("FireSprite", 181), 1);
        Frozen(CreateObject("FireSprite", 182), 1);
        Frozen(CreateObject("FireSprite", 183), 1);
        Frozen(CreateObject("FireSprite", 184), 1);
        Frozen(CreateObject("FireSprite", 185), 1);
        SetCallback(ptr + (k * 6), 9, FireTouch);
        SetCallback(ptr + (k * 6) + 1, 9, FireTouch);
        SetCallback(ptr + (k * 6) + 2, 9, FireTouch);
        SetCallback(ptr + (k * 6) + 3, 9, FireTouch);
        SetCallback(ptr + (k * 6) + 4, 9, FireTouch);
        SetCallback(ptr + (k * 6) + 5, 9, FireTouch);
        TeleportLocationVector(180, 24.0, 24.0);
        TeleportLocationVector(181, -24.0, -24.0);
        TeleportLocationVector(182, 24.0, 24.0);
        TeleportLocationVector(183, -24.0, -24.0);
        TeleportLocationVector(184, 24.0, 24.0);
        TeleportLocationVector(185, -24.0, -24.0);
    }
}

void InitMovingDemonTraps()
{
    int count;

    if (count < 3)
    {
        PutDemonMovingTraps(36 + count, 225, 2.0, -2.0);
        PutDemonMovingTraps(41 - count, 225, 2.0, -2.0);
        PutDemonMovingTraps(44 - count, 92, -2.0, 2.0);
        PutDemonMovingTraps(45 + count, 92, -2.0, 2.0);
        count ++;
        SecondTimer(2, InitMovingDemonTraps);
    }
}

void DemonMovingLoop(int unit)
{
    int count = GetDirection(unit + 1);

    if (IsObjectOn(unit))
    {
        if (count < 207)
        {
            MoveObjectVector(unit, GetObjectZ(unit + 1), GetObjectZ(unit + 2));
            LookWithAngle(unit + 1, ++count);
        }
        else
        {
            MoveObject(unit, GetObjectX(unit + 1), GetObjectY(unit + 1));
            LookWithAngle(unit + 1, 0);
        }
        FrameTimerWithArg(1, unit, DemonMovingLoop);
    }
}

void PutDemonMovingTraps(int wp, int dir, float x_vect, float y_vect)
{
    int unit = CreateObject("Demon", wp);

    Raise(CreateObject("InvisibleLightBlueHigh", wp), x_vect);
    Raise(CreateObject("InvisibleLightBlueHigh", wp), y_vect);
    Frozen(unit, 1);
    SetCallback(unit, 9, FireTouch);
    LookWithAngle(unit, dir);
    FrameTimerWithArg(1, unit, DemonMovingLoop);
}

void InitWispBridge()
{
    int count, ptr, link, last;

    if (count < 33)
    {
        ptr = WispSpawn(48 + count);
        WispSpawn2(147 + count);
        link = CreateObject("InvisibleLightBlueHigh", 48 + count);
        Raise(link, ToFloat(ptr));
        SetOwner(last, link);
        last = link;
        count ++;
        FrameTimer(1, InitWispBridge);
    }
    else
        FrameTimerWithArg(1, last, DelayWispOn);
}

int WispSpawn(int wp)
{
    int unit = CreateObject("WillOWisp", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Frozen(unit, 1);
    SetCallback(unit, 9, FireTouch);
    return unit;
}

int WispSpawn2(int wp)
{
    int unit = CreateObject("WillOWisp", wp);

    ObjectOff(CreateObject("InvisibleLightBlueHigh", wp));
    Frozen(unit, 1);
    SetCallback(unit, 9, FireTouch);
    return unit;
}

void MoveObjectVector(int unit, float xVect, float yVect)
{
    int *ptr = UnitToPtr(unit);

    if (ptr != NULLPTR)
    {
        float *coor = ptr;
        MoveObject(unit, coor[14] + xVect, coor[15] + yVect);
    }
}

void DelayWispOn(int ptr)
{
    if (IsObjectOn(ptr))
    {
        WispBridgeMoving(ToInt(GetObjectZ(ptr)));
        FrameTimerWithArg(5, GetOwner(ptr), DelayWispOn);
        Delete(ptr);
    }
}

void WispBridgeMoving(int ptr)
{
    int count = GetDirection(ptr + 1);
    if (IsObjectOn(ptr + 1))
    {
        MoveObjectVector(ptr, 2.0, 2.0);
        MoveObjectVector(ptr + 2, 2.0, 2.0);
    }
    else
    {
        MoveObjectVector(ptr, -2.0, -2.0);
        MoveObjectVector(ptr + 2, -2.0, -2.0);
    }
    if (count < 104)
    {
        LookWithAngle(ptr + 1, count + 2);
    }
    else
    {
        LookWithAngle(ptr + 1, 0);
        ObjectToggle(ptr + 1);
    }
    if (IsObjectOn(ptr))
        FrameTimerWithArg(1, ptr, WispBridgeMoving);
}

void ResetReviveAnkh()
{
    int ankh[10], k;

    for (k = 0 ; k < 10 ; Nop(k ++))
    {
        if (!IsObjectOn(ankh[k]))
            ankh[k] = SpawnAnkh(k + 24);
    }
}

int SpawnAnkh(int wp)
{
    int unit = CreateObjectAt("Maiden", LocationX(wp), LocationY(wp));

    Frozen(CreateObjectAt("AnkhTradable", LocationX(wp), LocationY(wp)), 1);
    Frozen(unit, 1);
    UnitNoCollide(unit + 1);
    SetCallback(unit, 9, ReviveHuman);

    return unit;
}

void ReviveHuman()
{
    int k;

    if (CurrentHealth(other))
    {
        MoveWaypoint(35, GetObjectX(self), GetObjectY(self));
        AudioEvent("AwardSpell", 35);
        GreenSparkFx(35);
        Delete(self);
        Delete(GetTrigger() + 1);
        UniPrintToAll("리바이브 앵크를 사용하여 죽은 플레이어 전원 부활합니다");
        for (k = 0 ; k < 10 ; Nop(k ++))
        {
            if (!CurrentHealth(PlrCre[k]) && CurrentHealth(player[k]))
            {
                PlrCre[k] = SpawnRunner(k, 35);
            }
        }
    }
}

void PlacedObstacle(int count)
{
    int k, unit = CreateObject("InvisibleLightBlueHigh", 16) + 1;
    Delete(unit - 1);

    if (count >= 0)
    {
        for (k = 0 ; k < 8 ; Nop(k ++))
        {
            Frozen(CreateObject("FireSprite", 16 + k), 1);
            SetCallback(unit + k, 9 , FireTouch);
            TeleportLocationVector(16 + k, 120.0, -120.0);
        }
        FrameTimerWithArg(1, count - 1, PlacedObstacle);
    }
}

void FireTouch()
{
    if (CurrentHealth(other))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(other), GetObjectY(other), 0.0, 0.0);
        Damage(other, 0, 999, 14);
    }
}

void StandBy1(int startLocation)
{
    if (!GameEnd)
    {
        UniPrintToAll("5 초 후 게임을 시작합니다, 맵에 입장한 플레이어가 한명도 없을 경우 게임이 시작되지 않습니다");
        FrameTimer(3, ClearFireWay);
        FrameTimerWithArg(3, 0, SetFireways);
        FrameTimer(1, ResetReviveAnkh);
        GameReset();
        FrameTimerWithArg(1, 0, ResetFireWalls);
        SecondTimerWithArg(5, startLocation, AllPlayersRunner);
    }
}

void GameReset()
{
    Switch1 = 0;
    PutSwitchs();
}

void AllPlayersRunner(int location)
{
    int k;

    for (k = 9 ; k >= 0 ; Nop(k --))
    {
        if (CurrentHealth(player[k]))
        {
            PlrCre[k] = SpawnRunner(k, location);
            if (MaxHealth(PlrCam[k]))
                MoveObject(PlrCam[k], LocationX(location), LocationY(location));
        }
    }
}

void InitStartSwitch(int startLocation, int len)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", LocationX(startLocation), LocationY(startLocation)) + 1, k;

    for (k = 0 ; k < len ; Nop(k ++))
    {
        SetCallback(CreateObjectAt("Wizard", GetObjectX(unit + k - 1) + 23.0, GetObjectY(unit + k - 1) + 23.0), 9, StartTheGame);
        SetOwner(GetMaster(), unit + k);
        SetUnitFlags(unit + k, GetUnitFlags(unit + k) ^ 0x400);
        Damage(unit + k, 0, 999, 14);
    }
    Delete(unit - 1);
}

void StartTheGame()
{
    int startPos = StartPointNumber, goLocation = GoToLocation;

    if (CurrentHealth(other) && !IsObjectOn(StartPtr))
    {
        if (HasEnchant(other, "ENCHANT_CROWN"))
        {
            StartPtr = CreateObjectAt("InvisibleLightBlueLow", LocationX(startPos), LocationY(startPos));
            Raise(StartPtr, goLocation);
            CreateMoverFix(StartPtr, 0, 16.0);
            FrameTimerWithArg(1, StartPtr, DisplayDeadline);
            FrameTimerWithArg(30, StartPtr, DelayMoveDeadline);
        }
    }
}

void DelayMoveDeadline(int deadline)
{
    Move(deadline, ToInt(GetObjectZ(deadline)));
}

void ClearDeadline()
{
    if (IsObjectOn(StartPtr))
    {
        Delete(StartPtr);
        Delete(StartPtr + 1);
    }
}

void CamMoveWithDeadline(int plr)
{
    if (IsObjectOn(StartPtr))
    {
        if (Distance(GetObjectX(PlrCam[plr]), GetObjectY(PlrCam[plr]), GetObjectX(StartPtr), GetObjectY(StartPtr)) >= 23.0)
        {
            PushObjectTo(PlrCam[plr], UnitRatioX(StartPtr, PlrCam[plr], 1.8), UnitRatioY(StartPtr, PlrCam[plr], 1.8));
        }
    }
}

void LoopPreserveDisplayDeadline(int baseunit)
{
    int unit = GetOwner(baseunit - 1), k;

    if (IsObjectOn(unit))
    {
        for (k = 0 ; k < 45 ; Nop(k ++))
        {
            MoveObject(baseunit + k, GetObjectX(unit) + MathSine(k * 8 + 90, XRange), GetObjectY(unit) + MathSine(k * 8, XRange));
            if (k < 10)
            {
                if (CurrentHealth(PlrCre[k]))
                {
                    if (Distance(GetObjectX(unit), GetObjectY(unit), GetObjectX(PlrCre[k]), GetObjectY(PlrCre[k])) > XRange)
                        Damage(PlrCre[k], 0, 999, 14);
                }
            }
        }
        FrameTimerWithArg(1, baseunit, LoopPreserveDisplayDeadline);
    }
    else
    {
        Delete(baseunit - 1);
        for (k = 0 ; k < 45 ; Nop(k ++))
            Delete(baseunit + k);
    }
}

void DisplayDeadline(int unit)
{
    int baseunit = CreateObject("InvisibleLightBlueHigh", 10), k;

    SetOwner(unit, baseunit);
    for (k = 0 ; k < 45 ; Nop(k ++))
        Nop(CreateObjectAt("CharmOrb", GetObjectX(baseunit + k) + 1.0, GetObjectY(baseunit + k)));
    FrameTimerWithArg(1, baseunit + 1, LoopPreserveDisplayDeadline);
}

int RunnerClassChat(int pUnit, int plrPtr, int messagePtr)
{
    int plr = GetPlayerScrIndex(pUnit);

    if (plr + 1)
    {
        int camPtr = UnitToPtr(PlrCre[plr]);
        if (camPtr)
        {
            // SetMemory(messagePtr + 0x290, GetMemory(camPtr + 0x24));
            UniChatCore(camPtr, messagePtr, 180);
            return 1;
        }
    }
    return 0;
}

int ChatMessageHandler(int msgptr)
{
    int ptr2 = GetPlayerPtrByNetCode(GetMemory(msgptr + 0x290)), pic = GetMemory(msgptr);

    if (ptr2 && pic)
	{
        int unit = GetMemory(ptr2 + 0x2c);

        if (RunnerClassChat(unit, ptr2, msgptr))
        {
            UniChatMessage(unit, " ", 1);
            return 1;
        }
	}
    return 0;
}

void ChatMessageLoop()
{
	int ptr = GetMemory(0x6f8acc);

	while (ptr)
	{
        int tempNext = GetMemory(ptr + 0x2ac);

		Nop(ChatMessageHandler(ptr));
        ptr = tempNext;
	}
	FrameTimer(6, ChatMessageLoop);
}

void MapInitialize()
{
    int startLoc = StartPointNumber;
    int startBtnLoc = StartButtonPos;

    MusicEvent();
    GetMaster();
    InitPlayerColor();
    InitGameScreen();
    InitStartSwitch(startBtnLoc, 11);
    FrameTimer(60, PreservePlayerLoop);
    FrameTimerWithArg(80, startLoc, StandBy1);
    FrameTimer(1, InitUseMapSetting);
}

void InitPlayerColor()
{
    ColorR[0] = 255; ColorG[0] = 127; ColorB[0] = 64;
    ColorR[1] = 0; ColorG[1] = 64; ColorB[1] = 128;
    ColorR[2] = 128; ColorG[2] = 64; ColorB[2] = 0;
    ColorR[3] = 128; ColorG[3] = 128; ColorB[3] = 192;
    ColorR[4] = 0; ColorG[4] = 255; ColorB[4] = 255; //Cyan
    ColorR[5] = 255; ColorG[5] = 0; ColorB[5] = 255; //Violet
    ColorR[6] = 255; ColorG[6] = 255; ColorB[6] = 0; //Yellow
    ColorR[7] = 0; ColorG[7] = 0; ColorB[7] = 255; //Blue
    ColorR[8] = 0; ColorG[8] = 255; ColorB[8] = 0; //Green
    ColorR[9] = 255; ColorG[9] = 0; ColorB[9] = 0; //Red
}

void ResourceMapBgmData()
{ }

void PlayerClassCommonWhenEntry()
{
    ExtractMapBgm("..\\BgmSong.mp3", ResourceMapBgmData);
    ShowMessageBox("게임설명", "캐릭터를 움직이려면 이동하려는 지점을 클릭합니다.\n초록색 원을 벗어나면 죽게됩니다");
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;
    if (ValidPlayerCheck(pUnit))
    {
        if (pUnit ^ GetHost())
            NetworkUtilClientEntry(pUnit);
        else
            PlayerClassCommonWhenEntry();
        FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);
    }
    ObserverPlayerCameraLock(pUnit);
    return plr;
}

void PlayerRegist()
{
    int k, plr;

    while (1)
    {
        if (CurrentHealth(other))
        {
            plr = CheckPlayer();
            for (k = 9 ; k >= 0 && plr < 0 ; Nop(k --))
            {
                if (!MaxHealth(player[k]))
                {
                    plr = PlayerClassOnInit(k, GetCaller());
                    break;
                }
            }
            if (plr + 1)
            {
                PlayerJoin(plr);
                break;
            }
        }
        CantJoin();
        break;
    }
}

void PlayerJoin(int plr)
{
    int startLoc = StartPointNumber + 1;

    if (PlayerClassDeathFlagCheck(plr))
        PlayerClassDeathFlagSet(plr);
    Enchant(player[plr], "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(player[plr], "ENCHANT_FREEZE", 0.0);
    MoveObject(player[plr], GetWaypointX(6), GetWaypointY(6));
    if (!MaxHealth(PlrCam[plr]))
    {
        PlrCam[plr] = SetupPlayerCamera(plr, startLoc);
    }
}

int SpawnRunner(int plr, int wp)
{
    int unit = ColorMaiden(ColorR[plr], ColorG[plr], ColorB[plr], wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Enchant(unit, "ENCHANT_CROWN", 0.0);
    SetOwner(player[plr], unit);
    LookWithAngle(unit + 1, plr);
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    SetCallback(unit, 5, RunnerDeadEvent);
    SetCallback(unit, 9, RunnerTouchFire);
    SetUnitSpeed(unit, 2.3);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    PlrCnt ++;

    return unit;
}

void RunnerTouchFire()
{
    if (HasClass(other, "FIRE") && CurrentHealth(self))
    {
        Damage(self, 0, 999, 14);
    }
}

void RunnerDeadEvent()
{
    int plr = GetDirection(GetTrigger() + 1), startLoc = StartPointNumber;

    if (CurrentHealth(player[plr]))
    {
        UniPrint(player[plr], "======= 당신의 러너가 죽었습니다 ======");
        UniPrintToAll("방금 " + PlayerIngameNick(player[plr]) + " 가 조종하고 있던 러너가 격추되었습니다");
    }
    Delete(GetTrigger() + 1);
    DeleteObjectTimer(self, 180);
    PlrCnt --;
    if (!PlrCnt && IsObjectOn(StartPtr))
    {
        UniPrintToAll("러너가 전멸되었습니다, 게임이 초기화 됩니다");
        ClearDeadline();
        SecondTimerWithArg(2, startLoc, StandBy1);
    }
}

int SetupPlayerCamera(int plr, int location)
{
    int unit = CreateObjectAt("Maiden", LocationX(location), LocationY(location));

    UnitNoCollide(unit);
    CreateObjectAt("InvisibleLightBlueHigh", LocationX(location), LocationY(location));
    SetOwner(player[plr], unit);
    LookWithAngle(unit + 1, plr);
    GiveCreatureToPlayer(player[plr], unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Enchant(player[plr], "ENCHANT_BURNING", 0.0);

    return unit;
}

void CantJoin()
{
    Enchant(other, "ENCHANT_FREEZE", 0.0);
    Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(other, "ENCHANT_BLINDED", 0.0);
    MoveObject(other, GetWaypointX(6), GetWaypointY(6));
}

void RunnerClassLoop(int plr)
{
    int pCam = PlrCam[plr];

    if (MaxHealth(pCam))
    {
        if (CheckWatchFocus(player[plr]))
        {
            int glow = CreateObjectAt("Moonglow", LocationX(7), LocationY(7));
            SetOwner(player[plr], glow);
            LookWithAngle(glow, plr);
            PlayerLook(player[plr], pCam);
            FrameTimerWithArg(1, glow, GoTarget);
        }
        CamMoveWithDeadline(plr);
    }
}

int PlayerClassDeathFlagCheck(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassDeathFlagSet(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

void PlayerClassOnDeath(int plr)
{
    return;
}

void PlayerClassOnFree(int plr)
{
    if (CurrentHealth(PlrCre[plr]))
    {
        PlrCnt --;
        Delete(PlrCre[plr]);
        Delete(PlrCre[plr] + 1);
    }
    if (MaxHealth(PlrCam[plr]))
    {
        Delete(PlrCam[plr]);
        Delete(PlrCam[plr] + 1);
    }
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PreservePlayerLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; Nop(i --))
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    RunnerClassLoop(i);
                    break;
                }
                else
                {
                    if (PlayerClassDeathFlagCheck(i)) break;
                    else
                    {
                        PlayerClassDeathFlagSet(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, PreservePlayerLoop);
}

void GoTarget(int glow)
{
	int plr = GetDirection(glow), mark[10];

	if (CurrentHealth(player[plr]) && !HasEnchant(player[plr], "ENCHANT_ETHEREAL"))
	{
		if (CurrentHealth(PlrCre[plr]))
        {
            if (IsObjectOn(mark[plr]))
                Delete(mark[plr]);
            mark[plr] = CreateObjectAt("TeleportGlyph1", GetObjectX(glow), GetObjectY(glow));
            LookWithAngle(mark[plr], plr);
            SetOwner(player[plr], mark[plr]);
			LookAtObject(PlrCre[plr], mark[plr]);
            FrameTimerWithArg(1, mark[plr], MaidenMoving);
        }
	}
	Delete(glow);
}

void MaidenMoving(int ptr)
{
    int plr = GetDirection(ptr);

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(PlrCre[plr]))
        {
            if (Distance(GetObjectX(PlrCre[plr]), GetObjectY(PlrCre[plr]), GetObjectX(ptr), GetObjectY(ptr)) > 8.0)
            {
                LookAtObject(PlrCre[plr], ptr);
                Walk(PlrCre[plr], GetObjectX(PlrCre[plr]), GetObjectY(PlrCre[plr]));
                MoveObjectVector(PlrCre[plr], UnitRatioX(ptr, PlrCre[plr], 4.0), UnitRatioY(ptr, PlrCre[plr], 4.0));
                FrameTimerWithArg(1, ptr, MaidenMoving);
            }
            else
            {
                PauseObject(PlrCre[plr], 10);
                LookAtObject(PlrCre[plr], ptr);
                Delete(ptr);
            }
        }
    }
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; Nop(k --))
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectAt("Hecubah", 5500.0, 100.0);
        Frozen(unit, 1);
    }
    return unit;
}

void GreenSparkFx(int wp)
{
    int ptr = CreateObject("MonsterGenerator", wp);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; arr[27] = 1; arr[28] = 1106247680; 
		arr[29] = 22; arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; 
		arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObjectAt("Bear2", LocationX(wp), LocationY(wp));
    int ptr1 = GetMemory(0x750710), k;

    SetMemory(ptr1 + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e4, MaidenBinTable());

    return unit;
}

void StrDestLocation()
{
	int arr[17], i = 0;
	string name = "SpiderSpit";
	arr[0] = 2116256764; arr[1] = 544709; arr[2] = 34095232; arr[3] = 125763617; arr[4] = 4442145; arr[5] = 809632800; arr[6] = 144703752; arr[7] = 2115051784; arr[8] = 573058147; arr[9] = 32840; 
	arr[10] = 2208; arr[11] = 304217602; arr[12] = 263164; arr[13] = 1141393540; arr[14] = 545784319; arr[15] = 4129; arr[16] = 1069678624; 
	while (i < 17)
	{
		drawStrDestLocation(arr[i++], name);
	}
}

void drawStrDestLocation(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = LocationX(327);
		pos_y = LocationY(327);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 327), 1);
		if (count % 48 == 47)
            TeleportLocationVector(327, -141.0, 3.0);
		else
            TeleportLocationVector(327, 3.0, 0.0);
        Nop(++ count);
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(327, pos_x, pos_y);
	}
}

void StrReviveTeam()
{
	int arr[19], i = 0;
	string name = "SpiderSpit";
	arr[0] = 1076068990; arr[1] = 8659008; arr[2] = 270537249; arr[3] = 139444990; arr[4] = 168816649; arr[5] = 37232673; arr[6] = 164135944; arr[7] = 2113994768; arr[8] = 1174152067; arr[9] = 1073742015; 
	arr[10] = 553648144; arr[11] = 133955584; arr[12] = 1880916478; arr[13] = 1082261631; arr[14] = 16779280; arr[15] = 69222910; arr[16] = 8421376; arr[17] = 33431520; arr[18] = 4177984; 
	while(i < 19)
	{
		drawStrReviveTeam(arr[i++], name);
	}
}

void drawStrReviveTeam(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(328);
		pos_y = GetWaypointY(328);
	}
	for (i = 1 ; i > 0 && count < 589 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 328), 1);
		if (count % 53 == 52)
            TeleportLocationVector(328, -156.0, 3.0);
		else
            TeleportLocationVector(328, 3.0, 0.0);
		Nop(count ++);
	}
	if (count >= 589)
	{
		count = 0;
		MoveWaypoint(328, pos_x, pos_y);
	}
}

void StrStartArrow()
{
	int arr[22], i, count = 0;
	string name = "CharmOrb";
	float pos_x = LocationX(394), pos_y = LocationY(394);

	arr[0] = 1041301052; arr[1] = 508; arr[2] = 1108348994; arr[3] = 67239968; arr[4] = 1109921858; arr[5] = 235339808; arr[6] = 1109921794; arr[7] = 353009696; 
	arr[8] = 1044647948; arr[9] = 67239968; arr[10] = 1111756848; arr[11] = 67239968; arr[12] = 1115426880; arr[13] = 67239968; arr[14] = 1115820098; arr[15] = 67239968; 
	arr[16] = 1115820098; arr[17] = 67239968; arr[18] = 1115820092; arr[19] = 67239968; arr[20] = 0; arr[21] = 67239936; 
	for (i = 0 ; i < 22 ; i ++)
		count = DrawStrStartArrow(arr[i], name, count);
	TeleportLocation(394, pos_x, pos_y);
}

int DrawStrStartArrow(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg)
		    Nop(CreateObject(name, 394));
		if (count % 62 == 61)
            TeleportLocationVector(394, -124.0, -120.0);
		else
            TeleportLocationVector(394, 2.0, 2.0);
		Nop(count ++);
	}
	return count;
}
