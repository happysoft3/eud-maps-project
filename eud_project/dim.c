
#include "dim_initscan.h"
#include "dim_resource.h"
#include "dim_reward.h"
#include "dim_sub.h"
#include "libs/printutil.h"
#include "libs/sound_define.h"
#include "libs/playerupdate.h"
#include "libs/reaction.h"
#include "libs/coopteam.h"
#include "libs/bind.h"
#include "libs/hash.h"
#include "libs/weaponcapacity.h"
#include "libs/meleeattack.h"
#include "libs/monsteraction.h"
#include "libs/potionex.h"
#include "libs/absolutelypickup.h"
#include "libs/fixtellstory.h"
#include "libs/spellutil.h"
#include "libs/npc.h"
#include "libs/format.h"
#include "libs/logging.h"
#include "libs/indexloop.h"
#include "libs/game_flags.h"
#include "libs/voiceList.h"

int m_bossRespectPosArray[100];
int m_bossAmuletCount;
int m_player[10];
int m_pFlag[10];

int *m_pImgVector;

#define PLAYER_DEATH_FLAG 2
#define PLAYER_FLAG_ALL_BUFF 4
#define PLAYER_EXCHANGE_ABILITY 8
#define PLAYER_FLAG_BOOSTER 16

int *m_monsterTable;
int m_monsterTableLength;
// string *m_weaponNameTable;
// int m_weaponNameTableLength;
// string *m_armorNameTable;
// int m_armorNameTableLength;
// string *m_potionNameTable;
// int m_potionNameTableLength;

// string *m_staffNameTable;
// int m_staffNameTableLength;

int *m_itemFunctionTable;
int m_itemFunctionTableLength;

int *m_itemBetterFunctionTable;
int m_itemBetterTableLength;
int *m_strikeHashInstance;

int m_masterUnit;

int *m_pBooster;
int *m_pBoosterFx;
int m_boostTime[10];


void MobMaster()
{
    m_masterUnit = CreateObjectAt("Hecubah", 5500.0, 100.0);

    Frozen(m_masterUnit, TRUE);
}

void WispDeathFx(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

void InitPlayerBooster()
{
    string marker="AmbBeachBirds";
    string boosterName = "Maiden";
    int locationId = 31;
    int booster[10], u;
    int boosterFx[10];

    for (u=0;u<sizeof(m_player);Nop(++u))
    {
        booster[u] = DummyUnitCreateAt(boosterName, LocationX(locationId), LocationY(locationId));
        CreateObjectAtEx(marker, LocationX(locationId), LocationY(locationId), 0);
        boosterFx[u] = CreateObjectAt("BlackPowder", LocationX(locationId), LocationY(locationId));
        Enchant(boosterFx[u], EnchantList(ENCHANT_RUN), 0.0);
    }
    m_pBooster=&booster;
    m_pBoosterFx=&boosterFx;
}

int CheckFatalErrorMon(int mon)
{
    if (!IsMonsterUnit(mon))
    {
        UniPrintToAll("dim.c::CheckFatalErrorMon::fatal error");
        UniPrintToAll(IntToString(GetUnitThingID(mon)));
        return FALSE;
    }
    return TRUE;
}

static void NetworkUtilClientMain()
{
    InitializeCommonImage();
}

void WispOnStrike(int attacker, int victim)
{
    MonsterForceCastSpell(attacker, 0, GetObjectX(attacker) + UnitRatioX(victim, attacker, 13.0), GetObjectY(attacker) + UnitRatioX(victim, attacker, 13.0));
    Effect("LIGHTNING", GetObjectX(attacker), GetObjectY(attacker), GetObjectX(victim), GetObjectY(victim));
}

int MinionWisp(float xpos, float ypos)
{
    int mon = CreateObjectAt("WillOWisp", xpos, ypos);

    if (!CheckFatalErrorMon(mon))
        return 0;
    WillOWispSubProcess(mon);
    RegistUnitStrikeHook(mon);
    return mon;
}

int MinionLizard(float xpos, float ypos)
{
    int mon = CreateObjectAt("WeirdlingBeast", xpos, ypos);

    if (!CheckFatalErrorMon(mon))
        return 0;
    WeirdlingBeastSubProcess(mon);
    return mon;
}

int MinionBomber(float xpos, float ypos)
{
    string bombName[] = {"Bomber", "BomberBlue", "BomberGreen", "BomberYellow"};
    int mob = CreateObjectAt(bombName[Random(0, sizeof(bombName)-1)], xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    BomberSubProcess(mob);
    BomberSetMonsterCollide(mob);
    return mob;
}

void ShopkeeperOnStrike(int attacker, int victim)
{
    CreateObjectAt("BarrelBreakingLOTD", GetObjectX(attacker), GetObjectY(attacker));
    Effect("DAMAGE_POOF", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0), 0.0, 0.0);
}

int MinionLoproc(float xpos, float ypos)
{
    int mob = CreateObjectAt("Bear2", xpos, ypos);
    int *ptr = UnitToPtr(mob);

    if (!CheckFatalErrorMon(mob))
        return 0;
    ptr[1] = OBJ_SHOPKEEPER_WIZARD_REALM;
    ShopkeeperSubProcess(mob);
    RegistUnitStrikeHook(mob);
    SetUnitVoice(mob, MONSTER_VOICE_Scorpion);
    return mob;
}

int MinionSummonProto(int functionId, float xpos, float ypos)
{
    return Bind(functionId, &functionId+4);
}

void MinionDeathEvent()
{
    WriteLog("miniondeathevent-start");
    if (Random(0, 3))
        CreateRandomItemCommon(CreateObjectAt("PlayerWaypoint", GetObjectX(SELF), GetObjectY(SELF)));

    WriteLog("miniondeathevent-end");
    DeleteObjectTimer(SELF, 60);
}

void CreateRandomMinion(int posUnit)
{
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    Delete(posUnit);

    WriteLog("CreateRandomMinion");
    int functions[] = {MinionBomber, MinionLizard, MinionLoproc, MinionWisp, };
    int mon= MinionSummonProto(functions[Random(0, sizeof(functions)-1)], xpos, ypos);

    if (!mon)
    {
        UniPrintToAll("dim.c::CreateRandomMinion::patal error");
        return;
    }
    AggressionLevel(mon, 1.0);
    SetCallback(mon, 5, MinionDeathEvent);
    SetCallback(mon, 7, MonsterCommonHitEvent);
    SetOwner(m_masterUnit, mon);
    WriteLog("CreateRandomMinion-end");
}

void MapExit()
{
    MusicEvent();
    ResetHostileCritter();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void SetNextUnitFunctionId(int unit, int functionId)
{
    if (!IsMonsterUnit(unit))
    {
        char buff[128];
        int arg = GetUnitThingID(unit);

        NoxSprintfString(&buff, "error assertion-- the unit must be a monster (thingId:%d)", &arg, 1);
        WriteLog(ReadStringAddressEx(&buff));
        UniPrintToAll(ReadStringAddressEx(&buff));
        return;
    }
    SetUnit1C(--unit, functionId);
}

int GetNextUnitFunctionId(int subUnit, int *pDestFunctionId)
{
    if (!IsObjectOn(subUnit))
        return FALSE;

    pDestFunctionId[0] = GetUnit1C(subUnit);
    return TRUE;
}

void StrikeFunctionProto(int functionId, int attacker, int victim)
{
    Bind(functionId, &functionId+4);
}

static void MonsterStrikeDefaultCallback()
{
    int thingId = GetUnitThingID(SELF);

    if (thingId)
    {
        int action;

        if (HashGet(m_strikeHashInstance, thingId, &action, FALSE))
            StrikeFunctionProto(action, GetTrigger(), GetCaller());
    }
}

int EndMonsterRespawn(float xpos, float ypos)
{
    int flame = CreateObjectAt("MediumFlame", xpos, ypos);

    SetUnitEnchantCopy(flame, GetLShift(ENCHANT_FREEZE));
    UnitNoCollide(flame);
    DeleteObjectTimer(flame, 150);
    PlaySoundAround(flame, SOUND_BurnCast);
    Effect("SPARK_EXPLOSION", xpos, ypos, 0.0, 0.0);
    return flame;
}

int SummonLv1FireFairy(float xpos, float ypos)
{
    int mob = CreateObjectAt("FireSprite", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    FireSpriteSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv2EmberDemon);
    return mob;
}

int SummonLv2EmberDemon(float xpos, float ypos)
{
    int mob = CreateObjectAt("EmberDemon", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    SetUnitMaxHealth(mob, 225);
    SetNextUnitFunctionId(mob, SummonLv3DemonLord);
    return mob;
}

int SummonLv3DemonLord(float xpos, float ypos)
{
    int mob = CreateObjectAt("Demon", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    SetUnitMaxHealth(mob, 600);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    return mob;
}

int SummonLv1Bat(float xPos, float yPos)
{
    int mob = CreateObjectAt("Bat", xPos, yPos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    BatSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv2SkullLord);
    return mob;
}

void SkullLordStrikeFunction(int attacker, int victim)
{
    Damage(victim, 0, 10, DAMAGE_TYPE_ELECTRIC);
    Effect("LIGHTNING", GetObjectX(victim), GetObjectY(victim), GetObjectX(victim), GetObjectY(victim) - 150.0);
    PlaySoundAround(victim, SOUND_ElectricalArc1);
}

int SummonLv2SkullLord(float xpos, float ypos)
{
    int mob = CreateObjectAt("SkeletonLord", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    SkeletonLordSubProcess(mob);
    RegistUnitStrikeHook(mob);
    SetNextUnitFunctionId(mob, SummonLv3MecaGolem);
    return mob;
}

int SummonLv3MecaGolem(float xpos, float ypos)
{
    int mob = CreateObjectAt("MechanicalGolem", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    SetUnitMaxHealth(mob, 800);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    return mob;
}

int SummonLv1SmallSpider(float xpos, float ypos)
{
    int mob = CreateObjectAt("SmallAlbinoSpider", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    SmallAlbinoSpiderSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv2Spider);
    return mob;
}

int SummonLv2Spider(float xpos, float ypos)
{
    int mob = CreateObjectAt("AlbinoSpider", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    AlbinoSpiderSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv3Spider);
    return mob;
}

int SummonLv3Spider(float xpos, float ypos)
{
    int mob = CreateObjectAt("BlackWidow", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    SpiderSubProcess(mob);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    return mob;
}

int SummonLv1Goon(float xpos, float ypos)
{
    int mob = CreateObjectAt("Goon", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    GoonSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv2Shade);
    return mob;
}

int SummonLv2Shade(float xpos, float ypos)
{
    int mob = CreateObjectAt("Shade", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    ShadeSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv3Mimic);
    return mob;
}

int SummonLv3Mimic(float xpos, float ypos)
{
    int mob = CreateObjectAt("Mimic", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    MimicSubProcess(mob);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    return mob;
}

int SummonLv1Human(float xpos, float ypos)
{
    int mob = CreateObjectAt("Swordsman", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    SwordsmanSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv2Archer);
    return mob;
}

int SummonLv2Archer(float xpos, float ypos)
{
    int mob = CreateObjectAt("Archer", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    ArcherSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv3Horrendous);
    return mob;
}

int SummonLv3Horrendous(float xpos, float ypos)
{
    int mob = CreateObjectAt("Horrendous", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    HorrendousSubProcess(mob);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    return mob;
}

int SummonLv1Ghost(float xpos, float ypos)
{
    int mob = CreateObjectAt("Ghost", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    GhostSubProcess(mob);
    Enchant(mob, EnchantList(ENCHANT_ANCHORED), 0.0);
    SetNextUnitFunctionId(mob, SummonLv2Zombie);
    return mob;
}

int SummonLv2Zombie(float xpos, float ypos)
{
    int mob = CreateObjectAt("Zombie", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    VileZombieSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv3DeadWalker);
    return mob;
}

int SummonLv3DeadWalker(float xpos, float ypos)
{
    int mob = CreateObjectAt("LichLord", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    LichLordSubProcess(mob);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    Enchant(mob, EnchantList(ENCHANT_VAMPIRISM), 0.0);
    return mob;
}

int SummonLv1Maiden(float xpos, float ypos)
{
    int mob = CreateSingleColorMaidenAt(Random(0, 255), Random(0, 255), Random(0, 255), xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    Bear2SubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv2Dryad);
    return mob;
}

void DryadStrikeFunction(int attacker, int victim)
{
    MonsterForceCastSpell(attacker, 0, GetObjectX(attacker) + UnitRatioX(victim, attacker, 13.0), GetObjectY(attacker) + UnitRatioY(victim, attacker, 13.0));
}

int SummonLv2Dryad(float xpos, float ypos)
{
    int mob = CreateObjectAt("WizardGreen", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    WizardGreenSubProcess(mob);
    RegistUnitStrikeHook(mob);
    SetNextUnitFunctionId(mob, SummonLv3Hecubah);
    return mob;
}

int SummonLv3Hecubah(float xpos, float ypos)
{
    int mob = CreateObjectAt("Hecubah", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    HecubahSubProcess(mob);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    return mob;
}

int SummonLv1Frog(float xpos, float ypos)
{
    int mob = CreateObjectAt("GreenFrog", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    GreenFrogSubProcess(mob);
    SetNextUnitFunctionId(mob, SummonLv2Mystic);
    return mob;
}

void MysticStrikeFunction(int attacker, int victim)
{
    Damage(victim, 0, 20, DAMAGE_TYPE_ZAP_RAY);
    MonsterForceCastSpell(attacker, 0, GetObjectX(attacker) + UnitRatioX(victim, attacker, 13.0), GetObjectY(attacker) + UnitRatioY(victim, attacker, 13.0));
    Effect("SENTRY_RAY", GetObjectX(attacker), GetObjectY(attacker), GetObjectX(victim), GetObjectY(victim));
}

int SummonLv2Mystic(float xpos, float ypos)
{
    int mob = CreateObjectAt("Wizard", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    WizardSubProcess(mob);
    RegistUnitStrikeHook(mob);
    SetNextUnitFunctionId(mob, SummonLv3Plant);
    return mob;
}

int SummonLv3Plant(float xpos, float ypos)
{
    int mob = CreateObjectAt("CarnivorousPlant", xpos, ypos);

    if (!CheckFatalErrorMon(mob))
        return 0;
    CarnivorousPlantSubProcess(mob);
    SetNextUnitFunctionId(mob, EndMonsterRespawn);
    return mob;
}

void InitMonsterTable()
{
    int table[]={
        SummonLv1Bat, SummonLv1FireFairy, SummonLv1Frog, SummonLv1Ghost, 
        SummonLv1Goon, SummonLv1Human, SummonLv1Maiden, SummonLv1SmallSpider
        };

    m_monsterTable=table;
    m_monsterTableLength=sizeof(table);
}

int CreateMonsterProto(int functionId, float xpos, float ypos)
{
    StopScript(Bind(functionId, &functionId+4));
}

void MonsterCommonHitEvent()
{
    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 2, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void CommonCreateMons(int functionId, float xpos, float ypos, int *pDest)
{
    CreateObjectAtEx("InvisibleLightBlueLow", xpos, ypos, NULLPTR);
    int mon = CreateMonsterProto(functionId, xpos, ypos);

    RetreatLevel(mon, 0.0);
    ResumeLevel(mon, 1.0);
    SetCallback(mon, 5, MonsterCommonDeathEvent);
    SetCallback(mon, 7, MonsterCommonHitEvent);
    AggressionLevel(mon, 1.0);
    if (pDest)
        pDest[0] = mon;
    SetOwner(m_masterUnit, mon);
}

void RespawnMonster(int respawnUnit)
{
    int functionId = 0;

    if (!GetNextUnitFunctionId(respawnUnit, &functionId))
        return;

    int mon;

    CommonCreateMons(functionId, GetObjectX(respawnUnit), GetObjectY(respawnUnit), &mon);
    Delete(respawnUnit);
}

void MonsterCommonDeathEvent()
{
    WriteLog("monstercommondeathevent-start");
    if (Random(0, 2))
        CreateRandomItemCommon(CreateObjectAt("PlayerWaypoint", GetObjectX(SELF), GetObjectY(SELF)));

    int sub = GetTrigger() - 1;

    MoveObject(sub, GetObjectX(SELF), GetObjectY(SELF));
    FrameTimerWithArg(30, sub, RespawnMonster);
    DeleteObjectTimer(SELF, 150);
    PlaySoundAround(SELF, SOUND_BurnCast);
    Effect("SMOKE_BLAST", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    WriteLog("monstercommondeathevent-end");
}

void CreateRandomMonster(int posUnit)
{
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    WriteLog("CreateRandomMonster");
    Delete(posUnit);
    CommonCreateMons(m_monsterTable[Random(0, m_monsterTableLength-1)], xpos, ypos, NULLPTR);
    WriteLog("CreateRandomMonster-end");
}

void MecaArrowOnCollide()
{
    if (!GetTrigger())
        return;

    if (!CurrentHealth(OTHER))
        return;

    Damage(OTHER, 0, 30, DAMAGE_TYPE_EXPLOSION);
    DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 12);
    PlaySoundAround(OTHER, SOUND_PowderBarrelExplode);
}

void UnitDetectedMercArrow(int curId, int owner)
{
    Enchant(curId, EnchantList(ENCHANT_SHOCK), 0.0);
    SetUnitCallbackOnCollide(curId, MecaArrowOnCollide);
}

void UnitDetectedChakram(int curId, int owner)
{
    SetUnitEnchantCopy(curId, GetLShift(ENCHANT_RUN));
}

void UnitDetectedShuriken(int curId, int owner)
{
    if (!IsPlayerUnit(owner))
        return;

    int mis = CreateObjectAt("PitifulFireball", GetObjectX(curId), GetObjectY(curId));

    SetOwner(owner, mis);
    PushObject(mis, 25.0, GetObjectX(owner), GetObjectY(owner));
}

void UnitDetectedHarpoonBolt(int curId, int owner)
{
    SetUnitEnchantCopy(curId, GetLShift(ENCHANT_VAMPIRISM) | GetLShift(ENCHANT_SHOCK) | GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY) | GetLShift(ENCHANT_PROTECT_FROM_FIRE) |
        GetLShift(ENCHANT_PROTECT_FROM_POISON) | GetLShift(ENCHANT_HASTED) | GetLShift(ENCHANT_RUN) | GetLShift(ENCHANT_SHIELD) | GetLShift(ENCHANT_FREEZE));
}

#define THING_ID_DRYAD 1335
#define THING_ID_MYSTIC 1327
#define THING_ID_SKELETON_LORD 1315
#define THING_ID_SHOPKEEPER_WIZARD_REALM 1377
#define THING_ID_WISP 1326
#define THING_ID_JANDORS 1387

void InitStrikeHandler()
{
    HashCreateInstance(&m_strikeHashInstance);

    HashPushback(m_strikeHashInstance, THING_ID_JANDORS, StrikeCallbackAssociatedFinalBoss);
    HashPushback(m_strikeHashInstance, THING_ID_DRYAD, DryadStrikeFunction);
    HashPushback(m_strikeHashInstance, THING_ID_MYSTIC, MysticStrikeFunction);
    HashPushback(m_strikeHashInstance, THING_ID_SKELETON_LORD, SkullLordStrikeFunction);
    HashPushback(m_strikeHashInstance, THING_ID_SHOPKEEPER_WIZARD_REALM, ShopkeeperOnStrike);
    HashPushback(m_strikeHashInstance, THING_ID_WISP, WispOnStrike);
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 528, UnitDetectedMercArrow);
    HashPushback(pInstance, 526, UnitDetectedHarpoonBolt);
    HashPushback(pInstance, 703, UnitDetectedFinalBossMissile);
    HashPushback(pInstance, 1177, UnitDetectedChakram);
    HashPushback(pInstance, 1179, UnitDetectedShuriken);
}

void InitMarket()
{
    int shop;

    PlaceWeaponShop(LocationX(18), LocationY(18), &shop);
    LookWithAngle(shop, 32);

    PlaceArmorShop(LocationX(19), LocationY(19), &shop);
    LookWithAngle(shop, 32);

    PlacePotionShop(LocationX(29), LocationY(29), &shop);
    LookWithAngle(shop, 32);

    int special1 = DummyUnitCreateAt("Hecubah", LocationX(22), LocationY(22));

    StoryPic(special1, "HecubahPic");
    SetDialog(special1, "YESNO", ExpensionAbilityDesc, ExpensionAbilityTrade);

    int special2 = DummyUnitCreateAt("WizardGreen", LocationX(23), LocationY(23));

    StoryPic(special2, "MaidenPic5");
    SetDialog(special2, "YESNO", AllEnchantMarketDesc, AllEnchantMarketTrade);

    int special3 = DummyUnitCreateAt("WizardWhite", LocationX(24), LocationY(24));

    StoryPic(special3, "HorvathPic");
    SetDialog(special3, "YESNO", EnergyParSwordDesc, EnergyParSwordTrade);

    int special4 = CreateSingleColorMaidenAt(0, 0, 255, LocationX(26), LocationY(26));
     //DummyUnitCreateAt("UrchinShaman", LocationX(26), LocationY(26));

    Frozen(special4, TRUE);
    StoryPic(special4, "IngridPic");
    SetDialog(special4, "YESNO", InvincibleItemDesc, InvincibleItemTrade);

    int special5 = DummyUnitCreateAt("Archer", LocationX(27), LocationY(27));

    StoryPic(special5, "DrunkPic");
    SetDialog(special5, "YESNO", ArrowSwordDesc, ArrowSwordTrade);

    int special6 = DummyUnitCreateAt("Horrendous", LocationX(28), LocationY(28));

    StoryPic(special6, "HorrendousPic");
    SetDialog(special6, "YESNO", EarthquakeHammerDesc, EarthquakeHammerTrade);

    int special7 = DummyUnitCreateAt("Wizard", LocationX(30), LocationY(30));

    StoryPic(special7, "DrunkPic");
    SetDialog(special7, "YESNO", DeathRayStaffDesc, DeathRayStaffTrade);

    int hidden = DummyUnitCreateAt("Urchin", LocationX(51), LocationY(51));

    StoryPic(hidden, "DrunkPic");
    SetDialog(hidden, "YESNO", BuySozuDesc, BuySozuTrade);

    ThingDbEditChangeWorth(THING_ID_REDPOTION2, 30);
    ThingDbEditChangeWorth(646, 8000);
}

void MapInitialize()
{
    MusicEvent();

    CreateLogFile("dim-map.txt");
    int m_lastUnit=CreateObjectById(OBJ_RED_POTION, 100.0, 100.0);
    MobMaster();

    InitMonsterTable();
    InitStrikeHandler();
    InitPlayerBooster();
    FrameTimer(3, MakeCoopTeam);

    int initScanHash;
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_HECUBAH_MARKER, CreateFinalBossAmulet);
    HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, CreateRandomMonster);
    HashPushback(initScanHash, OBJ_REWARD_MARKER_PLUS, CreateRandomMinion);
    StartUnitScan(Object("firstscan"), m_lastUnit, initScanHash);

    FrameTimer(10, PlayerClassOnLoop);

    PlaceTeleportPortal(13, 15);
    PlaceTeleportPortal(14, 16);
    FrameTimer(1, SetHostileCritter);
    FrameTimer(30, InitMarket);
    FrameTimerWithArg(1, 20, DrawRingPotion);
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);

    RegistSignMessage(Object("sign1"), "이호성 지하 작업실 매점- 9999일째 운영중..");
    RegistSignMessage(Object("sign2"), "이곳은 이호성의 야구 연습장 입니다");
    RegistSignMessage(Object("sign3"), "이곳은 이호성의 식당 입니다");
    RegistSignMessage(Object("sign4"), "이곳은 이호성의 도서관 입니다. 몽땅 다 야설 뿐이군요...");
    RegistSignMessage(Object("sign5"), "이곳은 이호성의 불가마 사우나 입니다");
    RegistSignMessage(Object("sign6"), "이곳은 이호성의 갯벌 생태 체험장 입니다");
    RegistSignMessage(Object("sign7"), "이곳은 이호성의 산책로 입니다");
    RegistSignMessage(Object("sign8"), "이곳은 이호성의 주말농장 입니다. 잔디 밟지 마세요");
    RegistSignMessage(Object("sign9"), "이호성의 저택에서 살아남기 -- 제작. noxgameremaster");
    RegistSignMessage(Object("sign10"), "!!이호성의 저택에서 살아남아라!! 이곳의 최종 보스인 이호성.. 그를 처치해야 한다!!");
    RegistSignMessage(Object("sign11"), "이호성이 이번에 새로 장만한 옥좌이다...");
    RegistSignMessage(Object("sign12"), "지금 이것은 극성팬에 의해 파해쳐진 이호성의 무덤이다");
    RegistSignMessage(Object("sign13"), "이곳은 이호성의 지하 무덤입니다. 그가 이곳에 안치되어 있습니다");
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}
void RegistPlayer()
{
    PlayerClassOnEntry(GetCaller());
}

void PlayerNetInit()
{
    while (TRUE)
    {
        if (!IsPlayerUnit(OTHER))
            break;
        if (!CurrentHealth(OTHER))
            break;
        break;
    }
    MoveObjectVector(OTHER, 92.0, -92.0);
}

void PlayerSetAllBuff(int pUnit)
{
    Enchant(pUnit, EnchantList(ENCHANT_VAMPIRISM), 0.0);
    SetUnitEnchantCopy(pUnit, GetLShift(ENCHANT_PROTECT_FROM_FIRE) | GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY) | GetLShift(ENCHANT_PROTECT_FROM_POISON)
        | GetLShift(ENCHANT_INFRAVISION) | GetLShift(ENCHANT_REFLECTIVE_SHIELD));
}

int CheckPlayerWithId(int pUnit)
{
    int rep=-1;

    while ((++rep)<sizeof(m_player))
    {
        if (m_player[rep]^pUnit)
            continue;
        
        return rep;
    }
    return -1;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    m_player[plr]=pUnit;
    m_pFlag[plr]=1;

    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);

    ChangeGold(pUnit, -GetGold(pUnit));
    return plr;
}

void PlayerClassOnEntry(int pUnit)
{
    while (TRUE)
    {
        if (!CurrentHealth(pUnit))
            break;
        int plr = CheckPlayerWithId(pUnit), u;

        for (u = sizeof(m_player)-1; u >= 0 && plr < 0 ; Nop(--u))
        {
            if (!MaxHealth(m_player[u]))
            {
                plr = PlayerClassOnInit(u, pUnit);
                break;
            }
        }
        if (plr >= 0)
        {
            PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(pUnit);
        break;
    }
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void PlayerClassOnJoin(int plr)
{
    int pUnit = m_player[plr], destLocation = 12;
    
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
        PlayerSetAllBuff(pUnit);
    MoveObject(pUnit, LocationX(destLocation), LocationY(destLocation));
    Effect("TELEPORT", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    PlaySoundAround(pUnit, SOUND_BlindOff);
}

void PlayerClassOnDeath(int plr)
{
    int pUnit = m_player[plr];
    char destMsg[160];
    char *userNameSrc = StringUtilGetScriptStringPtr(PlayerIngameNick(pUnit));

    NoxSprintfString(&destMsg, "%s 님께서 적에게 격추되었습니다", &userNameSrc, 1);
    WriteLog(ReadStringAddressEx(&destMsg));
    UniPrintToAll(ReadStringAddressEx(&destMsg));

    PlaySoundAround(pUnit, SOUND_HecubahDieFrame98);
    Effect("EXPLOSION", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    WriteLog("playerclassondeath-end");
}

void PlayerClassOnShutdown(int plr)
{
    m_player[plr] = 0;
    m_pFlag[plr] = 0;
}

void ResetBoosterPos(int plr)
{
    if (ToInt( DistanceUnitToUnit(m_pBooster[plr], m_pBooster[plr]+1)) )
    {
        MoveObject(m_pBooster[plr], GetObjectX(m_pBooster[plr] + 1), GetObjectY(m_pBooster[plr] + 1));
        MoveObject(m_pBoosterFx[plr], GetObjectX(m_pBooster[plr] +1), GetObjectY(m_pBooster[plr]+1));
    }
}

void PlayerBoosterEffect(int plr, int pUnit)
{
    while (TRUE)
    {
        if (m_boostTime[plr] > 0)
        {
            --m_boostTime[plr];
            if (CheckPlayerInput(pUnit) == 2)
            {
                float calcx = GetObjectX(pUnit) - UnitAngleCos(pUnit, 10.0), calcy=GetObjectY(pUnit) - UnitAngleSin(pUnit, 10.0);

                MoveObject(m_pBooster[plr], calcx, calcy);
                MoveObject(m_pBoosterFx[plr], calcx, calcy);
                break;
            }
        }
        else
        {
            PlayerClassSetFlag(plr, PLAYER_FLAG_BOOSTER);
            WispDeathFx(GetObjectX(pUnit), GetObjectY(pUnit));
        }
        ResetBoosterPos(plr);
        break;
    }
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_SNEAK)))
    {
        if (PlayerClassCheckFlag(plr, PLAYER_EXCHANGE_ABILITY))
            WindCharging(pUnit);
        RemoveTreadLightly(pUnit);
        EnchantOff(pUnit, EnchantList(ENCHANT_SNEAK));
        EnchantOff(pUnit, EnchantList(ENCHANT_INVULNERABLE));
    }
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
    {
        if (IsPoisonedUnit(pUnit))
            CastSpellObjectObject("SPELL_CURE_POISON", pUnit, pUnit);
    }
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_BOOSTER))
        PlayerBoosterEffect(plr, pUnit);
}

void PlayerClassOnLoop()
{
    int u;

    for (u = sizeof(m_player)-1 ; u >= 0 ; Nop(--u))
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[u]))
            {
                if (GetUnitFlags(m_player[u]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[u]))
                {
                    PlayerClassOnAlive(u, m_player[u]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(u, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassOnDeath(u);
                        PlayerClassSetFlag(u, PLAYER_DEATH_FLAG);
                    }
                    break;
                }
            }
            if (m_pFlag[u])
                PlayerClassOnShutdown(u);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void TeleportPortalTriggered()
{
    if (!CurrentHealth(OTHER))
        return;

    int destUnit = GetTrigger() + 1;

    MoveObject(OTHER, GetObjectX(destUnit), GetObjectY(destUnit));
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    PlaySoundAround(OTHER, SOUND_BlindOff);

    if (IsPlayerUnit(OTHER))
        UniPrint(OTHER, "이동했습니다");
}

void PlaceTeleportPortal(int srcLocation, int destLocation)
{
    int portal = CreateObjectAt("WeirdlingBeast", LocationX(srcLocation), LocationY(srcLocation));

    CreateObjectAtEx("InvisibleLightBlueLow", LocationX(destLocation), LocationY(destLocation), NULLPTR);
    Damage(portal, 0, MaxHealth(portal) + 1, DAMAGE_TYPE_ZAP_RAY);

    int fx = CreateObjectAt("TeleportWake", GetObjectX(portal), GetObjectY(portal));

    Frozen(fx, TRUE);
    UnitNoCollide(fx);

    SetCallback(portal, 9, TeleportPortalTriggered);
}

int CheckPlayer()
{
    int u;

    for (u = 0 ; u < sizeof(m_player) ; Nop(u++))
    {
        if (IsCaller(m_player[u]))
            return u;
    }
    return -1;
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        if (CheckPlayer() >= 0)
        {
            MoveObject(OTHER, LocationX(17), LocationY(17));
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
        {
            MoveObject(OTHER, LocationX(11), LocationY(11));
        }        
    }
}

void MagicWeaponCreateImpl(int functionId, float xpos, float ypos, int *pDest)
{
    Bind(functionId, &functionId+4);
}

void BossSlidingCollide()
{
    if (!GetTrigger())
        return;

    if (!CurrentHealth(OTHER))
        return;

    if (IsAttackedBy(OTHER, GetOwner(SELF)))
    {
        Damage(OTHER, GetOwner(SELF), 100, DAMAGE_TYPE_IMPACT);
        Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
    }
}

void PutBossSliding(float xpos, float ypos, int owner, int *pDest)
{
    int damHelper = DummyUnitCreateAt("Demon", xpos, ypos);

    if (pDest)
        pDest[0] = damHelper;
    SetCallback(damHelper, 9, BossSlidingCollide);
    SetOwner(owner, damHelper);
    DeleteObjectTimer(damHelper, 1);

    int fx = CreateObjectAt("RoundChakramInMotion", xpos, ypos);

    Frozen(fx, TRUE);
    DeleteObjectTimer(fx, 12);
}

void CastBossMissileSliding(int sub)
{
    while (IsObjectOn(sub))
    {
        if (IsVisibleTo(sub, sub+1))
        {
            int owner = GetOwner(sub), durate = GetDirection(sub);

            if (CurrentHealth(owner) && durate)
            {
                FrameTimerWithArg(1, sub, CastBossMissileSliding);
                LookWithAngle(sub, --durate);
                PutBossSliding(GetObjectX(sub), GetObjectY(sub), owner, NULLPTR);
                MoveObjectVector(sub, GetObjectZ(sub), GetObjectZ(sub+1));
                MoveObjectVector(sub+1, GetObjectZ(sub), GetObjectZ(sub+1));
                break;
            }
        }
        Delete(sub++);
        Delete(sub++);
        break;
    }
}

void UnitDetectedFinalBossMissile(int curId, int owner)
{
    float xpos = GetObjectX(curId), ypos = GetObjectY(curId);
    float xvect = UnitRatioX(curId, owner, 19.0), yvect = UnitRatioY(curId, owner, 19.0);

    Delete(curId);

    int sub = CreateObjectAt("InvisibleLightBlueLow", xpos + xvect, ypos + yvect);

    Raise(CreateObjectAt("InvisibleLightBlueLow", xpos - xvect, ypos - yvect), yvect);
    Raise(sub, xvect);
    FrameTimerWithArg(1, sub, CastBossMissileSliding);
    SetOwner(owner, sub);
    LookWithAngle(sub, 48);
}

void StoneRockCollide()
{
    if (!GetTrigger())
        return;

    if (!CurrentHealth(OTHER))
        return;

    if (IsAttackedBy(OTHER, GetOwner(SELF)))
    {
        Damage(OTHER, 0, 100, DAMAGE_TYPE_CRUSH);
    }
}

void StoneTouchTheGround(int stone)
{
    float xpos = GetObjectX(stone), ypos = GetObjectY(stone);
    int damHelper = DummyUnitCreateAt("CarnivorousPlant", xpos, ypos);
    int owner = GetOwner(stone);

    Frozen(damHelper, TRUE);
    SetOwner(owner, damHelper);
    SetCallback(damHelper, 9, StoneRockCollide);
    DeleteObjectTimer(damHelper, 1);

    DeleteObjectTimer(CreateObjectAt("BigSmoke", xpos, ypos), 12);

    Effect("JIGGLE", xpos, ypos, 0.0, 0.0);
    PlaySoundAround(stone, SOUND_WallDestroyedStone);
}

void FallenAttack(int stone)
{
    while (IsObjectOn(stone))
    {
        if (GetObjectZ(stone) > 10.0)
        {
            FrameTimerWithArg(1, stone, FallenAttack);
            Raise(stone, GetObjectZ(stone) - 10.0);
            break;
        }
        else
            StoneTouchTheGround(stone);
        Delete(stone);
        break;
    }
}

void StrikeCallbackAssociatedFinalBoss(int attacker, int victim)
{
    int stone = CreateObjectAt("RollingBoulder", GetObjectX(victim), GetObjectY(victim));

    FrameTimerWithArg(1, stone, FallenAttack);
    Frozen(stone, TRUE);
    UnitNoCollide(stone);
    Raise(stone, 220.0);
    SetOwner(attacker, stone);

    int deco = CreateObjectAt("Orb", GetObjectX(attacker), GetObjectY(attacker) + 3.0);

    UnitNoCollide(deco);
    DeleteObjectTimer(deco, 12);
    PlaySoundAround(deco, SOUND_BallBounce);
}

void FinalBossDeadFx(int mark)
{
    float xLowHigh[] = {GetObjectX(mark), GetObjectX(mark)+240.0};
    float yLowHigh[] = {GetObjectY(mark)-240.0, GetObjectY(mark)};
    float destXY[2];

    if (!RhombusGetXYCoor(&xLowHigh, &yLowHigh, &destXY))
        return;

    GreenSparkAt(destXY[0], destXY[1]);
    Effect("LESSER_EXPLOSION", destXY[0], destXY[1], 0.0, 0.0);
}

void VictoryFlagPick()
{
    UniPrint(OTHER, "당신은 우승자 이다!! 당신은 그럴 자격을 갖추었소...");

    PlaySoundAround(SELF, SOUND_BigGong);
    Effect("VIOLET_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
}

void PutVictoryFlag(float xpos, float ypos)
{
    int flagUnit = CreateObjectAt("VioletFlag", xpos, ypos);

    SetUnitCallbackOnPickup(flagUnit, VictoryFlagPick);
}

void EndExplosionAround(int mark)
{
    float xpos = GetObjectX(mark), ypos = GetObjectY(mark);
    int gen= CreateObjectAt("MonsterGenerator", xpos, ypos);

    Damage(gen, 0, 999, DAMAGE_TYPE_PLASMA);

    PutVictoryFlag(xpos, ypos);

    PlaySoundAround(gen, SOUND_FirewalkOn);
    Effect("JIGGLE", xpos, ypos, 0.0, 0.0);

    UniPrintToAll("승리하셨습니다_!!");
    UniPrintToAll("승리하셨습니다_!!");
    UniPrintToAll("승리하셨습니다_!!");
}

void FinalBossExplosionAround(int mark)
{
    while (IsObjectOn(mark))
    {
        if (GetDirection(mark))
        {
            FrameTimerWithArg(1, mark, FinalBossExplosionAround);
            LookWithAngle(mark, GetDirection(mark) - 1);
            FinalBossDeadFx(mark);
            break;
        }
        else
        {
            EndExplosionAround(mark);
        }
        
        Delete(mark);
        break;
    }
}

void FinalBossDeathEvent()
{
    int mark = CreateObjectAt("Skull", GetObjectX(SELF), GetObjectY(SELF));

    FrameTimerWithArg(1, mark, FinalBossExplosionAround);
    UnitNoCollide(mark);
    Frozen(mark, TRUE);
    LookWithAngle(mark, 32);
    GreenExplosion(GetObjectX(SELF), GetObjectY(SELF));
    CastSpellLocationLocation("SPELL_TURN_UNDEAD", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(SELF), GetObjectY(SELF));
    PlaySoundAround(mark, SOUND_HecubahDieFrame0A);
    UniChatMessage(SELF, "말도안돼..!? 내가 죽다니!! 억울해 ;n; (!!)", 150);
}

void OnHearEnemy()
{
    if (MonsterGetCurrentAction(SELF) != 0)
        return;

    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);

        Attack(SELF, OTHER);
    }
}

void CreateFinalBoss(float xpos, float ypos, int *pDest)
{
    int boss = CreateObjectById(OBJ_AIRSHIP_CAPTAIN, xpos, ypos);

    if (!CheckFatalErrorMon(boss))
        return;

    if (pDest)
        pDest[0] = boss;
    SetOwner(m_masterUnit, boss);
    AirshipCaptainSubProcess(boss);
    RegistUnitStrikeHook(boss);
    SetCallback(boss, 5, FinalBossDeathEvent);
    SetCallback(boss, 10, OnHearEnemy);
    SetUnitScanRange(boss, 600.0);
    GreenExplosion(xpos, ypos);
    PlaySoundAround(boss, SOUND_HecubahDieFrame283);
    AttachHealthbar(boss);
}

void BossRespectEvent()
{
    int count;

    if (++count == m_bossAmuletCount)
    {
        int sel = m_bossRespectPosArray[Random(0, m_bossAmuletCount-1)];

        CreateFinalBoss(GetObjectX(sel), GetObjectY(sel), NULLPTR);
        UniPrintToAll("방금, 맵 어딘가에서 이호성이 등장하였습니다...!!");
    }
    else
    {
        UniPrintToAll("이호성: 감당할 수 있겠나? 잠시후에 보자");
    }
    
    PlaySoundAround(SELF, SOUND_NecromancerTaunt);
    Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

void CreateFinalBossAmulet(int posUnit)
{
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    Delete(posUnit);

    int fflag = CreateObjectById(OBJ_RED_FLAG, xpos, ypos);

    int *propertySlot = MemAlloc(24);
    
    NoxByteMemset(propertySlot, 24, 0);
    int *ptr = UnitToPtr(fflag);

    if (ptr==0)
    {
        WriteLog("CreateFinalBossAmulet is null");
        return;
    }

    ptr[173] = propertySlot;

    UnitNoCollide(fflag);
    SetUnitCallbackOnPickup(fflag, BossRespectEvent);
    m_bossRespectPosArray[m_bossAmuletCount++] = CreateObjectAt("AmbBeachBirds", GetObjectX(fflag), GetObjectY(fflag));
}

void AllEnchantMarketDesc()
{
    TellStoryUnitName("AmuletDrop", "armrlook.c:EnchantedChainmailName", "올 엔첸 3만");
    UniPrint(OTHER, "올 엔첸트를 구입하시겠어요? 3만 골드입니다");
}

void AllEnchantMarketTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    int plr = CheckPlayer();

    if (plr < 0)
        return;

    if (GetGold(OTHER) >= 30000)
    {
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
        {
            UniPrint(OTHER, "당신은 이미 그 능력을 가졌습니다");
            return;
        }
        PlayerClassSetFlag(plr, PLAYER_FLAG_ALL_BUFF);
        PlayerSetAllBuff(OTHER);
        ChangeGold(OTHER, -30000);
        UniPrint(OTHER, "이 능력은 이제 당신의 것 입니다");
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        PlaySoundAround(OTHER, SOUND_AwardSpell);
    }
    else
    {
        UniPrint(OTHER, "금화가 부족합니다");
    }
}

void ExpensionAbilityDesc()
{
    TellStoryUnitName("AmuletDrop", "GeneralPrint:InformExpansion", "조심스럽게 걷기\n업그레이드\n2만");
    UniPrint(OTHER, "조심스럽게 걷기 능력을 강화하시겠어요? 금액은 2만 골드 입니다");
}

void ExpensionAbilityTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    int plr = CheckPlayer();

    if (plr < 0)
        return;

    if (GetGold(OTHER) >= 20000)
    {
        if (PlayerClassCheckFlag(plr, PLAYER_EXCHANGE_ABILITY))
        {
            UniPrint(OTHER, "당신은 이미 그 능력을 가졌습니다");
            return;
        }
        PlayerClassSetFlag(plr, PLAYER_EXCHANGE_ABILITY);
        ChangeGold(OTHER, -20000);
        UniPrint(OTHER, "이 능력은 이제 당신의 것 입니다");
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        PlaySoundAround(OTHER, SOUND_AwardSpell);
    }
    else
        UniPrint(OTHER, "이런!, 금액이 부족하군요...");
}

void InvincibleItemDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:InvulnerabilityPotionDescription", "아이템 무적화");
    UniPrint(OTHER, "아이템을 무적으로 만들어드릴 까요? 금액은 1회 시행 시 7,000 골드 입니다");
}

void InvincibleItemTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) < 7000)
    {
        UniPrint(OTHER, "잔액 부족입니다 손님");
        return;
    }
    int count = 0;

    AllItemSetInvincible(OTHER, &count);
    if (count)
    {
        ChangeGold(OTHER, -7000);
        UniPrint(OTHER, "처리되었습니다~");
    }
    else
    {
        UniPrint(OTHER, "처리할 것이 없네요. 처리되지 않았어요");
    }
    
}

void EnergyParSwordDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:GreatSwordDescription", "에너지파서드");
    UniPrint(OTHER, "에너지파 소드를 구입하시겠어요? 금액은 5만 입니다");
}

void EnergyParSwordTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) >= 50000)
    {
        ChangeGold(OTHER, -50000);
        int sword;

        PlacingEnergyParSword(GetObjectX(OTHER), GetObjectY(OTHER), &sword);
        Enchant(sword, EnchantList(ENCHANT_ANCHORED), 0.0);
        UniPrint(OTHER, "에너지파 서드를 구입하였어요! 그것은 당신 아래에 있어요");
    }
    else
    {
        UniPrint(OTHER, "잔액 부족입니다!");
    }    
}

void ArrowSwordDesc()
{
    TellStoryUnitName("AmuletDrop", "", "에로우 서드");
    UniPrint(OTHER, "에로우 서드를 구입하시겠어요? 금액은 4만 입니다");
}

void ArrowSwordTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 40000)
    {
        ChangeGold(OTHER, -40000);
        int sword;

        PlacingArrowSword(GetObjectX(OTHER), GetObjectY(OTHER), &sword);
        Enchant(sword, EnchantList(ENCHANT_FREEZE), 0.0);
        UniPrint(OTHER, "거래가 완료되었어요!");
    }
    else
    {
        UniPrint(OTHER, "잔액이 부족합니다");
    }
    
}

void EarthquakeHammerDesc()
{
    TellStoryUnitName("AmuletDrop", "", "지진망치");
    UniPrint(OTHER, "지진망치를 구입하시겠어요? 금액은 5만 입니다");
}

void EarthquakeHammerTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 40000)
    {
        ChangeGold(OTHER, -40000);
        int sword;

        PlacingEarthQuakeHammer(GetObjectX(OTHER), GetObjectY(OTHER), &sword);
        Enchant(sword, EnchantList(ENCHANT_FREEZE), 0.0);
        UniPrint(OTHER, "거래가 완료되었어요!");
    }
    else
    {
        UniPrint(OTHER, "잔액이 부족합니다");
    }
}

void WindChargingCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, GetOwner(SELF)))
        {
            Damage(OTHER, GetOwner(SELF), 65, DAMAGE_TYPE_ZAP_RAY);
            Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
        }
    }
}

void WindChargingLoop(int sub)
{
    while (IsObjectOn(sub))
    {
        int count = GetDirection(sub), owner = GetOwner(sub);

        if (CurrentHealth(owner) && count)
        {
            LookWithAngle(sub, --count);
            FrameTimerWithArg(1, sub, WindChargingLoop);

            int pusher = DummyUnitCreateAt("CarnivorousPlant", GetObjectX(owner) - GetObjectZ(sub), GetObjectY(owner) - GetObjectZ(sub+1));

            DeleteObjectTimer(pusher, 1);
            SetOwner(owner, pusher);
            SetCallback(pusher, 9, WindChargingCollide);
            Effect("YELLOW_SPARKS", GetObjectX(pusher), GetObjectY(pusher), 0.0, 0.0);
            PlaySoundAround(pusher, SOUND_PixieHit);            
            break;
        }
        Delete(sub++);
        Delete(sub++);
        break;
    }
}

void WindCharging(int pUnit)
{
    float xvect = UnitAngleCos(pUnit, 19.0), yvect = UnitAngleSin(pUnit, 19.0);
    int sub = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(pUnit) + xvect, GetObjectY(pUnit) + yvect);

    Raise(sub, xvect);
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sub), GetObjectY(sub)), yvect);
    LookWithAngle(sub, 16);
    SetOwner(pUnit, sub);
    PlaySoundAround(sub, SOUND_SummonAbort);
    FrameTimerWithArg(1, sub, WindChargingLoop);
}

void AllItemSetInvincible(int holder, int *pCount)
{
    int cur = GetLastItem(holder), count = 0;

    while (cur)
    {
        if (!UnitCheckEnchant(cur, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(cur, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            Nop(++count);
        }
        cur = GetPreviousItem(cur);
    }
    if (pCount)
        pCount[0] = count;
}

void OnUseSozu()
{
    if (!IsPlayerUnit(OTHER))
        return;
    
    Delete(SELF);
    int plr = CheckPlayer();

    if (plr < 0)
        return;

    if (!PlayerClassCheckFlag(plr, PLAYER_FLAG_BOOSTER))
        PlayerClassSetFlag(plr, PLAYER_FLAG_BOOSTER);

    m_boostTime[plr] = 300;
    UniPrint(OTHER, "소주를 마셨습니다. 잠시동안 이동속도 대폭증가");
}

void ShotDeathRayInRange()
{
    Damage(OTHER, SELF, 150, DAMAGE_TYPE_ZAP_RAY);
    Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
}

void DeathRayStaffOnUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 60)
    {
        UniPrint(OTHER, "쿨다운 입니다... 잠시만 기다려 주세요");
        return;
    }

    float xpos = GetObjectX(OTHER), ypos = GetObjectY(OTHER);
    int vision = CreateObjectAt("WeirdlingBeast", xpos + UnitAngleCos(OTHER, 5.0), ypos + UnitAngleSin(OTHER, 5.0));

    SetOwner(OTHER, vision);
    SetUnitScanRange(vision, 400.0);
    LookWithAngle(vision, GetDirection(OTHER));
    SetCallback(vision, 3, ShotDeathRayInRange);
    DeleteObjectTimer(vision, 1);
    PlaySoundAround(vision, SOUND_DeathRayKill);
}

void MagicStaffClassPick()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 10)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        AbsolutelyWeaponPickupAndEquip(OTHER, SELF);
        UniPrint(OTHER, "무기를 바꾼 상태에서 이 마법 지팡이를 장착하려면 버렸다가 다시 주워야 합니다");
    }
}

void PlacingDeathRayStaff(float xpos, float ypos, int *pDest)
{
    int wand = CreateObjectAt("DeathRayWand", xpos, ypos);

    if (pDest)
        pDest[0] = wand;

    SetUnitCallbackOnPickup(wand, MagicStaffClassPick);
    SetUnitCallbackOnUseItem(wand, DeathRayStaffOnUse);
}

void DeathRayStaffDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:GreatSwordDescription", "데스레이 스태프");
    UniPrint(OTHER, "데스레이 스태프를 구입하시겠어요? 금액은 6만 입니다");
}

void DeathRayStaffTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) >= 60000)
    {
        ChangeGold(OTHER, -60000);
        int staff;

        PlacingDeathRayStaff(GetObjectX(OTHER), GetObjectY(OTHER), &staff);

        Enchant(staff, EnchantList(ENCHANT_ANCHORED), 0.0);
        UniPrint(OTHER, "데스레이 스태프를 구입하였어요! 그것은 당신 아래에 있어요");
    }
    else
    {
        UniPrint(OTHER, "잔액 부족입니다!");
    }
}

void BuySozuDesc()
{
    TellStoryUnitName("AmuletDrop", "Con05:Bartender01", "소주한잔");
    UniPrint(OTHER, "소주 한병 사시겠어요? 금액은 5천 골드 입니다");
}

void BuySozuTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) >= 5000)
    {
        ChangeGold(OTHER, -5000);
        int sozu = CreateObjectAt("Cider", GetObjectX(OTHER), GetObjectY(OTHER));

        UnitNoCollide(sozu);
        SetUnitCallbackOnUseItem(sozu, OnUseSozu);
    }
    else
    {
        UniPrint(OTHER, "잔액이 부족합니다 또는 성인이 아니신건지...?");
    }
    
}

static void OnPlayerEntryMap(int pInfo)
{
    ExecWhenPlayerEnteredMap(pInfo);
}

