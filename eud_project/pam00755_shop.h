
#include "pam00755_gvar.h"
#include "pam00755_utils.h"
#include "pam00755_player.h"
#include "libs/networkRev.h"
#include "libs/waypoint.h"
#include "libs/gui_window.h"
#include "libs/queueTimer.h"
#include "libs/fixtellstory.h"
#include "libs/weapon_effect.h"
#include "libs/itemproperty.h"

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10

void deferredChangeDialogContext(int *pParams) //override
{
    int user=pParams[0];
    int message=pParams[1];

    FreeSmartMemEx(pParams);
    if (!CurrentHealth(user))
        return;

    if (user==GetHost())
    {
        onPopupMessageChanged(message);
        return;
    }
    ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, message);
}

void sendChangeDialogCtx(int user, int param)
{
    int params[]={
        user,
        param,
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
}

void popupMessgeStorage(int n, string set, string *get)
{
    string table[32];
    int *sub=get;

    if(sub)
    {
        sub[0]=table[n];
        return;
    }
    table[n]=set;
}

void guiDialogCtx(int set, int *get)
{
    int ctx;
    if (set)
        ctx=set;
    if (get)
        get[0]=ctx;
}

void initDialog()
{
    int *wndptr=0x6e6a58;
    int ctx;

    GUIFindChild(wndptr[0], 3901, &ctx);
    guiDialogCtx(ctx,0);
}

void onPopupMessageChanged(int messageId)
{
    string s;
    int ctx;

    guiDialogCtx(0,&ctx);
    popupMessgeStorage(messageId, "null", &s);
    GUISetWindowScrollListboxText(ctx, s, NULLPTR);
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[1])
    {
        onPopupMessageChanged(type[1]);
        type[1]=0;
    }
    if (type[0])
    {
        type[1]=type[0];
        type[0]=0;
    }
    FrameTimer(1, ClientProcLoop);
}

#define GUI_DIALOG_TELEPORT 1
#define GUI_DIALOG_BERSERKER_CHARGE 2
#define GUI_DIALOG_HARPOON 3
#define GUI_DIALOG_WARCRY 4
#define GUI_DIALOG_THREAD_LIGHTLY 5
#define GUI_DIALOG_SELL_ALL_GERM 6
#define GUI_DIALOG_GOD_ITEM 7
#define GUI_DIALOG_GO_GAUNTLET_NOW 8
#define GUI_DIALOG_GOOD_LUCKY 9
#define GUI_DIALOG_BATTLE_FIGHT 10
#define GUI_DIALOG_SHINE_WISP 11
#define GUI_DIALOG_AIRSHIP_CAPTAIN 12
#define GUI_DIALOG_WEAPON_SHOP 13
#define GUI_DIALOG_SPEED_SHOP 14

void initPopupMessages()
{
    string *str=NULLPTR;
    popupMessgeStorage(GUI_DIALOG_TELEPORT, "마지막 위치로 공간이동을 하시겠습니까? 예를 누르세요", str );
    popupMessgeStorage(GUI_DIALOG_BERSERKER_CHARGE, "버저커 차지 능력을 배우시겠습니까? 20,000 골드가 필요합니다", str );
    popupMessgeStorage(GUI_DIALOG_HARPOON, "전사의 능력인 작살 기술을 배우시려면 20,000 골드를 지불하십시오", str );
    popupMessgeStorage(GUI_DIALOG_WARCRY, "전사의 함성 능력을 배우시겠습니까? 20,000골드가 필요합니다", str );
    popupMessgeStorage(GUI_DIALOG_THREAD_LIGHTLY, "순보 능력을 배우시겠습니까? 짧은거리를 빠르게 이동하는 능력입니다. 25,000골드 필요함", str );
    popupMessgeStorage(GUI_DIALOG_SELL_ALL_GERM, "당신이 가진 모든 보석을 팔아드리겠습니다, 수락하려면 예 버튼을 누르세요", str);
    popupMessgeStorage(GUI_DIALOG_GOD_ITEM, "당신의 모든 아이템의 내구도를 무한으로 만들어드리겠습니다, 회차 당 4,500 골드입니다", str);
    popupMessgeStorage(GUI_DIALOG_GO_GAUNTLET_NOW, "가게! 거기에 멍하니 서 있지만 말고 움직이라 이말이야!", str);
    popupMessgeStorage(GUI_DIALOG_GOOD_LUCKY, "내가 여기 문을 열어줄 터이니, 행운을 빌겠네. 아마 당신에게 이곳은 매우 쉬울 것이여", str);
    popupMessgeStorage(GUI_DIALOG_BATTLE_FIGHT, "네의 꿈은 모두 부질없는 것이야. 네가 우리 땅에 살아있는 육체로 오염시키도록 가만두지 않을것이야", str);
    popupMessgeStorage(GUI_DIALOG_SHINE_WISP, "에구 무서워! 나 때릴거야!?", str);
    popupMessgeStorage(GUI_DIALOG_AIRSHIP_CAPTAIN, "이 맵의 모든 구간을 깨셨습니다! 당신이 최초이며 최고입니다! 아직 많이 부족한 맵이지만 수고하셨습니다", str);
    popupMessgeStorage(GUI_DIALOG_WEAPON_SHOP, "아이스크라운 서드를 구입하실래요? 비싸지만은 그래도 좋을 것이에요. 가격은 85,000골드입니다", str);
    popupMessgeStorage(GUI_DIALOG_SPEED_SHOP, "랜덤하게 이속갑옷을 드리고 있습니다, 회차 당 가격은 18,799 골드 입니다", str);
}

int SellGerm(int inv)
{
    int thingId = GetUnitThingID(inv), pic;

    if (thingId >= OBJ_DIAMOND && thingId <= OBJ_RUBY)
    {
        Delete(inv);

        int pay[] = {1000, 5000, 10000};
        return pay[OBJ_RUBY - thingId];
    }
    else
        return 0;
}

int FindItemGerm(int holder)
{
    int inv = GetLastItem(holder), res = 0, nextInv;

    while (inv)
    {
        nextInv = GetPreviousItem(inv);
        res += SellGerm(inv);
        inv = nextInv;
    }
    return res;
}

void onSellGermDialogStart()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_SELL_ALL_GERM);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "모든 보석\n판매하기");
}

void StartHorrendousTalk()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_GO_GAUNTLET_NOW);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "훈육 조교");
}

void StartLuckyBoyTalk()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_GOOD_LUCKY);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "럭키Boy");
}
void StartLizardTalk()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_BATTLE_FIGHT);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "도롱뇽");
}
void StartWispTalk()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_SHINE_WISP);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "수줍은\n위스프");
}

void tradeSellGerm()
{
    if (GetAnswer(SELF)^1)
        return;

    int trdRes = FindItemGerm(OTHER);
    char msg[128];

    if (trdRes)
    {
        ChangeGold(OTHER, trdRes);
        NoxSprintfString(msg, "가지고 있던 모든 보석을 팔아서 %d골드를 추가했습니다", &trdRes, 1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
    else
        UniPrint(OTHER, "당신은 보석을 하나도 가지고 있지 않아요. 거래를 계속할 수 없습니다");
}

void placeSellGerm(int location)
{
    int npc=DummyUnitCreateById(OBJ_URCHIN_SHAMAN, LocationX(location), LocationY(location));

    SetDialog(npc,"YESNO",onSellGermDialogStart,tradeSellGerm);
    StoryPic(npc,"PriestPic");
    LookWithAngle(npc,32);
}

void onAwardAbilityDialogStart()
{
    char messages[]={GUI_DIALOG_BERSERKER_CHARGE,GUI_DIALOG_HARPOON,GUI_DIALOG_WARCRY,GUI_DIALOG_THREAD_LIGHTLY,};
    string names[]={"버저커차지\n구입", "능력-작살\n구입", "전사의 함성\n구입", "순보 구입",};
    int id=GetUnit1C(SELF);

    sendChangeDialogCtx(GetCaller(), messages[id]);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", names[id]);
}

void tradeAwardAbility()
{
    if (GetAnswer(SELF)^1)
        return;

    int pIndex=GetPlayerIndex(OTHER);
    if (pIndex<0)
        return;

    short charges[]={20000, 20000, 20000, 25000,};
    int flags[]={PLAYER_FLAG_BERSERKER_CHARGE, PLAYER_FLAG_HARPOON, PLAYER_FLAG_WARCRY, PLAYER_FLAG_THREADLIGHTLY,};
    string names[]={"버저커차지", "작살", "전사의 함성", "순보",};
    int id=GetUnit1C(SELF);

    if (GetGold(OTHER) >= charges[id])
    {
        if (PlayerClassCheckFlag(pIndex, flags[id]))
        {
            UniPrint(OTHER, "그 능력을 이미 배우셨습니다!");
            return;
        }
        PlayerClassSetFlag(pIndex, flags[id]);
        ChangeGold(OTHER, -charges[id]);
        GreenSparkAt(GetObjectX(OTHER),GetObjectY(OTHER));
        int args =  StringUtilGetScriptStringPtr(names[id]);
        char message[128];

        NoxSprintfString(message, "당신은 %s 능력을 배웠습니다", &args, 1);
        UniPrint(OTHER, ReadStringAddressEx(message));
        return;
    }
    UniPrint(OTHER, "금화가 부족합니다!");    
}

void placeAbilityMarket(short location, int abilityId)
{
    int npc=DummyUnitCreateById(OBJ_ARCHER, LocationX(location), LocationY(location));

    SetDialog(npc,"YESNO",onAwardAbilityDialogStart,tradeAwardAbility);
    StoryPic(npc,"MorganPic");
    LookWithAngle(npc,32);
    SetUnit1C(npc,abilityId);
}

void onTeleportingNpcDialogStart()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_TELEPORT);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "마지막위치로\n공간이동");
}

void onTeleportingNpcDialogEnd()
{
    if (GetAnswer(SELF)^1)
        return;

    int pIndex=GetPlayerIndex(OTHER);
    if (pIndex<0)
        return;

    int mark=GetUserRespawnMark(pIndex);

    MoveObject(OTHER,GetObjectX(mark),GetObjectY(mark));
}

void placeTeleportingNpc(short locationId)
{
    int npc=DummyUnitCreateById(OBJ_WIZARD, LocationX(locationId), LocationY(locationId));

    SetDialog(npc,"YESNO",onTeleportingNpcDialogStart,onTeleportingNpcDialogEnd);
    StoryPic(npc,"HorvathPic");
    LookWithAngle(npc, 64);
}

int SetInvulnerabilityItem(int unit)
{
    int count = 0, inv = GetLastItem(unit);

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void onGodItemDialogStart()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_GOD_ITEM);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "내구도 무한");
}

void tradeGodItem()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=4500)
    {
        int res = SetInvulnerabilityItem(OTHER);

        if (!res)
        {
            UniPrintToAll("처리할 아이템이 없습니다- 거래 취소됨");
            return;
        }
        DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE, GetObjectX(SELF),GetObjectY(SELF)), 18);
        ChangeGold(OTHER,-4500);
        UniPrintToAll("거래가 완료되었습니다");
        return;
    }
    UniPrintToAll("금화가 부족하군요!");
}

void placeGodItemMarket(int locationId)
{
    int npc=DummyUnitCreateById(OBJ_SWORDSMAN, LocationX(locationId), LocationY(locationId));

    SetDialog(npc,"YESNO",onGodItemDialogStart,tradeGodItem);
    StoryPic(npc,"MordwynPic");
    LookWithAngle(npc, 96);
}

void startFinalNpcDialog()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_AIRSHIP_CAPTAIN);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "-제작진-");
}
void endFinalNpcDialog()
{
    if (GetAnswer(SELF)^1)
        return;

    UniPrint(OTHER,"그동안 수고하셨습니다! -제작자");
}

void placeFinalNPC(int locationId)
{
    int npc=CreateObjectById(OBJ_AIRSHIP_CAPTAIN,LocationX(locationId),LocationY(locationId));

    Frozen(npc,TRUE);
    StoryPic(npc,"AirshipCaptainPic");
    SetDialog(npc,"YESNO",startFinalNpcDialog,endFinalNpcDialog);
}
void onIceSplas()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER))
                Damage(OTHER, SELF, 225, DAMAGE_TYPE_PLASMA);
        }
    }
}
void OnIcecrystalCollide()
{
    while (TRUE)
    {
        if (!GetCaller())
            1;
        else if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(GetTrigger()+1);

            if (!IsAttackedBy(OTHER, owner))
                break;

            SplashDamageAtEx(owner, GetObjectX(OTHER),GetObjectY(OTHER),81.0,onIceSplas );
        }
        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(GetTrigger() + 1);
        Delete(SELF);
        break;
    }
}

int CreateIceCrystal(int owner, float gap, int lifetime)
{
    int cry=CreateObjectAt("GameBall", GetObjectX(owner) + UnitAngleCos(owner, gap), GetObjectY(owner) + UnitAngleSin(owner, gap));

    SetOwner(owner, CreateObjectAt("ImaginaryCaster", GetObjectX(cry), GetObjectY(cry)));
    SetUnitCallbackOnCollide(cry, OnIcecrystalCollide);
    DeleteObjectTimer(cry, lifetime);
    DeleteObjectTimer(cry+1, lifetime + 15);
    return cry;
}

void CUserWeaponShootIceCrystal()
{
    PushObject(CreateIceCrystal(OTHER, 19.0, 75), 22.0, GetObjectX(OTHER), GetObjectY(OTHER));
    WispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(OTHER, SOUND_LightningWand);
    PlaySoundAround(OTHER, SOUND_ElevLOTDDown);
}

void SetWeaponUserProperty1(int weapon)
{
    int spro;
    int fxcode,exec;

    if (!spro)
    {
        // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_VIOLET_SPARK, SOUND_MetalHitEarth, &fxcode);
        SpecialWeaponPropertyCreate(&spro);
        SpecialWeaponPropertyExecuteScriptCodeGen(CUserWeaponShootIceCrystal, &exec);
        // SpecialWeaponPropertySetFXCode(spro, fxcode);
        SpecialWeaponPropertySetExecuteCode(spro, exec);
    }
    SetWeaponPropertiesDirect(weapon, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, spro);
}

void weaponShopDesc()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_WEAPON_SHOP);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "-특수무기1-");
}

void weaponShopTrade()
{
    if (GetAnswer(SELF)^1)
        return;
    if (GetGold(OTHER)>=85000)
    {
        ChangeGold(OTHER,-85000);
        SetWeaponUserProperty1(CreateObjectById(OBJ_GREAT_SWORD, GetObjectX(OTHER),GetObjectY(OTHER)));
        UniPrint(OTHER, "당신 아래에 무기가 있어요");
    }
    else
        UniPrint(OTHER,"금액부족");
}

void weaponShopPlace(short locationId)
{
    int npc=DummyUnitCreateById(OBJ_HECUBAH, LocationX(locationId), LocationY(locationId));

    Frozen(npc,TRUE);
    StoryPic(npc,"AirshipCaptainPic");
    SetDialog(npc,"YESNO",weaponShopDesc,weaponShopTrade);
}

void speedShopDesc()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_SPEED_SHOP);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", "-갑옷1-");
}

void placeRandomARMM(float x,float y)
{
    short tys[]={OBJ_ORNATE_HELM,OBJ_STEEL_HELM,OBJ_STEEL_SHIELD,OBJ_WOODEN_SHIELD,OBJ_PLATE_ARMS,OBJ_PLATE_BOOTS,OBJ_PLATE_LEGGINGS,OBJ_CHAIN_COIF,
        OBJ_CHAIN_LEGGINGS,OBJ_CHAIN_TUNIC,};

    SetArmorPropertiesDirect(CreateObjectById(tys[Random(0,sizeof(tys)-1)],x,y),ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial5,ITEM_PROPERTY_Speed4,ITEM_PROPERTY_Speed4);
}

void speedShopTrade()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=18799)
    {
        ChangeGold(OTHER,-18799);
        placeRandomARMM( GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER, "당신 아래에 갑옷가 있어요");
    }
    else
        UniPrint(OTHER,"금액부족");
}

void speedShopPlace(short locationId)
{
    int npc=DummyUnitCreateById(OBJ_HECUBAH, LocationX(locationId), LocationY(locationId));

    Frozen(npc,TRUE);
    StoryPic(npc,"AirshipCaptainPic");
    SetDialog(npc,"YESNO",speedShopDesc,speedShopTrade);
}

#define ABILITY_MARKET_BERSERKER_CHARGE 0
#define ABILITY_MARKET_HARPOON 1
#define ABILITY_MARKET_WARCRY 2
#define ABILITY_MARKET_THREAD_LIGHTLY 3

void InitializeShop()
{
    placeTeleportingNpc(162);
    placeAbilityMarket(164,ABILITY_MARKET_BERSERKER_CHARGE);
    placeAbilityMarket(165,ABILITY_MARKET_HARPOON);
    placeAbilityMarket(166,ABILITY_MARKET_WARCRY);
    placeAbilityMarket(167,ABILITY_MARKET_THREAD_LIGHTLY);
    placeSellGerm(168);
    placeGodItemMarket(169);
    placeFinalNPC(248);
    weaponShopPlace(258);
    speedShopPlace(259);
}
