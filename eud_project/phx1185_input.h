
#include "observerPatch.h"
#include "libs/keyscan.h"
#include "libs/queueTimer.h"
#include "libs/networkRev.h"

// #define SEND_UP_SHIFT 
#define SEND_LEFT_SHIFT 1
#define SEND_RIGHT_SHIFT 2
#define SEND_L_CTRL_SHIFT 3
#define SEND_KEY_I_SHIFT 4
#define SEND_KEY_Z_SHIFT 5

void ServerInputLoop(int *pState)
{
    int up = KeyboardIOCheckKeyEx(0xc8);
    int ctrl = KeyboardIOCheckKeyEx(VIRTUAL_KEY_LCTRL)<<SEND_L_CTRL_SHIFT;
    int left = KeyboardIOCheckKeyEx(0xcb)<<SEND_LEFT_SHIFT;
    int right = KeyboardIOCheckKeyEx(0xcd)<<SEND_RIGHT_SHIFT;
    int iKey = KeyboardIOCheckKeyEx(VIRTUAL_KEY_I)<<SEND_KEY_I_SHIFT;
    int zKey = KeyboardIOCheckKeyEx(VIRTUAL_KEY_Z)<<SEND_KEY_Z_SHIFT;
    int prevState, state = up|left|right|ctrl|iKey|zKey;

    if (prevState!=state)
    {
        if (KeyboardIO_IsInputMode())
            state=0;
            
        prevState=state;
        pState[31]=state;
    }
    PushTimerQueue(3, pState, ServerInputLoop);
}

void ClientKeyHandlerRemix(int clientId)
{
    int up = KeyboardIOCheckKeyEx(0xc8);
    int ctrl = KeyboardIOCheckKeyEx(VIRTUAL_KEY_LCTRL)<<SEND_L_CTRL_SHIFT;
    int left=KeyboardIOCheckKeyEx(0xcb)<<SEND_LEFT_SHIFT;
    int right=KeyboardIOCheckKeyEx(0xcd)<<SEND_RIGHT_SHIFT;
    int iKey = KeyboardIOCheckKeyEx(VIRTUAL_KEY_I)<<SEND_KEY_I_SHIFT;
    int zKey = KeyboardIOCheckKeyEx(VIRTUAL_KEY_Z)<<SEND_KEY_Z_SHIFT;
    int prevState, state = up|left|right|ctrl|iKey|zKey;

    if (prevState!=state)
    {
        if (KeyboardIO_IsInputMode())
            state=0;
        prevState=state;

        char testPacket[]={0x3d, 0x00+(clientId*4),0x10,0x75,0x00, 0, 0,0,0};
        int *p = &testPacket[1];

        p[1] = prevState;
        NetClientSendRaw(31, 0, testPacket, sizeof(testPacket));
    }
}

void ClientClassTimerLoop(int clientId)
{
    ClientKeyHandlerRemix(clientId);
    FrameTimerWithArg(1, clientId, ClientClassTimerLoop);
}

void InitialServerPatchMapleOnly() //maple only
{
    int oldProtect;

    WinApiVirtualProtect(0x51cf84, 1024, 0x40, &oldProtect);
    char *p = 0x51cf84;

    p[23]=0x7a;
    SetMemory(0x51d0c8, 0x513f10);
    WinApiVirtualProtect(0x51cf84, 1024, oldProtect, NULLPTR);
    //46 8B 06 8B 4E 04 89 08 83 C6 09 E9 24 8F 00 00

    WinApiVirtualProtect(0x513f10, 256, 0x40, &oldProtect);

    char servcode[]={0x46, 0x8B, 0x06, 0x8B, 0x4E, 0x04, 0x89, 0x08, 0x83, 0xC6, 0x08, 0xE9, 0x24, 0x8F, 0x00, 0x00};

    NoxByteMemCopy(servcode, 0x513f10, sizeof(servcode));
    WinApiVirtualProtect(0x513f10, 256, oldProtect, NULLPTR);

    SetMemory(0x5c31ec, 0x513f30);
    DoObserverPatchRev();
}
