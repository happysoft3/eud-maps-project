
#include "phx1185_gvar.h"
#include "phx1185_utils.h"
#include "libs/itemproperty.h"
#include "libs/format.h"
#include "libs/fixtellstory.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/gui_window.h"
#include "libs/networkRev.h"
#include "libs/bind.h"
#include "libs/hash.h"
#include "libs/fxeffect.h"

int TryItemUpgrade(int pIndex, int item) //virtual
{ }

int TryIncreaseUpgradeRate(int user) //virtual
{ }

#define GUI_DIALOG_MESSAGE_WEAPON_2 1
#define GUI_DIALOG_MESSAGE_WEAPON_3 2
#define GUI_DIALOG_MESSAGE_WEAPON_4 3
#define GUI_DIALOG_MESSAGE_WEAPON_5 4
#define GUI_DIALOG_MESSAGE_WEAPON_6 5
#define GUI_DIALOG_MESSAGE_UPGRADE_WEAPON 6
#define GUI_DIALOG_MESSAGE_UPGRADE 7
#define GUI_DIALOG_MESSAGE_BOSS_MONSTER 8
#define GUI_DIALOG_MESSAGE_SELL_WEAPON 9
#define GUI_DIALOG_MESSAGE_WEAPON_7 10
#define GUI_DIALOG_MESSAGE_WEAPON_8 11
#define GUI_DIALOG_MESSAGE_WEAPON_1 12
#define GUI_DIALOG_MESSAGE_WEAPON_9 13
#define GUI_DIALOG_MESSAGE_WEAPON_10 14

void onPopupMessageChanged(int messageId)
{
    GUISetWindowScrollListboxText(GetDialogCtx(), GetPopupMessage(messageId), NULLPTR);
}

void initDialog()
{
    int *wndptr=0x6e6a58;
    int ctx=NULLPTR;
    GUIFindChild(wndptr[0], 3901, &ctx);
    if (ctx!=NULLPTR)
        SetDialogCtx(ctx);
}

void initFillPopupMessageArray(string *pPopupMessage)
{
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_1]="식칼을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+5. 가격: 9골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_2]="커터칼을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+8. 공격속도+4. 메소드롭률+10. 가격: 41골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_3]="도끼을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+11. 치명타+6. 공격속도-3. 가격: 50골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_4]="청동검을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+7. 치명타+12. 공격속도+9. 가격: 48골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_5]="뿅망치을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+22. 공격속도-10. 넓은 공격범위. 가격: 75골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_6]="식칼(강화10강)을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+6. 강화+10 가격: 330골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_UPGRADE_WEAPON]="현재 장착중인 무기를 강화하시겠어요? !!강화에 실패할 수도 있어요!! 가격은 대화상자 위 문구를 확인하세요";
    pPopupMessage[GUI_DIALOG_MESSAGE_UPGRADE]="강화 성공확률을 구입하시겠어요? 가격은 회차당 50골드입니다. 강화성공확률 1퍼 증가";
    pPopupMessage[GUI_DIALOG_MESSAGE_BOSS_MONSTER]="보스몬스터와 싸우실래요? 2분안에 잡아야 합니다. 잡지 못하여도 패널티는 없어요";
    pPopupMessage[GUI_DIALOG_MESSAGE_SELL_WEAPON]="장착중인 무기를 저에게 파시겠어요? 무기마다 가격이 다르니 주의하세요!";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_7]="황제의 검(강화7강)을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+46. 치명타+33. 강화+7 가격: 523골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_8]="무녀도(강화8강)을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+31. 공격속도+13. 치명타+14. 강화+8 가격: 517골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_10]="문라이트 서드(강화8강)을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+50. 강화+8 가격: 536골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_9]="헬파이어엑스(강화4강)을 구입하려면, '예'를 누르세요. 효력: 기본 데미지+76. 치명타+42. 강화+4 가격: 1200골드";
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[1])
    {
        onPopupMessageChanged(type[1]);
        type[1]=0;
    }
    if (type[0])
    {
        type[1]=type[0];
        type[0]=0;
    }
    FrameTimer(3, ClientProcLoop);
}

void InitPopupMessage(string *popupMessages)
{
    initDialog();
    // initspecialWeaponShop();
    initFillPopupMessageArray(popupMessages);
}

void deferredChangeDialogContext(int *pParams) //override
{
    int user=pParams[0];
    int message=pParams[1];

    FreeSmartMemEx(pParams);
    if (!CurrentHealth(user))
        return;

    if (user==GetHost())
    {
        onPopupMessageChanged(message);
        return;
    }
    ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, message);
}

void sendDialogMessage(int user, int messageId)
{
    int params[]={
        user,
        messageId,
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
}

int showSellWeaponWorth(int user,int inv)
{
    if (!ToInt(GetObjectX(inv)))
        return 0;

    int price=0, powerLv = GetUnit1C(inv);
    char message[128];

    HashGet(GetSellWeaponHash(), GetUnitThingID(inv), &price, FALSE);
    if (powerLv)
        price+=(powerLv*5);
    NoxSprintfString(message, "그 무기의 판매 시 돌려받는 가격은 %d 골드 입니다", &price, 1);
    UniPrint(user, ReadStringAddressEx(message));
    return price;
}

void bossCheckerCountdown(int checker)
{
    if (!ToInt(GetObjectX(checker)))
        return;

    int boss = GetBossMonster();

    if (MaxHealth(boss))
    {
        PushTimerQueue(30, checker, bossCheckerCountdown);
        return;
    }
    int dur=GetDirection(checker);

    if (dur)
    {
        PushTimerQueue(30, checker, bossCheckerCountdown);
        LookWithAngle(checker, dur-1);
        return;
    }
    Delete(checker);
    UniPrintToAll("!--지금, 보스소환 쿨다운이 완료되었습니다--!");
}

void DialogTradeCallBoss()
{
    if (GetAnswer(SELF)^1)
        return;

    int timeChecker=GetUnit1C(SELF);

    if (ToInt(GetObjectX(timeChecker)))
    {
        UniPrint(OTHER, "쿨다운 입니다...");
        return;
    }

    if (TrySummonBossMonster(GetTrigger()))
    {
        timeChecker=CreateObjectById(OBJ_MANA_BOMB_ORB, GetObjectX(SELF), GetObjectY(SELF));

        SetUnit1C(SELF, timeChecker);
        LookWithAngle(timeChecker, 240);
        PushTimerQueue(1, timeChecker, bossCheckerCountdown);
        UniPrint(OTHER, "보스 몬스터가 소환되었습니다. 제한시간 이내로 잡아야 합니다");
        return;
    }
    UniPrint(OTHER, "이미 보스 몬스터가 소환되어 있습니다");
}

void DialogExplainCallBoss()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_BOSS_MONSTER);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "보스몹 소환");
}

void itemCommon(int item, int powerLv)
{
    SetItemPropertyAllowAllDrop(item);
    SetUnit1C(item,powerLv);
    SetUnitCallbackOnDiscard(item, OnDropItem);
}

void nullUseFunction()
{ }

void CopyWeapon(int target, int holder)
{
    int weapon=CreateObjectById(GetUnitThingID(target), GetObjectX(holder),GetObjectY(holder)-5.0);
    int *ptr=UnitToPtr(weapon);
    int *targetPtr = UnitToPtr(target);

    itemCommon(weapon, GetUnit1C(target));
    SetUnitCallbackOnUseItem(weapon, nullUseFunction);
    SetMemory(ptr+ImportUseItemFuncOffset(), GetMemory(targetPtr+ImportUseItemFuncOffset()));
    int flags=GetUnitFlags(weapon);

    if (flags&UNIT_FLAG_NO_COLLIDE)
        flags^=UNIT_FLAG_NO_COLLIDE;
    if (!(flags&UNIT_FLAG_NO_PUSH_CHARACTERS))
        flags^=UNIT_FLAG_NO_PUSH_CHARACTERS;
    SetUnitFlags(weapon, flags);
    Frozen(weapon, TRUE);
    SetOwner(GetAbandondedItemOwner(),weapon);
}

void OnDropItem()
{
    int pIndex = GetPlayerIndex(OTHER);

    if (pIndex>=0)
    {
        CopyWeapon(SELF, GetPlayerAvata(pIndex));
        OnItemDropped(pIndex, GetTrigger());
    }
    Delete(SELF); //임시처리임...
}

void commonWeaponClicked(int user, int inv, string fmt)
{
    int pIndex =GetPlayerIndex(user);

    if (pIndex<0)
        return;

    char buff[128];
    int dam=GetUnit1C(SELF);

    TryItemUse(pIndex, inv);
    NoxSprintfString(buff, fmt, &dam, 1);
    showSellWeaponWorth(user, inv);
    UniPrint(user, ReadStringAddressEx(buff));
    if (GetPlayerEquipment(pIndex)!=inv)
        UniPrint(user, "그 무기를 장착하려면 한번 더 클릭해주세요");
}

void ExplainWeaponSword()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 식칼. 기본 데미지- 6. 강화 데미지- %d");
}

int WeaponSword(int user, int powerLv)
{
    int weapon=CreateObjectById(OBJ_AMULET_OF_CLARITY, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainWeaponSword);
    itemCommon(weapon, powerLv);
    return weapon;
}

int tradeCommonChecking(int user, int pay, short thingId)
{
    if (GetAnswer(SELF)^1)
        return FALSE;

    if (GetGold(user)<pay)
    {
        UniPrint(user, "골드가 부족해요");
        return FALSE;
    }
    if (HasSpecifyItem(user, thingId))
    {
        UniPrint(user, "이미 그것을 가졌어요");
        return FALSE;
    }
    ChangeGold(user, -pay);
    UpdatePlayerGold(user);
    UniPrint(user, "거래가 완료되었어요");
    return TRUE;
}

void DialogTradeWeaponSword()
{
    if (!tradeCommonChecking(OTHER, 9, OBJ_AMULET_OF_CLARITY))
        return;

    SafetyPickup(OTHER, WeaponSword(OTHER, 0));
}

void DialogExplainWeaponSword()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_1);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "식칼 구입");
}
void DialogTradeWeaponSwordLv10()
{
    if (!tradeCommonChecking(OTHER, 330, OBJ_AMULET_OF_CLARITY))
        return;

    SafetyPickup(OTHER, WeaponSword(OTHER, 10));
}

void DialogExplainWeaponSwordLv10()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_6);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "식칼 구입");
}

void ExplainShortSword()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 커터칼. 기본 데미지+8. 공속+4. 메소 드롭률+10. 강화 데미지- %d");
}

int WeaponShortSword(int user)
{
    int weapon=CreateObjectById(OBJ_AMULETOF_COMBAT, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainShortSword);
    itemCommon(weapon, 0);
    return weapon;
}

void DialogTradeWeaponShortSword()
{
    if (!tradeCommonChecking(OTHER, 41, OBJ_AMULETOF_COMBAT))
        return;

    SafetyPickup(OTHER, WeaponShortSword(OTHER));
}

void DialogExplainWeaponShortSword()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_2);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "커터칼 구입");
}

void ExplainPortableAxe()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 도끼. 기본 데미지+14. 공속-3. 치명타+12. 강화 데미지+%d");
}

int WeaponPortableAxe(int user)
{
    int weapon=CreateObjectById(OBJ_AMULETOF_MANIPULATION, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainPortableAxe);
    itemCommon(weapon, 0);
    return weapon;
}

void DialogTradeWeaponAxe()
{
    if (!tradeCommonChecking(OTHER, 50, OBJ_AMULETOF_MANIPULATION))
        return;

    SafetyPickup(OTHER, WeaponPortableAxe(OTHER));
}

void DialogExplainWeaponAxe()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_3);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "도끼 구입");
}

void ExplainWeaponNoName4()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 청동검. 기본 데미지+7. 공속+9. 치명타+12. 강화 데미지+%d");
}

int WeaponNoName4(int user)
{
    int weapon=CreateObjectById(OBJ_AMULETOF_NATURE, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainWeaponNoName4);
    itemCommon(weapon, 0);
    return weapon;
}

void DialogTradeWeapon4()
{
    if (!tradeCommonChecking(OTHER, 48, OBJ_AMULETOF_NATURE))
        return;

    SafetyPickup(OTHER, WeaponNoName4(OTHER));
}

void DialogExplainWeapon4()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_4);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "청동검 구입");
}

void ExplainWeaponPongHammer()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 뿅망치. 기본 데미지+22. 공속-10. 넓은 공격 범위. 강화 데미지+%d.");
}

int WeaponPongHammer(int user)
{
    int weapon=CreateObjectById(OBJ_AMULET_OF_TELEPORTATION, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainWeaponPongHammer);
    itemCommon(weapon, 0);
    return weapon;
}

void DialogTradeWeaponPongHammer()
{
    if (!tradeCommonChecking(OTHER, 75, OBJ_AMULET_OF_TELEPORTATION))
        return;

    SafetyPickup(OTHER, WeaponPongHammer(OTHER));
}

void DialogExplainWeaponPongHammer()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_5);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "뿅망치 구입");
}

void ExplainWeaponKingSword()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 황제의 검. 기본 데미지+46. 치명타+33. 강화 데미지+%d.");
}

int WeaponKingSword(int user)
{
    int weapon=CreateObjectById(OBJ_BOOK_OF_OBLIVION, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainWeaponKingSword);
    itemCommon(weapon, 7);
    return weapon;
}

void DialogTradeWeaponKingSword()
{
    if (!tradeCommonChecking(OTHER, 523, OBJ_BOOK_OF_OBLIVION))
        return;

    SafetyPickup(OTHER, WeaponKingSword(OTHER));
}

void DialogExplainWeaponKingSword()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_7);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "황제의 검 구입");
}

void ExplainWeaponSharkSword()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 무녀도. 기본 데미지+31. 공속+13. 치명타+14. 강화 데미지+%d.");
}

int WeaponSharkSword(int user)
{
    int weapon=CreateObjectById(OBJ_BOOTS_OF_SPEED, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainWeaponSharkSword);
    itemCommon(weapon, 8);
    return weapon;
}

void DialogTradeWeaponSharkSword()
{
    if (!tradeCommonChecking(OTHER, 517, OBJ_BOOTS_OF_SPEED))
        return;

    SafetyPickup(OTHER, WeaponSharkSword(OTHER));
}

void DialogExplainWeaponSharkSword()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_8);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "무녀도 구입");
}

void ExplainWeaponHellfireAxe()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 헬파이어 엑스. 기본 데미지+76. 치명타+42. 강화 데미지+%d.");
}

int WeaponHellfireAxe(int user)
{
    int weapon=CreateObjectById(OBJ_BRACELETOF_ACCURACY, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainWeaponHellfireAxe);
    itemCommon(weapon, 4);
    return weapon;
}

void DialogTradeWeaponHellfireAxe()
{
    if (!tradeCommonChecking(OTHER, 1200, OBJ_BRACELETOF_ACCURACY))
        return;

    SafetyPickup(OTHER, WeaponHellfireAxe(OTHER));
}

void DialogExplainWeaponHellfireAxe()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_9);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "헬파이어 엑스 구입");
}

void ExplainWeaponMoonlight()
{
    commonWeaponClicked(OTHER, GetTrigger(), "이름- 헬파이어 엑스. 기본 데미지+76. 치명타+42. 강화 데미지+%d.");
}

int WeaponMoonlightSword(int user)
{
    int weapon=CreateObjectById(OBJ_BRACELETOF_CHANNELING, GetObjectX(user), GetObjectY(user));

    SetUnitCallbackOnUseItem(weapon, ExplainWeaponMoonlight);
    itemCommon(weapon, 8);
    return weapon;
}

void DialogTradeWeaponMoonlight()
{
    if (!tradeCommonChecking(OTHER, 536, OBJ_BRACELETOF_CHANNELING)) //BraceletofChanneling
        return;

    SafetyPickup(OTHER, WeaponMoonlightSword(OTHER));
}

void DialogExplainWeaponMoonlight()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_WEAPON_10);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "문라이트 서드구입");
}

void DialogTradeUpgradeWeapon()
{
    if (GetAnswer(SELF)^1)
        return;

    int pIndex=GetPlayerIndex(OTHER);

    if (pIndex<0)
        return;

    int weapon=GetPlayerEquipment(pIndex);

    if (!HasItem(OTHER, weapon))
    {
        UniPrint(OTHER, "장착중인 무기가 없습니다");
        return;
    }
    
    int powerLv = GetUnit1C(weapon);
    int pay = 10+(powerLv*10);
    char buff[128];

    if (GetGold(OTHER)<pay)
    {
        UniPrint(OTHER, "골드가 부족합니다");
        return;
    }
    ChangeGold(OTHER, -pay);
    UpdatePlayerGold(OTHER);
    if (!TryItemUpgrade(pIndex, weapon))
    {
        if (powerLv==0)
        {
            OnItemDropped(pIndex, weapon);
            Delete(weapon);
            UniPrint(OTHER, "강화 실패입니다- 장비가 파괴되었습니다!");
        }
        --powerLv;
        NoxSprintfString(buff, "강화 실패입니다. 강화레밸이 %d으로 감소되었습니다", &powerLv, 1);
        UniPrint(OTHER, ReadStringAddressEx(buff));
        return;
    }
    ++powerLv;
    NoxSprintfString(buff, "강화 성공..! 강화레밸이 %d으로 증가되었습니다", &powerLv, 1);
    GreenExplosion(GetObjectX(GetPlayerAvata(pIndex)), GetObjectY(GetPlayerAvata(pIndex)) );
    UniPrint(OTHER, ReadStringAddressEx(buff));
}

void DialogExplainUpgradeWeapon()
{
    int pIndex=GetPlayerIndex(OTHER);

    if (pIndex<0)
        return;

    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_UPGRADE_WEAPON);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "장착된무기 강화");

    int weapon=GetPlayerEquipment(pIndex);

    if (!HasItem(OTHER, weapon))
        return;
    
    char buff[192];
    int powerLv = GetUnit1C(weapon);
    int args[]={powerLv, 10 + (powerLv*10), };

    NoxSprintfString(buff, "장착중인 무기의 강화레밸은 %d 입니다. 다음레밸로 강화하려면, %d 골드가 필요합니다", args,sizeof(args));
    UniPrint(OTHER, ReadStringAddressEx(buff));
}

#define UP_RATE_PAY 50
void DialogTradeIncreaseUpgradeRate()
{
    if (GetAnswer(SELF)^1)
        return;
    if (GetGold(OTHER)<UP_RATE_PAY)
    {
        UniPrint(OTHER, "골드가 부족합니다");
        return;
    }
    int lv = TryIncreaseUpgradeRate(OTHER);
    if (lv)
    {
        ChangeGold(OTHER, -UP_RATE_PAY);
        UpdatePlayerGold(OTHER);
        char buff[128];

        NoxSprintfString(buff, "강화 성공확률 상승! 현재 강화 성공률은 %d 입니다", &lv,1);
        UniPrint(OTHER, ReadStringAddressEx(buff));
        return;
    }
    UniPrint(OTHER, "업그레이드 실패- 최대 레밸이거나 내부 오류입니다");
}

void DialogExplainIncreaseUpgradeRate()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_UPGRADE);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "강화성공확률 구입");
}

void DialogTradeSellWeapon()
{
    if (GetAnswer(SELF)^1)
        return;

    int pIndex=GetPlayerIndex(OTHER);
    int inv=GetPlayerEquipment(pIndex);

    if (!ToInt(GetObjectX(inv)))
    {
        UniPrint(OTHER, "처리실패! 장착중인 무기가 없습니다");
        return;
    }
    int price = showSellWeaponWorth(OTHER, inv);
    char message[128];

    OnItemDropped(pIndex, inv);
    Delete(inv);
    if (price)
    {
        ChangeGold(OTHER, price);
        UpdatePlayerGold(OTHER);
    }
    NoxSprintfString(message, "그 무기를 팔고, %d 골드를 돌려받았습니다", &price, 1);
    UniPrint(OTHER, ReadStringAddressEx(message));
}

void DialogExplainSellWeapon()
{
    showSellWeaponWorth(OTHER, GetPlayerEquipment(GetPlayerIndex(OTHER)));
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_SELL_WEAPON);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "장착중인 무기팔기");
}

void OnNpcCollide() //마우스와 충돌하면 대화창 열리면서 yes no 선택.
{
    if (MaxHealth(OTHER))
    {
        int owner=GetOwner(OTHER);

        if (!CurrentHealth(owner))
            return;

        if (!IsPlayerUnit(owner))
            return;

        StartDialog(SELF, owner);
    }
}

void PlacingWeaponNPC(short location, short itemTy, int descFn, int tradeFn)
{
    int npc=DummyUnitCreateById(OBJ_BOMBER_BLUE, LocationX(location),LocationY(location));

    UnitNoCollide(CreateObjectById(itemTy, GetObjectX(npc), GetObjectY(npc)));
    SetUnitFlags(npc,GetUnitFlags(npc)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(npc,9,OnNpcCollide);
    StoryPic(npc, "MaidenPic5");
    SetDialog(npc,"YESNO",descFn,tradeFn);
}

void WeaponEffect1(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=5;
}

void WeaponShortCutterEffect(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=8;

    int *pDelay;
    if (HashGet(hash,EFFECT_KEY_ATTACK_DELAY,&pDelay,TRUE))
        pDelay[0]-=4;

    int *pDropRate;
    if (HashGet(hash,EFFECT_KEY_DROP_GOLD_RATE,&pDropRate,TRUE))
        pDropRate[0]+=10;
}

void WeaponAxeEffect(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=14;

    int *pDelay;
    if (HashGet(hash,EFFECT_KEY_ATTACK_DELAY,&pDelay,TRUE))
        pDelay[0]+=3;

    int *pCritical;

    if (HashGet(hash, EFFECT_KEY_CRITICAL_RATE, &pCritical, TRUE))
        pCritical[0]+=12;
}

void WeaponEffect4(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=7;

    int *pDelay;
    if (HashGet(hash,EFFECT_KEY_ATTACK_DELAY,&pDelay,TRUE))
        pDelay[0]-=9;

    int *pCritical;

    if (HashGet(hash, EFFECT_KEY_CRITICAL_RATE, &pCritical, TRUE))
        pCritical[0]+=12;
}

void WeaponPongHammerEffect(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=22;

    int *pDelay;
    if (HashGet(hash,EFFECT_KEY_ATTACK_DELAY,&pDelay,TRUE))
        pDelay[0]+=10;

    int *pMultiple;
    if (HashGet(hash,EFFECT_KEY_MULTIPLE,&pMultiple,TRUE))
        pMultiple[0]=TRUE;
}

void WeaponKingSwordEffect(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=46;

    int *pCritical;

    if (HashGet(hash, EFFECT_KEY_CRITICAL_RATE, &pCritical, TRUE))
        pCritical[0]+=33;
}

void WeaponSharkSwordEffect(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=31;

    int *pDelay;
    if (HashGet(hash,EFFECT_KEY_ATTACK_DELAY,&pDelay,TRUE))
        pDelay[0]-=13;

    int *pCritical;

    if (HashGet(hash, EFFECT_KEY_CRITICAL_RATE, &pCritical, TRUE))
        pCritical[0]+=14;
}

void WeaponHellfireEffect(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=76;

    int *pCritical;

    if (HashGet(hash, EFFECT_KEY_CRITICAL_RATE, &pCritical, TRUE))
        pCritical[0]+=42;
}

void WeaponMoonlightSwordEffect(int weapon, int hash)
{
    int *pDamage;

    if (HashGet(hash, EFFECT_KEY_DAMAGE, &pDamage, TRUE))
        pDamage[0]+=50;
}

void invokeDoWeaponEffect(int fn, int weapon, int hash)
{
    Bind(fn, &fn+4);
}

void WeaponUtilLoadEffect(int weapon, int hash)
{
    int key=GetUnitThingID(weapon), fn=0;

    if (!key)
        return;

    if (HashGet(GetWeaponEffectHash(), key, &fn, FALSE))
    {
        if (!fn)
            return;

        invokeDoWeaponEffect(fn, weapon, hash);
    }
}

void InitWeaponHash(int instance, int sellHash)
{
    HashPushback(instance, OBJ_AMULET_OF_CLARITY, WeaponEffect1);
    HashPushback(sellHash, OBJ_AMULET_OF_CLARITY, 5);
    HashPushback(instance, OBJ_AMULETOF_COMBAT, WeaponShortCutterEffect);
    HashPushback(sellHash, OBJ_AMULETOF_COMBAT, 10);
    HashPushback(instance, OBJ_AMULETOF_MANIPULATION, WeaponAxeEffect);
    HashPushback(instance, OBJ_AMULETOF_NATURE, WeaponEffect4);
    HashPushback(instance, OBJ_AMULET_OF_TELEPORTATION, WeaponPongHammerEffect);
    HashPushback(instance, OBJ_BOOK_OF_OBLIVION, WeaponKingSwordEffect);
    HashPushback(sellHash, OBJ_BOOK_OF_OBLIVION, 100);
    HashPushback(instance, OBJ_BOOTS_OF_SPEED, WeaponSharkSwordEffect);
    HashPushback(sellHash, OBJ_BOOTS_OF_SPEED, 110);
    HashPushback(instance, OBJ_BRACELETOF_ACCURACY, WeaponHellfireEffect);
    HashPushback(sellHash, OBJ_BRACELETOF_ACCURACY, 110);
    HashPushback(instance, OBJ_BRACELETOF_CHANNELING, WeaponMoonlightSwordEffect);
    HashPushback(sellHash, OBJ_BRACELETOF_CHANNELING, 30);
}

void InitMapWeapons()
{
    PlacingWeaponNPC(76, OBJ_AMULET_OF_CLARITY, DialogExplainWeaponSword,DialogTradeWeaponSword);
    PlacingWeaponNPC(82, OBJ_AMULET_OF_CLARITY, DialogExplainWeaponSwordLv10,DialogTradeWeaponSwordLv10);
    PlacingWeaponNPC(80, OBJ_AMULETOF_COMBAT, DialogExplainWeaponShortSword,DialogTradeWeaponShortSword);
    PlacingWeaponNPC(78, OBJ_AMULETOF_MANIPULATION, DialogExplainWeaponAxe,DialogTradeWeaponAxe);
    PlacingWeaponNPC(81, OBJ_AMULETOF_NATURE, DialogExplainWeapon4,DialogTradeWeapon4);
    PlacingWeaponNPC(79, OBJ_AMULET_OF_TELEPORTATION, DialogExplainWeaponPongHammer,DialogTradeWeaponPongHammer);
    PlacingWeaponNPC(112, OBJ_BOOK_OF_OBLIVION, DialogExplainWeaponKingSword,DialogTradeWeaponKingSword);
    PlacingWeaponNPC(113, OBJ_BOOTS_OF_SPEED, DialogExplainWeaponSharkSword,DialogTradeWeaponSharkSword);
    PlacingWeaponNPC(87, OBJ_FOIL_CANDLE_UNLIT, DialogExplainUpgradeWeapon,DialogTradeUpgradeWeapon);
    PlacingWeaponNPC(88, OBJ_BLACK_BOOK_1, DialogExplainIncreaseUpgradeRate,DialogTradeIncreaseUpgradeRate);
    PlacingWeaponNPC(142, OBJ_BRACELETOF_ACCURACY, DialogExplainWeaponHellfireAxe,DialogTradeWeaponHellfireAxe);
    PlacingWeaponNPC(143, OBJ_BRACELETOF_CHANNELING, DialogExplainWeaponMoonlight,DialogTradeWeaponMoonlight);

    PlacingWeaponNPC(101, OBJ_BARREL_STEEL_1, DialogExplainCallBoss,DialogTradeCallBoss);
    PlacingWeaponNPC(75, OBJ_BARREL_STEEL_2, DialogExplainSellWeapon,DialogTradeSellWeapon);
}

