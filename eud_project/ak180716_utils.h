
#include "ak180716_gvar.h"
#include "libs/queueTimer.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/buff.h"
#include "libs/playerinfo.h"
#include "LIBS/bind.h"
#include "libs/waypoint.h"

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}

int IsNumber(int ch)
{
    return (ch >= '0' && ch <= '9');
}
int CheckVaildNumber(int num)
{
    return (IsNumber(num & 0xff) && IsNumber((num>>8) & 0xff) && IsNumber((num>>16) & 0xff) && IsNumber((num>>24) & 0xff));
}

int StringByteToInt(int byte)
{
    return ((byte>>24) & 0xff) - '0' + ((((byte>>16) & 0xff) - '0') * 10) + ((((byte>>8) & 0xff) - '0') * 100) + (((byte & 0xff) - '0') * 1000);
}

int UnitScrNameToNumber(int unit)
{
    int ptr = UnitToPtr(unit), raw, num = -1;

    if (ptr)
    {
        ptr = GetMemory(ptr);
        if (ptr)
        {
            raw = GetMemory(ptr);
            if (raw)
            {
                if (CheckVaildNumber(raw))
                    num = StringByteToInt(raw);
            }
        }
    }
    return num;
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

#define TRANSPORT_SUB 0
#define TRANSPORT_COUNTER 1
#define TRANSPORT_CUSTOMER 2
#define TRANSPORT_EFFECT 3
#define TRANSPORT_MAX 4

void onTransportProcedure(int *pTransp)
{
    int owner = pTransp[TRANSPORT_CUSTOMER];

    if (CurrentHealth(owner))
    {
        int count = pTransp[TRANSPORT_COUNTER];

        if (count)
        {
            int eff=pTransp[TRANSPORT_EFFECT];

            if (DistanceUnitToUnit(owner, eff) < 23.0)
            {
                pTransp[TRANSPORT_COUNTER]-=1;
                PushTimerQueue(1, pTransp, onTransportProcedure);
                return;
            }
        }
        else
        {
            Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            int destination = pTransp[TRANSPORT_SUB]+1;

            MoveObject(owner, GetObjectX(destination), GetObjectY(destination));
            PlaySoundAround(owner, SOUND_BlindOff);
            Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
        }
        EnchantOff(owner, "ENCHANT_BURNING");
    }
    Delete(pTransp[TRANSPORT_EFFECT]);
    FreeSmartMemEx(pTransp);
}

void onTransportCollide()
{
    if (CurrentHealth(OTHER))
    {
        if (!IsPlayerUnit(OTHER)) return;        
        if (!UnitCheckEnchant(OTHER, GetLShift(ENCHANT_BURNING)))
        {
            int *pTransp;            
            AllocSmartMemEx(TRANSPORT_MAX*4, &pTransp);
            pTransp[TRANSPORT_SUB]=GetTrigger();
            pTransp[TRANSPORT_COUNTER] = 48;
            pTransp[TRANSPORT_CUSTOMER]=GetCaller();
            pTransp[TRANSPORT_EFFECT]=CreateObjectAt("VortexSource", GetObjectX(SELF), GetObjectY(SELF));
            PushTimerQueue(1, pTransp, onTransportProcedure);
            GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
            Enchant(OTHER, "ENCHANT_BURNING", 4.0);
        }
    }
}

#undef TRANSPORT_SUB
#undef TRANSPORT_COUNTER
#undef TRANSPORT_CUSTOMER
#undef TRANSPORT_EFFECT
#undef TRANSPORT_MAX

int DispositionTransport(float x,float y, float dx,float dy)
{
    int tp=CreateObjectById(OBJ_WEIRDLING_BEAST, x,y );

    CreateObjectById(OBJ_MAGIC_ENERGY, dx,dy);
    UnitNoCollide(CreateObjectById(OBJ_CORPSE_SKULL_SW, GetObjectX(tp), GetObjectY(tp)));
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH, x,y);
    Frozen(tp+2, TRUE);
    SetCallback(tp, 9, onTransportCollide);
    Damage(tp, 0, MaxHealth(tp)+1, -1);
    return tp;
}

void onRectangleCollide(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Bind(ToInt(GetObjectZ(GetTrigger()+1)), 0);
            int r= CreateObjectById(OBJ_CORPSE_SKULL_N,GetObjectX(SELF),GetObjectY(SELF));
            UnitNoCollide(r);
            Frozen(r,TRUE);
            Delete(GetTrigger()+1);
            Delete(SELF);
        }
    }
}

int DispositionRectangle(float x, float y, short fn){
    int rect=CreateObjectById(OBJ_CORPSE_SKULL_E,x,y);

    Frozen(rect,TRUE);
    Raise( CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH,x,y), fn );
    SetUnitCallbackOnCollide(rect,onRectangleCollide);
    return rect;
}

void SendPacketAll(char *packet, int packetLength)
{
    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x90, 0x53, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x1C, 0x31, 0xC0, 0xC3,
    };
    int args[]={
        packet,0xff,packetLength,
    }; //0xff or 0x9f
    invokeRawCode(code, args);
}

void SendPacketSpecifyOne(int user, char *packet, int packetLength)
{
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0)
        return;
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
    };
    int args[]={
        packet,pIndex,packetLength,
    };
    invokeRawCode(code, args);
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}

void CustomMeleeAttackCode(char *bin, int fn)
{
    char code[]={
        0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
        0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
        0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
        0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
    };
    
    int *pStrike = &bin[0xEC];

    pStrike[0] = code;
    int *hack = &bin[0x94];

    hack[15]=fn;
}

void WallCoorToUnitCoor(int wall, float *destXy)
{
    int x=wall>>0x10, y=wall&0xff;

    destXy[0]=IntToFloat((x*23) + 11);
    destXy[1]=IntToFloat((y*23)+11);
}

void onInvisibleTeleportTriggered(){
    if (CurrentHealth(OTHER)){
        int dest=GetTrigger()+1;
        float point[]={GetObjectX(dest),GetObjectY(dest)};
        Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
        MoveObject(OTHER, point[0],point[1]);
    }
}

void placingInvisibleTeleport(short srcLocationId, short destLocationId){
    int telp=DummyUnitCreateById(OBJ_BOMBER_BLUE,LocationX(srcLocationId),LocationY(srcLocationId));
    
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,LocationX(destLocationId),LocationY(destLocationId));
    SetCallback(telp,9,onInvisibleTeleportTriggered);
}

