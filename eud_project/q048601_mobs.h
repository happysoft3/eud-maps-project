
#include "libs/define.h"
#include "q048601_common.h"
#include "libs/format.h"
#include "libs/printutil.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/thingdbedit.h"
#include "libs/queueTimer.h"
#include "libs/monsteraction.h"
#include "libs/meleeattack.h"
#include "libs/buff.h"
#include "libs/hash.h"
#include "libs/bind.h"
#include "libs/waypoint.h"
#include "libs/objectIDdefines.h"

////Dungeon Settings////

//Area-1
#define AREA11_HP_WISP 85
#define AREA12_HP_URCHIN 80
#define AREA13_HP_GOON 160
#define AREA14_HP_FROG 130
#define AREA15_HP_WOUNDED_APPRENTICE 185

#define AREA11_MOB_COUNT 42
#define AREA12_MOB_COUNT 36
#define AREA13_MOB_COUNT 42
#define AREA14_MOB_COUNT 32
#define AREA15_MOB_COUNT 38

#define AREA13_DAMAGE_GOON 10
#define AREA14_DAMAGE_FROG 6
#define AREA15_DAMAGE_WOUNDED_APPR 8

//Area-2
#define AREA21_HP_SCORPION 210
#define AREA22_HP_GHOST 185
#define AREA23_HP_JANDOR 225

#define AREA21_MOB_COUNT 36
#define AREA22_MOB_COUNT 32
#define AREA23_MOB_COUNT 32

#define AREA21_DAMAGE_SCORPION 30
#define AREA21_SPLASH_DAMAGE_SCORPION 3
#define AREA22_DAMAGE_GHOST 13
#define AREA23_DAMAGE_JANDOR 21
#define AREA23_DAMAGE_BONUS_JANDOR 2

//Area-3
#define AREA31_HP_WHITE_WIZ 260
#define AREA32_HP_OGRE_AXE 248
#define AREA33_HP_OBSTACLE 285
#define AREA34_HP_BLACKSPIDER 260
#define AREA35_HP_TALL_HORRENDOUS 225

#define AREA31_MOB_COUNT 28
#define AREA32_MOB_COUNT 28
#define AREA33_MOB_COUNT 36
#define AREA34_MOB_COUNT 36
#define AREA35_MOB_COUNT 24

#define AREA31_DAMAGE_WHITE_WIZ 15
#define AREA32_DAMAGE_OGREAXE 22
#define AREA32_DAMAGE_SPLASH_OGREAXE 8
#define AREA33_DAMAGE_OBSTACLE 21
#define AREA34_DAMAGE_BLACK_SPIDER 25
#define AREA34_DAMAGE_MISSILE_BLACK_SPIDER 16
#define AREA35_DAMAGE_HORRENDOUS 17
#define AREA35_DAMAGE_BONUS_HORRENDOUS 2

//Area-4
#define AREA41_HP_HECUBAH 325
#define AREA42_HP_LICH 325
#define AREA43_HP_SHAMAN 295
#define AREA44_HP_BLACK_WOLF 275

#define AREA41_MOB_COUNT 32
#define AREA42_MOB_COUNT 36
#define AREA43_MOB_COUNT 32
#define AREA44_MOB_COUNT 32

#define AREA41_DAMAGE_HECUBAH 50
#define AREA42_DAMAGE_LICH 37
#define AREA43_DAMAGE_SHAMAN 24

//Area-6
#define AREA61_HP_OGRE_LORD 420
#define AREA62_HP_BEHOLDER 435
#define AREA63_HP_HIVE 395
#define AREA64_HP_MIMIC 450
#define AREA65_HP_STORMS 411
#define AREA66_HP_CARN_PLANT 433

#define AREA61_MOB_COUNT 32
#define AREA62_MOB_COUNT 24
#define AREA63_MOB_COUNT 24
#define AREA64_MOB_COUNT 36
#define AREA65_MOB_COUNT 24
#define AREA66_MOB_COUNT 36

#define BEHOLDER_LIGHTNING_DURATION 32

#define AREA61_DAMAGE_OGRE_LORD 45
#define AREA61_DAMAGE_MISSILE_OGRE_LORD 72
#define AREA63_DAMAGE_HIVE 66
#define AREA65_DAMAGE_MISSILE_STORMS 48
#define AREA66_DAMAGE_CARNI_PLANT 100

//Area-5
#define AREA51_HP_KILLERIAN 475
#define AREA52_HP_RED_WIZ 430
#define AREA53_HP_STONE_GIANT 555
#define AREA54_HP_WOUNDED_WAR 496
#define AREA55_HP_BULDAK 480

#define AREA51_MOB_COUNT 32
#define AREA52_MOB_COUNT 32
#define AREA53_MOB_COUNT 28
#define AREA54_MOB_COUNT 24
#define AREA55_MOB_COUNT 24

#define AREA51_DAMAGE_KILLERIAN 85
#define AREA51_DAMAGE2_KILLERIAN 10
#define AREA52_DAMAGE_MISSILE_RED_WIZ 75
#define AREA53_DAMAGE_STONE_GIANT 100
#define AREA54_DAMAGE_WOUNDED_WAR 98
#define AREA55_DAMAGE_BULDAK 44

int MasterUnit() //virtual
{
    return 0;
}

int MonsterSightHash() //virtual
{
    return 0;
}

void onMonsterDeath() //virtual
{ }

void onMonsterHurt() //virtual
{ }

void deferredEnableObject(int obj)
{
    if (ToInt(GetObjectX(obj)))
        ObjectOn(obj);
}

void deferredObjectOn(int delay, int obj)
{
    PushTimerQueue(delay, obj, deferredEnableObject);
    ObjectOff(obj);
}

void ResetUnitSight(int unit)
{
    int arg;

    if (HashGet(MonsterSightHash(), unit, &arg, TRUE))
    {
        Enchant(unit, "ENCHANT_BLINDED", 0.06);
        AggressionLevel(unit, 1.0);
    }
}

void CheckResetSight(int unit, int arg, int delay)
{
    if (!HashGet(MonsterSightHash(), unit, NULLPTR, TRUE))
    {
        HashPushback(MonsterSightHash(), unit, arg);
        PushTimerQueue(delay, unit, ResetUnitSight);
    }
}

#define DOOR_LOCKSTYLE_SILVER 0x100
#define DOOR_LOCKSTYLE_GOLD 0x200
#define DOOR_LOCKSTYLE_RUBY 0x300
#define DOOR_LOCKSTYLE_SAPPHIRE 0x400
#define DOOR_LOCKSTYLE_MECA 0x500

int GetLockStyle(int door)
{
    int ptr=UnitToPtr(door);
    return GetMemory(GetMemory(ptr+0x2ec));
}

void UpdateLockDoor(int door)
{
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x50, 0xB8, 0x90, 0x83, 0x4E, 0x00, 0xFF, 0xD0, 0xA1, 0x04, 0xEA, 0x84, 0x00, 0x50, 0xB8, 0xE0, 0x71, 0x4D, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3
    };
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=code;
    Unused1f(UnitToPtr(door));
    pBuiltins[31]=pOld;
}

void LockDoorEx(int door, int lockStyle, short soundId)
{
    char *pcode;

    if (!pcode)
    {
        char scode[]={
            0x56, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0xF0, 0x6A, 0x00, 0x6A, 0x00, 0xFF, 0x36, 
            0xFF, 0x76, 0x04, 0xB8, 0x60, 0x19, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0x5E, 0xC3
        };
        pcode=scode;
    }
    UpdateLockDoor(door);
    int *ptr = UnitToPtr(door);
    int args[]={ptr, soundId};
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    short *ec = ptr[187];
    ec[0]=lockStyle;
    pBuiltins[31]=pcode;
    Unused1f(args);
    pBuiltins[31]=pOld;
}

void ModifyDoorSpriteDrawFn()
{
    char *pcode;

    if (!pcode)
    {
        char scode[] = {
            0x83, 0xEC, 0x08, 0x31, 0xC0, 0x53, 0x55, 0x56, 0x8B, 0x74, 0x24, 0x1C, 
            0x8B, 0x6C, 0x24, 0x18, 0x8B, 0x8E, 0x30, 0x01, 0x00, 0x00, 0x8A, 0x86, 
            0x2B, 0x01, 0x00, 0x00, 0x8B, 0x51, 0x04, 0x8B, 0x04, 0x82, 0x50, 0x56, 
            0x55, 0xB8, 0x70, 0x47, 0x4C, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xBB, 
            0x01, 0x00, 0x00, 0x00, 0x68, 0x24, 0xCA, 0x4B, 0x00, 0xC3};
        pcode=&scode;
    }
    int *pSprite = GetMemory(0x6D3DC0);

    while (pSprite)
    {
        if (pSprite[75]==0x4bc9e0)
            pSprite[75] = pcode;
        pSprite=pSprite[92];
    }
}

int findGateObject(int part, int sub, char prefix)
{
    char buff[32];
    int args[]={part, sub, prefix};

    NoxSprintfString(buff, "gateSec%d%d%c", args, sizeof(args));
    return Object(ReadStringAddressEx(buff));
}

int invokeSpawnMonster(int fn, float x, float y)
{
    return Bind(fn, &fn + 4);
}

void commonMonsterProperty(int mon)
{
    SetOwner(MasterUnit(), mon);
    RetreatLevel(mon, 0.0);
    ResumeLevel(mon, 0.0);
    AggressionLevel(mon, 1.0);
    SetCallback(mon, 5, onMonsterDeath);
    SetCallback(mon, 7, onMonsterHurt);
}

#define SPAWN_MON_MARK_PTR 0
#define SPAWN_MON_FN 1
#define SPAWN_MON_COUNT 2
#define SPAWN_MON_KEYINDEX 3
#define SPAWN_MON_MAX 4

void spawnDungeonMonsters(int *p)
{
    float point[2];

    if (--p[SPAWN_MON_COUNT]>=0)
    {
        PushTimerQueue(1, p, spawnDungeonMonsters);
        short *pMarks=p[SPAWN_MON_MARK_PTR];
        ComputeAreaRhombus(point, LocationX(pMarks[0]), LocationX(pMarks[1]), LocationY(pMarks[2]), LocationY(pMarks[1]));
        int mon = invokeSpawnMonster(p[SPAWN_MON_FN], point[0], point[1]);

        commonMonsterProperty(mon);
        if (p[SPAWN_MON_KEYINDEX]==p[SPAWN_MON_COUNT])
            HashPushback(GenericHash(), mon, SPECIFY_KEY_ID);
        return;
    }
    FreeSmartMemEx(p);
}

void startSpawnDungeonMonsters(short *marks, int fn, int count)
{
    int a;

    AllocSmartMemEx(SPAWN_MON_MAX*4, &a);
    int *p=a;

    PushTimerQueue(1, p, spawnDungeonMonsters);
    p[SPAWN_MON_COUNT]=count;
    p[SPAWN_MON_FN]=fn;
    p[SPAWN_MON_MARK_PTR]=marks;
    p[SPAWN_MON_KEYINDEX]=Random(0, count-1);
}


////end Dungeon Settings///

int commonSectionCheck()
{
    int key=findOutSpecificInventory(OTHER, SPECIFY_KEY_ID);
    char buff[128];

    if (!key)
    {
        deferredObjectOn(10, GetTrigger());
        int *sprite = ThingDbEditGetArray(SPECIFY_KEY_ID);

        NoxSprintfString(buff, "이 문을 열기 위해서는 %s 이 필요합니다", ArrayRefN(sprite, 1), 1);
        UniPrint(OTHER, ReadStringAddressEx(buff));
        PlaySoundAround(OTHER, SOUND_NoCanDo);
        return FALSE;
    }
    Delete(key);
    ObjectOff(SELF);
    return TRUE;
}

int wispBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1886611831; arr[24] = 1065353216; arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; 
		arr[40] = 116; arr[53] = 1133903872; arr[55] = 12; arr[56] = 20;
		link = &arr;
	}
	return link;
}

int sectionMonster11(float x, float y)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, x,y);

    SetUnitMaxHealth(unit, AREA11_HP_WISP);
    UnitLinkBinScript(unit, wispBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    return unit;
}

void OpenSection11()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(1,1,0);
    if (!door1)
        return;
    int door2=findGateObject(1,1,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={17,18,19};

    startSpawnDungeonMonsters(marks, sectionMonster11, AREA11_MOB_COUNT);
}

int UrchinBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 28265; arr[16] = 11000; arr[17] = 80; arr[19] = 75; 
		arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[37] = 1769498960; arr[38] = 101; 
		arr[53] = 1128792064; arr[54] = 4; arr[55] = 20; arr[56] = 30; arr[60] = 1339; 
		arr[61] = 46904832; 
	pArr = &arr;
	return pArr;
}

void UrchinSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074790400;		ptr[137] = 1074790400;
	int *hpTable = ptr[139];
	hpTable[0] = AREA12_HP_URCHIN;	hpTable[1] = AREA12_HP_URCHIN;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = UrchinBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

int sectionMonster12(float x, float y)
{
    int unit = CreateObjectById(OBJ_URCHIN, x,y);

    UrchinSubProcess(unit);
    return unit;
}

void OpenSection12()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(1,2,0);
    if (!door1)
        return;
    int door2=findGateObject(1,2,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={21,22,23};

    startSpawnDungeonMonsters(marks, sectionMonster12, AREA12_MOB_COUNT);
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 160; arr[19] = 80; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = AREA13_DAMAGE_GOON; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 1; arr[35] = 1; 
		arr[36] = 10; arr[59] = 5543680; 
	pArr = &arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = AREA13_HP_GOON;	hpTable[1] = AREA13_HP_GOON;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster13(float x, float y)
{
    int unit = CreateObjectById(OBJ_GOON, x,y);

    GoonSubProcess(unit);
    return unit;
}

void OpenSection13()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(1,3,0);
    if (!door1)
        return;
    int door2=findGateObject(1,3,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={24,29,30};

    startSpawnDungeonMonsters(marks, sectionMonster13, AREA13_MOB_COUNT);
}

int GreenFrogBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[17] = 20; arr[18] = 1; 
		arr[19] = 64; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 0; 
		arr[28] = 1118437376; arr[29] = AREA14_DAMAGE_FROG; arr[31] = 10; arr[32] = 14; arr[33] = 24; 
		arr[59] = 5542784; arr[60] = 1313; arr[61] = 46905856; 
	pArr = &arr;
	return pArr;
}

void GreenFrogSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073070735;		ptr[137] = 1073070735;
	int *hpTable = ptr[139];
	hpTable[0] = AREA14_HP_FROG;	hpTable[1] = AREA14_HP_FROG;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GreenFrogBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

void reventHold(int sub)
{
    int owner = GetOwner(sub);

    if (ToInt(GetObjectX(sub)))
    {
        if (CurrentHealth(owner))
        {
            int target = ToInt(GetObjectZ(sub));

            if (CurrentHealth(target))
            {
                if (DistanceUnitToUnit(owner, target)<150.0)
                {
                    float x=GetObjectX(owner),y=GetObjectY(owner);
                    float vect[]={UnitRatioX(target,owner, 4.0), UnitRatioY(target,owner,4.0)};
                    MoveObject(owner, GetObjectX(target) + vect[0], GetObjectY(target) + vect[1]);
                    Effect("DRAIN_MANA", x,y,GetObjectX(owner)+(vect[0]*3.0),GetObjectY(owner)+(vect[1]*3.0));
                    Damage(target, owner, AREA14_DAMAGE_FROG, DAMAGE_TYPE_CLAW);
                    PlaySoundAround(owner, SOUND_PullCast);
                }
            }
        }
        Delete(sub);
    }
}

// void onFrogSight()
// {
//     if (!CurrentHealth(SELF))
//         return;

// 	if (CurrentHealth(OTHER))
//     {
// 		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
//         {
//             int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(SELF), GetObjectY(SELF));

//             SetOwner(SELF, sub);
//             Raise(sub, GetCaller());
// 			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
// 			PushTimerQueue(7, sub, reventHold);
// 		}
// 	}
//     CheckResetSight(GetTrigger(), 0, 48);
// }

int sectionMonster14(float x, float y)
{
    int unit = CreateObjectById(OBJ_GREEN_FROG, x,y);

    GreenFrogSubProcess(unit);
    // SetCallback(unit, 3, onFrogSight);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection14()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(1,4,0);
    if (!door1)
        return;
    int door2=findGateObject(1,4,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={25,31,28};

    startSpawnDungeonMonsters(marks, sectionMonster14, AREA14_MOB_COUNT);
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 100; arr[19] = 40; arr[23] = 32768; arr[24] = 1067450368; arr[26] = 4; 
		arr[28] = 1112014848; arr[29] = AREA15_DAMAGE_WOUNDED_APPR; arr[30] = 1106247680; arr[31] = 2; arr[32] = 16; 
		arr[33] = 28; arr[59] = 5542784; 
	pArr = &arr;
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1067030937;		ptr[137] = 1067030937;
	int *hpTable = ptr[139];
	hpTable[0] = AREA15_HP_WOUNDED_APPRENTICE;	hpTable[1] = AREA15_HP_WOUNDED_APPRENTICE;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

int sectionMonster15(float x, float y)
{
    int unit = CreateObjectById(OBJ_WOUNDED_APPRENTICE, x,y);

    WoundedApprenticeSubProcess(unit);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection15()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(1,5,0);
    if (!door1)
        return;
    int door2=findGateObject(1,5,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={26,32,27};

    startSpawnDungeonMonsters(marks, sectionMonster15, AREA15_MOB_COUNT);
}

int CreateToxicCloud(float xpos, float ypos, int owner, short time)
{
    int cloud=CreateObjectAt("ToxicCloud", xpos, ypos);
    int *ptr=UnitToPtr(cloud);
    int *pEC=ptr[187];

    pEC[0]=time;
    SetOwner(owner, cloud);
}

int ScorpionBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919902547; arr[1] = 1    8    5    2    7    95                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   248; arr[17] = 110; arr[19] = 70; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1067869798; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1106247680; arr[29] = AREA21_DAMAGE_SCORPION; arr[30] = 1101004800; arr[31] = 3; arr[32] = 9; 
		arr[33] = 17; arr[34] = 25; arr[35] = 2; arr[36] = 4; arr[59] = 5543344; 
		arr[60] = 1373; arr[61] = 46895952; 
	pArr = &arr;
	return pArr;
}

void ScorpionSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = AREA21_HP_SCORPION;	hpTable[1] = AREA21_HP_SCORPION;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = ScorpionBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onScorpionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER))
                Damage(OTHER, SELF, AREA21_SPLASH_DAMAGE_SCORPION, DAMAGE_TYPE_CRUSH);
        }
    }
}

void onScorpionSight()
{
    if (!CurrentHealth(SELF))
        return;

	if (CurrentHealth(OTHER))
    {
        float x=GetObjectX(SELF), y=GetObjectY(SELF);
        CreateToxicCloud(x,y, SELF, 90);
        SplashDamageAtEx(SELF, x,y,65.0,onScorpionSplash);
		PlaySoundAround(SELF, SOUND_PoisonTrapTriggered);
	}
    CheckResetSight(GetTrigger(), 0, 84);
}

int sectionMonster21(float x, float y)
{
    int unit = CreateObjectById(OBJ_SCORPION, x,y);

    ScorpionSubProcess(unit);
    SetCallback(unit, 3, onScorpionSight);
    return unit;
}

void OpenSection21()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(2,1,0);
    if (!door1)
        return;
    int door2=findGateObject(2,1,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={37,38,39};

    startSpawnDungeonMonsters(marks, sectionMonster21, AREA21_MOB_COUNT);
}

int GhostBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936681031; arr[1] = 116; arr[17] = 185; arr[19] = 60; arr[21] = 1065353216; 
		arr[23] = 32769; arr[24] = 1066108191; arr[27] = 1; arr[28] = 1108082688; arr[29] = AREA22_DAMAGE_GHOST; 
		arr[31] = 4; arr[59] = 5542784; arr[60] = 1325; arr[61] = 46900224; 
	pArr = arr;
	return pArr;
}

void GhostSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = AREA22_HP_GHOST;	hpTable[1] = AREA22_HP_GHOST;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = GhostBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onGhostImageLoop(int mon)
{
    if (CurrentHealth(mon))
    {
        PushTimerQueue(1, mon, onGhostImageLoop);
        if (ToInt(DistanceUnitToUnit(mon,mon+1)) )
            MoveObject(mon+1, GetObjectX(mon),GetObjectY(mon));
        return;
    }
    Delete(mon+1);
}

int sectionMonster22(float x, float y)
{
    int unit = CreateObjectById(OBJ_GHOST, x,y);

    GhostSubProcess(unit);
    int image=CreateObjectById(OBJ_SMALL_FIST, GetObjectX(unit),GetObjectY(unit));
    UnitNoCollide(image);
    Frozen(image,TRUE);
    PushTimerQueue(1, unit,onGhostImageLoop);
    return unit;
}

void OpenSection22()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(2,2,0);
    if (!door1)
        return;
    int door2=findGateObject(2,2,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={40,41,42};

    startSpawnDungeonMonsters(marks, sectionMonster22, AREA22_MOB_COUNT);
}

int AirshipCaptainBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 180; 
		arr[19] = 60; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[28] = 1108082688; 
		arr[29] = AREA23_DAMAGE_JANDOR; arr[32] = 20; arr[33] = 28; arr[57] = 5546320; arr[59] = 5542784; 
		arr[60] = 1387; arr[61] = 46915328; 
	pArr = &arr;
	return pArr;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = AREA23_HP_JANDOR;	hpTable[1] = AREA23_HP_JANDOR;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = AirshipCaptainBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

void onCaptainLoop(int mon)
{
    if (CurrentHealth(mon))
    {
        PushTimerQueue(1, mon, onCaptainLoop);
        if (!ToInt( GetObjectZ(mon)) )
            Raise(mon, 220.0);
    }
}

int sectionMonster23(float x, float y)
{
    int unit = CreateObjectById(OBJ_AIRSHIP_CAPTAIN, x,y);

    AirshipCaptainSubProcess(unit);
    PushTimerQueue(1, unit,onCaptainLoop);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection23()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(2,3,0);
    if (!door1)
        return;
    int door2=findGateObject(2,3,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={43,44,45};

    startSpawnDungeonMonsters(marks, sectionMonster23, AREA23_MOB_COUNT);
}

int StrongWizardWhiteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[17] = 200; arr[18] = 55; arr[19] = 60; arr[21] = 1065353216; arr[23] = 34816; 
		arr[24] = 1069547520; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; 
	pArr = &arr;
	return pArr;
}

void StrongWizardWhiteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = AREA31_HP_WHITE_WIZ;	hpTable[1] = AREA31_HP_WHITE_WIZ;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = StrongWizardWhiteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onLaiserCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            int hash=GetUnit1C(SELF);

            if (!hash)
                return;

            if (HashGet(hash, GetCaller(), NULLPTR, FALSE))
                return;

            HashPushback(hash, GetCaller(), TRUE);
            Damage(OTHER, GetOwner(SELF), AREA31_DAMAGE_WHITE_WIZ, DAMAGE_TYPE_ZAP_RAY);
        }
    }
}

void spawnLaiserDamageHelper(int posUnit, int owner, int hash)
{
    if (!CurrentHealth(owner))
        return;

    int dam=DummyUnitCreateById(OBJ_DEMON, GetObjectX(posUnit),GetObjectY(posUnit));

    SetOwner(owner, dam);
    SetUnitFlags(dam, GetUnitFlags(dam)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    DeleteObjectTimer(dam,1);
    SetCallback(dam, 9, onLaiserCollide);
    SetUnit1C(dam, hash);
}

void onLaiserMove(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int hash=GetUnit1C(sub);
        int dur=GetDirection(sub);
        if (dur)
        {
            if (IsVisibleTo(sub,sub+1))
            {
                PushTimerQueue(1, sub, onLaiserMove);
                LookWithAngle(sub,dur-1);
                MoveObjectVector(sub, GetObjectZ(sub), GetObjectZ(sub+1));
                MoveObjectVector(sub+1, GetObjectZ(sub), GetObjectZ(sub+1));
                spawnLaiserDamageHelper(sub, GetOwner(sub), hash);
                Effect("SENTRY_RAY", GetObjectX(sub), GetObjectY(sub),GetObjectX(sub+1),GetObjectY(sub+1));
                return;
            }
        }

        if (hash)
            HashDeleteInstance(hash);
        Delete(sub);
        Delete(sub+1);
    }
}

void shootLaiserBolt(int caster, int target)
{
    float point[]={GetObjectX(caster),GetObjectY(caster)};
    float vect[]={UnitRatioX(target,caster,13.0),UnitRatioY(target,caster,13.0)};
    int unit=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,point[0]+vect[0],point[1]+vect[1]);
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,point[0]-vect[0],point[1]-vect[1]);
    LookWithAngle(unit, 32);
    Raise(unit, vect[0]);
    Raise(unit+1,vect[1]);
    SetOwner(caster, unit);
    int hash;

    HashCreateInstance(&hash);
    SetUnit1C(unit, hash);
    PushTimerQueue(1, unit, onLaiserMove);
}

void onWhiteWizSight()
{
    if (!CurrentHealth(SELF))
        return;

	if (CurrentHealth(OTHER))
    {
		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
        {
			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
            shootLaiserBolt(GetTrigger(), GetCaller());
		}
	}
    CheckResetSight(GetTrigger(), 0, 60);
}

int sectionMonster31(float x, float y)
{
    int unit = CreateObjectById(OBJ_STRONG_WIZARD_WHITE, x,y);

    StrongWizardWhiteSubProcess(unit);
    SetCallback(unit, 3, onWhiteWizSight);
    return unit;
}

void OpenSection31()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(3,1,0);
    if (!door1)
        return;
    int door2=findGateObject(3,1,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={52,51,50};

    startSpawnDungeonMonsters(marks, sectionMonster31, AREA31_MOB_COUNT);
}

int GruntAxeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853190727; arr[1] = 1702379892; arr[17] = 180; arr[19] = 90; arr[23] = 32768; 
		arr[24] = 1068205343; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1106247680; 
		arr[29] = AREA32_DAMAGE_OGREAXE; arr[30] = 1097859072; arr[32] = 25; arr[33] = 35; arr[59] = 5543904; 
		arr[60] = 1336; 
	pArr = &arr;
	return pArr;
}

void GruntAxeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = AREA32_HP_OGRE_AXE;	hpTable[1] = AREA32_HP_OGRE_AXE;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GruntAxeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

void onOgreAxeSight()
{
    if (!CurrentHealth(SELF))
        return;

	if (CurrentHealth(OTHER))
    {
		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 1.8);
	}
    CheckResetSight(GetTrigger(), 0, 60);
}

void onOgreAxeSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER))
                Damage(OTHER, SELF, AREA32_DAMAGE_SPLASH_OGREAXE, DAMAGE_TYPE_CRUSH);
        }
    }
}

void onOgreAxeLoop(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int owner=GetOwner(sub);
        if (CurrentHealth(owner))
        {
            if (UnitCheckEnchant(owner,GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
            {
                if (DistanceUnitToUnit(sub, owner)>13.0)
                {
                    float x=GetObjectX(owner),y=GetObjectY(owner);
                    MoveObject(sub,x,y);
                    SplashDamageAtEx(owner, x,y,81.0, onOgreAxeSplash);
                    Effect("DAMAGE_POOF",x,y,0.0,0.0);
                    Effect("SMOKE_BLAST",x,y,0.0,0.0);
                    Effect("JIGGLE",x,y,10.0,0.0);
                    PlaySoundAround(sub, SOUND_EarthquakeCast);
                }
            }
            PushTimerQueue(3,sub,onOgreAxeLoop);
            return;
        }
        Delete(sub);
    }
}

int sectionMonster32(float x, float y)
{
    int unit = CreateObjectById(OBJ_GRUNT_AXE, x,y);

    GruntAxeSubProcess(unit);
    SetCallback(unit, 3, onOgreAxeSight);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(unit),GetObjectY(unit));

    SetOwner(unit,sub);
    PushTimerQueue(1,sub,onOgreAxeLoop);
    return unit;
}

void OpenSection32()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(3,2,0);
    if (!door1)
        return;
    int door2=findGateObject(3,2,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={55,54,53};

    startSpawnDungeonMonsters(marks, sectionMonster32, AREA32_MOB_COUNT);
}

int TalkingSkullBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 188; arr[18] = 200; 
		arr[19] = 80; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[28] = 1108082688; 
		arr[29] = AREA33_DAMAGE_OBSTACLE; arr[32] = 18; arr[33] = 27; arr[59] = 5542784; arr[60] = 2303; 
		arr[61] = 46917632; 
	pArr = &arr;
	return pArr;
}

void TalkingSkullSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = AREA33_HP_OBSTACLE;	hpTable[1] = AREA33_HP_OBSTACLE;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = TalkingSkullBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

void onTalkingSkullLoop(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int owner=GetOwner(sub);
        if (CurrentHealth(owner))
        {
            PushTimerQueue(1,sub,onTalkingSkullLoop);
            if (ToInt(DistanceUnitToUnit(owner,sub)))
                MoveObject(sub,GetObjectX(owner),GetObjectY(owner));
            return;
        }
        Delete(sub);
    }
}

int sectionMonster33(float x, float y)
{
    int unit = CreateObjectById(OBJ_TALKING_SKULL, x,y);

    TalkingSkullSubProcess(unit);
    short arr[]={
        OBJ_DARK_WOODEN_CHAIR_1, OBJ_DARK_WOODEN_CHAIR_2,OBJ_DARK_WOODEN_CHAIR_3,OBJ_DARK_WOODEN_CHAIR_4,OBJ_DARK_WOODEN_CHAIR_5,OBJ_DARK_WOODEN_CHAIR_6,OBJ_DARK_WOODEN_CHAIR_7,OBJ_DARK_WOODEN_CHAIR_8,
        OBJ_CANDLEABRA_1,OBJ_FAIRY_JAR,OBJ_ORB,OBJ_DUN_MIR_SCALE_TORCH_1, OBJ_DUN_MIR_SCALE_TORCH_2,OBJ_LOTD_CANDLEABRA_2,OBJ_ROUND_TABLE_WITH_FOOD,
    };
    int sub=CreateObjectById(arr[Random(0,sizeof(arr)-1)], GetObjectX(unit),GetObjectY(unit));

    UnitNoCollide(sub);
    SetOwner(unit,sub);
    PushTimerQueue(1, sub,onTalkingSkullLoop);
    return unit;
}

void OpenSection33()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(3,3,0);
    if (!door1)
        return;
    int door2=findGateObject(3,3,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={58,57,56};

    startSpawnDungeonMonsters(marks, sectionMonster33, AREA33_MOB_COUNT);
}

int BlackWidowBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 225; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1106247680; arr[29] = AREA34_DAMAGE_BLACK_SPIDER; arr[31] = 8; arr[32] = 13; arr[33] = 21; 
		arr[34] = 1; arr[35] = 1; arr[36] = 3; arr[37] = 1684631635; arr[38] = 1884516965; 
		arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
	pArr = &arr;
	return pArr;
}

void BlackWidowSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = AREA34_HP_BLACKSPIDER;	hpTable[1] = AREA34_HP_BLACKSPIDER;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = BlackWidowBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster34(float x, float y)
{
    int unit = CreateObjectById(OBJ_BLACK_WIDOW, x,y);

    BlackWidowSubProcess(unit);
    return unit;
}

void OpenSection34()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(3,4,0);
    if (!door1)
        return;
    int door2=findGateObject(3,4,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={59,60,61};

    startSpawnDungeonMonsters(marks, sectionMonster34, AREA34_MOB_COUNT);
}

#define HORRENDOUS_SPECIAL_UNIT 0
#define HORRENDOUS_SPECIAL_ARRAY 1
#define HORRENDOUS_SPECIAL_COUNT 2
#define HORRENDOUS_SPECIAL_MAX 3

#define MAX_HORRENDOUS_MIRRORCOUNT 4

void procHorrendousSpecial(int *p)
{
    int mon = p[HORRENDOUS_SPECIAL_UNIT];
    int mirror = p[HORRENDOUS_SPECIAL_ARRAY];
    int count;

    if (MaxHealth(mon))
    {
        if (CurrentHealth(mon))
        {
            FrameTimerWithArg(1, p, procHorrendousSpecial);
            float height=10.0;
            count=p[HORRENDOUS_SPECIAL_COUNT];
            while (--count>=0)
            {
                Raise(mirror+count, height);
                height+=20.0;
            }
            if (ToInt(DistanceUnitToUnit(mon, mirror)))
            {
                float x=GetObjectX(mon), y=GetObjectY(mon);
                count=p[HORRENDOUS_SPECIAL_COUNT];
                while (--count>=0)
                    MoveObject(mirror+count, x,y);
            }
            return;
        }
        count = p[HORRENDOUS_SPECIAL_COUNT];
        while (--count>=0)
        {
            SetUnitMaxHealth(mirror+count, 1);
            Damage(mirror+count, 0, MaxHealth(mon)+1, DAMAGE_TYPE_PLASMA);
        }
    }
    FreeSmartMemEx(p);
}

int createTallHorrendous(float x, float y)
{
    int mon=CreateObjectById(OBJ_HORRENDOUS, x,y);
    int mirror=mon+1;
    int count = MAX_HORRENDOUS_MIRRORCOUNT;

    while (--count>=0)
        CreateObjectById(OBJ_HORRENDOUS, x,y);
    int *pmem;

    AllocSmartMemEx(HORRENDOUS_SPECIAL_MAX*4, &pmem);
    pmem[HORRENDOUS_SPECIAL_UNIT]=mon;
    pmem[HORRENDOUS_SPECIAL_ARRAY]=mirror;
    pmem[HORRENDOUS_SPECIAL_COUNT]=MAX_HORRENDOUS_MIRRORCOUNT;
    FrameTimerWithArg(1, pmem, procHorrendousSpecial);
    count = MAX_HORRENDOUS_MIRRORCOUNT;

    while (--count>=0)
    {
        SetUnitMaxHealth(mirror+count, 0); //invincible
        UnitNoCollide(mirror+count);
        Enchant(mirror+count, "ENCHANT_HELD", 0.0);
    }
    return mon;
}

int HorrendousBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[16] = 100000; arr[17] = 400; 
		arr[18] = 400; arr[19] = 95; arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; 
		arr[26] = 9; arr[27] = 5; arr[28] = 1109393408; arr[29] = AREA35_DAMAGE_HORRENDOUS; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1386; arr[61] = 46907648; 
	pArr = &arr;
	return pArr;
}

void HorrendousSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077306982;		ptr[137] = 1077306982;
	int *hpTable = ptr[139];
	hpTable[0] = AREA35_HP_TALL_HORRENDOUS;	hpTable[1] = AREA35_HP_TALL_HORRENDOUS;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = HorrendousBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

int sectionMonster35(float x, float y)
{
    int unit = createTallHorrendous(x,y);

    HorrendousSubProcess(unit);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection35()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(3,5,0);
    if (!door1)
        return;
    int door2=findGateObject(3,5,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={62,63,64};

    startSpawnDungeonMonsters(marks, sectionMonster35, AREA35_MOB_COUNT);
}

int HecubahBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 275; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = AREA41_DAMAGE_HECUBAH; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; 
		arr[59] = 5542784; 
	pArr = &arr;
	return pArr;
}

void HecubahSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = AREA41_HP_HECUBAH;	hpTable[1] = AREA41_HP_HECUBAH;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = HecubahBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster41(float x, float y)
{
    int unit = CreateObjectById(OBJ_HECUBAH, x,y);

    HecubahSubProcess(unit);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection41()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(4,1,0);
    if (!door1)
        return;
    int door2=findGateObject(4,1,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={69,70,71};

    startSpawnDungeonMonsters(marks, sectionMonster41, AREA41_MOB_COUNT);
}

int LichBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[17] = 300; arr[19] = 55; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; arr[28] = 1109393408; arr[29] = AREA42_DAMAGE_LICH; 
		arr[31] = 11; arr[32] = 10; arr[33] = 18; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1342; arr[61] = 46909440; 
	pArr = &arr;
	return pArr;
}

void LichSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = AREA42_HP_LICH;	hpTable[1] = AREA42_HP_LICH;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = LichBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster42(float x, float y)
{
    int unit = CreateObjectById(OBJ_LICH, x,y);

    LichSubProcess(unit);
    return unit;
}

void OpenSection42()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(4,2,0);
    if (!door1)
        return;
    int door2=findGateObject(4,2,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={72,73,74};

    startSpawnDungeonMonsters(marks, sectionMonster42, AREA42_MOB_COUNT);
}

int UrchinShamanBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 1750298217; arr[2] = 1851878753; arr[17] = 180; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[37] = 1869768788; 
		arr[38] = 1735289207; arr[39] = 1852798035; arr[40] = 101; arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 48; arr[56] = 60; arr[60] = 1340; arr[61] = 46904576; 
	pArr = &arr;
	return pArr;
}

void UrchinShamanSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = AREA43_HP_SHAMAN;	hpTable[1] = AREA43_HP_SHAMAN;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = UrchinShamanBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onGasCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER,SELF))
    {
        int hash=GetUnit1C(SELF);

        if (HashGet(hash,GetCaller(),NULLPTR,FALSE))
            return;

        Damage(OTHER,SELF,AREA43_DAMAGE_SHAMAN,DAMAGE_TYPE_POISON);
        HashPushback(hash,GetCaller(),TRUE);
    }
}

void onSmokingGunLoop(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int hash=GetUnit1C(sub);
        int owner=GetOwner(sub);
        if(CurrentHealth(owner)){
            if(IsVisibleTo(sub,sub+1))
            {
                int dur=GetDirection(sub); 
                if (dur)
                {
                    PushTimerQueue(1,sub,onSmokingGunLoop);
                    LookWithAngle(sub,dur-1);
                    MoveObjectVector(sub,GetObjectZ(sub),GetObjectZ(sub+1));
                    MoveObjectVector(sub+1,GetObjectZ(sub),GetObjectZ(sub+1));
                    MoveObjectVector(sub+2,GetObjectZ(sub),GetObjectZ(sub+1));
                    return;
                }
            }
        }
        Delete(sub);
        Delete(sub+1);
        Delete(sub+2);
        if(hash)
            HashDeleteInstance(hash);
    }
}

void startSmokingGun(int caster, int target)
{
    float vect[]={UnitRatioX(target,caster,13.0),UnitRatioY(target,caster,13.0)};
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,GetObjectX(caster)+vect[0],GetObjectY(caster)+vect[1]);
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(caster)-vect[0],GetObjectY(caster)-vect[1]);
    int gas= CreateObjectById(OBJ_BIG_SMOKE, GetObjectX(sub),GetObjectY(sub));

    UnitNoCollide(gas);
    Frozen(gas, TRUE);
    SetUnitFlags(gas,GetUnitFlags(gas)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetUnitCallbackOnCollide(gas, onGasCollide);
    Raise(sub,vect[0]);
    Raise(sub+1,vect[1]);
    SetOwner(caster,sub);
    SetOwner(caster,gas);
    LookWithAngle(sub,21);
    PushTimerQueue(1,sub,onSmokingGunLoop);
    int hash;
    HashCreateInstance(&hash);
    SetUnit1C(sub,hash);
    SetUnit1C(gas,hash);
    MonsterForceCastSpell(caster,0,GetObjectX(caster)+vect[0],GetObjectY(caster)+vect[1]);
}

void onShamanSight()
{
    if (!CurrentHealth(SELF))
        return;

	if (CurrentHealth(OTHER))
    {
		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
        {
			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
            startSmokingGun(GetTrigger(),GetCaller());
		}
	}
    CheckResetSight(GetTrigger(),0,48);
}

int sectionMonster43(float x, float y)
{
    int unit = CreateObjectById(OBJ_URCHIN_SHAMAN, x,y);

    UrchinShamanSubProcess(unit);
    SetCallback(unit,3,onShamanSight);
    AggressionLevel(unit, 1.0);
    return unit;
}

void OpenSection43()
{
    if (!commonSectionCheck())
        return;

    WallOpen(Wall(136,228));
    WallOpen(Wall(137,229));
    WallOpen(Wall(138,230));
    short marks[]={78,79,80};
    startSpawnDungeonMonsters(marks, sectionMonster43, AREA43_MOB_COUNT);
}

void removeMecaShot(int meca)
{
    if (ToInt(GetObjectX(meca)))
    {
        Delete(meca);
    }
}

void putMecaShot(int owner, int target)
{
    int meca=CreateObjectById(OBJ_MECHANICAL_GOLEM,GetObjectX(owner)+UnitRatioX(target,owner,13.0),GetObjectY(owner)+UnitRatioY(target,owner,13.0));
    SetOwner(owner,meca);
    LookAtObject(meca,target);
    UnitNoCollide(meca);
    SetUnitMaxHealth(meca,0);
    HitLocation(meca,GetObjectX(meca),GetObjectY(meca));
    PushTimerQueue(6,meca,removeMecaShot);
}

void onBlackWolfSight()
{
    if (!CurrentHealth(SELF))
        return;

	if (CurrentHealth(OTHER))
    {
		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
        {
			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
            putMecaShot(GetTrigger(),GetCaller());
		}
	}
    CheckResetSight(GetTrigger(),0,60);
}

int sectionMonster44(float x, float y)
{
    int unit = CreateObjectById(OBJ_BLACK_WOLF, x,y);

    SetUnitMaxHealth(unit, AREA44_HP_BLACK_WOLF);
    SetCallback(unit,3,onBlackWolfSight);
    return unit;
}

void OpenSection44()
{
    if (!commonSectionCheck())
        return;

    WallOpen(Wall(90,182));
    WallOpen(Wall(91,183));
    WallOpen(Wall(92,184));
    short marks[]={75,76,77};
    startSpawnDungeonMonsters(marks, sectionMonster44, AREA44_MOB_COUNT);
}

int DemonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869440324; arr[1] = 110; arr[17] = 200; arr[19] = 96; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1066192077; arr[26] = 4; arr[27] = 4; arr[28] = 1108082688; 
		arr[29] = AREA51_DAMAGE2_KILLERIAN; arr[31] = 10; arr[53] = 1128792064; arr[54] = 4; arr[58] = 5545472; 
		arr[59] = 5542784; arr[60] = 1347; arr[61] = 46910976; 
	pArr = &arr;
	return pArr;
}

void DemonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077432811;		ptr[137] = 1077432811;
	int *hpTable = ptr[139];
	hpTable[0] = AREA51_HP_KILLERIAN;	hpTable[1] = AREA51_HP_KILLERIAN;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = DemonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

// void onDemonSight()
// {
//     if (!CurrentHealth(SELF))
//         return;

// 	if (CurrentHealth(OTHER))
//     {
// 		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
//         {
// 			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
//             demonStrike(GetTrigger(),GetCaller());
// 		}
// 	}
//     CheckResetSight(GetTrigger(),0,42);
// }

int sectionMonster51(float x, float y)
{
    int unit = CreateObjectById(OBJ_DEMON, x,y);

    DemonSubProcess(unit);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection51()
{
    if (!commonSectionCheck())
        return;

    WallOpen(Wall(145,39));
    WallOpen(Wall(146,40));
    WallOpen(Wall(147,41));
    WallOpen(Wall(148,42));
    short marks[]={85,86,87};
    startSpawnDungeonMonsters(marks, sectionMonster51, AREA51_MOB_COUNT);
}

int WizardRedBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[17] = 275; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; 
		arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; arr[53] = 1128792064; arr[55] = 14; 
		arr[56] = 24; 
	pArr = &arr;
	return pArr;
}

void WizardRedSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = AREA52_HP_RED_WIZ;	hpTable[1] = AREA52_HP_RED_WIZ;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WizardRedBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster52(float x, float y)
{
    int unit = CreateObjectById(OBJ_WIZARD_RED, x,y);

    WizardRedSubProcess(unit);
    return unit;
}

void OpenSection52()
{
    if (!commonSectionCheck())
        return;

    WallOpen(Wall(167,61));
    WallOpen(Wall(168,62));
    WallOpen(Wall(169,63));
    WallOpen(Wall(170,64));
    short marks[]={88,89,90};
    startSpawnDungeonMonsters(marks, sectionMonster52, AREA52_MOB_COUNT);
}

int StoneGolemBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852798035; arr[1] = 1819232101; arr[2] = 28005; arr[17] = 300; arr[19] = 200; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; 
		arr[28] = 1112014848; arr[29] = AREA53_DAMAGE_STONE_GIANT; arr[31] = 2; arr[58] = 5545888; arr[59] = 5543904; 
		arr[60] = 1324; arr[61] = 46901248; 
	pArr = &arr;
	return pArr;
}

void StoneGolemSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1084751872;		ptr[137] = 1084751872;
	int *hpTable = ptr[139];
	hpTable[0] = AREA53_HP_STONE_GIANT;	hpTable[1] = AREA53_HP_STONE_GIANT;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = StoneGolemBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster53(float x, float y)
{
    int unit = CreateObjectById(OBJ_STONE_GOLEM, x,y);

    StoneGolemSubProcess(unit);
    return unit;
}

void OpenSection53()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(5,3,0);
    if (!door1)
        return;
    int door2=findGateObject(5,3,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={91,92,93};

    startSpawnDungeonMonsters(marks, sectionMonster53, AREA53_MOB_COUNT);
}

int WoundedWarriorBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1466197348; arr[2] = 1769108065; arr[3] = 29295; arr[17] = 325; 
		arr[19] = 90; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1112014848; 
		arr[29] = AREA54_DAMAGE_WOUNDED_WAR; arr[32] = 4; arr[33] = 9; arr[59] = 5542784; arr[60] = 2272; 
		arr[61] = 46912512; 
	pArr = &arr;
	return pArr;
}

void WoundedWarriorSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = AREA54_HP_WOUNDED_WAR;	hpTable[1] = AREA54_HP_WOUNDED_WAR;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WoundedWarriorBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

int sectionMonster54(float x, float y)
{
    int unit = CreateObjectById(OBJ_WOUNDED_WARRIOR, x,y);

    WoundedWarriorSubProcess(unit);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection54()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(5,4,0);
    if (!door1)
        return;
    int door2=findGateObject(5,4,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={103,104,105};

    startSpawnDungeonMonsters(marks, sectionMonster54, AREA54_MOB_COUNT);
}

int BuldakBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 325; arr[19] = 150; 
		arr[23] = 34816; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1113325568; arr[29] = 1; 
		arr[31] = 11; arr[32] = 3; arr[33] = 6; arr[59] = 5542784; arr[60] = 2303; 
		arr[61] = 46917632; 
	pArr = arr;
	return pArr;
}

void BuldakSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1083179008;		ptr[137] = 1083179008;
	int *hpTable = ptr[139];
	hpTable[0] = AREA55_HP_BULDAK;	hpTable[1] = AREA55_HP_BULDAK;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = BuldakBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

int sectionMonster55(float x, float y)
{
    int unit = CreateObjectById(OBJ_WOUNDED_CONJURER, x,y);

    BuldakSubProcess(unit);
    // int img=CreateObjectById(OBJ_URCHIN_TABLE_LARGE, GetObjectX(unit), GetObjectY(unit));

    // UnitNoCollide(img);
    // Frozen(img,TRUE);
    // SetOwner(unit, img);
    // PushTimerQueue(1,img, onTalkingSkullLoop);
    RegistUnitStrikeHook(unit);
    return unit;
}

void OpenSection55()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(5,5,0);
    if (!door1)
        return;
    int door2=findGateObject(5,5,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={108,109,110};

    startSpawnDungeonMonsters(marks, sectionMonster55, AREA55_MOB_COUNT);
}

int OgreWarlordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996367; arr[1] = 1819435351; arr[2] = 6582895; arr[17] = 130; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1068540887; arr[25] = 2; arr[26] = 10; 
		arr[27] = 7; arr[28] = 1109393408; arr[29] = AREA61_DAMAGE_OGRE_LORD; arr[30] = 1112014848; arr[31] = 11; 
		arr[32] = 13; arr[33] = 21; arr[37] = 1701996367; arr[38] = 1920297043; arr[39] = 1852140393; 
		arr[53] = 1128792064; arr[54] = 4; arr[55] = 20; arr[56] = 30; arr[57] = 5548368; 
		arr[59] = 5542784; arr[60] = 1389; arr[61] = 46914048; 
	pArr = arr;
	return pArr;
}

void OgreWarlordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = AREA61_HP_OGRE_LORD;	hpTable[1] = AREA61_HP_OGRE_LORD;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = OgreWarlordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster61(float x, float y)
{
    int unit = CreateObjectById(OBJ_OGRE_WARLORD, x,y);

    OgreWarlordSubProcess(unit);
    return unit;
}

void OpenSection61()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(6,1,0);
    if (!door1)
        return;
    int door2=findGateObject(6,1,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={111,112,113};
    startSpawnDungeonMonsters(marks, sectionMonster61, AREA61_MOB_COUNT);
}

int BeholderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869112642; arr[1] = 1919247468; arr[17] = 250; arr[19] = 60; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[37] = 1818324307; arr[38] = 1701725548; 
		arr[39] = 1115252594; arr[40] = 7629935; arr[53] = 1128792064; arr[55] = 44; arr[56] = 88; 
		arr[60] = 1382; arr[61] = 46909696; 
	pArr = &arr;
	return pArr;
}

void BeholderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = AREA62_HP_BEHOLDER;	hpTable[1] = AREA62_HP_BEHOLDER;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = BeholderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster62(float x, float y)
{
    int unit = CreateObjectById(OBJ_BEHOLDER, x,y);

    BeholderSubProcess(unit);
    return unit;
}

void OpenSection62()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(6,2,0);
    if (!door1)
        return;
    int door2=findGateObject(6,2,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={115,116,117};
    startSpawnDungeonMonsters(marks, sectionMonster62, AREA62_MOB_COUNT);
}

int WaspNestBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 200; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1110704128; 
		arr[29] = AREA63_DAMAGE_HIVE; arr[31] = 11; arr[32] = 7; arr[33] = 13; arr[59] = 5542784; 
		arr[60] = 2303; arr[61] = 46917632; 
	pArr = &arr;
	return pArr;
}

void WaspNestSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WaspNestBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onNestLoop(int unit)
{
    if (ToInt(GetObjectX(unit)))
    {
        if (CurrentHealth(unit))
        {
            int nest=unit+1;
            if (CurrentHealth(nest))
            {
                PushTimerQueue(1, unit,onNestLoop);
                if (ToInt( DistanceUnitToUnit(unit,nest)) )
                    MoveObject(nest,GetObjectX(unit),GetObjectY(unit));
                return;
            }
        }
        Damage(unit,0,MaxHealth(unit)+1,DAMAGE_TYPE_PLASMA);
        Delete(unit+1);
    }
}

int sectionMonster63(float x, float y)
{
    int mon=CreateObjectById(OBJ_TALKING_SKULL, x,y);

    TalkingSkullSubProcess(mon);
    UnitNoCollide(mon);
    int nest=CreateObjectById(OBJ_WASP_NEST, GetObjectX(mon),GetObjectY(mon));
    SetUnitMaxHealth(nest,AREA63_HP_HIVE);
    PushTimerQueue(1,mon,onNestLoop);
    return mon;
}

void OpenSection63()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(6,3,0);
    if (!door1)
        return;
    int door2=findGateObject(6,3,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={121,122,123};
    startSpawnDungeonMonsters(marks, sectionMonster63, AREA63_MOB_COUNT);
}

void deferredInitMimic(int mon)
{
    if(CurrentHealth(mon)){
    int *p = MonsterActionPush(mon, 3);
    p[1]=0;
    p[2]=0;
    p[3]=UnitToPtr(mon);}
}

void onMimicLostEnemy()
{
    AggressionLevel(SELF,1.0);
    deferredInitMimic(GetTrigger());
}

int sectionMonster64(float x, float y)
{
    int mon=CreateObjectById(OBJ_MIMIC, x,y);

    PushTimerQueue(1,mon,deferredInitMimic);
    SetUnitMaxHealth(mon, AREA64_HP_MIMIC);
    SetCallback(mon, 13, onMimicLostEnemy);
    return mon;
}

void OpenSection64()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(6,4,0);
    if (!door1)
        return;
    int door2=findGateObject(6,4,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={124,125,126};
    startSpawnDungeonMonsters(marks, sectionMonster64, AREA64_MOB_COUNT);
}

int StormCloudBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 200; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; 
		arr[37] = 1769236816; arr[38] = 1181513062; arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1128792064; 
		arr[55] = 28; arr[56] = 38; arr[60] = 2303; arr[61] = 46917632; 
	pArr = &arr;
	return pArr;
}

void StormCloudSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = AREA65_HP_STORMS;	hpTable[1] = AREA65_HP_STORMS;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = StormCloudBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster65(float x, float y)
{
    int mon=CreateObjectById(OBJ_TALKING_SKULL, x,y);
    int img=CreateObjectById(OBJ_STORM_CLOUD,x,y);

    StormCloudSubProcess(mon);
    PushTimerQueue(1,mon,onGhostImageLoop);
    return mon;
}

void OpenSection65()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(6,5,0);
    if (!door1)
        return;
    int door2=findGateObject(6,5,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={127,128,129};
    startSpawnDungeonMonsters(marks, sectionMonster65, AREA65_MOB_COUNT);
}

int CarnivorousPlantBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 100; 
		arr[19] = 80; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[27] = 5; 
		arr[28] = 1109393408; arr[29] = AREA66_DAMAGE_CARNI_PLANT; arr[31] = 8; arr[59] = 5542784; arr[60] = 1371; 
		arr[61] = 46901760; 
	pArr = &arr;
	return pArr;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = AREA66_HP_CARN_PLANT;	hpTable[1] = AREA66_HP_CARN_PLANT;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = CarnivorousPlantBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int sectionMonster66(float x, float y)
{
    int mon=CreateObjectById(OBJ_CARNIVOROUS_PLANT, x,y);

    CarnivorousPlantSubProcess(mon);
    AggressionLevel(mon, 1.0);
    SetUnitScanRange(mon, 400.0);
    return mon;
}

void OpenSection66()
{
    if (!commonSectionCheck())
        return;

    int door1=findGateObject(6,6,0);
    if (!door1)
        return;
    int door2=findGateObject(6,6,'A');
    if (!door2)
        return;

    LockDoorEx(door1,0,SOUND_Unlock);
    LockDoorEx(door2,0,SOUND_Unlock);
    short marks[]={130,131,132};
    startSpawnDungeonMonsters(marks, sectionMonster66, AREA66_MOB_COUNT);
}

void buldakStrikeFunction(int attacker, int victim)
{
    Damage(OTHER, SELF, AREA55_DAMAGE_BULDAK, DAMAGE_TYPE_FLAME);
    DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 18);
}

void frogStrikeFunction(int attacker, int victim)
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(SELF), GetObjectY(SELF));

    SetOwner(attacker, sub);
    Raise(sub, victim);
    PushTimerQueue(4, sub, reventHold);
}

void woundedApprenticeStrikeFunction(int attacker, int victim)
{
    Effect("DAMAGE_POOF",GetObjectX(victim),GetObjectY(victim),0.0,0.0);
}

void jandorStrikeFunction(int attacker, int victim)
{
    Effect("SENTRY_RAY", GetObjectX(attacker),GetObjectY(attacker),GetObjectX(victim),GetObjectY(victim));
    Damage(victim,attacker,AREA23_DAMAGE_BONUS_JANDOR, DAMAGE_TYPE_ZAP_RAY);
}

void horrendousStrikeFunction(int attacker, int victim)
{
    Damage(victim,attacker,AREA35_DAMAGE_BONUS_HORRENDOUS, DAMAGE_TYPE_ZAP_RAY);
    Effect("RICOCHET", GetObjectX(victim),GetObjectY(victim),0.0,0.0);
}

void hecubahStrikeFunction(int attacker,int victim)
{
    Enchant(victim, "ENCHANT_FREEZE", 1.0);
}

void woundedWarStrikeFunction(int attacker, int victim)
{
    float point[]={GetObjectX(victim),GetObjectY(victim)};
    short objs[]={OBJ_CRATE_BREAKING_1,OBJ_CRATE_BREAKING_2,OBJ_COFFIN_BREAKING_1,OBJ_COFFIN_BREAKING_2,OBJ_LARGE_BARREL_BREAKING_1, OBJ_LARGE_BARREL_BREAKING_2};
    
    Effect("SMOKE_BLAST",point[0],point[1],0.0,0.0);
    CreateObjectById(objs[Random(0,sizeof(objs)-1)],point[0],point[1]);
}

void demonStrike(int me, int you)
{
    DeleteObjectTimer(CreateObjectById(OBJ_METEOR_EXPLODE,GetObjectX(you),GetObjectY(you)),9);
    Damage(you,me,AREA51_DAMAGE_KILLERIAN,DAMAGE_TYPE_CRUSH);
}

void InitialFillMonsterStrikeHash(int pInstance)
{
    HashPushback(pInstance, OBJ_WOUNDED_CONJURER, buldakStrikeFunction);
    HashPushback(pInstance, OBJ_GREEN_FROG, frogStrikeFunction);
    HashPushback(pInstance, OBJ_WOUNDED_APPRENTICE, woundedApprenticeStrikeFunction);
    HashPushback(pInstance, OBJ_AIRSHIP_CAPTAIN, jandorStrikeFunction);
    HashPushback(pInstance, OBJ_HORRENDOUS, horrendousStrikeFunction);
    HashPushback(pInstance, OBJ_HECUBAH, hecubahStrikeFunction);
    HashPushback(pInstance, OBJ_WOUNDED_WARRIOR, woundedWarStrikeFunction);
    HashPushback(pInstance, OBJ_DEMON, demonStrike);
}

void ogreShooterFlying(int sub)
{
    int owner=GetOwner(sub);
    while (CurrentHealth(owner))
    {
        int target=GetUnit1C(sub);
        if (CurrentHealth(target))
        {
            if (IsVisibleTo(sub,sub+1))
            {
                int dur=GetDirection(sub);

                if (dur)
                {
                    if (DistanceUnitToUnit(sub,target)<36.0)
                    {
                        Damage(target,owner,AREA61_DAMAGE_MISSILE_OGRE_LORD,DAMAGE_TYPE_CLAW);
                        DeleteObjectTimer(CreateObjectById(OBJ_THIN_FIRE_BOOM,GetObjectX(target),GetObjectY(target)),9);
                        break;
                    }
                    float vect[]={UnitRatioX(target,sub,11.0),UnitRatioY(target,sub,11.0)};
                    PushTimerQueue(1,sub,ogreShooterFlying);
                    LookWithAngle(sub,dur-1);
                    MoveObjectVector(sub,vect[0],vect[1]);
                    MoveObjectVector(sub+1,vect[0],vect[1]);
                    return;
                }
            }
        }
        break;
    }
    Delete(sub);
    Delete(sub+1);
}

void startOgreAttack(int sub)
{
    int owner=GetOwner(sub), target=GetUnit1C(sub);

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        int unit=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(owner)+UnitRatioX(target,owner,11.0),GetObjectY(owner)+UnitRatioY(target,owner,11.0));

        CreateObjectById(OBJ_OLD_SMOKE, GetObjectX(owner)-UnitRatioX(target,owner,11.0),GetObjectY(owner)-UnitRatioY(target,owner,11.0));
        PushTimerQueue(1,unit,ogreShooterFlying);
        LookWithAngle(unit,32);
        SetOwner(owner,unit);
        SetUnit1C(unit,target);
    }
    Delete(sub);
}

void onOgreSightSub()
{
    int ctarget=GetUnit1C(SELF);

    if (CurrentHealth(ctarget))
    {
        if (DistanceUnitToUnit(OTHER,SELF)>=DistanceUnitToUnit(SELF,ctarget))
            return;
    }
    SetUnit1C(SELF,GetCaller());
}

void onOgreShurikenDetected(int cur,int owner)
{
    float point[]={GetObjectX(cur),GetObjectY(cur)};
    Delete(cur);
    int sub=CreateObjectById(OBJ_WEIRDLING_BEAST, point[0],point[1]);
    PushTimerQueue(1,sub,startOgreAttack);
    SetOwner(owner,sub);
    SetUnit1C(sub,0);
    LookWithAngle(sub,GetDirection(owner));
    SetCallback(sub,3,onOgreSightSub);
}

void onBlackwidowWebCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, GetOwner(SELF), AREA34_DAMAGE_MISSILE_BLACK_SPIDER, DAMAGE_TYPE_IMPACT);
            Enchant(OTHER, "ENCHANT_SLOWED", 3.0);
        }
    }
    Delete(SELF);
}

void OnThrowingStoneCollide()
{
    if (!GetTrigger())
        return;

    while (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            Damage(OTHER, SELF, AREA52_DAMAGE_MISSILE_RED_WIZ, DAMAGE_TYPE_IMPACT);
            break;
        }
        return;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    PlaySoundAround(SELF, SOUND_HitEarthBreakable);
    Delete(SELF);
}

void MultipleThrowingStone(int caster)
{
    float xvect = UnitAngleCos(caster, 9.0), yvect = UnitAngleSin(caster, 9.0);
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(caster) + xvect -(yvect*2.0), GetObjectY(caster) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(caster);

    while (++rep<5)
    {
        single= CreateObjectById(OBJ_WEAK_ARCHER_ARROW, GetObjectX(posUnit), GetObjectY(posUnit));
        LookWithAngle(single, dir);
        SetOwner(caster, single);
        SetUnitEnchantCopy(single, GetLShift(ENCHANT_SLOWED));
        SetUnitCallbackOnCollide(single, OnThrowingStoneCollide);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
    PlaySoundAround(caster, SOUND_BlindOff);
}

void onSpiderSpitIndexLoop(int cur,int owner)
{
    int ownerId=GetUnitThingID(owner);

    if (ownerId==OBJ_BLACK_WIDOW)
        SetUnitCallbackOnCollide(cur, onBlackwidowWebCollide);
    else if (ownerId==OBJ_WIZARD_RED)
    {
        MultipleThrowingStone(owner);
        Delete(cur);
    }
}

void removeBeholderTrap(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        Delete(sub);
        Delete(sub+1);
    }
}

void onBeholderMisDetected(int cur,int owner)
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(owner),GetObjectY(owner));
    int t= CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(cur),GetObjectY(cur));

    PushTimerQueue(BEHOLDER_LIGHTNING_DURATION, sub, removeBeholderTrap);
    SetOwner(owner,sub);
    CastSpellObjectObject("SPELL_CHAIN_LIGHTNING",sub,t);
    Delete(cur);
}

void onPitifulLoop(int sub)
{
    int owner=GetOwner(sub);
    while (CurrentHealth(owner))
    {
        int target=GetUnit1C(sub);
        if (CurrentHealth(target))
        {
            if (IsVisibleTo(sub,sub+1))
            {
                int dur=GetDirection(sub);

                if (dur)
                {
                    if (DistanceUnitToUnit(sub,target)<36.0)
                    {
                        Damage(target,owner,AREA65_DAMAGE_MISSILE_STORMS,DAMAGE_TYPE_CLAW);
                        DeleteObjectTimer(CreateObjectById(OBJ_THIN_FIRE_BOOM,GetObjectX(target),GetObjectY(target)),9);
                        break;
                    }
                    float vect[]={UnitRatioX(target,sub,11.0),UnitRatioY(target,sub,11.0)};
                    PushTimerQueue(1,sub,onPitifulLoop);
                    LookWithAngle(sub,dur-1);
                    MoveObjectVector(sub,vect[0],vect[1]);
                    MoveObjectVector(sub+1,vect[0],vect[1]);
                    return;
                }
            }
        }
        break;
    }
    Delete(sub);
    Delete(sub+1);
}

void startPitifulLoop(int sub)
{
    int owner=GetOwner(sub), target=GetUnit1C(sub);

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        int unit=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(owner)+UnitRatioX(target,owner,11.0),GetObjectY(owner)+UnitRatioY(target,owner,11.0));

        CreateObjectById(OBJ_MAGIC_SPARK, GetObjectX(owner)-UnitRatioX(target,owner,11.0),GetObjectY(owner)-UnitRatioY(target,owner,11.0));
        PushTimerQueue(1,unit,onPitifulLoop);
        LookWithAngle(unit,38);
        SetOwner(owner,unit);
        SetUnit1C(unit,target);
    }
    Delete(sub);
}

void onPitfulFireballDetected(int cur,int owner)
{
    int sub=CreateObjectById(OBJ_WEIRDLING_BEAST,GetObjectX(cur),GetObjectY(cur));

    PushTimerQueue(1,sub,startPitifulLoop);
    Delete(cur);
    SetOwner(owner,sub);
    SetUnit1C(sub,0);
    LookWithAngle(sub,GetDirection(owner));
    SetCallback(sub,3,onOgreSightSub);
}

void InitialFillIndexLoopHash(int pInstance)
{
    HashPushback(pInstance, OBJ_SPIDER_SPIT, onSpiderSpitIndexLoop);
    HashPushback(pInstance, OBJ_OGRE_SHURIKEN, onOgreShurikenDetected);
    HashPushback(pInstance, OBJ_SMALL_ENERGY_BOLT, onBeholderMisDetected);
    HashPushback(pInstance, OBJ_PITIFUL_FIREBALL, onPitfulFireballDetected);
}

void NOXLibraryEntryPointFunction()
{ }

