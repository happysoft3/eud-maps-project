
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/waypoint.h"
#include "libs/queueTimer.h"
#include "libs/objectiddefines.h"

int LichLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 295; arr[19] = 96; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 30; arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; 
		arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void LichLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077432811;		ptr[137] = 1077432811;
	int *hpTable = ptr[139];
	hpTable[0] = 295;	hpTable[1] = 295;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4; 		
		arr[53] = 1128792064; arr[54] = 4; 
		link=arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 30; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; arr[34] = 0; 
		arr[58] = 5546320; arr[59] = 5542784; 
		arr[61] = 0; 
		link=arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743;
		
		arr[17] = 85; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		arr[61] = 0; 
		link=arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[4] = 0;  
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 8; arr[32] = 8; arr[33] = 16; arr[34] = 0; 
		arr[57] = 5548112; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;		
        arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064; arr[54] = 0; 
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link=arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 90; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 		
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link=arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
        arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link=arr;
	}
	return link;
}

int WoundedApprenticeBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 		
		arr[17] = 50; arr[18] = 10; arr[19] = 40; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067450368; 
		arr[26] = 4; arr[28] = 1112014848; arr[29] = 40; 
		arr[30] = 1106247680; arr[31] = 2; arr[32] = 12; arr[33] = 20; arr[34] = 0; 
		arr[57] = 5548112; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

void CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        arr[5] = MonsterGoonProcess; arr[30] = MonsterWeirdlingBeastProcess; arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process; arr[12] = MonsterFireSpriteProcess;
        arr[73] = MonsterWizardRedProcess; arr[29] = MonsterAirshipCaptainProcess; arr[39] = MonsterWoundedApprentice;
		arr[OBJ_LICH_LORD%97]=LichLordSubProcess;
    }
    if (thingID)
    {
        if (arr[key])
            CallFunctionWithArg(arr[key], unit);
    }
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
    }
}

void MonsterWoundedApprentice(int unit)
{
    //TODO: Index. 9, ThingName= WoundedApprentice
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedApprenticeBinTable());
        SetUnitMaxHealth(unit, 230);
        SetUnitSpeed(unit, 1.5);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

void StrStartAdventure()
{
	string name = "ManaBombOrb";
	float pos_x = LocationX(1), pos_y = LocationY(1);
	int arr[]={
		1008845820, 1606688772, 285216800, 270598180, 167543044, 17301793, 1115949089, 138414344,
		572540692, 1107314567, 301861136, 671219746, 16797764, 1074790670, 134217762,  8388608,
		1075831074, 133988224, 1887439120, 537003135, 67129408, 2088960, 537001986, };
	int i, count;

	for (i = 0 ; i < 23 ; i ++)
		count = DrawStrStartAdventure(arr[i], name, count);
	TeleportLocation(1, pos_x, pos_y);
}

int DrawStrStartAdventure(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 713 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 65 == 64)
			TeleportLocationVector(1, -130.0, -126.0);
		else
			TeleportLocationVector(1, 2.0, 2.0);
		count ++;
	}
	return count;
}

void PutStartStampString()
{
    TeleportLocation(1, LocationX(141), LocationY(141));
    StrStartAdventure();
}

void FistTrapCountdown(int trp)
{
    int count = GetDirection(trp);

    if (count)
    {
        LookWithAngle(trp, --count);

        FrameTimerWithArg(3, trp, FistTrapCountdown);
    }
}

void TriggeredFistTrap()
{
    if (CurrentHealth(OTHER) && MaxHealth(SELF))
    {
        if (GetDirection(SELF))
            return;
        else
        {
            int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));

            Delete(unit);
            CastSpellLocationLocation("SPELL_FIST", GetObjectX(SELF) - 2.0, GetObjectY(SELF), GetObjectX(SELF), GetObjectY(SELF));
            SetOwner(SELF, unit + 1);
            LookWithAngle(SELF, 30);
            FrameTimerWithArg(1, GetTrigger(), FistTrapCountdown);
        }
    }
}

int PutFistTrap(int locationId)
{
    int trp = CreateObjectAt("Wizard", LocationX(locationId), LocationY(locationId));

    Damage(trp, 0, MaxHealth(trp) + 1, -1);
    LookWithAngle(trp, 0);
    SetCallback(trp, 9, TriggeredFistTrap);
    return trp;
}

void InitializeSubPart()
{
	PutFistTrap(142);
	PutFistTrap(143);
	PutFistTrap(144);
	PutFistTrap(145);
	PutFistTrap(146);
	PutFistTrap(147);
}


