
#include "ak180716_utils.h"
#include "ak180716_reward.h"
#include "libs/voiceList.h"
#include"libs/spellutil.h"

void deferredCreateItemMob(int mark){
	if (ToInt(GetObjectX(mark)))
		CreateRandomItemCommon(mark);
}

void onMonsterCommonHitEvent()
{
    if (GetCaller())
        return;

    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 3, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectById(OBJ_GREEN_PUFF, GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void onMonsterDeath(){
	float x=GetObjectX(SELF), y=GetObjectY(SELF);

	DeleteObjectTimer(SELF,1);
	Effect("SPARK_EXPLOSION", x,y,0.0,0.0);
	CreateObjectById(OBJ_BREAKING_SOUP, x,y);
	if (Random(0,3))
		PushTimerQueue(10,CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,x,y),deferredCreateItemMob);
}

void monsterCommonSettings(int mon){
    RetreatLevel(mon,0.0);
    AggressionLevel(mon,1.0);
	SetCallback(mon,5,onMonsterDeath);
	SetCallback(mon,7,onMonsterCommonHitEvent);
	LookWithAngle(mon,Random(0,255));
	SetOwner(GetMasterUnit(),mon);
}

#define QUEEN_ATTACK_DATA_TARGET 0
#define QUEEN_ATTACK_DATA_OWNER 1
#define QUEEN_ATTACK_DATA_SUB 2
#define QUEEN_ATTACK_DATA_COUNT 3
#define QUEEN_ATTACK_DATA_MAX 4

void onQueenAttackLoop(int *d){
	int sub=d[QUEEN_ATTACK_DATA_SUB],target=d[QUEEN_ATTACK_DATA_TARGET];
	if (CurrentHealth(target)){
		if (IsVisibleOr(target,sub)){
			int owner=d[QUEEN_ATTACK_DATA_OWNER];
			if (DistanceUnitToUnit(target,sub)>=28.0){
				if (--d[QUEEN_ATTACK_DATA_COUNT]>=0){	
					PushTimerQueue(1,d,onQueenAttackLoop);
					MoveObjectVector(sub,UnitRatioX(target,sub,7.0),UnitRatioY(target,sub,7.0));
					return;
				}
			}
			else
			{
				if (CurrentHealth(owner)){
					Damage(target,owner,10,DAMAGE_TYPE_CLAW);
				}
			}
		}
	}
	Delete(sub);
	FreeSmartMemEx(d);
}

void startQueenAttack(int me, int you){
	int *d;

	AllocSmartMemEx(QUEEN_ATTACK_DATA_MAX*4,&d);
	d[QUEEN_ATTACK_DATA_TARGET]=you;
	d[QUEEN_ATTACK_DATA_COUNT]=31;
	d[QUEEN_ATTACK_DATA_OWNER]=me;
	int sub=CreateObjectById(OBJ_PLAYER_WAYPOINT,GetObjectX(me)+UnitAngleCos(me, 13.0),GetObjectY(me)+UnitAngleSin(me,13.0));
	SetUnitEnchantCopy(sub,GetLShift(ENCHANT_SLOWED));
	d[QUEEN_ATTACK_DATA_SUB]=sub;
	PushTimerQueue(1,d,onQueenAttackLoop);
}

void onQueenAttack(){
	if (!GetCaller())
        return;

	startQueenAttack(GetTrigger(),GetCaller());
}

int SmallSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1768969068; arr[2] = 7497060; arr[17] = 108; arr[19] = 80; 
		arr[21] = 1065353216; arr[24] = 1065353216; arr[26] = 4; arr[27] = 6; arr[28] = 1128792064; 
		arr[29] = 10; arr[32] = 12; arr[33] = 18; arr[59] = 5542784; arr[60] = 1354; 
		arr[61] = 46906880; 
	pArr = arr;
	CustomMeleeAttackCode(arr, onQueenAttack);
	return pArr;
}

void SmallSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 108;	hpTable[1] = 108;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = SmallSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonQueen(float x,float y){
	int mon=CreateObjectById(OBJ_SMALL_SPIDER,x,y);

	SetUnitVoice(mon,MONSTER_VOICE__Maiden2);
	SmallSpiderSubProcess(mon);
	SetUnitMass(mon, 999.0);
	return mon;
}

int SmallAlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1651261804; arr[2] = 1399811689; arr[3] = 1701079408; arr[4] = 114; 
		arr[17] = 130; arr[18] = 1; arr[19] = 70; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; arr[28] = 1108082688; arr[29] = 12; 
		arr[31] = 11; arr[32] = 9; arr[33] = 17; arr[59] = 5542784; arr[60] = 1356; 
		arr[61] = 46906624; 
	pArr = arr;
	return pArr;
}

void SmallAlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 130;	hpTable[1] = 130;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SmallAlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonRedgirl(float x,float y){
    int mon=CreateObjectById(OBJ_SMALL_ALBINO_SPIDER,x,y);
    SmallAlbinoSpiderSubProcess(mon);
    SetUnitVoice(mon,MONSTER_VOICE__Maiden1);
    return mon;
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 200; arr[19] = 83; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; 
		arr[26] = 4; arr[28] = 1112014848; arr[29] = 20; arr[30] = 1106247680; arr[31] = 2; 
		arr[32] = 13; arr[33] = 21; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075797033;		ptr[137] = 1075797033;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonWoundedApprentice(float x,float y){
    int mon=CreateObjectById(OBJ_WOUNDED_APPRENTICE,x,y);

    WoundedApprenticeSubProcess(mon);
    return mon;
}

int GhostBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936681031; arr[1] = 116; arr[17] = 210; arr[19] = 60; arr[21] = 1065353216; 
		arr[23] = 65537; arr[24] = 1066108191; arr[27] = 1; arr[28] = 1092616192; arr[29] = 15; 
		arr[31] = 4; arr[32] = 5; arr[33] = 9; arr[59] = 5542784; arr[60] = 1325; 
		arr[61] = 46900224; 
	pArr = arr;
	return pArr;
}

void GhostSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 210;	hpTable[1] = 210;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = GhostBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonMaidenGhost(float x,float y){
    int mon=CreateObjectById(OBJ_GHOST,x,y);
    GhostSubProcess(mon);
    return mon;
}

void onPunchGirlAttack(){
	if (!GetCaller()) return;
	Damage(OTHER,SELF,8,DAMAGE_TYPE_IMPACT);
	Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
}

int AlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1768057921; arr[1] = 1884516206; arr[2] = 1919247465; arr[17] = 200; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1086324736; arr[29] = 10; arr[31] = 12; arr[32] = 15; arr[33] = 25; 
		arr[35] = 10; arr[59] = 5544896; arr[60] = 1355; arr[61] = 46908928; 
	pArr = arr;
	CustomMeleeAttackCode(arr, onPunchGirlAttack);
	return pArr;
}

void AlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = AlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonPunchGirl(float x,float y){
	int mon=CreateObjectById(OBJ_ALBINO_SPIDER,x,y);
	AlbinoSpiderSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE_Maiden);
	return mon;
}

void onRedMistAttack(){
	if (!GetCaller()) return;
	Effect("SENTRY_RAY",GetObjectX(SELF),GetObjectY(SELF),GetObjectX(OTHER),GetObjectY(OTHER));
	Damage(OTHER,SELF,10,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
}

int WoundedConjurerBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1130653028; arr[2] = 1969909359; arr[3] = 7497074; arr[17] = 275; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1106247680;
		arr[29] = 1; arr[31] = 12; arr[32] = 16; arr[33] = 26; arr[59] = 5542784; 
		arr[60] = 2271; arr[61] = 46912768; 
	pArr = arr;
	CustomMeleeAttackCode(arr, onRedMistAttack);
	return pArr;
}

void WoundedConjurerSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 275;	hpTable[1] = 275;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedConjurerBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonRedMist(float x,float y){
	int mon=CreateObjectById(OBJ_WOUNDED_CONJURER,x,y);
	WoundedConjurerSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE_Beholder);
	return mon;
}

void onMBAttack(){
	if (!GetCaller()) return;
	Damage(OTHER,SELF,10,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
	CreateObjectById(OBJ_BARREL_BREAKING_LOTD,GetObjectX(OTHER),GetObjectY(OTHER));
}

int WoundedWarriorBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1466197348; arr[2] = 1769108065; arr[3] = 29295; arr[17] = 325; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1106247680; arr[29] = 10; arr[32] = 16; arr[33] = 26; arr[59] = 5542784; 
		arr[60] = 2272; arr[61] = 46912512; 
	pArr = arr;
	CustomMeleeAttackCode(arr, onMBAttack);
	return pArr;
}

void WoundedWarriorSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedWarriorBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonMBLee(float x,float y){
	int mon=CreateObjectById(OBJ_WOUNDED_WARRIOR,x,y);
	WoundedWarriorSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE__MaleNPC1);
	return mon;
}
int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 260; arr[19] = 80; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; arr[35] = 3; 
		arr[36] = 20; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int summonSoldier(float x, float y){
	int mon=CreateObjectById(OBJ_GOON,x,y);
	GoonSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE__Guard1);
	SetUnitMass(mon,999.0);
	return mon;
}
int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 350; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 34817; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1101004800; arr[29] = 20; arr[32] = 6; arr[33] = 12; arr[58] = 5546320; 
		arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328;
        link=arr;
	}
	return link;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076677837);
		SetMemory(ptr + 0x224, 1076677837);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34817);
		SetMemory(GetMemory(ptr + 0x22c), 350);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 350);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}
int summonBeachGirl(float x,float y){
	int mon=CreateObjectById(OBJ_AIRSHIP_CAPTAIN,x,y);
	AirshipCaptainSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE__Maiden2);
	return mon;
}

int GiantLeechBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1851877703; arr[1] = 1701137524; arr[2] = 26723; arr[17] = 330; arr[19] = 95; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 1; 
		arr[27] = 5; arr[28] = 1097859072; arr[29] = 40; arr[31] = 11; arr[32] = 11; 
		arr[33] = 18; arr[59] = 5542784; arr[60] = 1358; arr[61] = 46895696; 
	pArr = arr;
	return pArr;
}

void GiantLeechSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077306982;		ptr[137] = 1077306982;
	int *hpTable = ptr[139];
	hpTable[0] = 330;	hpTable[1] = 330;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GiantLeechBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int summonSlime(float x,float y){
	int mon=CreateObjectById(OBJ_GIANT_LEECH,x,y);
	GiantLeechSubProcess(mon);
	SetUnitMass(mon,999.0);
	return mon;
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 60; 
		arr[19] = 70; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1108082688; arr[29] = 3; arr[31] = 3; arr[32] = 6; arr[33] = 12; 
		arr[59] = 5542784; arr[60] = 1388; arr[61] = 46915072; 
	pArr = arr;
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 60;	hpTable[1] = 60;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int summonRoach(float x,float y){
	int mon=CreateObjectById(OBJ_WEIRDLING_BEAST,x,y);
	WeirdlingBeastSubProcess(mon);
	SetUnitMass(mon, 999.0);
	return mon;
}

int ShadeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684105299; arr[1] = 101; arr[17] = 225; arr[19] = 120; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; arr[28] = 1110704128; 
		arr[29] = 15; arr[31] = 4; arr[32] = 10; arr[33] = 20; arr[59] = 5542784; 
		arr[60] = 1362; arr[61] = 46905088; 
	pArr = arr;
	return pArr;
}

void ShadeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = ShadeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonSketchMan(float x,float y){
	int mon=CreateObjectById(OBJ_SHADE,x,y);
	ShadeSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE_Necromancer);
	return mon;
}

int summonBillyOni(float x,float y){
	int m=CreateObjectById(OBJ_MECHANICAL_GOLEM,x,y);
	SetUnitMaxHealth(m,600);
	return m;
}
int SpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684631635; arr[1] = 29285; arr[17] = 500; arr[19] = 85; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; 
		arr[29] = 40; arr[31] = 2; arr[32] = 15; arr[33] = 25; arr[34] = 2; 
		arr[35] = 2; arr[36] = 30; arr[59] = 5544896; arr[60] = 1353; arr[61] = 46914304; 
	pArr = arr;
	return pArr;
}

void SpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 800;	hpTable[1] = 800;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int summonLargeGopdong(float x,float y){
	int m=CreateObjectById(OBJ_BLACK_WIDOW,x,y);
	SpiderSubProcess(m);
	SetUnitVoice(m,MONSTER_VOICE_SpittingSpider);
	SetUnitMass(m,999.0);
	return m;
}
int summonNecromancer(float x,float y)
{
    int unit = CreateObjectById(OBJ_NECROMANCER, x,y);
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 350);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(450.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_LIGHTNING"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x8000000);
    }
    SetUnitVoice(unit, MONSTER_VOICE__Maiden1);
    return unit;
}
int summonMixedFinal(float x, float y){
	short fns[]={summonNecromancer, summonLargeGopdong, summonBillyOni,};
	int *pArg=&x;

	return Bind(fns[Random(0,2)], pArg);
}
int SkeletonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818585939; arr[1] = 1852798053; arr[17] = 355; arr[19] = 72; arr[21] = 1065353216; 
		arr[23] = 65537; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 4; 
		arr[28] = 1110704128; arr[29] = 75; arr[32] = 9; arr[33] = 18; arr[58] = 5546768; 
		arr[59] = 5542784; arr[60] = 1314; arr[61] = 46910720; 
	pArr = arr;
	return pArr;
}

void SkeletonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074412912;		ptr[137] = 1074412912;
	int *hpTable = ptr[139];
	hpTable[0] = 355;	hpTable[1] = 355;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = SkeletonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int summonSurpermanOni(float x,float y){
	int m=CreateObjectById(OBJ_SKELETON,x,y);
	SkeletonSubProcess(m);
	SetUnitVoice(m,MONSTER_VOICE_EmberDemon);
	SetUnitMass(m,999.0);
	return m;
}
int placingSmallFist(float x, float y)
{
    int mark=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);
    CastSpellLocationLocation("SPELL_FIST", x-1.0,y,x,y);
    Delete(mark);
    return ++mark;
}
void onXiatallOniAttack(){
	if (!GetCaller()) return;
	// int fist=//placingSmallFist(GetObjectX(OTHER),GetObjectY(OTHER));

	// SetOwner(GetMasterUnit(),fist);
	Damage(OTHER,SELF,50,DAMAGE_TYPE_CRUSH);
	Effect("VIOLET_SPARKS",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
}
int SkeletonLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818585939; arr[1] = 1852798053; arr[2] = 1685221196; arr[17] = 444; arr[19] = 84; 
		arr[21] = 1065353216; arr[23] = 65537; arr[24] = 1065353216; arr[26] = 4; arr[27] = 4; 
		arr[28] = 1114636288; arr[29] = 50; arr[31] = 2; arr[32] = 10; arr[33] = 15; 
		arr[59] = 5542784; arr[60] = 1315; arr[61] = 46910464; 
	pArr = arr;
	CustomMeleeAttackCode(arr, onXiatallOniAttack);
	return pArr;
}

void SkeletonLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075922862;		ptr[137] = 1075922862;
	int *hpTable = ptr[139];
	hpTable[0] = 444;	hpTable[1] = 444;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = SkeletonLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int summonXiatallOni(float x,float y){
	int m=CreateObjectById(OBJ_SKELETON_LORD,x,y);
	SkeletonLordSubProcess(m);
	SetUnitVoice(m,MONSTER_VOICE_Mimic);
	SetUnitMass(m,999.0);
	return m;
}
int UrchinBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 28265; arr[17] = 363; arr[18] = 10; arr[19] = 95; 
		arr[21] = 1065353216; arr[23] = 65537; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1112014848; 
		arr[29] = 20; arr[31] = 2; arr[32] = 9; arr[33] = 17; arr[34] = 5; 
		arr[35] = 5; arr[36] = 30; arr[54] = 4; arr[59] = 5542784; arr[60] = 1339; 
		arr[61] = 46904832; 
	pArr = arr;
	return pArr;
}

void UrchinSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077306982;		ptr[137] = 1077306982;
	int *hpTable = ptr[139];
	hpTable[0] = 363;	hpTable[1] = 363;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = UrchinBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int summonXiatallOniMixed(float x,float y){
	int m=CreateObjectById(OBJ_URCHIN,x,y);
	UrchinSubProcess(m);
	SetUnitVoice(m,MONSTER_VOICE_VileZombie);
	SetUnitMass(m,999.0);
	return m;
}
void doSummonEffect(int *args){
    float *xy=args;
    PlaySummonEffect(xy,OBJ_WILL_O_WISP_GENERATOR,60);
}

#define MON_LIST_MAX 32

void loadMonList(int n, int *get, int set){
    int data[MON_LIST_MAX];

    if (get)
    {
        get[0]=data[n];
        return;
    }
    data[n]=set;
}

void AppearMonster(short fn, int n){
    int list,node, mk;

    loadMonList(n,&list,0);
    while ( LinkedListFront(list, &node) ){
        mk=LinkedListGetValue(node);
        if (mk){
            int args[]={GetObjectX(mk),GetObjectY(mk),};
            int mon= Bind(fn, args);
            monsterCommonSettings(mon);
            doSummonEffect(args);
            Delete(mk);
        }
        LinkedListPopFront(list);
    }
}

void OnMonsterMarker(int pos){
    int n= UnitScrNameToNumber(pos),list;

    loadMonList(n%MON_LIST_MAX, &list,0);
    LinkedListPushback(list,CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,GetObjectX(pos),GetObjectY(pos)), NULLPTR);
    Delete(pos);
}

void OnRoom1Enter(){
    AppearMonster(summonWoundedApprentice,0);
    AppearMonster(summonMaidenGhost,1);
}

void OnRoom2Enter(){
    AppearMonster(summonRedgirl,2);
    AppearMonster(summonQueen,3);
}
void OnRoom3Enter(){
    AppearMonster(summonPunchGirl,4);
    AppearMonster(summonRedMist,5);
}

void OnRoom4Enter(){
	AppearMonster(summonMBLee,6);
	AppearMonster(summonSurpermanOni,18);
}
void OnRoom5Enter(){
	AppearMonster(summonSoldier,7);
	AppearMonster(summonBeachGirl,8);
}
void OnRoom6Enter(){
	AppearMonster(summonSlime,9);
	AppearMonster(summonRoach,10);
	AppearMonster(summonSketchMan,11);
}
void OnRoom7Enter(){
	AppearMonster(summonBillyOni,12);
	AppearMonster(summonMaidenGhost,13);
}
void OnRoom8Enter(){
	AppearMonster(summonLargeGopdong,14);
	AppearMonster(summonMBLee,15);
}
void OnCenterHallEnter(){
	AppearMonster(summonNecromancer,16);
	AppearMonster(summonMixedFinal, 17);
}
int summonMixedPart9(float x, float y){
	short fns[]={summonPunchGirl, summonRedgirl, summonBeachGirl,summonRoach,};
	int *pArg=&x;

	return Bind(fns[Random(0,3)], pArg);
}
void OnRoom9Enter(){
	AppearMonster(summonMixedPart9, 19);
	AppearMonster(summonXiatallOni, 20);
	AppearMonster(summonXiatallOniMixed, 21);
}

void InitializeMonster(){
    int rep=MON_LIST_MAX,list;

    while (--rep>=0){
        LinkedListCreateInstance(&list);
        loadMonList(rep,0,list);
    }
}

int FallingMeteor(float sX, float sY, int sDamage, float sSpeed)
{
    int mUnit = CreateObjectById(OBJ_METEOR, sX, sY);
    int *ptr = UnitToPtr(mUnit);

    if (ptr)
    {
        int *pEC=ptr[187];

        pEC[0]=sDamage;
        ptr[5]|=0x20;
        Raise(mUnit, 255.0);
        ptr[27]=sSpeed;
    }
    return mUnit;
}

void bossAttackMeteor(int me, int you){
	int m= FallingMeteor(GetObjectX(you)+UnitAngleCos(you, 8.0),GetObjectY(you)+UnitAngleSin(you,8.0), 120, -8.0);

	SetOwner(me, m);
	PlaySoundAround(m,SOUND_MeteorCast);
}

void onFireballSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 135, DAMAGE_TYPE_EXPLOSION);
        }
    }
}

void onFireballCollide(){
	if (!GetCaller()) return;
	if (!GetTrigger()) return;
	if (CurrentHealth(OTHER)){
		int owner=GetOwner(SELF);

		if (IsAttackedBy(owner,OTHER))
		{
			SplashDamageAtEx(owner,GetObjectX(SELF),GetObjectY(SELF),85.0,onFireballSplash);
			Effect("EXPLOSION",GetObjectX(SELF),GetObjectY(SELF),0.0,0.0);
			Delete(SELF);
		}
	}
}

void bossAttackFireball(int me, int you){
	float xvect=UnitRatioX(you,me,13.0),yvect=UnitRatioY(you,me,13.0);
	int fb=CreateObjectById(OBJ_TITAN_FIREBALL,GetObjectX(me)+xvect,GetObjectY(me)+yvect);
	SetOwner(me,fb);
	LookAtObject(fb,you);
	SetUnitCallbackOnCollide(fb,onFireballCollide);
	PushObjectTo(fb,xvect*2.2,yvect*2.2);
}

void onFallAttackSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 100, DAMAGE_TYPE_EXPLOSION);
        }
    }
}
#define BOSS_FALL_BOSS 0
#define BOSS_FALL_SUB 1
#define BOSS_FALL_MAX 2

void onBossFallenAttack(int *d)
{
	int sub=d[BOSS_FALL_SUB];

	if (ToInt(GetObjectX(sub))){
		if (ToInt(GetObjectZ(sub))){
			Raise(sub,GetObjectZ(sub)-20.0);
			PushTimerQueue(1,d,onBossFallenAttack);
			return;
		}
		SplashDamageAtEx(d[BOSS_FALL_BOSS],GetObjectX(sub),GetObjectY(sub),100.0,onFallAttackSplash);
		Frozen(sub,FALSE);
		Damage(sub,0,999,DAMAGE_TYPE_PLASMA);
	}
	FreeSmartMemEx(d);
}

void bossAttackFallenTrash(int me, int you){
	short items[]={OBJ_LARGE_BARREL_1,OBJ_LARGE_BARREL_2,OBJ_COFFIN_1,OBJ_COFFIN_2,};
	int sub=CreateObjectById(items[Random(0,3)],GetObjectX(you),GetObjectY(you));

	UnitNoCollide(sub);
	Frozen(sub,TRUE);
	Raise(sub,200.0);
	int *d;
	AllocSmartMemEx(BOSS_FALL_MAX*4,&d);
	d[BOSS_FALL_BOSS]=me;
	d[BOSS_FALL_SUB]=sub;
	PushTimerQueue(1,d,onBossFallenAttack);
}

#define BOSS_ATTACK_DATA_TARGET 0
#define BOSS_ATTACK_DATA_OWNER 1
#define BOSS_ATTACK_DATA_SUB 2
#define BOSS_ATTACK_DATA_COUNT 3
#define BOSS_ATTACK_DATA_MAX 4

void onBossAttackLoop(int *d){
	int sub=d[BOSS_ATTACK_DATA_SUB],target=d[BOSS_ATTACK_DATA_TARGET];
	if (CurrentHealth(target)){
		if (IsVisibleOr(target,sub)){
			int owner=d[BOSS_ATTACK_DATA_OWNER];
			if (DistanceUnitToUnit(target,sub)>=28.0){
				if (--d[BOSS_ATTACK_DATA_COUNT]>=0){	
					PushTimerQueue(1,d,onQueenAttackLoop);
					MoveObjectVector(sub,UnitRatioX(target,sub,10.0),UnitRatioY(target,sub,10.0));
					return;
				}
			}
			else
			{
				if (CurrentHealth(owner)){
					Damage(target,owner,50,DAMAGE_TYPE_IMPACT);
				}
			}
		}
	}
	Delete(sub);
	FreeSmartMemEx(d);
}

void bossAttackVampirism(int me, int you){
	int *d;

	AllocSmartMemEx(BOSS_ATTACK_DATA_MAX*4, &d);
	d[BOSS_ATTACK_DATA_OWNER]=me;
	d[BOSS_ATTACK_DATA_COUNT]=32;
	d[BOSS_ATTACK_DATA_TARGET]=you;
	int sub=CreateObjectById(OBJ_PLAYER_WAYPOINT,GetObjectX(me)+UnitAngleCos(me, 13.0),GetObjectY(me)+UnitAngleSin(me,13.0));
	SetUnitEnchantCopy(sub,GetLShift(ENCHANT_VAMPIRISM));
	d[BOSS_ATTACK_DATA_SUB]=sub;
	PushTimerQueue(1,d,onBossAttackLoop);
}

void bossHitDeathball(int me, int you){
	float xvect=UnitRatioX(you,me,13.0),yvect=UnitRatioY(you,me,13.0);
	int fon=CreateObjectById(OBJ_DEATH_BALL_FRAGMENT,GetObjectX(me)+xvect,GetObjectY(me)+yvect);

	SetOwner(me, fon);
	PushObjectTo(fon, xvect*3.0, yvect*3.0);
	PlaySoundAround(fon,SOUND_ForceOfNatureRelease);
}

void onMoveAttackSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 100, DAMAGE_TYPE_EXPLOSION);
			PushObjectTo(OTHER,UnitRatioX(SELF,OTHER,21.0),UnitRatioY(SELF,OTHER,21.0));
        }
    }
}

void bossMoveAttack(int me, int you){
	Raise(me, 100.0);
    PushObjectTo(me, UnitAngleCos(me, 60.0), UnitAngleSin(me, 60.0));
    SplashDamageAtEx(me, GetObjectX(me), GetObjectY(me), 180.0,onMoveAttackSplash);
    GreenExplosion(GetObjectX(me), GetObjectY(me));
    Effect("JIGGLE", GetObjectX(me), GetObjectY(me), 50.0, 0.0);
    PlaySoundAround(me, SOUND_MeteorShowerCast);
    PlaySoundAround(me, SOUND_CrushHard);
}

void onFinalBossAttack(){
	if (!GetCaller()) return;

	short att[]={bossMoveAttack,bossAttackVampirism,bossAttackFallenTrash,bossAttackFireball,bossAttackMeteor,bossHitDeathball, };
	int args[]={GetTrigger(),GetCaller(),};
	Bind(att[Random(0,5)], args);
}

int LichLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 2800; arr[19] = 96; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 200.0; arr[29] = 50; arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; 
		arr[59] = 5542784; 
	pArr = arr;
	CustomMeleeAttackCode(arr,onFinalBossAttack);
	return pArr;
}

void LichLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077432811;		ptr[137] = 1077432811;
	int *hpTable = ptr[139];
	hpTable[0] = 2800;	hpTable[1] = 2800;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = LichLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateFinalMonster(float x,float y){
	int boss=CreateObjectById(OBJ_LICH_LORD,x,y);
	LichLordSubProcess(boss);
	SetUnitVoice(boss,MONSTER_VOICE__Maiden2);
	monsterCommonSettings(boss);
	return boss;
}
