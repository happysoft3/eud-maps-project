
#include "noxscript\builtins.h"
#include "libs\typecast.h"
#include "libs\unitstruct.h"
#include "libs\unitutil.h"
#include "libs\printutil.h"
#include "libs\mathlab.h"
#include "libs\username.h"
#include "libs\fxeffect.h"

#define NULLPTR 0
#define PLAYER_FLAG_DEATH 0x80000000

int TEAM_RED, TEAM_BLUE;
int player[20], FLY_TIME = 0;
string TEAM_ECT[2];

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & PLAYER_FLAG_DEATH;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ PLAYER_FLAG_DEATH;
}

void MapInitialize()
{
    MusicEvent();
    TEAM_ECT[0] = "ENCHANT_RUN";
    TEAM_ECT[1] = "ENCHANT_HASTED";
    TEAM_RED = Object("RedTeam");
    TEAM_BLUE = Object("BlueTeam");
    GetMaster();
    StrRedTeam();
    StrBlueTeam();
    FrameTimer(1, PlayerClassOnLoop);
    FrameTimer(30, SentryTrapOn);
    FrameTimer(45, LoopSearchIndex);
    FrameTimer(150, StartMent);

    string guideMessage = "**살인피구** 공(위스프)을 클릭하면 클릭 위치로 발사됩니다. 더블클릭 시 강속구!";
    
    RegistSignMessage(Object("Picket1"), guideMessage);
    RegistSignMessage(Object("Picket11"), guideMessage);
}

int CheckTeam(int unit)
{
    if (!IsAttackedBy(unit, TEAM_RED))
        return 1;
    else if (!IsAttackedBy(unit, TEAM_BLUE))
        return 2;
    return 0;
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectAt("Hecubah", 5050.0, 100.0);
        Frozen(unit, 1);
    }
    return unit;
}

void SentryTrapOn()
{
    ObjectOn(Object("westSentry"));
    ObjectOn(Object("eastSentry"));
}

void StartMent()
{
    int k;

    UniPrintToAll("신나는 살인피구 게임~!! 어딘가에 공이 생성 되었습니다.");
    UniPrintToAll("공을 <클릭> 하시면 공을 던지실 수 있습니다.");
    FrameTimerWithArg(1, Random(3, 4), SpawnBall);

    for (k = 18 ; k >= 0 ; k -= 1)
        WallOpen(Wall(128 - k, 110 + k));
}

void SpawnBall(int wp)
{
    int unit = CreateObject("Maiden", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    CreateObject("GameBall", wp);
    CreateObject("InvisibleLightBlueHigh", wp + 1); //+3
    CreateObject("InvisibleLightBlueHigh", wp + 1); //+4
    CreateObject("InvisibleLightBlueHigh", wp + 1); //+5
    CreateObject("InvisibleLightBlueHigh", wp); //+6
    LookWithAngle(unit, 0);
    LookWithAngle(unit + 1, 4);
    ObjectOff(unit);
    Damage(unit, 0, CurrentHealth(unit) + 1, -1);
    SetUnitFlags(unit + 2, GetUnitFlags(unit + 2) ^ 0x40);
    SetCallback(unit, 9, TouchedBall);
    SetDialog(unit, "NORMAL", ClickTheBall, DummyFunction);
    FrameTimerWithArg(1, unit, LoopCheckingBall);
}

void ClickTheBall()
{
    float size;

    if (!HasEnchant(other, "ENCHANT_VAMPIRISM"))
    {
        MoveWaypoint(22, GetObjectX(self), GetObjectY(self));
        if (!HasEnchant(other, "ENCHANT_VILLAIN"))
        {
            size = 10.0;
            Enchant(other, "ENCHANT_VILLAIN", 0.3);
        }
        else
        {
            UniChatMessage(other, "강속구!", 120);
            AudioEvent("SecretFound", 22);
            Effect("MechGolemHitting", GetObjectX(self), GetObjectY(self), 60.0, 0.0);
            Enchant(other, "ENCHANT_VAMPIRISM", 1.0);
            size = 18.0;
        }
        if (CheckTeam(other))
        {
            EnchantOff(GetTrigger() + 2, TEAM_ECT[0]);
            EnchantOff(GetTrigger() + 2, TEAM_ECT[1]);
            Enchant(GetTrigger() + 2, TEAM_ECT[CheckTeam(other) - 1], 7.0);
        }
        LookWithAngle(self, GetDirection(other));
        FLY_TIME = 7 * 30;
        Raise(GetTrigger() + 1, size);
        LookWithAngle(GetTrigger() + 1, 4);
        SetOwner(other, self);
        DeleteObjectTimer(CreateObject("MagicEnergy", 22), 10);
        AudioEvent("ChakramThrow", 22);
    }
}

int WallAngleTable(int num)
{
    int angle[4] = {45, 135, 315, 225};

    return angle[num];
}

void TouchedBall()
{
    int wall_t, reflect, c_wall = GetDirection(GetTrigger() + 1);

    if (!GetCaller())
    {
        wall_t = GetWallDirection(GetTrigger());
        if (wall_t != c_wall && wall_t >= 0)
        {
            reflect = (2 * WallAngleTable(wall_t)) - ((DirToAngle(GetDirection(self)) + 180) % 360);
            if (reflect < 0) reflect += 360;
            LookWithAngle(self, AngleToDir(reflect));
            LookWithAngle(GetTrigger() + 1, wall_t);
            MoveWaypoint(22, GetObjectX(self), GetObjectY(self));
            AudioEvent("ElectricalArc1", 22);
            Effect("VIOLET_SPARKS", GetWaypointX(22), GetWaypointY(22), 0.0, 0.0);
        }
    }
    else if (IsPlayerUnit(other))
    {
        Damage(other, self, 150, 14);
    }
}

void LoopCheckingBall(int ball)
{
    if (FLY_TIME) //Waypoint: 23
    {
        FLY_TIME --;
        if (!FLY_TIME)
        {
            EnchantOff(ball + 2, TEAM_ECT[0]);
            EnchantOff(ball + 2, TEAM_ECT[1]);
            Raise(ball + 1, 3.0);
            SetOwner(GetMaster(), ball);
        }
    }
    PushObjectTo(ball, UnitAngleCos(ball, GetObjectZ(ball + 1)), UnitAngleSin(ball, GetObjectZ(ball + 1)));
    MoveObject(ball + 2, GetObjectX(ball), GetObjectY(ball));
    FrameTimerWithArg(1, ball, LoopCheckingBall);
}

int GetWallDirection(int unit)
{
    int res = -1, k;
    float pos_x, pos_y;

    for (k = 0 ; k < 4 ; k ++)
    {
        if (k & 1) pos_x = 20.0;
        else pos_x = -20.0;
        if (k & 2) pos_y = 20.0;
        else pos_y = -20.0;
        MoveObject(unit + k + 3, GetObjectX(unit) + pos_x, GetObjectY(unit) + pos_y);
        if (!IsVisibleTo(unit + k + 3, unit))
        {
            res = k;
        }
        if (res >= 0) break;
    }
    return res;
}

int DirToAngle(int num)
{
    return num * 45 / 32;
}

int AngleToDir(int num)
{
    return num * 32 / 45;
}

void DummyFunction()
{
    return;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    UniPrintToAll(PlayerIngameNick(pUnit) + " 님께서 입장하였습니다");
    return plr;
}

void GetPlayers()
{
    int plr, k;

    while (1)
    {
        if (CurrentHealth(other))
        {
            plr = CheckPlayer();
            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    plr = PlayerClassOnInit(k, GetCaller());
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerJoin(plr);
                break;
            }
        }
        PlayerCantJoin();
        break;
    }
}

void PlayerJoin(int plr)
{
    int team = GetUnitTeam(other), wp;

    if (PlayerClassCheckDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    RemoveAllPicks(player[plr]);
    if (team)
    {
        //7 ~ 12, 13 ~ 
        Enchant(player[plr], "ENCHANT_ANTI_MAGIC", 0.0);
        wp = ((team - 1) * 6) + 7 + Random(0, 5);
        Enchant(player[plr], "ENCHANT_CROWN", 0.0);
        EnchantOff(player[plr], "ENCHANT_INVULNERABLE");
        MoveObject(player[plr], GetWaypointX(wp), GetWaypointY(wp));
    }
    else
    {
        MoveObject(player[plr], GetWaypointX(36), GetWaypointY(36));
        UniPrint(player[plr], "팀이 생성되지 않았습니다, 팀을 생성하신 후 다시 시도하십시오");
    }
}

void PlayerCantJoin()
{
    Enchant(other, "ENCHANT_FREEZE", 0.0);
    Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
    MoveObject(other, GetObjectX(self) - 48.0, GetObjectY(self) - 48.0);
}

void RemoveAllPicks(int unit)
{
    while (IsObjectOn(GetLastItem(unit)))
        Delete(GetLastItem(unit));
}

int CheckPlayer()
{
    int k;
    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    int model = DemonGrp() + plr;
    if (!ToInt(Distance(GetObjectX(pUnit), GetObjectY(pUnit) + 4.0, GetObjectX(model), GetObjectY(model))))
    {
        if (HasEnchant(model, "ENCHANT_VILLAIN"))
        {
            PauseObject(model, 1);
            EnchantOff(model, "ENCHANT_VILLAIN");
        }
    }
    else
    {
        Frozen(model, 0);
        Enchant(model, "ENCHANT_VILLAIN", 0.0);
        if (!HasEnchant(model, "ENCHANT_DETECTING"))
        {
            PlaySoundAround(pUnit, 374);
            Enchant(model, "ENCHANT_DETECTING", 0.25);
        }
        Frozen(model, 1);
        LookWithAngle(model, GetDirection(pUnit));
        Walk(model, GetObjectX(model), GetObjectY(model));
        MoveObject(model, GetObjectX(pUnit), GetObjectY(pUnit) + 4.0);
    }
    if (!HasEnchant(pUnit, "ENCHANT_INVISIBLE"))
    {
        Enchant(pUnit, "ENCHANT_INVISIBLE", 0.0);
    }
}

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnDeath(int plr)
{
    DeadDemon(plr, 22);
    MoveObject(DemonGrp() + plr, GetWaypointX(24 + plr), GetWaypointY(24 + plr));
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 격추되었습니다");
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (true)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void DeadDemon(int plr, int wp)
{
    int unit;

    if (MaxHealth(player[plr]))
    {
        MoveWaypoint(wp, GetObjectX(player[plr]), GetObjectY(player[plr]));
        unit = CreateObject("Demon", wp);
        SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x40);
        LookWithAngle(unit, GetDirection(player[plr]));
        Damage(unit, 0, 9999, 14);
    }
}

void HarpoonEvent(int owner, int cur)
{
    if (CurrentHealth(owner))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
        Enchant(owner, "ENCHANT_PROTECT_FROM_MAGIC", 0.7);
        Delete(cur);
    }
}

void DetectedSpecificIndex(int curId)
{
    if (!IsMissileUnit(curId))
        return;

    int thingId = GetUnitThingID(curId);

    if (thingId == 526)
        HarpoonEvent(GetOwner(curId), curId);
}

void LoopSearchIndex()
{
    int curId, tempId;
    int *ptr = 0x750710;

    ptr = ptr[0];
    while (ptr != NULLPTR)
    {
        tempId = ptr[11];
        if (curId)
        {
            while (curId < tempId)
            {
                curId += 1;
                DetectedSpecificIndex(curId);
            }
            break;
        }
        curId = tempId;
        break;
    }
    FrameTimer(1, LoopSearchIndex);
}

int CheckOwner(int unit)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsOwnedBy(unit, player[k]))
            return k;
    }
    return -1;
}

int DemonGrp()
{
    int unit, k;

    if (!unit)
    {
        unit = CreateObject("InvisibleLightBlueHigh", 34) + 1;
        for (k = 0 ; k < 10 ; k ++)
        {
            CreateObject("Demon", k + 24);
            SetUnitFlags(unit + k, GetUnitFlags(unit + k) ^ (0x1000000 | 0x40));
            Frozen(unit + k, 1);
        }
    }
    return unit;
}

int GetUnitTeam(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        return GetMemory(ptr + 0x34);
    return 0;
}

void StrRedTeam()
{
	int arr[12], i = 0;
	string name = "ManaBombOrb";
	arr[0] = 529530942; arr[1] = 75498001; arr[2] = 1677725832; arr[3] = 1010336847; arr[4] = 304341506; arr[5] = 287379474; arr[6] = 2030616465; arr[7] = 1212153993; arr[8] = 1111475264; arr[9] = 285352516; 
	arr[10] = 135389636; arr[11] = 2139095040; 
	while (i < 12)
	{
		drawStrRedTeam(arr[i], name);
		i ++;
	}
}

void drawStrRedTeam(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(3);
		pos_y = GetWaypointY(3);
	}
	for (i = 1 ; i > 0 && count < 372 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 3);
		if (count % 34 == 33)
			MoveWaypoint(3, GetWaypointX(3) - 66.000000, GetWaypointY(3) + 2.000000);
		else
			MoveWaypoint(3, GetWaypointX(3) + 2.000000, GetWaypointY(3));
		count ++;
	}
	if (count >= 372)
	{
		count = 0;
		MoveWaypoint(3, pos_x, pos_y);
	}
}

void StrBlueTeam()
{
	int arr[14], i = 0;
	string name = "ManaBombOrb";
	arr[0] = 2080375358; arr[1] = 37001; arr[2] = 2368002; arr[3] = 1225301888; arr[4] = 1042293640; arr[5] = 134812196; arr[6] = 2118420769; arr[7] = 522340421; arr[8] = 1217531904; arr[9] = 612663168; 
	arr[10] = 521151012; arr[11] = 67662306; arr[12] = 2113929216; arr[13] = 3; 
	while(i < 14)
	{
		drawStrBlueTeam(arr[i], name);
		i ++;
	}
}

void drawStrBlueTeam(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(4);
		pos_y = GetWaypointY(4);
	}
	for (i = 1 ; i > 0 && count < 434 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 4);
		if (count % 37 == 36)
			MoveWaypoint(4, GetWaypointX(4) - 72.000000, GetWaypointY(4) + 2.000000);
		else
			MoveWaypoint(4, GetWaypointX(4) + 2.000000, GetWaypointY(4));
		count ++;
	}
	if (count >= 434)
	{
		count = 0;
		MoveWaypoint(4, pos_x, pos_y);
	}
}
