
#include "libs/define.h"
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"
#include "libs/logging.h"
#include "libs/displayInfo.h"
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/queueTimer.h"

int m_genericHash;

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

void onHealthbarProgress(int t)
{
    int owner=GetOwner(t);

    if (MaxHealth(t)){
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,t,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (GetUnit1C(t)!=perc)
            {
                controlHealthbarMotion(t,perc);
                SetUnit1C(t,perc);
            }
            return;
        }
    }
    Delete(t);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));

    UnitNoCollide(bar);
    SetOwner(unit,bar);
    SetUnit1C(bar,0);
    PushTimerQueue(1,bar,onHealthbarProgress);
}

int tileRed(){
    int *ret;

    if (!ret){
        short tile[1058];
        int r=1058;
        while (--r>=0)
            tile[r]=0xF000;
        ret=tile;
    }
    return ret;
}
int tileBlue(){
    int *ret;

    if (!ret){
        short tile[1058];
        int r=1058;
        while (--r>=0)
            tile[r]=0x029E;
        ret=tile;
    }
    return ret;
}

void DoImageProc()
{
    int imgVector =CreateImageVector(256);

    // initializeImageHealthBar(imgVector);
    AppendImageFrame(imgVector, tileRed(), 1);
    AppendImageFrame(imgVector, tileBlue(), 2);
    DoImageDataExchange(imgVector);
}

void CustomMeleeAttackCode(char *bin, int fn)
{
    char code[]={
        0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
        0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
        0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
        0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
    };
    
    int *pStrike = &bin[0xEC];

    pStrike[0] = code;
    int *hack = &bin[0x94];

    hack[15]=fn;
}


void deferredObjectOn(int obj)
{
    if (ToInt(GetObjectX(obj)))
    {
        if (IsObjectOn(obj))
            return;
        ObjectOn(obj);
    }
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 355; arr[19] = 98; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 45; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; arr[35] = 3; 
		arr[36] = 20; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077684469;		ptr[137] = 1077684469;
	int *hpTable = ptr[139];
	hpTable[0] = 355;	hpTable[1] = 355;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int LichBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[17] = 375; arr[19] = 60; arr[21] = 1065353216; arr[23] = 2048; 
		arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; arr[28] = 1106771968; arr[29] = 80; 
		arr[31] = 10; arr[32] = 8; arr[33] = 11; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1342; arr[61] = 46909440; 
	pArr = arr;
	return pArr;
}

void LichSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 375;	hpTable[1] = 375;
	int *uec = ptr[187];
	uec[360] = 2048;		uec[121] = LichBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int HecubahBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 300; arr[19] = 100; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; 
		arr[29] = 50; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void HecubahSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 300;	hpTable[1] = 300;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = HecubahBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

#define GREENGIANT_VECT_X 0
#define GREENGIANT_VECT_Y 1
#define GREENGIANT_OWNER 2
#define GREENGIANT_COUNT 3
#define GREENGIANT_SUB 4
#define GREENGIANT_DAMAGE_HASH 5
#define GREENGIANT_MAX 6

void flyingAcid(int *d){
    int sub=d[GREENGIANT_SUB];

    if (ToInt(GetObjectX(sub))){
        if (--d[GREENGIANT_COUNT]>=0){
            int owner=d[GREENGIANT_OWNER];
            float *pVect=&d[GREENGIANT_VECT_X];
            if (CurrentHealth(owner)&&IsVisibleTo(sub,owner)){
                PushTimerQueue(1,d,flyingAcid);
                MoveObjectVector(sub,pVect[0],pVect[1]);
                CreateObjectById(OBJ_LARGE_BARREL_BREAKING_1,GetObjectX(sub),GetObjectY(sub));
                return;
            }
        }
        HashGet(m_genericHash,sub,NULLPTR,TRUE);
        Delete(sub);
    }
    HashDeleteInstance(d[GREENGIANT_DAMAGE_HASH]);
    FreeSmartMemEx(d);
}

void onGreenGiantMissileCollide(){
    if (!GetTrigger())return;
    if (CurrentHealth(OTHER)){
        int *d;
        if( HashGet(m_genericHash,GetTrigger(),&d,FALSE))
        {
            int owner=d[GREENGIANT_OWNER];
            if (IsAttackedBy(OTHER,owner)){
                if (HashGet(d[GREENGIANT_DAMAGE_HASH],GetCaller(),NULLPTR,FALSE))
                    return;

                Damage(OTHER,owner,63,DAMAGE_TYPE_BLADE);
                Damage(OTHER,owner,10,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
                HashPushback(d[GREENGIANT_DAMAGE_HASH],GetCaller(),TRUE);
            }
        }
    }
}

void startGreenGiantShot(int caster, float *pVect){
    int *d;
    AllocSmartMemEx(GREENGIANT_MAX*4,&d);
    PushTimerQueue(1,d,flyingAcid);
    d[GREENGIANT_OWNER]=GetTrigger();
    d[GREENGIANT_COUNT]=32;
    int unit =DummyUnitCreateById(OBJ_AIRSHIP_CAPTAIN,GetObjectX(caster)+pVect[0],GetObjectY(caster)+pVect[1]);
    SetCallback(unit,9,onGreenGiantMissileCollide);
    HashPushback(m_genericHash,unit,d);
    d[GREENGIANT_SUB]=unit;
    d[GREENGIANT_VECT_X]=pVect[0];
    d[GREENGIANT_VECT_Y]=pVect[1];
    HashCreateInstance(&d[GREENGIANT_DAMAGE_HASH]);
}

void onGreenGiantMeleeAttack(){
    float vect[2];
    if (GetCaller())
    {
        vect[0]=UnitRatioX(OTHER,SELF,13.0);
        vect[1]=UnitRatioY(OTHER,SELF,13.0);
    }else{
        vect[0]=UnitAngleCos(SELF,13.0);
        vect[1]=UnitAngleSin(SELF,13.0);
    }
    startGreenGiantShot(GetTrigger(),vect);
}

int BlackBearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1667329090; arr[1] = 1634026091; arr[2] = 114; arr[17] = 500; arr[19] = 92; 
		arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1069547520; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1114636288; arr[29] = 88; arr[30] = 1128792064; arr[31] = 11; 
		arr[59] = 5542784; arr[60] = 1366; arr[61] = 46902784; 
	pArr = arr;
    CustomMeleeAttackCode(arr, onGreenGiantMeleeAttack);
	return pArr;
}

void BlackBearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076929495;		ptr[137] = 1076929495;
	int *hpTable = ptr[139];
	hpTable[0] = 500;	hpTable[1] = 500;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = BlackBearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int *m_vec;
#include"libs/format.h"
#include"libs/printutil.h"
#include "libs/spellutil.h"
void createGoon()
{
    int unit=CreateObjectById(OBJ_GOON,GetObjectX(SELF),GetObjectY(SELF));

    GoonSubProcess(unit);
    ObjectOff(SELF);
    FrameTimerWithArg(5,GetTrigger(),deferredObjectOn);
    // AttachHealthbar(unit);
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("!atest_log.txt");
    m_vec=CreateVector(1);
    DoImageProc();
    HashCreateInstance(&m_genericHash);
    // CreateRecoveryWhenMapChangedDestructorInstance(0);
    // SetRecoveryDataType2(0x753298,33);

}

void displayTest()
{
    ObjectOff(SELF);
    short wbuff[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("당신의 임무- 집으로 돌아가라!"), wbuff);
    AddShowInfoData(200, 30, wbuff, 0x07F5);
    AddShowInfoData(200, 60, 0x69c9c8, 0xD7E0);
}


