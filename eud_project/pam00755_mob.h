
#include "pam00755_utils.h"
#include "pam00755_reward.h"
#include "libs/bind.h"
#include "libs/spellutil.h"

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 260; arr[19] = 80; arr[21] = 1065353216; arr[23] = 34816; 
		arr[24] = 1066192077; arr[26] = 4; arr[28] = 1106247680; arr[29] = 15; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; arr[35] = 3; 
		arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobGoon(float x, float y)
{
    int m=CreateObjectById(OBJ_GOON, x,y);

    GoonSubProcess(m);
    return m;
}

int FireSpriteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 185; arr[19] = 82; 
		arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 13; arr[56] = 21; 
		arr[58] = 5545472; 
	pArr = arr;
	return pArr;
}

void FireSpriteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075671204;		ptr[137] = 1075671204;
	int *hpTable = ptr[139];
	hpTable[0] = 185;	hpTable[1] = 185;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = FireSpriteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobFireFairy(float x,float y)
{
    int m=CreateObjectById(OBJ_FIRE_SPRITE,x,y);
    FireSpriteSubProcess(m);
    return m;
}

int createMobStoneGiant(float x,float y)
{
    int m=CreateObjectById(OBJ_STONE_GOLEM, x,y);
    SetUnitMaxHealth(m,900);
    return m;
}

int createMobShade(float x,float y)
{
    int m=CreateObjectById(OBJ_SHADE,x,y);
    SetUnitMaxHealth(m,225);
    return m;
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 225; 
		arr[19] = 55; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1071225242; arr[26] = 4; 
		arr[28] = 1104150528; arr[29] = 18; arr[31] = 3; arr[32] = 6; arr[33] = 15; 
		arr[59] = 5542784; arr[60] = 1388; arr[61] = 46915072; 
	pArr = arr;
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobLizard(float x,float y)
{
    int mon=CreateObjectById(OBJ_WEIRDLING_BEAST, x,y);

    WeirdlingBeastSubProcess(mon);
    return mon;
}

int BlackWidowBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 225; arr[18] = 45; 
		arr[19] = 85; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1069547520; arr[26] = 4; 
		arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; arr[31] = 8; arr[32] = 13; 
		arr[33] = 21; arr[34] = 50; arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; 
		arr[38] = 1884516965; arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; 
		arr[59] = 5544896; arr[61] = 45071360; 
	pArr = arr;
	return pArr;
}

void BlackWidowSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = BlackWidowBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobSpider(float x,float y)
{
    int m=CreateObjectById(OBJ_BLACK_WIDOW,x,y);

    BlackWidowSubProcess(m);
    return m;
}

int TrollBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1819243092; arr[1] = 108; arr[17] = 360; arr[19] = 65; arr[21] = 1056964608; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 7; arr[27] = 4; 
		arr[28] = 1108082688; arr[29] = 40; arr[30] = 1109393408; arr[31] = 11; arr[32] = 40; 
		arr[33] = 60; arr[59] = 5542784; arr[60] = 2273; arr[61] = 46912256; 
	pArr = arr;
	return pArr;
}

void TrollSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073322393;		ptr[137] = 1073322393;
	int *hpTable = ptr[139];
	hpTable[0] = 360;	hpTable[1] = 360;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = TrollBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1056964608;
}

int createMobTroll(float x,float y)
{
    int m=CreateObjectById(OBJ_TROLL,x,y);
    TrollSubProcess(m);
    return m;
}

int createEmberDemon(float x,float y)
{
    short ty[]={OBJ_EMBER_DEMON,OBJ_MELEE_DEMON,OBJ_MELEE_DEMON,};
    int m=CreateObjectById(ty[Random(0,sizeof(ty)-1)],x,y);
    SetUnitMaxHealth(m,240);
    return m;
}

int createMobMystic(float x, float y)
{
    int m=CreateObjectById(OBJ_WIZARD,x,y);
    
    SetUnitMaxHealth(m,275);
    SpellUtilSetUnitSpell(m, "SPELL_BLINK", 0);
    SpellUtilSetUnitSpell(m,"SPELL_SHIELD",SPELLFLAG_ON_ESCAPE);
    return m;
}

int createMobBeholder(float x, float y)
{
    int m=CreateObjectById(OBJ_BEHOLDER,x,y);

    SetUnitMaxHealth(m,375);
    SpellUtilSetUnitSpell(m,"SPELL_BLINK",0);
    SpellUtilSetUnitSpell(m,"SPELL_INVISIBILITY",SPELLFLAG_ON_ESCAPE);
    return m;
}

int WizardRedBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[17] = 350; arr[19] = 65; 
		arr[21] = 1065353216; arr[23] = 34848; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; 
		arr[53] = 1128792064; arr[54] = 4; 
	pArr = arr;
	return pArr;
}

void WizardRedSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073322393;		ptr[137] = 1073322393;
	int *hpTable = ptr[139];
	hpTable[0] = 350;	hpTable[1] = 350;
	int *uec = ptr[187];
	uec[360] = 34848;		uec[121] = WizardRedBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobRedWizard(float x,float y)
{
    int m=CreateObjectById(OBJ_WIZARD_RED,x,y);

    WizardRedSubProcess(m);
	SpellUtilSetUnitSpell(m, "SPELL_MAGIC_MISSILE", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(m, "SPELL_SHIELD", SPELLFLAG_ON_DEFFENSIVE);
	SpellUtilSetUnitSpell(m, "SPELL_STUN", SPELLFLAG_ON_DISABLING);
	SpellUtilSetUnitSpell(m, "SPELL_SHOCK", SPELLFLAG_ON_ESCAPE);
	SpellUtilSetUnitSpell(m, "SPELL_FIREBALL", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(m, "SPELL_DEATH_RAY", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(m, "SPELL_BURN", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(m, "SPELL_INVERSION", SPELLFLAG_ON_REACTION);
	SpellUtilSetUnitSpell(m, "SPELL_COUNTERSPELL", SPELLFLAG_ON_REACTION);
	SpellUtilSetSpellDelayOnDeffensive(m, 15,30);
	SpellUtilSetSpellDelayOnDisabling(m,30,60);
	SpellUtilSetSpellDelayOnEscape(m,30,60);
	SpellUtilSetSpellDelayOnOffensive(m,30,60);
	SpellUtilSetSpellDelayOnReaction(m, 3,6);
	SpellUtilSetUnitSpellLevel(m,5);
    return m;
}

int createMobNecromancer(float x,float y)
{
	int nec=CreateObjectById(OBJ_NECROMANCER,x,y);

	SpellUtilSetUnitSpell(nec, "SPELL_MAGIC_MISSILE", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(nec, "SPELL_SHIELD", SPELLFLAG_ON_DEFFENSIVE);
	SpellUtilSetUnitSpell(nec, "SPELL_SLOW", SPELLFLAG_ON_DISABLING);
	SpellUtilSetUnitSpell(nec, "SPELL_SHOCK", SPELLFLAG_ON_ESCAPE);
	SpellUtilSetUnitSpell(nec, "SPELL_FIREBALL", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(nec, "SPELL_LIGHTNING", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(nec, "SPELL_BURN", SPELLFLAG_ON_OFFENSIVE);
	SpellUtilSetUnitSpell(nec, "SPELL_INVERSION", SPELLFLAG_ON_REACTION);
	SpellUtilSetUnitSpell(nec, "SPELL_COUNTERSPELL", SPELLFLAG_ON_REACTION);
	SpellUtilSetSpellDelayOnDeffensive(nec, 15,30);
	SpellUtilSetSpellDelayOnDisabling(nec,30,60);
	SpellUtilSetSpellDelayOnEscape(nec,30,60);
	SpellUtilSetSpellDelayOnOffensive(nec,30,60);
	SpellUtilSetSpellDelayOnReaction(nec, 3,6);
	SetUnitMaxHealth(nec, 325);
	return nec;
}

int createMobDryad(float x, float y)
{
    int m=CreateObjectById(OBJ_WIZARD_GREEN,x,y);

    SetUnitMaxHealth(m,275);
    SpellUtilSetUnitSpell(m,"SPELL_BLINK",0);
    SpellUtilSetUnitSpell(m,"SPELL_INVISIBILITY",SPELLFLAG_ON_ESCAPE);
    return m;
}

int createMobSkeleton(float x, float y)
{
    int m=CreateObjectById(OBJ_SKELETON,x,y);
    SetUnitMaxHealth(m,260);
    return m;
}

int createMobSkullLord(float x,float y)
{
    int m=CreateObjectById(OBJ_SKELETON_LORD,x,y);
    SetUnitMaxHealth(m,325);
    return m;
}

int LichLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 425; arr[19] = 96; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 50; arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; 
		arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void LichLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077432811;		ptr[137] = 1077432811;
	int *hpTable = ptr[139];
	hpTable[0] = 425;	hpTable[1] = 425;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobLichLord(float x,float y)
{
	int m=CreateObjectById(OBJ_LICH_LORD,x,y);

	LichLordSubProcess(m);
	return m;
}

int LichBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[17] = 360; arr[19] = 55; arr[21] = 1065353216; arr[23] = 34816; 
		arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; arr[28] = 1106247680; arr[29] = 30; 
		arr[53] = 1128792064; arr[54] = 4; arr[59] = 5542784; arr[60] = 1342; arr[61] = 46909440; 
	pArr = arr;
	return pArr;
}

void LichSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 360;	hpTable[1] = 360;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMobLich(float x,float y)
{
	int m=CreateObjectById(OBJ_LICH,x,y);

	LichSubProcess(m);
	return m;
}

int createMobBear(float x, float y)
{
    short ty[]={OBJ_BLACK_BEAR,OBJ_BLACK_BEAR,OBJ_BEAR,};    
    short pic = ty[Random(0,sizeof(ty)-1)];
    int m=CreateObjectById(pic,x,y);
    short hp = 325;

    if (pic==OBJ_BEAR)
        hp=420;
    
    SetUnitMaxHealth(m,hp);
    return m;
}

int createMobOgreAxe(float x, float y)
{
    int m=CreateObjectById(OBJ_GRUNT_AXE,x,y);

    SetUnitMaxHealth(m,260);
    return m;
}

int createMobOgreBrute(float x, float y)
{
    int m=CreateObjectById(OBJ_OGRE_BRUTE,x,y);
    SetUnitMaxHealth(m,325);
    return m;
}

int createMobOgreLord(float x,float y)
{
    int m=CreateObjectById(OBJ_OGRE_WARLORD,x,y);

    SetUnitMaxHealth(m,375);
    return m;
}

int createMobScorpion(float x,float y)
{
    int m=CreateObjectById(OBJ_SCORPION,x,y);
    SetUnitMaxHealth(m,260);
    return m;
}

int createMobThief(float x, float y)
{
    short ty[]={OBJ_SWORDSMAN,OBJ_ARCHER,};
    short hp[]={325,130,};
    int pic=Random(0,1);
    int m=CreateObjectById(ty[pic], x,y);
    SetUnitMaxHealth(m,hp[pic]);
	SetUnitStatus(m,GetUnitStatus(m)^MON_STATUS_ALWAYS_RUN);
    return m;
}

int CarnivorousPlantBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 480; 
		arr[19] = 85; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[27] = 5; 
		arr[28] = 1109393408; arr[29] = 100; arr[31] = 8; arr[59] = 5542784; arr[60] = 1371; 
		arr[61] = 46901760; 
	pArr = arr;
	return pArr;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 480;	hpTable[1] = 480;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = CarnivorousPlantBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateMobPlant(float x,float y)
{
    int m=CreateObjectById(OBJ_CARNIVOROUS_PLANT,x,y);

    CarnivorousPlantSubProcess(m);
    AggressionLevel(m,1.0);
    SetUnitScanRange(m,400.0);
    CreatureIdle(m);
    return m;
}

int StrongWizardWhiteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[17] = 295; arr[19] = 60; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; arr[55] = 20; arr[56] = 30; 
	pArr = arr;
	return pArr;
}

void StrongWizardWhiteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 295;	hpTable[1] = 295;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = StrongWizardWhiteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateMobStrongWizard(float x,float y)
{
    int m=CreateObjectById(OBJ_STRONG_WIZARD_WHITE,x,y);

    StrongWizardWhiteSubProcess(m);
    return m;
}

int CreateMobGargoyle(float x, float y)
{
    int m=CreateObjectById(OBJ_EVIL_CHERUB,x,y);

    SetUnitMaxHealth(m,96);
    return m;
}

int ScorpionBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919902547; arr[1] = 1852795248; arr[17] = 250; arr[18] = 75; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1106247680; arr[29] = 90; arr[30] = 1101004800; arr[31] = 3; 
		arr[32] = 3; arr[33] = 6; arr[34] = 25; arr[35] = 2; arr[36] = 16; 
		arr[59] = 5543344; arr[60] = 1373; arr[61] = 46895952; 
	pArr = arr;
	return pArr;
}

void ScorpionSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 250;	hpTable[1] = 250;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = ScorpionBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateScorpionMonster(float x,float y)
{
    int m=CreateObjectById(OBJ_SCORPION,x,y);
    ScorpionSubProcess(m);
    return m;
}

int CreateHorrendousMonster(float x,float y)
{
	int m=CreateObjectById(OBJ_HORRENDOUS,x,y);

	SetUnitMaxHealth(m,480);
	RetreatLevel(m,0.0);
	return m;
}

int createWolfMonster(float x,float y)
{
    short wolfTy[]={OBJ_WOLF,OBJ_WHITE_WOLF,OBJ_BLACK_WOLF,OBJ_WOLF,};
    short hp[]={180,225,260,180,};
    int rd=Random(0,sizeof(wolfTy)-1);
    int m=CreateObjectById(wolfTy[rd],x,y);
    SetUnitMaxHealth(m,hp[rd]);
    return m;
}

int invokeMobCreator(int fn, float x, float y)
{
    return Bind(fn, &fn+4);
}

int getRandomMobCreator()
{
    int *ret, count;

    if (!ret)
    {
        int mons[]={
            createMobGoon,createMobFireFairy,createMobStoneGiant,createMobShade,
            createMobLizard,CreateScorpionMonster,
            createMobTroll,createEmberDemon,createMobMystic,createMobBeholder,
            createMobSkeleton,createMobSkullLord,createMobBear,createMobOgreAxe,
            createMobOgreBrute,createMobOgreLord,createMobScorpion,createMobThief,
            CreateMobPlant,   CreateMobStrongWizard,  CreateMobGargoyle,       
            createMobSpider,createWolfMonster,createMobLich,
        };
        ret=mons;
        count=sizeof(mons);
    }
    return ret[Random(0,count-1)];
}

void deferredPutRandomItem(int sub)
{
    if (ToInt(GetObjectX(sub)))
        CreateRandomItemCommon(sub);
}

void onRandomMonsterDeath()
{
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,x,y);

    PushTimerQueue(25, sub, deferredPutRandomItem);
	DeleteObjectTimer(SELF, 90);
}

void onMonsterPoisoned(int mob)
{
	int poisonLv = IsPoisonedUnit(mob);

    if (poisonLv)
    {
        Damage(SELF, 0, poisonLv, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(mob), GetObjectY(mob)), 9);
    }
}

void onMonsterHit()
{
	onMonsterPoisoned(SELF);
}

void SettingsMonsterCommon(int mob)
{
	SetCallback(mob,5,onRandomMonsterDeath);
    SetCallback(mob,7,onMonsterHit);
    RetreatLevel(mob,0.0);
}

void CreateRandomMonster(int pos)
{
    float x=GetObjectX(pos),y=GetObjectY(pos);
    Delete(pos);
    int m=invokeMobCreator(getRandomMobCreator(), x,y);
    SettingsMonsterCommon(m);
}
