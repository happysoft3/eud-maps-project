
#include "a_player.h"
#include "a_initscan.h"
#include "a_reward.h"
#include "a_mob.h"
#include "a_desc.h"
#include "a_clientside.h"
#include "a_gen.h"
#include "libs/coopteam.h"
#include "libs/indexloop.h"
#include "libs/fixtellstory.h"
#include "libs/absolutelypickup.h"
#include "libs/game_flags.h"
#include "libs/wandpatch.h"
#include "libs/networkRev.h"

void AnotherMissileCollide()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        if (HasEnchant(OTHER, "ENCHANT_FREEZE"))
            return;
        Damage(OTHER, SELF, 180, 16);
        Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
    }
}

int HarpoonBoltCreate(int sOwner, float sX, float sY)
{
    int unit = CreateObjectAt("HarpoonBolt", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2e8, 5483536); //projectile update
        SetUnitCallbackOnCollide(unit,AnotherMissileCollide);
        LookAtObject(unit, sOwner);
        LookWithAngle(unit, GetDirection(unit) + 128);
        SetOwner(sOwner, unit);
    }
    return unit;
}

void FireRingRed(int sCur, int owner)
{
    if (CurrentHealth(owner))
    {
        if (ToInt(DistanceUnitToUnit(owner, sCur)))
        {
            PushObject(HarpoonBoltCreate(owner, GetObjectX(sCur), GetObjectY(sCur)), 22.0, GetObjectX(owner), GetObjectY(owner));
        }
        Delete(sCur);
    }
}

void FireRingBlue(int sCur, int owner){}

void onHarpoonSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
            Damage(OTHER, SELF, 200, DAMAGE_TYPE_PLASMA);
    }
}

void HarpoonCollide()
{
    int owner = GetOwner(SELF);

    if (!IsCaller(owner))
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            MoveObject(OTHER, GetObjectX(OTHER),GetObjectY(OTHER));
            SplashDamageAtEx(owner, GetObjectX(SELF), GetObjectY(SELF), 80.0, onHarpoonSplash);
            PlaySoundAround(SELF, 86);
            DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 18);
            DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(OTHER), GetObjectY(OTHER)), 21);
        }
    }
}

void WarHarpoonDetected(int sCur, int owner)
{
    int hPtr;

    if (CurrentHealth(owner))
    {
        if (HasEnchant(owner, "ENCHANT_CROWN"))
        {
            hPtr = UnitToPtr(sCur);
            if (hPtr)
            {
                SetMemory(hPtr + 0x2b8, ImportUnitCollideFunc());
                SetMemory(hPtr + 0x2fc, HarpoonCollide);
                Enchant(sCur, "ENCHANT_SHOCK", 0.0);
            }
        }
    }
}

void TeleportProgress(int point)
{
    int owner = GetOwner(point), count = GetDirection(point);

    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                if (DistanceUnitToUnit(point, owner) < 23.0)
                {
                    LookWithAngle(point, count - 1);
                    FrameTimerWithArg(1, point, TeleportProgress);
                    break;
                }
            }
            else
            {
                MoveObject(owner, GetObjectX(ToInt(GetObjectZ(point))), GetObjectY(ToInt(GetObjectZ(point))));
                MoveWaypoint(1, GetObjectX(owner), GetObjectY(owner));
                AudioEvent("BlindOff", 1);
                Effect("TELEPORT", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
                Effect("TELEPORT", GetObjectX(point), GetObjectY(point), 0.0, 0.0);
                Effect("SMOKE_BLAST", GetObjectX(point), GetObjectY(point), 0.0, 0.0);
            }
            EnchantOff(owner, "ENCHANT_BURNING");
        }
        Delete(point);
        Delete(point + 1);
        break;
    }
}

void EntryTeleportPortal()
{
    int point;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(OTHER, "ENCHANT_BURNING"))
        {
            Enchant(OTHER, "ENCHANT_BURNING", 4.0);
            point = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));
            CreateObjectAt("VortexSource", GetObjectX(point), GetObjectY(point));
            Raise(point, GetOwner(GetTrigger() + 1));
            SetOwner(OTHER, point);
            LookWithAngle(point, 48);
            Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
            FrameTimerWithArg(1, point, TeleportProgress);
            AudioEvent("LongBellsUp", 1);
            UniPrint(OTHER, "공간이동을 준비 중 입니다. 공간이동을 취소 하려면 캐릭터를 움직이세요");
        }
    }
}

int TeleportSetupDestUnit(int srcWp, int destUnit)
{
    int telp = CreateObject("WeirdlingBeast", srcWp);

    if (IsObjectOn(destUnit))
    {
        SetUnitMaxHealth(CreateObject("InvisibleLightBlueLow", srcWp) - 1, 10);
        Enchant(CreateObject("InvisibleLightBlueLow", srcWp), "ENCHANT_ANCHORED", 0.0);
        SetOwner(destUnit, telp + 1);
        Damage(telp, 0, MaxHealth(telp) + 1, -1);
        SetCallback(telp, 9, EntryTeleportPortal);
    }
    else
    {
        Delete(telp);
        telp = 0;
    }
    return telp;
}

void InitPartTeleport()
{
    QueryDungeonPortal(0,0, TeleportSetupDestUnit(9, CreateObject("MagicEnergy", 10)));
    QueryDungeonPortal(0,1, TeleportSetupDestUnit(11, CreateObject("MagicEnergy", 14)));
    QueryDungeonPortal(0,2, TeleportSetupDestUnit(12, CreateObject("MagicEnergy", 13)));
    TeleportSetupDestUnit(15, CreateObject("MagicEnergy", 16));
    TeleportSetupDestUnit(17, CreateObject("MagicEnergy", 18));
    TeleportSetupDestUnit(19, CreateObject("MagicEnergy", 20));
    TeleportSetupDestUnit(21, CreateObject("MagicEnergy", 22));
    TeleportSetupDestUnit(23, CreateObject("MagicEnergy", 24));
    TeleportSetupDestUnit(25, CreateObject("MagicEnergy", 26));
    TeleportSetupDestUnit(27, CreateObject("MagicEnergy", 28));
    TeleportSetupDestUnit(29, CreateObject("MagicEnergy", 30));
    TeleportSetupDestUnit(31, CreateObject("MagicEnergy", 32));
    TeleportSetupDestUnit(33, CreateObject("MagicEnergy", 34));
    TeleportSetupDestUnit(35, CreateObject("MagicEnergy", 36));
    TeleportSetupDestUnit(37, CreateObject("MagicEnergy", 38));
    TeleportSetupDestUnit(39, CreateObject("MagicEnergy", 40));
    TeleportSetupDestUnit(41, CreateObject("MagicEnergy", 42));
    TeleportSetupDestUnit(43, CreateObject("MagicEnergy", 44));
    TeleportSetupDestUnit(45, CreateObject("MagicEnergy", 46));
    TeleportSetupDestUnit(47, CreateObject("MagicEnergy", 48));
    TeleportSetupDestUnit(49, CreateObject("MagicEnergy", 50));
    TeleportSetupDestUnit(78, CreateObject("InvisibleLightBlueLow", 79));
    TeleportSetupDestUnit(80, CreateObject("InvisibleLightBlueLow", 81));
    TeleportSetupDestUnit(85, CreateObject("MagicEnergy", 86));
    TeleportSetupDestUnit(142, CreateObject("MagicEnergy", 143));
}

void StaffClassCreateMagicMissile(int sOwner)
{
    int sUnit;

    if (CurrentHealth(sOwner))
    {
        sUnit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sOwner) + UnitAngleCos(sOwner, 17.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 17.0));
        SetOwner(sOwner, sUnit);
        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", sUnit, GetObjectX(sUnit) + UnitAngleCos(sOwner, 17.0), GetObjectY(sUnit) + UnitAngleCos(sOwner, 17.0));
        Delete(sUnit + 2);
        Delete(sUnit + 3);
        Delete(sUnit + 4);
        DeleteObjectTimer(sUnit, 120);
    }
}

void StaffClassSmallFonCollide()
{
    int owner;

    if (CurrentHealth(OTHER))
    {
        if (HasEnchant(OTHER, "ENCHANT_FREEZE"))
            return;
        else
        {
            owner = GetOwner(SELF);
            Damage(OTHER, owner, 30, 14);
            Enchant(OTHER, "ENCHANT_FREEZE", 0.3);
        }
    }
}

void StaffClassLargeFonCollide()
{
    int owner;

    if (CurrentHealth(OTHER))
    {
        owner = GetOwner(SELF);
        Damage(OTHER, owner, 200, 14);
        if (CurrentHealth(OTHER))
            Enchant(OTHER, "ENCHANT_AFRAID", 1.0);
        Delete(SELF);
    }
}

int StaffClassSmallFON(int sOwner)
{
    int mis = CreateObjectAt("DeathBallFragment", GetObjectX(sOwner) + UnitAngleCos(sOwner, 19.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 19.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, StaffClassSmallFonCollide);
    SetOwner(sOwner, mis);
    DeleteObjectTimer(mis, 90);
    return mis;
}

int StaffClassLargeFON(int sOwner)
{
    int mis = CreateObjectAt("DeathBall", GetObjectX(sOwner) + UnitAngleCos(sOwner, 19.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 19.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, StaffClassLargeFonCollide);
    SetOwner(sOwner, mis);
    DeleteObjectTimer(mis, 90);
    return mis;
}

void StaffClassMagicMisWand()
{
    if (CurrentHealth(OTHER))
    {
        StaffClassCreateMagicMissile(OTHER);
    }
}

void StaffClassFonWand()
{
    int mis;

    if (CurrentHealth(OTHER))
    {
        if (HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
            return;
        mis = StaffClassSmallFON(OTHER);
        PushObject(mis, 20.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 204);
        Enchant(OTHER, "ENCHANT_ETHEREAL", 0.8);
    }
}

void StaffClassLargeFonWand()
{
    int mis;

    if (CurrentHealth(OTHER))
    {
        if (HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
            return;
        mis = StaffClassLargeFON(OTHER);
        PushObject(mis, 20.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 204);
        Enchant(OTHER, "ENCHANT_ETHEREAL", 1.7);
    }
}

void HealingAnkhCollide()
{
    if (CurrentHealth(OTHER))
    {
        if (CurrentHealth(OTHER) ^ MaxHealth(OTHER))
        {
            RestoreHealth(OTHER, 3);
            Effect("SENTRY_RAY", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(SELF), GetObjectY(SELF));
        }
    }
}

int HealingAnkhPlacing(float sX, float sY)
{
    int aUnit = CreateObjectAt("Ankh", sX, sY);

    SetUnitCallbackOnCollide(aUnit, HealingAnkhCollide);
    return aUnit;
}

void InitMapPickets()
{
    RegistSignMessage(Object("UniSign1"), "감시광선의 시련: 감시광선 패턴을 잘 이해하고 움직임이 재빨라야 한다");
    RegistSignMessage(Object("UniSign2"), "당신은 이곳을 통과할 자격이 있는가?");
    RegistSignMessage(Object("UniSign3"), "괴물들의 시련: 어서와요! 괴물의 숲...");
    RegistSignMessage(Object("UniSign4"), "괴물을 지배할 것인가? 아니면 괴물에게 지배당할 것인가");
    RegistSignMessage(Object("UniSign5"), "화이어볼 함정의 시련: 화이어볼과 바닥구멍의 환상의 하모니! 이것은 나를 미치게 만들지");
    RegistSignMessage(Object("UniSign6"), "너가 그렇게 자신있니? 한번 해봐!");
    RegistSignMessage(Object("UniSign7"), "준비가 되었다면 이 아래로 떨어지세요");
    RegistSignMessage(Object("UniSign8"), "너무 긴장한 나머지 떨고 있는건 아니지...? 그런 것 같아 보이는데!");
    RegistSignMessage(Object("UniSign9"), "이 아래에서 엄청난 화염의 열기가 느껴진다...");
    RegistSignMessage(Object("UniSign10"), "두려워 말라 내가 너와 함께 하리라");
    RegistSignMessage(Object("WallText1"), "필드로 나가려면 이 게이트 안으로 들어가세요");
    RegistSignMessage(Object("WallText2"), "이것은 내부 개발 테스트 목적의 지도입니다");
    RegistSignMessage(Object("SignNorJa"), "*** 초고수가 되기 위한 길 ***");
    RegistSignMessage(Object("DungeonSign1"), "외계생명체 실험실: 4차원 이상의 차원에서 넘어온 외계 생명체들이 모여있는 곳입니다");
    RegistSignMessage(Object("DungeonSign2"), "키러리언 던전: 문 너머로 강력한 괴물들이 서식하고 있으니 안으로 들어갈 땐 한발한발 조심스럽게");
    RegistSignMessage(Object("DungeonSign3"), "네크로멘서 연구실: 문 너머로 강력한 괴물들이 서식하고 있으니 안으로 들어갈 땐 한발한발 조심스럽게");
    RegistSignMessage(Object("DungeonSign4"), "야동배우 대기실: 성깔이 사나우니까 함부로 세우지 마시오");
}

void DungeonMobMakeGenericInit()
{
    int mobMake1 = MobMakeClassInit(GetWaypointX(51), GetWaypointY(51), 10, 30);
    int mobMake2 = MobMakeClassInit(GetWaypointX(52), GetWaypointY(52), 10, 30);
    int mobMake3 = MobMakeClassInit(GetWaypointX(53), GetWaypointY(53), 10, 35);
    int mobMake4 = MobMakeClassInit(GetWaypointX(54), GetWaypointY(54), 10, 40);
    int mobMake5 = MobMakeClassInit(GetWaypointX(55), GetWaypointY(55), 8, 45);

    Raise(mobMake1 + 1, MobClassArcher);
    Raise(mobMake1 + 2, MobClassBomber);
    Raise(mobMake1 + 3, MobClassFrog);
    Raise(mobMake1 + 4, MobClassSwordsman);
    FrameTimerWithArg(10, mobMake1, MobMakeClassStartInitSum);
    
    Raise(mobMake2 + 1, MobClassBlackWolf);
    Raise(mobMake2 + 2, MobClassWolf);
    Raise(mobMake2 + 3, MobClassWhiteWolf);
    Raise(mobMake2 + 4, MobClassGoon);
    FrameTimerWithArg(10, mobMake2, MobMakeClassStartInitSum);

    Raise(mobMake3 + 1, MobClassEmberDemon);
    Raise(mobMake3 + 2, MobClassFireSprite);
    Raise(mobMake3 + 3, MobClassHorrendous);
    Raise(mobMake3 + 4, MobClassHorrendous);
    FrameTimerWithArg(10, mobMake3, MobMakeClassStartInitSum);

    Raise(mobMake4 + 1, MobClassCrazyGirl);
    Raise(mobMake4 + 2, MobClassNecromancer);
    Raise(mobMake4 + 3, MobClassOgreLord);
    Raise(mobMake4 + 4, MobClassSpider);
    FrameTimerWithArg(10, mobMake4, MobMakeClassStartInitSum);

    Raise(mobMake5 + 1, MobClassLichLord);
    Raise(mobMake5 + 2, MobClassMecaGolem);
    Raise(mobMake5 + 3, MobClassPlant);
    Raise(mobMake5 + 4, MobClassOrbHecubah);
    FrameTimerWithArg(10, mobMake5, MobMakeClassStartInitSum);
}

void putMapTextSingle(int location, int dir)
{
    int txt=CreateObjectById(OBJ_RAT,LocationX(location),LocationY(location));
    LookWithAngle(txt,dir);
    Damage(txt,0,999,DAMAGE_TYPE_PLASMA);
}

void initPlaceMobGen()
{
    TeleportSetupDestUnit(120, CreateObject("MagicEnergy", 121));
    TeleportSetupDestUnit(122, CreateObject("MagicEnergy", 123));
    TeleportSetupDestUnit(124, CreateObject("MagicEnergy", 125));
    TeleportSetupDestUnit(126, CreateObject("MagicEnergy", 127));
    TeleportSetupDestUnit(140, CreateObject("MagicEnergy", 141));
    CreateMonsterGenerator(95,OBJ_URCHIN,SPAWN_RATE_LOW,SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(96,OBJ_BAT,SPAWN_RATE_LOW,SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(97,OBJ_URCHIN,SPAWN_RATE_LOW,SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(98,OBJ_SCORPION,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(99,OBJ_SHADE,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(100,OBJ_SPITTING_SPIDER,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(101,OBJ_SKELETON_LORD,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(102,OBJ_GHOST,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(103,OBJ_SKELETON_LORD,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(104,OBJ_GHOST,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);

    CreateMonsterGenerator(105,OBJ_OGRE_BRUTE,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(106,OBJ_OGRE_BRUTE,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(107,OBJ_OGRE_BRUTE,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(108,OBJ_OGRE_WARLORD,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(109,OBJ_OGRE_WARLORD,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(110,OBJ_OGRE_WARLORD,SPAWN_RATE_LOW,SPAWN_LIMIT_LOW);

    CreateMonsterGenerator(111,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(112,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(113,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(114,OBJ_EMBER_DEMON,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(115,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(116,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(117,OBJ_EMBER_DEMON,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(118,OBJ_EMBER_DEMON,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(119,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);

    CreateMonsterGenerator(130,OBJ_HORRENDOUS,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(128,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(129,OBJ_HORRENDOUS,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);

    CreateMonsterGenerator(131,OBJ_HORRENDOUS,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(132,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);

    CreateMonsterGenerator(133,OBJ_DEMON,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(134,OBJ_DEMON,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(135,OBJ_DEMON,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(136,OBJ_HORRENDOUS,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(137,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(138,OBJ_DEMON,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(139,OBJ_MECHANICAL_GOLEM,SPAWN_RATE_LOW,SPAWN_LIMIT_SINGULAR);

    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH, LocationX(147),LocationY(147));
    MobClassJandor(sub);
    MobClassJandor(sub);
    MobClassJandor(sub);
}

void SomeDecorations()
{
    HealingAnkhPlacing(GetWaypointX(5), GetWaypointY(5));
    QueryMainHall(0,CreateObject("VortexSource", 5));
    HookFireballTraps(Object("AnOTHERFireTrap1"),OBJ_DEATH_BALL_FRAGMENT);
    InitMapPickets();
    PlaceSpecialMarket();
    FrameTimer(1, DungeonMobMakeGenericInit);
    FrameTimerWithArg(3, 5, InitPlayerLastPosition);
    putMapTextSingle(90, 0);
    putMapTextSingle(91, 64);
    putMapTextSingle(92, 128);
    putMapTextSingle(93, 192);
    initPlaceMobGen();
}

void commonServerClientProcedure(){
    AppendAllDummyStaffsToWeaponList();
    InitializeClientResource();
}//put resource here

void initGame()
{
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}


void OnInitializeMap(){
    MusicEvent();
    CreateLogFile("a_log.txt");
    
    int last=CreateObjectById(OBJ_RED_POTION,100.0,100.0);
    InitializeMapTranslate();
    FrameTimer(3, SomeDecorations);
    SetHostileCritter();
    InitPartTeleport();
    int initScanHash;
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    StartUnitScan(Object("FirstSearchUnit"),last,initScanHash);
    initGame();
    MakeCoopTeam();
    blockObserverMode();
}
void OnShutdownMap(){
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
    ResetHostileCritter();
}

void TeleportHome()
{
    MoveObject(OTHER, LocationX(86), LocationY(86));
}

void TeleportWandFx(int glow)
{
    if (IsObjectOn(glow))
    {
        int owner = GetOwner(glow);

        if (CurrentHealth(owner))
        {
            if (IsVisibleTo(glow, owner) && IsVisibleTo(owner, glow))
            {
                DeleteObjectTimer(CreateObjectAt("Smoke", GetObjectX(owner), GetObjectY(owner)), 18);
                MoveObject(owner, GetObjectX(glow), GetObjectY(glow));
                Effect("TELEPORT", GetObjectX(glow), GetObjectY(glow), 0.0, 0.0);
                PlaySoundAround(owner, 221);
            }
        }
        Delete(glow);
    }
}

void MagicStaffClassPick()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 10)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        AbsolutelyWeaponPickupAndEquip(OTHER, SELF);
        UniPrint(OTHER, "픽업했습니다");
    }
}

void UseTeleportWand()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 35)
        UniPrint(OTHER, "쿨다운 입니다...");
    else if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        int glowPoint = CreateObjectAt("Moonglow", GetObjectX(OTHER), GetObjectY(OTHER));

        SetOwner(OTHER, glowPoint);
        FrameTimerWithArg(1, glowPoint, TeleportWandFx);
    }
}

int TeleportWandCreate(int location)
{
    int wand = CreateObjectAt("SulphorousFlareWand", LocationX(location), LocationY(location));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2c4, ImportUnitPickupFunc());
    SetMemory(ptr + 0x2e4, MagicStaffClassPick);
    SetMemory(ptr + 0x2dc, ImportUseItemFunc());
    SetMemory(ptr + 0x2fc, UseTeleportWand);
    return wand;
}

int ListClassGetPrevNode(int sCur)
{
    return GetOwner(sCur);
}

int ListClassGetNextNode(int sCur)
{
    return ToInt(GetObjectZ(sCur));
}

void ListClassSetPrevNode(int sCur, int sTarget)
{
    SetOwner(sTarget, sCur);
}

void ListClassSetNextNode(int sCur, int sTarget)
{
    Raise(sCur, sTarget);
}

void ListClassAddList(int sHead, int sNew, int sData)
{
    int tNode = ListClassGetNextNode(sHead);

    if (IsObjectOn(tNode))
        ListClassSetNextNode(sNew, tNode);
    ListClassSetNextNode(sHead, sNew);
    SetUnit1C(sNew, sData);
}

void MobMakeClassUnitDeath()
{
    int sUnit = GetOwner(GetTrigger() + 1);
    int sNew = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));

    ListClassAddList(sUnit, sNew, GetTrigger());
    MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
    if (GetDirection(sUnit) ^ GetDirection(sUnit + 1))
        LookWithAngle(sUnit, GetDirection(sUnit + 1));
}

int MobMakeClassCreate(int sHeadNode, int sUnit, int sFunc)
{
    int unit = CallFunctionWithArgInt(sFunc, sUnit);

    if (unit)
    {
        SetCallback(unit, 5, MobMakeClassUnitDeath);
        RetreatLevel(unit, 0.0);
        AggressionLevel(unit, 1.0);
        SetOwner(sHeadNode, CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)));
    }
    return unit;
}

void MobMakeClassRespawn(int sCurNode)
{
    int dNode, headNode, func;

    if (IsObjectOn(sCurNode))
    {
        dNode = GetUnit1C(sCurNode);
        headNode = GetOwner(dNode + 1);
        if (IsObjectOn(dNode + 1))
        {
            func = ToInt(GetObjectZ(headNode + Random(1, 4)));
            if (func)
                MobMakeClassCreate(headNode, sCurNode, func);
            FrameTimerWithArg(1, ListClassGetNextNode(sCurNode), MobMakeClassRespawn);
            if (MaxHealth(dNode))
                Delete(dNode);
            Delete(dNode + 1);
        }
        Delete(sCurNode);
    }
}

void MobMakeClassLoop(int sUnit)
{
    int count = GetDirection(sUnit);

    if (IsObjectOn(sUnit))
    {
        if (count)
        {
            LookWithAngle(sUnit, count - 1);
        }
        else
        {
            MobMakeClassRespawn(ListClassGetNextNode(sUnit));
            LookWithAngle(sUnit, GetDirection(sUnit + 1));
        }
        SecondTimerWithArg(1, sUnit, MobMakeClassLoop);
    }
}

void MobMakeClassStartInitSum(int sUnit)
{
    int count = GetDirection(sUnit), func = ToInt(GetObjectZ(sUnit + Random(1, 4)));

    if (IsObjectOn(sUnit))
    {
        if (count)
        {
            if (func)
                MobMakeClassCreate(sUnit, sUnit, func);
            LookWithAngle(sUnit, count - 1);
            FrameTimerWithArg(1, sUnit, MobMakeClassStartInitSum);
        }
        else
            MobMakeClassLoop(sUnit);
    }
}

int MobMakeClassInit(float sX, float sY, int sAmount, int sRespTime)
{
    int mobMake = CreateObjectAt("InvisibleLightBlueLow", sX, sY);

    LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", sX, sY), sRespTime);
    CreateObjectAt("InvisibleLightBlueLow", sX, sY);
    CreateObjectAt("InvisibleLightBlueLow", sX, sY);
    CreateObjectAt("InvisibleLightBlueLow", sX, sY);
    LookWithAngle(mobMake, sAmount);
    return mobMake;
}

void PlayerClassAllEnchantment(int sPlrUnit)
{
    Enchant(sPlrUnit, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
    Enchant(sPlrUnit, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
    Enchant(sPlrUnit, "ENCHANT_PROTECT_FROM_POISON", 0.0);
    Enchant(sPlrUnit, "ENCHANT_VAMPIRISM", 0.0);
    Enchant(sPlrUnit, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
}

int SellAllGerm(int sOwner)
{
    int inv = GetLastItem(sOwner), res = 0;
    int tId, cur;

    while (inv)
    {
        cur = inv;
        inv = GetPreviousItem(inv);
        tId = GetUnitThingID(cur);
        while (1)
        {
            if (tId == 2795)
                ChangeGold(sOwner, 10000);
            else if (tId == 2796)
                ChangeGold(sOwner, 5000);
            else if (tId == 2797)
                ChangeGold(sOwner, 1000);
            else
                break;
            Delete(cur);
            res ++;
            break;
        }
    }
    return res;
}

int InvincibleInventory(int sUnit)
{
    int inv = GetLastItem(sUnit), res = 0;

    while (inv)
    {
        if (!GetDirection(inv) && !HasEnchant(inv, "ENCHANT_INVULNERABLE"))
        {
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
            res ++;
        }
        inv = GetPreviousItem(inv);
    }
    return res;
}

void AutoTargetDeathrayWand()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner))
    {
        Damage(OTHER, owner, 250, 17);
        PlaySoundAround(OTHER, 299);
        Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    }
}

void OblivionUseHandler()
{
    int unit;

    if (HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
        return;
    else if (CurrentHealth(OTHER))
    {
        unit = CreateObjectAt("WeirdlingBeast", GetObjectX(OTHER), GetObjectY(OTHER));
        UnitNoCollide(unit);
        SetOwner(OTHER, unit);
        DeleteObjectTimer(unit, 1);
        SetUnitScanRange(unit, 450.0);
        LookWithAngle(unit, GetDirection(OTHER));
        SetCallback(unit, 3, AutoTargetDeathrayWand);
        PlaySoundAround(OTHER, 221);
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Enchant(OTHER, "ENCHANT_ETHEREAL", 0.9);
    }
}

void DelayGiveToOwner(int sTarget)
{
    int sOwner = GetOwner(sTarget);

    if (IsObjectOn(sTarget) && CurrentHealth(sOwner))
        Pickup(sOwner, sTarget);
    else
        Delete(sTarget);
}

int SummonOblivion(int sOwner)
{
    int orb = CreateObjectAt("OblivionOrb", GetObjectX(sOwner), GetObjectY(sOwner));

    SetUnitCallbackOnUseItem(orb,OblivionUseHandler);
    SetItemPropertyAllowAllDrop(orb);
    DisableOblivionItemPickupEvent(orb);
    SetOwner(sOwner, orb);
    FrameTimerWithArg(1, orb, DelayGiveToOwner);
    return orb;
}

void UseTeleportAmulet()
{
    int centerHallUnit;

    QueryMainHall(&centerHallUnit, 0);
    if (CurrentHealth(OTHER) && IsObjectOn(centerHallUnit))
    {
        if (IsVisibleTo(OTHER, centerHallUnit) || IsVisibleTo(centerHallUnit, OTHER))
            UniPrint(OTHER, MapDescTable(7));
        else
        {
            Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            MoveObject(OTHER, GetObjectX(centerHallUnit), GetObjectY(centerHallUnit));
            PlaySoundAround(OTHER, 6);
            DeleteObjectTimer(CreateObjectAt("BlueRain", GetObjectX(OTHER), GetObjectY(OTHER)), 21);
            Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            Delete(SELF);
        }
    }
}

void DescAllEnchant()
{
    TellStoryUnitName("AmuletDrop", "Con05C.scr:OgreTaunt", "올 엔첸트 구입\n4만 골드 필요\n");
    UniPrint(OTHER, MapDescTable(5));
}

void TradeAllEnchant()
{
    int plr = GetPlayerIndex(OTHER);

    if (GetAnswer(SELF) ^ 1) return;
    if (plr < 0) return;

    if (GetGold(OTHER) >= 40000)
    {
        if (PlayerClassCheckFlag(plr,PLAYER_FLAG_ALL_ENCHANT))
            UniPrint(OTHER, MapDescTable(6));
        else
        {
            ChangeGold(OTHER, -40000);
            PlayerClassSetFlag(plr,PLAYER_FLAG_ALL_ENCHANT);
            PlaySoundAround(OTHER, 227);
            DeleteObjectTimer(CreateObjectAt("ManaBombCharge", GetObjectX(OTHER), GetObjectY(OTHER)), 75);
            Effect("WHITE_FLASH", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            PlayerClassAllEnchantment(OTHER);
            UniPrint(OTHER, MapDescTable(8));
        }
    }
    else
        UniPrint(OTHER, MapDescTable(9));
}

void WarHarpoonDesc()
{
    TellStoryUnitName("AmuletDrop", "Con02:BarkeeperDefault", "작살 강화\n6만 골드 필요\n");
    UniPrint(OTHER, MapDescTable(10));
}

void WarHarpoonTrade()
{
    int plr = GetPlayerIndex(OTHER);

    if (GetAnswer(SELF) ^ 1 || plr < 0) return;
    if (GetGold(OTHER) >= 60000)
    {
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ADVANCED_HARPOON))
            UniPrint(OTHER, "거래가 취소되었습니다-- 이미 당신은 이 능력을 가졌습니다");
        else
        {
            ChangeGold(OTHER, -60000);
            PlaySoundAround(OTHER, 226);
            // PlayerSetAdvHarpoon(plr);
            PlayerClassSetFlag(plr, PLAYER_FLAG_ADVANCED_HARPOON);
            Enchant(OTHER, "ENCHANT_CROWN", 0.0);
            Effect("WHITE_FLASH", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
            UniPrint(OTHER, "당신은 향상된 작살능력을 가졌습니다 -60000골드 차감");
        }
    }
}

void DescOblivionStaff()
{
    TellStoryUnitName("AmuletDrop", "War05A.scr:DrunkGreeting", "자동 타겟데스레이 지팡이\n금화 6만골드 필요");
    UniPrint(OTHER, MapDescTable(11));
    Frozen(OTHER, 0);
}

void OblivionStaffTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 30000)
    {
        ChangeGold(OTHER, -30000);
        SummonOblivion(OTHER);
        UniPrint(OTHER, "거래완료: 자동 타겟 데스레이 지팡이는 이제 당신의 것 입니다");
    }
    else
        UniPrint(OTHER, "거래실패: 잔액이 부족합니다-- 자동 타겟 데스레이 지팡이는 6만 골드가 필요합니다");
}

void TeleportAmuletDesc()
{
    TellStoryUnitName("AmuletDrop", "Wiz07.scr:Hecubah02", "공간이동 목걸이 판매원\n1개당 1000골드");
    UniPrint(OTHER, MapDescTable(12));
    Frozen(OTHER, 0);
}

void TeleportAmuletTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 1000)
    {
        ChangeGold(OTHER, -1000);
        int unit = CreateObjectAt("AmuletofManipulation", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(OTHER, unit);
        SetUnitCallbackOnUseItem(unit, UseTeleportAmulet);
        FrameTimerWithArg(1, unit, DelayGiveToOwner);
        UniPrint(OTHER, "거래성공: 공간이동 목걸이 구입에 성공하였습니다 -1000 골드 차감");
    }
    else
        UniPrint(OTHER, "거래실패: 잔액이 부족합니다-- 공간이동 목걸이는 개당 1000 골드 입니다");
}

void InvincibleInvenDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:DiscDiamond", "내구도 무한");
    UniPrint(OTHER, MapDescTable(15));
}

void InvincibleInvenTrade()
{
    int res;

    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 7500)
    {
        res = InvincibleInventory(OTHER);
        if (res)
        {
            ChangeGold(OTHER, -7500);
            char msg[256];

            NoxSprintfString(&msg, "거래완료: 모두 %d개의 아이템이 처리되었습니다", &res,1);
            UniPrint(OTHER, ReadStringAddressEx(msg));
        }
        else
            UniPrint(OTHER, "거래실패: 더 이상 처리할 아이템이 없습니다");
    }
    else
        UniPrint(OTHER, "거래실패: 잔액이 부족합니다-- 인벤토리 파괴불능 설정은 7500골드가 요구됩니다");
}

void SellAllGermDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:DiscDiamond", "보석 감별사");
    UniPrint(OTHER, MapDescTable(13));
}

void SellAllGermTrade()
{
    int res;

    if (GetAnswer(SELF) ^ 1) return;
    res = SellAllGerm(OTHER);
    if (res)
    {
        char msg[256];
        PlaySoundAround(OTHER, 1016);
        NoxSprintfString(msg,"보석 판매완료: 모두 %d개의 아이템이 처리되었습니다", &res,1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
    else
        UniPrint(OTHER, "판매할 보석이 없네요");
}

void TradeTeleportWand()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 80000)
    {
        ChangeGold(OTHER, -80000);
        TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
        TeleportWandCreate(1);
        UniPrint(OTHER, "거래가 완료되었습니다! 구입하신 지팡이는 당신 아래에 생겨났어요!");
    }
    else
        UniPrint(OTHER, "잔액이 부족합니다");
}

void DescTeleportWand()
{
    TellStoryUnitName("AmuletDrop", "thing.db:DiscDiamond", "공간이동 지팡이 구입?");
    UniPrint(OTHER, "공간이동 지팡이를 구입하시겠어요? 금화 8만이 필요합니다");
}

int PosStoreStoneCreate(int sOwner)
{
    int unit = CreateObjectAt("TorchInventory", GetObjectX(sOwner), GetObjectY(sOwner));
    int ptr = GetMemory(0x750710);

    UnitNoCollide(unit);
    SetOwner(sOwner, unit);
    SetMemory(ptr + 0x2fc, UseStoreLastPosition);
    SetMemory(ptr + 0x2dc, ImportUseItemFunc());
    FrameTimerWithArg(1, unit, DelayGiveToOwner);

    return unit;
}

void TradeDemonWand()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 30000)
    {
        ChangeGold(OTHER, -30000);
        int sd = CreateObjectAt("DemonsBreathWand", GetObjectX(OTHER), GetObjectY(OTHER));
        int ptr = GetMemory(0x750710);

        SetMemory(GetMemory(ptr + 736) + 108, 0xc8c8);
        Enchant(sd, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
        UniPrint(OTHER, "거래가 완료되었습니다! 구입하신 지팡이는 당신 아래에 있습니다!");
        UniPrint(OTHER, "내리실 때 두고 가시는 물건이 없는 지 확인해 주시기 바랍니다");
    }
    else
        UniPrint(OTHER, "잔액이 부족합니다");
}

void DescDemonWand()
{
    TellStoryUnitName("AmuletDrop", "Con07:SaveDunMir07", "화염 지팡이 구입?");
    UniPrint(OTHER, "용의 숨결 지팡이를 구입하시겠어요? 금화 3만원이 필요해요!");
}

int PosMoveStoneCreate(int sOwner)
{
    int unit = CreateObjectAt("AnkhTradable", GetObjectX(sOwner), GetObjectY(sOwner));
    int ptr = GetMemory(0x750710);

    UnitNoCollide(unit);
    SetOwner(sOwner, unit);
    SetMemory(ptr + 0x2c4, 5190112);
    SetMemory(ptr + 0x2fc, UseMoveLastPosition);
    SetMemory(ptr + 0x2dc, ImportUseItemFunc());
    FrameTimerWithArg(1, unit, DelayGiveToOwner);

    return unit;
}

void PosStoreStoneDesc()
{
    TellStoryUnitName("AmuletDrop", "Con02a:OfficialSEG2", "위치 저장및 로드 도구 판매 1호점\n");
    UniPrint(OTHER, "위치 저장 및 로드의 촛불을 구입하시겠어요? 이 작업은 1000 골드가 필요해요 (1000골드 당 1개 셋트(저장 및 이동) 지급)");
    UniPrint(OTHER, "위치 저장 및 로드의 촛불: 촛불 사용 시 현재 위치 저장, 앵크 사용 시 저장된 위치로 이동");
    Frozen(OTHER, 0);
}

void PosStoreStoneTrade()
{
    int unit, ptr;

    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 1000)
    {
        ChangeGold(OTHER, -1000);
        DeleteObjectTimer(PosStoreStoneCreate(OTHER), 600);
        DeleteObjectTimer(PosMoveStoneCreate(OTHER), 600);
        UniPrint(OTHER, "거래성공: 위치 저장및 로드의 촛불 구입에 성공하였습니다 -1000 골드 차감");
    }
    else
        UniPrint(OTHER, "거래실패: 잔액이 부족합니다-- 위치 저장및 로드의 촛불는 개당 1000 골드 입니다");
}

void PlaceSpecialMarket()
{
    SetDialog(DummyUnitCreateById(OBJ_URCHIN_SHAMAN, LocationX(56), LocationY(56)), "YESNO", DescAllEnchant, TradeAllEnchant);
    SetDialog(DummyUnitCreateById(OBJ_HECUBAH_WITH_ORB,LocationX(57),LocationY(57)), "YESNO", DescOblivionStaff, OblivionStaffTrade);
    SetDialog(DummyUnitCreateById(OBJ_WIZARD, LocationX(61),LocationY(61)), "YESNO", TeleportAmuletDesc, TeleportAmuletTrade);
    SetDialog(DummyUnitCreateById(OBJ_OGRE_BRUTE,LocationX(62),LocationY(62)), "YESNO", SellAllGermDesc, SellAllGermTrade);
    SetDialog(DummyUnitCreateById(OBJ_WIZARD_GREEN,LocationX(63),LocationY(63)), "YESNO", PosStoreStoneDesc, PosStoreStoneTrade);
    SetDialog(DummyUnitCreateById(OBJ_HORRENDOUS,LocationX(83),LocationY(83) ), "YESNO", WarHarpoonDesc, WarHarpoonTrade);

    SetDialog(DummyUnitCreateById(OBJ_WIZARD_WHITE, LocationX(88),LocationY(88) ), "YESNO", DescTeleportWand, TradeTeleportWand);
    SetDialog(DummyUnitCreateById(OBJ_DEMON,LocationX(89),LocationY(89)), "YESNO", DescDemonWand, TradeDemonWand);
    SetDialog(DummyUnitCreateById(OBJ_SWORDSMAN,LocationX(94),LocationY(94) ), "YESNO", InvincibleInvenDesc, InvincibleInvenTrade);
}

void Gauntlet1Clear()
{
    ObjectOff(SELF);
    PlaySoundAround(SELF, 306);
    UniPrint(OTHER, "이 구간을 클리어 하셨습니다");
    int dun;
    QueryDungeonPortal(&dun,0,0);
    Delete(dun);
    // FrameTimerWithArg(24, 9, StrGauntletClear);
    putMapTextSingle(9,32);
    Effect("WHITE_FLASH", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
}

void Gauntlet2Clear()
{
    ObjectOff(SELF);
    PlaySoundAround(SELF, 306);
    UniPrint(OTHER, "이 구간을 클리어 하셨습니다");
    int dun;
    QueryDungeonPortal(&dun,1,0);
    Delete(dun);
    // FrameTimerWithArg(24, 59, StrGauntletClear);
    putMapTextSingle(59,32);
    Effect("WHITE_FLASH", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
}

void Gauntlet3Clear()
{
    ObjectOff(SELF);
    PlaySoundAround(SELF, 306);
    UniPrint(OTHER, "이 구간을 클리어 하셨습니다");
    int dun;
    QueryDungeonPortal(&dun,2,0);
    Delete(dun);
    // FrameTimerWithArg(24, 58, StrGauntletClear);
    putMapTextSingle(58,32);
    Effect("WHITE_FLASH", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
}

void InitPlayerLastPosition(int sLocation)
{
    float xProfile = GetWaypointX(sLocation), yProfile = GetWaypointY(sLocation);
    int unit = CreateObject("InvisibleLightBlueLow", sLocation), i;

    Delete(unit);
    for (i = 0 ; i < 10 ; i ++)
        DrawMagicIconAt(xProfile + MathSine(i * 36 + 90, 43.0), yProfile + MathSine(i * 36, 43.0));
    QueryPlayerLastPos(0,0,++unit);
}

void UseMoveLastPosition()
{
    int plr = GetPlayerIndex(OTHER);
    int posUnit;

    QueryPlayerLastPos(&posUnit, plr, 0);
    if (CurrentHealth(OTHER) && plr + 1)
    {
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER, GetObjectX(posUnit), GetObjectY(posUnit));
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        PlaySoundAround(OTHER, 228);
        UniPrint(OTHER, "저장된 위치로 이동되었습니다");
        Delete(SELF);
    }
}

void UseStoreLastPosition()
{
    int plr = GetPlayerIndex(OTHER);
    int posUnit;

    QueryPlayerLastPos(&posUnit, plr, 0);
    if (CurrentHealth(OTHER) && plr + 1)
    {
        MoveObject(posUnit, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 39);
        GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
        UniPrint(OTHER, "현재 위치가 저장되었습니다");
        Delete(SELF);
    }
}

void HellHere()
{
    if (MaxHealth(OTHER))
    {
        if (CurrentHealth(OTHER))
        {
            if (HasEnchant(OTHER, "ENCHANT_INVULNERABLE"))
                EnchantOff(OTHER, "ENCHANT_INVULNERABLE");
        }
        else
            PlaySoundAround(SELF, SOUND_HumanMaleDie);
    }
}

void SingleMobMakeClassMobDead()
{
    PlaySoundAround(SELF, SOUND_HumanMaleDie);
    CreateRandomItemCommon(CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH, GetObjectX(SELF), GetObjectY(SELF)));
    DeleteObjectTimer(CreateObjectAt("Puff", GetObjectX(SELF), GetObjectY(SELF)), 21);
    DeleteObjectTimer(SELF, 150);
}

int SingleMobMakeClassMakeMob(int sUnit, int sFunc)
{
    int mob = Bind(sFunc, &sUnit);

    RetreatLevel(mob, 0.0);
    AggressionLevel(mob, 1.0);
    SetCallback(mob, 5, SingleMobMakeClassMobDead);
    return mob;
}

void SingleMobMakeClassSpawn(int sMobMake)
{
    int count = GetDirection(sMobMake), func;

    while (IsObjectOn(sMobMake))
    {
        if (count)
        {
            func = ToInt(GetObjectZ(sMobMake + Random(0, 3)));
            if (func)
                SingleMobMakeClassMakeMob(sMobMake, func);
            LookWithAngle(sMobMake, count - 1);
            FrameTimerWithArg(1, sMobMake, SingleMobMakeClassSpawn);
            break;
        }
        Delete(sMobMake);
        Delete(sMobMake + 1);
        Delete(sMobMake + 2);
        Delete(sMobMake + 3);
        break;
    }
}

int SingleMobMakeClassCreate(int sLocation, int sAmount)
{
    int mobMake = CreateObject("InvisibleLightBlueLow", sLocation);

    LookWithAngle(CreateObject("InvisibleLightBlueLow", sLocation) - 1, sAmount);
    CreateObject("InvisibleLightBlueLow", sLocation);
    CreateObject("InvisibleLightBlueLow", sLocation);
    return mobMake;
}

void MobMakeClassRedWizStartSummon(int sLocation)
{
    int mobMake = SingleMobMakeClassCreate(sLocation, 64);

    Raise(mobMake, MobClassRedWiz);
    Raise(mobMake + 1, MobClassRedWiz);
    Raise(mobMake + 2, MobClassRedWiz);
    Raise(mobMake + 3, MobClassRedWiz);
    FrameTimerWithArg(10, mobMake, SingleMobMakeClassSpawn);
}

void ClearGWalls()
{
    int count;

    count ++;
    ObjectOff(SELF);
    if (count ^ 4) return;
    WallGroupOpen(0);
    FrameTimerWithArg(80, 82, MobMakeClassRedWizStartSummon);
    UniPrintToAll(MapDescTable(1));
}

void Dungeon2MobMakeInit()
{
    int mobMake1 = SingleMobMakeClassCreate(74, 45);
    int mobMake2 = SingleMobMakeClassCreate(75, 30);
    int mobMake3 = SingleMobMakeClassCreate(76, 30);
    int mobMake4 = SingleMobMakeClassCreate(77, 40);

    WriteLog("Dungeon2MobMakeInit");

    Raise(mobMake1 + 0, MobClassGiantLeech);
    Raise(mobMake1 + 1, MobClassBeast);
    Raise(mobMake1 + 2, MobClassGoon);
    Raise(mobMake1 + 3, MobClassGiantLeech);
    FrameTimerWithArg(1, mobMake1, SingleMobMakeClassSpawn);

    Raise(mobMake2 + 0, MobClassDemon);
    Raise(mobMake2 + 1, MobClassDemon);
    Raise(mobMake2 + 2, MobClassFireSprite);
    Raise(mobMake2 + 3, MobClassEmberDemon);
    FrameTimerWithArg(10, mobMake2, SingleMobMakeClassSpawn);

    Raise(mobMake3 + 0, MobClassHecubah);
    Raise(mobMake3 + 1, MobClassHecubah);
    Raise(mobMake3 + 2, MobClassOrbHecubah);
    Raise(mobMake3 + 3, MobClassHecubah);
    FrameTimerWithArg(20, mobMake3, SingleMobMakeClassSpawn);

    Raise(mobMake4 + 0, MobClassJandor);
    Raise(mobMake4 + 1, MobClassJandor);
    Raise(mobMake4 + 2, MobClassJandor);
    Raise(mobMake4 + 3, MobClassJandor);
    FrameTimerWithArg(30, mobMake4, SingleMobMakeClassSpawn);
    WriteLog("Dungeon2MobMakeInit");
}

void OpenDungeon2()
{
    ObjectOff(SELF);
    FrameTimer(1, Dungeon2MobMakeInit);
    PlaySoundAround(SELF, 903);
    WriteLog("OpenDungeon2");
    UniPrintToAll(PlayerIngameNick(OTHER) + MapDescTable(2));
    WriteLog("OpenDungeon2");
}

void OnInitIndexLoop(int hash)
{
    HashPushback(hash,OBJ_HARPOON_BOLT,WarHarpoonDetected);
    // HashPushback(hash,OBJ_BLUE_FLAME_CLEANSE,FireRingBlue);
    HashPushback(hash,OBJ_FLAME_CLEANSE,FireRingRed);
    HashPushback(hash,OBJ_MEDIUM_FLAME_CLEANSE,FireRingRed);
    HashPushback(hash,OBJ_SMALL_FLAME_CLEANSE,FireRingRed);
}

void invokeSummonMonsterProcedure(int f, int mob)
{
    Bind(f, &mob);
}

void OnMonsterGeneratorSummon(int gen, int mob){
    int fn;

    if (HashGet(MonHash(), GetUnitThingID(mob), &fn, FALSE))
        invokeSummonMonsterProcedure(fn, mob);
    
    LookAtObject(mob, gen);
    LookWithAngle(mob, GetDirection(mob)+128);
    AggressionLevel(mob, 1.0);
    RetreatLevel(mob, 0.0);
}

void OnPlayerEnteredMap(int pInfo){
    int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
    {
        pUnit = GetMemory(ptr+0x2c);
        int pIndex = GetMemory(pInfo+0x810);
        PlayerClassOnInit   (pIndex, pUnit);
        ShowQuestIntroOne(pUnit, 237, "CopyrightScreen", "Wolchat.c:PleaseWait");
    }
    
    if (pInfo==0x653a7c)
    {
        commonServerClientProcedure();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
