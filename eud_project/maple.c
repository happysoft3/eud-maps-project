
#include "libs\define.h"
#include "libs\printutil.h"
#include "libs\network.h"
#include "libs/winapi.h"
#include "libs\waypoint.h"
#include "libs\unitstruct.h"
#include "libs\sound_define.h"
#include "libs\fxeffect.h"
#include "libs\buff.h"
#include "libs\array.h"
#include "libs\drawtext.h"
#include "libs\clientside.h"
#include "libs\imageutil.h"
#include "libs/playerinfo.h"
#include "libs/cmdline.h"
#include "libs/format.h"

int LastUnitID = 41;
int m_plrMover[20], plr_fall[10];
#define SENTRY_COLOR_BLUE 0x037F

float INIT_HIGH = 25.0;

int m_player[10];
int m_playerFlag[10];
int m_plrAction[10];

int *m_bottomCheck;
int *m_pImgVector;
int m_clientNetId;

int *m_clientKeyState;
int m_testMark = 0xdeadface;

#define KEY_UP_SHIFT 1
#define KEY_LEFT_SHIFT 2
#define KEY_RIGHT_SHIFT 4


#define PLAYER_DEATH_FLAG 2

void UserMapBgmData()
{ }

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_playerFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_playerFlag[plr] ^= flags;
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

int CreateBottomChecker(int plr, int pUnit)
{
    int botChecker=DummyUnitCreateAt("WeirdlingBeast", GetObjectX(pUnit), GetObjectY(pUnit) + 25.0);

    SetCallback(botChecker, 9, BottomCollide);
    LookWithAngle(botChecker, plr);
    DeleteObjectTimer(botChecker, 1);
    return botChecker;
}

void InitPlayerBottomChecker()
{
    int botChecker[10];
    int u;

    for (u=sizeof(botChecker)-1 ; u >= 0 ; Nop(--u))
    {
        botChecker[u] = DummyUnitCreateAt("WeirdlingBeast", LocationX(4+u), LocationY(4+u));
        SetCallback(botChecker[u], 9, BottomCollide);
        LookWithAngle(botChecker[u], u);
    }

    m_bottomCheck = &botChecker;
}

int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, LastUnitID);
        }
    }
    LastUnitID ++;

    return unitMover;
}

static void initialServerPatch()
{
    int oldProtect;

    WinApiVirtualProtect(0x51cf84, 1024, 0x40, &oldProtect);
    char *p = 0x51cf84;

    p[23]=0x7a;
    SetMemory(0x51d0c8, 0x513f10);
    WinApiVirtualProtect(0x51cf84, 1024, oldProtect, NULLPTR);
    //46 8B 06 8B 4E 04 89 08 83 C6 09 E9 24 8F 00 00

    WinApiVirtualProtect(0x513f10, 256, 0x40, &oldProtect);

    char servcode[]={0x46, 0x8B, 0x06, 0x8B, 0x4E, 0x04, 0x89, 0x08, 0x83, 0xC6, 0x08, 0xE9, 0x24, 0x8F, 0x00, 0x00};

    NoxByteMemCopy(&servcode, 0x513f10, sizeof(servcode));
    WinApiVirtualProtect(0x513f10, 256, oldProtect, NULLPTR);

    SetMemory(0x5c31ec, 0x513f30);
}

void MapInitialize()
{
    MusicEvent();
    m_clientKeyState = 0x751000;
    InitPlayerBottomChecker();
    initialServerPatch();
    // FrameTimer(1, StrFinish);
    // FrameTimer(1, ModifyWarriorAbilityCooldown);
    FrameTimer(20, DrawingMap);
    FrameTimer(1, PutImage);

    RegistSignMessage(Object("mappic1"), "인내의 숲에 들어가려면 좌측 비콘을 밟으세요. 그리고 마우스를 손에서 놓으세요~");
    RegistSignMessage(Object("mappic2"), "조작방법: 왼쪽화살표- 좌로 이동, 오른쪽화살표- 우로 이동, 위쪽화살표- 점프");
    RegistSignMessage(Object("mappic3"), "인내의 숲에 입장하려면 파란 불꽃위에 서세요!");

    PlaceMainTeleport(82, 81);
    PlaceMainTeleport(83, 81);
}

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    m_clientNetId = GetMemory(0x979720);
    FrameTimer(30, ClientClassTimerLoop);
    CommonProcess();
}

void MapExit()
{
    MusicEvent();
}

void MainGameLoop()
{
    GreenBallTrap();
    ObjectOn(Object("MainGameSwitch"));
    PlayerMoverSetting();
    LaserTrap();
    FrameTimer(10, PlayerClassOnLoop);
    FrameTimer(30, StartMovingBlockTrap);
    FrameTimer(30, PutFlames);
    FrameTimer(45, FinalDeathBallTrap);
    FrameTimer(60, serverInputLoop);
}

void PlayerMoverSetting()
{
    string name = "Maiden", fall_name = "InvisibleLightBlueHigh";
    int k;

    for (k = 9 ; k >= 0 ; Nop(k --))
    {
        m_plrMover[k] = CreateObject(name, k + 4);
        plr_fall[k] = CreateObject(fall_name, 14);
        m_plrMover[k + 10] = CreateObject(name, k + 15);
        LookWithAngle(m_plrMover[k], k);
        Frozen(m_plrMover[k], 1);
        Frozen(m_plrMover[k + 10], 1);
        Raise(plr_fall[k], INIT_HIGH);
        TeleportLocationVector(14, 23.0, 0.0);
    }
}

void BottomCollide()
{
    int plr = GetDirection(SELF);

    if (HasClass(OTHER, "IMMOBILE"))
    {
        int owner = GetOwner(SELF);

        if (!UnitCheckEnchant(owner, GetLShift(ENCHANT_ETHEREAL)))
        {
            Raise(plr_fall[plr], INIT_HIGH);
            Enchant(owner, "ENCHANT_DETECTING", 0.1);
            Enchant(owner, "ENCHANT_CROWN", 0.1);
            MoveObject(m_plrMover[plr + 10], LocationX(plr + 15), LocationY(plr + 15));
            // Delete(SELF);
        }
    }
}

void DrawingMap()
{
    UniPrintToAll("맵을 그리는 중 입니다... 잠시만 기다려주십시오.");
    DrawBottomTexture(671.0, 1558.0, 4153.0);
    DrawBottomTexture(1675.0, 1795.0, 4059.0);
    DrawBottomTexture(1886.0, 1999.0, 3946.0);
    DrawBottomTexture(1677.0, 1793.0, 3828.0);
    DrawBottomTexture(1377.0, 1587.0, 3715.0);
    DrawBottomTexture(1013.0, 1177.0, 3715.0);
    DrawBottomTexture(688.0, 895.0, 3625.0);
    DrawBottomTexture(966.0, 1182.0, 3485.0);
    DrawBottomTexture(1377.0, 1592.0, 3485.0);
    DrawBottomTexture(1377.0, 1592.0, 3485.0);

    FrameTimer(1, DrawingMap2);
    FrameTimer(2, DrawingMap3);
    FrameTimer(2, MovingFlameLoop);
    FrameTimer(2, SentryRayTrap1);
    FrameTimer(3, DrawingMap4);
    FrameTimer(4, ShurikenTrapLoop);
    // FrameTimer(10, StrKeyInfo);
    FrameTimer(10, MovingBlockLoop2);
    FrameTimer(10, FXExitPortal);
    FrameTimer(20, MainGameLoop);
}

void DrawingMap2()
{
    int unit = CreateObject("CarnivorousPlant", 37);
    CreateObject("LargeFlame", 37);
    SetCallback(unit, 9, TouchedFire);
    Damage(unit, 0, 999, -1);
    DrawBottomTexture(1794.0, 2140.0, 3485.0);
    DrawBottomTexture(2161.0, 2416.0, 3669.0);
    DrawBottomTexture(2569.0, 2743.0, 4034.0);
    DrawBottomTexture(2893.0, 3060.0, 4034.0);
    DrawBottomTexture(3221.0, 3380.0, 4034.0);
    DrawBottomTexture(3543.0, 3705.0, 4034.0);
    DrawBottomTexture(3841.0, 3956.0, 3918.0);
    DrawBottomTexture(3539.0, 3700.0, 3804.0);
    DrawBottomTexture(3218.0, 3380.0, 3804.0);
    DrawBottomTexture(2897.0, 3060.0, 3804.0);
    DrawBottomTexture(2707.0, 2829.0, 3713.0);
    DrawBottomTexture(2917.0, 3082.0, 3603.0);
}

void DrawingMap3()
{
    DrawBottomTexture(3241.0, 3364.0, 3599.0);
    DrawBottomTexture(3518.0, 3680.0, 3599.0);
    DrawBottomTexture(3768.0, 3937.0, 3486.0);
    DrawBottomTexture(3515.0, 3681.0, 3368.0);
    DrawBottomTexture(3196.0, 3361.0, 3368.0);
    DrawBottomTexture(2871.0, 3041.0, 3368.0);
    DrawBottomTexture(2617.0, 2784.0, 3254.0);
    DrawBottomTexture(2341.0, 2508.0, 3160.0);
    DrawBottomTexture(2018.0, 2190.0, 3160.0);
    DrawBottomTexture(1789.0, 1874.0, 3208.0);
    DrawBottomTexture(1508.0, 1594.0, 3208.0);
    DrawBottomTexture(1235.0, 1316.0, 3208.0);
    DrawBottomTexture(1694.0, 1729.0, 3116.0);
    DrawBottomTexture(1372.0, 1452.0, 3116.0);
}

void DrawingMap4()
{
    DrawBottomTexture(1005.0, 1089.0, 3208.0);
    DrawBottomTexture(1005.0, 1089.0, 2980.0);
    DrawBottomTexture(1005.0, 1089.0, 2750.0);
    DrawBottomTexture(843.0, 923.0, 3094.0);
    DrawBottomTexture(843.0, 923.0, 2864.0);
    DrawBottomTexture(1214.0, 1704.0, 2818.0);
    DrawBottomTexture(1811.0, 1938.0, 2726.0);
    DrawBottomTexture(2042.0, 2485.0, 2816.0);
    DrawBottomTexture(2620.0, 2696.0, 2747.0);
    DrawBottomTexture(2800.0, 2882.0, 2700.0);
    DrawBottomTexture(3057.0, 3726.0, 2957.0);
}

void MovingFlameLoop()
{
    int ptr;

    if (ptr)
    {
        if (!GetDirection(ptr))
        {
            if (GetObjectY(ptr) <= 4082.0)
            {
                MoveObject(ptr, GetObjectX(ptr), GetObjectY(ptr) + 3.0);
                MoveObject(ptr + 1, GetObjectX(ptr + 1), GetObjectY(ptr + 1) + 3.0);
            }
            else
                LookWithAngle(ptr, 1);
        }
        else
        {
            if (GetObjectY(ptr) >= GetWaypointY(38))
            {
                MoveObject(ptr, GetObjectX(ptr), GetObjectY(ptr) - 3.0);
                MoveObject(ptr + 1, GetObjectX(ptr + 1), GetObjectY(ptr + 1) - 3.0);
            }
            else
                LookWithAngle(ptr, 0);
        }
    }
    else
    {
        ptr = CreateObject("SpikeBlock", 38);
        CreateObject("SpikeBlock", 39);
        LookWithAngle(ptr, 0);
        Frozen(ptr, 1);
        Frozen(ptr + 1, 1);
    }
    FrameTimer(1, MovingFlameLoop);
}

void VictoryEvent()
{
    int k;

    ObjectOff(Object("MainGameSwitch"));

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(m_player[k]))
        {
            MoveObject(m_player[k], LocationX(46), LocationY(46));
            m_player[k] = 0;
        }
    }
    Effect("WHITE_FLASH", LocationX(46), LocationY(46), 0.0, 0.0);
    AudioEvent("BigGong", 46);
    AudioEvent("LongBellsDown", 46);
    FrameTimer(1, StrVictory);
    FrameTimer(10, YouWin);
}

void YouWin()
{
    UniPrintToAll("당신이 이겼습니다, 인내의 숲을 모두 통과하셨습니다_!!");
}

void RegistPlayer()
{
    int user = GetCaller();

    if (MaxHealth(user) == 150)
        PlayerClassOnEntry(user);
    else
    {
        MoveObject(user, LocationX(80), LocationY(80));
        UniPrint(user, "전사로 도전하세요~");
    }
}

int CheckPlayerWithId(int pUnit)
{
    int rep=-1;

    while ((++rep)<sizeof(m_player))
    {
        if (m_player[rep]^pUnit)
            continue;
        
        return rep;
    }
    return -1;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    m_player[plr]=pUnit;
    m_playerFlag[plr]=1;
    m_clientKeyState[GetPlayerIndex(pUnit)]=0;

    ChangeGold(pUnit, -GetGold(pUnit));
    return plr;
}

void PlayerClassOnEntry(int pUnit)
{
    while (TRUE)
    {
        if (!CurrentHealth(pUnit))
            break;
        int plr = CheckPlayerWithId(pUnit), u;

        for (u = sizeof(m_player)-1; u >= 0 && plr < 0 ; Nop(--u))
        {
            if (!MaxHealth(m_player[u]))
            {
                plr = PlayerClassOnInit(u, pUnit);
                break;
            }
        }
        if (plr >= 0)
        {
            PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(pUnit);
        break;
    }
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(2), LocationY(2));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void PlayerClassOnJoin(int plr)
{
    int pUnit = m_player[plr];

    m_plrAction[plr]=0;
    RemoveInventory(pUnit);
    EnchantOff(pUnit, "ENCHANT_INVULNERABLE");
    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);
    MoveObject(pUnit, LocationX(3), LocationY(3));
    LookWithAngle(pUnit, 0);
    Effect("TELEPORT", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    PlaySoundAround(pUnit, SOUND_PlayerExit);
    UniChatMessage(pUnit, "인내의 숲 입장!", 120);
    SetOwner(pUnit, m_bottomCheck[plr]);
}

void PlayerClassOnDeath(int plr)
{ }

void PlayerGetXYPos(int pUnit, int *pXypos)
{
    if (!IsPlayerUnit(pUnit))
        return;

    int pIndex = GetPlayerIndex(pUnit);

    if (pIndex < 0)
        return;

    int *userdb=0x62f1d8 + (pIndex*0x12dc);

    pXypos[0] = FloatToInt(ToFloat(userdb[908]));
    pXypos[1] = FloatToInt(ToFloat(userdb[909]));
}

void ServerTaskUserOutput(int plr, int pUnit)
{
    int pIndex = GetPlayerIndex(pUnit);
    int mover = m_plrMover[plr], baselocation=plr+4;

    if (m_clientKeyState[pIndex] & KEY_UP_SHIFT)    // if (m_clientPressUp[pIndex])
        JumpEvent(plr, pUnit);
    if (m_clientKeyState[pIndex] & KEY_LEFT_SHIFT) // else if (m_clientPressLeft[pIndex])
    {
        MoveObject(mover, GetObjectX(pUnit) + 18.0, GetObjectY(pUnit));     //원래 +
        LookWithAngle(pUnit, 128);
    }
    else if (m_clientKeyState[pIndex] & KEY_RIGHT_SHIFT) // else if (m_clientPressRight[pIndex])
    {
        MoveObject(mover, GetObjectX(pUnit) - 18.0, GetObjectY(pUnit)); //원래 -
        LookWithAngle(pUnit, 0);
    }
    else
    {
        if (ToInt(Distance(GetObjectX(mover), GetObjectY(mover), LocationX(baselocation), LocationY(baselocation))))
            MoveObject(mover, LocationX(baselocation), LocationY(baselocation));
    }
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    int botCheck = m_bottomCheck[plr];

    MoveObject(botCheck, GetObjectX(pUnit), GetObjectY(pUnit)+25.0);
    ServerTaskUserOutput(plr, pUnit);
    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_DETECTING) | GetLShift(ENCHANT_ETHEREAL)))
    {
        MoveObject(m_plrMover[plr + 10], GetObjectX(pUnit), GetObjectY(pUnit) - GetObjectZ(plr_fall[plr]));
        if (GetObjectZ(plr_fall[plr]) > 1.0)
            Raise(plr_fall[plr], GetObjectZ(plr_fall[plr]) - 1.0);
    }

    if (GetObjectX(pUnit) <= 637.0 || GetObjectY(pUnit) >= 4218.0)
        Damage(pUnit, 0, 255, 14);
}

void PlayerClassOnShutdown(int plr)
{
    m_player[plr] = 0;
    m_playerFlag[plr] = 0;
}

void PlayerClassOnLoop()
{
    int u;

    for (u = sizeof(m_player)-1 ; u >= 0 ; Nop(--u))
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[u]))
            {
                if (GetUnitFlags(m_player[u]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[u]))
                {
                    PlayerClassOnAlive(u, m_player[u]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(u, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassOnDeath(u);
                        PlayerClassSetFlag(u, PLAYER_DEATH_FLAG);
                    }
                    break;
                }
            }
            if (m_playerFlag[u])
                PlayerClassOnShutdown(u);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void RemoveInventory(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void DrawBottomTexture(float x_left, float x_right, float high)
{
    string name = "Bench3Immobile";
    int k;

    TeleportLocation(25, x_left, high + 10.0);
    while (LocationX(25) < x_right)
    {
        CreateObjectAtEx(name, LocationX(25), LocationY(25), NULLPTR);
        TeleportLocationVector(25, 21.0, 0.0);
    }
}

void PlayerOnJump(int plr)
{
    while (CurrentHealth(m_player[plr]))
    {
        if (GetObjectZ(plr_fall[plr]) < 25.0)
        {
            MoveObject(m_plrMover[plr + 10], GetObjectX(m_player[plr]), GetObjectY(m_player[plr]) + GetObjectZ(plr_fall[plr]));
            Raise(plr_fall[plr], GetObjectZ(plr_fall[plr]) + 1.0);
            FrameTimerWithArg(1, plr, PlayerOnJump);
            break;
        }

        EnchantOff(m_player[plr], "ENCHANT_ETHEREAL");
        break;
    }
}

void JumpEvent(int plr, int pUnit)
{
    // if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    //     return;

    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_DETECTING)))
        return;
    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_ETHEREAL)))
    {
        // Enchant(pUnit, EnchantList(ENCHANT_PROTECT_FROM_MAGIC), 2.0);
        Enchant(pUnit, EnchantList(ENCHANT_ETHEREAL), 0.0);
        Raise(plr_fall[plr], 8.0);
        Effect("COUNTERSPELL_EXPLOSION", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
        PlaySoundAround(pUnit, SOUND_BallBounce);
        FrameTimerWithArg(1, plr, PlayerOnJump);
    }
}

void TouchedFire()
{
    if (IsPlayerUnit(OTHER) && CurrentHealth(OTHER))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Damage(OTHER, SELF, 255, 14);
    }
}

void SentryRayTrap1()
{
    int trap[4];

    if (trap[0])
    {
        if (!IsObjectOn(trap[0]))
        {
            ObjectOn(trap[0]);
            ObjectOn(trap[1]);
            ObjectOn(trap[2]);
            ObjectOn(trap[3]);
            FrameTimer(15, SentryRayTrap1);
        }
        else
        {
            ObjectOff(trap[0]);
            ObjectOff(trap[1]);
            ObjectOff(trap[2]);
            ObjectOff(trap[3]);
            SecondTimer(3, SentryRayTrap1);
        }
    }
    else
    {
        trap[0] = CreateObject("SentryGlobeMovable", 40);
        // MoveWaypoint(40, GetWaypointX(40), GetWaypointY(40) + 23.0);
        TeleportLocationVector(40, 0.0, 23.0);
        trap[1] = CreateObject("SentryGlobeMovable", 40);
        trap[2] = Object("LaserLeft1");
        trap[3] = Object("LaserLeft2");
        ObjectOff(trap[0]);
        ObjectOff(trap[1]);
        ObjectOff(trap[2]);
        ObjectOff(trap[3]);
        FrameTimer(1, SentryRayTrap1);
    }
}

void GreenBallTrap()
{
    int ptr = CreateObject("DeathBall", 42);
    // DisableMagicalProperties(GetMemory(0x750710));
    SetUnitSubclass(ptr, GetUnitSubclass(ptr) ^ 1);
    CreateObject("DeathBall", 43);
    // DisableMagicalProperties(GetMemory(0x750710));
    SetUnitSubclass(ptr+1, GetUnitSubclass(ptr+1) ^ 1);

    DeleteObjectTimer(ptr, 60);
    DeleteObjectTimer(ptr + 1, 60);

    PushObject(ptr, -10.0, LocationX(43), LocationY(43));
    PushObject(ptr + 1, -10.0, LocationX(42), LocationY(42));
    AudioEvent("ForceOfNatureRelease", 42);
    AudioEvent("ForceOfNatureRelease", 43);
    SecondTimer(4, GreenBallTrap);
}

void StartMovingBlockTrap()
{
    int ptr;

    ptr = CreateObject("SpikeBlock", 44);
    CreateObject("SpikeBlock", 45);
    Frozen(ptr, 1);
    Frozen(ptr + 1, 1);
    LookWithAngle(ptr + 1, 1);
    FrameTimerWithArg(1, ptr, BlockRotation);
    FrameTimerWithArg(1, ptr + 1, BlockRotation);
}

void BlockRotation(int block)
{
    if (!GetDirection(block))
    {
        if (GetObjectY(block) <= GetWaypointY(45))
            MoveObject(block, GetObjectX(block), GetObjectY(block) + 3.0);
        else if (GetObjectX(block) >= GetWaypointX(45))
            MoveObject(block, GetObjectX(block) - 3.0, GetObjectY(block));
        else
            LookWithAngle(block, 1);
    }
    else
    {
        if (GetObjectY(block) >= GetWaypointY(44))
            MoveObject(block, GetObjectX(block), GetObjectY(block) - 3.0);
        else if (GetObjectX(block) <= GetWaypointX(44))
            MoveObject(block, GetObjectX(block) + 3.0, GetObjectY(block));
        else
            LookWithAngle(block, 0);
    }
    FrameTimerWithArg(1, block, BlockRotation);
}

void ShurikenTrapLoop()
{
    int arr[10];
    int flag[2];
    int k;
    string name = "OgreShuriken";

    if (flag[0] | flag[1])
    {
        TeleportLocation(51, LocationX(flag[0] + 47), LocationY(flag[0] + 47));
        TeleportLocation(52, LocationX(flag[0] + 48), LocationY(flag[0] + 48));
        for (k = 4 ; k >= 0 ; Nop(k --))
        {
            arr[k * 2] = CreateObject(name, 51);
            arr[k * 2 + 1] = CreateObject(name, 52);
            PushObjectTo(arr[k * 2], 0.0, -15.0);
            PushObjectTo(arr[k * 2 + 1], 0.0, 15.0);
            TeleportLocationVector(51, 23.0, 0.0);
            TeleportLocationVector(52, 23.0, 0.0);
        }
        flag[0] = flag[0] ^ flag[1];
        flag[1] = flag[1] ^ flag[0];
        flag[0] = flag[0] ^ flag[1];
    }
    else
        flag[1] = 2;
    SecondTimer(2, ShurikenTrapLoop);
}

void LaserTrap()
{
    int ptr[8];
    int flag;
    int k;

    if (ptr[0])
    {
        for (k = 7 ; k >= 0 ; k --)
            ObjectToggle(ptr[k]);
    }
    else
    {
        ptr[0] = Object("LaserBeamBase");
        for (k = 1 ; k < 8 ; k ++)
        {
            ptr[k] = ptr[0] + (k * 2);
            if (k >= 4)
                ObjectOn(ptr[k]);
        }
    }
    SecondTimer(4, LaserTrap);
}

void MovingBlockLoop2()
{
    int block;

    if (block)
    {
        if (!GetDirection(block))
        {
            if (GetObjectX(block) <= GetWaypointX(54))
                MoveObject(block, GetObjectX(block) + 3.0, GetObjectY(block));
            else
                LookWithAngle(block, 1);
        }
        else
        {
            if (GetObjectX(block) >= GetWaypointX(53))
                MoveObject(block, GetObjectX(block) - 3.0, GetObjectY(block));
            else
                LookWithAngle(block, 0);
        }
    }
    else
    {
        block = CreateObject("SpikeBlock", 53);
        LookWithAngle(block, 0);
        Frozen(block, 1);
    }
    FrameTimer(1, MovingBlockLoop2);
}

void FinalDeathBallTrap()
{
    int unit = CreateObject("DeathBall", 60);
    
    SetUnitSubclass(unit, GetUnitSubclass(unit) ^ 1);
    CreateObject("DeathBall", 62);
    SetUnitSubclass(unit + 1, GetUnitSubclass(unit + 1) ^ 1);
    FrameTimerWithArg(120, CreateMoverFix(unit, 59, 30.0), RemoveDeathballMover);
    FrameTimerWithArg(120, CreateMoverFix(unit + 1, 61, 70.0), RemoveDeathballMover);
    FrameTimer(125, FinalDeathBallTrap);
}

void RemoveDeathballMover(int unit)
{
    Delete(unit);
}

void PutFlames()
{
    int k;
    int unit = CreateObject("InvisibleLightBlueHigh", 53);
    int ptr;

    for (k = 0 ; k < 5 ; Nop(k ++))
    {
        CreateObjectAtEx("CarnivorousPlant", LocationX(65), LocationY(65), NULLPTR);
        Frozen(CreateObject("LargeFist", 65), TRUE);
        Damage(unit + (k * 2) + 1, 0, 999, -1);
        SetCallback(unit + (k * 2) + 1, 9, TouchedFire);
        TeleportLocationVector(65, 46.0, 0.0);
    }

    ptr = CreateObject("CarnivorousPlant", 66);
    Frozen(CreateObject("LargeFist", 66), 1);
    Damage(unit + (k * 2) + 1, 0, 999, -1);
    SetCallback(unit + (k * 2) + 1, 9, TouchedFire);
    CreateMoverFix(ptr, 68, 20.0);
    CreateMoverFix(ptr + 1, 68, 20.0);
}

void FXExitPortal()
{
    TeleportLocation(56, LocationX(55), LocationY(55) - 25.0);
    int magic[3];

    magic[0] = CreateObject("Magic", 56);
    Frozen(magic[0], 1);
    SetUnitSubclass(magic[0], GetUnitSubclass(magic[0]) ^ 1);
    // DisableMagicalProperties(GetMemory(0x750710));
    // MoveWaypoint(56, GetWaypointX(56), GetWaypointY(56) - 20.0);
    TeleportLocationVector(56, 0.0, -20.0);
    magic[1] = CreateObject("Magic", 56);
    // Frozen(CreateObject("Magic", 56), 1);
    Frozen(magic[1], TRUE);
    SetUnitSubclass(magic[1], GetUnitSubclass(magic[1]) ^ 1);
    // DisableMagicalProperties(GetMemory(0x750710));
    // MoveWaypoint(56, GetWaypointX(56), GetWaypointY(56) - 20.0);
    TeleportLocationVector(56, 0.0, -20.0);
    magic[2] = CreateObject("Magic", 56);
    // Frozen(CreateObject("Magic", 56), 1);
    Frozen(magic[2], TRUE);
    SetUnitSubclass(magic[2], GetUnitSubclass(magic[2]) ^ 1);
    // Frozen(CreateObject("Magic", 56), 1);
    // DisableMagicalProperties(GetMemory(0x750710));
}

void StrFinish()
{
	int arr[13];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 252844414; arr[1] = 1225556257; arr[2] = 172003472; arr[3] = 541608009; arr[4] = 1679828106; arr[5] = 1885489447; arr[6] = 1092915239; arr[7] = 340267537; arr[8] = 604571905; arr[9] = 75793556; 
	arr[10] = 1111641124; arr[11] = 505811460; arr[12] = 66; 
	while(i < 13)
	{
		drawStrFinish(arr[i++], name);
	}
}

void drawStrFinish(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(57);
		pos_y = GetWaypointY(57);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 57), 1);
		if (count % 38 == 37)
			TeleportLocationVector(57, -111.000000, 3.000000);
		else
			TeleportLocationVector(57, 3.000000, 0.0);
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		TeleportLocation(57, pos_x, pos_y);
	}
}

void StrVictory()
{
	int arr[13];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
		drawStrVictory(arr[i++], name);
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(46);
		pos_y = GetWaypointY(46);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 46), 1);
		if (count % 38 == 37)
			TeleportLocationVector(46, - 111.000000, 3.000000);
		else
			TeleportLocationVector(46, 3.000000, 0.0);
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		TeleportLocation(46, pos_x, pos_y);
	}
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; Nop(k --))
    {
        if (IsCaller(m_player[k]))
            return k;
    }
    return -1;
}

int PlayerFlag(int num)
{
    int flag[10];

    if (num < 0)
    {
        flag[0] = Object("flagBase");
        int k;
        for (k = 1 ; k < 10 ; k ++)
            flag[k] = flag[0] + (k * 2);
        return 0;
    }
    return flag[num];
}

void ClientMouseGetXY(int *pPosX, int *pPosY)
{
    int *offset = 0x6990b0;

    pPosX[0] = offset[0];
    pPosY[0] = offset[1];
}

void ClientMouseSetXY(int posX, int posY)
{
    int *offset = 0x6990b0;

    if (posX!=-1)
        offset[0]=posX;
    if (posY!=-1)
        offset[1]=posY;
}

int KeyboardIOCheckKey(int keyId)
{
    int *keyTable = 0x6950b0;
    int *selkey = keyTable+(keyId*8);
    int key = (selkey[0]>>8)&0xff;

    return key==2;
}

#define SEND_LEFT_SHIFT 1
#define SEND_RIGHT_SHIFT 2

void serverInputLoop()
{
    int up = KeyboardIOCheckKey(0xc8);
    int left = KeyboardIOCheckKey(0xcb)<<SEND_LEFT_SHIFT;
    int right = KeyboardIOCheckKey(0xcd)<<SEND_RIGHT_SHIFT;
    int prevState, state = up|left|right;

    if (prevState!=state)
    {
        prevState=state;
        m_clientKeyState[31]=state;
    }
    FrameTimer(3, serverInputLoop);
}

static void ClientKeyHandlerRemix()
{
    int up = KeyboardIOCheckKey(0xc8);
    int left=KeyboardIOCheckKey(0xcb)<<SEND_LEFT_SHIFT;
    int right=KeyboardIOCheckKey(0xcd)<<SEND_RIGHT_SHIFT;
    int prevState, state = up|left|right;

    if (prevState!=state)
    {
        prevState=state;
        // int params[]={m_clientNetId+'0', prevState+'0'};
        // char buffer[32];
        // NoxSprintfString(&buffer, "sysop #23#%c%c", &params, sizeof(params));
        // CmdLine(ReadStringAddressEx(&buffer), FALSE);

        char testPacket[]={0x3d, 0x00+(m_clientNetId*4),0x10,0x75,0x00, 0, 0,0,0};
        int *p = &testPacket + 1;

        p[1] = prevState;
        NetClientSendRaw(31, 0, &testPacket, sizeof(testPacket));
    }
}

void ClientClassTimerLoop()
{
    ClientKeyHandlerRemix();
    FrameTimer(1, ClientClassTimerLoop);
}

void CommonProcess()
{
    int drawtextFunction = DrawTextBuildTextDrawFunction();

    // DrawTextSetupBottmText(2558, drawtextFunction, 0xf812, "인내의 숲-noxgameremaster");
    // DrawTextSetupBottmText(2559, drawtextFunction, 0x7f6, "조작방법: 왼쪽화살표-왼쪽이동");
    // DrawTextSetupBottmText(2561, drawtextFunction, 0x7f6, "오른쪽화살표-오른쪽이동");
    // DrawTextSetupBottmText(2562, drawtextFunction, 0x7f6, "위쪽화살표-점프");
    DrawTextSetupBottmText(2560, drawtextFunction, 0xf812, "F.I.N.I.S.H.");

    m_pImgVector = CreateImageVector(32);

    InitializeImageHandlerProcedure(m_pImgVector);
    AddImageFromResource(m_pImgVector, GetScrCodeField(KeySettingImage) + 4, 131930, 0);
    ExtractMapBgm("..\\ggrudi.mp3", UserMapBgmData);
    short *pSentryColor=0x7170D8;

    pSentryColor[0]=SENTRY_COLOR_BLUE;
    pSentryColor[1]=SENTRY_COLOR_BLUE;
}

void DrawImageAtEx(float x, float y, int thingId, int *pDest)
{
    int *ptr = UnitToPtr(CreateObjectAt("AirshipBasketShadow", x, y));

    ptr[1] = thingId;
    if (pDest)
        pDest[0] = ptr[11];
}

void KeySettingImage()
{ }

void HideAppearImage(int *pImage)
{
    int loc[] = {71, 76}, tof;
    int u;

    for (u = 0 ; u < 4 ; Nop(++u))
    {
        MoveObject(pImage[u], LocationX(loc[tof]+u), LocationY(loc[tof]+u));
    }
    FrameTimerWithArg(15, pImage, HideAppearImage);
    tof^=TRUE;
}

void PutImage()
{
    int image[4];

    // DrawImageAtEx(LocationX(71), LocationY(71), 2558, ArrayRefN(&image, 0));
    // DrawImageAtEx(LocationX(72), LocationY(72), 2559, ArrayRefN(&image, 1));
    // DrawImageAtEx(LocationX(73), LocationY(73), 2561, ArrayRefN(&image, 2));
    // DrawImageAtEx(LocationX(74), LocationY(74), 2562, ArrayRefN(&image, 3));

    DrawImageAtEx(LocationX(57), LocationY(57), 2560, 0);
    // FrameTimerWithArg(3, &image, HideAppearImage);
}

void PlayerFastJoin()
{
    if (!CurrentHealth(OTHER))
        return;
    
    Enchant(OTHER, EnchantList(ENCHANT_ANTI_MAGIC), 0.0);
    int plr = CheckPlayer();

    if (plr < 0)
    {
        MoveObject(OTHER, LocationX(69), LocationY(69));
        return;
    }
    MoveObject(OTHER, LocationX(70), LocationY(70));
    UniPrint(OTHER, "패스트 조인 되었습니다");
}

void TelpoToGoMain()
{
    if (!CurrentHealth(OTHER))
        return;

    if (!IsPlayerUnit(OTHER))
        return;

    int pUnit = GetCaller();

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
        {
            CommonProcess();
        }
        FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);  //일단 빼놓음
    }
    int destUnit = GetTrigger()+1;

    MoveObject(pUnit, GetObjectX(destUnit), GetObjectY(destUnit));
}

void PlaceMainTeleport(int srcLocation, int destLocation)
{
    int dum = CreateObjectAt("WeirdlingBeast", LocationX(srcLocation), LocationY(srcLocation));

    CreateObjectAtEx("InvisibleLightBlueLow", LocationX(destLocation), LocationY(destLocation), 0);
    ObjectOff(dum);
    Damage(dum, 0, MaxHealth(dum)+1, 14);
    SetCallback(dum, 9, TelpoToGoMain);

    int fxUnit;
    CreateObjectAtEx("TeleportWake", GetObjectX(dum), GetObjectY(dum), &fxUnit);
    Frozen(fxUnit, TRUE);
    UnitNoCollide(fxUnit);

    int blueFire;

    CreateObjectAtEx("MediumFlame", GetObjectX(fxUnit), GetObjectY(fxUnit), &blueFire);
    Enchant(blueFire, EnchantList(ENCHANT_FREEZE), 0.0);
    UnitNoCollide(blueFire);
}