

#include "a_main.h"

void MapExit()
{
    OnShutdownMap();
}

void MapInitialize()
{
    OnInitializeMap();
}

int m_player[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex]; }

static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user; }

int m_playerFlags[MAX_PLAYER_COUNT];

static int GetPlayerFlags(int pIndex) //virtual
{ return m_playerFlags[pIndex]; }

static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_playerFlags[pIndex]=flags; }

// int GetPlayerItem(int pIndex) //virtual
// { }

// void SetPlayerItem(int pIndex, int item) //virtual
// { }


static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    OnInitIndexLoop(pInstance);
}

int m_mainHallUnit;

static void QueryMainHall(int *get, int set){
    if (get)
        get[0]=m_mainHallUnit;
    if (set)
        m_mainHallUnit=set;
}//virtual

int m_plrLastPosUnit;
static void QueryPlayerLastPos(int *get, int n, int set){
    if (get) get[0]=m_plrLastPosUnit+n;
    if (set) m_plrLastPosUnit=set;
}//virtual

static void NetworkUtilClientMain(){
    commonServerClientProcedure();
}

int m_dungeonPortal[12];

static void QueryDungeonPortal(int *get, int n, int set){
    if (get)
    {
        get[0]=m_dungeonPortal[n];
        return;
    }
    m_dungeonPortal[n]=set;
}//virtual

static void OnGeneratorSummoned(int gen, int mob){
    OnMonsterGeneratorSummon(gen,mob);
} //virtual
static void OnPlayerEntryMap(int pInfo)
{ OnPlayerEnteredMap(pInfo);}
