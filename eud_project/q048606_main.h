
#include "q048606_resource.h"
#include "q048606_utils.h"
#include "q048606_initscan.h"
#include "q048606_reward.h"
#include "libs/coopteam.h"
#include"libs/recovery.h"
#include "libs/playerupdate.h"
#include"libs/game_flags.h"
void initMapReadable()
{
    string comm1="아이템 내구도 무한 비콘입니다";
    string comm2="무기 내구도 무한 설정 비콘입니다";
    string comm3="갑옷 내구도 무한 설정 비콘입니다";
    RegistSignMessage(Object("mpic1"), comm1);
    RegistSignMessage(Object("mpic6"), comm1);
    RegistSignMessage(Object("mpic2"), comm2);
    RegistSignMessage(Object("mpic8"), comm2);
    RegistSignMessage(Object("mpic3"), comm3);
    RegistSignMessage(Object("mpic7"), comm3);
    RegistSignMessage(Object("mpic4"), "전사 스킬을 배우는 곳입니다. 클릭해 보세요");
    RegistSignMessage(Object("mpic5"), " - 블러드 패스티발 - 제작. panic");
    RegistSignMessage(Object("mpic9"), "이곳은 엉터리 상점구역 입니다");
    RegistSignMessage(Object("read1"), "맵의 이름= 블러드 패스티벌 - 목적. 괴물과 대치하여 살아남기. 앞에있는 파란 불빛을 따라가시오");
}

void SetHostileCritter(){
	SetRecoveryDataType2(0x833e64, 0x540);		//CarnivorousPlant
	SetRecoveryDataType2(0x833e70, 0x540);		//FishBig
	SetRecoveryDataType2(0x833e74, 0x540);		//FishSmall
	SetRecoveryDataType2(0x833e78, 0x540);		//Rat
	SetRecoveryDataType2(0x833e7c, 0x540);		//GreenFrog
}
void initGame()
{
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}
void DoInitResources(){
    InitializeResource();
}
void OnInitializeMap(){
    MusicEvent();
    CreateLogFile("q048606.txt");
    MakeCoopTeam();
    SetHostileCritter();
int initScanHash,lastUnit=GetMasterUnit();
initMapReadable();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    // HashPushback(initScanHash, OBJ_HECUBAH_MARKER, placeSoulGate);
    // HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, CreateRandomMonster);
    StartUnitScan(Object("firstscan"), lastUnit, initScanHash);initGame();}
void OnShutdownMap(){
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}
