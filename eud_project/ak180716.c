
#include "ak180716_main.h"

void MapInitialize(){OnInitializeMap();}
void MapExit(){OnShutdownMap();}
int m_player[MAX_PLAYER_COUNT];
static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex]; }
static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user; }
int m_playerFlags[MAX_PLAYER_COUNT];
static int GetPlayerFlags(int pIndex) //virtual
{return m_playerFlags[pIndex]; }
static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_playerFlags[pIndex]=flags;}
static void NetworkUtilClientMain(){
    InitializeClientOnly();
    }
static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}
int m_baseorb[9];
static void QueryBaseOrb(int n, int *get, int set){
    if (get){
        get[0]=m_baseorb[n];
        return;
    }
    m_baseorb[n]=set;
} //virtual

static void OnPlayerEntryMap(int pInfo)
{
    ConfirmPlayerEntryMap(pInfo);
}