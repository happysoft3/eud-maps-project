
#include "subway_gvar.h"
#include "libs/gui_window.h"
#include "libs/format.h"

void QueryDialogCtx(int *get, int *set){
    int ctx;
    if (get)get[0]=ctx;
    if (set)ctx=set[0];
}

void initDialog()
{
    int *wndptr=0x6e6a58,ctx;
    GUIFindChild(wndptr[0], 3901, &ctx);
    QueryDialogCtx(NULLPTR, &ctx);
}

void InitFillPopupMessageArray(int *pPopupMessage)
{
    pPopupMessage[GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM]="당신의 인벤토리를 무적화 하시겠어요? 필요한 금액은 7,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_AWARD_FAST_MOVE]="짧은거리를 빠르게 이동하는 \"순보\" 기술을 배우실래요? 가격은 20,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_AWARD_BERSERKER]="버저커차지를 활성화 하려면 가격은 20,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_WOLF_RUN_SWORD]="늑데돌진 서드를 구입하려면 예를 누르세요, 검을 휘두를 시, 늑데가 돌진하며 피해를 줍니다. 60,000골드 ";
    pPopupMessage[GUI_DIALOG_MESSAGE_MONEY_EXCHANGER]="당신이 가진 여러 보석들을 모두 금화로 환전해 드립니다, 예를 누르시면 보석을 금화로 환전해드릴게요";
    pPopupMessage[GUI_DIALOG_MESSAGE_THUNDER_SWORD]="피카추의 백만볼트 서드를 구입하려면 예를 누르세요, 검을 휘두르면 전방에 백만볼트를 발사합니다,  60,000골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_BACKSTEP_HAMMER]="백스탭 망치- 주변에 200의 피해를 주면서 짧은 거리를 순간적으로 후퇴합니다, 가격은 60,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_ALL_ENCHANTMENT]="올 엔첸트- 유용한 버프를 항상 지속합니다, 가격은 45,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_FLY_SWATTER]="이빨오니- 당신을 계속 따라다니며 적군을 물리친다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 60,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_BEAR_GRYLLS]="베어그릴스- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 61,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_PESTICIDE]="살충제- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 60,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_BLUE_GIANT]="파랑거인- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 66,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_STELS_SHIP]="스텔스십- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 77,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_ICE_CRYSTAL_AXE]="아이스 엑스- 크리스털 하나를 날린다. 폭발 시 주변을 얼리며 피해를 준다, 가격은 64,000골드입니다";
}

void initPopupMsg(int *pPopupMessage)
{
    initDialog();
    InitFillPopupMessageArray(pPopupMessage);
}

void queryPopMessage(int **get){
    int msg[32];

    if (get) get[0]=msg;
}

void InitializePopupMessages(){
    int once;
    if (once) return;
    once=TRUE;
    int *s;
    queryPopMessage(&s);
    initPopupMsg(s);
}

