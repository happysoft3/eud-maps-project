
#include "subway_initscan.h"
#include "xtest511_resource.h"
#include "xtest511_mons.h"
#include "xtest511_reward.h"
#include "libs/coopteam.h"
#include "libs/game_flags.h"
#include "libs/format.h"
#include "libs/fixtellstory.h"
#include "libs/absolutelypickup.h"
#include "libs/playerupdate.h"
#include "libs/wallutil.h"
#include "libs/reaction.h"
#include "libs/networkRev.h"

void BuyNewSkill(){}//virtual
void PlayerPreserveHandler(){}


int AbilityButton(string name, int wp, int idx, int flag, int pay)
{
    int unit = CreateObject(name, wp);

    Raise(CreateObject("InvisibleLightBlueHigh", wp), ToFloat(pay));
    LookWithAngle(unit + 1, flag);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    LookWithAngle(unit, idx);
    ObjectOff(unit);
    Frozen(unit, 1);
    return unit;
}

void SlideSecretCase(int sUnit)
{
    int durate = GetDirection(sUnit);

    if (durate)
    {
        MoveObject(sUnit, GetObjectX(sUnit) + 1.0, GetObjectY(sUnit) - 1.0);
        LookWithAngle(sUnit, durate - 1);
        FrameTimerWithArg(1, sUnit, SlideSecretCase);
    }
    else
    {
        WallOpen(WallUtilGetWallAtObjectPosition(sUnit + 1));
        Delete(sUnit + 1);
        FrameTimer(1, InitCandleRoom);
    }
}

void MikyDeadTouched()
{
    int ptr;

    if (CurrentHealth(OTHER) && MaxHealth(SELF))
    {
        ptr = UnitToPtr(OTHER);
        if (ptr)
        {
            if (GetMemory(ptr + 8) & 0x04)
            {
                FrameTimerWithArg(3, GetTrigger() + 1, SlideSecretCase);
                Delete(SELF);
                UniPrint(OTHER, "뒤쪽에서 뭔가가 움직이고 있습니다");
            }
        }
    }
}
int MagicMissileCreate(int sUnit, float gap)
{
    int mis = CreateObjectAt("Magic", GetObjectX(sUnit) + UnitAngleCos(sUnit, gap), GetObjectY(sUnit) + UnitAngleSin(sUnit, gap));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, MagicMissileCollide);
    SetOwner(sUnit, mis);
    return mis;
}

void MagicMissileWandClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 30)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        PushObject(MagicMissileCreate(OTHER, 16.0), 24.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 480);
    }
}

void onRunningZombieSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 85, DAMAGE_TYPE_PLASMA);
        }
    }
}

void RunningZombieLifeTime(int sUnit)
{
    int durate = GetDirection(sUnit), owner;

    while (IsObjectOn(sUnit))
    {
        if (MaxHealth(sUnit - 1))
        {
            if (durate && CurrentHealth(sUnit - 1))
            {
                LookWithAngle(sUnit, durate - 1);
                FrameTimerWithArg(1, sUnit, RunningZombieLifeTime);
                break;
            }
            owner = GetOwner(sUnit - 1);
            MoveObject(sUnit, GetObjectX(sUnit - 1), GetObjectY(sUnit - 1));
            Damage(sUnit - 1, 0, CurrentHealth(sUnit - 1) + 1, 14);
            if (CurrentHealth(owner))
                SplashDamageAtEx(owner, GetObjectX(sUnit), GetObjectY(sUnit), 84.0,onRunningZombieSplash );
        }
        Delete(sUnit);
        break;
    }
}

void OnRunningZombieDeath()
{
    float xy[]={GetObjectX(SELF), GetObjectY(SELF)};

    Delete(SELF);
    DeleteObjectTimer(CreateObjectAt("MediumFlame", xy[0], xy[1]), 12);
    DeleteObjectTimer(CreateObjectAt("Explosion", xy[0], xy[1]), 12);
    Effect("SPARK_EXPLOSION", xy[0], xy[1], 0.0, 0.0);
}

void ZombieAttackReportComplete()
{
    if (!GetCaller()) return;

    Damage(OTHER, SELF, 100, 5);
    if (GetDirection(GetTrigger() + 1))
        LookWithAngle(GetTrigger() + 1, 0);
}



int ZombieBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339098; arr[1] = 25961; arr[17] = 75; arr[19] = 150; arr[23] = 32768; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1106247680; 
		arr[31] = 10; arr[59] = 5542784; arr[60] = 1360; arr[61] = 46895440;
		link = arr;
        CustomMeleeAttackCode(arr,ZombieAttackReportComplete);
	}
	return link;
}

void ZombieSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1083179008);
		SetMemory(ptr + 0x224, 1083179008);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 75);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 75);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, ZombieBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 0);
	}
}

int RunningZombieSpawn(int owner, float gap)
{
    int zomb = CreateObjectAt("Zombie", GetObjectX(owner) + UnitAngleCos(owner, gap), GetObjectY(owner) + UnitAngleSin(owner, gap));
    LookWithAngle(CreateObjectAt("ImaginaryCaster", GetObjectX(zomb), GetObjectY(zomb)), 120);

    SetOwner(owner, zomb);
    FrameTimerWithArg(1, zomb + 1, RunningZombieLifeTime);
    ZombieSubProcess(zomb);
    LookWithAngle(zomb, GetDirection(owner));
    SetCallback(zomb, 5, OnRunningZombieDeath);
    return zomb;
}

void RunningZombieSummonWandClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF), zomb;

    if (ABS(cFps - cTime) < 40)
    {
        UniPrint(OTHER, "쿨다운 입니다... 잠시만 기다려 주세요");
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        zomb = RunningZombieSpawn(OTHER, 23.0);
        GreenLightningFx(FloatToInt(GetObjectX(OTHER)), FloatToInt(GetObjectY(OTHER)), FloatToInt(GetObjectX(zomb)), FloatToInt(GetObjectY(zomb)), 24);
        CreatureGuard(zomb, GetObjectX(zomb), GetObjectY(zomb), GetObjectX(zomb) + UnitAngleCos(zomb, 128.0), GetObjectY(zomb) + UnitAngleSin(zomb, 128.0), 600.0);
        PlaySoundAround(OTHER, 591);
    }
}
void FONCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 255, 1);
            GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int ForceOfNature(int sUnit, float gap)
{
    int mis = CreateObjectAt("DeathBall", GetObjectX(sUnit) + UnitAngleCos(sUnit, gap), GetObjectY(sUnit) + UnitAngleSin(sUnit, gap));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, FONCollide);
    SetOwner(sUnit, mis);
    return mis;
}

void FONStaffClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 10)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        PushObject(ForceOfNature(OTHER, 16.0), 18.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 560);
    }
}

int FallingMeteor(float sX, float sY, int sDamage, float sSpeed)
{
    int mUnit = CreateObjectAt("Meteor", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec), sDamage);
        SetMemory(ptr + 0x14, GetMemory(ptr + 0x14) | 0x20);
        Raise(mUnit, 255.0);
        SetMemory(ptr + 0x6c, ToInt(sSpeed));
    }
    return mUnit;
}

void MeteorWandClassStrike(int sGlow)
{
    int owner = GetOwner(sGlow);

    if (CurrentHealth(owner))
    {
        if (IsVisibleTo(owner, sGlow))
        {
            SetOwner(owner, FallingMeteor(GetObjectX(sGlow), GetObjectY(sGlow), 220, -8.0));
            PlaySoundAround(sGlow, 85);
        }
        else
            UniPrint(owner, "마법 시전을 실패하였습니다. 현재의 마우스 포인터 위치는 캐릭터가 볼 수 없는 지역입니다");
    }
    Delete(sGlow);
}

void MeteorWandClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF), glow;

    if (ABS(cFps - cTime) < 25)
    {
        UniPrint(OTHER, "메테오 완드 재사용 대기중 입니다");
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        glow = CreateObjectAt("Moonglow", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(OTHER, glow);
        FrameTimerWithArg(1, glow, MeteorWandClassStrike);
    }
}
void InitSecretExitWalls(int location)
{
    int beacon = CreateObject("CarnivorousPlant", location), i;

    for (i = 0 ; i < 5 ; i ++)
        CreateObject("InvisibleLightBlueLow", location + 1 + i);
    Damage(beacon, 0, CurrentHealth(beacon) + 1, -1);
    SetCallback(beacon, 9, PressExitSecretZoneBeacon);
}

void LaiserSword(int owner)     //SpecialProperty - Laiser Sword
{
    float x_vect = UnitAngleCos(owner, 30.0), y_vect = UnitAngleSin(owner, 30.0);
    int k, ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner)) + 1;

    Enchant(owner, EnchantList(19), 0.9);
    Delete(ptr - 1);
    MoveWaypoint(1, GetObjectX(owner) + x_vect, GetObjectY(owner) + y_vect);
    for (k = 0 ; k < 11 ; k ++)
    {
        Frozen(CreateObject("Maiden", 1), 1);
        SetOwner(owner, ptr + k);
        DeleteObjectTimer(ptr + k, 1);
        SetCallback(ptr + k, 9, LaiserLifleTouched);
        Effect("SENTRY_RAY", GetObjectX(owner), GetObjectY(owner), GetObjectX(ptr + k), GetObjectY(ptr + k));
        TeleportLocationVector(1, x_vect, y_vect);
        if (!IsVisibleTo(owner, ptr + k))
            break;
    }
}

void onWhit3Splash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 225, DAMAGE_TYPE_PLASMA);
        }
    }
}

void WhiteOutHammer(int owner)      // SpecialProperty - Whiteout Hammer
{
    int unit;

    UnitSetEnchantTime(owner, 19, 33);
    SplashDamageAtEx(owner, GetObjectX(owner), GetObjectY(owner), 200.0, onWhit3Splash);
    unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));
    CastSpellObjectObject("SPELL_TURN_UNDEAD", unit, unit);
    DeleteObjectTimer(unit, 30);
}

void EnergyParHammer(int owner)     // SpecialProperty - 
{
    float xVect = UnitAngleCos(owner, 23.0), yVect = UnitAngleSin(owner, 23.0);
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) + xVect, GetObjectY(owner) + yVect);

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) - xVect, GetObjectY(owner) - yVect), yVect);
    Raise(unit, xVect);

    SetOwner(owner, unit);
    LookWithAngle(unit, 150);
    UnitSetEnchantTime(owner, 19, 50);
    FrameTimerWithArg(1, unit, EnergyParHandler);
}

void TripleArrowHammer(int owner)       // SpecialProperty -
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));

    SetOwner(owner, unit);
    UnitSetEnchantTime(owner, 19, 45);
    FrameTimerWithArg(1, unit, TripleArrowShot);
}

void DeathraySword(int owner)       // SpecialProperty -
{
    float xVect = UnitAngleCos(owner, 23.0), yVect = UnitAngleSin(owner, 23.0);
    int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(owner) + xVect, GetObjectY(owner) + yVect);

    MoveWaypoint(1, GetObjectX(owner), GetObjectY(owner));
    AudioEvent("TripleChime", 1);
    AudioEvent("FirewalkOn", 1);
    Effect("WHITE_FLASH", LocationX(1), LocationY(1), 0.0, 0.0);
    DeleteObjectTimer(CreateObjectAt("ManaBombCharge", GetObjectX(owner), GetObjectY(owner)), 18);
    SetOwner(owner, unit);
    SetUnitScanRange(unit, 450.0);
    LookWithAngle(unit, GetDirection(owner));
    SetCallback(unit, 3, DeathrayDetectedTarget);
    DeleteObjectTimer(unit, 1);
    Enchant(owner, "ENCHANT_PROTECT_FROM_MAGIC", 0.8);
}

void ShurikenSword(int owner)       // SpecialProperty -
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) + UnitAngleCos(owner, 21.0), GetObjectY(owner) + UnitAngleSin(owner, 21.0));
    LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), GetDirection(owner));
    Raise(unit, 250.0);
    SetOwner(owner, unit);
    LookWithAngle(unit, 35);
    UnitSetEnchantTime(owner, 19, 60);
    FrameTimerWithArg(1, unit, AutoTrackingMissile);
}

void WolfShooting(int owner)        // SpecialProperty - NEW
{
    int unit = DummyUnitCreateById(OBJ_WHITE_WOLF , GetObjectX(owner) + UnitAngleCos(owner, 23.0), GetObjectY(owner) + UnitAngleSin(owner, 23.0));

    SetOwner(owner, CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)));
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), UnitAngleSin(owner, 21.0));
    Raise(unit + 1, UnitAngleCos(owner, 21.0));
    LookWithAngle(unit + 1, 30);
    LookWithAngle(unit, GetDirection(owner));
    SetCallback(unit, 9, FlyingWolfCollideHandler);
    FrameTimerWithArg(1, unit + 1, FlyingWolf);
    Enchant(owner, "ENCHANT_PROTECT_FROM_MAGIC", 0.8);
}

void BomberSummon(int owner)        // SpecialProperty - NEW
{
    int unit = CreateObjectAt("BomberGreen", GetObjectX(owner) + UnitAngleCos(owner, 23.0), GetObjectY(owner) + UnitAngleSin(owner, 23.0));

    LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), 180); //TODO: Duration 6 seconds
    Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
    SetOwner(owner, unit);
    SetOwner(owner, unit + 1);
    SetCallback(unit, 9, BomberCollideHandler);
    FrameTimerWithArg(1, unit + 1, BomberLifeTime);
    Enchant(owner, "ENCHANT_PROTECT_FROM_MAGIC", 1.8);
}

void CollideHandlerEnergyPar()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 200, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void EnergyParHandler(int sUnit)
{
    int owner = GetOwner(sUnit), durate = GetDirection(sUnit), unit;

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(owner) && IsVisibleTo(sUnit, sUnit + 1))
        {
            if (durate)
            {
                unit = DummyUnitCreateById(OBJ_DEMON , GetObjectX(sUnit), GetObjectY(sUnit));
                DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(unit), GetObjectY(unit)), 9);
                MoveObject(sUnit, GetObjectX(sUnit) + GetObjectZ(sUnit), GetObjectY(sUnit) + GetObjectZ(sUnit + 1));
                MoveObject(sUnit + 1, GetObjectX(sUnit + 1) + GetObjectZ(sUnit), GetObjectY(sUnit + 1) + GetObjectZ(sUnit + 1));
                SetOwner(owner, unit);
                DeleteObjectTimer(unit, 1);
                SetCallback(unit, 9, CollideHandlerEnergyPar);
                FrameTimerWithArg(1, sUnit, EnergyParHandler);
                LookWithAngle(sUnit, durate - 1);
                break;
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        break;
    }
}

void LaiserLifleTouched()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("SentryRayHit", 1);
        Damage(OTHER, owner, 185, 16);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.1);
        Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
        Effect("VIOLET_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    }
}

void TripleArrowShot(int ptr)
{
    int owner = GetOwner(ptr), unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)) + 1, i;

    if (CurrentHealth(owner))
    {
        LookWithAngle(ptr, GetDirection(owner) - 15);
        for (i = 0 ; i < 11 ; i ++)
        {
            SpawnBullet(owner, GetObjectX(unit - 1) + UnitAngleCos(ptr, 21.0), GetObjectY(unit - 1) + UnitAngleSin(ptr, 21.0), 175, 38.0);
            LookWithAngle(ptr, GetDirection(ptr) + 3);
        }
    }
    Delete(ptr);
}

void TripleArrowCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 175, 9);
            UnitSetEnchantTime(OTHER, 25, 35);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int SpawnBullet(int owner, float x, float y, int dam, float force)
{
    int unit = CreateObjectAt("LightningBolt", x, y);

    SetUnitCallbackOnCollide(unit, TripleArrowCollide);
    LookAtObject(unit, owner);
    LookWithAngle(unit, GetDirection(unit) + 128);
    PushObjectTo(unit, UnitRatioX(unit, owner, force), UnitRatioY(unit, owner, force));
    SetOwner(owner, unit);
    return unit;
}
void RemoveSecretEntranceWalls(int sUnit)
{
    int i;

    for (i = 0 ; i < 4 ; i ++)
        WallUtilOpenWallAtObjectPosition(sUnit + i);
}

void HiddenCandleTouch()
{
    if (IsObjectOn(SELF) && CurrentHealth(OTHER))
    {
        Frozen(CreateObjectAt("DunMirScaleTorch1", GetObjectX(SELF), GetObjectY(SELF)), 1);
        UniChatMessage(OTHER, "근처에서 비밀의 벽이 열렸습니다", 150);
        FrameTimerWithArg(3, GetTrigger() + 1, RemoveSecretEntranceWalls);
        Delete(SELF);
    }
}

int HiddenCandle(int location)
{
    int candle = CreateObject("DunMirScaleTorch1", location);

    SetUnitCallbackOnCollide(candle, HiddenCandleTouch);
    Frozen(candle, 1);
    return candle;
}
int CheckWeaponStrike(int unit)
{
    int inv = PlayerGetEquipedWeapon(unit);

    if (IsObjectOn(inv))
        return ((CheckPlayerInput(unit) == 6) && !HasEnchant(unit, "ENCHANT_PROTECT_FROM_MAGIC"));
    return 0;
}

void DeathrayDetectedTarget()
{
    Effect("GREATER_HEAL", GetObjectX(OTHER), GetObjectX(OTHER), GetObjectX(SELF), GetObjectY(SELF));
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
    RestoreHealth(GetOwner(SELF), 2);
}

void AutoTrackingMissile(int sUnit)
{
    int owner = GetOwner(sUnit), durate = GetDirection(sUnit), unit;

    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (durate && IsVisibleTo(sUnit, sUnit + 1))
            {
                unit = CreateObjectAt("AirshipCaptain", GetObjectX(sUnit), GetObjectY(sUnit));
                Frozen(CreateObjectAt("HarpoonBolt", GetObjectX(unit), GetObjectY(unit)), 1);
                SetOwner(sUnit, unit);
                LookWithAngle(unit, GetDirection(sUnit + 1));
                LookWithAngle(unit + 1, GetDirection(sUnit + 1));
                DeleteObjectTimer(unit, 1);
                DeleteObjectTimer(unit + 1, 3);
                SetCallback(unit, 3, DetectTrackingMissile);
                SetCallback(unit, 9, CollideTrackingMissile);
                MoveObject(sUnit + 1, GetObjectX(sUnit), GetObjectY(sUnit));
                MoveObject(sUnit, GetObjectX(sUnit) + UnitAngleCos(sUnit + 1, 21.0), GetObjectY(sUnit) + UnitAngleSin(sUnit + 1, 21.0));
                LookWithAngle(sUnit, durate - 1);
                FrameTimerWithArg(1, sUnit, AutoTrackingMissile);
                break;
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        break;
    }
}

void DetectTrackingMissile()
{
    int ptr = GetOwner(SELF);
    int tg = ToInt(GetObjectZ(ptr + 1));

    if (CurrentHealth(tg))
    {
        if (IsVisibleTo(tg, ptr))
        {
            LookAtObject(SELF, tg);
            LookWithAngle(ptr + 1, GetDirection(SELF));
        }
        else
            Raise(ptr + 1, ToFloat(0));
    }
    else
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < GetObjectZ(ptr))
            Raise(ptr + 1, ToFloat(GetCaller()));
    }
}

void CollideTrackingMissile()
{
    int ptr = GetOwner(SELF);
    int owner = GetOwner(ptr);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, owner, 750, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        Delete(SELF);
        Delete(ptr);
    }
}

void FlyingWolfCollideHandler()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER) && CurrentHealth(owner) && IsAttackedBy(OTHER, owner) && MaxHealth(SELF))
    {
        MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
        LookWithAngle(GetTrigger() + 2, 1); //TODO: Start Explosion
        Delete(SELF);
    }
}
void onFlySplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 170, DAMAGE_TYPE_PLASMA);
        }
    }
}

void RemoveSecretExitWalls(int sUnit)
{
    int i;

    for (i = 0 ; i < 5 ; i ++)
        WallUtilOpenWallAtObjectPosition(sUnit + i);
}

void PressExitSecretZoneBeacon()
{
    if (CurrentHealth(OTHER) && MaxHealth(SELF))
    {
        if (IsPlayerUnit(OTHER))
        {
            FrameTimerWithArg(3, GetTrigger() + 1, RemoveSecretExitWalls);
            UniPrint(OTHER, "전방에 있는 비밀벽이 열립니다");
            Delete(SELF);
        }
    }
}
void FlyingWolfExplosion(int sUnit)
{
    float xProfile = GetObjectX(sUnit), yProfile = GetObjectY(sUnit);
    int owner = GetOwner(sUnit);

    PlaySoundAround(sUnit, 594);
    PlaySoundAround(sUnit, 487);
    SplashDamageAtEx(owner, xProfile, yProfile, 120.0, onFlySplash);
    GreenExplosion(xProfile, yProfile);
    DeleteObjectTimer(CreateObjectAt("ForceOfNatureCharge", xProfile, yProfile), 30);
    Effect("JIGGLE", xProfile, yProfile, 20.0, 0.0);
}

void FlyingWolf(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), explo = GetDirection(ptr + 1);

    while (1)
    {
        if (CurrentHealth(owner) && count)
        {
            if (explo)
                FlyingWolfExplosion(ptr);
            else if (IsVisibleTo(ptr, ptr - 1))
            {
                MoveObject(ptr - 1, GetObjectX(ptr - 1) + GetObjectZ(ptr), GetObjectY(ptr - 1) + GetObjectZ(ptr + 1));
                LookWithAngle(ptr, count - 1);
                FrameTimerWithArg(1, ptr, FlyingWolf);
                break;
            }
        }
        Delete(ptr);
        Delete(ptr + 1);
        if (MaxHealth(ptr - 1))
            Delete(ptr - 1);
        break;
    }
}

void ReleaseForceofNature(int ptr)
{
    int owner = GetOwner(ptr), mis;

    if (CurrentHealth(owner))
    {
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        mis = CreateObject("DeathBall", 1);
        SetOwner(owner, mis);
        DeleteObjectTimer(mis, 85);
        AudioEvent("ForceOfNatureRelease", 1);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void BomberExplosion(int unit)
{
    int owner = GetOwner(unit), ptr;

    if (CurrentHealth(unit))
    {
        if (CurrentHealth(owner))
        {
            MoveWaypoint(1, GetObjectX(unit), GetObjectY(unit));
            ptr = CreateObject("InvisibleLightBlueLow", 1);
            SetOwner(owner, CreateObject("ForceOfNatureCharge", 1) - 1);
            AudioEvent("ForceOfNatureCast", 1);
            FrameTimerWithArg(25, ptr, ReleaseForceofNature);
        }
        Delete(unit);
    }
    Delete(unit + 1);
}

void BomberCollideHandler()
{
    if (CurrentHealth(SELF) && CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
    {
        Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
        BomberExplosion(GetTrigger());
        LookWithAngle(GetTrigger(), 0); //TODO: LifeTime SetTo 0
    }
}

void BomberLifeTime(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr);

    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                LookWithAngle(ptr, count - 1);
                FrameTimerWithArg(1, ptr, BomberLifeTime);
                break;
            }
            else
                BomberExplosion(ptr - 1);
        }
        if (MaxHealth(ptr - 1))
            Delete(ptr - 1);
        Delete(ptr);
        break;
    }
}
void InitCandleRoom()
{
    int unit;
    
    if (!unit)
    {
        unit = HiddenCandle(77);
        CreateObject("InvisibleLightBlueLow", 78);
        CreateObject("InvisibleLightBlueLow", 79);
        CreateObject("InvisibleLightBlueLow", 80);
        CreateObject("InvisibleLightBlueLow", 81);
        FrameTimerWithArg(1, 82, InitSecretExitWalls);
    }
}

void MagicMissileCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 150, 9);
            GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
            break;
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}
void MultiAutoRayInSight()
{
    AddNewNode(GetTrigger() + 1, GetCaller());
    SetOwner(GetOwner(SELF), CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER)));
}

void DeathRayCasting(int sUnit)
{
    int owner = GetOwner(sUnit + 1);
    int sightMonster = CreateObjectAt("WeirdlingBeast", GetObjectX(sUnit), GetObjectY(sUnit));

    AddNewNode(CreateObjectAt("ImaginaryCaster", GetObjectX(sightMonster), GetObjectY(sightMonster)), sightMonster + 1);
    SetOwner(owner, CreateObjectAt("ImaginaryCaster", GetObjectX(sightMonster + 1), GetObjectY(sightMonster + 1)));
    SetOwner(owner, sightMonster);
    UnitNoCollide(sightMonster);
    SetCallback(sightMonster, 3, MultiAutoRayInSight);
    LookWithAngle(sightMonster, GetDirection(sUnit));
    DeleteObjectTimer(sightMonster, 1);
    FrameTimerWithArg(1, sightMonster + 1, StartChain);

    Delete(sUnit);
}

void DeathRayWandClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF), caster;

    if (ABS(cFps - cTime) < 68)
    {
        UniPrint(OTHER, "쿨다운 입니다... 잠시만 기다려 주세요");
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        caster = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + UnitAngleCos(OTHER, 21.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 21.0));
        UnitSetEnchantTime(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster), GetObjectY(caster)), 13, 60);
        SetOwner(OTHER, caster + 1);
        DeleteObjectTimer(caster + 1, 48);
        LookWithAngle(caster, GetDirection(OTHER));
        PlaySoundAround(OTHER, 44);
        FrameTimerWithArg(3, caster, DeathRayCasting);
    }
}
int MagicWeaponFunctionTable(int index)
{
    int functions[] = {
        LaiserSword,
        WhiteOutHammer,
        EnergyParHammer,
        TripleArrowHammer,
        DeathraySword,
        ShurikenSword,
        WolfShooting,
        BomberSummon };
    return functions[index];
}

int MagicWeaponProperty(int unit)
{
    int owner = GetOwner(unit);

    if (CurrentHealth(owner))
    {
        if (PlayerGetEquipedWeapon(owner) == unit)
        {
            if (CheckWeaponStrike(owner))
                CallFunctionWithArg(MagicWeaponFunctionTable(GetDirection(unit + 1)), owner);
        }
    }
    return IsObjectOn(unit);
}

void ChainLightningProc(int cur)
{
    int unit = GetUnit1C(cur), owner = GetOwner(cur + 1), next;

    if (IsObjectOn(cur))
    {
        next = GetNextNode(cur);
        if (IsObjectOn(next))
            GreenLightningFx(FloatToInt(GetObjectX(cur)), FloatToInt(GetObjectY(cur)), FloatToInt(GetObjectX(next)), FloatToInt(GetObjectY(next)), 24);
            //Effect("SENTRY_RAY", GetObjectX(cur), GetObjectY(cur), GetObjectX(next), GetObjectY(next));
            //GreenLightningFx(FloatToInt(GetObjectX(cur)), FloatToInt(GetObjectY(cur)), FloatToInt(GetObjectX(next)), FloatToInt(GetObjectY(next)), 20);
        if (CurrentHealth(unit) && CurrentHealth(owner))
            Damage(unit, owner, 80, 9);
        //FrameTimerWithArg(1, next, ChainLightningProc);
        Delete(cur);
        Delete(cur + 1);
        ChainLightningProc(next);
    }
}

void StartChain(int headNode)
{
    FrameTimerWithArg(1, GetNextNode(headNode), ChainLightningProc);
    Delete(headNode);
}
void RepairShoppingNet()
{
    int res;

    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 3000)
        {
            res = RepairAll(OTHER);
            if (res)
            {
                MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
                DeleteObjectTimer(CreateObjectAt("BigSmoke", GetObjectX(SELF), GetObjectY(SELF)), 12);
                Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
                AudioEvent("ShopRepairItem", 1);
                ChangeGold(OTHER, -3000);
char buff[128];
NoxSprintfString(buff, "결제성공- 모두 %d 개의 인벤토리가 수리되었습니다", &res, 1);

                UniPrint(OTHER, ReadStringAddressEx(buff));
            }
        }
        else
            UniPrint(OTHER, "금액이 부족합니다");
    }
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.3);
        UniPrint(OTHER, "자신이 소유한 모든 인벤토리를 수리합니다, 이 작업은 3000원이 필요하며 계속 진행하려면 더블클릭 하세요");
    }
}


void BuySpecialWeapon()
{
    int plr, pay = ToInt(GetObjectZ(GetTrigger() + 1)), idx = GetDirection(SELF);

    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= pay)
        {
            ChangeGold(OTHER, -pay);
            MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
            AudioEvent("TreasureDrop", 1);
            CallFunctionWithArg(SpecialWeaponPlaceFunctionTable(idx), 1);
            UniPrint(OTHER, "결제완료");
        }
        else
            UniPrint(OTHER, "잔액이 부족합니다!");
    }
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.3);
        UniPrint(OTHER, DescriptSpecialWeapon(idx));
        char buff[128];

        NoxSprintfString(buff, "이 작업은 %d골드가 필요해요. 계속하려면 더블클릭 하세요", &pay, 1);
        UniPrint(OTHER, ReadStringAddressEx(buff));
    }
}

int SpecialWeaponShop(string name, int wp, int idx, int pay)
{
    int unit = AbilityButton(name, wp, idx, 0, pay);

    Enchant(CreateObjectAt("RoundChakramInMotion", GetObjectX(unit), GetObjectY(unit)), "ENCHANT_FREEZE", 0.0);
    Enchant(unit + 2, "ENCHANT_SHOCK", 0.0);
    Frozen(unit + 2, 1);
    return unit;
}

int SpecialWeaponPlaceFunctionTable(int index)
{
    int functions[] = {
        SpecialSword1, SpecialSword2, SpecialSword3,
        SpecialSword4, SpecialSword5, SpecialSword6, SpecialSword7,
        SpecialSword8
    };
    return functions[index % 8];
}

int SpecialSword1(int wp)
{
    int unit = CreateObject("GreatSword", wp);

    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 0);
    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, GetMemory(0x5BA24C), GetMemory(0x005BA264), GetMemory(0x5BA30C));
    NewList(unit);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);

    return unit;
}

int SpecialSword2(int wp)
{
    int unit = CreateObject("WarHammer", wp);

    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 1);
    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, GetMemory(0x005BA354), GetMemory(0x5BA384), GetMemory(0x5BA42C));
    NewList(unit);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    Enchant(unit, "ENCHANT_RUN", 0.0);
    return unit;
}

int SpecialSword3(int wp)
{
    int unit = CreateObject("GreatSword", wp);

    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 2);
    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, GetMemory(0x005BA354), GetMemory(0x5BA384), GetMemory(0x5BA42C));
    NewList(unit);
    Enchant(unit, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
    Enchant(unit, "ENCHANT_RUN", 0.0);
    return unit;
}

int SpecialSword4(int wp)
{
    int unit = CreateObject("WarHammer", wp);

    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 3);
    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, GetMemory(0x5BA2AC), GetMemory(0x005BA30C), GetMemory(0x5BA3CC));
    NewList(unit);
    Enchant(unit, "ENCHANT_AFRAID", 0.0);
    return unit;
}

int SpecialSword5(int wp)
{
    int unit = CreateObject("GreatSword", wp);

    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 4);
    NewList(unit);
    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, GetMemory(0x5BA60C), GetMemory(0x5BA204), GetMemory(0x5BA36C));
    Enchant(unit, "ENCHANT_AFRAID", 0.0);
    return unit;
}

int SpecialSword6(int wp)
{
    int unit = CreateObject("GreatSword", wp);

    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, GetMemory(0x5BA42C), GetMemory(0x5BA444), GetMemory(0x5BA24C)); //Venom+fire
    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 5);
    NewList(unit);
    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    return unit;
}

int SpecialSword7(int wp)
{
    int unit = CreateObject("GreatSword", wp);

    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, WeaponEffect(21), GetMemory(0x5BA2C4), GetMemory(0x5BA36C)); //Impact+light
    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 6);
    NewList(unit);
    Enchant(unit, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
    return unit;
}

int SpecialSword8(int wp)
{
    int unit = CreateObject("WarHammer", wp);
    int ptr = GetMemory(0x750710);

    SetWeaponPropertiesDirect(unit, ITEM_PROPERTY_weaponPower6, WeaponEffect(21), WeaponEffect(20), WeaponEffect(15));
    LookWithAngle(CreateObject("InvisibleLightBlueHigh", wp), 7);
    NewList(unit);
    Enchant(unit, "ENCHANT_PROTECT_FROM_POISON", 0.0);
    return unit;
}

void AbilityAwardEvent(int unit)
{
    TeleportLocation(1, GetObjectX(unit), GetObjectY(unit));
    Effect("WHITE_FLASH", LocationX(1), LocationY(1), 0.0, 0.0);
    GreenSparkAt(LocationX(1), LocationY(1));
    AudioEvent("AwardSpell", 1);
}

string SkillDescript(int num)
{
    string name[] = {
        "[스킬설명] 순보: 짧은 거리를 빠르게 이동한다. 조심스럽게 걷기 시전을 통해 발동된다",
        "[스킬설명] 피카츄의 \"백만볼트\": 피카 츄~~~~~~. 발동키: J",
        "[스킬설명] 버저커차지 시전 쿨다운을 없앱니다. 조심스럽게 걷기 시전을 통해 발동되며 순보 마술과 중복됨에 유의하세요",
        "[패시브] 캐릭터의 체력회복과 이동속도 모두 눈에띄게 빨라집니다"};

    return name[num];
}

string DescriptSpecialWeapon(int num)
{
    string name[] = {
        "[스페셜 무기] 광선검: 스타워즈에서 나오는 그 광선검이다",
        "[스페셜 무기] 백야 혼동망치: 내리치면 흰색 오로라가 생기며 주위에 200의 피해를 준다",
        "[스페셜 무기] 에너지파 소드: 손오공의 에너지파가 발사되는 검이다",
        "[스페셜 무기] 수리검해머: 해머를 내리치면 해머 파편이 사방으로 튀면서 적에게 피해를 준다",
        "[스페셜 무기] 죽음의 흡혈소드: 전방 적에게 데스레이 한방을 쏘고 맞은 적으로 부터 체력을 2씩 회복한다",
        "[스페셜 무기] 극사 소드: 적을 끝까지 추적하며 \"대상은 무조건 죽는다\"",
        "[스페셜 무기] 하이에나 소드: 전방으로 하이에나를 돌진시키며 적과 닿으면 폭발한다",
        "[스페셜 무기] 봄버 소환 해머: 해머를 내리칠 때 마다 봄버가 소환된다. 이 봄버는 6 초 후 사라진다"};

    return name[num];
}




void TakeNewLifeDesc()
{
    int coin;
    GetXTraCoin(0, &coin);
    PlaySoundAround(OTHER, 801);
    UniChatMessage(SELF, "원활한 게임 진행을 위해 필요한 코인 1개를 구입하시겠어요?", 150);
    UniPrint(OTHER, "코인 1개 구입은 3만 골드가 요구됩니다. 거래를 계속하려면 '예'를 누르세요");
    char buff[128];

    NoxSprintfString(buff, "현재 %d개 코인을 보유중입니다", &coin, 1);
    UniPrint(OTHER, ReadStringAddressEx(buff));
    TellStoryUnitName("aa", "Con10B.scr:HecubahLine3", "코인1개 구입");
}

void TakeNewLifeTrade()
{
    int coin;
    GetXTraCoin(0, &coin);
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 30000)
    {
        if (coin < 100)
        {
            PlaySoundAround(OTHER, 1004);
            ChangeGold(OTHER, -30000);
            GetXTraCoin(++coin,0);
            Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
        else
            UniPrint(OTHER, "소유 가능한 최대 라이프 수에 도달했기 때문에 더 이상 라이프를 추가할 수 없습니다");
    }
    else
        UniPrint(OTHER, "금액이 부족합니다");

    char buff[92];

    NoxSprintfString(buff, "현재 보유중인 코인은%d개 입니다", &coin, 1);
    UniPrint(OTHER, ReadStringAddressEx(buff));
}

int ShopClassMagicStaffShopData(int index)
{
    int pay[5] = {51000, 55000, 48000, 65000, 60000};

    return pay[index];
}

void MagicStaffClassPick()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 10)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        AbsolutelyWeaponPickupAndEquip(OTHER, SELF);
        UniPrint(OTHER, "무기를 바꾼 상태에서 이 마법 지팡이를 장착하려면 버렸다가 다시 주워야 합니다");
    }
}

void MagicStaffPreProc(int staff, int useFunc)
{
    SetUnitCallbackOnPickup(staff, MagicStaffClassPick);
    SetUnitCallbackOnUseItem(staff, useFunc);
}

int WandCreateFunctionPtrTable(int order)
{
    int createActions[] = {FONStaffClassCreate, MeteorWandClassCreate, MagicMissileCreate, RunningZombieSumStaffCreate,
        AutoTargetRayStaffCreate};
    
    return createActions[order];
}

int FONStaffClassCreate(int owner)
{
    int stf = CreateObjectAt("InfinitePainWand", GetObjectX(owner), GetObjectY(owner));

    MagicStaffPreProc(stf, FONStaffClassUse);
    return stf;
}

int MeteorWandClassCreate(int owner)
{
    int wnd = CreateObjectAt("FireStormWand", GetObjectX(owner), GetObjectY(owner));

    MagicStaffPreProc(wnd, MeteorWandClassUse);
    return wnd;
}

int MagicMissileWandClassCreate(int owner)
{
    int stf = CreateObjectAt("SulphorousFlareWand", GetObjectX(owner), GetObjectY(owner));

    MagicStaffPreProc(stf, MagicMissileWandClassUse);
    return stf;
}

int RunningZombieSumStaffCreate(int owner)
{
    int stf = CreateObjectAt("ForceWand", GetObjectX(owner), GetObjectY(owner));

    MagicStaffPreProc(stf, RunningZombieSummonWandClassUse);
    return stf;
}

int AutoTargetRayStaffCreate(int owner)
{
    int stf = CreateObjectAt("DeathRayWand", GetObjectX(owner), GetObjectY(owner));

    MagicStaffPreProc(stf, DeathRayWandClassUse);
    return stf;
}

void SpawnLowLevelMonsters(int mark)
{
    int count = GetDirection(mark);

    if (count > 0)
    {
        RhombusUnitTeleport(mark, 2981.0, 3320.0, 3353.0, 4106.0);
        SetCallback(CallFunctionWithArgInt(LowLevelMobFunctions(Random(0, 5)), mark), 7, FieldMonsterHurt);
        LookWithAngle(mark, --count);
        FrameTimerWithArg(1, mark, SpawnLowLevelMonsters);
    }
}

void onInitscanEnded(int *pParams)
{
    int count =pParams[UNITSCAN_PARAM_COUNT];
    char msg[128];

    NoxSprintfString(msg, "%d개의 유닛을 스캔했습니다", &count, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
    
    int spawnMark = CreateObjectAt("InvisibleLightBlueHigh", LocationX(1), LocationY(1));
    char buff[128];

    LookWithAngle(spawnMark, 180);
    FrameTimerWithArg(180, spawnMark, SpawnLowLevelMonsters);
}

void startInitscan(){
    int initScanHash, lastUnit=GetMaster();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemNormalCommon);
    HashPushback(initScanHash, OBJ_REWARD_MARKER_PLUS, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, SpawnMarkerMonster);
    StartUnitScanEx(Object("FirstMonsterMarker"), lastUnit, initScanHash, onInitscanEnded);
}

int BackRow()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("IronBlock", 61);
        Frozen(CreateObject("IronBlock", 62), 1);
        Frozen(CreateObject("IronBlock", 63), 1);
        Frozen(CreateObject("IronBlock", 64), 1);
        Frozen(CreateObject("IronBlock", 65), 1);
        Frozen(CreateObject("IronBlock", 66), 1);
        Frozen(ptr, 1);
    }
    return ptr;
}

int HealingWishWell(int wp)
{
    int unit = CreateObject("WizardWhite", wp);

    Enchant(CreateObject("InvisibleLightBlueHigh", wp), "ENCHANT_SHIELD", 0.0);
    LookWithAngle(unit, 64);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    SetDialog(unit, "a", StartUnitHealing, StartUnitHealing);

    return unit;
}

void StartUnitHealing()
{
    int unit;

    if (!HasEnchant(OTHER, "ENCHANT_CROWN"))
    {
        Enchant(OTHER, "ENCHANT_CROWN", 8.0);
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("ProtectionFromPoisonEffect", 1);
        AudioEvent("LongBellsDown", 1);
        unit = CreateObject("InvisibleLightBlueHigh", 1);
        SetOwner(OTHER, unit);
        Enchant(unit, "ENCHANT_RUN", 0.0);
        FrameTimerWithArg(1, unit, UnitHealingHandler);
        UniPrint(OTHER, "힐링버프 감지됨, 잠시동안 당신의 체력이 지속적으로 회복됩니다");
    }
}

void UnitHealingHandler(int ptr)
{
    int owner = GetOwner(ptr);

    if (HasEnchant(owner, "ENCHANT_CROWN") && IsObjectOn(ptr))
    {
        RestoreHealth(owner, 1);
        MoveObject(ptr, GetObjectX(owner), GetObjectY(owner));
        FrameTimerWithArg(1, ptr, UnitHealingHandler);
    }
    else
        Delete(ptr);
}

void dungeonGateStorage(int **get)
{
    int arr[DUNGEON_COUNT];

    get[0]=arr;
}

int CheckDungeon(int unit)
{
    float temp = 5138.0, dist;
    int i, res = -1, *gate;

    dungeonGateStorage(&gate);
    for (i = 0 ; i < DUNGEON_COUNT ; i += 1)
    {
        dist = DistanceUnitToUnit(unit, gate[i]);
        if (dist < temp)
        {
            temp = dist;
            res = i;
        }
    }
    return res;
}

void RemoveMagicSword(int unit)
{
    if (IsObjectOn(unit))
        Delete(unit);
    Delete(unit + 1);
}

void MagicWeaponLoop()
{
    int gnode;

    GlobalHeadNode(0,&gnode);
    int cur = GetNextNode(gnode);

    while (IsObjectOn(cur))
    {
        if (ToInt(GetObjectZ(cur + 1)))
        {
            if (MagicWeaponProperty(ToInt(GetObjectZ(cur + 1))))
                cur = GetNextNode(cur);
            else
            {
                RemoveMagicSword(ToInt(GetObjectZ(cur + 1)));
                cur = RemoveList(cur);
            }
        }
        else
            break;
    }
    int gover;
    QueryGGOver(0,&gover);
    if (gover)  return;
    else        FrameTimer(1, MagicWeaponLoop);
}


void InitList()
{
    int gnode=CreateObject("InvisibleLightBlueHigh", 1);
    GlobalHeadNode( gnode, 0);
    CreateObject("InvisibleLightBlueHigh", 1);
    CreateObject("InvisibleLightBlueHigh", 1);
    SetNextNode(gnode, gnode + 1);
    SetPrevNode(gnode + 1, gnode);
    FrameTimer(100, MagicWeaponLoop);
}

void InitDungeonGates(int max)
{
    int i, *gate;

    dungeonGateStorage(&gate);
    for (i = 0 ; i < max ; i += 1)
        gate[i] = Object("DunGate" + IntToString(i + 1));
}

void PutSpecialShop()
{
    //LifeShop
    //DamageUpgradeShop
    //etc...
    int ptr = CreateObject("Maiden", 21);

    Frozen(CreateObject("Ankh", 21), 1);
    SetDialog(AbilityButton("Swordsman", 67, 0, 2, 15000), "a", BuyNewSkill, BuyNewSkill);
    SetDialog(AbilityButton("Swordsman", 32, 1, 4, 80000), "a", BuyNewSkill, BuyNewSkill);
    SetDialog(AbilityButton("Swordsman", 33, 2, 8, 30000), "a", BuyNewSkill, BuyNewSkill);
    SetDialog(AbilityButton("Swordsman", 49, 3, 0x10, 80000), "a", BuyNewSkill, BuyNewSkill);

    SetDialog(SpecialWeaponShop("Maiden", 34, 0, 85000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(SpecialWeaponShop("Maiden", 35, 1, 80000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(SpecialWeaponShop("Maiden", 36, 2, 80000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(SpecialWeaponShop("Maiden", 37, 3, 87000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(SpecialWeaponShop("Maiden", 38, 4, 90000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(SpecialWeaponShop("Maiden", 39, 5, 75000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(SpecialWeaponShop("Maiden", 40, 6, 75000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(SpecialWeaponShop("Maiden", 41, 7, 75000), "a", BuySpecialWeapon, BuySpecialWeapon);
    SetDialog(AbilityButton("Necromancer", 43, 0, 0, 0), "a", RepairShoppingNet, RepairShoppingNet);
    Frozen(ptr, 1);
    SetDialog(ptr, "YESNO", TakeNewLifeDesc, TakeNewLifeTrade);
    StoryPic(ptr, "MaidenPic5");
}

void InitStampStrings()
{
    StrWarSkill();
    StrSpecialWeapon();
}

void TestFunction()
{
    UniPrintToAll("***        던전 레이드하기_____ 제작. Rainbow Company                   ***");
    UniPrintToAll("*** 주 임무: 6개의 던전을 다 털고나서 열리는 마지막 던전을 털면 승리합니다 ***");
    UniPrintToAll("초반 20라이프가 제공됩니다, 라이프는 돈으로 추가할 수 있으며 라이프가 모두 소모되면 게임오버됩니다");
}

void ShopClassMagicStaffDescr()
{
    int curIndex = GetDirection(SELF);
    string descMessage[] = {
        "포스오브네이쳐 지팡이",
        "메테오 완드",
        "매직 미사일 완드",
        "러닝 좀비서먼 완드",
        "오토 타겟팅 데스레이 지팡이"
    };
    char buff[192];
    int args[]={StringUtilGetScriptStringPtr(descMessage[curIndex]), ShopClassMagicStaffShopData(curIndex) };
    //CancelDialog(SELF);
    NoxSprintfString(buff, "%s을 구입하시겠어요? 그것의 가격은 %d골드 입니다", args, sizeof(args));
    UniPrint(OTHER, ReadStringAddressEx(buff));
    UniPrint(OTHER, "구입하려면 '예' 를 누르시고 다른 아이템을 보시려면 '아니오' 를 누르세요. 거래를 취소하려면 '떠나기' 를 누르세요");
    Raise(GetTrigger() + 1, descMessage);
    TellStoryUnitName("AA", "thing.db:IdentifyDescription", descMessage[curIndex]);
}

void ShopClassMagicStaffTrade()
{
    int dlgRes = GetAnswer(SELF), curIndex = GetDirection(SELF), staff;
    string *staffName = ToStr(ToInt(GetObjectZ(GetTrigger() + 1)));
    char buff[256];

    if (dlgRes == 1)
    {
        if (GetGold(OTHER) >= ShopClassMagicStaffShopData(curIndex))
        {
            ChangeGold(OTHER, -ShopClassMagicStaffShopData(curIndex));
            PlaySoundAround(OTHER, 308);
            staff = CallFunctionWithArgInt(WandCreateFunctionPtrTable(curIndex), OTHER);
            SetOwner(OTHER, staff);
            FrameTimerWithArg(1, staff, DelayForcePickItemToOwner);
            int args[]={StringUtilGetScriptStringPtr(staffName[curIndex]), StringUtilGetScriptStringPtr(staffName[curIndex])};
            NoxSprintfString(buff, "%s구입 거래가 완료되었습니다. %s이 캐릭터에게 장착되었습니다", args, sizeof(args));
            UniPrint(OTHER, ReadStringAddressEx(buff));
            args[0]=StringUtilGetScriptStringPtr(PlayerIngameNick(OTHER));
            NoxSprintfString(buff, "%s님께서 %s을 구입했습니다", args, sizeof(args));
            UniPrintToAll(ReadStringAddressEx(buff));
        }
        else
        {
            int pay=ShopClassMagicStaffShopData(curIndex) - GetGold(OTHER);
            NoxSprintfString(buff, "거래가 취소되었습니다. 잔액이 부족합니다(금화%d이 더필요합니다)", &pay, 1);
            UniPrint(OTHER, ReadStringAddressEx(buff));
        }
    }
    else if (dlgRes == 2)
    {
        UniPrint(OTHER, "'아니오' 를 누르셨습니다. 다음 판매 품목을 보여드립니다");
        LookWithAngle(SELF, (curIndex + 1) % 5);
        ShopClassMagicStaffDescr();
    }
}

int ShopClassMagicalStaffCreate(int location)
{
    int shop = DummyUnitCreateById(OBJ_WIZARD_GREEN , LocationX(location), LocationY(location));

    LookWithAngle(CreateObjectAt("ImaginaryCaster", GetObjectX(shop), GetObjectY(shop)) - 1, 0);
    SetDialog(shop, "YESNO", ShopClassMagicStaffDescr, ShopClassMagicStaffTrade);
    return shop;
}

void ShopClassInit(int location)
{
    ShopClassMagicStaffShopData(0);
    int s= ShopClassMagicalStaffCreate(location);

    StoryPic(s, "DryadPic");
}

void InitUpLevelSecretZone()
{
    int dam = CreateObject("Rat", 75);

    Frozen(CreateObject("MovableBookcase1", 76), 1);
    SetCallback(CreateObject("InvisibleLightBlueLow", 76) - 2, 9, MikyDeadTouched);
    LookWithAngle(dam + 1, 48);
    Damage(dam, 0, 100, 14);
}

void MapDecorations()
{
    BackRow();
    Frozen(CreateObjectAt("DunMirScaleTorch2", 2588.0, 4635.0), 1);
    Frozen(CreateObjectAt("DunMirScaleTorch2", 2771.0, 4446.0), 1);
    Frozen(CreateObjectAt("DunMirScaleTorch2", 3197.0, 3820.0), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 42), 1);
    HealingWishWell(58);
    HealingWishWell(59);
    HealingWishWell(60);
    InitDungeonGates(DUNGEON_COUNT);
    FrameTimer(1, InitMapSigns);
    FrameTimer(1, PutSpecialShop);
    //FrameTimer(3, PutDungeonNameStampString);
    FrameTimer(15, InitStampStrings);
    FrameTimer(220, TestFunction);
    FrameTimer(31, InitList);
    ShopClassInit(68);
    FrameTimer(50, InitUpLevelSecretZone);
}

void OnInitializeMap()
{
    MusicEvent();
    CreateLogFile("xtest511-log.txt");
    startInitscan();
    MakeCoopTeam();
    GetSafeZone();
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
    FrameTimer(8, MapDecorations);
    FrameTimer(10, PlayerPreserveHandler);
    FrameTimerWithArg(20, Object("UndergroundElev"), DisableObject);
    GetXTraCoin(20,0);
}

void OnShutdownMap()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void OnPlayerEntryMapProc(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    // if (pUnit)
    //     ShowQuestIntroOne(pUnit, 9999, "WarriorChapterBegin9", "Noxworld.wnd:Ready");
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
