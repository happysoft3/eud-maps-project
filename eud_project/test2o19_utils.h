
#include "libs/define.h"
#include "libs/format.h"
#include "libs/printutil.h"

void StrFailMission()
{
	int arr[22];
	string name = "DrainManaOrb";
	int i = 0;
	arr[0] = 270598782; arr[1] = 285235140; arr[2] = 272171586; arr[3] = 285233156; arr[4] = 673251906; arr[5] = 285233796; arr[6] = 1210221122; arr[7] = 285233796; arr[8] = 71074370; arr[9] = 285233797; 
	arr[10] = 36192834; arr[11] = 285241984; arr[12] = 2082480706; arr[13] = 285233799; arr[14] = 2097730; arr[15] = 21124; arr[16] = 2080391746; arr[17] = 285233799; arr[18] = 67125886; arr[19] = 293361600; 
	arr[20] = 2084553216; arr[21] = 20487; 
	while(i < 22)
	{
		drawStrFailMission(arr[i], name);
		i ++;
	}
}

void drawStrFailMission(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(1);
		pos_y = GetWaypointY(1);
	}
	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 62 == 61)
			MoveWaypoint(1, GetWaypointX(1) - 122.000000, GetWaypointY(1) + 2.000000);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.000000, GetWaypointY(1));
		count ++;
	}
	if (count >= 682)
	{
		count = 0;
		MoveWaypoint(1, pos_x, pos_y);
	}
}

void StrSelGameType()
{
	int arr[27];
	string name = "CharmOrb";
	int i = 0;
	arr[0] = 2082718366; arr[1] = 1107328903; arr[2] = 144968056; arr[3] = 2163218; arr[4] = 37888256; arr[5] = 1082426450; arr[6] = 255852560; arr[7] = 554992111; arr[8] = 270402; arr[9] = 1615348816; 
	arr[10] = 135302923; arr[11] = 287571970; arr[12] = 537038884; arr[13] = 8357880; arr[14] = 571046049; arr[15] = 2097153; arr[16] = 2105344; arr[17] = 133714176; arr[18] = 16; arr[19] = 337772048; 
	arr[20] = 262660; arr[21] = 1073743872; arr[22] = 2021722632; arr[23] = 33816383; arr[24] = 1115693056; arr[25] = 127; arr[26] = 134283008; 
	while(i < 27)
	{
		drawStrSelGameType(arr[i], name);
		i ++;
	}
}

void drawStrSelGameType(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(1);
		pos_y = GetWaypointY(1);
	}
	for (i = 1 ; i > 0 && count < 837 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 76 == 75)
			MoveWaypoint(1, GetWaypointX(1) - 150.000000, GetWaypointY(1) + 2.000000);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.000000, GetWaypointY(1));
		count ++;
	}
	if (count >= 837)
	{
		count = 0;
		MoveWaypoint(1, pos_x, pos_y);
	}
}

void StrEasyMode()
{
	int arr[17];
	string name = "BlueMoveOrb";
	int i = 0;
	arr[0] = 2083512892; arr[1] = 75792263; arr[2] = 17305732; arr[3] = 554177040; arr[4] = 606077984; arr[5] = 138445856; arr[6] = 138449024; arr[7] = 553656578; arr[8] = 1140597378; arr[9] = 1157923967; 
	arr[10] = 134217984; arr[11] = 33628690; arr[12] = 1143218176; arr[13] = 2052; arr[14] = 2139752591; arr[15] = 16793587; arr[16] = 32; 
	while(i < 17)
	{
		drawStrEasyMode(arr[i], name);
		i ++;
	}
}

void drawStrEasyMode(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(1);
		pos_y = GetWaypointY(1);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 48 == 47)
			MoveWaypoint(1, GetWaypointX(1) - 94.000000, GetWaypointY(1) + 2.000000);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.000000, GetWaypointY(1));
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(1, pos_x, pos_y);
	}
}

void StrHardMode()
{
	int arr[17];
	string name = "BlueMoveOrb";
	int i = 0;
	arr[0] = 2021622840; arr[1] = 268500751; arr[2] = 69222404; arr[3] = 1089472; arr[4] = 8450; arr[5] = 67633284; arr[6] = 34660353; arr[7] = 1074012224; arr[8] = 2131751816; arr[9] = 35790817; 
	arr[10] = 32768; arr[11] = 4368; arr[12] = 1145044996; arr[13] = 1048576; arr[14] = 2145524736; arr[15] = 16764924; arr[16] = 16; 
	while(i < 17)
	{
		drawStrHardMode(arr[i], name);
		i ++;
	}
}

void drawStrHardMode(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(1);
		pos_y = GetWaypointY(1);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 49 == 48)
			MoveWaypoint(1, GetWaypointX(1) - 96.000000, GetWaypointY(1) + 2.000000);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.000000, GetWaypointY(1));
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(1, pos_x, pos_y);
	}
}

void StrDoubleMode()
{
	int arr[17];
	string name = "BlueMoveOrb";
	int i = 0;
	arr[0] = 2082488958; arr[1] = 67403655; arr[2] = 17305855; arr[3] = 553783312; arr[4] = 538969120; arr[5] = 138446840; arr[6] = 536408192; arr[7] = 16785666; arr[8] = 1140588546; arr[9] = 2139358335; 
	arr[10] = 134217984; arr[11] = 33619984; arr[12] = 2082480128; arr[13] = 1073743879; arr[14] = 2139099327; arr[15] = 1627406323; arr[16] = 63; 
	while(i < 17)
	{
		drawStrDoubleMode(arr[i], name);
		i ++;
	}
}

void drawStrDoubleMode(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(1);
		pos_y = GetWaypointY(1);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 48 == 47)
			MoveWaypoint(1, GetWaypointX(1) - 94.000000, GetWaypointY(1) + 2.000000);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.000000, GetWaypointY(1));
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(1, pos_x, pos_y);
	}
}

void BomberNormalCollide(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(ptr + 0x2b8, 0x4e83b0);
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; arr[27] = 1; arr[28] = 1106247680; 
		arr[29] = 22; arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; 
		arr[58] = 5546320; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

#define PRINT_ALL -1

void PrintMessageFormatOne(int user, string fmt, int arg)
{
    char message[192];

    NoxSprintfString(message, fmt, &arg, 1);
    if (user==-1)
    {
        UniPrintToAll(ReadStringAddressEx(message));
        return;
    }
    UniPrint(user, ReadStringAddressEx(message));
}
