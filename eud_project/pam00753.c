
#include "pam00753_sub.h"
#include "pam00753_initscan.h"
#include "pam00753_mon.h"
#include "pam00753_item.h"
#include "pam00753_player.h"
#include "pam00753_utils.h"
#include "pam00753_gvar.h"
#include "libs/objectIDdefines.h"

int m_pItemInfo;

static int GetItemInformationStruct() //virtual
{
	return m_pItemInfo;
}

int m_initScanHash;

int m_lastUnit;

static int GetMasterUnit() //virtual
{
	return m_lastUnit;
}

void MapInitialize()
{
    MusicEvent();
    m_lastUnit=DummyUnitCreateById(OBJ_HECUBAH,100.0,100.0);
    InitializeSubPart();
    HashCreateInstance(&m_initScanHash);
    HashPushback(m_initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(m_initScanHash, OBJ_NECROMANCER_MARKER, CreateMonsterMarker);
    InitItemNames(&m_pItemInfo);
    StartUnitScan(Object("firstscan"), m_lastUnit, m_initScanHash);
}

void MapExit()
{
    MusicEvent();
    DeInitializeSubPart();
}

int m_player[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{
    return m_player[pIndex];
}

static void SetPlayer(int pIndex, int user) //virtual
{
    m_player[pIndex]=user;
}

int m_pFlags[MAX_PLAYER_COUNT];

static int GetPlayerFlags(int pIndex) //virtual
{
    return m_pFlags[pIndex];
}

static void SetPlayerFlags(int pIndex, int flags) //virtual
{
    m_pFlags[pIndex]=flags;
}

int m_mobMarker[10];

static void StoreMonsterMarker(int id, int mark) //virtual
{
	SetUnit1C(mark,m_mobMarker[id]);
	m_mobMarker[id]=mark;
}

static int ReleaseMonsterMarker(int id) //virtual
{
	int ret=m_mobMarker[id];
	
	m_mobMarker[id]=0;
	return ret;
}





















