
#include "p0110011_gvar.h"
#include "libs/grplib.h"
#include "libs/format.h"
#include "libs/animFrame.h"

void GRPDump_KeyLayout(){}
void GRPDump_DvaOutput(){}
void GRPDumpRoachOniOutput(){}
void GRPDumpTeletobbyOutput(){}

void UpdateDamageUpgradeText()
{
    char *p=CLIENT_SUBDATA_OFF,buff[128];
    int args[]={p[SUBDATA_REPORT_DAMAGE_UP], ComputeDamageUpPay(p[SUBDATA_REPORT_DAMAGE_UP]),};
    short *pMessage;
    NoxSprintfString(buff,"데미지업그레이드LV%d->%d골드",args,sizeof(args));
    GetBeaconGuideMessage(&pMessage,0);
    NoxUtf8ToUnicode(buff,pMessage);
}
void UpdateHpUpgradeText()
{
    char *p=CLIENT_SUBDATA_OFF,buff[128];
    int args[]={p[SUBDATA_REPORT_HP_UP], ComputeHpUpPay(p[SUBDATA_REPORT_HP_UP]),};
    short *pMessage;
    NoxSprintfString(buff,"HP업그레이드LV%d->%d골드",args,sizeof(args));
    GetBeaconGuideMessage(&pMessage,1);
    NoxUtf8ToUnicode(buff,pMessage);
}
void UpdateSpeedUpgradeText()
{
    char *p=CLIENT_SUBDATA_OFF,buff[128];
    int args[]={p[SUBDATA_REPORT_SPEED_UP], ComputeSpeedUpPay(p[SUBDATA_REPORT_SPEED_UP]),};
    short *pMessage;
    NoxSprintfString(buff,"이동속도 업그레이드LV%d->%d골드",args,sizeof(args));
    GetBeaconGuideMessage(&pMessage,3);
    NoxUtf8ToUnicode(buff,pMessage);
}

void initializeStringSprite(int imgVector)
{
    short *pMessage;
    GetBeaconGuideMessage(&pMessage, 0);
    AppendTextSprite(imgVector, 0xF920, pMessage, 133699);
    GetBeaconGuideMessage(&pMessage, 1);
    AppendTextSprite(imgVector, 0xF920, pMessage, 112958);
    GetBeaconGuideMessage(&pMessage, 2);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("25골드를 경험치 10으로 변환"), pMessage);
    AppendTextSprite(imgVector, 0xF920, pMessage, 112959);
    GetBeaconGuideMessage(&pMessage, 4);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("110골드를 경험치\n50으로 변환"), pMessage);
    AppendTextSprite(imgVector, 0xF920, pMessage, 112987);
    GetBeaconGuideMessage(&pMessage, 5);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("체력FULL회복+1분무적-5골드"), pMessage);
    AppendTextSprite(imgVector, 0xF920, pMessage, 112997);
    GetBeaconGuideMessage(&pMessage, 3);
    AppendTextSprite(imgVector, 0xF920, pMessage, 112960);
    short telpoMessage[128];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("마지막 위치로 이동"), telpoMessage);
    AppendTextSprite(imgVector, 0x059F, telpoMessage, 133814);
    UpdateDamageUpgradeText();
    UpdateHpUpgradeText();
    UpdateSpeedUpgradeText();

    short winMessage[128];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("!임무완료, YOU WIN!"), winMessage);
    AppendTextSprite(imgVector, 0xF920, winMessage, 112992);
}

void initializeImageFrameDva(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WIZARD_GREEN;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_DvaOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0, 116177, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, 116177+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, 116177+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, 116177+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, 116177+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, 116177+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(30, 116177+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameAssign8Directions(35, 116177+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameAssign8Directions(40, 116177+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);

    AnimFrameCopy8Directions(116177+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(116177+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(116177+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(116177+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(116177+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions(116177+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions(116177+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions(116177+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);

    AnimFrameAssign8Directions(85,116177+72,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,0);
}

void initializeImageFrameRoachOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRoachOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign4Directions(0, 130133, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, 130133+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     130133  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, 130133+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     130133+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     130133   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     130133+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
}
void GRPDumpTileFloorOutput(){}
void initializeImageFloor(int v){
    int *frames, *sizes, thingId=OBJ_CORPSE_SKULL_SW;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpTileFloorOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AppendImageFrame(v,frames[0],132568);
}

void GRPDump_RedCloakOutput(){}

#define RED_CLOAK_BASE_IMAGE_ID 122915
void initializeImageFrameRedCloakGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_RedCloakOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, RED_CLOAK_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, RED_CLOAK_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     RED_CLOAK_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, RED_CLOAK_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     RED_CLOAK_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     RED_CLOAK_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     RED_CLOAK_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     RED_CLOAK_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     RED_CLOAK_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 3);
}

#define TELETOBBY_BASE_IMAGE_ID 124053
void initializeImageFrameTeletobby(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BLACK_WOLF;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpTeletobbyOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,3,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, TELETOBBY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, TELETOBBY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, TELETOBBY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
}

void GRPDump_BatAooniOutput(){}

#define BAT_AOONI_BASE_IMAGE_ID 113935
void initializeImageFrameFlyingAooni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_EVIL_CHERUB;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BatAooniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BAT_AOONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy4Directions(     BAT_AOONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BAT_AOONI_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BAT_AOONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
}

void GRPDump_AooniOutput(){}

#define AOONI_BASE_IMAGE_ID 125517
void initializeImageFrameAooniDefault(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SCORPION;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_AooniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,3,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, AOONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, AOONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     AOONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, AOONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     AOONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     AOONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     AOONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     AOONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     AOONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameCopy4Directions(     AOONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
}

void GRPDump_Sketch(){}

#define SKETCHMAN_IMAGE_ID 120669

void initializeImageFrameSketchMan(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_SPIDER;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_Sketch)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,USE_DEFAULT_SETTINGS);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,USE_DEFAULT_SETTINGS,USE_DEFAULT_SETTINGS);
    AnimFrameAssign8Directions(0,SKETCHMAN_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SKETCHMAN_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_WALK,0    );
    AnimFrameAssign8Directions(10,SKETCHMAN_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_WALK,1 );
    AnimFrameAssign8Directions(15,SKETCHMAN_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameAssign8Directions(20,SKETCHMAN_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,0    );
    AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_RUN,1 );
    AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
}

void GRPDumpBlueBirdOutput(){}

#define BLUE_BIRD_GIRL_IMAGE_START_ID 119258
void initializeImageBlueBird(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_IMP;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpBlueBirdOutput)+4, thingId, xyInc, &frames, &sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,3,2,2);
    AnimFrameAssign4Directions(0, BLUE_BIRD_GIRL_IMAGE_START_ID,   IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BLUE_BIRD_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign4Directions(6, BLUE_BIRD_GIRL_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameCopy4Directions(     BLUE_BIRD_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 1);
    AnimFrameCopy4Directions(BLUE_BIRD_GIRL_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 2);
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void initializeKeylayoutImage(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GAME_BALL;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_KeyLayout)+4, thingId, NULLPTR, &frames, &sizes);
    AppendImageFrame(imgVector, frames[0], 131930);
}

void GRPDump_MikaOniOutput(){}

#define MIKAONI_BASE_IMAGE_ID 128403
void initializeImageFrameMikaOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH_WITH_ORB;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_MikaOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, MIKAONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, MIKAONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     MIKAONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, MIKAONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     MIKAONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     MIKAONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     MIKAONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     MIKAONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     MIKAONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     MIKAONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_ToothOniOutput(){}

#define TOOTH_ONI_BASE_IMAGE_ID 116856
void initializeImageFrameToothOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_EMBER_DEMON;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_ToothOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, TOOTH_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, TOOTH_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, TOOTH_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
}

void GRPDump_TurtleneckOniOutput(){}

#define TURTLE_NECK_ONI_BASE_IMAGE_ID 123725
void initializeImageFrameTurtleNeckOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WHITE_WOLF;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_TurtleneckOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1, USE_DEFAULT_SETTINGS, 2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4, 2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, TURTLE_NECK_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, TURTLE_NECK_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     TURTLE_NECK_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, TURTLE_NECK_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     TURTLE_NECK_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     TURTLE_NECK_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     TURTLE_NECK_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     TURTLE_NECK_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     TURTLE_NECK_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     TURTLE_NECK_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void InitializeResource()
{
    int imgVector=CreateImageVector(2048);

    initializeStringSprite(imgVector);
    initializeImageFrameDva(imgVector);
    initializeImageFrameRoachOni(imgVector);
    initializeImageFrameTeletobby(imgVector);
    initializeImageFrameFlyingAooni(imgVector);
    initializeImageFrameAooniDefault(imgVector);
    initializeImageFrameSketchMan(imgVector);
    initializeImageBlueBird(imgVector);
    initializeImageHealthBar(imgVector);
    initializeImageFrameRedCloakGirl(imgVector);
    initializeKeylayoutImage(imgVector);
    initializeImageFrameMikaOni(imgVector);
    initializeImageFrameToothOni(imgVector);
    initializeImageFrameTurtleNeckOni(imgVector);
    initializeImageFloor(imgVector);
    DoImageDataExchange(imgVector);
}
