
#include "libs/define.h"
#include "lzz99mh_gvar.h"
#include "libs/gui_window.h"
#include "libs/format.h"

void initspecialWeaponShop()
{
    string desc[]={
        "화살비 서드", "늑데돌진서드", "극사 서드", "백만볼트 서드", 
        "트리플 고드름 해머", "데스레이 서드", };
    int pay[]={95308, 89930, 119900, 71200, 
        81900, 74300, };
    int messages[]={GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_1, GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_2,GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_3,GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_4,
        GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_5, GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_6,};
    SetSpecialWeaponNameArray(desc, sizeof(desc));
    SetSpecialWeaponPayAndMessage(pay, messages);
}

void initDialog()
{
    int *wndptr=0x6e6a58;
    GUIFindChild(wndptr[0], 3901, GetDialogCtxPtr());
}

string specialItemFormat(string input, int index)
{
    int args[]={StringUtilGetScriptStringPtr(GetSpecialWeaponName(index)),GetSpecialWeaponPay(index)};

    NoxSprintfString(StringUtilGetScriptStringPtr(input), "%s을 구입할래요?가격은 %d골드입니다.아니오를 누르면 다른것도 보여줄게요", args,sizeof(args));
    return input;
}

void InitFillPopupMessageArray(string *pPopupMessage)
{
    pPopupMessage[GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM]="당신의 인벤토리를 무적화 하시겠어요? 필요한 금액은 3,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_1]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ1",0);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_2]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ2",1);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_3]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ3",2);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_4]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ4",3);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_5]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ5",4);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_6]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ6",5);
    pPopupMessage[GUI_DIALOG_MESSAGE_SUMMON_CARROT]="케럿을 구입하실래요? 필요한 금액은 120,000 골드입니다. 유저 마다 오직 1개만 보유할 수 있습니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_HEALING_AMULET_SHOP]="힐링목걸이를 풀로 6개 채워드립니다. 금액은 1,500 골드입니다!";
    pPopupMessage[GUI_DIALOG_MESSAGE_POTION_SHOP]="체력회복약을 풀로 6개 채워드립니다. 금액은 1,000 골드입니다!";
    pPopupMessage[GUI_DIALOG_MESSAGE_TELEPORT_TO_HOME]="마을지역으로 돌아가길 원하세요? 마을지역으로 돌아가려면 \"예\" 버튼을 누르십시오";
    pPopupMessage[GUI_DIALOG_MESSAGE_TELEPORT_TO_LAST]="최근 저장된 위치로 이동하시려면 \"예\" 버튼을 누르십시오";
}

void InitPopupMessage(string *pPopupMessage)
{
    initDialog();
    initspecialWeaponShop();
    InitFillPopupMessageArray(pPopupMessage);
}


