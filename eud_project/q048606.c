
#include "q048606_mon.h"
#include "q048606_main.h"
#include "libs/weaponcapacity.h"
#include "libs/indexloop.h"
 #include "libs/weapon_effect.h"
#include "libs/fixtellstory.h"
#include "libs/networkRev.h"

#define PLAYER_DEATH_FLAG 0x80000000
#define PLAYER_WINDBOOST_FLAG 0x2

#define GAME_MAX_WAVE 12

int m_genericHash;
int *m_randomItemFn;
int m_randomItemFnCount;

string *m_specialWeaponNameArray;
int *m_specialWeaponPays;
int *m_specialWeaponFn;

int *m_pWeaponUserSpecialProperty1;
int *m_pWeaponUserSpecialPropertyExecutor1;
// int *m_pWeaponUserSpecialPropertyFxCode1;

int *m_pWeaponUserSpecialProperty2;
int *m_pWeaponUserSpecialPropertyExecutor2;
// int *m_pWeaponUserSpecialPropertyFxCode2;

int *m_pWeaponUserSpecialProperty3;
int *m_pWeaponUserSpecialPropertyExecutor3;
// int *m_pWeaponUserSpecialPropertyFxCode3;

int *m_pWeaponUserSpecialProperty4;
int *m_pWeaponUserSpecialPropertyExecutor4;
// int *m_pWeaponUserSpecialPropertyFxCode4;

int *m_pWeaponUserSpecialProperty5;
int *m_pWeaponUserSpecialPropertyExecutor5;
// int *m_pWeaponUserSpecialPropertyFxCode5;

int *m_pWeaponUserSpecialProperty6;
int *m_pWeaponUserSpecialPropertyExecutor6;
// int *m_pWeaponUserSpecialPropertyFxCode6;

int *m_pWeaponUserSpecialProperty7;
int *m_pWeaponUserSpecialPropertyExecutor7;
// int *m_pWeaponUserSpecialPropertyFxCode7;

int *m_pWeaponUserSpecialProperty8;
int *m_pWeaponUserSpecialPropertyExecutor8;
// int *m_pWeaponUserSpecialPropertyFxCode8;

int *m_pWeaponUserSpecialProperty9;
int *m_pWeaponUserSpecialPropertyExecutor9;
// int *m_pWeaponUserSpecialPropertyFxCode9;

int *m_pWeaponUserSpecialPropertyA;
int *m_pWeaponUserSpecialPropertyExecutorA;
// int *m_pWeaponUserSpecialPropertyFxCodeA;
//@ignore_object_type

// int player[40], PlrNum = 0;
int m_userCount;
short*m_userRespawnLocation;
int m_userRespawnLocationCount;
int m_player[10];
int m_pFlags[10];
int CUNIT[180], MainProc, CreCnt, CreMx = 20;
int ITEMS[180];

int m_guardianFn[3];
int m_spawnFn[46];

void initUserRespawnLocation()
{
    short userRespawnLocation[]={1,2,3,4,5,6,7,8};

    m_userRespawnLocation=userRespawnLocation;
    m_userRespawnLocationCount=sizeof(userRespawnLocation);
}

void initSpwnFunctions()
{
    m_spawnFn[0]=SummonMonsterRat;
    m_spawnFn[1]=SummonUrchin;
    m_spawnFn[2]=SummonImp;
    m_spawnFn[3]=SummonFrog;
    m_spawnFn[4]=SummonMiniSpider;
    m_spawnFn[5]=SummonWildWolf;
    m_spawnFn[6]=SummonTheif;
    m_spawnFn[7]=SummonTheifWithBow;
    m_spawnFn[8]=SummonGiantWorms;
    m_spawnFn[9]=SummonFlier;
    m_spawnFn[10]=SummonWebSpider;
    m_spawnFn[11]=SummonShade;
    m_spawnFn[12]=SummonScorpion;
    m_spawnFn[13]=SummonBlueDog;
    m_spawnFn[14]=SummonWhiteWolf;
    m_spawnFn[15]=SummonTroll;
    m_spawnFn[16]=SummonWisp;
    m_spawnFn[17]=SummonGruntAxe;
    m_spawnFn[18]=SummonOgreBrute;
    m_spawnFn[19]=SummonDryad;
    m_spawnFn[20]=SummonDarkBear;
    m_spawnFn[21]=SummonGoon;
    m_spawnFn[22]=SummonShaman;
    m_spawnFn[23]=SummonJandor;
    m_spawnFn[24]=SummonBigRedHead; //SummonSkeletonLord;

    //the end here
    m_spawnFn[25]=SummonZombie;
    m_spawnFn[26]=SummonDarkSpider;
    m_spawnFn[27]=SummonGargoyle;
    m_spawnFn[28]=SummonFireFairy;
    m_spawnFn[29]=SummonEmberDemon;
    m_spawnFn[30]=SummonMystic;
    m_spawnFn[31]=SummonFeminist;
    m_spawnFn[32]=SummonOgreWarlord;
    m_spawnFn[33]=SummonSpecialBear;
    m_spawnFn[34]=SummonDeadFlower;
    m_spawnFn[35]=SummonJandor;
    m_spawnFn[36]=SummonBeholder;
    m_spawnFn[37]=SummonGreenBomber;
    m_spawnFn[38]=SummonBigRedHead;
    m_spawnFn[39]=SummonDemon;
    m_spawnFn[40]=SummonMimic;
    m_spawnFn[41]=SummonStoneGolem;
    m_spawnFn[42]=SummonGirl2;
    m_spawnFn[43]=SummonLich;
    m_spawnFn[44]=SummonHorrendous;
    m_spawnFn[45]=SummonBeast;
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlags[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlags[plr] ^= flags;
}

void MapLightingSetup()
{
    //54~115
    int k;
    for (k = 54 ; k <= 115 ; k ++)
        Enchant(CreateObject("InvisibleLightBlueHigh", k), "ENCHANT_LIGHT", 0.0);
}

void ManageItem(int unit)
{
    int key;

    if (IsObjectOn(ITEMS[key]))
    {
        Effect("CYAN_SPARKS", GetObjectX(ITEMS[key]), GetObjectY(ITEMS[key]), 0.0, 0.0);
        DeleteObjectTimer(ITEMS[key], 3);
    }
    ITEMS[key] = unit;
    key = (key + 1) % 180;
}

void InitTombGuardianNPC()
{
    int ptr = CreateObject("WeirdlingBeast", 48);

    UnitNoCollide(ptr);
    ObjectOff(ptr);
    ObjectOff(CreateObject("WeirdlingBeast", 49));
    UnitNoCollide(ptr + 1);
    ObjectOff(CreateObject("WeirdlingBeast", 50));
    UnitNoCollide(ptr + 2);
    ObjectOff(CreateObject("WeirdlingBeast", 51));
    UnitNoCollide(ptr + 3);
    SetUnitMaxHealth(ptr, 10);
    SetUnitMaxHealth(ptr + 1, 10);
    SetUnitMaxHealth(ptr + 2, 10);
    SetUnitMaxHealth(ptr + 3, 10);
    Frozen(CreateObject("TraderArmorRack1", 51), 1);
    SetDialog(ptr, "NORMAL", BuyGuardian, NothingAnymore);
    SetDialog(ptr + 1, "NORMAL", BuyGuardian, NothingAnymore);
    SetDialog(ptr + 2, "NORMAL", BuyGuardian, NothingAnymore);
    SetDialog(ptr + 3, "NORMAL", BuyPowerWeapon, NothingAnymore);
    LookWithAngle(ptr + 1, 1);
    LookWithAngle(ptr + 2, 2);
    Damage(ptr, 0, MaxHealth(ptr) + 1, -1);
    Damage(ptr + 1, 0, MaxHealth(ptr + 1) + 1, -1);
    Damage(ptr + 2, 0, MaxHealth(ptr + 2) + 1, -1);
    Damage(ptr + 3, 0, MaxHealth(ptr + 3) + 1, -1);
}

string WeaponLevelFx(int num)
{
    string ect[] = {"ENCHANT_AFRAID", "ENCHANT_ANCHORED", "ENCHANT_SHIELD", "ENCHANT_FREEZE", "ENCHANT_VAMPIRISM", "ENCHANT_RUN", "ENCHANT_REFLECTIVE_SHIELD",
        "ENCHANT_PROTECT_FROM_POISON", "ENCHANT_PROTECT_FROM_FIRE", "ENCHANT_PROTECT_FROM_ELECTRICITY"};

    return ect[num];
}

void BuyPowerWeapon()
{
    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.5);
        UniPrint(OTHER,"현재 착용중인 무기를 강화합니다. 이 작업은 15,000 골드 이(가) 필요하며, 계속하려면 더블클릭 하십시오");
        UniPrint(OTHER,"주의사항!! [25% 의 확률] 로 무기의 강화 레밸이 낮아지며, 강화 1 레밸에서 하락 시 무기가 사라집니다");
    }
    else
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 15000)
        {
            ChangeGold(OTHER, -15000);
            WeaponUpgrade(EquipedWeapon(GetMemory(0x979720)));
        }
    }
}

void WeaponUpgrade(int unit)
{
    int lv = GetDirection(unit), rnd = Random(0, 3), owner;
    char buff[128];

    if (IsObjectOn(unit))
    {
        if (lv < 10)
        {
            if (rnd)
            {
                owner = GetOwner(unit);
                MoveWaypoint(45, GetObjectX(owner), GetObjectY(owner));
                Effect("WHITE_FLASH", LocationX(45), LocationY(45), 0.0, 0.0);
                DeleteObjectTimer(CreateObject("LevelUp", 45), 28);
                AudioEvent("LevelUp", 45);
                Enchant(unit, WeaponLevelFx(lv), 0.0);
                LookWithAngle(unit, lv + 1);
                int arg=lv+1;
                NoxSprintfString(&buff, "레밸 %d 으로의 강화 성공!", &arg, 1);
                UniPrint(OTHER, ReadStringAddressEx(&buff));
            }
            else
            {
                if (!lv)
                {
                    Delete(unit);
                    UniPrint(OTHER, "0 레밸에서 강화 실패로 인한 무기 사라짐");
                }
                else
                {
                    LookWithAngle(unit, lv - 1);
                    EnchantOff(unit, WeaponLevelFx(lv));
                    int lv2=lv-1;
                    NoxSprintfString(&buff, "강화에 실패하였습니다. 현재 무기수준은 %d 입니다", &lv2, 1);
                    UniPrint(OTHER, ReadStringAddressEx(&buff));
                }
            }
        }
        else
            UniPrint(OTHER, "이 무기는 이미 강화 최대레밸에 도달했기 때문에 더 이상 강화를 진행할 수 없습니다");
    }
    else
        UniPrint(OTHER,"에러! 무기정보 없음");
}

void BuyGuardian()
{
    int pay[] = {55000,50000,50000}, idx = GetDirection(SELF);
    string name[] = { "빨간망토 차차", "닌자 가이텐", "아오오니"};
    char buff[192];

    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        PlaySoundAround(OTHER, SOUND_Gear1);
        Enchant(OTHER, "ENCHANT_AFRAID", 0.7);
        int args[]={StringUtilGetScriptStringPtr( name[idx]), pay[idx]};

        NoxSprintfString(&buff, "용병[%s]을 소환하며, 이 작업은 %d골드가 필요합니다. 계속하려면 더블클릭하세요", &args, sizeof(args));
        UniPrint(OTHER, ReadStringAddressEx(&buff));
        return;
    }
    EnchantOff(OTHER, "ENCHANT_AFRAID");
    if (GetGold(OTHER) >= pay[idx])
    {
        if (CreCnt < CreMx)
        {
            int gargs[]={GetObjectX(OTHER), GetObjectY(OTHER),};
            Bind(m_guardianFn[idx], gargs);
            ChangeGold(OTHER, -pay[idx]);
        }
        else
        {
            int args2[]={CreMx, CreCnt};
            NoxSprintfString(&buff, "더 이상 소환할 수 없습니다, 최대로 소환 가능한 개수 %d을 초과했어요(현재수:%d)", &args2, sizeof(args2));
            UniPrint(OTHER, ReadStringAddressEx(&buff));
        }
        return;
    }
    int iArg =pay[idx];

    NoxSprintfString(&buff, "금화가 %d만큼 더 필요해요", &iArg,1);
    UniPrint(OTHER, ReadStringAddressEx(&buff));
}

void guardianHurt(){
    if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_INVULNERABLE)))
    {
        Enchant(SELF, EnchantList(ENCHANT_INVULNERABLE), 3.0);
        RestoreHealth(SELF, 2);
    }
}

void GuardianRedWizard(float x, float y)
{
    int unit = CreateObjectById(OBJ_WIZARD_RED, x,y);

    UnitLinkBinScript(CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y) - 1, WizardRedBinTable());
    SetUnitMaxHealth(unit, 1600);
    SetOwner(GetHost(), unit);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    CreCnt ++;
    SetCallback(unit, 3, RedWizSightEvent);
    SetCallback(unit, 5, GuardianDeath);
    SetCallback(unit, 7, guardianHurt);
    SetDialog(unit, "NORMAL", GuardianEscortMode, NothingAnymore);
    AttachHealthbar(unit);
}

void GuardianNinja(float x, float y)
{
    int unit = CreateObjectById(OBJ_HORRENDOUS,x,y);

    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,x,y);
    SetUnitMaxHealth(unit, 1770);
    SetOwner(GetHost(), unit);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    CreCnt ++;
    SetCallback(unit, 3, NinjaSightEvent);
    SetCallback(unit, 5, GuardianDeath);
    SetCallback(unit, 7, guardianHurt);
    SetDialog(unit, "NORMAL", GuardianEscortMode, NothingAnymore);
    AttachHealthbar(unit);
}

void GuardianHidden(float x,float y)
{
    int unit = CreateObjectById(OBJ_AIRSHIP_CAPTAIN,x,y);

    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,x,y);
    AirshipCaptainSubProcess2(unit);
    CreCnt ++;
    SetCallback(unit, 3, JandorSightEvent);
    SetCallback(unit, 5, GuardianDeath);
    SetCallback(unit, 7, guardianHurt);
    SetOwner(GetHost(), unit);
    SetDialog(unit, "NORMAL", GuardianEscortMode, NothingAnymore);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    AttachHealthbar(unit);
}

void RedWizSightEvent()
{
    Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    Damage(OTHER, SELF, 30, 16);
    CheckResetSight(GetTrigger(), 18);
}

void NinjaSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 0.9);
        PlaySoundAround(SELF, SOUND_CrossBowShoot);
        int ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF),GetObjectY(SELF));
        SetOwner(SELF, ptr);
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr), GetObjectY(ptr)), ToFloat(GetCaller()));
        PushTimerQueue(1, ptr, ShootingNinjaSword);
    }
    CheckResetSight(GetTrigger(), 15);
}

void JandorSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 1.5);
        PlaySoundAround(SELF, SOUND_BlindOn);
        PlaySoundAround(SELF, SOUND_DeathRayKill);
        int ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));
        SetOwner(SELF, ptr);
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF)), ToFloat(GetCaller()));
        PushTimerQueue(1, ptr, JandorStrikeLoop);
    }
    CheckResetSight(GetTrigger(), 25);
}

void ShootingNinjaSword(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr + 1)), count = GetDirection(ptr), unit;

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner) && CurrentHealth(tg) && count < 35)
        {
            if (DistanceUnitToUnit(ptr, tg) > 25.0)
            {
                MoveObject(ptr, GetObjectX(ptr) + UnitRatioX(tg, ptr, 15.0), GetObjectY(ptr) + UnitRatioY(tg, ptr, 15.0));
                unit = CreateObjectAt("HarpoonBolt", GetObjectX(ptr), GetObjectY(ptr));
                Frozen(unit, 1);
                DeleteObjectTimer(unit, 3);
                LookAtObject(unit, tg);
                LookWithAngle(ptr, count + 1);
            }
            else
            {
                PlaySoundAround(ptr, SOUND_HitEarthBreakable);
                PlaySoundAround(ptr, SOUND_EggBreak);
                SplashDamage(owner, 35, 135.0, GetObjectX(ptr), GetObjectY(ptr));
                BloodingFx(ptr);
                LookWithAngle(ptr, 200);
            }
        }
        else
        {
            Delete(ptr);
            Delete(ptr + 1);
        }
        PushTimerQueue(1, ptr, ShootingNinjaSword);
    }
}

void GuardianEscortMode()
{
    if (!IsAttackedBy(OTHER, GetHost()))
    {
        if (GetOwner(GetTrigger() + 1) ^ GetCaller())
        {
            PlaySoundAround(OTHER, SOUND_BigGong);
            UniChatMessage(SELF, "경호모드", 90);
            UniPrint(OTHER, "경호모드로 설정되었습니다");
            SetOwner(OTHER, SELF);
            SetOwner(OTHER, GetTrigger() + 1);
            CreatureFollow(SELF, OTHER);
            AggressionLevel(SELF, 1.0);
        }
    }
}

void GuardianDeath()
{
    int count = CreCnt --;
    char msg[256];
    
    NoxSprintfString(msg,"방금 가디언 1개가 파괴되었습니다! 남은 가디언 개수는 %d 입니다", &count, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
    Delete(GetTrigger() + 1);
    DeleteObjectTimer(SELF, 30);

}

void TakeDeathRay()
{
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
}

void AutoTargetDeathray(int user)
{
    int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(user), GetObjectY(user));

    UnitNoCollide(unit);
    SetOwner(user, unit);
    SetUnitScanRange(unit, 600.0);
    LookWithAngle(unit, GetDirection(user));
    DeleteObjectTimer(unit, 1);
    SetCallback(unit, 3, TakeDeathRay);
}

void JandorStrikeLoop(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner) && CurrentHealth(tg) && count < 30)
        {
            if (Distance(GetObjectX(owner), GetObjectY(owner), GetObjectX(tg), GetObjectY(tg)) > 38.0)
            {
                MoveObject(owner, GetObjectX(owner) + UnitRatioX(tg, owner, 23.0), GetObjectY(owner) + UnitRatioY(tg, owner, 23.0));
                PlaySoundAround(owner,SOUND_PlayerEliminated);
                DeleteObjectTimer(CreateObjectAt("ReleasedSoul", GetObjectX(owner), GetObjectY(owner)), 12);
                LookWithAngle(ptr, count + 1);
            }
            else
            {
                Effect("JIGGLE", GetObjectX(owner), GetObjectY(owner), 19.0, 0.0);
                PlaySoundAround(owner, SOUND_BerserkerCrash);
                SplashDamage(owner, 30, 100.0, GetObjectX(owner), GetObjectY(owner));
                PushTimerQueue(1, 45, SmokeRingFx);
                PushTimerQueue(2, 45 | (16 << 1), SmokeRingFx);
                LookWithAngle(ptr, 200);
            }
        }
        else
        {
            Delete(ptr);
            Delete(ptr + 1);
        }
        PushTimerQueue(1, ptr, JandorStrikeLoop);
    }
}

void NewSkillEffect(int unit)
{
    float xpos = GetObjectX(unit),ypos=GetObjectY(unit);

    PlaySoundAround(unit, SOUND_LevelUp);
    GreenSparkAt(xpos,ypos);
    Effect("WHITE_FLASH", xpos,ypos, 0.0, 0.0);
    DeleteObjectTimer(CreateObjectAt("LevelUp", xpos,ypos), 45);
    UniPrint(unit, "새로운 기술을 습득했습니다!");
}

void AwardNewSkill()
{
    int plr = CheckPlayer();

    if (plr + 1)
    {
        if (MaxHealth(OTHER) == 150)
        {
            if (GetGold(OTHER) >= 15000)
            {
                if (!PlayerClassCheckFlag(plr, PLAYER_WINDBOOST_FLAG))
                {
                    NewSkillEffect(OTHER);
                    PlayerClassSetFlag(plr, PLAYER_WINDBOOST_FLAG);
                    UniPrint(OTHER, "빠른이동: 조심스럽게 걷기를 시전하면 대쉬가 발동됩니다");
                    ChangeGold(OTHER, -15000);
                }
                // else if (!(player[plr + 30] & 0x2))
                // {
                //     NewSkillEffect(player[plr]);
                //     player[plr + 30] = player[plr + 30] ^ 2;
                //     ChangeGold(OTHER, -15000);
                //     Enchant(player[plr], "ENCHANT_VILLAIN", 0.0);
                //     UniPrint(OTHER, "작살을 사용하면 적을 관통하는 회오리가 발사됩니다 (작살기능 포기해야 됨)");
                // }
                // else if (!(player[plr + 30] & 0x4))
                // {
                //     NewSkillEffect(player[plr]);
                //     player[plr + 30] = player[plr + 30] ^ 4;
                //     Enchant(player[plr], "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
                //     ChangeGold(OTHER, -15000);
                //     UniPrint(OTHER, "메두사의 눈: 늑데의 눈 시전 시 전방 적에게 데스레이 자동 타격합니다 (다수 타겟 가능)");
                // }
                else
                    UniPrint(OTHER, "당신은 이곳에서 습득할 수 있는 모든 기술을 배웠기 때문에 더 이상 배울 기술이 없습니다");
            }
            else
                UniPrint(OTHER, "전사 새로운 스킬 구입에 필요한 금액은 1만 5천 입니다, 현재 잔액을 다시 확인해 보세요");
        }
        else
            UniPrint(OTHER, "오류! 전사만 이용가능합니다");
    }
}

int WishCrown(int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 10);
    ObjectOff(unit);
    UnitNoCollide(unit);
    Frozen(CreateObject("SpinningCrown", wp), 1);
    SetDialog(unit, "NORMAL", UseWishWell, UseWishWell);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);

    return unit;
}

void HealingWellBerp(int unit)
{
    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit, "ENCHANT_ETHEREAL"))
        {
            Effect("GREATER_HEAL", GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit) - 150.0);
            RestoreHealth(unit, 1);
            PushTimerQueue(3, unit, HealingWellBerp);
        }
    }
}

void UseWishWell()
{
    if (CurrentHealth(OTHER) && !HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
    {
        float xpos = GetObjectX(OTHER), ypos = GetObjectY(OTHER);

        DeleteObjectTimer(CreateObjectAt("MagicSpark", xpos, ypos), 16);
        PlaySoundAround(SELF, SOUND_LongBellsDown);
        PlaySoundAround(OTHER, SOUND_PotionUse);
        Effect("GREATER_HEAL", xpos, ypos, GetObjectX(SELF), GetObjectY(SELF));
        UniPrint(OTHER, "이 우물이 잠시동안 당신의 체력을 회복시켜 줄 것입니다 (12 초간 지속)");
        Enchant(OTHER, "ENCHANT_ETHEREAL", 15.0);
        PushTimerQueue(3, GetCaller(), HealingWellBerp);
    }
}

void SettingItemShop()
{
    int ptr = CreateObject("Maiden", 46);
    ObjectOff(ptr);
    Damage(ptr, 0, MaxHealth(ptr) + 1, -1);
    Frozen(ptr, 1);
    SetDialog(ptr, "NORMAL", AwardNewSkill, AwardNewSkill);
    SetShopkeeperText(Object("ShopWarWeapon"), "전사 무기점");
    SetShopkeeperText(Object("ShopWarArmor"), "전사 갑옷점");
    SetShopkeeperText(Object("WarArmorShop2"), "전사 갑옷 2호점");
    SetShopkeeperText(Object("WarArmorShop3"), "전사 갑옷 3호점");
    //WishWell_setting
    WishCrown(38);
    WishCrown(39);
    WishCrown(40);
    WishCrown(41);
    WishCrown(42);
    PushTimerQueue(1, 0, StrNewSkills);
    PushTimerQueue(1, 0, StrTomb);
    PushTimerQueue(1, 0, StrWarShop);
    PushTimerQueue(2, 0, StrWeaponUpgrade);
}

void StartGameMent()
{
    MainProc = CreateObject("InvisibleLightBlueHigh", 13);
    Raise(GetMasterUnit() + 1, ToFloat(CreateObject("InvisibleLightBlueHigh", 13) - 1));
    //+0 dir: stage level
    //+0 z: kills
    //+1 dir: unit_counts
    UniPrintToAll("Blood Festival (피의 축제)                                                v0.1 제작. 237");
    PushTimerQueue(320, MainProc, NextStage);
    PushTimerQueue(10, MainProc, LineMonsterAI);
}

void RemoveMainProc()
{
    Delete(MainProc);
    Delete(MainProc + 1);
}

void LineMonsterAI(int ptr)
{
    int idx, target;

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(CUNIT[idx]))
        {
            target = GetNearlyPlayer(CUNIT[idx]);
            if (target + 1)
            {
                CreatureFollow(CUNIT[idx], m_player[target]);
                AggressionLevel(CUNIT[idx], 1.0);
            }
        }
        idx = (idx + 1) % 180;
        PushTimerQueue(1, ptr, LineMonsterAI);
    }
}

void SpawnLineMonsters(int ptr)
{
    int lv = GetDirection(ptr), cur;

    if (IsObjectOn(ptr))
    {
        cur = ToInt(GetObjectZ(ptr + 1));
        if (cur < GetDirection(ptr + 1))
        {
            //14~36 23counts
            CUNIT[ToInt(GetObjectZ(ptr + 1))] = CallFunctionWithArgInt(m_spawnFn[ Random(0, 2) + (lv * 2)], Random(14, 36));
            Raise(ptr + 1, ToFloat(cur + 1));
            PushTimerQueue(1, ptr, SpawnLineMonsters);
        }
        else
        {
            PlayWav(SOUND_SwitchToggle);
            char buff[64];

            lv+=1;
            NoxSprintfString(&buff, "지금 %d 라운드가 시작됩니다",&lv, 1);
            UniPrintToAll(ReadStringAddressEx(&buff));
            Raise(ptr + 1, ToFloat(0));
        }
    }
}

static void NextStage(int ptr)
{
    int lv = GetDirection(ptr);
    char buff[128];

    if (IsObjectOn(ptr))
    {
        if (lv < GAME_MAX_WAVE)
        {
            Raise(ptr, ToFloat(0)); //kills SetTo 0
            LookWithAngle(ptr + 1, 80 + ((m_userCount - 1) * 10));
            PlayWav(SOUND_QuestPlayerJoinGame);
            lv+=1;
            NoxSprintfString(&buff, "[!!] 잠시 후 %d 웨이브가 시작됩니다. 미리 준비하세요", &lv, 1);
            UniPrintToAll(ReadStringAddressEx(&buff));
            SecondTimerWithArg(5, ptr, SpawnLineMonsters);
        }
        else
        {
            NoxSprintfString(&buff, "축하합니다. %d 웨이브를 모두 클리어했습니다! 대단합니다",&lv,1);
            UniPrintToAll(ReadStringAddressEx(&buff));
            PlayWav(SOUND_ArcheryContestBegins);
            PushTimerQueue(1, 35, TeleportAllPlayers);
            PushTimerQueue(20, 35, VictoryEvent);
        }
    }
}

void initGuardianFunctions()
{
    m_guardianFn[0] = GuardianRedWizard;
    m_guardianFn[1] =GuardianNinja;
    m_guardianFn[2] =GuardianHidden;
}

void wolfOnWalking(int wolf)
{
    int dur;

    if (IsVisibleTo(wolf+1, wolf))
    {
        dur = GetDirection(wolf+1);
        if (dur)
        {
            PushTimerQueue(1, wolf, wolfOnWalking);
            LookWithAngle(wolf+1, dur-1);
            MoveObjectVector(wolf, UnitAngleCos(wolf, 14.0), UnitAngleSin(wolf, 14.0));
            if (dur&1)
                Walk(wolf, GetObjectX(wolf),GetObjectY(wolf));
            return;
        }
    }
    Delete(wolf++);
    Delete(wolf++);
}

void wolfOnCollide()
{
    if (!MaxHealth(SELF))
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            float xpos =GetObjectX(SELF),ypos=GetObjectY(SELF);
            GreenExplosion(xpos,ypos);
            DeleteObjectTimer(CreateObjectAt("ForceOfNatureCharge", xpos,ypos), 48);
            SplashDamage(GetOwner(SELF), 200, 160.0, xpos,ypos);
            PlaySoundAround(SELF, SOUND_HecubahDieFrame439);
            Delete(SELF);
        }
    }
}

void startWolfWalking()
{
    int wolf=CreateObjectAt("WhiteWolf", GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));

    PushTimerQueue(1, wolf, wolfOnWalking);
    LookWithAngle( CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER)), 42);
    SetOwner(OTHER, wolf);
    SetUnitFlags(wolf, GetUnitFlags(wolf) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    LookWithAngle(wolf, GetDirection(OTHER));
    Frozen(wolf, TRUE);
    SetCallback(wolf, 9, wolfOnCollide);
    GreenSparkAt(GetObjectX(wolf), GetObjectY(wolf));
}

int DispositionWolfSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty2);
    return sd;
}

void dropRainSingle(float xpos, float ypos)
{
    int angle = Random(0, 359);
        float    rgap=RandomFloat(10.0, 200.0);
        int    arw= CreateObjectAt("CherubArrow", xpos+MathSine(angle+90, rgap),ypos+MathSine(angle,rgap));
            LookWithAngle(arw, 64);
            UnitNoCollide(arw);
            Raise(arw, 250.0);
            DeleteObjectTimer(arw, 20);
            PlaySoundAround(arw, SOUND_CrossBowShoot);
}

void arrowRainLoop(int sub)
{
    float xpos=GetObjectX(sub),ypos=GetObjectY(sub);
    int dur;

    while (ToInt(xpos))
    {
        dur=GetDirection(sub);
        if (dur)
        {
            dropRainSingle(xpos, ypos);
            if (dur&1)
                SplashDamage(GetOwner(sub), 15, 200.0, xpos, ypos);
            PushTimerQueue(1, sub, arrowRainLoop);
            LookWithAngle(sub, dur-1);
            break;
        }
        WispDestroyFX(xpos, ypos);
        Delete(sub);
        break;
    }
}

void startArrowRain()
{
    int ix,iy;

    if(! GetPlayerMouseXY(OTHER, &ix,&iy))
        return;
    
    float x=IntToFloat(ix), y=IntToFloat(iy);

    if (!checkCoorValidate(x,y))
        return;

    int sub=CreateObjectAt("PlayerWaypoint", x,y);

    PushTimerQueue(1, sub, arrowRainLoop);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, 60);
    PlaySoundAround(sub, SOUND_MetallicBong);
}

int DispositionArrowrainSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty1);
    return sd;
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 135, DAMAGE_TYPE_ELECTRIC);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.1);
            Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
    }
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 19.0), yvect = UnitAngleSin(OTHER, 19.0);
    int arr[30], u=0;

    arr[u] = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateById(OBJ_ARCHER, GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    DrawYellowLightningFx(GetObjectX(arr[0]), GetObjectY(arr[0]), GetObjectX(arr[u-1]), GetObjectY(arr[u-1]), 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}

int DispositionThunderLightningSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_stun3, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty3);
    return sd;
}

void OnIcecrystalCollide()
{
    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(GetTrigger()+1);

            if (!IsAttackedBy(OTHER, owner))
                break;

            Damage(OTHER, owner, 275, DAMAGE_TYPE_ELECTRIC);
            Enchant(OTHER, "ENCHANT_FREEZE", 2.0);
        }
        else if (GetCaller())
            break;

        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(GetTrigger() + 1);
        Delete(SELF);
        break;
    }
}

int CreateIceCrystal(int owner, float gap, int lifetime)
{
    int cry=CreateObjectAt("GameBall", GetObjectX(owner) + UnitAngleCos(owner, gap), GetObjectY(owner) + UnitAngleSin(owner, gap));

    SetOwner(owner, CreateObjectAt("ImaginaryCaster", GetObjectX(cry), GetObjectY(cry)));
    SetUnitCallbackOnCollide(cry, OnIcecrystalCollide);
    DeleteObjectTimer(cry, lifetime);
    DeleteObjectTimer(cry+1, lifetime + 15);
    SetUnitFlags(cry, GetUnitFlags(cry) ^ (UNIT_FLAG_AIRBORNE|UNIT_FLAG_SHORT));
    return cry;
}

void IceCrystalAxeTriggered()
{
    PushObject(CreateIceCrystal(OTHER, 19.0, 75), 22.0, GetObjectX(OTHER), GetObjectY(OTHER));
    WispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(OTHER, SOUND_LightningWand);
    PlaySoundAround(OTHER, SOUND_ElevLOTDDown);
}

int DispositionIceCrystalAxe(float xpos, float ypos)
{
    int sd=CreateObjectAt("BattleAxe", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty4);
    return sd;
}

void StrikeFlameCollide()
{
    while (TRUE)
    {
        if (!GetTrigger())
            break;
        int owner = GetOwner(SELF);

        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            int hash = GetUnit1C(SELF);

            if (!HashGet(hash, GetCaller(), NULLPTR, FALSE))
            {
                Damage(OTHER, owner, 135, DAMAGE_TYPE_EXPLOSION);
                HashPushback(hash, GetCaller(), TRUE);
            }
        }
        break;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

int BurningZombieFx(float xProfile, float yProfile)
{
    int fx;

    CreateObjectAtEx("Zombie", xProfile, yProfile, &fx);
    UnitNoCollide(fx);
    ObjectOff(fx);
    Damage(fx, 0, 999, DAMAGE_TYPE_FLAME);
    return fx;
}

#define STRIKE_FLAME_MARK_UNIT 0
#define STRIKE_FLAME_XVECT 1
#define STRIKE_FLAME_YVECT 2
#define STRIKE_FLAME_COUNT 3
#define STRIKE_FLAME_OWNER 4
#define STRIKE_FLAME_HASHOBJECT 5
#define STRIKE_FLAME_MAX 6

void StrikeFlameLoop(int *pArgs)
{
    int owner=pArgs[STRIKE_FLAME_OWNER];
    while (TRUE)
    {
        if (CurrentHealth(owner))
        {
            int *count=ArrayRefN(pArgs, STRIKE_FLAME_COUNT), mark = pArgs[STRIKE_FLAME_MARK_UNIT];

            if (count[0] && IsVisibleTo(mark, mark+1))
            {
                PushTimerQueue(1, pArgs, StrikeFlameLoop);
                float xvect = pArgs[STRIKE_FLAME_XVECT], yvect=pArgs[STRIKE_FLAME_YVECT];

                MoveObjectVector(mark, xvect, yvect);
                MoveObjectVector(mark+1, xvect, yvect);
                int dum=DummyUnitCreateById(OBJ_DEMON, GetObjectX(mark), GetObjectY(mark));

                SetUnit1C(dum, pArgs[STRIKE_FLAME_HASHOBJECT]);
                SetCallback(dum, 9, StrikeFlameCollide);
                SetOwner(owner, dum);
                DeleteObjectTimer(dum, 1);
                DeleteObjectTimer( BurningZombieFx(GetObjectX(mark), GetObjectY(mark)), 12);
                
                --count[0];
                break;
            }
        }
        HashDeleteInstance(pArgs[STRIKE_FLAME_HASHOBJECT]);
        FreeSmartMemEx(pArgs);
        break;
    }
}

void StrikeFlameTriggered(int caster)
{
    float xvect=UnitAngleCos(caster, 17.0), yvect=UnitAngleSin(caster, 17.0);
    int mark = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) + xvect, GetObjectY(caster) + yvect);
    int visCheck=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) - xvect, GetObjectY(caster) - yvect);

    int *args;

    AllocSmartMemEx(STRIKE_FLAME_MAX*4, &args);
    args[STRIKE_FLAME_MARK_UNIT]=mark;
    args[STRIKE_FLAME_XVECT]=xvect;
    args[STRIKE_FLAME_YVECT]=yvect;
    args[STRIKE_FLAME_COUNT]=30; //count
    args[STRIKE_FLAME_OWNER]=caster;

    int hash;
    HashCreateInstance(&hash);
    args[STRIKE_FLAME_HASHOBJECT]=hash;
    PushTimerQueue(1, args, StrikeFlameLoop);
}

void StartBurningFlame()
{
    StrikeFlameTriggered(GetCaller());
    PlaySoundAround(OTHER, SOUND_MeteorShowerCast);
}

int DispositionRisingFlameAxe(float xpos, float ypos)
{
    int sd=CreateObjectAt("BattleAxe", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty5);
    return sd;
}

#define ARROW_PULL_FORCE 2.2

int arrowWaveCreateSingle(int posUnit, int owner, int angle, float dx, float dy)
{
    int arw=CreateObjectAt("ArcherArrow", GetObjectX(posUnit), GetObjectY(posUnit));

    SetOwner(owner, arw);
    LookWithAngle(arw, angle);
    PushObjectTo(arw, -(dx*ARROW_PULL_FORCE), -(dy*ARROW_PULL_FORCE));
    return arw;
}

void arrowWaveHandler(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int dur = GetDirection(sub);
        float dx=GetObjectZ(sub), dy=GetObjectZ(sub+1);

        if (dur)
        {
            PushTimerQueue(1, sub, arrowWaveHandler);
            LookWithAngle(sub, dur-1);
            arrowWaveCreateSingle(sub, GetOwner(sub), GetDirection(sub+1), dx, dy);
            arrowWaveCreateSingle(sub+1, GetOwner(sub), GetDirection(sub+1), dx, dy);
            MoveObjectVector(sub, -dy, dx);
            MoveObjectVector(sub+1, dy, -dx);
            GreenLightningFx(FloatToInt(GetObjectX(sub)), FloatToInt(GetObjectY(sub)), 
                FloatToInt(GetObjectX(sub+1)), FloatToInt(GetObjectY(sub+1)), 12);
            return;
        }
        Delete(sub);
        Delete(sub+1);
    }
}

#define WAVE_GAP_FVALUE 10.0
void StartArrowWave()
{
    float cx=GetObjectX(OTHER), cy=GetObjectY(OTHER);
    float dx = UnitAngleCos(OTHER, -WAVE_GAP_FVALUE), dy = UnitAngleSin(OTHER, -WAVE_GAP_FVALUE);
    int mark=CreateObjectAt("InvisibleLightBlueLow", cx+dx+(dy*WAVE_GAP_FVALUE), cy+dy-(dx*WAVE_GAP_FVALUE));
    
    CreateObjectAt("InvisibleLightBlueLow", cx+dx-(dy*WAVE_GAP_FVALUE), cy+dy+(dx*WAVE_GAP_FVALUE));
    LookWithAngle(mark, 22);
    LookWithAngle(mark+1, GetDirection(OTHER));
    Raise(mark, dx);
    Raise(mark+1, dy);
    SetOwner(OTHER, mark);
    PushTimerQueue(1, mark, arrowWaveHandler);
    PlaySoundAround(mark, SOUND_GlyphCast);
}

int DispositionArrowWaveHammer(float xpos, float ypos)
{
    int sd=CreateObjectAt("WarHammer", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_impact4, ITEM_PROPERTY_venom4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty6);
    return sd;
}

void onyellowRushCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, GetOwner(SELF)))
        {
            int hash = GetUnit1C(SELF);

            if (!HashGet(hash, GetCaller(), NULLPTR, FALSE))
            {
                Damage(OTHER, GetOwner(SELF), 130, DAMAGE_TYPE_CRUSH);
                if (CurrentHealth(OTHER))
                    HashPushback(hash, GetCaller(), TRUE);
            }
        }
    }
}

void onyellowRushHandler(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int dur = GetDirection(sub), owner=GetOwner(sub), hash=GetUnit1C(sub);
        if (dur && CurrentHealth(owner))
        {
            float x=GetObjectX(owner),y=GetObjectY(owner);
            float xvect=GetObjectZ(sub), yvect=GetObjectZ(sub+1);
            if (IsVisibleTo(sub, sub+1))
            {
                LookWithAngle(sub, dur-1);
                PushTimerQueue(1, sub, onyellowRushHandler);
                MoveObject(sub, x-xvect, y-yvect);
                MoveObject(sub+1, x+xvect, y+yvect);
                int dam=DummyUnitCreateById(OBJ_DEMON, x-(xvect/14.0), y-(yvect/14.0));

                SetOwner(owner, dam);
                SetUnit1C(dam, hash);
                SetCallback(dam, 9, onyellowRushCollide);
                DeleteObjectTimer(dam, 1);
                Effect("YELLOW_SPARKS", x,y,0.0,0.0);
                return;
            }
        }
        HashDeleteInstance(hash);
        Delete(sub++);
        Delete(sub++);
    }
}

void StartYellowRush()
{
    float xvect=UnitAngleCos(OTHER, 14.0), yvect=UnitAngleSin(OTHER, 14.0);
    int sub=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER)-xvect, GetObjectY(OTHER)-yvect);

    Raise( CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect), yvect);
    Raise(sub, xvect);
    LookWithAngle(sub, 21);
    SetOwner(OTHER, sub);
    int hash;
    HashCreateInstance(&hash);
    SetUnit1C(sub, hash);
    PlaySoundAround(sub, SOUND_SummonAbort);
    PushTimerQueue(1, sub, onyellowRushHandler);
}

int DispositionYellowRushSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_impact4, ITEM_PROPERTY_Speed4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty7);
    return sd;
}

void TurnUndeadHammerTriggered()
{
    float x=GetObjectX(OTHER),y=GetObjectY(OTHER);
    int m=CreateObjectAt("Imp", x,y);
    SetOwner(GetMasterUnit(), m);
    CastSpellObjectObject("SPELL_TURN_UNDEAD", m,m);
    Delete(m);
    SplashDamage(OTHER, 160, 135.0, x,y);
}

int DispositionTurnUndeadHammer(float xpos, float ypos)
{
    int sd=CreateObjectAt("BattleAxe", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_confuse3, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty8);
    return sd;
}

int placingSmallFist(float x, float y)
{
    int mark=CreateObjectAt("InvisibleLightBlueLow", x,y);
    CastSpellLocationLocation("SPELL_FIST", x-1.0,y,x,y);
    Delete(mark);
    return ++mark;
}

void StrikeFistLine()
{
    float xvect=UnitAngleCos(OTHER, 46.0), yvect=UnitAngleSin(OTHER, 46.0);
    int mark=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);
    int count = 12,fist;

    while (--count>=0)
    {
        if (!IsVisibleTo(OTHER, mark))
            break;
        fist=placingSmallFist(GetObjectX(mark), GetObjectY(mark));
        SetOwner(OTHER, fist);
        MoveObjectVector(mark, xvect, yvect);
    }
}

int DispositionFistLine(float xpos, float ypos)
{
    int sd=CreateObjectAt("WarHammer", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_impact4, ITEM_PROPERTY_vampirism4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty9);
    return sd;
}

#define PUSH_KILLERIAN_DM 0
#define PUSH_KILLERIAN_COUNT 1
#define PUSH_KILLERIAN_XVECT 2
#define PUSH_KILLERIAN_YVECT 3
#define PUSH_KILLERIAN_MAX 4

void onPushKillerianLoop(int *p)
{
    int count=p[PUSH_KILLERIAN_COUNT];
    int dm=p[PUSH_KILLERIAN_DM];

    if (count)
    {
        float xvect= p[PUSH_KILLERIAN_XVECT], yvect=p[PUSH_KILLERIAN_YVECT];
        int sub=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(dm)+xvect,GetObjectY(dm)+yvect);

        DeleteObjectTimer(sub, 1);
        if (IsVisibleTo(dm, sub))
        {
            PushTimerQueue(1, p, onPushKillerianLoop);
            MoveObjectVector(dm,xvect, yvect);
            --p[PUSH_KILLERIAN_COUNT];
            return;
        }
    }
    Frozen(dm,FALSE);
    ObjectOn(dm);
    FreeSmartMemEx(p);
}

void PushKillerian()
{
    int cx,cy;

    if(! GetPlayerMouseXY(OTHER, &cx, &cy))
        return;

    float fromPoint[]={GetObjectX(OTHER), GetObjectY(OTHER)};
    float toPoint[]={ IntToFloat(cx),IntToFloat(cy) };
    float vect[2];
    float scale = Distance(fromPoint[0], fromPoint[1], toPoint[0], toPoint[1]);

    ComputePointRatio(  toPoint,fromPoint, vect, scale/9.0);
    int *m;

    AllocSmartMemEx(PUSH_KILLERIAN_MAX*4, &m);
    PushTimerQueue(1, m, onPushKillerianLoop);
    int dm=DummyUnitCreateById(OBJ_DEMON, fromPoint[0], fromPoint[1]);
    LookWithAngle(dm,GetDirection(OTHER));
    SetOwner(OTHER,dm);
    SetUnitFlags(dm, GetUnitFlags(dm)^UNIT_FLAG_NO_COLLIDE_OWNER);
    m[PUSH_KILLERIAN_COUNT]=9;
    m[PUSH_KILLERIAN_DM]=dm;
    m[PUSH_KILLERIAN_XVECT]=vect[0];
    m[PUSH_KILLERIAN_YVECT]=vect[1];
}

int DispositionPushKillerianHam(float xpos, float ypos)
{
    int sd=CreateObjectAt("WarHammer", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_impact4, ITEM_PROPERTY_vampirism4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyA);
    return sd;
}

void initSpecialWeapons()
{
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_VIOLET_SPARK, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty1);
    SpecialWeaponPropertyExecuteScriptCodeGen(startArrowRain, &m_pWeaponUserSpecialPropertyExecutor1);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyExecutor1);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty2);
    SpecialWeaponPropertyExecuteScriptCodeGen(startWolfWalking, &m_pWeaponUserSpecialPropertyExecutor2);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyExecutor2);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_BLUE_SPARKS, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty3);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &m_pWeaponUserSpecialPropertyExecutor3);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyExecutor3);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_PLASMA_SPARK, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty4);
    SpecialWeaponPropertyExecuteScriptCodeGen(IceCrystalAxeTriggered, &m_pWeaponUserSpecialPropertyExecutor4);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyExecutor4);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_THIN_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty5);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartBurningFlame, &m_pWeaponUserSpecialPropertyExecutor5);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyExecutor5);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_THIN_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode6);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty6);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartArrowWave, &m_pWeaponUserSpecialPropertyExecutor6);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty6, m_pWeaponUserSpecialPropertyFxCode6);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty6, m_pWeaponUserSpecialPropertyExecutor6);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_LESSER_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode7);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty7);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartYellowRush, &m_pWeaponUserSpecialPropertyExecutor7);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty7, m_pWeaponUserSpecialPropertyFxCode7);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty7, m_pWeaponUserSpecialPropertyExecutor7);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode8);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty8);
    SpecialWeaponPropertyExecuteScriptCodeGen(TurnUndeadHammerTriggered, &m_pWeaponUserSpecialPropertyExecutor8);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty8, m_pWeaponUserSpecialPropertyFxCode8);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty8, m_pWeaponUserSpecialPropertyExecutor8);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode9);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty9);
    SpecialWeaponPropertyExecuteScriptCodeGen(StrikeFistLine, &m_pWeaponUserSpecialPropertyExecutor9);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty9, m_pWeaponUserSpecialPropertyFxCode9);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty9, m_pWeaponUserSpecialPropertyExecutor9);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeA);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyA);
    SpecialWeaponPropertyExecuteScriptCodeGen(PushKillerian, &m_pWeaponUserSpecialPropertyExecutorA);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyA, m_pWeaponUserSpecialPropertyFxCodeA);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyA, m_pWeaponUserSpecialPropertyExecutorA);
}

#define SPECIAL_WEAPON_MAX_COUNT 8

void initspecialWeaponShop()
{
    string desc[]={
        "턴언데드 엑스", "화살비 서드", "화살파도 망치", "아이스크리스탈 엑스", 
    "불타는불꽃엑스",
    "라이트닝서드", "늑데돌진서드", "노랑돌진서드"};
    int pay[]={51308, 99930, 51040, 30000, 
    42000,
    33750,30100,37550};

    m_specialWeaponNameArray = &desc;
    m_specialWeaponPays=&pay;
    int specialFn[]={DispositionTurnUndeadHammer ,DispositionArrowrainSword, DispositionArrowWaveHammer, DispositionIceCrystalAxe, 
    DispositionRisingFlameAxe,
        DispositionThunderLightningSword, DispositionWolfSword, DispositionYellowRushSword};

    m_specialWeaponFn=&specialFn;
}

void specialWeaponDesc()
{
    int pIndex=GetPlayerIndex(OTHER);
    int *p;

    if (!HashGet(m_genericHash, GetTrigger(), &p, FALSE))
        return;
    int cur= p[pIndex];
    int args[]={StringUtilGetScriptStringPtr( m_specialWeaponNameArray[cur]), m_specialWeaponPays[cur]};
    char buff[192];
    NoxSprintfString(&buff, "제품 %s 을 구입하려면, 예를 누르세요. 가격은 %d입니다", &args, sizeof(args));
    UniPrint(OTHER, ReadStringAddressEx(&buff));
    TellStoryUnitName("AA", "bindevent:NullEvent", m_specialWeaponNameArray[cur]);
}

int CreateSpecialWeaponProto(int functionId, float x,float y)
{
    StopScript( Bind(functionId, &functionId + 4) );
}

void specialWeaponTrade()
{
    int pIndex=GetPlayerIndex(OTHER), dlgRes = GetAnswer(SELF);
    int *p;

    if (!HashGet(m_genericHash, GetTrigger(), &p, FALSE))
        return;
    int cur=p[pIndex];
    if (dlgRes==2)
    {
        UniPrint(OTHER, "'아니오'을 누르셨어요. 다음 항목을 보여드리겠어요");
        p[pIndex]=(cur+1)%SPECIAL_WEAPON_MAX_COUNT;
        specialWeaponDesc();
    }
    else if (dlgRes==1)
    {
        if (GetGold(OTHER)>=m_specialWeaponPays[cur])
        {
            ChangeGold(OTHER, -m_specialWeaponPays[cur]);
            CreateSpecialWeaponProto(m_specialWeaponFn[cur], GetObjectX(OTHER), GetObjectY(OTHER));
            UniPrint(OTHER, "거래완료!- 당신 아래에 있어요");
        }
        else
            UniPrint(OTHER, "거래실패!- 잔액이 부족합니다");
    }
}

int PlaceSpecialWeaponShop(int location)
{
    int s=DummyUnitCreateById(OBJ_WIZARD_WHITE, LocationX(location), LocationY(location));

    SetDialog(s, "YESNO", specialWeaponDesc, specialWeaponTrade);
    int *p;

    AllocSmartMemEx(32*4, &p);
    NoxDwordMemset(p, 32, 0);
    HashPushback(m_genericHash, s, p);
    return s;
}

void testThisMap()
{
    DispositionPushKillerianHam(LocationX(112), LocationY(112));
}

void PlayerClassOnShutdown(int rep)
{
    m_player[rep]=0;
    m_pFlags[rep]=0;
    --m_userCount;
}

void PlayerClassOnDeath(int rep, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "> %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

#define WINDBOOSTER_DISTANCE 70.0

void skillSetWindBooster(int pUnit)
{
    PushObjectTo(pUnit, UnitAngleCos(pUnit, WINDBOOSTER_DISTANCE), UnitAngleSin(pUnit, WINDBOOSTER_DISTANCE));
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void PlayerClassOnAlive(int plr, int user)
{
    if (UnitCheckEnchant(user, GetLShift(ENCHANT_SNEAK)))
    {
        EnchantOff(user, "ENCHANT_SNEAK");
        RemoveTreadLightly(user);
        if (PlayerClassCheckFlag(plr, PLAYER_WINDBOOST_FLAG))
            skillSetWindBooster(user);
    }
}

void PlayerClassOnLoop()
{
    int rep = sizeof(m_player);

    while (--rep>=0)
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[rep]))
            {
                if (GetUnitFlags(m_player[rep]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[rep]))
                {
                    PlayerClassOnAlive(rep, m_player[rep]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(rep, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(rep, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(rep, m_player[rep]);
                    }
                    break;
                }                
            }
            if (m_pFlags[rep])
                PlayerClassOnShutdown(rep);
            break;
        }
    }
    PushTimerQueue(1, 0, PlayerClassOnLoop);
}

void deferredInit()
{
    UniPrintToAll("[테스트 메시지]: 맵이 정상적으로 동작되고 있습니다");
    InitTombGuardianNPC();
    WUpgradeTable(0);
    PushTimerQueue(180,0,  MapLightingSetup);
}


void MapInitialize()
{
    OnInitializeMap();
    HashCreateInstance(&m_genericHash);
    initMapReadable();
    initUserRespawnLocation();
    SettingItemShop();
    initGuardianFunctions();
    initSpwnFunctions();
    PushTimerQueue(10,0, PlayerClassOnLoop);
    PushTimerQueue(60,0, StartGameMent);
    PushTimerQueue(1,0, deferredInit);
    initSpecialWeapons();
    initspecialWeaponShop();
    PlaceSpecialWeaponShop(174);
    PlaceSpecialWeaponShop(175);
}

void YouCantJoin()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
    MoveObject(OTHER, LocationX(9), LocationY(9));
    UniPrint(OTHER,"당신은 이 맵에 입장할 수 없습니다, 이 문제가 계속되면 맵 제작자에게 보고하십시오");
}

static void PlayWav(int soundId)
{
    int k=sizeof(m_player);

    while (--k>=0)
    {
        if (CurrentHealth(m_player[k]))
            PlaySoundAround(m_player[k], soundId);
    }
}

void MapExit()
{
    OnShutdownMap();
}

int GetNearlyPlayer(int unit)
{
    float temp = 9999.0, r;
    int k = sizeof(m_player), res = -1;

    while (--k>=0)
    {
        if (CurrentHealth(m_player[k]))
        {
            r = DistanceUnitToUnit(m_player[k], unit);
            if (r < temp)
            {
                temp = r;
                res = k;
            }
        }
    }
    return res;
}

int GetCurrentLevel()
{
    if (IsObjectOn(ToInt(GetObjectZ(GetMasterUnit() + 1))))
    return GetDirection(ToInt(GetObjectZ(GetMasterUnit() + 1)));
    else return 0;
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
	HashPushback(pInstance, 1179, WarShurikenEffect);
	HashPushback(pInstance, 1177, WarChakramEffect);
	HashPushback(pInstance, 526, HarpoonEvent);
	HashPushback(pInstance, 709, ShotMagicMissile);
	HashPushback(pInstance, 706, ShootFireball);
}

void WarChakramEffect(int cur,int owner)
{
    float x = GetObjectX(cur), y = GetObjectY(cur);
    MoveObject(cur, 100.0, 100.0);
    Enchant(cur, "ENCHANT_SHIELD", 0.0);
    Enchant(cur, "ENCHANT_PROTECT_FROM_POISON", 0.0);
    Enchant(cur, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
    Enchant(cur, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
    MoveObject(cur, x, y);
    PushObjectTo(cur, UnitRatioX(cur, owner, 55.0), UnitRatioY(cur, owner, 55.0));
}

void WarShurikenEffect(int cur, int owner)
{
    if (CurrentHealth(owner) && IsPlayerUnit(owner) && !(MaxHealth(owner) ^ 150))
    {
        int mis = CreateObjectAt("WeakFireball", GetObjectX(cur), GetObjectY(cur));
        SetOwner(owner, mis);
        PushObjectTo(mis, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
    }
}

void ShootFireball(int c,int owner)
{
    if (CurrentHealth(owner))
    {
        DeleteObjectTimer(CreateObjectAt("ManaBombCharge", GetObjectX(owner), GetObjectY(owner)), 20);
        PlaySoundAround(owner, SOUND_ManaBombCast);
        SplashDamage(owner, 125, 130.0, GetObjectX(owner),GetObjectY(owner));
    }Delete(c);
}

void ShotMagicMissile(int cur,int owner)
{
    if (CurrentHealth(owner))
    {
        int ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner));
        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", owner, GetObjectX(owner) + UnitAngleCos(owner, 30.0), GetObjectY(owner) + UnitAngleSin(owner, 30.0));
        Delete(ptr);
        Delete(ptr + 2);
        Delete(ptr + 3);
        Delete(ptr + 4);
    }
    Delete(cur);
}

void HarpoonEvent(int cur,int owner)
{
    int ptr;
    if (CurrentHealth(owner))
    {
        if (HasEnchant(owner, "ENCHANT_VILLAIN"))
        {
            ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) - UnitRatioX(owner, cur, 28.0), GetObjectY(cur) - UnitRatioY(owner, cur, 28.0));
            Delete(cur);
            Raise(ptr, -UnitRatioX(owner, cur, 28.0));
            Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr),GetObjectY(ptr)), -UnitRatioY(owner, cur, 28.0));
            SetOwner(owner, ptr);
            PushTimerQueue(1, ptr, HurricanseWavePar);
        }
    }
}

void HurricanseWavePar(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(owner) && IsObjectOn(ptr))
    {
        if (count < 30 && CheckUnitLimitPos(ptr))
        {
            int unit = CreateObjectAt("ShopkeeperLandOfTheDead", GetObjectX(ptr), GetObjectY(ptr));

            PlaySoundAround(unit, SOUND_SwordMissing);
            DeleteObjectTimer(CreateObjectAt("WhirlWind", GetObjectX(unit),GetObjectY(unit)), 9);
            Frozen(unit, TRUE);
            SetCallback(unit, 9, HurricanTouch);
            SetOwner(owner, unit);
            DeleteObjectTimer(unit, 1);
            MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr), GetObjectY(ptr) + GetObjectZ(ptr + 1));
            LookWithAngle(ptr, count + 1);
        }
        else
            ObjectOff(ptr);
        PushTimerQueue(1, ptr, HurricanseWavePar);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void HurricanTouch()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 150, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
    }
}



void TeleportAllPlayers(int wp)
{
    int k = sizeof(m_player);

    while (--k>=0)
    {
        if (CurrentHealth(m_player[k]))
            MoveObject(m_player[k], LocationX(wp), LocationY(wp));
    }
}


void VictoryEvent(int wp)
{
    TeleportLocation(53, LocationX(wp), LocationY(wp));
    Effect("WHITE_FLASH", LocationX(53), LocationY(53), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("VortexSource", wp), 900);
    DeleteObjectTimer(CreateObject("LevelUp", wp), 900);
    PushTimerQueue(1, 0, StrYourWinner);
    UniPrintToAll("승리하셨습니다");
    AudioEvent("BigGong", wp);
    AudioEvent("LongBellsDown", wp);
}

int CheckPlayer()
{
    int rep = sizeof(m_player);

    while(--rep>=0)
    {
        if (IsCaller(m_player[rep]))
            break;
    }
    return rep;
}

int CheckPlayerWithId(int pUnit)
{
    int rep=sizeof(m_player);

    while (--rep>=0)
    {
        if (m_player[rep]^pUnit)
            continue;       
        break;
    }
    return rep;
}

int PlayerClassOnInit(int plr, int pUnit, char *userInit)
{
    m_player[plr]=pUnit;
    m_pFlags[plr]=1;
    Enchant(pUnit, "ENCHANT_BLINDED", 0.8);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    EmptyAll(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));
    ++m_userCount;
    return plr;
}

void TeleportPlayerArbitrary(int user)
{
    short respawnPoint = m_userRespawnLocation[Random(0, m_userRespawnLocationCount-1)];

    MoveObject(user, LocationX(respawnPoint), LocationY(respawnPoint));
    DeleteObjectTimer(CreateObjectAt("BlueRain", GetObjectX(user), GetObjectY(user)), 12);
    PlaySoundAround(user, SOUND_WallOff);
}

void PlayerClassOnJoin(int plr)
{
    int user = m_player[plr];

    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    TeleportPlayerArbitrary(user);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
    EnchantOff(user,"ENCHANT_ANTI_MAGIC");
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(12), LocationY(12));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int plr)
{
    MoveObject(user, LocationX(9), LocationY(9));
    Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    PushTimerQueue(3, plr, PlayerClassOnJoin);
    UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
}

void thisMapInformationPrintMessage(int user)
{
    PlaySoundAround(user, SOUND_JournalEntryAdd);
    UniPrint(user, "<<---블러드 페스티벌--제작. 237------------------------------------------------<");
}

void PlayerClassOnEntry(int plrUnit)
{
    while (TRUE)
    {
        if (!CurrentHealth(plrUnit))
            break;
        int plr = CheckPlayerWithId(plrUnit), rep = sizeof(m_player);
        char initialUser=FALSE;

        while (--rep>=0&&plr<0)
        {
            if (!MaxHealth(m_player[rep]))
            {
                plr = PlayerClassOnInit(rep, plrUnit, &initialUser);
                PushTimerQueue(66, plrUnit, thisMapInformationPrintMessage);
                break;
            }
        }
        if (plr >= 0)
        {
            if (initialUser)
                OnPlayerDeferredJoin(plrUnit, plr);
            else
                PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(plrUnit);
        break;
    }
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (CheckPlayer() >= 0)
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(9), LocationY(9));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

static void NetworkUtilClientMain()
{
    DoInitResources();
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);
    
    // if (pUnit)
    //     ShowQuestIntroOne(pUnit, 237, "NoxWorldMap", "Noxworld.wnd:NotReady");

    if (pInfo==0x653a7c)
    {
        DoInitResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
