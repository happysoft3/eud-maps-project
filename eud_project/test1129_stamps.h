
#include "libs/define.h"
#include "libs/format.h"
#include "libs/printutil.h"

#define PRINT_ALL -1

void PrintMessageFormatOne(int user, string fmt, int arg)
{
    char message[192];

    NoxSprintfString(message, fmt, &arg, 1);
    if (user==-1)
    {
        UniPrintToAll(ReadStringAddressEx(message));
        return;
    }
    UniPrint(user, ReadStringAddressEx(message));
}


void StrDrawPicture()
{
	int arr[23];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 1076356092; arr[1] = 1338309759; arr[2] = 9439232; arr[3] = 538050816; arr[4] = 41820162; arr[5] = 4719616; arr[6] = 134774793; arr[7] = 18878464; arr[8] = 539099172; arr[9] = 83640320; 
	arr[10] = 1065877648; arr[11] = 269549569; arr[12] = 2097696; arr[13] = 1078198272; arr[14] = 1887438976; arr[15] = 17825823; arr[16] = 1090527490; arr[17] = 69206080; arr[18] = 536379912; arr[19] = 1946091778; 
	arr[20] = 132655; arr[21] = 2040; arr[22] = 524416; 
	while(i < 23)
	{
		drawStrDrawPicture(arr[i], name);
		i ++;
	}
}

void drawStrDrawPicture(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(25);
		pos_y = GetWaypointY(25);
	}
	for (i = 1 ; i > 0 && count < 713 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 25), 1);
		if (count % 64 == 63)
			MoveWaypoint(25, GetWaypointX(25) - 189.000000, GetWaypointY(25) + 3.000000);
		else
			MoveWaypoint(25, GetWaypointX(25) + 3.000000, GetWaypointY(25));
		count ++;
	}
	if (count >= 713)
	{
		count = 0;
		MoveWaypoint(25, pos_x, pos_y);
	}
}

void StrWatch()
{
	int arr[17];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 472376316; arr[1] = 67129284; arr[2] = 268959984; arr[3] = 2032144385; arr[4] = 537018407; arr[5] = 4195392; arr[6] = 255885321; arr[7] = 1179918; arr[8] = 237117665; arr[9] = 1047620; 
	arr[10] = 8915080; arr[11] = 286325762; arr[12] = 67371536; arr[13] = 68174916; arr[14] = 117968912; arr[15] = 1075843121; arr[16] = 537002015; 
	while(i < 17)
	{
		drawStrWatch(arr[i], name);
		i ++;
	}
}

void drawStrWatch(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(26);
		pos_y = GetWaypointY(26);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 26), 1);
		if (count % 48 == 47)
			MoveWaypoint(26, GetWaypointX(26) - 141.000000, GetWaypointY(26) + 3.000000);
		else
			MoveWaypoint(26, GetWaypointX(26) + 3.000000, GetWaypointY(26));
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(26, pos_x, pos_y);
	}
}

void StrEraiser()
{
	int arr[13];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 2115994366; arr[1] = 67650052; arr[2] = 17318032; arr[3] = 554177025; arr[4] = 553795616; arr[5] = 1346896880; arr[6] = 142606344; arr[7] = 268435732; arr[8] = 134095938; arr[9] = 134514754; 
	arr[10] = 10520608; arr[11] = 268501762; arr[12] = 2097216; 
	while(i < 13)
	{
		drawStrEraiser(arr[i], name);
		i ++;
	}
}

void drawStrEraiser(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(14);
		pos_y = GetWaypointY(14);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 14), 1);
		if (count % 36 == 35)
			MoveWaypoint(14, GetWaypointX(14) - 105.000000, GetWaypointY(14) + 3.000000);
		else
			MoveWaypoint(14, GetWaypointX(14) + 3.000000, GetWaypointY(14));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(14, pos_x, pos_y);
	}
}

void StrChangeColor()
{
	int arr[15];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 1648493192; arr[1] = 67440719; arr[2] = 237039625; arr[3] = 555753456; arr[4] = 539238441; arr[5] = 1212174980; arr[6] = 11046720; arr[7] = 1077729; arr[8] = 163328; arr[9] = 67110904; 
	arr[10] = 268959870; arr[11] = 268501504; arr[12] = 33816640; arr[13] = 1887404096; arr[14] = 7; 
	while(i < 15)
	{
		drawStrChangeColor(arr[i], name);
		i ++;
	}
}

void drawStrChangeColor(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(12);
		pos_y = GetWaypointY(12);
	}
	for (i = 1 ; i > 0 && count < 465 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 12), 1);
		if (count % 40 == 39)
			MoveWaypoint(12, GetWaypointX(12) - 117.000000, GetWaypointY(12) + 3.000000);
		else
			MoveWaypoint(12, GetWaypointX(12) + 3.000000, GetWaypointY(12));
		count ++;
	}
	if (count >= 465)
	{
		count = 0;
		MoveWaypoint(12, pos_x, pos_y);
	}
}

void StrShutdown()
{
	int arr[13];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 2116542978; arr[1] = 75513924; arr[2] = 268961936; arr[3] = 16847361; arr[4] = 539115556; arr[5] = 71828608; arr[6] = 142837816; arr[7] = 269549825; arr[8] = 34611234; arr[9] = 1090782274; 
	arr[10] = 415205408; arr[11] = 268501776; arr[12] = 2097664; 
	while(i < 13)
	{
		drawStrShutdown(arr[i], name);
		i ++;
	}
}

void drawStrShutdown(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(13);
		pos_y = GetWaypointY(13);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 13), 1);
		if (count % 36 == 35)
			MoveWaypoint(13, GetWaypointX(13) - 105.000000, GetWaypointY(13) + 3.000000);
		else
			MoveWaypoint(13, GetWaypointX(13) + 3.000000, GetWaypointY(13));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(13, pos_x, pos_y);
	}
}

