
#include "phx1185_utils.h"
#include "libs/grplib.h"

void ImageResourceMotIdleLeft()
{ }

void ImageResourceMotMoveLeft()
{ }

void ImageResourceMotJumpLeft()
{ }

void ImageResourceMotAttackLeft()
{ }

void ImageResourceMotAttack2Left()
{ }

void ImageResourceBottom()
{ }

void ImageResourceKeyData()
{ }

void ImageResourceWeapon1()
{ }
void ImageResourceWeapon2()
{ }
void ImageResourceWeapon3()
{ }
void ImageResourceWeapon4()
{ }
void ImageResourceWeapon5()
{ }

void ImageResourceMaya()
{ }

void ImageResourceNpc2()
{ }
void ImageResourceNpc3()
{ }

void ImageResourceMobL1()
{ }

void ImageResourceMobL2()
{ }

void ImageResourceMobL3()
{ }

void ImageResourceMobL4()
{ }

void ImageResourceMobL5()
{ }

void ImageResourceMobL6()
{ }

void ImageResourceMobL7()
{ }

void ImageResourceMobL8()
{ }

void ImageResourceMobL9()
{ }

void ImageResourceMobL10()
{ }

void ImageResourceWeapon6()
{ }
void ImageResourceWeapon7()
{ }

void ImageResourceMeso()
{ }

void ImageResourceGoHunt()
{ }
void ImageResourceGoQuest()
{ }
void ImageResourceGoShop()
{ }

void ImageResourceBoss1()
{ }
void ImageResourceBoss2()
{ }
void ImageResourceBoss3()
{ }
void ImageResourceBoss4()
{ }
void ImageResourceBoss5()
{ }
void ImageResourceBoss6()
{ }
void ImageResourceBoss7()
{ }
void ImageResourceBoss8()
{ }

void ImageResourceBoss9()
{ }

void ImageCommonPortrait()
{ }
void ImageMapleIntro()
{ }
void ImageResourceWeapon8()
{ }

void ImageResourceWeapon9()
{ }

int makeSolidTile(short colr){
    short *ret;

    AllocSmartMemEx(2116,&ret);
    int r=1058;
    while (--r>=0)
        ret[r]=colr;
    return ret;
}

int DecodePCXToRawStream(char *pcx){
    char width=pcx[0];
    char height=pcx[4];

    if (width==45){
        if (height==46){
            short *r;

            AllocSmartMemEx(4232, &r);
            int c=17,wr=0, pic;
            while (TRUE)
            {
                pic =pcx[c++];
                if (pic==2){
                    int count=pcx[c++];
                    short *sp = &pcx[c];
                    c+=(count*2);
                    while (--count>=0){
                        r[wr++]=sp[0];
                        sp=&sp[1];
                    }
                }
                else if (pic==1){
                    c+=1;
                }
                else break;
            }
            return r;
        }
    }
    return makeSolidTile(0xE1BF);
}

// void GRPDumpTileSet(){}

void initializeCustomTileset(int imgVector){
    // int *frames, sizes;
    // int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpTileSet)+4, OBJ_MOVER, NULLPTR, &frames,&sizes);

    AppendImageFrame(imgVector, makeSolidTile(0x0E3E), 2962);
    AppendImageFrame(imgVector, makeSolidTile(0xE317), 325);
    AppendImageFrame(imgVector, makeSolidTile(0x1BD4), 326);
    AppendImageFrame(imgVector, makeSolidTile(0x167C), 2963);
    AppendImageFrame(imgVector, makeSolidTile(0x1DDC), 2964);
    AppendImageFrame(imgVector, makeSolidTile(0x366D), 2965);
}

 //6
// void SolidTile(int section, int subIndex, short color)
// {
//     int *p=GetMemory(0x694860) + (section*36);
//     short *pPixels=p[0] + (2116*subIndex);
//     int count=1058;
//     while (--count>=0)
//         pPixels[count]=color;
// }

// void EnableTileTexture(int enabled)
// {
//     //0x5acd50
//     int *p = 0x5acd50;

//     if (p[0]==enabled)
//         return;
        
//     char code[]={0xB8, 0x15, 0xB7, 0x4C, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC3,};

//     InvokeRawCode(code, NULLPTR);
// }

// void onUpdateTileChanged()
// {
//     EnableTileTexture(FALSE);
//     SolidTile(628, 6,0x07FA);
//     EnableTileTexture(TRUE);
// }

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,58};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

#define MOTION_IDLE 0
#define MOTION_JUMP 1
#define MOTION_MOVE 2
#define MOTION_ATTACK 3
#define MOTION_ATTACK_2 4

void CommonInitializeImage(int pImgVector)
{
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMotIdleLeft)+4, 131761);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMotJumpLeft)+4, 131762);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMotMoveLeft)+4, 131763);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMotAttackLeft)+4, 131764);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMotAttack2Left)+4, 131765);

    int rightMot=GetInversionGrpStreamFromFunction(ImageResourceMotIdleLeft);
    AppendImageFrame(pImgVector, rightMot, 131777);
    AppendImageFrame(pImgVector, GetInversionGrpStreamFromFunction(ImageResourceMotJumpLeft), 131778);
    AppendImageFrame(pImgVector, GetInversionGrpStreamFromFunction(ImageResourceMotMoveLeft), 131779);
    AppendImageFrame(pImgVector, GetInversionGrpStreamFromFunction(ImageResourceMotAttackLeft), 131780);
    AppendImageFrame(pImgVector, GetInversionGrpStreamFromFunction(ImageResourceMotAttack2Left), 131781);

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon1)+4, 133699);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon3)+4, 112958); //combat
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon4)+4, 112959); //manipulation
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon5)+4, 112960); //nature
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon2)+4, 133814); //telpo

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMaya)+4, 112997); //foil-unlit
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceNpc2)+4, 132730); //foil-unlit

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL1)+4, 117361); //urchin
    int *mobR1 = GetInversionGrpStreamFromFunction(ImageResourceMobL1);
    AppendImageFrame(pImgVector, mobR1, 117369); //urchin
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL2)+4, 121188); //OBJ_SMALL_ALBINO_SPIDER
    int *mobR2 = GetInversionGrpStreamFromFunction(ImageResourceMobL2);
    AppendImageFrame(pImgVector, mobR2, 121200); //OBJ_SMALL_ALBINO_SPIDER
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL3)+4, 120768); //OBJ_SMALL_SPIDER
    int *mobR3 = GetInversionGrpStreamFromFunction(ImageResourceMobL3);
    AppendImageFrame(pImgVector, mobR3, 120780); //OBJ_SMALL_SPIDER
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL4)+4, 121716); //leech
    int *mobR4 = GetInversionGrpStreamFromFunction(ImageResourceMobL4);
    AppendImageFrame(pImgVector, mobR4, 121720); //leech
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL5)+4, 119137); //archer
    int *mobR5 = GetInversionGrpStreamFromFunction(ImageResourceMobL5);
    AppendImageFrame(pImgVector, mobR5, 119148); //archer
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL6)+4, 120608); //spider
    int *mobR6 = GetInversionGrpStreamFromFunction(ImageResourceMobL6);
    AppendImageFrame(pImgVector, mobR6, 120619); //spider
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL7)+4, 120979); //albino
    int *mobR7 = GetInversionGrpStreamFromFunction(ImageResourceMobL7);
    AppendImageFrame(pImgVector, mobR7, 120992); //albino
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL8)+4, 124649); //wolf
    int *mobR8 = GetInversionGrpStreamFromFunction(ImageResourceMobL8);
    AppendImageFrame(pImgVector, mobR8, 124661); //wolf
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL9)+4, 124323); //black_w
    int *mobR9 = GetInversionGrpStreamFromFunction(ImageResourceMobL9);
    AppendImageFrame(pImgVector, mobR9, 124333); //
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMobL10)+4, 114933); //mystic
    int *mobR10 = GetInversionGrpStreamFromFunction(ImageResourceMobL10);
    AppendImageFrame(pImgVector, mobR10, 114956); //mystic

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKeyData)+4, 131930);
    
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBottom)+4, 113100);

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon7)+4, 17787); //boots_of_speed
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon7)+4, 17788); //boots_of_speed
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon6)+4, 133698); //book_of_oblivion
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceNpc3)+4, 132731); //foil-unlit
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceMeso)+4, 14411); //gold
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceGoHunt)+4, 17464); //gold
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceGoQuest)+4, 17465); //gold
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceGoShop)+4, 17466); //gold

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss1)+4, 118496); //lich
    mobR1 = GetInversionGrpStreamFromFunction(ImageResourceBoss1);
    AppendImageFrame(pImgVector, mobR1, 118508); //lich

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss2)+4, 113854); //f-golem
    mobR2 = GetInversionGrpStreamFromFunction(ImageResourceBoss2);
    AppendImageFrame(pImgVector, mobR2, 113870); //f-golem

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss3)+4, 116398); //g-wiz
    mobR3 = GetInversionGrpStreamFromFunction(ImageResourceBoss3);
    AppendImageFrame(pImgVector, mobR3, 116422); //g-wiz

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss4)+4, 122875); //shade
    mobR4 = GetInversionGrpStreamFromFunction(ImageResourceBoss4);
    AppendImageFrame(pImgVector, mobR4, 122883); //shade

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss5)+4, 119746); //killerian
    mobR5 = GetInversionGrpStreamFromFunction(ImageResourceBoss5);
    AppendImageFrame(pImgVector, mobR5, 119754); //killerian


    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss7)+4, 125680); //scorpion
    mobR7 = GetInversionGrpStreamFromFunction(ImageResourceBoss7);
    AppendImageFrame(pImgVector, mobR7, 125689); //scorpion

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss8)+4, 127586); //beholder
    mobR8 = GetInversionGrpStreamFromFunction(ImageResourceBoss8);
    AppendImageFrame(pImgVector, mobR8, 127612); //beholder
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss6)+4, 130786); //ogre lord
    mobR6 = GetInversionGrpStreamFromFunction(ImageResourceBoss6);
    AppendImageFrame(pImgVector, mobR6, 130800); //ogre lord

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon8)+4, 112961); //bracele
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon8)+4, 112962); //
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon8)+4, 112963); //
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceWeapon9)+4, 112964); //
    AppendImageFrame(pImgVector, GetScrCodeField(ImageMapleIntro)+4, 14611); //
    AppendImageFrame(pImgVector, GetScrCodeField(ImageCommonPortrait)+4, 14983); //
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceBoss9)+4, 134261); //beholder
    mobR9 = GetInversionGrpStreamFromFunction(ImageResourceBoss9);
    AppendImageFrame(pImgVector, mobR9, 134280); //beholder

    initializeCustomTileset(pImgVector);
    initializeImageHealthBar(pImgVector);
}
