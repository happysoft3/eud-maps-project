
#include "q048601_common.h"
#include "libs/queueTimer.h"
#include "libs/playerinfo.h"
#include "libs/unitstruct.h"
#include "libs/fxeffect.h"
#include "libs/hash.h"
#include "libs/buff.h"
#include "libs/wallutil.h"
#include "libs/objectIDdefines.h"
#include "libs/sound_define.h"
#include "libs/logging.h"

#define SPECIAL_WEAPON_WOLF_WALKING_DAMAGE 320
#define SPECIAL_WEAPON_AUTO_TRACKING_DAMAGE 750
#define SPECIAL_WEAPON_THUNDER_LIGHTNING_DAMAGE 160
#define SPECIAL_WEAPON_TRIPLE_ARROW_DAMAGE 135
#define SPECIAL_WEAPON_ARROW_RAIN_DURATION 42
#define SPECIAL_WEAPON_ARROW_RAIN_DAMAGE 3
#define SPECIAL_WEAPON_DEATHRAY_DAMAGE 100

 int CheckCoorValidate(float x, float y)
{
    if (x<100.0)
        return FALSE;
    if (y<100.0)
        return FALSE;
    if (x>5750.0)
        return FALSE;
    if (y>5750.0)
        return FALSE;
    return TRUE;
}


 void dropRainSingle(float xpos, float ypos)
{
    int angle = Random(0, 359);
        float    rgap=RandomFloat(10.0, 200.0);
        int    arw= CreateObjectById(OBJ_CHERUB_ARROW, xpos+MathSine(angle+90, rgap),ypos+MathSine(angle,rgap));
            LookWithAngle(arw, 64);
            UnitNoCollide(arw);
            Raise(arw, 250.0);
            DeleteObjectTimer(arw, 20);
            PlaySoundAround(arw, SOUND_CrossBowShoot);
}

void onArrowRainSplash()
{
    if (!ValidCallerCheck())
    {
        UniPrintToAll("ERROR-onArrowRainSplash");
        return;
    }

    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (!(GetUnitClass(OTHER) & (UNIT_CLASS_MONSTER|UNIT_CLASS_PLAYER)))
            return;

        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER))
                Damage(OTHER, SELF, SPECIAL_WEAPON_ARROW_RAIN_DAMAGE, DAMAGE_TYPE_PLASMA);
        }
    }
}

 void arrowRainLoop(int sub)
{
    float xpos=GetObjectX(sub),ypos=GetObjectY(sub);
    int dur;

    while (ToInt(xpos))
    {
        dur=GetDirection(sub);
        if (dur)
        {
            dropRainSingle(xpos, ypos);
            if (dur&1)                
                SplashDamageAtEx(GetOwner(sub), xpos, ypos, 190.0, onArrowRainSplash);
            FrameTimerWithArg(1, sub, arrowRainLoop);
            LookWithAngle(sub, dur-1);
            break;
        }
        WispDestroyFX(xpos, ypos);
        Delete(sub);
        WriteLog("end-arrowRainLoop");
        break;
    }
}

void StartArrowRain()
{
    int ix,iy;

    WriteLog("startarrowrain");
    if (!ValidCallerCheck())
    {
        WriteLog("ERROR-startarrowrain");
        return;
    }

    if(! GetPlayerMouseXY(OTHER, &ix,&iy))
        return;
    
    WriteLog("startarrowrain-getcursor");
    float x=IntToFloat(ix), y=IntToFloat(iy);

    if (!CheckCoorValidate(x,y))
        return;

    int sub=CreateObjectAt("PlayerWaypoint", x,y);

    if (!IsVisibleTo(OTHER,sub))
    {
        UniPrint(OTHER,"거기는 볼수 없는 지역입니다");
        return;
    }
    FrameTimerWithArg(1, sub, arrowRainLoop);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, SPECIAL_WEAPON_ARROW_RAIN_DURATION);
    PlaySoundAround(sub, SOUND_MetallicBong);
    WriteLog("startarrowrain-end");
}#include "libs/printutil.h"

 void wolfOnWalking(int wolf)
{
    int dur;

    if (IsVisibleTo(wolf+1, wolf))
    {
        dur = GetDirection(wolf+1);
        if (dur)
        {
            PushTimerQueue(1, wolf, wolfOnWalking);
            LookWithAngle(wolf+1, dur-1);
            MoveObjectVector(wolf, UnitAngleCos(wolf, 14.0), UnitAngleSin(wolf, 14.0));
            if (dur&1)
                Walk(wolf, GetObjectX(wolf),GetObjectY(wolf));
            return;
        }
    }
    Delete(wolf++);
    Delete(wolf++);
}

void onWolfSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (!(GetUnitClass(OTHER) & (UNIT_CLASS_MONSTER|UNIT_CLASS_PLAYER)))
            return;

        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER))
                Damage(OTHER, SELF, SPECIAL_WEAPON_WOLF_WALKING_DAMAGE, DAMAGE_TYPE_PLASMA);
        }
    }
}

 void wolfOnCollide()
{
    if (!MaxHealth(SELF))
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            float xpos =GetObjectX(SELF),ypos=GetObjectY(SELF);
            GreenExplosion(xpos,ypos);
            DeleteObjectTimer(CreateObjectAt("ForceOfNatureCharge", xpos,ypos), 48);
            SplashDamageAtEx(GetOwner(SELF), xpos,ypos, 160.0, onWolfSplash);
            PlaySoundAround(SELF, SOUND_HecubahDieFrame439);
            Delete(SELF);
        }
    }
}

 void startWolfWalking()
{
    int wolf=CreateObjectAt("WhiteWolf", GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));

    PushTimerQueue(1, wolf, wolfOnWalking);
    LookWithAngle( CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER)), 42);
    SetOwner(OTHER, wolf);
    SetUnitFlags(wolf, GetUnitFlags(wolf) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    LookWithAngle(wolf, GetDirection(OTHER));
    Frozen(wolf, TRUE);
    SetCallback(wolf, 9, wolfOnCollide);
    GreenSparkFx(GetObjectX(wolf), GetObjectY(wolf));
}

void DetectTrackingMissile()
{
    int ptr = GetOwner(SELF);
    int tg = ToInt(GetObjectZ(ptr + 1));

    if (CurrentHealth(tg))
    {
        if (IsVisibleTo(tg, ptr))
        {
            LookAtObject(SELF, tg);
            LookWithAngle(ptr + 1, GetDirection(SELF));
        }
        else
            Raise(ptr + 1, ToFloat(0));
    }
    else
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < GetObjectZ(ptr))
            Raise(ptr + 1, ToFloat(GetCaller()));
    }
}

void CollideTrackingMissile()
{
    int ptr = GetOwner(SELF);
    int owner = GetOwner(ptr);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, owner, SPECIAL_WEAPON_AUTO_TRACKING_DAMAGE, DAMAGE_TYPE_PLASMA);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        Delete(SELF);
        Delete(ptr);
    }
}

void AutoTrackingMissile(int sUnit)
{
    int owner = GetOwner(sUnit), durate = GetDirection(sUnit), unit;

    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (durate && IsVisibleTo(sUnit, sUnit + 1))
            {
                unit = CreateObjectAt("AirshipCaptain", GetObjectX(sUnit), GetObjectY(sUnit));
                Frozen(CreateObjectAt("HarpoonBolt", GetObjectX(unit), GetObjectY(unit)), 1);
                SetOwner(sUnit, unit);
                LookWithAngle(unit, GetDirection(sUnit + 1));
                LookWithAngle(unit + 1, GetDirection(sUnit + 1));
                DeleteObjectTimer(unit, 1);
                DeleteObjectTimer(unit + 1, 3);
                SetCallback(unit, 3, DetectTrackingMissile);
                SetCallback(unit, 9, CollideTrackingMissile);
                MoveObject(sUnit + 1, GetObjectX(sUnit), GetObjectY(sUnit));
                MoveObject(sUnit, GetObjectX(sUnit) + UnitAngleCos(sUnit + 1, 21.0), GetObjectY(sUnit) + UnitAngleSin(sUnit + 1, 21.0));
                LookWithAngle(sUnit, durate - 1);
                FrameTimerWithArg(1, sUnit, AutoTrackingMissile);
                break;
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        break;
    }
}

 void startAutoTrackingSword()       // SpecialProperty -
{
    int owner=OTHER;
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) + UnitAngleCos(owner, 21.0), GetObjectY(owner) + UnitAngleSin(owner, 21.0));
    LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), GetDirection(owner));
    Raise(unit, 250.0);
    SetOwner(owner, unit);
    LookWithAngle(unit, 35);
    UnitSetEnchantTime(owner, 19, 60);
    FrameTimerWithArg(1, unit, AutoTrackingMissile);
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            int hash=GetUnit1C(SELF);

            if (hash)
            {
                if (HashGet(hash,GetCaller(),NULLPTR,FALSE))
                    return;
                HashPushback(hash,GetCaller(),TRUE);
                Damage(OTHER, owner, SPECIAL_WEAPON_THUNDER_LIGHTNING_DAMAGE, DAMAGE_TYPE_ELECTRIC);
                Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            }
        }
    }
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 17.0), yvect = UnitAngleSin(OTHER, 17.0);
    int arr[30], u=0,hash;

    arr[u] = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(OTHER)+xvect,GetObjectY(OTHER)+yvect);
    HashCreateInstance(&hash);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateById(OBJ_DEMON, GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetUnitFlags(arr[u],GetUnitFlags(arr[u])^UNIT_FLAG_NO_PUSH_CHARACTERS);
        SetUnit1C(arr[u],hash);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    DrawYellowLightningFx(GetObjectX(arr[0]), GetObjectY(arr[0]), GetObjectX(arr[u-1]), GetObjectY(arr[u-1]), 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}

void TripleArrowCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, SPECIAL_WEAPON_TRIPLE_ARROW_DAMAGE, DAMAGE_TYPE_PLASMA);
            Enchant(OTHER,"ENCHANT_FREEZE", 1.3);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int SpawnBullet(int owner, float x, float y, int dam, float force)
{
    int unit = CreateObjectAt("LightningBolt", x, y);

    SetUnitCallbackOnCollide(unit, TripleArrowCollide);
    LookAtObject(unit, owner);
    LookWithAngle(unit, GetDirection(unit) + 128);
    PushObjectTo(unit, UnitRatioX(unit, owner, force), UnitRatioY(unit, owner, force));
    SetOwner(owner, unit);
    return unit;
}

void TripleArrowShot(int ptr)
{
    int owner = GetOwner(ptr), unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)) + 1, i;

    if (CurrentHealth(owner))
    {
        LookWithAngle(ptr, GetDirection(owner) - 15);
        for (i = 0 ; i < 11 ; i ++)
        {
            SpawnBullet(owner, GetObjectX(unit - 1) + UnitAngleCos(ptr, 21.0), GetObjectY(unit - 1) + UnitAngleSin(ptr, 21.0), 175, 38.0);
            LookWithAngle(ptr, GetDirection(ptr) + 3);
        }
    }
    Delete(ptr);
}

void StartTripleArrowHammer(int owner)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));

    SetOwner(owner, unit);
    PushTimerQueue(1, unit, TripleArrowShot);
}

#define _AUTODETECT_SUBUNIT_ 0
#define _AUTODETECT_XVECT_ 1
#define _AUTODETECT_YVECT_ 2
#define _AUTODETECT_MAX_ 4

void OnAutoDetectorEnemySighted()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner))
    {
        Damage(OTHER, SELF, SPECIAL_WEAPON_DEATHRAY_DAMAGE, DAMAGE_TYPE_ZAP_RAY);
        PlaySoundAround(OTHER, SOUND_DeathRayKill);
        Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
        RestoreHealth(owner, 3);
    }
    if (IsObjectOn(GetTrigger()+1))
        Delete(GetTrigger() + 1);
}

void ConfirmAutoDetectingInvalidTarget(int *pMem)
{
    int sub1 = pMem[_AUTODETECT_SUBUNIT_];

    if (IsObjectOn(sub1))
    {
        float *vect = ArrayRefN(pMem, _AUTODETECT_XVECT_);
        float xpos = GetObjectX(sub1), ypos = GetObjectY(sub1);

        Delete(sub1);
        Effect("DEATH_RAY", xpos, ypos, xpos+(vect[0]*64.0), ypos+(vect[1]*64.0));
    }
    FreeSmartMemEx(pMem);
}

void AutoDetectingTriggered()
{
    float xvect=UnitAngleCos(OTHER, 3.0),yvect=UnitAngleSin(OTHER, 3.0);
    int sub = CreateObjectAt("WeirdlingBeast", GetObjectX(OTHER) + xvect, GetObjectY(OTHER) + yvect);
    int validate = CreateObjectAt("BlueSummons", GetObjectX(sub), GetObjectY(sub));

    int *pMem;
    AllocSmartMemEx(_AUTODETECT_MAX_*4, &pMem);
    FrameTimerWithArg(2, pMem, ConfirmAutoDetectingInvalidTarget);
    pMem[_AUTODETECT_SUBUNIT_]=sub+1;
    pMem[_AUTODETECT_XVECT_]=xvect;
    pMem[_AUTODETECT_YVECT_]=yvect;
    SetCallback(sub, 3, OnAutoDetectorEnemySighted);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, GetDirection(OTHER));
    SetUnitScanRange(sub, 420.0);
    DeleteObjectTimer(sub, 1);
    SetUnitFlags(sub, GetUnitFlags(sub) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    PlaySoundAround(OTHER, SOUND_FirewalkOff);
}

void NOXLibraryEntryPointFunction()
{ }

