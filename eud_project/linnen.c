
#include "linnen_utils.h"
#include "libs\printutil.h"
#include "libs\spellutil.h"
#include "libs/wallutil.h"
#include "libs\coopteam.h"
#include "libs\playerupdate.h"

#include "libs\itemproperty.h"
#include "libs\mathlab.h"
#include "libs\buff.h"
#include "libs\fxeffect.h"
#include "libs\reaction.h"
#include "libs/anti_charm.h"
#include "libs/waypoint.h"
#include "libs/imageutil.h"
#include "libs/network.h"
#include "libs/format.h"
#include "libs/fixtellstory.h"
#include "libs/winapi.h"
#include "libs/indexloop.h"
#include "libs/objectIDdefines.h"
#include "libs/queueTimer.h"
#include "libs/sound_define.h"

int m_jandorMeleeAttackFn;

#define UNIT_AMOUNT_GAUGE 18

int LastUnitID = 477, ThisMapLastUnit;
int player[20];
float UNIT_SPEED = 0.6;
int YOUR_LIFE = 10;
int GGOVER = 0;
int *m_pImgVector;

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

void RemoveItem(int item)
{
    if (!HasItem(GetOwner(item), item))
        Delete(item);
}

int DropItemTable(int data)
{
    int item[400], pick;

    if (data)
        item[pick++] = data;
    else
    {
        while (pick)
            RemoveItem(item[--pick]);
    }
    return pick;
}

int SpawnReward(int sUnit)
{
    CallFunctionWithArgInt(DropItemFuncTable(Random(0, 11)), sUnit);
    Delete(sUnit);
}

int SpawnRewardPlus(int sUnit)
{
    CallFunctionWithArgInt(DropItemFuncTable(Random(0, 11)), sUnit);
    Delete(sUnit);
}

int ThingIDCheckingProcess(int cur)
{
    int thingId = GetUnitThingID(cur);

    if (thingId == 2672)
    {
        SpawnReward(cur);
        return 1;
    }
    else if (thingId == 2673)
    {
        SpawnRewardPlus(cur);
        return 1;
    }
    return 0;
}

void EndScan(int count)
{
    UniPrintToAll("스캔완료!: " + IntToString(count) + " 개 유닛을 처리완료 하였습니다");
}

void InitMarkerScan(int cur)
{
    int count;

    if (cur < ThisMapLastUnit)
    {
        int k;
        for (k = 0 ; k < 30 ; k += 1)
        {
            if (ThingIDCheckingProcess(cur + (k * 2)))
                count += 1;
        }
        FrameTimerWithArg(1, cur + 60, InitMarkerScan);
    }
    else
        EndScan(count);
}

void StartMarkerScan(int unit)
{
    if (IsObjectOn(unit))
    {
        InitMarkerScan(unit);
    }
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[17] = 100; arr[18] = 30; arr[19] = 50; 
		arr[20] = 1045220557; arr[21] = 1061158912; arr[23] = 32; arr[24] = 1067869798; 
		arr[26] = 4; 
		arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; 
		arr[40] = 116; arr[53] = 1073741824; arr[55] = 12; arr[56] = 20;
		link = &arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 15; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 1; 
		arr[35] = 2; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = &arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		
		
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146; 
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
        arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
        arr[57] = 5548112; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
        arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064; 
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = arr;
	}
	return link;
}

void onJandorSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 100, DAMAGE_TYPE_PLASMA);
        }
    }
}

void onJandorMeleeAttack()
{
    GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
    SplashDamageAtEx(SELF, GetObjectX(OTHER), GetObjectY(OTHER), 85.0, onJandorSplash);
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20; arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432;
        CustomMeleeAttackCode(arr, onJandorMeleeAttack);
		link = arr;
	}
	return link;
}

void CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        arr[5] = MonsterGoonProcess; arr[72] = MonsterStrongWhiteWizProcess; arr[30] = MonsterWeirdlingBeastProcess; arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process; arr[12] = MonsterFireSpriteProcess;
        arr[73] = MonsterWizardRedProcess; arr[29] = MonsterAirshipCaptainProcess;
    }
    if (thingID & arr[key] != 0)
        CallFunctionWithArg(arr[key], unit);
}

void DefaultMonsterProcess(int unit)
{
    return;
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        UnitZeroFleeRange(unit);
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        UnitZeroFleeRange(unit);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, LastUnitID);
        }
    }
    LastUnitID ++;

    return unitMover;
}

//시험
static void initialServerPatch()
{
    int oldProtect;

    WinApiVirtualProtect(0x51cf84, 1024, 0x40, &oldProtect);
    char *p = 0x51cf84;

    p[23]=0x7a;
    SetMemory(0x51d0c8, 0x513f10);
    WinApiVirtualProtect(0x51cf84, 1024, oldProtect, NULLPTR);
    //46 8B 06 8B 4E 04 89 08 83 C6 09 E9 24 8F 00 00

    WinApiVirtualProtect(0x513f10, 256, 0x40, &oldProtect);

    char servcode[]={0x46, 0x8B, 0x06, 0x8B, 0x4E, 0x04, 0x89, 0x08, 0x83, 0xC6, 0x08, 0xE9, 0x24, 0x8F, 0x00, 0x00};

    NoxByteMemCopy(&servcode, 0x513f10, sizeof(servcode));
    WinApiVirtualProtect(0x513f10, 256, oldProtect, NULLPTR);

    SetMemory(0x5c31ec, 0x513f30);
}

void MapInitialize()
{
    ThisMapLastUnit = MasterUnit();
    MusicEvent();
    WallChecker(-1);
    FrameTimerWithArg(1, Object("FirstScanUnit"), StartMarkerScan);
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(30, GameMainInit);
    initialServerPatch();
}

void MapExit()
{
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
    MusicEvent();
}

void GameMainInit()
{
    FrameTimerWithArg(480, 0, ControlStage); //이거 주석 풀어야합니다//
    FrameTimer(10, LoopPreservePlayer);
    FrameTimer(3, PutTeleportStatue);
    SecondTimer(1, LoopDisplayGameInfo);
    dispositionMeteorShower();
    initialNotes();
    MoveWaypoint(92, 0.0, 0.0);
    LoopReventWall();
}

void onUseMeteorShower()
{
    Delete(SELF);

    if (CurrentHealth(OTHER))
        CastSpellObjectObject("SPELL_METEOR_SHOWER", OTHER, OTHER);
}

void descMeteorShower()
{
    TellStoryUnitName("null", "thing.db:SulphorousShowerWandDescription", "운석소나기 구입?");
    UniChatMessage(SELF, "운석 소나기 목걸이를 살래요? 1만 골드에요!", 120);
}

void tradeMeteorShower()
{
    int result=GetAnswer(SELF);

    if (result==1)
    {
        if (GetGold(OTHER) >= 10000)
        {
            ChangeGold(OTHER, -10000);
            int i = CreateObjectAt("AmuletofCombat", GetObjectX(OTHER), GetObjectY(OTHER));

            SetUnitCallbackOnUseItem(i, onUseMeteorShower);
        }
        else
        {
            UniChatMessage(SELF, "잔액이 부족합니다", 90);
        }
    }
}

void dispositionMeteorShower()
{
    int m=DummyUnitCreateAt("UrchinShaman", LocationX(117), LocationY(117));

    SetDialog(m, "YESNO", descMeteorShower, tradeMeteorShower);
    LookWithAngle(m, 160);
}

void initialNotes()
{
    RegistSignMessage(Object("snote1"), "!--원웨이 디펜스(One way defence)--!");
    RegistSignMessage(Object("snote2"), "이 동상을 더블클릭하면, 안으로 들어갈 수 있습니다. 안에서 다시 클릭하면 밖으로 나옵니다");
    RegistSignMessage(Object("snote3"), "--원웨이 디펜스-- 제작자 noxgameremaster");
    string minfo = "0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz";
    char *s = 0x974ac0;
    NoxSprintfString(StringUtilGetScriptStringPtr(minfo), "-원웨이 디펜스- 이 지도의 버전은 %s 입니다!", &s, 1);
    RegistSignMessage(Object("snote4"), minfo);
}

void PutTeleportStatue()
{
    int ptr, k;
    string name = "Maiden";

    CreateObject("MovableStatueVictory4W", 103);
    CreateObject("MovableStatueVictory4N", 104);
    CreateObject("MovableStatueVictory4E", 105);
    ptr = CreateObject("MovableStatueVictory4S", 106);

    for (k = 0 ; k < 4 ; k ++)
    {
        CreateObject(name, k + 103);
        Frozen(ptr + k+1, 1);
        SetOwner(GetHost(), ptr + k+1);
        SetDialog(ptr + k+1, "NORMAL", TeleportGround, DummyFunction);
    }
    FrameTimer(1, PutMagicShop);
}

void PutMagicShop()
{
    string name = "WizardWhite";
    int ptr;

    ptr = CreateObject(name, 96);
    CreateObject(name, 112);
    CreateObject(name, 114);
    CreateObject(name, 115);
    LookWithAngle(ptr, 192);
    LookWithAngle(ptr + 1, 64);
    LookWithAngle(ptr + 2, 32);
    LookWithAngle(ptr + 3, 128);
    SetOwner(GetHost(), ptr);
    SetOwner(GetHost(), ptr + 1);
    SetOwner(GetHost(), ptr + 2);
    SetOwner(GetHost(), ptr + 3);
    Frozen(ptr, 1);
    Frozen(ptr + 1, 1);
    Frozen(ptr + 2, 1);
    Frozen(ptr + 3, 1);
    SetDialog(ptr, "NORMAL", BuyBarricade, DummyFunction);
    SetDialog(ptr + 1, "NORMAL", BuyCreature, DummyFunction);
    SetDialog(ptr + 2, "NORMAL", BuyWarriorSkill, DummyFunction);
    SetDialog(ptr + 3, "NORMAL", BuyLife, DummyFunction);
}

void BuyBarricade()
{
    int unit, idx;

    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        UniChatMessage(SELF, "단돈 3만원에 바리케이드를 구입하실 수 있습니다, 계속하시려면 저를 한번 더 클릭하세요.", 150);
        Enchant(OTHER, "ENCHANT_AFRAID", 2.0);
    }
    else
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 30000)
        {
            UniPrint(OTHER, "바리케이드가 결제되었습니다. (-30000 gold)");
            unit = CreateObject("PiledBarrels2", 97);
            if (idx < 10)
            {
                idx ++;
                MoveWaypoint(97, GetWaypointX(97) + 30.0, GetWaypointY(97));
            }
            else
                MoveWaypoint(97, 1874.0, 3668.0);
            Enchant(unit, "ENCHANT_FREEZE", 0.0);
            CreateObject("InvisibleLightBlueHigh", 97);
            SetUnitMaxHealth(unit, 340);
            Raise(unit + 1, ToFloat(GetCaller()));
            ChangeGold(OTHER, -30000);
            FrameTimerWithArg(1, unit, DelayGiveTo);
        }
        else
            UniPrint(OTHER, "금화가 부족합니다.");
    }
}

void BuyCreature()
{
    int pic, unit;

    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        UniChatMessage(SELF, "단돈 3만원에 소환수를 구입하실 수 있습니다, 계속하시려면 저를 한번 더 클릭하세요.", 150);
        Enchant(OTHER, "ENCHANT_AFRAID", 2.0);
    }
    else
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 30000)
        {
            pic = Random(0, 6);
            UniPrint(OTHER, "소환수가 생성 되었습니다. (-30000 gold)");
            MoveWaypoint(113, GetObjectX(OTHER), GetObjectY(OTHER));
            unit = CreateObject(RandomlizeCreature(pic), 113);
            CreateObject("InvisibleLightBlueHigh", 113);
            CheckMonsterThing(unit);
            if (pic == 5)
                SetCallback(unit, 3, FlyingMecaSight);
            Raise(unit + 1, ToFloat(GetCaller()));
            SetOwner(OTHER, unit);
            ChangeGold(OTHER, -30000);
            UnitQueue(unit);
            FrameTimerWithArg(1, unit, DelayGuardUnit);
        }
        else
            UniPrint(OTHER, "금화가 부족합니다.");
    }
}

int PlayerClassCheckFlag2(int plr)
{
    return player[plr + 10] & 0x02;
}

void PlayerClassSetFlag2(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x02;
}

int PlayerClassCheckFlag4(int plr)
{
    return player[plr + 10] & 0x04;
}

void PlayerClassSetFlag4(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x04;
}

void GreenSparkFx(float x, float y)
{
    int ptr = CreateObjectAt("MonsterGenerator", x, y);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void BuyWarriorSkill1(int plr)
{
    if (UnitCheckEnchant(OTHER, GetLShift(11)))
    {
        if (GetGold(OTHER) >= 20000)
        {
            PlaySoundAround(OTHER, 226);
            ChangeGold(OTHER, -20000);
            PlayerClassSetFlag4(plr);
            GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
            UniPrint(OTHER, "향상된 작살능력이 결제되었습니다. 금화 2만 골드가 차감됩니다");
        }
        else
            UniPrint(OTHER, "금화가 부족해서 거래가 취소되었습니다");
    }
    else
    {
        UnitSetEnchantTime(OTHER, 11, 10);
        UniPrint(OTHER, "기존 작살 능력을 업그레이드 할래요? 2만 골드가 필요합니다");
        UniPrint(OTHER, "거래를 계속하려면 한번 더 클릭하세요");
    }
}

void BuyWarriorSkill()
{
    int plr = CheckPlayer();

    if (plr < 0) return;
    if (!PlayerClassCheckFlag4(plr))
    {
        BuyWarriorSkill1(plr);
        return;
    }
    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        UniPrint(OTHER, "전사 늑데의 눈 스킬을 배우시려면 금화 4만원이 필요합니다, 계속하시려면 저를 한번 더 클릭하십시오");
        Enchant(OTHER, "ENCHANT_AFRAID", 1.0);
    }
    else
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (PlayerClassCheckFlag2(plr))
            UniPrint(OTHER, "거래가 취소되었습니다. 이미 이 능력을 배우셨습니다");
        else if (GetGold(OTHER) >= 40000)
        {
            PlaySoundAround(OTHER, 226);
            ChangeGold(OTHER, -40000);
            PlayerClassSetFlag2(plr);
            GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
            UniPrint(OTHER, "늑데의 눈 스킬을 배우셨습니다, 만약 죽게되면 스킬을 재 구입해야 하니 조심하시기 바랍니다");
        }
        else
            UniPrint(OTHER, "금화가 부족합니다");
    }
}

void BuyLife()
{
    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        UniChatMessage(SELF, "단돈 5만원에 라이프 하나를 구입하시겠습니까, 계속하시려면 저를 클릭하세요", 150);
        Enchant(OTHER, "ENCHANT_AFRAID", 1.0);
    }
    else
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 50000)
        {
            ChangeGold(OTHER, -50000);
            YOUR_LIFE ++;
            PlaySoundAround(OTHER, 1004);
            UniPrintToAll("목숨 하나를 구입하셨습니다. 현재 남은 라이프: " + IntToString(YOUR_LIFE) + "개 남았습니다");
            Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
        else
        {
            PlaySoundAround(OTHER, 925);
            UniPrint(OTHER, "금화가 " + IntToString(50000 - GetGold(OTHER)) + " 가 더 필요합니다");
        }
    }
}

void DelayGiveTo(int unit)
{
    int owner = ToInt(GetObjectZ(unit + 1));

    Pickup(owner, unit);
    Delete(unit + 1);
}

void DelayGuardUnit(int unit)
{
    int owner = ToInt(GetObjectZ(unit + 1));

    if (CurrentHealth(unit))
    {
        CreatureFollow(unit, owner);
        AggressionLevel(unit, 1.0);
        SetDialog(unit, "NORMAL", ClickCreature, DummyFunction);
    }
    Delete(unit + 1);
}

void UnitQueue(int unit)
{
    int arr[10], cur;

    if (CurrentHealth(arr[cur]))
        Delete(arr[cur]);
    arr[cur] = unit;
    cur = (cur + 1) % 10;
}

void TeleportGround()
{
    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        UniChatMessage(SELF, "저를 더블클릭 해주세요.", 150);
        Enchant(OTHER, "ENCHANT_AFRAID", 2.0);
    }
    else
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (!HasEnchant(OTHER, "ENCHANT_CROWN"))
        {
            Enchant(OTHER, "ENCHANT_CROWN", 0.0);
            MoveObject(OTHER, GetWaypointX(93), GetWaypointY(93));
            Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
        }
        else
        {
            EnchantOff(OTHER, "ENCHANT_CROWN");
            MoveObject(OTHER, GetObjectX(SELF), GetObjectY(SELF));
        }
        Effect("TELEPORT", GetWaypointX(93), GetWaypointY(93), 0.0, 0.0);
    }
}

void ClickCreature()
{
    if (!HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        UniPrint(OTHER, "소환수를 현재 유저의 위치로 데려오려면 더블클릭 하십시오");
        Enchant(OTHER, "ENCHANT_AFRAID", 1.0);
    }
    else
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (CurrentHealth(SELF))
        {
            PlaySoundAround(SELF, 899);
            MoveObject(SELF, GetObjectX(OTHER), GetObjectY(OTHER));
            UniChatMessage(SELF, "우히힛", 150);
            CreatureFollow(SELF, OTHER);
            AggressionLevel(SELF, 1.0);
        }
    }
}

void DummyFunction()
{
    return;
}

void Image132051()
{}

void Image02()
{}

void Image03()
{}

void AnimateCent1(){}
void AnimateCent2(){}
void AnimateCent3(){}
void AnimateCent4(){}
void AnimateCent5(){}
void AnimateCent6(){}

static void clientOnTest()
{
    char testPacket[]={0x3d, 0x00,0x10,0x75,0x00, 0x30, 0,0,0};

    NetClientSendRaw(31, 0, &testPacket, sizeof(testPacket));
}

void PlayerCommonProc()
{
    m_pImgVector = CreateImageVector(32);

    InitializeImageHandlerProcedure(m_pImgVector);
    int index=0;
    AddImageFromResource(m_pImgVector, GetScrCodeField(Image132051) + 4, 132048, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(Image02) + 4, 132917, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(Image03) + 4, 117757, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent1) + 4, 112596, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent1) + 4, 112597, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent2) + 4, 112598, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent3) + 4, 112599, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent4) + 4, 112600, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent5) + 4, 112601, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent6) + 4, 112602, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(AnimateCent1) + 4, 112603, index++);
    // clientOnTest(); //!RemoveMe!//
}

static void NetworkUtilClientMain()
{
    PlayerCommonProc();
}

void PlayerClassOnInit(int plr, int unit)
{
    player[plr] = unit;
    player[plr + 10] = 1;
    SelfDamageClassEntry(unit);
    DiePlayerHandlerEntry(unit);
    if (ValidPlayerCheck(unit))
    {
        if (unit ^ GetHost())
            NetworkUtilClientEntry(unit);
        else
            PlayerCommonProc();
    }
    ChangeGold(unit, -GetGold(unit));
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

void JoinTheMap(int plr)
{
    int wp = Random(74, 81), pUnit = player[plr];

    if (PlayerClassCheckDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    Enchant(pUnit, EnchantList(14), 0.0);
    MoveObject(pUnit, GetWaypointX(wp), GetWaypointY(wp));
    DeleteObjectTimer(CreateObjectAt("RainOrbBlue", GetObjectX(pUnit), GetObjectY(pUnit)), 25);
    PlaySoundAround(pUnit, 1008);
    UniChatMessage(pUnit, "초전박살 입장! 앞으로 가!", 150);
}

void CantJoin()
{
    MoveObject(OTHER, GetWaypointX(90), GetWaypointY(90));
    Enchant(OTHER, EnchantList(14), 0.0);
    Enchant(OTHER, EnchantList(29), 0.0);
    Enchant(OTHER, EnchantList(25), 0.0);
    UniPrint(OTHER, "이 맵에 참가하실 수 없습니다. 아래의 사항을 참고해 주십시오.");
    UniPrint(OTHER, "이 맵이 수용가능한 인원 10명이 초과되었습니다.");
}

void RegistPlayer()
{
    int plr, k;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            plr = CheckPlayer();

            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    PlayerClassOnInit(k, GetCaller());
                    plr = k;
                    break;
                }
            }
            if (plr >= 0)
            {
                JoinTheMap(plr);
                break;
            }
        }
        CantJoin();
        break;
    }
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        int plr = CheckPlayer();

        Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
        if (GGOVER)
            MoveObject(OTHER, LocationX(90), LocationY(90));
        else if (plr < 0)
            MoveObject(OTHER, LocationX(116), LocationY(116));
        else
            RegistPlayer();
    }
}

void PlayerClassUseSkill(int plr)
{
    int pUnit = player[plr];

    if (UnitCheckEnchant(pUnit, GetLShift(31)))
    {
        EnchantOff(pUnit, EnchantList(31));
        RemoveTreadLightly(pUnit);
        Windbooster(pUnit, 90.0);
    }
    else if (PlayerClassCheckFlag2(plr))
    {
        if (!UnitCheckEnchant(pUnit, GetLShift(21)))
        {
            if (CheckPlayerInput(pUnit) == 0x2f)
            {
                UnitSetEnchantTime(pUnit, 21, 15);
                SkillMagicEye(plr);
            }
        }
    }
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + "님께서 적에게 격추되었습니다");
}

void PlayerClassOnFree(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void LoopPreservePlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassUseSkill(i);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i)) break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayer);
}

void Windbooster(int unit, float force)
{
    PushObjectTo(unit, UnitAngleCos(unit, force), UnitAngleSin(unit, force));
    Effect("RICOCHET", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
}

void SetShurkenAmount(int shuriken)
{
    int *ptr = UnitToPtr(shuriken);

    if (ptr == NULLPTR)
        return;

    if (ptr[1] == 1178)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
    else if (ptr[1] == 1168)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
}

int CreateYellowPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 639); //YellowPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateBlackPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 641); //BlackPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateWhitePotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 640); //WhitePotion
    SetMemory(ptr + 12, GetMemory(ptr + 12) ^ 0x20);
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = CreateYellowPotion(125, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 640)
        x = CreateWhitePotion(100, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 641)
        x = CreateBlackPotion(85, GetObjectX(unit), GetObjectY(unit));
    if (x ^ unit) Delete(unit);

    return x;
}

int DropItemFuncTable(int index)
{
    int table[] = {SpawnPotions, SpecialPotion, SpawnArmors, SpawnWeapon,SpawnFanchakOrQuiver, 
        SpawnBowOrXBow, SpawnOblivion, MagicStaff, ManaPotions, OnlyManaHpPotion, CheatGold, GermDrop};
    
    return table[index % 12];
}

int SpawnPotions(int sUnit)
{
    string pot[] = {"RedPotion", "BluePotion", "CurePoisonPotion", "VampirismPotion", "HastePotion", "ShieldPotion", "Cider", "RedApple", "Meat", "ManaCrystalLarge",
        "FireProtectPotion", "ShockProtectPotion", "PoisonProtectPotion", "InvisibilityPotion", "YellowPotion", "Mushroom", "WhitePotion", "BlackPotion",
        "InvulnerabilityPotion"};
    int unit = CheckPotionThingID(CreateObjectAt(pot[Random(0, 18)], GetObjectX(sUnit), GetObjectY(sUnit)));

    Frozen(unit, 1);
    return unit;
}

int SpecialPotion(int sUnit)
{
    return CreateObjectAt("RedPotion", GetObjectX(sUnit), GetObjectY(sUnit));
}

int SpawnArmors(int sUnit)
{
    string name[] = {"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "MedievalCloak", "MedievalPants", "MedievalShirt",
        "WizardHelm", "ChainCoif", "ChainLeggings", "ChainTunic", "WizardRobe", "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots",
        "LeatherBoots", "LeatherHelm", "LeatherLeggings", "ConjurerHelm", "SteelHelm", "SteelShield", "WoodenShield"};
    int unit = CreateObjectAt(name[Random(0, 22)], GetObjectX(sUnit), GetObjectY(sUnit));

    SetArmorProperties(unit, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    Frozen(unit, 1);
    return unit;
}

int SpawnWeapon(int sUnit)
{
    string name[] = {"StaffWooden", "OgreAxe", "RoundChakram", "WarHammer", "GreatSword", "Sword", "Longsword", "BattleAxe", "MorningStar", 
        "RoundChakram", "RoundChakram", "RoundChakram", "GreatSword", "WarHammer", "RoundChakram"};
    int unit = CreateObjectAt(name[Random(0, 13)], GetObjectX(sUnit), GetObjectY(sUnit));

    SetWeaponProperties(unit, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    Frozen(unit, 1);
    return unit;
}

int SpawnFanchakOrQuiver(int sUnit)
{
    string name[] = {"Quiver", "FanChakram", "FanChakram"};
    int unit = CreateObjectAt(name[Random(0, 2)], GetObjectX(sUnit), GetObjectY(sUnit));

    SetShurkenAmount(unit);
    SetWeaponProperties(unit, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));

    return unit;
}

int SpawnBowOrXBow(int sUnit)
{
    string name[] = {"Bow", "CrossBow", "RoundChakram"};
    int unit = CreateObjectAt(name[Random(0, 2)], GetObjectX(sUnit), GetObjectY(sUnit));

    SetWeaponProperties(unit, Random(0, 5), Random(0, 5), Random(29, 36), Random(29, 36));
    return unit;
}

int SpawnOblivion(int sUnit)
{
    string name[] = {"OblivionHalberd", "OblivionHeart", "OblivionWierdling", "OblivionOrb"};
    int oblivion = CreateObjectAt(name[Random(0, 2)], GetObjectX(sUnit), GetObjectY(sUnit));

    DisableOblivionItemPickupEvent(oblivion);
    SetItemPropertyAllowAllDrop(oblivion);
    
    SetWeaponProperties(oblivion, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    return oblivion;
}

int MagicStaff(int sUnit)
{
    string name[] = {"DeathRayWand", "ForceWand", "LesserFireballWand", "FireStormWand", "InfinitePainWand",
        "SulphorousFlareWand"};

    return CreateObjectAt(name[Random(0, 5)], GetObjectX(sUnit), GetObjectY(sUnit));
}

int ManaPotions(int sUnit)
{
    string name[] = {"BluePotion", "ManaCrystalCluster", "ManaCrystalLarge", "ManaCrystalSmall", "WhitePotion", "RedPotion", "CurePoisonPotion"};
    int unit = CheckPotionThingID(CreateObjectAt(name[Random(0, 6)], GetObjectX(sUnit), GetObjectY(sUnit)));

    Frozen(unit, 1);
    return unit;
}

int OnlyManaHpPotion(int sUnit)
{
    string name[] = {"RedPotion", "BluePotion", "CurePoisonPotion"};
    int unit = CreateObjectAt(name[Random(0, 2)], GetObjectX(sUnit), GetObjectY(sUnit));

    return unit;
}

int CheatGold(int sUnit)
{
    int gUnit = CreateObjectAt("QuestGoldChest", GetObjectX(sUnit), GetObjectY(sUnit));
    int ptr = GetMemory(0x750710);

    SetMemory(GetMemory(ptr + 0x2b4), Random(200, 2000));
    return gUnit;
}

int GermDrop(int sUnit)
{
    string gName[] = {"Ruby", "Ruby", "Ruby", "Emerald", "Diamond"};

    return CreateObjectAt(gName[0], GetObjectX(sUnit), GetObjectY(sUnit));
}

void RemoveDropItem()
{
    DropItemTable(0);
}

void notifyBeforeStartWave(int stage)
{
    char buffer[128];

    NoxSprintfString(&buffer, "잠시 후 스테이지%d 이 시작됩니다.", &stage, 1);
    UniPrintToAll(ReadStringAddressEx(&buffer));
    UniPrintToAll("스테이지가 곧 시작될 예정이오니 서둘러 준비해 주시기 바랍니다.");
    UniPrintToAll("5 초 후 필드가 청소될거에요. 필요한 아이템이 있다면 얼른 주우세요");
}

void ControlStage(int stage)
{
    if (GGOVER)
        return;
    if (stage < 20)
    {
        if (stage % 4 == 3)
            UNIT_SPEED += 0.1;
        stage ++;
        MoveWaypoint(92, ToFloat(stage), 0.0);
        notifyBeforeStartWave(stage);
        FrameTimer(150, RemoveDropItem);
        FrameTimerWithArg(200, stage, SpawnUnitRows);
    }
    else
        VictoryEvent();
}

void SpawnUnitRows(int stage)
{
    int row, k, hp = UnitHealth(stage - 1);
    string name = UnitName(stage - 1);

    if (row < UNIT_AMOUNT_GAUGE)
    {
        int unit = CreateObject("InvisibleLightBlueHigh", 93) + 1;
        for (k = 0 ; k < 8 ; k += 1)
        {
            CreateObject(name, k + 82);
            SetUnitMaxHealth(unit + k, hp);
            SetUnitEnchantCopy(unit+k, GetLShift(ENCHANT_BLINDED) | GetLShift(ENCHANT_FREEZE));
            AggressionLevel(unit + k, 0.0);
            SetOwner(MasterUnit(), unit + k);
            SetCallback(unit+k, 5, UnitDeaths);
            SetCallback(unit+k, 7, OnUnitDamaged);
            LookWithAngle(unit + k, 96);
            AntiCharmSetMonster(unit);
            ObjectOff(unit);
        }
        FrameTimerWithArg(1, unit, MovingUnit);
        Delete(unit - 1);
        row ++;
        FrameTimerWithArg(45, stage, SpawnUnitRows);
    }
    else
        row = 0;
}

void MovingUnit(int unit)
{
    int bit = 1, k;

    for (k = 7 ; k >= 0 ; k -= 1)
    {
        if (!MaxHealth(unit + k))
            continue;

        if (CurrentHealth(unit + k))
        {
            bit = bit << 1;
            // MoveObject(unit + (k * 2), GetObjectX(unit + (k * 2)) - UNIT_SPEED, GetObjectY(unit + (k * 2)) + UNIT_SPEED);
            MoveObjectVector(unit + k, -UNIT_SPEED, UNIT_SPEED);
            // if (HasEnchant(unit + k, "ENCHANT_CHARMING"))
            // {
            //     MoveObject(AntiCharm(), GetObjectX(unit + (k * 2)), GetObjectY(unit + (k * 2)));
            //     CastSpellObjectObject("SPELL_COUNTERSPELL", AntiCharm(), AntiCharm());
            // }
        }
        else if (!IsObjectOn(unit + k))
            ObjectOn(unit + k);
    }
    if (bit - 1)
        FrameTimerWithArg(1, unit, MovingUnit);
}

int AntiCharm()
{
    int anti;

    if (!anti)
        anti = CreateObject("InvisibleLightBlueHigh", 94);
    return anti;
}

void UnitDeaths()
{
    int count;

    if (Random(0, 1))
        DropItemTable(CallFunctionWithArgInt(DropItemFuncTable(Random(0, 11)), SELF));
    DeleteObjectTimer(SELF, 1);
    if (++count >= UNIT_AMOUNT_GAUGE * 8)
    {
        UniPrintToAll("이번 스테이지를 클리어 하셨습니다!");
        UniPrintToAll("1분 후 새 게임이 시작되며 몹이 드랍한 아이템들은 바닥에서 지워집니다");
        SecondTimerWithArg(60, ToInt(GetWaypointX(92)), ControlStage);
        count = 0;
    }
}

void OnUnitDamaged()
{
    if (GetCaller())
        return;

    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 2, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void VictoryEvent()
{
    TeleportAllPlayers(93);
    UniPrintToAll("승리_!! 모든 스테이지를 클리어 하셨습니다__!!");
    // UniBroadcast("모든 적들을 잡아 족쳤습니다!!\n클리어!");
    Effect("WHITE_FLASH", GetWaypointX(93), GetWaypointY(93), 0.0, 0.0);
    AudioEvent("FlagCapture", 93);
    FrameTimer(1, StrVictory);
}

void TeleportAllPlayers(int wp)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
            MoveObject(player[k], GetWaypointX(wp), GetWaypointY(wp));
    }
}

void ForceOfNatureEvent(int cur, int owner)
{
    Delete(cur);

    if (HasItem(owner, cur))
        return;

    if (CurrentHealth(owner))
    {
        float x=GetObjectX(owner),y=GetObjectY(owner);
        int mis = CreateObjectAt("TitanFireball", x + UnitAngleCos(owner, 9.0), y + UnitAngleSin(owner, 9.0));
        SetOwner(owner, mis);        
        PushObject(mis, 50.0, x,y);
        LookWithAngle(mis,GetDirection(owner));
    }
}

int GetPlayerScrNumber(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        if (unit ^ player[i]) continue;
        return i;
    }
    return -1;
}

void onHurricaneCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER,SELF,100,DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        // Delete(SELF);
        break;
    }
}

void onHurricaneLoop(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        if (IsVisibleTo(sub,sub+1))
        {
            int owner=GetOwner(sub);

            if (CurrentHealth(owner))
            {
                int dur=GetDirection(sub);
                if (dur)
                {
                    PushTimerQueue(1, sub, onHurricaneLoop);
                    LookWithAngle(sub,dur-1);
                    MoveObjectVector(sub,GetObjectZ(sub),GetObjectZ(sub+1));
                    MoveObjectVector(sub+1,GetObjectZ(sub),GetObjectZ(sub+1));
                    PlaySoundAround(sub,SOUND_SwordMissing);
                    float x=GetObjectX(sub),y=GetObjectY(sub);
                    int dam=DummyUnitCreateById(OBJ_DEMON, x,y);

                    SetCallback(dam,9,onHurricaneCollide);
                    SetOwner(owner,dam);
                    DeleteObjectTimer(dam,1);
                    DeleteObjectTimer(CreateObjectById(OBJ_WHIRL_WIND,x,y), 9);
                    return;
                }
            }
        }
        Delete(sub);
        Delete(sub+1);
    }
}

void castHurricane(int owner)
{
    float x=GetObjectX(owner),y=GetObjectY(owner);
    float vectX=UnitAngleCos(owner,13.0),vectY=UnitAngleSin(owner,13.0);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x+vectX,y+vectY);
    int sub2=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x-vectX,y-vectY);
    SetOwner(owner,sub);
    LookWithAngle(sub,32);
    Raise(sub,vectX);
    Raise(sub2,vectY);
    PushTimerQueue(1,sub,onHurricaneLoop);
}

void HarpoonEvent(int cur,int owner)
{
    int unit, plr = GetPlayerScrNumber(owner);

    if (plr < 0) return;
    if (CurrentHealth(player[plr]))
    {
        if (PlayerClassCheckFlag4(plr))
        {
            Delete(cur);
            castHurricane(owner);
            // MoveWaypoint(plr + 50, UnitAngleCos(player[plr], 18.0), UnitAngleSin(player[plr], 18.0));
            // MoveWaypoint(plr + 60, GetObjectX(player[plr]) + GetWaypointX(plr + 50), GetObjectY(player[plr]) + GetWaypointY(plr + 50));
            // unit = CreateObject("InvisibleLightBlueHigh", plr + 60);
            // CreateObject("InvisibleLightBlueHigh", plr + 60);
            // LookWithAngle(unit, plr);
            // FrameTimerWithArg(1, unit, MovingHurricane);
        }
    }
}

void MagicMissileStaff(int cur, int owner)
{
    int unit;

    if (CurrentHealth(owner))
    {
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(cur), GetObjectY(cur));
        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", owner, GetObjectX(owner) + UnitAngleCos(owner, 46.0), GetObjectY(owner) + UnitAngleSin(owner, 46.0));
        Delete(unit);
        Delete(unit + 2);
        Delete(unit + 3);
        Delete(unit + 4);
        Delete(cur);
    }
}

void MovingHurricane(int ptr)
{
    int plr = GetDirection(ptr), unit;

    while (1)
    {
        if (CurrentHealth(player[plr]))
        {
            if (GetDirection(ptr + 1) < 30 && CheckMapBoundaryUnit(ptr))
            {
                MoveWaypoint(plr + 60, GetWaypointX(plr + 60) + GetWaypointX(plr + 50), GetWaypointY(plr + 60) + GetWaypointY(plr + 50));
                unit = CreateObject("Shopkeeper", plr + 60);
                Frozen(unit, 1);
                DeleteObjectTimer(unit, 1);
                LookWithAngle(unit, plr);
                SetCallback(unit, 9, HurricaneTouched);
                DeleteObjectTimer(CreateObject("WhirlWind", plr + 60), 7);
                AudioEvent("SwordMissing", plr + 60);
                LookWithAngle(ptr + 1, GetDirection(ptr + 1) + 1);
                FrameTimerWithArg(1, ptr, MovingHurricane);
                break;
            }
        }
        Delete(ptr);
        Delete(++ptr);
        break;
    }
}

void HurricaneTouched()
{
    int plr = GetDirection(SELF);

    if (!HasEnchant(OTHER, "ENCHANT_DETECTING") && IsAttackedBy(OTHER, player[plr]) && IsVisibleTo(player[plr], OTHER))
    {
        Damage(OTHER, player[plr], 80, 14);
        Enchant(OTHER, "ENCHANT_DETECTING", 0.8);
    }
}

void SkillMagicEye(int plr)
{
    if (CurrentHealth(player[plr]))
    {
        float pos_x = UnitAngleCos(player[plr], 20.0), pos_y = UnitAngleSin(player[plr], 20.0);
        int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(player[plr]) + pos_x, GetObjectY(player[plr]) + pos_y);
        CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
        DeleteObjectTimer(unit, 1);
        DeleteObjectTimer(unit + 1, 3);
        SetOwner(player[plr], unit);
        AggressionLevel(unit, 1.0);
        CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit) + pos_x, GetObjectY(unit) + pos_y, 450.0);
        LookWithAngle(unit, GetDirection(player[plr]));
        LookWithAngle(unit + 1, plr);
        SetCallback(unit, 3, ShotMagicRay);
    }
}

void ShotMagicRay()
{
    int plr = GetDirection(GetTrigger() + 1);

    Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    Damage(OTHER, player[plr], 100, 16);
}

void MobRangeOver()
{
    if (IsOwnedBy(OTHER, MasterUnit()))
    {
        if (YOUR_LIFE)
        {
            UniPrintToAll("유닛이 임계치를 넘었습니다, 남은 목숨 하나를 잃게됩니다_!!");
            YOUR_LIFE --;
            EnchantOff(OTHER, EnchantList(23));
            EnchantOff(OTHER, EnchantList(26));
            Damage(OTHER, 0, 9999, 14);
        }
        else if (!GGOVER)
        {
            GGOVER = 1;
            GameOver();
        }
    }
}

void LoopDisplayGameInfo()
{
    char buffer[128];

    if (YOUR_LIFE)
    {
        int params[]={ToInt(LocationX(92)), YOUR_LIFE};

        NoxSprintfString(&buffer, "STAGE:\t%d\nLIFE:\t%d", &params, sizeof(params));
        UniChatMessage(MasterUnit(), ReadStringAddressEx(&buffer), 40);
        SecondTimer(1, LoopDisplayGameInfo);
    }
}

void GameOver()
{
    int loc = Object("StartLocation");

    MoveObject(loc, GetWaypointX(90), GetWaypointY(90));
    UniPrintToAll("게임오버_!! 더 이상 남아있는 목숨이 없습니다.");
    TeleportAllPlayers(90);
}

int WallChecker(int num)
{
    int arr[5];
    int k;

    if (num >= 0)
        return arr[num];
    for (k = 4 ; k >= 0 ; k --)
        arr[k] = CreateObject("InvisibleLightBlueHigh", k + 107);
    return 0;
}

void LoopReventWall()
{
    if (!IsVisibleTo(WallChecker(0), WallChecker(1)) || !IsVisibleTo(WallChecker(1), WallChecker(2)) ||
        !IsVisibleTo(WallChecker(2), WallChecker(3)) || !IsVisibleTo(WallChecker(3), WallChecker(4)))
        ReventSkillWall();
    FrameTimer(1, LoopReventWall);
}

void ReventSkillWall()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]) && MaxHealth(player[k]) == 75)
            CastSpellObjectLocation("SPELL_WALL", player[k], 0.0, 0.0);
    }
}

void FlyingMecaSight()
{
    int mis;

    if (CurrentHealth(OTHER) && !HasEnchant(SELF, "ENCHANT_DETECTING"))
    {
        LookAtObject(SELF, OTHER);
        Enchant(SELF, "ENCHANT_DETECTING", 0.0);
        MoveWaypoint(113, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 9.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 9.0));
        mis = CreateObject("ArcherBolt", 113);
        Enchant(mis, "ENCHANT_SHOCK", 0.0);
        LookWithAngle(mis, GetDirection(SELF));
        PushObject(mis, 16.0, GetObjectX(SELF), GetObjectY(SELF));
        SetOwner(SELF, mis);
        FrameTimerWithArg(60, GetTrigger(), ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    Enchant(unit, "ENCHANT_BLINDED", 0.1);
    EnchantOff(unit, "ENCHANT_DETECTING");
    AggressionLevel(unit, 1.0);
}

void StrVictory()
{
	int arr[13];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
	{
		drawStrVictory(arr[i], name);
		i ++;
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(93);
		pos_y = GetWaypointY(93);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 93);
		if (count % 38 == 37)
			MoveWaypoint(93, GetWaypointX(93) - 74.000000, GetWaypointY(93) + 2.000000);
		else
			MoveWaypoint(93, GetWaypointX(93) + 2.000000, GetWaypointY(93));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(93, pos_x, pos_y);
	}
}

string UnitName(int num)
{
    string name[] = {
        "Imp", "Bat", "GiantLeech",
        "FlyingGolem", "AlbinoSpider", "Scorpion",
        "Skeleton", "Swordsman", "Shade",
        "VileZombie", "BlackBear", "MeleeDemon",
        "Necromancer", "SkeletonLord", "Horrendous",
        "Mimic", "StoneGolem", "CarnivorousPlant",
        "EvilCherub", "Hecubah"
    };
    return name[num];
}

int UnitHealth(int num)
{
    int data[] = {
        40, 80, 130,
        176, 195, 220,
        248, 275, 315,
        340, 355, 390,
        420, 450, 470,
        488, 520, 533,
        550, 583
    };
    return data[num];
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k -= 1)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int MasterUnit()
{
    int master;

    if (!master)
    {
        master = CreateObject("Hecubah", 100);
        Frozen(master, 1);
    }
    return master;
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObject("RottenMeat", 99));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

int CheckMapBoundaryUnit(int unit)
{
    float pos_x = GetObjectX(unit);
    float pos_y = GetObjectY(unit);

    if (pos_x > 100.0 && pos_y > 100.0 && pos_x < 5532.0 && pos_y < 5600.0)
        return 1;
    else
        return 0;
}

string RandomlizeCreature(int index)
{
    string table[] = {"WeirdlingBeast", "AirshipCaptain", "BlackWidow", "Mimic", "MeleeDemon", "FlyingGolem", "Shade"};

    return table[index];
}

void StrWarSkill()
{
	int arr[26];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 1075905278; arr[1] = 133713856; arr[2] = 67649679; arr[3] = 18874881; arr[4] = 956449312; arr[5] = 2080900128; arr[6] = 151552009; arr[7] = 1343258900; arr[8] = 570434048; arr[9] = 1107595336; 
	arr[10] = 8881216; arr[11] = 337714704; arr[12] = 6344232; arr[13] = 1073750020; arr[14] = 565376; arr[15] = 603922400; arr[16] = 608305184; arr[17] = 4194304; arr[18] = 1081601; arr[19] = 2113929506; 
	arr[20] = 1140589571; arr[21] = 2143618048; arr[22] = 269484049; arr[23] = 268566032; arr[24] = 1077919744; arr[25] = 32640; 
	while(i < 26)
	{
		drawStrWarSkill(arr[i], name);
		i ++;
	}
}

void drawStrWarSkill(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(114);
		pos_y = GetWaypointY(114);
	}
	for (i = 1 ; i > 0 && count < 806 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 114);
		if (count % 72 == 71)
			MoveWaypoint(114, GetWaypointX(114) - 142.000000, GetWaypointY(114) + 2.000000);
		else
			MoveWaypoint(114, GetWaypointX(114) + 2.000000, GetWaypointY(114));
		count ++;
	}
	if (count >= 806)
	{
		count = 0;
		MoveWaypoint(114, pos_x, pos_y);
	}
}

void WeaponTestCreate(string name)
{
    int unit = CreateObject(name, 30);

    SetWeaponProperties(GetMemory(0x750710), Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
}

void TestDebug()
{
    string name[] = {"StaffWooden", "OgreAxe", "RoundChakram", "WarHammer", "GreatSword", "Sword", "Longsword", "BattleAxe", "MorningStar", 
        "RoundChakram", "RoundChakram", "RoundChakram", "GreatSword", "WarHammer", "RoundChakram"};
    int idx;

    UniPrint(OTHER, name[idx]);
    FrameTimerWithArg(90, name[idx], WeaponTestCreate);
    idx = (idx + 1) % 13;
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
	HashPushback(pInstance, OBJ_HARPOON_BOLT, HarpoonEvent);
	HashPushback(pInstance, OBJ_DEATH_BALL, ForceOfNatureEvent);
	HashPushback(pInstance, OBJ_YELLOW_STAR_SHOT, MagicMissileStaff);
}
