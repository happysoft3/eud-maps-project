
#include "gollest_bintable.h"
#include "libs/objectIDdefines.h"
#include "libs/define.h"
#include "libs/waypoint.h"
#include "libs/stringutil.h"
#include "libs/buff.h"
#include "libs/unitstruct.h"
#include "libs/npc.h"
#include "libs/potionex.h"
#include "libs/winapi.h"
#include "libs/printutil.h"
#include "libs/mathlab.h"
#include "libs/recovery.h"
#include "libs/itemproperty.h"
#include "libs/fxeffect.h"
#include "libs/playerinfo.h"
#include "libs/networkRev.h"

void SendPacketAll(char *packet, int packetLength)
{
    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x90, 0x53, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x1C, 0x31, 0xC0, 0xC3,
    };
    int args[]={
        packet,0xff,packetLength,
    }; //0xff or 0x9f
    invokeRawCode(code, args);
}

void SendPacketSpecifyOne(int user, char *packet, int packetLength)
{
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0)
        return;
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
    };
    int args[]={
        packet,pIndex,packetLength,
    };
    invokeRawCode(code, args);
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}

void StrYourWinner()
{
	int arr[26];
	string name = StringUtilFindUnitNameById(947); //"CharmOrb";
	int i = 0;
	arr[0] = 16777346; arr[1] = 2656256; arr[2] = 34078720; arr[3] = 536872960; arr[4] = 324; arr[5] = 2176; arr[6] = 17891328; arr[7] = 570425344; arr[8] = 120066332; arr[9] = 126821712; 
	arr[10] = 1149804871; arr[11] = 356533440; arr[12] = 52577417; arr[13] = 285353029; arr[14] = 304392706; arr[15] = 285739282; arr[16] = 672106513; arr[17] = 667455781; arr[18] = 541362240; arr[19] = 1149796356; 
	arr[20] = 553714244; arr[21] = 17891876; arr[22] = 152183876; arr[23] = 252577801; arr[24] = 286263048; arr[25] = 2328721; 
	while(i < 26)
	{
		drawStrYourWinner(arr[i++], name);
	}
}

void drawStrYourWinner(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(65);
		pos_y = LocationY(65);
	}
	for (i = 1 ; i > 0 && count < 806 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 65);
		if (count % 80 == 79)
            TeleportLocationVector(65, - 158.000000, 2.000000);
		else
            TeleportLocationVector(65, 2.0,0.0);
		count ++;
	}
	if (count >= 806)
	{
		count = 0;
		TeleportLocation(65, pos_x, pos_y);
	}
}

void BomberSetMonsterCollide(int bombUnit)
{
    int ptr = UnitToPtr(bombUnit);

    if (ptr)
        SetMemory(ptr + 0x2b8, 0x4e83b0);
}

void WizRunAway()
{
    if (UnitCheckEnchant(SELF, GetLShift(ENCHANT_ANTI_MAGIC)))
		EnchantOff(SELF, EnchantList(ENCHANT_ANTI_MAGIC));
}

int BgmTable(char idx)
{
	short misc[] = {
        33,33,33,32,60,
        46,86,48,33,38,
        32,39,36,73,264,
        236,216,265,254,239,
        244,274,181,99,112,
        68,150,118,54};

	return misc[idx];
}

void StartBgmLoop()
{
	SecondTimer(2, MapBgmLoop);
}

void MapBgmLoop()
{
	char miscKey = Random(1, 29);

	MusicEvent();
	SecondTimerWithArg(3, miscKey, PlayMapBgm);
	SecondTimer(BgmTable(miscKey) + 3, MapBgmLoop);
}

void PlayMapBgm(char num)
{
	Music(num, 100);
}

int SummonRedWiz(int target)
{
    int unit = CreateObjectAt(StringUtilFindUnitNameById(1334)  /*"WizardRed"*/, GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    UnitLinkBinScript(unit, WizardRedBinTable());
    UnitZeroFleeRange(unit);
    SetUnitMaxHealth(unit, 435);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    SetCallback(unit, 8, WizRunAway);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
		SetMemory(uec + 0x520, ToInt(400.0));
		uec += 0x5d0;
		SetMemory(uec + (50*4) /*GetSpellNumber("SPELL_MAGIC_MISSILE")*/, 0x40000000);
		SetMemory(uec +(51*4)/* GetSpellNumber("SPELL_SHIELD")*/, 0x10000000);
		SetMemory(uec +(74*4)/* GetSpellNumber("SPELL_STUN")*/, 0x20000000);
		SetMemory(uec +(71*4)/* GetSpellNumber("SPELL_SHOCK")*/, 0x10000000);
		SetMemory(uec +(27*4) /*GetSpellNumber("SPELL_FIREBALL")*/, 0x40000000);
		SetMemory(uec +(16*4)/* GetSpellNumber("SPELL_DEATH_RAY")*/, 0x40000000);
		SetMemory(uec +(5*4)/* GetSpellNumber("SPELL_BURN")*/, 0x40000000);
        SetMemory(uec +(38*4) /*GetSpellNumber("SPELL_INVERSION")*/, 0x08000000);
        SetMemory(uec +(13*4)/* GetSpellNumber("SPELL_COUNTERSPELL")*/, 0x08000000);
    }
    return unit;
}

int SummonNecromancer(int target)
{
    int unit = CreateObjectAt(StringUtilFindUnitNameById(1391) /*"Necromancer"*/, GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 308);
    SetCallback(unit, 8, WizRunAway);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec +(24*4)/* GetSpellNumber("SPELL_LIGHTNING")*/, 0x40000000);
		SetMemory(uec +(51*4)/* GetSpellNumber("SPELL_SHIELD")*/, 0x10000000);
        SetMemory(uec +(72*4)/* GetSpellNumber("SPELL_SLOW")*/, 0x20000000);
		SetMemory(uec +(39*4)/* GetSpellNumber("SPELL_INVISIBILITY")*/, 0x10000000);
		SetMemory(uec +(27*4)/* GetSpellNumber("SPELL_FIREBALL")*/, 0x40000000);
        SetMemory(uec +(38*4)/* GetSpellNumber("SPELL_INVERSION")*/, 0x8000000);
        SetMemory(uec +(13*4)/*+ GetSpellNumber("SPELL_COUNTERSPELL")*/, 0x8000000);
    }
    return unit;
}

int SummonLich(int target)
{
    int unit = CreateObjectById(OBJ_LICH, GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 405);
    SetCallback(unit, 8, WizRunAway);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec + (43*4)/* GetSpellNumber("SPELL_CHAIN_LIGHTNING")*/, 0x40000000);
		SetMemory(uec +(51*4)// GetSpellNumber("SPELL_SHIELD")
        , 0x10000000);
		SetMemory(uec +(12*4)// GetSpellNumber("SPELL_CONFUSE")
        , 0x20000000);
        SetMemory(uec +(72*4)// GetSpellNumber("SPELL_SLOW")
        , 0x20000000);
		SetMemory(uec +(39*4)// GetSpellNumber("SPELL_INVISIBILITY")
        , 0x10000000);
		SetMemory(uec +(50*4)// GetSpellNumber("SPELL_MAGIC_MISSILE")
        , 0x40000000);
		SetMemory(uec +(101*4)// GetSpellNumber("SPELL_SUMMON_STONE_GOLEM")
        , 0x40000000);
        SetMemory(uec +(38*4)// GetSpellNumber("SPELL_INVERSION")
        , 0x08000000);
        SetMemory(uec +(13*4)// GetSpellNumber("SPELL_COUNTERSPELL")
        , 0x08000000);
        SetMemory(uec +(33*4)// GetSpellNumber("SPELL_FUMBLE")
        , 0);
        SetMemory(uec +(71*4)// GetSpellNumber("SPELL_SHOCK")
        , 0);
    }
    return unit;
}

int SummonGreenWiz(int target)
{
    int unit = CreateObjectById(OBJ_WIZARD_GREEN, GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 265);
    SetCallback(unit, 8, WizRunAway);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec +(52*4)// GetSpellNumber("SPELL_METEOR")
        , 0x40000000);
		SetMemory(uec +(51*4)// GetSpellNumber("SPELL_SHIELD")
        , 0x10000000);
		SetMemory(uec +(12*4)// GetSpellNumber("SPELL_CONFUSE")
        , 0x20000000);
        SetMemory(uec +(72*4)// GetSpellNumber("SPELL_SLOW")
        , 0x20000000);
        SetMemory(uec +(60*4)// GetSpellNumber("SPELL_POISON")
        , 0x20000000);
		SetMemory(uec +(39*4)// GetSpellNumber("SPELL_INVISIBILITY")
        , 0x10000000);
		SetMemory(uec +(58*4)// GetSpellNumber("SPELL_PIXIE_SWARM")
        , 0x40000000);
		SetMemory(uec +(114*4)// GetSpellNumber("SPELL_SUMMON_URCHIN_SHAMAN")
        , 0x40000000);
        SetMemory(uec +(29*4) //GetSpellNumber("SPELL_FIST")
        , 0x40000000);
        SetMemory(uec +(38*4) //GetSpellNumber("SPELL_INVERSION")
        , 0x08000000);
        SetMemory(uec +(13*4) //GetSpellNumber("SPELL_COUNTERSPELL")
		, 0x08000000);
        SetMemory(uec +(4*4) //GetSpellNumber("SPELL_BLINK")
        , 0);
    }
    return unit;
}

int SummonMystic(int target)
{
    int unit = CreateObjectById(OBJ_WIZARD, GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    Enchant(unit, EnchantList(ENCHANT_ANCHORED), 0.0);
    SetUnitMaxHealth(unit, 237);
    SetCallback(unit, 8, WizRunAway);
    SetMemory(uec + 0x528, ToInt(1.0));
    SetMemory(uec + 0x520, ToInt(400.0));
    return unit;
}

int SummonHecubah(int target)
{
    int unit = CreateObjectById(OBJ_HECUBAH, GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 1050);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        SetMemory(uec + 0x5a8, 0x0f0000);
        SetMemory(uec + 0x5b0, 0x0f0000);
        SetMemory(uec + 0x5c0, 0x0f0000);
        uec += 0x5d0;
        SetMemory(uec + (16*4)// GetSpellNumber("SPELL_DEATH_RAY")
        , 0x40000000);
		SetMemory(uec +(51*4)// GetSpellNumber("SPELL_SHIELD")
        , 0x10000000);
        SetMemory(uec +(72*4)// GetSpellNumber("SPELL_SLOW")
        , 0x20000000);
		SetMemory(uec +(39*4)// GetSpellNumber("SPELL_INVISIBILITY")
        , 0x10000000);
        SetMemory(uec +(38*4) //GetSpellNumber("SPELL_INVERSION")
        , 0x8000000);
        SetMemory(uec +(43*4) //GetSpellNumber("SPELL_CHAIN_LIGHTNING")
        , 0x40000000);
    }
    return unit;
}

int SummonLichLord(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1341) /*"LichLord"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    UnitLinkBinScript(sMob, LichLord2BinTable());
    UnitZeroFleeRange(sMob);
    SetUnitStatus(sMob, GetUnitStatus(sMob) ^ 0x20);
    SetUnitMaxHealth(sMob, 370);
    return sMob;
}

int SummonFrog(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1313)/* "GreenFrog"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    UnitLinkBinScript(sMob, GreenFrogBinTable());
    UnitZeroFleeRange(sMob);
    SetUnitMaxHealth(sMob, 160);
    return sMob;
}

int SummonFireFairy(int pUnit)
{
    int sMob = CreateObjectAt(/*"FireSprite"*/ StringUtilFindUnitNameById(1370), GetObjectX(pUnit), GetObjectY(pUnit));

    UnitLinkBinScript(sMob, FireSpriteBinTable());
    UnitZeroFleeRange(sMob);
    SetUnitStatus(sMob, GetUnitStatus(sMob) ^ 0x10000);
    SetUnitMaxHealth(sMob, 160);
    return sMob;
}

int SummonBlackSpider(int pUnit)
{
    int sMob = CreateObjectAt(/*"BlackWidow"*/ StringUtilFindUnitNameById(1392), GetObjectX(pUnit), GetObjectY(pUnit));

    UnitLinkBinScript(sMob, BlackWidowBinTable());
    SetUnitMaxHealth(sMob, 275);
    SetUnitVoice(sMob, 33);
    return sMob;
}

int SummonGoon(int pUnit)
{
    int sMob = CreateObjectAt(/*"Goon"*/ StringUtilFindUnitNameById(1363), GetObjectX(pUnit), GetObjectY(pUnit));

    UnitLinkBinScript(sMob, GoonBinTable());
    SetUnitMaxHealth(sMob, 200);
    SetUnitSpeed(sMob, 2.2);
    SetMonsterPoisonImmune(sMob);
    return sMob;
}

int SummonStrongWizWhite(int pUnit)
{
    int sMob = CreateObjectAt(/*"StrongWizardWhite"*/ StringUtilFindUnitNameById(1333), GetObjectX(pUnit), GetObjectY(pUnit));

    UnitLinkBinScript(sMob, StrongWizardWhiteBinTable());
    SetUnitMaxHealth(sMob, 275);
    return sMob;
}

int SummonBomber(int pUnit)
{
    int bombIds[] = {1348,1349,1350,1351};
    int unit = CreateObjectAt(StringUtilFindUnitNameById(bombIds[Random(0, sizeof(bombIds)-1)]), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(unit, 180);
    BomberSetMonsterCollide(unit);
    UnitLinkBinScript(unit, BomberGreenBinTable());
    return unit;
}

int SummonJandor(int pUnit)
{
    int unit = CreateObjectAt(/*"AirshipCaptain"*/StringUtilFindUnitNameById(1387), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(unit, 325);
    UnitLinkBinScript(unit, AirshipCaptainBinTable());
    return unit;
}

int SummonOrbHecubah(int pUnit)
{
    int unit = CreateObjectAt(/*"HecubahWithOrb"*/StringUtilFindUnitNameById(1384), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(unit, 400);
    UnitZeroFleeRange(unit);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    UnitLinkBinScript(unit, HecubahWithOrbBinTable());
    return unit;
}

int SummonFemale(int pUnit)
{
    int sMob = CreateSingleColorMaidenAt(250, 16, 220, GetObjectX(pUnit), GetObjectY(pUnit));
    int red = 250, grn = 16, blue = 220;

    SetUnitMaxHealth(sMob, 325);
    SetUnitVoice(sMob, 7);
    UnitLinkBinScript(sMob, MaidenBinTable());
    
    return sMob;
}

int SummonMovingPlant(int pUnit)
{
    int sMob = CreateObjectAt(/*"CarnivorousPlant"*/StringUtilFindUnitNameById(1371), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitSpeed(sMob, 2.5);
    SetUnitMaxHealth(sMob, 420);
    return sMob;
}

int SummonEmberDemon(int pUnit)
{
    int ids[] = {1337, 1338/*"MeleeDemon"*/, 1369 /*"Wolf"*/};
    int sPic = Random(0, 2);
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(ids[sPic]), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 220);
    return sMob;
}

int SummonStoneGolem(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1324)/* "StoneGolem"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 900);
    return sMob;
}

int SummonBeast(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1388)/* "WeirdlingBeast"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 225);
    UnitZeroFleeRange(sMob);
    UnitLinkBinScript(sMob, WeirdlingBeastBinTable());
    
    return sMob;
}

int SummonSwordsman(int pUnit)
{
    // string sName[] = {"Swordsman", "OgreBrute", "SkeletonLord", "BlackBear"};
    int ids[]={1346,1390,1315,1366};
    int pic = Random(0, 3);
    int sMob = CreateObjectAt(StringUtilFindUnitNameById( ids[pic]            ), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 325);
    return sMob;
}

int SummonArcher(int pUnit)
{
    // string sMobName[] = {"Archer", "EvilCherub", "Urchin", "FlyingGolem", "Ghost"};
    int ids[]={1345, 1317, 1339, 1316, 1325};
    int sPic = Random(0, 4);
    int sMob = CreateObjectAt(StringUtilFindUnitNameById( ids[sPic]  ), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 98);
    return sMob;
}

int SummonOgreAxe(int pUnit)
{
    // string sName[] = {"GruntAxe", "Shade", "AlbinoSpider", "SpittingSpider", "UrchinShaman", "WhiteWolf"};
    int ids[]={1336,1362,1355,1352,1340,1367};
    int pic = Random(0, 5);
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(ids[pic]), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 260);
    return sMob;
}

int SummonSkeleton(int pUnit)
{
    // string sName[] = {"Skeleton", "Scorpion", "Spider", "BlackWolf"};
    int ids[]={1314, 1373, 1353, 1368};
    int sPic = Random(0, 3);
    int sMob = CreateObjectAt(StringUtilFindUnitNameById( ids[sPic]), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 295);
    return sMob;
}

int SummonOgreLord(int pUnit)
{
    // string sName[] = {"OgreWarlord", "Bear", "Troll"};
    int ids[]={1389, 1365,2273};
    int sPic = Random(0, 2);
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(ids 
                   [sPic]), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 360);
    return sMob;
}

int SummonMimic(int pUnit)
{
    int sMob = CreateObjectAt(/*"Mimic"*/StringUtilFindUnitNameById(1372), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 500);
    return sMob;
}

int SummonLeech(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1358)/* "GiantLeech"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 135);
    return sMob;
}

int SummonSmallAlbiSpider(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1356) /*"SmallAlbinoSpider"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitSpeed(sMob, 1.2);
    SetUnitMaxHealth(sMob, 110);
    return sMob;
}

int SummonSmallSpider(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1354)/* "SmallSpider"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitSpeed(sMob, 1.2);
    SetUnitMaxHealth(sMob, 90);
    return sMob;
}

int SummonMimiFlier(int pUnit)
{
    // string sName[] = {"Bat", "Imp", "Wasp"};
    int ids[]={1357, 1328,1331};
    int sPic = Random(0, 2);
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(ids[sPic]), GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitMaxHealth(sMob, 64);
    return sMob;
}

int SummonHorrendous(int pUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1386) /* "Horrendous"*/, GetObjectX(pUnit), GetObjectY(pUnit));

    SetUnitSpeed(sMob, 1.9);
    SetUnitMaxHealth(sMob, 380);
    return sMob;
}

int SummonMobRat(int posUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1359)/* "Rat"*/, GetObjectX(posUnit), GetObjectY(posUnit));

    SetUnitMaxHealth(sMob, 135);
    UnitZeroFleeRange(sMob);
    UnitLinkBinScript(sMob, RatBinTable());
    return sMob;
}

int SummonMobFish(int posUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1329)/* "FishBig" */, GetObjectX(posUnit), GetObjectY(posUnit));

    SetUnitMaxHealth(sMob, 170);
    UnitZeroFleeRange(sMob);
    UnitLinkBinScript(sMob, FishBigBinTable());
    return sMob;
}

int SummonMecaGolem(int posUnit)
{
    int sMob = CreateObjectAt(StringUtilFindUnitNameById(1318)/* "MechanicalGolem"*/, GetObjectX(posUnit), GetObjectY(posUnit));

    SetUnitMaxHealth(sMob, 800);
    return sMob;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);

    return x;
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

void DrawImageAtEx(float x, float y, int thingId)
{
    int *ptr = UnitToPtr(CreateObjectAt(StringUtilFindUnitNameById(2026), x, y));

    ptr[1] = thingId;
}

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

void ProcessMapReadableText()
{
    RegistSignMessage(Object("UniSign1"), "특수용품 가게: 일반 상점에서 팔지 않았던 것을 이곳에서 싸게(?) 판매합니다!");
    RegistSignMessage(Object("UniSign2"), "이 아래에는 메인 필드가 있어요! 무기, 갑옷 그리고 전투 보조용품 들.. 단단히 챙겨 내려가세요!");
    RegistSignMessage(Object("UniSign3"), "당신이 바로 오늘 이 게임의 승리자 이다!");
    RegistSignMessage(Object("UniSign4"), "이 지도는 필드에 있는 모든 오벨리스크를 깨면 승리합니다");
    RegistSignMessage(Object("UniSign5"), "오벨리스크가 파괴될 때 안에서 아오오니이 등장할 수 있으니 조심해서 파괴하세요");
    RegistSignMessage(Object("UniSign6"), "휴게실: 이곳은 금연구역 입니다!");
    RegistSignMessage(Object("UniSign7"), "나무꼭대기 마트: 멋지고 강력한 아이템을 많이 구입하여주세요");
}

void ForceOfNatureCollide()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 400, 14);
        Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Delete(SELF);
        Delete(GetTrigger() + 1);
    }
    else if (!GetCaller())
    {
        Delete(SELF);
        Delete(GetTrigger() + 1);
    }
}

int ForceOfNature(int sOwner)
{
    int mis = CreateObjectAt("GameBall", GetObjectX(sOwner) + UnitAngleCos(sOwner, 19.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 19.0));

    //SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, ForceOfNatureCollide);
    SetOwner(sOwner, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(mis), GetObjectY(mis)));
    DeleteObjectTimer(mis, 90);
    DeleteObjectTimer(mis + 1, 100);
    return mis;
}

void OblivionUseHandler()
{
    if (HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
        return;
    else if (CurrentHealth(OTHER))
    {
        PushObject(ForceOfNature(OTHER), 20.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 204);
        Enchant(OTHER, "ENCHANT_ETHEREAL", 0.9);
    }
}

void DelayGiveToOwner(int sTarget)
{
    int sOwner = GetOwner(sTarget);

    if (IsObjectOn(sTarget) && CurrentHealth(sOwner))
        Pickup(sOwner, sTarget);
    else
        Delete(sTarget);
}

int SummonOblivion(int sOwner)
{
    int orbliv = CreateObjectAt("OblivionOrb", GetObjectX(sOwner), GetObjectY(sOwner));
    
    SetUnitCallbackOnUseItem(orbliv, OblivionUseHandler);
    DisableOblivionItemPickupEvent(orbliv);
    SetItemPropertyAllowAllDrop(orbliv);
    
    SetOwner(sOwner, orbliv);
    FrameTimerWithArg(1, orbliv, DelayGiveToOwner);
    return orbliv;
}

void PlayerClassCommonWhenEntry(){}

void ConfirmPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    if (pUnit)
    {
        ShowQuestIntroOne(pUnit, 9999, "ConjurerChapterLoss10", "Journal:GauntletQuest");
        PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    }
    if (pInfo==0x653a7c)
    {
        PlayerClassCommonWhenEntry();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}


