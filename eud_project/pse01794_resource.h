
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"
#include "libs/clientside.h"
#include "libs/wandpatch.h"

void ImageResourceField1()
{
    return;
}

void ImageResourceKor() {}
void ImageResourceChin() {}

void RedPotionImage() {}
void PoisonPotionImage() {}
void VampPotionImage() {}
void YellowPotionImage() {}
void MagicPotionImage() {}

void ResourceBucherImage() {}

void ResourceDollar()
{ }

void ResourcePortraitGirl()
{ }

void ResourcePortraitHhp()
{ }

void ResourcePortraitFrog()
{}

void ResourcePortraitSpare()
{}

void ImageGoReviveroom()
{ }
void ImageMapLogo()
{}
void ImageYouwin()
{ }

void EnableTileTexture(int enabled)
{
    //0x5acd50
    int *p = 0x5acd50;

    if (p[0]==enabled)
        return;
        
    char code[]={0xB8, 0x15, 0xB7, 0x4C, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC3,};

    invokeRawCode(code, NULLPTR);
    
}

void onUpdateTileChanged()
{
    EnableTileTexture(FALSE);
    // SolidTile(628, 6,0x07FA);
    EnableTileTexture(TRUE);
}
void GRPDumpGeometricTileSet(){}
void GRPDumpLoveTileSet(){}

void initializeCustomTileset(int imgVector){
    int *pack=GetScrCodeField(GRPDumpGeometricTileSet)+4;
    int *off=&pack[1];
    int frames[]={pack+off[0],pack+off[1], pack+off[2],pack+off[3],pack+off[4],pack+off[5],};

    AppendImageFrame(imgVector, frames[0], 361);
    AppendImageFrame(imgVector, frames[1], 362);
    AppendImageFrame(imgVector, frames[2], 363);
    AppendImageFrame(imgVector, frames[3], 364);
    AppendImageFrame(imgVector, frames[4], 365);
    AppendImageFrame(imgVector, frames[5], 366);
    AppendImageFrame(imgVector, frames[0], 367);
    AppendImageFrame(imgVector, frames[1], 368);
    AppendImageFrame(imgVector, frames[2], 369);

    pack=GetScrCodeField(GRPDumpLoveTileSet)+4;
    off=&pack[1];
    int frames2[]={pack+off[0],pack+off[1], pack+off[2],pack+off[3],pack+off[4],pack+off[5],};

    AppendImageFrame(imgVector, frames2[0], 9444);
    AppendImageFrame(imgVector, frames2[1], 9445);
    AppendImageFrame(imgVector, frames2[2], 9446);
    AppendImageFrame(imgVector, frames2[3], 9447);
    AppendImageFrame(imgVector, frames2[4], 9448);
    AppendImageFrame(imgVector, frames2[5], 9449);
    AppendImageFrame(imgVector, frames2[0], 9450);
    AppendImageFrame(imgVector, frames2[1], 9451);
    AppendImageFrame(imgVector, frames2[2], 9452);
    onUpdateTileChanged();
}
 
void InitializeResources()
{
    int vec = CreateImageVector(2048);

    AppendImageFrame(vec, GetScrCodeField(ImageResourceField1) + 4, 17456);
    AppendImageFrame(vec, GetScrCodeField(RedPotionImage) + 4, 17827);
    AppendImageFrame(vec, GetScrCodeField(VampPotionImage) + 4, 138743);
    AppendImageFrame(vec, GetScrCodeField(PoisonPotionImage) + 4, 17804);
    AppendImageFrame(vec, GetScrCodeField(MagicPotionImage) + 4, 138747);
    AppendImageFrame(vec, GetScrCodeField(YellowPotionImage) + 4, 17830);
    AppendImageFrame(vec, GetScrCodeField(ResourceBucherImage) + 4, 17457);
    AppendImageFrame(vec, GetScrCodeField(ImageResourceChin) + 4, 17458);
    AppendImageFrame(vec, GetScrCodeField(ImageResourceKor) + 4, 17459);
    AppendImageFrame(vec, GetScrCodeField(ResourceDollar) + 4, 14411);
    AppendImageFrame(vec, GetScrCodeField(ResourcePortraitGirl) + 4, 14922);
    AppendImageFrame(vec, GetScrCodeField(ResourcePortraitHhp) + 4, 14856);
    AppendImageFrame(vec, GetScrCodeField(ResourcePortraitFrog) + 4, 15033);
    AppendImageFrame(vec, GetScrCodeField(ResourcePortraitSpare) + 4, 14996);
    AppendImageFrame(vec, GetScrCodeField(ResourcePortraitSpare) + 4, 14822);
    AppendImageFrame(vec, GetScrCodeField(ImageYouwin)+4, 132317);
    AppendImageFrame(vec, GetScrCodeField(ImageMapLogo)+4, 132263);
    AppendImageFrame(vec, GetScrCodeField(ImageGoReviveroom)+4, 132264);
    initializeCustomTileset(vec);
    DoImageDataExchange(vec);
    AppendAllDummyStaffsToWeaponList();
    ShowMessageBox("중국인 입국막기", "중국인들의 무분별한 국내 입국을 대 학살을 통해 막아보아요");

}
