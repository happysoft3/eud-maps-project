
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/wallutil.h"
#include "libs/queueTimer.h"
#include "libs/playerinfo.h"
#include "libs/format.h"
#include "libs/printutil.h"
#include "libs/hash.h"
#include "libs/objectIDdefines.h"
#include "libs/winapi.h"
#include "libs/recovery.h"

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void WallCoorToUnitCoor(int wall, float *destXy)
{
    int x=wall>>0x10, y=wall&0xff;

    destXy[0]=IntToFloat((x*23) + 11);
    destXy[1]=IntToFloat((y*23)+11);
}
#define WALL_TYPE_INVISIBLE_BLOCKING_WALL_SET 0x31
#define WALL_TYPE_FIELD_STONE_SHORT 0x42

int checkAvataAroundWall(int avata)
{
    int wall=WallUtilGetWallAtObjectPosition(avata);

    if (!wall)
        return 0;

    char *pWall=WallUtilWallUniqueIdToPtr(wall); //0x31

    if (pWall[1]!=WALL_TYPE_INVISIBLE_BLOCKING_WALL_SET) //FieldStoneShort 종류
        return 0;
    // if ( pWall[0]!=7) //방향값 입니다
    //     return 0;

    return wall;
}

int CheckBottom(int sub)
{
    int wall=checkAvataAroundWall(sub);

    if (wall)
        return wall;
    return checkAvataAroundWall(sub+1);
}

void ReportHealthChanged(int unit, int deviation)
{
    char *bptr = UnitToPtr(unit);

    if (!bptr)
        return;

    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0x6A, 0x05, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x50, 0x6A, 0x1F, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3, 0x90
    }, *pDev=&deviation;
    char packet[]={0x42, bptr[36],bptr[25], pDev[0], pDev[1], 0,0,0,};
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=code;
    Unused1f(packet);
    pBuiltins[31]=pOld;
}

void deferredDelete(int mon)
{
    if (MaxHealth(mon))
        Delete(mon);
}

void deferredDeath(int mon)
{
    if (CurrentHealth(mon))
    {
        Frozen(mon, FALSE);
        Damage(mon,0,GetUnit1C(mon),DAMAGE_TYPE_PLASMA);
        ObjectOff(mon);
        PushTimerQueue(12, mon, deferredDelete);
    }
}

void ReportHealthChangedPrimary(int unit, int deviation)
{
    int sub=CreateObjectById(OBJ_GHOST, GetObjectX(unit),GetObjectY(unit)+1.0);

    SetUnitMaxHealth(sub,5000);
    UnitNoCollide(sub);
    SetUnit1C(sub,deviation);
    Frozen(sub, TRUE);
    LookWithAngle(sub,64);
    // CreateObjectById(OBJ_STORM_CLOUD, GetObjectX(sub),GetObjectY(sub));    
    PushTimerQueue(1, sub, deferredDeath);
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

void onDeferredPickup(int item)
{
    if (ToInt(GetObjectX(item)))
    {
        int owner=GetOwner(item);

        if (CurrentHealth(owner))
            Pickup(owner, item);
    }
}

void SafetyPickup(int user, int item)
{
    if (CurrentHealth(user))
    {
        if (ToInt(GetObjectX(item)))
        {
            SetOwner(user,item);
            PushTimerQueue(1, item, onDeferredPickup);
        }
    }
}

int HasSpecifyItem(int user, short thingId)
{
    int inv=GetLastItem(user);

    while (inv)
    {
        if (GetUnitThingID(inv)==thingId)
            return TRUE;
        inv=GetPreviousItem(inv);
    }
    return FALSE;
}

void UpdatePlayerGold(int user)
{
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0x6A, 0x05, 
            0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3, 0x90
        };
        pCode=code;
    }
    int pIndex=GetPlayerIndex(user);

    if (pIndex<0)
        return;

    char packet[]={0x4a, 0,0,0,0};
    int *p = &packet[1];

    p[0]=GetGold(user);
    int args[]={packet,pIndex};
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=pCode;
    Unused1f(args);
    pBuiltins[31]=pOld;
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_LIMIT_VALUE 3
#define HPDATA_MAX 4

void onHealthbarProgressMapleOnly(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER],hp=GetUnit1C(owner);
        if (hp){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgressMapleOnly);
            int perc= computePercent(hp,hpdata[HPDATA_LIMIT_VALUE],10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

int AttachHealthbarMapleOnly(int unit, int max)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    hpdata[HPDATA_LIMIT_VALUE]=max;
    SetUnitFlags(bar,GetUnitFlags(bar)^(UNIT_FLAG_NO_COLLIDE|UNIT_FLAG_AIRBORNE));
    PushTimerQueue(1,hpdata,onHealthbarProgressMapleOnly);
    return bar;
}

int monsterHpReportHash(){
    int hash;
    if (!hash) HashCreateInstance(&hash);
    return hash;
}

#define SUBHPDATA_DUR 0
#define SUBHPDATA_UNIT 1
#define SUBHPDATA_HASHKEY 2
#define SUBHPDATA_MAX 3

void countdownRemainDisplay(int *data){
    if (--data[SUBHPDATA_DUR]>=0)
    {
        PushTimerQueue(1,data,countdownRemainDisplay);
        return;
    }
    HashGet(monsterHpReportHash(),data[SUBHPDATA_HASHKEY],NULLPTR,TRUE);
    Delete(data[SUBHPDATA_UNIT]);
    FreeSmartMemEx(data);
}

void MonsterReportHitpointsEx(int unit, int cur, int max, int durations){
    int *p;
    if (HashGet(monsterHpReportHash(), unit, &p, FALSE)){
        p[SUBHPDATA_DUR]=durations;
        return;
    }
    AllocSmartMemEx(SUBHPDATA_MAX*4,&p);
    p[SUBHPDATA_DUR]=durations;
    p[SUBHPDATA_UNIT]=AttachHealthbarMapleOnly(unit,max);
    p[SUBHPDATA_HASHKEY]=unit;
    HashPushback(monsterHpReportHash(), unit, p);
    PushTimerQueue(1,p,countdownRemainDisplay);
}

void MonsterReportHitpoints(int unit, int cur, int max, int durations)
{
    char slots[11];

    NoxByteMemset(slots,sizeof(slots)-1,' ');
    int perc=computePercent(cur,max, 10);

    while (--perc>=0)
        slots[perc]='#';

    char buff[32], *p=slots;

    NoxSprintfString(buff, "[%s]", &p, 1);
    UniChatMessage(unit, ReadStringAddressEx(buff), durations);
}

void InvokeRawCode(char *pCode, int *args)
{
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=pCode;
    Unused1f(args);
    pBuiltins[31]=pOld;
}

void SendPacketAll(char *packet, int packetLength)
{
    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x90, 0x53, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x1C, 0x31, 0xC0, 0xC3,
    };
    int args[]={
        packet,0xff,packetLength,
    }; //0xff or 0x9f
    InvokeRawCode(code, args);
}

void SendPacketSpecifyOne(int user, char *packet, int packetLength)
{
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0)
        return;
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
    };
    int args[]={
        packet,pIndex,packetLength,
    };
    InvokeRawCode(code, args);
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}

void ShowQuestIntroAll(int questLv, string introImg, string introTxt)
{
    ShowQuestIntroOne(0, questLv, introImg, introTxt);
}

void KickPlayer(int user)
{
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
            0xB8, 0xB0, 0xEA, 0x4D, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    int args[]={4, GetPlayerIndex(user), };
    InvokeRawCode(pCode, args);
}

int GetPlayerXwisId(int user, char *buff)
{
    if (!IsPlayerUnit(user))
        return FALSE;

    int ptr = UnitToPtr(user);
    
    if (ptr)
    {
        char *xwis_id = GetMemory(GetMemory(ptr + 0x2ec) + 0x114) + 0x830;

        NoxByteMemCopy(xwis_id, buff, 12);
        return TRUE;
    }
    return FALSE;
}

int GetPercentRate(int cur, int max)
{
    if (cur>=max)
        return TRUE;

    return Random(1, max)<=cur;
}
