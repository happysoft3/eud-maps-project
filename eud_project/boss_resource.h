
#include "boss_utils.h"
#include "boss_gvar.h"
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/sound_define.h"
#include "libs/abilitydb.h"

void GRPDump_BabyOniOutput(){}

#define BABY_ONI_BASE_IMAGE_ID 121039
void initializeImageFrameBabyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_ALBINO_SPIDER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BabyOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BABY_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BABY_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BABY_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_DoraemonOutput(){}

#define DORAEMON_BASE_IMAGE_ID 133943
void initializeImageFrameDoraemon(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_DoraemonOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, DORAEMON_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, DORAEMON_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, DORAEMON_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     DORAEMON_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_fanmadeBearOutput(){}
#define FANMADE_BEAR_BASE_IMAGE_ID 120669
void initializeImageFrameFanmadeBear(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_SPIDER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_fanmadeBearOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, FANMADE_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, FANMADE_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, FANMADE_BEAR_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     FANMADE_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_WhiteManOutput(){}

#define WHITEMAN_OUTPUT_BASE_IMAGE_ID 130005
void initializeImageFrameWhiteMan(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_WhiteManOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  WHITEMAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(5,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(10,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(15,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(20,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(25,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(30,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(35,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions( WHITEMAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions( WHITEMAN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_SegaFrontierEleanorOutput(){}
#define SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID 115005
void initializeImageFrameSegaFrontierEleanor(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_IMP;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SegaFrontierEleanorOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10,       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions( 15,      SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20,       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(25,       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(30,       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions(       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(       SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
    AnimFrameCopy8Directions(SEGAFRONTIER_ELEANOR_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_ZerglingDumpOutput(){}

#define ZERGLING_IMAGE_START_AT 122643

void initializeImageZergZergling(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SHADE;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_ZerglingDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount-7);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,6,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    ZERGLING_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssign8Directions(5,    ZERGLING_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssign8Directions(10,    ZERGLING_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign8Directions(15,    ZERGLING_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameAssign8Directions(20,    ZERGLING_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameAssign8Directions(25,    ZERGLING_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(30,    ZERGLING_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(35,    ZERGLING_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(40,    ZERGLING_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(45,    ZERGLING_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(50,    ZERGLING_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(55,    ZERGLING_IMAGE_START_AT+88,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+88,    IMAGE_SPRITE_MON_ACTION_WALK, 5);

    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+96,frames[60],IMAGE_SPRITE_MON_ACTION_SLAIN,0);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+97,frames[61],IMAGE_SPRITE_MON_ACTION_SLAIN,1);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+98,frames[62],IMAGE_SPRITE_MON_ACTION_SLAIN,2);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+99,frames[63],IMAGE_SPRITE_MON_ACTION_SLAIN,3);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+100,frames[64],IMAGE_SPRITE_MON_ACTION_SLAIN,4);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+101,frames[65],IMAGE_SPRITE_MON_ACTION_SLAIN,5);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+102,frames[66],IMAGE_SPRITE_MON_ACTION_DUMMY,0);
}

void GRPDump_FubaoPandaOutput(){}
#define PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID 121247
void initializeImageFramePandaFubaoOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BAT;
    int xyinc[]={0,7};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_FubaoPandaOutput)+4, thingId, xyinc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     PANDA_FUBAO_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_BranchOniOutput(){}
#define BRANCHONI_OUTPUT_BASE_IMAGE_ID 133955
void initializeImageFrameBranchOniOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BranchOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BRANCHONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BRANCHONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BRANCHONI_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDumpYoureWinner(){}

void initializeImageFrameYoureWinner(int imgvec)
{
    int *frames, *sizes, thingId=OBJ_RED_ORB;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDumpYoureWinner)+4, thingId, xyInc, &frames,&sizes);

    AppendImageFrame(imgvec,frames[0],16102);
    AppendImageFrame(imgvec,frames[1],16103);
    AppendImageFrame(imgvec,frames[1],16104);
    AppendImageFrame(imgvec,frames[2],16105);
    AppendImageFrame(imgvec,frames[3],16106);
    AppendImageFrame(imgvec,frames[4],16107);
    AppendImageFrame(imgvec,frames[4],16108);
    AppendImageFrame(imgvec,frames[3],16109);
    AppendImageFrame(imgvec,frames[2],16110);
    AppendImageFrame(imgvec,frames[1],16111);
    AppendImageFrame(imgvec,frames[1],16112);
    AppendImageFrame(imgvec,frames[0],16113);
}

void GRPDump_AckMechanical(){}

#define ACK_MECHANICAL_IMAGE_START_AT 124381

void initializeImageACKMECHANICAL(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOLF;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_AckMechanical)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,2,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    ACK_MECHANICAL_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);

    AnimFrameAssign8Directions(5,    ACK_MECHANICAL_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10,    ACK_MECHANICAL_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15,    ACK_MECHANICAL_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20,    ACK_MECHANICAL_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(25,    ACK_MECHANICAL_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(30,    ACK_MECHANICAL_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(35,    ACK_MECHANICAL_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(40,    ACK_MECHANICAL_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(        ACK_MECHANICAL_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameAssign8Directions(45,    ACK_MECHANICAL_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssign8Directions(50,    ACK_MECHANICAL_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_BillyFaceOniOutput(){}
#define BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID 133967
void initializeImageFrameBillyFaceAooniOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_WARRIOR;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyFaceOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BILLY_FACE_AOONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_globglogabgalabOutput(){}

#define GLOBGLOGABGALAB_IMAGE_START_ID 133979
void initializeImage_globglogabgalab(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_TROLL;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_globglogabgalabOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4, 3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4, 3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    AnimFrameAssign8Directions(0,GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(10,GLOBGLOGABGALAB_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_FlyingPaperOutput(){}

#define FLYING_PAPER_OUTPUT_BASE_IMAGE_ID 121736
void initializeImageFrameFlyingPaper(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_RAT;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_FlyingPaperOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  FLYING_PAPER_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_StelsOutput(){}

#define STELS_OUTPUT_BASE_IMAGE_ID 113678
void initializeImageFrameStelsShip(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_FLYING_GOLEM;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_StelsOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,3,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  STELS_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10,  STELS_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 1);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 2);    
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);    
}

void GRPDump_ProtossArchonOutput(){}

#define PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT 123052

void initializeImageFrameProtossArchon(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_ProtossArchonOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,4,3,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(45, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign8Directions(50, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign8Directions(55, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign8Directions(5,  PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssign8Directions(10, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssign8Directions(15, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign8Directions(20, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameAssign8Directions(25, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameAssign8Directions(30, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameAssign8Directions(35, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameAssign8Directions(40, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT,  IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT,  IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_WALK,3);
}

void GRPDump_MaidenGhostOutput(){}
#define MAIDENGHOST_OUTPUT_BASE_IMAGE_ID 114546
void initializeImageFrameMaidenGhost(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GHOST;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MaidenGhostOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign8Directions(10, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign8Directions(15, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign8Directions(20, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);
    AnimFrameAssign8Directions(25, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_IDLE, 5);
    AnimFrameAssign8Directions(30, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_IDLE, 6);
    AnimFrameAssign8Directions(35, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_IDLE, 7);

    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 114160
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_MECHANICAL_GOLEM;
    int xyInc[]={0,-30};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount-4);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}

void GRPDump_RedmistOutput(){}
#define REDMIST_OUTPUT_BASE_IMAGE_ID 115366
void initializeImageFrameRedmistOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WASP;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_RedmistOutput)+4, thingId, xyinc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1, USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void imageButtonset1(){}
void imageButtonset2(){}
void imageButtonset3(){}

void initializeSingleImage(int v){
    AppendImageFrame(v, GetScrCodeField(imageButtonset1) + 4, 132048);
    AppendImageFrame(v, GetScrCodeField(imageButtonset1) + 4, 132053);
    AppendImageFrame(v, GetScrCodeField(imageButtonset2) + 4, 132052);
    AppendImageFrame(v, GetScrCodeField(imageButtonset2) + 4, 132057);
    AppendImageFrame(v, GetScrCodeField(imageButtonset3) + 4, 132056);
    AppendImageFrame(v, GetScrCodeField(imageButtonset3) + 4, 132051);
}

void initializeAbilities(){
    AbilityDbSetCooldown(ABILITY_DB_EYE_OF_WOLF_ID, WAR_SKILL_COOLDOWN_AUTO_DEATHRAY);
    short message1[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("메두사의 눈빛"), message1);
    AbilityDbSetName(ABILITY_DB_EYE_OF_WOLF_ID, message1);
    short message11[256];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("전방에 보이는 적들에게 데스레이를 발사합니다"), message11);
    AbilityDbSetExplanation(ABILITY_DB_EYE_OF_WOLF_ID, message11);
    short message2[128],message22[256];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("윈드부스터"), message2);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("짧은 거리를 순식간에 이동합니다"), message22);
    AbilityDbSetName(ABILITY_DB_TREAD_LIGHTLY_ID, message2);
    AbilityDbSetExplanation(ABILITY_DB_TREAD_LIGHTLY_ID, message22);
    AbilityDbChangeCastSound(ABILITY_DB_TREAD_LIGHTLY_ID, SOUND_BeholderMove);
    short message3[128],message33[256];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("트리플 에로우 샷"), message3);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("다중화살을 적에게 발사를 합니다"), message33);
    AbilityDbSetName(ABILITY_DB_BERSERKER_CHARGE_ID, message3);
    AbilityDbSetExplanation(ABILITY_DB_BERSERKER_CHARGE_ID, message33);
    AbilityDbChangeCastSound(ABILITY_DB_BERSERKER_CHARGE_ID, SOUND_FlyingGolemRecognize);
}

void initializeRenameItems(){
    ChangeSpriteItemNameAsString(OBJ_AMULET_OF_CLARITY, "패스트힐링 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULET_OF_CLARITY, "잠시동안 체력회복 속도를 대폭 높혀준다");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULET_OF_TELEPORTATION, "안전한 곳으로 공간이동을 합니다");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_MANIPULATION, "운석소나기 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_MANIPULATION, "이 목걸이를 사용한 지역에 운석 소나기가 내리게 한다");
    ChangeSpriteItemNameAsString(OBJ_FEAR, "전기 팬던트");
    ChangeSpriteItemDescriptionAsString(OBJ_FEAR, "이 팬던트를 사용하면 체력회복 및 쇼크 엔첸트가 부여됩니다");
    ChangeSpriteItemNameAsString(OBJ_YELLOW_POTION, "황금 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_YELLOW_POTION, "체력을 많이 회복해준다");
    ChangeSpriteItemNameAsString(OBJ_BLACK_POTION, "검은 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_BLACK_POTION, "체력을 적당히 회복해준다");
    ChangeSpriteItemNameAsString(OBJ_WHITE_POTION, "백색 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_WHITE_POTION, "체력과 마력을 모두 회복해준다");
}

void initializeTextSpriteYouareLoser(int vec){
    short wMessage[32];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr( "!!미션 실패!!"), wMessage);
    AppendTextSprite(vec, 0xE841, wMessage, 112979);
    AppendTextSprite(vec, 0x185D, wMessage, 112980);
    AppendTextSprite(vec, 0x1F67, wMessage, 112981);
    AppendTextSprite(vec, 0xEF80, wMessage, 112982);
    AppendTextSprite(vec, 0xD87C, wMessage, 112983);
}

void InitializeResources(){
    int imgVector=CreateImageVector(2048);

    initializeImageFrameBabyOni(imgVector);
    initializeImageFrameDoraemon(imgVector);
    initializeImageFrameFanmadeBear(imgVector);
    initializeImageFrameSegaFrontierEleanor(imgVector);
    initializeImageFrameWhiteMan(imgVector);
    initializeImageZergZergling(imgVector);
    initializeImageFramePandaFubaoOutput(imgVector);
    initializeImageFrameBranchOniOutput(imgVector);
    initializeImageFrameYoureWinner(imgVector);
    initializeImageACKMECHANICAL(imgVector);
    initializeImageFrameBillyFaceAooniOutput(imgVector);
    initializeImage_globglogabgalab(imgVector);
    initializeImageFrameFlyingPaper(imgVector);
    initializeSingleImage(imgVector);
    initializeImageFrameStelsShip(imgVector);
    initializeImageFrameProtossArchon(imgVector);
    initializeImageFrameMaidenGhost(imgVector);
    initializeImageFrameBillyOni(imgVector);
    initializeImageHealthBar(imgVector);
    initializeTextSpriteYouareLoser(imgVector);
    initializeImageFrameRedmistOutput(imgVector);
    DoImageDataExchange(imgVector);
    initializeAbilities();
    initializeRenameItems();
}
