
#include "libs/define.h"
#include "libs/printutil.h"

void InitializeReadables()
{
    RegistSignMessage(Object("read1"), "<<---러블리 마이홈----------------------------- 제작. 237--------------<<");
    RegistSignMessage(Object("read2"), "--이 맵의 목표--필드로 나가서 600개의 모든 캐릭터를 잡아 족치세요--");
    RegistSignMessage(Object("read3"), "러블리 마이홈: 필드로 들어가는 관문입니다        --제작.237");
    RegistSignMessage(Object("read4"), "러블리 마이홈: 이곳은 GRP 실험실 입니다        --제작.237");
    RegistSignMessage(Object("read5"), "러블리 마이홈: --이 맵의 목표--필드로 나가서 600개의 모든 캐릭터를 잡아 족치세요--        --제작.237");
    string message ="러블리 마이홈: 캐릭터 잡는 필드로 나가는 포탈. 가만히 서서 3초간 대기하면 이동      --제작.237";
    RegistSignMessage(Object("read1A"), message);
    RegistSignMessage(Object("read1B"), message);
}
