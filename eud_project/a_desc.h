
#include "libs/printutil.h"
#include "libs/bind.h"

int mapTranslateAsEnglish(int n)
{
    int *ptb;

    if (ptb==0)
    {
        int tb[]={
            " is dead!",
            "secret wall is open",
            " has join to underground dungeon!",
            "end search for unit",
            "i will go",        //,
            "do you want have a all enchantments? you need 40000 gold",
            "trading is fail: you have already this ability.",
            "you can't use this yet. already you are here!",
            "trade is complete. now you have all enchantment",
            "not enough gold! you need 40000 gold",    //,
            "do you want have a advance harpoon? you need 60000 gold",
            "do you want auto tracking deathray wand?, you need 60000 gold",
            "buy teleport amulet. you need 1000 gold",
            "germ trader: fast trade germ to gold. if you want continue, click to yes",
            "Fail to join",
            "press yes to make all items invinciblitiy. you need 7,500 gold",
        };
        ptb=tb;
    }
    return ptb[n];
}

int mapTranslateAsKor(int n)
{
    int *ptb;

    if (ptb==0)
    {
        int tb[]={
            "님께서 적에게 격추되었습니다",
            "비밀의 벽이 개방되었습니다",
            " 님께서 버려진 지하실에 입장하셨습니다",
            "유닛 검색이 모두 끝났습니다",
            "출격준비 완료",
            "올 엔첸트 능력(유용한 엔첸트를 항상 유지)을 구입하겠어요? 4만골드가 필요해요",
            "거래실패: 당신은 이미 이 능력을 가졌어요",
            "아직은 이것을 사용할 수 없어요. 이미 대피소 위치 주변입니다",
            "거래완료: 이제 당신은 올 엔첸 능력을 가졌어요",
            "거래실패: 금화가 부족합니다-- 올 엔첸 능력: 4만 골드 필요!",
            "향상된 작살능력을 구입하시겠어요? 6만 골드가 필요해요",
            "자동 타겟 데스레이 지팡이를 구입하겠어요? 금화 6만 골드가 필요해요",
            "이곳으로 공간이동할 수 있는 목걸이를 구입하겠어요? 1000골드가 필요해요",
            "보석 교환소: 당신이 소지한 보석을 돈으로 빠르게 교환해드립니다. 계속하려면 예를 눌러보세요",
            "지도 입장실패!",
            "당신이 소유한 모든 아이템의 내구도를 무한으로 설정합니다, 가격은 7,500 골드입니다. 계속하려면 예를 누르세요",
        };
        ptb=tb;
    }
    return ptb[n];
}

void mapTranslateFnStorage(int *getCb, int setCb)
{
    int cb;
    if (getCb)
    {
        getCb[0]=cb;
        return;
    }
    cb=setCb;
}

string MapDescTable(int sIndex)
{
    int cb;

    mapTranslateFnStorage(&cb,0);
    string ret = Bind(cb,&sIndex);
    return ret;
}

void InitializeMapTranslate()
{
    int cb;
    if (CheckGameKorLanguage()) cb=mapTranslateAsKor;
    else cb=mapTranslateAsEnglish;
    mapTranslateFnStorage(0,cb);
}


