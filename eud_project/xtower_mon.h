
#include "xtower_utils.h"
#include "libs/printutil.h"

void OpenWallsArea11()
{
    WallOpen(Wall(24, 196));
    WallOpen(Wall(23, 197));
    WallOpen(Wall(22, 198));
    WallOpen(Wall(21, 199));
    Start_wp = 14;
}

void OpenWallsArea12()
{
    UnlockDoor(Object("Area2Gate"));
    Start_wp = 16;
}

void OpenWallsArea13()
{
    int k;

    Start_wp = 40;
    for (k = 7 ; k >= 0 ; k --)
        WallOpen(Wall(39 + k, 115 + k));
}

void OpenWallsArea14()
{
    int k;

    Start_wp = 43;
    for (k = 7 ; k >= 0 ; k --)
        WallOpen(Wall(51 - k, 101 + k));
}

void OpenWallsArea15()
{
    int k;

    Start_wp = 55;
    for (k = 4 ; k >= 0 ; k --)
        WallOpen(Wall(42 - k, 58 + k));
}

void OpenWallsArea16()
{
    int k;

    Start_wp = 62;
    for (k = 4 ; k >= 0 ; k --)
        WallOpen(Wall(38 - k, 18 + k));
}

void OpenWallsArea1Boss()
{
    int k;

    Start_wp = 81;
    for (k = 3 ; k >= 0 ; k --)
        WallOpen(Wall(77 + k, 233 + k));
}

void OpenWallsArea21()
{
    Start_wp = 93;
}

void OpenWallsArea22()
{
    int k;

    Start_wp = 113;
    for (k = 6 ; k >= 0 ; k --)
        WallOpen(Wall(63 + k, 125 + k));
}

void OpenWallsArea23()
{
    Start_wp = 119;
    WallOpen(Wall(69, 85));
    WallOpen(Wall(68, 86));
    WallOpen(Wall(67, 87));
}

void OpenWallsArea24()
{
    Start_wp = 129;
}

void OpenWallsArea25()
{
    int k;
    Start_wp = 141;

    for (k = 6 ; k >= 0 ; k --)
        WallOpen(Wall(95 - k, 29 + k));
}

void OpenWallsArea2Boss()
{
    UniPrintToAll("보스를 처치하셨습니다, 방금 포탈이 열렸습니다");
    Start_wp = 194;
}

void OpenWallsArea31()
{
    int k;

    Start_wp = 208;
    for (k = 4 ; k >= 0 ; k --)
        WallOpen(Wall(130 - k, 198 + k));
}

void OpenWallsArea32()
{
    Start_wp = 214;
    WallOpen(Wall(114, 206));
    WallOpen(Wall(115, 207));
    WallOpen(Wall(116, 208));
    WallOpen(Wall(117, 209));
}

void OpenWallsArea33()
{
    int k;
    Start_wp = 215;
    for (k = 0 ; k < 8 ; k ++)
        WallOpen(Wall(101 + k, 175 + k));
}

void OpenWallsArea34()
{
    Start_wp = 224;
}

void OpenWallsArea35()
{
    Start_wp = 233;
    WallOpen(Wall(133, 127));
    WallOpen(Wall(134, 128));
    WallOpen(Wall(135, 129));
    WallOpen(Wall(136, 130));
    WallOpen(Wall(137, 131));
}

void OpenWallsArea36()
{
    Start_wp = 239;
}

void OpenWallsArea37()
{
    Start_wp = 251;
}

void OpenWallsArea38()
{
    Start_wp = 255;
    WallOpen(Wall(164, 48));
    WallOpen(Wall(165, 49));
    WallOpen(Wall(166, 50));
    WallOpen(Wall(167, 51));
    WallOpen(Wall(168, 52));
}

void DelaySpawnF371()
{
    int k;

    for (k = 0 ; k < 8 ; k ++)
    {
        SpawnHorrendous(246);
        SpawnMystic(247);
        MoveWaypoint(246, GetWaypointX(246) + 46.0, GetWaypointY(246) + 46.0);
        MoveWaypoint(247, GetWaypointX(247) + 46.0, GetWaypointY(247) + 46.0);
    }
}

void DelaySpawnF37()
{
    int k;

    for (k = 0 ; k < 8 ; k ++)
    {
        SpawnMonster("TalkingSkull", 244, 180, 200);
        SpawnMonster("Swordsman", 245, 325, 240);
        MoveWaypoint(244, GetWaypointX(244) + 46.0, GetWaypointY(244) + 46.0);
        MoveWaypoint(245, GetWaypointX(245) + 46.0, GetWaypointY(245) + 46.0);
    }
    FrameTimer(1, DelaySpawnF371);
}

void VictoryEvent()
{ }

int WizardRedBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[16] = 80000; arr[17] = 300; 
		arr[18] = 100; arr[19] = 50; arr[21] = 1065353216; arr[23] = 32800; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; 
		arr[54] = 4; arr[55] = 14; arr[56] = 24; 
		link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1065688760; 
		arr[25] = 0; arr[26] = 0; arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; arr[58] = 5546320; arr[59] = 5542784; 
        link = arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 80; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[27] = 0; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680;
        link = arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[19] = 1; arr[24] = 1065772646; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
        link = arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 470; 
		arr[19] = 55; arr[21] = 1065353216; arr[24] = 1071225242; arr[26] = 4; arr[28] = 1106247680; 
		arr[29] = 44; arr[31] = 3; arr[32] = 4; arr[33] = 5; arr[59] = 5542784; 
		arr[60] = 1388; arr[61] = 46915072; 
		link = arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 180; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1097859072; arr[29] = 25; arr[31] = 8; arr[32] = 13; arr[33] = 21; 
		arr[34] = 4; arr[35] = 2; arr[36] = 9; arr[37] = 1684631635; arr[38] = 1884516965; 
		arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 168; arr[19] = 71; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[27] = 3; arr[28] = 1106247680; 
		arr[29] = 10; arr[31] = 10; arr[32] = 13; arr[33] = 21; arr[37] = 1769236816; 
		arr[38] = 1181513062; arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 18; 
		arr[56] = 28; arr[58] = 5545472; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 325; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 34817; arr[24] = 1065353216; arr[26] = 4; 
		arr[37] = 1919248451; arr[38] = 1916887669; arr[39] = 7827314; arr[53] = 1133903872; arr[55] = 3; 
		arr[56] = 8; arr[58] = 5546320; arr[60] = 1387; arr[61] = 46915328; 
		link = arr;
	}
	return link;
}

int WoundedApprenticeBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 50; arr[18] = 10; arr[19] = 40; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 0; arr[24] = 1067450368; 
		arr[25] = 0; arr[26] = 4; arr[27] = 0; arr[28] = 1112014848; arr[29] = 40; 
		arr[30] = 1106247680; arr[31] = 2; arr[32] = 12; arr[33] = 20;
		arr[57] = 5548112; arr[58] = 0; arr[59] = 5542784; 
        link = arr;
	}
	return link;
}

int CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        arr[5] = MonsterGoonProcess; arr[72] = MonsterStrongWhiteWizProcess; arr[30] = MonsterWeirdlingBeastProcess; arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process;
        arr[12] = MonsterFireSpriteProcess; arr[73] = MonsterWizardRedProcess; arr[29] = MonsterAirshipCaptainProcess;
    }
    if (thingID && arr[key] != 0)
    {
        CallFunctionWithArg(arr[key], unit);
        return TRUE;
    }
    return FALSE;
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit); //, uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        /*uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }*/
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

void MonsterWoundedApprentice(int unit)
{
    //TODO: Index. 9, ThingName= WoundedApprentice
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedApprenticeBinTable());
        SetUnitMaxHealth(unit, 230);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}

void UnitSettingBased(int unit, int hp)
{
    CheckMonsterThing(unit);
    SetUnitMaxHealth(unit, hp + 1000);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    SetCallback(unit, 5, DeadMonster);
    // SetCallback(unit, 7, HurtMonster);
    SetOwner(MasterUnit(), unit);
}

// void HurtMonster()
// {
//     int plr;
//     if (CurrentHealth(SELF) <= 1000)
//     {
//         Damage(SELF, OTHER, CurrentHealth(SELF) + 1, 14);
//         plr = CheckOwner(OTHER);
//         if (plr >= 0)
//         {
//             ChangeGold(player[plr], ToInt(GetObjectZ(GetTrigger() + 1)));
//         }
//     }
// }

void DeadBossMonster()
{
    GoldEventWhenDeadBoss(ToInt(GetObjectZ(GetTrigger() + 1)));
    Delete(GetTrigger() + 1);
    CountDungeonMobDeaths();
}

int getNextRoomFn(int floor){
    int *d;

    if(d==0){
        int a[]={
            OpenWallsArea11,OpenWallsArea12,OpenWallsArea13,OpenWallsArea14,OpenWallsArea15,OpenWallsArea16,
            OpenWallsArea1Boss,
            OpenWallsArea21,OpenWallsArea22,OpenWallsArea23,OpenWallsArea24,OpenWallsArea25,
OpenWallsArea2Boss,
OpenWallsArea31,OpenWallsArea32,OpenWallsArea33,OpenWallsArea34,OpenWallsArea35,OpenWallsArea36,OpenWallsArea37,OpenWallsArea38,
            };d=a;
    }return d[floor];
}

void CountDungeonMobDeaths()
{
    int unit;

    SetMobCount(GetMobCount()-1);
    if (!GetMobCount())
    {
        int floor;
        QueryFloorLevel(&floor,0);
        if (floor < 22)
        {
            unit = CreateObject("Maiden", Exit_wp);
            ObjectOff(unit);
            Frozen(unit, TRUE);
            SetCallback(unit, 9, TeleportToNextArea);
            UniPrintToAll("다음 구역으로 가는 비밀계단이 개방됩니다");
            FrameTimer(1, getNextRoomFn( floor) );
            FrameTimer(1, FixGoingBackMark);
            return;
        }
        VictoryEvent();
    }
}

void DeadMonster()
{
    int rnd = Random(0, 4);

    if (!rnd)
    {
        int item = CreateObjectAt(ItemList(Random(0, 13)), GetObjectX(SELF), GetObjectY(SELF));

        CheckWeaponProperty(item);
        SetWeaponProperties(item, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    }
    else if (rnd == 1)
        GoldDrop(SELF);
    else
        CheckPotionThingID(CreateObjectAt(PotionList(Random(0, 8)), GetObjectX(SELF), GetObjectY(SELF)));
    DeleteObjectTimer(SELF, 90);
    Delete(GetTrigger() + 1);
    CountDungeonMobDeaths();
}

int SpawnBeholder(int wp)
{
    int unit = CreateObject("Beholder", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(833));
    UnitSettingBased(unit, 295);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SpawnBoss(string name, int wp, int hp, int score)
{
    int unit = CreateObject(name, wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    CheckMonsterThing(unit);
    Raise(unit + 1, ToFloat(score));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, hp);
    SetCallback(unit, 5, DeadBossMonster);
    return unit;
}

int SpawnMonster(string name, int wp, int hp, int score)
{
    int unit = CreateObject(name, wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(score));
    UnitSettingBased(unit, hp);
    return unit;
}

int SpawnImp(int wp)
{
    int unit = CreateObject("Imp", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(95));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    UnitSettingBased(unit, 64);
    SetCallback(unit, 3, ImpShots);
    return unit;
}

int SpawnMecaFlier(int wp)
{
    int unit = CreateObject("FlyingGolem", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(60));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    UnitSettingBased(unit, 70);
    SetCallback(unit, 3, MecaFlyingWeapon);
    return unit;
}

int SpawnStaffLich(int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr = GetMemory(0x750710);

    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(300));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(32));
    UnitSettingBased(unit,  225);
    //SetCallback(unit, 3, LichWeapon);
    return unit;
}

int SpawnBear(int wp)
{
    int unit = CreateObject("Bear", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(300));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    UnitSettingBased(unit, 250);
    SetCallback(unit, 3, BearJumping);
    return unit;
}

int SpawnDryad(int wp)
{
    int unit = CreateObject("WizardGreen", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(300));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    UnitSettingBased(unit, 175);
    return unit;
}

int SpawnShopkeeper(int wp)
{
    int unit = CreateObject("Bear2", wp);

    Raise(CreateObject("InvisibleLightBlueHigh", wp), ToFloat(30));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    UnitSettingBased(unit, 275);
    return unit;
}

int SpawnGoon(int wp)
{
    int unit = CreateObject("Goon", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(30));
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    UnitSettingBased(unit, 128);
    return unit;
}

int SpawnFireSprite(int wp)
{
    int unit = CreateObject("FireSprite", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(60));
    SetUnitSpeed(unit, 0.8);
    UnitSettingBased(unit, 96);
    return unit;
}

int SpawnPlant(int wp)
{
    int unit = CreateObject("CarnivorousPlant", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(450));
    SetUnitSpeed(unit, 1.0);
    UnitSettingBased(unit, 300);
    AggressionLevel(unit, 1.0);
    return unit;
}

int SpawnBlackSpider(int wp)
{
    int unit = CreateObject("BlackWidow", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(328));
    UnitSettingBased(unit, 245);
    SetCallback(unit, 5, SpiderWhenDead);
    return unit;
}

int SpawnDeadWorker(int wp)
{
    int unit = CreateObject("VileZombie", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(411));
    SetUnitSpeed(unit, 1.2);
    UnitSettingBased(unit, 325);
    SetCallback(unit, 3, DeadWorkerWeapon);
    return unit;
}

int SpawnMystic(int wp)
{
    int unit = CreateObject("Wizard", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(400));
    UnitSettingBased(unit, 175);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    MysticLoop(unit);
    return unit;
}

int SpawnWhiteWiz(int wp)
{
    int unit = CreateObject("WizardWhite", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(480));
    UnitSettingBased(unit, 237);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SpawnHorrendous(int wp)
{
    int unit = CreateObject("Horrendous", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(780));
    SetUnitSpeed(unit, 0.9);
    UnitSettingBased(unit, 300);
    SetCallback(unit, 3, HorrendousDash);
    return unit;
}

void PlacedMobF1()
{
    SetMobCount(6);
    Exit_wp = 13;
    SpawnMonster("Swordsman", 10, 100, 70);
    SpawnMonster("Swordsman", 10, 100, 70);
    SpawnMonster("Swordsman", 10, 100, 70);
    SpawnMecaFlier(11);
    SpawnMecaFlier(12);
    SpawnMecaFlier(12);
}

void PlacedMobF2()
{
    SetMobCount(18);
    Exit_wp = 15;
    SpawnMonster("Ghost", 17, 60, 50);
    SpawnMonster("Ghost", 18, 60, 50);
    SpawnMonster("Ghost", 19, 60, 50);
    SpawnMonster("Ghost", 20, 60, 50);
    SpawnMonster("Skeleton", 21, 150, 90);
    SpawnMonster("Skeleton", 22, 150, 90);
    SpawnMonster("SkeletonLord", 21, 220, 130);
    SpawnMonster("SkeletonLord", 22, 220, 130);
    SpawnMonster("EvilCherub", 23, 80, 50);
    SpawnMonster("EvilCherub", 24, 80, 50);
    SpawnMonster("EvilCherub", 25, 80, 50);
    SpawnMonster("EvilCherub", 26, 80, 50);
    SpawnStaffLich(27);
    SpawnStaffLich(28);
    SpawnMonster("SkeletonLord", 29, 220, 160);
    SpawnMonster("SkeletonLord", 30, 220, 160);
    SpawnMonster("SkeletonLord", 31, 220, 160);
    SpawnMonster("SkeletonLord", 32, 220, 160);
}

void PlacedMobF3()
{
    SetMobCount(12);
    Exit_wp = 39;
    SpawnMonster("BlackBear", 33, 225, 250);
    SpawnMonster("BlackBear", 34, 225, 250);
    SpawnMonster("Shade", 33, 128, 180);
    SpawnMonster("Shade", 34, 128, 180);
    SpawnBear(35);
    SpawnMonster("SpittingSpider", 35, 95, 80);
    SpawnBear(36);
    SpawnMonster("SpittingSpider", 36, 95, 80);
    SpawnDryad(37);
    SpawnBear(38);
    SpawnMonster("Shade", 37, 128, 180);
    SpawnMonster("Shade", 37, 128, 180);
}

void PlacedMobF4()
{
    SetMobCount(27);
    Exit_wp = 42;
    SpawnMonster("Shade", 44, 128, 80);
    SpawnMonster("Shade", 44, 128, 80);
    SpawnGoon(44);
    SpawnGoon(44);
    SpawnDryad(45);
    SpawnDryad(46);
    SpawnMonster("Shade", 47, 128, 80);
    SpawnMonster("Shade", 47, 128, 80);
    SpawnMonster("BlackWolf", 48, 195, 105);
    SpawnMonster("BlackWolf", 48, 195, 105);
    SpawnMonster("BlackWolf", 48, 195, 105);
    SpawnGoon(49);
    SpawnGoon(49);
    SpawnGoon(49);
    SpawnDryad(50);
    SpawnDryad(51);
    SpawnGoon(52);
    SpawnMonster("Shade", 52, 128, 80);
    SpawnMonster("Shade", 52, 128, 80);
    SpawnMonster("Shade", 52, 128, 80);
    SpawnMonster("Scorpion", 53, 225, 130);
    SpawnMonster("Scorpion", 53, 225, 130);
    SpawnMonster("Scorpion", 53, 225, 130);
    SpawnMonster("BlackWolf", 53, 195, 105);
    SpawnMonster("BlackWolf", 53, 195, 105);
    SpawnMonster("Scorpion", 53, 225, 130);
    SpawnMonster("Scorpion", 53, 225, 130);
}

void PlacedMobF5()
{
    SetMobCount(18);
    Exit_wp = 54;
    SpawnMonster("Imp", 58, 60, 30);
    SpawnMonster("Imp", 58, 60, 30);
    SpawnMonster("MeleeDemon", 58, 128, 210);
    SpawnMonster("MeleeDemon", 58, 128, 210);
    SpawnMonster("MeleeDemon", 58, 128, 210);
    SpawnMonster("MeleeDemon", 58, 128, 210);
    SpawnMonster("MeleeDemon", 58, 128, 210);
    SpawnFireSprite(59);
    SpawnFireSprite(56);
    SpawnMonster("EmberDemon", 56, 108, 230);
    SpawnFireSprite(57);
    SpawnMonster("EmberDemon", 57, 108, 230);
    SpawnFireSprite(60);
    SpawnMonster("EmberDemon", 60, 108, 230);
    SpawnMonster("EmberDemon", 59, 108, 230);
    SpawnMonster("MeleeDemon", 59, 128, 210);
    SpawnMonster("MeleeDemon", 59, 128, 210);
    SpawnMonster("MeleeDemon", 59, 128, 210);
}

void PlacedMobF6()
{
    SetMobCount(22);
    Exit_wp = 61;
    SpawnShopkeeper(67);
    SpawnShopkeeper(67);
    SpawnShopkeeper(67);
    SpawnMonster("GruntAxe", 67, 260, 340);
    SpawnMonster("OgreWarlord", 63, 325, 420);
    SpawnMonster("OgreBrute", 63, 325, 420);
    SpawnMonster("OgreBrute", 63, 325, 420);
    SpawnMonster("GruntAxe", 64, 325, 420);
    SpawnMonster("GruntAxe", 64, 325, 420);
    SpawnMonster("GruntAxe", 64, 325, 420);
    SpawnGoon(66);
    SpawnGoon(66);
    SpawnShopkeeper(65);
    SpawnShopkeeper(65);
    SpawnMonster("OgreWarlord", 65, 325, 440);
    SpawnMonster("OgreWarlord", 65, 325, 440);
    SpawnGoon(65);
    SpawnMonster("GruntAxe", 68, 260, 340);
    SpawnMonster("GruntAxe", 68, 260, 340);
    SpawnMonster("OgreBrute", 68, 325, 420);
    SpawnMonster("OgreBrute", 68, 325, 420);
    SpawnMonster("Scorpion", 68, 240, 130);
}

void PlacedMobBoss1()
{
    int boss;
    SetMobCount(8);
    Exit_wp = 69;
    SpawnPlant(74);
    SpawnPlant(75);
    SpawnPlant(76);
    SpawnMonster("UrchinShaman", 70, 98, 260);
    SpawnMonster("UrchinShaman", 71, 98, 260);
    SpawnMonster("UrchinShaman", 72, 98, 260);
    SpawnMonster("UrchinShaman", 73, 98, 260);
    boss = SpawnBoss("StoneGolem", 77, 1050, 1700);
    SetCallback(boss, 3, BossGolemWeapon);
    SetCallback(boss, 9, GolemTouchedOnNormal);
}

void PlacedMobF21()
{
    SetMobCount(14);
    Exit_wp = 82;
    SpawnMystic(90);
    SpawnMystic(91);
    SpawnMonster("Zombie", 83, 97, 60);
    SpawnMonster("Zombie", 84, 97, 60);
    SpawnMonster("Zombie", 85, 97, 60);
    SpawnMonster("Zombie", 86, 97, 60);
    SpawnMonster("SkeletonLord", 83, 225, 110);
    SpawnMonster("SkeletonLord", 84, 225, 110);
    SpawnMonster("SkeletonLord", 85, 225, 110);
    SpawnMonster("SkeletonLord", 86, 225, 110);
    SpawnDeadWorker(87);
    SpawnMonster("Ghost", 88, 60, 20);
    SpawnDeadWorker(89);
    SpawnMonster("WillOWisp", 92, 300, 450);
}

void PlacedMobF22()
{
    SetMobCount(16);
    Exit_wp = 94;

    SpawnBlackSpider(99);
    SpawnBlackSpider(102);
    SpawnBlackSpider(105);
    SpawnBlackSpider(108);
    SpawnMonster("WillOWisp", 96, 300, 450);
    SpawnMonster("WillOWisp", 101, 300, 450);
    SpawnMonster("WillOWisp", 106, 300, 450);
    SpawnMonster("WillOWisp", 111, 300, 450);
    SpawnImp(97);
    SpawnImp(98);
    SpawnImp(100);
    SpawnImp(103);
    SpawnImp(104);
    SpawnImp(107);
    SpawnImp(109);
    SpawnImp(110);
}

void PlacedMobF23()
{
    int k;

    SetMobCount(36);
    Exit_wp = 112;

    SpawnMonster("Lich", 114, 350, 560);
    SpawnMonster("Lich", 115, 350, 560);
    for (k = 0 ; k < 17 ; k ++)
    {
        SpawnMonster("SkeletonLord", 116, 250, 200);
        SpawnMonster("Skeleton", 117, 195, 137);
        MoveWaypoint(116, GetWaypointX(116) + 23.0, GetWaypointY(116) + 23.0);
        MoveWaypoint(117, GetWaypointX(117) + 23.0, GetWaypointY(117) + 23.0);
    }
}

void PlacedMobF24()
{
    int k;

    SetMobCount(53);
    Exit_wp = 118;
    for (k = 16 ; k >= 0 ; k --)
    {
        SpawnMonster("EvilCherub", 124, 64, 100);
        SpawnMonster("Shade", 125, 128, 160);
        SpawnBlackSpider(126);
        MoveWaypoint(124, GetWaypointX(124) - 23.0, GetWaypointY(124) + 23.0);
        MoveWaypoint(125, GetWaypointX(125) - 23.0, GetWaypointY(125) + 23.0);
        MoveWaypoint(126, GetWaypointX(126) - 23.0, GetWaypointY(126) + 23.0);
    }
    Enchant(SpawnMonster("StoneGolem", 127, 700, 842), "ENCHANT_INVISIBLE", 0.0);
    Enchant(SpawnMonster("StoneGolem", 128, 700, 842), "ENCHANT_INVISIBLE", 0.0);
}

void PlacedMobF25()
{
    int k;

    SetMobCount(60);
    Exit_wp = 130;
    for (k = 17 ; k >= 0 ; k --)
    {
        SpawnImp(131);
        SpawnMonster("MeleeDemon", 132, 98, 226);
        SpawnDeadWorker(133);
        MoveWaypoint(131, GetWaypointX(131) - 23.0, GetWaypointY(131) + 23.0);
        MoveWaypoint(132, GetWaypointX(132) - 23.0, GetWaypointY(132) + 23.0);
        MoveWaypoint(133, GetWaypointX(133) - 23.0, GetWaypointY(133) + 23.0);
    }
    SpawnMystic(135);
    SpawnMystic(136);
    SpawnMystic(137);
    SpawnMystic(138);
    SpawnMystic(139);
    SpawnMonster("Demon", 134, 400, 360);
}

void PlacedMobF26()
{
    int k;

    SetMobCount(35);
    Exit_wp = 142;

    for (k = 14 ; k >= 0 ; k --)
    {
        SpawnMonster("WillOWisp", 140, 225, 472);
        SpawnFireSprite(140);
    }
    SpawnBeholder(143);
    SpawnBeholder(144);
    SpawnBeholder(145);
    SpawnBeholder(146);
    SpawnBeholder(147);
}

void PlacedMobBoss2()
{
    int boss;

    SetMobCount(1);
    Exit_wp = 149;

    boss = SpawnBoss("WizardRed", 190, 2080, 3800);
    SetUnitStatus(boss, GetUnitStatus(boss) ^ 0x20);
    SetCallback(boss, 3, SecondBossWeapon);
}

void PlacedMobF31()
{
    SetMobCount(12);
    Exit_wp = 195;
    SpawnMonster("EmberDemon", 204, 98, 225);
    SpawnMonster("EmberDemon", 205, 98, 225);
    SpawnMonster("EmberDemon", 206, 98, 225);
    SpawnMonster("EmberDemon", 207, 98, 225);
    SpawnMonster("WillOWisp", 196, 225, 370);
    SpawnMonster("WillOWisp", 197, 225, 370);
    SpawnMonster("WillOWisp", 198, 225, 370);
    SpawnMonster("WillOWisp", 199, 225, 370);
    SpawnHorrendous(200);
    SpawnHorrendous(201);
    SpawnHorrendous(202);
    SpawnHorrendous(203);
}

void PlacedMobF32()
{
    int k;
    SetMobCount(44);
    Exit_wp = 209;

    for (k = 0 ; k < 18 ; k ++)
    {
        SpawnMonster("Swordsman", 210, 306, 400);
        SpawnMonster("Archer", 211, 98, 78);
        if (k < 8)
        {
            SpawnHorrendous(212);
            MoveWaypoint(212, GetWaypointX(212) + 30.0, GetWaypointY(212) + 30.0);
        }
        MoveWaypoint(210, GetWaypointX(210) + 23.0, GetWaypointY(210) + 23.0);
        MoveWaypoint(211, GetWaypointX(211) + 23.0, GetWaypointY(211) + 23.0);
    }
}

void PlacedMobF33()
{
    int k;
    SetMobCount(78);
    Exit_wp = 213;
    
    for (k = 0 ; k < 18 ; k ++)
    {
        SpawnMonster("Shade", 217, 98, 80);
        SpawnMonster("SpittingSpider", 218, 64, 40);
        SpawnMonster("MeleeDemon", 219, 98, 91);
        SpawnMonster("SkeletonLord", 220, 240, 105);
        if (k < 6)
        {
            SpawnMystic(221);
            MoveWaypoint(221, GetWaypointX(221) + 30.0, GetWaypointY(221) + 30.0);
        }
        MoveWaypoint(217, GetWaypointX(217) + 23.0, GetWaypointY(217) + 23.0);
        MoveWaypoint(218, GetWaypointX(218) + 23.0, GetWaypointY(218) + 23.0);
        MoveWaypoint(219, GetWaypointX(219) + 23.0, GetWaypointY(219) + 23.0);
        MoveWaypoint(220, GetWaypointX(220) + 23.0, GetWaypointY(220) + 23.0);
    }
}

void PlacedMobF34()
{
    int k;
    SetMobCount(14);
    Exit_wp = 216;
    for (k = 0 ; k < 7 ; k ++)
    {
        SpawnWhiteWiz(222);
        SpawnMystic(223);
    }
}

void PlacedMobF35()
{
    int k;

    SetMobCount(14);
    Exit_wp = 225;
    SpawnMonster("MechanicalGolem", 226, 700, 1000);
    SpawnMonster("MechanicalGolem", 227, 700, 1000);
    SpawnMonster("MechanicalGolem", 228, 700, 1000);
    SpawnMonster("MechanicalGolem", 229, 700, 1000);

    for (k = 0 ; k < 5 ; k ++)
    {
        SpawnDeadWorker(230);
        SpawnMonster("WeirdlingBeast", 231, 175, 600);
        MoveWaypoint(230, GetWaypointY(230) - 32.0, GetWaypointY(230) + 32.0);
        MoveWaypoint(231, GetWaypointY(231) - 32.0, GetWaypointY(231) + 32.0);
    }
}

void PlacedMobF36()
{
    int k;
    SetMobCount(25);
    Exit_wp = 232;
    
    for (k = 4 ; k >= 0 ; k --)
    {
        SpawnMonster("WeirdlingBeast", 234, 147, 620);
        SpawnMonster("WoundedApprentice", 235, 237, 700);
        SpawnBlackSpider(236);
        SpawnMystic(237);
        SpawnMonster("OgreWarlord", 238, 325, 340);
        MoveWaypoint(234, GetWaypointX(234) + 46.0, GetWaypointY(234));
        MoveWaypoint(235, GetWaypointX(235) + 46.0, GetWaypointY(235));
        MoveWaypoint(236, GetWaypointX(236) + 46.0, GetWaypointY(236));
        MoveWaypoint(237, GetWaypointX(237) + 46.0, GetWaypointY(237));
        MoveWaypoint(238, GetWaypointX(238) + 46.0, GetWaypointY(238));
    }
}

void PlacedMob37()
{
    int k;
    SetMobCount(8*7);
    Exit_wp = 240;

    for (k = 0 ; k < 8 ; k ++)
    {
        SpawnImp(241);
        SpawnFireSprite(242);
        SpawnMonster("GruntAxe", 243, 237, 130);
        MoveWaypoint(241, GetWaypointX(241) + 46.0, GetWaypointY(241) + 46.0);
        MoveWaypoint(242, GetWaypointX(242) + 46.0, GetWaypointY(242) + 46.0);
        MoveWaypoint(243, GetWaypointX(243) + 46.0, GetWaypointY(243) + 46.0);
    }
    FrameTimer(1, DelaySpawnF37);
}

void PlacedMob38()
{
    int k, boss;
    SetMobCount(12);
    Exit_wp = 252;
    
    for (k = 0 ; k < 8 ; k ++)
    {
        SpawnHorrendous(248);
        if (k < 3)
        {
            SpawnMonster("MechanicalGolem", 249, 750, 900);
            MoveWaypoint(249, GetWaypointX(249) - 46.0, GetWaypointY(249) + 46.0);
        }
        MoveWaypoint(248, GetWaypointX(248) - 23.0, GetWaypointY(248) + 23.0);
    }
    boss = SpawnBoss("Demon", 250, 1260, 3000);
    SetCallback(boss, 3, ThirdBossSkills);
}

void PlacedCastleTop()
{
    int boss, addr;

    SetMobCount(1);
    Exit_wp = 256;
    boss = SpawnBoss("Goon", 257, 2000, 300000);
    CreateObject("InvisibleLightBlueHigh", 258);
    addr = UnitToPtr(boss);
    if (addr)
    {
        SetMemory(addr + 0x14, 0x22);                           //can't visible
        SetMemory(addr + 0xc, GetMemory(addr + 0xc) ^ 0x200);   //Immune_poison
    }
    SetCallback(boss, 3, KeeperWaepon);
    LookWithAngle(boss, 0);
    FrameTimerWithArg(1, boss, LoopDetectPlayers);
}

void DestinationCastleTop()
{
    SetMobCount(9999);
    Exit_wp = 258;
}

int GetPlaceMobFunction(int floor){
    /*
    PlacedMobF1
6

PlacedMobBoss1
PlacedMobF21
6

PlacedMobBoss2
PlacedMobF31
8
PlacedCastleTop
DestinationCastleTop
    */
   int *d;
   if (d==0){
    int a[]={
        PlacedMobF1,PlacedMobF2,PlacedMobF3,PlacedMobF4,PlacedMobF5,PlacedMobF6,
        PlacedMobBoss1,
        PlacedMobF21,PlacedMobF22,PlacedMobF23,PlacedMobF24,PlacedMobF25,PlacedMobF26,
        PlacedMobBoss2,
        PlacedMobF31,PlacedMobF32,PlacedMobF33,PlacedMobF34,PlacedMobF35,PlacedMobF36,PlacedMob37,PlacedMob38,
        PlacedCastleTop,DestinationCastleTop,
    };d=a;
   }return d[floor];
}
