
#include "subway_resource.h"
#include "subway_shop.h"
#include "subway_player.h"
#include "subway_initscan.h"
#include "subway_reward.h"
#include "subway_mon.h"
#include "libs/coopteam.h"
#include "libs\itemproperty.h"
#include "libs\reaction.h"
#include "libs\observer.h"
#include "libs\printutil.h"
#include "libs\unitstruct.h"
#include "libs/winapi.h"
#include "libs/recovery.h"
#include "libs/game_flags.h"

// int player[20], PlrCre[10], AGate[8];
// int LUnit[10], DLord[21];

#define SHOP_NOT_ENOUGH_GOLD    "잔액이 부족합니다"

//2180 Blue
//2182 Red

void VictoryEvent()
{
    int ptr, unit;

    if (CurrentHealth(OTHER))
    {
        unit = GetPlayerUnit(OTHER);
        if (unit < 0) return;
        if (IsObjectOn(SELF) && !ptr)
        {
            float x=GetObjectX(OTHER), y=GetObjectY(OTHER);
            ptr = DispositionTransport(LocationX(50),LocationY(50),LocationX(51),LocationY(51));
            DeleteObjectTimer(CreateObjectById(OBJ_OBLIVION_UP, x,y), 150);
            Effect("WHITE_FLASH", x,y, 0.0, 0.0);
            PlaySoundAround(OTHER, SOUND_StaffOblivionAchieve1);
            Delete(SELF);
            UniPrintToAll("축하드립니다! 승리하셨습니다!");
        }
    }
}

void OpenExitWalls()
{
    int i;

    for (i = 0 ; i < 7 ; i ++)
        WallOpen(Wall(138 + i, 158 - i));
}

void DoCheckDemonDeathCount(int count)
{
    int ptr;

    if (count == DEMON_MAX_COUNT && !ptr)
    {
        OpenExitWalls();
        ptr = DispositionTransport(LocationX(48),LocationY(48), LocationX(49), LocationY(49));
        SetCallback(PutBeacon(49), 9, VictoryEvent);
        UniPrintToAll("출구가 열렸습니다");
    }
}

void StartSummonDemonlord(int ptr)
{
    int count = GetDirection(ptr), unit;

    if (count < DEMON_MAX_COUNT)
    {
        SpawnDragon(ptr);
        GreenSparkAt(GetObjectX(ptr), GetObjectY(ptr));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(8, ptr, StartSummonDemonlord);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void OpenDragonWalls()
{
    int i;

    for (i = 0 ; i < 13 ; i ++)
        WallOpen(Wall(i + 146, 172 - i));
}

void CheckKeybaseOn()
{
    int ptr;

    if (IsObjectOn(Object("RedKeyBase")) && IsObjectOn(Object("BlueKeyBase")) && !ptr)
    {
        ObjectOn(Object("LeftBossLit"));
        ObjectOn(Object("RightBossLit"));
        ptr = CreateObject("InvisibleLightBlueHigh", 47);
        CreateObject("BlueSummons", 47);
        CreateObject("ForceOfNatureCharge", 47);
        AudioEvent("DemonDie", 47);
        AudioEvent("HorrendousIsKilled", 47);
        OpenDragonWalls();
        FrameTimerWithArg(45, ptr, StartSummonDemonlord);
        UniPrintToAll("전설의 레드 드래곤이 등장합니다, 레드 드레곤을 모두 물리치면 출구가 열립니다");
    }
}

void CheckPutRedKey()
{
    if (GetUnitThingID(OTHER) == 2182)
    {
        Effect("VIOLET_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("Gear2", 1);
        ObjectOn(Object("RedKeyBase"));
        Delete(OTHER);
        Delete(SELF);
        CheckKeybaseOn();
    }
}

void CheckPutBlueKey()
{
    if (GetUnitThingID(OTHER) == 2180)
    {
        Effect("BLUE_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("Gear2", 1);
        ObjectOn(Object("BlueKeyBase"));
        Delete(OTHER);
        Delete(SELF);
        CheckKeybaseOn();
    }
}

void OpenPart1Gate()
{
    int unit = GetPlayerUnit(OTHER);
    if (unit)
    {
        UnlockDoor(Object("MecaGate1"));
        UnlockDoor(Object("MecaGate2"));
        ObjectOff(SELF);
        UniPrint(unit, "역무원실 출입문의 잠금이 해제되었습니다");
    }
}

int Part2EntranceBlock()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("IronBlock", 36);
        CreateObject("IronBlock", 37);
        CreateObject("IronBlock", 38);
    }
    return ptr;
}

void Part2BlocksMoving(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 80 && IsObjectOn(ptr))
    {
        MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) - 2.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 2.0, GetObjectY(ptr + 1) - 2.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + 2.0, GetObjectY(ptr + 2) - 2.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, Part2BlocksMoving);
    }
}

void EnableMyMecaGears()
{
    int i;

    for (i = 0 ; i < 12 ; i ++)
        ObjectOn(Object("SeoulMetrolGear" + IntToString(i + 1)));
}

void MecaGearActivateFx(float x, float y)
{
    MoveWaypoint(1, x, y);
    AudioEvent("MechGolemDie", 1);
    DeleteObjectTimer(CreateObject("BigSmoke", 1), 9);
    Effect("DAMAGE_POOF", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
    Effect("JIGGLE", GetWaypointX(1), GetWaypointY(1), 30.0, 0.0);
}

void RollPart2Blocks()
{
    int ptr = Part2EntranceBlock(), unit = GetPlayerUnit(OTHER);

    if (unit)
    {
        MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("CreatureCageAppears", 1);
        AudioEvent("ChainPull", 1);

        MecaGearActivateFx(1578.0, 1878.0);
        MecaGearActivateFx(1464.0, 1997.0);

        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        FrameTimerWithArg(15, ptr, Part2BlocksMoving);
        int lunit;
        QueryLUnit(&lunit,0,2);
        FrameTimerWithArg(30, lunit, AwakeLineMonster);
        AudioEvent("SpikeBlockMove", 1);
        UniPrintToAll("지하 2층 대합실의 비밀벽이 열렸습니다");
        ObjectOff(SELF);
    }
}

void OpenPairWalls(int x, int y)
{
    WallOpen(Wall(x, y));
    WallOpen(Wall(x + 1, y - 1));
}

void OpenWallAtAllign(int x, int y)
{
    OpenPairWalls(x, y);
    OpenPairWalls(x + 9, y - 9);
    OpenPairWalls(x + 18, y - 18);
    OpenPairWalls(x + 27, y - 27);
}

void OpenFirstArea()
{
    int unit = GetPlayerUnit(OTHER);

    if (unit)
    {
        OpenWallAtAllign(163, 57);
        OpenWallAtAllign(125, 95);
        OpenWallAtAllign(87, 133);
        OpenWallAtAllign(49, 171);
        OpenWallAtAllign(11, 209);
        QueryLUnit(&unit,0,1);
        FrameTimerWithArg(3, unit, AwakeLineMonster);
        ObjectOff(SELF);
        UniPrintToAll("열차의 모든 출입문을 개방하였습니다");
    }
}

void OpenSecondArea()
{
    int unit = GetPlayerUnit(OTHER);

    if (unit)
    {
        OpenWallAtAllign(36, 234);
        OpenWallAtAllign(74, 196);
        OpenWallAtAllign(112, 158);
        OpenWallAtAllign(150, 120);
        OpenWallAtAllign(188, 82);
        int lunit;
        QueryLUnit(&lunit,0,3);
        FrameTimerWithArg(3, lunit, AwakeLineMonster);
        ObjectOff(SELF);
        UniPrintToAll("열차의 모든 출입문을 개방하였습니다");
    }
}

int GetPlayerUnit(int unit)
{
    if (IsPlayerUnit(unit))
        return unit;
    if (IsPlayerUnit(GetOwner(unit)))
        return GetOwner(unit);
    return 0;
}

int GetMaster()
{
    int unit;
    
    if (!unit)
    {
        unit = CreateObjectAt("Hecubah", 5500.0, 100.0);
        MoveWaypoint(1, GetObjectX(unit), GetObjectY(unit));
        CreateObject("BlackPowder", 1);
        Frozen(unit, 1);
        //SetCallback(unit, 9, DisplayLadderBoard);
    }
    return unit;
}

void DisplayLadderBoard()
{
    /*string txt = "플레이어 킬 스코어:\n";
    int scd, min, hor;

    if (IsCaller(GetTrigger() + 1))
    {
        if (GetDirection(SELF) < 30)
            LookWithAngle(SELF, GetDirection(SELF) + 1);
        else
        {
            LookWithAngle(SELF, 0);
            txt += (IntToString(GenCnt) + "\n\n데스: \n" + IntToString(PlrDeadCnt) + "\n\n경과시간:\n");
            scd ++;
            if (scd == 60)
            {
                scd = 0;
                min ++;
                if (min == 60)
                {
                    min = 0;
                    hor ++;
                }
            }
            txt += (IntToString(hor) + "시간 " + IntToString(min) + "분 " + IntToString(scd) + "초");
            Chat(SELF, txt);
        }
    }*/
    return;
}

int PutBeacon(int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 10);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    return unit;
}

int SpawnDummy(string name, int dir, int wp)
{
    int unit = CreateObject(name, wp);

    ObjectOff(unit);
    LookWithAngle(unit, dir);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);

    return unit;
}

void MapDecorations()
{
    Part2EntranceBlock();
    SetCallback(PutBeacon(45), 9, CheckPutRedKey);
    SetCallback(PutBeacon(46), 9, CheckPutBlueKey);
    CreateObject("RedOrbKeyOfTheLich", 43);
    CreateObject("BlueOrbKeyOfTheLich", 44);
    CreateObject("BlackPowderBarrel", 34);
    CreateObject("BlackPowderBarrel", 35);
    CreateObject("BlackPowderBarrel2", 39);
    CreateObject("BlackPowderBarrel", 40);
    CreateObject("BlackPowderBarrel2", 41);
    CreateObject("BlackPowderBarrel", 42);
}

void InitAutoGates(int arg0)
{
    if (arg0 >= 0)
    {
        QueryAGate(0, SpawnAutoGate(arg0 + 25, 32.0),arg0);
        SpawnAutoGateBeacon(arg0 + 25, arg0);
        FrameTimerWithArg(1, arg0 - 1, InitAutoGates);
    }
}

int SpawnAutoGateBeacon(int wp, int idx)
{
    int unit = CreateObjectAt("WeirdlingBeast", GetWaypointX(wp) - 39.0, GetWaypointY(wp) + 39.0);
    LookWithAngle(CreateObjectAt("WeirdlingBeast", GetWaypointX(wp) + 39.0, GetWaypointY(wp) - 39.0), idx);
    LookWithAngle(CreateObjectAt("WeirdlingBeast", GetWaypointX(wp), GetWaypointY(wp)), idx);

    SetUnitMaxHealth(unit, 10);
    SetUnitMaxHealth(unit + 1, 10);
    SetUnitMaxHealth(unit + 2, 10);
    Enchant(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), "ENCHANT_ANCHORED", 0.0);
    Enchant(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit + 1), GetObjectY(unit + 1)), "ENCHANT_ANCHORED", 0.0);
    Enchant(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit + 2), GetObjectY(unit + 2)), "ENCHANT_ANCHORED", 0.0);
    Frozen(CreateObjectAt("IronBlock", GetWaypointX(wp) - 81.0, GetWaypointY(wp) - 81.0), 1);
    Frozen(CreateObjectAt("IronBlock", GetWaypointX(wp) + 81.0, GetWaypointY(wp) + 81.0), 1);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Damage(unit + 1, 0, MaxHealth(unit + 1) + 1, -1);
    Damage(unit + 2, 0, MaxHealth(unit + 2) + 1, -1);
    LookWithAngle(unit, idx);
    SetCallback(unit, 9, StartMoveAGate);
    SetCallback(unit + 1, 9, StartMoveAGate);
    SetCallback(unit + 2, 9, StartMoveAGate);
    return unit;
}

int SpawnAutoGate(int wp, float gap) //gap is 32.0
{
    int unit = CreateObject("InvisibleLightBlueHigh", wp);

    Frozen(CreateObjectAt("MovableBookcase4", GetObjectX(unit), GetObjectY(unit) - gap), 1);
    Frozen(CreateObjectAt("MovableBookcase4", GetObjectX(unit) + gap, GetObjectY(unit)), 1);
    Frozen(CreateObjectAt("MovableBookcase4", GetObjectX(unit) - gap, GetObjectY(unit)), 1);
    Frozen(CreateObjectAt("MovableBookcase4", GetObjectX(unit), GetObjectY(unit) + gap), 1);
    return unit;
}

void StartMoveAGate()
{
    int unit = -1;

    if (CurrentHealth(OTHER))
    {
        int gate;
        QueryAGate(&gate, 0,GetDirection(SELF));
        if (OpenAutoGate(gate))
        {
            PlaySoundAround(OTHER, SOUND_SpikeBlockMove);
            if (IsPlayerUnit(OTHER))
                unit = GetCaller();
            else if (IsPlayerUnit(GetOwner(OTHER)))
                unit = GetOwner(OTHER);
            if (unit >= 0)
                UniPrint(unit, "열차 통로 출입문이 열립니다");
        }
    }
}

int OpenAutoGate(int ptr)
{
    if (!GetDirection(ptr + 1))
    {
        LookWithAngle(ptr + 1, 1);
        FrameTimerWithArg(1, ptr, OpenGateHandler);
        return 1;
    }
    return 0;
}

void OpenGateHandler(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 40)
    {
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - 1.0, GetObjectY(ptr + 1) - 1.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + 1.0, GetObjectY(ptr + 2) + 1.0);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) - 1.0, GetObjectY(ptr + 3) - 1.0);
        MoveObject(ptr + 4, GetObjectX(ptr + 4) + 1.0, GetObjectY(ptr + 4) + 1.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, OpenGateHandler);
    }
    else
    {
        LookWithAngle(ptr, 0);
        FrameTimerWithArg(80, ptr, CloseGateHandler);
    }
}

void CloseGateHandler(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 40)
    {
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 1.0, GetObjectY(ptr + 1) + 1.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) - 1.0, GetObjectY(ptr + 2) - 1.0);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) + 1.0, GetObjectY(ptr + 3) + 1.0);
        MoveObject(ptr + 4, GetObjectX(ptr + 4) - 1.0, GetObjectY(ptr + 4) - 1.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, CloseGateHandler);
    }
    else
    {
        LookWithAngle(ptr, 0);
        LookWithAngle(ptr + 1, 0);
    }
}

void EndScan(int *pParams)
{
    int count =pParams[UNITSCAN_PARAM_COUNT];
    char msg[128];

    NoxSprintfString(msg, "%d개의 유닛을 스캔했습니다", &count, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
    WallOpen(Wall(15, 235));
    WallOpen(Wall(16, 236));
    int lunit;
    QueryLUnit(&lunit,0,0);
    AwakeLineMonster(lunit);
    SecondTimer(2, StartGameMent);
}

int UnitEquipedWeapon(int unit)
{
    int ptr = UnitToPtr(unit), pic;
    
    if (ptr)
    {
        pic = GetMemory(GetMemory(ptr + 0x2ec) + 0x810);
        if (pic)
            return GetMemory(pic + 0x2c);
    }
    return 0;
}

void CheckResetSight(int unit, int delay)
{
    if (!HasEnchant(unit, "ENCHANT_DETECTING"))
    {
        Enchant(unit, "ENCHANT_DETECTING", 0.0);
        FrameTimerWithArg(delay, unit, ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.06);
    AggressionLevel(unit, 1.0);
}

void SpawnMarkerMonster(int cur)
{
    int unit, idx = UnitScrNameToNumber(cur), res = 0;
    int low = idx % 100, high = idx / 100;
    int lunit;
    
    if (idx >= 0)
    {
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(cur), GetObjectY(cur));
        LookWithAngle(unit, low);
        QueryLUnit(&lunit,0,high);
        if (IsObjectOn(lunit))
            SetOwner(lunit, unit);
        QueryLUnit(0,unit,high);
    }
    Delete(cur);
}

int AwakeLineMonsterSingle(int cur){
    int pic = cur;

    if (IsObjectOn(pic))
    {
        int fn=0;
        QueryMonsterFunctions(&fn,GetDirection(pic) % 100,0);
        Bind(fn,&pic);
        cur = GetOwner(cur);
        Delete(pic);
        return cur;
    }
    return 0;
}

void AwakeLineMonster(int cur)
{
    // int pic = cur;

    // if (IsObjectOn(pic))
    // {
    //     int fn=0;
    //     QueryMonsterFunctions(&fn,GetDirection(pic) % 100,0);
    //     Bind(fn,&pic);
    //     cur = GetOwner(cur);
    //     PushTimerQueue(1, cur, AwakeLineMonster);
    //     Delete(pic);
    // }
    int r=3;
    while (--r>=0){
        cur=AwakeLineMonsterSingle(cur);
        if (!cur)
            return;
    }
    PushTimerQueue(1,cur,AwakeLineMonster);
}

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

void GameMent2()
{
    UniPrintToAll("따라서 당신은 지하철 역 내 모든 사람을 죽이고 최종 목적지에 도달하면 승리합니다");
    UniPrintToAll("시작 위치에는 장비를 구입할 수 있는 상점과 특수용품 숍이 구축되어 있으니 많은 이용바랍니다");
}

void StartGameMent()
{
    UniPrintToAll("오늘은 지겨운 일상속에서 벗어나보세요");
    UniPrintToAll("지금 당신에게 주어진 임무는 도심의 한 지하철역을 테러하는 것이에요");
    UniPrintToAll("이곳의 모든 사람들은 이미 당신을 테러리스트로 간주하여 사살을 시도할 것입니다");
    SecondTimer(2, GameMent2);
}

void startInitscan(){
    int initScanHash, lastUnit=GetMasterUnit();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, SpawnMarkerMonster);
    StartUnitScanEx(Object("firstscan"), lastUnit, initScanHash, EndScan);
}

void gameSettings(){
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}

void initializeReadables(){
    RegistSignMessage(Object("read1"),"지하철 테러리스트- 제작. panic");
    RegistSignMessage(Object("read2"),"지독하게 오래동안 비가 내릴 적에...");
    RegistSignMessage(Object("read3"),"오른쪽으로 계속하여 가면은 평촌역이 있어");
    RegistSignMessage(Object("read4"),"이 맵은 지하철 테러리스트 이라고 불리우는 지도입니다. 제작 panic 입니다");
    RegistSignMessage(Object("read5"),"이 맵은 지하철 테러리스트 이라고 불리우는 지도입니다. 버그제보 및 건의사항 받습니다!");
    RegistSignMessage(Object("read6"),"지하철 테러리스트- 제작.panic. 열차 내 모든 승객을 죽이시오. 그들은 좀비다!");
    RegistSignMessage(Object("read7"),"평촌역 나가는 곳. 계단 이용하시오!");
}

void defaultItemSword(float x,float y){
    int sd=CreateObjectById(OBJ_SWORD,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower3,ITEM_PROPERTY_Matrial5,ITEM_PROPERTY_fire2,0);
    UnitNoCollide(sd);
}
void defaultItemPotion(float x,float y){
    UnitNoCollide( CreateObjectById(OBJ_RED_POTION, x,y) );
}

void placingDefaultItems(int sub){
    if (ToInt(GetObjectX(sub))){
        int dur=GetDirection(sub);
        if (dur){
            short fns[]={defaultItemSword,defaultItemPotion,};
            int xy[]={GetObjectX(sub),GetObjectY(sub),};
            PushTimerQueue(1,sub,placingDefaultItems);
            LookWithAngle(sub,--dur);
            Bind(fns[Random(0,1)],&xy);
            return;
        }
        Delete(sub);
    }
}

void initialDrawText(){
    DrawTextOnBottom(2, 0, LocationX(81),LocationY(81));
    DrawTextOnBottom(1, 0, LocationX(82),LocationY(82));
    DrawTextOnBottom(0, 0, LocationX(78),LocationY(78));
    DrawTextOnBottom(0, 0, LocationX(77),LocationY(77));
    DrawTextOnBottom(0, 0, LocationX(79),LocationY(79));
    DrawTextOnBottom(0, 0, LocationX(80),LocationY(80));
    DrawTextOnBottom(3, 0, LocationX(83),LocationY(83));
}

void OnInitializeMap(){
    MusicEvent();
    MakeCoopTeam();
    startInitscan();
    FrameTimer(1, MapDecorations);
    InitializePlayerSystem();
    FrameTimerWithArg(3, 7, InitAutoGates);
    InitializeMonsterFunctions();
    blockObserverMode();
    gameSettings();
    initializeReadables();
    InitializeShopSystem();
    int sub=CreateObjectById(OBJ_FEAR,LocationX(76),LocationY(76));

    LookWithAngle(sub,32);
    PushTimerQueue(1,sub,placingDefaultItems);
    initialDrawText();

    DispositionTransport(LocationX(84),LocationY(84),LocationX(85),LocationY(85));
}

void OnShutdownMap(){
    MusicEvent();    
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}

void DoInitializeResources(){
    InitializePopupMessages();
    InitializeResource();
}

void InitializeClientOnly(){
    DoInitializeResources();
    ClientProcLoop();
}

void ConfirmPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    if (pUnit)
        ShowQuestIntroOne(pUnit, 9999, "WizardChapterBegin11", "Journal:Wiz07EscapeUnderworld");
    if (pInfo==0x653a7c)
    {
        InitServerResource();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

