
#include "q048601_common.h"
#include "q048601_item.h"
#include "libs/shop.h"
#include "libs/npc.h"
#include "libs/format.h"
#include "libs/itemproperty.h"
#include "libs/buff.h"
#include "libs/printutil.h"
#include "libs/fixtellstory.h"
#include "libs/playerinfo.h"
#include "libs/waypoint.h"
#include "libs/bind.h"
#include "libs/potionpickup.h"

#define SPECIAL_WEAPON_MAX_COUNT 6
#define MAX_POTION_COUNT 6

#define GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM 1

#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_1 2
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_2 3
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_3 4
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_4 5
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_5 6
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_6 7

#define GUI_DIALOG_MESSAGE_SUMMON_CARROT 8
#define GUI_DIALOG_MESSAGE_POTION_SHOP 9
#define GUI_DIALOG_MESSAGE_HEALING_AMULET_SHOP 10

#define CARROT_HIT_DAMAGE 400

int GetSpecialWeaponPay(int cur) //virtual
{}

string GetSpecialWeaponName(int cur) //virtual
{}

string specialItemFormat(string input, int index)
{
    int args[]={StringUtilGetScriptStringPtr(GetSpecialWeaponName(index)),GetSpecialWeaponPay(index)};

    NoxSprintfString(StringUtilGetScriptStringPtr(input), "%s을 구입할래요?가격은 %d골드입니다.아니오를 누르면 다른것도 보여줄게요", args,sizeof(args));
    return input;
}

void InitFillPopupMessageArray(string *pPopupMessage)
{
    pPopupMessage[GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM]="당신의 인벤토리를 무적화 하시겠어요? 필요한 금액은 5,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_1]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ1",0);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_2]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ2",1);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_3]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ3",2);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_4]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ4",3);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_5]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ5",4);
    pPopupMessage[GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_6]=specialItemFormat("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ6",5);
    pPopupMessage[GUI_DIALOG_MESSAGE_SUMMON_CARROT]="붉은 용가리을 구입하실래요? 필요한 금액은 120,000 골드입니다. 유저 마다 오직 1개만 보유할 수 있습니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_HEALING_AMULET_SHOP]="힐링목걸이를 풀로 6개 채워드립니다. 금액은 1,500 골드입니다!";
    pPopupMessage[GUI_DIALOG_MESSAGE_POTION_SHOP]="체력회복약을 풀로 6개 채워드립니다. 금액은 1,000 골드입니다!";
}

void openShopForced()
{
    char openshopcode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 
    0x10, 0xEF, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3, 0x90
};int args[]={UnitToPtr(SELF),UnitToPtr(OTHER)};
int *pExec=0x5c336c;
		int *pOld=pExec[0];
		pExec[0]= openshopcode;
		Unknownb8(args);
		pExec[0]=pOld;
}

int dispositionShop(short thingId, float xpos, float ypos)
{
    int lh=DummyUnitCreateById(thingId, xpos, ypos);
    int ptr=UnitToPtr(lh);

    SetMemory(ptr+12,GetMemory(ptr+12)^8);
    int p=MemAlloc(0x6c0);
    NoxByteMemset(p, 0x6c0, 0);
    SetMemory(ptr+0x2b4, p);
    SetDialog(lh,"NORMAL",openShopForced,openShopForced);
    return lh;
}

void deferredChangeDialogContext(int *pParams){} //virtual

int SetInvulnerabilityItem(int unit)
{
    int count = 0, inv = GetLastItem(unit);

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void tradeInvincibleItem()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=5000)
    {
        ChangeGold(OTHER, -5000);
        int count = SetInvulnerabilityItem(OTHER);
        char buff[128];

        NoxSprintfString(buff, "%d개 아이템이 처리되었습니다", &count,1);
        UniPrint(OTHER,ReadStringAddressEx(buff));
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descInvincibleItem()
{
    int params[]={
        GetCaller(),
        GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM,
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "인벤토리 무적화");
}

int placingInvincibleItemShop(short location)
{
    int s=DummyUnitCreateById(OBJ_ARCHER, LocationX(location),LocationY(location));

    SetDialog(s,"YESNO",descInvincibleItem,tradeInvincibleItem);
    StoryPic(s,"WarriorPic");
}

void tradePotionShop()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=1000)
    {
        int count = MAX_POTION_COUNT - retrieveSpecifyInventoryCount(OTHER,OBJ_RED_POTION);
        
        if (!count)
        {
            UniPrint(OTHER, "물약을 최대로 가지고 계시네요...");
            return;
        }
        ChangeGold(OTHER, -1000);
        int pot;
        while (--count>=0)
        {
            pot=CreateObjectById(OBJ_RED_POTION,GetObjectX(OTHER),GetObjectY(OTHER));
            PotionPickupRegist(pot);
            itemPickupSafety(OTHER,pot);
        }
        UniPrint(OTHER,"물약 충전 성공입니다!");
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descPotionShop()
{
    int params[]={GetCaller(),GUI_DIALOG_MESSAGE_POTION_SHOP};
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "포션팔이소녀");
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int placingPotionShop(short location)
{
    int s=CreateSingleColorMaidenAt(255,0,0,LocationX(location),LocationY(location));

    UnitLinkBinScript(s,MaidenBinTable());
    ObjectOff(s);
    Damage(s,0,999,DAMAGE_TYPE_PLASMA);
    Frozen(s,TRUE);
    SetDialog(s,"YESNO",descPotionShop,tradePotionShop);
    StoryPic(s,"GlyndaPic");
    LookWithAngle(s,96);
    return s;
}

void tradeHealingPotionShop()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=1500)
    {
        int count = MAX_POTION_COUNT - retrieveSpecifyInventoryCount(OTHER,OBJ_AMULETOF_NATURE);
        
        if (count<=0)
        {
            UniPrint(OTHER, "힐링목걸이를 최대로 가지고 계시네요... (최대 6개 까지만 인정)");
            return;
        }
        ChangeGold(OTHER, -1500);
        int amulet;

        while (--count>=0)
        {
            amulet=CreateObjectById(OBJ_AMULETOF_NATURE,GetObjectX(OTHER),GetObjectY(OTHER));
            
            healingAmulet(amulet);
            UnitNoCollide(amulet);
            itemPickupSafety(OTHER,amulet);
        }
        UniPrint(OTHER,"힐링목걸이 충전 성공입니다!");
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descHealingPotionShop()
{
    int params[]={GetCaller(),GUI_DIALOG_MESSAGE_HEALING_AMULET_SHOP};
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "힐링팬던트팔이");
}

int placingHealingPotionShop(short location)
{
    int s=CreateSingleColorMaidenAt(55,155,131,LocationX(location),LocationY(location));

    UnitLinkBinScript(s,MaidenBinTable());
    ObjectOff(s);
    Damage(s,0,999,DAMAGE_TYPE_PLASMA);
    Frozen(s,TRUE);
    LookWithAngle(s,96);
    SetDialog(s,"YESNO",descHealingPotionShop,tradeHealingPotionShop);
    StoryPic(s,"GlyndaPic");
    return s;
}

void onCarrotStatusLoop(int mon)
{
    if (CurrentHealth(mon))
    {
        int owner=GetOwner(mon);

        if (MaxHealth(owner))
        {
            PushTimerQueue(7, mon, onCarrotStatusLoop);
            if (!UnitCheckEnchant(owner,GetLShift(ENCHANT_INVULNERABLE)))
            {
                if (!IsVisibleTo(mon, owner))
                {
                    Effect("COUNTERSPELL_EXPLOSION",GetObjectX(mon),GetObjectY(mon),0.0,0.0);
                    MoveObject(mon,GetObjectX(owner),GetObjectY(owner));
                    Effect("COUNTERSPELL_EXPLOSION",GetObjectX(mon),GetObjectY(mon),0.0,0.0);
                }
            }
            RestoreHealth(mon, 30);
            return;
        }
        EnchantOff(mon, "ENCHANT_INVULNERABLE");
        Damage(mon, 0, 9999, DAMAGE_TYPE_PLASMA);
        DeleteObjectTimer(mon, 90);
    }
    Delete(mon+1);
}

void deferredFollowing(int carrot)
{
    if (CurrentHealth(carrot))
    {
        int owner=GetOwner(carrot);

        if (CurrentHealth(owner))
        {
            CreatureFollow(carrot, owner);
            AggressionLevel(carrot, 1.0);
        }
    }
}

void onCarrotHurt()
{
    if (!UnitCheckEnchant(SELF,GetLShift(ENCHANT_INVULNERABLE)))
    {
        Enchant(SELF,"ENCHANT_INVULNERABLE",0.0);
        Enchant(SELF,"ENCHANT_SHIELD",0.0);
        RestoreHealth(SELF, 100);
    }
}

int CarrotBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[1] = 50; arr[17] = 3000; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 32769; arr[24] = 1065353216; arr[27] = 0; arr[28] = 1115815936; arr[29] = CARROT_HIT_DAMAGE; 
		arr[31] = 2; arr[32] = 3; arr[33] = 3; arr[58] = 5547856; arr[59] = 5543904;
	pArr = arr;
	return pArr;
}

void CarrotSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 3000;	hpTable[1] = 3000;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = CarrotBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int summonCarrot(int owner)
{
    int mon=CreateObjectById(OBJ_HECUBAH_WITH_ORB, GetObjectX(owner)+UnitAngleCos(owner,17.0),GetObjectY(owner)+UnitAngleSin(owner,17.0));

    PushTimerQueue(1, mon, onCarrotStatusLoop);
    SetOwner(owner, mon);
    CarrotSubProcess(mon);
    SetCallback(mon, 7, onCarrotHurt);
    GreenExplosion(GetObjectX(mon),GetObjectY(mon));
    UniChatMessage(mon, "붉은 용가리, 등장이요!!", 150);
    SetUnitScanRange(mon, 300.0);
    SetUnitVoice(mon, 38);
    PushTimerQueue(1, mon, deferredFollowing);
    return mon;
}

int BeforeCarrotBought(int user, int pIndex){} //virtual
void AfterCarrotBought(int user, int carrot, int pIndex){} //virtual

void tradeSummonCarrot()
{
    if (GetAnswer(SELF)^1)
        return;

    int pIndex=GetPlayerIndex(OTHER);

    if (GetGold(OTHER)>=120000)
    {
        if (BeforeCarrotBought(OTHER, pIndex))
        {
            ChangeGold(OTHER, -120000);
            AfterCarrotBought(OTHER, summonCarrot(OTHER), pIndex);
        }
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descSummonCarrot()
{
    int params[]={
        GetCaller(),
        GUI_DIALOG_MESSAGE_SUMMON_CARROT,
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "붉은용가리\n구입");
}

int placingCarrotShop(short location)
{
    int s=DummyUnitCreateById(OBJ_WIZARD_GREEN, LocationX(location),LocationY(location));

    SetDialog(s,"YESNO",descSummonCarrot,tradeSummonCarrot);
    StoryPic(s,"DryadPic");
}

int placingArmorShop(short location, int property1, int property2)
{
    int shop= dispositionShop(OBJ_HORRENDOUS, LocationX(location), LocationY(location));

    UnitNoCollide(shop);
    ShopUtilSetTradePrice(shop, 1.9, 0.0);
    ShopUtilAppendItemWithProperties(shop, OBJ_ORNATE_HELM, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_BREASTPLATE, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_PLATE_ARMS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_PLATE_BOOTS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_PLATE_LEGGINGS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_MEDIEVAL_CLOAK, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_MEDIEVAL_PANTS, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_MEDIEVAL_SHIRT, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop, OBJ_STEEL_SHIELD, 6, ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,property1,property2);
    LookWithAngle(shop, 32);
    return shop;
}

int placingWeaponShop(short location, int property1, int property2)
{
    int shop=dispositionShop(OBJ_SWORDSMAN,LocationX(location),LocationY(location));
    UnitNoCollide(shop);
    ShopUtilSetTradePrice(shop,1.6,0.0);
    ShopUtilAppendItemWithProperties(shop,OBJ_WAR_HAMMER,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_GREAT_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_ROUND_CHAKRAM,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_BATTLE_AXE,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property1,property2);
    ShopUtilAppendItemWithProperties(shop,OBJ_WAR_HAMMER,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_GREAT_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_ROUND_CHAKRAM,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_BATTLE_AXE,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    ShopUtilAppendItemWithProperties(shop,OBJ_SWORD,6,ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,property2,property1);
    LookWithAngle(shop,32);
    return shop;
}

int GetSpecialWeaponMessage(int cur) //virtual
{}

void specialWeaponDesc()
{
    int pIndex=GetPlayerIndex(OTHER);
    int *p;

    if (!HashGet(GenericHash(), GetTrigger(), &p, FALSE))
        return;

    int cur= p[pIndex];
    int params[]={
        GetCaller(),
        GetSpecialWeaponMessage(cur),
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
    TellStoryUnitName("AA", "MainBG.wnd:Loading", GetSpecialWeaponName(cur));
}

int CreateSpecialWeaponProto(int functionId, float x,float y)
{
    return Bind(functionId, &functionId + 4) ;
}

int GetSpecialWeaponFunction(int cur) //virtual
{}

void specialWeaponTrade()
{
    int pIndex=GetPlayerIndex(OTHER), dlgRes = GetAnswer(SELF);
    int *p;

    if (!HashGet(GenericHash(), GetTrigger(), &p, FALSE))
        return;
    int cur=p[pIndex];
    if (dlgRes==2)
    {
        UniPrint(OTHER, "'아니오'을 누르셨어요. 다음 항목을 보여드리겠어요");
        p[pIndex]=(cur+1)%SPECIAL_WEAPON_MAX_COUNT;
        specialWeaponDesc();
    }
    else if (dlgRes==1)
    {
        int pay=GetSpecialWeaponPay(cur);
        if (GetGold(OTHER)>=pay)
        {
            ChangeGold(OTHER, -pay);
            CreateSpecialWeaponProto(GetSpecialWeaponFunction(cur), GetObjectX(OTHER), GetObjectY(OTHER));
            UniPrint(OTHER, "거래완료!- 당신 아래에 있어요");
        }
        else
            UniPrint(OTHER, "거래실패!- 잔액이 부족합니다");
    }
}

int PlaceSpecialWeaponShop(int location)
{
    int s=DummyUnitCreateById(OBJ_WIZARD_WHITE, LocationX(location), LocationY(location));

    SetDialog(s, "YESNO", specialWeaponDesc, specialWeaponTrade);
    StoryPic(s,"HorvathPic");
    int *p;

    AllocSmartMemEx(32*4, &p);
    NoxDwordMemset(p, 32, 0);
    HashPushback(GenericHash(), s, p);
    return s;
}

