
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/memutil.h"
#include "libs/printutil.h"
#include "libs/format.h"

#define PRINT_ALL -1

void PrintMessageFormatOne(int user, string fmt, int arg)
{
    char message[192];

    NoxSprintfString(message, fmt, &arg, 1);
    if (user==-1)
    {
        UniPrintToAll(ReadStringAddressEx(message));
        return;
    }
    UniPrint(user, ReadStringAddressEx(message));
}

int GetNearlyPlayer(int unit)
{
    int *p=0x62f9e0;
    int count = 32, *t, user;
    float dist=3999.0, rd;
    int pick=0;

    while (--count>=0)
    {
        if (p[0])
        {
            t=p[0];
            user=t[11];
            if (CurrentHealth(user))
            {
                if (IsVisibleTo(unit,user))
                {
                    rd=DistanceUnitToUnit(unit,user);
                    if (rd<dist)
                    {
                        dist=rd;
                        pick=user;
                    }
                }
            }
        }
        p+=0x12dc;
    }
    return pick;
}

int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAt(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}
