
#include "fov_utils.h"
#include "libs/buff.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/printutil.h"

void onWarSkillSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 125, DAMAGE_TYPE_PLASMA);
        }
    }
}

void WarPlusSkill(int caster)
{
    if (CurrentHealth(caster))
    {
        if (UnitCheckEnchant(caster, GetLShift(ENCHANT_AFRAID) ))
        {
            PlaySoundAround(caster, SOUND_NoCanDo);
            UniPrint(caster, "쿨다운 중 입니다... 잠시만 기다리세요");
            return;
        }
        
        Enchant(caster, EnchantList(ENCHANT_AFRAID), 8.0);
        float x=GetObjectX(caster) + UnitAngleCos(caster, 25.0),y= GetObjectY(caster) + UnitAngleSin(caster, 25.0);
        DrawGeometryRing(OBJ_CHARM_ORB, x,y);        
        SplashDamageAtEx(caster, x,y, 120.0, onWarSkillSplash);
        PlaySoundAround(caster, SOUND_CrushLight);
    }
}

