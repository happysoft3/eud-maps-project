
#include "libs/define.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/unitstruct.h"
#include "libs/mathlab.h"
#include "libs/playerinfo.h"
#include "libs/observer.h"
#include "libs/username.h"

int PlrCnt, Start, PlrTb[10], PlrPic, Murder, Live;
int player[20], PlrCre[10], PlrSrc[10];

string GameGuideMessage(int num)
{
    string msg[] = {
    "어친 술래잡기 v0.1121                                                           제작. 237",
    "게임 팁- 기본적인 조작방법: 이동하려는 지역을 클릭하면 캐릭터가 목표지점으로 이동합니다",
    "2명 이상의 플레이어가 모이면 그 중 랜덤한 한명이 술래가 됩니다, 술래(샤먼)는 도망자(어친)를 모두 잡으면 승리합니다",
    "도망자인 어친은 술래 샤먼을 피해 180 초 동안 살아남으면 승리합니다",
    "술래가 도망자를 잡을 때 마다 1 점이 오르며, 도망자는 제한시간 동안 생존하면 3점을 얻습니다",
    "게임이 진행되지 않는 버그가 발생하게 되면 관객모드 상태로 전환했다가 다시 시도해 보십시오",
    "참고사항-- 게임 내 스코어는 아직 아무 쓸모가 없습니다, 이용에 착오 없으시길 바랍니다",
    "녹스 맵을 두는 공간 blog.daum.net/ky10613 을 방문하세요"};
    return msg[num];
}

void ShowGuideMessage(int num)
{
    if (num < 8)
    {
        UniPrintToAll(GameGuideMessage(num));
        SecondTimerWithArg(6, num + 1, ShowGuideMessage);
    }
    else
        UniPrintToAll("가장 최근에 릴리즈 된 맵입니다...");
}

int GetMasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 1);
        Frozen(unit, 1);
        MoveObject(unit, 5500.0, 100.0);
    }
    return unit;
}

void WritePlayerTable(int plr)
{
    PlrTb[PlrPic++] = plr;
}

void MixPlayerTable()
{
    int k, rnd;

    for (k = 0 ; k < PlrPic ; k += 1)
    {
        rnd = Random(0, PlrPic - 1);
        if (k ^ rnd)
        {
            PlrTb[k] = PlrTb[k] ^ PlrTb[rnd];
            PlrTb[rnd] = PlrTb[rnd] ^ PlrTb[k];
            PlrTb[k] = PlrTb[k] ^ PlrTb[rnd];
        }
    }
}

int SelectZombiePlayer()
{
    int k;
    for (k = 9 ; k >= 0 ; k -= 1)
    {
        if (CurrentHealth(player[k]))
            WritePlayerTable(k);
    }
    MixPlayerTable();
    MixPlayerTable();
    MixPlayerTable();
    return PlrTb[0];
}

void CheckPlayerCount(int plr)
{
    if (!Start && PlrCnt > 1)
    {
        if (IsObjectOn(Murder))
            Delete(Murder);
        Murder = 0;
        Start = CreateObject("InvisibleLightBlueHigh", 1);
        SecondTimerWithArg(3, Start, GameStandByMent);
        SecondTimerWithArg(10, Start, RespectZombiePlayer);
    }
}

void CheckPlayerOut(int plr)
{
    PlrCnt --;
    while (1)
    {
        if (Murder) //술래가 나간경우
        {
            if (player[plr] == GetOwner(Murder))
            {
                UniPrintToAll("게임도중 술래가 나갔습니다, 게임을 리셋합니다");
                GameOff();
                break;
            }
        }
        if (PlrCnt < 2) //플레이어 1명 이하인 경우
        {
            UniPrintToAll("플레이어 수가 1 명 이하 입니다, 게임이 리셋됩니다");
            GameOff();
        }
        break;
    }
}

void GameOff()
{
    int k;

    if (IsObjectOn(Start))
        Delete(Start);
    Start = 0;
    for (k = 9 ; k >= 0 ; k -=1)
    {
        if (MaxHealth(player[k]))
        {
            MoveObject(player[k], GetWaypointX(41), GetWaypointY(41));
            FreePlayerCreature(k);
        }
    }
}

void RespectZombiePlayer(int ptr)
{
    if (IsObjectOn(ptr))
    {
        Raise(ptr, ToFloat(60 * 3));
        int sel = SelectZombiePlayer();
        UniPrintToAll("누군가 술래가 되었습니다!! 술래를 피해 3분만 버티세요!!");
        ChangeZombieUnit(sel);
        Live = PlrCnt - 1;
        Murder = CreateObject("InvisibleLightBlueHigh", 43);
        SetOwner(player[sel], Murder);
        LookWithAngle(Murder, sel);
        SecondTimerWithArg(1, ptr, Countdown);
    }
}

void Countdown(int ptr)
{

    if (IsObjectOn(ptr))
    {
        int count = ToInt(GetObjectZ(ptr));
        if (count)
        {
            if (count == 1)
                LivePlayer(ptr);
            UniChatMessage(GetMasterUnit(), "남은 시간: " + IntToString(count) + "초\n현재 생존자: " + IntToString(Live) + "명", 50);
            Raise(ptr, ToFloat(count - 1));
        }
        SecondTimerWithArg(1, ptr, Countdown);
    }
}

void LivePlayer(int ptr)
{
    if (IsObjectOn(ptr))
    {
        int k;

        for (k = 9 ; k >= 0 ; k --)
        {
            if (CurrentHealth(PlrCre[k]) && k ^ GetDirection(Murder))
            {
                ObjectOff(PlrCre[k] + 1);
                UniChatMessage(player[k], "생존자", 150);
                ChangeScore(player[k], 1);
                PlrSrc[k] += 3;
            }
        }
        if (CurrentHealth(GetOwner(Murder)))
        {
            Enchant(PlrCre[GetDirection(Murder)], "ENCHANT_FREEZE", 0.0);
        }
        UniPrintToAll("이번 게임의 승자는 생존하신 플레이어 유저분들 입니다, 생존자 분들 에게는 스코어 3점이 지급됩니다");
        int reset = CreateObject("InvisibleLightBlueHigh", 42);
        SetOwner(Start, reset);
        FrameTimerWithArg(60, reset, GameResetReady);
        FrameTimerWithArg(180, reset, ShowAllPlayerScore);
        FrameTimerWithArg(280, reset, GameReset);
    }
}

void GameStandByMent(int ptr)
{
    if (IsObjectOn(ptr))
    {
        UniPrintToAll("잠시 후 술래가 결정됩니다, 입장한 플레이어들 중 누가 술래가 될 지 모르니 서로 멀어지세요");
    }
}

void ChangeZombieUnit(int plr)
{
    int temp;
    UniPrint(player[plr], "술래가 되셨습니다, 생존자들을 모두 잡으시면 승리합니다");
    if (CurrentHealth(PlrCre[plr]))
    {
        temp = PlrCre[plr];
        MoveWaypoint(42, GetObjectX(temp), GetObjectY(temp));
        PlrCre[plr] = SpawnHorrendous(plr, 42);
        Enchant(player[plr], "ENCHANT_ETHEREAL", 0.5);
        Delete(temp);
        Delete(temp + 1);
    }
}

void PlayerRegist()
{
    int plr, k;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            plr = CheckPlayer();
            for (k = 9 ; k >= 0 && plr < 0 ; k -= 1)
            {
                if (!MaxHealth(player[k]))
                {
                    player[k] = GetCaller();
                    player[k + 10] = 1;
                    UniPrintToAll(PlayerIngameNick(player[k]) + " 님이 어친이 되었습니다");
                    PlrSrc[k] = 0;
                    plr = k;
                    PlrCnt ++;
                    break;
                }
            }
            if (plr + 1)
            {
                PlayerEntry(plr);
                break;
            }
        }
        MapJoinError();
        break;
    }
}

void PlayerEntry(int plr)
{
    if (CheckPlayerDeathFlag(plr))
        SetPlayerDeathFlag(plr);
    MoveObject(player[plr], GetWaypointX(28), GetWaypointY(28));

    EnchantOff(player[plr], "ENCHANT_VILLAIN");
    Enchant(player[plr], "ENCHANT_ETHEREAL", 0.5);
    CheckPlayerCount(plr);
    if (!CurrentHealth(PlrCre[plr]))
    {
        PlrCre[plr] = SpawnUrchin(plr, Random(29, 40));
    }
}

void MapJoinError()
{
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    MoveObject(OTHER, LocationX(27), LocationY(27));
    UniPrint(OTHER, "맵 파일 로딩 에러: 맵이 수용할 수 있는 인원을 초과했거나 현재 버전의 녹스가 이 맵을 지원하지 않습니다");
    UniPrint(OTHER, "blog.daum.net/ky10613/178 을 방문하여 문제를 해결하십시오");
}

int SpawnUrchin(int plr, int wp)
{
    int unit = CreateObject("Urchin", wp);

    SetUnitMaxHealth(unit, 75);
    LookWithAngle(CreateObject("InvisibleLightBlueLow", wp), plr);
    Raise(unit + 1, 4.5);
    SetOwner(player[plr], unit);
    GiveCreatureToPlayer(player[plr], unit);
    PlayerLook(player[plr], unit);
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    SetCallback(unit, 5, CreatureDeath);

    return unit;
}

int SpawnPlayerCamera(int plr, int wp)
{
    int unit = CreateObject("Maiden", wp);

    LookWithAngle(CreateObject("InvisibleLightBlueLow", wp), plr);
    UnitNoCollide(unit);
    SetOwner(player[plr], unit);
    GiveCreatureToPlayer(player[plr], unit);
    PlayerLook(player[plr], unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);

    return unit;
}

int SpawnHorrendous(int plr, int wp)
{
    int unit = CreateObject("UrchinShaman", wp);

    SetUnitMaxHealth(unit, 150);
    LookWithAngle(CreateObject("InvisibleLightBlueLow", wp), plr);
    Raise(unit + 1, 5.0);
    SetOwner(player[plr], unit);
    GiveCreatureToPlayer(player[plr], unit);
    PlayerLook(player[plr], unit);
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    SetCallback(unit, 5, CreatureDeath);
    SetCallback(unit, 9, ICanTrackYou);

    return unit;
}

void CreatureDeath()
{
    int plr = GetDirection(GetTrigger() + 1);

    FreePlayerCreature(GetDirection(plr));
    TeleportLocation(42, GetObjectX(SELF), GetObjectY(SELF));
    Live --;
    VictoryEvent();
    if (CurrentHealth(player[plr]) && CurrentHealth(GetOwner(Murder)))
    {
        PlrCre[plr] = SpawnPlayerCamera(plr, 42);
        UniPrint(player[plr], "관전모드로 전환되었습니다");
        Enchant(player[plr], "ENCHANT_VILLAIN", 0.0);
    }
}

void ICanTrackYou()
{
    if (CurrentHealth(OTHER) && !HasEnchant(SELF, "ENCHANT_FREEZE"))
    {
        if (IsPlayerUnit(GetOwner(OTHER)))
        {
            int plr = GetDirection(GetTrigger() + 1);

            TeleportLocation(42, GetObjectX(SELF), GetObjectY(SELF));
            AudioEvent("NecromancerTaunt", 42);
            GreenSparkFx(42);
            UniChatMessage(SELF, "잡았다 요놈!", 120);
            PlrSrc[plr] ++;
            UniPrint(player[plr], "게임 스코어 +1");
            Damage(OTHER, SELF, 999, 14);
            if (!HasEnchant(SELF, "ENCHANT_VAMPIRISM"))
            {
                Enchant(SELF, "ENCHANT_VAMPIRISM", 1.0);
                FrameTimerWithArg(1, GetTrigger(), DisplayCatchLatter);
            }
        }
    }
}

void VictoryEvent()
{
    if (!Live && IsObjectOn(Start))
    {
        UniPrintToAll("술래 승! 생존자를 전멸 시켰습니다");
        if (CurrentHealth(GetOwner(Murder)))
        {
            int m_plr = GetDirection(Murder);
            TeleportLocation(44, GetObjectX(PlrCre[m_plr]), GetObjectY(PlrCre[m_plr]));
            Effect("WHITE_FLASH", LocationX(44), LocationY(44), 0.0, 0.0);
            AudioEvent("StaffOblivionAchieve1", 44);
            UniPrintToAll("이번 게임의 승리자는 어친샤먼 조종하신 " + PlayerIngameNick(player[m_plr]) + " 님 입니다!");
            UniPrint(player[m_plr], "당신이 승리했습니다!");
            int reset = CreateObject("InvisibleLightBlueHigh", 44);
            SetOwner(Start, reset);
            FrameTimer(1, StrVictory);
            FrameTimerWithArg(90, 403, RemoveStrTexts);
            FrameTimerWithArg(60, reset, GameResetReady);
            FrameTimerWithArg(200, reset, ShowAllPlayerScore);
            FrameTimerWithArg(300, reset, GameReset);
        }
    }
}

void GameResetReady(int ptr)
{
    if (IsObjectOn(ptr))
    {
        if (IsObjectOn(GetOwner(ptr)))
        {
            UniPrintToAll("잠시 후 게임이 리셋됩니다");
        }
        else
            Delete(ptr);
    }
}

void GameReset(int ptr)
{
    if (IsObjectOn(ptr) && IsObjectOn(GetOwner(ptr)))
    {
        GameOff();
    }
    Delete(ptr);
}

int CheckPlayer()
{
    int k;
    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int CheckPlayerDeathFlag(int plr)
{
    return player[plr + 10] & 0x02;
}

void SetPlayerDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x02;
}

void PlayerOnDeath(int plrIndex)
{
    UniPrintToAll(PlayerIngameNick(player[plrIndex]) + " 님이 격추되었습니다");
}

void PlayerOnShutdown(int plrIndex)
{
    CheckPlayerOut(plrIndex);
    FreePlayerCreature(plrIndex);
    UniPrintToAll(PlayerIngameNick(player[plrIndex]) + " 님이 게임을 떠나셨습니다");
}

void PreservePlayersLoop()
{
    int k;

    for (k = 9 ; k >= 0 ; k -= 1)
    {
        while (1)
        {
            if (MaxHealth(player[k]))
            {
                if (GetUnitFlags(player[k]) & 0x40)
                    1;
                else if (CurrentHealth(player[k]))
                {
                    if (CurrentHealth(PlrCre[k]))
                    {
                        if (CheckWatchFocus(player[k]))
                            TakePointer(k);
                    }
                    else if (HasEnchant(player[k], "ENCHANT_VILLAIN"))
                    {
                        if (MaxHealth(PlrCre[k]))
                            ObserveToKiller(k, GetDirection(Murder));
                    }
                    break;
                }
                else
                {
                    if (!CheckPlayerDeathFlag(k))
                    {
                        SetPlayerDeathFlag(k);
                        PlayerOnDeath(k);
                    }
                    break;
                }
            }
            if (player[k + 10])
            {
                PlayerOnShutdown(k);
                player[k] = 0;
                player[k + 10] = 0;
            }
            break;
        }
    }
    FrameTimer(1, PreservePlayersLoop);
}

void ObserveToKiller(int plr, int k_plr)
{
    if (ToInt(DistanceUnitToUnit(PlrCre[plr], PlrCre[k_plr])))
        MoveObject(PlrCre[plr], GetObjectX(PlrCre[k_plr]), GetObjectY(PlrCre[k_plr]));
    if (CheckWatchFocus(player[plr]))
        PlayerLook(player[plr], PlrCre[plr]);
}

void TakePointer(int plr)
{
    int glow = CreateObject("Moonglow", 42);
    LookWithAngle(glow, plr);
    SetOwner(player[plr], glow);
    PlayerLook(player[plr], PlrCre[plr]);
    FrameTimerWithArg(1, glow, GoTarget);
}

void GoTarget(int glow)
{
	int plr = GetDirection(glow), mark[10];

	if (CurrentHealth(player[plr]) && !HasEnchant(player[plr], "ENCHANT_ETHEREAL"))
	{
		if (CurrentHealth(PlrCre[plr]))
        {
            MoveWaypoint(42, GetObjectX(glow), GetObjectY(glow));
            if (IsObjectOn(mark[plr]))
                Delete(mark[plr]);
            mark[plr] = CreateObject("TeleportGlyph1", 42);
            LookWithAngle(mark[plr], plr);
            SetOwner(player[plr], mark[plr]);
			LookAtObject(PlrCre[plr], mark[plr]);
            FrameTimerWithArg(1, mark[plr], MaidenMoving);
        }
	}
	Delete(glow);
}

void MaidenMoving(int ptr)
{

    if (IsObjectOn(ptr))
    {
        int plr = GetDirection(ptr);

        if (CurrentHealth(PlrCre[plr]) && IsObjectOn(PlrCre[plr] + 1))
        {
            if (DistanceUnitToUnit(PlrCre[plr], ptr) > 8.0)
            {
                float sp = GetObjectZ(PlrCre[plr] + 1);
                LookAtObject(PlrCre[plr], ptr);
                Walk(PlrCre[plr], GetObjectX(PlrCre[plr]), GetObjectY(PlrCre[plr]));
                MoveObject(PlrCre[plr], GetObjectX(PlrCre[plr]) + UnitRatioX(ptr, PlrCre[plr], sp), GetObjectY(PlrCre[plr]) + UnitRatioY(ptr, PlrCre[plr], sp));
                FrameTimerWithArg(1, ptr, MaidenMoving);
            }
            else
            {
                CreatureIdle(PlrCre[plr]);
                LookAtObject(PlrCre[plr], ptr);
                Delete(ptr);
            }
        }
    }
}

void FreePlayerCreature(int plr)
{
    if (CurrentHealth(PlrCre[plr]))
    {
        Delete(PlrCre[plr]);
    }
    Delete(PlrCre[plr] + 1);
}

void MapDecorations()
{
    int ptr = CreateObject("DunMirScaleTorch2", 1);
    Frozen(ptr, 1);
    Frozen(CreateObject("DunMirScaleTorch2", 2), 1);
    Frozen(CreateObject("DunMirScaleTorch2", 3), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 4), 1);
    Frozen(CreateObject("DunMirScaleTorch2", 5), 1);
    Frozen(CreateObject("DunMirScaleTorch2", 6), 1);
    Frozen(CreateObject("TraderArmorRack1", 7), 1);
    Frozen(CreateObject("TraderArmorRack2", 8), 1);
    Frozen(CreateObject("TraderArmorRack1", 9), 1);
    Frozen(CreateObject("TraderArmorRack2", 10), 1);
    Frozen(CreateObject("TraderArmorRack2", 11), 1);
    Frozen(CreateObject("TraderArmorRack1", 12), 1);
    Frozen(CreateObject("DarkWoodenChairFallen1", 14), 1);
    Frozen(CreateObject("LargeBarrel2", 15), 1);
    Frozen(CreateObject("LargeBarrel2", 16), 1);
    Frozen(CreateObject("Candleabra3", 17), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 26), 1);
    FrameTimer(1, InitPlaceBookApart);
}

void InitPlaceBookApart()
{
    BookApartToSouth("MovableBookcase2", 13, 8);
    BookApartToSouth("MovableBookcase4", 18, 8);
    BookApartToWest("MovableBookcase1", 19, 12);
    BookApartToWest("MovableBookcase1", 21, 12);
    BookApartToWest("MovableBookcase1", 23, 12);
    BookApartToWest("MovableBookcase3", 20, 12);
    BookApartToWest("MovableBookcase3", 22, 12);
    BookApartToWest("MovableBookcase3", 24, 12);
    BookApartToWest("Table1", 25, 12);
}

void BookApartToSouth(string name, int wp, int max)
{
    int k;

    for (k = 0 ; k < max ; k += 1)
    {
        Frozen(CreateObject(name, wp), TRUE);
        TeleportLocationVector(wp, 23.0, 23.0);
    }
}

void BookApartToWest(string name, int wp, int max)
{
    int k;

    for (k = 0 ; k < max ; k +=1)
    {
        Frozen(CreateObject(name, wp), TRUE);
        TeleportLocationVector(wp, -23.0, 23.0);
    }
}

void MapInitialize()
{
    MusicEvent();
    GetMasterUnit();
    
    FrameTimer(1, MapDecorations);
    FrameTimer(1, PreservePlayersLoop);
    SecondTimerWithArg(8, 0, ShowGuideMessage);
}

void GreenSparkFx(int wp)
{
    int ptr = CreateObject("MonsterGenerator", wp);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void StrVictory()
{
	int arr[13];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while (i < 13)
	{
		drawStrVictory(arr[i++], name);
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(44);
		pos_y = LocationY(44);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObjectAtEx(name, LocationX(44), LocationY(44), 0);
		if (count % 38 == 37)
			TeleportLocationVector(44, - 74.000000,  2.000000);
		else
			TeleportLocationVector(44, 2.000000, 0.0);
		count += 1;
	}
	if (count >= 403)
	{
		count = 0;
		TeleportLocation(44, pos_x, pos_y);
	}
}

void RemoveStrTexts(int count)
{
    int k, ptr = CreateObject("RedPotion", 1) - 1;

    Delete(ptr + 1);
    for (k = 0 ; k < count ; k += 1)
    {
        Delete(ptr + k);
    }
}

void StrCatchYou()
{
	int arr[24], i = 0;
	string name = "CharmOrb";

	arr[0] = 2116272894; arr[1] = 8511492; arr[2] = 151553032; arr[3] = 545521921; arr[4] = 1122075136; arr[5] = 1082146884; arr[6] = 144740336; arr[7] = 537925409; arr[8] = 1142964256; arr[9] = 67389316; 
	arr[10] = 135267360; arr[11] = 17826113; arr[12] = 2145447950; arr[13] = 1073741828; arr[14] = 4325504; arr[15] = 135274752; arr[16] = 276832289; arr[17] = 133695480; arr[18] = 537397316; arr[19] = 34669064; 
	arr[20] = 50211458; arr[21] = 71340028; arr[22] = 1120511; arr[23] = 534773761; 
	while(i < 24)
	{
		drawStrCatchYou(arr[i++], name);
	}
}

void drawStrCatchYou(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = LocationX(45);
		pos_y = LocationY(45);
	}
	for (i = 1 ; i > 0 && count < 744 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObjectAtEx(name, LocationX(45), LocationY(45), NULLPTR);
		if (count % 68 == 67)
			TeleportLocationVector(45, - 134.000000, 2.000000);
		else
			TeleportLocationVector(45,  2.000000, 0.0);
		count += 1;
	}
	if (count >= 744)
	{
		count = 0;
		TeleportLocation(45, pos_x, pos_y);
	}
}

void DisplayCatchLatter(int unit)
{
    if (CurrentHealth(unit))
    {
        TeleportLocation(45, GetObjectX(unit) - 60.0, GetObjectY(unit) - 60.0);
        int ptr = CreateObject("RedPotion", 45) + 1;
        Delete(ptr - 1);
        StrCatchYou();
        FrameTimerWithArg(30, ptr, RemoveCatchLatter);
    }
}

void RemoveCatchLatter(int ptr)
{
    int k;
    for (k = 0 ; k < 744 ; k += 1)
        DeleteObjectTimer(ptr + k, 1);
}

void ShowAllPlayerScore(int ptr)
{
    int k;
    
    if (IsObjectOn(ptr))
    {
        if (IsObjectOn(GetOwner(ptr)))
        {
            UniPrintToAll("||||          모든 플레이어 누적 스코어 현황         ||||");
            for (k = 9 ; k >= 0 ; k -= 1)
            {
                if (MaxHealth(player[k]))
                    UniPrintToAll(PlayerIngameNick(player[k]) + ": " + IntToString(PlrSrc[k]) + "점");
            }
        }
        else
            Delete(ptr);
    }
}
