
#include "demono_player.h"
#include "demono_resource.h"
#include "libs\mathlab.h"
#include "libs\printutil.h"
#include "libs\buff.h"
#include "libs\itemproperty.h"
#include "libs\weaponcapacity.h"
#include "libs\waypoint.h"
#include "libs\wallutil.h"
#include "libs\fxeffect.h"
#include "libs\coopteam.h"
#include "libs\playerupdate.h"
#include "libs\spellutil.h"
#include "libs/potionex.h"

int *m_pImgVector;
int LastUnitPtr;
int D_RAND[3];
int FBTRAP = 0;

void GuideMessage()
{
    UniPrintToAll("나는 당신이 위험한 이곳으로 부터 안전하게 탈출하길 바랍니다");
    UniPrintToAll("이곳의 보스 호렌더스를 잡으면 여기서 나갈 수 있습니다");
    UniPrintToAll("버저커 차지는 사용할 수 없지만 대신에 조심스럽게 걷기에 또 다른 능력을 심어두었습니다");
    PlaySoundAround(OTHER, SOUND_JournalEntryAdd);
}

void FastHealingBeaconInit()
{
    PlaceFastHealingBeacon(433);
    PlaceFastHealingBeacon(424);
    PlaceFastHealingBeacon(425);
    PlaceFastHealingBeacon(426);
    PlaceFastHealingBeacon(427);
    PlaceFastHealingBeacon(428);
}

void Decorations()
{
    RegistSignMessage(Object("DungeonGuide3"), "보스전이 가까웠다. 제군들 모두 전투준비!");
    RegistSignMessage(Object("DungeonGuide2"), "동상을 클릭하여 이동시킬 수 있습니다. 동상을 움직여서 방향이 맞는 곳에 두세요");
    RegistSignMessage(Object("DungeonGuide1"), "되도록 적을 처리한 후 열쇠를 먹도록 하자 (열쇠가 증발되니까..)");
    RegistSignMessage(Object("PassGuide"), "각 숫자패드를 밟아서 4자리 비밀번호를 입력해주세요. 틀려도 아무일 없습니다");
    RegistSignMessage(Object("StartGuide3"), "감마선 구역이므로 조심하도록 한다");
    RegistSignMessage(Object("StartGuide2"), "전방에 천장 붕괴에 주의하라...");
    RegistSignMessage(Object("StartGuide1"), "옆에 나열된 앵크들이 남은 목숨이다. 죽을 때 마다 1개씩 소모되며 하나도 없을 시 부활불가");
    RegistSignMessage(Object("Room3Read"), "강당: 악령에 대한 주술적 행위가 이뤄지는 장소이기도 함");
    RegistSignMessage(Object("Room1Read"), "별관으로 가는 게이트");
    RegistSignMessage(Object("Room2Read"), "중앙 메인 홀: 이곳의 센터구역");
    RegistSignMessage(Object("DangerSign1"), "위험구역! 끊임없이 스파크를 발산하는 바위와 닿게되면 몸 전체에 고압전류가 흐를 것임!");
    RegistSignMessage(Object("DangerSign2"), "던전에 오신것을 환영합니다. 물론 오고 싶어 온것은 아니겠지만...");
    ContainerKeyInit("BraceletofAccuracy");
    ContainersInit();
    PlaceFistTrapInit();

    FrameTimer(10, FastHealingBeaconInit);
}

void RemoveNorthSecretWalls()
{
    if (MaxHealth(SELF))
    {
        if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
        {
            WallOpen(WallUtilGetWallAtObjectPosition(SELF));
            Delete(SELF);
            UniPrint(OTHER, "비밀 통로가 생겨났습니다");
        }
    }
}

void EnableObject(int obj)
{
    ObjectOn(obj);
}

void DisableSentryTrap()
{
    FrameTimerWithArg(60, GetTrigger(), EnableObject);
    ObjectOff(SELF);
    ObjectGroupToggle(0);
}

void PlayerCheckPointInit(int reviveLocation)
{
    SetCallback(DeadUnitCreate("WeirdlingBeast", 264), 9, RemoveNorthSecretWalls);
    PlayerCheckPointCreate(263, reviveLocation);
    PlayerCheckPointCreate(265, reviveLocation);
    PlayerCheckPointCreate(276, reviveLocation);
    PlayerCheckPointCreate(412, reviveLocation);
    PlayerCheckPointCreate(413, reviveLocation);
}

void MapInitialize()
{
    LastUnitPtr = CreateObject("RedPotion", 266);
    
    InitInvPropertiesSet();
    InitiPartition();
    FrameTimer(1, MakeCoopTeam);
    FrameTimerWithArg(30, 67, PlayerCheckPointInit);
    FrameTimer(30, Decorations);
    FrameTimerWithArg(1, Object("FirstScan"), InitNPCScan);
    FrameTimerWithArg(30, 1, DisableUnitGroup);
    InitializePlayerSystem();
    blockObserverMode();
}

void InitiPartition()
{
    PutInventorys(66);
    setLife(-2);

    D_RAND[0] = Random(1000, 5000);
    D_RAND[1] = Random(1000, 4999);
    D_RAND[2] = Random(0, 15);

    FrameTimerWithArg(3, 35, callTurboTrigger);
    FrameTimer(1, BufferDeleyRun);
    FrameTimerWithArg(30, 0x100e00, PlaceGlyphGroup);
    FrameTimer(50, CallNumberPack);
    FrameTimerWithArg(130, 113, PutCurePotions);
    FrameTimerWithArg(130, -114, PutCurePotions);
    FrameTimerWithArg(130, -115, PutCurePotions);
    FrameTimerWithArg(131, 116, PutCurePotions);
    FrameTimerWithArg(131, 117, PutCurePotions);

    getMaster();
}

void BufferDeleyRun()
{
    PutCurePotions(-148);
    PutCurePotions(-149);
    PutCurePotions(150);
    FrameTimerWithArg(1, 161, PutYellowPotion);
}

void PutCurePotions(int wp)
{
    int potion;
    float neg = 1.0;
    
    if (wp < 0)
    {
        wp = -wp;
        neg = -neg;
    }
    potion = CreateObject("RedPotion", wp);

    MoveWaypoint(wp, GetWaypointX(wp) + (18.0 * neg), GetWaypointY(wp) + 18.0);
    CreateObject("RedPotion", wp);
    MoveWaypoint(wp, GetWaypointX(wp) + (18.0 * neg), GetWaypointY(wp) + 18.0);
    CreateObject("RedPotion", wp);
}

void PlaceGlyphGroup(int arg0)
{
    int count = arg0 & 0xff, baseLocation = (arg0 >> 8) & 0xff, location = (arg0 >> 16) & 0xff;
    int i;

    if (count < 14)
    {
        TeleportLocation(location, LocationX(baseLocation), LocationY(baseLocation));
        for (i = 0 ; i < 16 && count != 5 ; i ++)
        {
            if (i ^ 8)
                VirtualGlyphCreate(location);
            TeleportLocationVector(location, 23.0, 23.0);
        }
        TeleportLocationVector(baseLocation, -23.0, 23.0);
        count ++;
        FrameTimerWithArg(1, count | (baseLocation << 8) | (location << 16), PlaceGlyphGroup);
    }
}

void southWallOpen()
{
    int count;

    ObjectOff(SELF);
    if (++count == 2)
    {
        UniPrintToAll("방금 도어락 잠금이 해제되었습니다.");
        UnlockDoor(Object("southDoor1"));
        UnlockDoor(Object("southDoor2"));
        orbTrapOn();
        count = 0;
    }
}

void orbTrapOn()
{
    spawnOrbTower("MovableStatueVictory4SE", 18, 32);
    spawnOrbTower("MovableStatueVictory4SE", 20, 32);
    spawnOrbTower("MovableStatueVictory4NW", 19, 160);

    spawnStructure("BarrelLOTD", 23);
}

void crashGlyphZone()
{
    int buff;

    if (!buff)
    {
        Effect("JIGGLE", GetObjectX(SELF), GetObjectY(SELF), 50.0, 0.0);
        buff = CreateObject("Rock1", 25);
        CreateObject("Rock5", 25);
        CreateObject("rock6", 25);
        Raise(buff, 250.0);
        Raise(buff + 1, 250.0);
        Raise(buff + 2, 250.0);
        AudioEvent("WallDestroyedStone", 25);

        CreateObject("LargeFlame", 28);
        Enchant(buff + 3, "ENCHANT_FREEZE", 0.0);

        AudioEvent("EarthquakeCast", 25);
    }
    ObjectOff(SELF);
}

void PreserveOrbTower()
{
    int parent = GetTrigger() + 1, mis;

    if (IsCaller(parent))
    {
        if (GetDirection(parent) < 210)
            LookWithAngle(parent, GetDirection(parent) + 1);
        else
        {
            LookWithAngle(parent, 0);
            mis = CreateObjectAt("DeathBall", GetObjectX(parent) + UnitAngleCos(SELF, 13.0), GetObjectY(parent) + UnitAngleSin(SELF, 13.0));
            PushObject(mis, 9.0, GetObjectX(parent), GetObjectY(parent));
        }
        MoveObject(SELF, GetObjectX(parent), GetObjectY(parent));
    }
}

int spawnOrbTower(string name, int wp, int angle)
{
    int trap = CreateObjectAt("fishSmall", LocationX(wp), LocationY(wp));

    LookWithAngle(CreateObjectAt(name, GetObjectX(trap), GetObjectY(trap)) - 1, angle);
    Frozen(trap, 1);
    SetCallback(trap, 9, PreserveOrbTower);
}

void removeZombieWalls()
{
    ObjectOff(SELF);

    SpawnFastedZombie(24);
    SpawnFastedZombie(26);
    SpawnFastedZombie(27);

    WallOpen(Wall(80, 190));
    WallOpen(Wall(79, 191));
    WallOpen(Wall(78, 192));
    WallOpen(Wall(77, 193));
    WallOpen(Wall(76, 194));
    WallOpen(Wall(75, 195));
    WallOpen(Wall(74, 196));
    WallOpen(Wall(73, 197));
    WallOpen(Wall(93, 203));
    WallOpen(Wall(92, 204));
    WallOpen(Wall(91, 205));
    WallOpen(Wall(90, 206));
    WallOpen(Wall(89, 207));
    WallOpen(Wall(88, 208));
    WallOpen(Wall(87, 209));
    WallOpen(Wall(86, 210));
    WallOpen(Wall(84, 190));
    WallOpen(Wall(85, 191));
    WallOpen(Wall(86, 192));
    WallOpen(Wall(87, 193));
    WallOpen(Wall(88, 194));
    WallOpen(Wall(89, 195));
    WallOpen(Wall(90, 196));
    WallOpen(Wall(91, 197));
    WallOpen(Wall(92, 198));
    WallOpen(Wall(93, 199));

    CreateObject("BlackPowderBarrel2", 30);
    MoveWaypoint(30, GetWaypointX(30) + 23.0, GetWaypointY(30) - 23.0);
    CreateObject("BlackPowderBarrel2", 30);
    MoveWaypoint(30, GetWaypointX(30) + 23.0, GetWaypointY(30) - 23.0);
    CreateObject("BlackPowderBarrel2", 30);

    initPassPad();
}

void activateSentry()
{
    int ssen, nsen;

    ObjectOff(SELF);
    if (!ssen)
    {
        ssen = Object("southSentry");
        nsen = Object("northSentry");
        ObjectOn(ssen);
        ObjectOn(nsen);
        Move(ssen, 31);
        Move(nsen, 33);

        int questitem = CreateObject("BlackBook2", 36);
        SetUnitCallbackOnPickup(questitem, paperHint);
        Frozen(questitem, TRUE);
    }
}

int CheckUnitOrLocation(int arg0)
{
    float xProfile, yProfile;

    if (arg0 >> 0x10) //unit
    {
        xProfile = GetObjectX(arg0);
        yProfile = GetObjectY(arg0);
    }
    else
    {
        xProfile = LocationX(arg0);
        yProfile = LocationY(arg0);
    }
    return ToInt( &xProfile );
}

int MobClassHecubah(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("Hecubah", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    HecubahSubProcess(mob);
    return mob;
}

int MobClassBlackSpider(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("BlackWidow", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    BlackWidowSubProcess(mob);
    return mob;
}

int MobClassFireSprite(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("FireSprite", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    FireSpriteSubProcess(mob);
    return mob;
}

int MobClassSkeletonLord(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("SkeletonLord", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    SetUnitMaxHealth(mob, 325);
    return mob;
}

int MobClassMystic(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("Wizard", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    SetUnitMaxHealth(mob, 250);
    Enchant(mob, EnchantList(14), 0.0);
    return mob;
}

int MobClassBeholder(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("Beholder", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    SetUnitMaxHealth(mob, 420);
    Enchant(mob, EnchantList(14), 0.0);
    return mob;
}

int MobClassGargoyle(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("EvilCherub", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    SetUnitMaxHealth(mob, 98);
    return mob;
}

int MobClassLich(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("Lich", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    LichSubProcess(mob);
    return mob;
}

int MobClassOrbHecubah(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("HecubahWithOrb", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    HecubahOrbSubProcess(mob);
    return mob;
}

int MobClassEmberDemon(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("EmberDemon", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    SetUnitMaxHealth(mob, 225);
    return mob;
}

int MobClassMecaGolem(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);
    int mob = CreateObjectAt("MechanicalGolem", GetMemoryFloat(link), GetMemoryFloat(link + 4));

    SetUnitMaxHealth(mob, 700);
    return mob;
}

int MobClassNormalZombie(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);

    TeleportLocation(1, GetMemoryFloat(link), GetMemoryFloat(link + 4));
    return SpawnNormalZombie(1);
}

int MobClassFastedZombie(int sUnit)
{
    int link = CheckUnitOrLocation(sUnit);

    TeleportLocation(1, GetMemoryFloat(link), GetMemoryFloat(link + 4));
    return SpawnFastedZombie(1);
}

void RespectGargoyles(int sUnit)
{
    int count = GetDirection(sUnit), target = ToInt(GetObjectZ(sUnit)), mob;

    while (IsObjectOn(sUnit))
    {
        if (count)
        {
            FrameTimerWithArg(1, sUnit, RespectGargoyles);
            mob = MobClassGargoyle(sUnit);
            if (CurrentHealth(target))
                Attack(mob, target);
            LookWithAngle(sUnit, count - 1);
            break;
        }
        Delete(sUnit);
        break;
    }
}

void HookArrowTraps(int trapUnit)
{
    int ptr = UnitToPtr(trapUnit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x0c, 693);
}

int InitUnderfootArrowTraps()
{
    int i, trap;

    if (!trap)
    {
        trap = Object("UnderFarrTrp");
        for (i = 0 ; i < 15 ; Nop(i ++))
        {
            HookArrowTraps(trap + (i * 2));
            ObjectOff(trap + (i * 2));
        }
    }
    return trap;
}

void UnderfootArrowTrapOff(int trap)
{
    int i;

    for (i = 0 ; i < 15 ; Nop(i ++))
        ObjectOff(trap + (i * 2));
}

void UnderfootArrowTrapOn()
{
    int i, trap = InitUnderfootArrowTraps();

    for (i = 0 ; i < 15 ; Nop(++i))
        ObjectOn(trap + (i * 2));
    FrameTimerWithArg(1, trap, UnderfootArrowTrapOff);
}

void SurpriseRespectMonster()
{
    int mob = MobClassHecubah(415);

    UniChatMessage(mob, "ㅋㅋㅋ 개깜놀", 180);
}

void ClearUnderfootExit()
{
    ObjectOff(SELF);

    RemoveWallsFromLocation(380, 5, -23.0, -23.0);
    FrameTimer(3, SurpriseRespectMonster);
}

void VampireKnightBat23(int vampNpc, int location)
{
    int bat = CreateObjectAt("Bat", LocationX(location), LocationY(location));

    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(bat), GetObjectY(bat)), vampNpc);
    SetUnitMaxHealth(bat, 64);
    SetCallback(bat, 5, VampireKnightBatDie);
}

void ClearTunnelWalls()
{
    int flag;

    ObjectOff(SELF);
    if (!flag)
    {
        flag = -1;
        RemoveWallsFromLocation(384, 5, 23.0, 23.0);
        VampireKnightBat23(Object("VampireKnight2"), 404);
        VampireKnightBat23(Object("VampireKnight3"), 405);
        VampireKnightBat23(Object("VampireKnight4"), 406);
        MobClassOrbHecubah(407);
        MobClassOrbHecubah(408);
        MobClassMecaGolem(409);
        MobClassMecaGolem(410);
        spawnFlyingShooter(411, 160);
        StartFallGargoyle(414, 60, 2);
    }
}

void HookFireballTraps(int trapUnit, int misThingId)
{
    int ptr = UnitToPtr(trapUnit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x0c, misThingId);
    }
}

int InitTunnelTraps(int trap, int trapCount)
{
    int i;

    if (trap)
    {
        for (i = 0 ; i < trapCount ; Nop(i ++))
            HookFireballTraps(trap + (i * 2), 696);
    }
    return trap;
}

void ClearFinalWalls()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(385, 2, 23.0, -23.0);
    UniPrintToAll("주변 어딘가에서 벽이 열렸습니다");
}

void TwoButtons()
{
    int count;

    ObjectOff(SELF);
    count ++;
    if (count ^ 2)
        return;
    RemoveWallsFromLocation(383, 5, 23.0, 23.0);
    CreateAnyKeyAtLocation(386, 0, "마지막 방 열쇠");
    UniPrintToAll("가운데 통로가 열렸습니다");
}

void PlaceLastPartMobs()
{
    MobClassBeholder(394);
    MobClassBeholder(397);
    MobClassBlackSpider(395);
    MobClassBlackSpider(398);
    MobClassGargoyle(396);
    MobClassGargoyle(399);
    MobClassOrbHecubah(400);
    MobClassOrbHecubah(401);
    MobClassMystic(402);
    MobClassMystic(403);
    MobClassSkeletonLord(402);
    MobClassSkeletonLord(403);
}

void ClearUpEntrance()
{
    ObjectOff(SELF);

    RemoveWallsFromLocation(381, 2, 23.0, 23.0);
    RemoveWallsFromLocation(382, 2, 23.0, 23.0);
    InitTunnelTraps(Object("TunnelTrapFirst"), 11);
    InitTunnelTraps(Object("TunnelTrapScd"), 11);
    PlaceLastPartMobs();
}

void VampireKnightRespect(int parent)
{
    int npc = ToInt(GetObjectZ(parent));

    if (CurrentHealth(npc))
    {
        Effect("TELEPORT", GetObjectX(parent), GetObjectY(parent), 0.0, 0.0);
        ObjectOn(npc);
        MoveObject(npc, GetObjectX(parent), GetObjectY(parent));
        AggressionLevel(npc, 1.0);
    }
    Delete(parent);
}

void VampireKnightBatDie()
{
    MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
    FrameTimerWithArg(3, GetTrigger() + 1, VampireKnightRespect);
    Effect("SMOKE_BLAST", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

void VampireKnightBat1()
{
    int bat = CreateObjectAt("Bat", LocationX(379), LocationY(379));

    InitUnderfootArrowTraps();
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(bat), GetObjectY(bat)), Object("VampireKnight1"));
    SetUnitMaxHealth(bat, 64);
    SetCallback(bat, 5, VampireKnightBatDie);
    ObjectOff(SELF);
}

void RemoveWallsFromLocation(int location, int maxCount, float xVect, float yVect)
{
    int i;

    for (i = 0 ; i < maxCount ; Nop(i ++))
    {
        WallOpen(WallUtilGetWallAtObjectPosition(location));
        TeleportLocationVector(location, xVect, yVect);
    }
}

void ClearEastLongWalls()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(300, 17, 23.0, 23.0);
    MobClassFastedZombie(332);
    MobClassFastedZombie(333);
    MobClassNormalZombie(328);
    MobClassNormalZombie(329);
    MobClassNormalZombie(330);
    MobClassNormalZombie(331);
    MobClassLich(334);
    MobClassEmberDemon(335);
    MobClassEmberDemon(336);
    MobClassEmberDemon(337);
    MobClassBeholder(327);
    MobClassBeholder(327);
    MobClassBeholder(327);
    MobClassBeholder(327);
    MobClassBeholder(327);
}

void PutMonsters2(int target)
{
    LookAtObject(MobClassLich(307), target);
    LookAtObject(MobClassLich(312), target);
    LookAtObject(MobClassGargoyle(303), target);
    LookAtObject(MobClassGargoyle(304), target);
    LookAtObject(MobClassGargoyle(305), target);
    LookAtObject(MobClassGargoyle(306), target);
    LookAtObject(MobClassGargoyle(308), target);
    LookAtObject(MobClassGargoyle(309), target);
    LookAtObject(MobClassGargoyle(310), target);
    LookAtObject(MobClassGargoyle(311), target);
    LookAtObject(MobClassFastedZombie(304), target);
    LookAtObject(MobClassFastedZombie(309), target);
    LookAtObject(MobClassFastedZombie(306), target);
    LookAtObject(MobClassFastedZombie(311), target);
}

void ClearEast2LongWalls()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(301, 12, 23.0, 23.0);
    RemoveWallsFromLocation(302, 12, 23.0, 23.0);
    FrameTimerWithArg(1, GetCaller(), PutMonsters2);
}

void ClearWallsLeftPart()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(313, 2, 23.0, 23.0);
    MobClassNormalZombie(316);
    MobClassNormalZombie(317);
    MobClassNormalZombie(318);
    MobClassMystic(319);
    MobClassMystic(320);
    MobClassMystic(321);
    MobClassFastedZombie(322);
    MobClassFastedZombie(323);
    MobClassMystic(324);
    MobClassMystic(325);
    RemoveWallsFromLocation(326, 3, 23.0, -23.0);
}

void ClearWallsRightPart()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(314, 2, 23.0, 23.0);
}

void GargoyleFall(int mobMake)
{
    int durate = GetDirection(mobMake);

    while (IsObjectOn(mobMake))
    {
        if (durate)
        {
            FrameTimerWithArg(2, mobMake, GargoyleFall);
            LookWithAngle(mobMake, durate - 1);
            Raise(MobClassGargoyle(mobMake), 250.0);
            break;
        }
        Delete(mobMake);
        break;
    }
}

void StartFallGargoyle(int location, int amount, int delay)
{
    int mobMake = CreateObjectAt("ImaginaryCaster", LocationX(location), LocationY(location));

    LookWithAngle(mobMake, amount);
    FrameTimerWithArg(delay, mobMake, GargoyleFall);
}

void ClearWallsBottomPart()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(315, 3, -23.0, 23.0);
    StartFallGargoyle(338, 30, 10);
}

void DisableUnitGroup(int groupId)
{
    ObjectGroupOff(groupId);
}

void FistTrapCountdown(int trp)
{
    int count = GetDirection(trp);

    if (count)
    {
        LookWithAngle(trp, count - 1);
        FrameTimerWithArg(1, trp, FistTrapCountdown);
    }
}

void TriggeredFistTrap()
{
    int unit;

    if (CurrentHealth(OTHER) && MaxHealth(SELF))
    {
        if (GetDirection(SELF))
            return;
        else
        {
            unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));
            Delete(unit);
            CastSpellLocationLocation("SPELL_FIST", GetObjectX(SELF) - 2.0, GetObjectY(SELF), GetObjectX(SELF), GetObjectY(SELF));
            SetOwner(getMaster(), unit + 1);
            LookWithAngle(SELF, 180);
            FrameTimerWithArg(1, GetTrigger(), FistTrapCountdown);
        }
    }
}

int PutFistTrap(int location)
{
    int trp = CreateObjectAt("Wizard", LocationX(location), LocationY(location));

    Damage(trp, 0, MaxHealth(trp) + 1, -1);
    LookWithAngle(trp, 0);
    SetCallback(trp, 9, TriggeredFistTrap);
    return trp;
}

void PlaceFistTrapInit()
{
    PutFistTrap(352);
    PutFistTrap(353);
    PutFistTrap(354);
    PutFistTrap(355);
    PutFistTrap(356);
    PutFistTrap(357);
    PutFistTrap(358);
    VirtualGlyphCreate(359);
    VirtualGlyphCreate(360);
    VirtualGlyphCreate(361);
    VirtualGlyphCreate(362);
    VirtualGlyphCreate(363);
    VirtualGlyphCreate(364);
}

void OpenMecaGolemGate()
{
    int count;

    count ++;
    if (count ^ 3)
        return;
    else
    {
        UnlockDoor(Object("MecaGate1"));
        UnlockDoor(Object("MecaGate11"));
        UnlockDoor(Object("MecaGate2"));
        UnlockDoor(Object("MecaGate21"));
        MobClassBlackSpider(342);
        MobClassBlackSpider(346);
        MobClassBlackSpider(348);
        MobClassFireSprite(343);
        MobClassFireSprite(344);
        MobClassFireSprite(349);
        MobClassSkeletonLord(350);
        MobClassSkeletonLord(345);
        MobClassSkeletonLord(347);
        StartFallGargoyle(351, 50, 1);
        UniPrint(OTHER, "문이 열렸습니다");
    }
}

void MecaGolemPut()
{
    SetCallback(MobClassMecaGolem(339), 5, OpenMecaGolemGate);
    SetCallback(MobClassMecaGolem(340), 5, OpenMecaGolemGate);
    SetCallback(MobClassMecaGolem(341), 5, OpenMecaGolemGate);
}

void StartThreeMecaLift()
{
    ObjectOff(SELF);
    ObjectGroupOn(2);
    ObjectGroupOn(1);
    FrameTimerWithArg(30, 1, DisableUnitGroup);
    FrameTimer(8, MecaGolemPut);
    PlaySoundAround(OTHER, 919);
    PlaySoundAround(OTHER, 801);
}

void DisableUnderfootLockGate()
{
    int mobMake = CreateObjectAt("ImaginaryCaster", LocationX(278), LocationY(278)), i;

    LookWithAngle(mobMake, 12);
    Raise(mobMake, GetCaller());
    ObjectOff(SELF);
    FrameTimerWithArg(1, mobMake, RespectGargoyles);
    RemoveWallsFromLocation(279, 8, 23.0, 23.0);
    PlaceTombStatuesInit();
    for (i = 0 ; i < 4 ; i ++)
        LookAtObject(MobClassSkeletonLord(CreateObjectAt("ImaginaryCaster", LocationX(288 + i), LocationY(288 + i))), OTHER);
}

void AwakeMono(int stUnit, int target)
{
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(stUnit), GetObjectY(stUnit));
    int mob;

    Delete(stUnit);
    mob = MobClassSkeletonLord(posUnit);
    LookAtObject(mob, target);
    Delete(posUnit);
    Effect("SMOKE_BLAST", GetObjectX(mob), GetObjectY(mob), 0.0, 0.0);
}

void TombStatueAwake(int first, int target)
{
    int amount = GetDirection(first), i;

    Delete(first);
    first ++;
    for (i = 0 ; i < amount ; i ++)
        AwakeMono(first + i, target);
    Delete(first);
}

void TombStatueTouch()
{
    int tombGr;

    if (CurrentHealth(OTHER))
    {
        if (IsPlayerUnit(OTHER) && IsObjectOn(SELF))
        {
            tombGr = PlaceTombStatuesInit();
            if (GetDirection(tombGr))
            {
                TombStatueAwake(PlaceTombStatuesInit(), OTHER);
            }
        }
    }
}

int TombStatueCreate(string stName, int location)
{
    int tombStatue = CreateObjectAt(stName, LocationX(location), LocationY(location));

    SetUnitCallbackOnCollide(tombStatue, TombStatueTouch);
    return tombStatue;
}

void PlaceTombStatues(int location, int count)
{
    string stName[] = {"MovableStatue2a", "MovableStatue2b", "MovableStatue2c", "MovableStatue2d",
        "MovableStatue2e", "MovableStatue2f", "MovableStatue2g", "MovableStatue2h"};
    int i;

    for (i = 0 ; i < count ; Nop(i ++))
    {
        Nop(TombStatueCreate(stName[Random(0, 7)], location));
        TeleportLocationVector(location, -46.0, 46.0);
    }
}

int PlaceTombStatuesInit()
{
    int i, first;
    
    if (!first)
    {
        first = CreateObjectAt("ImaginaryCaster", LocationX(292), LocationY(292));
        for (i = 0 ; i < 8 ; Nop(i ++))
            PlaceTombStatues(292 + i, 4);
        LookWithAngle(first, 8 * 4);
    }
    return first;
}

void paperHint()
{
    UniChatMessage(SELF, "책의 내용 일부: PASS = " + IntToString(D_RAND[0]) + "+" + IntToString(D_RAND[1]), 180);
    AudioEvent("JournalEntryAdd", 36);
}

void nullPointer()
{ }

void initPassPad()
{
    int var_0[10];
    int i;

    var_0[0] = Object("passPadBase");
    LookWithAngle(var_0[0], 0);
    for (i = 1; i < 10 ; i ++)
    {
        var_0[i] = var_0[0] + (i * 2);
        LookWithAngle(var_0[i], i);
    }
}

void pressPasswordNumber()
{
    int innum = GetDirection(SELF);

    UniChatMessage(OTHER, "입력한 숫자: " + IntToString(innum), 150);
    PlaySoundAround(OTHER, 296);
    Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    scanPassword(innum);
}

void scanPassword(int i)
{
    int var_0;
    int var_1;

    if (var_0 != 255)
    {
        if (!var_0)
        {
            var_1 = i * 1000;
            var_0 = 1;
        }
        else if (var_0 == 1)
        {
            var_1 += i * 100;
            var_0 = 2;
        }
        else if (var_0 == 2)
        {
            var_1 += i * 10;
            var_0 = 3;
        }
        else if (var_0 == 3)
        {
            var_1 += i;
            if (var_1 == D_RAND[0] + D_RAND[1])
            {
                openPasswordWall();
                UniPrintToAll("잠금이 해제되었습니다.");
                var_0 = 255;
            }
            else
            {
                UniPrintToAll("비밀번호가 일치하지 않습니다.");
                var_0 = 0;
            }
        }
        UniPrintToAll("입력된 비밀번호: " + IntToString(var_1));
    }
}

void openPasswordWall()
{
    int lockKey = CreateAnyKeyAtLocation(38, 1, "중앙 메인 홀 열쇠"), x = 61, y = 161;

    Enchant(lockKey, "ENCHANT_FREEZE", 0.0);
    for (x ; x >= 49 ; Nop(x --))
        WallOpen(Wall(x, y++));
}

int DummyUnitCreate(string name, int wp)
{
    int unit = CreateObjectAt(name, LocationX(wp), LocationY(wp));

    if (CurrentHealth(unit))
    {
        ObjectOff(unit);
        Damage(unit, 0, MaxHealth(unit) + 1, -1);
        Frozen(unit, 1);
    }
    return unit;
}

int DeadUnitCreate(string unitName, int location)
{
    int deadUnit = CreateObjectAt(unitName, LocationX(location), LocationY(location));

    Damage(deadUnit, 0, CurrentHealth(deadUnit) + 1, -1);
    return deadUnit;
}

int PlayerCheckPointLocation(int sUnit)
{
    return GetDirection(sUnit) | (GetDirection(sUnit + 1) << 0x8);
}

void PlayerCheckPointTouch()
{
    if (MaxHealth(SELF))
    {
        if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
        {
            TeleportLocation(PlayerCheckPointLocation(GetTrigger()), GetObjectX(SELF), GetObjectY(SELF));
            GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
            Delete(GetTrigger() + 1);
            Delete(SELF);
            UniPrint(OTHER, "이곳으로 체크포인트 지정이 완료되었습니다");
        }
    }
}

int PlayerCheckPointCreate(int location, int reviveLocation)
{
    int ckPoint = DeadUnitCreate("WeirdlingBeast", location);

    SetCallback(CreateObjectAt("BlueSummons", GetObjectX(ckPoint), GetObjectY(ckPoint)) - 1, 9, PlayerCheckPointTouch);
    LookWithAngle(ckPoint, reviveLocation & 0xff);
    LookWithAngle(ckPoint + 1, reviveLocation >> 8);
    return ckPoint;
}

void entrancePart2()
{
    int monleader;

    ObjectOff(SELF);
    if (!monleader)
    {
        spawnNormalUnit("skeletonLord", 39, 325, 96);
        spawnNormalUnit("skeletonLord", 45, 325, 96);
        spawnNormalUnit("skeleton", 40, 295, 96);
        spawnNormalUnit("skeleton", 44, 295, 96);
        SpawnFastedZombie(41);
        SpawnFastedZombie(43);
        monleader = spawnNormalUnit("wizard", 42, 260, 96);
        Enchant(monleader, "ENCHANT_ANCHORED", 0.0);
        spawnGlyph(50, 225, "SPELL_FIREBALL", "SPELL_CLEANSING_FLAME", "NULL");
        spawnGlyph(51, 96, "SPELL_FIREBALL", "SPELL_CLEANSING_FLAME", "NULL");
        spawnGlyph(52, 225, "SPELL_FIREBALL", "SPELL_CLEANSING_FLAME", "NULL");
        spawnKeybox("Coffin4");
        FrameTimer(30, spawnStones);
    }
}

void VirtualGlyphCollide()
{
    if (CurrentHealth(OTHER) && IsObjectOn(SELF))
    {
        if (IsPlayerAndMobUnit(OTHER))
        {
            Damage(OTHER, 0, 100, 1);
            Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            Delete(SELF);
        }
    }
}

int VirtualGlyphCreate(int location)
{
    int glyph = CreateObjectAt("Glyph", LocationX(location), LocationY(location));
    
    SetUnitCallbackOnCollide(glyph, VirtualGlyphCollide);
    return glyph;
}

void delayTeleportWaypoints()
{
    int i;

    for (i = 0 ; i < 4 ; i ++)
        TeleportLocationVector(i + 46, -69.0, 69.0);
}

void spawnKeybox(string name)
{
    int i, repeat;

    if (repeat < 16)
    {
        for (i = 0 ; i < 4; i ++)
        {
            if (repeat == D_RAND[2])
                spawnCoff(name, i + 46, 0);
            else
                spawnCoff(name, i + 46, 1);
            repeat ++;
        }
        FrameTimer(2, delayTeleportWaypoints);
        FrameTimerWithArg(3, name, spawnKeybox);
    }
}

void fieldIn()
{
    MoveObject(OTHER, 2705.0, 3346.0);
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    MoveWaypoint(22, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("BlindOff", 22);
}

void fieldOut()
{
    MoveObject(OTHER, GetWaypointX(160), GetWaypointY(160));
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    MoveWaypoint(22, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("BlindOff", 22);
}

void spawnStones()
{
    int rep;

    if (rep < 36)
    {
        spawnUnitMovingPower("TraderArmorRack1", 54);
        rep ++;
        FrameTimer(3, spawnStones);
    }
}

void putLightningTraps()
{
    int cand, i;

    ObjectOff(SELF);
    if (!cand)
    {
        cand = CreateObject("Candleabra3", 58);
        spawnPowerPlant(59, 400);
        Enchant(cand, "ENCHANT_FREEZE", 0.0);
        for (i = 3 ; i >= 0 ; i --)
        {
            CreateObject("Skull3", 56);
            TeleportLocationVector(56, 69.0, 69.0);
        }
    }
}

void freeSouthSingleWall()
{
    int lockKey = CreateAnyKeyAtLocation(57, 2, "별관입구 열쇠");

    Enchant(lockKey, "ENCHANT_FREEZE", 0.0);
    Raise(lockKey, 250.0);
    AudioEvent("KeyDrop", 57);

    ObjectOff(SELF);
    WallOpen(Wall(52, 140));

    spawnFlyingShooter(68, 275);
    spawnFlyingShooter(69, 275);
    SpawnFastedZombie(70);
    SpawnFastedZombie(71);
    SpawnFastedZombie(72);
    SpawnFastedZombie(73);
}

void freeNorthEastWall()
{
    int x1 = 89, y1 = 103;
    int x2 = 90, y2 = 104;

    ObjectOff(SELF);
    for (x1 ; x1 >= 72 ; Nop(x1 --))
    {
        WallOpen(Wall(x1, y1++));
        if (x2 <= 103)
            WallOpen(Wall(x2++, y2++));
    }
    SetCallback(spawnNormalUnit("StoneGolem", 61, 500, 32), 5, golemSetDeaths);
    SetCallback(spawnNormalUnit("StoneGolem", 62, 500, 32), 5, golemSetDeaths);
    SetCallback(spawnNormalUnit("StoneGolem", 63, 500, 96), 5, golemSetDeaths);
    SetCallback(spawnNormalUnit("StoneGolem", 64, 500, 96), 5, golemSetDeaths);
}

void golemSetDeaths()
{
    int counting;

    if (++counting == 4)
    {
        UniPrintToAll("방금 10시 방향의 비밀벽이 열렸습니다.");
        WallOpen(Wall(75, 109));
        WallOpen(Wall(74, 110));
        WallOpen(Wall(73, 111));
        WallOpen(Wall(72, 112));
        FBTRAP = 1;
        crossBowTraps();
    }
}

void crossBowTraps()
{
    int statues[6], i;

    if (!statues[0])
    {
        for (i = 0 ; i < 6 ; i ++)
        {
            statues[i] = CreateObject("MovableStatueVictory3SW", 65);
            TeleportLocationVector(65, 23.0, 23.0);
        }
        FrameTimer(30, crossBowTraps);
    }
    else if (!GetDirection(statues[0]))
    {
        for (i = 0 ; i < 6 ; i ++)
        {
            MoveWaypoint(65, GetObjectX(statues[i]) - 23.0, GetObjectY(statues[i]) + 23.0);
            int mis = CreateObject("ArcherBolt", 65);
            LookWithAngle(mis, 96);
            PushObjectTo(mis, -32.0, 32.0);
        }
        if (FBTRAP == 1)
            SecondTimer(2, crossBowTraps);
    }
}

void castRingOfFire()
{
    int fire = fireRing("Flame", 60);
    
    if (!IsObjectOn(fire))
    {
        Effect("SPARK_EXPLOSION", LocationX(60), LocationY(60), 0.0, 0.0);
        AudioEvent("CleansingFlameCast", 60);
        AudioEvent("FirewalkOn", 60);
    }
}

int fireRing(string name, int wp)
{
	int var_0[31];
	int i;

	for (i = 0 ; i < 30 && !IsObjectOn(var_0[i]) ; i ++)
	{
		MoveWaypoint(58, GetWaypointX(wp) + MathSine(i * 12 + 90, 10.0), GetWaypointY(wp) + MathSine(i * 12, 10.0));
		var_0[i] = CreateObject(name, 58);
		LookWithAngle(var_0[i], i);
		DeleteObjectTimer(var_0[i], 180);
		FrameTimerWithArg(1, var_0[i], preserveFireRing);
	}
    return var_0[0];
}

void preserveFireRing(int unit)
{
	int var_0 = GetDirection(unit) * 12;

	if (IsObjectOn(unit))
	{
		PushObjectTo(unit, MathSine(var_0 + 90, 1.0), MathSine(var_0, 1.0));
		FrameTimerWithArg(1, unit, preserveFireRing);
	}
	else
		Delete(unit);
}

void removeFinalWalls()
{
    int x = 108, y = 84;

    ObjectOff(SELF);
    UniPrintToAll("스톤골렘을 파괴하게 되면 스톤골렘 뒷편에 있는 벽이 사라집니다.");
    for (x ; x >= 94 ; Nop(x --))
        WallOpen(Wall(x, y++));

    int golm = spawnNormalUnit("StoneGolem", 79, 600, 160);

    SetCallback(golm, 5, removeGolemWall);
    CastSpellObjectObject("SPELL_TURN_UNDEAD", x, x);
    spawnFlyingShooter(75, 240);
    spawnFlyingShooter(76, 240);
    spawnFlyingShooter(77, 240);
    spawnFlyingShooter(78, 240);

    CreateAnyKeyAtLocation(80, 0, "강당열쇠");
}

void removeGolemWall()
{
    UniPrintToAll("마법의 벽이 사라졌습니다...!");
    WallOpen(Wall(113, 101));
    WallOpen(Wall(112, 102));
    MagicStatues(-1);
    FBTRAP = 0;
}

void ReviveFlameTower()
{
    int i;
    int res = 0;

    for (i = 3 ; i >= 0 ; i --)
    {
        if (Distance(GetWaypointX(i + 118), GetWaypointY(i + 118), GetObjectX(MagicStatues(i)), GetObjectY(MagicStatues(i))) < 23.0)
            res ++;
    }
    if (res == 4)
    {
        MoveWaypoint(81, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("BoulderRoll", 81);
        AudioEvent("AwardSpell", 81);
        AudioEvent("SpellDrop", 81);
        Effect("WHITE_FLASH", GetWaypointX(81), GetWaypointY(81), 60.0, 0.0);
        Effect("JIGGLE", GetWaypointX(81), GetWaypointY(81), 60.0, 0.0);
        ObjectOff(SELF);
        ObjectOn(Object("FlameTower"));
        FrameTimer(120, OpenFlameTowerEastWall);
    }
}

void OpenFlameTowerEastWall()
{
    int x;

    UniPrint(OTHER, "뒤쪽 벽이 열립니다.");
    NoWallSound(1);
    for (x = 53 ; x <= 71 ; Nop(x ++))
        WallOpen(Wall(x, x + 10));
    spawnFlyingShooter(122, 500);
    spawnFlyingShooter(123, 500);
    spawnFlyingShooter(124, 500);
    CreateAnyKeyAtLocation(127, 2, "별관 출구 열쇠");
    initFinalArea();
}

void DelayUnitOn(int unit)
{
    LookWithAngle(unit, 160);
}

int ArrowTrp(int num)
{
    int unit[18], i;

    if (num == -1)
    {
        for (i = 0 ; i < 9 ; Nop(i ++))
        {
            unit[i * 2] = CreateObject("ArrowTrap1", 128);
            unit[i * 2 + 1] = CreateObject("ArrowTrap1", 129);
            TeleportLocationVector(128, 12.0, -12.0);
            TeleportLocationVector(129, 12.0, -12.0);
            ObjectOff(unit[i * 2]);
            ObjectOff(unit[i * 2 + 1]);
            FrameTimerWithArg(1, unit[i * 2 + 1], DelayUnitOn);
        }
        return 0;
    }
    return unit[num];
}

void ActivateArrowTraps()
{
    int i;

    for (i = 17 ; i >= 0 ; Nop(i --))
    {
        ObjectOn(ArrowTrp(i));
    }
    FrameTimer(1, DisableArrowTraps);
}

void DisableArrowTraps()
{
    int i;

    for (i = 17 ; i >= 0 ; Nop(i --))
    {
        ObjectOff(ArrowTrp(i));
    }
}

void initFinalArea()
{
    ArrowTrp(-1);
    spawnFlyingShooter(130, 500);
    spawnFlyingShooter(131, 500);
    spawnFlyingShooter(140, 500);
    spawnFlyingShooter(141, 500);
    PutCurePotions(-134);
    PutCurePotions(-135);
    FrameTimerWithArg(1, SummonGen(136), LoopSummonGen);
    FrameTimerWithArg(1, SummonGen(137), LoopSummonGen);
    FrameTimerWithArg(1, SummonGen(138), LoopSummonGen);
    FrameTimer(1, PutIxPart);
}

void PutIxPart()
{
    int i;

    for (i = 0 ; i < 8 ; Nop(i ++))
    {
        spawnNormalUnit("Shade", 132, 300, 160);
        spawnNormalUnit("Mimic", 133, 450, 160);
        MoveWaypoint(132, GetWaypointX(132) - 46.0, GetWaypointY(132) + 46.0);
        MoveWaypoint(133, GetWaypointX(133) - 46.0, GetWaypointY(133) + 46.0);
    }
    PutCurePotions(144);
    PutCurePotions(145);
    PutCurePotions(-146);
    PutCurePotions(-147);
}

void destroyGen()
{
    int count;

    ObjectOff(SELF);
    if (++count == 2)
    {
        UniPrintToAll("비밀의 벽이 열립니다.");
        int x;

        for (x = 110 ; x <= 112 ; Nop(x ++))
        {
            WallOpen(Wall(x + 60, x));
        }
        spawnFlyingShooter(142, 500);
        spawnFlyingShooter(142, 500);
        int hit = spawnFlyingShooter(142, 500);
        SetCallback(hit, 5, FlyingKeyDrop);
    }
}

void FlyingKeyDrop()
{
    MoveWaypoint(136, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("KeyDrop", 136);
    CreateAnyKeyAtLocation(136, 3, "루비열쇠 입니다");
    RemoveWallsFromLocation(434, 6, -23.0, 23.0);
    UniPrintToAll("비밀의 벽이 열립니다");
    DeleteObjectTimer(SELF, 1);
}

int SummonGen(int wp)
{
    int gen = CreateObject("StoneGolemGenerator", wp);

    SetOwner(getMaster(), gen);
    SetUnitMaxHealth(gen, 600);
    ObjectOff(gen);
    return gen;
}

void EntryLastBossRoom()
{
    ObjectOff(SELF);
    GetFinalBoss(143);
    PutCurePotions(-151);
    PutCurePotions(-152);
    PutCurePotions(-153);
    RWizSummon(154, 600);
    RWizSummon(155, 600);
}

int GetFinalBoss(int wp)
{
    int unit;

    if (!unit)
    {
        unit = spawnNormalUnit("Horrendous", wp, 2000, 225);
        SetCallback(unit, 3, FinalBossDetected);
        FrameTimerWithArg(1, unit, LoopFinalBoss);
    }
    return unit;
}

void LoopFinalBoss(int unit)
{
    if (CurrentHealth(unit) > 0)
    {
        if (HasEnchant(unit, "ENCHANT_ETHEREAL") && !HasEnchant(unit, "ENCHANT_DETECTING"))
        {
            EnchantOff(unit, "ENCHANT_ETHEREAL");
            Enchant(unit, "ENCHANT_BLINDED", 0.1);
        }
        FrameTimerWithArg(30, unit, LoopFinalBoss);
    }
}

void FinalBossDetected()
{
    int pick;
    float durate = 10.0;

    if (CurrentHealth(SELF) > 0)
    {
        if (CurrentHealth(OTHER) > 0)
        {
            pick = Random(0, 3);
            if (!pick)
            {
                FinalBossAbility1();
            }
            else if (pick == 1)
                FinalBossAbility2();
            Enchant(SELF, "ENCHANT_DETECTING", durate);
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.0);
        }
    }
}

void FinalBossDie()
{
    UniPrintToAll("승리__!! 마지막 보스를 죽였습니다..!");
    MoveWaypoint(143, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("Wizard1Retreat", 143);
    AudioEvent("StaffOblivionAchieve4", 143);
    DeleteObjectTimer(CreateObject("LevelUp", 143), 1800);
    Effect("WHITE_FLASH", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    DeleteObjectTimer(SELF, 30);
    strDeadFBoss();
}

void strDeadFBoss()
{
	int arr[31];
	string name = "HealOrb";
	int i = 0;
	arr[0] = 71287352; arr[1] = 2139097092; arr[2] = 2099168; arr[3] = 67636228; arr[4] = 270008336; arr[5] = 100417552; arr[6] = 269479972; arr[7] = 270551040; arr[8] = 1124614272; arr[9] = 5247009; 
	arr[10] = 2065164; arr[11] = 538972289; arr[12] = 69664; arr[13] = 1090519040; arr[14] = 1070071330; arr[15] = 1341918232; arr[16] = 744620543; arr[17] = 524288; arr[18] = 65536; arr[19] = 1883374592; 
	arr[20] = 4103; arr[21] = 33431520; arr[22] = 135300096; arr[23] = 32; arr[24] = 134349856; arr[25] = 2131763583; arr[26] = 536903655; arr[27] = 1557512; arr[28] = 8066; arr[29] = 2017460224; 
	arr[30] = 15; 
	while (i < 31)
	{
		drawstrDeadFBoss(arr[i], name);
		Nop(++i);
	}
}

void drawstrDeadFBoss(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(143);
		pos_y = LocationY(143);
	}
	for (i = 1 ; i > 0 && count < 961 ; i <<= 1)
	{
		if (i & arg_0)
			Nop(CreateObject(name, 143));
		if (count % 86 == 85)
            TeleportLocationVector(143, -170.0, 2.0);
		else
            TeleportLocationVector(143, 2.0, 0.0);
		Nop(count ++);
	}
	if (count >= 961)
	{
		count = 0;
		TeleportLocation(143, pos_x, pos_y);
	}
}

void FinalBossAbility1()
{
    float pos_x = UnitRatioX(SELF, OTHER, 20.0), pos_y = UnitRatioY(SELF, OTHER, 20.0);
    int k;
    int missile;

    MoveWaypoint(143, GetObjectX(SELF) + (1.0 / 1.6 * pos_y) - pos_x, GetObjectY(SELF) - (1.0 / 1.6 * pos_x) - pos_y);
    AudioEvent("MysticChant", 143);
    for (k = 0 ; k < 9 ; Nop(k ++))
    {
        pos_x = GetRatioUnitWpXY(SELF, 143, 0, 23.0);
        pos_y = GetRatioUnitWpXY(SELF, 143, 1, 23.0);
        MoveWaypoint(143, GetObjectX(SELF) - (1.0 / 8.0 * pos_y) - pos_x, GetObjectY(SELF) + (1.0 / 8.0 * pos_x) - pos_y);
        missile = CreateObject("ArcherArrow", 143);
        LookAtObject(missile, SELF);
        LookWithAngle(missile, GetDirection(missile) + 128);
        SetOwner(getMaster(), missile);
        PushObject(missile, 20.0, GetObjectX(SELF), GetObjectY(SELF));
    }
}

void FinalBossAbility2()
{
    CastSpellObjectLocation("SPELL_FIST", SELF, GetObjectX(OTHER), GetObjectY(OTHER));
    CastSpellObjectLocation("SPELL_BURN", SELF, GetObjectX(OTHER), GetObjectY(OTHER));
    Enchant(SELF, "ENCHANT_INVULNERABLE", 3.0);
}

void LoopSummonGen(int base)
{
    int unit;
    int i;

    if (CurrentHealth(base) > 0)
    {
        i = CheckSeePlayers(base);
        if (i >= 0 && !CurrentHealth(unit))
        {
            int user=GetPlayerUnit(i);
            MoveWaypoint(136, GetObjectX(base) - UnitRatioX(base, user, 40.0), GetObjectY(base) - UnitRatioY(base, user, 40.0));
            unit = spawnNormalUnit("StoneGolem", 136, 700, 225);
            DeleteObjectTimer(CreateObject("ForceOfNatureCharge", 136), 7);
            Effect("LIGHTNING", GetObjectX(base), GetObjectY(base), GetWaypointX(136), GetWaypointY(136));
            PlaySoundAround(unit,SOUND_MonsterGeneratorSpawn);
        }
        FrameTimerWithArg(45, base, LoopSummonGen);
    }
}

int spawnNormalUnit(string name, int wp, int hp, int angle)
{
    int var_0 = CreateObject(name, wp);

    SetUnitMaxHealth(var_0, hp);
    CreatureGuard(var_0, GetObjectX(var_0), GetObjectY(var_0), GetObjectX(var_0), GetObjectY(var_0), 450.0);
    LookWithAngle(var_0, angle);
    SetOwner(getMaster(), var_0);

    return var_0;
}
void ZombieWeaponEvent()
{
    int mis;

    if (CurrentHealth(SELF))
    {
        if (IsVisibleTo(OTHER, SELF) && CurrentHealth(OTHER))
        {
            if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < 100.0 && !UnitCheckEnchant(SELF, GetLShift(10)))
            {
                Enchant(SELF, EnchantList(10), 1.0);
                mis = CreateObjectAt("ThrowingStone", GetObjectX(OTHER), GetObjectY(OTHER));
                SetOwner(SELF, mis);
            }
            else
            {
                LookAtObject(SELF, OTHER);
                Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
                PushObjectTo(SELF, UnitRatioX(SELF, OTHER, -5.0), UnitRatioY(SELF, OTHER, -5.0));
            }
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.08);
        AggressionLevel(SELF, 1.0);
    }
}

void ReviveZombie(int sUnit)
{
    int zomb = ToInt(GetObjectZ(sUnit));

    if (MaxHealth(zomb))
    {
        DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(zomb), GetObjectY(zomb) + 4.0), 7);
        RaiseZombie(zomb);
        PlaySoundAround(zomb, 591);
    }
    else
    {
        PlaySoundAround(sUnit, 8);
        DeleteObjectTimer(CreateObjectAt("Flame", GetObjectX(sUnit), GetObjectY(sUnit)), 210);
        Effect("SPARK_EXPLOSION", GetObjectX(sUnit), GetObjectY(sUnit), 0.0, 0.0);
    }
    Delete(sUnit);
}

void DeadZombieEvent()
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));

    Raise(unit, GetTrigger());
    FrameTimerWithArg(23, unit, ReviveZombie);
}

void NormalDeadZombieEvent()
{
    DeleteObjectTimer(CreateObjectAt("MediumFlame", GetObjectX(SELF), GetObjectY(SELF)), 90);
    PlaySoundAround(SELF, 8);
    Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
}

int SpawnFastedZombie(int location)
{
    int mob = CreateObjectAt("VileZombie", LocationX(location), LocationY(location));

    Raise(mob, 280.0);
    VileZombieSubProcess(mob);
    SetUnitScanRange(mob, 450.0);
    SetCallback(mob, 5, DeadZombieEvent);
    SetCallback(mob, 3, ZombieWeaponEvent);
    SetOwner(getMaster(), mob);

    return mob;
}

int SpawnNormalZombie(int location)
{
    int mob = CreateObjectAt("Zombie", LocationX(location), LocationY(location));

    ZombieSubProcess(mob);
    SetUnitScanRange(mob, 450.0);
    SetCallback(mob, 5, NormalDeadZombieEvent);
    SetOwner(getMaster(), mob);

    return mob;
}

void WeaponWithWizard()
{
    if (CurrentHealth(SELF))
    {
        if (CurrentHealth(OTHER))
        {
            if (HasEnchant(SELF, "ENCHANT_ANTI_MAGIC"))
                EnchantOff(SELF, "ENCHANT_ANTI_MAGIC");
            else
            {
                Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
                Damage(OTHER, SELF, 10, 17);
            }
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.2);
        AggressionLevel(SELF, 1.0);
    }
}

int spawnPowerPlant(int wp, int hp)
{
    int unit = CreateObject("CarnivorousPlant", wp);

    SetUnitMaxHealth(unit, hp);
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 500.0);
    SetUnitSpeed(unit, 1.6);
    SetOwner(getMaster(), unit);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);

    return unit;
}

int spawnFlyingShooter(int wp, int hp)
{
    int unit = CreateObject("FlyingGolem", wp);

    SetUnitMaxHealth(unit, hp);
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 450.0);
    SetCallback(unit, 3, flyingShooterAttack);
    SetOwner(getMaster(), unit);

    return unit;
}

int RWizSummon(int wp, int hp)
{
    int unit = CreateObject("WizardRed", wp);
    int ptr = GetMemory(0x750710);

    SetUnitMaxHealth(unit, hp);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 450.0);
    SetCallback(unit, 3, WeaponWithWizard);
    SetOwner(getMaster(), unit);
    return unit;
}

void flyingShooterAttack()
{
    int missile;

    if (CurrentHealth(SELF))
    {
        if (IsVisibleTo(OTHER, SELF) && !HasEnchant(SELF, "ENCHANT_VILLAIN"))
        {
            Enchant(SELF, "ENCHANT_VILLAIN", 1.5);
            MoveWaypoint(74, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 11.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 11.0));
            Effect("CYAN_SPARKS", GetWaypointX(74), GetWaypointY(74), 0.0, 0.0);
            AudioEvent("EmberDemonMissing", 74);
            missile = spawnHarpoonShot(74, OTHER);
            LookAtObject(missile, OTHER);
            SetOwner(SELF, missile);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.4);
        AggressionLevel(SELF, 1.0);
    }
}

void FlyingMissileTouched()
{
    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, getMaster()))
    {
        Damage(OTHER, SELF, 30, 14);
        Delete(SELF);
    }
}

void HarpoonDraw(int unit)
{
    if (GetObjectX(unit) != 0.0)
    {
        if (CheckUnitLimitLine(unit))
        {
            MoveObject(unit, GetObjectX(unit) + UnitAngleCos(unit, 20.0), GetObjectY(unit) + UnitAngleSin(unit, 20.0));
            MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
        }
        else
            Delete(unit);
        FrameTimerWithArg(1, unit, HarpoonDraw);
    }
    else
        Delete(unit + 1);
}

int spawnHarpoonShot(int wp, int target)
{
    int bber = CreateObject("Bomber", wp);

    SetOwner(SELF, bber);
    SetCallback(bber, 9, FlyingMissileTouched);
    LookAtObject(bber, target);
    Damage(bber, 0, 255, -1);
    ObjectOff(bber);
    DeleteObjectTimer(bber, 40);
    CreateObject("BlueOrb", wp);
    Frozen(bber, 1);
    Frozen(bber + 1, 1);

    FrameTimerWithArg(1, bber, HarpoonDraw);

    return bber;
}

void plantWeaponTrigger()
{
    if (CurrentHealth(SELF))
    {
        if (IsVisibleTo(OTHER, SELF) && CurrentHealth(OTHER))
        {
            if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < 100.0)
            {
                if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
                {
                    DeleteObjectTimer(CreateObjectAt("MagicEnergy", GetObjectX(OTHER), GetObjectY(OTHER)), 10);
                    PlaySoundAround(OTHER, 117);
                    Damage(OTHER, SELF, 25, 2);
                    Enchant(SELF, "ENCHANT_VILLAIN", 1.3);
                    MoveObject(SELF, GetObjectX(OTHER), GetObjectY(OTHER));
                }
            }
            else
            {
                LookAtObject(SELF, OTHER);
                PushObjectTo(SELF, UnitAngleCos(SELF, 5.0), UnitAngleSin(SELF, 5.0));
            }
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.08);
        AggressionLevel(SELF, 1.0);
    }
}

int setLife(int flags)
{
    int lifes[12], count, i;

    if (flags == -2)
    {
        for (i = 11 ; i >= 0 ; Nop(i --))
        {
            lifes[i] = CreateObjectAt("Ankh", LocationX(17), LocationY(17));
            TeleportLocationVector(17, -23.0, 23.0);
            Nop(count ++);
        }
        return 0;
    }
    else if (flags == -1)
    {
        count --;
        Delete(lifes[count]);
        if (!count)
            noCoin();
    }
    return count;
}

void noCoin()
{
    UniPrintToAll("더 이상 목숨이 남아있지 않기 때문에 부활하실 수 없습니다");
    MoveObject(Object("StartLocation"), LocationX(157), LocationY(157));
}

string SupplyInventoryNames(int indexNumber)
{
    string itemName[] = {"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots",
        "PlateLeggings", "GreatSword", "RedPotion", "RedPotion"};
    
    return itemName[indexNumber];
}

void PutInventorys(int location)
{
    int k;

    if (k < 8)
    {
        float posX = LocationX(location), posY = LocationY(location);
        int i;
        for (i = 14 ; i >= 0 ; Nop(i --))
        {
            Nop(CreateObjectAt(SupplyInventoryNames(k), LocationX(location), LocationY(location)));
            TeleportLocationVector(location, 23.0, 23.0);
        }
        TeleportLocation(location, posX + 23.0, posY - 23.0);
        k ++;
        FrameTimerWithArg(3, location, PutInventorys);
    }
}

void goLastTransmission()
{
    MoveObject(OTHER, LocationX(67), LocationY(67));
}

static void NetworkUtilClientMain()
{
    PlayerClassCommonWhenEntry();
}

void glyphMove(int unit)
{
    MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
}

void preserveGlyph()
{
    int subsub = GetTrigger() - 2;

    if (!IsObjectOn(subsub + 1) && IsCaller(subsub + 3))
    {
        ObjectOn(subsub);
        Delete(SELF);
        Delete(subsub + 3);
    }
}

int spawnGlyph(int wp, int angle, string spell_1, string spell_2, string spell_3)
{
    int bber = CreateObject("BomberBlue", wp);
    float xpro;

    xpro += 21.0;

    CreateObject("glyph", wp);
    MoveObject(bber + 1, 5080.0, 5080.0);
    CreateObject("fishSmall", wp);
    CreateObject("shadow", wp);
    TrapSpells(bber, spell_1, spell_2, spell_3);
    Frozen(bber + 2, 1);
    SetCallback(bber + 2, 9, preserveGlyph);
    ObjectOff(bber);
    Damage(bber, 0, 20, -1);
    MoveObject(bber + 2, xpro, 100.0);
    MoveObject(bber + 3, xpro, 100.0);

    FrameTimerWithArg(1, bber, glyphMove);

    return bber;
}

int spawnStructure(string name, int wp)
{
    int subsub = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(subsub, 30);
    Damage(subsub, 0, 500, -1);
    SetCallback(subsub, 9, preserveStructure);
    CreateObject(name, 1);
    CreateObject("CaveRocksTiny", wp);
    LookWithAngle(subsub + 1, wp);
    FrameTimerWithArg(1, subsub + 1, delayTeleportDummy);

    return subsub;
}

void delayTeleportDummy(int unit)
{
    int locationId = GetDirection(unit);

    MoveObject(unit, LocationX(locationId), LocationY(locationId));
    MoveObject(unit + 1, LocationX(locationId), LocationY(locationId));
}

void preserveStructure()
{
    int subsub = GetTrigger() + 1;

    if (IsCaller(subsub))
    {
        MoveObject(subsub, GetObjectX(SELF), GetObjectY(SELF));
        MoveObject(subsub + 1, GetObjectX(SELF), GetObjectY(SELF) - 1.0);
    }
    else if (!CurrentHealth(subsub))
    {
        CreateAnyKey(SELF, 1, "감마선 구역 입구열쇠");
        Delete(SELF);
        Delete(subsub + 1);
    }
}

void callTurboTrigger(int wp)
{
    int turbo = CreateObject("FishSmall", wp);

    SetCallback(turbo, 9, turboTrigger);
    CreateObject("FishSmall", wp);
    Frozen(turbo++, 1);
    Frozen(turbo++, 1);
}

void turboTrigger()
{
    MoveObject(SELF, GetObjectX(SELF), GetObjectY(SELF));
}

void spawnCoff(string name, int wp, int event)
{
    int subsub = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(subsub, 30);
    Damage(subsub, 0, 500, -1);
    SetCallback(subsub, 9, preserveCoff);
    CreateObject(name, 53);
    CreateObject("CaveRocksPebbles", 53);
    LookWithAngle(subsub + 2, event);
    Frozen(subsub + 2, 1);
    FrameTimerWithArg(1, subsub + 1, delayTeleportUnit);
}

void delayTeleportUnit(int unit)
{
    MoveObject(unit, GetObjectX(unit - 1), GetObjectY(unit - 1));
    MoveObject(unit + 1, GetObjectX(unit - 1), GetObjectY(unit - 1));
}

void preserveCoff()
{
    int subsub = GetTrigger() + 2;

    if (IsCaller(subsub))
    {
        MoveObject(subsub, GetObjectX(SELF), GetObjectY(SELF));
        if (!CurrentHealth(subsub - 1))
        {
            MoveWaypoint(22, GetObjectX(SELF), GetObjectY(SELF));
            if (!GetDirection(subsub))
                CreateAnyKey(SELF, 3, "메인 홀 출구 게이트 열쇠");
            else
            {
                if (!Random(0, 3))
                    CreateObject("RedPotion", 22);
                else
                    spawnNormalUnit("skeleton", 22, 295, 64);
            }
            Delete(SELF);
            Delete(subsub);
        }
    }
}

void spawnUnitMovingPower(string name, int wp)
{
    int subsub = CreateObject("FishSmall", wp);

    Damage(subsub, 0, 500, -1);
    ObjectOff(subsub);
    CreateObject("CaveBoulders", wp);
    SetCallback(subsub, 9, natureMoving);
    SetOwner(getMaster(), subsub + 1);
}

void natureMoving()
{
    int subsub = GetTrigger() + 1;

    if (IsCaller(subsub))
    {
        if (!HasEnchant(subsub, "ENCHANT_SHOCK"))
            Enchant(subsub, "ENCHANT_SHOCK", 0.0);
        MoveObject(SELF, GetObjectX(subsub), GetObjectY(subsub));
    }
}

void ClickSlideUnit()
{
    float pos_x = GetObjectX(SELF) - GetObjectX(OTHER), pos_y = GetObjectY(SELF) - GetObjectY(OTHER);
    float dir_x = 0.0, dir_y = 0.0;

    if (pos_x > 0.0)
    {
        dir_x = -23.0;
        if (pos_y > 0.0)
            dir_y = -23.0;
        else if (pos_y < 0.0)
            dir_y = 23.0;
    }
    else if (pos_x < 0.0)
    {
        dir_x = 23.0;
        if (pos_y > 0.0)
            dir_y = -23.0;
        else if (pos_y < 0.0)
            dir_y = 23.0;
    }
    if (dir_x != 0.0 && dir_y != 0.0)
    {
        int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF) - dir_x, GetObjectY(SELF) - dir_y);
        if (IsVisibleTo(unit, SELF))
        {
            PlaySoundAround(unit, 910);
            MoveObject(SELF, GetObjectX(unit), GetObjectY(unit));
            MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
        }
        Delete(unit);
    }
}

string BringStatue(int num)
{
    string name[] = {"MovableStatueVictory4NE", "MovableStatueVictory4NW", "MovableStatueVictory4SE", "MovableStatueVictory4SW"};

    return name[num % 4];
}

int MagicStatues(int num)
{
    int unit[4], i;

    if (num == -1)
    {
        for (i = 0 ; i < 4 ; Nop(i ++))
        {
            unit[i] = CreateObject("BomberGreen", 81);
            Nop(CreateObject(BringStatue(i), 81));
            Damage(unit[i], 0, 200, -1);
            ObjectOff(unit[i]);
            Frozen(unit[i], 1);
            SetDialog(unit[i], "NORMAL", ClickSlideUnit, nullPointer);
            MoveWaypoint(81, GetWaypointX(81) + 23.0, GetWaypointY(81) + 23.0);
        }
        return 0;
    }
    return unit[num];
}

int getMaster()
{
    int master;

    if (!master)
    {
        master = CreateObject("hecubah", 55);
        Frozen(master, 1);
    }
    return master;
}

int checkWaypointLimitLine(int wp)
{
    float pos_x = GetWaypointX(wp);
    float pos_y = GetWaypointY(wp);

    if (pos_x > 100.0 && pos_y > 100.0 && pos_x < 5532.0 && pos_y < 5532.0)
        return 1;
    else
        return 0;
}

int CheckUnitLimitLine(int unit)
{
    float pos_x = GetObjectX(unit);
    float pos_y = GetObjectY(unit);

    if (pos_x > 100.0 && pos_y > 100.0 && pos_x < 5532.0 && pos_y < 5532.0)
        return 1;
    else
        return 0;
}

void CallNumberPack()
{
    createNumber(LocationX(103),LocationY(103),0);
    createNumber(LocationX(104),LocationY(104),1);
    createNumber(LocationX(105),LocationY(105),2);
    createNumber(LocationX(106),LocationY(106),3);
    createNumber(LocationX(107),LocationY(107),4);
    createNumber(LocationX(108),LocationY(108),5);
    createNumber(LocationX(109),LocationY(109),6);
    createNumber(LocationX(110),LocationY(110),7);
    createNumber(LocationX(111),LocationY(111),8);
    createNumber(LocationX(112),LocationY(112),9);
}


int IsPlayerAndMobUnit(int sUnit)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
        return GetMemory(ptr + 0x08) & 0x06;
    return 0;
}

int FindItemAmulet(int unit)
{
    //BraceletofAccuracy
    int inv = GetLastItem(unit);

    while (inv)
    {
        if (GetUnitThingID(inv) == 1186)
            return inv;
        inv = GetPreviousItem(inv);
    }
    return 0;
}

void StrangerChestSet(int chestObj)
{
    ChangeChestHandler(chestObj, StrangerChestCollide);
}

void ContainersInit()
{
    ChangeChestHandler(Object("LotdCont4"), 0);
    ChangeChestHandler(Object("LotdCont3"), 0);
    ChangeChestHandler(Object("DungeonCont1"), 0);
    ChangeChestHandler(Object("DungeonCont2"), 0);
    ChangeChestHandler(Object("DungeonCont3"), 0);
    ChangeChestHandler(Object("DungeonCont4"), 0);
    StrangerChestSet(Object("FightContainer"));
    ChangeChestHandler(Object("NewPartCont1"), 0);
    ChangeChestHandler(Object("LotdCont1"), 0);
    ChangeChestHandler(Object("LotdCont2"), 0);
    ChangeChestHandler(Object("TestCon1"), 0);
    ChangeChestHandler(Object("TestCon2"), 0);
    ChangeChestHandler(Object("TestCon3"), 0);
    ChangeChestHandler(Object("TestCon4"), 0);
    ChangeChestHandler(Object("TestCon5"), 0);
    ChangeChestHandler(Object("TestCon6"), 0);
    ChangeChestHandler(Object("TestCon7"), 0);
    ChangeChestHandler(Object("TestCon8"), 0);
    ChangeChestHandler(Object("SecretReward1"), 0);
    ChangeChestHandler(Object("SecretReward2"), 0);
    ChangeChestHandler(Object("SecretReward3"), 0);
    ChangeChestHandler(Object("SecretReward4"), 0);
    ChangeChestHandler(Object("SecretReward5"), 0);

    ChangeChestHandler(Object("UnderCont1"), 0);
    ChangeChestHandler(Object("UnderCont2"), 0);
    ChangeChestHandler(Object("UnderCont3"), 0);
    ChangeChestHandler(Object("UnderCont4"), 0);
    ChangeChestHandler(Object("UnderCont5"), 0);
    ChangeChestHandler(Object("UnderCont6"), 0);
    ChangeChestHandler(Object("UnderCont7"), 0);
    ChangeChestHandler(Object("UnderCont8"), 0);
    ChangeChestHandler(Object("DunCont1"), 0);
}

void CheckSpecialItem(int weapon)
{
    int thingid = GetUnitThingID(weapon);

    if (thingid >= 222 && thingid <= 225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingid == 1178 || thingid == 1168)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
}

void ContainerKeyInit(string keyName)
{
    CreateObjectAt(keyName, LocationX(267), LocationY(267));
    CreateObjectAt(keyName, LocationX(268), LocationY(268));
    CreateObjectAt(keyName, LocationX(269), LocationY(269));
    CreateObjectAt(keyName, LocationX(270), LocationY(270));
    CreateObjectAt(keyName, LocationX(271), LocationY(271));
    CreateObjectAt(keyName, LocationX(272), LocationY(272));
    CreateObjectAt(keyName, LocationX(273), LocationY(273));
    CreateObjectAt(keyName, LocationX(274), LocationY(274));
    CreateObjectAt(keyName, LocationX(275), LocationY(275));
}

void EndUnitScan(int count) //here
{
    return;
}

void InitNPCScan(int cur)
{
    int count, i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (cur < LastUnitPtr)
        {
            if (GetUnitThingID(cur) == 2673)
            {
                CreateObjectAt("BraceletofAccuracy", GetObjectX(cur), GetObjectY(cur));
                Delete(cur);
                count ++;
            }
            else if (GetUnitThingID(cur) == 2672)
            {
                RewardClassItemCreateAtUnitPos(cur);
                Delete(cur);
                count ++;
            }
            cur += 2;
        }
        else
        {
            EndUnitScan(count);
            return;
        }
    }
    FrameTimerWithArg(1, cur, InitNPCScan);
}

void BeastDelayDropReward(int sUnit)
{
    int durate = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (durate)
        {
            LookWithAngle(sUnit, durate - 1);
            FrameTimerWithArg(1, sUnit, BeastDelayDropReward);
            break;
        }
        RewardClassItemCreateAtUnitPos(sUnit);
        Delete(sUnit);
        break;
    }
}

void BeastDie()
{
    int rewardMark = CreateObjectAt("ImaginaryCaster", GetObjectX(SELF), GetObjectY(SELF));

    DeleteObjectTimer(CreateObjectAt("BreakingSoup", GetObjectX(SELF), GetObjectY(SELF)), 25);
    LookWithAngle(rewardMark, 50);
    FrameTimerWithArg(1, rewardMark, BeastDelayDropReward);
    DeleteObjectTimer(SELF, 90);
}

int SummonBeast(int sUnit)
{
    int mob = CreateObjectAt("WeirdlingBeast", GetObjectX(sUnit), GetObjectY(sUnit));

    WeirdlingBeastSubProcess(mob);
    return mob;
}

void StartBeastSummon(int mobMake)
{
    int durate = GetDirection(mobMake);

    while (IsObjectOn(mobMake))
    {
        if (durate)
        {
            SetCallback(SummonBeast(mobMake), 5, BeastDie);
            LookWithAngle(mobMake, durate - 1);
            FrameTimerWithArg(1, mobMake, StartBeastSummon);
            break;
        }
        Delete(mobMake);
        break;
    }
}

int BeastGroupRespect(float xProfile, float yProfile, int amount)
{
    int mobMake = CreateObjectAt("ImaginaryCaster", xProfile, yProfile);

    LookWithAngle(mobMake, amount);
    FrameTimerWithArg(1, mobMake, StartBeastSummon);
    return mobMake;
}

void OnChestCollideNop(){}

void StrangerChestCollide()
{
    int cFps = GetMemory(0x84ea04);
	int ptr = UnitToPtr(SELF);

	if (ptr && IsPlayerUnit(OTHER))
	{
		if (ABS(cFps - GetMemory(ptr + 0x2e4)) > 30)
		{
			SetMemory(ptr + 0x2e4, cFps);

            int item = BeastGroupRespect(GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 32.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 32.0), 6);
            CreateObjectAt("BraceletofAccuracy", GetObjectX(item), GetObjectY(item));
            SetUnitFlags(SELF, GetUnitFlags(SELF) ^ UNIT_FLAG_DEAD);
            SetUnitCallbackOnCollide(SELF, OnChestCollideNop);
		}
	}
}
void ChestDefaultCollide()
{
    if (GetUnitFlags(SELF) & UNIT_FLAG_DEAD)
        return;

	int cFps = GetMemory(0x84ea04);
	int ptr = UnitToPtr(SELF), key, item;

	if (ptr && IsPlayerUnit(OTHER))
	{
		if (ABS(cFps - GetMemory(ptr + 0x2e4)) > 30)
		{
			SetMemory(ptr + 0x2e4, cFps);
            if (FindItemAmulet(OTHER))
            {
                Delete(FindItemAmulet(OTHER));
                RewardClassItemCreateAt(GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 32.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 32.0));
                SetUnitFlags(SELF, GetUnitFlags(SELF) ^ UNIT_FLAG_DEAD);
                UniPrint(OTHER, "금고열쇠를 사용하여 잠긴 금고를 열었습니다");
            }
            else
            {
                PlaySoundAround(OTHER, 1012);
                UniPrint(OTHER, "이 금고를 열려면 금고열쇠 가 필요합니다");
            }
		}
	}
}

void UnitStatusHealingBuff(int sUnit)
{
    int durate = GetDirection(sUnit), owner = GetOwner(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(owner) && durate)
        {
            FrameTimerWithArg(1, sUnit, UnitStatusHealingBuff);
            if (ToInt(DistanceUnitToUnit(owner, sUnit + 1)))
                MoveObject(sUnit + 1, GetObjectX(owner), GetObjectY(owner));
            LookWithAngle(sUnit, durate - 1);
            RestoreHealth(owner, 1);
            break;
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        break;
    }
}

void ItemUseFastHealing()
{
    int unit;

    if (CurrentHealth(OTHER))
    {
        unit = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER), GetObjectY(OTHER));
        LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)) - 1, 250);
        Enchant(unit + 1, EnchantList(8), 0.0);
        SetOwner(OTHER, unit);
        FrameTimerWithArg(1, unit, UnitStatusHealingBuff);
        Delete(SELF);
        UniPrint(OTHER, "힐링포션을 사용했습니다. 약 8초간 체력 회복속도가 향상됩니다");
    }
}

void SpecialItemDemonBreathWand(int staff)
{
    SetConsumablesWeaponCapacity(staff, 0xc8, 0xc8);
}

void SpecialItemUseHealingPotion(int amlet)
{
    SetUnitCallbackOnUseItem(amlet, ItemUseFastHealing);
}

void SpecialItemUseNop(int item)
{
    return;
}

void ItemUseShock()
{
    if (CurrentHealth(OTHER))
    {
        Enchant(OTHER, EnchantList(22), 70.0);
        while (IsPoisonedUnit(OTHER))
            CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
        RestoreHealth(OTHER, 50);
        Delete(SELF);
    }
}

void ItemUseMeteorShower()
{
    Delete(SELF);

    CastSpellObjectLocation("SPELL_METEOR_SHOWER", OTHER, GetObjectX(OTHER), GetObjectY(OTHER));
    UniChatMessage(OTHER, "뜨거운 불맛을 좀 보아라!", 150);
}

void SpecialItemShock(int amlet)
{
    SetUnitCallbackOnUseItem(amlet, ItemUseShock);
}

void SpecialItemMeteor(int amlet)
{
    SetUnitCallbackOnUseItem(amlet, ItemUseMeteorShower);
}

int ItemDropClassHotPotion(int sUnit)
{
    return CreateObjectAt("RedPotion", GetObjectX(sUnit), GetObjectY(sUnit));
}

int ItemDropClassMagicalPotions(int sUnit)
{
    string potionList[] = {
        "RedPotion", "BluePotion", "CurePoisonPotion", "VampirismPotion",
        "HastePotion", "InvisibilityPotion", "InfravisionPotion", "InvulnerabilityPotion",
        "ShieldPotion", "FireProtectPotion", "ShockProtectPotion", "PoisonProtectPotion"
        };
    return CreateObjectAt(potionList[Random(0, 11)], GetObjectX(sUnit), GetObjectY(sUnit));
}

int ItemDropClassWeapon(int sUnit)
{
    string weaponList[] = {
        "GreatSword", "Longsword", "Sword", "MorningStar",
        "OgreAxe", "StaffWooden", "BattleAxe", "FanChakram",
        "RoundChakram", "WarHammer", "OblivionHalberd", "OblivionWierdling",
        "OblivionHeart"
    };
    int weaponUnit = CreateObjectAt(weaponList[Random(0, 12)], GetObjectX(sUnit), GetObjectY(sUnit));

    CheckSpecialItem(weaponUnit);
    SetWeaponProperties(weaponUnit, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    return weaponUnit;
}

int ItemDropClassArmor(int sUnit)
{
    string armorList[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots",
        "PlateLeggings", "MedievalCloak", "ChainCoif", "ChainLeggings",
        "ChainTunic", "SteelHelm", "LeatherArmbands", "LeatherArmor",
        "LeatherArmoredBoots", "LeatherBoots", "LeatherHelm", "LeatherLeggings",
        "MedievalPants", "MedievalShirt"
    };
    int armorUnit = CreateObjectAt(armorList[Random(0, 17)], GetObjectX(sUnit), GetObjectY(sUnit));

    SetArmorProperties(armorUnit, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return armorUnit;
}

int ItemDropClassSpecial(int sUnit)
{
    string specialList[] = {"BraceletofAccuracy", "Fear", "AmuletofManipulation", "BraceletofHealth", "DemonsBreathWand"};
    int specialActions[] = {
        SpecialItemDemonBreathWand, SpecialItemUseHealingPotion, SpecialItemMeteor, SpecialItemShock,
        SpecialItemUseNop};
    int pic = Random(0, 4);
    int item = CreateObjectAt(specialList[pic], GetObjectX(sUnit), GetObjectY(sUnit));

    CallFunctionWithArg(specialActions[pic], item);
    return item;
}

int RewardClassItemCreateAtUnitPos(int sUnit)
{
    int pic = Random(0, 4);
    int actions[] = {
        ItemDropClassSpecial,
        ItemDropClassArmor,
        ItemDropClassHotPotion,
        ItemDropClassMagicalPotions,
        ItemDropClassWeapon};

    return CallFunctionWithArgInt(actions[pic], sUnit);
}

int RewardClassItemCreateAt(float xProfile, float yProfile)
{
    int posUnit = CreateObjectAt("ImaginaryCaster", xProfile, yProfile);
    int reward = RewardClassItemCreateAtUnitPos(posUnit);

    Delete(posUnit);
    return reward;
}

void ChangeChestHandler(int chestObj, int openFunction)
{
    if (openFunction)
        SetUnitCallbackOnCollide(chestObj, openFunction);
    else
    {
        SetUnitCallbackOnCollide(chestObj, ChestDefaultCollide);
    }
}

void TeleportProcess(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    while (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                if (DistanceUnitToUnit(owner, ToInt(GetObjectZ(ptr))) < 23.0)
                {
                    LookWithAngle(ptr, count - 1);
                    FrameTimerWithArg(1, ptr, TeleportProcess);
                    break;
                }
            }
            else
            {
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                MoveObject(owner, GetObjectX(ptr), GetObjectY(ptr));
                MoveWaypoint(1, GetObjectX(owner), GetObjectY(owner));
                AudioEvent("BlindOff", 1);
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            }
            EnchantOff(owner, "ENCHANT_BURNING");
        }
        Delete(ptr++);
        Delete(ptr++);
        break;
    }
}

void UnitTeleportHandler()
{
    int unit;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(OTHER, "ENCHANT_BURNING"))
        {
            Enchant(OTHER, "ENCHANT_BURNING", 4.0);
            unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(GetTrigger() + 1), GetObjectY(GetTrigger() + 1));
            SetOwner(OTHER, unit);
            Raise(unit, ToFloat(GetTrigger()));
            LookWithAngle(unit, 48);
            CreateObjectAt("VortexSource", GetObjectX(SELF), GetObjectY(SELF));
            GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
            FrameTimerWithArg(1, unit, TeleportProcess);
            UniPrint(OTHER, "공간이동을 준비중입니다, 취소하려면 이곳에서 벗어나세요");
        }
    }
}

int TeleportPortal(int srcLocation, int destLocation)
{
    int unit = CreateObject("WeirdlingBeast", srcLocation);

    SetUnitMaxHealth(CreateObject("InvisibleLightBlueLow", destLocation) - 1, 30);
    UnitNoCollide(CreateObjectAt("TeleportWake", GetObjectX(unit), GetObjectY(unit)));
    Frozen(unit + 2, 1);
    SetCallback(unit, 9, UnitTeleportHandler);
    Damage(unit, 0, 999, -1);

    return unit;
}

void PutMagicSpellIcons()
{
    int i;

    ObjectOff(SELF);
    for (i = 0 ; i < 4 ; i ++)
        DrawMagicIcon( GetWaypointX(118 + i) - 11.0, GetWaypointY(118 + i) - 11.0, OBJ_SPELL_ICONS);
}

void PutStoneTrapPassTeleport()
{
    int i;

    ObjectOff(SELF);
    TeleportPortal(159, 160);
    for (i = 0 ; i < 4 ; i ++)
        WallOpen(Wall(151 + i, 175 - i));
    UniPrint(OTHER, "스톤필드 함정을 패스할 수 있는 텔레포트 실 비밀벽이 열렸습니다");
}

void PutYellowPotion(int LocationNum)
{
    if (LocationNum <= 262)
    {
        if (Random(0, 3))
            CreateObject("RedPotion", LocationNum);
        else
            PotionExCreateYellowPotion(LocationX(LocationNum), LocationY(LocationNum),125);
        FrameTimerWithArg(1, LocationNum + 1, PutYellowPotion);
    }
}

string ItemClassKeyType(int typeNumb)
{
    string keys[] = {"SapphireKey", "SilverKey", "GoldKey", "RubyKey"};

    return keys[typeNumb % 4];
}

void ItemClassKeyDropFunction()
{
    int owner = GetOwner(SELF), keyType = GetDirection(GetTrigger() + 1), keyDesc = ToInt(GetObjectZ(GetTrigger() + 1));
    int kDir = GetDirection(SELF);

    Delete(SELF);
    Delete(GetTrigger() + 1);
    int key = CreateAnyKey(owner, keyType, ToStr(keyDesc));
    UniChatMessage(key, ToStr(keyDesc), 150);
    Raise(key, 100.0);
    LookWithAngle(key, kDir);
    PlaySoundAround(key, 821);
}

int CreateAnyKey(int sUnit, int keyType, string keyDescFunc)
{
    int sKey = CreateObjectAt(ItemClassKeyType(keyType), GetObjectX(sUnit), GetObjectY(sUnit));

    LookWithAngle(CreateObjectAt("ImaginaryCaster", GetObjectX(sKey), GetObjectY(sKey)), keyType);
    Raise(sKey + 1, keyDescFunc);
    SetUnitCallbackOnDiscard(sKey, ItemClassKeyDropFunction);
    return sKey;
}

int CreateAnyKeyAtLocation(int sLocation, int keyType, string keyDescFunc)
{
    int sKey = CreateObjectAt(ItemClassKeyType(keyType), LocationX(sLocation), LocationY(sLocation));

    LookWithAngle(CreateObjectAt("ImaginaryCaster", GetObjectX(sKey), GetObjectY(sKey)), keyType);
    Raise(sKey + 1, keyDescFunc);
    SetUnitCallbackOnDiscard(sKey, ItemClassKeyDropFunction);
    return sKey;
}

void MapExit()
{
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}

void HealingTarget(int ptr)
{
    int unit = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(unit) && count)
    {
        MoveObject(ptr, GetObjectX(unit), GetObjectY(unit));
        if (count % 2)
            RestoreHealth(unit, 1);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, HealingTarget);
    }
    else if (IsObjectOn(ptr))
        Delete(ptr);
}

void CureBeaconTouched()
{
    int ptr;

    if (CurrentHealth(OTHER) && !HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC"))
    {
        Enchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC", 5.0);
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("LongBellsDown", 1);
        ptr = CreateObject("InvisibleLightBlueLow", 1);
        Enchant(ptr, "ENCHANT_RUN", 0.0);
        Enchant(ptr, "ENCHANT_SHIELD", 0.0);
        SetOwner(OTHER, ptr);
        LookWithAngle(ptr, 150);
        FrameTimerWithArg(1, ptr, HealingTarget);
    }
}

int PlaceFastHealingBeacon(int destLocation)
{
    int unit = CreateObjectAt("WeirdlingBeast", LocationX(destLocation), LocationY(destLocation));

    Enchant(CreateObjectAt("InvisibleLightBlueLow", LocationX(destLocation), LocationY(destLocation)), EnchantList(14), 0.0);
    SetCallback(unit, 9, CureBeaconTouched);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    UnitNoCollide(CreateObjectAt("MovableStatue1b", GetObjectX(unit), GetObjectY(unit)));

    return unit;
}

void BackBlockMovement()
{
    Move(Object("BlockTrap1"), 375);
    Move(Object("BlockTrap2"), 377);
}

void StartBlockMovement()
{
    ObjectOff(SELF);
    Move(Object("BlockTrap1"), 371);
    Move(Object("BlockTrap2"), 373);
    FrameTimer(120, BackBlockMovement);
    FrameTimerWithArg(180, GetTrigger(), EnableObject);
}

void OpenSurpriseWalls()
{
    ObjectOff(SELF);

    RemoveWallsFromLocation(370, 8, -23.0, -23.0);
    MobClassFastedZombie(365);
    MobClassFastedZombie(369);
    MobClassSkeletonLord(366);
    MobClassSkeletonLord(368);
    MobClassEmberDemon(367);
    FrameTimer(1, PlaceOrbHecubahInit);
}

void OpenSurpriseWalls2()
{
    ObjectOff(SELF);

    RemoveWallsFromLocation(389, 5, 23.0, -23.0);
    RemoveWallsFromLocation(390, 3, 23.0, 23.0);
    MobClassEmberDemon(387);
    MobClassFireSprite(387);
    MobClassFireSprite(387);
    MobClassFastedZombie(388);
    MobClassFastedZombie(388);
    MobClassFastedZombie(388);
    MobClassGargoyle(388);
}

void OrbHecubahDie()
{
    WallGroupOpen(3);
    FrameTimer(30, OpenSurpriseWalls2);
}

void PlaceOrbHecubahInit()
{
    int mob = MobClassOrbHecubah(393);

    SetUnitStatus(mob, GetUnitStatus(mob) ^ 0x40);
    SetCallback(mob, 5, OrbHecubahDie);
}

void SecretWallsClear1()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(391, 1, 0.0, 0.0);
}

void SecretWallsClear2()
{
    ObjectOff(SELF);
    RemoveWallsFromLocation(392, 4, -23.0, 23.0);
}

void TestFireTrap()
{
    ObjectOff(SELF);
    HookFireballTraps(Object("TestFTrap"), 696);
}

void IxSecretZoneInit()
{
    int chkSum;

    ObjectOff(SELF);
    if (!chkSum)
    {
        chkSum = MobClassBlackSpider(429);
        MobClassNormalZombie(430);
        spawnFlyingShooter(431, 200);
        spawnFlyingShooter(427, 200);
        MobClassNormalZombie(432);
        ChangeChestHandler(Object("OgreCont1"), 0);
        ChangeChestHandler(Object("OgreCont2"), 0);
        ChangeChestHandler(Object("OgreCont3"), 0);
    }
}

void LavaTelpo()
{
    MoveObject(OTHER, LocationX(442), LocationY(442));
}
static void OnPlayerEntryMap(int pInfo){
    int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
    {
        pUnit = GetMemory(ptr+0x2c);
        int pIndex = GetMemory(pInfo+0x810);
        PlayerClassOnInit   (pIndex, pUnit);
        ShowQuestIntroOne(pUnit, 237, "CopyrightScreen", "Wolchat.c:PleaseWait");
    }
    
    if (pInfo==0x653a7c)
    {
        PlayerClassCommonWhenEntry();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

