
#include "libs/define.h"
#include "libs/unitutil.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/memutil.h"

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        /*
        push ebp
        mov ebp,esp
        push [ebp+0C]
        push [ebp+08]
        push 00000001
        mov eax,game.exe+107310
        call eax
        add esp,0C
        pop ebp
        ret 
        */
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        /*
        mov eax,game.exe+107250
        call eax
        push [eax]
        push [eax+04]
        push [eax+08]
        push [eax+0C]
        mov eax,game.exe+117F90
        call eax
        add esp,10
        xor eax,eax
        ret 
        */
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

void NPCRemoveInventory(int sUnit)
{
    int iCount = GetUnit1C(sUnit);
    int cur = sUnit + 2, i;

    for (i = 0 ; i < iCount ; i ++)
        Delete(cur + (i * 2));
}

void NPCSetInventoryCount(int sUnit)
{
    int *ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        int cur = GetLastItem(sUnit), count=0;
        while (cur)
        {
            // if (GetUnitClass(cur)&UNIT_CLASS_ARMOR)
            //     NpcArmorProperty(cur);
            Enchant(cur,"ENCHANT_INVULNERABLE", 0.0);
            count ++;
            cur = GetPreviousItem(cur);
        }
        ptr[7]=count;
    }
}

void HookFireballTraps(int trapUnit, short missileTy)
{
    int ptr = UnitToPtr(trapUnit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x0c, missileTy);
}

void WallCoorToUnitCoor(int wall, float *destXy)
{
    int x=wall>>0x10, y=wall&0xff;

    destXy[0]=IntToFloat((x*23) + 11);
    destXy[1]=IntToFloat((y*23)+11);
}
