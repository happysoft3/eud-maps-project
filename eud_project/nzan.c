
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/coopteam.h"
#include "libs/itemproperty.h"
#include "libs/waypoint.h"
#include "libs/weaponcapacity.h"
#include "libs/playerupdate.h"
#include "libs/fixtellstory.h"
#include "libs/mathlab.h"
#include "libs/printutil.h"
#include "libs/buff.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/spellutil.h"
#include "libs/username.h"
#include "libs/potionpickup.h"
#include "libs/monsteraction.h"
#include "libs/potionex.h"
#include "libs/logging.h"

int m_LastUnitGlobalID;
int m_LastUnitID = 2700;
int player[20];

int *m_itemCreateExecTable;

void InitItemTable()
{
    int table[] = {SpawnGold, CreateRandomWeapon, CreateRandomArmor, MagicalPotionCreate, HotPotionCreate,
        GermCreate, MagicalItemCreate, ChestKeyDrop};

    m_itemCreateExecTable=&table;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);
    PotionPickupRegist(x);

    return x;
}

int GhostBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1936681031; arr[1] = 116;
        arr[17] = 40; arr[18] = 20; arr[19] = 60; 
		arr[21] = 1065353216;arr[24] = 1066192077; 
		arr[27] = 1; arr[28] = 1092616192; arr[29] = 10; 
		arr[31] = 4; arr[32] = 1; arr[33] = 3;
		arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		 arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801;
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[17] = 520; arr[18] = 100; 
		arr[19] = 70; arr[21] = 1065353216; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1112014848; 
		arr[29] = 80; arr[32] = 13; arr[33] = 21; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void WizardRedSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074161254);
		SetMemory(ptr + 0x224, 1074161254);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 520);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 520);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1852796743;
		arr[17] = 85; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = &arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
        arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
        arr[53] = 1128792064; 
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = &arr;
	}
	return link;
}

void UseMapSetting()
{
    SetMemory(0x5d5330, 0x2000);
    SetMemory(0x5d5394, 1);
}

int DummyUnitCreate(string name, float locX, float locY)
{
    int unit = CreateObjectAt(name, locX, locY);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    ObjectOff(unit);
    Frozen(unit, 1);
    return unit;
}

void SpecialUnitSearchEnd(int allCount)
{
    UniPrintToAll("모두 " + IntToString(allCount) + "개의 리워드 마커를 처리 완료했습니다");
}

void SpecialUnitRewardMarker(int cur)
{
    MoveWaypoint(1, GetObjectX(cur), GetObjectY(cur));
    Delete(cur);
    CallFunctionWithArgInt(m_itemCreateExecTable[Random(0, 6)], 1);
}

void SpecialUnitSearching(int cur)
{
    int count, i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (cur < m_LastUnitGlobalID)
        {
            if (GetUnitThingID(cur) == 2672)
            {
                SpecialUnitRewardMarker(cur);
                count ++;
            }
            cur += 2;
        }
        else
        {
            SpecialUnitSearchEnd(count);
            return;
        }
    }
    FrameTimerWithArg(1, cur, SpecialUnitSearching);
}

void StartSpecialUnitSearch(int firstUnit)
{
    if (IsObjectOn(firstUnit))
        SpecialUnitSearching(firstUnit + 2);
}

void MapInitialize()
{
    MusicEvent();
    m_LastUnitGlobalID = MasterUnit();    
    MapPotionPlace("RedPotion");
    InitJailSwitchs();
    RedMagicItem();
    BlueMagicItem();
    OrbGreen();
    InitItemTable();
    FrameTimerWithArg(25, Object("StopElevator"), DelayDisableUnit);
    FrameTimerWithArg(25, Object("OgreElev"), DelayDisableUnit);
    FrameTimerWithArg(35, Object("MinesNewElev"), DelayDisableUnit);
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(20, MapStandBy);
    CreateLogFile("nzan_log.txt");
}

void MapStandBy()
{
    MinesBook(44);
    UseMapSetting();
    Sentry();
    OgreKey();
    PutBones(10, 23);
    SetStartSwitch(1);
    GridKey();
    SpawnSpellbooks();
    FrameTimer(1, PutMapDecorations);
    FrameTimerWithArg(1, Object("FirstSearchUnit"), StartSpecialUnitSearch);
    FrameTimer(2, PlaceArea1Monsters);
    FrameTimer(10, LoopPreservePlayers);
    FrameTimer(20, InitGirls);
    SecondTimerWithArg(3, 0, HowToPlay);
}

string GameGuide(int num)
{
    string table[] = {
        "맵 이름: 11명의 여자구출작전",
        "당신의 임무: 이 마을 곳곳에 11명의 여자들이 인질로 잡혀있습니다, 당신은 이 여자들을 모두 구출해 내야합니다",
        "여자를 발견했다면 클릭(말걸기)하여 자신을 따라오게 한 후 필드 시작지점 출입문 안으로 데려오면 구출된 것으로 간주됩니다",
        "캐릭터의 부활터에 있는 두 여성은 단순한 NPC가 아닌 상인입니다, 클릭하여 상점을 이용할 수 있습니다",
        "게임 팁_ 조심스럽게 걷기를 시전하면 전방에 나무구조물을 던져 적에게 피해를 줄 수 있습니다",
        "게임 팁_ 필드 곳곳에 랜덤한 아이템들이 널부러져 있습니다, 가끔은 골드가 있을 수도 있고 최고급 옵션의 무기나 갑옷이 있을 수도 있습니다",
        "게임 팁_ 미션완수도 중요하지만 조금만 더 여유를 가지시고 맵 구석구석을 잘 조사하신다면 좋은 아이템을 얻으실 수 있을것입니다",
        "게임 팁_ 독저항 물약을 사용하면 부활터로 이동됩니다, 귀찮음을 방지하기 위해서라도 독저항 물약을 많이 모아두시기 바랍니다",
        "게임 팁_ 이곳에서의 우물은 일정시간동안 당신의 체력을 서서히 회복시켜줍니다, 우물을 버프삼아 전투를 한다면 많은 도움이 될 것입니다",
        "게임 팁_ 이 맵에는 3개의 보스 몬스터가 존재합니다, 참고하시기 바랍니다",
        "게임 팁_ 이 맵에서는 캐릭터가 격추되어도 아이템을 잃지 않습니다, 그렇지만 원활한 기능동작을 위해 무기 갑옷되살리기 기능을 끄십시오"};
    return table[num];
}

void HowToPlay(int num)
{
    if (num < 10)
    {
        UniPrintToAll(GameGuide(num));
        SecondTimerWithArg(5, num + 1, HowToPlay);
    }
}

void UserMapSign()
{
    RegistSignMessage(Object("UniSign1"), "마나광산 입구: 이곳에 들어가려면 출입증이 필요하다");
    RegistSignMessage(Object("UniSign2"), "지하 채굴장 출입구");
    RegistSignMessage(Object("UniSign3"), "이 우물을 클릭하면 잠시동안 체력 회복속도가 향상됩니다");
    RegistSignMessage(Object("UniSign4"), "11명의 여자 인질 구출 대작전!");
    RegistSignMessage(Object("UniSign5"), "육군 교도소- 붉은 마법사에게서 열쇠를 얻어낼 수 있을것이다");
    RegistSignMessage(Object("UniSign6"), "마을 회장님의 저택입니다. 캡스 경비구역이기도 하니 관계자 외 출입을 삼가하십시오");
    RegistSignMessage(Object("UniSign7"), "쥐불놀이라고 들어봤니?");
    RegistSignMessage(Object("UniSign8"), "커닝시티 워닝 스트리트 삼거리");
    RegistSignMessage(Object("UniSign9"), "정말로 필드로 나가실거에요? 거긴 위험해요!");
    RegistSignMessage(Object("UniSign10"), "지도 곳곳에 흩어진 11명의 행방불명 실종 여고생들을 구출하라!");
    RegistSignMessage(Object("UniSign11"), "바이올런스 가");
    RegistSignMessage(Object("UniSign12"), "이 아래는 지하 창고 입니다");
    RegistSignMessage(Object("UniSign13"), "바이올런스 가: 어둡고 음침한 도시지역이다");
    RegistSignMessage(Object("UniSign14"), "자신이 힘좀 쎄다고 생각하면 걸어올라 와봐 by. 오우거 총장");
    RegistSignMessage(Object("UniSign15"), "오우거 밀집 거주지역");
    RegistSignMessage(Object("UniSign16"), "노예 수용시설");
    RegistSignMessage(Object("UniSign17"), "건물 옥상 축구경기장");
}

void PutMapDecorations()
{
    int unit;
    
    FrameTimer(2, MovingTestStart);
    FrameTimer(1, UserMapSign);
    Frozen(CreateObject("DunMirScaleTorch1", 59), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 60), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 61), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 62), 1);
    unit = CreateObject("Maiden", 68);
    CreateObject("Maiden", 69);
    CreateObject("Maiden", 71);
    CreateObject("Maiden", 123);
    CreateObject("Maiden", 53);
    CreateObject("Maiden", 52);
    CreateObject("Maiden", 169);
    Frozen(unit, 1);
    Frozen(unit + 1, 1);
    Frozen(unit + 2, 1);
    Frozen(unit + 3, 1);
    Frozen(unit + 4, 1);
    LookWithAngle(unit + 4, 0);
    Frozen(unit + 5, 1);
    LookWithAngle(unit + 5, 1);
    Frozen(unit + 6, 1);
    SetCallback(unit, 9, OpenWoodGate);
    SetCallback(unit + 1, 9, OpenHiddenGate);
    SetCallback(unit + 2, 9, OpenUnderGate);
    SetCallback(unit + 3, 9, OpenOgreDoors);
    SetCallback(unit + 4, 9, FieldTeleport);
    SetCallback(unit + 5, 9, FieldTeleport);
    SetCallback(unit + 6, 9, OpenUndergroundGate);
    SpawnWaspNest(118);
    SpawnWaspNest(119);
    SpawnWaspNest(120);
    SpawnWaspNest(121);
    Frozen(CreateObject("Candleabra5", 136), 1);
    Frozen(CreateObject("Candleabra5", 137), 1);
    Frozen(CreateObject("Candleabra5", 138), 1);
    FrameTimer(1, Dec2);
    FrameTimerWithArg(2, 0, AllChestInit);
    FrameTimerWithArg(2, 1, AllChestInit);
    FrameTimerWithArg(2, 2, AllChestInit);
    FrameTimerWithArg(3, Object("OgrePartChest"), GoOgrePartBox);
    HiddenMarketChest("HiddenChest", 1);
}

void Dec2()
{
    CreateObject("BlackPowderBarrel", 222);
    CreateObject("BlackPowderBarrel", 223);
    CreateObject("BlackPowderBarrel2", 223);
    Frozen(CreateObject("BoulderCave", 224), 1);
    Frozen(CreateObject("BoulderCave", 227), 1);
    CreateObject("Boulder", 225);
    SpawnBlackSpider(225);
}

void PlaceArea1Monsters()
{
    int ptr;
    SpawnWolf(24); SpawnWolf(25); SpawnMecaFly(23); SpawnMecaFly(26);

    ptr = SpawnRedWizard(39);
    SetCallback(ptr, 3, JailKeeperTalk);
    SetCallback(ptr, 5, DeadJailRedWizard);
    SpawnBlackSpider(2);
    SpawnBlackSpider(2);
    SpawnBlackSpider(2);
    SpawnBear(155);
    SpawnArcher(156);
    SpawnArcher(157);
    SpawnArcher(158);
    SpawnArcher(159);
    SpawnMecaFly(160);
    SpawnBlackSpider(36);
    SpawnCreature("Shade", 29, 180);
    SpawnCreature("Shade", 29, 180);
    SpawnCreature("Shade", 29, 180);
    SpawnArcher(161);
    SpawnArcher(162);
    SpawnArcher(163);
    SpawnArcher(164);
    SpawnSwordsman(165);
    SpawnSwordsman(165);
    SpawnSwordsman(165);
    SpawnSwordsman(18);
    SpawnCreature("AlbinoSpider", 15, 130);
    SpawnCreature("AlbinoSpider", 15, 130);
    SpawnBlackSpider(16);
    SpawnCreature("SpittingSpider", 30, 140);
    SpawnCreature("SpittingSpider", 30, 140);
    SpawnCreature("SpittingSpider", 30, 140);
    SpawnMecaFly(34);
    SpawnMecaFly(34);
}

void PlaceArea2Monsters()
{
    int unit = SpawnWisp(178);
    SetCallback(unit, 5, Area2WispDead);

    SpawnSwordsman(179);
    SpawnArcher(179);
    SpawnSwordsman(179);
    SpawnBlackSpider(171);
    SpawnMystic(171);
    SpawnCreature("BlackWolf", 172, 225);
    SpawnCreature("BlackWolf", 172, 225);
    SpawnCreature("BlackWolf", 172, 225);
    SpawnMecaFly(177);
    SpawnMecaFly(177);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnSwordsman(178);
    SpawnGhost(174);
    SpawnGhost(173);
    SpawnGhost(176);
    SpawnBlackSpider(175);
    SpawnBear(175);
}

void PlaceArea21Monsters()
{
    SpawnZombie(180);
    SpawnSkeletonLord(180);
    SpawnZombie(181);
    SpawnSkeletonLord(181);
    SpawnSkeleton(182);
    SpawnSkeletonLord(182);
    SpawnSkeleton(183);
    SpawnSkeletonLord(183);
    SpawnGhost(184);
    SpawnGhost(184);
    SpawnMystic(184);
    SpawnCreature("EvilCherub", 185, 64);
    SpawnCreature("EvilCherub", 185, 64);
    SpawnMecaFly(185);
    SpawnCreature("EvilCherub", 186, 64);
    SpawnCreature("EvilCherub", 186, 64);
    SpawnArcher(186);
    SpawnGhost(187);
    SpawnSkeleton(187);
    SpawnZombie(42);
}

void PlaceOgreMonsters()
{
    ObjectOff(SELF);
    SpawnWisp(201);
    SpawnWisp(202);
    SpawnOgress(189);
    SpawnOgress(189);
    SpawnScorpion(10);
    SpawnOgress(10);
    SpawnOgre(190);
    SpawnOgre(191);
    SpawnOgress(192);
    SpawnOgre(122);
    SpawnScorpion(122);
    SpawnBear(193);
    SpawnOgre(194);
    SpawnOgre(195);
    SpawnOgress(195);
    SpawnOgre(196);
    SpawnScorpion(196);
    SpawnOgre(197);
    SpawnOgress(198);
    SpawnOgress(199);
    SpawnBear(200);
    SpawnOgre(203);
    SpawnSkeletonLord(203);
    SpawnOgre(204);
    SpawnSkeletonLord(204);
    FrameTimerWithArg(1, 188, PutSwampUrchins);
}

void LastPartMonsters()
{
    int unit = CreateObject("Maiden", 414);
    Frozen(unit, 1);
    SetCallback(unit, 9, OpenLastArchDoors);
    SpawnSkeletonLord(408);
    SpawnSkeletonLord(408);
    SpawnSkeletonLord(408);
    SpawnImp(409);
    SpawnImp(409);
    SpawnMystic(410);
    SpawnMystic(410);
    SpawnSwordsman(410);
    SpawnGoon(411);
    SpawnGoon(411);
    SpawnGoon(412);
    SpawnGoon(412);
    SpawnImp(412);
    SpawnImp(415);
    SpawnImp(415);
    SpawnImp(415);
    SpawnImp(416);
    SpawnImp(416);
    SpawnImp(416);
    SpawnSwordsman(417);
    SpawnSwordsman(417);
    SpawnImp(417);
    SpawnSwordsman(418);
    SpawnSwordsman(418);
    SpawnSwordsman(418);
    SpawnSwordsman(419);
    SpawnSkeletonLord(419);
    SpawnGoon(419);
    FrameTimer(3, LastPartMonsters2);
}

void LastPartMonsters2()
{
    SpawnCreature("EmberDemon", 420, 96);
    SpawnImp(420);
    SpawnImp(420);
    SpawnZombie(421);
    SpawnZombie(421);
    SpawnWolf(421);
    SpawnImp(422);
    SpawnArcher(422);
    SpawnSwordsman(422);
    SpawnGoon(423);
    SpawnGoon(423);
    SpawnScorpion(423);
    SpawnMystic(424);
    SpawnImp(425);
    SpawnImp(425);
    SpawnMystic(426);
    SpawnImp(427);
    SpawnImp(427);
}

int MasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 150);
        Frozen(unit, 1);
    }
    return unit;
}

void DeadJailRedWizard()
{
    int ptr;
    MoveObject(RedMagicItem(), GetObjectX(SELF), GetObjectY(SELF));
    ptr = SpawnBlackSpider(38);
    SpawnBlackSpider(64);
    SpawnBlackSpider(65);
    SpawnBlackSpider(66);
    SpawnBlackSpider(67);
    LookWithAngle(ptr, 96);
    LookWithAngle(ptr+1, 96);
    LookWithAngle(ptr+2, 96);
    LookWithAngle(ptr+3, 96);
    LookWithAngle(ptr+4, 96);
}

void RedWizardMeleeAttack()
{
    int victim = GetVictimUnit();

    if (CurrentHealth(victim) && IsVisibleTo(SELF, victim))
    {
        MonsterForceCastSpell(SELF, 0, GetObjectX(SELF) + UnitRatioX(victim, SELF, 30.0), GetObjectY(SELF) + UnitRatioY(victim, SELF, 30.0));        
        Damage(victim, SELF, 30, 16);
    }
}

void NothingTrigger()
{
    return;
}

void JailKeeperTalk()
{
    SetCallback(SELF, 3, NothingTrigger);
    PlaySoundAround(SELF, 614);
    UniChatMessage(SELF, "아니 이봐, 당신은 이곳에 들어올 수 없어!\n저놈을 당장 죽여!", 150);
}

int SpawnCreature(string name, int wp, int hp)
{
    int unit = CreateObject(name, wp);

    SetUnitMaxHealth(unit, hp);
    SetCallback(unit, 5, FieldMonsterDeath);
    return unit;
}

int SpawnRedWizard(int wp)
{
    int id = CreateObject("WizardRed", wp);

    WizardRedSubProcess(id);
    RegistryUnitStrikeFunction(id, RedWizardMeleeAttack);
    //SetCallback(id, 3, RWizWeapon);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 5, FieldMonsterDeath);
    //SetCallback(id, 8, FleeEvent);
    //SetCallback(id, 13, FleeEvent);
    return id;
}

int SpawnBlackSpider(int wp)
{
    int id = CreateObject("BlackWidow", wp);

    UnitLinkBinScript(id, BlackWidowBinTable());
    SetUnitVoice(id, 29);
    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetUnitMaxHealth(id, 250);
    SetCallback(id, 5, BlackSpiderDeath);
    SetOwner(MasterUnit(), id);

    return id;
}

static void deferredMimicAction(int mon)
{
    int act = MonsterActionPush(mon, 3);

    if (act)
	{
		int ptr = UnitToPtr(mon);
		
        SetMemory(act + 4, 0);
        SetMemory(act + 8, 0);
        SetMemory(act + 12, ptr);
	}
    AggressionLevel(mon, 1.0);
}

int SpawnMimic(int wp)
{
    int mon=CreateObject("Mimic", wp);

    FrameTimerWithArg(1, mon, deferredMimicAction);
    SetUnitMaxHealth(mon, 540);
    SetUnitScanRange(mon, 450.0);
    SetCallback(mon, 5, FieldMonsterDeath);
    RetreatLevel(mon, 0.0);
    ResumeLevel(mon, 0.0);
    return mon;
}

int SpawnMecaFly(int wp)
{
    int unit = CreateObject("FlyingGolem", wp);

    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 450.0);
    SetUnitMaxHealth(unit, 130);
    SetCallback(unit, 3, MecaFlyWeapon);
    SetCallback(unit, 5, FieldMonsterDeath);
    SetOwner(MasterUnit(), unit);

    return unit;
}

int SpawnUrchinShaman(int wp)
{
    int unit = CreateObject("UrchinShaman", wp);

    SetUnitMaxHealth(unit, 170);
    SetOwner(MasterUnit(), unit);
    SetCallback(unit, 5, FieldMonsterDeath);
    SetCallback(unit, 8, FleeEvent);
    SetCallback(unit, 13, FleeEvent);
    return unit;
}

int SpawnMystic(int wp)
{
    int id = CreateObject("Wizard", wp);

    SetUnitMaxHealth(id, 225);
    Enchant(id, "ENCHANT_ANCHORED", 0.0);
    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 5, FieldMonsterDeath);
    SetCallback(id, 8, FleeEvent);
    SetCallback(id, 13, FleeEvent);

    return id;
}

int SpawnSwordsman(int wp)
{
    int id = CreateObject("Swordsman", wp);

    SetUnitMaxHealth(id, 310);
    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetCallback(id, 3, TheifWeapon);
    SetCallback(id, 5, FieldMonsterDeath);
    SetOwner(MasterUnit(), id);
    RetreatLevel(id, 0.0);
    ResumeLevel(id, 1.0);
    AggressionLevel(id, 1.0);

    return id;
}

int SpawnArcher(int wp)
{
    int id = CreateObject("Archer", wp);

    SetUnitMaxHealth(id, 98);
    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 5, FieldMonsterDeath);
    RetreatLevel(id, 0.0);
    ResumeLevel(id, 1.0);
    AggressionLevel(id, 1.0);
    return id;
}

int SpawnGhost(int wp)
{
    int id = CreateObject("Ghost", wp);

    UnitLinkBinScript(id, GhostBinTable());
    Enchant(id, "ENCHANT_HASTED", 0.0);
    Enchant(id, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
    Enchant(id, "ENCHANT_VAMPIRISM", 0.0);
    Enchant(id, "ENCHANT_INFRAVISION", 0.0);
    SetUnitMaxHealth(id, 64);
    SetCallback(id, 5, FieldMonsterDeath);
    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetOwner(MasterUnit(), id);
    RetreatLevel(id, 0.0);
    AggressionLevel(id, 1.0);

    return id;
}

int SpawnWisp(int wp)
{
    int id = CreateObject("WillOWisp", wp);

    SetUnitMaxHealth(id, 295);
    SetOwner(MasterUnit(), id);
    UnitZeroFleeRange(id);
    SetCallback(id, 5, FieldMonsterDeath);
    SetCallback(id, 8, FleeEvent);
    SetCallback(id, 13, FleeEvent);

    return id;
}

int SpawnWolf(int wp)
{
    int id = CreateObject("WhiteWolf", wp);

    SetUnitMaxHealth(id, 130);
    SetCallback(id, 5, FieldMonsterDeath);
    SetOwner(MasterUnit(), id);
    return id;
}

int SpawnBear(int wp)
{
    int id = CreateObject("Bear", wp);

    SetUnitMaxHealth(id, 325);
    SetCallback(id, 5, FieldMonsterDeath);
    SetOwner(MasterUnit(), id);
    RetreatLevel(id, 0.0);
    return id;
}

int SpawnSpirit(int wp)
{
    int id = CreateObject("FireSprite", wp);

    UnitLinkBinScript(id, FireSpriteBinTable());
    
    SetUnitMaxHealth(id, 128);
    SetUnitStatus(id, GetUnitStatus(id) ^ 0x10000);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnZombie(int wp)
{
    int id = CreateObject("VileZombie", wp);
    int ptr = GetMemory(0x750710);

    SetUnitMaxHealth(id, 306);
    SetMemory(ptr + 0x224, ToInt(1.5));
    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 5, DeadZombieEvent);
    SetCallback(id, 3, ZombieStrike);

    return id;
}

int SpawnSkeletonLord(int wp)
{
    int id = CreateObject("SkeletonLord", wp);

    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetUnitMaxHealth(id, 300);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 3, SkullLordStrike);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnSkeleton(int wp)
{
    int id = CreateObject("Skeleton", wp);

    CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 450.0);
    SetUnitMaxHealth(id, 225);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnScorpion(int wp)
{
    int id = CreateObject("Scorpion", wp);

    SetUnitMaxHealth(id, 225);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 3, ReleasePoison);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnOgress(int wp)
{
    int id = CreateObject("GruntAxe", wp);
    int ptr = GetMemory(0x750710);

    SetUnitMaxHealth(id, 225);
    SetOwner(MasterUnit(), id);
    SetMemory(ptr + 0xc, GetMemory(ptr + 0xc) ^ 0x200);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnOgre(int wp)
{
    int id = CreateObject("OgreBrute", wp);
    int ptr = GetMemory(0x750710);

    SetUnitMaxHealth(id, 280);
    SetOwner(MasterUnit(), id);
    SetMemory(ptr + 0xc, GetMemory(ptr + 0xc) ^ 0x200);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnGoon(int wp)
{
    int id = CreateObject("Goon", wp);
    int ptr = GetMemory(0x750710);

    UnitLinkBinScript(id, GoonBinTable());
    SetUnitVoice(id, 63);
    SetUnitMaxHealth(id, 128);
    SetOwner(MasterUnit(), id);
    SetMemory(ptr + 0xc, GetMemory(ptr + 0xc) ^ 0x200);
    SetCallback(id, 3, GoonAttack);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnImp(int wp)
{
    int id = CreateObject("Imp", wp);

    SetUnitMaxHealth(id, 96);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 3, ImpStrike);
    SetCallback(id, 5, FieldMonsterDeath);

    return id;
}

int SpawnBeast(int wp)
{
    int id = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(id, 130);
    UnitLinkBinScript(id, WeirdlingBeastBinTable());
    UnitZeroFleeRange(id);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 5, FieldMonsterDeath);
    
    return id;
}

int SpawnDryad(int wp)
{
    int id = CreateObject("WizardGreen", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
    CreateObject("InvisibleLightBlueHigh", wp);

    Enchant(id, "ENCHANT_ANCHORED", 0.0);
    SetUnitMaxHealth(id, 192);
    SetOwner(MasterUnit(), id);
    SetCallback(id, 3, DryadStrike);
    SetCallback(id, 5, DryadDeath);

    return id;
}

void RWizWeapon()
{
    if (CurrentHealth(OTHER))
    {
        Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
        Damage(OTHER, SELF, 30, 16);
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(30, GetTrigger(), ResetUnitSight);
        }
    }
}

void MecaFlyWeapon()
{
    int mis;
    if (CurrentHealth(OTHER))
    {
        MoveWaypoint(1, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 19.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 19.0));
        mis = CreateObject("CherubArrow", 1);
        SetOwner(SELF, mis);
        LookAtObject(mis, OTHER);
        PushObject(mis, -45.0, GetObjectX(OTHER), GetObjectY(OTHER));
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(25, GetTrigger(), ResetUnitSight);
        }
    }
}

void TheifWeapon()
{
    int mis;

    if (CurrentHealth(OTHER))
    {
        MoveWaypoint(1, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 19.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 19.0));
        AudioEvent("ChakramThrow", 1);
        mis = CreateObject("OgreShuriken", 1);
        SetOwner(SELF, mis);
        Enchant(mis, "ENCHANT_SHOCK", 0.0);
        PushObject(mis, -45.0, GetObjectX(OTHER), GetObjectY(OTHER));
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(45, GetTrigger(), ResetUnitSight);
        }
    }
}

void GhostAttack()
{
	if (CurrentHealth(OTHER))
	{
		CreatureIdle(SELF);
		LookAtObject(SELF, OTHER);
		AggressionLevel(SELF, 1.0);
		CreatureHunt(SELF);
		if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(15, GetTrigger(), ResetUnitSight);
		}
	}
}

void ZombieStrike()
{
    if (CurrentHealth(OTHER))
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < 80.0)
        {
            MoveWaypoint(41, GetObjectX(SELF), GetObjectY(SELF));
            AudioEvent("CrushHard", 41);
            PushObjectTo(SELF, -UnitRatioX(SELF, OTHER, 80.0), -UnitRatioY(SELF, OTHER, 80.0));
            Effect("CHARM", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, SELF, 20, 10);
            RestoreHealth(SELF, 10);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(65, GetTrigger(), ResetUnitSight);
		}
    }
}

void SkullLordStrike()
{
    if (CurrentHealth(OTHER))
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < 100.0)
        {
            MoveWaypoint(41, GetObjectX(OTHER), GetObjectY(OTHER));
            Effect("LIGHTNING", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(OTHER), GetObjectY(OTHER) - 200.0);
            AudioEvent("FirewalkFlame", 41);
            FrameTimerWithArg(10, CreateObject("PlayerWaypoint", 41), ThunderStorm);
            CreateObject("PlayerWaypoint", 41);
            CreateObject("PlayerWaypoint", 41);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(90, GetTrigger(), ResetUnitSight);
		}
    }
}

void ReleasePoison()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_BURNING"))
        {
            Enchant(SELF, "ENCHANT_BURNING", 1.0);
            MoveWaypoint(41, GetObjectX(SELF), GetObjectY(SELF));
            SetOwner(SELF, SpawnToxicCloud(41, 20));
            AudioEvent("PoisonTrapTriggered", 41);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(75, GetTrigger(), ResetUnitSight);
		}
    }
}

void GoonAttack()
{
    int mis;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_BURNING"))
        {
            Enchant(SELF, "ENCHANT_BURNING", 1.0);
            MoveWaypoint(41, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 19.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 19.0));
            AudioEvent("EggBreak", 41);
            mis = CreateObject("ThrowingStone", 41);
            CreateObject("SpiderSpit", 41);
            SetOwner(SELF, mis);
            SetOwner(SELF, mis + 1);
            PushObject(mis, -30.0, GetObjectX(OTHER), GetObjectY(OTHER));
            PushObject(mis + 1, -30.0, GetObjectX(OTHER), GetObjectY(OTHER));
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(30, GetTrigger(), ResetUnitSight);
		}
    }
}

void ImpStrike()
{
    if (CurrentHealth(OTHER))
    {
        MoveWaypoint(41, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 17.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 17.0));
        CreateMagicMissile(41, GetMemory(0x979724));
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(30, GetTrigger(), ResetUnitSight);
		}
    }
}

void DryadStrike()
{
    int ptr = GetTrigger() + 1;
    int unit;
    int rnd;
    int exist;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_BURNING"))
        {
            rnd = Random(0, 2);
            exist = ToInt(GetObjectZ(ptr + GetDirection(ptr)));
            MoveWaypoint(1, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 23.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 23.0));
            if (CurrentHealth(exist))
            {
                Effect("COUNTERSPELL_EXPLOSION", GetObjectX(exist), GetObjectY(exist), 0.0, 0.0);
                Delete(exist);
            }
            DeleteObjectTimer(CreateObject("BlueSummons", 1), 10);
            if (!rnd)
                unit = SpawnBear(1);
            else if (rnd == 1)
                unit = SpawnOgre(1);
            else
                unit = SpawnOgress(1);
            MoveObject(ptr + GetDirection(ptr), GetObjectX(SELF), GetObjectY(SELF));
            Raise(ptr + GetDirection(ptr), ToFloat(unit));    //regist_on_list
            FrameTimerWithArg(1, unit, DelayAttackToTarget);
            FrameTimerWithArg(1, GetCaller(), DelayAttackToTarget);
            AudioEvent("TelekinesisOn", 1);
            Enchant(SELF, "ENCHANT_BURNING", 6.0);
            LookWithAngle(ptr, (GetDirection(ptr) + 1) % 4);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(270, GetTrigger(), ResetUnitSight);
		}
    }
}

void DelayPlaceReward(int sUnit)
{
    if (IsObjectOn(sUnit))
    {
        MoveWaypoint(1, GetObjectX(sUnit), GetObjectY(sUnit));
        RewardMarkerFunc(1, 7);
        Delete(sUnit);
    }
}

void ReviveZombie(int sUnit)
{
    int zomb = ToInt(GetObjectZ(sUnit));

    if (MaxHealth(zomb))
    {
        DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(zomb), GetObjectY(zomb) + 4.0), 7);
        RaiseZombie(zomb);
    }
    else
    {
        FrameTimerWithArg(85, sUnit, DelayPlaceReward);
        PlaySoundAround(sUnit, 8);
        DeleteObjectTimer(CreateObjectAt("Flame", GetObjectX(sUnit), GetObjectY(sUnit)), 210);
        Effect("SPARK_EXPLOSION", GetObjectX(sUnit), GetObjectY(sUnit), 0.0, 0.0);
    }
}

void DeadZombieEvent()
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));

    Raise(unit, GetTrigger());
    FrameTimerWithArg(23, unit, ReviveZombie);
}

void DelayAttackToTarget(int ptr)
{
    int unit;

    if (!unit)
        unit = ptr;
    else
    {
        CreatureFollow(unit, ptr);
        AggressionLevel(unit, 1.0);
        unit = 0;
    }
}

void DelaySpiderReward(int dummyUnit)
{
    if (IsObjectOn(dummyUnit))
    {
        MoveWaypoint(1, GetObjectX(dummyUnit), GetObjectY(dummyUnit));
        RewardMarkerFunc(1, 7);
        Delete(dummyUnit);
    }
}

void BlackSpiderDeath()
{
    int unit;

    FrameTimerWithArg(150, CreateObjectAt("Mover", GetObjectX(SELF), GetObjectY(SELF)), DelaySpiderReward);

    unit = CreateObjectAt("WaterBarrelBreaking", GetObjectX(SELF), GetObjectY(SELF));
    FieldMonsterDeath();
    DeleteObjectTimer(CreateObjectAt("BigSmoke", GetObjectX(unit), GetObjectY(unit)), 7);
    PlaySoundAround(unit, 847);
    PlaySoundAround(CreateObjectAt("ArachnaphobiaFocus", GetObjectX(unit), GetObjectY(unit)), 563);
    Delete(SELF);
}

void MysticDeath()
{
    int unit;

    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    unit = CreateObject("InvisibleLightBlueHigh", 1);
    SpawnBeast(1);
    SpawnBeast(1);
    SpawnBeast(1);
    SetOwner(MasterUnit(), unit);
    CastSpellObjectObject("SPELL_TURN_UNDEAD", unit, unit);
    Delete(unit);
}

void DryadDeath()
{
    int k;
    int ptr = GetTrigger() + 1;

    for (k = 3 ; k >= 0 ; k --)
    {
        if (CurrentHealth(ToInt(GetObjectZ(ptr + k))))
        {
            DeleteObjectTimer(ToInt(GetObjectZ(ptr + k)), 1);
        }
        Delete(ptr + k);
    }
}

void ThunderStorm(int unit)
{
    int unit2;
    MoveWaypoint(41, GetObjectX(unit), GetObjectY(unit));
    Delete(unit);
    Delete(unit + 1);
    Delete(unit + 2);
    unit2 = CreateObject("Maiden", 41);
    Frozen(unit2, 1);
    SetCallback(unit2, 9, ThunderTouched);
    DeleteObjectTimer(unit2, 1);
    Effect("BLUE_SPARKS", GetWaypointX(41), GetWaypointY(41), 0.0, 0.0);
    Effect("JIGGLE", GetObjectX(unit), GetObjectY(unit), 35.0, 0.0);
}

void ThunderTouched()
{
    if (CurrentHealth(OTHER) && !IsOwnedBy(OTHER, MasterUnit()))
    {
        Damage(OTHER, MasterUnit(), 30, 9);
        Effect("RICOCHET", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Enchant(OTHER, "ENCHANT_CHARMING", 1.0);
    }
}

void InitJailSwitchs()
{
    int ptr;
    int k;

    for (k = 4; k >= 0 ; k --)
    {
        ptr = Object("sw" + IntToString(k + 1));
        LookWithAngle(ptr, k);
    }
}

void WishWellEvent()
{
    if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    {
        Enchant(OTHER, "ENCHANT_VILLAIN", 10.0);
        UniPrint(OTHER, "이 우물이 잠시동안 당신의 체력을 회복시켜 줄 것입니다.");
        MoveWaypoint(48, GetObjectX(SELF), GetObjectY(SELF));
        DeleteObjectTimer(CreateObject("MagicSpark", 48), 10);
        AudioEvent("PotionUse", 48);
        LoopUnitHeal(GetCaller());
    }
}

void LoopUnitHeal(int unit)
{
    if (CurrentHealth(unit) && HasEnchant(unit, "ENCHANT_VILLAIN"))
    {
        Effect("GREATER_HEAL", GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit) - 100.0);
        RestoreHealth(unit, 1);
        FrameTimerWithArg(2, unit, LoopUnitHeal);
    }
}

void GMOn()
{
    int ankh;

    if (!GetDirection(SELF))
    {
        LookWithAngle(SELF, 1);
        ankh = CreateObject("Ankh", 5);
        CreateObject("Ankh", 6);
        CreateObject("Ankh", 8);
        CreateObject("Ankh", 9);
        FixCreateMover(ankh, 0, 20.0);
        FixCreateMover(ankh + 1, 0, 20.0);
        FixCreateMover(ankh + 2, 0, 20.0);
        FixCreateMover(ankh + 3, 0, 20.0);
        Raise(Object("DanceColor"), ankh);
        SpawnGhost(168);
        SpawnGhost(168);
        SpawnGhost(168);
    }
    else if (GetDirection(SELF) == 1)
    {
        LockDoor(Object("SentryGate"));
        UniPrint(OTHER, "옆 방 출입문이 잠겼습니다");
        Move(ankh, 6);
        Move(ankh + 1, 8);
        Move(ankh + 2, 9);
        Move(ankh + 3, 5);
        LookWithAngle(SELF, 2);
    }
    else
    {
        if (IsObjectOn(Object("DanceColor")))
        {
            UnlockDoor(Object("SentryGate"));
            UniPrint(OTHER, "옆 방 출입문의 잠금이 해제되었습니다");
        }
        Move(ankh, 56);
        Move(ankh + 1, 55);
        Move(ankh + 2, 54);
        Move(ankh + 3, 57);
        LookWithAngle(SELF, 1);
    }
}

void CLOn()
{
    int col;

    if (!col)
    {
        col = Object("DanceColor");
        LoopAnkhSentryRay(col);
    }
    ObjectToggle(col);
}

void LoopAnkhSentryRay(int unit)
{
    int k, ptr = ToInt(GetObjectZ(unit));
    string name = "SENTRY_RAY";

    if (IsObjectOn(unit) && ptr)
    {
        Effect(name, GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        Effect(name, GetObjectX(ptr + 1), GetObjectY(ptr + 1), GetObjectX(ptr + 2), GetObjectY(ptr + 2));
        Effect(name, GetObjectX(ptr + 2), GetObjectY(ptr + 2), GetObjectX(ptr + 3), GetObjectY(ptr + 3));
        Effect(name, GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr + 3), GetObjectY(ptr + 3));
        Effect(name, GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr + 2), GetObjectY(ptr + 2));
        Effect(name, GetObjectX(ptr + 1), GetObjectY(ptr + 1), GetObjectX(ptr + 3), GetObjectY(ptr + 3));
    }
    FrameTimerWithArg(1, unit, LoopAnkhSentryRay);
}

void ToggleJailGate()
{
    int door = Object("Door" + IntToString(GetDirection(SELF) + 1));

    if (IsLocked(door))
        UnlockDoor(door);
    else
        LockDoor(door);
}

void UndergroundElevator()
{
    int elv[2];

    if (!elv[0])
    {
        elv[0] = Object("ToggleElev");
        elv[1] = Object("ToggleElevGear");
    }
    ObjectToggle(elv[0]);
    ObjectToggle(elv[1]);
    MoveWaypoint(58, GetObjectX(SELF), GetObjectY(SELF));
    if (IsObjectOn(elv[0]))
        AudioEvent("CreatureCageAppears", 58);
    else
        AudioEvent("SwitchToggle", 58);
}

int BlueMagicItem()
{
    int item;

    if (!item)
        item = CreateObject("BlueOrbKeyOfTheLich", 63);
    return item;
}

int RedMagicItem()
{
    int item;

    if (!item)
        item = CreateObject("RedOrbKeyOfTheLich", 1);
    return item;
}

int OrbGreen()
{
    int orb;

    if (!orb)
        orb = CreateObject("GreenOrb", 17);
    return orb;
}

void BookInfo()
{
    PlaySoundAround(OTHER, 903);
    UniPrint(OTHER, "책의 내용 일부에 '광산 출입 허가증' 이라고 써있습니다.");
}

int MinesBook(int location)
{
    int book, ptr;

    if (!book)
    {
        book = CreateObject("BlackBook1", location);
        ptr = GetMemory(0x750710);
        SetMemory(ptr + 0x2dc, ImportUseItemFunc());
        CancelTimer(FrameTimerWithArg(10, BookInfo, BookInfo));
        SetMemory(ptr + 0x2fc, GetMemory(GetMemory(0x83395c) + 8));
    }
    return book;
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_ANTI_MAGIC");
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.08);
    AggressionLevel(unit, 1.0);
}

void OpenWoodGate()
{
    if (HasClass(OTHER, "PLAYER") && CurrentHealth(OTHER))
    {
        if (HasItem(OTHER, RedMagicItem()))
        {
            Delete(RedMagicItem());
            Delete(SELF);
            UnlockDoor(Object("WoodGateMan1"));
            UnlockDoor(Object("WoodGateMan2"));
        }
    }
}

void OpenHiddenGate()
{
    if (HasClass(OTHER, "PLAYER") && CurrentHealth(OTHER))
    {
        if (HasItem(OTHER, BlueMagicItem()))
        {
            Delete(BlueMagicItem());
            Delete(SELF);
            UnlockDoor(Object("HiddenGate"));
            SpawnUrchinShaman(19);
            SpawnBlackSpider(19);
            SpawnBlackSpider(19);
            SpawnMecaFly(70);
            SpawnMecaFly(70);
            SpawnMecaFly(70);
            SpawnMecaFly(70);
            SpawnMecaFly(70);
            SpawnSwordsman(18);
            SpawnSwordsman(18);
            SpawnUrchinShaman(32);
            SpawnArcher(156);
            SpawnArcher(157);
            SpawnMecaFly(158);
            SpawnMecaFly(159);
        }
    }
}

void OpenMagicDoors()
{
    ObjectOff(SELF);
    UniPrint(OTHER, "어딘가에서 문의 잠금이 해제되었습니다.");
    UnlockDoor(Object("MagicDoor1"));
    UnlockDoor(Object("MagicDoor2"));
    SpawnMystic(11);
    SpawnMystic(12);
    SpawnMystic(13);
    SpawnMystic(14);
    SpawnGhost(12);
    SpawnGhost(12);
    SpawnGhost(12);
    SpawnArcher(5);
    SpawnArcher(6);
    SpawnArcher(8);
    SpawnArcher(9);
    SpawnMecaFly(30);
    SpawnMecaFly(30);
    SpawnMecaFly(30);
    SpawnBlackSpider(27);
}

void OpenUnderGate()
{
    if (CurrentHealth(OTHER) && HasClass(OTHER, "PLAYER"))
    {
        if (HasItem(OTHER, OrbGreen()))
        {
            Delete(SELF);
            Delete(OrbGreen());
            UnlockDoor(Object("Part2Gate1"));
            UnlockDoor(Object("Part2Gate2"));
            FrameTimer(1, PlaceArea2Monsters);
        }
    }
}

void OpenMinesGate()
{
    int unit, id;

    if (IsCaller(MinesBook(0)))
    {
        UniPrintToAll("광산 출입문이 열렸습니다");
        Delete(MinesBook(0));
        ObjectOff(SELF);
        UnlockDoor(Object("MinesGate"));
        int fx = CreateObject("Magic", 73);
        SetUnitSubclass(fx, GetUnitSubclass(fx) ^ 1);
        Frozen(fx, TRUE);
        id = CreateObject("WeirdlingBeast", 73);
        Frozen(id - 1, 1);
        Damage(id, 0, 9999, 14);
        ObjectOff(id);
        SetDialog(id, "NORMAL", RemoveMagicWall, DummyFunction);
        FrameTimer(1, PutMinesMob);
    }
}

void PutMinesMob()
{
    SpawnSpirit(205);
    SpawnScorpion(206);
    SpawnSkeletonLord(207);
    SpawnSkeleton(208);
    SpawnSkeletonLord(209);
    SpawnSpirit(210);
    SpawnSpirit(211);
    SpawnSpirit(212);
    SpawnScorpion(213);
    SpawnScorpion(475);
    SpawnBlackSpider(476);
    SpawnBear(477);
    SpawnScorpion(478);
    SpawnBlackSpider(479);
    SpawnSkeletonLord(480);
    SpawnBear(481);
    FrameTimer(1, GasBarrelBreakingWalls);
}

void GasBarrelBreakingWalls()
{
    string name[] = {"BlackPowderBarrel", "BlackPowderBarrel2"};
    int i;
    for (i = 0 ; i < 5 ; i ++)
        CreateObject(name[Random(0, 1)], 482 + i);
}

void RemoveMagicWall()
{
    int k;
    ObjectOn(Object("MinesNewElev"));
    UniPrint(OTHER, "비밀의 벽이 열렸습니다");
    WallOpen(Wall(217, 89));
    WallOpen(Wall(216, 90));
    SpawnBear(214);
    SpawnCreature("BlackBear", 214, 250);
    SpawnCreature("BlackBear", 215, 250);
    SpawnGoon(215);
    SpawnCreature("GiantLeech", 216, 98);
    SpawnCreature("GiantLeech", 216, 98);
    SpawnCreature("GiantLeech", 217, 98);
    SpawnCreature("GiantLeech", 217, 98);
    SpawnCreature("GiantLeech", 218, 98);
    SpawnCreature("GiantLeech", 218, 98);
    SpawnSwordsman(405);
    SpawnSwordsman(406);
    SpawnSwordsman(405);
    SpawnSwordsman(406);
    SpawnSwordsman(405);
    SpawnSwordsman(406);
    SpawnGoon(407);
    SpawnGoon(407);
    SpawnGoon(407);
    SpawnBear(219);
    FrameTimer(1, PutMinesMob2);
    Delete(SELF);
}

void PutMinesMob2()
{
    SpawnGoon(220);
    SpawnGoon(220);
    SpawnScorpion(221);
    SpawnCreature("GiantLeech", 222, 98);
    SpawnOgre(226);
    SpawnBlackSpider(226);
    SpawnScorpion(226);
    SpawnBlackSpider(227);
    SpawnCreature("Troll", 227, 325);
    SpawnCreature("Troll", 229, 325);
    SpawnGoon(228);
    SpawnMecaFly(228);
    SpawnBear(230);
    SpawnWolf(230);
    SpawnOgress(231);
    SpawnOgress(231);
    SpawnOgre(401);
    SpawnScorpion(401);
    SpawnBlackSpider(401);
    SpawnBlackSpider(402);
    SpawnUrchinShaman(402);
    SpawnSwordsman(93);
    SpawnSwordsman(93);
    SpawnSwordsman(93);
    SpawnSwordsman(403);
    SpawnSwordsman(403);
    SpawnSwordsman(403);
    SpawnRedWizard(98);
    SpawnSwordsman(404);
    SpawnSwordsman(404);
    SpawnBear(404);
}

void FxRumble()
{
    int ptr;

    if (IsPlayerUnit(OTHER) && CurrentHealth(OTHER))
    {
        MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("EarthRumbleMajor", 1);
        ptr = CreateObject("CaveRocksMedium", 1);
        CreateObject("CaveRocksSmall", 1);
        CreateObject("CaveRocksSmall", 1);
        Raise(ptr, 200.0);
        Raise(ptr + 1, 200.0);
        Raise(ptr + 2, 200.0);
        Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 50.0, 0.0);
        Delete(SELF);
    }
}

void SpawnRumbleUnit()
{
    int k, rumbUnit = CreateObject("AirshipCaptain", 74);

    Damage(rumbUnit, 0, 9999, -1);
    SetCallback(rumbUnit, 9, FxRumble);
    for (k = 1 ; k < 15 ; Nop(k ++))
    {
        CreateObject("AirshipCaptain", 74 + k);
        Damage(rumbUnit + k, 0, 9999, -1);
        SetCallback(rumbUnit + k, 9, FxRumble);
    }
}

int CaveGasBarrels()
{
    int barr, k;

    if (!barr)
    {
        barr = CreateObject("BlackPowderBarrel2", 97);
        Frozen(barr, 1);
        for (k = 98 ; k <= 116 ; Nop(k ++))
            Frozen(CreateObject("BlackPowderBarrel2", k), 1);
        return 0;
    }
    return barr;
}

void ExplosionAllGasBarrels(int barr)
{
    int k;

    for (k = 19 ; k >= 0 ; Nop(k --))
    {
        Frozen(barr + k, FALSE);
        Damage(barr + k, 0, 255, 14);
    }
    Effect("JIGGLE", GetWaypointX(100), GetWaypointY(100), 100.0, 0.0);
    Effect("JIGGLE", GetWaypointX(111), GetWaypointY(111), 100.0, 0.0);
    RemoveCaveWalls();
    RockFallingOnBottom(97);
}

void RockFallingOnBottom(int wp)
{
    int unit;

    if (wp <= 116)
    {
        unit = CreateObject(Rocks(), wp);
        Raise(unit, 200.0);
        Effect("SMOKE_BLAST", GetWaypointX(wp), GetWaypointY(wp), 0.0, 0.0);
        AudioEvent("FistHit", wp);
        FrameTimerWithArg(1, wp + 1, RockFallingOnBottom);
    }
}

string Rocks()
{
    string rockTable[] = {"Rock2", "BoulderCave", "CaveBoulders", "CaveRocksHuge", "CaveRocksLarge"};
    
    return rockTable[Random(0, 4)];
}

void CavePartInit()
{
    ObjectOff(SELF);
    FireChunk();
    CaveGasBarrels();
    SpawnRumbleUnit();
}

void ActionFireboom()
{
    int unit;
    int next = CaveBarrel();

    ObjectOff(SELF);
    Rocks();

    Effect("JIGGLE", GetWaypointX(117), GetWaypointY(117), 80.0, 0.0);
    AudioEvent("EarthRumbleMajor", 117);
    unit = CreateObject("CaveBoulders", 117);
    Raise(unit, 200.0);
    DeleteObjectTimer(unit, 60);
    FrameTimerWithArg(25, next, DelayCrashBarrel);
}

void DelayCrashBarrel(int unit)
{
    int fire = CreateObject("MediumFlame", 117);
    
    FixCreateMover(fire, 0, 40.0);
    Frozen(unit, 0);
    Damage(unit, 0, 999, 14);
    FrameTimerWithArg(1, fire, DelayMover);
    FrameTimerWithArg(1, 90, DelayMover);
    FrameTimerWithArg(160, CaveGasBarrels(), ExplosionAllGasBarrels);
}

void DelayMover(int arg)
{
    int unit;

    if (!unit)
        unit = arg;
    else
    {
        Move(unit, arg);
        unit = 0;
    }
}

void RemoveCaveWalls()
{
    NoWallSound(1);
    WallOpen(Wall(61, 175));
    WallOpen(Wall(60, 176));
    WallOpen(Wall(59, 175));
    WallOpen(Wall(58, 176));
    WallOpen(Wall(57, 177));
    WallOpen(Wall(56, 178));
    WallOpen(Wall(55, 179));
    WallOpen(Wall(54, 180));
    WallOpen(Wall(55, 181));
    NoWallSound(0);
}

int CaveBarrel()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("BlackPowderBarrel", 117);
        Frozen(unit, 1);
    }
    return unit;
}

int OgreKey()
{
    int key;

    if (!key)
        key = CreateObject("RedOrbKeyOfTheLich", 122);
    return key;
}

void OpenOgreDoors()
{
    if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
    {
        if (HasItem(OTHER, OgreKey()))
        {
            UniPrint(OTHER, "열쇠를 사용하여 문의 잠금을 해제하였습니다.");
            Delete(SELF);
            Delete(OgreKey());
            UnlockDoor(Object("OgrePartDoor1"));
            UnlockDoor(Object("OgrePartDoor2"));
            PutTwoHints();
        }
    }
}

void HintPick()
{
    int flag, unit;

    flag ++;
    if (flag == 1)
    {
        WallOpen(Wall(143, 19));
        WallOpen(Wall(144, 20));
        WallOpen(Wall(145, 21));
        unit = CreateObject("RollingBoulder", 129);
        FixCreateMover(unit, 0, 60.0);
        FrameTimerWithArg(30, unit, DelayMover);
        FrameTimerWithArg(30, 124, DelayMover);
    }
    else if (flag == 2)
    {
        WallOpen(Wall(146, 16));
        WallOpen(Wall(147, 17));
        WallOpen(Wall(148, 18));
        unit = CreateObject("RollingBoulder", 130);
        FixCreateMover(unit, 0, 60.0);
        FrameTimerWithArg(30, unit, DelayMover);
        FrameTimerWithArg(30, 124, DelayMover);
        FrameTimer(60, RemoveLastGoalWalls);
    }
    Delete(SELF);
}

void PutTwoHints()
{
    RegistItemPickupCallback(CreateObject("RedOrb", 132), HintPick);
    RegistItemPickupCallback(CreateObject("BlueOrb", 132), HintPick);
}

void RemoveLastGoalWalls()
{
    int unit = CreateObject("RollingBoulder", 131);

    WallOpen(Wall(149, 13));
    WallOpen(Wall(150, 14));
    WallOpen(Wall(151, 15));
    FixCreateMover(unit, 0, 60.0);
    FrameTimerWithArg(30, unit, DelayMover);
    FrameTimerWithArg(30, 124, DelayMover);
    SecondTimer(4, SpawnOgreBoss);
}

void SpawnOgreBoss()
{
    int boss = CreateObject("OgreWarlord", 134);

    SetUnitMaxHealth(boss, 900);
    LookWithAngle(boss, 96);
    SetCallback(boss, 3, BossOgreSkill);
    SetCallback(boss, 5, DeadBossOgre);
}

void BossOgreSkill()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
        {
            if (!Random(0, 1))
                CastTripleArrowShot();
            else
                FallingStoneAttack();
            Enchant(SELF, "ENCHANT_VILLAIN", 2.0);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(95, GetTrigger(), ResetUnitSight);
        }
    }
}

void CastTripleArrowShot()
{
    float pos_x = UnitRatioX(SELF, OTHER, 19.0);
    float pos_y = UnitRatioY(SELF, OTHER, 19.0);
    int k;
    int missile;

    MoveWaypoint(1, GetObjectX(SELF) + (1.0 / 1.6 * pos_y) - pos_x, GetObjectY(SELF) - (1.0 / 1.6 * pos_x) - pos_y);
    
    for (k = 8 ; k >= 0 ; k --)
    {
        pos_x = GetRatioUnitWpX(SELF, 1, 19.0);
        pos_y = GetRatioUnitWpY(SELF, 1, 19.0);
        MoveWaypoint(1, GetObjectX(SELF) - (1.0 / 8.0 * pos_y) - pos_x, GetObjectY(SELF) + (1.0 / 8.0 * pos_x) - pos_y);
        missile = CreateObject("OgreShuriken", 1);
        SetOwner(SELF, missile);
        PushObject(missile, 23.0, GetObjectX(SELF), GetObjectY(SELF));
    }
}

void OgreHitStone()
{
    Damage(OTHER, GetOwner(GetOwner(SELF)), 100, 2);
}

void UnitVisibleSplashHuman()
{
    int parent = GetOwner(SELF);

    if (!HasEnchant(OTHER, "ENCHANT_DETECTING") && CurrentHealth(GetOwner(parent)))
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
        {
            CallFunction(ToInt(GetObjectZ(parent)));
            Enchant(OTHER, "ENCHANT_DETECTING", 0.1);
        }
    }
}

void UnitVisibleSplash()
{
    int parent = GetOwner(SELF);
    int spIdx = ToInt(GetObjectZ(parent + 1)), ptr = UnitToPtr(OTHER);

    if (ptr)
    {
        if (IsPlayerUnit(OTHER))
            UnitVisibleSplashHuman();
        else if (GetMemory(ptr + 0x1c) ^ spIdx && CurrentHealth(GetOwner(parent)))
        {
            if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
            {
                CallFunction(ToInt(GetObjectZ(parent)));
                SetMemory(ptr + 0x1c, spIdx);
            }
        }
    }
}

void SplashHandler(int owner, int func, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, k, SplashIdx;

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(func));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 32);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(ptr - 2, 2);
    SplashIdx ++;
}

void FallenStoneHanlder(int ptr)
{
    int owner = GetOwner(ptr);
    float posZ = GetObjectZ(ptr + 1);
    
    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (ToInt(posZ))
            {
                Raise(ptr + 1, posZ - 20.0);
                FrameTimerWithArg(1, ptr, FallenStoneHanlder);
                break;
            }
            else
            {
                Raise(ptr, OgreHitStone);
                SplashHandler(owner, ToInt(GetObjectZ(ptr)), GetObjectX(ptr), GetObjectY(ptr), 140.0);
                Effect("JIGGLE", GetObjectX(ptr), GetObjectY(ptr), 32.0, 0.0);
            }
        }
        Delete(ptr);
        Delete(ptr + 1);
        break;
    }
}

void FallingStoneAttack()
{
    int unit;

    MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("HitStoneBreakable", 1);
    unit = CreateObject("InvisibleLightBlueLow", 1);
    UnitNoCollide(CreateObject("CaveBoulders", 1));
    Raise(unit + 1, 200.0);
    SetOwner(SELF, unit);
    FrameTimerWithArg(1, unit, FallenStoneHanlder);
}

void DeadBossOgre()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(CreateObject("StormCloud", 1), 15);
    AudioEvent("HecubahDieFrame283", 1);
    //FireChunk();
    MoveObject(MinesBook(0), GetWaypointX(1), GetWaypointY(1));
    DeleteObjectTimer(SELF, 30);
    UniPrintToAll("오우거로드가 죽었습니다");
}

int FireChunk()
{
    int key;

    if (!key)
        key = CreateObject("BraceletofHealth", 491);
    return key;
}

void EnableOgreElevator()
{
    UniPrint(OTHER, "엘리베이터가 작동되었습니다.");
    ObjectOff(SELF);
    ObjectOn(Object("OgreElev"));
}

void Enable3PartElev()
{
    if (HasItem(OTHER, FireChunk()))
    {
        UniPrint(OTHER, "정령의 돌을 이용해 엘리베이터를 작동시켰습니다.");
        ObjectOff(SELF);
        ObjectOn(Object("CaveConnectElev"));
        Delete(FireChunk());
        FrameTimer(1, LastPartMonsters);
        FrameTimerWithArg(11, GetCaller(), SpawnMonsterGroups);
    }
}

void SpawnMonsterGroups(int enemy)
{
    int ptr = SpawnSwordsman(135);
    SpawnSwordsman(135);
    SpawnSwordsman(135);
    SpawnArcher(135);
    SpawnArcher(135);
    SpawnArcher(135);
    SpawnMecaFly(135);
    SpawnMecaFly(135);
    SpawnMecaFly(135);
    FrameTimerWithArg(1, ptr, TargetForAttack);
}

void FieldMonsterDeath()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    RewardMarkerFunc(1, 7);
}

void TargetForAttack(int ptr)
{
    int target;
    int k;

    if (!target)
        target = ptr;
    else
    {
        for (k = 8 ; k >= 0 ; k --)
        {
            CreatureFollow(ptr + k, target);
            AggressionLevel(ptr + k, 1.0);
        }
        target = 0;
    }
}

void SpawnSpellbooks()
{
    int flag;

    if (!flag)
    {
        flag = 1;
        CreateEMagicBook(40, 0, 6);
        CreateEMagicBook(139, 1, 6);
        CreateEMagicBook(140, 2, 6);
        FrameTimer(30, SpawnSpellbooks);
        LibraryColor();
    }
    else
    {
        CreateEMagicBook(141, 3, 8);
        CreateEMagicBook(142, 0, 6);
        CreateEMagicBook(143, 1, 6);
    }
}

void CreateEMagicBook(int wp, int type, int amount)
{
    string bookList[] = {"ConjurerSpellBook", "WizardSpellBook", "AbilityBook", "FieldGuide"};
    int k, exec[] = {BookEventWithConjurer, BookEventWithWizard, BookEventWithWarrior, CreatureGuidePick};

    for (k = amount - 1 ; k >= 0 ; Nop(k --))
    {
        RegistItemPickupCallback(CreateObject(bookList[type], wp), exec[type]);
        TeleportLocationVector(wp, -46.0, 46.0);
    }
}

int LibraryColor()
{
    int col;

    if (!col) col = CreateObject("InvisibleLightBlueHigh", 7);
    return col;
}

void BookEventWithConjurer()
{
    MoveObject(LibraryColor(), GetObjectX(OTHER), GetObjectY(OTHER));
    if (!Random(0, 1))
        CastSpellObjectLocation("SPELL_METEOR", LibraryColor(), GetObjectX(OTHER), GetObjectY(OTHER));
    else if (!Random(0, 1))
        CastSpellObjectLocation("SPELL_FIST", LibraryColor(), GetObjectX(OTHER), GetObjectY(OTHER));
    else
    {
        MoveWaypoint(41, GetObjectX(SELF), GetObjectY(SELF));
        CreateObject("ArachnaphobiaFocus", 41);
        DeleteObjectTimer(CreateObject("BigSmoke", 41), 7);
        AudioEvent("PoisonTrapTriggered", 41);
    }
    Delete(SELF);
}

void BookEventWithWizard()
{
    MoveObject(LibraryColor(), GetObjectX(OTHER), GetObjectY(OTHER));
    if (Random(0, 1))
        CastSpellObjectObject("SPELL_CLEANSING_FLAME", SELF, SELF);
    else
        CastSpellObjectLocation("SPELL_FIREBALL", LibraryColor(), GetObjectX(OTHER) + UnitAngleCos(OTHER, 17.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 17.0));
    Delete(SELF);
}

void BookEventWithWarrior()
{
    MoveWaypoint(41, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("Clank1", 41);
    if (!Random(0, 1))
        CreateObject("BearTrap", 41);
    else
        CreateObject("PoisonGasTrap", 41);
    Delete(SELF);
}

void CreatureGuidePick()
{
    string spell = "SPELL_SUMMON_";
    string creNameList[] = {"EMBER_DEMON", "BAT", "BEAR", "BLACK_BEAR", "BLACK_WOLF", "LICH", "SKELETON_LORD", "SPIDER", "URCHIN_SHAMAN", "VILE_ZOMBIE"};
    int owner = CreateObjectAt("BlackPowder", GetObjectX(SELF), GetObjectY(SELF));

    SetOwner(MasterUnit(), owner);
    MoveObject(LibraryColor(), GetObjectX(SELF), GetObjectY(SELF));
    CastSpellObjectObject(spell + creNameList[ Random(0, 9)], owner, owner);
    Delete(SELF);
}

void InitLibarySoulFlame()
{
    int id = CreateObject("Maiden", 144);
    CreateObject("Maiden", 145);
    LibraryObstacle();

    Enchant(id, "ENCHANT_HASTED", 0.0);
    Enchant(id + 1, "ENCHANT_HASTED", 0.0);
    FixCreateMover(id, 0, 30.0);
    FixCreateMover(id + 1, 0, 30.0);
    Frozen(id, 1);
    Frozen(id + 1, 1);
    SetCallback(id, 9, SoulTouched);
    SetCallback(id + 1, 9, SoulTouched);
    FrameTimerWithArg(1, id, DelayMover);
    FrameTimerWithArg(1, 146, DelayMover);
    FrameTimerWithArg(1, id + 1, DelayMover);
    FrameTimerWithArg(1, 148, DelayMover);
}

int LibraryObstacle()
{
    int block;

    if (!block)
    {
        block = CreateObject("IronBlock", 151);
        CreateObject("IronBlock", 152);
        FixCreateMover(block, 0, 10.0);
        FixCreateMover(block + 1, 0, 10.0);
    }
    return block;
}

void SoulTouched()
{
    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, MasterUnit()))
    {
        Damage(OTHER, MasterUnit(), 20, 16);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void OpenLibraryBlocks()
{
    ObjectOff(SELF);
    AudioEvent("SpikeBlockMove", 151);
    MoveWaypoint(151, GetWaypointX(151) + 46.0, GetWaypointY(151) - 46.0);
    MoveWaypoint(152, GetWaypointX(152) - 46.0, GetWaypointY(152) + 46.0);
    Move(LibraryObstacle(), 151);
    Move(LibraryObstacle() + 1, 152);
    SpawnSecondBoss();
    SpawnGoon(  340);
    SpawnGoon(  341);
    SpawnMystic(493);
    SpawnMystic(494);
}

int SpawnSecondBoss()
{
    int id = CreateObject("WizardWhite", 153);

    SetUnitMaxHealth(id, 900);
    Enchant(id, "ENCHANT_ANCHORED", 0.0);
    SetCallback(id, 3, ScdBossWeapon);
    SetCallback(id, 5, DeadScdBoss);
    SetCallback(id, 8, FleeEvent);
    SetCallback(id, 13, FleeEvent);
    SetUnitScanRange(id, 500.0);
    return id;
}

void DeadScdBoss()
{
    int unit = CreateObject("Maiden", 154);

    Frozen(unit, 1);
    SetCallback(unit, 9, OpenLastDoors);
    MoveWaypoint(41, GetObjectX(SELF), GetObjectY(SELF));
    LastKey();
    ObjectOn(Object("StopElevator"));
    RemovePart3Walls();
}

int LastKey()
{
    int key;

    if (!key) key = CreateObject("ProtectionEnchantments", 41);
    return key;
}

void FleeEvent()
{
    CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
    EnchantOff(SELF, "ENCHANT_ANTI_MAGIC");
    AggressionLevel(SELF, 1.0);
}

void ScdBossWeapon()
{
    if (CurrentHealth(OTHER))
    {
        WriteLog("scdbossweapon-start");
        int rnd = Random(0, 3);

        if (!rnd)
        {
            MoveWaypoint(41, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 21.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 21.0));
            CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
            CreateMagicMissile(41, GetMemory(0x979724));
        }
        else if (rnd == 1)
            CastSpellObjectLocation("SPELL_DEATH_RAY", SELF, GetObjectX(OTHER) + RandomFloat(-12.0, 12.0), GetObjectY(OTHER) + RandomFloat(-12.0, 12.0));
        else if (rnd == 2)
        {
            CastSpellObjectObject("SPELL_SHOCK", SELF, SELF);
            CastSpellObjectObject("SPELL_LIGHTNING", SELF, OTHER);
            PauseObject(SELF, 60);
        }
        else
        {
            CastSpellObjectObject("SPELL_FIREBALL", SELF, OTHER);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(45, GetTrigger(), ResetUnitSight);
        }
        WriteLog("scdbossweapon-end");
    }
}

void RemovePart3Walls()
{
    int k;
    WallOpen(Wall(214, 200));
    for (k = 21 ; k >= 0 ; Nop(k --))
        WallOpen(Wall(213 - k, 199 + k));
}

void OpenLastDoors()
{
    if (CurrentHealth(OTHER) && HasClass(OTHER, "PLAYER"))
    {
        if (HasItem(OTHER, LastKey()))
        {
            UniChatMessage(OTHER, "문의 잠금을 해제했습니다.", 150);
            Delete(LastKey());
            Delete(SELF);
            UnlockDoor(Object("LastPartDoor1"));
            UnlockDoor(Object("LastPartDoor2"));
        }
    }
}

int GridKey()
{
    int key;

    if (!key) key = CreateObject("Fear", 44);
    return key;
}

void OpenUndergroundGate()
{
    if (CurrentHealth(OTHER) && HasClass(OTHER, "PLAYER"))
    {
        if (HasItem(OTHER, GridKey()))
        {
            UniChatMessage(OTHER, "문의 잠금을 해제했습니다.", 150);
            Delete(GridKey());
            Delete(SELF);
            UnlockDoor(Object("GridGate1"));
            UnlockDoor(Object("GridGate2"));
            FrameTimer(1, PlaceArea21Monsters);
        }
    }
}

void Area2WispDead()
{
    MoveObject(GridKey(), GetObjectX(SELF), GetObjectY(SELF));
}

void ToggleOgreGate()
{
    int door[2];

    if (!door[0])
    {
        door[0] = Object("OgrePartCage1");
        door[1] = Object("OgrePartCage2");
    }
    if (IsLocked(door[0]))
    {
        UniPrint(OTHER, "잠금해제");
        UnlockDoor(door[0]);
        UnlockDoor(door[1]);
    }
    else
    {
        UniPrint(OTHER, "잠금");
        LockDoor(door[0]);
        LockDoor(door[1]);
    }
}

void PutSwampUrchins(int wp)
{
    int k;
    SpawnUrchinShaman(wp);
    for (k = 9 ; k >= 0 ; k --)
        SpawnCreature("Urchin", wp, 64);
}

void OpenLastArchDoors()
{
    if (CurrentHealth(OTHER) && HasClass(OTHER, "PLAYER"))
    {
        UnlockDoor(Object("ArchDoor1"));
        UnlockDoor(Object("ArchDoor2"));
        SpawnMystic(413);
        SpawnMystic(413);
        SpawnGoon(413);
        Delete(SELF);
    }
}

void RespectGWizs()
{
    int unit;

    ObjectOff(SELF);
    if (!unit)
    {
        unit = SpawnDryad(428);
        SpawnDryad(429);
        Effect("TELEPORT", GetWaypointX(428), GetWaypointY(428), 0.0, 0.0);
        Effect("TELEPORT", GetWaypointX(429), GetWaypointY(429), 0.0, 0.0);
        // SpawnBlackSpider(430);
        // SpawnBlackSpider(430);
        SpawnMimic(430);
        SpawnMimic(430);
    }
}

void RespectFinalBoss()
{
    int unit;
    ObjectOff(SELF);
    if (!unit)
        unit = FinalBoss();
}

int FinalBoss()
{
    int finBoss, id, fx;

    if (!finBoss)
    {
        finBoss = CreateObject("Horrendous", 431);
        fx = CreateObject("InvisibleLightBlueHigh", 431);
        SetOwner(MasterUnit(), id);
        SetOwner(MasterUnit(), fx);
        SetUnitMaxHealth(finBoss, 1500);
        CreatureGuard(id, GetObjectX(id), GetObjectY(id), GetObjectX(id), GetObjectY(id), 600.0);
        CastSpellObjectObject("SPELL_TURN_UNDEAD", fx, fx);
        AudioEvent("HecubahDieFrame283", 431);
        UnlockDoor(Object("GM1"));
        UnlockDoor(Object("GM2"));
        SetCallback(id, 3, FBossSkill);
        SetCallback(id, 5, DeadFinalBoss);
    }
    return id;
}

void DeadFinalBoss()
{
    UniPrintToAll("최종보스를 죽였습니다.");
    MoveWaypoint(431, GetObjectX(SELF), GetObjectY(SELF));
    //TeleportAllPlayers(431);
    Effect("WHITE_FLASH", GetWaypointX(431), GetWaypointY(431), 0.0, 0.0);
    AudioEvent("StaffOblivionAchieve1", 431);
    //FrameTimer(1, StrVictory);
}

void FBossSkill()
{
    int enemy;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_BURNING"))
        {
            Enchant(SELF, "ENCHANT_BURNING", 10.0);
            if (Random(0, 2))
            {
                LookAtObject(SELF, OTHER);
                ShootFireArrow(GetTrigger());
            }
            else
            {
                MoveWaypoint(41, GetObjectX(OTHER), GetObjectY(OTHER));
                enemy = CreateObject("InvisibleLightBlueHigh", 41);
                Raise(enemy, ToFloat(GetTrigger()));
                Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
                ObjectOff(SELF);
                Enchant(SELF, "ENCHANT_INVULNERABLE", 0.0);
                Enchant(SELF, "ENCHANT_FREEZE", 0.0);
                FrameTimerWithArg(10, enemy, SentryTrick);
            }
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(30, GetTrigger(), ResetUnitSight);
        }
    }
}

int MakeArrow(int unit)
{
	int ptr;
	float gap = 7.0;
	float pos_x = -UnitAngleCos(unit, gap);
	float pos_y = -UnitAngleSin(unit, gap);
	string name = "SmallFlame";
	float x_buf[2]; float y_buf[2];

	MoveWaypoint(431, GetObjectX(unit) - (pos_x * 3.0), GetObjectY(unit) - (pos_y * 3.0));
	ptr = CreateObject(name, 431);
	MoveWaypoint(431, GetWaypointX(431) - pos_x, GetWaypointY(431) - pos_y);
	CreateObject(name, 431);
	MoveWaypoint(431, GetWaypointX(431) - pos_x, GetWaypointY(431) - pos_y);
	CreateObject(name, 431);
	MoveWaypoint(431, GetWaypointX(431) - pos_x, GetWaypointY(431) - pos_y);
	CreateObject(name, 431);
	MoveWaypoint(431, GetWaypointX(431) - pos_x, GetWaypointY(431) - pos_y);
	CreateObject(name, 431);
	MoveWaypoint(431, GetWaypointX(431) - pos_x, GetWaypointY(431) - pos_y);
	CreateObject(name, 431);
	MoveWaypoint(431, GetWaypointX(431) - pos_x, GetWaypointY(431) - pos_y);
	CreateObject(name, 431); //6
	MoveWaypoint(431, GetObjectX(ptr + 6) + pos_y, GetObjectY(ptr + 6) - pos_x);
	MoveWaypoint(432, GetObjectX(ptr + 6) - pos_y, GetObjectY(ptr + 6) + pos_x);
	x_buf[0] = UnitWpRatioX(ptr + 5, 431, gap); x_buf[1] = UnitWpRatioX(ptr + 5, 432, gap);
	y_buf[0] = UnitWpRatioY(ptr + 5, 431, gap); y_buf[1] = UnitWpRatioY(ptr + 5, 432, gap);
	MoveWaypoint(431, GetObjectX(ptr + 6) + x_buf[0], GetObjectY(ptr + 6) + y_buf[0]);
	MoveWaypoint(432, GetObjectX(ptr + 6) + x_buf[1], GetObjectY(ptr + 6) + y_buf[1]);
	CreateObject(name, 431);
	CreateObject(name, 432);
	MoveWaypoint(431, GetWaypointX(431) + x_buf[0], GetWaypointY(431) + y_buf[0]);
	MoveWaypoint(432, GetWaypointX(432) + x_buf[1], GetWaypointY(432) + y_buf[1]);
	CreateObject(name, 431);
	CreateObject(name, 432);
	MoveWaypoint(431, GetWaypointX(431) + x_buf[0], GetWaypointY(431) + y_buf[0]);
	MoveWaypoint(432, GetWaypointX(432) + x_buf[1], GetWaypointY(432) + y_buf[1]);
	CreateObject(name, 431);
	CreateObject(name, 432);
    Raise(CreateObject("InvisibleLightBlueHigh", 431), pos_x);
    Raise(CreateObject("InvisibleLightBlueHigh", 431), pos_y);
    return ptr;
}

void ShootFireArrow(int caster)
{
    int ptr;
    int unit;
    if (CurrentHealth(caster))
    {
        ptr = MakeArrow(caster);
        MoveWaypoint(431, GetObjectX(ptr + 6), GetObjectY(ptr + 6));
        unit = CreateObject("Maiden", 431);
        SetCallback(unit, 9, FireArrowTouched);
        Frozen(unit, 1);
        FrameTimerWithArg(1, ptr, MovingFireArrow);
    }
}

void MovingFireArrow(int ptr)
{
    int k;

    if (GetDirection(ptr) < 60 && CurrentHealth(ptr + 15) && CheckUnitMapBoundary(ptr + 15))
    {
        for (k = 15 ; k >= 0 ; k --)
            MoveObject(ptr + k, GetObjectX(ptr + k) - (GetObjectZ(ptr + 13) * 2.0), GetObjectY(ptr + k) - (GetObjectZ(ptr + 14) * 2.0));
        LookWithAngle(ptr, GetDirection(ptr) + 1);
        FrameTimerWithArg(1, ptr, MovingFireArrow);
    }
    else
    {
        for (k = 15 ; k >= 0 ; k --)
            Delete(ptr + k);
    }
}

void FireArrowTouched()
{
    int plr = GetDirection(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, MasterUnit()))
    {
        Delete(SELF);
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, SELF, 100, 1);
    }
}

void TeleportAllPlayers(int wp)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
            MoveObject(player[k], GetWaypointX(wp), GetWaypointY(wp));
    }
}

void OpenGirlCage()
{
    int k;
    ObjectOff(SELF);
    UniPrint(OTHER, "격자창이 열립니다.");
    for (k = 9 ; k >= 0 ; k --)
        WallOpen(Wall(202 + k, 146 + k));
}

void InitGirls()
{
    int k, unit;

    for (k = 10 ; k >= 0 ; k --)
    {
        unit = Object("FemaleVictim" + IntToString(k + 1));
        SetDialog(unit, "NORMAL", TalkWithMaiden, DummyFunction);
        RetreatLevel(unit, 0.0);
        SetCallback(unit, 5, MaidenDead);
    }
}

string FemaleVictimMent(int num)
{
    string femCry[] = {
    "저를 구출해 주셔서 고맙습니다,\n저를 안전한 곳으로 데려다 주세요", //0
    "저를 도와주세요!\n이곳은 혼자있기에 너무 무서운 곳이었어요!", //1
    "오! 나의 구세주...", //2
    "저 여기있어요! 저좀 꺼내 주세요!", //3
    "드디어 이곳에서 탈출할 수 있게 되었네요. 고마워요 이름모를 모험가씨", //4
    "아~ 지금이 몇시인데 이제와? 빨리 안와", //5
    "정말 감사합니다!\n엉엉 ㅠㅠㅠ", //6
    "당신의 뒤만 따르겠어요", //7
    "길을 헤매는 동안 괴물들이 나를 봉쇄했어요\n잠시의 틈을 통해 그들에게 도망쳐 나올 수 있었죠\n그리고 여기에 숨었고\n마침내 당신이 온거죠", //8
    "다른이들은 위험하다며 날 버렸지만..\n당신만은 날...\n착하시군요?", //9
    "나.. 드디어 여기서 나갈 수 있는거야??\n (기절)", //10
    "Thank You! (외국인)" //11
    };

    return "MISSING: " + femCry[num];
}

void TalkWithMaiden()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 7.0);
        MoveObject(SELF, GetObjectX(OTHER), GetObjectY(OTHER));
        if (!IsOwnedBy(SELF, OTHER))
        {
            if (MaxHealth(SELF) != 300)
                SetUnitMaxHealth(GetMemory(0x979724), 300);
            UniChatMessage(SELF, FemaleVictimMent(GetDirection(GetTrigger() + 1)), 180);
            SetOwner(OTHER, SELF);
            MoveWaypoint(41, GetObjectX(SELF), GetObjectY(SELF));
            CreatureFollow(SELF, OTHER);
            AudioEvent("MaidenTalkable", 41);
        }
    }
}

void MaidenDead()
{
    int ggover;
    MoveWaypoint(41, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("MaidenDie", 41);
    DeleteObjectTimer(SELF, 1);
    if (!ggover)
    {
        UniPrintToAll("미션실패_! 방금 한명의 여성이 희생되었습니다");
        TeleportAllPlayers(433);
        MoveObject(Object("StartLocation"), GetWaypointX(433), GetWaypointY(433));
        SetStartSwitch(0);
        FrameTimer(1, StrFailMission);
    }
}

void BringGirl()
{
    int save;

    if (GetUnitThingID(OTHER) == 1385)
    {
        UniPrintToAll("방금 한명의 여성이 구출되었습니다 (11명 중 " + IntToString(++save) + " 명)");
        UniChatMessage(OTHER, "감사합니다!", 150);
        Frozen(OTHER, 1);
        CancelDialog(OTHER);
        LookWithAngle(OTHER, 32);
        MoveObject(OTHER, LocationX(434), LocationY(434));
        TeleportLocationVector(434, 23.0, -23.0);
        if (save == 11)
        {
            UniPrintToAll("미션성공, 모든 여성을 맵에서 구출했습니다");
            TeleportAllPlayers(433);
            MoveObject(Object("StartLocation"), GetWaypointX(433), GetWaypointY(433));
            Effect("WHITE_FLASH", GetWaypointX(433), GetWaypointY(433), 0.0, 0.0);
            MoveWaypoint(431, GetWaypointX(433), GetWaypointY(433));
            StrVictory();
            ObjectOff(SELF);
        }
    }
}

void MapPotionPlace(string name)
{
    PotionPickupRegist(CreateObject(name, 22));
    PotionPickupRegist(CreateObject(name, 435));
    PotionPickupRegist(CreateObject(name, 436));
    PotionPickupRegist(CreateObject(name, 239));
    PotionPickupRegist(CreateObject(name, 240));
    PotionPickupRegist(CreateObject(name, 241));
    PotionPickupRegist(CreateObject(name, 242));
    PotionPickupRegist(CreateObject(name, 27));
    PotionPickupRegist(CreateObject(name, 70));
    PotionPickupRegist(CreateObject(name, 18));
    PotionPickupRegist(CreateObject(name, 34));
    PotionPickupRegist(CreateObject(name, 11));
    PotionPickupRegist(CreateObject(name, 12));
    PotionPickupRegist(CreateObject(name, 13));
    FrameTimerWithArg(1, name, MapPotionPlace2);
}

void MapPotionPlace2(string name)
{
    PotionPickupRegist(CreateObject(name, 171));
    PotionPickupRegist(CreateObject(name, 174));
    PotionPickupRegist(CreateObject(name, 173));
    PotionPickupRegist(CreateObject(name, 180));
    PotionPickupRegist(CreateObject(name, 185));
    PotionPickupRegist(CreateObject(name, 181));
    PotionPickupRegist(CreateObject(name, 182));
    PotionPickupRegist(CreateObject(name, 183));
    PotionPickupRegist(CreateObject(name, 184));
    PotionPickupRegist(CreateObject(name, 10));
    PotionPickupRegist(CreateObject(name, 437));
    PotionPickupRegist(CreateObject(name, 438));
    PotionPickupRegist(CreateObject(name, 122));
    FrameTimerWithArg(1, name, MapPotionPlace3);
}

void MapPotionPlace3(string name)
{
    PotionPickupRegist(CreateObject(name, 217));
    PotionPickupRegist(CreateObject(name, 221));
    PotionPickupRegist(CreateObject(name, 224));
    PotionPickupRegist(CreateObject(name, 230));
    PotionPickupRegist(CreateObject(name, 231));
    PotionPickupRegist(CreateObject(name, 227));
    PotionPickupRegist(CreateObject(name, 93));
    PotionPickupRegist(CreateObject(name, 92));
    PotionPickupRegist(CreateObject(name, 439));
    PotionPickupRegist(CreateObject(name, 440));
    PotionPickupRegist(CreateObject(name, 441));
    PotionPickupRegist(CreateObject(name, 442));
    PotionPickupRegist(CreateObject(name, 443));
    PotionPickupRegist(CreateObject(name, 444));
    FrameTimerWithArg(1, name, MapPotionPlace4);
}

void MapPotionPlace4(string name)
{
    int k;
    for (k = 445 ; k <= 464 ; k ++)
        PotionPickupRegist(CreateObject(name, k));
}

void SetStartSwitch(int stat)
{
    int ptr;

    if (!ptr) ptr = Object("MainGameSw");
    if (!stat)
        ObjectOff(ptr);
    else
        ObjectOn(ptr);
}

void PlayerInit(int plr, int plrUnit)
{
    player[plr] = plrUnit;
    player[plr + 10] = 1;
    DiePlayerHandlerEntry(plrUnit);
    SelfDamageClassEntry(plrUnit);
    ChangeGold(plrUnit, -GetGold(plrUnit));
    UniPrintToAll(PlayerIngameNick(plrUnit) + " 님께서 지도에 입장하셨습니다");
}

void PlayerEntryPoint()
{
    int plr, k;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            if (MaxHealth(OTHER) == 150)
            {
                plr = CheckPlayer();

                for (k = 9 ; k >= 0 && plr < 0 ; k --)
                {
                    if (!MaxHealth(player[k]))
                    {
                        PlayerInit(k, GetCaller());
                        plr = k;
                        break;
                    }
                }
                if (plr >= 0)
                {
                    PlayerJoin(plr);
                    break;
                }
            }
        }
        CantJoin();
        break;
    }
}

void PlayerJoin(int plr)
{
    if (PlayerClassCheckDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    MoveObject(player[plr], LocationX(51), LocationY(51));
    DeleteObjectTimer(CreateObject("BlueRain", 51), 10);
    Effect("TELEPORT", LocationX(51), LocationY(51), 0.0, 0.0);
}

void CantJoin()
{
    Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    MoveObject(OTHER, LocationX(50), LocationY(50));
    UniPrint(OTHER, "이 맵은 전사만 입장할 수 있습니다.");
    UniPrint(OTHER, "또한 기술적 한계로 인하여 이 맵은 최대 10명 까지만 입장가능합니다.");
}

void EmptyAll(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void GreenSparkFx(int wp)
{
    int ptr = CreateObject("MonsterGenerator", wp);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void GreenSparkFxAt(float sX, float sY)
{
    int fxGen = CreateObjectAt("MonsterGenerator", sX, sY);

    Damage(fxGen, 0, 10, 100);
    Delete(fxGen);
}

int PlayerCheckFlagSkill2(int plr)
{
    return player[plr + 10] & 0x02;
}

void PlayerSetFlagSkill2(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x02;
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & 0x04;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x04;
}

void PlayerOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

void CheckAwardNewSkill(int plr)
{
    int inv = GetLastItem(player[plr]);

    if (GetUnitThingID(inv) == 2676)
    {
        MoveWaypoint(1, GetObjectX(player[plr]), GetObjectY(player[plr]));
        GreenSparkFx(1);
        AudioEvent("AwardSpell", 1);
        player[plr + 10] = player[plr + 10] ^ 2;
        Delete(inv);
        UniPrint(player[plr], "새로운 능력을 배웠습니다!");
        UniPrint(player[plr], "조심스럽게 걷기를 시전하면 새로운 능력이 발동됩니다");
    }
}

void PlayerOnFree(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void LoopPreservePlayers()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    if (UnitCheckEnchant(player[i], GetLShift(31)))
                    {
                        EnchantOff(player[i], EnchantList(31));
                        RemoveTreadLightly(player[i]);
                        if (PlayerCheckFlagSkill2(i))
                            FrameTimerWithArg(1, player[i], RideVicle);
                    }
                    else if (!PlayerCheckFlagSkill2(i))
                        CheckAwardNewSkill(i);
                    break;
                }
                else
                {
                    if (!PlayerClassCheckDeathFlag(i) && player[i + 10])
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
            {
                PlayerOnFree(i);
            }
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayers);
}

void CollideVicle()
{
    int owner = GetOwner(GetTrigger() + 2);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner) && MaxHealth(SELF))
    {
        CreateObjectAt("CrateBreaking2", GetObjectX(SELF), GetObjectY(SELF));
        Delete(SELF);
        Delete(GetTrigger() + 2);
        Damage(OTHER, owner, 125, 11);
    }
}

void RideVicle(int caster)
{
    int unit;
    float vectX, vectY;

    if (!UnitCheckEnchant(caster, GetLShift(11)))
    {
        UnitSetEnchantTime(caster, 11, 240);
        vectX = UnitAngleCos(caster, 20.0);
        vectY = UnitAngleSin(caster, 20.0);
        unit = DummyUnitCreate("Maiden", GetObjectX(caster) + vectX, GetObjectY(caster) + vectY);
        Frozen(unit, 1);
        SetCallback(unit, 9, CollideVicle);
        UnitNoCollide(CreateObjectAt("AirshipBasket", GetObjectX(unit), GetObjectY(unit)));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), vectX);
        SetOwner(caster, unit + 2);
        LookWithAngle(unit + 2, 40);
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(caster) - vectX, GetObjectY(caster) - vectY), vectY);
        FrameTimerWithArg(1, unit + 2, LoopRideHandling);
    }
}

void LoopRideHandling(int sUnit)
{
    int durate = GetDirection(sUnit), owner = GetOwner(sUnit);

    while (1)
    {
        if (CurrentHealth(owner) && IsVisibleTo(sUnit, sUnit + 1))
        {
            if (durate)
            {
                MoveObject(sUnit, GetObjectX(sUnit) + GetObjectZ(sUnit), GetObjectY(sUnit) + GetObjectZ(sUnit + 1));
                MoveObject(sUnit + 1, GetObjectX(sUnit + 1) + GetObjectZ(sUnit), GetObjectY(sUnit + 1) + GetObjectZ(sUnit + 1));
                MoveObject(sUnit - 1, GetObjectX(sUnit), GetObjectY(sUnit));
                MoveObject(sUnit - 2, GetObjectX(sUnit), GetObjectY(sUnit));
                FrameTimerWithArg(1, sUnit, LoopRideHandling);
                LookWithAngle(sUnit, durate - 1);
                break;
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        Delete(sUnit - 1);
        Delete(sUnit - 2);
        break;
    }
}

void WispDestroyFx(float sX, float sY)
{
    int wisp = CreateObjectAt("WillOWisp", sX, sY);

    UnitNoCollide(wisp);
    Damage(wisp, 0, CurrentHealth(wisp) + 1, -1);
}

void TeleportToHome(int sUnit)
{
    int owner = GetOwner(sUnit);

    if (CurrentHealth(owner) && UnitCheckEnchant(owner, GetLShift(15)))
    {
        EnchantOff(owner, EnchantList(15));
        if (Distance(GetObjectX(owner), GetObjectY(owner), GetObjectX(sUnit), GetObjectY(sUnit)) < 15.0)
        {
            Effect("COUNTERSPELL_EXPLOSION", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            MoveObject(owner, GetWaypointX(167), GetWaypointY(167));
            AudioEvent("HecubahDieFrame98", 167);
            Effect("COUNTERSPELL_EXPLOSION", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            UniPrint(owner, "안전한 피신처로 이동했습니다");
        }
        else
            UniPrint(owner, "공간이동을 취소했습니다");
    }
    Delete(sUnit);
    Delete(sUnit + 1);
}

void HealingPotion(int sUnit)
{
    int durate = GetDirection(sUnit), owner = GetOwner(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(owner))
        {
            if (durate)
            {
                RestoreHealth(owner, 1);
                LookWithAngle(sUnit, durate - 1);
                FrameTimerWithArg(1, sUnit, HealingPotion);
                MoveObject(sUnit, GetObjectX(owner), GetObjectY(owner));
                break;
            }
            WispDestroyFx(GetObjectX(owner), GetObjectY(owner));
        }
        Delete(sUnit);
        break;
    }
}

void RottenMeatClassSetUnitHealth(int unit, int amount)
{
    if (UnitCheckEnchant(unit, GetLShift(23)))
        EnchantOff(unit, EnchantList(23));
    if (UnitCheckEnchant(unit, GetLShift(26)))
        EnchantOff(unit, EnchantList(26));
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void PlayerSendToStartZone()
{
    int unit;

    if (!UnitCheckEnchant(OTHER, GetLShift(15)))
    {
        Delete(SELF);
        UnitSetEnchantTime(OTHER, 15, 30 * 7);
        PlaySoundAround(OTHER, 592);
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(OTHER, CreateObjectAt("VortexSource", GetObjectX(unit), GetObjectY(unit)) - 1);
        FrameTimerWithArg(60, unit, TeleportToHome);
        UniPrint(OTHER, "부활터로 귀환을 시도하고 있습니다, 취소하려면 움직이세요");
    }
    else
        UniPrint(OTHER, "쿨다운 입니다...");
}

void ApplyShockField()
{
    Delete(SELF);
    UnitSetEnchantTime(OTHER, 22, 30 * 60);
}

void UseHealingPotion()
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));

    FrameTimerWithArg(1, unit, HealingPotion);
    SetOwner(OTHER, unit);
    LookWithAngle(unit, 250);
    SetUnitEnchantCopy(unit, GetLShift(8) | GetLShift(26) | GetLShift(4));
    GreenSparkFxAt(GetObjectX(OTHER), GetObjectY(OTHER));
    Delete(SELF);
    UniPrint(OTHER, "힐링포션을 사용했습니다. 잠시동안 체력이 빠르게 회복됩니다 (중첩가능)");
}

void UseMeteorShowerStone()
{
    CastSpellObjectLocation("SPELL_METEOR_SHOWER", OTHER, GetObjectX(OTHER), GetObjectY(OTHER));
    Delete(SELF);
    UniPrint(OTHER, "메테오 샤워를 시전합니다");
}

void UseRottenMeat()
{
    int ptr = UnitToPtr(OTHER);

    if (ptr)
    {
        if (GetMemory(ptr + 0x08) & 4)
        {
            RottenMeatClassSetUnitHealth(OTHER, MaxHealth(OTHER) * 2);
            UniPrint(OTHER, "역시 썩은고기! 썩은고기로 인해 현재 체력 수치가 최대체력의 2배가 됩니다");
        }
    }
    Delete(SELF);
}

int Sentry()
{
    int trap;

    if (!trap)
    {
        trap = CreateObject("SentryGlobeMovable", 47);
        ObjectOff(trap);
    }
    return trap;
}

void SentryTrick(int unit)
{
    int me = ToInt(GetObjectZ(unit)), xray, ptr;

    if (CurrentHealth(me))
    {
        xray = Sentry();
        ptr = UnitToPtr(xray);
        MoveObject(xray, GetObjectX(me) - UnitRatioX(me, unit, 23.0), GetObjectY(me) + UnitRatioY(me, unit, 23.0));
        LookAtObject(me, unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 4, ToInt(3.141592 / 180.0 * IntToFloat(GetDirection(me)) * 45.0 / 32.0));
        SetOwner(me, xray);
        FrameTimerWithArg(10, xray, BackToPosition);
        FrameTimerWithArg(10, me, EnableFinalBoss);
    }
    Delete(unit);
}

void BackToPosition(int unit)
{
    ObjectOff(unit);
    MoveObject(unit, GetWaypointX(47), GetWaypointY(47));
}

void EnableFinalBoss(int unit)
{
    ObjectOn(unit);
    EnchantOff(unit, "ENCHANT_INVULNERABLE");
    EnchantOff(unit, "ENCHANT_FREEZE");
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int FixCreateMover(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, m_LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, m_LastUnitID);
        }
    }
    m_LastUnitID ++;

    return unitMover;
}

void HiveDestroy()
{
    float xProfile = GetObjectX(SELF), yProfile = GetObjectY(SELF);

    PlaySoundAround(SELF, 292);
    CreateObjectAt("WaspNestDestroy", xProfile, yProfile);
    Delete(SELF);
    if (Random(0, 1))
    {
        SetUnitMaxHealth(CreateObjectAt("Wasp", xProfile, yProfile), 64);
        SetUnitMaxHealth(CreateObjectAt("Wasp", xProfile, yProfile), 64);
        SetUnitMaxHealth(CreateObjectAt("Wasp", xProfile, yProfile), 64);
        SetUnitMaxHealth(CreateObjectAt("Wasp", xProfile, yProfile), 64);
    }
    else
    {
        MoveWaypoint(1, xProfile, yProfile);
        RewardMarkerFunc(1, 7);
    }
}

int SpawnWaspNest(int wp)
{
    int hive = CreateObject("WaspNest", wp);
    int ptr = GetMemory(0x750710);

    SetUnitMaxHealth(hive, 600);
    SetUnitCallbackOnDeath(hive, HiveDestroy);
    return hive;
}

void PutBones(int wp, int amount)
{
    int k;

    for (k = amount ; k ; k --)
        CreateObject(Bones(), wp);
}

string Bones()
{
    int k;
    string name[19];

    if (!k)
    {
        k = 1;
        name[0] = "CorpseLeftLowerArm"; name[1] = "CorpseLeftLowerLeg"; name[2] = "CorpseLeftUpperArm";
        name[3] = "CorpseLeftUpperLeg"; name[4] = "CorpsePelvis"; name[5] = "CorpseRibCage";
        name[6] = "CorpseRightLowerArm"; name[7] = "CorpseRightLowerLeg"; name[8] = "CorpseRightUpperArm";
        name[9] = "CorpseRightUpperLeg"; name[10] = "CorpseSkull";
        name[11] = "E"; name[12] = "N"; name[13] = "NE"; name[14] = "NW"; name[15] = "S"; name[16] = "SE"; name[17] = "SW"; name[18] = "W";
        return "NULL";
    }
    return name[Random(0, 10)] + name[Random(11, 18)];
}

void DelayDisableUnit(int unit)
{
    ObjectOff(unit);
}

int CreateMagicMissile(int wp, int owner)
{
	int mis = CreateObject("MagicMissile", wp);
    int ptr = GetMemory(0x750710);

	SetMemory(GetMemory(ptr + 0x2ec), owner);
	SetMemory(ptr + 0x1fc, owner);

    return mis;
}

#define USER_START_LOCATION 166
// #define USER_START_LOCATION 355

void FieldTeleport()
{
    if (IsPlayerUnit(OTHER))
    {
        if (!GetDirection(SELF))
            MoveObject(OTHER, GetWaypointX(USER_START_LOCATION), GetWaypointY(USER_START_LOCATION));
        else
            MoveObject(OTHER, GetWaypointX(167), GetWaypointY(167));
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

int SpawnToxicCloud(int wp, int time)
{
	int cloud = CreateObject("ToxicCloud", wp);
    int ptr = GetMemory(0x750710);

	SetMemory(GetMemory(ptr + 0x2ec), time);
    return cloud;
}

string ArmorList()
{
    string table[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "ChainCoif", "ChainLeggings", "ChainTunic",
        "MedievalCloak", "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", "MedievalPants", "MedievalShirt",
        "LeatherHelm", "SteelShield", "WoodenShield"
    };
    return table[Random(0, sizeof(table)-1)];
}

string WeaponList()
{
    string table[] = {
        "WarHammer", "GreatSword", "StaffWooden", "RoundChakram", "FanChakram", "MorningStar", "OgreAxe", "BattleAxe",
        "Longsword", "Sword", "OblivionHalberd", "OblivionHeart", "OblivionWierdling"
    };
    return table[Random(0, sizeof(table)-1)];
}

int SpawnGold(int wp)
{
    string name[] = {"QuestGoldChest", "QuestGoldPile", "Gold"};
    int unit = CreateObject(name[Random(0, sizeof(name)-1)], wp);

    UnitStructSetGoldAmount(unit, Random(200, 1000));    
    return unit;
}

int CreateRandomWeapon(int wp)
{
    int k, weapon = CreateObject(WeaponList(), wp);

    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));

    int thingId = GetUnitThingID(weapon);

    if (thingId == 1168 || thingId == 1178)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
    else if (thingId >= 222 && thingId <= 225)
    {
        SetItemPropertyAllowAllDrop(weapon);
        DisableOblivionItemPickupEvent(weapon);
    }
    return weapon;
}

int CreateRandomArmor(int wp)
{
    int k, armor = CreateObject(ArmorList(), wp);

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));    
    return armor;
}

int MagicalPotionCreate(int wp)
{
    string name[] = {
        "RedPotion", "CurePoisonPotion", "VampirismPotion", "BlackPotion", "YellowPotion",
        "Meat", "Soup", "RedApple", "HastePotion", "PoisonProtectPotion",
        "FireProtectPotion", "ShockProtectPotion", "InfravisionPotion", "InvulnerabilityPotion", "ShieldPotion",
        "RedPotion2"
    };
    return CheckPotionThingID(CreateObject(name[Random(0, sizeof(name)-1)], wp));
}

int HotPotionCreate(int wp)
{
    return PotionPickupRegist(CreateObject("RedPotion", wp));
}

int GermCreate(int wp)
{
    string germ[] = {"Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Diamond"};

    return CreateObject(germ[Random(0, sizeof(germ)-1)], wp);
}

int MagicalItemCreate(int location)
{
    string magicName[] = {"AmuletofManipulation", "Fear", "BottleCandle", "ProtectionFire", "RottenMeat"};
    int execFunctions[] = {PlayerSendToStartZone, ApplyShockField, UseHealingPotion, UseMeteorShowerStone, UseRottenMeat};
    int rndPick = Random(0, 4);
    int magic = CreateObjectAt(magicName[rndPick], LocationX(location), LocationY(location));
    
    SetItemPropertyAllowAllDrop(magic);
    SetUnitCallbackOnUseItem(magic, execFunctions[rndPick]);

    return magic;
}

int ChestKeyDrop(int location)
{
    return CreateObject("Befuddle", location);
}

void RewardMarkerFunc(int wp, int maxRange)
{
    int unit = CreateObject("InvisibleLightBlueHigh", wp);

    Raise(unit, SpawnGold);
    CallFunctionWithArgInt(m_itemCreateExecTable[ Random(0, maxRange) ], wp);
    Delete(unit);
}

void DummyFunction()
{
    return;
}

float GetRatioUnitWpX(int unit, int wp, float size)
{
    return (GetObjectX(unit) - GetWaypointX(wp)) / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp)) * size;
}

float GetRatioUnitWpY(int unit, int wp, float size)
{
    return (GetObjectY(unit) - GetWaypointY(wp)) / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp)) * size;
}

float UnitWpRatioX(int unit, int wp, float size)
{
	return (GetObjectX(unit) - GetWaypointX(wp)) / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp)) * size;
}

float UnitWpRatioY(int unit, int wp, float size)
{
	return (GetObjectY(unit) - GetWaypointY(wp)) / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp)) * size;
}

int CheckUnitMapBoundary(int unit)
{
    float pos_x = GetObjectX(unit);
    float pos_y = GetObjectY(unit);

    if (pos_x > 100.0 && pos_y > 100.0 && pos_x < 5532.0 && pos_y < 5600.0)
        return 1;
    else
        return 0;
}

void StrVictory()
{
	int arr[13];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
	{
		drawStrVictory(arr[i], name);
		i ++;
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(431);
		pos_y = GetWaypointY(431);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 431), 1);
		if (count % 38 == 37)
			MoveWaypoint(431, GetWaypointX(431) - 111.000000, GetWaypointY(431) + 3.000000);
		else
			MoveWaypoint(431, GetWaypointX(431) + 3.000000, GetWaypointY(431));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(431, pos_x, pos_y);
	}
}

void StrFailMission()
{
	int arr[22];
	string name = "HealOrb";
	int i = 0;
	arr[0] = 270598782; arr[1] = 285235140; arr[2] = 272171586; arr[3] = 285233156; arr[4] = 673251906; arr[5] = 285233796; arr[6] = 1210221122; arr[7] = 285233796; arr[8] = 71074370; arr[9] = 285233797; 
	arr[10] = 36192834; arr[11] = 285241984; arr[12] = 2082480706; arr[13] = 285233799; arr[14] = 2097730; arr[15] = 21124; arr[16] = 2080391746; arr[17] = 285233799; arr[18] = 67125886; arr[19] = 293361600; 
	arr[20] = 2084553216; arr[21] = 20487; 
	while(i < 22)
	{
		drawStrFailMission(arr[i], name);
		i ++;
	}
}

void drawStrFailMission(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(433);
		pos_y = GetWaypointY(433);
	}
	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 433);
		if (count % 62 == 61)
			MoveWaypoint(433, GetWaypointX(433) - 122.000000, GetWaypointY(433) + 2.000000);
		else
			MoveWaypoint(433, GetWaypointX(433) + 2.000000, GetWaypointY(433));
		count ++;
	}
	if (count >= 682)
	{
		count = 0;
		MoveWaypoint(433, pos_x, pos_y);
	}
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

int ImportMonsterStrikeFunc()
{
	int arr[14], link;

	if (!link)
	{
		arr[0] = 0x448B5650; arr[1] = 0xC0850C24; arr[2] = 0xB08B2574; arr[3] = 0x000002EC; arr[4] = 0x1B74F685; arr[5] = 0x0830B68B; arr[6] = 0xFE830000;
		arr[7] = 0x68107C00; arr[8] = 0x00507310; arr[9] = 0x56006A50; arr[10] = 0x0C2454FF; arr[11] = 0x5E10C483; arr[12] = 0x93806858; arr[13] = 0x90C30054;
		link = &arr;
	}
	return link;
}

int GetVictimUnit()
{
	int ptr = GetMemory(0x834a40);

	if (ptr)
		return GetMemory(ptr + 0x2c);
	return 0;
}

void RegistryUnitStrikeFunction(int sUnit, int sFunc)
{
    int ptr = UnitToPtr(sUnit), temp, binScrPtr;

    if (ptr)
    {
        temp = GetMemory(ptr + 0x2ec);
        if (temp)
        {
            binScrPtr = GetMemory(GetMemory(ptr + 0x2ec) + 0x1e4);
            if (binScrPtr)
            {
                SetMemory(binScrPtr + 0xec, ImportMonsterStrikeFunc());
                SetMemory(temp + 0x830, sFunc);
            }
        }
    }
}

void MovingTestStart()
{
    int obj = CreateObject("BlueRain", 490);

    FixCreateMover(obj, 487, 60.0);
}

int HiddenShopClassFind(string headerName)
{
    int pic;
    int unit = Object(headerName + IntToString(pic + 1));

    if (unit)
    {
        pic ++;
        return unit;
    }
    return 0;
}

int PlacingStaticSprite(float sX, float sY, int thingId)
{
    int sprite = CreateObjectAt("AirshipBasketShadow", sX, sY);
    int *ptr = UnitToPtr(sprite);

    ptr[1] = thingId;
    return sprite;
}

int PlacingStaticSpriteByVirtCoor(float *coorPtr, float hardX, float hardY, int thingId)
{
    int sprite = CreateObjectAt(
        "AirshipBasketShadow",
        coorPtr[0] + hardX - coorPtr[2],
        coorPtr[1] + hardY - coorPtr[3]);
    int *ptr = UnitToPtr(sprite);

    ptr[1] = thingId;
    return sprite;
}

void PlaceShopDecoration(float sX, float sY)
{
    float virX = 1599.0, virY = 3391.0;
    int zeroSet = PlacingStaticSprite(sX, sY, 1870);

    PlacingStaticSpriteByVirtCoor(&sX, 1631.0, 3316.0, 1881);
    PlacingStaticSpriteByVirtCoor(&sX, 1608.0, 3351.0, 1867);
    PlacingStaticSpriteByVirtCoor(&sX, 1625.0, 3329.0, 1768);
    PlacingStaticSpriteByVirtCoor(&sX, 1608.0, 3370.0, 1883);
    PlacingStaticSpriteByVirtCoor(&sX, 1555.0, 3384.0, 1866);
    PlacingStaticSpriteByVirtCoor(&sX, 1547.0, 3344.0, 1871);
    PlacingStaticSpriteByVirtCoor(&sX, 1529.0, 3336.0, 1871);
    PlacingStaticSpriteByVirtCoor(&sX, 1545.0, 3431.0, 1882);
    PlacingStaticSpriteByVirtCoor(&sX, 1510.0, 3428.0, 1880);
    PlacingStaticSpriteByVirtCoor(&sX, 1498.0, 3469.0, 1765);
}

void HiddenShopClassNotify(int mketUnit)
{
    string repMent[] = {
        "조아써! 오늘은 이곳이다!\n곧 바로 장사를 게시하겠다...",
        "나이스! 오늘 왠지 천만원 이상 벌것 같은데?",
        "초대형 로프록 마트! GRAND OPEN!",
        "어서옵쇼!"};

    UniChatMessage(mketUnit, repMent[Random(0, 3)], 180);
    PlaceShopDecoration(GetObjectX(mketUnit) - 10.0, GetObjectY(mketUnit) - 10.0);
    RepairShopCreate(GetObjectX(mketUnit) - 23.0, GetObjectY(mketUnit) + 23.0);
}

void HiddenShopClassTeleport(int sUnit)
{
    int mket = HiddenShopClassFind("HiddenMarket");

    if (mket)
    {
        MoveObject(mket, GetObjectX(sUnit), GetObjectY(sUnit));
        PlaySoundAround(mket, 486);
        FrameTimerWithArg(15, mket, HiddenShopClassNotify);
        Effect("SMOKE_BLAST", GetObjectX(mket), GetObjectY(mket), 0.0, 0.0);
        Effect("JIGGLE", GetObjectX(mket), GetObjectY(mket), 12.0, 0.0);
        Delete(sUnit);
    }
}

int FindRubyKey(int unit)
{
	int inv = GetLastItem(unit);

	while (inv)
	{
		if (GetUnitThingID(inv) == 240)
			return inv;
		inv = GetPreviousItem(inv);
	}
	return 0;
}

void ChestCollide()
{
	int *cFps = 0x84ea04;
	int *ptr = UnitToPtr(SELF);

	if (ptr && IsPlayerUnit(OTHER))
	{
		if (ABS(cFps[0] - ptr[185]) > 30)
		{
            ptr[185] = cFps[0];
            if (!ptr[188])
			{
				if (FindRubyKey(OTHER))
				{
					Delete(FindRubyKey(OTHER));
					int item = CreateObjectAt("RewardMarker", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 32.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 32.0));
                    SpecialUnitRewardMarker(item);
                    SetUnitFlags(SELF, GetUnitFlags(SELF) ^ UNIT_FLAG_DEAD);
                    ptr[188] = TRUE;
					UniPrint(OTHER, "암흑의 소용돌이를 사용하여 잠긴 금고를 열었습니다");
				}
				else
				{
					PlaySoundAround(OTHER, 1012);
					UniPrint(OTHER, "이 금고를 열려면 암흑의 소용돌이가 필요합니다");
				}
			}
		}
	}
}

void OgrePartChestCollide()
{
    int *cFps = 0x84ea04;
	int *ptr = UnitToPtr(SELF);

	if (ptr && IsPlayerUnit(OTHER))
	{
		if (ABS(cFps[0] - ptr[185]) > 30)
		{
            ptr[185] = cFps[0];
			if (!ptr[188])
			{
                ptr[188] = TRUE;
                SetUnitFlags(SELF, GetUnitFlags(SELF) ^ UNIT_FLAG_DEAD);
                int item = CreateObjectAt("SilverKey", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 32.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 32.0));
                PlaySoundAround(item, 821);
                UniPrint(OTHER, "금고를 열었습니다");
			}
		}
	}
}

void RespectShopkeeper()
{
    int *cFps = 0x84ea04;
	int *ptr = UnitToPtr(SELF), key, item;

	if (ptr && IsPlayerUnit(OTHER))
	{
		if (ABS(cFps[0] - ptr[185]) > 30)
		{
            ptr[185] = cFps[0];
            if (!ptr[188])
			{
				if (FindRubyKey(OTHER))
				{
					Delete(FindRubyKey(OTHER));
					FrameTimerWithArg(18,
                        CreateObjectAt("RewardMarkerPlus", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 32.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 32.0)),
                        HiddenShopClassTeleport);
                    ptr[188] = TRUE;
                    SetUnitFlags(SELF, GetUnitFlags(SELF) ^ UNIT_FLAG_DEAD);
					UniPrint(OTHER, "암흑의 소용돌이를 사용하여 잠긴 금고를 열었습니다");
				}
				else
				{
					PlaySoundAround(OTHER, 1012);
					UniPrint(OTHER, "이 금고를 열려면 암흑의 소용돌이가 필요합니다");
				}
			}
		}
	}
}

void AllChestInit(int multiple)
{
    string chestName = "MyChest";
    int i;

    multiple *= 10;
    for (i = 0 ; i < 10 ; Nop( i ++) )
        SetUnitCallbackOnCollide(Object(chestName + IntToString(multiple + i + 1)), ChestCollide);
}

void HiddenMarketChest(string chestName, int max)
{
    int i;

    for (i = 0 ; i < max ; i ++)
        SetUnitCallbackOnCollide(Object(chestName + IntToString(i + 1)), RespectShopkeeper);
}

void GoOgrePartBox(int chestObj)
{
    SetUnitCallbackOnCollide(chestObj, OgrePartChestCollide);
}

void UpdateRepairItem(int plrIndex, int item)
{
    int *pCode, *targetBuiltins = 0x5c3108, *ptr = UnitToPtr(item);

    if (!pCode)
    {
        int codes[] = {
            0x50685056, 0xFF005072, 0x708B2414, 0x04C48304, 0x4D87A068, 0x30FF5600, 0x082454FF,
            0x580CC483, 0x9090C35E };
		pCode = &codes;
    }
    if (ptr)
    {
        int oldProc = targetBuiltins[0];
        targetBuiltins[0] = pCode;
        int args[] = {plrIndex, ptr};
        Unused1f(&args);
        targetBuiltins[0] = oldProc;
    }
}

int RepairAll(int unit)
{
    int inv = GetLastItem(unit), rCount = 0, pIndex = GetPlayerIndex(unit);

    if (pIndex < 0) return 0;
    while (inv)
    {
        if (MaxHealth(inv) ^ CurrentHealth(inv))
        {
            RestoreHealth(inv, MaxHealth(inv) - CurrentHealth(inv));
            UpdateRepairItem(pIndex, inv);
            rCount += 1;
        }
        inv = GetPreviousItem(inv);
    }
    return rCount;
}

void RepairShopDesc()
{
    TellStoryUnitName("aa", "GuiInv.c:ShopRepair", "인벤토리 전체 수리: 1,000");
    UniPrint(OTHER, "금화 1,000에 인벤토리를 전체 수리하시겠어요");
}

void RepairShopTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 1000)
    {
        int res = RepairAll(OTHER);
        if (res)
        {
            ChangeGold(OTHER, -1000);
            UniPrint(OTHER, IntToString(res) + "개의 아이템이 수리되었습니다 (-1,000 골드 차감)");
        }
        else
            UniPrint(OTHER, "이미 모든 아이템이 수리된 상태입니다");
    }
    else
        UniPrint(OTHER, "거래가 취소되었습니다. 잔액이 부족합니다");
}

int RepairShopCreate(float sX, float sY)
{
    int repair = DummyUnitCreate("Archer", sX, sY);

    SetDialog(repair, "YESNO", RepairShopDesc, RepairShopTrade);
    return repair;
}

