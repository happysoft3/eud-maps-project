
#include "libs/define.h"
#include "libs\printutil.h"
#include "libs\unitstruct.h"
#include "libs\reaction.h"
#include "libs\spellutil.h"
#include "libs\buff.h"
#include "libs\mathlab.h"
#include "libs\fxeffect.h"
#include "libs\waypoint.h"
#include "libs\username.h"
#include "libs\playerupdate.h"
#include "libs\coopteam.h"
#include "libs\itemproperty.h"
#include "libs/format.h"
#include "libs/fixtellstory.h"
#include "libs/network.h"
#include "libs/imageutil.h"
#include "libs/gui_window.h"
#include "libs/logging.h"
#include "libs/anti_charm.h"
#include "libs/weaponcapacity.h"
#include "libs/hash.h"
#include "libs/weapon_effect.h"
#include "libs/queueTimer.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"

#define PLAYER_DEATH_FLAG 0x80000000

int m_potFnHash;
int m_genericHash;
int *m_pImgVector;
int *m_pWeaponUserSpecialPropertyR;
int *m_pWeaponUserSpecialPropertyExecutorR;
int *m_pWeaponUserSpecialPropertyFxCodeR;

int *m_pWeaponUserSpecialPropertyJ;
int *m_pWeaponUserSpecialPropertyExecutorJ;
int *m_pWeaponUserSpecialPropertyFxCodeJ;

int *m_pWeaponUserSpecialPropertyB;
int *m_pWeaponUserSpecialPropertyExecutorB;
int *m_pWeaponUserSpecialPropertyFxCodeB;

int *m_homeItemCreateFn;
int *m_homePotCreateFn;

int m_player[10];
int m_pFlags[10];
int DEATHS = 90;
char m_pUPGRADE[10];

int m_dialogCtx;

static int CheckCoorValidate(float x, float y)
{
    if (x<100.0)
        return FALSE;
    if (y<100.0)
        return FALSE;
    if (x>5750.0)
        return FALSE;
    if (y>5750.0)
        return FALSE;
    return TRUE;
}

void UnitVisibleSplashA()
{
    int parent = GetOwner(SELF);
    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (CurrentHealth(GetOwner(parent)))
    {
        if (GetUnit1C(OTHER) ^ spIdx)
        {
            if (DistanceUnitToUnit(SELF, OTHER) <= GetObjectX(parent))
            {
                Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
                SetUnit1C(OTHER, spIdx);
            }
        }
    }
}

void SplashDamageAt(int owner, int dam, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, SplashIdx;

    SplashIdx ++;
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(dam));

    int k = -1;
    while (++k < 4)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 64);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplashA);
    }
    DeleteObjectTimer(ptr - 1, 2);
    DeleteObjectTimer(ptr - 2, 2);
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlags[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlags[plr] ^= flags;
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

static void deferredEnableObject(int obj)
{
    if (ToInt(GetObjectX(obj)))
        ObjectOn(obj);
}

static void deferredObjectOn(int delay, int obj)
{
    PushTimerQueue(delay, obj, deferredEnableObject);
    ObjectOff(obj);
}

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10
#define _CLIENT_OPTION_OFF1_ 0x753B11
#define _CLIENT_OPTION_OFF2_ 0x753B12

static void onClientPopupMessage1()
{
    GUISetWindowScrollListboxText(m_dialogCtx, "당신의 창고를 업그레이드 하시겠어요? 10,000 골드가 필요합니다", 0);
}
static void onClientPopupMessage2()
{
    GUISetWindowScrollListboxText(m_dialogCtx, "번개 메이스를 구입하시겠어요? 10,000 골드가 필요합니다", 0);
}
static void onClientPopupMessage3()
{
    GUISetWindowScrollListboxText(m_dialogCtx, "화살비 서드를 구입하시겠어요? 20,000 골드가 필요합니다", 0);
}
static void onClientPopupMessage4()
{
    GUISetWindowScrollListboxText(m_dialogCtx, "멀티플 에로우 서드를 구입하시겠어요? 18,000 골드가 필요합니다", 0);
}
static void onClientPopupMessage5()
{
    GUISetWindowScrollListboxText(m_dialogCtx, "무기와 갑옷 그리고 휴게공간이 있는 창고로 이동하시겠어요?", 0);
}
static void onClientPopupMessage6()
{
    GUISetWindowScrollListboxText(m_dialogCtx, "망각의 지팡이를 구입하실래요? 가격은 30,000 골드 입니다", 0);
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[0])
    {
        if (type[0]==1)
            onClientPopupMessage1();
        else if (type[0]==2)
            onClientPopupMessage2();
        else if (type[0]==3)
            onClientPopupMessage3();
        else if (type[0]==4)
            onClientPopupMessage4();
        else if (type[0]==5)
            onClientPopupMessage5();
        else if (type[0]==6)
            onClientPopupMessage6();
        type[0]=0;
    }
    FrameTimer(3, ClientProcLoop);
}

static void imagePortGirl()
{ }
static void imagePortHorr()
{ }
static void imagePortArcher()
{ }
static void imagePortLitz()
{ }
static void imageAmulet()
{ }
static void imagePortGranpar()
{ }

#define SENTRY_COLOR_BLUE 0x2FE0
void ClientsideProcess()
{
    m_pImgVector = CreateImageVector(8);

    InitializeImageHandlerProcedure(m_pImgVector);
    int index=0;
    AddImageFromResource(m_pImgVector, GetScrCodeField(imagePortGirl)+4, 14982, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imagePortHorr)+4, 14970, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imagePortArcher)+4, 14985, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imagePortLitz)+4, 14986, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imageAmulet) + 4, 112960, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(imagePortGranpar) + 4, 14974, index++);
    short *pSentryColor=0x7170D8;

    pSentryColor[0]=SENTRY_COLOR_BLUE;
    pSentryColor[1]=SENTRY_COLOR_BLUE;
}

void InitDialog()
{
    int *wndptr=0x6e6a58;
    GUIFindChild(wndptr[0], 3901, &m_dialogCtx);
}

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    ClientsideProcess();
    InitDialog();
    FrameTimer(10, ClientProcLoop);
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[27] = 1; arr[28] = 1106247680; 
		arr[29] = 80; arr[30] = 1101004800; arr[31] = 8; arr[32] = 22; arr[33] = 30; 
		arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1069547520; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; 
		arr[54] = 4; arr[55] = 20; arr[56] = 30; arr[57] = 5547984; 
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 85; 
		arr[18] = 50; arr[19] = 55; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 40; arr[31] = 2; arr[32] = 8; 
		arr[33] = 16; arr[57] = 5548112; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 130; arr[18] = 45; 
		arr[19] = 85; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; 
		arr[27] = 3; arr[28] = 1097859072; arr[29] = 100; arr[31] = 8; arr[32] = 13; 
		arr[33] = 21; arr[34] = 1; arr[35] = 5; arr[36] = 20; arr[37] = 1684631635; 
		arr[38] = 1884516965; arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; 
		arr[59] = 5544896; arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int LichBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[17] = 306; arr[19] = 60; arr[21] = 1065353216; arr[23] = 34816; 
		arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; arr[28] = 1106771968; arr[29] = 35; 
		arr[31] = 10; arr[32] = 8; arr[33] = 11; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1342; arr[61] = 46909440; 
	pArr = arr;
	return pArr;
}

void LichSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 306;	hpTable[1] = 306;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int HecubahBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 350; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 50; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; 
		arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void HecubahSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 350;	hpTable[1] = 350;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = HecubahBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CarnivorousPlantBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 325; 
		arr[19] = 95; arr[21] = 1065353216; arr[24] = 1065353216; arr[27] = 5; arr[28] = 1109393408; 
		arr[29] = 60; arr[31] = 8; arr[59] = 5542784; arr[60] = 1371; arr[61] = 46901760; 
	pArr = arr;
	return pArr;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077306982;		ptr[137] = 1077306982;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = CarnivorousPlantBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int WillOWispBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1819044183; arr[1] = 1936283471; arr[2] = 112; arr[17] = 50; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1069547520; arr[28] = 1113325568; arr[29] = 2; 
		arr[31] = 9; arr[32] = 16; arr[33] = 22; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1326; arr[61] = 46913280; 
	pArr = &arr;
	return pArr;
}

void WillOWispSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = 50;	hpTable[1] = 50;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = WillOWispBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        arr[5] = GoonSubProcess; arr[72] = MonsterStrongWhiteWizProcess; arr[30] = MonsterWeirdlingBeastProcess; arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process;
        arr[OBJ_LICH%97]=LichSubProcess; arr[OBJ_HECUBAH%97]=HecubahSubProcess; arr[OBJ_CARNIVOROUS_PLANT%97]=CarnivorousPlantSubProcess;
        arr[OBJ_WILL_O_WISP%97]=WillOWispSubProcess;
    }
    if (thingID)
    {
        AntiCharmSetMonster(unit);
        if (arr[key] != 0)
        {
            CallFunctionWithArg(arr[key], unit);
            return TRUE;
        }
    }
    return FALSE;
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 275; arr[19] = 70; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 15; arr[30] = 1092616192; 
		arr[31] = 5; arr[32] = 20; arr[33] = 28; arr[34] = 2; arr[35] = 3; 
		arr[36] = 3; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 275;	hpTable[1] = 275;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
        UnitZeroFleeRange(unit);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

static void stopChainLightning(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        WispDestroyFX(GetObjectX(sub), GetObjectY(sub));
        Delete(sub);
        Delete(sub+1);
    }
}

static void doChainLightning(int owner)
{
    float x=GetObjectX(owner),y=GetObjectY(owner);
    int sub=CreateObjectAt("MovableStatueVictory4S", x,y);
    int to= CreateObjectAt("Orb", x+UnitAngleCos(owner, 14.0),y+UnitAngleSin(owner,14.0));

    LookWithAngle(sub, GetDirection(owner));
    SecondTimerWithArg(3, sub, stopChainLightning);
    Frozen(to, TRUE);
    UnitNoCollide(sub);
    UnitNoCollide(to);
    SetOwner(owner, sub);
    CastSpellObjectObject("SPELL_CHAIN_LIGHTNING", sub, to);
}

static void startChainLightning()
{
    if (!UnitCheckEnchant(OTHER,GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    {
        Enchant(OTHER, EnchantList(ENCHANT_PROTECT_FROM_MAGIC), 1.5);
        doChainLightning(OTHER);
    }
}

int DispositionChainLightningMace(float x, float y)
{
    int sd=CreateObjectAt("MorningStar", x,y);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_Speed4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyR);
    return sd;
}

static void dropRainSingle(float xpos, float ypos)
{
    int angle = Random(0, 359);
        float    rgap=RandomFloat(10.0, 200.0);
        int    arw= CreateObjectAt("CherubArrow", xpos+MathSine(angle+90, rgap),ypos+MathSine(angle,rgap));
            LookWithAngle(arw, 64);
            UnitNoCollide(arw);
            Raise(arw, 250.0);
            DeleteObjectTimer(arw, 20);
            PlaySoundAround(arw, SOUND_CrossBowShoot);
}

static void arrowRainLoop(int sub)
{
    float xpos=GetObjectX(sub),ypos=GetObjectY(sub);
    int dur;

    while (ToInt(xpos))
    {
        dur=GetDirection(sub);
        if (dur)
        {
            dropRainSingle(xpos, ypos);
            if (dur&1)                
                SplashDamageAt(GetOwner(sub), Random(3, 5), xpos, ypos, 190.0);
            PushTimerQueue(1, sub, arrowRainLoop);
            LookWithAngle(sub, dur-1);
            break;
        }
        WispDestroyFX(xpos, ypos);
        Delete(sub);
        break;
    }
}

static void startArrowRain()
{
    int ix,iy;

    if(! GetPlayerMouseXY(OTHER, &ix,&iy))
        return;
    
    float x=IntToFloat(ix), y=IntToFloat(iy);

    if (!CheckCoorValidate(x,y))
        return;

    int sub=CreateObjectAt("PlayerWaypoint", x,y);

    PushTimerQueue(1, sub, arrowRainLoop);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, 42);
    PlaySoundAround(sub, SOUND_MetallicBong);
}

int DispositionArrowrainSword(float x, float y)
{
    int sd=CreateObjectAt("GreatSword", x,y);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyJ);
    return sd;
}

void OnThrowingStoneCollide()
{
    if (!GetTrigger())
        return;

    while (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            Damage(OTHER, SELF, 95, DAMAGE_TYPE_IMPACT);
            break;
        }
        return;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    PlaySoundAround(SELF, SOUND_HitEarthBreakable);
    Delete(SELF);
}

void MultipleThrowingStone()
{
    float xvect = UnitAngleCos(OTHER, 9.0), yvect = UnitAngleSin(OTHER, 9.0);
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + xvect -(yvect*2.0), GetObjectY(OTHER) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(OTHER);

    while (++rep<5)
    {
        CreateObjectAtEx("WeakArcherArrow", GetObjectX(posUnit), GetObjectY(posUnit), &single);
        LookWithAngle(single, dir);
        SetOwner(OTHER, single);
        SetUnitEnchantCopy(single, GetLShift(ENCHANT_SLOWED));
        SetUnitCallbackOnCollide(single, OnThrowingStoneCollide);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
    PlaySoundAround(OTHER, SOUND_UrchinThrow);
}

int DispositionMultipleThrowingStone(float x, float y)
{
    int sd=CreateObjectAt("GreatSword", x,y);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_confuse1, ITEM_PROPERTY_impact4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyB);
    return sd;
}

void InitializeUserSpecialWeapon()
{
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyR);
    SpecialWeaponPropertyExecuteScriptCodeGen(startChainLightning, &m_pWeaponUserSpecialPropertyExecutorR);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyR, m_pWeaponUserSpecialPropertyFxCodeR);
    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_RICOCHET, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeR);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyR, m_pWeaponUserSpecialPropertyExecutorR);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyJ);
    SpecialWeaponPropertyExecuteScriptCodeGen(startArrowRain, &m_pWeaponUserSpecialPropertyExecutorJ);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyJ, m_pWeaponUserSpecialPropertyFxCodeJ);
    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_PLASMA_SPARK, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeJ);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyJ, m_pWeaponUserSpecialPropertyExecutorJ);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeB);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyB);
    SpecialWeaponPropertyExecuteScriptCodeGen(MultipleThrowingStone, &m_pWeaponUserSpecialPropertyExecutorB);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyB, m_pWeaponUserSpecialPropertyFxCodeB);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyB, m_pWeaponUserSpecialPropertyExecutorB);
}

void InitPickets()
{
    RegistSignMessage(Object("PicketHorr"), "호렌더스의 체력을 회복시켜줍니다. 가격은 2만 골드입니다");
    RegistSignMessage(Object("PicketStartGame"), "게임 시작버튼입니다. 최대 20 라운드 까지 있습니다");
    RegistSignMessage(Object("PicketUpgrade"), "창고 업그레이드 입니다. 가격은 만 골드입니다");
    RegistSignMessage(Object("PicketStaff"), "망각 지팡이 구입입니다. 가격은 30,000 골드입니다");
    RegistSignMessage(Object("readable1"), "이곳은 던미르 구내식당입니다- 정량 배식하십시오");
    RegistSignMessage(Object("readable2"), "호렌더스 수호하기- 제작. 231- 게임을 시작하려면 이 문을 열고 안으로 들어오세요");
}

static void tradeChainLightningMace()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    if (GetGold(OTHER) >= 10000)
    {
        ChangeGold(OTHER, -10000);
        DispositionChainLightningMace(GetObjectX(OTHER), GetObjectY(OTHER));
        UniPrint(OTHER, "번개 메이스 구입완료!");
        return;
    }
    UniPrint(OTHER, "번개 메이스 잔액이 부족합니다");
}

void DeferredChangeDialogContext2(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user==(GetHost()))    //Server only
        onClientPopupMessage2();
    else
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 2);
}

static void descChainLightningMace()
{
    PushTimerQueue(1, GetCaller(), DeferredChangeDialogContext2);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "번개 메이스구입");
}

static void tradeArrowRainSword()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    if (GetGold(OTHER) >= 20000)
    {
        ChangeGold(OTHER, -20000);
        DispositionArrowrainSword(GetObjectX(OTHER), GetObjectY(OTHER));
        UniPrint(OTHER, "화살비 서드 구입완료!");
        return;
    }
    UniPrint(OTHER, "화살비 서드를 구입하기에는 당신의 잔액이 부족합니다");
}

void DeferredChangeDialogContext3(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user==(GetHost()))    //Server only
        onClientPopupMessage3();
    else
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 3);
}

static void descRaindropSword()
{
    PushTimerQueue(1, GetCaller(), DeferredChangeDialogContext3);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "화살비 서드구입");
}

static void tradeTripleArrowSword()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    if (GetGold(OTHER) >= 18000)
    {
        ChangeGold(OTHER, -18000);
        DispositionMultipleThrowingStone(GetObjectX(OTHER), GetObjectY(OTHER));
        UniPrint(OTHER, "멀티플 에로우 서드를 구입했습니다! 당신 아래에 있어요");
        return;
    }
    UniPrint(OTHER, "멀티플 에로우 서드를 구입하기에는 당신의 잔액이 부족합니다");
}

void DeferredChangeDialogContext4(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user==(GetHost()))    //Server only
        onClientPopupMessage4();
    else
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 4);
}

static void descTripleArcherSword()
{
    PushTimerQueue(1, GetCaller(), DeferredChangeDialogContext4);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "멀티플에로우 서드구입");
}

void DeferredChangeDialogContext6(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user==(GetHost()))    //Server only
        onClientPopupMessage6();
    else
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 6);
}

static void descOblivionStaff()
{
    PushTimerQueue(1, GetCaller(), DeferredChangeDialogContext6);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "하버드 지팡이구입");
}

static void initialSpecialNpc()
{
    int s1= DummyUnitCreateAt("WizardGreen", LocationX(45), LocationY(45));

    SetDialog(s1, "YESNO", WouldyouWantUpgrade, UpgradeHome);
    StoryPic(s1, "MaidenPic4");
    LookWithAngle(s1, 160);

    int s2= DummyUnitCreateAt("Swordsman", LocationX(46), LocationY(46));

    SetDialog(s2, "YESNO", descChainLightningMace, tradeChainLightningMace);
    StoryPic(s2, "MalePic1");
    LookWithAngle(s2, 32);

    int s3 = DummyUnitCreateAt("Beholder", LocationX(48), LocationY(48));

    SetDialog(s3, "YESNO", descRaindropSword, tradeArrowRainSword);
    StoryPic(s3, "MalePic2");
    LookWithAngle(s3, 32);

    int s4= DummyUnitCreateAt("Archer", LocationX(49), LocationY(49));

    SetDialog(s4, "YESNO", descTripleArcherSword, tradeTripleArrowSword);
    StoryPic(s4, "MalePic1");
    LookWithAngle(s4, 32);

    int s5=DummyUnitCreateAt("WizardWhite", LocationX(260), LocationY(260));

    StoryPic(s5, "HorvathPic");
    LookWithAngle(s5, 160);
    SetDialog(s5, "YESNO", descOblivionStaff, BuyObilivStaff);
}

void MapExit()
{
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
    MusicEvent();
}

static void checkWeaponDetails(int weapon)
{
    int thingId = GetUnitThingID(weapon);

    if (thingId>=222&&thingId<=225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId==1178||1168==thingId)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
}

static void placingInventorySingle(short invId, float x, float y, int *pProperties)
{
    int inv =CreateObjectById(invId, x,y);
    
    if (MaxHealth(inv))
        SetUnitMaxHealth(inv, MaxHealth(inv)*3);
    checkWeaponDetails(inv);
    SetWeaponPropertiesDirect(inv, pProperties[0], pProperties[1], pProperties[2], pProperties[3]);
}

static void placingInventory(short invId, short locationId, int count, float *pVect, int *pProperties)
{
    if (count>10)
        count=10;

    while (--count>=0)
    {
        placingInventorySingle(invId, LocationX(locationId), LocationY(locationId), pProperties);
        TeleportLocationVector(locationId, pVect[0], pVect[1]);
    }
}

static void placingPotSingle(short potId, float x, float y)
{
    int pot=CreateObjectById(potId, x,y);
    int fn;

    if (HashGet( m_potFnHash, potId, &fn, FALSE))
        CallFunctionWithArg(fn, pot);
}

static void placingPotion(short potId, short locationId, int count, float *pVector)
{
    if (count>16)
        count=16;

    while (--count>=0)
    {
        placingPotSingle(potId, LocationX(locationId), LocationY(locationId));
        TeleportLocationVector(locationId, pVector[0], pVector[1]);
    }
}

#define POTION_DISPOS_GAP 8.0

static void placingPotLv6()
{
    float vect[]={POTION_DISPOS_GAP, -POTION_DISPOS_GAP};
    placingPotion(OBJ_RED_POTION, 243, 20, vect);
    placingPotion(OBJ_RED_POTION, 244, 20, vect);
    placingPotion(OBJ_RED_POTION, 245, 20, vect);
    placingPotion(OBJ_RED_POTION, 246, 20, vect);
    placingPotion(OBJ_BLUE_POTION, 248, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 249, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 250, 20, vect);
    placingPotion(OBJ_VAMPIRISM_POTION, 251, 20, vect);
    placingPotion(OBJ_SHIELD_POTION, 255, 20, vect);
    placingPotion(OBJ_VAMPIRISM_POTION, 253, 20, vect);
    placingPotion(OBJ_HASTE_POTION, 254, 20, vect);
    placingPotion(OBJ_BLACK_BOOK_1, 257, 20, vect);
    placingPotion(OBJ_BLACK_BOOK_1, 258, 20, vect);
}
static void placingPotLv5()
{
    float vect[]={-POTION_DISPOS_GAP, POTION_DISPOS_GAP};
    placingPotion(OBJ_RED_POTION, 232, 20, vect);
    placingPotion(OBJ_RED_POTION, 233, 20, vect);
    placingPotion(OBJ_RED_POTION, 234, 20, vect);
    placingPotion(OBJ_BLUE_POTION, 236, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 237, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 238, 20, vect);
    placingPotion(OBJ_VAMPIRISM_POTION, 239, 20, vect);
    placingPotion(OBJ_SHIELD_POTION, 240, 20, vect);
    placingPotion(OBJ_VAMPIRISM_POTION, 241, 20, vect);
    placingPotion(OBJ_HASTE_POTION, 242, 20, vect);
}
static void placingPotLv4()
{
    float vect[]={POTION_DISPOS_GAP, POTION_DISPOS_GAP};
    placingPotion(OBJ_RED_POTION, 221, 16, vect);
    placingPotion(OBJ_RED_POTION, 222, 16, vect);
    placingPotion(OBJ_RED_POTION, 223, 16, vect);
    placingPotion(OBJ_RED_POTION, 224, 16, vect);
    placingPotion(OBJ_BLUE_POTION, 225, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 226, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 227, 20, vect);
    placingPotion(OBJ_VAMPIRISM_POTION, 228, 20, vect);
    placingPotion(OBJ_SHIELD_POTION, 229, 20, vect);
    placingPotion(OBJ_VAMPIRISM_POTION, 230, 20, vect);
    placingPotion(OBJ_HASTE_POTION, 231, 20, vect);
}
static void placingPotLv3()
{
    float vect[]={-POTION_DISPOS_GAP, -POTION_DISPOS_GAP};
    placingPotion(OBJ_RED_POTION, 212, 16, vect);
    placingPotion(OBJ_RED_POTION, 213, 16, vect);
    placingPotion(OBJ_RED_POTION, 214, 16, vect);
    placingPotion(OBJ_RED_POTION, 215, 16, vect);
    placingPotion(OBJ_BLUE_POTION, 216, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 217, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 218, 20, vect);
    placingPotion(OBJ_VAMPIRISM_POTION, 219, 20, vect);
    placingPotion(OBJ_SHIELD_POTION, 220, 20, vect);
}
static void placingPotLv2()
{
    float vect[]={POTION_DISPOS_GAP,POTION_DISPOS_GAP};
    placingPotion(OBJ_RED_POTION, 207, 16, vect);
    placingPotion(OBJ_RED_POTION, 208, 16, vect);
    placingPotion(OBJ_RED_POTION, 209, 16, vect);
    placingPotion(OBJ_BLUE_POTION, 210, 20, vect);
    placingPotion(OBJ_CURE_POISON_POTION, 211, 24, vect);
}

static void placingPotLv1()
{
    float vect[]={POTION_DISPOS_GAP,POTION_DISPOS_GAP};
    placingPotion(OBJ_RED_POTION, 206, 20, vect);
    placingPotSingle(OBJ_AMULETOF_NATURE, LocationX(259), LocationY(259));
}

static void placingHomeLv6()
{
    float vect[]={-8.0, 8.0};
    int cbowp[]={0,0,ITEM_PROPERTY_readiness3,ITEM_PROPERTY_projectileSpeed3};
    placingInventory(OBJ_CROSS_BOW, 169,10,vect,cbowp);
    int cbowp2[]={0,0,ITEM_PROPERTY_impact2,ITEM_PROPERTY_projectileSpeed3};
    placingInventory(OBJ_CROSS_BOW, 169,10,vect,cbowp2);
    int bowp[]={0,ITEM_PROPERTY_Speed2,ITEM_PROPERTY_readiness4,ITEM_PROPERTY_projectileSpeed4};
    placingInventory(OBJ_CROSS_BOW, 170,10,vect,bowp);
    int bowp2[]={0,ITEM_PROPERTY_Speed2,ITEM_PROPERTY_readiness4,ITEM_PROPERTY_impact2};
    placingInventory(OBJ_CROSS_BOW, 170,10,vect,bowp2);

    int weaponProperty1[]={ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,ITEM_PROPERTY_fire4,ITEM_PROPERTY_vampirism3};

    placingInventory(OBJ_WAR_HAMMER, 171,10,vect,weaponProperty1);
    placingInventory(OBJ_GREAT_SWORD, 172,10,vect,weaponProperty1);
    placingInventory(OBJ_ROUND_CHAKRAM, 173,10,vect,weaponProperty1);
    placingInventory(OBJ_QUIVER, 174, 10, vect, weaponProperty1);
    placingInventory(OBJ_FAN_CHAKRAM, 175, 10, vect, weaponProperty1);

    int weaponProperty2[]={ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,ITEM_PROPERTY_lightning4,ITEM_PROPERTY_venom3};
    placingInventory(OBJ_WAR_HAMMER, 171,10,vect,weaponProperty2);
    placingInventory(OBJ_GREAT_SWORD, 172,10,vect,weaponProperty2);
    placingInventory(OBJ_ROUND_CHAKRAM, 173,10,vect,weaponProperty2);
    placingInventory(OBJ_QUIVER, 174, 10, vect, weaponProperty2);
    placingInventory(OBJ_FAN_CHAKRAM, 175, 10, vect, weaponProperty2);

    int weaponProperty3[]={ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,ITEM_PROPERTY_venom4,ITEM_PROPERTY_stun2};
    placingInventory(OBJ_WAR_HAMMER, 171,10,vect,weaponProperty3);
    placingInventory(OBJ_GREAT_SWORD, 172,10,vect,weaponProperty3);
    placingInventory(OBJ_ROUND_CHAKRAM, 173,10,vect,weaponProperty3);
    placingInventory(OBJ_QUIVER, 174, 10, vect, weaponProperty3);
    placingInventory(OBJ_FAN_CHAKRAM, 175, 10, vect, weaponProperty3);

    int armorProperty1[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, 0};
    placingInventory(OBJ_ORNATE_HELM, 176, 10, vect, armorProperty1);
    placingInventory(OBJ_PLATE_ARMS, 177, 10, vect, armorProperty1);
    placingInventory(OBJ_PLATE_BOOTS, 178, 10, vect, armorProperty1);
    placingInventory(OBJ_PLATE_LEGGINGS, 179, 10, vect, armorProperty1);
    placingInventory(OBJ_BREASTPLATE, 180, 10, vect, armorProperty1);
    placingInventory(OBJ_CONJURER_HELM, 181, 10, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMBANDS, 182, 10, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMOR, 183, 10, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 184, 10, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_LEGGINGS, 185, 10, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 186, 10, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_PANTS, 187, 10, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 188, 10, vect, armorProperty1);
    placingInventory(OBJ_STEEL_SHIELD, 189, 10, vect, armorProperty1);

    int armorProperty2[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, 0};
    placingInventory(OBJ_ORNATE_HELM, 176, 10, vect, armorProperty2);
    placingInventory(OBJ_PLATE_ARMS, 177, 10, vect, armorProperty2);
    placingInventory(OBJ_PLATE_BOOTS, 178, 10, vect, armorProperty2);
    placingInventory(OBJ_PLATE_LEGGINGS, 179, 10, vect, armorProperty2);
    placingInventory(OBJ_BREASTPLATE, 180, 10, vect, armorProperty2);
    placingInventory(OBJ_CONJURER_HELM, 181, 10, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMBANDS, 182, 10, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMOR, 183, 10, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 184, 10, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_LEGGINGS, 185, 10, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 186, 10, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_PANTS, 187, 10, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 188, 10, vect, armorProperty2);
    placingInventory(OBJ_STEEL_SHIELD, 189, 10, vect, armorProperty2);

    int armorProperty3[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_PoisonProtect3};
    placingInventory(OBJ_ORNATE_HELM, 176, 10, vect, armorProperty3);
    placingInventory(OBJ_PLATE_ARMS, 177, 10, vect, armorProperty3);
    placingInventory(OBJ_PLATE_BOOTS, 178, 10, vect, armorProperty3);
    placingInventory(OBJ_PLATE_LEGGINGS, 179, 10, vect, armorProperty3);
    placingInventory(OBJ_BREASTPLATE, 180, 10, vect, armorProperty3);
    placingInventory(OBJ_CONJURER_HELM, 181, 10, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMBANDS, 182, 10, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMOR, 183, 10, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 184, 10, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_LEGGINGS, 185, 10, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 186, 10, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_PANTS, 187, 10, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 188, 10, vect, armorProperty3);
    placingInventory(OBJ_STEEL_SHIELD, 189, 10, vect, armorProperty3);

    int armorProperty4[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Speed3, ITEM_PROPERTY_Regeneration3};
    placingInventory(OBJ_ORNATE_HELM, 176, 10, vect, armorProperty4);
    placingInventory(OBJ_PLATE_ARMS, 177, 10, vect, armorProperty4);
    placingInventory(OBJ_PLATE_BOOTS, 178, 10, vect, armorProperty4);
    placingInventory(OBJ_PLATE_LEGGINGS, 179, 10, vect, armorProperty4);
    placingInventory(OBJ_BREASTPLATE, 180, 10, vect, armorProperty4);
    placingInventory(OBJ_CONJURER_HELM, 181, 10, vect, armorProperty4);
    placingInventory(OBJ_LEATHER_ARMBANDS, 182, 10, vect, armorProperty4);
    placingInventory(OBJ_LEATHER_ARMOR, 183, 10, vect, armorProperty4);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 184, 10, vect, armorProperty4);
    placingInventory(OBJ_LEATHER_LEGGINGS, 185, 10, vect, armorProperty4);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 186, 10, vect, armorProperty4);
    placingInventory(OBJ_MEDIEVAL_PANTS, 187, 10, vect, armorProperty4);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 188, 10, vect, armorProperty4);
    placingInventory(OBJ_STEEL_SHIELD, 189, 10, vect, armorProperty4);

    int weaponProperty4[]={ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,ITEM_PROPERTY_impact3,ITEM_PROPERTY_confuse2};
    placingInventory(OBJ_WAR_HAMMER, 190,8,vect,weaponProperty4);
    placingInventory(OBJ_GREAT_SWORD, 191,8,vect,weaponProperty4);
    placingInventory(OBJ_ROUND_CHAKRAM, 192,8,vect,weaponProperty4);
    placingInventory(OBJ_QUIVER, 193, 8, vect, weaponProperty4);
    placingInventory(OBJ_FAN_CHAKRAM, 194, 8, vect, weaponProperty4);
}

static void placingHomeLv5()
{
    float vect[]={-8.0, 8.0};
    int cbowp[]={0,0,ITEM_PROPERTY_readiness2,ITEM_PROPERTY_projectileSpeed2};
    placingInventory(OBJ_CROSS_BOW, 142,12,vect,cbowp);
    int bowp[]={0,0,ITEM_PROPERTY_readiness4,ITEM_PROPERTY_projectileSpeed3};
    placingInventory(OBJ_CROSS_BOW, 143,12,vect,bowp);
    int weaponProperty1[]={ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,ITEM_PROPERTY_fire4,0};

    placingInventory(OBJ_WAR_HAMMER, 144,12,vect,weaponProperty1);
    placingInventory(OBJ_GREAT_SWORD, 145,12,vect,weaponProperty1);
    placingInventory(OBJ_ROUND_CHAKRAM, 146,12,vect,weaponProperty1);
    placingInventory(OBJ_QUIVER, 147, 12, vect, weaponProperty1);
    placingInventory(OBJ_FAN_CHAKRAM, 148, 12, vect, weaponProperty1);

    int weaponProperty2[]={ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,ITEM_PROPERTY_lightning4,0};
    placingInventory(OBJ_WAR_HAMMER, 144,12,vect,weaponProperty2);
    placingInventory(OBJ_GREAT_SWORD, 145,12,vect,weaponProperty2);
    placingInventory(OBJ_ROUND_CHAKRAM, 146,12,vect,weaponProperty2);
    placingInventory(OBJ_QUIVER, 147, 12, vect, weaponProperty2);
    placingInventory(OBJ_FAN_CHAKRAM, 148, 12, vect, weaponProperty2);

    int weaponProperty3[]={ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7,ITEM_PROPERTY_venom3,ITEM_PROPERTY_vampirism2};
    placingInventory(OBJ_WAR_HAMMER, 144,12,vect,weaponProperty3);
    placingInventory(OBJ_GREAT_SWORD, 145,12,vect,weaponProperty3);
    placingInventory(OBJ_ROUND_CHAKRAM, 146,12,vect,weaponProperty3);
    placingInventory(OBJ_QUIVER, 147, 12, vect, weaponProperty3);
    placingInventory(OBJ_FAN_CHAKRAM, 148, 12, vect, weaponProperty3);

    int armorProperty1[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, 0};
    placingInventory(OBJ_ORNATE_HELM, 149, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_ARMS, 150, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_BOOTS, 151, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_LEGGINGS, 152, 12, vect, armorProperty1);
    placingInventory(OBJ_BREASTPLATE, 153, 12, vect, armorProperty1);
    placingInventory(OBJ_CONJURER_HELM, 154, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMBANDS, 155, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMOR, 156, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 157, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_LEGGINGS, 158, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 159, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_PANTS, 160, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 161, 12, vect, armorProperty1);
    placingInventory(OBJ_STEEL_SHIELD, 162, 12, vect, armorProperty1);

    int armorProperty2[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, 0};
    placingInventory(OBJ_ORNATE_HELM, 149, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_ARMS, 150, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_BOOTS, 151, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_LEGGINGS, 152, 12, vect, armorProperty2);
    placingInventory(OBJ_BREASTPLATE, 153, 12, vect, armorProperty2);
    placingInventory(OBJ_CONJURER_HELM, 154, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMBANDS, 155, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMOR, 156, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 157, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_LEGGINGS, 158, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 159, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_PANTS, 160, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 161, 12, vect, armorProperty2);
    placingInventory(OBJ_STEEL_SHIELD, 162, 12, vect, armorProperty2);

    int armorProperty3[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_PoisonProtect3};
    placingInventory(OBJ_ORNATE_HELM, 149, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_ARMS, 150, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_BOOTS, 151, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_LEGGINGS, 152, 12, vect, armorProperty3);
    placingInventory(OBJ_BREASTPLATE, 153, 12, vect, armorProperty3);
    placingInventory(OBJ_CONJURER_HELM, 154, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMBANDS, 155, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMOR, 156, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 157, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_LEGGINGS, 158, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 159, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_PANTS, 160, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 161, 12, vect, armorProperty3);
    placingInventory(OBJ_STEEL_SHIELD, 162, 12, vect, armorProperty3);

    int armorProperty4[]={ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_Speed1};
    placingInventory(OBJ_PLATE_BOOTS, 163, 8, vect, armorProperty4);
    placingInventory(OBJ_STEEL_SHIELD, 163, 8, vect, armorProperty4);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 163, 8, vect, armorProperty4);
}

static void placingHomeLv4()
{
    float vect[]={8.0, 8.0};
    int cbowp[]={0,0,ITEM_PROPERTY_readiness2,ITEM_PROPERTY_projectileSpeed1};
    placingInventory(OBJ_CROSS_BOW, 113,12,vect,cbowp);
    int bowp[]={0,0,ITEM_PROPERTY_projectileSpeed2,ITEM_PROPERTY_readiness3};
    placingInventory(OBJ_CROSS_BOW, 114,12,vect,bowp);
    int weaponProperty1[]={ITEM_PROPERTY_weaponPower5,ITEM_PROPERTY_Matrial6,ITEM_PROPERTY_fire3,0};

    placingInventory(OBJ_WAR_HAMMER, 115,12,vect,weaponProperty1);
    placingInventory(OBJ_GREAT_SWORD, 116,12,vect,weaponProperty1);
    placingInventory(OBJ_ROUND_CHAKRAM, 117,12,vect,weaponProperty1);
    placingInventory(OBJ_QUIVER, 118, 12, vect, weaponProperty1);
    placingInventory(OBJ_FAN_CHAKRAM, 119, 12, vect, weaponProperty1);

    int weaponProperty2[]={ITEM_PROPERTY_weaponPower5,ITEM_PROPERTY_Matrial6,ITEM_PROPERTY_lightning3,0};
    placingInventory(OBJ_WAR_HAMMER, 115,12,vect,weaponProperty2);
    placingInventory(OBJ_GREAT_SWORD, 116,12,vect,weaponProperty2);
    placingInventory(OBJ_ROUND_CHAKRAM, 117,12,vect,weaponProperty2);
    placingInventory(OBJ_QUIVER, 118, 12, vect, weaponProperty2);
    placingInventory(OBJ_FAN_CHAKRAM, 119, 12, vect, weaponProperty2);

    int weaponProperty3[]={ITEM_PROPERTY_weaponPower5,ITEM_PROPERTY_Matrial6,ITEM_PROPERTY_venom2,ITEM_PROPERTY_vampirism1};
    placingInventory(OBJ_WAR_HAMMER, 115,12,vect,weaponProperty3);
    placingInventory(OBJ_GREAT_SWORD, 116,12,vect,weaponProperty3);
    placingInventory(OBJ_ROUND_CHAKRAM, 117,12,vect,weaponProperty3);
    placingInventory(OBJ_QUIVER, 118, 12, vect, weaponProperty3);
    placingInventory(OBJ_FAN_CHAKRAM, 119, 12, vect, weaponProperty3);

    int armorProperty1[]={ITEM_PROPERTY_armorQuality5, ITEM_PROPERTY_Matrial6, ITEM_PROPERTY_FireProtect3, 0};
    placingInventory(OBJ_ORNATE_HELM, 120, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_ARMS, 121, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_BOOTS, 122, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_LEGGINGS, 123, 12, vect, armorProperty1);
    placingInventory(OBJ_BREASTPLATE, 124, 12, vect, armorProperty1);
    placingInventory(OBJ_CONJURER_HELM, 125, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMBANDS, 126, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMOR, 127, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 128, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_LEGGINGS, 129, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 130, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_PANTS, 131, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 132, 12, vect, armorProperty1);

    int armorProperty2[]={ITEM_PROPERTY_armorQuality5, ITEM_PROPERTY_Matrial6, ITEM_PROPERTY_LightningProtect3, 0};
    placingInventory(OBJ_ORNATE_HELM, 120, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_ARMS, 121, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_BOOTS, 122, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_LEGGINGS, 123, 12, vect, armorProperty2);
    placingInventory(OBJ_BREASTPLATE, 124, 12, vect, armorProperty2);
    placingInventory(OBJ_CONJURER_HELM, 125, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMBANDS, 126, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMOR, 127, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 128, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_LEGGINGS, 129, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 130, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_PANTS, 131, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 132, 12, vect, armorProperty2);

    int armorProperty3[]={ITEM_PROPERTY_armorQuality5, ITEM_PROPERTY_Matrial6, ITEM_PROPERTY_PoisonProtect3, ITEM_PROPERTY_Regeneration2};
    placingInventory(OBJ_ORNATE_HELM, 120, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_ARMS, 121, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_BOOTS, 122, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_LEGGINGS, 123, 12, vect, armorProperty3);
    placingInventory(OBJ_BREASTPLATE, 124, 12, vect, armorProperty3);
    placingInventory(OBJ_CONJURER_HELM, 125, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMBANDS, 126, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMOR, 127, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 128, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_LEGGINGS, 129, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 130, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_PANTS, 131, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 132, 12, vect, armorProperty3);
}

static void placingHomeLv3()
{
    float vect[]={8.0, 8.0};
    int cbowp[]={0,0,ITEM_PROPERTY_readiness1,0};
    placingInventory(OBJ_CROSS_BOW, 83,12,vect,cbowp);
    int bowp[]={0,0,ITEM_PROPERTY_projectileSpeed2,ITEM_PROPERTY_readiness2};
    placingInventory(OBJ_CROSS_BOW, 83,12,vect,bowp);
    int weaponProperty1[]={ITEM_PROPERTY_weaponPower4,ITEM_PROPERTY_Matrial5,ITEM_PROPERTY_fire2,0};

    placingInventory(OBJ_WAR_HAMMER, 84,12,vect,weaponProperty1);
    placingInventory(OBJ_GREAT_SWORD, 85,12,vect,weaponProperty1);
    placingInventory(OBJ_ROUND_CHAKRAM, 86,12,vect,weaponProperty1);
    placingInventory(OBJ_QUIVER, 87, 12, vect, weaponProperty1);
    placingInventory(OBJ_FAN_CHAKRAM, 88, 12, vect, weaponProperty1);

    int weaponProperty2[]={ITEM_PROPERTY_weaponPower4,ITEM_PROPERTY_Matrial5,ITEM_PROPERTY_lightning2,0};
    placingInventory(OBJ_WAR_HAMMER, 84,12,vect,weaponProperty2);
    placingInventory(OBJ_GREAT_SWORD, 85,12,vect,weaponProperty2);
    placingInventory(OBJ_ROUND_CHAKRAM, 86,12,vect,weaponProperty2);
    placingInventory(OBJ_QUIVER, 87, 12, vect, weaponProperty2);
    placingInventory(OBJ_FAN_CHAKRAM, 88, 12, vect, weaponProperty2);

    int weaponProperty3[]={ITEM_PROPERTY_weaponPower4,ITEM_PROPERTY_Matrial5,ITEM_PROPERTY_venom2,0};
    placingInventory(OBJ_WAR_HAMMER, 84,12,vect,weaponProperty3);
    placingInventory(OBJ_GREAT_SWORD, 85,12,vect,weaponProperty3);
    placingInventory(OBJ_ROUND_CHAKRAM, 86,12,vect,weaponProperty3);
    placingInventory(OBJ_QUIVER, 87, 12, vect, weaponProperty3);
    placingInventory(OBJ_FAN_CHAKRAM, 88, 12, vect, weaponProperty3);

    int armorProperty1[]={ITEM_PROPERTY_armorQuality4, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_FireProtect2, 0};
    placingInventory(OBJ_ORNATE_HELM, 89, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_ARMS, 90, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_BOOTS, 91, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_LEGGINGS, 92, 12, vect, armorProperty1);
    placingInventory(OBJ_BREASTPLATE, 93, 12, vect, armorProperty1);
    placingInventory(OBJ_CONJURER_HELM, 94, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMBANDS, 95, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMOR, 96, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 97, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_LEGGINGS, 98, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 99, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_PANTS, 100, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 101, 12, vect, armorProperty1);

    int armorProperty2[]={ITEM_PROPERTY_armorQuality4, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_LightningProtect2, 0};
    placingInventory(OBJ_ORNATE_HELM, 89, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_ARMS, 90, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_BOOTS, 91, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_LEGGINGS, 92, 12, vect, armorProperty2);
    placingInventory(OBJ_BREASTPLATE, 93, 12, vect, armorProperty2);
    placingInventory(OBJ_CONJURER_HELM, 94, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMBANDS, 95, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMOR, 96, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 97, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_LEGGINGS, 98, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 99, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_PANTS, 100, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 101, 12, vect, armorProperty2);

    int armorProperty3[]={ITEM_PROPERTY_armorQuality4, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_PoisonProtect2, ITEM_PROPERTY_Regeneration2};
    placingInventory(OBJ_ORNATE_HELM, 89, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_ARMS, 90, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_BOOTS, 91, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_LEGGINGS, 92, 12, vect, armorProperty3);
    placingInventory(OBJ_BREASTPLATE, 93, 12, vect, armorProperty3);
    placingInventory(OBJ_CONJURER_HELM, 94, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMBANDS, 95, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMOR, 96, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 97, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_LEGGINGS, 98, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 99, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_PANTS, 100, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 101, 12, vect, armorProperty3);
}

static void placingHomeLv2()
{
    float vect[]={8.0, -8.0};
    int empty[]={0,0,0,0};
    placingInventory(OBJ_CROSS_BOW, 82,12,vect,empty);
    int bowp[]={0,0,ITEM_PROPERTY_readiness1,0};
    placingInventory(OBJ_CROSS_BOW, 205,12,vect,bowp);
    int weaponProperty1[]={ITEM_PROPERTY_weaponPower3,ITEM_PROPERTY_Matrial4,ITEM_PROPERTY_fire1,0};

    placingInventory(OBJ_WAR_HAMMER, 64,12,vect,weaponProperty1);
    placingInventory(OBJ_GREAT_SWORD, 65,12,vect,weaponProperty1);
    placingInventory(OBJ_ROUND_CHAKRAM, 66,12,vect,weaponProperty1);
    placingInventory(OBJ_QUIVER, 67, 12, vect, weaponProperty1);
    placingInventory(OBJ_FAN_CHAKRAM, 67, 12, vect, weaponProperty1);

    int weaponProperty2[]={ITEM_PROPERTY_weaponPower3,ITEM_PROPERTY_Matrial4,ITEM_PROPERTY_lightning1,0};
    placingInventory(OBJ_WAR_HAMMER, 64,12,vect,weaponProperty2);
    placingInventory(OBJ_GREAT_SWORD, 65,12,vect,weaponProperty2);
    placingInventory(OBJ_ROUND_CHAKRAM, 66,12,vect,weaponProperty2);
    placingInventory(OBJ_QUIVER, 67, 12, vect, weaponProperty2);
    placingInventory(OBJ_FAN_CHAKRAM, 67, 12, vect, weaponProperty2);

    int weaponProperty3[]={ITEM_PROPERTY_weaponPower3,ITEM_PROPERTY_Matrial4,ITEM_PROPERTY_venom1,0};
    placingInventory(OBJ_WAR_HAMMER, 64,12,vect,weaponProperty3);
    placingInventory(OBJ_GREAT_SWORD, 65,12,vect,weaponProperty3);
    placingInventory(OBJ_ROUND_CHAKRAM, 66,12,vect,weaponProperty3);
    placingInventory(OBJ_QUIVER, 67, 12, vect, weaponProperty3);
    placingInventory(OBJ_FAN_CHAKRAM, 67, 12, vect, weaponProperty3);

    int armorProperty1[]={ITEM_PROPERTY_armorQuality3, ITEM_PROPERTY_Matrial4, ITEM_PROPERTY_FireProtect1, 0};
    placingInventory(OBJ_ORNATE_HELM, 68, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_ARMS, 69, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_BOOTS, 70, 12, vect, armorProperty1);
    placingInventory(OBJ_PLATE_LEGGINGS, 71, 12, vect, armorProperty1);
    placingInventory(OBJ_BREASTPLATE, 72, 12, vect, armorProperty1);
    placingInventory(OBJ_CONJURER_HELM, 73, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMBANDS, 74, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMOR, 75, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 76, 12, vect, armorProperty1);
    placingInventory(OBJ_LEATHER_LEGGINGS, 77, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 78, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_PANTS, 79, 12, vect, armorProperty1);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 80, 12, vect, armorProperty1);

    int armorProperty2[]={ITEM_PROPERTY_armorQuality3, ITEM_PROPERTY_Matrial4, ITEM_PROPERTY_LightningProtect1, 0};
    placingInventory(OBJ_ORNATE_HELM, 68, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_ARMS, 69, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_BOOTS, 70, 12, vect, armorProperty2);
    placingInventory(OBJ_PLATE_LEGGINGS, 71, 12, vect, armorProperty2);
    placingInventory(OBJ_BREASTPLATE, 72, 12, vect, armorProperty2);
    placingInventory(OBJ_CONJURER_HELM, 73, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMBANDS, 74, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMOR, 75, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 76, 12, vect, armorProperty2);
    placingInventory(OBJ_LEATHER_LEGGINGS, 77, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 78, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_PANTS, 79, 12, vect, armorProperty2);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 80, 12, vect, armorProperty2);

    int armorProperty3[]={ITEM_PROPERTY_armorQuality3, ITEM_PROPERTY_Matrial4, ITEM_PROPERTY_PoisonProtect1, ITEM_PROPERTY_Regeneration1};
    placingInventory(OBJ_ORNATE_HELM, 68, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_ARMS, 69, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_BOOTS, 70, 12, vect, armorProperty3);
    placingInventory(OBJ_PLATE_LEGGINGS, 71, 12, vect, armorProperty3);
    placingInventory(OBJ_BREASTPLATE, 72, 12, vect, armorProperty3);
    placingInventory(OBJ_CONJURER_HELM, 73, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMBANDS, 74, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMOR, 75, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 76, 12, vect, armorProperty3);
    placingInventory(OBJ_LEATHER_LEGGINGS, 77, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_CLOAK, 78, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_PANTS, 79, 12, vect, armorProperty3);
    placingInventory(OBJ_MEDIEVAL_SHIRT, 80, 12, vect, armorProperty3);
}

static void placingHomeLv1()
{
    float vect[]={11.0, -11.0};
    int properties[]={0,0,0,0};
    placingInventory(OBJ_ORNATE_HELM, 50,12,vect,properties);
    placingInventory(OBJ_BREASTPLATE, 51,12,vect,properties);
    placingInventory(OBJ_PLATE_ARMS, 52,12,vect,properties);
    placingInventory(OBJ_WAR_HAMMER, 53,12,vect,properties);
    placingInventory(OBJ_GREAT_SWORD, 54,12,vect,properties);
    placingInventory(OBJ_PLATE_BOOTS, 55,12,vect,properties);
    placingInventory(OBJ_PLATE_LEGGINGS, 56,12,vect,properties);
    placingInventory(OBJ_CONJURER_HELM, 57,12,vect,properties);
    placingInventory(OBJ_LEATHER_ARMBANDS, 58,12,vect,properties);
    placingInventory(OBJ_LEATHER_ARMOR, 59,12,vect,properties);
    placingInventory(OBJ_LEATHER_LEGGINGS, 60,12,vect,properties);
    placingInventory(OBJ_LEATHER_ARMORED_BOOTS, 61,12,vect,properties);
    placingInventory(OBJ_CROSS_BOW, 62,12,vect,properties);
    placingInventory(OBJ_QUIVER, 63,12,vect,properties);
    placingInventory(OBJ_QUIVER, 50,12,vect,properties);
    placingInventory(OBJ_FAN_CHAKRAM, 51,12,vect,properties);
}

static void onVampPotUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_VAMPIRISM), 0.0);
}

static void onVampPotHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onVampPotUse);
}

static void onInvulnPotUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_INVULNERABLE), 25.0);
}

static void onInvulnPotHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onInvulnPotUse);
}

static void onBlackBookUse()
{
    Delete(SELF);
    CastSpellObjectObject("SPELL_FORCE_OF_NATURE", OTHER, OTHER);
    UniChatMessage(OTHER, "괴물녀석, 이거나 먹어라!", 90);
}

static void onBlackBookHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onBlackBookUse);
}

static void onHealingBuff(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int owner= GetOwner(sub);
        if (CurrentHealth(owner))
        {
            int dur=GetDirection(sub);
            if (dur)
            {
                if (ToInt(DistanceUnitToUnit(sub, owner)))
                    MoveObject(sub, GetObjectX(owner), GetObjectY(owner));
                LookWithAngle(sub, dur-1);
                RestoreHealth(owner, 4);
                PushTimerQueue(3, sub, onHealingBuff);
                return;
            }
        }
        Delete(sub);
    }
}

static void startHealingBuff(int owner)
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(owner), GetObjectY(owner));

    LookWithAngle(sub, 80);
    SetOwner(owner, sub);
    SetUnitEnchantCopy(sub, GetLShift(ENCHANT_RUN));
    PushTimerQueue(1, sub, onHealingBuff);
}

static void onAmuletofNatureUse()
{
    if (UnitCheckEnchant(OTHER, GetLShift(ENCHANT_CROWN)))
    {
        PlaySoundAround(OTHER, SOUND_NoCanDo);
        UniPrint(OTHER, "아직 힐링포션 효력이 남아있는 상태입니다");
        return;
    }
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_CROWN), 6.0);
    startHealingBuff(GetCaller());
}

static void onAmuletofNatureHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onAmuletofNatureUse);
}

static void initialPotionHash()
{
    HashCreateInstance(&m_potFnHash);

    HashPushback(m_potFnHash, OBJ_VAMPIRISM_POTION, onVampPotHash);
    HashPushback(m_potFnHash, OBJ_INVULNERABILITY_POTION, onInvulnPotHash);
    HashPushback(m_potFnHash, OBJ_BLACK_BOOK_1, onBlackBookHash);
    HashPushback(m_potFnHash, OBJ_AMULETOF_NATURE, onAmuletofNatureHash);
}

void GreenSparkFx(float x, float y)
{
    int ptr = CreateObjectAt("MonsterGenerator", x, y);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

static void onCollideHomeStart()
{
    if (!GetTrigger())
        return;

    if (IsPlayerUnit(OTHER))
    {
        int n;

        WriteLog("oncollidehomestart");
        if (HashGet(m_genericHash, GetTrigger(), &n, FALSE))
        {
            WriteLog("oncollidehomestart");
            WriteLog(IntToString(n));
            CallFunction( m_homeItemCreateFn[n] );
            CallFunction( m_homePotCreateFn[n] );
        }
        GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
        Delete(SELF);
        WriteLog("oncollidehomestart-end");
    }
}

static void initialHomeTrigger()
{    
    int n;

    while (TRUE)
    {    
        int location = HomeTeleportNum(n++);

        if (!location)
            break;
        int t=DummyUnitCreateAt("Bomber", LocationX(location), LocationY(location));

        SetCallback(t,9,onCollideHomeStart);
        HashPushback(m_genericHash, t, n);
    }
    int itemFn[]={0,placingHomeLv1,placingHomeLv2,placingHomeLv3,placingHomeLv4,placingHomeLv5,placingHomeLv6,};
    int potFn[]={0,placingPotLv1,placingPotLv2,placingPotLv3,placingPotLv4,placingPotLv5,placingPotLv6,};
    m_homeItemCreateFn=itemFn;
    m_homePotCreateFn=potFn;
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("xisland-log.txt");
    HashCreateInstance(&m_genericHash);
    initialHomeTrigger();
    InitPickets();
    InitializeUserSpecialWeapon();
    initialPotionHash();
    ParentNode();
    initialSpecialNpc();
    InitDialog();

    //loop_run
    PushTimerQueue(10, 0,PlayerClassOnLoop);
    // FrameTimer(2, loopReventCharm);
    PushTimerQueue(1, 0,MakeCoopTeam);
    PushTimerQueue(3, GetCaptain(),loopHorrendousStatus);

    //delay_run
    // FrameTimer(10, strCureHealth);
    // FrameTimer(11, strStartButton);
    // FrameTimer(12, strUpgradeHome);
    // FrameTimer(13, strPlasmaStaff);
    PushTimerQueue(30, 13, callTurboTrigger);
    // FrameTimer(150, PutShopInfoStamp);
    PushTimerQueue(160,0, ShowHorrendousHP);
}

void EntryPlayer(int plr, int pUnit)
{
    int location = Random(5, 8);

    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);
    MoveObject(pUnit, LocationX(location), LocationY(location));
    DeleteObjectTimer(CreateObject("BlueRain", location), 10);
    AudioEvent("BlindOff", location);
}

void CantPlayerEntry()
{
    UniPrintToAll("맵이 수용할 수 있는 플레이어 최대 수를 넘었기 때문에 더 이상 입장하실 수 없습니다.");
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    MoveObject(OTHER, GetWaypointX(40), GetWaypointY(40));
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_SNEAK)))
    {
        RemoveTreadLightly(pUnit);
        EnchantOff(pUnit, EnchantList(ENCHANT_SNEAK));
        CastWindbooster(pUnit);
    }
    if (CheckPlayerInput(pUnit) == 7)   //jump
    {
        CastDeathCharging(pUnit);
    }
}

void PlayerClassOnDeath(int plr, int user)
{
    char buff[128], *p = StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(buff, "%s 님께서 적에게 격추당하셨습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

void PlayerClassOnShutdown(int plr)
{
    m_player[plr]=0;
    m_pFlags[plr]=0;
}

void PlayerClassOnLoop()
{
    int rep = sizeof(m_player);

    while (--rep>=0)
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[rep]))
            {
                if (GetUnitFlags(m_player[rep]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[rep]))
                {
                    PlayerClassOnAlive(rep, m_player[rep]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(rep, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(rep, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(rep, m_player[rep]);
                    }
                    break;
                }                
            }
            if (m_pFlags[rep])
                PlayerClassOnShutdown(rep);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void CastWindbooster(int plrUnit)
{
    Effect("RICOCHET", GetObjectX(plrUnit), GetObjectY(plrUnit), 0.0, 0.0);
    PushObjectTo(plrUnit, UnitAngleCos(plrUnit, 75.0), UnitAngleSin(plrUnit, 75.0));
}

void CastTripleArrowShot(int plrUnit)
{
    float xVect = UnitAngleCos(plrUnit, -20.0), yVect = UnitAngleSin(plrUnit, -20.0);
    int k, missile;

    TeleportLocation(11, GetObjectX(plrUnit) + (1.0 / 1.6 * yVect) - xVect, GetObjectY(plrUnit) - (1.0 / 1.6 * xVect) - yVect);
    
    for (k = 0 ; k < 9 ; k += 1)
    {
        xVect = GetRatioUnitWpXY(plrUnit, 11, 0, 23.0);
        yVect = GetRatioUnitWpXY(plrUnit, 11, 1, 23.0);

        TeleportLocation(11, GetObjectX(plrUnit) - (1.0 / 8.0 * yVect) - xVect, GetObjectY(plrUnit) + (1.0 / 8.0 * xVect) - yVect);
        missile = CreateObject("ArcherArrow", 11);
        LookAtObject(missile, plrUnit);
        LookWithAngle(missile, GetDirection(missile) + 128);
        SetOwner(plrUnit, missile);
        PushObject(missile, 35.0, GetObjectX(plrUnit), GetObjectY(plrUnit));
    }
}

void PowerChargingCollide()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 75, 14);
            Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

int PowerChargingCreateSubUnit(int owner, float xProfile, float yProfile)
{
    int subUnit = CreateObjectAt("CarnivorousPlant", xProfile, yProfile);

    ObjectOff(subUnit);
    Damage(subUnit, 0, MaxHealth(subUnit) + 1, -1);
    Frozen(subUnit, TRUE);
    SetOwner(owner, subUnit);
    DeleteObjectTimer(subUnit, 1);

    return subUnit;
}

void PowerChargingRelay(int subUnit)
{
    int owner = GetOwner(subUnit), count = GetDirection(subUnit);

    while (IsObjectOn(subUnit))
    {
        if (CurrentHealth(owner) && count)
        {
            PushTimerQueue(1, subUnit, PowerChargingRelay);
            LookWithAngle(subUnit, --count);
            SetCallback(PowerChargingCreateSubUnit(owner, GetObjectX(owner) - UnitAngleCos(owner, 20.0), GetObjectY(owner) - UnitAngleSin(owner, 20.0)), 9, PowerChargingCollide);
            PlaySoundAround(owner, 297);
            Effect("YELLOW_SPARKS", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            break;
        }
        Delete(subUnit);
        break;
    }
}

void CastDeathCharging(int plrUnit)
{
    if (MaxHealth(plrUnit) == 150 && !UnitCheckEnchant(plrUnit, GetLShift(ENCHANT_AFRAID)))
    {
        Enchant(plrUnit, EnchantList(ENCHANT_AFRAID), 15.0);
        PlaySoundAround(plrUnit, 900);
        int subUnit = CreateObjectAt("AmbBeachBirds", GetObjectX(plrUnit), GetObjectY(plrUnit));
        SetOwner(plrUnit, subUnit);
        LookWithAngle(subUnit, 16);
        PushTimerQueue(1, subUnit, PowerChargingRelay);
    }
}

void TeleportPlayerAt(int wp)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(m_player[i]))
            MoveObject(m_player[i], LocationX(wp), LocationY(wp));
    }
}

void ChangeUnitAffiliatedTeam(int unit, int teamId)
{
    int *ptr = UnitToPtr(unit);

    if (ptr != NULLPTR)
        ptr[13] = teamId;
}

void TeleportPlayerHome()
{
    if (GetAnswer(SELF)^1)
        return;

    int i = CheckPlayer();

    if (i<0)
        return;

    char msg[128], *pName = StringUtilGetScriptStringPtr(HomeName(m_pUPGRADE[i]));

    Enchant(OTHER, "ENCHANT_RUN", 0.1);
    MoveObject(OTHER, GetWaypointX(HomeTeleportNum(m_pUPGRADE[i])), GetWaypointY(HomeTeleportNum(m_pUPGRADE[i])));
    DeleteObjectTimer(CreateObject("BlueRain", HomeTeleportNum(m_pUPGRADE[i])), 10);
    PlaySoundAround(OTHER, SOUND_BlindOff);
    NoxSprintfString(msg, "%s 창고로 이동했습니다", &pName, 1);
    UniChatMessage(OTHER, ReadStringAddressEx(msg), 150);
}

void DeferredChangeDialogContext5(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user==(GetHost()))    //Server only
        onClientPopupMessage5();
    else
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 5);
}

static void descGoHome()
{
    PushTimerQueue(1, GetCaller(), DeferredChangeDialogContext5);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "창고로 이동");
}

static void captainDialogNothing()
{ }

void SwitchingCaptainDialog(int unit, int enabled)
{
    if (enabled)
    {
        SetDialog(unit, "YESNO", descGoHome, TeleportPlayerHome);
        return;
    }
    SetDialog(unit, "NORMAL", captainDialogNothing, captainDialogNothing);
}

int GetCaptain()
{
    int unit;
    
    if (!unit)
    {
        unit = CreateObject("Horrendous", 3);
        SetUnitHealth(unit, 30000);
        SetCallback(unit, 3, GiveWeaponToHorrendous);
        SetCallback(unit, 5, HorrendousDie);
        // SetDialog(unit, "YESNO", descGoHome, TeleportPlayerHome);
        SwitchingCaptainDialog(unit, TRUE);
        StoryPic(unit, "HorrendousPic");
        // SetOwner(GetHost(), unit);
        ChangeUnitAffiliatedTeam(unit, 1);
    }
    return unit;
}

int HomeTeleportNum(int num)
{
    int arr[] = {25, 47, 26, 27, 22, 204, 0};

    return arr[num % sizeof(arr)];
}

string HomeName(int num)
{
    string arr[] = {"기본옵션", "초급", "중급", "상급", "최고급", "브루주아"};
        
    return arr[num % sizeof(arr)];
}

void DeferredChangeDialogContext(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user==(GetHost()))    //Server only
    {
        onClientPopupMessage1();
    }
    else
    {
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 1);
    }
}

void UpgradeHome()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    if (GetGold(OTHER) >= 10000)
    {
        int i = CheckPlayer();

        if (i < 0)
            return;

        if (m_pUPGRADE[i] < 5)
        {
            m_pUPGRADE[i] ++;
            ChangeGold(OTHER, -10000);
            PlaySoundAround(OTHER, SOUND_AwardGuide);
            Effect("WHITE_FLASH", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            UniPrintToAll("창고가 업그레이드 되었습니다_!");
            return;
        }
    }
    UniPrintToAll("거래실패!- 잔액이 부족하거나, 창고가 최대로 업그레이드 되었습니다");
}

void WouldyouWantUpgrade()
{
    PushTimerQueue(1, GetCaller(), DeferredChangeDialogContext);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "창고 업그레이드");
}

int DummyUnitCreateAt(string name, float xProfile, float yProfile)
{
    int unit = CreateObjectAt(name, xProfile, yProfile);

    if (CurrentHealth(unit))
    {
        ObjectOff(unit);
        Damage(unit, 0, MaxHealth(unit) + 1, -1);
        Frozen(unit, 1);
    }
    return unit;
}

void BurnningZombieCollide()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 170, 14);
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
            Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

int FxBurnningZombie(float xProfile, float yProfile)
{
    int fxU = CreateObjectAt("Zombie", xProfile, yProfile);

    UnitNoCollide(fxU);
    ObjectOff(fxU);
    Damage(fxU, 0, MaxHealth(fxU) + 1, 1);
    return fxU;
}

void BurnningSunFlying(int subunit)
{
    int durate = GetDirection(subunit);

    while (IsObjectOn(subunit))
    {
        int owner = GetOwner(subunit);

        if (CurrentHealth(owner) && durate)
        {
            PushTimerQueue(1, subunit, BurnningSunFlying);
            LookWithAngle(subunit, durate - 1);
            int coll = DummyUnitCreateAt("Troll", GetObjectX(subunit), GetObjectY(subunit));
            SetCallback(coll, 9, BurnningZombieCollide);
            SetOwner(owner, coll);
            DeleteObjectTimer(coll, 1);
            DeleteObjectTimer(FxBurnningZombie(GetObjectX(subunit), GetObjectY(subunit)), 18);
            MoveObject(subunit, GetObjectX(subunit) + GetObjectZ(subunit), GetObjectY(subunit) + GetObjectZ(subunit + 1));
            MoveObject(subunit + 1, GetObjectX(subunit + 1) + GetObjectZ(subunit), GetObjectY(subunit + 1) + GetObjectZ(subunit + 1));
            if (IsVisibleTo(subunit + 1, subunit))
                break;
        }
        Delete(subunit);
        break;
    }
}

void OblivionUseHandler()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 20)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        float xVect = UnitAngleCos(OTHER, 19.0), yVect = UnitAngleSin(OTHER, 19.0);
        int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER) + xVect, GetObjectY(OTHER) + yVect);

        Raise(unit, xVect);
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit) - xVect, GetObjectY(unit) - yVect), yVect);
        SetOwner(OTHER, unit);
        LookWithAngle(unit, 32);
        PushTimerQueue(1, unit, BurnningSunFlying);
        PlaySoundAround(OTHER, 221);
    }
}

int SummonOblivionStaff(float sX, float sY)
{
    int unit = CreateObjectAt("OblivionOrb", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        int thingId = GetUnitThingID(unit);

        if (thingId >= 222 && thingId <= 225)
        {
            SetMemory(ptr + 0x2c4, 0x53a720);
            SetMemory(ptr + 0x2c8, ImportAllowAllDrop());
        }
        else
        {
            return 0;
        }
        
        SetUnitCallbackOnUseItem(unit, OblivionUseHandler);
    }
    return unit;
}

void BuyObilivStaff()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER) >= 30000)
    {
        ChangeGold(OTHER, -30000);

        int staff = SummonOblivionStaff(GetObjectX(SELF), GetObjectY(SELF));

        if (staff)
            PlaySoundAround(staff, 226);
        else
        {
            UniPrint(OTHER, "something happen wrong");
        }
        
        Effect("WHITE_FLASH", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("CYAN_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        UniPrint(OTHER, "망각의 지팡이를 구입하셨습니다.");
    }
    else
        UniPrint(OTHER, "거래실패! 금액이 모자랍니다. 망각의 지팡이는 3만 골드입니다");
}

void SetInfinite()
{
    int cur = GetLastItem(OTHER);
    int count = 0;

    while (IsObjectOn(cur))
    {
        if (GetUnitClass(cur) & (UNIT_CLASS_ARMOR|UNIT_CLASS_WEAPON))
        {
            if (!UnitCheckEnchant(cur, GetLShift(ENCHANT_INVULNERABLE)))
            {
                count ++;
                Enchant(cur, "ENCHANT_INVULNERABLE", 0.0);
            }
        }
        cur = GetPreviousItem(cur);
    }
    char buff[192];

    if (count > 0)
    {
        NoxSprintfString(buff, "처리결과, 총 %d개의 아이템을 무적화 했습니다.", &count, 1);
        UniPrint(OTHER, ReadStringAddressEx(buff));
        Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
    }
}

void outItemRoom()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 1.5);
    MoveObject(OTHER, GetWaypointX(3), GetWaypointY(3));
    AudioEvent("BlindOff", 3);
}

void goSelectRoom()
{
    if (DEATHS == 90)
    {
        Enchant(OTHER, "ENCHANT_FREEZE", 2.0);
        MoveObject(OTHER, GetWaypointX(30), GetWaypointY(30));
    }
    else
    {
        UniPrint(OTHER, "Debug::" + IntToString(DEATHS));
    }
    
}

void setHorrendousHealthAdd()
{
    deferredObjectOn(24, GetTrigger());
    if (GetGold(OTHER) >= 20000 && CurrentHealth(GetCaptain()) < 25000)
    {
        ChangeGold(OTHER, -20000);
        SetUnitHealth(GetCaptain(), CurrentHealth(GetCaptain()) + 5000);
        UniPrintToAll("호렌더스의 현재체력을 5000 만큼 회복시켰습니다. 현재체력: " + IntToString(CurrentHealth(GetCaptain())));
    }
}

void StartCurrentStage()
{
    deferredObjectOn(30, GetTrigger());
    if (DEATHS == 90)
    {
        DEATHS = 0;
        Enchant(OTHER, "ENCHANT_RUN", 0.1);
        TeleportPlayerAt(3);
        int lv = GetStage(1);
        char buff[192];

        NoxSprintfString(buff, "시작버튼을 누르셨습니다, 잠시 후 스테이지%d 이 시작됩니다.", &lv, 1);
        UniPrintToAll(ReadStringAddressEx(buff));
        FrameTimer(150, ControlStage);
    }
}

void ControlStage()
{
    WriteLog("ControlStage");
    if (GetStage(0) == 21)
    {
        UniPrintToAll("승리__ 21 개의 모든 스테이지를 클리어 하셨습니다_!!");
        TeleportPlayerAt(3);
        AudioEvent("FlagCapture", 3);
        Effect("WHITE_FLASH", GetWaypointX(3), GetWaypointY(3), 0.0, 0.0);
        MoveWaypoint(3, 2465.0, 2321.0);
        FrameTimer(3, StrVictory);
    }
    else
    {
        SwitchingCaptainDialog(GetCaptain(), FALSE);
        PushTimerQueue(150, 90, SpawnMonster);
        WriteLog("call timer SpawnMonster");
    }
    WriteLog("ControlStage-end");
}

void loopHorrendousStatus(int captain)
{
    if (CurrentHealth(captain))
    {
        if (Distance(GetObjectX(captain), GetObjectY(captain), LocationX(3), LocationY(3)) > 300.0)
        {
            Effect("TELEPORT", GetObjectX(captain), GetObjectY(captain), 0.0, 0.0);
            MoveObject(captain, LocationX(3), LocationY(3));
            Effect("TELEPORT", GetObjectX(captain), GetObjectY(captain), 0.0, 0.0);
        }
        PushTimerQueue(10, captain, loopHorrendousStatus);
    }
}

void HorrendousDie()
{
    UniPrintToAll("임무실패__!! 방금 호렌더스가 적에게 격추되었습니다.");
    UniPrintToAll("생존자들은 호렌더스를 지켜내지 못했습니다");
    MoveObject(Object("PlayerStartPic"), GetWaypointX(36), GetWaypointY(36));
    TeleportPlayerAt(36);
    AudioEvent("ManaBombEffect", 36);
    FrameTimer(3, strMissionFail);
}

int WaveClassCreate(float x,float y)
{
    return CreateObjectAt(MonsterName(GetStage(0) - 1), x,y);
}

void OnEntityHurt()
{
    int poisonlv=IsPoisonedUnit(SELF);

    if (poisonlv)
    {
        int tmp=poisonlv>>1;
        if (!tmp)
            tmp = 1;
        Damage(SELF, 0, tmp, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

static void goAttackToCenter2(int mob)
{
    if (CurrentHealth(mob))
    {
        CreatureFollow(mob, GetCaptain());
        AggressionLevel(mob, 1.0);
    }
}

void SpawnMonster(int index)
{
    if (--index>=0)
    {
        PushTimerQueue(1, index, SpawnMonster);
        int mob = WaveClassCreate(LocationX(3) + MathSine(index * 4, 1000.0), LocationY(3) + MathSine(index * 4 + 90, 1000.0));
        CheckMonsterThing(mob);
        SetUnitMaxHealth(mob, MonsterHealth(GetStage(0) - 1));
        SetCallback(mob, 5, SetDeaths);
        SetCallback(mob, 7, OnEntityHurt);
        AntiCharmSetMonster(mob);
        SetOwner(ParentNode(), mob);
        RetreatLevel(mob, 0.0);
        ResumeLevel(mob, 1.0);
        PushTimerQueue(1, mob, goAttackToCenter2);
    }
}

void ExceptionProperty(int unit)
{
    if (GetStage(0) - 1 == 15)
    {
        CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit) - 36.0, GetObjectY(unit) - 36.0, 400.0);
        SetCallback(unit, 3, GiveWeaponToRedWiz);
    }
}

void GiveWeaponToRedWiz()
{
    if (CurrentHealth(SELF) > 0)
    {
        if (CurrentHealth(OTHER) > 0)
        {
            Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, SELF, 15, 17);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.2);
        AggressionLevel(SELF, 1.0);
    }
}

int GetUnitTopParent(int unit)
{
    int next;

    while (TRUE)
    {
        next = GetOwner(unit);
        if (next)
            unit = next;
        else
            break;
    }
    return unit;
}

int GetKillCreditTopParent()
{
    int *victim = GetMemory(0x979724);

    if (victim != NULLPTR)
    {
        int *attacker = victim[130];

        if (attacker != NULLPTR)
            return GetUnitTopParent(attacker[11]);
    }
    return NULLPTR;
}

void KillEvent(int me)
{
    int kill = GetKillCreditTopParent();

    if (CurrentHealth(kill))
    {
        if (IsPlayerUnit(kill))
            ChangeGold(kill, Random(100, 400));
    }
}

void SetDeaths()
{
    if (++DEATHS == 90)
    {
        UniPrintToAll("이번 스테이지 완료!");
        int cap=GetCaptain();

        PlaySoundAround(cap, 777);
        SwitchingCaptainDialog(cap, TRUE);
    }
    KillEvent(GetTrigger());
    DeleteObjectTimer(SELF, 30);
}

void GiveWeaponToHorrendous()
{
    if (CurrentHealth(SELF))
    {
        if (CurrentHealth(OTHER))
        {
            Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, SELF, 128, 17);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.3);
        AggressionLevel(SELF, 1.0);
    }
}

int GetStage(int mode)
{
    int stage;

    if (mode == 1)
        stage ++;
    return stage;
}

int CharmLight()
{
    int unit;

    if (!unit)
        unit = CreateObject("InvisibleLightBlueHigh", 3);
    return unit;
}

string MonsterName(int num)
{
    string name[] = {
        "WillOWisp",
        "Bat", "SmallAlbinoSpider", "GiantLeech", "Imp", "Swordsman",
        "WhiteWolf", "Scorpion", "Skeleton", "OgreBrute", "Bear",
        "SkeletonLord", "OgreWarlord", "MeleeDemon", "Mimic", "Shade",
        "WeirdlingBeast", "Goon", "Lich", "CarnivorousPlant", "Hecubah",
        };
    return name[num];
}

int MonsterHealth(int num)
{
    int hpTable[] = {
        1,
        30, 60, 90, 120, 150,
        180, 200, 220, 250, 275,
        275, 290, 275, 310, 295,
        320, 335, 340, 340, 360,
    };

    return hpTable[num];
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void PutShopInfoStamp()
{
    int i;

    if (i < 4)
    {
        if (i == 1)
            MoveWaypoint(38, 2541.0, 4323.0);
        else if (i == 2)
            MoveWaypoint(38, 4416.0, 2450.0);
        else if (i == 3)
            MoveWaypoint(38, 2679.0, 688.0);
        i ++;
        FrameTimer(1, strEntryShop);
        FrameTimer(3, PutShopInfoStamp);
    }
}

void StrVictory()
{
	string name = "HealOrb";
	int i = -1, arr[]={
	    2613312, 301998097, 7080064, 1099186194, 35653889, 268762112, 33718242, 16777488, 132155394,134217985,
	    570458248, 2086650888, 536999970, 
    };
	while(++i < 13)
	{
		drawStrVictory(arr[i], name);
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(3);
		pos_y = GetWaypointY(3);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 3);
		if (count % 38 == 37)
			MoveWaypoint(3, GetWaypointX(3) - 74.000000, GetWaypointY(3) + 2.000000);
		else
			MoveWaypoint(3, GetWaypointX(3) + 2.000000, GetWaypointY(3));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(3, pos_x, pos_y);
	}
}

void strMissionFail()
{
	int arr[17];
	string name = "HealOrb";
	int i = 0;
	arr[0] = 272613948; arr[1] = 75782084; arr[2] = 537411713; arr[3] = 1090654737; arr[4] = 606245922; arr[5] = 675873800; arr[6] = 267423498; arr[7] = 1352002; arr[8] = 1073872898; arr[9] = 114; 
	arr[10] = 1889865712; arr[11] = 268697375; arr[12] = 1075856020; arr[13] = 86540160; arr[14] = 16842881; arr[15] = 33428976; arr[16] = 671349764; 
	while(i < 17)
	{
		drawstrMissionFail(arr[i], name);
		i ++;
	}
}

void drawstrMissionFail(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(34);
		pos_y = GetWaypointY(34);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 34);
		if (count % 48 == 47)
			MoveWaypoint(34, GetWaypointX(34) - 94.000000, GetWaypointY(34) + 2.000000);
		else
			MoveWaypoint(34, GetWaypointX(34) + 2.000000, GetWaypointY(34));
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(34, pos_x, pos_y);
	}
}

void strEntryShop()
{
	string name = "ManaBombOrb";
	int arr[]={
        1009246736, 138436548, 135401732, 1200637186, 604520592, 37888398, 151654853, 555880929, 296000, 2568,
	    132096, 285149152, 541129744, 142574600, 270598664, 1881162244, 267452167, 252, 
    }, i=-1;
	while(++i < 18)
		drawstrEntryShop(arr[i], name);
}

void drawstrEntryShop(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(38);
		pos_y = GetWaypointY(38);
	}
	for (i = 1 ; i > 0 && count < 558 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 38);
		if (count % 49 == 48)
			MoveWaypoint(38, GetWaypointX(38) - 96.000000, GetWaypointY(38) + 2.000000);
		else
			MoveWaypoint(38, GetWaypointX(38) + 2.000000, GetWaypointY(38));
		count ++;
	}
	if (count >= 558)
	{
		count = 0;
		MoveWaypoint(38, pos_x, pos_y);
	}
}

int ParentNode()
{
    int node;

    if (!node)
    {
        node = CreateObject("Hecubah", 28);
        Frozen(node, 1);
    }
    return node;
}

float GetRatioUnitWpXY(int unit, int wp, int mode, float size)
{
    if (!mode)
        return (GetObjectX(unit) - GetWaypointX(wp)) * size / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp));
    else
        return (GetObjectY(unit) - GetWaypointY(wp)) * size / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp));
}

void callTurboTrigger(int wp)
{
    int var_0 = CreateObject("FishSmall", wp);

    SetCallback(var_0, 9, turboTrigger);
    CreateObject("FishSmall", wp);
    Frozen(var_0, 1);
    Frozen(var_0 + 1, 1);
}

void turboTrigger()
{
    MoveObject(SELF, GetObjectX(SELF), GetObjectY(SELF));
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 41);
        Frozen(unit, 1);
    }
    return unit;
}

void ShowHorrendousHP()
{
    if (IsObjectOn(GetCaptain()))
    {
        char buff[192];
        int args[]={GetStage(0), CurrentHealth(GetCaptain())};

        NoxSprintfString(buff, "현재 스테이지: %d\n호렌더스 현재체력: %d", args, sizeof(args));
        UniChatMessage(GetMaster(), ReadStringAddressEx(buff), 35);
        SecondTimer(1, ShowHorrendousHP);
    }
}

int CheckPlayer()
{
    int rep = sizeof(m_player);

    while(--rep>=0)
    {
        if (IsCaller(m_player[rep]))
            break;
    }
    return rep;
}

int CheckPlayerWithId(int pUnit)
{
    int rep=sizeof(m_player);

    while (--rep>=0)
    {
        if (m_player[rep]^pUnit)
            continue;       
        break;
    }
    return rep;
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

int PlayerClassOnInit(int plr, int pUnit, char *userInit)
{
    m_player[plr]=pUnit;
    m_pFlags[plr]=1;
    m_pUPGRADE[plr]=0;

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
            ClientsideProcess();
        userInit[0] = TRUE;
    }
    // Enchant(pUnit, "ENCHANT_BLINDED", 0.8);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    EmptyAll(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));
    return plr;
}

void PlayerClassOnJoin(int plr, int user)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    // if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
    //     PlayerSetAllBuff(pUnit);
    EntryPlayer(plr, user);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
    EnchantOff(user,"ENCHANT_ANTI_MAGIC");
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(44), LocationY(44));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int plr)
{
    // MoveObject(user, LocationX(474), LocationY(474));
    // Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    // FrameTimerWithArg(3, plr, PlayerClassOnJoin);
    // UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
    PlayerClassOnJoin(plr, user);
}

static void thisMapInformationPrintMessage(int user)
{
    UniPrint(user, "<<---호렌더스 수호하기---제작. 231 ----------------------------------------<");
    PlaySoundAround(user, SOUND_JournalEntryAdd);
}

void PlayerClassOnEntry(int plrUnit)
{
    WriteLog("PlayerClassOnEntry:start");
    while (TRUE)
    {
        if (!CurrentHealth(plrUnit))
            break;
        int plr = CheckPlayerWithId(plrUnit), rep = sizeof(m_player);
        char initialUser=FALSE;

        while (--rep>=0&&plr<0)
        {
            if (!MaxHealth(m_player[rep]))
            {
                plr = PlayerClassOnInit(rep, plrUnit, &initialUser);
                PushTimerQueue(66, plrUnit, thisMapInformationPrintMessage);
                break;
            }
        }
        if (plr >= 0)
        {
            if (initialUser)
                OnPlayerDeferredJoin(plrUnit, plr);
            else
                PlayerClassOnJoin(plr, plrUnit);
            break;
        }
        PlayerClassOnEntryFailure(plrUnit);
        break;
    }
    WriteLog("PlayerClassOnEntry:end");
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (CheckPlayer() >= 0)
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(44), LocationY(44));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

