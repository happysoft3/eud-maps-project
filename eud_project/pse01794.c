
#include "pse01794_sub.h"
#include "libs\buff.h"
#include "libs\fxeffect.h"
#include "libs\spellutil.h"
#include "libs\itemproperty.h"
#include "libs\cweaponproperty.h"
#include "libs/potionpickup.h"
#include "libs\monsteraction.h"
#include "libs\weaponcapacity.h"
#include "libs\potionex.h"
#include "libs/format.h"
#include "libs/indexloop.h"

int m_masterMob;
int XMonStat = 0;
int XMainFlag = 0;
int player[20];
int XTelpo;
int XItemNode;
int XCreCount = 0;

int *m_pImgVector;

/****
def BitShow(num):
	bitArr=[]
	t=1<<31
	for i in range(32):
		if num&t:
			bitArr.append(1)
		else:
			bitArr.append(0)
		t=t>>1
	print(bitArr)
****/

int CreClassGetCurrentCount()
{
    return XCreCount & 0x1f;
}

void CreClassSetCurrentCount(int setTo)
{
    XCreCount = (XCreCount & (~0x1f)) ^ setTo;
}

void CreClassIncreaseCurrentCount()
{
    CreClassSetCurrentCount(CreClassGetCurrentCount() + 1);
}

void CreClassDecreaseCurrentCount()
{
    if (CreClassGetCurrentCount())
        CreClassSetCurrentCount(CreClassGetCurrentCount() - 1);
}

int LineStatClassMobCountCheck()
{
    return XMonStat & 0xffff;
}

int LineStatClassKillCountCheck()
{
    return XMonStat >> 0x10;
}

void LineStatClassMobCountSet(int amount)
{
    XMonStat = (XMonStat & (~0xffff)) ^ amount;
}

void LineStatClassKillCountSet(int amount)
{
    XMonStat = (XMonStat & 0xffff) ^ (amount << 0x10);
}

int GameClassMobAccessCheck()
{
    return XMainFlag & 0xf;
}

void GameClassMobAccessSet(int setTo)
{
    XMainFlag = (XMainFlag & (~0xf)) ^ (setTo & 0xf);
}

int GameClassLifeCheck()
{
    return XMainFlag >> 0x18;
}

void GameClassLifeSet(int setTo)
{
    XMainFlag = (XMainFlag & 0xffffff) ^ ((setTo & 0xff) << 0x18);
}

int GameClassWaveCheck()
{
    return (XMainFlag >> 0x10) & 0xff;
}

void GameClassWaveSet(int setTo)
{
    XMainFlag = (XMainFlag & 0xff00ffff) ^ ((setTo & 0xfff) << 0x10);
}

int GameClassMainIsShutdown()
{
    return XMainFlag & 0x10;
}

void GameClassMainSetShutdown()
{
    XMainFlag = XMainFlag ^ 0x10;
}

int GameClassLineIsStart()
{
    return XMainFlag & 0x20;
}

void GameClassLineStartSet()
{
    XMainFlag = XMainFlag ^ 0x20;
}

static int PotionPickupMaxCount()
{
    return 6;
}

void MapExit()
{
    MusicEvent();
    DeinitializeSubpart();
    ResetHostileCritter();
}

void MapInitialize()
{
    MusicEvent();
    InitializeSubpart();
    FrameTimer(1, DelayInitRun); 
    ImportMonsterStrikeFunc();
}

void ImpShotCollide()
{
    int owner = GetOwner(SELF);

    while (owner)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 20, DAMAGE_TYPE_FLAME);
            Effect("EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

void LoopCatchImpshot(int curId, int owner)
{
    // int mis = MagicMissileUtilSummon("DeathBallFragment", owner, 0.0, UnitAngleCos(owner, 11.0), UnitAngleSin(owner, 11.0));

    SetUnitCallbackOnCollide(curId, ImpShotCollide);
    // Delete(curId);
}



void SpiderWebCollide()
{
    int owner = GetOwner(SELF);

    while (owner)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 14, DAMAGE_TYPE_CRUSH);
            Enchant(OTHER, EnchantList(4), 3.0);
            MakePoisoned(OTHER, owner);
            if (IsPlayerUnit(OTHER))
                UniPrint(OTHER, "거미줄이 당신의 움직임을 둔화 시키고 있습니다");
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

static void onSpiderSpitDetected(int cur, int owner)
{
    SetUnitCallbackOnCollide(cur, ImpShotCollide);
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 708, LoopCatchImpshot);  //ImpShot
    HashPushback(pInstance, 524, onSpiderSpitDetected); //spiderSpit
    HashPushback(pInstance, 526, HarpoonEvent);
    HashPushback(pInstance, 1177, ChakramOnThrow);
    HashPushback(pInstance, 706, ShotSmallDeathBall);
    HashPushback(pInstance, 709, ShotMagicMissile);
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

int CheckPlayerScrIndex(int pUnit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (!(player[i] ^ pUnit))
            return i;
    }
    return -1;
}

int PlayerClassOnInit(int pUnit, int plr)
{
    int res = plr;

    player[plr] = pUnit;
    player[plr + 10] = 1;
    EmptyAll(pUnit);
        SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));
    FrameTimerWithArg(90, pUnit, UserShowGameInfo);
    return res;
}

void RespawnPlayer(int location, int pUnit)
{
    MoveObject(pUnit, LocationX(location), LocationY(location));
    PlaySoundAround(pUnit, 6);
    DeleteObjectTimer(CreateObjectAt("BlueRain", LocationX(location), LocationY(location)), 21);
    Effect("TELEPORT", LocationX(location), LocationY(location), 0.0, 0.0);
}

void PlayerClassOnJoin(int pUnit, int plr)
{
    if (PlayerClassDeathFlagCheck(plr))
        PlayerClassDeathFlagSet(plr);
    Enchant(pUnit, EnchantList(25), 2.0);
    Enchant(pUnit, EnchantList(14), 0.0);
    RespawnPlayer(15, pUnit);
}

void PlayerClassOnFailedEntry(int pUnit)
{
    if (GameClassMainIsShutdown())
    {
        MoveObject(pUnit, LocationX(30), LocationY(30));
        UniPrint(pUnit, "게임 오버. ..");
    }
    else
    {
        MoveObject(pUnit, LocationX(13), LocationY(13));
        UniPrint(pUnit, "지도 입장에 실패하였습니다");
    }
}

void TeleportPlayer(int pArg)
{
    int location = pArg & 0x3ff, plr = (pArg >> 10) & 0x3ff;
    int pUnit = player[plr];

    if (CurrentHealth(pUnit))
        MoveObject(pUnit, LocationX(location), LocationY(location));
}

void PlayerClassFirstJoin(int pUnit, int plr)
{
    MoveObject(pUnit, LocationX(13), LocationY(13));
    FrameTimerWithArg(5, (plr << 10) | 11, TeleportPlayer);
    UniPrint(pUnit, "지도에 입장을 시도하고 있습니다... 잠시만 기다려 주세요");
}

void PlayerClassEntry()
{
    int i, plr;

    while (1)
    {
        if (CurrentHealth(OTHER) && !GameClassMainIsShutdown())
        {
            plr = CheckPlayer();
            for (i = 9 ; i >= 0 && plr < 0 ; i --)
            {
                if (!MaxHealth(player[i]))
                {
                    plr = PlayerClassOnInit(GetCaller(), i);
                    break;
                }
            }
            if (plr + 1)
            {
                if (plr >> 8)
                    PlayerClassFirstJoin(OTHER, plr & 0xff);
                else
                    PlayerClassOnJoin(OTHER, plr);
                break;
            }
        }
        PlayerClassOnFailedEntry(OTHER);
        break;
    }
}

void PlayerClassFastJoin()
{
    int pIndex;

    if (CurrentHealth(OTHER))
    {
        pIndex = CheckPlayer();
        if (pIndex + 1)
            MoveObject(OTHER, LocationX(11), LocationY(11));
        else
            MoveObject(OTHER, LocationX(12), LocationY(12));
    }
}

void AbilityAwardFx(int sUnit)
{
    PlaySoundAround(sUnit, 226);
    GreenSparkFx(GetObjectX(sUnit), GetObjectY(sUnit));
    Effect("WHITE_FLASH", GetObjectX(sUnit), GetObjectY(sUnit), 0.0, 0.0);
}

void WarAbilityAwardWarcry(int owner, int plr)
{
    if (PlayerClassSkillFlagCheck2(plr))
        UniPrint(owner, "이미 이 능력을 배우셨습니다 -향상된 전쟁의 함성");
    else
    {
        PlayerClassSkillFlagSet2(plr);
        AbilityAwardFx(owner);
        UniPrint(owner, "향상된 전쟁의 함성을 배우셨습니다! 주변 유닛 데미지 85");
    }
}

void WarAbilityAwardHarpoon(int owner, int plr)
{
    if (PlayerClassSkillFlagCheck3(plr))
        UniPrint(owner, "이미 이 능력을 배우셨습니다 -향상된 작살");
    else
    {
        PlayerClassSkillFlagSet3(plr);
        AbilityAwardFx(owner);
        Enchant(owner, EnchantList(30), 0.0);
        UniPrint(owner, "향상된 작살을 배우셨습니다! 일직선 범위 데미지 100");
    }
}

void WarAbilityAwardThreadLigthly(int owner, int plr)
{
    if (PlayerClassSkillFlagCheck1(plr))
        UniPrint(owner, "이미 이 능력을 배우셨습니다 -윈드 부스터");
    else
    {
        PlayerClassSkillFlagSet1(plr);
        AbilityAwardFx(owner);
        UniPrint(owner, "윈드 부스터을 배우셨습니다! 짧은 거리를 빠르게 이동합니다");
    }
}

void BookOfAbilityCheck(int curId, int owner)
{
    int ptr = UnitToPtr(curId), type;
    int plr = CheckPlayerFromUnitId(owner);

    if (ptr && plr + 1)
    {
        while (1)
        {
            type = GetMemory(GetMemory(ptr + 0x2e0)) & 0xff;
            if (type == 2)
                WarAbilityAwardWarcry(owner, plr);
            else if (type == 3)
                WarAbilityAwardHarpoon(owner, plr);
            else if (type == 4)
                WarAbilityAwardThreadLigthly(owner, plr);
            else
                break;
            Delete(curId);
            break;
        }
    }
}

void InventoryChecking(int pUnit)
{
    int inv = GetLastItem(pUnit);

    if (GetUnitThingID(inv) == 2676)
        BookOfAbilityCheck(inv, pUnit);
}

void UserPoisonedHandler(int sPlr)
{
    int arr[10], plrUnit = player[sPlr];

    if (UnitCheckEnchant(plrUnit, GetLShift(18)))
    {
        if (IsPoisonedUnit(plrUnit))
        {
            CastSpellObjectObject("SPELL_CURE_POISON", plrUnit, plrUnit);
            Effect("VIOLET_SPARKS", GetObjectX(plrUnit), GetObjectY(plrUnit), 0.0, 0.0);
        }
    }
    else if (IsPoisonedUnit(plrUnit))
    {
        if (arr[sPlr] < 60)
            arr[sPlr] += IsPoisonedUnit(plrUnit);
        else
        {
            Damage(plrUnit, 0, IsPoisonedUnit(plrUnit), 5);
            arr[sPlr] = 0;
        }
    }
    else if (arr[sPlr])
        arr[sPlr] = 0;
}

void PlayerClassStatCheck(int plr)
{
    int pUnit = player[plr];

    InventoryChecking(pUnit);
    UserPoisonedHandler(plr);
    if (UnitCheckEnchant(pUnit, GetLShift(31)))
    {
        EnchantOff(pUnit, EnchantList(31));
        RemoveTreadLightly(pUnit);
        if (PlayerClassSkillFlagCheck1(plr))
            SkillSetWindBooster(pUnit);
    }
    if (PlayerClassSkillFlagCheck2(plr))
    {
        WarAbilityUse(pUnit, 2, SkillSetWarCry);
    }
}

void SkillSetWindBooster(int pUnit)
{
    PushObjectTo(pUnit, UnitAngleCos(pUnit, 80.0), UnitAngleSin(pUnit, 80.0));
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void ManaBombCancelFx(int sUnit)
{
    int caster = CreateObjectAt("ImaginaryCaster", GetObjectX(sUnit), GetObjectY(sUnit));

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}

void UnitVisibleSplashA()
{
    int parent = GetOwner(SELF);
    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (CurrentHealth(GetOwner(parent)))
    {
        if (GetUnit1C(OTHER) ^ spIdx)
        {
            if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
            {
                Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
                SetUnit1C(OTHER, spIdx);
            }
        }
    }
}

void SplashDamageAt(int owner, int dam, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, k, SplashIdx;

    SplashIdx ++;
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(dam));
    for (k = 0 ; k < 4 ; k ++)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 64);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplashA);
    }
    DeleteObjectTimer(ptr - 1, 2);
    DeleteObjectTimer(ptr - 2, 2);
}

void SkillSetWarCry(int pUnit)
{
    SplashDamageAt(pUnit, 130, GetObjectX(pUnit), GetObjectY(pUnit), 200.0);
    ManaBombCancelFx(pUnit);
    UniChatMessage(pUnit, "프리 홍콩!! 타이완 넘버 원!!", 120);
}

int WarAbilityTable(int aSlot, int pIndex)
{
    return GetMemory(0x753600 + (pIndex * 24) + (aSlot * 4));
}

void WarAbilityUse(int pUnit, int aSlot, int actionFunction)
{
    int chk[160], pIndex = GetPlayerIndex(pUnit), cTime;
    int arrPic;

    if (!(pIndex >> 0x10))
    {
        arrPic = pIndex * 5 + aSlot; //EyeOf=5, harpoon=3, sneak=4, berserker=1
        cTime = WarAbilityTable(aSlot, pIndex);
        if (cTime ^ chk[arrPic])
        {
            if (!chk[arrPic])
            {
                CallFunctionWithArg(actionFunction, pUnit);
            }
            chk[arrPic] = cTime;
        }
    }
}

int PlayerClassDeathFlagCheck(int pIndex)
{
    return player[pIndex + 10] & 0x02;
}

void PlayerClassDeathFlagSet(int pIndex)
{
    player[pIndex + 10] = player[pIndex + 10] ^ 0x02;
}

int PlayerClassSkillFlagCheck1(int pIndex)
{
    return player[pIndex + 10] & 0x04;
}

void PlayerClassSkillFlagSet1(int pIndex)
{
    player[pIndex + 10] = player[pIndex + 10] ^ 4;
}

int PlayerClassSkillFlagCheck2(int pIndex)
{
    return player[pIndex + 10] & 0x08;
}

void PlayerClassSkillFlagSet2(int pIndex)
{
    player[pIndex + 10] = player[pIndex + 10] ^ 8;
}

int PlayerClassSkillFlagCheck3(int pIndex)
{
    return player[pIndex + 10] & 0x10;
}

void PlayerClassSkillFlagSet3(int pIndex)
{
    player[pIndex + 10] = player[pIndex + 10] ^ 0x10;
}

void ProtectItemTryPick()
{
    int cFps, ptr;

    if (IsPlayerUnit(OTHER))
    {
        ptr = UnitToPtr(SELF);
        while (1)
        {
            if (GetOwner(SELF) ^ GetCaller())
            {
                cFps = GetMemory(0x84ea04);
                if (!GetMemory(ptr + 0x2f4))
                    SetMemory(ptr + 0x2f4, cFps);
                if (ABS(cFps - GetMemory(ptr + 0x2f4)) >= 90)
                    1;
                else
                {
                    UniPrint(OTHER, "아직 이 아이템을 픽업하실 수 없습니다. 소유자가 있는 아이템 입니다");
                    break;
                }
            }
            SetMemory(ptr + 0x2c4, GetMemory(ptr + 0x1c));
            SetMemory(ptr + 0x1c, 0);
            SetMemory(ptr + 0x228, 0);
            SetMemory(ptr + 0x2f4, 0);
            Pickup(OTHER, SELF);
            break;
        }
    }
}

void LaiserParTouch()
{
    int owner = ToInt(GetObjectZ(GetOwner(SELF)));

    if (IsAttackedBy(OTHER, owner) && CurrentHealth(OTHER))
    {
        Damage(OTHER, owner, 100, 14);
        Enchant(OTHER, EnchantList(28), 0.5);
    }
}

void HarpoonEvent(int curId, int owner)
{
    float xVect, yVect;
    int i, unit;

    if (UnitCheckEnchant(owner, GetLShift(30)))
    {
        xVect = UnitAngleCos(owner, 30.0);
        yVect = UnitAngleSin(owner, 30.0);

        Delete(curId);
        TeleportLocation(1, GetObjectX(owner) + xVect, GetObjectY(owner) + yVect);
        unit = CreateObjectAt("InvisibleLightBlueLow", LocationX(1), LocationY(1)) + 1;
        Raise(unit - 1, owner);
        for (i = 0 ; i < 15 ; i ++)
        {
            Frozen(CreateObjectAt("Maiden", LocationX(1), LocationY(1)), 1);
            SetCallback(unit + i, 9, LaiserParTouch);
            SetUnitFlags(unit + i, GetUnitFlags(unit + i) ^ 0x2000);
            SetOwner(unit - 1, unit + i);
            DeleteObjectTimer(unit + i, 1);
            PlaySoundAround(unit + i, 299);
            TeleportLocationVector(1, xVect, yVect);
        }
        DeleteObjectTimer(unit - 1, 30);
        Effect("SENTRY_RAY", GetObjectX(unit - 1), GetObjectY(unit - 1), LocationX(1), LocationY(1));
    }
}

void ChakramOnThrow(int curId, int owner)
{ }

void ShotSmallDeathBall(int curId, int owner)
{ }

void ShotMagicMissile( int curId, int owner)
{ }

void PlayerClassOnDeath(int pIndex)
{
    char buff[128], *pUsername = StringUtilGetScriptStringPtr(PlayerIngameNick(player[pIndex]));

    NoxSprintfString(buff, "%s 님께서 적에게 격추되었습니다", &pUsername, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

void PlayerClassOnFree(int pIndex)
{
    player[pIndex] = 0;
    player[pIndex + 10] = 0;
}

void PlayerClassLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassStatCheck(i);
                    break;
                }
                else
                {
                    if (!PlayerClassDeathFlagCheck(i))
                    {
                        PlayerClassDeathFlagSet(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassLoop);
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

int CheckPlayerFromUnitId(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (!(player[i] ^ unit))
            return i;
    }
    return -1;
}

void GreenSparkFx(float xProfile, float yProfile)
{
    int fxTarget = CreateObjectAt("MonsterGenerator", xProfile, yProfile);

    Damage(fxTarget, 0, 1, -1);
    Delete(fxTarget);
}

void TeleportProgress(int sUnit)
{
    int owner = GetOwner(sUnit), dest = ToInt(GetObjectZ(sUnit)), count = GetDirection(sUnit);

    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                if (DistanceUnitToUnit(sUnit, owner) < 23.0)
                {
                    LookWithAngle(sUnit, count - 1);
                    FrameTimerWithArg(1, sUnit, TeleportProgress);
                    break;
                }
            }
            else
            {
                Effect("TELEPORT", GetObjectX(sUnit), GetObjectY(sUnit), 0.0, 0.0);
                Effect("SMOKE_BLAST", GetObjectX(sUnit), GetObjectY(sUnit), 0.0, 0.0);
                MoveObject(owner, GetObjectX(dest), GetObjectY(dest));
                PlaySoundAround(owner, 6);
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            }
            EnchantOff(owner, "ENCHANT_BURNING");
        }
        Delete(sUnit);
        Delete(++sUnit);
        break;
    }
}

void TeleportPortal()
{
    int unit;

    if (CurrentHealth(OTHER) && IsObjectOn(OTHER))
    {
        if (!UnitCheckEnchant(OTHER, GetLShift(12)))
        {
            Enchant(OTHER, EnchantList(12), 4.0);
            unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));
            Raise(unit, GetTrigger() + 1);
            LookWithAngle(unit, 48); //TODO: 1.XX seconds...
            CreateObjectAt("VortexSource", GetObjectX(unit), GetObjectY(unit));
            Effect("YELLOW_SPARKS", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
            SetOwner(OTHER, unit);
            TeleportProgress(unit);
            PlaySoundAround(OTHER, 772);
            UniPrint(OTHER, "공간이동을 준비 중 입니다. 취소하려면 움직이세요");
        }
    }
}

int TeleportSetup(int srcWp, int dstWp)
{
    int unit = CreateObjectAt("WeirdlingBeast", LocationX(srcWp), LocationY(srcWp));

    SetUnitMaxHealth(CreateObjectAt("InvisibleLightBlueHigh", LocationX(dstWp), LocationY(dstWp)) - 1, 10);
    Enchant(CreateObjectAt("InvisibleLightBlueHigh", LocationX(srcWp), LocationY(srcWp)), "ENCHANT_ANCHORED", 0.0);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, TeleportPortal);

    return unit;
}

void ModifySetupTeleportDestination(int sUnit, int modLocation)
{
    if (MaxHealth(sUnit))
    {
        if (IsObjectOn(sUnit + 1))
            MoveObject(sUnit + 1, LocationX(modLocation), LocationY(modLocation));
    }
}

void Description()
{
    SellGermPlace(50);
    RepairItemPlaceKp(51);
    SellGermPlace(52);
    RepairItemPlaceKp(53);
    RegistSignMessage(Object("MPik4"), "중국인 입국 막기를 시작하려면 오른쪽 스위치를 누르세요");
    RegistSignMessage(Object("MPik3"), "이 선을 넘어가면 라이프가 하나 씩 깎임!!");
    RegistSignMessage(Object("MPik2"), "입국 심사장: 저 너머 문을 통해 중국인들이 입국을 시작한다. 우리는 그들을 막아야만 한다");
    RegistSignMessage(Object("MPik1"), "크리쳐 지우기 비콘: 지우려는 크리쳐를 이 비콘 위에 올리면 지울 수 있습니다. 3천골드를 되돌려 받게 돼요");
}

void DelayInitRun()
{
    ManaTileAll(18);
    FrameTimer(5, Description);
    m_masterMob=CreateObjectAt("Hecubah", LocationX(14), LocationY(14));
    Frozen(m_masterMob, TRUE);
    XTelpo = TeleportSetup(22, 16);
    XItemNode = CreateObjectAt("ImaginaryCaster", LocationX(1), LocationY(1));

    GameClassLifeSet(20);
    FrameTimerWithArg(1, 20 | (28 << 8), PlaceDefaultItems);
    FrameTimer(1, PlayerClassLoop);
    FrameTimer(1, SetHostileCritter);
    FrameTimer(1, ShopClassInitPlace);
    FrameTimer(1, CreClassMarketPlace);
    FrameTimer(1, LineSetInit);
}

void NoKeepZoneSystem(int sUnit, int backLocation)
{
    if (IsPlayerUnit(sUnit))
    {
        MoveObject(sUnit, LocationX(backLocation), LocationY(backLocation));
        UniPrint(sUnit, "이봐... 당신은 이곳에 접근할 수 없어!");
    }
}

void HumanNoKeepNorth()
{
    NoKeepZoneSystem(OTHER, 16);
}

void HumanNoKeepSouth()
{
    NoKeepZoneSystem(OTHER, 21);
}

int CheckMonsterSection(float xProfile, float yProfile)
{
    int location = 16, i;

    for (i = 0 ; i < 5 ; i ++)
    {
        if (Distance(xProfile, yProfile, LocationX(location + i), LocationY(location + i)) < 990.0)
            return i + 1;
    }
    return 0;
}

void MonsterEntrySection()
{
    int curSection;

    if (IsMonsterUnit(OTHER))
    {
        curSection = CheckMonsterSection(GetObjectX(OTHER), GetObjectY(OTHER));
        if (curSection)
        {
            if (curSection > GameClassMobAccessCheck())
            {
                ModifySetupTeleportDestination(XTelpo, 16 + curSection);
                GameClassMobAccessSet(curSection);
            }
        }
    }
}

void RespawnLocationReset()
{
    ModifySetupTeleportDestination(XTelpo, 16);
}

void GetUnitXYProfile(int sUnit, int dest)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        SetMemory(dest, GetMemory(ptr + 0x38));
        SetMemory(dest + 4, GetMemory(ptr + 0x3c));
    }
}

int AnyargUnitOrLocation(int anyArg)
{
    float xProfile, yProfile;
    int link;

    if (!link)
    {
        link = &xProfile;
    }
    if (anyArg >> 0x18)
        GetUnitXYProfile(anyArg, link);
    else
    {
        xProfile = LocationX(anyArg & 0xfff);
        yProfile = LocationY(anyArg & 0xfff);
    }
    return link;
}

int WillOWispBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1819044183; arr[1] = 1936283471; arr[2] = 112; arr[17] = 500; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1069547520; arr[25] = 1; arr[26] = 3; 
		arr[28] = 1120403456; arr[29] = 10; arr[31] = 9; arr[32] = 15; arr[33] = 31; 
		arr[53] = 1128792064; arr[54] = 4; arr[59] = 5542784; arr[60] = 1326; arr[61] = 46913280; 
		link = arr;
	}
	return link;
}

void WillOWispSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1069547520);
		SetMemory(ptr + 0x224, 1069547520);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32769);
		SetMemory(GetMemory(ptr + 0x22c), 500);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 500);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WillOWispBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void FONCollide()
{
    int owner = GetOwner(SELF);

    while (owner)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 130, 14);
            Enchant(OTHER, EnchantList(4), 1.0);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

int FONCreate(int me, float xProfile, float yProfile)
{
    int mis = CreateObjectAt("DeathBall", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2e8, 5483536); //projectile update
        SetUnitCallbackOnCollide(mis, FONCollide);
        SetOwner(me, mis);
    }
    return mis;
}

void AbsoluteTargetStrike(int owner, int target, float threshold, int func)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target), GetObjectY(target));

    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner)), threshold);

    SetOwner(owner, unit);
    Raise(unit, target);
    FrameTimerWithArg(1, unit, func);
}

void OrbHecubahOneShot(int sUnit)
{
    int owner = GetOwner(sUnit), target = ToInt(GetObjectZ(sUnit)), mis;
    float dt = Distance(GetObjectX(sUnit), GetObjectY(sUnit), GetObjectX(target), GetObjectY(target));
    float thresHold;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        mis = FONCreate(owner, GetObjectX(owner) + UnitRatioX(target, owner, 19.0), GetObjectY(owner) + UnitRatioY(target, owner, 19.0));
        thresHold = DistanceUnitToUnit(mis, target) / GetObjectZ(sUnit + 1);
        MoveObject(sUnit, GetObjectX(target) + UnitRatioX(target, sUnit, dt * thresHold), GetObjectY(target) + UnitRatioY(target, sUnit, dt * thresHold));
        if (IsVisibleTo(sUnit, owner))
        {
            PushObject(mis, -41.0, GetObjectX(sUnit), GetObjectY(sUnit));
        }
        else
        {
            PushObject(mis, -41.0, GetObjectX(target), GetObjectY(target));
        }
        DeleteObjectTimer(mis, 21);
    }
    Delete(sUnit);
    Delete(sUnit + 1);
}

void RedWizStrikeTrigger()
{
    int victim = GetVictimUnit2(SELF);

    if (MaxHealth(victim))
    {
        MonsterForceCastSpell(SELF, 0, GetObjectX(SELF), GetObjectY(SELF));
        Damage(OTHER, SELF, 80, 16);
        Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(victim), GetObjectY(victim));
    }
}

void OrbHecubahStrikeTrigger()
{
    int victim = GetVictimUnit2(SELF);

    if (MaxHealth(victim))
    {
        MonsterForceCastSpell(SELF, 0, GetObjectX(SELF), GetObjectY(SELF));
        AbsoluteTargetStrike(GetTrigger(), victim, 85.0, OrbHecubahOneShot);
    }
}

void WispStrikeTrigger()
{
    int victim = GetVictimUnit2(SELF);

    if (CurrentHealth(victim))
    {
        Damage(victim, SELF, 10, 9);
        MonsterForceCastSpell(SELF, 0, GetObjectX(SELF), GetObjectY(SELF));
        Effect("LIGHTNING", GetObjectX(victim), GetObjectY(victim), GetObjectX(SELF), GetObjectY(SELF));
    }
}

int MonTypeFrog(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("GreenFrog", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    GreenFrogSubProcess(unit);
    return unit;
}

int BatBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 7627074; arr[17] = 60; arr[18] = 1; arr[19] = 95; arr[21] = 1056964608; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1097859072; arr[29] = 4; arr[31] = 8; arr[59] = 5542784; arr[60] = 1357; 
		arr[61] = 46906368; 
		link=arr;
	}
	return link;
}

void BatSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077306982);
		SetMemory(ptr + 0x224, 1077306982);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 60);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 60);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BatBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1056964608);
	}
}

int MonTypeBat(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Bat", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    BatSubProcess(unit);
    return unit;
}

int SmallAlbinoSpiderBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1818324307; arr[1] = 1651261804; arr[2] = 1399811689; arr[3] = 1701079408; arr[4] = 114; 
		arr[17] = 75; arr[18] = 1; arr[19] = 76; arr[21] = 1065353216; arr[23] = 34816; 
		arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; arr[28] = 1097859072; arr[29] = 6; 
		arr[31] = 8; arr[59] = 5542784; arr[60] = 1356; arr[61] = 46906624; 
		link=arr;
	}
	return link;
}

void SmallAlbinoSpiderSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074916229);
		SetMemory(ptr + 0x224, 1074916229);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 75);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 75);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, SmallAlbinoSpiderBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeSmallSpider(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("SmallAlbinoSpider", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SmallAlbinoSpiderSubProcess(unit);
    return unit;
}

int MonTypeLeech(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("GiantLeech", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 80);
    return unit;
}

int MonTypeSwordman(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Swordsman", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 130);
    return unit;
}

int FlyingGolemBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1769565254; arr[1] = 1866950510; arr[2] = 7169388; arr[17] = 98; arr[18] = 10; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1065353216; arr[26] = 4; 
		arr[37] = 1701605191; arr[38] = 1920090477; arr[39] = 30575; arr[53] = 1125515264; arr[54] = 4; 
		arr[55] = 25; arr[56] = 25; arr[60] = 1316; arr[61] = 46903808; 
		link=arr;
	}
	return link;
}

void FlyingGolemSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076677837);
		SetMemory(ptr + 0x224, 1076677837);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32769);
		SetMemory(GetMemory(ptr + 0x22c), 98);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 98);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FlyingGolemBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeMecaFlying(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("FlyingGolem", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    FlyingGolemSubProcess(unit);
    return unit;
}

int MonTypeUrchin(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Urchin", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 120);
    UnitZeroFleeRange(unit);
    return unit;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 180; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1097859072; arr[29] = 25; arr[31] = 8; arr[32] = 13; arr[33] = 21; 
		arr[34] = 4; arr[35] = 2; arr[36] = 9; arr[37] = 1684631635; arr[38] = 1884516965; 
		arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link=arr;
	}
	return link;
}

void BlackWidowSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 180);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 180);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeSpider(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("BlackWidow", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    BlackWidowSubProcess(unit);
    return unit;
}

int MonTypeArcher(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Archer", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 180);
    return unit;
}

int MonTypeScorpion(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Scorpion", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 225);
    return unit;
}

int MonTypeGargoyle(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("EvilCherub", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 180);
    return unit;
}

int MonTypeSkeleton(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Skeleton", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 260);
    return unit;
}

int MonTypeOgreAxe(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("GruntAxe", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 275);
    return unit;
}

int MonTypeOgre(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("OgreBrute", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 306);
    return unit;
}

int MonTypeBear(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Bear", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 425);
    return unit;
}

int MonTypeSkullLord(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("SkeletonLord", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 325);
    return unit;
}

int MonTypeShade(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Shade", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 275);
    return unit;
}

int MonTypeOgreLord(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("OgreWarlord", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 360);
    return unit;
}

int MonTypeFirebat(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    string fbat[] = {"EmberDemon", "MeleeDemon", "MeleeDemon", "MeleeDemon"};
    int unit = CreateObjectAt(fbat[Random(0, 3)], GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 225);
    return unit;
}

int MonTypeImp(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Imp", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 192);
    return unit;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 168; arr[19] = 71; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[27] = 3; arr[28] = 1106247680; 
		arr[29] = 10; arr[31] = 10; arr[32] = 13; arr[33] = 21; arr[37] = 1769236816; 
		arr[38] = 1181513062; arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 18; 
		arr[56] = 28; arr[58] = 5545472; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

void FireSpriteSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074287083);
		SetMemory(ptr + 0x224, 1074287083);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 168);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 168);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeFireFairy(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("FireSprite", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    FireSpriteSubProcess(unit);
    return unit;
}

int NecromancerBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919116622; arr[1] = 1851878767; arr[2] = 7497059; arr[17] = 370; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1070302495; arr[25] = 1; arr[26] = 2; 
		arr[28] = 1103626240; arr[29] = 48; arr[30] = 1092616192; arr[31] = 11; arr[32] = 7; 
		arr[33] = 15; arr[34] = 1; arr[35] = 1; arr[36] = 10; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

void NecromancerSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1072064102);
		SetMemory(ptr + 0x224, 1072064102);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 370);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 370);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, NecromancerBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeNecromancer(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Necromancer", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    NecromancerSubProcess(unit);
    return unit;
}

int HecubahBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 720; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 130; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; 
		arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

void HecubahSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 720);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 720);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeHecubah(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Hecubah", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    HecubahSubProcess(unit);
    return unit;
}

int MonTypeMecaGolem(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("MechanicalGolem", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 900);
    return unit;
}

int MonTypeWisp(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("WillOWisp", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    WillOWispSubProcess(unit);
    RegistryUnitStrikeFunction(unit, WispStrikeTrigger);
    return unit;
}

int MonTypeGhost(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Ghost", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 160);
    return unit;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 295; arr[19] = 97; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1066192077; arr[26] = 4; arr[28] = 1106247680; arr[29] = 50; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 18; arr[33] = 21; arr[34] = 3; arr[35] = 6; 
		arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = arr;
	}
	return link;
}

void GoonSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077558640);
		SetMemory(ptr + 0x224, 1077558640);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 295);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 295);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeGoon(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int unit = CreateObjectAt("Goon", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    GoonSubProcess(unit);
    SetUnitVoice(unit, 63);
    return unit;
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 425; arr[19] = 126; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1113325568; arr[29] = 100; arr[30] = 1092616192; arr[32] = 15; arr[33] = 21; 
		arr[57] = 5548288; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

void LichLordSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1081207685);
		SetMemory(ptr + 0x224, 1081207685);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 425);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 425);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichLordBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeLichlord(int anyarg)
{
    int xyTable = AnyargUnitOrLocation(anyarg);
    int unit = CreateObjectAt("LichLord", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    LichLordSubProcess(unit);
    return unit;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 325; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 34817; arr[24] = 1065353216; arr[26] = 4; 
		arr[37] = 1919248451; arr[38] = 1916887669; arr[39] = 7827314; arr[53] = 1133903872; arr[55] = 3; 
		arr[56] = 8; arr[58] = 5546320; arr[60] = 1387; arr[61] = 46915328; 
		link = arr;
	}
	return link;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076677837);
		SetMemory(ptr + 0x224, 1076677837);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34817);
		SetMemory(GetMemory(ptr + 0x22c), 325);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 325);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeJandor(int anyarg)
{
    int xyTable = AnyargUnitOrLocation(anyarg);
    int unit = CreateObjectAt("AirshipCaptain", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    AirshipCaptainSubProcess(unit);
    return unit;
}

int CarnivorousPlantBinTable2()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 600; 
		arr[18] = 200; arr[19] = 100; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 4; arr[27] = 5; arr[28] = 1109393408; arr[29] = 100; 
		arr[31] = 8; arr[34] = 3; arr[35] = 3; arr[36] = 20; arr[59] = 5544320; 
		arr[60] = 1371; arr[61] = 46901760; 
		link = arr;
	}
	return link;
}

void CarnivorousPlantSubProcess2(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 600);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 600);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, CarnivorousPlantBinTable2());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeCarnPlant(int anyarg)
{
    int xyTable = AnyargUnitOrLocation(anyarg);
    int unit = CreateObjectAt("CarnivorousPlant", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    CarnivorousPlantSubProcess2(unit);
    return unit;
}

int MonTypeDryad(int anyarg)
{
    int xyTable = AnyargUnitOrLocation(anyarg);
    int unit = CreateObjectAt("WizardGreen", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitMaxHealth(unit, 275);
    Enchant(unit, EnchantList(14), 0.0);

    return unit;
}

int StoneGolemBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852798035; arr[1] = 1819232101; arr[2] = 28005; arr[17] = 750; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 5; 
		arr[28] = 1117782016; arr[29] = 100; arr[31] = 2; arr[32] = 60; arr[33] = 90; 
		arr[58] = 5545472; arr[59] = 5543904; arr[60] = 1324; arr[61] = 46901248; 
		link = arr;
	}
	return link;
}

void StoneGolemSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074161254);
		SetMemory(ptr + 0x224, 1074161254);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 750);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 750);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StoneGolemBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeStoneGiant(int anyarg)
{
    int xyTable = AnyargUnitOrLocation(anyarg);
    int golem = CreateObjectAt("StoneGolem", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    StoneGolemSubProcess(golem);
    return golem;
}

int HorrendousBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 600; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 9; 
		arr[27] = 5; arr[28] = 1109393408; arr[29] = 62; arr[54] = 4; arr[59] = 5542784; 
		arr[60] = 1386; arr[61] = 46907648; 
		link = arr;
	}
	return link;
}

void HorrendousSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 600);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 600);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HorrendousBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeHorrendous(int anyarg)
{
    int xyTable = AnyargUnitOrLocation(anyarg);
    int horr = CreateObjectAt("Horrendous", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    HorrendousSubProcess(horr);
    return horr;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[17] = 325; arr[19] = 65; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; arr[54] = 4; arr[55] = 20; 
		arr[56] = 30; arr[58] = 5546320; 
		link = arr;
	}
	return link;
}

void StrongWizardWhiteSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1073322393);
		SetMemory(ptr + 0x224, 1073322393);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 325);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 325);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MonTypeHovath(int anyarg)
{
    int xyTable = AnyargUnitOrLocation(anyarg);
    int hovath = CreateObjectAt("StrongWizardWhite", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    StrongWizardWhiteSubProcess(hovath);
    return hovath;
}

void GodModePotionFeatureSwap(int potion)
{
    SetUnitCallbackOnUseItem(potion, UseGodModePotion);
    Enchant(potion, EnchantList(22), 0.0);
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);

    return x;
}

void CheckSpecialItem(int weapon)
{
    int thingId = GetUnitThingID(weapon);

    if (thingId >= 222 && thingId <= 225)
    {
        SetItemPropertyAllowAllDrop(weapon);
        DisableOblivionItemPickupEvent(weapon);
    }
    else if (thingId == 1178 || thingId == 1168)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
}

int ItemDropClassHotPotion(int sUnit)
{
    return PotionPickupRegist( CreateObjectAt("RedPotion", GetObjectX(sUnit), GetObjectY(sUnit)) );
}

int ItemDropClassMagicalPotions(int sUnit)
{
    string potionList[] = {
        "RedPotion", "BluePotion", "CurePoisonPotion", "VampirismPotion",
        "HastePotion", "InvisibilityPotion", "InfravisionPotion", "WhitePotion",
        "ShieldPotion", "FireProtectPotion", "ShockProtectPotion", "PoisonProtectPotion",
        "YellowPotion", "BlackPotion"
        };
    return PotionPickupRegist( CheckPotionThingID(CreateObjectAt(potionList[Random(0, 13)], GetObjectX(sUnit), GetObjectY(sUnit))) );
}

int ItemDropClassWeapon(int sUnit)
{
    string weaponList[] = {
        "GreatSword", "Longsword", "Sword", "MorningStar",
        "OgreAxe", "StaffWooden", "BattleAxe", "FanChakram",
        "RoundChakram", "WarHammer", "OblivionHalberd", "OblivionWierdling",
        "OblivionHeart"
    };
    int weaponUnit = CreateObjectAt(weaponList[Random(0, 12)], GetObjectX(sUnit), GetObjectY(sUnit));
    int cWave = GameClassWaveCheck();

    CheckSpecialItem(weaponUnit);
    if (cWave >= 4)
        SetWeaponProperties(weaponUnit, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    return weaponUnit;
}

int ItemDropClassArmor(int sUnit)
{
    string armorList[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots",
        "PlateLeggings", "MedievalCloak", "ChainCoif", "ChainLeggings",
        "ChainTunic", "SteelHelm", "LeatherArmbands", "LeatherArmor",
        "LeatherArmoredBoots", "LeatherBoots", "LeatherHelm", "LeatherLeggings",
        "MedievalPants", "MedievalShirt"
    };
    int armorUnit = CreateObjectAt(armorList[Random(0, 17)], GetObjectX(sUnit), GetObjectY(sUnit));
    int cWave = GameClassWaveCheck();

    if (cWave >= 4)
        SetArmorProperties(armorUnit, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return armorUnit;
}

int ItemDropClassMoney(int sUnit)
{
    int ggg = CreateObjectAt("Gold", GetObjectX(sUnit), GetObjectY(sUnit));
    int incRate = GameClassWaveCheck() * 50;

    UnitStructSetGoldAmount(ggg, Random(100 + incRate, 400 + (incRate * 4)));
    return ggg;
}

int ItemDropClassGerm(int sUnit)
{
    string gmList[] = {"Ruby", "Ruby", "Emerald", "Diamond"};
    int germ = CreateObjectAt(gmList[Random(0, GameClassWaveCheck() / 3)], GetObjectX(sUnit), GetObjectY(sUnit));
    
    if (!(GetUnitFlags(germ) & 0x40))
        UnitNoCollide(germ);
    return germ;
}

int RewardItemFunctionTable(int pickIndex)
{
    int functions[6] = {
        ItemDropClassMoney, ItemDropClassGerm, ItemDropClassArmor,
        ItemDropClassHotPotion, ItemDropClassMagicalPotions, ItemDropClassWeapon
    };
    return functions[pickIndex];
}

int RewardClassItemCreateAtUnitPos(int sUnit)
{
    int pickFunction = RewardItemFunctionTable(Random(0, 5));

    if (!pickFunction)
        pickFunction = ItemDropClassHotPotion;

    return CallFunctionWithArgInt(pickFunction, sUnit);
}

void AllWaveClear(int wave)
{
    if (!GameClassMainIsShutdown())
        GameClassMainSetShutdown();
    AllPlayerTeleportTo(74);
    UnitNoCollide(CreateObjectAt("OgreStool1", LocationX(74), LocationY(74)));
    char buff[192];

    NoxSprintfString(buff, "** *축하합니다! %d개의 웨이브를 클리어하셨습니다* **", &wave, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

int LineMobFunctionInfo(int index)
{
    int *fptr;

    if (fptr == NULLPTR)
    {
        int fTb[34] = {
            MonTypeArcher, MonTypeBat, MonTypeBear, MonTypeFireFairy,
            MonTypeFirebat, MonTypeFrog, MonTypeWisp, MonTypeUrchin,
            MonTypeSwordman, MonTypeSpider, MonTypeSmallSpider, MonTypeSkullLord,
            MonTypeShade, MonTypeScorpion, MonTypeOgreLord, MonTypeOgreAxe,
            MonTypeOgre, MonTypeNecromancer, MonTypeGargoyle, MonTypeGhost,
            MonTypeHecubah, MonTypeImp, MonTypeLeech, MonTypeMecaFlying,
            MonTypeMecaGolem, MonTypeSkeleton, MonTypeGoon, MonTypeLichlord,
            MonTypeCarnPlant, MonTypeDryad, MonTypeStoneGiant, MonTypeHorrendous,
            MonTypeHovath, MonTypeJandor
        };
        fptr = fTb;
    }
    return fptr[index % 34];
}

void LineMobSetOrder(int *arrPtr, int set1, int set2, int set3, int set4)
{
    arrPtr[0] = set1 | (set2 << 8) | (set3 << 16) | (set4 << 24);
}

int LineMobOrder(int wave)
{
    int arr[14], *ptr;

    if (wave < 0)
    {
        ptr = arr;
        LineMobSetOrder(ptr, 5, 5, 1, 1);
        LineMobSetOrder(ptr + 4, 22, 22, 10, 10);
        LineMobSetOrder(ptr + 8, 8, 23, 8, 23);
        LineMobSetOrder(ptr + 12, 7, 9, 9, 7);     //spider, urchin
        LineMobSetOrder(ptr + 16, 13, 13, 26, 0);
        LineMobSetOrder(ptr + 20, 25, 9, 18, 19);
        LineMobSetOrder(ptr + 24, 14, 16, 2, 26);
        LineMobSetOrder(ptr + 28, 12, 27, 14, 11);
        LineMobSetOrder(ptr + 32, 17, 21, 3, 4);
        LineMobSetOrder(ptr + 36, 6, 19, 20, 24);
        LineMobSetOrder(ptr + 40, 28, 29, 28, 30);
        LineMobSetOrder(ptr + 44, 31, 32, 31, 33);
        LineMobSetOrder(ptr + 48, 24, 6, 27, 33);
        return 0;
    }
    return arr[wave];
}

int LineMobSpawnMarker(int index)
{
    int mark;

    if (!mark)
    {
        mark = CreateObject("ImaginaryCaster", 23);
        LookWithAngle(CreateObject("ImaginaryCaster", 24), 24);
        LookWithAngle(CreateObject("ImaginaryCaster", 25), 25);
        LookWithAngle(CreateObject("ImaginaryCaster", 26), 26);
        LookWithAngle(CreateObject("ImaginaryCaster", 27), 27);
        LookWithAngle(mark, 23);
    }
    return mark + index;
}

void LineSetInit()
{
    LineMobFunctionInfo(0);
    LineMobOrder(-1);
    LineMobSpawnMarker(0);
}

void CompleteCurrentWave()
{
    int wave = GameClassWaveCheck();
    char msg[192];

    NoxSprintfString(msg, "현재 %d웨이브를 완료하셨습니다. 시작 버튼이 활성화 되었습니다", &wave, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
}

void RewardClassPick()
{
    if (IsObjectOn(GetTrigger() + 1))
    {
        LookWithAngle(GetTrigger() + 1, 1);
    }
}

void RewardClassDrop(int sUnit)
{
    int rwd = RewardClassItemCreateAtUnitPos(sUnit);

    ListClassAddNode(XItemNode, rwd);
    RegistItemPickupCallback(rwd, RewardClassPick);
}

void LineMobDeathHandler()
{
    if (LineStatClassMobCountCheck())
    {
        LineStatClassMobCountSet(LineStatClassMobCountCheck() - 1);
        if (!LineStatClassMobCountCheck())
            CompleteCurrentWave();
    }
    LineStatClassKillCountSet(LineStatClassKillCountCheck() + 1);
    if (Random(0, 2))
        RewardClassDrop(SELF);
    DeleteObjectTimer(SELF, 90);
}

void LineMobHurtHandler()
{
    if (!GetCaller())
    {
        if (IsPoisonedUnit(SELF))
        {
            Damage(SELF, 0, IsPoisonedUnit(SELF) + 1, 5);
            DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 15);
        }
    }
}

void LineMobSightHandler()
{
    return;
}

string LineMobGreetMessage()
{
    string greet[] = {
        "니하오?",
        "이곳 한국도 이제 중국의 일부라는 말이!",
        "중국이 대국임을 명심하고 섬겨라!",
        "그 누구든 공산주의 세력에 반대하면 용서하지 않는다!",
        "다시는 중국을 무시하지 말라!",
        "니 지금 방금 무어라 했니?"};

    return greet[Random(0, 5)];
}

void LineMobGreetOnSight()
{
    SetCallback(SELF, 3, LineMobSightHandler);
    UniChatMessage(SELF, LineMobGreetMessage(), 180);
}

void LineMobGoToTarget(int mob)
{
    CreatureFollow(mob, m_masterMob);
    AggressionLevel(mob, 1.0);
}

int MobSummonSingle(int mobInfo, int anyUnit)
{
    int mob = CallFunctionWithArgInt(LineMobFunctionInfo((mobInfo >> (8 * Random(0, 3))) & 0xff), anyUnit);

    RetreatLevel(mob, 0.0);
    ResumeLevel(mob, 1.0);
    SetCallback(mob, 5, LineMobDeathHandler);
    SetCallback(mob, 7, LineMobHurtHandler);
    if (!Random(0, 7))
        SetCallback(mob, 3, LineMobGreetOnSight);
    FrameTimerWithArg(1, mob, LineMobGoToTarget);
    return mob;
}

void MobSummonMultiple(int mobInfo)
{
    int mark = LineMobSpawnMarker(0), i;

    for (i = 0 ; i < 5 ; i ++)
    {
        MobSummonSingle(mobInfo, mark + i);
        MoveObjectVector(mark + i, -60.0, 60.0);
    }
}

void MarkResetPosition(int mark)
{
    int i;

    for (i = 0 ; i < 5 ; i ++)
        MoveObject(mark + i, LocationX(GetDirection(mark + i)), LocationY(GetDirection(mark + i)));
}

void MobWaveClassSummon(int arg)
{
    int wave = (arg >> 4) & 0xff, count = arg & 0xf;

    if (count)
    {
        MobSummonMultiple(LineMobOrder(wave));
        FrameTimerWithArg(1, (--count) | (wave << 4), MobWaveClassSummon);
    }
    else
        MarkResetPosition(LineMobSpawnMarker(0));
}

void MobWaveSummonControl(int arg)
{
    int wave = (arg >> 0x4) & 0xff, count = arg & 0xf;

    if (count && !GameClassMainIsShutdown())
    {
        MobWaveClassSummon((wave << 4) | 6);
        SecondTimerWithArg(20, (--count) | (wave << 4), MobWaveSummonControl);
    }
    else if (GameClassLineIsStart())
        GameClassLineStartSet();
}

void MobWaveClassStart()
{
    int wave = GameClassWaveCheck();

    if (LineStatClassMobCountCheck())
    {
        UniPrintToAll("라인에 아직 남아있는 적군이 있습니다. 모두 제거 후 다시 시도하세요");
        return;
    }
    GameClassLineStartSet();
    if (wave < 12)
    {
        LineStatClassMobCountSet(300);
        RespawnLocationReset();
        GameClassWaveSet(wave + 1);
        ListClassClearCondition(ListClassGetHeadNext());
        FrameTimerWithArg(15, 10 | (wave << 4), MobWaveSummonControl);

        char buff[128];
        int waveCopy=wave+1;

        NoxSprintfString(buff, "** *현재 웨이브 %d을 지금 시작합니다* **", &waveCopy, 1);
        UniPrintToAll(ReadStringAddressEx(buff));
    }
    else
    {
        AllWaveClear(wave);
    }
}

void GGStart()
{
    MobWaveClassStart();
}

void KillLineMonster(int mob)
{
    Damage(mob, 0, MaxHealth(mob) + 1, -1);
}

void AllPlayerTeleportTo(int location)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]))
            MoveObject(player[i], LocationX(location), LocationY(location));
    }
}

void GGOver()
{
    if (!GameClassMainIsShutdown())
    {
        GameClassMainSetShutdown();
        AllPlayerTeleportTo(29);
        UniPrintToAll("나라가 점령당했습니다! 패배. ..");
    }
}

void MonsKeepOver()
{
    if (IsMonsterUnit(OTHER))
    {
        if (GameClassLifeCheck())
        {
            KillLineMonster(OTHER);
            GameClassLifeSet(GameClassLifeCheck() - 1);
            char buff[128];
            int life=GameClassLifeCheck();

            NoxSprintfString(buff, "중국인 1명이 국경을 넘었습니다!! (남은 기회: %d)", &life, 1);
            UniPrintToAll( ReadStringAddressEx(buff));
        }
        else
            GGOver();
    }
}

void ComeInHome()
{
    if (CurrentHealth(OTHER))
    {
        if (IsPlayerUnit(OTHER))
            MoveObject(OTHER, LocationX(15), LocationY(15));
    }
}

string DefaultItem(int num)
{
    string item[] = {
        "SteelHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "GreatSword", "RedPotion", "RedPotion", "RedPotion", "CurePoisonPotion",
        "CurePoisonPotion"};

    return item[num];
}

void PlaceItemOnLine(int arg)
{
    int index = arg & 0xf, count = (arg >> 4) & 0xf, location = (arg >> 8) & 0xffff, i;
    int sub = CreateObjectAt("ImaginaryCaster", LocationX(location), LocationY(location));
    string item = DefaultItem(index);
    int unit;

    for (i = 0 ; i < count ; i +=1)
    {
        unit = CreateObjectAt(item, GetObjectX(sub + i) + 23.0, GetObjectY(sub + i) + 23.0);

        if (GetUnitClass(unit)&UNIT_CLASS_FOOD)
            PotionPickupRegist(unit);
    }
    Delete(sub);
}

void PlaceDefaultItems(int arg)
{
    int count = arg & 0xff, location = (arg >> 8) & 0xffff;

    if (count)
    {
        PlaceItemOnLine((11 << 4) | (count % 11) | (location << 8));
        TeleportLocationVector(location, -23.0, 23.0);
        count --;
        FrameTimerWithArg(1, count | (location << 8), PlaceDefaultItems);
    }
}

int ListClassGetNext(int node)
{
    return ToInt(GetObjectZ(node));
}

void ListClassSetNext(int node, int nextNode)
{
    Raise(node, nextNode);
}

int ListClassAddNode(int prevNode, int parent)
{
    int nNode = CreateObjectAt("ImaginaryCaster", GetObjectX(parent), GetObjectY(parent));

    ListClassSetNext(nNode, ListClassGetNext(prevNode));
    ListClassSetNext(prevNode, nNode);
    return nNode;
}

void ListClassClearCondition(int node)
{
    int next;

    if (IsObjectOn(node))
    {
        next = ListClassGetNext(node);
        if (!GetDirection(node) && IsObjectOn(node - 1))
            Delete(node - 1);
        Delete(node);
        ListClassClearCondition(next);
    }
}

int ListClassGetHeadNext()
{
    return ListClassGetNext(XItemNode);
}

void HitFireballHammer()
{
    CastSpellObjectLocation("SPELL_FIREBALL", OTHER, GetObjectX(OTHER) + UnitAngleCos(OTHER, 17.0), UnitAngleSin(OTHER, 17.0));
}

int PowerItemMagicMisHammer(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int weapon = CreateObjectAt("WarHammer", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    RegistWeaponCPropertyExecScript(weapon, 2, HitFireballHammer);
    return weapon;
}

int PowerItemFireSword(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int weapon = CreateObjectAt("GreatSword", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    RegistWeaponCPropertyExecScript(weapon, 2, MagicMissileSword);
    return weapon;
}

void ShopClassItemDesc()
{
    int remArg = ToInt(GetObjectZ(GetTrigger() + 1)), pay = GetDirection(GetTrigger() + 1) * 1000;
    int dtStr = ToInt(GetObjectZ(GetTrigger() + 2));
    string itemName = ToStr((remArg >> 10) & 0x3ff);
    char intro[64], *pItemname = StringUtilGetScriptStringPtr(itemName);

    NoxSprintfString(intro, "%s 구입", &pItemname, 1);
    TellStoryUnitName("oAo", ToStr((dtStr >> 10)& 0x3ff), ReadStringAddressEx(intro));
    char buff[128];
    int args[]={StringUtilGetScriptStringPtr(itemName), pay};

    NoxSprintfString(buff, "%s을 구입하겠습니까? 그것은 %d 골드가 필요합니다", args, sizeof(args));
    UniPrint(OTHER, ReadStringAddressEx(buff));
    UniPrint(OTHER, "거래를 계속 하시려면 '예' 버튼을 누르세요");
}

void ShopClassKeeperRegistDialog(int kp)
{
    SetDialog(kp, "YESNO", ShopClassItemDesc, ShopClassItemTrade);
}

void ShopClassTradeResult()
{
    ShopClassKeeperRegistDialog(SELF);
}

void ShopClassTradeResultD()
{
    return;
}

void ShopClassItemTrade()
{
    int remArg = ToInt(GetObjectZ(GetTrigger() + 1)), pay = GetDirection(GetTrigger() + 1) * 1000;
    string tradeStr = ToStr((ToInt(GetObjectZ(GetTrigger() + 2)) >> 20) & 0x3ff);
    string itemName = ToStr((remArg >> 10) & 0x3ff);

    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= pay)
    {
        ChangeGold(OTHER, -pay);
        CallFunctionWithArgInt((remArg >> 20) & 0x3ff, OTHER);
        SetDialog(SELF, "NORMAL", ShopClassTradeResultD, ShopClassTradeResultD);
        char intro[64], *pItemname=StringUtilGetScriptStringPtr(itemName);

        NoxSprintfString(intro, "%s 구입완료", &pItemname, 1);
        TellStoryUnitName("oAo", tradeStr, ReadStringAddressEx(intro));
        ShopClassKeeperRegistDialog(SELF);
        char msg[128], *pName = StringUtilGetScriptStringPtr(itemName);

        NoxSprintfString(msg, "거래가 완료되었습니다! 구입하신 %s은 당신 발 아래에 있어요", &pName, 1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
    else
        UniPrint(OTHER, "거래가 취소되었습니다. 사유: 잔액 부족");
}

int DeadUnitCreateAtUnitPos(int sUnit, string unitName)
{
    int dead = CreateObjectAt(unitName, GetObjectX(sUnit), GetObjectY(sUnit));

    ObjectOff(dead);
    Damage(dead, 0, CurrentHealth(dead) + 1, -1);
    return dead;
}

int DeadUnitCreateAt(float xProfile, float yProfile, string unitName)
{
    int dead = CreateObjectAt(unitName, xProfile, yProfile);

    ObjectOff(dead);
    Damage(dead, 0, CurrentHealth(dead) + 1, -1);
    return dead;
}

int DeadUnitCreate(int location, string unitName)
{
    int dead = CreateObjectAt(unitName, LocationX(location), LocationY(location));

    ObjectOff(dead);
    Damage(dead, 0, CurrentHealth(dead) + 1, -1);
    return dead;
}

int ShopClassPlaceShopkeeper(int loLochiPay, int remArg, int dtStr)
{
    int location = loLochiPay & 0x3ff;
    int kp = DeadUnitCreate(location, ToStr(remArg & 0x3ff));

    Frozen(CreateObjectAt("ImaginaryCaster", LocationX(location), LocationY(location)) - 1, 1);
    Raise(CreateObjectAt("ImaginaryCaster", LocationX(location), LocationY(location)) - 1, remArg);
    Raise(kp + 2, dtStr);
    LookWithAngle(kp + 1, (loLochiPay >> 10) & 0xff);
    ShopClassKeeperRegistDialog(kp);
    StoryPic(kp, ToStr(dtStr & 0x3ff));
    return kp;
}

static int pushPlaceShopParams2(string unitname, string nick, int functionId)
{
    return SToInt(unitname)|(SToInt(nick)<<10)|(functionId<<20);
}

static int pushPlaceShopParams3(string portraitName, string content, string tradeDone)
{
    return SToInt(portraitName)|(SToInt( content) <<10)|(SToInt(tradeDone)<<20);
}

void ShopClassInitPlace()
{
    ShopClassPlaceShopkeeper(83 | (99 << 10), pushPlaceShopParams2("Mimic", "부처님 완드", PlaceBucherStaff),
        pushPlaceShopParams3("WolfPic","NoxDemo:BuyNox","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(35 | (43 << 10), pushPlaceShopParams2("Horrendous", "오우거로드엑스", PlaceAxeForOgre),
        pushPlaceShopParams3("HorrendousPic","NoxDemo:BuyNox","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(31 | (40 << 10), pushPlaceShopParams2("Wizard", "화이어볼 해머\n", PowerItemMagicMisHammer),
        pushPlaceShopParams3("AirshipCaptainPic","NoxDemo:BuyNox","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(32 | (38 << 10), pushPlaceShopParams2("EmberDemon", "마법미사일 서드\n", PowerItemFireSword),
        pushPlaceShopParams3("DemonPic","NoxDemo:BuyNox","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(33 | (42 << 10), pushPlaceShopParams2("Archer", "화살서드\n", PlaceArrowSword),
        pushPlaceShopParams3("GerardPic","NoxDemo:BuyNox","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(34 | (47 << 10), pushPlaceShopParams2("WizardWhite", "드레인서드\n", PlaceDrainSword),
        pushPlaceShopParams3("HorvathPic","NoxDemo:BuyNox","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(36 | (48 << 10), pushPlaceShopParams2("WizardGreen", "포오네서드\n", PlaceFonSword),
        pushPlaceShopParams3("DryadPic","War08z:EstimatedGoodies","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(37 | (44 << 10), pushPlaceShopParams2("Spider", "유도탄 메이스\n", PlaceAutoMissileMace),
        pushPlaceShopParams3("CreatureCageSpider","thing.db:SPELL_MAGIC_MISSILE_DESC","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(54 | (53 << 10), pushPlaceShopParams2("Beholder", "자동타겟\n데스레이서드\n", PlaceAutoTargetSword),
        pushPlaceShopParams3("MaleStaffDeathRay","thing.db:DeathRay","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(62 | (12 << 10), pushPlaceShopParams2("Lich", "아마겟돈 목걸이\n1회용\n", PlaceMeteorAmulet),
        pushPlaceShopParams3("MalePic12","thing.db:SPELL_METEOR_DESC","GUITrade.c:TradeVendorAccept"));
    ShopClassPlaceShopkeeper(65 | (11 << 10), pushPlaceShopParams2("Demon", "용의숨결지팡이\n", PlaceDemonWand),
        pushPlaceShopParams3("MaleStaffDeathRay","thing.db:LesserFireballWandDescription","GUITrade.c:TradeVendorAccept"));
}

int GreenFrogBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[17] = 35; arr[19] = 64; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1067869798; arr[25] = 1; arr[26] = 1; 
		arr[28] = 1103626240; arr[29] = 8; arr[31] = 10; arr[32] = 14; arr[33] = 24; 
		arr[59] = 5542784; arr[60] = 1313; arr[61] = 46905856; 
		link = arr;
	}
	return link;
}

void GreenFrogSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1073070735);
		SetMemory(ptr + 0x224, 1073070735);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32769);
		SetMemory(GetMemory(ptr + 0x22c), 35);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 35);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GreenFrogBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void WeaponExecScript1()
{
    int arw = CreateObjectAt("ArcherArrow", GetObjectX(OTHER) + UnitAngleCos(OTHER, 8.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 8.0));

    LookWithAngle(arw, GetDirection(OTHER));
    SetOwner(OTHER, arw);
    PushObject(arw, 34.0, GetObjectX(OTHER), GetObjectY(OTHER));
}

void DrainTouched()
{
    int owner = GetOwner(SELF);

    while (owner)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 100, 7);
            Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

void NatureShotCollide()
{
    int owner = GetOwner(SELF);

    while (owner)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 210, 14);
            Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

void OgreJinCollide()
{
    int owner = GetOwner(SELF);

    while (owner)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 130, 14);
            Enchant(OTHER, EnchantList(28), 0.3);
            Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            break;
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

void BreakingBarrelFx(int owner, float xProfile, float yProfile)
{
    int subD = DeadUnitCreateAt(xProfile, yProfile, "Necromancer");

    Frozen(subD, 1);
    SetCallback(CreateObjectAt("PiledBarrels3Breaking", xProfile, yProfile) - 1, 9, OgreJinCollide);
    DeleteObjectTimer(subD, 1);
    SetOwner(owner, subD);
}

void AxeJinHandler(int sub)
{
    float xVect = GetObjectZ(sub), yVect = GetObjectZ(sub + 1);
    int amount = GetDirection(sub), owner = GetOwner(sub), i;

    for (i = amount ; i ; i --)
    {
        if (IsVisibleTo(sub, sub + 1))
        {
            MoveObjectVector(sub, xVect, yVect);
            BreakingBarrelFx(owner, GetObjectX(sub), GetObjectY(sub));
        }
        else
        {
            break;
        }
    }
    Delete(sub);
    Delete(sub + 1);
}

void BreakingAxeJin()
{
    float xVect = UnitAngleCos(OTHER, 21.0), yVect = UnitAngleSin(OTHER, 21.0);
    int sub = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER) + xVect, GetObjectY(OTHER) + yVect);

    FrameTimerWithArg(1, sub, AxeJinHandler);
    Raise(sub, xVect);
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sub), GetObjectY(sub)), yVect);
    LookWithAngle(sub, 12);
    SetOwner(OTHER, sub);
}

void DrainSubUnitCreate(int sub, int owner)
{
    int dm = DeadUnitCreateAtUnitPos(sub, "Troll");

    Frozen(dm, 1);
    DeleteObjectTimer(dm, 1);
    SetCallback(dm, 9, DrainTouched);
    SetOwner(owner, dm);
}

void DrainSwordHandler(int sub)
{
    int count = GetDirection(sub), owner = GetOwner(sub);

    while (IsObjectOn(sub))
    {
        if (count && CurrentHealth(owner))
        {
            if (IsVisibleTo(sub, sub + 1))
            {
                FrameTimerWithArg(1, sub, DrainSwordHandler);
                LookWithAngle(sub, count - 1);
                DrainSubUnitCreate(sub, owner);
                MoveObjectVector(sub, GetObjectZ(sub), GetObjectZ(sub + 1));
                Effect("CHARM", GetObjectX(sub), GetObjectY(sub), GetObjectX(sub + 1), GetObjectY(sub + 1));
                Effect("CHARM", GetObjectX(sub + 1), GetObjectY(sub + 1), GetObjectX(sub), GetObjectY(sub));
                break;
            }
        }
        Delete(sub);
        Delete(sub + 1);
        break;
    }
}

void DrainSwordFx()
{
    float xVect = UnitAngleCos(OTHER, 19.0),yVect = UnitAngleSin(OTHER, 19.0);
    int sub = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER) + xVect, GetObjectY(OTHER) + yVect);

    FrameTimerWithArg(1, sub, DrainSwordHandler);
    LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sub), GetObjectY(sub)) - 1, 18);
    SetOwner(OTHER, sub);
    Raise(sub, xVect);
    Raise(sub + 1, yVect);
}

void MagicMissileSword()
{
    float xVect = UnitAngleCos(OTHER, 23.0), yVect = UnitAngleSin(OTHER, 23.0);
    int sub = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + xVect, GetObjectY(OTHER) + yVect);

    SetOwner(OTHER, sub);
    CastSpellObjectLocation("SPELL_MAGIC_MISSILE", sub, GetObjectX(sub) + xVect, GetObjectY(sub) + yVect);
    DeleteObjectTimer(sub, 90);
}

void ForceOfNatureShot()
{
    float xVect = UnitAngleCos(OTHER, 16.0), yVect = UnitAngleSin(OTHER, 16.0);
    int mis = CreateObjectAt("DeathBall", GetObjectX(OTHER) + xVect, GetObjectY(OTHER) + yVect);
    int ptr = GetMemory(0x750710);

    SetOwner(OTHER, mis);
    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, NatureShotCollide);
    PushObject(mis, 30.0, GetObjectX(OTHER), GetObjectY(OTHER));
}

void AutoDetectMissileCollide()
{
    int owner = GetOwner(SELF);

    while (owner)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 230, 7);
            Effect("LESSER_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

void ShotAutoDetectMissile()
{
    int mis=CreateObjectById(OBJ_PIXIE,GetObjectX(OTHER)+UnitAngleCos(OTHER,13.0),GetObjectY(OTHER)+UnitAngleSin(OTHER,13.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetOwner(OTHER,mis);
    PlaySoundAround(OTHER, 348);
    SetUnitCallbackOnCollide(mis, AutoDetectMissileCollide);
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[17] = 300; arr[18] = 100; 
		arr[19] = 50; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1067869798; arr[26] = 4; 
		arr[28] = 1128792064; arr[29] = 20; arr[31] = 16; arr[32] = 8; arr[33] = 15; 
		arr[53] = 1128792064; arr[54] = 4; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

void WizardRedSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1069547520);
		SetMemory(ptr + 0x224, 1069547520);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 700);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 700);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
        RegistryUnitStrikeFunction(sUnit, RedWizStrikeTrigger);
	}
}

int HecubahWithOrbBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 700; 
		arr[19] = 85; arr[21] = 1065353216; arr[23] = 65536; arr[25] = 1; arr[26] = 2; 
		arr[28] = 1128792064; arr[29] = 20; arr[31] = 16; arr[32] = 5; 
		arr[33] = 11; arr[59] = 5542784; arr[60] = 1384; arr[61] = 46914560; 
		link = arr;
	}
	return link;
}

void HecubahWithOrbSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 700);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 700);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahWithOrbBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
        RegistryUnitStrikeFunction(sUnit, OrbHecubahStrikeTrigger);
	}
}

int MechanicalGolemBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751344461; arr[1] = 1667853921; arr[2] = 1866951777; arr[3] = 7169388; arr[17] = 800; 
		arr[19] = 55; arr[21] = 1065353216; arr[23] = 1; arr[24] = 1067869798; arr[26] = 4; 
		arr[27] = 5; arr[28] = 1112014848; arr[29] = 100; arr[30] = 1120403456; arr[31] = 2; 
		arr[32] = 24; arr[33] = 48; arr[58] = 5545616; arr[59] = 5544288; arr[60] = 1318; 
		arr[61] = 46900992; 
		link = arr;
	}
	return link;
}

void MechanicalGolemSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 1);
		SetMemory(GetMemory(ptr + 0x22c), 800);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 800);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MechanicalGolemBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CarnivorousPlantBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 800; 
		arr[19] = 84; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; 
		arr[26] = 2; arr[27] = 5; arr[28] = 1109393408; arr[29] = 150; arr[31] = 8; 
		arr[59] = 5542784; arr[60] = 1371; arr[61] = 46901760; 
		link = arr;
	}
	return link;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1075922862);
		SetMemory(ptr + 0x224, 1075922862);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 800);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 800);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, CarnivorousPlantBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void CreClassRevive(int sub)
{
    int time = GetDirection(sub), nameProcF;
    int cre;

    while (1)
    {
        if (GetDirection(sub))
        {
            FrameTimerWithArg(2, sub, CreClassRevive);
            LookWithAngle(sub, time - 1);
            break;
        }
        else
        {
            nameProcF = ToInt(GetObjectZ(sub));
            cre = CreClassSummon(nameProcF, sub);
            CreClassCommonProperty(cre, GetOwner(sub));
            UniChatMessage(cre, "크리쳐 부활했당!\n한꺼번에 다 덤벼봐랑!", 120);
            Delete(sub);
        }
        Delete(sub);
        break;
    }
}

void CreClassDeath()
{
    int sub = GetTrigger() + 1;

    MoveObject(sub, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(SELF, 120);
    LookWithAngle(sub, 250); //TODO: Revive Timer
    FrameTimerWithArg(3, sub, CreClassRevive);
    UniChatMessage(SELF, "크리쳐가 적에게 피습당함...\n잠시 후 부활합니다", 150);
}

int CreClassSummon(int nameProcF, int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg), procF = (nameProcF >> 10) & 0x3ff;
    int cre, subF = (nameProcF >> 20) & 0x3ff;

    cre = CreateObjectAt(ToStr(nameProcF & 0x3ff), GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));
    if (cre)
    {
        if (procF)
            CallFunctionWithArg(procF, CreateObjectAt("ImaginaryCaster", GetObjectX(cre), GetObjectY(cre)) - 1);
        if (subF)
            CallFunctionWithArg(subF, cre);
        SetCallback(cre, 5, CreClassDeath);
        Enchant(cre, EnchantList(30), 0.0);
        Raise(cre + 1, nameProcF);
    }
    return cre;
}

int CreClassSummonBuffer(int nameProcF, int anyArg)
{
    if (CreClassGetCurrentCount() < 30)
    {
        CreClassIncreaseCurrentCount();
        return CreClassSummon(nameProcF, anyArg);
    }
    return 0;
}

void CreClassGuardMode(int cre, int caster)
{
    MonsterSetActionGuard(cre);
    UniChatMessage(cre, "홀드모드로 설정되었습니다", 150);
}

void CreClassEscort(int cre, int caster)
{
    CreatureFollow(cre, caster);
    UniChatMessage(cre, "에스코트 모드로 전환되었습니다", 150);
}

void CreClassOnClick()
{
    int act = MonsterGetCurrentAction(SELF), owner = GetOwner(SELF), cFps = GetMemory(0x84ea04);

    if (ABS(cFps - GetUnit1C(GetTrigger() + 1)) < 12)
    {
        if (owner ^ GetCaller())
            SetOwner(OTHER, SELF);
        LookAtObject(SELF, OTHER);
        if (act ^ 4) //isNotGuard
            CreClassGuardMode(SELF, OTHER);
        else
            CreClassEscort(SELF, OTHER);
        PlaySoundAround(SELF, 312);
        Effect("BLUE_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    }
    SetUnit1C(GetTrigger() + 1, cFps);
}

void CreClassCommonProperty(int cre, int owner)
{
    SetDialog(cre, "NORMAL", CreClassOnClick, CreClassOnClick);
    if (!MaxHealth(owner))
        owner = GetHost();
    SetOwner(owner, cre);
    SetOwner(owner, cre + 1);
}

void CreClassShowSumError()
{
    UniPrintToAll("**예외발생** 생산 가능한 최대치에 도달했기 때문에 더 이상 유닛을 만들 수 없습니다");
}

void CreSumClassMecaGolem(int anyArg, int owner)
{
    int pay = 57917, cre;
    int function = MechanicalGolemSubProcess;

    if (owner)
    {
        cre = CreClassSummonBuffer(SToInt("MechanicalGolem") | (function << 10), anyArg);
        if (cre)
            CreClassCommonProperty(cre, owner);
        else
            CreClassShowSumError();
    }
}

void CreSumClassPlant(int anyArg, int owner)
{
    int pay = 51008, cre;
    int function = CarnivorousPlantSubProcess;

    if (owner)
    {
        cre = CreClassSummonBuffer(SToInt("CarnivorousPlant") | (function << 10), anyArg);
        if (cre)
            CreClassCommonProperty(cre, owner);
        else
            CreClassShowSumError();
    }
}

void CreSumClassOrbHecubah(int anyArg, int owner)
{
    int pay = 50033, cre;
    int processer = HecubahWithOrbSubProcess;

    if (owner)
    {
        cre = CreClassSummonBuffer(SToInt("HecubahWithOrb") | (processer << 10), anyArg);
        if (cre)
        {
            CreClassCommonProperty(cre, owner);
        }
        else
            CreClassShowSumError();
    }
}

void CreSumClassRedWiz(int anyArg, int owner)
{
    int pay = 52349, cre;
    int processer = WizardRedSubProcess;

    if (owner)
    {
        cre = CreClassSummonBuffer(SToInt("WizardRed") | (processer << 10), anyArg);
        if (cre)
        {
            CreClassCommonProperty(cre, owner);
        }
        else
            CreClassShowSumError();
    }
}

void CalleeTwoArg(int arg1, int arg2)
{
    CalleeTwoArg(arg1, arg2);
}

void CallFunctionWithTwoArg(int func, int arg1, int arg2)
{
    int link;

    if (!link)
        link = GetScrCodeField(CalleeTwoArg);
    else
    {
        SetMemory(link + 0x1c, func);
        CalleeTwoArg(arg1, arg2);
    }
}

void CreClassMarketDesc()
{
    int lFunction = ToInt(GetObjectZ(GetTrigger() + 1));
    int pay = 45000; //CreClassGetSumPay(lFunction);
    char msg[128];

    TellStoryUnitName("oAo", "War08b:HenrickSaleSuccessful", "크리쳐 구입?");
    NoxSprintfString(msg, "이 크리처를 구입하려면 %d 골드가 필요합니다", &pay, 1);
    UniPrint(OTHER, ReadStringAddressEx(msg));
    UniPrint(OTHER, "거래를 계속하려면 예를 누르세요");
}

void CreClassMarketTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    int lFunction = ToInt(GetObjectZ(GetTrigger() + 1));
    int pay = 45000; //CreClassGetSumPay(lFunction);
    if (GetGold(OTHER) >= pay)
    {
        CallFunctionWithTwoArg(lFunction, OTHER, GetCaller());
        ChangeGold(OTHER, -pay);
        UniPrint(OTHER, "크리쳐 구입에 성공했습니다");
    }
    else
    {
        UniPrint(OTHER, "잔액이 부족하여 거래가 취소되었습니다");
    }
}

int CreClassPlaceMarket(int args)
{
    int location = args & 0x3ff, loadFunction = (args >> 10) & 0x3ff;
    string kpName = ToStr((args >> 20) & 0x3ff);
    int kp = DeadUnitCreateAt(LocationX(location), LocationY(location), kpName);

    Frozen(CreateObjectAt("ImaginaryCaster", LocationX(location), LocationY(location)) - 1, 1);
    Raise(kp + 1, loadFunction);
    SetDialog(kp, "YESNO", CreClassMarketDesc, CreClassMarketTrade);
    return kp;
}

static int pushPlaceMarketParam(short locationId, int functionId, string unitName)
{
    return locationId | (functionId<<10) | (SToInt(unitName)<<20);
}

void CreClassMarketPlace()
{
    CreSumClassMecaGolem(0, 0);
    CreSumClassOrbHecubah(0, 0);
    CreSumClassPlant(0, 0);
    CreSumClassRedWiz(0, 0);
    CreClassPlaceMarket(pushPlaceMarketParam(38, CreSumClassMecaGolem, "FlyingGolem"));
    CreClassPlaceMarket(pushPlaceMarketParam(48, CreSumClassOrbHecubah, "Hecubah"));
    CreClassPlaceMarket(pushPlaceMarketParam(49, CreSumClassPlant, "CarnivorousPlant"));
    CreClassPlaceMarket(pushPlaceMarketParam(64, CreSumClassRedWiz, "WizardRed"));
    AliveLifePlaceMarket(73);
}

void SellCre()
{
    int owner;

    if (IsMonsterUnit(OTHER))
    {
        owner = GetOwner(OTHER);
        if (UnitCheckEnchant(OTHER, GetLShift(30)))
        {
            Delete(GetCaller() + 1);
            Delete(OTHER);
            CreClassDecreaseCurrentCount();
            if (CurrentHealth(owner) && IsPlayerUnit(owner))
            {
                ChangeGold(owner, 3000);
                UniPrint(owner, "크리쳐 1개를 지웠습니다 (+3000골드 돌려받음)");
            }
        }
    }
}

void BucherOneShot()
{
    int cFps = GetMemory(0x84ea04), ptr = UnitToPtr(SELF), stf;

    if (ptr)
    {
        stf = GetMemory(ptr + 0x2e0);
        if (ABS(cFps - GetMemory(stf + 0x64)) > 50)
        {
            SetMemory(stf + 0x64, cFps);
            ThrowBucherFx(OTHER);
        }
    }
}

int PlaceArrowSword(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int sd = CreateObjectAt("Sword", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetWeaponProperties(sd, 5, 5, 8, 3);
    RegistWeaponCPropertyExecScript(sd, 2, WeaponExecScript1);
    return sd;
}

int PlaceDrainSword(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int sd = CreateObjectAt("GreatSword", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetWeaponProperties(sd, 5, 5, 8, 3);
    RegistWeaponCPropertyExecScript(sd, 2, DrainSwordFx);
    return sd;
}

int PlaceFonSword(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int sd = CreateObjectAt("GreatSword", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetWeaponProperties(sd, 5, 5, 8, 3);
    RegistWeaponCPropertyExecScript(sd, 2, ForceOfNatureShot);
    return sd;
}

int PlaceAxeForOgre(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int sd = CreateObjectAt("OgreAxe", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetWeaponProperties(sd, 0, 5, 0, 0);
    RegistWeaponCPropertyExecScript(sd, 2, BreakingAxeJin);
    return sd;
}

int PlaceAutoMissileMace(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int sd = CreateObjectAt("MorningStar", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetWeaponProperties(sd, 0, 5, 0, 0);
    RegistWeaponCPropertyExecScript(sd, 2, ShotAutoDetectMissile);
    return sd;
}

int PlaceBucherStaff(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int staff = CreateObjectAt("OblivionOrb", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetItemPropertyAllowAllDrop(staff);
    DisableOblivionItemPickupEvent(staff);
    SetUnitCallbackOnUseItem(staff, BucherOneShot);
    return staff;
}



static void NetworkUtilClientMain()
{
    InitializeResources();
}
static void OnPlayerEntryMap(int pInfo)
{
    ExecResourceWhenPlayerEntryMap(pInfo);
}

int SellGermPlace(int location)
{
    int kp = DeadUnitCreateAt(LocationX(location), LocationY(location), "UrchinShaman");

    Frozen(kp, 1);
    SetDialog(kp, "YESNO", SellGermDesc, SellGermTrade);
    return kp;
}

int RepairSingle(int inv, int pIndex)
{
    if (MaxHealth(inv) ^ CurrentHealth(inv))
    {
        RestoreHealth(inv, 99999);
        UpdateRepairItem(pIndex, inv);
        return 1;
    }
    return 0;
}

int RepairItemOnInventory(int holder)
{
    int count = 0, inv = GetLastItem(holder), pIndex = GetPlayerIndex(holder);

    while (inv)
    {
        count += RepairSingle(inv, pIndex);
        inv = NextItem(inv);
    }
    return count;
}

void RepairItemDesc()
{
    TellStoryUnitName("oAo", "WarriorHint:Repair", "아이템 수리?");
    UniPrint(OTHER, "인벤토리 내 모든 아이템을 금화 4천에 수리해 드려요~ 계속 거래하시려면 '예' 를 누르세요");
}

void RepairItemTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 4000)
    {
        char msg[128];
        int iRes = RepairItemOnInventory(OTHER);

        if (iRes)
        {
            ChangeGold(OTHER, -4000);
            PlaySoundAround(SELF, 803);
            NoxSprintfString(msg, "%d개 아이템이 수리되었습니다 (-4000 골드 차감됨)", &iRes, 1);
            UniPrint(OTHER, ReadStringAddressEx(msg));
            Effect("LESSER_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
    }
    else
        UniPrint(OTHER, "금화가 부족하여 처리되지 않았습니다. 금화를 모은 후 다시 시도해 보세요");
}

int RepairItemPlaceKp(int location)
{
    int kp = DeadUnitCreateAt(LocationX(location), LocationY(location), "Urchin");

    Frozen(kp, 1);
    SetDialog(kp, "YESNO", RepairItemDesc, RepairItemTrade);
    return kp;
}

void ExplosionMeteor(int sub)
{
    int owner = GetOwner(sub);

    SplashDamageAt(owner, 300, GetObjectX(sub), GetObjectY(sub), 200.0);
    PlaySoundAround(sub, 87);
    DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(sub), GetObjectY(sub)), 12);
    DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(sub), GetObjectY(sub)), 12);
}

void ThrowMeteor(int sub)
{
    int count = GetDirection(sub), owner = GetOwner(sub);

    while (IsObjectOn(sub))
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                Raise(sub + 1, GetObjectZ(sub + 1) - 5.0);
                FrameTimerWithArg(1, sub, ThrowMeteor);
                LookWithAngle(sub, count - 1);
                break;
            }
            else
                ExplosionMeteor(sub);
        }
        Delete(sub);
        Delete(sub + 1);
        break;
    }
}

void AmuletUseHitMeteor()
{
    int sub;

    if (IsObjectOn(SELF) && CurrentHealth(OTHER))
    {
        sub = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER), GetObjectY(OTHER));
        Raise(CreateObjectAt("BoulderMine", GetObjectX(sub), GetObjectY(sub)), 250.0);
        FrameTimerWithArg(1, sub, ThrowMeteor);
        PlaySoundAround(sub, 85);
        UnitNoCollide(sub + 1);
        SetOwner(OTHER, sub);
        LookWithAngle(sub, 50);
        Delete(SELF);
    }
}

void AutoTargetDeathraySight()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && CurrentHealth(SELF))
    {
        Damage(OTHER, owner, 100, 16);
        PlaySoundAround(OTHER, 299);
        Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    }
}

void AutoDeathRaySword()
{
    int sub = CreateObjectAt("WeirdlingBeast", GetObjectX(OTHER) + UnitAngleCos(OTHER, 6.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 6.0));

    UnitNoCollide(sub);
    SetOwner(OTHER, sub);
    SetCallback(sub, 3, AutoTargetDeathraySight);
    LookWithAngle(sub, GetDirection(OTHER));
    DeleteObjectTimer(sub, 1);
    PlaySoundAround(sub, 27);
    Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
}

int PlaceAutoTargetSword(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int sd = CreateObjectAt("GreatSword", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetWeaponProperties(sd, 5, 5, 19, 23);
    RegistWeaponCPropertyExecScript(sd, 2, AutoDeathRaySword);
    return sd;
}

int PlaceMeteorAmulet(int sArg)
{
    int xyTable = AnyargUnitOrLocation(sArg);
    int sd = CreateObjectAt("AmuletofManipulation", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));

    SetUnitCallbackOnUseItem(sd, AmuletUseHitMeteor);
    return sd;
}

void UserShowGameInfo(int pUnit)
{
    if (MaxHealth(pUnit))
    {
        UniPrint(pUnit, "우리는 중국인_version. 2                                                         기획 및 제작. 237");
        UniPrint(pUnit, "때는 2025년... 한국 본토 점령 프로젝트 준비를 위해 중국 공산당 간첩들이 대거 국내로 입국하게 되는데... ");
    }
}

void AliveLifeDesc()
{
    int cce = GameClassLifeCheck();
    char buff[128];

    TellStoryUnitName("oAo", "Con10B.scr:HecubahDialog1", "라이프구입?");
    NoxSprintfString(buff, "라이프를 구입할래요? 5만골드가 필요합니다(현재 수: %d)", &cce, 1);
    UniPrint(OTHER, ReadStringAddressEx(buff));
}

void AliveLifeTrade()
{
    int cce;

    if (GetAnswer(SELF) ^ 1) return;
    cce = GameClassLifeCheck();
    if (cce < 48)
    {
        if (GetGold(OTHER) >= 50000)
        {
            ChangeGold(OTHER, -50000);
            GameClassLifeSet(cce + 1);
            PlaySoundAround(OTHER, 1004);
            char msg[128];

            NoxSprintfString(msg, "라이프 1개를 추가 했어요! (남은 라이프 %d 개)", &cce, 1);
            UniPrint(OTHER, ReadStringAddressEx(msg));
            Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
        else
            UniPrint(OTHER, "거래 실패! 금화 부족");
    }
    else
        UniPrint(OTHER, "더 이상 라이프를 추가할 수 없어요!");
}

int AliveLifePlaceMarket(int location)
{
    int dm = DeadUnitCreateAt(LocationX(location), LocationY(location), "Maiden");

    Frozen(CreateObjectAt("Ankh", LocationX(location), LocationY(location)) - 1, 1);
    SetDialog(dm, "YESNO", AliveLifeDesc, AliveLifeTrade);
    return dm;
}

int PlaceDemonWand(int anyArg)
{
    int xyTable = AnyargUnitOrLocation(anyArg);
    int sd = CreateObjectAt("DemonsBreathWand", GetMemoryFloat(xyTable), GetMemoryFloat(xyTable + 4));
    
    SetConsumablesWeaponCapacity(sd, 0xc8, 0xc8);
    return sd;
}

void ManaTileSingle(int location, float xVect, float yVect)
{
    CreateObjectAt("InvisibleObelisk", LocationX(location), LocationY(location));
    TeleportLocationVector(location, xVect, yVect);
}

void ManaTileAll(int count)
{
    int i;

    if (count)
    {
        for (i = 0 ; i < 7 ; i += 1)
            ManaTileSingle(i + 66, 23.0, 23.0);
        FrameTimerWithArg(1, count - 1, ManaTileAll);
    }
}

void RepeatDrawSmokeRing(float x, float y, float gap, int rep)
{
    if (rep)
    {
        DeleteObjectTimer(CreateObjectAt("OldSmoke", x + MathSine(rep * 15 + 90, gap), y + MathSine(rep * 15, gap)), 3);
        RepeatDrawSmokeRing(x, y, gap, rep - 1);
    }
}

void SpreadSmokeRing(int sub, int count, float gap)
{
    float xProfile = GetObjectX(sub), yProfile = GetObjectY(sub);

    if (count)
    {
        RepeatDrawSmokeRing(xProfile, yProfile, gap, 24);
        SpreadSmokeRing(sub, count - 1, gap + 20.0);
    }
}

void SmokeRingBlast(int unit, int count, float gap)
{
    int sub = CreateObjectAt("ImaginaryCaster", GetObjectX(unit), GetObjectY(unit));

    SpreadSmokeRing(sub, count, gap);
    Delete(sub);
}

void ThrowBucherDestroyed(int sub)
{
    SmokeRingBlast(sub, 8, 20.0);
    SplashDamageAt(GetOwner(sub), 240, GetObjectX(sub), GetObjectY(sub), 200.0);
    PlaySoundAround(sub, 759);
    Effect("JIGGLE", GetObjectX(sub), GetObjectY(sub), 20.0, 0.0);
}

void ThrowBucherDurate(int sub)
{
    int owner = GetOwner(sub), durate = GetDirection(sub);

    while (IsObjectOn(sub))
    {
        if (CurrentHealth(owner))
        {
            if (durate)
            {
                FrameTimerWithArg(1, sub, ThrowBucherDurate);
                LookWithAngle(sub, durate - 1);
                Raise(sub + 1, GetObjectZ(sub + 1) - 10.0);
                break;
            }
            else
                ThrowBucherDestroyed(sub);
        }
        Delete(sub);
        Delete(++sub);
        break;
    }
}

void ThrowBucherFx(int owner)
{
    float xVect = UnitAngleCos(owner, 23.0),yVect = UnitAngleSin(owner, 23.0);
    int sub = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner) + xVect, GetObjectY(owner) + yVect);

    FrameTimerWithArg(1, sub, ThrowBucherDurate);
    LookWithAngle(CreateObjectAt("MovableStatue1b", GetObjectX(sub), GetObjectY(sub)) - 1, 25);
    UnitNoCollide(sub + 1);
    Raise(sub + 1, 250.0);
    SetOwner(owner, sub);
}

void GodModePotionFx(int fxUnit)
{
    int owner = GetOwner(fxUnit);

    while (IsObjectOn(fxUnit))
    {
        if (CurrentHealth(owner))
        {
            int durate = ToInt(GetObjectZ(fxUnit));

            if (durate)
            {
                if (!UnitCheckEnchant(owner, GetLShift(23)))
                    Enchant(owner, EnchantList(23), 0.0);
                FrameTimerWithArg(1, fxUnit, GodModePotionFx);
                Raise(fxUnit, durate - 1);
                MoveObject(fxUnit + 1, GetObjectX(owner), GetObjectY(owner));
            }
            EnchantOff(owner, EnchantList(23));
        }
        Delete(fxUnit);
        Delete(fxUnit + 1);
        break;
    }
}

void UseGodModePotion()
{
    Delete(SELF);
    if (!UnitCheckEnchant(OTHER, GetLShift(23)))
    {
        UniPrint(OTHER, "무적의 물약을 사용하셨습니다. 15 초간 무적상태가 유지됩니다");
        int fxUnit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));

        SetOwner(OTHER, CreateObjectAt("Smoke", GetObjectX(OTHER), GetObjectY(OTHER)) - 1);
        Raise(fxUnit, 450);
        FrameTimerWithArg(1, fxUnit, GodModePotionFx);
    }
}

