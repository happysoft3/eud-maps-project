

#include "libs/objectIDdefines.h"
#include "libs/animFrame.h"
#include "libs/grplib.h"

#define IMAGE_VECTOR_SIZE 256

void ImageSpellset()
{ }
void ImageSpellsetLeftbase()
{ }
void ImageSpellsetRightbase()
{ }
void GRPDump_SoldierOutput(){}

void imageAmulet(){}
void imageEastGate(){}

void GRPDumpRoachOniOutput(){}

void initializeImageFrameRoachOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRoachOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign4Directions(0, 130133, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, 130133+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     130133  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, 130133+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     130133+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     130133   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     130133+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
}

#define SOLDIER_IMAGE_ID 122964
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5);
    AnimFrameAssign8Directions(0, SOLDIER_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, SOLDIER_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, SOLDIER_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, SOLDIER_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, SOLDIER_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, SOLDIER_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(SOLDIER_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( SOLDIER_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( SOLDIER_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( SOLDIER_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( SOLDIER_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
}

void introduceCustomImage(int *pImgVector)
{
    initializeImageFrameRoachOni(pImgVector);
    initializeImageFrameSoldier(pImgVector);

    AppendImageFrame(pImgVector, GetScrCodeField(imageAmulet) + 4, 112960);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageSpellset)+4, 14415);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageSpellsetLeftbase)+4, 14446);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageSpellsetRightbase)+4, 14447);
    AppendImageFrame(pImgVector, GetScrCodeField(imageEastGate)+4, 15171);

    short mapName[64];
    short author[64];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr( "!!전사의 모험!!"), mapName);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("!!제작-237!!"), author);
    AppendTextSprite(pImgVector, 0xE841, mapName, 112979);
    AppendTextSprite(pImgVector, 0x185D, mapName, 112980);
    AppendTextSprite(pImgVector, 0x1F67, mapName, 112981);
    AppendTextSprite(pImgVector, 0xEF80, author, 112982);
    AppendTextSprite(pImgVector, 0xD87C, author, 112983);
}

void InitializeCommonImage()
{
    int imgVector = CreateImageVector(IMAGE_VECTOR_SIZE);

    introduceCustomImage(imgVector);
    DoImageDataExchange(imgVector);
}


