
#include "test2o19_utils.h"
#include "libs\unitstruct.h"
#include "libs\username.h"
#include "libs\mathlab.h"
#include "libs\itemproperty.h"
#include "libs\waypoint.h"
#include "libs/spellutil.h"
#include "libs/coopteam.h"
#include "libs/indexloop.h"
#include "libs/objectIDdefines.h"

int MainProc = 1, Chance = 10, SelMode, RspRate, RspNext;
int SelectHardMode = 0;
int HardModeCount = 8;
int player[20], MobCnt, MobLmt = 40, MobKills, DoubleMode;
float HARD;


int HardModeValue(int multiple)
{
    return SelectHardMode * multiple;
}

void GameMessage()
{
    UniPrintToAll("게임 방법: 무기로 공을 쳐내어 적을 맞추셔야 합니다 !!");
    PrintMessageFormatOne(PRINT_ALL, "적이 %d 개 이상 쌓이면 패배하게 됩니다", MobLmt);
    UniPrintToAll("공이 용암쪽으로 떨어지면 라이프가 하나 차감되며 라이프가 0이 되어도 패배처리 됩니다");
    FrameTimerWithArg(200, 0, ControlSpawnMob);
    if (DoubleMode)
        FrameTimerWithArg(210, 103, PlaceBall);
    FrameTimerWithArg(180, 103, PlaceBall);
}

void PlayerEntryPoint()
{
    int k, plr;

    while (1)
    {
        if (CurrentHealth(OTHER) && MainProc)
        {
            if (!(MaxHealth(OTHER) ^ 150))
            {
                plr = CheckPlayer();
                for (k = 9 ; k >= 0 && plr < 0 ; k --)
                {
                    if (!MaxHealth(player[k]))
                    {
                        player[k] = GetCaller();
                        player[k + 10] = 1;
                        plr = k;
                        break;
                    }
                }
                if (plr + 1)
                {
                    PlayerJoin(plr);
                    break;
                }
            }
        }
        CantJoinPlayer();
        break;
    }
}

void CantJoinPlayer()
{
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    MoveObject(OTHER, GetWaypointX(104), GetWaypointY(104));
    UniPrint(OTHER, "당신 계급은 이 게임에 참가할 수 없음");
}

void PlayerJoin(int plr)
{
    ChangeGold(player[plr], -GetGold(player[plr]));
    EmptyInventory(player[plr]);
    MoveObject(player[plr], GetWaypointX(113), GetWaypointY(113));
    DeleteObjectTimer(CreateObject("BlueRain", 113), 18);
    AudioEvent("BlindOff", 113);
}

void PreservePlayerTrigger()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (MaxHealth(player[k]))
        {
            if (GetUnitFlags(player[k]) & 0x40) player[k] = 0;
            else if (CurrentHealth(player[k]) && MainProc)
            {
                CheckPlayerAction(k);
                CheckDashAbility(k);
            }
        }
        else if (player[k + 10])
        {
            player[k] = 0;
            player[k + 10] = 0;
        }
    }
    FrameTimer(1, PreservePlayerTrigger);
}

void WindBooster(int unit)
{
    Enchant(unit, "ENCHANT_RUN", 0.21);
    Effect("RICOCHET", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    PushObjectTo(unit, UnitAngleCos(unit, 86.0), UnitAngleSin(unit, 86.0));
}

void MakeWall(int unit)
{
    int ptr;

    if (!HasEnchant(unit, "ENCHANT_ANTI_MAGIC"))
    {
        Enchant(unit, "ENCHANT_ANTI_MAGIC", 15.0);
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
        SetOwner(unit, CreateObjectAt("Moonglow", GetObjectX(unit), GetObjectY(unit)));
        SetOwner(unit, ptr);
        FrameTimerWithArg(1, unit + 1, DelayCastWall);
        FrameTimerWithArg(105, ptr, TimeOutPreserveWall);
    }
    else
        UniPrint(unit, "쿨다운 15 초 입니다...");
}

void DelayCastWall(int glow)
{
    int owner = GetOwner(glow);

    if (CurrentHealth(owner))
    {
        if (IsVisibleTo(owner, glow))
            CastSpellObjectLocation("SPELL_WALL", owner, GetObjectX(glow), GetObjectY(glow));
        else
            CastSpellObjectLocation("SPELL_WALL", owner, GetObjectX(owner) + UnitAngleCos(owner, 30.0), GetObjectY(owner) + UnitAngleSin(owner, 30.0));
    }
    Delete(glow);
}

void TimeOutPreserveWall(int ptr)
{
    int owner = GetOwner(ptr);
    
    if (CurrentHealth(owner))
    {
        CastSpellObjectLocation("SPELL_WALL", owner, 0.0, 0.0);
        UniPrint(owner, "벽이 효력 유효시간이 지나서 사라집니다");
    }

    Delete(ptr);
}

void CheckDashAbility(int plr)
{
    if (HasEnchant(player[plr], "ENCHANT_CROWN"))
    {
        if (HasEnchant(player[plr], "ENCHANT_SNEAK"))
        {
            RemoveTreadLightly(player[plr]);
            EnchantOff(player[plr], "ENCHANT_SNEAK");
            WindBooster(player[plr]);
        }
    }
}

void CheckPlayerAction(int plr)
{
    if (!(GetPlayerAction(player[plr]) ^ 0x01) && HasClass(EquipedWeapon(player[plr]), "WEAPON"))
    {
        if (!HasEnchant(player[plr], "ENCHANT_ETHEREAL"))
        {
            FrameTimerWithArg(1, player[plr], HitFrontYard);
            Enchant(player[plr], "ENCHANT_ETHEREAL", 0.7);
        }
    }
}

void HitFrontYard(int unit)
{
    int ptr;

    if (CurrentHealth(unit))
    {
        ptr = UnitToPtr(EquipedWeapon(unit));
        if (ptr)
        {
            CallFunctionWithArg(WeaponFxFuncPtr() + WeaponFxTable(GetMemory(ptr + 4) % 100), unit);
        }
    }
}

int WeaponFxTable(int idx)
{
    int arr[100];

    if (!(idx + 1))
    {
        arr[23] = 1; arr[22] = 2; arr[70] = 3; arr[75] = 4; arr[24] = 5;
        arr[73] = 6; arr[72] = 7; arr[25] = 8; arr[71] = 9;
    }
    return arr[idx];
}

int WeaponFxFuncPtr()
{
    StopScript(NothingAction);
}

void NothingAction(int arg)
{
    return;
}

void HeartHitting(int unit)
{
    int ptr;

    MoveWaypoint(1, GetObjectX(unit) + UnitAngleCos(unit, 24.0), GetObjectY(unit) + UnitAngleSin(unit, 24.0));
    ptr = CreateObject("Maiden", 1);
    Frozen(CreateObjectAt("Maiden", GetObjectX(ptr) + UnitAngleCos(unit, 26.0), GetObjectY(ptr) + UnitAngleSin(unit, 26.0)), 1);
    SetOwner(unit, ptr);
    SetOwner(unit, ptr + 1);
    Frozen(ptr, 1);
    SetCallback(ptr, 9, FullSwing);
    SetCallback(ptr + 1, 9, FullSwing);
    IceFlameFx(GetObjectX(ptr), GetObjectY(ptr));
    IceFlameFx(GetObjectX(ptr + 1), GetObjectY(ptr + 1));
    DeleteObjectTimer(ptr, 1);
    DeleteObjectTimer(ptr + 1, 1);
    AudioEvent("BeholderMove", 1);
}

void HalberdHitting(int unit)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit) + UnitAngleCos(unit, 27.0), GetObjectY(unit) + UnitAngleSin(unit, 27.0));

    Raise(ptr, HalberdSwing);
    SplashBound(unit, ToInt(GetObjectZ(ptr)), GetObjectX(ptr), GetObjectY(ptr), 65.0);
    FrameTimerWithArg(1, ptr, DoubleRingFx);
    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
    AudioEvent("WallOff", 1);
}

void GSwordHitting(int unit)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    SetOwner(unit, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)));
    SetOwner(unit, ptr);
    LookWithAngle(ptr, GetDirection(unit) - 9);
    LookWithAngle(ptr + 1, GetDirection(unit) + 9);
    FrameTimerWithArg(1, ptr, VKFrontTargeted);
    FrameTimerWithArg(1, ptr + 1, VKFrontTargeted);
}

void HammerHitting(int unit)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));

    Frozen(CreateObjectAt("Maiden", GetObjectX(ptr) - UnitAngleCos(unit, 65.0), GetObjectY(ptr) - UnitAngleSin(unit, 65.0)), 1);
    Frozen(CreateObjectAt("Maiden", GetObjectX(ptr) - UnitAngleCos(unit, 65.0), GetObjectY(ptr) - UnitAngleSin(unit, 65.0)), 1);
    SetOwner(unit, ptr + 1);
    SetOwner(unit, ptr + 2);
    SetCallback(ptr + 1, 9, HammerFxTouched);
    SetCallback(ptr + 2, 9, HammerFxTouched);
    SetOwner(unit, ptr);
    LookWithAngle(ptr, 30);
    LookWithAngle(ptr + 1, GetDirection(unit));
    LookWithAngle(ptr + 2, GetDirection(unit));
    FrameTimerWithArg(1, ptr, HammerCrushed);
}

void WierdlingHitting(int unit)
{
    int ptr = CreateObjectAt("Maiden", GetObjectX(unit) + UnitAngleCos(unit, 24.0), GetObjectY(unit) + UnitAngleSin(unit, 24.0));

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), UnitRatioX(ptr, unit, 24.0));
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), UnitRatioY(ptr, unit, 24.0));
    SetOwner(unit, ptr);
    Frozen(ptr, 1);
    SetCallback(ptr, 9, WierdlingTouched);
    Effect("EXPLOSION", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
    FrameTimerWithArg(1, ptr, WierdlingStright);
}

void OgreAxeHitting(int unit)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit) + UnitAngleCos(unit, 3.0), GetObjectY(unit) + UnitAngleSin(unit, 3.0)), k;
    float x_vect = UnitRatioX(ptr, unit, 25.0), y_vect = UnitRatioY(ptr, unit, 25.0);

    SetOwner(unit, ptr);
    LookWithAngle(ptr, 10);
    for (k = 0 ; k < 10 ; k ++)
    {
        Frozen(CreateObjectAt("Maiden", GetObjectX(ptr + k) + x_vect, GetObjectY(ptr + k) + y_vect), 1);
        SetOwner(ptr, ptr + k + 1);
        SetCallback(ptr + k + 1, 9, OgreHammerTouched);
    }
    FrameTimerWithArg(2, ptr, BoxCrashFxForOgreHam);
}

void BattleHammerHitting(int unit)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit) + UnitAngleCos(unit, 43.0), GetObjectY(unit) + UnitAngleSin(unit, 43.0));

    SetOwner(unit, ptr);
    CastSpellObjectLocation("SPELL_FIST", ptr, GetObjectX(ptr), GetObjectY(ptr));
    FrameTimerWithArg(3, ptr, AlreadyHitFist);
    FrameTimerWithArg(11, ptr, AlreadyHitFist);
    FrameTimerWithArg(17, ptr, FistDropHammer);
}

void StormStaffHitting(int unit)
{
    int ptr;
    float dx = UnitAngleCos(unit, -20.0), dy = UnitAngleSin(unit, -20.0);

    ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit) - dx + (dy * 10.0), GetObjectY(unit) - dy - (dx * 10.0));
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit) - dx - (dy * 10.0), GetObjectY(unit) - dy + (dx * 10.0)), dy);
    Raise(ptr, dx);
    Delete(ptr - 1);
    SetOwner(unit, ptr);
    SetOwner(unit, ptr + 1);

    FrameTimerWithArg(1, ptr, PreserveStorms);
}

void MaceHitting(int unit)
{
    int ptr = CreateObjectAt("MechanicalGolem", GetObjectX(unit) + UnitAngleCos(unit, 32.0), GetObjectY(unit) + UnitAngleSin(unit, 32.0));

    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
    WispDestroyFx(1);
    DeleteObjectTimer(CreateObject("MagicSpark", 1), 6);
    AudioEvent("ForceFieldCast", 1);
    SetCallback(ptr, 9, MaceCollide);
    Frozen(ptr, 1);
    SetOwner(unit, ptr);
    DeleteObjectTimer(ptr, 1);
}

void VKFrontTargeted(int ptr)
{
    int owner = GetOwner(ptr), count = ToInt(GetObjectZ(ptr));
    int unit;

    if (count < 6 && CurrentHealth(owner))
    {
        MoveObject(ptr, GetObjectX(ptr) + UnitAngleCos(ptr, 25.0), GetObjectY(ptr) + UnitAngleSin(ptr, 25.0));
        unit = CreateObjectAt("Maiden", GetObjectX(ptr), GetObjectY(ptr));
        DeleteObjectTimer(CreateObjectAt("MediumFireBoom", GetObjectX(ptr), GetObjectY(ptr)), 12);
        SetOwner(owner, unit);
        Frozen(unit, 1);
        DeleteObjectTimer(unit, 1);
        SetCallback(unit, 9, VKCollide);
        Raise(ptr, ToFloat(count + 1));
        FrameTimerWithArg(1, ptr, VKFrontTargeted);
    }
    else
        Delete(ptr);
}

void FullSwing()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()))
    {
        GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
        LookWithAngle(GetCaller(), GetDirection(owner));
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 6.0);
    }
}

void VKCollide()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()))
    {
        LookWithAngle(OTHER, GetDirection(owner));
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 4.0 + HARD);
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("BallBounce", 1);
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void HalberdSwing()
{
    int owner = GetOwner(GetOwner(SELF));

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()))
    {
        LookWithAngle(OTHER, GetDirection(owner));
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 5.0 + HARD);
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("BallBounce", 1);
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void HammerCrushed(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && count)
    {
        MoveObject(ptr + 1, GetObjectX(ptr) - UnitAngleCos(ptr + 1, 65.0), GetObjectY(ptr) - UnitAngleSin(ptr + 1, 65.0));
        MoveObject(ptr + 2, GetObjectX(ptr) - UnitAngleCos(ptr + 2, 65.0), GetObjectY(ptr) - UnitAngleSin(ptr + 2, 65.0));
        unit = CreateObjectAt("BlueOrb", GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        Frozen(CreateObjectAt("BlueOrb", GetObjectX(ptr + 2), GetObjectY(ptr + 2)), 1);
        Frozen(unit, 1);
        UnitNoCollide(unit);
        UnitNoCollide(unit + 1);
        DeleteObjectTimer(unit, 6);
        DeleteObjectTimer(unit + 1, 6);
        DeleteObjectTimer(BlueRiseFx(GetObjectX(ptr + 1), GetObjectY(ptr + 1)), 6);
        DeleteObjectTimer(BlueRiseFx(GetObjectX(ptr + 2), GetObjectY(ptr + 2)), 6);
        LookWithAngle(ptr, count - 1);
        LookWithAngle(ptr + 1, GetDirection(ptr + 1) + 5);
        LookWithAngle(ptr + 2, GetDirection(ptr + 2) - 5);
        FrameTimerWithArg(1, ptr, HammerCrushed);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void HammerFxTouched()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()) && IsObjectOn(SELF))
    {
        LookAtObject(OTHER, owner);
        LookWithAngle(OTHER, GetDirection(OTHER) + 128);
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 5.2 + HARD);
        Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        ObjectOff(SELF);
    }
}

void WierdlingTouched()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()) && IsObjectOn(SELF))
    {
        ObjectOff(SELF);
        LookAtObject(OTHER, SELF);
        LookWithAngle(OTHER, GetDirection(OTHER) + 128);
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 6.0 + HARD);
        GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
    }
}

void WierdlingStright(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(owner) && count < 32)
    {
        if (IsObjectOn(ptr))
            LookWithAngle(ptr, count + 1);
        else
            LookWithAngle(ptr, 200);
        MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr + 1), GetObjectY(ptr) + GetObjectZ(ptr + 2));
        Effect("EXPLOSION", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
        FrameTimerWithArg(1, ptr, WierdlingStright);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void OgreHammerTouched()
{
    int on = GetOwner(SELF), unit;
    int owner = GetOwner(on);

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()) && IsObjectOn(on))
    {
        unit = CreateObjectAt("Moonglow", GetObjectX(owner), GetObjectY(owner));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner)), ToFloat(GetCaller()));
        SetOwner(owner, unit);
        HecubahExplosion(GetObjectX(SELF), GetObjectY(SELF));
        ObjectOff(on);
        FrameTimerWithArg(1, unit, DelayHitBall);
    }
}

void DelayHitBall(int glow)
{
    int owner = GetOwner(glow), tg = ToInt(GetObjectZ(glow + 1));

    if (CurrentHealth(owner) && IsObjectOn(tg + 1))
    {
        LookAtObject(tg, glow);
        LookWithAngle(tg + 1, 4);
        Raise(tg + 1, 5.2 + HARD);
    }
    Delete(glow);
    Delete(glow + 1);
}

void BoxCrashFxForOgreHam(int ptr)
{
    int max = GetDirection(ptr), k;

    Delete(ptr);
    ptr ++;
    for (k = 0 ; k < max ; k ++)
    {
        UnitNoCollide(CreateObjectAt("PiledBarrels3Breaking", GetObjectX(ptr + k), GetObjectY(ptr + k)));
        Delete(ptr + k);
    }
}

void FistDropHammer(int ptr)
{
    int owner = GetOwner(ptr), unit;

    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
	UnitNoCollide(CreateObject("CoffinBreaking2", 1));
    DeleteObjectTimer(CreateObject("BigSmoke", 1), 9);
    unit = CreateObject("CarnivorousPlant", 1);
    SetOwner(owner, unit);
    SetCallback(unit, 9, FistHammerCollide);
    DeleteObjectTimer(unit, 1);
    Frozen(unit, 1);
    AudioEvent("FistHit", 1);
    Effect("JIGGLE", GetObjectX(ptr), GetObjectY(ptr), 13.0, 0.0);
    Delete(ptr + 1);
    Delete(ptr);
}

void AlreadyHitFist(int ptr)
{
    int owner = GetOwner(ptr), unit = CreateObjectAt("CarnivorousPlant", GetObjectX(ptr), GetObjectY(ptr));
    SetOwner(owner, unit);
    SetCallback(unit, 9, FistHammerCollide);
    DeleteObjectTimer(unit, 1);
    Frozen(unit, 1);
}

void FistHammerCollide()
{
    int owner = GetOwner(SELF), ptr;

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()))
    {
        ptr = CreateObjectAt("RedPotion", GetObjectX(SELF) + UnitRatioX(owner, SELF, 20.0), GetObjectY(SELF) + UnitRatioY(owner, SELF, 20.0));
        LookAtObject(OTHER, ptr);
        LookWithAngle(OTHER, GetDirection(OTHER) + 128);
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 6.0 + HARD);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Delete(ptr);
    }
}

void PreserveStorms(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && count < 12)
    {
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr));
        CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        SetUnit1C(unit, ToInt(GetObjectZ(ptr)));
        SetUnit1C(unit + 1, ToInt(GetObjectZ(ptr + 1)));
        Raise(unit, StormTouched);
        SetOwner(owner, unit);
        SetOwner(owner, unit + 1);
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr + 1), GetObjectY(ptr) + GetObjectZ(ptr));
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + GetObjectZ(ptr + 1), GetObjectY(ptr + 1) - GetObjectZ(ptr));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, unit, ThunderStormFx);
        FrameTimerWithArg(1, ptr, PreserveStorms);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void ThunderStormFx(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);
    float dx = ToFloat(GetUnit1C(ptr)), dy = ToFloat(GetUnit1C(ptr + 1));

    if (count < 10 && CurrentHealth(owner))
    {
        if (count % 2)
        {
            SplashBound(owner, ToInt(GetObjectZ(ptr)), GetObjectX(ptr), GetObjectY(ptr), 85.0);
            SplashBound(owner, ToInt(GetObjectZ(ptr)), GetObjectX(ptr + 1), GetObjectY(ptr + 1), 85.0);
        }
        DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(ptr), GetObjectY(ptr)), 8);
        DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(ptr + 1), GetObjectY(ptr + 1)), 8);
        MoveObject(ptr, GetObjectX(ptr) - dx, GetObjectY(ptr) - dy);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - dx, GetObjectY(ptr + 1) - dy);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, ThunderStormFx);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void StormTouched()
{
    int owner = GetOwner(GetOwner(SELF)), unit;
    float x, y;
    
    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()))
    {
        x = UnitRatioX(SELF, owner, 23.0);
        y = UnitRatioY(SELF, owner, 23.0);
        LookAtObject(OTHER, CreateObjectAt("RedPotion", GetObjectX(OTHER) + x, GetObjectY(OTHER) + y));
        Delete(GetMemory(GetMemory(0x750710) + 0x2c));
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 7.0 + (HARD * 2.0));
    }
}

void MaceCollide()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner) && IsOwnedBy(OTHER, MasterUnit()))
    {
        LookWithAngle(OTHER, GetDirection(owner));
        LookWithAngle(GetCaller() + 1, 4);
        Raise(GetCaller() + 1, 5.2 + HARD);
    }
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int MasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 1);
        CreateObject("BlackPowder", 1);
        MoveObject(unit, 5500.0, 100.0);
        MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
        SetCallback(unit, 9, DisplayLadderBoard);
        Frozen(unit, 1);
    }
    return unit;
}

void DisplayLadderBoard()
{
    char buff[256];

    if (IsCaller(GetTrigger() + 1))
    {
        int args[]={MobCnt, MobLmt, MobKills, Chance};

        NoxSprintfString(buff, "::짝퉁 벽돌깨기 스코어 전광판::\n유닛 수: %d / %d\n킬 수: %d\n남은 기회: %d", args, sizeof(args));
        UniChatMessage(SELF, ReadStringAddressEx(buff), 60);
    }
}

void DelayInit()
{
    int ptr = CreateObject("CarnivorousPlant", 106);

    Damage(CreateObject("Wizard", 113), 0, 9999, -1);
    SetCallback(ptr + 1, 9, NoticeSelectGameMode);
    FrameTimer(1, JustDecorations);
    FrameTimer(30, PreservePlayerTrigger);
    FrameTimer(30, InitSelectRoom);
    Damage(ptr, 0, MaxHealth(ptr) + 1, -1);
    SetCallback(ptr, 9, PlayerEntryPoint);
    FrameTimer(28, EnableDropOblivion);
    FrameTimer(59, InitMarket);
}

void MapInitialize()
{
    MusicEvent();
    MasterUnit();
    WeaponFxTable(-1);
    WallAngleTable(0);
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(1, DelayInit);
}

void NoticeSelectGameMode()
{
    if (IsPlayerUnit(OTHER) && CurrentHealth(OTHER))
    {
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("JournalEntryAdd", 1);
        UniPrintToAll(" :::게임 모드를 선택하여 주십시오::: ");
        UniPrintToAll("더블모드는 공이 2개로 나옵니다, 또한 이 모드는 기본적으로 3 라이프를 더 받고 시작합니다");
        Delete(SELF);
    }
}

void StartGame()
{
    FrameTimer(180, GameMessage);
    UniPrintToAll("잠시 후 게임은 시작 될 것입니다");
    UniPrintToAll("<<------짝퉁 벽돌깨기 -- 제작. 패닉--------------------------------------------------");
}

void JustDecorations()
{
    int ptr = CreateObject("TeleportWake", 103);

    UnitNoCollide(ptr);
    Frozen(ptr, 1);
    FrameTimerWithArg(37, 35, PutDefaultWeapons);
}

int SpawnBall(int wp)
{
    int unit = CreateObject("Maiden", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    CreateObject("MagicEnergy", wp);
    CreateObject("InvisibleLightBlueHigh", wp + 1); //+3
    CreateObject("InvisibleLightBlueHigh", wp + 1); //+4
    CreateObject("InvisibleLightBlueHigh", wp + 1); //+5
    CreateObject("InvisibleLightBlueHigh", wp); //+6
    LookWithAngle(unit, 0);
    LookWithAngle(unit + 1, 4);
    ObjectOff(unit);
    //Damage(unit, 0, CurrentHealth(unit) + 1, -1);
    Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
    SetOwner(MasterUnit(), unit);
    SetUnitFlags(unit + 2, GetUnitFlags(unit + 2) ^ 0x40);
    SetCallback(unit, 9, TouchedBall);
    FrameTimerWithArg(1, unit, LoopCheckingBall);
    return unit;
}

void LoopCheckingBall(int ball)
{
    if (IsObjectOn(ball + 1))
    {
        PushObjectTo(ball, UnitAngleCos(ball, GetObjectZ(ball + 1)), UnitAngleSin(ball, GetObjectZ(ball + 1)));
        MoveObject(ball + 2, GetObjectX(ball), GetObjectY(ball));
        FrameTimerWithArg(1, ball, LoopCheckingBall);
    }
}

void RemoveBallUnit(int ball)
{
    int k;

    for (k = 0 ; k < 7 ; k ++)
        Delete(ball + k);
}

int WallAngleTable(int num)
{
    int angle[4];

    if (!angle[0])
    {
        angle[0] = 45; angle[1] = 135; angle[2] = 315; angle[3] = 225;
        return 0;
    }
    return angle[num];
}

void TouchedBall()
{
    int wall_t, reflect, c_wall = GetDirection(GetTrigger() + 1);

    if (!GetCaller())
    {
        wall_t = GetWallDirection(GetTrigger());
        if (wall_t != c_wall && wall_t >= 0)
        {
            reflect = (2 * WallAngleTable(wall_t)) - ((DirToAngle(GetDirection(SELF)) + 180) % 360);
            if (reflect < 0) reflect += 360;
            LookWithAngle(SELF, AngleToDir(reflect));
            LookWithAngle(GetTrigger() + 1, wall_t);
            MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
            AudioEvent("LightningWand", 1);
            Effect("VIOLET_SPARKS", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
        }
    }
}

int DirToAngle(int num)
{
    return num * 45 / 32;
}

int AngleToDir(int num)
{
    return num * 32 / 45;
}

int GetWallDirection(int unit)
{
    int res = -1, k;
    float pos_x, pos_y;

    for (k = 0 ; k < 4 ; k ++)
    {
        if (k & 1) pos_x = 20.0;
        else pos_x = -20.0;
        if (k & 2) pos_y = 20.0;
        else pos_y = -20.0;
        MoveObject(unit + k + 3, GetObjectX(unit) + pos_x, GetObjectY(unit) + pos_y);
        if (!IsVisibleTo(unit + k + 3, unit))
        {
            res = k;
        }
        if (res >= 0) break;
    }
    return res;
}

int GetPlayerAction(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        //01- berserker, 05- run, 1a- laugh, 1b- point, 19- taunt
        return GetMemory(GetMemory(ptr + 0x2ec) + 0x58) & 0xff;
    }
    return 0;
}

void SetPlayerAction(int unit, int val)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x58, val);
}

int EquipedWeapon(int unit)
{
    int ptr = UnitToPtr(unit), pic;
    
    if (ptr)
    {
        pic = GetMemory(GetMemory(ptr + 0x2ec) + 0x68);
        if (pic)
            return GetMemory(pic + 0x2c);
    }
    return 0;
}

void GreenSparkFx(float x, float y)
{
    int ptr = CreateObjectAt("MonsterGenerator", x, y);

    Damage(ptr, 0, 10, -1);
    Delete(ptr);
}

void RhombusPut(int wp, float x_low, float x_high, float y_low, float y_high)
{
    float pos_x = RandomFloat(y_low, y_high), pos_y = RandomFloat(0.0, x_high - x_low);

    MoveWaypoint(wp, x_high - y_high + pos_x - pos_y, pos_x + pos_y);
}

void TeleportMarkerPlace(int wp)
{
    RhombusPut(wp, 3602.0, 3651.0, 1739.0, 2451.0);
}

int MobSpawnPtr()
{
    StopScript(Monster1);
}

int Monster1(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("Urchin", arg, 50);
}

int Monster2(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("Archer", arg, 85);
}

int Monster3(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("Swordsman", arg, 125);
}

int Monster4(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return ColorMaiden(225, 32, 250, arg, 160);
}

int Monster5(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("Wizard", arg, 205);
}

int Monster6(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("SkeletonLord", arg, 260);
}

int Monster7(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("OgreBrute", arg, 330);
}

int Monster8(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("Horrendous", arg, 400);
}

int Monster9(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return ColorMaiden(16, 128, 250, arg, 470);
}

int Monster10(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("EmberDemon", arg, 510);
}

int Monster11(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("GreenFrog", arg, 570);
}

int Monster12(int arg)
{
    TeleportMarkerPlace(arg & 0xff);
    return SpawnMonster("WizardGreen", arg, 600);
}

void ControlSpawnMob(int count)
{
    int wv;

    if (count < 30 + ((SelMode - 1) * 6) + HardModeValue(10))
    {
        if (MobCnt < MobLmt)
        {
            CallFunctionWithArgInt(MobSpawnPtr() + wv, 1 | (wv << 0x10));
            FrameTimerWithArg(RspRate, count + 1, ControlSpawnMob);
        }
        else
            GGOver(0);
    }
    else
    {
        if (wv < HardModeCount)
        {
            wv ++;
            PrintMessageFormatOne(PRINT_ALL, "[*] 지금 %d 웨이브로 전환되었습니다", wv);
            FrameTimerWithArg(RspNext, 0, ControlSpawnMob);
        }
        else
            VictoryEvent(wv);
    }
}

void GGOver(int cl)
{
    MainProc = 0;
    MoveObject(Object("PlayerStartLocation"), GetWaypointX(105), GetWaypointY(105));
    if (!cl)
    {
        UniPrintToAll("미션실패! 몬스터 한계치를 초과했습니다");
        UniPrintToAll("미션실패! 몬스터 한계치를 초과했습니다");
    }
    else
    {
        UniPrintToAll("더 이상 남아있는 공이 없습니다");
        UniPrintToAll("남아있는 공이 없어서 게임을 진행할 수 없습니다, 이 게임은 패배 처리 됩니다");
    }
    UniPrintToAll("게임이 종료되었습니다");
    FrameTimerWithArg(30, 105, AllPlayerTeleportAt);
    FrameTimerWithArg(60, 105, DisplayFailMissionStr);
    FrameTimerWithArg(120, "다음에 다시 도전해 보세요 ㅠㅠ", DelayPrintMessage);
}

void DisplayFailMissionStr(int loc)
{
    MoveWaypoint(1, GetWaypointX(loc) - 50.0, GetWaypointY(loc) - 50.0);
    StrFailMission();
    AudioEvent("GameOver", 1);
}

void DisplayMissionSuccessStr(int loc)
{
    MoveWaypoint(1, GetWaypointX(loc) - 50.0, GetWaypointY(loc) - 50.0);
    StrYourWinner();
    Effect("WHITE_FLASH", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
    AudioEvent("LevelUp", 1);
    AudioEvent("StaffOblivionAchieve1", 1);
}

void DelayPrintMessage(string txt)
{
    UniPrintToAll(txt);
}

void VictoryEvent(int wv)
{
    MainProc = 0;
    MoveObject(Object("PlayerStartLocation"), GetWaypointX(108), GetWaypointY(108));
    FrameTimerWithArg(30, 108, AllPlayerTeleportAt);
    FrameTimerWithArg(60, 108, DisplayMissionSuccessStr);
    UniPrintToAll("승리하셨습니다, 최종 " + IntToString(wv) + " 스테이지 클리어 완료!");
    FrameTimerWithArg(120, "멋진 플레이를 보여주셨군요!", DelayPrintMessage);
}

void AllPlayerTeleportAt(int loc)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
            MoveObject(player[k], GetWaypointX(loc), GetWaypointY(loc));
    }
}

int SpawnMonster(string mob, int arg, int hp)
{
    int wp = arg & 0xff;
    int unit = CreateObject(mob, wp);

    MonsterProperties(unit, hp);
    LookWithAngle(unit + 1, arg >> 0x10);
    return unit;
}

void MonsterProperties(int unit, int hp)
{
    MobCnt ++;
    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    if (SelectHardMode)
        SetUnitMaxHealth(unit, hp * 12 / 10);
    else
        SetUnitMaxHealth(unit, hp);
    ObjectOff(unit);
    SetCallback(unit, 5, MonsterDeath);
    SetCallback(unit, 9, MonsterCollide);
    SetUnitMass(unit, 9999.0);
    LookWithAngle(unit, 92);
    Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
}

void MonsterCollide()
{
    if (IsOwnedBy(OTHER, MasterUnit()) && !HasEnchant(SELF, "ENCHANT_CHARMING"))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        EnchantOff(SELF, "ENCHANT_INVULNERABLE");
        Damage(SELF, 0, 50 + MobKills, 14);
        if (CurrentHealth(SELF))
        {
            Enchant(SELF, "ENCHANT_INVULNERABLE", 0.0);
            Enchant(SELF, "ENCHANT_CHARMING", 0.4);
            if (ToInt(Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER))) && ToInt(GetObjectZ(GetCaller() + 1))) //new
            {
                LookAtObject(OTHER, SELF);
                LookWithAngle(OTHER, GetDirection(OTHER) + 128);
                LookWithAngle(GetCaller() + 1, 4);
            } //new end
        }
        else ObjectOn(SELF);
    }
}

void MonsterDeath()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(CreateObject("Explosion", 1), 9);
    MoneyDraggingToLava(Moneys(GetObjectX(SELF), GetObjectY(SELF), GetDirection(GetTrigger() + 1)));
    AudioEvent("PowderBarrelExplode", 1);
    AudioEvent("GolemHitting", 1);
    MobCnt --;
    MobKills ++;
    Delete(GetTrigger() + 1);
    DeleteObjectTimer(SELF, 30);
}

void MoneyDraggingToLava(int unit)
{
    if (IsObjectOn(unit))
    {
        if (GetObjectY(unit) - GetObjectX(unit) < 600.0)
        {
            MoveObject(unit, GetObjectX(unit) - 1.0, GetObjectY(unit) + 1.0);
            FrameTimerWithArg(1, unit, MoneyDraggingToLava);
        }
        else
        {
            MoveWaypoint(1, GetObjectX(unit), GetObjectY(unit));
            WispDestroyFx(1);
            Delete(unit);
        }
    }
}

void PlaceBall(int wp)
{
    int ptr, unit;

    if (MainProc)
    {
        if (Chance)
        {
            Chance --;
            unit = SpawnBall(wp);
            if (DoubleMode)
            {
                LookWithAngle(unit, Random(0, 254));
                LookWithAngle(unit + 1, 4);
                Raise(unit + 1, 3.5);
            }
            FrameTimerWithArg(1, unit, CheckBallBoundary);
            ptr = CreateObject("RedPotion", wp);
            CastSpellObjectLocation("SPELL_TURN_UNDEAD", CreateObject("InvisibleLightBlueLow", wp), GetWaypointX(wp), GetWaypointY(wp));
            Delete(ptr);
            Delete(ptr + 1);
            UniPrintToAll("맵 중심부에 공이 생성되었습니다");
            ShowRemainCount(Chance);
        }
        else
            GGOver(1);
    }
}

void CheckBallBoundary(int unit)
{
    float coor;
    
    if (IsObjectOn(unit + 1) && MainProc)
    {
        coor = GetObjectY(unit) - GetObjectX(unit);
        if (coor < 600.0)
            FrameTimerWithArg(1, unit, CheckBallBoundary);
        else
        {
            RemoveBallUnit(unit);
            FrameTimerWithArg(60, 103, PlaceBall);
            UniPrintToAll("방금 공이 한계선을 넘어갔습니다, 개수 1 개가 차감됩니다");
            ShowRemainCount(Chance - 1);
        }
    }
}

void ShowRemainCount(int cnt)
{
    if (cnt >= 0)
        PrintMessageFormatOne(PRINT_ALL, "[남은 공 개수: %d]", cnt);
}

void EmptyInventory(int unit)
{
    while (IsObjectOn(GetLastItem(unit)))
        Delete(GetLastItem(unit));
}

void OnOblivionOrbUse()
{ }

void PutDefaultWeapons(int count)
{
    if (count)
    {
        int weapon = CreateObject(ManyWeapons(Random(0, 8)), 107);

        SetWeaponProperties(weapon, 5, 5, 12, 12);
        int thingId = GetUnitThingID(weapon);

        if (thingId >= 222 && thingId <= 225)
        {
            DisableOblivionItemPickupEvent(weapon);
            SetItemPropertyAllowAllDrop(weapon);
            if (thingId==OBJ_OBLIVION_ORB)
                SetUnitCallbackOnUseItem(weapon, OnOblivionOrbUse);
        }
        Frozen(weapon, 1);
        TeleportLocationVector(107, 25.0, 25.0);
        FrameTimerWithArg(1, count - 1, PutDefaultWeapons);
    }
}

string ManyWeapons(int num)
{
    string wp[] = {"OblivionHalberd", "OblivionHeart", "OblivionWierdling", "GreatSword", "OgreAxe", "WarHammer",
        "BattleAxe", "OblivionOrb", "MorningStar"};

    return wp[num];
}

void MapExit()
{
    RemoveCoopTeamMode();
    SetMemory(0x5cb394, 6075528);
    SetMemory(0x5cb3a0, 6075544);
    SetMemory(0x5cb3b8, 6075580);
    SetMemory(0x5cb3ac, 6075560);
    MusicEvent();
}

void EnableDropOblivion()
{
    SetMemory(0x5cb394, 0);
    SetMemory(0x5cb3a0, 0);
    SetMemory(0x5cb3b8, 0);
    SetMemory(0x5cb3ac, 0);
}

void IceFlameFx(float x, float y)
{
    int unit = CreateObjectAt("FireGrateFlame", 100.0, 100.0);

    UnitNoCollide(unit);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    MoveObject(unit, x, y);
    DeleteObjectTimer(unit, 12);
}

void SparkSpreadFx(int flag)
{
    string orb[] = {"ManaBombOrb", "HealOrb", "CharmOrb", "DrainManaOrb"};
    int wp = flag & 0xff, color = (flag >> 0x10) & 0xff;
    int ptr = CreateObject("AmbBeachBirds", wp) + 1, k;

    Delete(ptr - 1);
    for (k = 0 ; k < 30 ; k ++)
        CreateObject(orb[color], wp);
    FrameTimerWithArg(1, ptr, MovingSpread);
}

void MovingSpread(int ptr)
{
    int k, count = GetDirection(ptr);

    if (count < 32)
    {
        for (k = 0 ; k < 30 ; k ++)
            MoveObject(ptr + k, GetObjectX(ptr + k) + MathSine((k * 12) + 90, 2.0), GetObjectY(ptr + k) + MathSine(k * 12, 2.0));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, MovingSpread);
    }
    else
    {
        for (k = 0 ; k < 30 ; k ++)
            Delete(ptr + k);
    }
}

void DoubleRingFx(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 5)
    {
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        SparkSpreadFx(1 | (Random(0, 3) << 0x10));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, DoubleRingFx);
    }
    else
        Delete(ptr);
}

int BlueRiseFx(float x, float y)
{
    int unit = CreateObjectAt("TeleportWake", x, y);

    UnitNoCollide(unit);
    Frozen(unit, 1);
    return unit;
}

int ColorMaiden(int red, int grn, int blue, int arg, int hp)
{
    int unit = CreateObject("Bear2", arg & 0xff);
    int ptr = GetMemory(0x750710), k;

    SetMemory(ptr + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(7));
    SetUnitVoice(unit, 7);
    UnitLinkBinScript(unit, MaidenBinTable());
    MonsterProperties(unit, hp);
    LookWithAngle(unit + 1, arg >> 0x10);

    return unit;
}

void HecubahExplosion(float x, float y)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", x, y);
    
    ObjectOff(CreateObjectAt("Hecubah", x, y));

    Raise(unit, ToFloat(38049)); //38817
    Damage(unit + 1, 0, MaxHealth(unit + 1) + 1, -1);
    FrameTimerWithArg(1, unit + 1, HecDeadProgressEnd);
}

void HecDeadProgressEnd(int unit)
{
    int count = GetDirection(unit - 1), mn = ToInt(GetObjectZ(unit - 1)), ptr = UnitToPtr(unit);

    if (ptr)
    {
        if (count < 5)
        {
            Raise(unit - 1, ToFloat(mn + 256));
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e0, mn);
            LookWithAngle(unit - 1, count + 1);
            FrameTimerWithArg(3, unit, HecDeadProgressEnd);
        }
        else
        {
            Delete(unit);
            Delete(unit - 1);
        }
    }
    else
        Delete(unit - 1);
}

void StrYourWinner()
{
	int arr[26];
	string name = "CharmOrb";
	int i = 0;
	arr[0] = 1008796286; arr[1] = 1338015748; arr[2] = 270541312; arr[3] = 134236228; arr[4] = 35653888; arr[5] = 75776516; arr[6] = 9438912; arr[7] = 1146208324; arr[8] = 25362576; arr[9] = 100417554; 
	arr[10] = 585874; arr[11] = 67190528; arr[12] = 659584; arr[13] = 269745921; arr[14] = 8388798; arr[15] = 4097; arr[16] = 2114454018; arr[17] = 16781312; arr[18] = 2105848; arr[19] = 1879183392; 
	arr[20] = 35668031; arr[21] = 553783300; arr[22] = 67239936; arr[23] = 1090543556; arr[24] = 537132063; arr[25] = 67124992; 
	while(i < 26)
	{
		drawStrYourWinner(arr[i], name);
		i ++;
	}
}

void drawStrYourWinner(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(1);
		pos_y = GetWaypointY(1);
	}
	for (i = 1 ; i > 0 && count < 806 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 74 == 73)
			MoveWaypoint(1, GetWaypointX(1) - 146.000000, GetWaypointY(1) + 2.000000);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.000000, GetWaypointY(1));
		count ++;
	}
	if (count >= 806)
	{
		count = 0;
		MoveWaypoint(1, pos_x, pos_y);
	}
}

void InitMarket()
{
    int ptr = CreateObject("Maiden", 109);

    Frozen(CreateObject("Maiden", 110), 1);
    Frozen(CreateObject("Maiden", 111), 1);
    Frozen(CreateObject("Maiden", 112), 1);
    Frozen(ptr, 1);
    UnitNoCollide(CreateObject("GameBall", 109));
    Frozen(ptr + 4, 1);
    SetDialog(ptr, "AA", BuyNewBall, NothingAction);
    SetDialog(ptr + 1, "AA", BuyKillScore, NothingAction);
    SetDialog(ptr + 2, "AA", BuyNewSkill, NothingAction);
    SetDialog(ptr + 3, "AA", BuySpeedBoots, NothingAction);
    Frozen(CreateObject("ConjurerSpellBook", 110), 1);
    Frozen(CreateObject("BookOfOblivion", 111), 1);
    Frozen(CreateObject("BootsOfSpeed", 112), 1);
}

void BuyNewSkill()
{
    if (MainProc)
    {
        if (HasEnchant(OTHER, "ENCHANT_CROWN"))
        {
            BuyAnOTHERSkill();
            return;
        }
        if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        {
            EnchantOff(OTHER, "ENCHANT_AFRAID");
            if (GetGold(OTHER) >= 35000)
            {
                GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
                DeleteObjectTimer(BlueRiseFx(GetObjectX(OTHER), GetObjectY(OTHER)), 18);
                ChangeGold(OTHER, -35000);
                Enchant(OTHER, "ENCHANT_CROWN", 0.0);
                UniPrint(OTHER,"대쉬 마법을 배우셨습니다, 조심스럽게 걷기 시전을 통해 대쉬 마법을 사용할 수 있습니다");
                UniPrint(OTHER,"죽게되면 다시 배워야 하므로 조심하시기 바랍니다");
            }
            else
                PrintMessageFormatOne(OTHER, "거래실패- 금화가 %d 만큼 더 필요합니다", 35000 - GetGold(OTHER));
        }
        else
        {
            UniPrint(OTHER, "대쉬 마법을 배웁니다, 35,000 원이 필요합니다");
            Enchant(OTHER, "ENCHANT_AFRAID", 0.5);
        }
    }
}

void BuyAnOTHERSkill()
{
    if (HasEnchant(OTHER, "ENCHANT_ANCHORED"))
    {
        UniPrint(OTHER, "이미 모든 마법을 다 배웠습니다");
        return;
    }
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 35000)
        {
            GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
            DeleteObjectTimer(BlueRiseFx(GetObjectX(OTHER), GetObjectY(OTHER)), 18);
            ChangeGold(OTHER, -35000);
            Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
            UniPrint(OTHER, "벽 생성 마법을 배우셨습니다, 작살 시전을 통해 이 마법을 사용할 수 있습니다");
            UniPrint(OTHER, "죽게되면 다시 배워야 하므로 조심하시기 바랍니다");
        }
        else
            PrintMessageFormatOne(OTHER, "거래 실패: 금화가 %d 만큼 더 필요합니다", 35000 - GetGold(OTHER));
    }
    else
    {
        UniPrint(OTHER, "벽 생성 마법을 배웁니다, 35,000 원이 필요합니다");
        Enchant(OTHER, "ENCHANT_AFRAID", 0.5);
    }
}

void BuyNewBall()
{
    if (MainProc)
    {
        if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        {
            EnchantOff(OTHER, "ENCHANT_AFRAID");
            if (GetGold(OTHER) >= 19000)
            {
                ChangeGold(OTHER, -19000);
                UniPrint(OTHER, "공 4개를 추가 구입하였습니다");
                Chance += 4;
                ShowRemainCount(Chance);
            }
            else
                PrintMessageFormatOne(OTHER, "거래 실패: 금화가 %d 만큼 더 필요합니다", 19000 - GetGold(OTHER));
        }
        else
        {
            UniPrint(OTHER, "공 4개를 추가 구입합니다, 19,000 원이 필요합니다");
            Enchant(OTHER, "ENCHANT_AFRAID", 0.5);
        }
    }
}

void BuyKillScore()
{
    if (MainProc)
    {
        if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        {
            EnchantOff(OTHER, "ENCHANT_AFRAID");
            if (GetGold(OTHER) >= 13000)
            {
                ChangeGold(OTHER, -13000);
                UniPrint(OTHER, "킬 스코어 5점을 추가 구입했습니다");
                MobKills += 5;
            }
            else
                PrintMessageFormatOne(OTHER, "거래 실패: 금화가 %d 만큼 더 필요합니다", 13000 - GetGold(OTHER));
        }
        else
        {
            UniPrint(OTHER, "킬 스코어 5점을 추가 구입합니다, 13,000 원이 필요합니다");
            Enchant(OTHER, "ENCHANT_AFRAID", 0.5);
        }
    }
}

void BuySpeedBoots()
{
    if (MainProc)
    {
        if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        {
            EnchantOff(OTHER, "ENCHANT_AFRAID");
            if (GetGold(OTHER) >= 10000)
            {
                ChangeGold(OTHER, -10000);
                Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
                SetArmorProperties(CreateObjectAt(SpeedArmor(Random(0, 10)), GetObjectX(OTHER), GetObjectY(OTHER)), 5, 5, Random(16, 19), 0);
                UniPrint(OTHER, "스피드 랜덤 갑옷을 구입하였습니다");
            }
            else
                PrintMessageFormatOne(OTHER, "거래 실패: 금화가 %d 만큼 더 필요합니다", 10000 - GetGold(OTHER));
        }
        else
        {
            UniPrint(OTHER, "스피드 랜덤 갑옷 구입은 10,000 원이 필요합니다");
            Enchant(OTHER, "ENCHANT_AFRAID", 0.5);
        }
    }
}

string SpeedArmor(int num)
{
    string am[] = {"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "MedievalCloak",
        "ChainCoif", "ChainLeggings", "ChainTunic", "SteelHelm", "LeatherArmoredBoots"};

    return am[num];
}

int Moneys(float x, float y, int lv)
{
    string name[] = {"QuestGoldChest", "QuestGoldPile"};
    int g = CreateObjectAt(name[Random(0, 1)], x, y);

    UnitNoCollide(g);
    SetMemory(GetMemory(UnitToPtr(g) + 0x2b4), Random(900, 990) + (lv * 12));
    return g;
}

void HarpoonEvent(int cur, int owner)
{
    if (CurrentHealth(owner))
    {
        if (HasEnchant(owner, "ENCHANT_ANCHORED"))
            MakeWall(owner);
    }
    Delete(cur);
}

void WispDestroyFx(int wp)
{
    int unit = CreateObject("WillOWisp", wp);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 3);
}

void InitSelectRoom()
{
    int ptr = CreateObject("Maiden", 114);
    Frozen(CreateObject("Maiden", 115), 1);
    Frozen(CreateObject("Maiden", 116), 1);
    Frozen(ptr, 1);

    Frozen(CreateObject("Fear", 114), 1);
    Frozen(CreateObject("Fear", 115), 1);
    Frozen(CreateObject("RewardMarker", 116), 1);
    LookWithAngle(ptr, 0);
    LookWithAngle(ptr + 1, 1);
    LookWithAngle(ptr + 2, 2);
    SetDialog(ptr, "AA", SelectGameMode, NothingAction);
    SetDialog(ptr + 1, "AA", SelectGameMode, NothingAction);
    SetDialog(ptr + 2, "AA", SelectGameMode, NothingAction);
    FrameTimer(1, DrawGameModeText);
}

void DrawGameModeText()
{
    int ptr = CreateObject("AmbBeachBirds", 113);
    LookWithAngle(ptr, 215);

    MoveWaypoint(1, GetWaypointX(113), GetWaypointY(113));
    StrSelGameType();
    MoveWaypoint(1, GetWaypointX(114), GetWaypointY(114));
    StrEasyMode();
    MoveWaypoint(1, GetWaypointX(115), GetWaypointY(115));
    StrHardMode();
    MoveWaypoint(1, GetWaypointX(116), GetWaypointY(116));
    StrDoubleMode();
    SaveData(ptr);
}

int SaveData(int num)
{
    int dat;
    if (num)
        dat = num;
    return dat;
}

void SelectGameMode()
{
    int mode = GetDirection(SELF);

    if (!SelMode)
    {
        SelMode = mode + 1;
        if (mode)
        {
            if (mode == 1)
            {
                HARD = 1.0;
                RspRate = 80;
                RspNext = 210;
                SelectHardMode = 1;
                HardModeCount = 10;
                //MobLmt += 10;
                UniPrintToAll(PlayerIngameNick(GetCaller()) + " 님께서 [하드] 모드를 선택하셨습니다");
            }
            else if (mode == 2)
            {
                RspRate = 90;
                RspNext = 210;
                Chance += 3;
                DoubleMode = 1;
                SelectHardMode = 1;
                HardModeCount = 10;
                //MobLmt += 10;
                UniPrintToAll(PlayerIngameNick(GetCaller()) + " 님께서 [더블] 모드를 선택하셨습니다");
            }
        }
        else
        {
            RspRate = 180;
            RspNext = 300;
            UniPrintToAll(PlayerIngameNick(GetCaller()) + " 님께서 [이지] 모드를 선택하셨습니다");
        }
        MoveWaypoint(113, GetWaypointX(103), GetWaypointY(103));
        AllPlayerTeleportAt(103);
        AudioEvent("ShellMouseBoom", 103);
        AudioEvent("ShellSlideIn", 103);
        FrameTimer(180, StartGame);
        FrameTimerWithArg(32, SaveData(0), RemoveStrText);
    }
    else
        MoveObject(OTHER, GetWaypointX(103), GetWaypointY(103));
}

void RemoveStrText(int ptr)
{
    int count = GetDirection(ptr), k;

    Delete(ptr);
    ptr ++;
    for (k = 0 ; k < count ; k ++)
        Delete(ptr + k);
}

void SplashBound(int owner, int func, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 1, k;

    SetOwner(owner, ptr - 1);
    Raise(ptr - 1, ToFloat(func));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 32);
        SetOwner(ptr - 1, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(ptr - 1, 2);
}

void UnitVisibleSplash()
{
    int parent;

    if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    {
        parent = GetOwner(SELF);
        if (CurrentHealth(GetOwner(parent)))
        {
            if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
            {
                Enchant(OTHER, "ENCHANT_VILLAIN", 0.1);
                CallFunction(ToInt(GetObjectZ(parent)));
            }
        }
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, OBJ_HARPOON_BOLT, HarpoonEvent);
}
