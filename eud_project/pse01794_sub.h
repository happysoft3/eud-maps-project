
#include "pse01794_resource.h"
#include "libs/coopteam.h"
#include "libs/playerupdate.h"
#include "libs/game_flags.h"
#include "libs/networkRev.h"
#include "libs/printutil.h"
#include "libs/format.h"
#include "libs/fixtellstory.h"
#include "libs/waypoint.h"
#include "libs/unitstruct.h"

void UpdateRepairItem(int pIndex, int item)
{
    int link, ptr = UnitToPtr(item);
    int codeline[] = {0x50685056, 0xFF005072, 0x708B2414, 0x04C48304,
        0x4D87A068, 0x30FF5600, 0x082454FF, 0x580CC483, 0x9090C35E};

    if (ptr)
    {
        int temp = GetMemory(0x5c3108);
        item = ptr;
        SetMemory(0x5c3108, codeline);
        Unused1f(&pIndex);
        SetMemory(0x5c3108, temp);
    }
}

int SellGerm(int inv)
{
    int thingId = GetUnitThingID(inv), pic;

    if (thingId >= 2795 && thingId <= 2797)
    {
        Delete(inv);

        int pay[] = {1000, 5000, 10000};
        return pay[2797 - thingId];
    }
    else
        return 0;
}

int NextItem(int item) { return GetPreviousItem(item); }

int FindItemGerm(int holder)
{
    int inv = GetLastItem(holder), res = 0, nextInv;

    while (inv)
    {
        nextInv = NextItem(inv);
        res += SellGerm(inv);
        inv = nextInv;
    }
    return res;
}

void SellGermDesc()
{
    UniPrint(OTHER, "당신이 소유하고 있는 모든 보석을 저에게 파시겠어요? 거래를 계속하려면 '예' 버튼을 누르세요");
    TellStoryUnitName("oAo", "thing.db:DiscDiamond", "보석 감별사");
}

void SellGermTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    int trdRes = FindItemGerm(OTHER);
    char msg[128];

    if (trdRes)
    {
        ChangeGold(OTHER, trdRes);
        NoxSprintfString(msg, "가지고 있던 모든 보석을 팔아서 %d골드를 추가했습니다", &trdRes, 1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
    else
        UniPrint(OTHER, "당신은 보석을 하나도 가지고 있지 않아요. 거래를 계속할 수 없습니다");
}

int DrawImageAt(float x, float y, int thingId)
{
    int unit = CreateObjectAt("AirshipBasketShadow", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, thingId);
    return unit;
}

int DrawImageAtLocation(int location, int thingId)
{
    return DrawImageAt(LocationX(location), LocationY(location), thingId);
}

void InitializeSubpart()
{
    MakeCoopTeam();
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}

void DeinitializeSubpart()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

int ImportMonsterStrikeFunc()
{
	int arr[14], link;

	if (!link) //arr[5] = 0x0830B68B;
	{
		arr[0] = 0x448B5650; arr[1] = 0xC0850C24; arr[2] = 0xB08B2574; arr[3] = 0x000002EC; arr[4] = 0x1B74F685; arr[5] = 0x04C8B68B; arr[6] = 0xFE830000;
		arr[7] = 0x68107C00; arr[8] = 0x00507310; arr[9] = 0x56006A50; arr[10] = 0x0C2454FF; arr[11] = 0x5E10C483; arr[12] = 0x93806858; arr[13] = 0x90C30054;
		link = arr;
	}
	return link;
}

int GetVictimUnit()
{
	int ptr = GetMemory(0x834a40);

	if (ptr)
		return GetMemory(ptr + 0x2c);
	return 0;
}

int GetVictimUnit2(int cre)
{
    int ptr = UnitToPtr(cre), ecPtr;

    if (ptr)
    {
        ecPtr = GetMemory(ptr + 0x2ec);
        if (GetMemory(ecPtr + 0x46c))
            return GetMemory(GetMemory(ecPtr + 0x46c) + 0x2c);
    }
    return 0;
}

void RegistryUnitStrikeFunction(int sUnit, int sFunc)
{
    int ptr = UnitToPtr(sUnit), ecPtr, binScrPtr;

    if (ptr)
    {
        ecPtr = GetMemory(ptr + 0x2ec);
        binScrPtr = GetMemory(GetMemory(ptr + 0x2ec) + 0x1e4);
        if (binScrPtr)
        {
            SetMemory(binScrPtr + 0xec, ImportMonsterStrikeFunc());
            //SetMemory(ecPtr + 0x830, sFunc);
            SetMemory(ecPtr + 0x4c8, sFunc);
        }
    }
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	//SetMemory(0x833e70, 1329);		//FishBig
	//SetMemory(0x833e74, 1330);		//FishSmall
	//SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
    SetMemory(0x833e5c, 0);         //wisp
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	//SetMemory(0x833e70, 0x540);		//FishBig
	//SetMemory(0x833e74, 0x540);		//FishSmall
	//SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
    SetMemory(0x833e5c, 0x540);     //
}

int MakePoisoned(int victim, int attacker)
{
    char *pcode;

    if (!pcode)
    {
        char code[]={0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
            0xB8, 0x90, 0x96, 0x54, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x50, 0xB8, 0x30, 
            0x72, 0x50, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3, 0x90, 0x90};
        pcode=code;
    }
    int params[]={UnitToPtr(victim), UnitToPtr(attacker)};
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=pcode;
    int ret = Unused5e(params);
    pExec[0x5e]=pOld;
    return ret;
}

void ExecResourceWhenPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    // if (pUnit)
    //     ShowQuestIntroOne(pUnit, 9999, "WizardChapterBegin11", "GeneralPrint:MapNameHecubahLair");
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        // InitializePopupMessages();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
