
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/networkEx.h"
#include "libs/fxeffect.h"
#include "libs/waypoint.h"

int PlayerRespawnMark(int idx)
{
    int ptr, k=32;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 11) + 1;
        Delete(ptr - 1);
        while (--k>=0)
            ObjectOff(CreateObject("InvisibleLightBlueHigh", 11));
    }
    return ptr + idx;
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int GetPlayerUnit(int pIndex){
    int *pInfo=0x62f9e0 + (0x12dc*pIndex);

    if (!pInfo[0])
        return 0;

    int *ptr=pInfo[0];
    return ptr[11];
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectAt("Hecubah", 5500.0, 100.0);
        Frozen(unit, 1);
    }
    return unit;
}

void CustomMeleeAttackCode(char *bin, int fn)
{
    char code[]={
        0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
        0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
        0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
        0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
    };
    
    int *pStrike = &bin[0xEC];

    pStrike[0] = code;
    int *hack = &bin[0x94];

    hack[15]=fn;
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

int LatestUnitPtr()
{
    int *latestunit = 0x750710;

    return latestunit[0];
}

void DisableObject(int unit)
{
    if (IsObjectOn(unit))
        ObjectOff(unit);
}

int GetListPrev(int cur)
{
    return GetOwner(cur);
}

int GetListNext(int cur)
{
    return ToInt(GetObjectZ(cur));
}

void SetListPrev(int cur, int tg)
{
    SetOwner(tg, cur);
}

void SetListNext(int cur, int tg)
{
    Raise(cur, ToFloat(tg));
}

int ImportSetPlayerActionFunc()
{
    int *codestream;

    if (codestream == NULLPTR)
    {
        int codebyte[] = {0x4FA02068, 0x72506800, 0x14FF0050, 0x54FF5024, 0xFF500424,
            0x830C2454, 0x90C310C4, 0x9090C390, 0xC03108C4, 0x909090C3};
        
        codestream = codebyte;
    }
    return codestream;
}

void SetPlayerActionFunc(int pUnit, int val)
{
    if (!IsPlayerUnit(pUnit))
        return;

    int *ptr = UnitToPtr(pUnit);

    if (ptr == NULLPTR)
        return;
    int *btbase = 0x5c308c;
    int *target = btbase[0x5a];
    int prev = target;

    target[0] = ImportSetPlayerActionFunc();
    Unused5a(ptr, val);
    target[0] = prev;
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

int DrawMagicIcon(float x, float y, short unitId)
{
    int unit = CreateObjectById(OBJ_AIRSHIP_BASKET_SHADOW, x, y);
    int *ptr = UnitToPtr(unit);

    ptr[1]= unitId;
    return unit;
}

void DrawGeometryRing(short orbType, float x, float y)
{
    int u=60;
    float speed = 2.3;

    while (--u>=0)
        LinearOrbMove(CreateObjectById(orbType, x,y), MathSine((u * 6) + 90, -12.0), MathSine(u * 6, -12.0), speed, 10);
}

void GreenLightningEffect(float x1, float y1, float x2, float y2)
{
    GreenLightningFx(FloatToInt(x1), FloatToInt(y1), FloatToInt(x2), FloatToInt(y2), 25);
}
void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}

void RiseBlueSpark(float x, float y)
{
    int fx = CreateObjectById(OBJ_TELEPORT_WAKE, x, y);
    Frozen(fx, TRUE);
    UnitNoCollide(fx);
}

void onCollideInvisbleTelepo(){
    if (CurrentHealth(OTHER))
    {
        int dest=GetTrigger()+1;

        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
        MoveObject(OTHER, GetObjectX(dest),GetObjectY(dest));
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
    }
}

void DeleteInvisibleTelpo(int t){
    if (GetUnitThingID(t) == OBJ_STORM_CLOUD)
    {
        if (GetUnitThingID(t+2)==OBJ_TELEPORT_WAKE){
        Delete(t++);
        Delete(t++);
        Delete(t++);
        }
    }
}

int PlacingInvisibleTeleporting(short srcLoc, short destLoc){
    int s=CreateObjectById(OBJ_STORM_CLOUD, LocationX(srcLoc),LocationY(srcLoc));

    SetUnitCallbackOnCollide(s, onCollideInvisbleTelepo);
    SetUnitFlags(s, GetUnitFlags(s) ^ (UNIT_FLAG_NO_COLLIDE|UNIT_FLAG_NO_PUSH_CHARACTERS));
    Frozen(s, TRUE);

    DrawMagicIcon(LocationX(destLoc),LocationY(destLoc),OBJ_SPELL_ICONS);
    RiseBlueSpark(GetObjectX(s),GetObjectY(s));
    return s;
}

void ManaBombCancelFx(int sUnit)
{
    int caster = CreateObjectById(OBJ_IMAGINARY_CASTER, GetObjectX(sUnit), GetObjectY(sUnit));

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}

int HecubahOrbBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 1332240738; arr[2] = 25202; arr[17] = 600; arr[19] = 110; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; 
		arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; arr[40] = 1852140903; arr[41] = 116; 
		arr[53] = 1133903872; arr[55] = 26; arr[56] = 36; arr[57] = 5547984; arr[58] = 5545616; 
		arr[60] = 1384; arr[61] = 46914560; 
		link = &arr;
	}
	return link;
}

void HecubahOrbSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1079194419);
		SetMemory(ptr + 0x224, 1079194419);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 600);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 600);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahOrbBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int HecubahBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 400; arr[19] = 84; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 150; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; 
		arr[57] = 5548288; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void HecubahSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1075922862);
		SetMemory(ptr + 0x224, 1075922862);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 400);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 295; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 2052; arr[24] = 1069547520; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1101529088; arr[29] = 40; arr[31] = 8; arr[32] = 13; arr[33] = 21; 
		arr[34] = 2; arr[35] = 3; arr[36] = 20; arr[37] = 1684631635; arr[38] = 1884516965; 
		arr[39] = 29801; arr[53] = 1128792064; arr[55] = 18; arr[56] = 25; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

void BlackWidowSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2052);
		SetMemory(GetMemory(ptr + 0x22c), 295);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 295);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 160; arr[19] = 73; 
		arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 19; arr[56] = 27; 
		arr[58] = 5545472; 
		link = &arr;
	}
	return link;
}

void FireSpriteSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074538742);
		SetMemory(ptr + 0x224, 1074538742);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 67584);
		SetMemory(GetMemory(ptr + 0x22c), 160);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 160);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}


int VileZombieBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701603670; arr[1] = 1651339098; arr[2] = 25961; arr[17] = 325; arr[18] = 50; 
		arr[19] = 55; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1067869798; arr[25] = 1; 
		arr[26] = 5; arr[27] = 1; arr[28] = 1106247680; arr[29] = 30; arr[31] = 10; 
		arr[34] = 3; arr[35] = 2; arr[36] = 20; arr[59] = 5543680; arr[60] = 1361; 
		arr[61] = 46895184; 
		link = &arr;
	}
	return link;
}

void VileZombieSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 325);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 325);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, VileZombieBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int ZombieBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339098; arr[1] = 25961; arr[17] = 275; arr[19] = 48; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1067869798; arr[25] = 1; arr[26] = 5; arr[27] = 2; 
		arr[28] = 1106247680; arr[29] = 20; arr[31] = 10; arr[59] = 5542784; arr[60] = 1360; 
		arr[61] = 46895440; 
		link = &arr;
	}
	return link;
}

void ZombieSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1069044203);
		SetMemory(ptr + 0x224, 1069044203);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 275);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 275);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, ZombieBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int LichBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[17] = 375; arr[19] = 60; arr[21] = 1065353216; arr[23] = 2048; 
		arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; arr[28] = 1106771968; arr[29] = 80; 
		arr[31] = 10; arr[32] = 8; arr[33] = 11; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1342; arr[61] = 46909440; 
		link = &arr;
	}
	return link;
}

void LichSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1072064102);
		SetMemory(ptr + 0x224, 1072064102);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2048);
		SetMemory(GetMemory(ptr + 0x22c), 375);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 375);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WizardRedBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
        arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4;
		arr[53] = 1128792064; arr[54] = 4; 
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 270; 
		arr[19] = 55; arr[21] = 1065353216; arr[24] = 1071225242; arr[26] = 4; arr[28] = 1101004800; 
		arr[29] = 20; arr[31] = 3; arr[32] = 5; arr[33] = 10; arr[59] = 5542784; 
		arr[60] = 1388; arr[61] = 46915072; 
		link = &arr;
	}
	return link;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 270);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 270);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}
void MyGreenLightningFloat(float x1, float y1, float x2, float y2)
{
    GreenLightningFx(FloatToInt(x1), FloatToInt(y1), FloatToInt(x2), FloatToInt(y2), 15);
}

int CheckSeePlayers(int unit)
{
    int u=32;

    while (--u>=0)
    {
        if (IsVisibleOr(unit, GetPlayerUnit(u)))
            return u;
    }
    return -1;
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 275; arr[19] = 80; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; arr[30] = 1102053376; 
		arr[31] = 4; arr[32] = 19; arr[33] = 27; arr[34] = 3; arr[35] = 3; 
		arr[36] = 20; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 275;	hpTable[1] = 275;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpawnMonsterGoon(float x,float y){
    int mon=CreateObjectById(OBJ_GOON,x,y);

    GoonSubProcess(mon);
    return mon;
}

float GetRatioUnitWpXY(int unit, int wp, int mode, float size)
{
    if (!mode)
        return (GetObjectX(unit) - GetWaypointX(wp)) * size / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp));
    else
        return (GetObjectY(unit) - GetWaypointY(wp)) * size / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp));
}

void changeNumber(int n, int value)
{
    controlHealthbarMotion(n,value%10);
}

int createNumber(float x, float y, int value)
{
    int n=DummyUnitCreateById(OBJ_BEAR_2,x,y);

    LookWithAngle(n,128);
    UnitNoCollide(n);
    if (value!=0)
        changeNumber(n,value);
    return n;
}
