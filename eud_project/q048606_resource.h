
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"

void GRPDump_BrownGirlOutput(){}
#define BROWN_GIRL_IMAGE_ID 115005
void initializeImageBrownGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_IMP;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BrownGirlOutput)+4, thingId, NULLPTR, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0,BROWN_GIRL_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign4Directions(3,BROWN_GIRL_IMAGE_ID+4,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy4Directions(BROWN_GIRL_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameAssign4Directions(6,BROWN_GIRL_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy4Directions(BROWN_GIRL_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy4Directions(BROWN_GIRL_IMAGE_ID+4,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy4Directions(BROWN_GIRL_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy4Directions(BROWN_GIRL_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy4Directions(BROWN_GIRL_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameCopy4Directions(BROWN_GIRL_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,0);
}
void GRPDumpBluePantsGirlOutput(){}

#define BLUE_PANTS_GIRL_IMAGE_START_ID 121736
void initializeImageBluePantsGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_RAT;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpBluePantsGirlOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, BLUE_PANTS_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, BLUE_PANTS_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BLUE_PANTS_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, BLUE_PANTS_GIRL_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BLUE_PANTS_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BLUE_PANTS_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BLUE_PANTS_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     BLUE_PANTS_GIRL_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BLUE_PANTS_GIRL_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BLUE_PANTS_GIRL_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDumpGoldWingsOutput(){}
#define GOLD_WINGS_IMAGE_START_ID 117185
void initializeImageGoldWings(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_URCHIN;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpGoldWingsOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, GOLD_WINGS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, GOLD_WINGS_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     GOLD_WINGS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, GOLD_WINGS_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     GOLD_WINGS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     GOLD_WINGS_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     GOLD_WINGS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     GOLD_WINGS_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     GOLD_WINGS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     GOLD_WINGS_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
}
void GRPDumpPinkHairOutput(){}
#define PINK_HAIR_IMAGE_START_ID 113171
void initializeImagePinkHairGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GREEN_FROG;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpPinkHairOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, PINK_HAIR_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, PINK_HAIR_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     PINK_HAIR_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, PINK_HAIR_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     PINK_HAIR_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     PINK_HAIR_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     PINK_HAIR_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     PINK_HAIR_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     PINK_HAIR_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     PINK_HAIR_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDumpVenusOutput(){}
#define VENUS_IMAGE_START_ID 121039
void initializeImageVenus(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_ALBINO_SPIDER;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpVenusOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, VENUS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, VENUS_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     VENUS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, VENUS_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     VENUS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     VENUS_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     VENUS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     VENUS_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     VENUS_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     VENUS_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDumpTeletobbyOutput(){}
#define TELETOBBY_BASE_IMAGE_ID 124381
void initializeImageFrameTeletobby(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOLF;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpTeletobbyOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, TELETOBBY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, TELETOBBY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, TELETOBBY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     TELETOBBY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_InuyashaOutput(){}
#define INUYASHA_BASE_IMAGE_ID  119258
void initializeImageFrameInuyasha(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SWORDSMAN;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_InuyashaOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, INUYASHA_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, INUYASHA_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     INUYASHA_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, INUYASHA_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     INUYASHA_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     INUYASHA_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     INUYASHA_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     INUYASHA_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     INUYASHA_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     INUYASHA_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_RedNinza2Output(){}
#define REDNINZA_BASE_IMAGE_ID   118946
void initializeImageFrameRedNinza2(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_ARCHER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_RedNinza2Output)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, REDNINZA_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, REDNINZA_BASE_IMAGE_ID +4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     REDNINZA_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, REDNINZA_BASE_IMAGE_ID +8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     REDNINZA_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     REDNINZA_BASE_IMAGE_ID +4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     REDNINZA_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     REDNINZA_BASE_IMAGE_ID +8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     REDNINZA_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     REDNINZA_BASE_IMAGE_ID +4, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
}

void GRPDump_SlimeDumpOutput(){}

#define SLIME_DUMP_OUTPUT_IMAGE_START_AT 121568

void initializeImageSlimeOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GIANT_LEECH;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SlimeDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,5,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign4Directions(6,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign4Directions(9,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign4Directions(12,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);

    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_YellowBearOutput(){}

#define YELLOW_BEAR_BASE_IMAGE_ID 113678
void initializeImageFrameYellowBear(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_FLYING_GOLEM;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_YellowBearOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  YELLOW_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  YELLOW_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(YELLOW_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  YELLOW_BEAR_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(YELLOW_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(YELLOW_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(YELLOW_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(YELLOW_BEAR_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(YELLOW_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(YELLOW_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
}

void GRPDump_RedWolfOutput(){}

#define RED_WOLF_BASE_IMAGE_ID 120299
void initializeImageFrameRedWolf(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SPITTING_SPIDER;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_RedWolfOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  RED_WOLF_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  RED_WOLF_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  RED_WOLF_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
    AnimFrameCopy4Directions(RED_WOLF_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_WhiteRebbitOutput(){}

#define WHITE_REBBIT_BASE_IMAGE_ID 122643
void initializeImageFrameWhiteRebbit(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SHADE;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_WhiteRebbitOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  WHITE_REBBIT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  WHITE_REBBIT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(WHITE_REBBIT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  WHITE_REBBIT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(WHITE_REBBIT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(WHITE_REBBIT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(WHITE_REBBIT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(WHITE_REBBIT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(WHITE_REBBIT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(WHITE_REBBIT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_OrangeDuckOutput(){}

#define ORANGE_DUCK_BASE_IMAGE_ID 125517
void initializeImageFrameOrangeDuck(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SCORPION;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_OrangeDuckOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  ORANGE_DUCK_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  ORANGE_DUCK_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(ORANGE_DUCK_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  ORANGE_DUCK_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(ORANGE_DUCK_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(ORANGE_DUCK_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(ORANGE_DUCK_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(ORANGE_DUCK_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(ORANGE_DUCK_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(ORANGE_DUCK_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_BlueDogOutput(){}

#define BLUE_DOG_BASE_IMAGE_ID 133943
void initializeImageFrameBlueDog(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BlueDogOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  BLUE_DOG_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  BLUE_DOG_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(BLUE_DOG_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  BLUE_DOG_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(BLUE_DOG_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(BLUE_DOG_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(BLUE_DOG_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(BLUE_DOG_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(BLUE_DOG_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(BLUE_DOG_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_TeddyBearOutput(){}
#define TEDDY_BEAR_BASE_IMAGE_ID 122915
void initializeImageFrameTeddyBear(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_TeddyBearOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  TEDDY_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  TEDDY_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(TEDDY_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  TEDDY_BEAR_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(TEDDY_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(TEDDY_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(TEDDY_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(TEDDY_BEAR_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(TEDDY_BEAR_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(TEDDY_BEAR_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_MIKUOutput(){}
#define MIKU_BASE_IMAGE_ID 123725
void initializeImageFrameMiku(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WHITE_WOLF;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MIKUOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  MIKU_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  MIKU_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(MIKU_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  MIKU_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(MIKU_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(MIKU_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(MIKU_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(MIKU_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(MIKU_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(MIKU_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void GRPDump_RedHeadOutput(){}

#define RED_HEAD_BASE_IMAGE_ID 133955
void initializeImageFrameRedHead(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_RedHeadOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  RED_HEAD_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  RED_HEAD_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(RED_HEAD_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  RED_HEAD_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(RED_HEAD_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(RED_HEAD_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(RED_HEAD_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(RED_HEAD_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(RED_HEAD_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(RED_HEAD_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_AooniOutput(){}
#define AOONI_NORMAL_BASE_IMAGE_ID 129717
void initializeImageFrameAooniNormal(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_AooniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  AOONI_NORMAL_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  AOONI_NORMAL_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_NORMAL_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_NORMAL_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_NORMAL_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_NORMAL_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_NORMAL_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_NORMAL_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_NORMAL_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(AOONI_NORMAL_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 133979
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_TROLL;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount-4);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}

void GRPDump_BlueHairFairyOutput(){}
#define BLUE_HAIR_FAIRY_BASE_IMAGE_ID 133967
void initializeImageFrameBlueHairFairy(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_WARRIOR;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BlueHairFairyOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  BLUE_HAIR_FAIRY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  BLUE_HAIR_FAIRY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign4Directions(6,  BLUE_HAIR_FAIRY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+4,IMAGE_SPRITE_MON_ACTION_IDLE,3);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+4,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+4,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameCopy4Directions(BLUE_HAIR_FAIRY_BASE_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_TomastrainOutput(){}

#define TOMASTRAIN_IMAGE_START_AT 116519

void initializeImageTomastrain(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GRUNT_AXE;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_TomastrainOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    TOMASTRAIN_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(          TOMASTRAIN_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(          TOMASTRAIN_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(           TOMASTRAIN_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);    
}

void GRPDump_ToothOniPurpleOutput(){}

#define TOOTH_ONI_PURPLE_BASE_IMAGE_ID 130863
void initializeImageFrameToothOniPurple(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_OGRE_BRUTE;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_ToothOniPurpleOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, TOOTH_ONI_PURPLE_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, TOOTH_ONI_PURPLE_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, TOOTH_ONI_PURPLE_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_BlackMaidenOutput(){}
#define BLACK_MAIDEN_BASE_IMAGE_ID 116110
void initializeImageFrameBlackMaiden(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WIZARD_GREEN;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BlackMaidenOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(3,  BLACK_MAIDEN_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(0,  BLACK_MAIDEN_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(BLACK_MAIDEN_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  BLACK_MAIDEN_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(BLACK_MAIDEN_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(BLACK_MAIDEN_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(BLACK_MAIDEN_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(BLACK_MAIDEN_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(BLACK_MAIDEN_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(BLACK_MAIDEN_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_Replenisher(){}

#define REPLENISHER_IMAGE_START_AT 123388

void initializeImageReplenisher(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BLACK_BEAR;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_Replenisher)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(   REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions(   REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(   REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
}

void GRPDump_MonkeyOutput(){}
#define MONKEY_OUTPUT_BASE_IMAGE_ID 117497
void initializeImageFrameMonkeyRed(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_URCHIN_SHAMAN;
    int xyInc[]={0,9};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MonkeyOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_CAST_SPELL,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  MONKEY_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  MONKEY_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  MONKEY_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(MONKEY_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 0);
}

void InitializeResource(){
    int imgVec=CreateImageVector(4096);
     initializeImageBrownGirl(imgVec);
      initializeImageBluePantsGirl(imgVec);
      initializeImageGoldWings(imgVec);
      initializeImagePinkHairGirl(imgVec);
      initializeImageVenus(imgVec);
      initializeImageFrameTeletobby(imgVec);
      initializeImageFrameInuyasha(imgVec);
      initializeImageFrameRedNinza2(imgVec);
      initializeImageSlimeOutput(imgVec);
      initializeImageFrameYellowBear(imgVec);
      initializeImageFrameRedWolf(imgVec);
      initializeImageFrameWhiteRebbit(imgVec);
      initializeImageFrameOrangeDuck(imgVec);
      initializeImageFrameBlueDog(imgVec);
      initializeImageFrameTeddyBear(imgVec);
      initializeImageFrameMiku(imgVec);
      initializeImageHealthBar(imgVec);
      initializeImageFrameRedHead(imgVec);
      initializeImageFrameAooniNormal(imgVec);
    //   WriteLog("a");
      initializeImageFrameBillyOni(imgVec);
    //   WriteLog("b");
      initializeImageFrameBlueHairFairy(imgVec);
    //   WriteLog("c");
      initializeImageTomastrain(imgVec);
    //   WriteLog("d");
      initializeImageFrameToothOniPurple(imgVec);
    //   WriteLog("e");
      initializeImageFrameBlackMaiden(imgVec);
    //   WriteLog("f");
      initializeImageReplenisher(imgVec);
    //   WriteLog("g");
    //   initializeImageFrameHiroshi(imgVec);
    //   WriteLog("h");
      initializeImageFrameMonkeyRed(imgVec);
    //   WriteLog("i");
       DoImageDataExchange(imgVec);
    //   WriteLog("j");
}
