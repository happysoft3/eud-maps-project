
#include "libs/winapi.h"

int INIWrite(string sectionName, string keyName, string value, string fileName)
{
    return WritePrivateProfileStringA(
        StringUtilGetScriptStringPtr(sectionName), 
        StringUtilGetScriptStringPtr(keyName),
        StringUtilGetScriptStringPtr(value),
        StringUtilGetScriptStringPtr(fileName)
    );
}

int INIRemoveKey(string sectionName, string keyName, string fileName)
{
    return WritePrivateProfileStringA(
        StringUtilGetScriptStringPtr(sectionName),
        StringUtilGetScriptStringPtr(keyName),
        NULLPTR,
        StringUtilGetScriptStringPtr(fileName)
    );
}

int INIRemoveSection(string sectionName, string fileName)
{
    return WritePrivateProfileStringA(
        StringUtilGetScriptStringPtr(sectionName),
        NULLPTR,
        NULLPTR,
        StringUtilGetScriptStringPtr(fileName)
    );
}

int INIRead(string sectionName, string keyName, string alt, char *pDest, int nSize, string fileName)
{
    return GetPrivateProfileStringA(
        StringUtilGetScriptStringPtr(sectionName),
         StringUtilGetScriptStringPtr(keyName), 
         StringUtilGetScriptStringPtr(alt), pDest, nSize,
          StringUtilGetScriptStringPtr(fileName));
}

#define LOCAL_BUFFER_SIZE 1024
string INIReadAsString(string sectionName, string keyName, string alt, string fileName)
{
    char local[LOCAL_BUFFER_SIZE];
    
    GetPrivateProfileStringA(
        StringUtilGetScriptStringPtr(sectionName),
         StringUtilGetScriptStringPtr(keyName), 
         StringUtilGetScriptStringPtr(alt), local, sizeof(local),
          StringUtilGetScriptStringPtr(fileName)
    );
    return ReadStringAddressEx(local);
}

int INICheckValue(string sectionName, string keyName, string value, string fileName)
{
    char local[LOCAL_BUFFER_SIZE];

    INIRead(sectionName,keyName,"####nodata####",local,sizeof(local),fileName);
    return !StringUtilCompare(StringUtilGetScriptStringPtr(value),local);
}
