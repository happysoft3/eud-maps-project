
#include "boss_gvar.h"
#include "boss_utils.h"
#include "libs/voiceList.h"
#include "libs/printutil.h"

int BearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[17] = 2000; arr[19] = 80; arr[21] = 1048576000; arr[23] = 65536; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1119092736; 
		arr[29] = 70; arr[31] = 2; arr[32] = 6; arr[33] = 6; arr[59] = 5542784; 
		arr[60] = 1365; arr[61] = 46903040; 
	pArr = arr;
	return pArr;
}

void BearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 2000;	hpTable[1] = 2000;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = BearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1048576000;
}

int createBossAckMech(float x, float y){
    int b=CreateObjectById(OBJ_BEAR,x,y);

    BearSubProcess(b);
    SetUnitVoice(b,MONSTER_VOICE_FlyingGolem);
    return b;
}

int OgreWarlordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996367; arr[1] = 1819435351; arr[2] = 6582895; arr[17] = 2350; arr[19] = 90; 
		arr[21] = 1049918177; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 2; arr[26] = 10; 
		arr[27] = 7; arr[28] = 1109393408; arr[29] = 100; arr[30] = 1112014848; arr[31] = 11; 
		arr[32] = 11; arr[33] = 19; arr[37] = 1701996367; arr[38] = 1920297043; arr[39] = 1852140393; 
		arr[53] = 1128792064; arr[54] = 4; arr[55] = 20; arr[56] = 30; arr[59] = 5542784; 
		arr[60] = 1389; arr[61] = 46914048; 
	pArr = arr;
	return pArr;
}

void OgreWarlordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 2350;	hpTable[1] = 2350;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = OgreWarlordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1049918177;
}

int createBossOgrelord(float x,float y){
    int b=CreateObjectById(OBJ_OGRE_WARLORD,x,y);
    OgreWarlordSubProcess(b);
    return b;
}

int GhostBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936681031; arr[1] = 116; arr[17] = 2700; arr[19] = 70; arr[21] = 1065353216; 
		arr[23] = 65537; arr[24] = 1065353216; arr[27] = 1; arr[28] = 1113325568; arr[29] = 35; 
		arr[31] = 4; arr[59] = 5542784; arr[60] = 1325; arr[61] = 46900224; 
	pArr = arr;
	return pArr;
}

void GhostSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 2700;	hpTable[1] = 2700;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = GhostBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createBossMaidenGhost(float x,float y){
    int b=CreateObjectById(OBJ_GHOST,x,y);
    GhostSubProcess(b);
    return b;
}
int DemonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869440324; arr[1] = 110; arr[17] = 3000; arr[19] = 96; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 3; arr[28] = 1112014848; 
		arr[29] = 80; arr[31] = 1; arr[53] = 1128792064; arr[54] = 4; arr[58] = 5545472; 
		arr[59] = 5542784; arr[60] = 1347; arr[61] = 46910976; 
	pArr = arr;
	return pArr;
}

void DemonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077432811;		ptr[137] = 1077432811;
	int *hpTable = ptr[139];
	hpTable[0] = 3000;	hpTable[1] = 3000;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = DemonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int createBossKillerian(float x,float y){
    int b=CreateObjectById(OBJ_DEMON,x,y);
    DemonSubProcess(b);
    return b;
}

int MechanicalGolemBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751344461; arr[1] = 1667853921; arr[2] = 1866951777; arr[3] = 7169388; arr[17] = 3500; 
		arr[19] = 55; arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1067869798; arr[26] = 4; 
		arr[27] = 5; arr[28] = 1112014848; arr[29] = 100; arr[30] = 1120403456; arr[31] = 2; 
		arr[58] = 5545616; arr[59] = 5544288; arr[60] = 1318; arr[61] = 46900992; 
	pArr = arr;
	return pArr;
}

void MechanicalGolemSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 3500;	hpTable[1] = 3500;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = MechanicalGolemBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createBossBillyOni(float x,float y){
    int b=CreateObjectById(OBJ_MECHANICAL_GOLEM,x,y);
    MechanicalGolemSubProcess(b);
    return b;
}

int bossEnding(float x,float y){
    UniPrintToAll("보스괴물 전멸! - 방금 모든 보스 괴물을 처치했습니다");
    QureyBossClear(0,TRUE);
    return 0;
}
