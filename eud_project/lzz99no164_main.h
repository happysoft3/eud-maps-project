
#include "subway_initscan.h"
#include "lzz99no164_reward.h"
#include "lzz99no164_player.h"
#include "lzz99no164_mob.h"
#include "lzzno164_resource.h"
#include "libs/coopteam.h"
#include "libs/networkRev.h"
#include "libs/groupUtils.h"

#define GROUP_finalSecretWalls 4
#define GROUP_bypassMidWalls 5
#define GROUP_startMagicWalls 0
#define GROUP_part3Walls 2
#define GROUP_part2Walls 1
#define GROUP_midPartWalls 3

void onStoneBlockDestroyed(){
    float x=GetObjectX(SELF), y=GetObjectY(SELF);
    DeleteObjectTimer(CreateObjectById(OBJ_OLD_BIG_SMOKE, x,y), 9);
    Delete(SELF);
    if (Random(0,5)){
        if (Random(0,3))
        {
            PushTimerQueue(6, CreateObjectById(OBJ_OLD_SMOKE, x,y), CreateRandomItemCommon);
            return;
        }
        PushTimerQueue(3, CreateObjectById(OBJ_GREEN_SMOKE,x,y),CreateRandomMonsterLow);
    }
}

void onStoneBlockDetected(int bl){
    if (Random(0, 9))
    {
        SetUnitMaxHealthEx(bl, 325);
        SetUnitCallbackOnDeath(bl, onStoneBlockDestroyed);
        return;
    }
    float x=GetObjectX(bl),y=GetObjectY(bl);
    Delete(bl);
    SetUnitMaxHealth( CreateObjectById(OBJ_BLACK_POWDER_BARREL_2, x,y), 200);
}

void onMecaBlockDestroyed(){
    float x=GetObjectX(SELF), y=GetObjectY(SELF);
    DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE, x,y), 9);
    Delete(SELF);
    if (Random(0,5)){
        if (Random(0,3))
        {
            PushTimerQueue(6, CreateObjectById(OBJ_SMOKE, x,y), CreateRandomItemCommon);
            return;
        }
        PushTimerQueue(3, CreateObjectById(OBJ_STORM_CLOUD,x,y),CreateRandomMonsterHigh);
    }
}

void onXMasTreeDestroyed(){
    float x=GetObjectX(SELF), y=GetObjectY(SELF);

    PlaySoundAround(SELF, SOUND_Chime);
    Delete(SELF);
    DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE, x,y), 9);
    if (Random(0,3)){
        PushTimerQueue(6, CreateObjectById(OBJ_SMOKE, x,y), CreateRandomItemCommon);
        return;
    }
    PushTimerQueue(3, CreateObjectById(OBJ_STORM_CLOUD,x,y),CreateRandomMonsterXMas);
}

void onMecaBlockDetected(int bl){
    SetUnitMaxHealthEx(bl, 508);
    SetUnitCallbackOnDeath(bl, onMecaBlockDestroyed);
}

void onXMasTreeDetected(int tr){
    SetUnitMaxHealthEx(tr, 600);
    SetUnitCallbackOnDeath(tr, onXMasTreeDestroyed);
}

void EndScan(int *pParams)
{
    // int count =pParams[UNITSCAN_PARAM_COUNT];
    // char msg[128];

    // NoxSprintfString(msg, "%d개의 유닛을 스캔했습니다", &count, 1);
    // UniPrintToAll(ReadStringAddressEx(msg));
    // WallOpen(Wall(15, 235));
    // WallOpen(Wall(16, 236));
    // int lunit;
    // QueryLUnit(&lunit,0,0);
    // AwakeLineMonster(lunit);
    // SecondTimer(2, StartGameMent);
    WallGroupSetting(GROUP_startMagicWalls, WALL_GROUP_ACTION_OPEN);
    UniPrintToAll("[][][][][]벽 뚫고 탈출하기 - 제작자237[][][][][]");
}

void startInitscan(){
    int initScanHash, lastUnit=GetMasterUnit();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_STONE_BLOCK, onStoneBlockDetected);
    HashPushback(initScanHash, OBJ_IRON_BLOCK, onMecaBlockDetected);
    HashPushback(initScanHash, OBJ_SPIKE_BLOCK, onXMasTreeDetected);
    StartUnitScanEx(Object("firstscan"), lastUnit, initScanHash, EndScan);
}

void initializeReadables(){
    RegistSignMessage(Object("read1"), "<<<<-----------벽 뚫고 탈출하기 - 제작. 237---------------");
    RegistSignMessage(Object("read2"), "-표지판- 이 벽 뒤에 갱도가 있음. 벽을 부수어 보시오");
    RegistSignMessage(Object("read3"), "-표지판- 서비스 공간");
    RegistSignMessage(Object("read4"), "-표지판- 승리- 탈출에 성공을 하시었어요!");
}

void onMonsterDeath(){
    float x=GetObjectX(SELF), y=GetObjectY(SELF);
    DeleteObjectTimer(SELF, 3);
    PushTimerQueue(6, CreateObjectById(OBJ_SMOKE, x,y), CreateRandomItemCommon);
}

void initializeMonsters(){
    QueryDeathEvent(0, onMonsterDeath);

    // createMobSantaClaus(LocationX(17), LocationY(17));
    // createMobSantaClaus(LocationX(18), LocationY(18));
    // createMobSantaClaus(LocationX(19), LocationY(19));
}

//reward
//initscan
//monsters
//resources
void OnInitializeMap(){
    MusicEvent();
    startInitscan();
    blockObserverMode();
    InitializePlayerSystem();
    initializeReadables();
    FrameTimer(1, MakeCoopTeam);
    initializeMonsters();
}
void OnShutdownMap(){
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void OnPlayerEntryMapWrapped(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
    {
        pUnit = GetMemory(ptr+0x2c);
        int pIndex = GetMemory(pInfo+0x810);
        PlayerClassOnInit   (pIndex, pUnit);
        // ShowQuestIntroOne(pUnit, 237, "CopyrightScreen", "Wolchat.c:PleaseWait");
    }
    
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

void ExecClientPart()
{
    InitializeResources();
    // InitializePopupMessages();
    // NetworkUtilClientTimerEnablerExec(ClientProcLoop);
}

void openMidWalls(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_midPartWalls, WALL_GROUP_ACTION_OPEN);
    UniPrintToAll("주변 어딘가에서 비밀벽이 열렸습니다");
}

void openPart2Walls(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_part2Walls, WALL_GROUP_ACTION_OPEN);
    UniPrintToAll("주변 어딘가에서 비밀벽이 열렸습니다");
}

void openFinalWalls(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_part3Walls, WALL_GROUP_ACTION_OPEN);
    UniPrintToAll("주변 어디선가에서 비밀벽이 하나 열렸습니다");
}
void openBypassWalls(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_finalSecretWalls, WALL_GROUP_ACTION_OPEN);
    WallGroupSetting(GROUP_bypassMidWalls, WALL_GROUP_ACTION_OPEN);
    UniPrintToAll("주변 어디선가에서 비밀벽이 하나 열렸습니다");
}

void onTryPickupCandle(){
    UniPrint(OTHER, "당신은 승리자 입니다!");
    PlaySoundAround(OTHER,SOUND_JournalEntryAdd);
}

void createWinCandle(float x, float y){
    int c=CreateObjectById(OBJ_CANDLE_1,x,y);

    SetUnitCallbackOnPickup(c,onTryPickupCandle);
}

void youWinEvent(){
    ObjectOff(SELF);
    float x=GetObjectX(OTHER), y=GetObjectY(OTHER);
    UniPrintToAll("!승리!- 모든 벽을 부수고 마침내 탈출하였습니다!!");
    DeleteObjectTimer(CreateObjectById(OBJ_LEVEL_UP, x,y), 300);
    DeleteObjectTimer(CreateObjectById(OBJ_MANA_BOMB_CHARGE, x,y), 180);
    PlaySoundAround(SELF,SOUND_LevelUp);
    PlaySoundAround(SELF,SOUND_StaffOblivionAchieve1);
    createWinCandle(x,y);
    createWinCandle(x,y);
    createWinCandle(x,y);
    createWinCandle(x,y);
}

