
// #include "test1130-sub.h"
#include "libs/define.h"
#include "libs/stringutil.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"

string ReadStringAddressXX(int t)
{
	return ToStr((t - 0x97bb40) / 4);
}

void BoltBugTest(){
    Effect("CHARM",GetObjectX(OTHER),GetObjectY(OTHER),-10.0, -10.0);
}
void OpenLichWall()
{
    PrintToAll(FloatToString(GetObjectX(SELF)));
    PrintToAll(FloatToString(GetObjectY(SELF)));
}

void onStormCollide(){
    if (CurrentHealth(OTHER))
    {
        Effect("RICOCHET",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
    }
}

void MapInitialize()
{
    MusicEvent();
    int s=CreateObjectById(OBJ_STORM_CLOUD, GetWaypointX(91),GetWaypointY(91));

    SetUnitCallbackOnCollide(s, onStormCollide);
    SetUnitFlags(s, GetUnitFlags(s) ^ (UNIT_FLAG_NO_COLLIDE|UNIT_FLAG_NO_PUSH_CHARACTERS));
    Frozen(s, TRUE);
}
