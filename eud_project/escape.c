
#include "noxscript\builtins.h"
#include "libs\typecast.h"
#include "libs\callmethod.h"
#include "libs\opcodehelper.h"
#include "libs\unitstruct.h"
#include "libs\unitutil.h"
#include "libs\printutil.h"
#include "libs\username.h"

#include "libs\mathlab.h"
#include "libs\coopteam.h"
#include "libs\waypoint.h"
#include "libs\fxeffect.h"
#include "libs\spellutil.h"

#define PLAYER_DEATH_FLAG 0x80000000
#define NULLPTR 0

int LastUnitID = 221;
int CurStage, PlrDead;
int player[20], Num_fst, Num_scd, Num_thd;


void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[17] = 100; arr[18] = 30; arr[19] = 50; 
		arr[20] = 1045220557; arr[21] = 1061158912; arr[23] = 32; arr[24] = 1067869798; 
		arr[26] = 4;arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; 
		arr[40] = 116; arr[53] = 1073741824;
		arr[55] = 12; arr[56] = 20;
		link = &arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 15; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 1; 
		arr[35] = 2; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = &arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101;
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146;
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801;
		arr[53] = 1128792064; 
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = &arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link = &arr;
	}
	return link;
}

void Nop(int n)
{ }

void CheckMonsterThing(int unit)
{
    int summonActions[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        summonActions[5] = MonsterGoonProcess; summonActions[72] = MonsterStrongWhiteWizProcess; summonActions[30] = MonsterWeirdlingBeastProcess; summonActions[34] = MonsterBlackWidowProcess; summonActions[6] = MonsterBear2Process;
        summonActions[12] = MonsterFireSpriteProcess; summonActions[73] = MonsterWizardRedProcess; summonActions[29] = MonsterAirshipCaptainProcess;
    }
    if (thingID)
    {
        if (summonActions[key] != 0)
            CallFunctionWithArg(summonActions[key], unit);
    }
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, LastUnitID);
        }
    }
    LastUnitID ++;

    return unitMover;
}

string GameMent(int num)
{
    string table[] = {
        "옵저버 피하기__ 제작: 패닉",
        "게임방법은 간단합니다, 필드에 있는 몬스터와 닿지 않고 진행방향을 따라 맵 끝으로 도달하면 다음 단계로 넘어가게 됩니다",
        "총 10단계로 구성되어 있어서 10개의 판을 모두 깨면 승리하게 됩니다",
        "시작지점에 생성되어 있는 지팡이를 이용하면 조금 더 쉽게 플레이를 하실 수 있을 겁니다",
        "시작지점 바닥에 쓰여있는 숫자는 유저들의 죽은 횟수를 나타냅니다, 이 숫자는 모든 유저의 죽음 수를 합산한 값입니다",
        "라인 중간에는 총 2곳의 안전지대가 있습니다, 안전지대에서는 몬스터와 닿아도 죽지 않으므로 잠시 쉬어갈 수 있습니다. 다만 몬스터의 공격은 여전히 피할 수 없습니다"};
    return table[num];
}

void HowToGamePlay(int num)
{
    if (num < 6)
    {
        UniPrintToAll(GameMent(num));
        FrameTimerWithArg(110, num + 1, HowToGamePlay);
    }
}

float FloatTable(int num)
{
	float arr[28], count;
	int k;

	if (num < 0)
	{
		count = 27.0;
		for (k = 27 ; k >= 0 ; k --)
		{
			arr[k] = count;
			count -= 1.0;
		}
		return ToFloat(0);
	}
	return arr[num];
}

int NumberData(int num)
{
	int data[10];

	if (!data[0])
	{
		data[0] = 110729622; data[1] = 239354980; data[2] = 252799126; data[3] = 110643350; data[4] = 143194521;
		data[5] = 110659359; data[6] = 110719382; data[7] = 71583903; data[8] = 110717334; data[9] = 110684566;
		return 0;
	}
	return data[num];
}

int NumberOrb(int wp)
{
	int ptr, k;

	if (wp)
	{
		ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1;
		for (k = 0 ; k < 28 ; k ++)
		{
			ObjectOff(CreateObject("ManaBombOrb", wp));
			MoveWaypoint(wp, GetWaypointX(wp) + 1.0, GetWaypointY(wp));
		}
		Raise(ptr - 1, ToFloat(wp));
	}
	return ptr;
}

void InitNumberDisplay()
{
    NumberData(-1);
    FloatTable(-1);
    Num_fst = NumberOrb(26);
    Num_scd = NumberOrb(27);
    Num_thd = NumberOrb(28);
    DisplayNumber(Num_fst, 6, NumberData(0));
    DisplayNumber(Num_scd, 5, NumberData(0));
    DisplayNumber(Num_thd, 4, NumberData(0));
}

void DisplayNumber(int ptr, int loc, int bytes)
{
	float pos_x = GetWaypointX(loc), pos_y = GetWaypointY(loc);
	int idx = 0, k, wp = ToInt(GetObjectZ(ptr - 1));

	for (k = 1 ; !(k & 0x10000000) ; k <<= 1)
	{
		if (bytes & k)
			MoveObject(ptr + idx, pos_x, pos_y);
		else
			MoveObject(ptr + idx, GetWaypointX(wp) + FloatTable(idx), GetWaypointY(wp));
		if (idx % 4 == 3)
		{
			pos_x = GetWaypointX(loc);
			pos_y += 2.0;
		}
		else
			pos_x += 2.0;
		Nop(idx ++);
	}
}

string UnitInfo(int num)
{
    string table[] = {
        "Swordsman", "Skeleton", "BlackWolf", "BlackBear", "AlbinoSpider",
        "OgreBrute", "MeleeDemon", "Troll", "AirshipCaptain"};
    return table[num];
}

void StartCurrentStage()
{
    if (CurStage < 10)
    {
        UniPrintToAll("현재 스테이지: " + IntToString(CurStage + 1) + " 시작");
        FrameTimer(60, StartPlacedDotges);
    }
    else
    {
        UniPrintToAll("승리: 모든 스테이지를 완료했습니다!");
        UniPrintToAll("대단합니다                       ");
    }
}

void StartPlacedDotges()
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 10) + 1;

    for (k = 0 ; k < 50 ; Nop(k ++))
    {
        PlacedDotge();
    }
    Delete(ptr - 1);
    SaveData(ptr);
    FrameTimerWithArg(1, ptr, SetDotgeUnitAiBy100);
    FrameTimerWithArg(1, ptr, SetInvincibility);
    FrameTimer(10, ResetPlaceMarker);
}

int SaveData(int args)
{
    int data;

    if (args)
        data = args;
    return data;
}

void RemoveUnits(int idx)
{
    int k, ptr = SaveData(0) + (idx * 100);

    for (k = 0 ; k < 100 ; Nop(k ++))
        Delete(ptr + k);
}

void ResetPlaceMarker()
{
    int k;
    for (k = 0 ; k < 6 ; k ++)
        MoveWaypoint(10 + k, GetWaypointX(16 + k), GetWaypointY(16 + k));
}

void SetDotgeUnitAiBy100(int ptr)
{
    int k, count, multp;

    if (count < 3)
    {
        multp = count * 100 + ptr;
        for (k = 99 ; k >= 0 ; Nop(k --))
            Enchant(multp + k, "ENCHANT_CONFUSED", 0.0);
        count ++;
        FrameTimerWithArg(1, ptr, SetDotgeUnitAiBy100);
    }
    else
        count = 0;
}

void PlacedDotge()
{
    int k, arr[6];

    for (k = 0 ; k < 6 ; k ++)
    {
        arr[k] = CreateObject(UnitInfo(CurStage), 10 + k);
        CheckMonsterThing(arr[k]);
        Enchant(arr[k], "ENCHANT_INVULNERABLE", 3.0);
        SetCallback(arr[k], 9, CollideEvent);
        MoveWaypoint(10 + k, GetWaypointX(10 + k) + 75.0, GetWaypointY(10 + k) - 75.0);
    }
}

void SetInvincibility(int ptr)
{
    int k;

    for (k = 0 ; k < 300 ; Nop(k ++))
    {
        EnchantOff(ptr + k, "ENCHANT_INVULNERABLE");
        SetUnitHealth(ptr + k, 20000);
    }
}

void CollideEvent()
{
    if (CurrentHealth(other) && IsPlayerUnit(other) && !HasEnchant(other, "ENCHANT_DETECTING"))
    {
        if (HasEnchant(other, "ENCHANT_INVULNERABLE"))
            EnchantOff(other, "ENCHANT_INVULNERABLE");
        Damage(other, self, 150, 1);
    }
}

void TakeARest()
{
    if (IsPlayerUnit(other))
    {
        MoveWaypoint(25, GetObjectX(other), GetObjectY(other));
        DeleteObjectTimer(CreateObject("MagicSpark", 25), 15);
        Enchant(other, "ENCHANT_DETECTING", 0.6);
    }
}

void PlayerDeath()
{
    PlrDead ++;
    DisplayNumber(Num_fst, 6, NumberData(PlrDead % 10));
    DisplayNumber(Num_scd, 5, NumberData((PlrDead / 10) % 10));
    DisplayNumber(Num_thd, 4, NumberData((PlrDead / 100) % 10));
}

void MapExit()
{
    MusicEvent();

    RemoveCoopTeamMode();
}

void MapInitialize()
{
    MusicEvent();
    //Init_run
    InitNumberDisplay();
    PlaceMagicStaffs();

    //delay_run
    StrGoalLoc();
    FrameTimer(10, StrGoArrow);
    FrameTimer(60, StartCurrentStage);
    SecondTimerWithArg(5, 0, HowToGamePlay);

    FrameTimer(1, MakeCoopTeam);
    //Loop_run
    FrameTimer(10, PlayerClassOnLoop);

    RegistSignMessage(Object("mapsign1"), "맵 제목: 옵저버 피하기!! 제작자: 녹스게임리마스터");
    RegistSignMessage(Object("mapsign2"), "게임방법: 앞에 보이는 몬스터는 죽일 수 없습니다! 몬스터와 닿지 않도록 잘 피해서 맨 윗지점으로 이동하세요");
}

void DrawGeometryRingAt(string orbName, float xProfile, float yProfile, int angleAdder)
{
    float speed = 2.3;
    int u;

    for (u = 0 ; u < 60 ; Nop(++u))
        LinearOrbMove(CreateObjectAt(orbName, xProfile, yProfile), MathSine((u * 6) + 90 + angleAdder, -12.0), MathSine((u * 6) + angleAdder, -12.0), speed, 10);
}

void MagicStaffHit()
{
    int owner = GetOwner(self);

    if (owner == 0)
        return;

    CastSpellObjectObject("SPELL_PUSH", owner, owner);
    PlaySoundAround(owner, 54);
    RestoreHealth(self, MaxHealth(self) - CurrentHealth(self));
    DrawGeometryRingAt("CharmOrb", GetObjectX(owner), GetObjectY(owner), 0);
}

int ImportWeaponHitFunc()
{
    int *streamptr;

    if (streamptr == NULLPTR)
    {
        int codeline[] = {0x50731068, 0x8B565000, 0x8B102474, 0x0002FC86,
            0x006A5600, 0x2454FF50, 0x0CC48314, 0xC483585E, 0x9090C304};
        streamptr = &codeline;
    }
    return streamptr;
}

int SummonMagicStaff(float xpos, float ypos)
{
    int mstaff = CreateObjectAt("StaffWooden", xpos, ypos);
    int *ptr = UnitToPtr(mstaff);

    if (ptr == NULLPTR)
        return 0;

    ptr[180] = ImportWeaponHitFunc();
    ptr[191] = MagicStaffHit;
    return mstaff;
}

void PlaceMagicStaffs()
{
    int k, baseunit = CreateObject("InvisibleLightBlueHigh", 33);

    for (k = 0 ; k < 10 ; Nop(k ++))
    {
        Enchant(SummonMagicStaff(GetObjectX(baseunit + k) + 23.0, GetObjectY(baseunit + k) + 23.0), "ENCHANT_ANCHORED", 0.0);
    }
}

void GoToNextStage()
{
    if (!IsPlayerUnit(other))
        return;

    if (CurStage < 9)
    {
        UniPrintToAll("스테이지 " + IntToString(CurStage + 1) + " 를 성공했습니다");
        CurStage ++;
        UniPrintToAll("잠시 후 스테이지 " + IntToString(CurStage + 1) + " 가 시작됩니다");
        FrameTimerWithArg(1, 0, RemoveUnits);
        FrameTimerWithArg(1, 1, RemoveUnits);
        FrameTimerWithArg(1, 2, RemoveUnits);
        TeleportAllPlayers();
        FrameTimerWithArg(10, "FlagCapture", PlayWav);
        FrameTimer(60, StartCurrentStage);
    }
    else
    {
        RemoveUnits(0);
        RemoveUnits(1);
        RemoveUnits(2);
        UniPrintToAll(PlayerIngameNick(other) + " 님께서 마지막 단계를 클리어하셨습니다");
        PlayWav("StaffOblivionAchieve1");
        ObjectOff(self);
        FrameTimer(60, VictoryEvent);
    }
}

void HarverdEffect(int unit)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 31);

    CreateObject("OblivionOrb", 31);
    Raise(ptr, ToFloat(unit));
    FrameTimerWithArg(2, ptr + 1, DelayRemoveUnit);
}

void DelayRemoveUnit(int unit)
{
    Delete(unit);
}

void DelayPickupUnit(int ptr)
{
    Pickup(ToInt(GetObjectZ(ptr)), ptr + 1);
    Delete(ptr);
}

void VictoryEvent()
{
    TeleportAllPlayersToLocation(31);
    PlayWav("LevelUp");
    Effect("WHITE_FLASH", GetWaypointX(31), GetWaypointY(31), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("LevelUp", 31), 1200);
    UniPrintToAll("승리: 모든 스테이지를 클리어 하셨습니다");
    FrameTimer(3, StrVictory);
}

void TeleportAllPlayersToLocation(int loc)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
            MoveObject(player[k], GetWaypointX(loc), GetWaypointY(loc));
    }
}

void PlayWav(string name)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (MaxHealth(player[k]))
        {
            MoveWaypoint(25, GetObjectX(player[k]), GetObjectY(player[k]));
            AudioEvent(name, 25);
        }
    }
}

void TeleportAllPlayers()
{
    int k;
    MoveWaypoint(24, GetWaypointX(23), GetWaypointY(23));
    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
        {
            Enchant(player[k], "ENCHANT_FREEZE", 2.5);
            MoveObject(player[k], GetWaypointX(24), GetWaypointY(24));
            MoveWaypoint(24, GetWaypointX(24) + 32.0, GetWaypointY(24) + 32.0);
        }
    }
}

/**
 * if (PlayerClassCheckDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
*/

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & PLAYER_DEATH_FLAG;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] ^= PLAYER_DEATH_FLAG;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    return plr;
}

void PlayerRegist()
{
    while (1)
    {
        if (CurrentHealth(other))
        {
            int plr = CheckPlayer(), k;

            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    plr = PlayerClassOnInit(k, GetCaller());
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerJoin(plr);
                break;
            }
        }
        CantJoinPlayer();
        break;
    }
}

void PlayerJoin(int plr)
{
    if (PlayerClassCheckDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    MoveObject(player[plr], GetWaypointX(3), GetWaypointY(3));
    Effect("TELEPORT", GetWaypointX(3), GetWaypointY(3), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("BlueRain", 3), 12);
    AudioEvent("BlindOff", 3);
    UniChatMessage(player[plr], "도전은 위대하다-!", 108);
}

void CantJoinPlayer()
{
    Enchant(other, "ENCHANT_FREEZE", 0.0);
    Enchant(other, "ENCHANT_BLINDED", 0.0);
    Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
    MoveObject(other, GetWaypointX(2), GetWaypointY(2));
    UniPrint(other, "죄송합니다, 이 지도는 지원되지 않습니다");
}

void PlayerClassOnDeath(int plr)
{ }

void PlayerClassOnAlive(int plr, int pUnit)
{ }

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (true)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void StrGoalLoc()
{
	int arr[17], i = 0;
	string name = "ManaBombOrb";

	arr[0] = 2116273148; arr[1] = 1140871109; arr[2] = 34095248; arr[3] = 18944257; arr[4] = 302237729; arr[5] = 809632900; arr[6] = 151650184; arr[7] = 1122568; arr[8] = 573055936; arr[9] = 1074264136; 
	arr[10] = 2208; arr[11] = 304152592; arr[12] = 71287804; arr[13] = 67651712; arr[14] = 545263617; arr[15] = 1644040225; arr[16] = 1069678655; 
	while (i < 17)
	{
		drawStrGoalLoc(arr[i], name);
		i ++;
	}
}

void drawStrGoalLoc(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(22);
		pos_y = GetWaypointY(22);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 22);
		if (count % 48 == 47)
			MoveWaypoint(22, GetWaypointX(22) - 94.000000, GetWaypointY(22) + 2.000000);
		else
			MoveWaypoint(22, GetWaypointX(22) + 2.000000, GetWaypointY(22));
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(22, pos_x, pos_y);
	}
}

void StrGoArrow()
{
	int arr[21], i = 0;
	string name = "ManaBombOrb";

	arr[0] = 1109967614; arr[1] = 17284; arr[2] = 277479556; arr[3] = 65040385; arr[4] = 669171489; arr[5] = 1354761470; arr[6] = 956883720; arr[7] = 606601482; arr[8] = 574761250; arr[9] = 1351156164; 
	arr[10] = 144673352; arr[11] = 269623313; arr[12] = 469762700; arr[13] = 67109892; arr[14] = 516096; arr[15] = 1073873024; arr[16] = 528614431; arr[17] = 134250512; arr[18] = 135299336; arr[19] = 2082463746; 
	arr[20] = 33038209; 
	while(i < 21)
	{
		drawStrGoArrow(arr[i], name);
		i ++;
	}
}

void drawStrGoArrow(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(29);
		pos_y = GetWaypointY(29);
	}
	for (i = 1 ; i > 0 && count < 651 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 29);
		if (count % 60 == 59)
			MoveWaypoint(29, GetWaypointX(29) - 118.000000, GetWaypointY(29) + 2.000000);
		else
			MoveWaypoint(29, GetWaypointX(29) + 2.000000, GetWaypointY(29));
		count ++;
	}
	if (count >= 651)
	{
		count = 0;
		MoveWaypoint(29, pos_x, pos_y);
	}
}

void StrVictory()
{
	int arr[13], i = 0;
	string name = "CharmOrb";
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
	{
		drawStrVictory(arr[i], name);
		i ++;
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count, i;
	float pos_x,  pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(32);
		pos_y = GetWaypointY(32);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 32);
		if (count % 38 == 37)
			MoveWaypoint(32, GetWaypointX(32) - 74.000000, GetWaypointY(32) + 2.000000);
		else
			MoveWaypoint(32, GetWaypointX(32) + 2.000000, GetWaypointY(32));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(32, pos_x, pos_y);
	}
}
