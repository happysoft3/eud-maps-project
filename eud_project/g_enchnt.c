
#include "libs/define.h"
#include "libs/printutil.h"
#include "libs/waypoint.h"
#include "libs/mathlab.h"
#include "libs/unitstruct.h"
#include "libs/itemproperty.h"
#include "libs/buff.h"
#include "libs/opcodehelper.h"
#include "libs/hash.h"
#include "libs/sound_define.h"

int m_COR_PASS;
string *m_pMapFileName;
string *m_pMapDesc;
int *m_pMapInfoHash;
int *m_pMapLoadHash;

static void deferredHorrenGen(int unit)
{
    UniPrintToAll(IntToString(CurrentHealth(unit)));
    UniPrintToAll(IntToString(MaxHealth(unit)));
}

static void OnHorrenGen()
{
    FrameTimerWithArg(1, GetCaller(), deferredHorrenGen);
    UniPrintToAll(IntToString(CurrentHealth(OTHER)));
    UniPrintToAll(IntToString(MaxHealth(OTHER)));
}

int GetRawCodeCreateObjectSpec()
{
    int *pCodeStream;

    if (!pCodeStream)
    {
        int codes[]= {0xDB624BE8, 0x45E850FF, 0x50FFD924, 0xDB621FE8, 0x315858FF, 0x9090C3C0};

        FixCallOpcode(&codes, 0x507250);
        FixCallOpcode(&codes+6, 0x4e3450);
        FixCallOpcode(&codes+0x0c, 0x507230);
        pCodeStream = &codes;
    }
    return pCodeStream;
}

void AllocUnitSpec(int thingId, int *pDestSpec)
{
    int *builtins = 0x5c308c;
    int *pOld = builtins[184];

    builtins[184] = GetRawCodeCreateObjectSpec();
    int allocSpec = Unknownb8(thingId);
    builtins[184] = pOld;
    if (pDestSpec)
        pDestSpec[0] = allocSpec;
}

void LoadmapImpl(int *pDestCode)
{
    int *pCode;

    if (!pCode)
    {
        //E8 4B 62 DB /FF 50 E8 45/ 14 D8 FF 58/ 31 C0 C3 90
        int codes[] = {0xdb624be8, 0x45e850ff, 0x58ffd814, 0x90c3c031};

        FixCallOpcode(&codes, 0x507250);
        FixCallOpcode(&codes + 6, 0x4d2450);
        pCode = &codes;
    }
    pDestCode[0] = pCode;
}

void Loadmap(string mapFileName)
{
    int *builtins=0x5c308c;
    int *pOld = builtins[94];
    int *pCode;

    LoadmapImpl(&pCode);
    builtins[94] = pCode;
    Unused5e(StringUtilGetScriptStringPtr(mapFileName));
    builtins[94] = pOld;
}

void PutDead()
{
    int unit = CreateObject("WeirdlingBeast", 47);
    SetCallback(unit, 3, CollideTest);
    Damage(unit, 0, 9999, 14);
    FrameTimerWithArg(1, unit, FreezUnit);
}

void FreezUnit(int unit)
{
    DeleteObjectTimer(unit, -1);
    Frozen(unit, TRUE);
}

int OrbStaffCreate(int location)
{
    int staff = CreateObjectAt("OblivionOrb", LocationX(location), LocationY(location));
    int *ptr = UnitToPtr(staff);

    if (ptr)
    {
        if (!(ptr[2]&40))
            ptr[2]^=0x40;
        DisableOblivionItemPickupEvent(staff);
        SetItemPropertyAllowAllDrop(staff);
    }
    return staff;
}

int ImportPlayerEnableObserverMode()
{
    //E8 4B 62 DB/ FF 6A 00 6A/ 00 50 E8 51/ 58 D9 FF 83/ C4 0C C3 90
    int code[] = {0xdb624be8, 0x6a006aff, 0x51e85000, 0x83ffd958, 0x90c30cc4};
    int *ptr = &code;

    FixCallOpcode(ptr, 0x507250);
    FixCallOpcode(ptr + 0xa, 0x4e6860);
    return ptr;
}

void PlayerGoObserverMode(int plrUnit)
{
    if (!IsPlayerUnit(plrUnit))
        return;

    int ptr = UnitToPtr(plrUnit);
    int pst = GetMemory(GetMemory(ptr + 0x2ec) + 0x114);
    int temp = GetMemory(0x5c3108);

    SetMemory(0x5c3108, ImportPlayerEnableObserverMode());
    Unused1f(pst);
    SetMemory(0x5c3108, temp);
}

void StaffC()
{
    CreateSpellBook(31, GetObjectX(OTHER), GetObjectY(OTHER));
}

void CollideTest()
{
    if (CurrentHealth(OTHER))
    {
        UniChatMessage(SELF, "회수처리됨", 120);
        Delete(SELF);
    }
}

void ShowMapDescription()
{
    int result = -1;

    if (HashGet(m_pMapInfoHash, GetTrigger(), &result, FALSE))
    {
        UniPrint(OTHER, m_pMapDesc[result] + " 맵으로 이동하려면 철창 너머에 있는 레바를 조작하세요");
    }
}

void LoadThatmap()
{
    ObjectOff(SELF);

    int res;

    if (HashGet(m_pMapLoadHash, GetTrigger(), &res, FALSE))
    {
        UniPrintToAll(m_pMapDesc[res] + " 지도를 로드하고 있어요!");
        Loadmap(m_pMapFileName[res]);
    }
}

void InitMapInfo()
{
    string fn[] = {"g_forest.map", "g_lava.map", "g_lotdd.map", "g_swamp.map", 
        "g_castld.map", "g_crypts.map", "g_mines.map", "g_templd.map"};
    string desc[] = {"어둠의 숲", "덤불", "죽은자의 땅", "황량함의 늪지대",
        "마녀의 성", "지하실", "광산", "익스사원"};

    m_pMapFileName=&fn;
    m_pMapDesc=&desc;

    HashCreateInstance(&m_pMapInfoHash);
    HashPushback(m_pMapInfoHash, Object("scforest"), 0);
    HashPushback(m_pMapInfoHash, Object("sclava"), 1);
    HashPushback(m_pMapInfoHash, Object("scice"), 2);
    HashPushback(m_pMapInfoHash, Object("scswamp"), 3);
    HashPushback(m_pMapInfoHash, Object("scgalav"), 4);
    HashPushback(m_pMapInfoHash, Object("sctomb"), 5);
    HashPushback(m_pMapInfoHash, Object("sccave"), 6);
    HashPushback(m_pMapInfoHash, Object("sctemple"), 7);

    HashCreateInstance(&m_pMapLoadHash);
    HashPushback(m_pMapLoadHash, Object("FLRoomFor"), 0);
    HashPushback(m_pMapLoadHash, Object("FLRoomLav"), 1);
    HashPushback(m_pMapLoadHash, Object("FLRoomLOTD"), 2);
    HashPushback(m_pMapLoadHash, Object("FLRoomSwamp"), 3);
    HashPushback(m_pMapLoadHash, Object("FLRoomCas"), 4);
    HashPushback(m_pMapLoadHash, Object("FLRoom6"), 5);
    HashPushback(m_pMapLoadHash, Object("FLRoom7"), 6);
    HashPushback(m_pMapLoadHash, Object("FLRoomIx"), 7);
}

void DelayInit()
{
    InitMapInfo();
    int r=Object("WishdomStuff");

    if (IsObjectOn(r))
    {
        Enchant(r, EnchantList(ENCHANT_VAMPIRISM), 0.0);
        MoveObject(r, LocationX(88), LocationY(88));
    }
}

void MapInitialize()
{
    MusicEvent();
    initCallee();
    SpawnGoldkey(10);
    RewardPotion(-1);
    SpawnRewardPotion(17);
    SpawnRewardPotion(17);
    SpawnRewardPotion(17);
    SpawnRewardPotion(17);
    SpawnRewardPotion(17);
    InitMapLighting();
    m_COR_PASS = 6849;
    FrameTimer(1, strPowerWeapon);
    FrameTimer(1, PutDead);
    FrameTimer(2, strLibrary);
    FrameTimer(3, strShop);
    FrameTimer(4, strPickAnkh);
    FrameTimer(5, PutAnkh);
    FrameTimerWithArg(10, 16, PutKeys);
    FrameTimerWithArg(10, 7, PutPotions);
    FrameTimerWithArg(10, 11, PutPotions);
    FrameTimerWithArg(11, 14, PutPotions);
    FrameTimer(20, PutSpecialPotion);
    FrameTimer(1, DelayInit);
}

void InitMapLighting()
{
    int k;
    for (k = 20 ; k <= 43 ; Nop(k ++))
        CreateObject("InvisibleLightBlueHigh", k);
}

void PutSpecialPotion()
{
    int k;
    for (k = 44 ; k <= 86 ; Nop(k ++))
        SpawnRewardPotion(k);

    ProduceTestGenerator(89);
}

void SpawnGoldkey(int wp)
{
    int k;
    for (k = 0 ; k < 3 ; k ++)
        CreateObject("GoldKey", wp);
}

void strPowerWeapon()
{
	int arr[27];
	string name = "DrainManaOrb";
	int i = 0;
	arr[0] = 69722878; arr[1] = 1107296388; arr[2] = 8391160; arr[3] = 2227730; arr[4] = 1107828992; arr[5] = 1082425412; arr[6] = 257953776; arr[7] = 537993700; arr[8] = 16322; arr[9] = 1212351568; 
	arr[10] = 536384008; arr[11] = 555877375; arr[12] = 613295138; arr[13] = 1073741952; arr[14] = 151586816; arr[15] = 33428033; arr[16] = 4177920; arr[17] = 72369280; arr[18] = 516224; arr[19] = 287374352; 
	arr[20] = 71287304; arr[21] = 1082652676; arr[22] = 280988664; arr[23] = 33620480; arr[24] = 33562688; arr[25] = 1057486912; arr[26] = 132185856; 
	while(i < 27)
	{
		drawstrPowerWeapon(arr[i], name);
		i ++;
	}
}

void drawstrPowerWeapon(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(1);
		pos_y = GetWaypointY(1);
	}
	for (i = 1 ; i > 0 && count < 837 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 76 == 75)
			MoveWaypoint(1, GetWaypointX(1) - 150.000000, GetWaypointY(1) + 2.000000);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.000000, GetWaypointY(1));
		count ++;
	}
	if (count >= 837)
	{
		count = 0;
		MoveWaypoint(1, pos_x, pos_y);
	}
}

void strLibrary()
{
	int arr[14];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 2116092924; arr[1] = 138412292; arr[2] = 268452128; arr[3] = 1067268; arr[4] = 68239624; arr[5] = 470839808; arr[6] = 2131837438; arr[7] = 72353794; arr[8] = 570556544; arr[9] = 8396802; 
	arr[10] = 2130710673; arr[11] = 272423; arr[12] = 2131230720; arr[13] = 1; 
	while(i < 14)
	{
		drawstrLibrary(arr[i], name);
		i ++;
	}
}

void drawstrLibrary(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(2);
		pos_y = GetWaypointY(2);
	}
	for (i = 1 ; i > 0 && count < 434 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 2);
		if (count % 37 == 36)
			MoveWaypoint(2, GetWaypointX(2) - 72.000000, GetWaypointY(2) + 2.000000);
		else
			MoveWaypoint(2, GetWaypointX(2) + 2.000000, GetWaypointY(2));
		count ++;
	}
	if (count >= 434)
	{
		count = 0;
		MoveWaypoint(2, pos_x, pos_y);
	}
}

void strShop()
{
	int arr[27];
	string name = "HealOrb";
	int i = 0;
	arr[0] = 505659964; arr[1] = 1191486469; arr[2] = 143690232; arr[3] = 687948050; arr[4] = 570957826; arr[5] = 1573160008; arr[6] = 200865664; arr[7] = 554832356; arr[8] = 622864418; arr[9] = 71500800; 
	arr[10] = 168333449; arr[11] = 555487888; arr[12] = 607011362; arr[13] = 79431036; arr[14] = 277416849; arr[15] = 4625; arr[16] = 2375680; arr[17] = 75778112; arr[18] = 1627324671; arr[19] = 303169041; 
	arr[20] = 2114114; arr[21] = 1082400832; arr[22] = 277809392; arr[23] = 2131755024; arr[24] = 33562690; arr[25] = 522304; arr[26] = 267419656; 
	while(i < 27)
	{
		drawstrShop(arr[i], name);
		i ++;
	}
}

void drawstrShop(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(3);
		pos_y = GetWaypointY(3);
	}
	for (i = 1 ; i > 0 && count < 837 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 3);
		if (count % 76 == 75)
			MoveWaypoint(3, GetWaypointX(3) - 150.000000, GetWaypointY(3) + 2.000000);
		else
			MoveWaypoint(3, GetWaypointX(3) + 2.000000, GetWaypointY(3));
		count ++;
	}
	if (count >= 837)
	{
		count = 0;
		MoveWaypoint(3, pos_x, pos_y);
	}
}

void strPickAnkh()
{
	int arr[23];
	string name = "HealOrb";
	int i = 0;
	arr[0] = 1614791244; arr[1] = 2139097167; arr[2] = 8390984; arr[3] = 8481; arr[4] = 33570082; arr[5] = 1082099588; arr[6] = 134255752; arr[7] = 4624; arr[8] = 1069698066; arr[9] = 20416; 
	arr[10] = 589832; arr[11] = 1072758785; arr[12] = 2099198; arr[13] = 4; arr[14] = 8257536; arr[15] = 2088968; arr[16] = 33817600; arr[17] = 138412032; arr[18] = 135270400; arr[19] = 553649151; 
	arr[20] = 528592896; arr[21] = 67108864; arr[22] = 1585407; 
	while(i < 23)
	{
		drawstrPickAnkh(arr[i], name);
		i ++;
	}
}

void drawstrPickAnkh(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(6);
		pos_y = GetWaypointY(6);
	}
	for (i = 1 ; i > 0 && count < 713 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 6);
		if (count % 64 == 63)
			MoveWaypoint(6, GetWaypointX(6) - 126.000000, GetWaypointY(6) + 2.000000);
		else
			MoveWaypoint(6, GetWaypointX(6) + 2.000000, GetWaypointY(6));
		count ++;
	}
	if (count >= 713)
	{
		count = 0;
		MoveWaypoint(6, pos_x, pos_y);
	}
}

void PutAnkh()
{
    int k;
    int ptr;
    string magic = "ENCHANT_FREEZE";

    ptr = CreateObject("InvisibleLightBlueHigh", 1);
    for (k = 1 ; k <= 9 ; k ++)
    {
        MoveWaypoint(4, GetWaypointX(4) + 23.0, GetWaypointY(4) + 23.0);
        CreateObject("Ankh", 4);
        Enchant(ptr + k, magic, 0.0);
    }
}

void PutKeys(int max)
{
    int k;
    string name = "SilverKey";

    for (k = 0 ; k < max ; k ++)
    {
        CreateObject(name, 5);
    }
}

void PutPotions(int wp)
{
    int k;

    for (k = 0 ; k < 12 ; k ++)
    {
        CreateObject("RedPotion", wp);
        CreateObject("BluePotion", wp + 1);
        CreateObject("CurePoisonPotion", wp + 2);
        MoveWaypoint(wp, GetWaypointX(wp) + 20.0, GetWaypointY(wp) + 20.0);
        MoveWaypoint(wp+1, GetWaypointX(wp+1) + 20.0, GetWaypointY(wp + 1) + 20.0);
        MoveWaypoint(wp+2, GetWaypointX(wp+2) + 20.0, GetWaypointY(wp + 2) + 20.0);
    }
}

void SetInvincibleItem(int owner)
{
    int inv = GetLastItem(owner), count=0;

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
}

void WandEnchant()
{
    SetInvincibleItem(OTHER);
}

void WeaponEnchant()
{
    SetInvincibleItem(OTHER);
}

void ArmorEnchant()
{
    SetInvincibleItem(OTHER);
}

void ManagerRoomWalls()
{
    WallOpen(Wall(19, 153));
    WallOpen(Wall(20, 152));
    WallOpen(Wall(21, 151));
    WallOpen(Wall(22, 150));
    WallOpen(Wall(23, 149));
    WallOpen(Wall(24, 148));
    WallOpen(Wall(25, 149));
    WallOpen(Wall(26, 150));
    WallOpen(Wall(27, 151));
    WallOpen(Wall(28, 152));
    WallOpen(Wall(29, 153));
    WallOpen(Wall(30, 154));
    WallOpen(Wall(31, 155));
    WallOpen(Wall(32, 156));
    WallOpen(Wall(33, 157));
    WallOpen(Wall(34, 158));
    WallOpen(Wall(35, 159));
}

void initCallee()
{
    int ptr = Object("PassSwitchPtr");
    int k;

    for (k = 0 ; k < 4 ; k ++)
        LookWithAngle(ptr + (k * 2), k);
}

void CallPassword()
{
    int pass;
    int arr[4];
    int idx = GetDirection(SELF);

    if (!pass)
    {
        arr[idx] = (arr[idx] + 1) % 10;
        pass = arr[0] + (arr[1] * 10) + (arr[2] * 100) + (arr[3] * 1000);
        UniChatMessage(OTHER, "입력하신 비밀번호: " + IntToString(pass), 120);
        if (pass == m_COR_PASS)
        {
            ManagerRoomWalls();
            UniPrint(OTHER, "비밀번호가 일치합니다.");
        }
        else
            pass = 0;
    }
}

string RewardPotion(int num)
{
    string name[14];

    if (num < 0)
    {
        name[0] = "RedPotion";
        name[1] = "BluePotion";
        name[2] = "CurePoisonPotion";
        name[3] = "VampirismPotion";
        name[4] = "InfravisionPotion";
        name[5] = "ShockProtectPotion";
        name[6] = "FireProtectPotion";
        name[7] = "ShieldPotion";
        name[8] = "InvulnerabilityPotion";
        name[9] = "HastePotion";
        name[10] = "Ruby";
        name[11] = "Emerald";
        name[12] = "Diamond";
        name[13] = "InvisibilityPotion";
        return "NULL";
    }
    return name[num];
}

void SpawnRewardPotion(int wp)
{
    int angle = Random(0, 360);

    CreateObjectAt(RewardPotion(Random(0, 13)), LocationX(wp) + MathSine(angle, RandomFloat(0.0, 48.0)), LocationY(wp) + MathSine(angle, RandomFloat(0.0, 48.0)));
}

void SummonCreature()
{
    if (HasSubclass(OTHER, "WARCRY_STUN"))
    {
        SetCallback(OTHER, 3, ShotWeapon);
        SetCallback(OTHER, 7, HurtCreature);
    }
}

void ShotDeathRay()
{
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
}

void ShotWeapon()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
        {
            int mis = CreateObjectAt("PitifulFireball", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 20.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 20.0));
            SetOwner(SELF, mis);
            PushObject(mis, -18.0, GetObjectX(OTHER), GetObjectY(OTHER));
            Enchant(SELF, "ENCHANT_VILLAIN", 1.0);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.25);
        AggressionLevel(SELF, 1.0);
    }
}

void HurtCreature()
{
    if (MaxHealth(SELF) - CurrentHealth(SELF) >= 225)
    {
        Damage(SELF, OTHER, 9999, 14);
    }
}

void CreateFieldGuide(string mobName, float xpos, float ypos, int *pDest)
{
    int guide = CreateObjectAt("FieldGuide", xpos, ypos);
    int *ptr=UnitToPtr(guide);

    if (pDest)
        pDest[0] = guide;

    int *detail = ptr[184];
    int *strPt = StringUtilGetScriptStringPtr(mobName);

    while (strPt[0]&0xff)
    {
        detail[0] = (strPt[0]&0xff)|(detail[0]&(~0xff));
        detail+=1;
        strPt+=1;
    }
}

void CreateSpellBook(int spellId, float xpos, float ypos)
{
    int book=CreateObjectAt("SpellBook", xpos, ypos);
    int *ptr=UnitToPtr(book);

    int *detail = ptr[184];

    detail[0] = spellId&0xff;
}

void CreateWarriorBook(int abilityId, float xpos, float ypos)
{
    int book=CreateObjectAt("AbilityBook", xpos, ypos);
    int *ptr=UnitToPtr(book);

    int *detail=ptr[184];
    detail[0] = abilityId%6;
}

void ProduceMonsterGenerator(float xpos, float ypos, int unitSlot1, int unitSlot2, int unitSlot3, int *pDestGen)
{
    int gen = CreateObjectAt("MonsterGenerator", xpos, ypos);
    int *ptr = UnitToPtr(gen);

    if (pDestGen)
        pDestGen[0] = gen;
    int *uec = ptr[187];

    int *slotPtr;
    AllocUnitSpec(unitSlot1, &slotPtr);
    if (slotPtr[4]&UNIT_FLAG_RESPAWN)
        slotPtr[4]^=UNIT_FLAG_RESPAWN;
    uec[0] = slotPtr;
    AllocUnitSpec(unitSlot2, &slotPtr);
    uec[4] = slotPtr;
    AllocUnitSpec(unitSlot3, &slotPtr);
    uec[8] = slotPtr;
    uec[23] = 1;
}

void ModificationMonsterGeneratorSummonInfo(int mobgen, int delayLv, int capaLv)
{
    int *ptr = UnitToPtr(mobgen);
    int *uec = ptr[187];

    delayLv &= 7;
    capaLv &= 7;

    uec[20] = delayLv|(delayLv<<8)|(delayLv<<16)|(capaLv<<24);
    uec[21] = capaLv|(capaLv<<8);
}

void SetMonsterGeneratorCallbackOnProduce(int mobgen, int functionId)
{
    int *ptr=UnitToPtr(mobgen);
    int *uec=ptr[187];

    uec[17] = functionId;
}

void GenOnProduce()
{
    UniPrintToAll("top: gen, bottom: product");
    UniPrintToAll(IntToString(UnitToPtr(SELF)));
    UniPrintToAll(IntToString(UnitToPtr(OTHER)));
}

void ProduceTestGenerator(int locationId)
{
    int mobgen;

    ProduceMonsterGenerator(LocationX(locationId), LocationY(locationId), 1346, 1338, 1315, &mobgen);
    ModificationMonsterGeneratorSummonInfo(mobgen, 3, 3);
    SetMonsterGeneratorCallbackOnProduce(mobgen, GenOnProduce);
}

void ScrTestGoToMap()
{
    ObjectOff(SELF);
    Loadmap("estate.map");
}

#define DOOR_LOCKSTYLE_SILVER 0x100
#define DOOR_LOCKSTYLE_GOLD 0x200
#define DOOR_LOCKSTYLE_RUBY 0x300
#define DOOR_LOCKSTYLE_SAPPHIRE 0x400
#define DOOR_LOCKSTYLE_MECA 0x500

int GetLockStyle(int door)
{
    int ptr=UnitToPtr(door);
    return GetMemory(GetMemory(ptr+0x2ec));
}

// void LockDoorEx(int door, int lockStyle)
// {
//     int ptr=UnitToPtr(door);
//     SetMemory(GetMemory(ptr+0x2ec), lockStyle);
// }

void UpdateLockDoor(int door)
{
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x50, 0xB8, 0x90, 0x83, 0x4E, 0x00, 0xFF, 0xD0, 0xA1, 0x04, 0xEA, 0x84, 0x00, 0x50, 0xB8, 0xE0, 0x71, 0x4D, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3
    };
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=code;
    Unused1f(UnitToPtr(door));
    pBuiltins[31]=pOld;
}

void LockDoorEx(int door, int lockStyle, short soundId)
{
    char *pcode;

    if (!pcode)
    {
        char scode[]={
            0x56, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0xF0, 0x6A, 0x00, 0x6A, 0x00, 0xFF, 0x36, 
            0xFF, 0x76, 0x04, 0xB8, 0x60, 0x19, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0x5E, 0xC3
        };
        pcode=scode;
    }
    UpdateLockDoor(door);
    int *ptr = UnitToPtr(door);
    int args[]={ptr, soundId};
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    short *ec = ptr[187];
    ec[0]=lockStyle;
    pBuiltins[31]=pcode;
    Unused1f(args);
    pBuiltins[31]=pOld;
}

void UnlockLeftDoor()
{
    ObjectOff(SELF);
    LockDoorEx(Object("leftgate1"),0,SOUND_Unlock);
    LockDoorEx(Object("leftgate2"),0,SOUND_Unlock);
}
