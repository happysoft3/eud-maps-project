
#include "p0110011_main.h"

int m_player[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex]; }

static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user; }

int m_pFlags[MAX_PLAYER_COUNT];

static int GetPlayerFlags(int pIndex) //virtual
{ return m_pFlags[pIndex]; }

static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_pFlags[pIndex]=flags; }

int *m_pClientKeyState;
static int GetClientKeyInput(int pIndex)//virtual
{return m_pClientKeyState[pIndex];}
static void SetClientKeyInput(int pIndex,int input)//virtual
{m_pClientKeyState[pIndex]=input;}
static void GetClientKeyInputPtr(int *pp)//virtual
{pp[0]=m_pClientKeyState;}

void MapInitialize()
{
    MusicEvent();
    m_pClientKeyState=0x751000;
    OnInitializeMap();
}

void MapExit()
{
    MusicEvent();
    OnShutdownMap();
}
int m_userCreatureUnit[MAX_PLAYER_COUNT];
static int GetAvata(int pIndex)//virtual
{return m_userCreatureUnit[pIndex];}
static void SetAvata(int pIndex, int cre)//virtual
{m_userCreatureUnit[pIndex]=cre;}

int m_respMark[MAX_PLAYER_COUNT];
static int GetUserRespawnMark(int pIndex) //virtual
{ return m_respMark[pIndex]; }
static void SetUserRespawnMark(int pIndex, int mark) //virtual
{ m_respMark[pIndex]=mark; }

int m_clientId;

static void NetworkUtilClientMain()
{
    m_clientId=GetMemory(0x979720);
    OnInitializeClient(m_clientId);
}
static int NetworkUtilClientTimerEnabler(){return TRUE;}

static int GenericHash() //virtual
{
    int hash;

    if (!hash)
        HashCreateInstance(&hash);
    return hash;
}

short m_creatureHpInfo[128];

static void GetCreatureHpInfoMessage(short **p) //virtual
{
    int *dest=p;

    dest[0]=m_creatureHpInfo;
}

short m_creatureLvExpInfo[256];

static void GetCreatureLevelExpInfoMessage(short **p) //virtual
{
    int *dest=p;
    dest[0]=m_creatureLvExpInfo;
}

short m_creatureHpPrev[MAX_PLAYER_COUNT];

static int GetCreatureHpPrev(int pIndex) //virtual
{ return m_creatureHpPrev[pIndex]; }

static void SetCreatureHpPrev(int pIndex,int value) //virtual
{m_creatureHpPrev[pIndex]=value;}

char m_creatureLevel[MAX_PLAYER_COUNT];
static int GetCreatureLevel(int pIndex) //virtual
{ return m_creatureLevel[pIndex];}
static void SetCreatureLevel(int pIndex, int setTo) //virtual
{m_creatureLevel[pIndex]=setTo; }

int m_creatureExp[MAX_PLAYER_COUNT];
static int GetCreatureExp(int pIndex) //virtual
{return m_creatureExp[pIndex]; }
static void SetCreatureExp(int pIndex, int setTo) //virtual
{m_creatureExp[pIndex]=setTo; }
int m_userSkillFn1[MAX_PLAYER_COUNT];
int m_userSkillFn2[MAX_PLAYER_COUNT];
int m_userSkillFn3[MAX_PLAYER_COUNT];
static int GetUserSkill1(int pIndex)//virtual
{return m_userSkillFn1[pIndex];}
static int GetUserSkill2(int pIndex)//virtual
{return m_userSkillFn2[pIndex];}
static int GetUserSkill3(int pIndex)//virtual
{return m_userSkillFn3[pIndex];}
static void SetUserSkill1(int pIndex, int fn)//virtual
{ m_userSkillFn1[pIndex]=fn;}
static void SetUserSkill2(int pIndex, int fn)//virtual
{ m_userSkillFn2[pIndex]=fn;}
static void SetUserSkill3(int pIndex, int fn)//virtual
{ m_userSkillFn3[pIndex]=fn;}
int m_userCool1[MAX_PLAYER_COUNT];
int m_userCool2[MAX_PLAYER_COUNT];
int m_userCool3[MAX_PLAYER_COUNT];
static int GetUserCooldown1(int pIndex)//virtual
{return m_userCool1[pIndex];}
static int GetUserCooldown2(int pIndex)//virtual
{return m_userCool2[pIndex];}
static int GetUserCooldown3(int pIndex)//virtual
{return m_userCool3[pIndex];}
static void SetUserCooldown1(int pIndex, int cool)//virtual
{ m_userCool1[pIndex]=cool;}
static void SetUserCooldown2(int pIndex, int cool)//virtual
{ m_userCool2[pIndex]=cool;}
static void SetUserCooldown3(int pIndex, int cool)//virtual
{ m_userCool3[pIndex]=cool;}

short m_skillMessage1[256];
short m_skillMessage2[256];
short m_skillMessage3[256];

static void GetSkillMessage(short **p, int n) //virtual
{
    int tb[]={m_skillMessage1,m_skillMessage2,m_skillMessage3,};
    int *dest=p;
    dest[0]=tb[n&3];
}

short m_damageUpgradeMsg[128];
short m_hpUpgradeMsg[128];
short m_goldToExpMsg[128];
short m_speedUpgradeMsg[128];
short m_goldToExpX5Msg[128];
short m_hpCure[128];

static void GetBeaconGuideMessage(short **p, int n)//virtual
{
    int tb[]={m_damageUpgradeMsg,m_hpUpgradeMsg,m_goldToExpMsg,m_speedUpgradeMsg,m_goldToExpX5Msg,m_hpCure,};
    int *dest=p;
    
    dest[0]=tb[n];
}
char m_damUpgradeLv[MAX_PLAYER_COUNT];
static int GetDamageUpgradeLevel(int pIndex) //virtual
{return m_damUpgradeLv[pIndex]; }
static void SetDamageUpgradeLevel(int pIndex,int setTo)//virtual
{ m_damUpgradeLv[pIndex]=setTo; }

char m_hpUpgradeLv[MAX_PLAYER_COUNT];
static int GetHpUpgradeLevel(int pIndex)//virtual
{ return m_hpUpgradeLv[pIndex]; }
static void SetHpUpgradeLevel(int pIndex,int setTo)//virtual
{ m_hpUpgradeLv[pIndex]=setTo;}
short m_defaultDamage[MAX_PLAYER_COUNT];
short m_defaultHp[MAX_PLAYER_COUNT];
static int GetDefaultStat(int pIndex, int ty)//virtual
{
    if (!ty)
        return m_defaultDamage[pIndex];
    return m_defaultHp[pIndex];
}
static void SetDefaultStat(int pIndex, int ty, int value)//virtual
{
    if (!ty)
    {
        m_defaultDamage[pIndex]=value;
        return;
    }
    m_defaultHp[pIndex]=value;
}
float m_creatureSpeed[MAX_PLAYER_COUNT];

static float GetCreatureSpeed(int pIndex)//virtual
{return m_creatureSpeed[pIndex]; }
static void SetCreatureSpeed(int pIndex,float speed)//virtual
{ m_creatureSpeed[pIndex]=speed; }
char m_speedUpLevel[MAX_PLAYER_COUNT];
static int GetSpeedUpgradeLevel(int pIndex)//virtual
{ return m_speedUpLevel[pIndex]; }
static void SetSpeedUpgradeLevel(int pIndex,int setTo)//virtual
{ m_speedUpLevel[pIndex]=setTo; }
char m_userCharacterId[MAX_PLAYER_COUNT];
static int GetUserCharacterId(int pIndex) //virtual
{return m_userCharacterId[pIndex]; }
static void SetUserCharacterId(int pIndex, int id) //virtual
{ m_userCharacterId[pIndex]=id;}
int m_charSpawnFn[MAX_PLAYER_COUNT];
static int GetCharacterSpawnFn(int pIndex)//virtual
{ return m_charSpawnFn[pIndex]; }
static void SetCharacterSpawnFn(int pIndex, int fn)//virtual
{ m_charSpawnFn[pIndex]=fn;}
int m_charAttackFn[MAX_PLAYER_COUNT];
static int GetCharacterDefaultAttack(int pIndex)//virtual
{ return m_charAttackFn[pIndex]; }
static void SetCharacterDefaultAttack(int pIndex, int fn)//virtual
{m_charAttackFn[pIndex]= fn;}

