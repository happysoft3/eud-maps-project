
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"

#define IMAGE_VECTOR_SIZE 256

void MapImageResourcePool(){ }
void ImageGirlPortrait(){ }
void ImageHorrendousPicNew(){ }
void ImageArcherPicNew(){ }
void ImageHovathPicNew(){ }
void ImageHecubahPicNew(){ }
void ImageMaidenPicNew(){ }
void imageCustomSlot(){ }
void GRPDumpRoachOniOutput(){}

#define ROACH_ONI_START_IMAGE_ID 130133

void initializeImageFrameRoachOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRoachOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, ROACH_ONI_START_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, ROACH_ONI_START_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, ROACH_ONI_START_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
}

void GRPDump_HosungOniOutput(){}

#define LEE_HO_SUNG_ONI_BASE_IMAGE_ID 129717
void initializeImageFrameHosungOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_HosungOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, LEE_HO_SUNG_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, LEE_HO_SUNG_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, LEE_HO_SUNG_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_FemaleBeachGirl(){}

#define BEACH_GRIL_START_IMAGE_AT 122915
void initializeImageFrameBeachGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_FemaleBeachGirl)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameAssign8Directions(0,  BEACH_GRIL_START_IMAGE_AT, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  BEACH_GRIL_START_IMAGE_AT+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10, BEACH_GRIL_START_IMAGE_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15, BEACH_GRIL_START_IMAGE_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20, BEACH_GRIL_START_IMAGE_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(      BEACH_GRIL_START_IMAGE_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(      BEACH_GRIL_START_IMAGE_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(      BEACH_GRIL_START_IMAGE_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(      BEACH_GRIL_START_IMAGE_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25, BEACH_GRIL_START_IMAGE_AT+40,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void GRPDump_ZergLurkerOutput(){}

#define ZERG_LURKER_IMAGE_START_AT 126257

void initializeImageFrameZergLurker(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SHOPKEEPER_WIZARD_REALM;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_ZergLurkerOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,4,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  ZERG_LURKER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  ZERG_LURKER_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10, ZERG_LURKER_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15, ZERG_LURKER_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20, ZERG_LURKER_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(25, ZERG_LURKER_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(30, ZERG_LURKER_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN,4);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN,5);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,2);
    AnimFrameCopy8Directions(      ZERG_LURKER_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,3);
}

void introduceCustomImage(int *pImgVector)
{
    if (LoadImageResource(GetScrCodeField(MapImageResourcePool)+4))
    {
        int num = 0;
        
        initializeImageFrameRoachOni(pImgVector);
        initializeImageFrameHosungOni(pImgVector);
        initializeImageFrameBeachGirl(pImgVector);
        initializeImageHealthBar(pImgVector);
        initializeImageFrameZergLurker(pImgVector);
        AppendImageFrame(pImgVector, GetGrpStream(num++), 17600);
        AppendImageFrame(pImgVector, GetGrpStream(num++), 132291);
        AppendImageFrame(pImgVector, GetGrpStream(num++), 132292);
        AppendImageFrame(pImgVector, GetGrpStream(num++), 17828);
        AppendImageFrame(pImgVector, GetGrpStream(num++), 17866);
        AppendImageFrame(pImgVector, GetScrCodeField(ImageHovathPicNew) + 4, 14974);
        AppendImageFrame(pImgVector, GetScrCodeField(ImageHecubahPicNew) + 4, 14962);
        AppendImageFrame(pImgVector, GetScrCodeField(ImageGirlPortrait) + 4, 14983);
        AppendImageFrame(pImgVector, GetScrCodeField(ImageArcherPicNew) + 4, 14910);
        AppendImageFrame(pImgVector, GetScrCodeField(ImageHorrendousPicNew) + 4, 14970);
        AppendImageFrame(pImgVector, GetScrCodeField(ImageMaidenPicNew) + 4, 14975);
        AppendImageFrame(pImgVector, GetScrCodeField(imageCustomSlot) + 4, 14415);
    }
}

void InitializeCommonImage()
{
    int imgVector = CreateImageVector(IMAGE_VECTOR_SIZE);

    introduceCustomImage(imgVector);
    DoImageDataExchange(imgVector);
}

