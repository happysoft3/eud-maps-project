

#include "libs/animFrame.h"
#include "libs/grplib.h"
// #include "libs\clientside.h"
#include "libs/objectIDdefines.h"

#define IMAGE_VECTOR_SIZE 2048

// void UserMapData()
// { }

void MapImageResourcePool()
{ }

static void imageEatRamen()
{ }
static void imageYouwin()
{ }
static void imageYoulose()
{ }

void GRPDump_globglogabgalabOutput(){}

#define GLOBGLOGABGALAB_IMAGE_START_ID 133979
void initializeImage_globglogabgalab(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_TROLL;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_globglogabgalabOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4, 3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4, 3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    AnimFrameAssign8Directions(0,GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(10,GLOBGLOGABGALAB_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_NashimanOutput(){}

#define NASHIMAN_IMAGE_ID 124381
void initializeImageFrameNashiman(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOLF;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_NashimanOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,7,USE_DEFAULT_SETTINGS, 2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,7,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,5,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, NASHIMAN_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, NASHIMAN_IMAGE_ID+ 8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, NASHIMAN_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, NASHIMAN_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, NASHIMAN_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, NASHIMAN_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(30, NASHIMAN_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameAssign8Directions(35, NASHIMAN_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(NASHIMAN_IMAGE_ID+ 8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopy8Directions( NASHIMAN_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
}

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 123052
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount-4);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}

void initializeClientStuff(){
    ChangeSpriteItemNameAsString(OBJ_AMULET_OF_TELEPORTATION, "불닭볶음면");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULET_OF_TELEPORTATION, "이것을 사용하면 약간의 체력을 회복시켜준다. 맵에 있는 모든 볶음면을 다 먹어야 한다");
    ChangeSpriteItemNameAsString(OBJ_CIDER, "소주");
    ChangeSpriteItemDescriptionAsString(OBJ_CIDER, "정신이 몽롱해지며 하늘을 달리는 기분을 준다");
    ChangeSpriteItemNameAsString(OBJ_COMPASS, "알람시계");
    ChangeSpriteItemDescriptionAsString(OBJ_COMPASS, "시간제한을 조금 더 늦춘다");
}

void GRPDump_RogueMercenaryOutput(){}

#define ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID 118946
void initializeImageFrameRogueMercenary(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_ARCHER;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_RogueMercenaryOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,10,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,10,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,6,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(25,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(30,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(35,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(40,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameAssign8Directions(45,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_WALK, 8);
    AnimFrameAssign8Directions(50,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_WALK, 9);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_RUN, 8);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_RUN, 9);
    AnimFrameAssign8Directions(55,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+88, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
    AnimFrameAssign8Directions(60,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+96, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 1);
    AnimFrameAssign8Directions(65,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+104, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 2);
    AnimFrameAssign8Directions(70,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+112, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 3);
    AnimFrameAssign8Directions(75,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+120, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 4);
    AnimFrameAssign8Directions(80,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+128, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 5);
}

void GRPDump_RumiaDumpOutput(){}

#define RUMIA_OUTPUT_IMAGE_START_AT 119258

void initializeImageRumia(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SWORDSMAN;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_RumiaDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,5,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(5,    RUMIA_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);    
    AnimFrameAssign8Directions(10,    RUMIA_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);    
    AnimFrameAssign8Directions(15,    RUMIA_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);    
    AnimFrameAssign8Directions(20,    RUMIA_OUTPUT_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);    
    AnimFrameAssign8Directions(0,    RUMIA_OUTPUT_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameAssign8Directions(25,    RUMIA_OUTPUT_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(30,    RUMIA_OUTPUT_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(35,    RUMIA_OUTPUT_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(40,    RUMIA_OUTPUT_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(45,    RUMIA_OUTPUT_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(50,    RUMIA_OUTPUT_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(55,    RUMIA_OUTPUT_IMAGE_START_AT+88,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(60,    RUMIA_OUTPUT_IMAGE_START_AT+96,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+88,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+96,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(         RUMIA_OUTPUT_IMAGE_START_AT+96,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
}

void GRPDump_BunnyGirlOutput(){}
#define BUNNYGIRL_OUTPUT_BASE_IMAGE_ID 133943
void initializeImageFrameBunnyGirlOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BunnyGirlOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BUNNYGIRL_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_ClownOutput(){}
#define CLOWN_OUTPUT_BASE_IMAGE_ID 117185
void initializeImageFrameClownOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_URCHIN;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_ClownOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, CLOWN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, CLOWN_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     CLOWN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, CLOWN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     CLOWN_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     CLOWN_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     CLOWN_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     CLOWN_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     CLOWN_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     CLOWN_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
}

void GRPDump_OrangeHairYogaOutput(){}
#define ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID 133955
void initializeImageFrameOrangeHairYogaOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_OrangeHairYogaOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     ORANGE_HAIR_YOGA_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void InitializeResource()
{
    int imgVec = CreateImageVector(IMAGE_VECTOR_SIZE);

    int index = 0;
    if (LoadImageResource(GetScrCodeField(MapImageResourcePool)+4))
    {
        int num = 0;

        AppendImageFrame(imgVec, GetGrpStream(num++), 133814);
        AppendImageFrame(imgVec, GetGrpStream(num++), 112974);
        AppendImageFrame(imgVec, GetGrpStream(num++), 17866);
    }
    AppendImageFrame(imgVec, GetScrCodeField(imageEatRamen)+4, 125419);
    AppendImageFrame(imgVec, GetScrCodeField(imageYouwin)+4, 132313);
    AppendImageFrame(imgVec, GetScrCodeField(imageYoulose)+4, 132314);
    initializeImage_globglogabgalab(imgVec);
    initializeImageFrameNashiman(imgVec);
    initializeImageFrameBillyOni(imgVec);
    initializeImageFrameRogueMercenary(imgVec);
    initializeImageFrameBunnyGirlOutput(imgVec);
    initializeImageRumia(imgVec);
    initializeImageFrameClownOutput(imgVec);
    initializeImageFrameOrangeHairYogaOutput(imgVec);
    DoImageDataExchange(imgVec);
    initializeClientStuff();
}

