
#include "p0110011_utils.h"
#include "libs/unitstruct.h"

int GetPlayer(int pIndex) //virtual
{ }

void SetPlayer(int pIndex, int user) //virtual
{ }

// #define SEND_UP_SHIFT 
#define SEND_LEFT_SHIFT 1 //move
#define SEND_RIGHT_SHIFT 2 //move
#define SEND_L_CTRL_SHIFT 3 //attack
#define SEND_KEY_Z_SHIFT 4 //skill
#define SEND_KEY_X_SHIFT 5 //skill
#define SEND_DOWN_SHIFT 6 //move
#define SEND_KEY_C_SHIFT 7 //skill
// #define SEND_KEY_C_SHIFT 7 //skill spare
// #define SEND_KEY_V_SHIFT 8 //skill spare

#define KEY_UP_SHIFT 1 
#define KEY_LEFT_SHIFT 2 
#define KEY_RIGHT_SHIFT 4 
#define KEY_L_CTRL_SHIFT 8 
#define KEY_Z_SHIFT 16 
#define KEY_X_SHIFT 32 
#define KEY_DOWN_SHIFT 64
#define KEY_C_SHIFT 128


#define MAX_PLAYER_COUNT 32

int GetUserRespawnMark(int pIndex) //virtual
{ }
void SetUserRespawnMark(int pIndex, int mark) //virtual
{ }

void ResetUserRespawnMarkPos(int pIndex)
{
    int mark=GetUserRespawnMark(pIndex);
    int initPos = mark+1;

    if (!ToInt(GetObjectX(initPos)))
    {
        // WriteLog("ResetUserRespawnMarkPos::error");
        return;
    }
    MoveObject(mark, GetObjectX(initPos), GetObjectY(initPos));
}

int WizardGreenBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 1917281394; arr[2] = 7234917; arr[17] = 500; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1066947052; arr[26] = 4; arr[53] = 1128792064; 
		arr[54] = 4; arr[60] = 1335; arr[61] = 46914816; 
	pArr = arr;
	return pArr;
}

void WizardGreenSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 500;	hpTable[1] = 500;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WizardGreenBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int HorrendousBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 50; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 9; 
		arr[27] = 5; arr[28] = 1065353216; arr[29] = 1; arr[54] = 4; arr[59] = 5542784; 
		arr[60] = 1386; arr[61] = 46907648; 
	pArr = arr;
	return pArr;
}

void HorrendousSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 50;	hpTable[1] = 50;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = HorrendousBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int GetClientKeyInput(int pIndex)//virtual
{}
void GetClientKeyInputPtr(int *pp)//virtual
{}
void SetClientKeyInput(int pIndex,int input)//virtual
{}

int GenericHash() //virtual
{ }

#define CLIENT_SUBDATA_OFF 0x753B10
#define SUBDATA_REPORT_HP 0
#define SUBDATA_REPORT_LEVEL 1
#define SUBDATA_REPORT_EXP 2
#define SUBDATA_REPORT_SKILL_1 3
#define SUBDATA_REPORT_SKILL_2 4
#define SUBDATA_REPORT_SKILL_3 5
#define SUBDATA_REPORT_DAMAGE_UP 6
#define SUBDATA_REPORT_HP_UP 7
#define SUBDATA_REPORT_UPDATE_FLAGS 8
#define SUBDATA_REPORT_SPEED_UP 9

void GetCreatureHpInfoMessage(short **p) //virtual
{ }

int GetCreatureHpPrev(int pIndex) //virtual
{ }
void SetCreatureHpPrev(int pIndex,int value) //virtual
{ }
int GetCreatureLevel(int pIndex) //virtual
{ }
void SetCreatureLevel(int pIndex, int setTo) //virtual
{ }
int GetCreatureExp(int pIndex) //virtual
{ }
void SetCreatureExp(int pIndex, int setTo) //virtual
{ }

void GetCreatureLevelExpInfoMessage(short **p) //virtual
{ }

int GetUserSkill1(int pIndex)//virtual
{}
int GetUserSkill2(int pIndex)//virtual
{}
int GetUserSkill3(int pIndex)//virtual
{}
void SetUserSkill1(int pIndex, int fn)//virtual
{ }
void SetUserSkill2(int pIndex, int fn)//virtual
{ }
void SetUserSkill3(int pIndex, int fn)//virtual
{ }
int GetUserCooldown1(int pIndex)//virtual
{}
int GetUserCooldown2(int pIndex)//virtual
{}
int GetUserCooldown3(int pIndex)//virtual
{}
void SetUserCooldown1(int pIndex, int cool)//virtual
{ }
void SetUserCooldown2(int pIndex, int cool)//virtual
{ }
void SetUserCooldown3(int pIndex, int cool)//virtual
{ }
void GetSkillMessage(short **p, int n) //virtual
{ }

int MasterUnit()
{
	int unit;

	if (!unit)
	{
		unit=CreateObjectById(OBJ_HECUBAH,100.0,100.0);
		Frozen(unit,TRUE);
	}
	return unit;
}

void GetBeaconGuideMessage(short **p, int n)//virtual
{ }

int GetDamageUpgradeLevel(int pIndex) //virtual
{ }
void SetDamageUpgradeLevel(int pIndex,int setTo)//virtual
{ }

int GetHpUpgradeLevel(int pIndex)//virtual
{ }
void SetHpUpgradeLevel(int pIndex,int setTo)//virtual
{ }
#define DEFAULT_STAT_DAMAGE 0
#define DEFAULT_STAT_HP 1
int GetDefaultStat(int pIndex, int ty)//virtual
{ }
void SetDefaultStat(int pIndex, int ty, int value)//virtual
{ }
float GetCreatureSpeed(int pIndex)//virtual
{ }
void SetCreatureSpeed(int pIndex,float speed)//virtual
{ }
int GetSpeedUpgradeLevel(int pIndex)//virtual
{ }
void SetSpeedUpgradeLevel(int pIndex,int setTo)//virtual
{ }

int GetLastTransmission()
{
	int unit;

	if(!unit)
	{
		unit=DummyUnitCreateById(OBJ_GHOST,GetWaypointX(12),GetWaypointY(12));
		UnitNoCollide(unit);
	}
	return unit;
}
void MoveLastTransmission(int posUnit)
{
	MoveObject(GetLastTransmission(),GetObjectX(posUnit),GetObjectY(posUnit));
}

int GetUserCharacterId(int pIndex) //virtual
{ }

void SetUserCharacterId(int pIndex, int id) //virtual
{ }

int GetCharacterSpawnFn(int pIndex)//virtual
{ }
void SetCharacterSpawnFn(int pIndex, int fn)//virtual
{ }

int GetCharacterDefaultAttack(int pIndex)//virtual
{ }
void SetCharacterDefaultAttack(int pIndex, int fn)//virtual
{ }

int ComputeDamageUpPay(int lv)
{
    return 10+(lv*10);
}
int ComputeHpUpPay(int lv)
{
    return 10+(lv*15);
}
int ComputeSpeedUpPay(int lv)
{
    return 25+(lv*12);
}

int ShadeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684105299; arr[1] = 101; arr[17] = 500; arr[18] = 30; arr[19] = 120; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1106247680; arr[29] = 40; arr[31] = 4; arr[32] = 7; arr[33] = 13; 
		arr[59] = 5542784; arr[60] = 1362; arr[61] = 46905088; 
	pArr = arr;
	return pArr;
}

void ShadeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 500;	hpTable[1] = 500;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = ShadeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

