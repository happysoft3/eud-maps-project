
#include "fkjapan_main.h"

void MapInitialize(){OnInitializeMap();}
void MapExit(){OnShutdownMap();}

// int m_player[MAX_PLAYER_COUNT];
// static int GetPlayer(int pIndex) //virtual
// { return m_player[pIndex]; }
// static void SetPlayer(int pIndex, int user) //virtual
// { m_player[pIndex]=user; }
// int m_playerFlags[MAX_PLAYER_COUNT];
// static int GetPlayerFlags(int pIndex) //virtual
// {return m_playerFlags[pIndex]; }
// static void SetPlayerFlags(int pIndex, int flags) //virtual
// { m_playerFlags[pIndex]=flags;}

int m_mainKey2;
static void QueryMainKeySecond(int *get, int set){
    if (get){
        get[0]=m_mainKey2;
        return;
    }
    m_mainKey2=set;
}//virtual

int m_mainKeyFirst;
static void QueryMainKeyFirst(int *get, int set){
    if (get){
        get[0]=m_mainKeyFirst;
        return;
    }
    m_mainKeyFirst=set;
}//virtual

int m_lbPtr;
static void QueryLibraryBookcases1(int *get, int set){
    if (get){
        get[0]=m_lbPtr;
        return;
    }
    m_lbPtr=set;
}//virtual
int m_lbPtr2;
static void QueryLibraryBookcases2(int *get, int set){
    if (get){
        get[0]=m_lbPtr2;
        return;
    }
    m_lbPtr2=set;
}//virtual
int m_lbCount;
static void QueryLibraryCount(int *get,int set){
    if (get){
        get[0]=m_lbCount;
        return;
    }
    m_lbCount=set;
}//virtual
int m_myBlock;
static void QueryMyBlockFirst(int *get,int set){
    if (get){
        get[0]=m_myBlock;
        return;
    }
    m_myBlock=set;
}//virtual
int m_myBlock2;
static void QueryMyBlockSecond(int *get,int set){
    if (get){
        get[0]=m_myBlock2;
        return;
    }
    m_myBlock2=set;
}//virtual

int m_campArea;
static void QueryCampArea(int *get,int set){
    if (get){
        get[0]=m_campArea;
        return;
    }
    m_campArea=set;
}//virtual

int m_thirdTrap;
static void QueryArrowTrapThird(int *get,int set){
    if (get){
        get[0]=m_thirdTrap;
        return;
    }
    m_thirdTrap=set;
}//virtual
int m_secondTrap;
static void QueryArrowTrapSecond(int *get,int set){
    if (get){
        get[0]=m_secondTrap;
        return;
    }
    m_secondTrap=set;
}//virtual
int m_firstTrap;
static void QueryArrowTrapFirst(int *get,int set){
    if (get){
        get[0]=m_firstTrap;
        return;
    }
    m_firstTrap=set;
}//virtual
int m_underRots;
static void QueryUnderfootRots(int *get,int set){
    if (get){
        get[0]=m_underRots;
        return;
    }
    m_underRots=set;
}//virtual
int m_fireTrap;
static void QueryFireTrap1(int *get,int set){
    if (get){
        get[0]=m_fireTrap;
        return;
    }
    m_fireTrap=set;
}//virtual

int m_libEx;
static void QueryLibraryBookEx(int *get,int set){
    if (get){
        get[0]=m_libEx;
        return;
    }
    m_libEx=set;
}//virtual

int m_libArrows[16];
static void QueryLibraryArrowTraps(int *get,int set, int n){
    if (get){
        get[0]=m_libArrows[n];
        return;
    }
    m_libArrows[n]=set;
}//virtual

static void NetworkUtilClientMain(){ InitializeResources(); }
static void IntroducedIndexLoopHashCondition(int pInstance){
    BeforeExecIndexLoop(pInstance);
}
static int ChatMessageLoopFrame() {return 30;}
static void OnPlayerEntryMap(int pInfo)
{ OnPlayerEnteredMap(pInfo);}
