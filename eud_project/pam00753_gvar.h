
#define MAX_PLAYER_COUNT 32

int GetPlayer(int pIndex) //virtual
{ }

void SetPlayer(int pIndex, int user) //virtual
{ }

int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_GOD_MODE 32

int PlayerClassCheckFlag(int pIndex, int flags)
{
    return GetPlayerFlags(pIndex) & flags;
}

void PlayerClassSetFlag(int pIndex, int flags)
{
    SetPlayerFlags(pIndex, GetPlayerFlags(pIndex)^flags);
}

void StoreMonsterMarker(int id, int mark) //virtual
{ 
}

int ReleaseMonsterMarker(int id) //virtual
{ }

int GetMasterUnit() //virtual
{ }
