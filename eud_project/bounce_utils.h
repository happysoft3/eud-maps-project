
#include "libs/define.h"
#include "libs/winapi.h"
#include "libs/recovery.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

int KeyboardIOCheckKey(int keyId)
{
    int *keyTable = 0x6950b0;
    int *selkey = keyTable+(keyId*8);
    int key = (selkey[0]>>8)&0xff;

    return key==2;
}
