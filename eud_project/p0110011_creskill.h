
#include "p0110011_gvar.h"
#include "libs/bind.h"
#include "libs/queueTimer.h"
#include "libs/hash.h"
#include "libs/wallutil.h"
#include "libs/fxeffect.h"
#include "libs/monsteraction.h"
#include "libs/waypoint.h"
#include "libs/sound_define.h"
#include "libs/buff.h"
#include "libs/printutil.h"

void OnSpikeRingCollide()
{
    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(SELF);

            if (!IsAttackedBy(OTHER, owner))
                return;

            Damage(OTHER, owner, 220, DAMAGE_TYPE_BLADE);
        }
        
        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(SELF);
        break;
    }
}

void LaunchSpikeRingSingle(int owner, float xBase, float yBase, int angle, float gap, float force)
{
    int sp=CreateObjectAt("OgreShuriken", xBase + MathSine(angle +90, gap), yBase + MathSine(angle, gap));

    SetOwner(owner, sp);
    PushObject(sp, force, xBase, yBase);
    SetUnitCallbackOnCollide(sp, OnSpikeRingCollide);
}

void UserSkillSpreadRing(int pIndex, int cre, int *pCooldown)
{
    int repeat=36;
    float xProfile = GetObjectX(cre), yProfile = GetObjectY(cre);

    while (--repeat>=0)
        LaunchSpikeRingSingle(cre, xProfile, yProfile, repeat * 10, 13.0, 33.0);

    PlaySoundAround(cre, SOUND_MonsterGeneratorSpawn);
}

void ManaBombCancelFx(int sUnit)
{
    int caster = CreateObjectAt("ImaginaryCaster", GetObjectX(sUnit), GetObjectY(sUnit));

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}

void onManaExplosionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 200, DAMAGE_TYPE_PLASMA);
        }
    }
}

void UserSkillManaExplosion(int pIndex, int cre, int *pCooldown)
{
    SplashDamageAtEx(cre, GetObjectX(cre),GetObjectY(cre), 200.0, onManaExplosionSplash);
    ManaBombCancelFx(cre);
}

void onAutoDeathRayVisible()
{
    Effect("SENTRY_RAY",GetObjectX(SELF),GetObjectY(SELF),GetObjectX(OTHER),GetObjectY(OTHER));
    Damage(OTHER,GetOwner(SELF),300,DAMAGE_TYPE_ZAP_RAY);
}

void UserSkillAutoDeathray(int pIndex, int cre, int *pCooldown)
{
    float x=GetObjectX(cre),y=GetObjectY(cre),xvect=UnitAngleCos(cre,3.0),yvect=UnitAngleSin(cre,3.0);
    int sub=CreateObjectById(OBJ_WEIRDLING_BEAST,x+xvect,y+yvect);

    SetOwner(cre,sub);
    LookWithAngle(sub,GetDirection(cre));
    UnitNoCollide(sub);
    DeleteObjectTimer(sub,1);
    SetCallback(sub,3,onAutoDeathRayVisible);
    SetUnitScanRange(sub,400.0);
    Effect("VIOLET_SPARKS",x,y,0.0,0.0);
    Effect("DEATH_RAY",x,y,x+(xvect*10.0),y+(yvect*10.0));
}

#define MEGA_TRAIN_SUBUNIT 0
#define MEGA_TRAIN_ENEMY 1
#define MEGA_TRAIN_CRE 2
#define MEGA_TRAIN_COUNT 3
#define MEGA_TRAIN_XVECT 4
#define MEGA_TRAIN_YVECT 5
#define MEGA_TRAIN_HELPER 6
#define MEGA_TRAIN_MAX 7

void preserveMegaTrain(int *d)
{
    int cre=d[MEGA_TRAIN_CRE],helper=d[MEGA_TRAIN_HELPER];
    float *xyVect = &d[MEGA_TRAIN_XVECT];

    while (CurrentHealth(cre))
    {
        if (--d[MEGA_TRAIN_COUNT]>=0)
        {
            int enemy=d[MEGA_TRAIN_ENEMY];

            if (CurrentHealth(enemy)&&IsVisibleOr(enemy,cre))
            {
                if (DistanceUnitToUnit(helper,enemy)>23.0)
                {
                    xyVect[0]=UnitRatioX(enemy,helper,11.0);
                    xyVect[1]=UnitRatioY(enemy,helper,11.0);
                }
                else
                {
                    Effect("THIN_EXPLOSION",GetObjectX(enemy),GetObjectY(enemy),0.0,0.0);
                    Damage(enemy, cre, 130, DAMAGE_TYPE_PLASMA);
                    break;
                }
            }
            MoveObjectVector(helper,xyVect[0],xyVect[1]);
            Effect("COUNTERSPELL_EXPLOSION",GetObjectX(helper),GetObjectY(helper),0.0,0.0);
            PushTimerQueue(1,d,preserveMegaTrain);
            return;
        }
        break;
    }
    Delete(helper);
    FreeSmartMemEx(d);
}

void doMegaTrain(int *d)
{
    HashGet(GenericHash(), d[MEGA_TRAIN_SUBUNIT], NULLPTR, TRUE);
    int cre=d[MEGA_TRAIN_CRE];
    d[MEGA_TRAIN_XVECT]=UnitAngleCos(cre,11.0);
    d[MEGA_TRAIN_YVECT]=UnitAngleSin(cre,11.0);
    d[MEGA_TRAIN_HELPER]=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,GetObjectX(cre),GetObjectY(cre));
    preserveMegaTrain(d);
}

void onMegaTrainVision()
{
    int *d;

    HashGet(GenericHash(), GetTrigger(), &d, FALSE);
    int enemy=d[MEGA_TRAIN_ENEMY];

    if (CurrentHealth(enemy))
    {
        float dist=DistanceUnitToUnit(SELF, enemy);

        if (DistanceUnitToUnit(SELF, OTHER)>=dist)
            return;
    }
    d[MEGA_TRAIN_ENEMY] =GetCaller();
}

void megaTrainStandby(int pIndex, int cre, int *d)
{
    float x=GetObjectX(cre),y=GetObjectY(cre),xvect=UnitAngleCos(cre,3.0),yvect=UnitAngleSin(cre,3.0);
    int sub=CreateObjectById(OBJ_WEIRDLING_BEAST, x+xvect,y+yvect);
    SetOwner(cre,sub);
    DeleteObjectTimer(sub,1);
    UnitNoCollide(sub);
    LookWithAngle(sub,GetDirection(cre));
    HashPushback(GenericHash(), sub, d);
    d[MEGA_TRAIN_SUBUNIT]=sub;
    SetUnitScanRange(sub,400.0);
    SetCallback(sub,3,onMegaTrainVision);
}

void UserSkillMegaTrain(int pIndex, int cre, int *pCooldown)
{
    int *d;

    AllocSmartMemEx(MEGA_TRAIN_MAX*4,&d);
    d[MEGA_TRAIN_CRE]=cre;
    d[MEGA_TRAIN_COUNT]=30;
    d[MEGA_TRAIN_XVECT]=0;
    d[MEGA_TRAIN_YVECT]=0;
    megaTrainStandby(pIndex,cre,d);
    PushTimerQueue(1,d, doMegaTrain);
}

void onBlackHoleEffectSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    int owner=GetOwner(SELF);

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, owner) && IsVisibleOr(OTHER, SELF))
        {
            float dist=DistanceUnitToUnit(SELF,OTHER);

            if (dist>30.0)
            {
                PushObjectTo(OTHER,UnitRatioX(SELF,OTHER,7.0),UnitRatioY(SELF,OTHER,7.0));
                return;
            }
            Damage(OTHER, owner, 85, DAMAGE_TYPE_PLASMA);
        }
    }
}

#define BLACK_HOLE_SUB 0
#define BLACK_HOLE_ENEMY 1
#define BLACK_HOLE_CRE 2
#define BLACK_HOLE_XVECT 3
#define BLACK_HOLE_YVECT 4
#define BLACK_HOLE_CORE 5
#define BLACK_HOLE_TIME 6
#define BLACK_HOLE_MAX 7

void movingBlackHole(int *d)
{
    int core=d[BLACK_HOLE_CORE];

    if (ToInt(GetObjectX(core)))
    {
        int owner=d[BLACK_HOLE_CRE];

        if (CurrentHealth(owner) && IsVisibleOr(core,owner))
        {
            int dur=--d[BLACK_HOLE_TIME];

            if (dur>=0)
            {
                PushTimerQueue(1,d,movingBlackHole);
                if (dur&1)
                    SplashDamageAtEx(core, GetObjectX(core),GetObjectY(core), 180.0, onBlackHoleEffectSplash);
                float *vect=&d[BLACK_HOLE_XVECT];
                MoveObjectVector(core,vect[0],vect[1]);
                return;
            }
        }
        Delete(core);
    }
    FreeSmartMemEx(d);
}

#define BLACKHOLE_GAP 13.0

void doSkillBlackHole(int *d)
{
    HashGet(GenericHash(), d[BLACK_HOLE_SUB],NULLPTR,TRUE);
    int enemy=d[BLACK_HOLE_ENEMY], cre=d[BLACK_HOLE_CRE];

    if (!CurrentHealth(cre))
        return;

    float vectXY[]={UnitAngleCos(cre,BLACKHOLE_GAP),UnitAngleSin(cre,BLACKHOLE_GAP)};

    if (CurrentHealth(enemy))
    {
        if (IsVisibleOr(enemy, cre))
        {
            vectXY[0]=UnitRatioX(enemy,cre,BLACKHOLE_GAP);
            vectXY[1]=UnitRatioY(enemy,cre,BLACKHOLE_GAP);
        }
    }
    d[BLACK_HOLE_XVECT]=vectXY[0];
    d[BLACK_HOLE_YVECT]=vectXY[1];
    int storm=CreateObjectById(OBJ_STORM_CLOUD,GetObjectX(cre)+vectXY[0],GetObjectY(cre)+vectXY[1]);
    d[BLACK_HOLE_CORE]=storm;
    SetOwner(cre,storm);
    d[BLACK_HOLE_TIME]=30;
    PushTimerQueue(1,d,movingBlackHole);
    // shotHarpoonBolt(cre,vectXY[0],vectXY[1], 33.0);
}

void UserSkillBlackHole(int pIndex, int cre, int *pCooldown)
{
    int *d;

    AllocSmartMemEx(BLACK_HOLE_MAX*4, &d);
    d[BLACK_HOLE_CRE]=cre;
    megaTrainStandby(pIndex,cre,d);
    PushTimerQueue(1,d,doSkillBlackHole);
}

void drawMeteorSingle(float x,float y)
{
    int unit=CreateObjectById(OBJ_METEOR_EXPLODE,x,y);

    Frozen(unit,TRUE);
}

#define FIRERING_DATA_RING_PTR 0
#define FIRERING_DATA_DURATION 1
#define FIRERING_DATA_OWNER 2
#define FIRERING_DATA_MAX 3

void drawFireRing(int owner, float gap,int *d)
{
    float x=GetObjectX(owner),y=GetObjectY(owner);
    int rep = 36,base=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,x,y);
    float prex,prey;

    while (--rep>=0)
    {
        prex=x+MathSine(rep*10 +90, gap);
        prey=y+MathSine(rep*10 , gap);
        if (prex<100.0)
            prex=100.0;
        if (prey<100.0)
            prey=100.0;       

        drawMeteorSingle(prex,prey);
    }
    d[FIRERING_DATA_RING_PTR]=base;
}

void removeFireRing(int base)
{
    int rep=36;
    Delete(base--);
    while (--rep>=0)
        Delete(base+rep);
}

void onFireRingSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
            Damage(OTHER, SELF, 20, DAMAGE_TYPE_FLAME);
    }
}

void onTimeFireRing(int *d)
{
    int owner=d[FIRERING_DATA_OWNER],base=d[FIRERING_DATA_RING_PTR];

    if (CurrentHealth(owner))
    {
        if (--d[FIRERING_DATA_DURATION]>=0)
        {
            PushTimerQueue(6,d,onTimeFireRing); //here
            SplashDamageAtEx(owner,GetObjectX(base),GetObjectY(base),180.0,onFireRingSplash);
            return;
        }
    }
    removeFireRing(base);
    FreeSmartMemEx(d);
}

void UseSkillFireRing(int pIndex, int cre, int *pCooldown)
{
    int *d;

    AllocSmartMemEx(FIRERING_DATA_MAX*4,&d);
    d[FIRERING_DATA_DURATION]=16;
    d[FIRERING_DATA_OWNER]=cre;
    drawFireRing(cre,180.0,d);
    PushTimerQueue(3,d,onTimeFireRing);
}

void onSpitFlameCollide()
{
    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(SELF);

            if (!IsAttackedBy(OTHER, owner))
                return;

            Damage(OTHER, owner, 14, DAMAGE_TYPE_LAVA);
        }
        
        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(SELF);
        break;
    }
}

#define SPITFLAME_DATA_OWNER 0
#define SPITFLAME_DATA_DURARTION 1
#define SPITFLAME_DATA_SUBUNIT 2
#define SPITFLAME_DATA_USER 3
#define SPITFLAME_DATA_MAX 4

void spitFlameSub(int owner, int user, int sub, float x, float y){
    float vect[]={UnitRatioX(sub,owner, 9.0),UnitRatioY(sub,owner,9.0)};
    int shot=CreateObjectById(OBJ_IMP_SHOT, x+vect[0],y+vect[1]);
    SetOwner(owner,shot);
    PushObjectTo(shot,vect[0]*3.0,vect[1]*3.0);
    Effect("THIN_EXPLOSION",GetObjectX(owner),GetObjectY(owner),0.0,0.0);
}

void onSpitFlame(int *d)
{
    int owner=d[SPITFLAME_DATA_OWNER];

    if (CurrentHealth(owner))
    {
        int user=d[SPITFLAME_DATA_USER];
        if (CurrentHealth(user)){
            if (--d[SPITFLAME_DATA_DURARTION]>=0)
            {
                PushTimerQueue(1,d,onSpitFlame);
                int sub=d[SPITFLAME_DATA_SUBUNIT], cx,cy;

                GetPlayerMouseXY(user,&cx,&cy);
                MoveObject(sub,IntToFloat(cx),IntToFloat(cy));
                spitFlameSub(owner,user,sub,GetObjectX(owner),GetObjectY(owner));
                return;
            }
        }
    }
    FreeSmartMemEx(d);
}

void UseSkillSpitFlame(int pIndex, int cre, int *pCooldown)
{
    int user = GetPlayer(pIndex);

    if (CurrentHealth(user))
    {
        UniPrint(user,"잠시동안 입에서 불꽃을 내뿜습니다, 마우스로 방향을 조절하세요");
        int *d;

        AllocSmartMemEx(SPITFLAME_DATA_MAX*4,&d);
        d[SPITFLAME_DATA_OWNER]=cre;
        d[SPITFLAME_DATA_DURARTION]=90;
        d[SPITFLAME_DATA_USER]=user;
        d[SPITFLAME_DATA_SUBUNIT]=CreateObjectById(OBJ_IMAGINARY_CASTER,GetObjectX(cre),GetObjectY(cre));
        PushTimerQueue(3,d,onSpitFlame);
        pCooldown[0]+=30;
    }
}

void onHarpoonExplosionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 90, DAMAGE_TYPE_PLASMA);
        }
    }
}

#define HORREN_HARPOON_SUBUNIT 0
#define HORREN_HARPOON_ENEMY 1
#define HORREN_HARPOON_CRE 2
#define HORREN_HARPOON_MAX 3

void onHorrendousHarpoonCollide()
{
    if (GetCaller()==0)
    {
        int wall = WallUtilGetWallAtObjectPosition(SELF);
        if (wall)
            WallBreak(wall);
    }
    else if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(SELF);
        
        if (IsAttackedBy(OTHER,owner))
        {
            SplashDamageAtEx(GetOwner(SELF), GetObjectX(SELF),GetObjectY(SELF),85.0, onHarpoonExplosionSplash);
        }
    }
    else return;
    DeleteObjectTimer(CreateObjectById(OBJ_EXPLOSION,GetObjectX(SELF),GetObjectY(SELF)), 12);
    Delete(SELF);
}

void shotHarpoonBolt(int owner, float xvect, float yvect,float force)
{
    float x=GetObjectX(owner),y=GetObjectY(owner);
    int mis=CreateObjectById(OBJ_HARPOON_BOLT, x+xvect,y+yvect);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetOwner(owner,mis);
    SetUnitCallbackOnCollide(mis, onHorrendousHarpoonCollide);
    LookWithAngle(mis,GetDirection(owner));
    PushObject(mis,force, x,y);
}

#define HARPOON_SPAWN_GAP 11.0

void doSkillHarpoon(int *d)
{
    HashGet(GenericHash(), d[HORREN_HARPOON_SUBUNIT],NULLPTR,TRUE);
    int enemy=d[HORREN_HARPOON_ENEMY], cre=d[HORREN_HARPOON_CRE];

    if (!CurrentHealth(cre))
        return;

    float vectXY[]={UnitAngleCos(cre,HARPOON_SPAWN_GAP),UnitAngleSin(cre,HARPOON_SPAWN_GAP)};

    if (CurrentHealth(enemy))
    {
        if (IsVisibleOr(enemy, cre))
        {
            vectXY[0]=UnitRatioX(enemy,cre,HARPOON_SPAWN_GAP);
            vectXY[1]=UnitRatioY(enemy,cre,HARPOON_SPAWN_GAP);
        }
    }
    shotHarpoonBolt(cre,vectXY[0],vectXY[1], 33.0);
}

void UserSkillHarpoon(int pIndex, int cre, int *pCooldown)
{
    int *d;

    AllocSmartMemEx(HORREN_HARPOON_MAX*4, &d);
    d[HORREN_HARPOON_CRE]=cre;
    megaTrainStandby(pIndex,cre,d);
    PushTimerQueue(1,d,doSkillHarpoon);
}

#define WAR_HARPOON_SUB 0
#define WAR_HARPOON_ENEMY 1
#define WAR_HARPOON_CRE 2
#define WAR_HARPOON_MAX 3

void doRealHarpoon(int *d)
{
    HashGet(GenericHash(), d[HORREN_HARPOON_SUBUNIT],NULLPTR,TRUE);
    int enemy=d[HORREN_HARPOON_ENEMY], cre=d[HORREN_HARPOON_CRE];

    if (!CurrentHealth(cre))
        return;

    if (IsVisibleOr(enemy, cre))
        LookAtObject(cre,enemy);
    castHarpoonForcedFix(cre, 11.0);
}

void UserSkillRealHarpoon(int pIndex, int cre, int *pCooldown)
{
    int *d;

    AllocSmartMemEx(HORREN_HARPOON_MAX*4, &d);
    d[HORREN_HARPOON_CRE]=cre;
    megaTrainStandby(pIndex,cre,d);
    PushTimerQueue(1,d,doRealHarpoon);
}

void onMultipleArrowCollide()
{
    if (!GetTrigger())
        return;

    if (!GetCaller())
    {
        WallUtilDestroyWallAtObjectPosition(SELF);
        Delete(SELF);
        return;
    }

    if (!CurrentHealth(OTHER))
        return;

    if (IsAttackedBy(OTHER, GetOwner(SELF)))
    {
        Damage(OTHER, GetOwner(SELF), 150, DAMAGE_TYPE_IMPACT);
        Delete(SELF);
    }
}

void doMultipleShot(int *d)
{
    int enemy=d[HORREN_HARPOON_ENEMY],cre=d[HORREN_HARPOON_CRE];
    float vectXY[2];

    HashGet(GenericHash(), d[HORREN_HARPOON_SUBUNIT],NULLPTR,TRUE);
    if (!CurrentHealth(cre))
        return;
    if (CurrentHealth(enemy)&&IsVisibleOr(cre,enemy))
    {
        vectXY[0]=UnitRatioX(enemy, cre, 9.0);
        vectXY[1]=UnitRatioY(enemy, cre, 9.0);
    }
    else
    {
        vectXY[0]=UnitAngleCos(cre,9.0);
        vectXY[1]=UnitAngleSin(cre,9.0);
    }
    float xy[]={GetObjectX(cre),GetObjectY(cre)};
    int rep=5,single,dir=GetDirection(cre);

    TeleportLocation(1, xy[0]+vectXY[0]-(vectXY[1]*2.0), xy[1]+vectXY[1]+(vectXY[0]*2.0));
    while (--rep>=0)
    {
        single=CreateObjectById(OBJ_ARCHER_BOLT,LocationX(1),LocationY(1));
        SetOwner(cre,single);
        PushObjectTo(single,vectXY[0]*2.0,vectXY[1]*2.0);
        LookWithAngle(single,dir);
        SetUnitCallbackOnCollide(single,onMultipleArrowCollide);
        TeleportLocationVector(1,vectXY[1],-vectXY[0]);
    }
}

void UserSkillMultipleShot(int pIndex, int cre, int *pCooldown)
{
    int *d;

    AllocSmartMemEx(HORREN_HARPOON_MAX*4,&d);
    d[HORREN_HARPOON_CRE]=cre;
    megaTrainStandby(pIndex,cre,d);
    PushTimerQueue(1,d,doMultipleShot);
}

#define SUMMON_MON_TARGET 0
#define SUMMON_MON_LIFETIME 1
#define SUMMON_MON_MAX 2

void onSummonMonTick(int *t)
{
    int mon=t[SUMMON_MON_TARGET];

    if (CurrentHealth(mon))
    {
        if (--t[SUMMON_MON_LIFETIME]>=0)
        {
            if (CurrentHealth(GetOwner(mon)))
            {
                PushTimerQueue(10,t,onSummonMonTick);
                return;
            }
        }
        EnchantOff(mon,EnchantList(ENCHANT_SHIELD));
        EnchantOff(mon,EnchantList(ENCHANT_INVULNERABLE));
        Damage(mon,0,9999,DAMAGE_TYPE_PLASMA);
    }
    FreeSmartMemEx(t);
}

void setSummonMonLifeTime(int mon, int count)
{
    int *t;

    AllocSmartMemEx(SUMMON_MON_MAX*4,&t);
    t[SUMMON_MON_TARGET]=mon;
    t[SUMMON_MON_LIFETIME]=count;
    PushTimerQueue(1,t,onSummonMonTick);
}

void summonBlackShadow(int owner, float x, float y)
{
    int sd=CreateObjectById(OBJ_SHADE,x,y);

    ShadeSubProcess(sd);
    SetOwner(owner,sd);
    AttachHealthbar(sd);
    LookAtObject(sd,owner);
    DeleteObjectTimer(CreateObjectById(OBJ_SMOKE,x,y), 12);
    PushTimerQueue(1,sd,DeferredFollowUnit);
    setSummonMonLifeTime(sd,38);
}

void UserSkillSummonBlackShadow(int pIndex, int cre, int *pCooldown)
{
    ///here
    float x=GetObjectX(cre),y=GetObjectY(cre);
    float xvect=UnitAngleCos(cre,13.0),yvect=UnitAngleSin(cre,13.0);
    float xpos=x+xvect,ypos=y+yvect;

    summonBlackShadow(cre, xpos,ypos);
    summonBlackShadow(cre, xpos,ypos);
    summonBlackShadow(cre, xpos,ypos);
    pCooldown[0]+=15;
}

//

void invokeSkillProto(int fn, int pIndex, int cre, int *pCooldown)
{
    Bind(fn,&pIndex);
}

void invokeCompleteProto(int fn, int pIndex)
{ Bind(fn,&pIndex);}

#define DEC_COOLDOWN_PINDEX 0
#define DEC_COOLDOWN_COMPLETE_FN 1
#define DEC_COOLDOWN_SETCOOL_FN 2
#define DEC_COOLDOWN_COOL 3
#define DEC_COOLDOWN_MAX 4

void decreaseSkillCooldown(int *d)
{
    int pIndex=d[DEC_COOLDOWN_PINDEX];
    int user=GetPlayer(pIndex);

    if (MaxHealth(user))
    {
        if (--d[DEC_COOLDOWN_COOL]>=0)
        {
            PushTimerQueue(1,d,decreaseSkillCooldown);
            return;
        }
        Bind(d[DEC_COOLDOWN_COMPLETE_FN], &pIndex);
    }
    int args[]={pIndex,0};
    Bind(d[DEC_COOLDOWN_SETCOOL_FN], args);
    FreeSmartMemEx(d);
}

#define ONE_SECOND_FRAME 30
int invokeSkillFunction(int skillFn, int pIndex, int cre, int cooldown, int coolfn, int coolsetFn, int completeFn)
{
    if (!skillFn) return FALSE;

    int cool = Bind(coolfn, &pIndex);
    
    if (cool) return FALSE;
    invokeSkillProto(skillFn, pIndex,cre, &cooldown);
    int setArgs[]={pIndex,cooldown};
    Bind(coolsetFn, setArgs);
    int *decData;

    AllocSmartMemEx(DEC_COOLDOWN_MAX*4,&decData);
    decData[DEC_COOLDOWN_PINDEX]=pIndex;
    decData[DEC_COOLDOWN_SETCOOL_FN]=coolsetFn;
    decData[DEC_COOLDOWN_COOL]=cooldown;
    decData[DEC_COOLDOWN_COMPLETE_FN]=completeFn;
    PushTimerQueue(1, decData, decreaseSkillCooldown);
    return TRUE;
}

int DoFirstSkill(int pIndex, int cre, int completeFn)
{
    return invokeSkillFunction(GetUserSkill1(pIndex), pIndex, cre, 10*ONE_SECOND_FRAME, GetUserCooldown1, SetUserCooldown1, completeFn);
}

int DoSecondSkill(int pIndex, int cre, int completeFn)
{ return invokeSkillFunction(GetUserSkill2(pIndex), pIndex, cre,20*ONE_SECOND_FRAME, GetUserCooldown2, SetUserCooldown2, completeFn); }

int DoLastSkill(int pIndex, int cre, int completeFn)
{ return invokeSkillFunction(GetUserSkill3(pIndex), pIndex, cre,40*ONE_SECOND_FRAME, GetUserCooldown3, SetUserCooldown3, completeFn); }


#define CRE_ATTACKDATA_CRE 0
#define CRE_ATTACKDATA_PTR 1
#define CRE_ATTACKDATA_ENEMY 3
#define CRE_ATTACKDATA_SUBUNIT 4
#define CRE_ATTACKDATA_MAX 5

void endCreatureAttack(int *d)
{
    int cre=d[CRE_ATTACKDATA_CRE];

    if (CurrentHealth(cre))
    {
        CreatureIdle(cre);
    }
    int *p=d[CRE_ATTACKDATA_PTR];

    p[0]=0;
    FreeSmartMemEx(d);
}

void onCreatureMissileCollide()
{
    if (GetCaller()==0)
    {
        int wall = WallUtilGetWallAtObjectPosition(SELF);
        if (wall)
            WallBreak(wall);
    }
    else if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(SELF), lv=0;
        int pIndex=GetPlayerIndex(owner);

        if (IsPlayerUnit(owner))
            lv=GetCreatureLevel( pIndex );
        
        if (IsAttackedBy(OTHER,owner))
        {
            Damage(OTHER,GetOwner(SELF), GetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE) +(lv*2), DAMAGE_TYPE_PLASMA);
        }
    }
    else return;
    GreenSparkAt(GetObjectX(SELF),GetObjectY(SELF));
    Delete(SELF);
}

void getNearlyEnemy()
{
    int *d;

    HashGet(GenericHash(), GetTrigger(), &d, FALSE);
    int enemy=d[CRE_ATTACKDATA_ENEMY];

    if (CurrentHealth(enemy))
    {
        float dist=DistanceUnitToUnit(SELF, enemy);

        if (DistanceUnitToUnit(SELF, OTHER)>=dist)
            return;
    }
    d[CRE_ATTACKDATA_ENEMY] =GetCaller();
}

#define CRE_MISSILE_FORCE 9.0
void doCreatureAttack(int *d)
{
    int cre=d[CRE_ATTACKDATA_CRE], enemy=d[CRE_ATTACKDATA_ENEMY];
    float x=GetObjectX(cre),y=GetObjectY(cre),vectX,vectY;
    
    if (enemy)
    {
        vectX=UnitRatioX(enemy,cre,3.0);
        vectY=UnitRatioY(enemy,cre,3.0);
    }
    else
    {
        vectX=UnitAngleCos(cre,3.0);
        vectY=UnitAngleSin(cre,3.0);
    }
    MonsterForceCastSpell(cre,0,x+vectX,y+vectY);
    int mis=CreateObjectById(OBJ_DEATH_BALL_FRAGMENT,x+(vectX*3.0),y+(vectY*3.0));

    SetOwner(cre, mis);
    SetUnitCallbackOnCollide(mis,onCreatureMissileCollide);
    PushObjectTo(mis,vectX*CRE_MISSILE_FORCE,vectY*CRE_MISSILE_FORCE);
    HashGet(GenericHash(),d[CRE_ATTACKDATA_SUBUNIT],NULLPTR, TRUE);
}
void checkBeforeCreatureAttack(int *d)
{
    int cre = d[CRE_ATTACKDATA_CRE];

    if (!CurrentHealth(cre))
        return;

    int sub=CreateObjectById(OBJ_WEIRDLING_BEAST,GetObjectX(cre)+UnitAngleCos(cre,3.0),GetObjectY(cre)+UnitAngleSin(cre,3.0));

    LookWithAngle(sub,GetDirection(cre));
    SetOwner(GetOwner(cre), sub);
    DeleteObjectTimer(sub,1);
    UnitNoCollide(sub);
    SetCallback(sub,3,getNearlyEnemy);
    HashPushback( GenericHash(), sub, d);
    d[CRE_ATTACKDATA_SUBUNIT]=sub;
}

void defaultAttackCommon(int cre, int pIndex, int attackFn, int endFrame)
{
    int attackData[MAX_PLAYER_COUNT];
    
    if (attackData[pIndex]) return;
    int *p;

    AllocSmartMemEx(CRE_ATTACKDATA_MAX*4,&p);
    attackData[pIndex]=p;
    p[CRE_ATTACKDATA_CRE]=cre;
    p[CRE_ATTACKDATA_PTR]=&attackData[pIndex];
    p[CRE_ATTACKDATA_ENEMY]=0;
    PushTimerQueue(1, p, checkBeforeCreatureAttack);
    PushTimerQueue(2, p, attackFn);
    PushTimerQueue(endFrame, p, endCreatureAttack);
}

void DryadDefaultAttack(int cre, int pIndex)
{
    defaultAttackCommon(cre,pIndex,doCreatureAttack,13);
}

void destroyFrontWall(float *point, float *vect, int range)
{
    int wall;

    TeleportLocation(1,point[0]+vect[0],point[1]+vect[1]);
    while (--range>=0)
    {
        if (LocationX(1)<100.0)
            break;
        if (LocationY(1)<100.0)
            break;
        if (LocationX(1)>5500.0)
            break;
        if (LocationY(1)>5500.0)
            break;
        wall=WallUtilGetWallAtObjectPosition(1);
        if (wall)
        {
            WallBreak(wall);
            break;
        }
        TeleportLocationVector(1,vect[0],vect[1]);
    }
}

void doHorrendousAttack(int *d)
{
    int cre=d[CRE_ATTACKDATA_CRE], enemy=d[CRE_ATTACKDATA_ENEMY];
    float x=GetObjectX(cre),y=GetObjectY(cre),vectX,vectY;

    if (CurrentHealth(cre))
    {
        int pIndex=GetPlayerIndex(GetOwner(cre));

        if (pIndex>=0)
        {
            if (enemy)
            {
                vectX=UnitRatioX(enemy,cre,3.0);
                vectY=UnitRatioY(enemy,cre,3.0);
            }
            else
            {
                vectX=UnitAngleCos(cre,3.0);
                vectY=UnitAngleSin(cre,3.0);
            }
            HitLocation(cre,x+vectX,y+vectY);
            float wv[]={vectX*6.0,vectY*6.0};
            destroyFrontWall(&x, wv, 10);
            Damage(enemy,cre,GetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE) +(GetCreatureLevel(pIndex)*2), DAMAGE_TYPE_PLASMA);
            Effect("DAMAGE_POOF",GetObjectX(enemy),GetObjectY(enemy),0.0,0.0);
        }
    }
    HashGet(GenericHash(),d[CRE_ATTACKDATA_SUBUNIT],NULLPTR, TRUE);
}

void HorrendousDefaultAttack(int cre, int pIndex)
{
    defaultAttackCommon(cre,pIndex,doHorrendousAttack,18);
}

#define MYSTIC_ATTACKDATA_SUB 0
#define MYSTIC_ATTACKDATA_COUNT 1
#define MYSTIC_ATTACKDATA_ENEMY 2
#define MYSTIC_ATTACKDATA_MAX 3

void mysticAttackProgress(int *d)
{
    int sub = d[MYSTIC_ATTACKDATA_SUB];

    if (ToInt(GetObjectX(sub)))
    {
        if (--d[MYSTIC_ATTACKDATA_COUNT]>=0)
        {
            int owner=GetOwner(sub);

            if (IsVisibleOr(owner,sub))
            {
                int enemy=d[MYSTIC_ATTACKDATA_ENEMY];

                if (CurrentHealth(enemy))
                    LookAtObject(sub,enemy);
                int wall=WallUtilGetWallAtObjectPosition(sub);
                if (wall)
                    WallBreak(wall);
                MoveObjectVector(sub,UnitAngleCos(sub,11.0),UnitAngleSin(sub,11.0));
                PushTimerQueue(1,d,mysticAttackProgress);
                return;
            }
        }
        Delete(sub);
    }
    FreeSmartMemEx(d);
}

void onMysticAttackCollide()
{
    while (TRUE)
    {
        if (!GetCaller())
        {
            WallBreak(WallUtilGetWallAtObjectPosition(SELF));
        }
        else if (CurrentHealth(OTHER))
        {
            int owner = GetUnitTopParent(SELF),pIndex,dam=1;

            if (IsPlayerUnit(owner))
            {
                pIndex=GetPlayerIndex(owner);
                dam=GetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE) +(GetCreatureLevel(pIndex)*2);
            }
            if (!IsAttackedBy(OTHER, owner))
                return;
            Damage(OTHER, GetOwner(SELF), dam, DAMAGE_TYPE_PLASMA);
        }
        else return;
        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(SELF);
        break;
    }
}

void doMysticAttack(int *d)
{
    int cre=d[CRE_ATTACKDATA_CRE], enemy=d[CRE_ATTACKDATA_ENEMY];
    float x=GetObjectX(cre),y=GetObjectY(cre);

    if (CurrentHealth(cre))
    {
        int sub=CreateObjectById(OBJ_FEAR, x,y),*data;

        AllocSmartMemEx(MYSTIC_ATTACKDATA_MAX*4,&data);
        data[MYSTIC_ATTACKDATA_SUB]=sub;
        data[MYSTIC_ATTACKDATA_COUNT]=22;
        data[MYSTIC_ATTACKDATA_ENEMY]=enemy;
        Frozen(sub,TRUE);
        SetUnitFlags(sub,GetUnitFlags(sub)^(UNIT_FLAG_NO_PUSH_CHARACTERS|UNIT_FLAG_SHORT));
        SetUnitCallbackOnCollide(sub,onMysticAttackCollide);
        SetOwner(cre,sub);
        if (IsVisibleOr(enemy,cre))
        {
            LookAtObject(cre,enemy);
        }
        LookWithAngle(sub,GetDirection(cre));
        MonsterForceCastSpell(cre,0,x+UnitAngleCos(cre,33.0),y+UnitAngleSin(cre,33.0));
        PushTimerQueue(1,data,mysticAttackProgress);
    }
}

void MysticDefaultAttack(int cre, int pIndex)
{
    defaultAttackCommon(cre,pIndex,doMysticAttack, 30);
}

void onFireboomExplosionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            int owner = GetOwner(SELF);
            int pIndex=GetPlayerIndex(owner);

            if (!IsPlayerUnit(owner))
                return;
            int lv=GetCreatureLevel( pIndex );
            Damage(OTHER,SELF, GetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE) +(lv*2), DAMAGE_TYPE_PLASMA);
            Effect("SPARK_EXPLOSION",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
        }
    }
}

void onKillerianMisCollide()
{
    if (GetCaller()==0)
    {
        int wall = WallUtilGetWallAtObjectPosition(SELF);
        if (wall)
            WallBreak(wall);
    }
    else if (CurrentHealth(OTHER))
    {        
        if (IsAttackedBy(OTHER,GetOwner(SELF)))
        {
            SplashDamageAtEx(GetOwner(SELF),GetObjectX(SELF),GetObjectY(SELF), 35.0, onFireboomExplosionSplash);
        }
    }
    else return;
    DeleteObjectTimer(CreateObjectById(OBJ_EXPLOSION,GetObjectX(SELF),GetObjectY(SELF)), 12);
    Delete(SELF);
}

#define KILLERIAN_MISSILE_FORCE 11.0
void doKillerianDefaultAttack(int *d)
{
    int cre=d[CRE_ATTACKDATA_CRE], enemy=d[CRE_ATTACKDATA_ENEMY];
    float x=GetObjectX(cre),y=GetObjectY(cre),vectX,vectY;
    
    if (enemy)
    {
        vectX=UnitRatioX(enemy,cre,3.0);
        vectY=UnitRatioY(enemy,cre,3.0);
    }
    else
    {
        vectX=UnitAngleCos(cre,3.0);
        vectY=UnitAngleSin(cre,3.0);
    }
    // MonsterForceCastSpell(cre,0,x+vectX,y+vectY);
    HitLocation(cre,x+vectX,y+vectY);
    int mis=CreateObjectById(OBJ_STRONG_FIREBALL,x+(vectX*3.0),y+(vectY*3.0));

    SetOwner(cre, mis);
    LookWithAngle(mis,GetDirection(cre));
    SetUnitCallbackOnCollide(mis,onKillerianMisCollide);
    PushObjectTo(mis,vectX*CRE_MISSILE_FORCE,vectY*CRE_MISSILE_FORCE);
    HashGet(GenericHash(),d[CRE_ATTACKDATA_SUBUNIT],NULLPTR, TRUE);
}

void KillerianDefaultAttack(int cre, int pIndex)
{
    defaultAttackCommon(cre,pIndex,doKillerianDefaultAttack, 30);
}

int FireSpriteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 200; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1801545047; arr[38] = 1701996870; 
		arr[39] = 1819042146; arr[53] = 1133903872; arr[55] = 13; arr[56] = 21; arr[58] = 5545344; 
	pArr = arr;
	return pArr;
}

void FireSpriteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = FireSpriteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


void UserSkillSummonFireboom(int pIndex, int cre, int *pCooldown)
{
    float x=GetObjectX(cre),y=GetObjectY(cre);
    int sd=CreateObjectById(OBJ_FIRE_SPRITE,x+UnitAngleCos(cre,9.0),y+UnitAngleSin(cre,9.0));

    FireSpriteSubProcess(sd);
    SetOwner(cre,sd);
    AttachHealthbar(sd);
    LookWithAngle(sd,GetDirection(cre));
    DeleteObjectTimer(CreateObjectById(OBJ_SMOKE,x,y), 12);
    PushTimerQueue(1,sd,DeferredFollowUnit);
    setSummonMonLifeTime(sd,15);
}
