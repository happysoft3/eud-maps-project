
#include "q048601_weapon.h"
#include "q048601_mobs.h"
#include "q048601_item.h"
#include "q048601_shop.h"
#include "q048601_resource.h"
#include "libs/waypoint.h"
#include "libs/cmdline.h"
#include "libs/format.h"
#include "libs/playerinfo.h"
#include "libs/fxeffect.h"
#include "libs/hash.h"
#include "libs/coopteam.h"
#include "libs/buff.h"
#include "libs/bind.h"
#include "libs/networkRev.h"
#include "libs/logging.h"
#include "libs/indexloop.h"
#include"libs/gui_window.h"
#include "libs/spellutil.h"
#include "libs/meleeattack.h"
#include "libs/playerupdate.h"
#include "libs/voiceList.h"

int m_monStrikeHash;
int m_tinyMobHash;
int m_monSightHash;
int m_dialogCtx;
int m_masterUnit;
short m_specifyKeyId;
int m_unitDeathCount;
short *m_tinyMobName;
int m_tinyMobCount;
short *m_tinyMobHp;
int m_genericHash;
string m_popupMessage[32];

int m_lastCreatedUnit;
int m_unitscanHashInstance;

string *m_weaponNameTable;
int m_weaponNameTableLength;
string *m_armorNameTable;
int m_armorNameTableLength;
string *m_potionNameTable;
int m_potionNameTableLength;
string *m_staffNameTable;
int m_staffNameTableLength;
int *m_itemFunctionTable;
int m_itemFunctionTableLength;

int m_potionHash;

int m_pFlag[MAX_PLAYER_COUNT];
int m_player[MAX_PLAYER_COUNT];
int m_pLastItem[MAX_PLAYER_COUNT];
int m_carrot[MAX_PLAYER_COUNT];

#define PLAYER_DEATH_FLAG 0x80000000
#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10


string *m_specialWeaponNameArray;
int m_specialWeaponNameArrayCount;
int *m_specialWeaponPays;
int *m_specialWeaponFn;
int *m_specialWeaponMessages;

#include "libs/weapon_effect.h"

///Special weapon settings ///

////

int *m_pWeaponUserSpecialProperty1;
int *m_pWeaponUserSpecialPropertyExecutor1;
// int *m_pWeaponUserSpecialPropertyFxCode1;

int *m_pWeaponUserSpecialProperty2;
int *m_pWeaponUserSpecialPropertyExecutor2;
// int *m_pWeaponUserSpecialPropertyFxCode2;

int *m_pWeaponUserSpecialProperty3;
int *m_pWeaponUserSpecialPropertyExecutor3;
// int *m_pWeaponUserSpecialPropertyFxCode3;

int *m_pWeaponUserSpecialProperty4;
int *m_pWeaponUserSpecialPropertyExecutor4;
// int *m_pWeaponUserSpecialPropertyFxCode4;

int *m_pWeaponUserSpecialProperty5;
int *m_pWeaponUserSpecialPropertyExecutor5;
// int *m_pWeaponUserSpecialPropertyFxCode5;

int *m_pWeaponUserSpecialProperty6;
int *m_pWeaponUserSpecialPropertyExecutor6;
// int *m_pWeaponUserSpecialPropertyFxCode6;

static int GenericHash() //virtual
{
    return m_genericHash;
}

static int MasterUnit() //virtual
{
    return m_masterUnit;
}

static int MonsterSightHash() //virtual
{
    return m_monSightHash;
}

static void invokeMonsterStrikeHandler(int fn, int attacker, int victim)
{
    Bind(fn, &attacker);
}

static void MonsterStrikeDefaultCallback()
{
    int fn;

    if (!HashGet(m_monStrikeHash, GetUnitThingID(SELF), &fn, FALSE))
        return;

    invokeMonsterStrikeHandler(fn, GetTrigger(), GetCaller());
}

static void initMonsterStrikeFunction()
{
    HashCreateInstance(&m_monStrikeHash);
    InitialFillMonsterStrikeHash(m_monStrikeHash);
}

static void reportWarAbilityCooldown(int user, short abilityId, char cop)
{
    char code[]={
         0x56, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0xF0, 0xB8, 
         0x00, 0x81, 0x4D, 0x00, 0xFF, 0x36, 0xFF, 0x76, 0x04, 0xFF, 0x76, 0x08, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5E, 0x31, 0xC0, 0xC3 };

    // int args[]={0, abilityId, UnitToPtr(user)};
    int args[]={cop, abilityId, UnitToPtr(user)};
    int *pExec=0x5c336c;
		int *pOld=pExec[0];
		pExec[0]= code;
		Unknownb8(args);
		pExec[0]=pOld;
}

static void setAbilityCooldown(int user, int abilityId, int cooldown)
{
    SpellUtilSetPlayerAbilityCooldown(user, abilityId, cooldown);
    reportWarAbilityCooldown(user, abilityId, cooldown==0);
}

static void onTransportCollide()
{
    if (CurrentHealth(OTHER))
    {
        int destination=GetTrigger()+1;

        if (MaxHealth(destination))
            MoveObject(OTHER, GetObjectX(destination), GetObjectY(destination));
    }
}

static void placingTransport(short srcLocation, short destLocation)
{
    int t=DummyUnitCreateById(OBJ_BOMBER, LocationX(srcLocation), LocationY(srcLocation));
    int sub= DummyUnitCreateById(OBJ_WEIRDLING_BEAST, LocationX(destLocation), LocationY(destLocation));
    UnitNoCollide(sub);
    SetCallback(t, 9, onTransportCollide);
}

static int GetPotionHash() //override
{
    return m_potionHash;
}

static void initPotionHash()
{
    HashCreateInstance(&m_potionHash);
    FillElemPotionHash(m_potionHash);
}

//override
static void SetItemFunctionPtr(int *p,int count) //override
{
    m_itemFunctionTable=p;
    m_itemFunctionTableLength=count;
}
static string GetWeaponName()
{
    return m_weaponNameTable[Random(0, m_weaponNameTableLength-1)];
}
static string GetArmorName()
{
    return m_armorNameTable[Random(0, m_armorNameTableLength-1)];
}
static string GetPotionName()
{
    return m_potionNameTable[Random(0, m_potionNameTableLength-1)];
}
static string GetWandName()
{return m_staffNameTable [Random(0, m_staffNameTableLength-1)];}
static void SetItemNameWeapon(int *p, int count)
{
    m_weaponNameTable=p;
    m_weaponNameTableLength=count;
}
static void SetItemNameArmor(int *p, int count)
{
    m_armorNameTable=p;
    m_armorNameTableLength=count;
}
static void SetItemNamePotion(int *p,int count)
{
    m_potionNameTable=p;
    m_potionNameTableLength=count;
}
static void SetItemNameStaff(int *p,int count)
{
    m_staffNameTable=p;
    m_staffNameTableLength=count;
}

static int PotionPickupMaxCount()
{
    return MAX_POTION_COUNT;
}

static int BeforeCarrotBought(int user, int pIndex) //override
{
    if (MaxHealth( m_carrot[pIndex]) )
    {
        UniPrint(user, "이미 붉은 용가리이 존재합니다 (유저 당 1개만 보유가능)!");
        return FALSE;
    }
    return TRUE;
}

static void AfterCarrotBought(int user, int carrot, int pIndex) //override
{
    m_carrot[pIndex]=carrot;
    UniPrint(user, "붉은 용가리 구입완료요~");
}

void ChakramCopyProperies(int src, int dest)
{
    int *srcPtr = UnitToPtr(src), *destPtr = UnitToPtr(dest);
    
    int *srcProperty = srcPtr[173], *destProperty = destPtr[173];

    int rep=-1;

    while (++rep<4)
        destProperty[rep] = srcProperty[rep];

    int rep2=-1;

    while (++rep2<32)
        destPtr[140+rep2] = srcPtr[140+rep2];
}

static void CreateChakram(float xpos, float ypos, int *pDest, int owner)
{
    int mis = CreateObjectAt("RoundChakramInMotion", xpos, ypos);
    int *ptr=UnitToPtr(mis);

    if (pDest)
        pDest[0] = mis;

    ptr[174] = 5158032;
    ptr[186] = 5483536;
    SetOwner(owner, mis);

    int *collideDataMb = MemAlloc(8);
    collideDataMb[0]=0;
    collideDataMb[1] = UnitToPtr(owner);
    ptr[175] = collideDataMb;
}

void OnChakramTracking(int owner, int cur)
{
    int mis;

    CreateChakram(GetObjectX(owner) + UnitAngleCos(owner, 11.0), GetObjectY(owner) + UnitAngleSin(owner, 11.0), &mis, owner);
    ChakramCopyProperies(cur, mis);

    PushObject(mis, 30.0, GetObjectX(owner), GetObjectY(owner));
}

void AfterChakramTracking(int *pMem)
{
    if (!pMem)
        return;

    int inv = pMem[0], owner = pMem[1];

    if (CurrentHealth(owner) && IsObjectOn(inv))
    {
        Pickup(owner, inv);
    }
    FreeSmartMemEx(pMem);
}

static void HookChakrm(int cur, int owner)
{
    // UnitSetEnchantTime(cur, ENCHANT_RUN, 0);
    // Enchant(cur, EnchantList(ENCHANT_ETHEREAL),0.0);

    int *ptr=UnitToPtr(cur);

    if (ptr[186] == 5496000)
    {
        if (!CurrentHealth(owner))
            return;

        int *pMem;

        AllocSmartMemEx(8, &pMem);
        pMem[0] = GetLastItem(cur);
        pMem[1] = owner;
        OnChakramTracking(owner, cur); //이게 여기로 옮겨지고 cur 인자를 더 전달합니다
        Delete(cur);
        FrameTimerWithArg(2, pMem, AfterChakramTracking);
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    InitialFillIndexLoopHash(pInstance);
    HashPushback(pInstance, OBJ_ROUND_CHAKRAM_IN_MOTION, HookChakrm);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
    ResetHostileCritter();
}

void OnEndedUnitScan()
{
    UniPrintToAll("dim.c::OnEndedUnitScan");
}

static void onUnitExchangeRewardmarker(int sub)
{
    CreateRandomItemCommon(sub, m_itemFunctionTable, m_itemFunctionTableLength);
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 85; 
		arr[19] = 55; arr[23] = 34816; arr[24] = 1071225242; arr[26] = 4;
		arr[28] = 1112014848; arr[29] = 3; arr[31] = 8; arr[32] = 14; arr[33] = 24; 
		arr[59] = 5542784; arr[60] = 1388; arr[61] = 46915072; 
	pArr = &arr;
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 85;	hpTable[1] = 85;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

static void invokeTinySpecial(int fn, int mon)
{
    Bind(fn, &fn+4);
}

static void spawnTinyRandomMob(float x,float y)
{
    int pic=Random(0,m_tinyMobCount-1);
    int mon=CreateObjectById(m_tinyMobName[pic],x,y);

    SetUnitMaxHealth(mon,m_tinyMobHp[pic]);
    commonMonsterProperty(mon);
    int fn;

    if (HashGet(m_tinyMobHash,m_tinyMobName[pic],&fn,FALSE))
        invokeTinySpecial(fn,mon);
}

static void onUnitExchangeNecroMarker(int sub)
{
    float x=GetObjectX(sub),y=GetObjectY(sub);
    
    Delete(sub);
    spawnTinyRandomMob(x,y);
}

static void UnitScanProcessProto(int functionId, int posUnit)
{
    Bind(functionId, &functionId + 4);
}

void UnitScanLoop(int cur)
{
    int rep = 30, action, thingId;

    while (rep--)
    {
        if (++cur >= m_lastCreatedUnit)
        {
            OnEndedUnitScan();
            return;
        }
        thingId=GetUnitThingID(cur);
        if (thingId)
        {
            if (HashGet(m_unitscanHashInstance, thingId, &action, FALSE))
                UnitScanProcessProto(action, cur);
        }
    }
    PushTimerQueue(1, cur, UnitScanLoop);
}

void startUnitScan(int cur)
{
    InitItemNames();
    HashCreateInstance(&m_unitscanHashInstance);
    HashPushback(m_unitscanHashInstance, OBJ_REWARD_MARKER, onUnitExchangeRewardmarker);
    HashPushback(m_unitscanHashInstance, OBJ_NECROMANCER_MARKER, onUnitExchangeNecroMarker);
    UniPrintToAll("mansion2.c::StartUnitScan");
    UnitScanLoop(cur);
}

static void initDialog()
{
    int *wndptr=0x6e6a58;
    GUIFindChild(wndptr[0], 3901, &m_dialogCtx);
}

static void initPopupMessage()
{
    initDialog();
    initspecialWeaponShop();
    InitFillPopupMessageArray(m_popupMessage);
}

static void onPopupMessageChanged(int messageId)
{
    GUISetWindowScrollListboxText(m_dialogCtx, m_popupMessage[messageId], NULLPTR);
}

static void deferredChangeDialogContext(int *pParams) //override
{
    int user=pParams[0];
    int message=pParams[1];

    FreeSmartMemEx(pParams);
    if (!CurrentHealth(user))
        return;

    if (user==GetHost())
    {
        onPopupMessageChanged(message);
        return;
    }
    ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, message);
}

static void dispositionMagicalShop()
{
    placingArmorShop(94,ITEM_PROPERTY_LightningProtect4,ITEM_PROPERTY_FireProtect4);
    placingArmorShop(95,ITEM_PROPERTY_Regeneration4,ITEM_PROPERTY_PoisonProtect4);
    placingArmorShop(96,ITEM_PROPERTY_Regeneration4,ITEM_PROPERTY_Speed4);
    placingWeaponShop(97,ITEM_PROPERTY_lightning4,ITEM_PROPERTY_stun4);
    placingWeaponShop(98,ITEM_PROPERTY_vampirism4,ITEM_PROPERTY_fire4);
    placingWeaponShop(99,ITEM_PROPERTY_venom4,ITEM_PROPERTY_vampirism4);
    placingInvincibleItemShop(100);
    placingPotionShop(137);
    placingHealingPotionShop(138);
    PlaceSpecialWeaponShop(101);
    placingCarrotShop(134);

    int speedShop =placingArmorShop(133,ITEM_PROPERTY_Speed4,ITEM_PROPERTY_Speed4);
    ShopUtilSetTradePrice(speedShop, 2.1, 0.0);
    MoveObject(Object("skillShop"),LocationX(102),LocationY(102));
}

int DispositionDeathraySword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_confuse1, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty6);
    return sd;
}

int DispositionArrowRainSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty1);
    return sd;
}

int DispositionWolfSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty2);
    return sd;
}

int DispositionAutoTrackingSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty3);
    return sd;
}

int DispositionYellowLightningSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("GreatSword", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_vampirism4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty4);
    return sd;
}

int DispositionTripleArrowHammer(float xpos, float ypos)
{
    int sd=CreateObjectAt("WarHammer", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_confuse3, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty5);
    return sd;
}

static void talkJandor2()
{
    char msg[128];

    if (m_unitDeathCount<20)
    {
        int rem=20-m_unitDeathCount;
        NoxSprintfString(msg, "오, 이런.. 아직 부족합니다. 아직 %d 개 남았으니 더 사냥을 하시오", &rem, 1);
        UniChatMessage(SELF, ReadStringAddressEx(msg), 150);
        return;
    }
    int key=CreateObjectById(m_specifyKeyId, GetObjectX(OTHER), GetObjectY(OTHER));

    PlaySoundAround(key, SOUND_KeyDrop);
    CancelDialog(SELF);
    dispositionMagicalShop();

    WallOpen(Wall(197,211));
    WallOpen(Wall(198,212));
    WallOpen(Wall(203,213));
    WallOpen(Wall(204,212));
    WallOpen(Wall(207,209));
    WallOpen(Wall(208,208));
    UniChatMessage(SELF, "오, 호호.. 여기, 무적의 아파트 열쇠입니다!", 180);
}

static void dropShortSword(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int count =GetDirection(sub);
        if (count)
        {
            PushTimerQueue(1,sub,dropShortSword);
            LookWithAngle(sub,count-1);
            int sword=CreateObjectById(OBJ_SWORD, GetObjectX(sub),GetObjectY(sub));

            SetWeaponPropertiesDirect(sword, ITEM_PROPERTY_weaponPower2, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_fire2, NULLPTR);
            SetUnitMaxHealth(sword, MaxHealth(sword)*4);
            UnitNoCollide(sword);
            return;
        }
        Delete(sub);
    }
}

static void talkJandor()
{
    int count=29;

    while (--count>=0)
        WallOpen(Wall(213+count, 239-count));
    UniChatMessage(SELF, "필드에서 괴물 20개를 잡아오면, 무적의 아파트 열쇠를 주겠습니다", 180);
    PlaySoundAround(SELF, SOUND_SwordsmanRecognize);
    SetDialog(SELF, "NORMAL", talkJandor2, talkJandor2);

    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(OTHER),GetObjectY(OTHER));

    LookWithAngle(sub, 32);
    PushTimerQueue(1, sub, dropShortSword);
}

static void placingJandor(short locationId)
{
    int npc=CreateObjectById(OBJ_AIRSHIP_CAPTAIN, LocationX(locationId), LocationY(locationId));

    Frozen(npc, TRUE);
    LookWithAngle(npc, 32);
    SetDialog(npc, "NORMAL", talkJandor, talkJandor);
    SetOwner(GetHost(), npc);
}

static void onEntranceFoilPickup()
{
    PlaySoundAround(OTHER, SOUND_ArcherDie);
    UniPrint(OTHER, "이것을 획득할 수 없습니다. 어서 무적의 아파트에 가세요");
}

static void initReadables()
{
    RegistSignMessage(Object("readable1"), "<<-무적의 아파트------ 제작. 237 ------<");
    RegistSignMessage(Object("readable2"), "강한자 만이 살아남을 수 있는, 무적의 아파트... 거길 가시려고요?");
    RegistSignMessage(Object("readable3"), "아파트 살면서 만날 수 있는 다양한 무개념 인간들을 표현했어요. 예를 들어, 발망치충, 실내흡연충, 괴성을 지르는 넘들..");
    int foil=CreateObjectById(OBJ_FOIL_CANDLE_LIT, LocationX(106),LocationY(106));

    Frozen(foil, TRUE);
    SetUnitCallbackOnPickup(foil,onEntranceFoilPickup);
    foil=CreateObjectById(OBJ_FOIL_CANDLE_LIT, LocationX(107),LocationY(107));

    Frozen(foil, TRUE);
    SetUnitCallbackOnPickup(foil,onEntranceFoilPickup);
}

static void deferredInit()
{
    HashCreateInstance(&m_genericHash);
    HashCreateInstance(&m_monSightHash);
    placingTransport(13,14);
    placingTransport(83,120);
    placingTransport(114,84);
    placingTransport(81,119);
    placingTransport(118,82);
    placingTransport(15,16);
    placingTransport(35,34);
    placingTransport(33,36);
    placingTransport(46,47);
    placingTransport(48,49);
    placingTransport(67,66);
    placingTransport(65,68);
    placingJandor(20);
    initReadables();

    SetMemory(0x5d5330, 0x2000);
    SetMemory(0x5d5394, 1);
}

void tinyMobWeirdlingBeast(int mon)
{
    WeirdlingBeastSubProcess(mon);
    SetUnitVoice(mon,MONSTER_VOICE__Maiden2);
}

int BomberBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1651339074; arr[1] = 29285; arr[17] = 98; arr[18] = 1; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1065353216; arr[26] = 4; arr[27] = 1; 
		arr[37] = 1399876937; arr[38] = 7630696; arr[53] = 1132068864; arr[55] = 13; arr[56] = 21; 
		arr[58] = 5546320; arr[60] = 1348; arr[61] = 46899968; 
	pArr = &arr;
	return pArr;
}

void BomberSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 98;	hpTable[1] = 98;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = BomberBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

static void tinyMobBomber(int mon)
{
    int *ptr=UnitToPtr(mon);
    
    SetMemory(ptr + 0x2b8, 0x4e83b0);
    BomberSubProcess(mon);
    TrapSpells(mon,"SPELL_METEOR","NULL","NULL");
}

int FishBigBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1752394054; arr[1] = 6777154; arr[17] = 108; arr[18] = 1; arr[19] = 95; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[27] = 5; arr[28] = 1113325568; 
		arr[29] = 6; arr[31] = 8; arr[32] = 16; arr[33] = 25; arr[59] = 5542784; 
		arr[60] = 1329; arr[61] = 46905600; 
	pArr = &arr;
	return pArr;
}

void FishBigSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077306982;		ptr[137] = 1077306982;
	int *hpTable = ptr[139];
	hpTable[0] = 108;	hpTable[1] = 108;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = FishBigBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

static void tinyMobFishBig(int mon)
{
    FishBigSubProcess(mon);
}

void tinySpider(int mon){
    SetUnitVoice(mon,MONSTER_VOICE_Beholder);
}

int ZombieBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1651339098; arr[1] = 25961; arr[16] = 30000; arr[17] = 75; arr[18] = 30; 
		arr[19] = 66; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[25] = 1; 
		arr[26] = 5; arr[27] = 5; arr[28] = 1106247680; arr[29] = 20; arr[31] = 10; 
		arr[32] = 3; arr[33] = 6; arr[59] = 5542784; arr[60] = 1360; arr[61] = 46895440; 
	pArr = &arr;
	return pArr;
}

void ZombieSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073574051;		ptr[137] = 1073574051;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = ZombieBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

static void onZombieLoop(int mirror)
{
    int mon=GetUnit1C(mirror);

    if (MaxHealth(mon))
    {
        if (CurrentHealth(mon))
        {
            PushTimerQueue(3, mirror, onZombieLoop);
            if (ToInt(DistanceUnitToUnit(mon,mirror)) )
            {
                MoveObject(mirror, GetObjectX(mon),GetObjectY(mon));
                LookWithAngle(mirror,GetDirection(mon));
            }
            return;
        }
        Damage(mon,0,1,DAMAGE_TYPE_FLAME);
        Delete(mirror);
    }
}

static void tinyMobZombie(int mon)
{
    int mirror=CreateObjectById(OBJ_ZOMBIE,GetObjectX(mon),GetObjectY(mon));
    SetUnitMaxHealth(mirror,0);
    Enchant(mirror,"ENCHANT_FREEZE",0.0);
    ObjectOff(mirror);
    UnitNoCollide(mirror);
    SetUnit1C(mirror, mon);
    PushTimerQueue(3, mirror,onZombieLoop);
    ZombieSubProcess(mon);
}

static void initTinyMons()
{
    short name[]={
        OBJ_WASP, OBJ_WEIRDLING_BEAST, OBJ_SWORDSMAN,OBJ_WOLF,
        OBJ_EVIL_CHERUB,OBJ_BLACK_BEAR,OBJ_BOMBER_YELLOW,OBJ_GIANT_LEECH,
        OBJ_ALBINO_SPIDER,OBJ_FISH_BIG,OBJ_FLYING_GOLEM,OBJ_TROLL,
        OBJ_SKELETON,OBJ_SMALL_SPIDER,OBJ_MELEE_DEMON,OBJ_SHADE,
        OBJ_ZOMBIE,
    };
    short hp[]={
        60, 75,100,110,
        98,200,98,135,
        138,108,64,200,
        100,225,98,98,
        1,
    };
    HashCreateInstance(&m_tinyMobHash);
    HashPushback(m_tinyMobHash,OBJ_WEIRDLING_BEAST,tinyMobWeirdlingBeast);
    HashPushback(m_tinyMobHash,OBJ_BOMBER_YELLOW,tinyMobBomber);
    HashPushback(m_tinyMobHash,OBJ_FISH_BIG,tinyMobFishBig);
    HashPushback(m_tinyMobHash,OBJ_ZOMBIE,tinyMobZombie);
    HashPushback(m_tinyMobHash,OBJ_SMALL_SPIDER,tinySpider);
    m_tinyMobName = name;
    m_tinyMobCount = sizeof(name);
    m_tinyMobHp=hp;
}

static void initializeUserSpecialWeapon()
{
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_RICOCHET, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty1);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartArrowRain, &m_pWeaponUserSpecialPropertyExecutor1);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyExecutor1);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty2);
    SpecialWeaponPropertyExecuteScriptCodeGen(startWolfWalking, &m_pWeaponUserSpecialPropertyExecutor2);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyExecutor2);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_THIN_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty3);
    SpecialWeaponPropertyExecuteScriptCodeGen(startAutoTrackingSword, &m_pWeaponUserSpecialPropertyExecutor3);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyExecutor3);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_YELLOW_SPARKS, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty4);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &m_pWeaponUserSpecialPropertyExecutor4);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyExecutor4);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SMOKE_BLAST, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty5);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartTripleArrowHammer, &m_pWeaponUserSpecialPropertyExecutor5);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyExecutor5);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_VIOLET_SPARK, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode6);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty6);
    SpecialWeaponPropertyExecuteScriptCodeGen(AutoDetectingTriggered, &m_pWeaponUserSpecialPropertyExecutor6);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty6, m_pWeaponUserSpecialPropertyFxCode6);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty6, m_pWeaponUserSpecialPropertyExecutor6);
}

static int GetSpecialWeaponMessage(int cur) //override
{
    return m_specialWeaponMessages[cur];
}
static string GetSpecialWeaponName(int cur) //override
{
    return m_specialWeaponNameArray[cur];
}
static int GetSpecialWeaponPay(int cur) //virtual
{
    return m_specialWeaponPays[cur];
}

static int GetSpecialWeaponFunction(int cur) //virtual
{
    return m_specialWeaponFn[cur];
}

static void initspecialWeaponShop()
{
    string desc[]={
        "화살비 서드", "늑데돌진서드", "극사 서드", "백만볼트 서드", 
        "트리플 고드름 해머", "데스레이 서드", };
    int pay[]={145308, 129930, 149900, 111200, 
        131900, 144300, };
    int specialFn[]={DispositionArrowRainSword ,DispositionWolfSword, DispositionAutoTrackingSword, DispositionYellowLightningSword,
        DispositionTripleArrowHammer, DispositionDeathraySword, };
    int messages[]={GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_1, GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_2,GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_3,GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_4,
        GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_5, GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_6,};
    m_specialWeaponNameArrayCount=sizeof(desc);
    m_specialWeaponNameArray = desc;
    m_specialWeaponPays=pay;
    m_specialWeaponFn=specialFn;
    m_specialWeaponMessages=messages;
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("q048601-log.txt");
    m_lastCreatedUnit=CreateObjectById(OBJ_RED_POTION, 100.0, 100.0);
    m_masterUnit=DummyUnitCreateById(OBJ_HECUBAH, 100.0, 100.0);
    m_specifyKeyId=OBJ_BLUE_ORB_KEY_OF_THE_LICH;
    PushTimerQueue(1, Object("firstscan"), startUnitScan);
    initTinyMons();
    initPotionHash();
    initPopupMessage();
    initDungeonFinish();
    initializeUserSpecialWeapon();
    PushTimerQueue(1, 0, MakeCoopTeam);
    PushTimerQueue(1, 0, deferredInit);
    initMonsterStrikeFunction();
    SetHostileCritter();
    WriteLog("mapinit-end");
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}

void PlayerClassOnShutdown(int pIndex)
{
    m_player[pIndex]=0;
    m_pFlag[pIndex]=0;
    if (MaxHealth( m_carrot[pIndex]) )
        Delete(m_carrot[pIndex]);

    char buff[128];

    NoxSprintfString(buff, "playeronshutdown. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void PlayerClassOnDeath(int pIndex, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

static int userAwardCommon(int user, int pIndex, int flag)
{
    if (PlayerClassCheckFlag(pIndex, flag))
    {
        UniPrint(user, "이미 그 기술을 배우셨어요");
        return FALSE;
    }
    PlayerClassSetFlag(pIndex,flag);
    float point[]={GetObjectX(user),GetObjectY(user)};
    GreenSparkFx(point[0],point[1]);
    Effect("WHITE_FLASH",point[0],point[1],0.0,0.0);
    PlaySoundAround(user,SOUND_AwardSpell);
    return FALSE;
}

static void onAwardThreadLightly(int pIndex)
{
    if (!userAwardCommon(m_player[pIndex], pIndex, PLAYER_FLAG_WINDBOOST))
        return;
}

static void onAwardWarcry(int pIndex)
{
    if (!userAwardCommon(m_player[pIndex], pIndex, PLAYER_FLAG_WARCRY))
        return;
}

static void onAwardBerserkerCharge(int pIndex)
{
    if (!userAwardCommon(m_player[pIndex], pIndex, PLAYER_FLAG_BERSERKER_CHARGE))
        return;
    setAbilityCooldown(m_player[pIndex],1,0);
}

static int onPlayerInventoryChanged(int inv, int pIndex)
{
    short thingId=GetUnitThingID(inv);

    if (thingId==OBJ_ABILITY_BOOK)
    {
        int *ptr=UnitToPtr(inv);
        char ty=GetMemory(GetMemory(ptr + 0x2e0));

        if (ty==4)
            onAwardThreadLightly(pIndex);
        else if (ty==2)
            onAwardWarcry(pIndex);
        else if (ty==1)
            onAwardBerserkerCharge(pIndex);

        Delete(inv);
        return 0;
    }
    return inv;
}

static void userCooldownHandler(int pIndex, int user)
{
    int cool=SpellUtilGetPlayerAbilityCooldown(user, 1);

    if (!(cool%8))
    {
        setAbilityCooldown(user, 1, 247);
    }
}

#define WINDBOOSTER_DISTANCE 70.0

static void skillSetWindBooster(int pUnit)
{
    PushObjectTo(pUnit, UnitAngleCos(pUnit, WINDBOOSTER_DISTANCE), UnitAngleSin(pUnit, WINDBOOSTER_DISTANCE));
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

// void ManaBombCancelFx(int sUnit)
// {
//     int caster = CreateObjectAt("ImaginaryCaster", GetObjectX(sUnit), GetObjectY(sUnit));

//     CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
//     Delete(caster);
// }

void onWarcrySplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (!(GetUnitClass(OTHER) & (UNIT_CLASS_MONSTER|UNIT_CLASS_PLAYER)))
            return;

        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER))
                Damage(OTHER, SELF, 135, DAMAGE_TYPE_PLASMA);
        }
    }
}

void SkillSetWarCry(int pUnit)
{
    SplashDamageAtEx(pUnit, GetObjectX(pUnit), GetObjectY(pUnit), 200.0, onWarcrySplash);
    // ManaBombCancelFx(pUnit);
}

int WarAbilityTable(int aSlot, int pIndex)
{
    return GetMemory(0x753600 + (pIndex * 24) + (aSlot * 4));
}

void WarAbilityUse(int pUnit, int aSlot, int actionFunction)
{
    int chk[160], pIndex = GetPlayerIndex(pUnit), cTime;
    int arrPic;

    if (!(pIndex >> 0x10))
    {
        arrPic = pIndex * 5 + aSlot; //EyeOf=5, harpoon=3, sneak=4, berserker=1
        cTime = WarAbilityTable(aSlot, pIndex);
        if (cTime ^ chk[arrPic])
        {
            if (!chk[arrPic])
            {
                CallFunctionWithArg(actionFunction, pUnit);
            }
            chk[arrPic] = cTime;
        }
    }
}

void PlayerClassSkillHandler(int pIndex, int user)
{
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WINDBOOST))
    {
        if (UnitCheckEnchant(user, GetLShift(ENCHANT_SNEAK)))
        {
            skillSetWindBooster(user);
            RemoveTreadLightly(user);
        }
    }
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WARCRY))
        WarAbilityUse(user, 2, SkillSetWarCry);
}

void PlayerClassOnAlive(int pIndex, int user)
{
    int inv=GetLastItem(user);

    if (inv!=m_pLastItem[pIndex])
        m_pLastItem[pIndex]=onPlayerInventoryChanged(inv, pIndex);

    if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_BERSERKER_CHARGE))
        userCooldownHandler(pIndex, user);
    PlayerClassSkillHandler(pIndex, user);
}

void PlayerClassOnLoop(int pIndex)
{
    int user = m_player[pIndex];

    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (m_pFlag[pIndex])
        {
            PlayerClassOnShutdown(pIndex);
            return;
        }
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[0])
    {
        onPopupMessageChanged(type[0]);
        type[0]=0;
    }
    FrameTimer(3, ClientProcLoop);
}

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    CommonServerClientProcedure();
    initPopupMessage();
    FrameTimer(10, ClientProcLoop);
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

void PlayerClassOnInit(int pIndex, int pUnit)
{
    m_player[pIndex]=pUnit;
    m_pFlag[pIndex]=1;
    ChangeGold(pUnit, -GetGold(pUnit));
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    EmptyAll(pUnit);

    char buff[128];

    NoxSprintfString(buff, "playeroninit. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
    WriteLog(ReadStringAddressEx( buff) );
}

void PlayerClassOnJoin(int user, int pIndex)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
    // if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_ALL_BUFF))
    //     PlayerSetAllBuff(pUnit);
    SetUnitEnchantCopy(user, GetLShift(ENCHANT_ANTI_MAGIC)|GetLShift(ENCHANT_ANCHORED));
    
    MoveObject(user, LocationX(PLAYER_START_LOCATION), LocationY(PLAYER_START_LOCATION));
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int pIndex)
{
    PlayerClassOnJoin(user, pIndex);
    // MoveObject(user, LocationX(474), LocationY(474));
    // Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    // PushTimerQueue(3, plr, PlayerClassOnJoin);
    // UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
}

void PlayerClassOnEntry(int plrUnit)
{
    if (!CurrentHealth(plrUnit))
        return;

    int pIndex = GetPlayerIndex(plrUnit);
    char initialUser=FALSE;

    if (!MaxHealth(m_player[pIndex]))
        PlayerClassOnInit(pIndex, plrUnit);
    if (pIndex >= 0)
    {
        if (initialUser)
            OnPlayerDeferredJoin(plrUnit, pIndex);
        else
            PlayerClassOnJoin(plrUnit, pIndex);
        return;
    }
    PlayerClassOnEntryFailure(plrUnit);
}

static int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(m_player[pIndex]))
        return m_player[pIndex]==pUnit;
    return FALSE;
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (checkIsPlayerAlive(GetPlayerIndex(OTHER), GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(2), LocationY(2));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

static void teleportPlayerAll(short location)
{
    float point[]={LocationX(location),LocationY(location)};
    int r=sizeof(m_player);
    while(--r>=0)
    {
        if (CurrentHealth(m_player[r]))
            MoveObject(m_player[r],point[0],point[1]);
    }
}

static void openArea2Entrance()
{
    WallOpen(Wall(12,118));
    WallOpen(Wall(13,117));
    WallOpen(Wall(14,116));
}
static void openArea3Entrance()
{
    WallOpen(Wall(8,200));
    WallOpen(Wall(9,199));
    WallOpen(Wall(10,198));
}
static void openArea4Entrance()
{
    WallOpen(Wall(23,65));
    WallOpen(Wall(24,64));
    WallOpen(Wall(25,63));
    WallOpen(Wall(26,62));
    WallOpen(Wall(27,61));
    WallOpen(Wall(28,60));
}
static void openArea5Entrance()
{
    WallOpen(Wall(91,217));
    WallOpen(Wall(92,216));
}
static void openArea6Entrance()
{
    WallOpen(Wall(121,147));
    WallOpen(Wall(122,146));
    WallOpen(Wall(123,145));
    WallOpen(Wall(124,144));
    WallOpen(Wall(125,143));
    WallOpen(Wall(126,142));
}

static void spawnUWON(int sub)
{
    int count=GetDirection(sub);

    if (count)
    {
        PushTimerQueue(2,sub,spawnUWON);
        LookWithAngle(sub,count-1);
        CreateObjectById(OBJ_FOIL_CANDLE_UNLIT, GetObjectX(sub),GetObjectY(sub));
        return;
    }
    CreateObjectById(OBJ_BLUE_RAIN, GetObjectX(sub), GetObjectY(sub));
    PlaySoundAround(sub, SOUND_StaffOblivionAchieve1);
    Delete(sub);
}

static void finalClear()
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, LocationX(135),LocationY(135));

    LookWithAngle(sub, 16);
    PushTimerQueue(1, sub, spawnUWON);
    teleportPlayerAll(135);
    UniPrintToAll("축하합니다-! 무적의 아파트를 클리어 하셨습니다--!");
}

int m_dungeonFinishCount;
int m_floorCount;
int m_finishCondition[7];
int *m_finishCb;

static void initDungeonFinish()
{
    m_finishCondition[0]=5;
    m_finishCondition[1]=3;
    m_finishCondition[2]=5;
    m_finishCondition[3]=4;
    m_finishCondition[4]=6;
    m_finishCondition[5]=5;
    m_finishCondition[6]=999999;
    int finishCb[]={openArea2Entrance,openArea3Entrance,openArea4Entrance,openArea5Entrance,openArea6Entrance,finalClear};
    m_finishCb=finishCb;
}

static void onMonsterDeath()
{
    int keyId;

    DeleteObjectTimer(SELF, 150);
    ++m_unitDeathCount;
    if (HashGet(m_genericHash, GetTrigger(), &keyId, TRUE))
    {
        int key=CreateObjectById(keyId, GetObjectX(SELF), GetObjectY(SELF));

        PlaySoundAround(key, SOUND_KeyDrop);
        if (++m_dungeonFinishCount==m_finishCondition[m_floorCount])
        {
            m_dungeonFinishCount=0;
            CallFunction(m_finishCb[m_floorCount++]);
            char buff[128];
            int floor=m_floorCount+1;
            NoxSprintfString(buff,"방금 %d 층으로 향하는 계단이 열렸습니다", &floor, 1);
            UniPrintToAll(ReadStringAddressEx(buff));
        }
        return;
    }
    PushTimerQueue(1, CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(SELF), GetObjectY(SELF)), onUnitExchangeRewardmarker);
}

static void onMonsterHurt()
{
    int poisonLv = IsPoisonedUnit(SELF);

    if (poisonLv)
    {
        Damage(SELF, 0, poisonLv, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

static void testTeleporting()
{
    MoveObject(OTHER, LocationX(136),LocationY(136));
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    if (pInfo==0x653a7c)
    {
        CommonServerClientProcedure();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}


