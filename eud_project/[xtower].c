
#include "xtower_gvar.h"
#include "xtower_main.h"
#include "libs\waypoint.h"
#include "libs\unitstruct.h"
#include "libs\printutil.h"
#include "libs\buff.h"

#include "libs\mathlab.h"


int LastUnitID = 920;

int m_floorLv;
static void QueryFloorLevel(int *get, int set){
    if (get) {get[0]=m_floorLv; return;}
    m_floorLv=set;
}//virtual

int Set_fptr, Next_fptr, Exit_wp, Start_wp;
// int Jandors[10], Plr_flag[10];
// int Weapon_fire4 = 0x5BA264, Weapon_fire3 = 0x5BA24C, Weapon_vam4 = 0x5BA3E4, Weapon_vam3 = 0x5BA3CC;
// int Weapon_blue4 = 0x5BA384, Weapon_blue3 = 0x5BA36C;
// int Armor_fire4 = 0x5BA4A4, Armor_blue4 = 0x5BA504, Armor_cure3 = 0x5BA5AC, Armor_wind3 = 0x5BA60C;
// int PlrSkill[10];

#include "libs\playerinfo.h"


#include "libs\reaction.h"
#include "libs\spellutil.h"

#include "libs\cmdline.h"

#include "libs\itemproperty.h"

void MapExit()
{
    OnShutdownMap();
}

void MapInitialize()
{
    OnInitializeMap();
}

int m_player[MAX_PLAYER_COUNT];
int m_pFlags[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex]; }

static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user; }

static int GetPlayerFlags(int pIndex) //virtual
{ return m_pFlags[pIndex]; }

static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_pFlags[pIndex]=flags; }

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    InitializeIndexLoop(pInstance);
}
int m_mobCount;

static int GetMobCount(){return m_mobCount;}//virtual
static void SetMobCount(int count){m_mobCount=count;}//virtual
