
#include "libs/define.h"
#include "libs/memutil.h"
#include "libs/stringutil.h"
#include "libs/opcodehelper.h"
#include "libs/spellutil.h"
#include "libs/printutil.h"
#include "libs/sound_define.h"
#include "libs/fxeffect.h"
#include "libs/array.h"
#include "libs/mathlab.h"
#include "libs/waypoint.h"
#include "libs/buff.h"

int *m_pWeaponProperty1;
int *m_pWeaponPropertyFxCode1;
int *m_pWeaponPropertyExecutor1;

int *m_pWeaponProperty2;
int *m_pWeaponPropertyFxCode2;
int *m_pWeaponPropertyScrExecutor2;

#define FX_ID_SUMMON_CANCEL 0x7f
#define FX_ID_SHIELD 0x80
#define FX_ID_BLUE_SPARKS 0x81
#define FX_ID_YELLOW_SPARKS 0x82
#define FX_ID_PLASMA_SPARK 0x83
#define FX_ID_VIOLET_SPARK 0x84
#define FX_ID_EXPLOSION 0x85
#define FX_ID_LESSER_EXPLOSION 0x86
#define FX_ID_COUNTERSPELL_EXPLOSION 0x87
#define FX_ID_THIN_EXPLOSION 0x88
#define FX_ID_TELEPORT 0x89
#define FX_ID_SMOKE_BLAST 0x8a
#define FX_ID_DAMAGE_POOF 0x8b
#define FX_ID_SPARK_EXPLOSION 0x93
#define FX_ID_RICOCHET 0x96
#define FX_ID_GREEN_EXPLOSION 0x99
#define FX_ID_WHITE_FLASH 0x9a

void SpecialWeaponPropertyExecuteFXCodeGen(int fxId, int soundId, int *pFXCode)
{
    int *code = MemAlloc(84);
	int calls[] = {0x4e0702, 0x4e0722, 0x4e0731, 0};

	OpcodeCopiesAdvance(code, &calls, 0x4e06f0, 0x4e073c);
	SetMemory(code + 46, fxId);
	SetMemory(code + 61, soundId);

	int *unitp = UnitToPtr(CreateObjectAt(StringUtilFindUnitNameById(1), 100.0, 100.0));

	unitp[187] = code;
	if (pFXCode)
		pFXCode[0] = code;
}

void SpecialWeaponPropertyExecuteScriptCodeGen(int execFunctionId, int *pScriptExecutor)
{
	int codes[] = {0x24448D50, 0xEC83520C, 0x85108B0C, 0xC71774D2, 0x00002404, 0x54890000, 0x408B0824,
		0x24448904, 0x62EAE804, 0xC483FFDB, 0xC3585A0C};
	int *alloc=MemAlloc(sizeof(codes)*4);
	int copyCounter =-1;

	while (++copyCounter<sizeof(codes))
		alloc[copyCounter] = codes[copyCounter];
	FixCallOpcode(alloc + 0x21, 0x507310);

	int *exec = alloc+18;
	exec[0] = execFunctionId;

	int *unitp = UnitToPtr(CreateObjectAt(StringUtilFindUnitNameById(1), 100.0, 100.0));

	unitp[187] = alloc;
	if (pScriptExecutor)
		pScriptExecutor[0] = alloc;
}

void SpecialWeaponPropertyExecuteCastSpellCodeGen(string spellName, int *pSpellExecutor)
{
    int codes[] = {0x0C24448B, 0x850CEC83, 0x8B3074C0, 0x508B3848, 0x244C893C, 0x244C8B04, 0x24548910, 0x2444C708, 0,
    	0x30518B00, 0x00244C8D, 0x50505152, 0xE80A6A50, 0x0004CF98, 0x8318C483, 0x90C30CC4};
	int *alloc = MemAlloc(sizeof(codes)*4);    
	int copyCounter=-1;
	int spellId = SpellUtilGetId(spellName);

	while (++copyCounter<sizeof(codes))
		alloc[copyCounter] = codes[copyCounter];
	
    FixCallOpcode(alloc + 0x33, 0x4fdd20);
	alloc[12] = (alloc[12]&(~0xff0000))^((spellId&0x7f) << 0x10);

	int *unitp = UnitToPtr(CreateObjectAt(StringUtilFindUnitNameById(1), 100.0, 100.0));

	unitp[187] = alloc;
	if (pSpellExecutor)
		pSpellExecutor[0] = alloc;
}

void SpecialWeaponPropertyDefaultExecuteFunction()
{ }

void SpecialWeaponPropertyCreate(int *pDest)
{
	int allocSize = 36*4;
	int *ptr = MemAlloc(allocSize);

	NoxDwordMemset(ptr, allocSize, 0);
	int *unitp = UnitToPtr(CreateObjectAt(StringUtilFindUnitNameById(1), 100.0, 100.0));

	unitp[187] = ptr;
	if (pDest)
		pDest[0] = ptr;
	ptr[0] = 0x5bac5c;
	ptr[1] = 0x61;
	ptr[5] = 0x4b0; 
	ptr[6] = 0xb40000; 
	ptr[7] = 0x1b2ff2;
	ptr[9] = 1;
}

void SpecialWeaponPropertySetName(int *pProperty, string propertyName)
{
	pProperty[0] = StringUtilGetScriptStringPtr(propertyName);
}

void SpecialWeaponPropertySetPlusDamageRate(int *pProperty, float plusDamageRate)
{
	pProperty[14] = ToInt(plusDamageRate);
}

void SpecialWeaponPropertySetId(int *pProperty, int propertyId)
{
	pProperty[1] = propertyId;
}

void SpecialWeaponPropertySetFXCode(int *pProperty, int *pFXCode)
{
	pProperty[13] = pFXCode;
}

void SpecialWeaponPropertySetExecuteCode(int *pProperty, int *pPropertyExecutor)
{
	pProperty[10] = pPropertyExecutor;
}

void SpecialWeaponPropertySetWeapon(int weapon, int slot, int *pProperty)
{
	int *ptr=UnitToPtr(weapon);

	if (!ptr)
		return;
	slot&=3;

	int counter=-1, *slots = ptr[173];

	slots[slot] = pProperty;
	while (++counter<32)
		ptr[140 + counter] = 0x200;
}

void TestHitWeapon()
{
	UniPrint(OTHER, "테스트!");
	Enchant(SELF, "ENCHANT_VAMPIRISM", 0.0);

	int res = StringCompare("Lightning3", ReadStringAddressEx(0x5bac5c));

	if (!res)
		UniPrint(OTHER, "문자열이 같아요!");
	else
		UniPrint(OTHER, "문자열이 달라요!");

	UniPrint(OTHER, IntToString(res));
}

int StringCompare(string cmp1, string cmp2)
{
	int *cmpPtr1 = StringUtilGetScriptStringPtr(cmp1);
	int *cmpPtr2 = StringUtilGetScriptStringPtr(cmp2);
	
	while ((cmpPtr1[0]&0xff) && ((cmpPtr1[0]&0xff)== (cmpPtr2[0]&0xff)))
	{
		Nop(++cmpPtr1);
		Nop(++cmpPtr2);
	}
	return (cmpPtr1[0]&0xff)-(cmpPtr2[0]&0xff);
}

int StringCompareWithPtr(int *cmp1, int *cmp2)
{
	while ((cmp1[0]&0xff) && ((cmp1[0]&0xff)==(cmp2[0]&0xff)))
	{
		Nop(++cmp1);
		Nop(++cmp2);
	}
	return (cmp1[0]&0xff)-(cmp2[0]&0xff);
}

int StrByte(int src, int *c)
{
	c[0]=src&0xff;
	return c[0];
}

void StrToUpper(string str, string *destStr)
{
	int *ptr = StringUtilGetScriptStringPtr(str);
	int destArr[100];
	int *destPtr = &destArr, c;

	while (StrByte(ptr[0], &c))
	{
		if (!c) break;
		if (c >= 'a' && c <= 'z')	c -= 32;
		destPtr[0] = (destPtr[0]&(~0xff))^c;
		ptr+=1;
		destPtr+=1;
	}
	destStr[0] = ReadStringAddressEx(&destArr);
}

void StrToLower(string str, string *destStr)
{
	int *ptr = StringUtilGetScriptStringPtr(str);
	int destArr[100];
	int *destPtr = &destArr, c;

	while (StrByte(ptr[0], &c))
	{
		if (!c) break;
		if (c >= 'A' && c <= 'Z') c += 32;
		destPtr[0] = (destPtr[0]&(~0xff))^c;
		ptr+=1;
		destPtr+=1;
	}
	destStr[0]=ReadStringAddressEx(&destArr);
}

void BeaconTouch()
{
	int weapon = CreateObjectAt("WarHammer", GetObjectX(OTHER), GetObjectY(OTHER));

	UnitNoCollide(weapon);
	SpecialWeaponPropertySetWeapon(weapon, 2, m_pWeaponProperty1);

	int sword = CreateObjectAt("GreatSword", GetObjectX(OTHER), GetObjectY(OTHER));

	UnitNoCollide(sword);
	SpecialWeaponPropertySetWeapon(sword, 2, m_pWeaponProperty2);
}

int GameDataUtilFindTable(string keyId, int *pDest, int *pCount)
{
	string *dest = StringUtilGetScriptStringPtr(keyId);

	int tableKey = (SToInt(dest[0]) & 0xff);
	if (tableKey < 'A' || tableKey > 'Z')
		return FALSE;

	tableKey -= 'A';
	
	int *gameDbBase = 0x6552d8;
	int *tablePtr = gameDbBase[0]+(tableKey*12);

	if (!tablePtr[0])
		return FALSE;

	pDest[0] = tablePtr[0];
	pCount[0] = tablePtr[1];
	return TRUE;
}

int GameDataUtilFindSubTable(string keyId, int *subTable, int count, int *pFindRes)
{
	if (!count)
		return FALSE;

	int *cmp = StringUtilGetScriptStringPtr(keyId);

	while (count--)
	{
		if (!StringCompareWithPtr(cmp, subTable[count*2]))
		{
			pFindRes[0] = ArrayRefN(subTable, count*2);
			return TRUE;
		}
	}
	return FALSE;
}

int GameDataUtilGetValueByIndex(string keyId, int index, float *pValue)
{
	int *pTable, count;

	if (!GameDataUtilFindTable(keyId, &pTable, &count))
		return FALSE;

	int *find;
	if (!GameDataUtilFindSubTable(keyId, pTable, count, &find))
		return FALSE;

	int *valuePtr = find[1];

	if (index >= valuePtr[1])
		return FALSE;

	int *val = valuePtr[0];
	pValue[0] = ToFloat(val[index]);
	return TRUE;
}

int GameDataUtilGetValue(string keyId, float *pValue)
{
	return GameDataUtilGetValueByIndex(keyId, 0, pValue);
}

int GameDataUtilSetValueByIndex(string keyId, int index, float setvalue)
{
	int *pTable, count;

	if (!GameDataUtilFindTable(keyId, &pTable, &count))
		return FALSE;

	int *find;
	if (!GameDataUtilFindSubTable(keyId, pTable, count, &find))
		return FALSE;

	int *valuePtr = find[1];

	if (index >= valuePtr[1])
		return FALSE;

	int *val = valuePtr[0];
	val[index] = ToInt(setvalue);
	return TRUE;
}

void OnPress()
{
	UniPrint(OTHER, "비콘을 밟으셨어요~");
}

static void deferredInit()
{
	MoveObject(Object("movingTrg"), LocationX(77), LocationY(77));
}

void MapInitialize()
{
	FrameTimer(1, deferredInit);
}

void FlagCreate(float xpos, float ypos)
{
	int flg = CreateObjectAt("Flag", xpos, ypos);
	int *ptr = UnitToPtr(flg);

	UniPrint(OTHER, IntToString(ptr[173]));
}

void AllocSmartMem(int allocsize, int *pDestptr, int *pDestUnit)
{
    int unit = CreateObjectAt(StringUtilFindUnitNameById(1), 100.0, 100.0);
    int *ptr = UnitToPtr(unit);

    pDestptr[0] = MemAlloc(allocsize);
    ptr[187] = pDestptr;
    if (pDestUnit)
        pDestUnit[0] = unit;
}

void ReadMemoryTo()
{
	int golem;

	if (!golem)
		GameDataUtilSetValueByIndex("BerserkerDamage", 0, 500.0);

	golem = CreateObject("StoneGolem", 54);
}


