
#include "libs/define.h"
#include "libs/opcodehelper.h"
#include "libs/memutil.h"

#define FIELD_GUIDE_INVALID 0
#define FIELD_GUIDE_BAT 1
#define FIELD_GUIDE_BLACKBEAR 2
#define FIELD_GUIDE_BEAR 3
#define FIELD_GUIDE_BEHOLDER 4
#define FIELD_GUIDE_BOMBER 5
#define FIELD_GUIDE_CARNIVOROUSPLANT 6
#define FIELD_GUIDE_ALBINOSPIDER 7
#define FIELD_GUIDE_SMALLALBINOSPIDER 8
#define FIELD_GUIDE_EVILCHERUB 9
#define FIELD_GUIDE_EMBERDEMON 10
#define FIELD_GUIDE_GHOST 11
#define FIELD_GUIDE_GIANTLEECH 12
#define FIELD_GUIDE_IMP 13
#define FIELD_GUIDE_FLYINGGOLEM 14
#define FIELD_GUIDE_MECHANICALGOLEM 15
#define FIELD_GUIDE_MIMIC 16
#define FIELD_GUIDE_GRUNTAXE 17
#define FIELD_GUIDE_OGREBRUTE 18
#define FIELD_GUIDE_OGREWARLORD 19
#define FIELD_GUIDE_SCORPION 20
#define FIELD_GUIDE_SHADE 21
#define FIELD_GUIDE_SKELETON 22
#define FIELD_GUIDE_SKELETONLORD 23
#define FIELD_GUIDE_SPIDER 24
#define FIELD_GUIDE_SMALLSPIDER 25
#define FIELD_GUIDE_SPITTINGSPIDER 26
#define FIELD_GUIDE_STONEGOLEM 27
#define FIELD_GUIDE_TROLL 28
#define FIELD_GUIDE_URCHIN 29
#define FIELD_GUIDE_WASP 30
#define FIELD_GUIDE_WILLOWISP 31
#define FIELD_GUIDE_WOLF 32
#define FIELD_GUIDE_BLACKWOLF 33
#define FIELD_GUIDE_WHITEWOLF 34
#define FIELD_GUIDE_ZOMBIE 35
#define FIELD_GUIDE_VILEZOMBIE 36
#define FIELD_GUIDE_DEMON 37
#define FIELD_GUIDE_LICH 38
#define FIELD_GUIDE_WIZARDGREEN 39
#define FIELD_GUIDE_URCHINSHAMAN 40

int getFieldGuideData(char guideId) //size: 56 bytes
{
    // char *base=0x689064;
    char *base=0x689080;

    return &base[guideId*28];
}

void ChangeGuideName(char guideId, short *name)
{
    int *field=getFieldGuideData(guideId);

    field[0]=name;
}

void ChangeGuideUnitID(char guideId, short thingId)
{
    int *field=getFieldGuideData(guideId);

    field[1]=thingId;
}

void ChangeGuideDescription(char guideId, short *explain)
{
    int *field=getFieldGuideData(guideId);

    field[2]=explain;
}

void ChangeGuideImage(char guideId, int btnImg, int img2)
{
    int *field=getFieldGuideData(guideId);

    field[3]=btnImg;
    field[4]=img2;
}

void ChangeGuideType(char guideId, char ty)
{
    int *field=getFieldGuideData(guideId);

    field[6]=ty;
}

void ResetFieldGuideData()
{
    char *p;

    if (!p)
    {
        char code[]={
            0xB8, 0x70, 0x70, 0x42, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC3,0x90,0x90,
        };
        p=code;
    }
    invokeRawCode(p,NULLPTR);
}

void ReserveResetFieldGuideData()
{
    char *p;

    if (p)
        return;

    char code[]={
        // 0xB8, 0x70, 0x70, 0x42, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC7, 0x05, 0x94, 0x90, 0x68, 0x00, 0x00, 0x00, 0x00,
        // 0x00, 0xC7, 0x05, 0x1C, 0x82, 0x59, 0x00, 0xE0, 0xA6, 0x42, 0x00, 0x68, 0xE0, 0xA6, 0x42, 0x00, 0xC3, 0x90
        0x8B, 0x05, 0xEC, 0xF0, 0x69, 0x00, 0x85, 0xC0, 0x74, 0x09, 0xB8, 0x70, 0x70, 0x42, 0x00, 0xFF, 0xD0, 0x31, 
        0xC0, 0xC7, 0x05, 0x1C, 0x82, 0x59, 0x00, 0xE0, 0xA6, 0x42, 0x00, 0x68, 0xE0, 0xA6, 0x42, 0x00, 0xC3, 0x90,
    };
    p=code;
    char *dest=MemAlloc(sizeof(code));

    NoxByteMemCopy(p,dest,sizeof(code));
    //0x689094
    int *t=0x59821c;
	int *c = &dest[25];

	c[0]=t[0];
	c=&dest[30];
	c[0]=t[0];
	t[0]=dest;
    // int *prev=0x689094;
    // if (prev[0])
        // MemFree(prev[0]);
    // prev[0]=dest;
}
