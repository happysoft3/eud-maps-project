
#include "mansion2_sub.h"
#include "mansion2_resource.h"
#include "libs/sound_define.h"
#include "libs/fxeffect.h"
#include "libs/printutil.h"
#include "libs/waypoint.h"
#include "libs/unitstruct.h"
#include "libs/playerupdate.h"
#include "libs/coopteam.h"
#include "libs/playerinfo.h"
#include "libs/network.h"
#include "libs/clientside.h"
#include "libs/npc.h"
#include "libs/format.h"
#include "libs/itemproperty.h"
#include "libs/weapon_effect.h"
#include "libs/bind.h"
#include "libs/hash.h"
#include "libs/linked_list.h"
#include "libs/buff.h"
#include "libs/logging.h"
#include "libs/potionex.h"
#include "libs/weaponcapacity.h"
#include "libs/meleeattack.h"
#include "libs/indexloop.h"
#include "libs/magicmissile.h"
#include "libs/game_flags.h"
#include "libs/networkEx.h"

#define GOBUNGAH_RUN_MULTIPLE 3.8

#define MOB_SCR_TABLE_RUN_MULTIPLE 24
#define MOB_SCR_TABLE_POISON_CHANCE 34
#define MOB_SCR_TABLE_STRIKE_HANDLER 59
#define MOB_SCR_TABLE_MELEE_DAMAGE 29
#define MOB_SCR_TABLE_MELEE_DAMAGE_TYPE 31
#define MOB_SCR_TABLE_MELEE_MIN_DELAY 32
#define MOB_SCR_TABLE_MELEE_MAX_DELAY 33
#define MOB_SCR_TABLE_MELEE_RANGE 28
#define MOB_SCR_TABLE_POISON_POWER 35
#define MOB_SCR_TABLE_POISON_MAX 36

int m_unitscanHashInstance;
int m_itemFunctionTable;
int m_itemFunctionTableLength;
int m_lastCreatedUnit;
int m_poisonUnit;
int m_potionHash;
float m_confuseLevels[8];
int m_entityRespawnTimeTable[32];
int m_userTrapLinkedList[32];
int m_player[10];
int m_pFlag[10];
short m_pDeath[10];
short* m_userRespawnPoints;
int m_userRespawnPointLength;
int m_selected = FALSE;
string *m_weaponNameTable;
int m_weaponNameTableLength;
string *m_armorNameTable;
int m_armorNameTableLength;
string *m_potionNameTable;
int m_potionNameTableLength;
string *m_staffNameTable;
int m_staffNameTableLength;
int m_fieldItemSpawnAmount=40;

int *m_pWeaponProperty1;
short *m_entityRespawnPoints;
int m_entityRespawnPointLength;
// int *m_pWeaponPropertyFxCode1;
int *m_pWeaponPropertyExecutor1;
int *m_pImgVector;
int *m_entityList;
int* m_entityInfoHash;
#define _ENTITY_HASH_NODE_ 0
#define _ENTITY_HASH_STRING_ 1
#define _ENTITY_HASH_MAX_ 2
char *m_entityName1, *m_entityName2;
char *m_entityName3;
char *m_entityName4;

int m_entityCount, m_currentEntityCount=0;

int*m_obungaHashData, m_obungaWeaponData;

int *m_pWeaponProperty2;
int *m_pWeaponPropertyExecutor2;
// int *m_pWeaponPropertyFxCode2;

short *m_userWeaponLocations;
char m_gameOption[32];
int m_userWeaponLocationLength;
char m_gameOption2[32];
int m_userSpecialWeaponRespawnTime, m_userSpecialWeaponRespawnTimeLimit=30;

//User weapons
int *m_userWeaponFxHash;
int *m_pWeaponUserSpecialProperty1;
int *m_pWeaponUserSpecialPropertyExecutor1;
// int *m_pWeaponUserSpecialPropertyFxCode1;

int *m_pWeaponUserSpecialProperty2;
int *m_pWeaponUserSpecialPropertyExecutor2;
// int *m_pWeaponUserSpecialPropertyFxCode2;

int *m_pWeaponUserSpecialProperty3;
int *m_pWeaponUserSpecialPropertyExecutor3;
// int *m_pWeaponUserSpecialPropertyFxCode3;

int *m_pWeaponUserSpecialProperty4;
int *m_pWeaponUserSpecialPropertyExecutor4;
// int *m_pWeaponUserSpecialPropertyFxCode4;

int *m_pWeaponUserSpecialProperty5;
int *m_pWeaponUserSpecialPropertyExecutor5;
// int *m_pWeaponUserSpecialPropertyFxCode5;

int *m_pWeaponUserSpecialProperty6;
int *m_pWeaponUserSpecialPropertyExecutor6;
// int *m_pWeaponUserSpecialPropertyFxCode6;

int *m_pWeaponUserSpecialProperty7;
int *m_pWeaponUserSpecialPropertyExecutor7;
// int *m_pWeaponUserSpecialPropertyFxCode7;

int *m_pWeaponUserSpecialProperty8;
int *m_pWeaponUserSpecialPropertyExecutor8;
// int *m_pWeaponUserSpecialPropertyFxCode8;

int *m_pWeaponUserSpecialProperty9;
int *m_pWeaponUserSpecialPropertyExecutor9;
// int *m_pWeaponUserSpecialPropertyFxCode9;

int *m_pWeaponUserSpecialPropertyA;
int *m_pWeaponUserSpecialPropertyExecutorA;
// int *m_pWeaponUserSpecialPropertyFxCodeA;

int *m_pWeaponUserSpecialPropertyB;
int *m_pWeaponUserSpecialPropertyExecutorB;
// int *m_pWeaponUserSpecialPropertyFxCodeB;
int *m_pWeaponUserSpecialPropertyC;
int *m_pWeaponUserSpecialPropertyExecutorC;
// int *m_pWeaponUserSpecialPropertyFxCodeC;

int *m_pWeaponUserSpecialPropertyD;
int *m_pWeaponUserSpecialPropertyExecutorD;
// int *m_pWeaponUserSpecialPropertyFxCodeD;

int *m_pWeaponUserSpecialPropertyE;
int *m_pWeaponUserSpecialPropertyExecutorE;
// int *m_pWeaponUserSpecialPropertyFxCodeE;
int *m_pWeaponUserSpecialPropertyF;
int *m_pWeaponUserSpecialPropertyExecutorF;
// int *m_pWeaponUserSpecialPropertyFxCodeF;
int *m_pWeaponUserSpecialPropertyG;
int *m_pWeaponUserSpecialPropertyExecutorG;
// int *m_pWeaponUserSpecialPropertyFxCodeG;
int *m_pWeaponUserSpecialPropertyH;
int *m_pWeaponUserSpecialPropertyExecutorH;
// int *m_pWeaponUserSpecialPropertyFxCodeH;

int *m_pWeaponUserSpecialPropertyI;
int *m_pWeaponUserSpecialPropertyExecutorI;
// int *m_pWeaponUserSpecialPropertyFxCodeI;
int *m_pWeaponUserSpecialPropertyJ;
int *m_pWeaponUserSpecialPropertyExecutorJ;
// int *m_pWeaponUserSpecialPropertyFxCodeJ;

int *m_pWeaponUserSpecialPropertyK;
int *m_pWeaponUserSpecialPropertyExecutorK;
// int *m_pWeaponUserSpecialPropertyFxCodeK;

int *m_pWeaponUserSpecialPropertyL;
int *m_pWeaponUserSpecialPropertyExecutorL;
// int *m_pWeaponUserSpecialPropertyFxCodeL;

int *m_pWeaponUserSpecialPropertyM;
int *m_pWeaponUserSpecialPropertyExecutorM;
// int *m_pWeaponUserSpecialPropertyFxCodeM;

int *m_pWeaponUserSpecialPropertyN;
int *m_pWeaponUserSpecialPropertyExecutorN;
// int *m_pWeaponUserSpecialPropertyFxCodeN;

int *m_pWeaponUserSpecialPropertyO;
int *m_pWeaponUserSpecialPropertyExecutorO;
// int *m_pWeaponUserSpecialPropertyFxCodeO;

int *m_pWeaponUserSpecialPropertyP;
int *m_pWeaponUserSpecialPropertyExecutorP;
// int *m_pWeaponUserSpecialPropertyFxCodeP;

int *m_pWeaponUserSpecialPropertyQ;
int *m_pWeaponUserSpecialPropertyExecutorQ;
// int *m_pWeaponUserSpecialPropertyFxCodeQ;

int *m_pWeaponUserSpecialPropertyR;
int *m_pWeaponUserSpecialPropertyExecutorR;
// int *m_pWeaponUserSpecialPropertyFxCodeR;

static void postItemCommon(int getitem)
{
    if (getitem)
    {
        if (GetUnitFlags(getitem) & UNIT_FLAG_NO_COLLIDE)
            UnitNoCollide(getitem);
        int durability=MaxHealth(getitem);

        if (durability)
        {
            durability*=4;
            if (durability<5000)
                SetUnitMaxHealth(getitem, durability);
        }
    }
}

#define CUSTOM_HIT_PROPERTY_PTR 2
#define CUSTOM_HIT_WEAPON_PTR 3
#define CUSTOM_HIT_USER_PTR 4
#define CUSTOM_HIT_VICTIM_PTR 5

int CreateCustomHitWeaponProperty(int execId)
{
    int ret;
    char code[]={
        0x56, 0x8B, 0x74, 0x24, 0x14, 0xF6, 0x46, 0x08, 0x06, 0x74, 0x1E, 0x8B, 
        0xF4, 0xB8, 0x30, 0x72, 0x50, 0x00, 0x56, 0xFF, 0xD0, 0x5E, 0x6A, 0x00, 
        0x6A, 0x00, 0x68, 0x80, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5E, 0xC3, 0x90
    };
    AllocSmartMemEx(sizeof(code), &ret);
    NoxByteMemCopy(code, ret, sizeof(code));
    int*p=ret+27;
    p[0]=execId;
    return ret;
}

static void onCustomHitConfuse(int *p)
{
    int *tb=p[CUSTOM_HIT_PROPERTY_PTR];
    int *ptr=p[CUSTOM_HIT_VICTIM_PTR];

    Enchant( ptr[11], "ENCHANT_CONFUSED", m_confuseLevels[tb[15]]);
}

static void onCustomHitSlowed(int *p)
{
    int *tb=p[CUSTOM_HIT_PROPERTY_PTR];
    int lv=(tb[15]-1);
    int *ptr=p[CUSTOM_HIT_VICTIM_PTR];

    Enchant( ptr[11], "ENCHANT_SLOWED", m_confuseLevels[lv*2]);
}

static void onCustomHitFreeze(int *p)
{
    int *tb=p[CUSTOM_HIT_PROPERTY_PTR];
    int *ptr=p[CUSTOM_HIT_VICTIM_PTR];
    int *ownerPtr=p[CUSTOM_HIT_USER_PTR];

    Enchant( ptr[11], "ENCHANT_AFRAID", m_confuseLevels[tb[15]-1]);
    Damage(ptr[11], ownerPtr[11], tb[15], DAMAGE_TYPE_PLASMA);
    float xy[]={ptr[14],ptr[15]};
    Effect("CYAN_SPARKS", xy[0],xy[1],0.0,0.0);
}

int m_customPropertyConfuse1;
int m_customPropertyConfuse2;
int m_customPropertyConfuse3;
int m_customPropertyConfuse4;

int m_customPropertySlow1;
int m_customPropertySlow2;
int m_customPropertySlow3;
int m_customPropertySlow4;

int m_customPropertyFreeze1;
int m_customPropertyFreeze2;
int m_customPropertyFreeze3;
int m_customPropertyFreeze4;
int m_propertyFireRing1;

int m_weaponPowerProperty[6];
int m_weaponMaterialProperty[6];
int m_weaponFxProperty[34];

static int macroCustomHit(int ordPropertyOff, int lv, int execId)
{
    int *p;

    AllocSmartMemEx(144, &p);
    NoxByteMemCopy(GetMemory(ordPropertyOff ), p, 144);
    p[13]= CreateCustomHitWeaponProperty(execId);
    p[15]=lv;
    return p;
}

static void initializeCustomHit()
{
    m_customPropertyConfuse1=macroCustomHit(ITEM_PROPERTY_confuse1, 1, onCustomHitConfuse);
    m_customPropertyConfuse2=macroCustomHit(ITEM_PROPERTY_confuse2 , 2, onCustomHitConfuse);
    m_customPropertyConfuse3=macroCustomHit(ITEM_PROPERTY_confuse3 , 3, onCustomHitConfuse);
    m_customPropertyConfuse4=macroCustomHit(ITEM_PROPERTY_confuse4 , 4, onCustomHitConfuse);

    m_customPropertySlow1=macroCustomHit(ITEM_PROPERTY_stun1 , 1, onCustomHitSlowed);
    m_customPropertySlow2=macroCustomHit(ITEM_PROPERTY_stun2  , 2, onCustomHitSlowed);
    m_customPropertySlow3=macroCustomHit(ITEM_PROPERTY_stun3  , 3, onCustomHitSlowed);
    m_customPropertySlow4=macroCustomHit(ITEM_PROPERTY_stun4 , 4, onCustomHitSlowed);

    int obj=Object("hackMana");
    int ptr=UnitToPtr(obj);
    int srcProperty=GetMemory(ptr+0x2b4);
    m_customPropertyFreeze1=macroCustomHit(srcProperty, 1, onCustomHitFreeze);
    m_customPropertyFreeze2=macroCustomHit(srcProperty+4, 2, onCustomHitFreeze);
    m_customPropertyFreeze3=macroCustomHit(srcProperty+8, 3, onCustomHitFreeze);
    m_customPropertyFreeze4=macroCustomHit(srcProperty+12, 4, onCustomHitFreeze);
    obj=Object("hackring");
    ptr=UnitToPtr(obj);
    srcProperty=GetMemory(ptr+0x2b4);
    m_propertyFireRing1=GetMemory(srcProperty);
}

static void initWeaponProperty()
{
    m_weaponMaterialProperty[1]=ITEM_PROPERTY_Matrial3;
    m_weaponMaterialProperty[2]=ITEM_PROPERTY_Matrial4;
    m_weaponMaterialProperty[3]=ITEM_PROPERTY_Matrial5;
    m_weaponMaterialProperty[4]=ITEM_PROPERTY_Matrial6;
    m_weaponMaterialProperty[5]=ITEM_PROPERTY_Matrial7;
    m_weaponPowerProperty[1]=ITEM_PROPERTY_weaponPower2;
    m_weaponPowerProperty[2]=ITEM_PROPERTY_weaponPower3;
    m_weaponPowerProperty[3]=ITEM_PROPERTY_weaponPower4;
    m_weaponPowerProperty[4]=ITEM_PROPERTY_weaponPower5;
    m_weaponPowerProperty[5]=ITEM_PROPERTY_weaponPower6;
    int index=1;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_fire1;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_fire2;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_fire3;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_fire4;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_lightning1;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_lightning2;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_lightning3;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_lightning4;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_venom1;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_venom2;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_venom3;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_venom4;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_vampirism1;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_vampirism2;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_vampirism3;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_vampirism4;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_impact1;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_impact2;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_impact3;
    m_weaponFxProperty[index++]=ITEM_PROPERTY_impact4;
    m_weaponFxProperty[index++]=&m_customPropertyConfuse1;
    m_weaponFxProperty[index++]=&m_customPropertyConfuse2;
    m_weaponFxProperty[index++]=&m_customPropertyConfuse3;
    m_weaponFxProperty[index++]=&m_customPropertyConfuse4;
    m_weaponFxProperty[index++]=&m_customPropertySlow1;
    m_weaponFxProperty[index++]=&m_customPropertySlow2;
    m_weaponFxProperty[index++]=&m_customPropertySlow3;
    m_weaponFxProperty[index++]=&m_customPropertySlow4;
    m_weaponFxProperty[index++]=&m_customPropertyFreeze1;
    m_weaponFxProperty[index++]=&m_customPropertyFreeze2;
    m_weaponFxProperty[index++]=&m_customPropertyFreeze3;
    m_weaponFxProperty[index++]=&m_customPropertyFreeze4;
    m_weaponFxProperty[index++]=&m_propertyFireRing1;
}

void CreateRandomWeapon(float xpos, float ypos, int *pDest)
{
    if (!SToInt(m_weaponNameTable))
        return;

    int weapon = CreateObjectAt(m_weaponNameTable[Random(0, m_weaponNameTableLength-1)], xpos, ypos);

    if (pDest)
        pDest[0] = weapon;

    // SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    int max=sizeof(m_weaponFxProperty)-1;
    SetWeaponPropertiesDirect(weapon, 
        m_weaponPowerProperty[Random(0, 5)],
        m_weaponMaterialProperty[Random(0, 5)],
        m_weaponFxProperty[Random(0,max)],m_weaponFxProperty[Random(0,max)]);
    
    int thingId = GetUnitThingID(weapon);

    if (thingId>=222&&thingId<=225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId==1178||1168==thingId)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
}

void CreateRandomArmor(float xpos, float ypos, int *pDest)
{
    if (!SToInt(m_armorNameTable))
        return;

    int armor = CreateObjectAt(m_armorNameTable[Random(0, m_armorNameTableLength-1)], xpos, ypos);

    if (pDest)
        pDest[0] = armor;

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
}

void CreateCoin(float xpos, float ypos, int *pDest)
{
    int coin = CreateObjectAt("Gold", xpos, ypos);

    if (pDest)
        pDest[0] = coin;
    UnitStructSetGoldAmount(coin, Random(500, 2000));
}

void CreateCoinBetter(float xpos, float ypos, int *pDest)
{
    int coin = CreateObjectAt("Gold", xpos, ypos);

    if (pDest)
        pDest[0] = coin;
    UnitStructSetGoldAmount(coin, Random(1000, 5000));
}

void CreateGerm(float xpos, float ypos, int *pDest)
{
    string germs[] = {"Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Emerald", "Diamond"};
    int pic = CreateObjectAt(germs[Random(0, sizeof(germs) - 1)], xpos, ypos);

    if (pDest)
        pDest[0] = pic;
}

// int CheckPotionThingID(int unit)
// {
//     int thingID = GetUnitThingID(unit), x = unit;

//     if (thingID == 639)
//         x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
//     else if (thingID == 640)
//         x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
//     else if (thingID == 641)
//         x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
//     if (x ^ unit) Delete(unit);

//     return x;
// }

void CreateRandomPotion(float xpos, float ypos, int *pDest)
{
    if (Random(0, 2))
    {
        CreateHotPotion(xpos, ypos, pDest);
        return;
    }
    if (!SToInt(m_potionNameTable))
        return;

    int potion = CreateObjectAt(m_potionNameTable[Random(0, m_potionNameTableLength-1)], xpos, ypos);

    // CheckPotionThingID(potion);
    int fn;
    if (HashGet(m_potionHash, GetUnitThingID(potion), &fn, FALSE))
        potion= potionHashProto(fn, potion);
    //
    if (pDest)
        pDest[0] = potion;
    if (MaxHealth(potion))
        SetUnitMaxHealth(potion, 0);
}

void CreateHotPotion(float xpos, float ypos, int *pDest)
{
    int potion = CreateObjectAt("RedPotion", xpos, ypos);

    if (pDest)
        pDest[0] = potion;
}

void CreateHotPotion2(float x,float y, int*p)
{
    string n[]={"RedPotion","CurePoisonPotion"};
    int pot=CreateObjectAt(n[Random(0,1)], x,y);
    if (p)
        p[0]=pot;
}

void CreateMagicalStaff(float xpos, float ypos, int *pDest)
{
    int s= CreateObjectAt(m_staffNameTable [Random(0, m_staffNameTableLength-1)], xpos, ypos);
    if(pDest)
     pDest[0]=s;
}

static void InitItemNames()
{
    int weapons[]={"WarHammer", "GreatSword", "Sword", "Longsword", "StaffWooden", 
    "OgreAxe", "BattleAxe", "FanChakram", "RoundChakram",
            "OblivionHalberd", "OblivionHeart", "OblivionWierdling"};
    int armors[]={"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", 
    "PlateLeggings", "ChainCoif", "ChainLeggings", "ChainTunic", "SteelShield","WoodenShield",
            "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", 
            "LeatherHelm", "LeatherLeggings", "MedievalCloak",
            "MedievalPants", "MedievalShirt", "StreetShirt", "StreetSneakers", "StreetPants"};
    int pots[]={"RedPotion", "RedPotion", "CurePoisonPotion", "VampirismPotion", "RedPotion",
    "ShieldPotion", "InvisibilityPotion", "InvulnerabilityPotion","CurePoisonPotion",
            "ShieldPotion", "HastePotion", "FireProtectPotion", "ShockProtectPotion", 
            "PoisonProtectPotion", "YellowPotion", "BlackPotion", "CurePoisonPotion",
            "CurePoisonPotion"};
    int wands[]={"DeathRayWand", "LesserFireballWand", "FireStormWand", "ForceWand", 
    "InfinitePainWand", "SulphorousFlareWand", "SulphorousShowerWand"};

    m_weaponNameTable=weapons;
    m_armorNameTable=armors;
    m_potionNameTable=pots;
    m_staffNameTable=wands;
    m_weaponNameTableLength=sizeof(weapons);
    m_armorNameTableLength=sizeof(armors);
    m_potionNameTableLength=sizeof(pots);
    m_staffNameTableLength=sizeof(wands);
}

void InitItemFunctionTable()
{
    int items[]={ CreateHotPotion, CreateHotPotion2, 
        CreateMagicalStaff, CreateRandomArmor, CreateRandomPotion, CreateRandomWeapon};

    m_itemFunctionTable=items;
    m_itemFunctionTableLength=sizeof(items);
    InitItemNames();
}

void CreateRandomItemProto(int functionId, float xpos, float ypos, int *ptr)
{
    Bind(functionId, &functionId+4);
}

void CreateRandomItemCommon(int posUnit, int *itemTablePtr, int tableLength)
{
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    Delete(posUnit);

    if (!itemTablePtr || !tableLength)
        return;

    if (!m_itemFunctionTable)
        return;

    int getitem = 0;

    CreateRandomItemProto(itemTablePtr[Random(0, tableLength-1)], xpos, ypos, &getitem);
    postItemCommon(getitem);
}

void PutRandomItem()
{
    if (!Random(0, 6))
        return;
    // short location=Random(159, 350);
    WriteLog("PutRandomItem-start");
    CreateRandomItem(CreateObject("Mover", Random(1, 465)));
    WriteLog("PutRandomItem-end");
}

#define MAX_ENTITYCOUNT 4
#define GOBUNAG_MAX_HP 900

int m_gobungaDeathCount;
int m_gobungaMaxCount=MAX_ENTITYCOUNT;
char *m_gobungaMaxCountOptions;
int m_gobungaMaxCountOptionLength;
int m_gobungaMaxHP=GOBUNAG_MAX_HP;
short *m_gobungaMaxHPOpt;
int m_gobungaMaxHpOptLen;

//주의- 이 함수는 클라이언트도 실행합니다//
void InitOptions()
{
    char maxCount[]={1, 2, 4, 6, 8, 9, 11, 16, 24};

    m_gobungaMaxCountOptions=maxCount;
    m_gobungaMaxCountOptionLength=sizeof(maxCount);

    short maxHp[]={500, 750, 900, 1100, 1300, 1550, 1900};
    m_gobungaMaxHPOpt=maxHp;
    m_gobungaMaxHpOptLen=sizeof(maxHp);
}

#define PLAYER_DEATH_FLAG 0x80000000
static int NetworkUtilClientTimerEnabler() { return TRUE; }

void DrawImageAtEx(float x, float y, int thingId)
{
    int *ptr = UnitToPtr(CreateObjectAt("AirshipBasketShadow", x, y));

    ptr[1] = thingId;
}

void OnThisGameEnded()
{
    int count=sizeof(m_player);

    while(--count>=0)
    {
        if (CurrentHealth(m_player[count]))
        {
            short destpos = Random(66, 69);

            MoveObject(m_player[count], LocationX(destpos), LocationY(destpos));
        }
    }
    DrawImageAtEx(LocationX(451), LocationY(451), 2560);
    UnitNoCollide(CreateObjectAt("OgreStool1", LocationX(66), LocationY(66)));
    UnitNoCollide(CreateObjectAt("OgreStool1", LocationX(67), LocationY(67)));
    UnitNoCollide(CreateObjectAt("OgreStool1", LocationX(68), LocationY(68)));
    UnitNoCollide(CreateObjectAt("OgreStool1", LocationX(69), LocationY(69)));
    UniPrintToAll("승리! 고분가를 모두 격추시켰어요!");
    DeleteObjectTimer(CreateObjectAt("ManaBombCharge", LocationX(452), LocationY(452)), 360);
    Effect("WHITE_FLASH", LocationX(452), LocationY(425), 0.0, 0.0);
}

int *m_dialogCtx;

#include "libs/wallutil.h"
#include "libs/fixtellstory.h"
#include "libs/gui_window.h"

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10
#define _CLIENT_OPTION_OFF1_ 0x753B11
#define _CLIENT_OPTION_OFF2_ 0x753B12

void ClientShowOption1()
{
    char *c=_CLIENT_OPTION_OFF1_;
    char dest[128];
    int ic=m_gobungaMaxCountOptions[ c[0] ];

    NoxSprintfString(dest, "고분가 수를 %d 으로 하시겠어요?", &ic, 1);
    GUISetWindowScrollListboxText(m_dialogCtx, ReadStringAddressEx(dest), 0);
}

void ClientShowOption2()
{
    char *c=_CLIENT_OPTION_OFF2_;
    char dest[128];
    int ic=m_gobungaMaxHPOpt[ c[0] ];

    NoxSprintfString(dest, "고분가 체력을 %d 으로 하시겠어요?", &ic, 1);
    GUISetWindowScrollListboxText(m_dialogCtx, ReadStringAddressEx(dest), 0);
}

void ClientPlayBriefing3()
{
    PlayNPCVoice("f7nec01k");
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[0])
    {
        if (type[0]==1)
            ClientShowOption1();
        else if (type[0]==2)
            ClientShowOption2();
        else if (type[0]==3)
            ClientPlayBriefing3();
        type[0]=0;
    }
    FrameTimer(3, ClientProcLoop);
}

void ServerSendBrief()
{
    int u=sizeof(m_player);

    while (--u>=0)
    {
        if (CurrentHealth(m_player[u]))
        {
            if (m_player[u] == GetHost())
                continue;
            ClientSetMemory(m_player[u], _CLIENT_OPTION_TYPE_OFF_, 3);
        }
    }
    FrameTimer(1, ClientPlayBriefing3);
}

void DeferredChangeDialogContext(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user==(GetHost()))    //Server only
    {
        char dest[128];
        int option = m_gobungaMaxCountOptions[ m_gameOption[index] ];

        NoxSprintfString(dest, "고분가 수를 %d 으로 하시겠어요?", &option, 1);
        GUISetWindowScrollListboxText(m_dialogCtx, ReadStringAddressEx(dest), 0);
    }
    else
    {
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 1);
        ClientSetMemory(user, _CLIENT_OPTION_OFF1_, m_gameOption[index]);
    }
}

void DeferredChangeDialogContext2(int user)
{
    if (!CurrentHealth(user))
        return;

    int index = GetPlayerIndex(user);

    if (user == GetHost())    //Server only
    {
        char dest[128];
        int option = m_gobungaMaxHPOpt[ m_gameOption2[index] ];

        NoxSprintfString(dest, "고분가 체력을 %d 으로 하시겠어요?", &option, 1);
        GUISetWindowScrollListboxText(m_dialogCtx, ReadStringAddressEx(dest), 0);
    }
    else
    {
        ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, 2);
        ClientSetMemory(user, _CLIENT_OPTION_OFF2_, m_gameOption2[index]);
    }
}

#undef _CLIENT_OPTION_TYPE_OFF_
#undef _CLIENT_OPTION_OFF1_
#undef _CLIENT_OPTION_OFF2_

void OnSelectorEndDialog()
{
    if (m_selected)
        return;

    int result=GetAnswer(SELF);
    int index=GetPlayerIndex(OTHER);

    if (result==1)  ///yes
    {
        int sel=m_gobungaMaxCountOptions[m_gameOption[ index]];
        m_gobungaMaxCount=sel;
        char msg[128];

        NoxSprintfString(msg, "고분가 최대 수가 %d으로 설정되었습니다", &sel, 1);
        UniPrintToAll(ReadStringAddressEx(msg));
    }
    else if (result==2)     ///no
    {
        m_gameOption[index]=(++m_gameOption[index])%m_gobungaMaxCountOptionLength;
        OnSelectorStartDialog();
    }    
}

void OnSelectorStartDialog()
{
    FrameTimerWithArg(1, GetCaller(), DeferredChangeDialogContext);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "게임옵션1");
}

void OnSelector2EndDialog()
{
    if (m_selected)
        return;

    int result=GetAnswer(SELF);
    int index=GetPlayerIndex(OTHER);

    if (result==1)  //yes
    {
        int sel=m_gobungaMaxHPOpt[m_gameOption2[ index]];
        m_gobungaMaxHP=sel;
        char msg[128];

        NoxSprintfString(msg, "고분가 체력이 %d으로 설정되었습니다", &sel, 1);
        UniPrintToAll(ReadStringAddressEx(msg));
    }
    else if (result==2) //no
    {
        m_gameOption2[index]=(++m_gameOption2[index])%m_gobungaMaxHpOptLen;
        OnSelector2StartDialog();
    }
}

void OnSelector2StartDialog()
{
    FrameTimerWithArg(1, GetCaller(), DeferredChangeDialogContext2);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "게임옵션2");
}

void PutSelect()
{
    int sel=DummyUnitCreateAt("Bomber", LocationX(154), LocationY(154));    //"MagicMissile

    SetDialog(sel, "YESNO", OnSelectorStartDialog, OnSelectorEndDialog);
    LookWithAngle(sel, 96);
    StoryPic(sel, "MalePic13");
    int sel1Fx=CreateObjectAt("MagicMissile", GetObjectX(sel), GetObjectY(sel));
    Frozen(sel1Fx, TRUE);
    SetUnitSubclass(sel1Fx, GetUnitSubclass(sel1Fx) ^ 1);

    int sel2=DummyUnitCreateAt("BomberBlue", LocationX(155), LocationY(155));

    SetDialog(sel2, "YESNO", OnSelector2StartDialog, OnSelector2EndDialog);
    LookWithAngle(sel2, 96);
    StoryPic(sel2, "Warrior7Pic");

    int sel2Fx=CreateObjectAt("Magic", GetObjectX(sel2), GetObjectY(sel2));
    Frozen(sel2Fx, TRUE);
    SetUnitSubclass(sel2Fx, GetUnitSubclass(sel2Fx) ^ 1);
}

int DispositionTeleportHammer(short locationId)
{
    int sd=CreateObjectAt("GreatSword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_vampirism4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyG);
    return sd;
}

int GetUnitTopParent(int unit)
{
    int next;

    while (TRUE)
    {
        next = GetOwner(unit);
        if (next)
            unit = next;
        else
            break;
    }
    return unit;
}

int GetKillCreditTopParent()
{
    int *victim = GetMemory(0x979724);

    if (victim != NULLPTR)
    {
        int *attacker = victim[130];

        if (attacker != NULLPTR)
            return GetUnitTopParent(attacker[11]);
    }
    return NULLPTR;
}

int DispositionSpiderSpitSword(short locationId)
{
    int sd=CreateObjectAt("MorningStar", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_impact4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyE);
    return sd;
}

int DispositionFlyoutMecaDrone(short locationId)
{
    int sd=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyF);
    return sd;
}

int DispositionPixieSword(short locationId)
{
    int sd=CreateObjectAt("MorningStar", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyH);
    return sd;
}

int DispositionThrowingPotionSword(short locationId)
{
    int sd=CreateObjectAt("Sword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_venom4, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_stun3);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyI);
    return sd;
}

int DispositionArrowrainSword(short locationId)
{
    int sd=CreateObjectAt("GreatSword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyJ);
    return sd;
}

int DispositionAutodetectSword(short locationId)
{
    int sd=CreateObjectAt("GreatSword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyD);
    return sd;
}

int DispositionFistlineAxe(short locationId)
{
    int sd=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_impact4, ITEM_PROPERTY_vampirism4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyK);
    return sd;
}

void ThisMapInformationPrintMessage(int user)
{
    if (CurrentHealth(user))
    {
        UniPrint(user, "-그를 추격하는 고분가..- 고분가를 피해 다니며 그를 처치해야 합니다... 원작. Gurfan Malifor, 수정. NEW-A(신에이)");
        PlaySoundAround(user, SOUND_JournalEntryAdd);
    }
}

int DispositionThrowingFrogSword(short locationId)
{
    int sd=CreateObjectAt("GreatSword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_venom4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyC);
    return sd;
}

int DispositionMultipleThrowingStone(short locationId)
{
    int sd=CreateObjectAt("GreatSword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_confuse1, ITEM_PROPERTY_impact4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyB);
    return sd;
}

int DispositionThunderSword(short locationId)
{
    int ham=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(ham, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(ham, 3, m_pWeaponUserSpecialPropertyA);
    return ham;
}

int DispositionFlatusHammer(short locationId)
{
    int weapon=CreateObjectAt("WarHammer", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(weapon, ITEM_PROPERTY_PoisonProtect4, ITEM_PROPERTY_venom4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialProperty7);
    return weapon;
}

int DispositionBackstepHammer(short locationId)
{
    int weapon=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(weapon, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_fire1, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialProperty6);
    return weapon;
}

int DispositionEntityWeapon(short locationId)
{
    int weapon=CreateObjectAt("WarHammer", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(weapon, 0, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_impact4, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialPropertyQ);
    return weapon;
}

int DispositionUserMeteorSword(short locationId)
{
    int weapon=CreateObjectAt("GreatSword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(weapon, 0, ITEM_PROPERTY_fire1, ITEM_PROPERTY_fire2, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialProperty5);
    return weapon;
}

int DispositionUserWeaponInvoke(int fid, int arg1)
{
    return Bind(fid, &arg1);
}

int DispositionUserWeaponStarshot(short locationId)
{
    int weapon=CreateObjectAt("MorningStar", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(weapon, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_fire4, ITEM_PROPERTY_lightning1, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialProperty1);
    return weapon;
}

int DispositionUserWeaponStoneHammer(short locationId)
{
    int ham=CreateObjectAt("WarHammer", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(ham, ITEM_PROPERTY_weaponPower2, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_impact1, 0);
    SpecialWeaponPropertySetWeapon(ham, 3, m_pWeaponUserSpecialProperty2);
    return ham;
}

int DispositionUserWeaponShurikenSword(short locationId)
{
    int sd=CreateObjectAt("Sword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd,ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_impact1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialProperty3);
    return sd;
}

int DispositionUserWeaponStrikeHammer(short locationId)
{
    int ham=CreateObjectAt("WarHammer", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(ham,0,ITEM_PROPERTY_FireProtect1, ITEM_PROPERTY_stun1, 0);
    SpecialWeaponPropertySetWeapon(ham, 3, m_pWeaponUserSpecialProperty4);
    return ham;
}

void DeferredPickupUserWeapon(int weapon)
{
    if (IsObjectOn(weapon))
    {
        RegistItemPickupCallback(weapon, OnPickupUserWeapon);
        return;
    }
    int del;

    HashGet(m_userWeaponFxHash, weapon, &del, TRUE);
}

void OnPickupUserWeapon()
{
    int *pMem;

    FrameTimerWithArg(1, GetTrigger(), DeferredPickupUserWeapon);
    if (HashGet(m_userWeaponFxHash, GetTrigger(), &pMem, FALSE))
    {
        Delete(pMem[0]);
        Effect("CYAN_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void DeferredDiscardUserWeapon(int weapon)
{
    if (!MaxHealth(weapon))
        return;

    int *pMem;
    float xy[]={GetObjectX(weapon), GetObjectY(weapon)};
    if (HashGet(m_userWeaponFxHash, weapon, &pMem, FALSE))
    {
        pMem[0]=CreateObjectAt("BlueSummons", xy[0], xy[1]);
        PlaySoundAround(weapon, SOUND_BurnCast);
        Effect("SPARK_EXPLOSION", xy[0], xy[1], 0.0, 0.0);
        WriteLog("deferreddiscarduserweapon-ok");
    }
    else
    {
        WriteLog("fail hash error - deferredDiscardUserWeapon");
    }
    
}

void OnDiscardUserWeapon()
{
    FrameTimerWithArg(1, GetTrigger(), DeferredDiscardUserWeapon);
}

void CreateUserWeaponFX(int weapon)
{
    int *pMem;

    AllocSmartMemEx(4, &pMem);
    pMem[0]=CreateObjectAt("BlueSummons", GetObjectX(weapon), GetObjectY(weapon));
    HashPushback(m_userWeaponFxHash, weapon, pMem);
    RegistItemPickupCallback(weapon, OnPickupUserWeapon);
    SetUnitCallbackOnDiscardBypass(weapon, OnDiscardUserWeapon);
}

void CreateSpecialUserWeapon()
{
    short location = m_userWeaponLocations[Random(0, m_userWeaponLocationLength-1)];
    int ft[]={DispositionUserWeaponShurikenSword, DispositionUserWeaponStarshot, DispositionUserWeaponStoneHammer, DispositionUserWeaponStrikeHammer,
        DispositionTeleportHammer,DispositionTeleportHammer,DispositionTeleportHammer,DispositionTeleportHammer,DispositionPixieSword,
        DispositionUserMeteorSword, DispositionBackstepHammer, DispositionFlatusHammer, DispositionIceCrystalHammer, DispositionSpikeRingHammer, DispositionThunderSword,
        DispositionMultipleThrowingStone, DispositionThrowingFrogSword, DispositionAutodetectSword, DispositionSpiderSpitSword, DispositionFlyoutMecaDrone, DispositionTeleportHammer,
        DispositionThrowingPotionSword, DispositionArrowrainSword, DispositionFistlineAxe, DispositionPoisonTrapMace, DispositionEarthquakeHammer,
        DispositionPushKillerianAxe, DispositionChainArrow, DispositionBlueTailAxe, DispositionEntityWeapon, DispositionChainLightningMace};
    int weapon = DispositionUserWeaponInvoke(ft[Random(0, sizeof(ft)-1)], location);

    Frozen(weapon, TRUE);
    CreateUserWeaponFX(weapon);
    PlaySoundAround(weapon, SOUND_PlayerEliminated);
    DeleteObjectTimer(CreateObjectAt("BlueRain", GetObjectX(weapon), GetObjectY(weapon)), 30);
    Effect("COUNTERSPELL_EXPLOSION", LocationX(location), LocationY(location), 0.0, 0.0);
}

void EntityWeaponCbSecond()
{
    Entity2WindStrike(GetCaller());
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

static void onRottenMeatUse()
{
    if (CurrentHealth(OTHER))
    {
        int owner= GetUnit1C(SELF);

        if (GetCaller()==owner)
            owner=0;
        else if (!IsAttackedBy(OTHER, owner))
            owner=0;
        Damage(OTHER, owner, 160, DAMAGE_TYPE_POISON);
        DeleteObjectTimer( CreateObjectAt("GreenSmoke", GetObjectX(OTHER), GetObjectY(OTHER)), 12);
    }
    Delete(SELF);
}

static int placingPoisonTrap(float x, float y, int owner)
{
    int trap=CreateObjectAt("Soup",x,y);

    SetUnitCallbackOnUseItem(trap, onRottenMeatUse);
    SetUnit1C(trap, owner);
    return trap;
}

static void dropPoisonTrap()
{
    int pUser[32];
    int pIndex=GetPlayerIndex(OTHER);

    if (pIndex<0)
    {
        WriteLog("dropPoisonTrap--pIndex==-1");
        return;
    }
    int pUnit=GetCaller();
    int list=m_userTrapLinkedList[pIndex];

    if (pUser[pIndex]!=pUnit)
    {
        pUser[pIndex]=pUnit;
        if (list)
        {
            LinkedListDeleteInstance(list);
            m_userTrapLinkedList[pIndex]=0;
            list=0;
        }
    }

    if (!list)
    {
        LinkedListCreateInstance(&list);
        LinkedListPushback(list, 0, 0);
        LinkedListPushback(list, 0, 0);
        LinkedListPushback(list, 0, 0);
        LinkedListPushback(list, 0, 0);
        LinkedListPushback(list, 0, 0);
        LinkedListPushback(list, 0, 0);
        // LinkedListPushback(list, 0, 0);
        // LinkedListPushback(list, 0, 0);
        m_userTrapLinkedList[pIndex]=list;
    }
    else
    {
        int node;

        if (LinkedListFront(list, &node))
        {
            Delete( LinkedListGetValue(node) );
            LinkedListPopFront(list);
        }
    }
    int trap=placingPoisonTrap(GetObjectX(OTHER), GetObjectY(OTHER), GetCaller());

    LinkedListPushback(list, trap, 0);
    UniChatMessage(trap, "이거 독먹이임. 먹으면 죽어", 90);
}

int DispositionPoisonTrapMace(short locationId)
{
    int sd=CreateObjectAt("MorningStar", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_venom4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyL);
    return sd;
}

int DispositionEarthquakeHammer(short locationId)
{
    int sd=CreateObjectAt("WarHammer", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_lightning1, ITEM_PROPERTY_lightning1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyM);
    return sd;
}

int DispositionChainLightningMace(short locationId)
{
    int sd=CreateObjectAt("MorningStar", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_Speed4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyR);
    return sd;
}

int DispositionPushKillerianAxe(short locationId)
{
    int sd=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, &m_customPropertyFreeze1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyN);
    return sd;
}

int DispositionChainArrow(short locationId)
{
    int sd=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_PoisonProtect4, ITEM_PROPERTY_venom4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyO);
    return sd;
}

int DispositionBlueTailAxe(short locationId)
{
    int sd=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_PoisonProtect4, ITEM_PROPERTY_Speed4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyP);
    return sd;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}

void MapExit()
{
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}

void EntitySearchTarget(int entity, int *pTarget)
{
    float dist=9999.0;
    int count=sizeof(m_player), targ=0;

    while(--count>=0)
    {
        if (CurrentHealth(m_player[count]))
        {
            float cdis=DistanceUnitToUnit(entity, m_player[count]);

            if (cdis<dist)
            {
                dist=cdis;
                targ=m_player[count];
            }
        }
    }
    pTarget[0]=targ;
}

void EntityOnAlive(int entity)
{
    int target;

    EntitySearchTarget(entity, &target);
    if (!target)
    {
        Wander(entity);
        return;
    }
    CreatureFollow(entity, target);
    AggressionLevel(entity, 1.0);
}

void EntityAILoop()
{
    int *node, entity;

    LinkedListFront(m_entityList, &node);
    while (node)
    {
        entity=LinkedListGetValue(node);
        if (CurrentHealth(entity))
            EntityOnAlive(entity);
        LinkedListNext(node, &node);
    }
    SecondTimer(3, EntityAILoop);
}

#define _GOBUNGA_MARK_ 0
#define _GOBUNGA_XVECT_ 1
#define _GOBUNGA_YVECT_ 2
#define _GOBUNGA_COUNT_ 3
#define _GOBUNGA_DAMAGE_HASH_ 4
#define _GOBUNGA_SKILL_MAX_ 5

void OnGobungahWeaponCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            int hash;
            if (!HashGet(m_obungaHashData, GetTrigger(), &hash, FALSE))
                return;
            
            int result;
            if (HashGet(hash, GetCaller(), &result, FALSE))
                return;

            HashPushback(hash, GetCaller(), TRUE);
            Damage(OTHER, owner, 153, DAMAGE_TYPE_CRUSH);
            Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
    }
    else if (!GetCaller())
    {
        WallUtilDestroyWallAtObjectPosition(SELF);
    }
}

void GobungahCollideNothing()
{ }

void GobungahWeaponDestructor(int mark)
{
    SetCallback(mark, 9, GobungahCollideNothing);
    Frozen(mark, FALSE);
    Damage(mark, 0, 9999, DAMAGE_TYPE_PLASMA);
    DeleteObjectTimer(mark, 66);
    WriteLog("GobungahWeaponDestructor");
}

void GobungahWeaponTriggered(int *pArgs)
{
    int mark = pArgs[_GOBUNGA_MARK_];

    if (IsObjectOn(mark))
    {
        int *pCount = ArrayRefN(pArgs, _GOBUNGA_COUNT_), owner = GetOwner(mark);

        if (CurrentHealth(owner) &&-- pCount[0] >= 0 && IsVisibleTo(owner, mark))
        {
            FrameTimerWithArg(1, pArgs, GobungahWeaponTriggered);

            float *vect=ArrayRefN(pArgs, _GOBUNGA_XVECT_);
            MoveObjectVector(mark, vect[0], vect[1]);
            Effect("VIOLET_SPARKS", GetObjectX(mark), GetObjectY(mark), 0.0, 0.0);
            return;
        }
        // Delete(mark);
        GobungahWeaponDestructor(mark);
    }
    HashGet(m_obungaHashData, mark, 0, TRUE);
    HashDeleteInstance(pArgs[_GOBUNGA_DAMAGE_HASH_]);
    FreeSmartMemEx(pArgs);
    WriteLog("GobungahWeaponTriggered");
}

void gobungahWeaponCbImpl(int me)
{
    float vectX = UnitAngleCos(me, 25.0), vectY = UnitAngleSin(me, 25.0);
    int mark = CreateObjectAt("CarnivorousPlant", GetObjectX(me) + (vectX), GetObjectY(me) + (vectY));

    // WriteLog("GobungahWeaponCb:start");
    SetOwner(me, mark);
    SetUnitFlags(mark, GetUnitFlags(mark) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    Frozen(mark, TRUE);
    SetCallback(mark, 9, OnGobungahWeaponCollide);
    LookWithAngle(mark, GetDirection(me));

    int hash;
    HashCreateInstance(&hash);
    HashPushback(m_obungaHashData, mark, hash);

    int *args;
    AllocSmartMemEx(_GOBUNGA_SKILL_MAX_*4, &args);
    FrameTimerWithArg(1, args, GobungahWeaponTriggered);
    args[_GOBUNGA_MARK_]=mark;
    args[_GOBUNGA_XVECT_]=vectX;
    args[_GOBUNGA_YVECT_]=vectY;
    args[_GOBUNGA_COUNT_]=30;
    args[_GOBUNGA_DAMAGE_HASH_]=hash;
    // WriteLog("GobungahWeaponCb:end");
    
    PlaySoundAround(me, SOUND_MechGolemPowerUp);
}

void GobungahWeaponCb()
{
gobungahWeaponCbImpl(SELF);
}

#define LAISER_GAP 135.0
void spreadLaiserFx(float centerX, float centerY)
{
    int count = 36;

    if (centerX<LAISER_GAP)
        return;
    if (centerY<LAISER_GAP)
        return;
    while (--count>=0)
    {
        Effect("SENTRY_RAY", centerX, centerY, centerX+MathSine(count*10+90, LAISER_GAP), centerY+MathSine(count*10, LAISER_GAP));
    }
}

void WispDeathFx(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

static void collideExplosion()
{
    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
            Damage(OTHER, 0, 64, DAMAGE_TYPE_FLAME);
    }
}

static void explosionForAbit(int mark)
{
    while (IsObjectOn(mark))
    {
        if (GetDirection(mark))
        {
            LookWithAngle(mark,GetDirection(mark)-1);
            FrameTimerWithArg(1, mark, explosionForAbit);
            DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(mark), GetObjectY(mark)), 9);
            int c=DummyUnitCreateAt("Demon", GetObjectX(mark), GetObjectY(mark));

            DeleteObjectTimer(c,1);
            SetOwner(GetOwner(mark), c);
            SetCallback(c,9,collideExplosion);
            SetUnitFlags(c, GetUnitFlags(c)^UNIT_FLAG_NO_PUSH_CHARACTERS);
            break;
        }
        WispDeathFx(GetObjectX(mark), GetObjectY(mark));
        Delete(mark);
        break;
    }
}

void MonsterAttack3()
{
    int mark=CreateObjectAt("RewardMarkerPlus", GetObjectX(OTHER) + UnitAngleCos(OTHER, 20.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 20.0));

    UnitNoCollide(mark);
    spreadLaiserFx(GetObjectX(mark), GetObjectY(mark));
    SetOwner(SELF, mark);
    LookWithAngle(mark, 60);
    FrameTimerWithArg(3, mark, explosionForAbit);
}

#undef _GOBUNGA_MARK_
#undef _GOBUNGA_XVECT_
#undef _GOBUNGA_YVECT_
#undef _GOBUNGA_COUNT_
#undef _GOBUNGA_DAMAGE_HASH_
#undef _GOBUNGA_SKILL_MAX_

static void entityFirstweapon(int me){
    Raise(me, 100.0);
    PushObjectTo(me, UnitAngleCos(me, 60.0), UnitAngleSin(me, 60.0));
    SplashDamageAt(me, 100, GetObjectX(me), GetObjectY(me), 180.0);
    GreenExplosion(GetObjectX(me), GetObjectY(me));
    Effect("JIGGLE", GetObjectX(me), GetObjectY(me), 50.0, 0.0);
    PlaySoundAround(me, SOUND_MeteorShowerCast);
    PlaySoundAround(me, SOUND_CrushHard);
}

void EntityWeaponCb()
{
    entityFirstweapon(SELF); //other
}

void CreateEntityWeapon(int entity, int *pDestWeapon, int *pProperty)
{
    int weapon = CreateObjectAt("Sword", GetObjectX(entity), GetObjectY(entity));
    int *ptr=UnitToPtr(weapon);

    SetWeaponPropertiesDirect(weapon, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_fire4, ITEM_PROPERTY_impact4);
    SpecialWeaponPropertySetWeapon(weapon, 3, pProperty);
    Frozen(weapon, TRUE);
    ptr[178]=0x408fdb;
    if (pDestWeapon)
        pDestWeapon[0] = weapon;
}

void EntityPickAndSet(int *mem)
{
    int entity=mem[0], weapon=mem[1];

    if (CurrentHealth(entity))
    {
        Pickup(entity, weapon);
        NPCDressupEquipment(entity, weapon, TRUE);
    }
    FreeSmartMemEx(mem);
}

static void UnitScanProcessProto(int functionId, int posUnit)
{
    Bind(functionId, &functionId + 4);
}

int BearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[17] = 120; arr[19] = 50; arr[23] = 32768; arr[MOB_SCR_TABLE_RUN_MULTIPLE]=GOBUNGAH_RUN_MULTIPLE; 
		arr[25] = 1; arr[26] = 5; 
        // arr[27] = 1; 
        arr[28] = 1120403456; arr[29] = 20; 
		arr[30] = 1114636288; arr[31] = 2; arr[32] = 6; arr[33] = 12; arr[MOB_SCR_TABLE_STRIKE_HANDLER] = 5542784; 
		arr[60] = 1365; arr[61] = 46903040; 
	pArr = arr;
	return pArr;
}

int YoshieBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[17] = 120; arr[19] = 50; arr[23] = 32768; arr[MOB_SCR_TABLE_RUN_MULTIPLE]=GOBUNGAH_RUN_MULTIPLE; 
		arr[25] = 1; arr[26] = 5; 
        // arr[27] = 1; 
        arr[MOB_SCR_TABLE_MELEE_RANGE] = ToInt(125.0); arr[MOB_SCR_TABLE_MELEE_DAMAGE] = 20; 
		arr[30] = 1114636288; arr[MOB_SCR_TABLE_MELEE_DAMAGE_TYPE] = DAMAGE_TYPE_DRAIN; 
        arr[MOB_SCR_TABLE_MELEE_MIN_DELAY] = 3; arr[MOB_SCR_TABLE_MELEE_MAX_DELAY] = 6; arr[MOB_SCR_TABLE_STRIKE_HANDLER] = 5543680; 
		arr[60] = 1365; arr[61] = 46903040; arr[MOB_SCR_TABLE_POISON_CHANCE]=70;
        arr[MOB_SCR_TABLE_POISON_POWER]=10; arr[MOB_SCR_TABLE_POISON_MAX]=40;
	pArr = arr;
	return pArr;
}

int BearBinTableNoImpact()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[17] = 120; arr[19] = 50; arr[23] = 32768; arr[24]=GOBUNGAH_RUN_MULTIPLE;
		arr[25] = 1; arr[26] = 5;
        // arr[27] = 1;
         arr[28] = 1120403456; arr[29] = 20; 
		arr[31] = 2; arr[32] = 6; arr[33] = 12; arr[59] = 5542784; 
		arr[60] = 1365; arr[61] = 46903040; 
	pArr = arr;
	return pArr;
}

void BearSubProcess(int sUnit,char*entityName)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = 120;	hpTable[1] = 120;
	int *uec = ptr[187];
	uec[360] = 32768;
    if (entityName==m_entityName1) uec[121] = BearBinTable();
    else if (entityName==m_entityName4) uec[121]= YoshieBinTable();
    else uec[121]= BearBinTableNoImpact();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = GOBUNAG_MAX_HP; arr[18] = 200; arr[19] = 500; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1120403456; arr[29] = 20; arr[30] = 1106247680; 
		arr[31] = 2; arr[32] = 3; arr[33] = 6; arr[59] = 5542784; arr[60] = 2270; 
		arr[61] = 46913024; 
	pArr = arr;
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1097859072;		ptr[137] = 1097859072;
	int *hpTable = ptr[139];
	hpTable[0] = GOBUNAG_MAX_HP;	hpTable[1] = GOBUNAG_MAX_HP;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void EntityImageLoop(int *pArgs)
{
    int entity=pArgs[0];

    if (CurrentHealth(entity))
    {
        int img=pArgs[1];
        if (ToInt( DistanceUnitToUnit(entity, img)) )
        {
            if (UnitCheckEnchant(entity, GetLShift(ENCHANT_AFRAID)))
                Enchant(img, "ENCHANT_AFRAID", 0.0);
            else
                EnchantOff(img, "ENCHANT_AFRAID");
            MoveObject(img, GetObjectX(entity), GetObjectY(entity));
        }
        FrameTimerWithArg(1, pArgs, EntityImageLoop);
    }
    else
    {
        Delete(pArgs[1]);
        FreeSmartMemEx(pArgs);
    }
}

void SetNpcRunmultiple(int npc, float value)
{
    int *ptr=UnitToPtr(npc);
    int *pEC=ptr[187];

    pEC[332]=value;
}

#define _push_target_ 0
#define _push_vect_x 1
#define _push_vect_y 2
#define _push_max_ 3
void DeferredPushing(int *pMem)
{
    int targ=pMem[_push_target_];
    float *vect=ArrayRefN(pMem, _push_vect_x);

    if (IsObjectOn(targ))
        PushObjectTo(targ, vect[0], vect[1]);
    FreeSmartMemEx(pMem);
}

#define _push_force_ 51.0

void OnEntityCollide()
{
    if (!CurrentHealth(SELF))
        return;
        
    int uClass = GetUnitClass(OTHER);

    if (uClass&UNIT_CLASS_IMMOBILE)
        return;

    if (uClass & UNIT_CLASS_OBSTACLE)
    {
        int *pMem;

        AllocSmartMemEx(_push_max_*4, &pMem);
        FrameTimerWithArg(1, pMem, DeferredPushing);
        pMem[_push_target_]=GetCaller();
        if (ToInt(DistanceUnitToUnit(SELF, OTHER)))
        {
            pMem[_push_vect_x]=UnitRatioX(OTHER, SELF, _push_force_);
            pMem[_push_vect_y]=UnitRatioY(OTHER, SELF, _push_force_);
        }
        else
        {
            int angle=Random(0,360);
            pMem[_push_vect_x]=MathSine(angle+90, 51.0);
            pMem[_push_vect_y]=MathSine(angle, 51.0);
        }
    }
    if (IsAttackedBy(OTHER, SELF))
    {
        Damage(OTHER, 0, 10, DAMAGE_TYPE_IMPACT);
        DeleteObjectTimer(CreateObjectAt("OldSmoke", GetObjectX(SELF), GetObjectY(SELF)), 18);
        PlaySoundAround(SELF, SOUND_HitStoneBreakable);
    }
}
#undef _push_force_

#undef _push_target_
#undef _push_vect_x
#undef _push_vect_y
#undef _push_max_

void EraseEntity(int entity, int *pDstValue)
{
    int *value;

    HashGet(m_obungaWeaponData, entity, NULLPTR, TRUE);
    if (HashGet(m_entityInfoHash, entity, &value, TRUE))
    {
        LinkedListPop(m_entityList, value[_ENTITY_HASH_NODE_]);
        if (pDstValue)
            pDstValue[0]=value;
        else
            FreeSmartMemEx(value);
        m_currentEntityCount-=1;
    }
}

void ResetUnitSight(int unit)
{
    if (CurrentHealth(unit))
    {
        EnchantOff(unit, "ENCHANT_DETECTING");
        Enchant(unit, "ENCHANT_BLINDED", 0.07);
        AggressionLevel(unit, 1.0);
    }
}

int MakePoisoned(int victim, int attacker)
{
    char *pcode;

    if (!pcode)
    {
        char code[]={0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
            0xB8, 0x90, 0x96, 0x54, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x50, 0xB8, 0x30, 
            0x72, 0x50, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3, 0x90, 0x90};
        pcode=code;
    }
    int params[]={UnitToPtr(victim), UnitToPtr(attacker)};
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=pcode;
    int ret = Unused5e(params);
    pExec[0x5e]=pOld;
    return ret;
}

#define FLYING_FROG_SPEED 14.0

void FrogFlyingHandler(int ptr)
{
	int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

	if (CurrentHealth(owner) && CurrentHealth(target) && count)
	{
		if (IsVisibleTo(ptr + 1, target))
		{
            if (DistanceUnitToUnit(ptr + 1, target) > 29.0)
			{
				MoveObjectVector(ptr + 1, UnitRatioX(target, ptr + 1, FLYING_FROG_SPEED), UnitRatioY(target, ptr + 1, FLYING_FROG_SPEED));
				LookAtObject(ptr + 1, target);
				Walk(ptr + 1, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
				LookWithAngle(ptr, count - 1);
                FrameTimerWithArg(1, ptr, FrogFlyingHandler);
                return;
			}
            DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(target), GetObjectY(target)), 9);
            Damage(target, owner, 5, DAMAGE_TYPE_DRAIN);
            if (!IsPoisonedUnit(target))
                MakePoisoned(target, m_poisonUnit);
            ObjectOn(ptr + 1);
            Damage(ptr + 1, 0, MaxHealth(ptr + 1) + 1, -1);
		}
	}
    Delete(ptr);
    Delete(ptr + 1);
}

void SummonFrog(int owner, int target)
{
	int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));

	UnitNoCollide(CreateObjectAt("GreenFrog", GetObjectX(unit), GetObjectY(unit)));
	ObjectOff(unit + 1);
    SetUnitMaxHealth(unit+1, 0);
	SetOwner(owner, unit);
	Raise(unit, ToFloat(target));
    LookWithAngle(unit, 72);
    PlaySoundAround(owner, SOUND_BeholderDie);
	FrameTimerWithArg(1, unit, FrogFlyingHandler);
}

#define MULTIPLE_FROG_ME 0
#define MULTIPLE_FROG_YOU 1
#define MULTIPLE_FROG_COUNT 2
#define MULTIPLE_FROG_MAX 3

static void summonMultipleFrog(int *p)
{
    int me=p[MULTIPLE_FROG_ME];
    int you=p[MULTIPLE_FROG_YOU];

    if (CurrentHealth(me)&&CurrentHealth(you))
    {
        int count =p[MULTIPLE_FROG_COUNT];

        if (count)
        {
            --p[MULTIPLE_FROG_COUNT];
            SummonFrog(me, you);
            FrameTimerWithArg(1, p, summonMultipleFrog);
            return;
        }
    }
    FreeSmartMemEx(p);
}

void CheckResetSight(int unit, int delay)
{
    if (!UnitCheckEnchant(unit, GetLShift( ENCHANT_DETECTING)))
    {
        Enchant(unit, "ENCHANT_DETECTING", 7.0);
        FrameTimerWithArg(delay, unit, ResetUnitSight);
    }
}

void onYoshieSight()
{
	if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    {
        Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
        int *p;
        AllocSmartMemEx(MULTIPLE_FROG_MAX*4, &p);
        FrameTimerWithArg(1, p, summonMultipleFrog);
        p[MULTIPLE_FROG_ME]=GetTrigger();
        p[MULTIPLE_FROG_YOU]=GetCaller();
        p[MULTIPLE_FROG_COUNT]=6;
    }
    CheckResetSight(GetTrigger(), 180);
}

void OnEntityHurt()
{
    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 2, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void OnEntityDeath()
{
    WriteLog("onentityDeath");
    int value=0;
    EraseEntity(GetTrigger(), &value);
    if (value)
    {
        int *pMem=value, killUser=GetKillCreditTopParent();
        char printMsg[128], *userName = StringUtilGetScriptStringPtr("누군가");
        if (killUser!=0)
            userName=StringUtilGetScriptStringPtr(PlayerIngameNick(killUser));
        int params[]={userName, pMem[_ENTITY_HASH_STRING_]};

        NoxSprintfString(printMsg, "[!!] 방금 %s가 %s 을 죽였습니다", params, sizeof(params));
        UniPrintToAll(ReadStringAddressEx(printMsg));
        FreeSmartMemEx(pMem);
    }
    int count=Random(1,12);
    while (--count>=0)
        CreateObjectAt("RewardMarker", GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(SELF, 1);

    int already;
    if ((!already) && ++m_gobungaDeathCount >= m_gobungaMaxCount)
    {
        already=TRUE;
        OnThisGameEnded();
    }
    WriteLog("onentityDeath end");
}

void CreateEntityCommon(int entity, char *entityName)
{
    //WoundedApprenticeSubProcess(entity);
    BearSubProcess(entity, entityName);

    if (entityName!=m_entityName4)
        RegistUnitStrikeHook(entity);
    SetUnitMaxHealth(entity, m_gobungaMaxHP);
    AggressionLevel(entity, 1.0);
    SetUnitScanRange(entity, 600.0);
    // SetNpcRunmultiple(entity, 7.1);
    SetCallback(entity, 5, OnEntityDeath);
    SetCallback(entity, 7, OnEntityHurt);
    SetCallback(entity, 9, OnEntityCollide);
    int node;
    LinkedListPushback(m_entityList, entity, &node);
    int *pData;

    AllocSmartMemEx(_ENTITY_HASH_MAX_ * 4, &pData);
    pData[_ENTITY_HASH_NODE_]=node;
    pData[_ENTITY_HASH_STRING_]=entityName;
    HashPushback(m_entityInfoHash, entity, pData);
    // int *ptr=UnitToPtr(entity);

    // ptr[5] = 0x22;  //HIDE//
}

void CreateEntity(short locationId, int *pDestEntity, char *entityName, short mobType)
{
    int entity=CreateObjectById(mobType, LocationX(locationId), LocationY(locationId));

    if (!entityName)
        entityName=StringUtilGetScriptStringPtr("이름없음");
    CreateEntityCommon(entity, entityName);
    AttachHealthbar(entity);
    if (pDestEntity)
        pDestEntity[0]=entity;
}

void EntitySetImage(int entity, string imgName)
{
    int *args;
    AllocSmartMemEx(8, &args);
    args[0]=entity;
    int img=CreateObjectAt(imgName, GetObjectX(entity), GetObjectY(entity));
    UnitNoCollide(img);
    args[1]=img;

    FrameTimerWithArg(1, args, EntityImageLoop);
}

void EntitySetWeapon(int entity, int weapon)
{
    int *mem;
    AllocSmartMemEx(8, &mem);
    mem[0]=entity;
    mem[1]=weapon;
    FrameTimerWithArg(1, mem, EntityPickAndSet);
}

void InitializeEntities()
{
    HashCreateInstance(&m_entityInfoHash);
    LinkedListCreateInstance(&m_entityList);
    m_entityName1=StringUtilGetScriptStringPtr("Cursed fase");
    m_entityName2=StringUtilGetScriptStringPtr("고분가");
    m_entityName3=StringUtilGetScriptStringPtr("글롭글로갑갈랍");
    m_entityName4=StringUtilGetScriptStringPtr("YOSHIE");
}

void InitializeEntityWeapon()
{
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_VIOLET_SPARK, SOUND_NecromancerTaunt, &m_pWeaponPropertyFxCode1);
    SpecialWeaponPropertyCreate(&m_pWeaponProperty1);
    SpecialWeaponPropertyExecuteScriptCodeGen(EntityWeaponCb, &m_pWeaponPropertyExecutor1);
    // SpecialWeaponPropertySetFXCode(m_pWeaponProperty1, m_pWeaponPropertyFxCode1);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponProperty1, m_pWeaponPropertyExecutor1);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_PLASMA_SPARK, SOUND_NecromancerTaunt, &m_pWeaponPropertyFxCode2);
    SpecialWeaponPropertyCreate(&m_pWeaponProperty2);
    SpecialWeaponPropertyExecuteScriptCodeGen(GobungahWeaponCb, &m_pWeaponPropertyExecutor2);
    // SpecialWeaponPropertySetFXCode(m_pWeaponProperty2, m_pWeaponPropertyFxCode2);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponProperty2, m_pWeaponPropertyExecutor2);
}

void InitializeUserSpecialWeapon()
{
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_VIOLET_SPARK, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty1);
    SpecialWeaponPropertyExecuteScriptCodeGen(UserSpecialWeaponCbFirst, &m_pWeaponUserSpecialPropertyExecutor1);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyExecutor1);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_VIOLET_SPARK, SOUND_HitStoneBreakable, &m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty2);
    SpecialWeaponPropertyExecuteScriptCodeGen(UserSpecialWeaponCbSecond, &m_pWeaponUserSpecialPropertyExecutor2);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyExecutor2);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_THIN_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty3);
    SpecialWeaponPropertyExecuteScriptCodeGen(UserSpecialWeaponCbThird, &m_pWeaponUserSpecialPropertyExecutor3);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyExecutor3);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_THIN_EXPLOSION, SOUND_MagicMissileDetonate, &m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty4);
    SpecialWeaponPropertyExecuteScriptCodeGen(UserSpecialWeaponCb4, &m_pWeaponUserSpecialPropertyExecutor4);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyExecutor4);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_THIN_EXPLOSION, SOUND_MagicMissileDetonate, &m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty5);
    SpecialWeaponPropertyExecuteScriptCodeGen(UserSpecialWeaponCb5, &m_pWeaponUserSpecialPropertyExecutor5);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyExecutor5);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_YELLOW_SPARKS, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode6);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty6);
    SpecialWeaponPropertyExecuteScriptCodeGen(BackstepHammer, &m_pWeaponUserSpecialPropertyExecutor6);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty6, m_pWeaponUserSpecialPropertyFxCode6);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty6, m_pWeaponUserSpecialPropertyExecutor6);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SMOKE_BLAST, SOUND_TrollHurt, &m_pWeaponUserSpecialPropertyFxCode7);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty7);
    SpecialWeaponPropertyExecuteScriptCodeGen(FlatusHammer, &m_pWeaponUserSpecialPropertyExecutor7);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty7, m_pWeaponUserSpecialPropertyFxCode7);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty7, m_pWeaponUserSpecialPropertyExecutor7);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SMOKE_BLAST, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode8);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty8);
    SpecialWeaponPropertyExecuteScriptCodeGen(CUserWeaponShootIceCrystal, &m_pWeaponUserSpecialPropertyExecutor8);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty8, m_pWeaponUserSpecialPropertyFxCode8);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty8, m_pWeaponUserSpecialPropertyExecutor8);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode9);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty9);
    SpecialWeaponPropertyExecuteScriptCodeGen(CUserWeaponShotSpikeRing, &m_pWeaponUserSpecialPropertyExecutor9);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty9, m_pWeaponUserSpecialPropertyFxCode9);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty9, m_pWeaponUserSpecialPropertyExecutor9);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_YELLOW_SPARKS, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeA);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyA);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &m_pWeaponUserSpecialPropertyExecutorA);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyA, m_pWeaponUserSpecialPropertyFxCodeA);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyA, m_pWeaponUserSpecialPropertyExecutorA);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeB);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyB);
    SpecialWeaponPropertyExecuteScriptCodeGen(MultipleThrowingStone, &m_pWeaponUserSpecialPropertyExecutorB);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyB, m_pWeaponUserSpecialPropertyFxCodeB);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyB, m_pWeaponUserSpecialPropertyExecutorB);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeC);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyC);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThrowingFrogTriggered, &m_pWeaponUserSpecialPropertyExecutorC);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyC, m_pWeaponUserSpecialPropertyFxCodeC);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyC, m_pWeaponUserSpecialPropertyExecutorC);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeD);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyD);
    SpecialWeaponPropertyExecuteScriptCodeGen(AutoDetectingTriggered, &m_pWeaponUserSpecialPropertyExecutorD);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyD, m_pWeaponUserSpecialPropertyFxCodeD);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyD, m_pWeaponUserSpecialPropertyExecutorD);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeE);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyE);
    SpecialWeaponPropertyExecuteScriptCodeGen(TakeShotSpiderWeb, &m_pWeaponUserSpecialPropertyExecutorE);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyE, m_pWeaponUserSpecialPropertyFxCodeE);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyE, m_pWeaponUserSpecialPropertyExecutorE);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeF);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyF);
    SpecialWeaponPropertyExecuteScriptCodeGen(TakeoffMecaFlying, &m_pWeaponUserSpecialPropertyExecutorF);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyF, m_pWeaponUserSpecialPropertyFxCodeF);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyF, m_pWeaponUserSpecialPropertyExecutorF);

    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeG);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyG);
    SpecialWeaponPropertyExecuteScriptCodeGen(TeleportHammerTriggered, &m_pWeaponUserSpecialPropertyExecutorG);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyG, m_pWeaponUserSpecialPropertyFxCodeG);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyG, m_pWeaponUserSpecialPropertyExecutorG);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeH);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyH);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartContinuousPixie, &m_pWeaponUserSpecialPropertyExecutorH);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyH, m_pWeaponUserSpecialPropertyFxCodeH);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyH, m_pWeaponUserSpecialPropertyExecutorH);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyI);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartThrowingStuff, &m_pWeaponUserSpecialPropertyExecutorI);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyI, m_pWeaponUserSpecialPropertyFxCodeI);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SPARK_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeI);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyI, m_pWeaponUserSpecialPropertyExecutorI);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyJ);
    SpecialWeaponPropertyExecuteScriptCodeGen(startArrowRain, &m_pWeaponUserSpecialPropertyExecutorJ);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyJ, m_pWeaponUserSpecialPropertyFxCodeJ);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_PLASMA_SPARK, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeJ);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyJ, m_pWeaponUserSpecialPropertyExecutorJ);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyK);
    SpecialWeaponPropertyExecuteScriptCodeGen(StrikeFistLine, &m_pWeaponUserSpecialPropertyExecutorK);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyK, m_pWeaponUserSpecialPropertyFxCodeK);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SMOKE_BLAST, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeK);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyK, m_pWeaponUserSpecialPropertyExecutorK);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyL);
    SpecialWeaponPropertyExecuteScriptCodeGen(dropPoisonTrap, &m_pWeaponUserSpecialPropertyExecutorL);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyL, m_pWeaponUserSpecialPropertyFxCodeL);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeL);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyL, m_pWeaponUserSpecialPropertyExecutorL);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyM);
    SpecialWeaponPropertyExecuteScriptCodeGen(strikeEarthquake, &m_pWeaponUserSpecialPropertyExecutorM);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyM, m_pWeaponUserSpecialPropertyFxCodeM);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeM);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyM, m_pWeaponUserSpecialPropertyExecutorM);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyN);
    SpecialWeaponPropertyExecuteScriptCodeGen(PushKillerian, &m_pWeaponUserSpecialPropertyExecutorN);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyN, m_pWeaponUserSpecialPropertyFxCodeN);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_LESSER_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeN);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyN, m_pWeaponUserSpecialPropertyExecutorN);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyO);
    SpecialWeaponPropertyExecuteScriptCodeGen(CastingChainArrow, &m_pWeaponUserSpecialPropertyExecutorO);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyO, m_pWeaponUserSpecialPropertyFxCodeO);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SMOKE_BLAST, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeO);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyO, m_pWeaponUserSpecialPropertyExecutorO);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyP);
    SpecialWeaponPropertyExecuteScriptCodeGen(CastingBlueTail, &m_pWeaponUserSpecialPropertyExecutorP);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyP, m_pWeaponUserSpecialPropertyFxCodeP);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_RICOCHET, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeP);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyP, m_pWeaponUserSpecialPropertyExecutorP);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyQ);
    SpecialWeaponPropertyExecuteScriptCodeGen(EntityStartWindStrike, &m_pWeaponUserSpecialPropertyExecutorQ);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyQ, m_pWeaponUserSpecialPropertyFxCodeQ);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SMOKE_BLAST, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeQ);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyQ, m_pWeaponUserSpecialPropertyExecutorQ);

    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyR);
    SpecialWeaponPropertyExecuteScriptCodeGen(startChainLightning, &m_pWeaponUserSpecialPropertyExecutorR);
    // SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyR, m_pWeaponUserSpecialPropertyFxCodeR);
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_RICOCHET, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeR);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyR, m_pWeaponUserSpecialPropertyExecutorR);
}

void InitializeUserRespawnPoint()
{
    short points[]=
    {
        27,28,29,30,31,32,33,34,35,36,23,24,    10,
        37,38,5,6
    };
    m_userRespawnPoints=points;
    m_userRespawnPointLength=sizeof(points);
}

void InitUserWeaponFx()
{
    HashCreateInstance(&m_userWeaponFxHash);
}

void InitializeEntityRespawnPoint()
{
    short points[]=
    {
        49,2,107,109,103,112,99,24,13,67,19,5,42,40,61,10,80,100,17
    };
    m_entityRespawnPoints=points;
    m_entityRespawnPointLength=sizeof(points);
}

void InitializeUserWeaponSpawnPoint()
{
    short locations[]=
    {
        75,23,41,113,477,310,114,51,115,58,199,116,117,118,483,88,92,119,120,121,104,122,72,
        123,124,125,126,100,35,127,15,64,16, 455,327,274,195
    };
    m_userWeaponLocations=locations;
    m_userWeaponLocationLength=sizeof(locations);
}

void PlayerClassOnShutdown(int rep)
{
    m_player[rep]=0;
    m_pDeath[rep]=0;
    m_pFlag[rep]=0;
}

void PlayerClassOnDeath(int rep, int user)
{
    WriteLog("playerclassondeath");
    char dieMsg[128];
    int params[]={StringUtilGetScriptStringPtr(PlayerIngameNick(user)), ++m_pDeath[rep]};

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다 (%d번째 사망)", params, sizeof(params));
    UniPrintToAll(ReadStringAddressEx(dieMsg));
    WriteLog("playerclassondeath end");
}

#define WINDBOOSTER_DISTANCE 70.0

static void skillSetWindBooster(int pUnit)
{
    PushObjectTo(pUnit, UnitAngleCos(pUnit, WINDBOOSTER_DISTANCE), UnitAngleSin(pUnit, WINDBOOSTER_DISTANCE));
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void PlayerClassOnAlive(int rep, int user)
{
    if (UnitCheckEnchant(user, GetLShift(ENCHANT_SNEAK)))
    {
        skillSetWindBooster(user);
        RemoveTreadLightly(user);
    }
}

void PlayerClassOnLoop()
{
    int rep = sizeof(m_player);

    while (--rep>=0)
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[rep]))
            {
                if (GetUnitFlags(m_player[rep]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[rep]))
                {
                    PlayerClassOnAlive(rep, m_player[rep]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(rep, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(rep, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(rep, m_player[rep]);
                    }
                    break;
                }                
            }
            if (m_pFlag[rep])
                PlayerClassOnShutdown(rep);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void EntityType1(int *pDestEntity)
{
    int entity; //, weapon;

    CreateEntity(m_entityRespawnPoints[Random(0, m_entityRespawnPointLength-1)], &entity, m_entityName1,OBJ_NECROMANCER);
    SetUnitVoice(entity, 33);
    HashPushback( m_obungaWeaponData, entity, EntityWeaponCb );
    SetMonsterPoisonImmune(entity);
    // EntitySetImage(entity, "MovableCrypt2");
    pDestEntity[0]=entity;
}

void EntityType2(int *pDestEntity)
{
    int entity;

    CreateEntity(m_entityRespawnPoints[Random(0, m_entityRespawnPointLength-1)], &entity, m_entityName2,OBJ_WOUNDED_APPRENTICE);
    SetUnitVoice(entity, 11);
    // EntitySetImage(entity, "MovableCrypt1");
    HashPushback( m_obungaWeaponData, entity, GobungahWeaponCb );
    pDestEntity[0]=entity;
}

void EntityType3(int *p)
{
    int e;

    CreateEntity(m_entityRespawnPoints[Random(0, m_entityRespawnPointLength-1)], &e, m_entityName3,OBJ_AIRSHIP_CAPTAIN); 
    SetUnitVoice(e, 11);
    // EntitySetImage(e, "OgreBench3");
    HashPushback(m_obungaWeaponData, e, MonsterAttack3);
    p[0]=e;
}

void EntityType4(int *p)
{
    int e;

    CreateEntity(m_entityRespawnPoints[Random(0, m_entityRespawnPointLength-1)], &e, m_entityName4,OBJ_GOON);
    SetUnitVoice(e, 32);
    // EntitySetImage(e, "OgreBench4");
    SetCallback(e, 3, onYoshieSight);
    if (!IsMonsterPoisonImmune(e))
        SetMonsterPoisonImmune(e);
    // HashPushback(m_obungaWeaponData, e, MonsterAttack3);
    p[0]=e;
}

void EntityTypeInvoke(int fid, int *arg)
{
    Bind(fid, &fid + 4);
}

void ExtenedEntityType(int *p)
{
    if (Random(0, 2))
    {
        EntityType3(p);
        return;
    }
    EntityType4(p);
}

void RespawnEntity()
{
    int entityCreator[]={EntityType2, EntityType1, ExtenedEntityType, EntityType2}, entity;
    int selectCreator=EntityType2;

    if (m_entityCount>1)
        selectCreator=entityCreator[ Random(0, sizeof(entityCreator)-1) ];
    EntityTypeInvoke(selectCreator, &entity);

    int *pValue;
    char *name=StringUtilGetScriptStringPtr("Untitled");

    if (HashGet(m_entityInfoHash, entity, &pValue, FALSE))
        name=pValue[_ENTITY_HASH_STRING_];
    char printMsg[192];
    int params[]={m_entityCount,name};

    NoxSprintfString(printMsg, "방금 %d번째 엔티티, %s이 생겨났습니다, 모두 긴장 빠십시오!!", params, sizeof(params));
    UniPrintToAll(ReadStringAddressEx(printMsg));
    ++m_currentEntityCount;

    FrameTimer(3, ServerSendBrief);
}

void RespawnEntityLoop(int time)
{
    if (time > 0)
    {
        if (!m_currentEntityCount)
        {
            if (time>38)
            {
                time=10;
                UniPrintToAll("엔티티가 전멸했으므로, 후속 엔티티가 10초 후 등장합니다");
            }
        }
        if (++m_userSpecialWeaponRespawnTime>=m_userSpecialWeaponRespawnTimeLimit)
        {
            m_userSpecialWeaponRespawnTime=0;
            m_userSpecialWeaponRespawnTimeLimit+=8;
            CreateSpecialUserWeapon();
        }
        SecondTimerWithArg(1, --time, RespawnEntityLoop);
        return;
    }
    int seconds=m_entityRespawnTimeTable[m_currentEntityCount];
    if (++m_entityCount < m_gobungaMaxCount)
    {
        SecondTimerWithArg(3, seconds, RespawnEntityLoop);
    }
    RespawnEntity();
}

void CSpawnFieldMonster()
{
    int mob =
    CreateObjectAt("Mimic", GetObjectX(OTHER), GetObjectY(OTHER));
    SetUnitMaxHealth(mob, 600);
}

void CUserWeaponUnitTest()
{
    short locationId=130;
    DispositionAutodetectSword(locationId++);
    DispositionBackstepHammer(locationId++);
    DispositionFlatusHammer(locationId++);
    DispositionIceCrystalHammer(locationId++);
    DispositionMultipleThrowingStone(locationId++);
    DispositionSpikeRingHammer(locationId++);
    DispositionThrowingFrogSword(locationId++);
    DispositionThunderSword(locationId++);
    DispositionUserMeteorSword(locationId++);
    DispositionUserWeaponShurikenSword(locationId++);
    DispositionUserWeaponStarshot(locationId++);
    DispositionUserWeaponStoneHammer(locationId++);
    DispositionUserWeaponStrikeHammer(locationId++);
}

void InitMapNotifications()
{
    RegistSignMessage(Object("mapsign1"), "그를 추격하는 고분가..- Gurfan Malifor 의 원작을 신에이(NEW-A) 가 수정함");
    RegistSignMessage(Object("mapsign2"), "이 팻말에는 '목재창고' 라고 적혀있어요");
    RegistSignMessage(Object("mapsign3"), "수심이 깊이므로 들어가지 마시오");
    RegistSignMessage(Object("mapsign4"), "이곳은 '바이올런스 가' 입니다");
    RegistSignMessage(Object("mapsign5"), "불꽃을 클릭하여 게임 옵션을 변경할 수 있어요 --신에이(New-A)");
    RegistSignMessage(Object("mapsign6"), "게임을 하려면 창문에 붙은 스위치를 누르시기 바랍니다");
}

void InitDialog()
{
    int *wndptr=0x6e6a58;
    GUIFindChild(wndptr[0], 3901, &m_dialogCtx);
}

void LoopPutRandomItem(int count)
{
    if (count)
    {
        PutRandomItem();
        PutRandomItem();
        FrameTimerWithArg(1, count-1, LoopPutRandomItem);
    }
}

static void onHiddenNpcDialogStart()
{
    UniPrint(OTHER, "오늘 시간어때요 오빠?");
    TellStoryUnitName("AA", "bindevent:NullEvent", "술집여자");
}

static void onHiddenNpcNormal()
{ }

static void onHiddenNpcDialogCompleted()
{
    int result=GetAnswer(SELF), once;

    if (once)
        return;
    once=TRUE;
    SetDialog(SELF, "NORMAL", onHiddenNpcNormal, onHiddenNpcNormal);
    if (result==1)
    {
        m_fieldItemSpawnAmount=200;
        UniChatMessage(SELF, "고마워효~(하트)", 150);
    }
}

static void settinghiddenNpc()
{
    int npc= DummyUnitCreateAt("WizardGreen", LocationX(484), LocationY(484));

    StoryPic(npc, "MaidenPic4");
    SetDialog(npc, "YESNO", onHiddenNpcDialogStart, onHiddenNpcDialogCompleted);
}

void DeferredInit()
{
    CreateLogFile("mansions2-log.txt");
    MakeCoopTeam();
    PutSelect();
    CreateObjectAt("BlackPowderBarrel", LocationX(462), LocationY(462));
    CreateObjectAt("BlackPowderBarrel2", LocationX(463), LocationY(463));
    CreateObjectAt("BlackPowderBarrel2", LocationX(464), LocationY(464));

    CreateObjectAt("TargetBarrel1", LocationX(468), LocationY(468));
    CreateObjectAt("TargetBarrel1", LocationX(469), LocationY(469));
    CreateObjectAt("TargetBarrel2", LocationX(470), LocationY(470));
    CreateObjectAt("TargetBarrel1", LocationX(471), LocationY(471));
    CreateObjectAt("TargetBarrel2", LocationX(472), LocationY(472));
    CreateObjectAt("TargetBarrel2", LocationX(473), LocationY(473));
    settinghiddenNpc();
}

void PostGuide()
{
    UniPrintToAll("-그를 추격하는 고분가.. -     Gurfan Malifor원작. 번역 및 수정- 신에이(New-A)");
    UniPrintToAll("[!!] 고분가를 피해 도망가세요! 고분가는 옵션에 정해진 수만큼 일정시간마다 등장합니다");
    UniPrintToAll("[!!] 고분가를 죽일 수도 있어요, 일정시간 마다 마법 무기가 생성되므로 그것을 사용하세요");
}

void ShowOptions()
{
    char msg[192];
    int params[]={m_gobungaMaxCount, m_gobungaMaxHP};

    NoxSprintfString(msg, "고분가 최대%d개, 체력%d 으로 게임이 설정되었습니다", params, sizeof(params));
    UniPrintToAll(ReadStringAddressEx(msg));
    SecondTimer(10, PostGuide);
}

void popupBeastScrollAllPlayer()
{
    char packet[]={0xD1, FIELD_GUIDE_BEHOLDER, 1};

    SendPacketAll(packet, sizeof(packet));
}

void SelectCompleted()
{
    if (m_selected)
        return;

    WriteLog("Selectedcompleted-start");
    m_selected=TRUE;
    FrameTimerWithArg(3, 20, RespawnEntityLoop);
    int count=5;
    FrameTimer(3, ShowOptions);
    while (--count>=0)
    {
        WallUtilOpenWallAtObjectPosition(156);
        TeleportLocationVector(156, 23.0, 23.0);
    }
    WriteLog("Selectedcompleted-mid");
    int join=sizeof(m_player);
    while (--join>=0)
    {
        if (CurrentHealth(m_player[join]))
        {
            PlayerClassOnEntry(m_player[join]);
        }
    }
    LoopPutRandomItem(m_fieldItemSpawnAmount);
    FrameTimer(15, popupBeastScrollAllPlayer);
    WriteLog("Selectedcompleted-end");
}

void OnEndedUnitScan()
{
    UniPrintToAll("dim.c::OnEndedUnitScan");
}

void UnitScanLoop(int cur)
{
    int rep = 30, action, thingId;

    while (rep--)
    {
        if (++cur >= m_lastCreatedUnit)
        {
            OnEndedUnitScan();
            return;
        }
        thingId=GetUnitThingID(cur);
        if (thingId)
        {
            if (HashGet(m_unitscanHashInstance, thingId, &action, FALSE))
                UnitScanProcessProto(action, cur);
        }
    }
    Nop(FrameTimerWithArg(1, cur, UnitScanLoop));
}

static void startUnitScan(int cur)
{
    InitItemFunctionTable();
    UniPrintToAll("mansion2.c::StartUnitScan");
    UnitScanLoop(cur);
}

void CreateRandomItem(int posUnit)
{
    CreateRandomItemCommon(posUnit, m_itemFunctionTable, m_itemFunctionTableLength);
}

static void createRandomItemTwoArg(int posUnit, int owner)
{
    int sub=CreateObjectAt("AmbBeachBirds", GetObjectX(posUnit) ,GetObjectY(posUnit));

    Delete(posUnit);
    CreateRandomItem(sub);
    Delete(sub);
}

static void initEntityRespawnTimeTable()
{
    int *p=m_entityRespawnTimeTable;

    NoxDwordMemset(p, sizeof(m_entityRespawnTimeTable), 300);
    p[0]=130;
    p[1]=180;
    p[2]=240;
}

static void initalTest()
{
    DispositionThrowingPotionSword(2);
}
static void initConfuseLevels()
{
    m_confuseLevels[0]=0.6;
    m_confuseLevels[1]=1.2;
    m_confuseLevels[2]=1.8;
    m_confuseLevels[3]=2.4;
    m_confuseLevels[4]=3.0;
    m_confuseLevels[5]=3.2;
    m_confuseLevels[6]=3.6;
    m_confuseLevels[7]=4.0;
}

static void memorySettingIfNotEqual(int *addr, int expect)
{
    if (addr[0]!=expect)
        addr[0]=expect;
}

static void disableNoLimitFlag()
{
    int set = 1;
    memorySettingIfNotEqual(0x587054, set);
    memorySettingIfNotEqual(0x5d4728, 4 - set);
    memorySettingIfNotEqual(0x5d47c4, 2 - set);
    memorySettingIfNotEqual(0x59dc08, set);
}

#define THING_ID_REWARD_MARKER 2672

static void initPoisonUnit()
{
    m_poisonUnit=DummyUnitCreateAt("Bomber", LocationX(485), LocationY(485));
    UnitLinkBinScript(m_poisonUnit, YoshieBinTable());
}
static int yellowPotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateYellowPotion(GetObjectX(del), GetObjectY(del), 125);
    Delete(del);
    return pot;
}
static int blackPotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateBlackPotion(GetObjectX(del), GetObjectY(del), 85);
    Delete(del);
    return pot;
}
static int whitePotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateWhitePotion(GetObjectX(del), GetObjectY(del), 100);
    Delete(del);
    return pot;
}
static void onGodPotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_INVULNERABLE), 25.0);
}
static int godPotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onGodPotionUse);
    return pot;
}

static void onHastePotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_HASTED), 0.0);
}

static int hastePotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onHastePotionUse);
}

static void onVampPotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_VAMPIRISM), 0.0);
}

static int vampPotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onVampPotionUse);
}

static int potionHashProto(int functionId, int pot)
{
    return Bind(functionId, &functionId+4);
}

static void initPotionHash()
{
    HashCreateInstance(&m_potionHash);

    HashPushback(m_potionHash, 639, yellowPotionHash);
    HashPushback(m_potionHash, 640, whitePotionHash);
    HashPushback(m_potionHash, 641, blackPotionHash);
    HashPushback(m_potionHash, 2686, godPotionHash);
    HashPushback(m_potionHash, 2679, hastePotionHash);
    HashPushback(m_potionHash, 2682, vampPotionHash);
}

void MapInitialize()
{
    MusicEvent();
    disableNoLimitFlag();
    m_lastCreatedUnit=CreateObjectAt("SpellBook", 100.0, 100.0);
    initPoisonUnit();
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);

    FrameTimer(1, DeferredInit);
    FrameTimer(3, PlayerClassOnLoop);
    FrameTimer(30, EntityAILoop);
    
    InitUserWeaponFx();
    InitializeUserRespawnPoint();
    InitializeEntityRespawnPoint();
    InitializeUserWeaponSpawnPoint();
    InitializeEntityWeapon();
    InitializeEntities();
    InitializeUserSpecialWeapon();
    initEntityRespawnTimeTable();
    initConfuseLevels();
    initWeaponProperty();
    initializeCustomHit();
    InitDialog();
    InitOptions();

    HashCreateInstance(&m_obungaHashData);
    HashCreateInstance(&m_obungaWeaponData);
    HashCreateInstance(&m_unitscanHashInstance);
    initPotionHash();
    HashPushback(m_unitscanHashInstance, THING_ID_REWARD_MARKER, CreateRandomItem);

    InitMapNotifications();
    FrameTimerWithArg(1, Object("firstscan"),startUnitScan);
}

static void NetworkUtilClientMain()
{
    ClientsideProcess();
    InitDialog();
    InitOptions();
    FrameTimer(10, ClientProcLoop);
}

int CheckPlayer()
{
    int rep = sizeof(m_player);

    while(--rep>=0)
    {
        if (IsCaller(m_player[rep]))
            break;
    }
    return rep;
}

int CheckPlayerWithId(int pUnit)
{
    int rep=sizeof(m_player);

    while (--rep>=0)
    {
        if (m_player[rep]^pUnit)
            continue;       
        break;
    }
    return rep;
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

int PlayerClassOnInit(int plr, int pUnit, char *userInit)
{
    m_player[plr]=pUnit;
    m_pFlag[plr]=1;
    m_pDeath[plr]=0;

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
            ClientsideProcess();
        FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);
        userInit[0] = TRUE;
    }
    Enchant(pUnit, "ENCHANT_BLINDED", 0.8);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    EmptyAll(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));
    return plr;
}

void PlayerDispatchOption(int user)
{
    MoveObject(user, LocationX(153), LocationY(153));
    char msg[192];
    int params[]={m_gobungaMaxCount, m_gobungaMaxHP};
    NoxSprintfString(msg, "게임 옵션을 선택하세요, 선택하지 않으면 디폴트 값 [최대 수: %d, 체력 %d] 으로 설정됩니다", params, sizeof(params));
    UniPrint(user, ReadStringAddressEx(msg));
}

void PlayerClassOnJoin(int plr)
{
    int user = m_player[plr];

    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    // if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
    //     PlayerSetAllBuff(pUnit);
    if (m_selected)
        TeleportPlayerArbitrary(user);
    else
        PlayerDispatchOption(user);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
    EnchantOff(user,"ENCHANT_ANTI_MAGIC");
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(12), LocationY(12));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int plr)
{
    MoveObject(user, LocationX(474), LocationY(474));
    Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    FrameTimerWithArg(3, plr, PlayerClassOnJoin);
    UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
}

void PlayerClassOnEntry(int plrUnit)
{
    WriteLog("PlayerClassOnEntry:start");
    while (TRUE)
    {
        if (!CurrentHealth(plrUnit))
            break;
        int plr = CheckPlayerWithId(plrUnit), rep = sizeof(m_player);
        char initialUser=FALSE;

        while (--rep>=0&&plr<0)
        {
            if (!MaxHealth(m_player[rep]))
            {
                plr = PlayerClassOnInit(rep, plrUnit, &initialUser);
                FrameTimerWithArg(66, plrUnit, ThisMapInformationPrintMessage);
                break;
            }
        }
        if (plr >= 0)
        {
            if (initialUser)
                OnPlayerDeferredJoin(plrUnit, plr);
            else
                PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(plrUnit);
        break;
    }
    WriteLog("PlayerClassOnEntry:end");
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (CheckPlayer() >= 0)
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(26), LocationY(26));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

void TeleportPlayerArbitrary(int user)
{
    short location = m_userRespawnPoints[Random(0, m_userRespawnPointLength-1)];

    MoveObject(user, LocationX(location), LocationY(location));
}

#define _Healing_unit_ 0
#define _Healing_time_ 1
#define _Healing_owner_ 2
#define _Healing_max_ 3

void OnUnitHealingLoop(int *pMem)
{
    int sub=pMem[_Healing_unit_];

    if (IsObjectOn(sub))
    {
        int owner=pMem[_Healing_owner_], *count=ArrayRefN(pMem,_Healing_time_);
        if (CurrentHealth(owner) && --count[0]>=0)
        {
            FrameTimerWithArg(2, pMem, OnUnitHealingLoop);
            if (ToInt(DistanceUnitToUnit(sub, owner)))
                MoveObject(sub, GetObjectX(owner), GetObjectY(owner));
            RestoreHealth(owner, 1);
            return;
        }
        WispDestroyFX(GetObjectX(sub), GetObjectY(sub));
        Delete(sub);
    }
    FreeSmartMemEx(pMem);
}

void DrinkWater(int unit)
{
    if (UnitCheckEnchant(unit, GetLShift(ENCHANT_ETHEREAL)))
        return;

    int sub=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit));

    SetUnitEnchantCopy(sub, GetLShift(ENCHANT_RUN));

    int *pMem;
    AllocSmartMemEx(_Healing_max_*4, &pMem);
    pMem[_Healing_time_]=120;
    pMem[_Healing_owner_]=unit;
    pMem[_Healing_unit_]=sub;
    FrameTimerWithArg(1, pMem, OnUnitHealingLoop);
    Enchant(unit, "ENCHANT_ETHEREAL", 7.0);
}

void HelloWorld()
{
    if (IsPlayerUnit(OTHER))
        UniPrint(OTHER, "이 우물이 잠시동안 당신의 체력을 지속적으로 회복해 줄 것입니다");
    DrinkWater(GetCaller());
}

void weapontest()
{
    CreateEntityWeapon(OTHER, 0, m_pWeaponProperty1);
}

int DispositionIceCrystalHammer(short locationId)
{
    int weapon=CreateObjectAt("BattleAxe", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(weapon, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialProperty8);
    return weapon;
}

int DispositionSpikeRingHammer(short locationId)
{
    int ham=CreateObjectAt("GreatSword", LocationX(locationId), LocationY(locationId));

    SetWeaponPropertiesDirect(ham, ITEM_PROPERTY_weaponPower4, ITEM_PROPERTY_impact4, ITEM_PROPERTY_lightning2, 0);
    SpecialWeaponPropertySetWeapon(ham, 3, m_pWeaponUserSpecialProperty9);
    return ham;
}

static void MonsterStrikeDefaultCallback()
{
//     int thingId=GetUnitThingID(SELF);

//     if (thingId==1364)
//     {
        int weapon=0;
        if (HashGet( m_obungaWeaponData, GetTrigger(), &weapon, FALSE))
        {
            if(weapon)
                CallFunction(weapon);
        }
    // }
}

static void ShotMagicMissile(int cur, int owner)
{
    int ptr;

    if (CurrentHealth(owner))
    {
        ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(cur), GetObjectY(cur));
        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", owner, GetObjectX(owner) + UnitAngleCos(owner, 23.0), GetObjectY(owner) + UnitAngleSin(owner, 23.0));
        Delete(ptr);
        Delete(ptr + 2);
        Delete(ptr + 3);
        Delete(ptr + 4);
    }
    Delete(cur);
}

void ChakramCopyProperies(int src, int dest)
{
    int *srcPtr = UnitToPtr(src), *destPtr = UnitToPtr(dest);
    
    int *srcProperty = srcPtr[173], *destProperty = destPtr[173];

    int rep=-1;

    while (++rep<4)
        destProperty[rep] = srcProperty[rep];

    int rep2=-1;

    while (++rep2<32)
        destPtr[140+rep2] = srcPtr[140+rep2];
}

static void CreateChakram(float xpos, float ypos, int *pDest, int owner)
{
    int mis = CreateObjectAt("RoundChakramInMotion", xpos, ypos);
    int *ptr=UnitToPtr(mis);

    if (pDest)
        pDest[0] = mis;

    ptr[174] = 5158032;
    ptr[186] = 5483536;
    SetOwner(owner, mis);

    int *collideDataMb = MemAlloc(8);
    collideDataMb[0]=0;
    collideDataMb[1] = UnitToPtr(owner);
    ptr[175] = collideDataMb;
}

void OnChakramTracking(int owner, int cur)
{
    int mis;

    CreateChakram(GetObjectX(owner) + UnitAngleCos(owner, 11.0), GetObjectY(owner) + UnitAngleSin(owner, 11.0), &mis, owner);
    ChakramCopyProperies(cur, mis);

    PushObject(mis, 30.0, GetObjectX(owner), GetObjectY(owner));
}

void AfterChakramTracking(int *pMem)
{
    if (!pMem)
        return;

    int inv = pMem[0], owner = pMem[1];

    if (CurrentHealth(owner) && IsObjectOn(inv))
    {
        Pickup(owner, inv);
    }
    FreeSmartMemEx(pMem);
}

static void HookChakrm(int cur, int owner)
{
    // UnitSetEnchantTime(cur, ENCHANT_RUN, 0);
    // Enchant(cur, EnchantList(ENCHANT_ETHEREAL),0.0);

    int *ptr=UnitToPtr(cur);

    if (ptr[186] == 5496000)
    {
        if (!CurrentHealth(owner))
            return;

        int *pMem;

        AllocSmartMemEx(8, &pMem);
        pMem[0] = GetLastItem(cur);
        pMem[1] = owner;
        OnChakramTracking(owner, cur); //이게 여기로 옮겨지고 cur 인자를 더 전달합니다
        Delete(cur);
        FrameTimerWithArg(2, pMem, AfterChakramTracking);
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
	HashPushback(pInstance, 709, ShotMagicMissile);
    HashPushback(pInstance, 1177, HookChakrm);
    HashPushback(pInstance, 2672, createRandomItemTwoArg);
}
#define GROUP_hiddenWalls1 0

void OpenHiddenWalls()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_hiddenWalls1);
    UniPrintToAll("어디에서인가 비밀의 벽이 열렸어요(어디인지는 모름 ㅡ,ㅡ)");
}
