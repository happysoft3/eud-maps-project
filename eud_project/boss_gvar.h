
#define MAX_PLAYER_COUNT 32
#define PLAYER_START_LOCATION_AT 11
#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_HARPOON 32
#define PLAYER_FLAG_THREADLIGHTLY 64
#define PLAYER_FLAG_ALL_BUFF 128
#define PLAYER_FLAG_EYE_OF_WOLF 256

#define GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM 1
#define GUI_DIALOG_MESSAGE_WEAPON_ARROW_AXE 2
#define GUI_DIALOG_MESSAGE_WEAPON_THUNDER_SWORD 3
#define GUI_DIALOG_MESSAGE_SKILL_CRITICAL_HIT 4
#define GUI_DIALOG_MESSAGE_MONEY_EXCHANGER 5
#define GUI_DIALOG_MESSAGE_CREATURE_STELLS 6
#define GUI_DIALOG_MESSAGE_CREATURE_HOSUNGLEE 7
#define GUI_DIALOG_MESSAGE_CREATURE_RUNNER_ONI 8
#define GUI_DIALOG_MESSAGE_WEAPON_FON_AXE 9
#define GUI_DIALOG_MESSAGE_SKILL_AUTODEATHRAY 10
#define GUI_DIALOG_MESSAGE_AWARD_WARCRY 11
#define GUI_DIALOG_MESSAGE_AWARD_BERSERKER 12
#define GUI_DIALOG_MESSAGE_SECRET_GARDEN 13
#define GUI_DIALOG_MESSAGE_AWARD_HARPOON 14
#define GUI_DIALOG_MESSAGE_SPEED_ARMOR 15
#define GUI_DIALOG_MESSAGE_CURE_ARMOR 16
#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10

#define WAR_SKILL_COOLDOWN_AUTO_DEATHRAY 360

void QureyBossClear(int *get,int set){
    int state;
    if (get){
        get[0]=state;
        return;
    }
    state=set;
}
void QueryGardenClear(int *get,int set){
    int state;
    if (get){
        get[0]=state;
        return;
    }
    state=set;
}
void QueryResultGameFn(int *get, int set){
    int fn;
    if (get){
        get[0]=fn;
        return;
    }
    fn=set;
}

#define WALLGROUP_startRoomWalls 5
#define WALLGROUP_frozenGarden2 6
#define WALLGROUP_ogreGardenWalls 7

int GenericHash(){return 0;} //virtual
