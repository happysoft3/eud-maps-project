
#include "libs/spriteDb.h"
#include "libs/clientside.h"
#include "libs/objectIDdefines.h"
#include "libs/grplib.h"
#include "libs/animFrame.h"

void GRPDump_DvaOutput(){}
void GRPDump_WomanOutput(){}
void GRPDumpRoachOniOutput(){}
void ImageYouwin(){ }

void initializeImageFrameRoachOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRoachOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign4Directions(0, 130133, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, 130133+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     130133  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, 130133+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     130133+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     130133   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     130133+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
}

void initializeImageFrameWoman1(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH_WITH_ORB;
    int frameCount=UnpackAllFromGrp(GetScrCodeField(GRPDump_WomanOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);

    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8);

    AnimFrameAssign8Directions(0,128403,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,128403+8,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameAssign8Directions(10,128403+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameAssign8Directions(15,128403+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameAssign8Directions(20,128403+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameAssign8Directions(25,128403+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
    AnimFrameAssign8Directions(30,128403+48,IMAGE_SPRITE_MON_ACTION_WALK,5);
    AnimFrameAssign8Directions(35,128403+56,IMAGE_SPRITE_MON_ACTION_WALK,6);
    AnimFrameAssign8Directions(40,128403+64,IMAGE_SPRITE_MON_ACTION_WALK,7);
}

void initializeImageFrameDva(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WIZARD_GREEN;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_DvaOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,1);
    ChangeMonsterSpriteImageUnkValue(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageUnkValue(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,2);

    AnimFrameAssign8Directions(0, 116177, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, 116177+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, 116177+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, 116177+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, 116177+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, 116177+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(30, 116177+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameAssign8Directions(35, 116177+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameAssign8Directions(40, 116177+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);

    AnimFrameCopy8Directions(116177+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(116177+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(116177+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(116177+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(116177+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions(116177+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions(116177+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions(116177+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);

    AnimFrameAssign8Directions(85,116177+72,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,0);
}

void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
}

void initializeImageData(int imgVector)
{ 
    initializeImageFrameDva(imgVector);
    initializeImageFrameWoman1(imgVector);
    initializeImageFrameRoachOni(imgVector);
    initializeImageFrameSoldier(imgVector);
    AppendImageFrame(imgVector, GetScrCodeField(ImageYouwin)+4, 132317);
    short message1[32];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("오브 반납하는 곳"), message1);
    AppendTextSprite(imgVector, 0x7f3, message1, 112987);

    short message2[32];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("화염기사의 저택"), message2);
    AppendTextSprite(imgVector, 0xf81b, message2, 112992);
    short message3[32];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("-저택 2층-"), message3);
    AppendTextSprite(imgVector, 0xf81b, message3, 112997);
}

void InitializeMapResources()
{
    int imgVector = CreateImageVector(2048);

    initializeImageData(imgVector);
    DoImageDataExchange(imgVector);
}


