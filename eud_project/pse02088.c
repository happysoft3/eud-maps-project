
#include "libs\define.h"
#include "libs\printutil.h"
#include "libs\waypoint.h"
#include "libs\wallutil.h"
#include "libs\coopteam.h"
#include "libs\playerupdate.h"
#include "libs\playerinfo.h"
#include "libs\reaction.h"
#include "libs\shop.h"
#include "libs\sound_define.h"
#include "libs\fxeffect.h"
#include "libs\buff.h"
#include "libs\mathlab.h"
#include "libs\meleeattack.h"
#include "libs\monsteraction.h"
#include "libs\itemproperty.h"
#include "libs\observer.h"
#include "libs\network.h"
#include "libs\wandpatch.h"
#include "libs\imageutil.h"
#include "libs\clientside.h"
#include "libs\bind.h"
#include "libs\hash.h"
#include "libs\absolutelypickup.h"
#include "libs/format.h"

int m_lastItemArr[10];
int *m_pImgVector;

#define PLAYER_DEATH_FLAG 0x80000000

int g_viewpromotion = TRUE;
int g_blueflame;
int g_player[20];
int g_camera[10];
int m_lastUnitId;
int m_promoteStarted = FALSE;

int m_swrdmanEnchant = FALSE;
int *m_strikeFunctionHashInstance;
int *m_changedItemHashInstance;

#define PLAYER_RESPAWN_LOCATION 14

int Part3Gate()
{
    int *pArr;

    if (pArr == NULLPTR)
    {
        int gates[6];

        int rep = -1, uu;
        char buff[32];

        while ((++rep) < 6)
        {
            uu=rep+1;
            NoxSprintfString(buff, "part3gate%d", &uu, 1);
            gates[rep] = Object(ReadStringAddressEx(buff));
        }
        pArr = gates;
    }
    return pArr;
}

void Part3GateUnlock()
{
    int *gates = Part3Gate(), rep = -1;

    while ((++rep) < 6)
        UnlockDoor(gates[rep]);
}

void ThrowBucherDestroyed(int sub)
{
    DrawGeometryRingAt("HealOrb", GetObjectX(sub), GetObjectY(sub), 0);
    SplashDamageAt(GetOwner(sub), 600, GetObjectX(sub), GetObjectY(sub), 200.0);
    PlaySoundAround(sub, SOUND_WallDestroyedMetal);
    Effect("JIGGLE", GetObjectX(sub), GetObjectY(sub), 20.0, 0.0);
}

void ThrowBucherDurate(int sub)
{
    int owner = GetOwner(sub), durate = GetDirection(sub);

    while (IsObjectOn(sub))
    {
        if (CurrentHealth(owner))
        {
            if (durate)
            {
                FrameTimerWithArg(1, sub, ThrowBucherDurate);
                LookWithAngle(sub, --durate);
                Raise(sub + 1, GetObjectZ(sub + 1) - 10.0);
                break;
            }
            else
                ThrowBucherDestroyed(sub);
        }
        Delete(sub++);
        Delete(sub++);
        break;
    }
}

void ThrowBucherFx(int owner)
{
    float xVect = UnitAngleCos(owner, 23.0), yVect = UnitAngleSin(owner, 23.0);
    int sub = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner) + xVect, GetObjectY(owner) + yVect);

    FrameTimerWithArg(1, sub, ThrowBucherDurate);
    LookWithAngle(CreateObjectAt("MovableStatue1b", GetObjectX(sub), GetObjectY(sub)) - 1, 25);
    UnitNoCollide(sub + 1);
    Raise(sub + 1, 250.0);
    SetOwner(owner, sub);
}

void BucherCandleOnUse()
{
    Delete(SELF);
    ThrowBucherFx(OTHER);
}

static void openShopForced()
{
    char openshopcode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 
    0x10, 0xEF, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3, 0x90
};int args[]={UnitToPtr(SELF),UnitToPtr(OTHER)};
int *pExec=0x5c336c;
		int *pOld=pExec[0];
		pExec[0]= openshopcode;
		Unknownb8(args);
		pExec[0]=pOld;
}

static int dispositionShop(string name, float xpos, float ypos)
{
    int lh=CreateObjectAt(name, xpos, ypos);
    int ptr=UnitToPtr(lh);

    SetMemory(ptr+12,GetMemory(ptr+12)^8);
    int p=MemAlloc(0x6c0);
    NoxByteMemset(p, 0x6c0, 0);
    SetMemory(ptr+0x2b4, p);
    SetDialog(lh,"NORMAL",openShopForced,openShopForced);
    return lh;
}

int WindboosterLocalStore()
{
    int arr[32];

    return arr;
}

void GetWindboosterPrivate(int *pDest, int pIndex, int pUnit)
{
    int *pLocal = WindboosterLocalStore();

    if (!MaxHealth(pLocal[pIndex]))
    {
        pLocal[pIndex] = DummyUnitCreate("AirshipCaptain", GetObjectX(pUnit), GetObjectY(pUnit));

        SetOwner(pUnit, pLocal[pIndex]);
    }
    pDest[0] = pLocal[pIndex];
}

void ClearBoosterPrivate()
{
    int *pLocal = WindboosterLocalStore();
    int rep = -1;

    while ((++rep) < 32)
    {
        if (!pLocal[rep])
            continue;

        if (!GetOwner(pLocal))
        {
            Delete(pLocal[rep]);
            pLocal[rep] = 0;
        }
    }
}

void UserWindbooster(int pUnit)
{
    int pIndex, subUnit = 0;

    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    {
        pIndex = GetPlayerIndex(pUnit);
        GetWindboosterPrivate(&subUnit, pIndex, pUnit);
        if (CheckPlayerInput(pUnit) == 2)
        {
            MoveObject(subUnit, GetObjectX(pUnit) - UnitAngleCos(pUnit, 6.0), GetObjectY(pUnit) - UnitAngleSin(pUnit, 6.0));
        }
        else if (subUnit)
        {
            if (DistanceUnitToUnit(pUnit, subUnit) < 100.0)
                MoveObject(subUnit, LocationX(357), LocationY(357));
        }
        
        FrameTimerWithArg(1, pUnit, UserWindbooster);
    }
    else if (MaxHealth(pUnit))
    {
        pIndex = GetPlayerIndex(pUnit);
        int *pLocal = WindboosterLocalStore();

        if (pLocal[pIndex])
        {
            Delete(pLocal[pIndex]);
            pLocal[pIndex] = 0;
        }
    }
    else
    {
        ClearBoosterPrivate();
    }
    
}

void WindRunBootsOnUse()
{
    if (UnitCheckEnchant(OTHER, GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    {
        UniPrint(OTHER, "아직 효력이 남아있어요");
        return;
    }
    FrameTimerWithArg(1, GetCaller(), UserWindbooster);
    Enchant(OTHER, EnchantList(ENCHANT_PROTECT_FROM_MAGIC), 15.0);
    Delete(SELF);
}

void SetUnitCurrentHealth(int target, int amount)
{
    int *ptr = UnitToPtr(target);

    if (!ptr)
        return;

    int *hp = ptr[139];
    hp[0] = amount & 0xfff;
}

void RottenMeatOnUse()
{
    Delete(SELF);

    if (IsPlayerUnit(OTHER))
    {
        if (CurrentHealth(OTHER) < 2000)
        {
            SetUnitCurrentHealth(OTHER, 2000);
            PlaySoundAround(OTHER, SOUND_PixieSwarmCast);
            UniChatMessage(OTHER, "체력 2000으로 점핑업!", 90);
        }
        else
            UniPrint(OTHER, "이미 남아있는 효력이 있습니다");
    }
    else if (IsMonsterUnit(OTHER))
    {
        Damage(OTHER, 0, 200, DAMAGE_TYPE_EXPLOSION);
        DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 12);
        PlaySoundAround(OTHER, SOUND_ManaBombEffect);
        Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 16.0, 0.0);
    }
    
}

void FixColorPotionThingDbSection(int thingId, int amount)
{
    int *target = ThingDbEditGetArray(thingId);
   
    target[7] |= 0x10;
    target[50] = 0x53ef70;
    if (target[51] == 0)
    {
        int *healAmount = MemAlloc(4);
        healAmount[0] = amount;
        target[51] = healAmount;
    }
    target[52] = 4;
}

void ToxicCollide()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 160, DAMAGE_TYPE_POISON);
            GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
        }
        else if (!GetCaller())
        {
            if (WallUtilGetWallAtObjectPosition(SELF))
                WallUtilDestroyWallAtObjectPosition(SELF);
        }
        else
            break;
        DeleteObjectTimer(CreateObjectAt("GreenSmoke", GetObjectX(SELF), GetObjectY(SELF)), 12);
        Delete(SELF);
        break;
    }
}

void ToxicDecay(int toxic)
{
    if (IsObjectOn(toxic))
    {
        DeleteObjectTimer(CreateObjectAt("GreenSmoke", GetObjectX(toxic), GetObjectY(toxic)), 12);
        Delete(toxic);
    }
}

void ToxicSummon(float xpos, float ypos, int owner)
{
    int polyp = CreateObjectAt("Polyp", xpos, ypos);

    SetOwner(owner, polyp);
    SetUnitMass(polyp, 15.0);
    SetUnitCallbackOnCollide(polyp, ToxicCollide);
    SetUnitFlags(polyp, GetUnitFlags(polyp) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    FrameTimerWithArg(60, polyp, ToxicDecay);
}

void ThrowOutToxic(int subsub)
{
    while (IsObjectOn(subsub))
    {
        int durate = GetDirection(subsub);

        if (durate)
        {
            int owner = GetOwner(subsub);

            if (CurrentHealth(owner))
            {
                FrameTimerWithArg(1, subsub, ThrowOutToxic);
                LookWithAngle(subsub, --durate);
                ToxicSummon(GetObjectX(owner) + UnitAngleCos(owner, 21.0), GetObjectY(owner) + UnitAngleSin(owner, 21.0), owner);
                break;
            }
        }
        Delete(subsub);
        break;
    }
}

void HarpoonOnCollide()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 150, DAMAGE_TYPE_FLAME);
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
            Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
        }
        else if (!GetCaller())
        {
            if (WallUtilGetWallAtObjectPosition(SELF))
                WallUtilDestroyWallAtObjectPosition(SELF);
        }
        else
            break;
        Effect("LESSER_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Delete(SELF);
        break;
    }
}

void FxCreateHarpoon(float xpos, float ypos, int *pResult)
{
    int harp = CreateObjectAt("HarpoonBolt", xpos, ypos);
    int *ptr = UnitToPtr(harp);

    ptr[186] = 5483536;
    SetUnitCallbackOnCollide(harp, HarpoonOnCollide);

    pResult[0] = harp;
}

void SpreadNinjaSword(int caster)
{
    int single, rep = -1;
    float xpos = GetObjectX(caster), ypos = GetObjectY(caster);

    while ((++rep) < 45)
    {
        FxCreateHarpoon(xpos + MathSine(rep * 8 + 90, 16.0), ypos + MathSine(rep * 8, 16.0), &single);
        SetOwner(caster, single);
        LookAtObject(single, caster);
        LookWithAngle(single, GetDirection(single) + 128);
        PushObject(single, 33.0, xpos, ypos);
    }
}

void UseNinjaSwordPendant()
{
    Delete(SELF);
    SpreadNinjaSword(OTHER);
}

void GlyphOnCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (CurrentHealth(owner))
        {
            if (IsAttackedBy(OTHER, owner))
            {
                Damage(OTHER, owner, 500, DAMAGE_TYPE_FLAME);
                Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
                Delete(SELF);
            }
        }
    }
}

void GasTrapOnDiscard()
{
    Delete(SELF);

    if (!IsPlayerUnit(OTHER))
        return;

    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + UnitAngleCos(OTHER, 21.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 21.0));

    LookWithAngle(posUnit, 18);
    SetOwner(OTHER, posUnit);
    FrameTimerWithArg(1, posUnit, ThrowOutToxic);
}

void CInstallWhenDiscard()
{
    Delete(SELF);

    if (!IsPlayerUnit(OTHER))
        return;

    int trp = CreateObjectAt("Glyph", GetObjectX(OTHER) + UnitAngleCos(OTHER, 14.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 14.0));

    SetOwner(OTHER, trp);
    SetUnitCallbackOnCollide(trp, GlyphOnCollide);
    PlaySoundAround(OTHER, SOUND_GlyphCast);
}

int StoneGolemBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852798035; arr[1] = 1819232101; arr[2] = 28005; arr[17] = 2000; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; 
		arr[28] = 1125515264; arr[29] = 100; arr[31] = 2; arr[32] = 3; arr[33] = 6; 
		arr[58] = 5545888; arr[59] = 5543904; arr[60] = 1324; arr[61] = 46901248; 
	pArr = arr;
	return pArr;
}

void StoneGolemSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 2000;	hpTable[1] = 2000;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = StoneGolemBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void DelayStoneGuardMode(int *pModeinfo)
{
    int owner = pModeinfo[0];
    int cre = pModeinfo[1];

    if (CurrentHealth(owner))
        CreatureFollow(cre, owner);
    AggressionLevel(cre, 1.0);
    MemFree(pModeinfo);
}

void RespectStoneGiant(int posUnit)
{
    int owner = GetOwner(posUnit);
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    Delete(posUnit);
    if (CurrentHealth(owner))
    {
        int stone = CreateObjectAt("StoneGolem", xpos, ypos);

        StoneGolemSubProcess(stone);
        RegistUnitStrikeHook(stone);
        SetOwner(owner, stone);
        LookWithAngle(stone, GetDirection(owner));
        SetUnitScanRange(stone, 300.0);
        Effect("TELEPORT", GetObjectX(stone), GetObjectY(stone), 0.0, 0.0);

        UniChatMessage(stone, "스톤 자이언트 등장이요!!", 120);

        int szMode[] = {owner, stone};
        int *senddata = MemAlloc(8);
        ArrayCopy(szMode, senddata, sizeof(szMode));
        FrameTimerWithArg(1, senddata, DelayStoneGuardMode);
    }
}

void PlayerUseItemSummonGolem()
{
    int posUnit = CreateObjectAt("RewardMarkerPlus", GetObjectX(OTHER), GetObjectY(OTHER));

    SetOwner(OTHER, posUnit);
    FrameTimerWithArg(14, posUnit, RespectStoneGiant);
    Effect("SMOKE_BLAST", GetObjectX(posUnit), GetObjectY(posUnit), 0.0, 0.0);
    Delete(SELF);
    UniPrint(OTHER, "스톤 자이언트가 당신의 위치에 소환될 것입니다");
}

void SplashOnSightItem1()
{
    int parent = GetOwner(SELF);

    if (!CurrentHealth(GetOwner(parent)))
        return;

    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (GetUnit1C(OTHER) ^ spIdx)
    {
        if (DistanceUnitToUnit(SELF, OTHER) <= GetObjectX(parent))
        {
            Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
            Enchant(OTHER, "ENCHANT_FREEZE", 5.0);
            if (IsPlayerUnit(OTHER))
                Enchant(OTHER, "ENCHANT_CHARMING", 0.08);
            else
                SetUnit1C(OTHER, spIdx);
        }
    }
}

void SozuDuration(int subUnit)
{
    while (IsObjectOn(subUnit))
    {
        int remain = GetDirection(subUnit);

        if (remain)
        {
            LookWithAngle(subUnit, --remain);
            int owner = GetOwner(subUnit);

            if (CurrentHealth(owner))
            {
                FrameTimerWithArg(1, subUnit, SozuDuration);

                if (ToInt(DistanceUnitToUnit(owner, subUnit)))
                    MoveObject(subUnit, GetObjectX(owner), GetObjectY(owner));

                RestoreHealth(owner, 1);
                break;
            }
        }
        Delete(subUnit);
        break;
    }
}

void PlayerUseItem1()
{
    SplashDamageAtPrivate(OTHER, 100, GetObjectX(OTHER), GetObjectY(OTHER), 150.0, SplashOnSightItem1);
    Delete(SELF);
    ManaBombCancelFx(GetObjectX(OTHER), GetObjectY(OTHER));
    UniPrint(OTHER, "아이스 에이지 팬던트 사용: 주변 적에게 데미지 100을 주고 5 초간 얼립니다");
}

void PlayerDrinkSozu()
{
    int durateSub = CreateObjectAt("CorpseLeftLowerArmSE", GetObjectX(OTHER), GetObjectY(OTHER));

    UnitNoCollide(durateSub);
    Frozen(durateSub, TRUE);
    SetOwner(OTHER, durateSub);
    Enchant(durateSub, "ENCHANT_RUN", 0.0);
    LookWithAngle(durateSub, 250);
    FrameTimerWithArg(1, durateSub, SozuDuration);

    Delete(SELF);
    UniPrint(OTHER, "소주를 마셨습니다. 잠시동안 체력이 회복됩니다");
}

void PlayerCastFON()
{
    CastSpellObjectLocation("SPELL_FORCE_OF_NATURE", OTHER, GetObjectX(OTHER) + UnitAngleCos(OTHER, 21.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 21.0));
    Delete(SELF);
}

int RewardGold(float *xypos)
{
    int ggg = CreateObjectAt("Gold", xypos[0], xypos[1]);

    UnitStructSetGoldAmount(ggg, Random(100, 300));
    return ggg;
}

int RewardPotion(float *xypos)
{
    return CreateObjectAt("RedPotion", xypos[0], xypos[1]);
}

int RewardFunctions() //here
{
    int *pFunction, length;

    if (!pFunction)
    {
        int functions[] =
        {
            RewardGold, RewardPotion, 0,0, RewardGold
        };
        length=sizeof(functions);
        pFunction = functions;
    }

    return pFunction[Random(0, length - 1)];
}

void SummonShopkeeperWithAttribute(float xpos, float ypos, int *pDest)
{
    int shop = dispositionShop("Bear2", xpos, ypos); // CreateObjectAt("ShopkeeperMagicShop", xpos, ypos);
    // int *ptr = UnitToPtr(shop);

    UnitNoCollide(shop);
    // ptr[5] = 0x22;  //anim flag hide
    Frozen(shop, TRUE);
    if (pDest)
        pDest[0] = shop;

    ShopUtilAppendItem(shop, 636, 32);
    ShopUtilAppendItem(shop, 1190, 32);
    ShopUtilAppendItem(shop, 1184, 32);
    ShopUtilAppendItem(shop, 622, 32);
    ShopUtilAppendItem(shop, 2178, 32);
    ShopUtilAppendItem(shop, 650, 32);
    ShopUtilAppendItem(shop, 2177, 32);
    ShopUtilAppendItem(shop, 1183, 32);
    ShopUtilAppendItem(shop, 1637, 32);
    ShopUtilAppendItem(shop, 1185, 32);
    ShopUtilAppendItem(shop, 1188, 32);
    ShopUtilAppendItem(shop, 2207, 32);
    ShopUtilAppendItem(shop, 2295, 32);
    ShopUtilAppendItem(shop, 639, 32);
    ShopUtilAppendItem(shop, 1197, 32);
    ShopUtilAppendItem(shop, 646, 32);
    ShopUtilAppendItem(shop, 2184, 32);

    int imgUnit = CreateObjectAt("UrchinShelvesEmpty1", xpos, ypos);

    UnitNoCollide(imgUnit);
    ShopUtilSetTradePrice(shop, 1.0, 0.0);
    Frozen(imgUnit, TRUE);
}

int FloatPtrToInt(float *ptr)
{
    StopScript(ptr);
}

int RewardFuncProto(int fId, float *xy)
{
    return Bind(fId, &fId+4);
}

void SwapMarkToReward(int mark, int *pCount)
{
    float xypos[] = {GetObjectX(mark), GetObjectY(mark)};

    Delete(mark);
    int fpick = RewardFunctions();

    if (!fpick)
        return;

    pCount[0] += (RewardFuncProto(fpick, xypos) != 0);
}

void SwapMarkToShop(int mark, int *pCount)
{
    float xpos = GetObjectX(mark), ypos = GetObjectY(mark);

    Delete(mark);
    SummonShopkeeperWithAttribute(xpos, ypos, NULLPTR);
    ++pCount[0];
}

void OnEndedMapUnitScan(int *pCount)
{
    // UniPrintToAll("debug.c::OnEndedMapUnitScan::result_value::" + IntToString(pCount[0]));
}

#define UNIT_SCAN_SIZE 30
void MapUnitScan(int cur)
{
    int count, thingid;

    int u = 0;

    while (u < UNIT_SCAN_SIZE)
    {
        if (cur + u > m_lastUnitId)
        {
            OnEndedMapUnitScan(&count);
            return;
        }
        thingid = GetUnitThingID(cur + u);

        if (thingid == 2672)
            SwapMarkToReward(cur + u, &count);
        else if (thingid == 2674)
            SwapMarkToShop(cur + u, &count);
        u += 2;
    }
    FrameTimerWithArg(1, cur + u, MapUnitScan);
}

void DelayConnectPlayerEntry(int *pParam)
{
    int plr = pParam[0];
    int pUnit = pParam[1];

    while (TRUE)
    {
        if (!CurrentHealth(pUnit))
            break;
        if (g_player[plr] ^ pUnit)
            break;

        PlayerClassEntryPrivate(plr, pUnit);
        break;
    }
    MemFree(pParam);
}

void PlayerClassRemoveCamera(int plr)
{
    if (g_camera[plr])
    {
        Delete(g_camera[plr]);
        g_camera[plr] = 0;
    }
    if (CurrentHealth(g_player[plr]))
    {
        if (HasEnchant(g_player[plr], "ENCHANT_INVULNERABLE"))
            EnchantOff(g_player[plr], "ENCHANT_INVULNERABLE");

        int szParams[] = {plr, g_player[plr]};
        int *pParam = MemAlloc(sizeof(szParams)*4);

        ArrayCopy(szParams, pParam, sizeof(szParams));
        FrameTimerWithArg(3, pParam, DelayConnectPlayerEntry);
    }
}

int DummyUnitCreate(string name, float locX, float locY)
{
    int unit = CreateObjectAt(name, locX, locY);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    ObjectOff(unit);
    Frozen(unit, TRUE);
    return unit;
}

void Nothing()
{ }

int PlayerClassGetDeathFlag(int plr)
{
    return g_player[plr + 10] & PLAYER_DEATH_FLAG;
}

void PlayerClassSetDeathFlag(int plr)
{
    g_player[plr + 10] ^= PLAYER_DEATH_FLAG;
}

void UnitVisibleSplashA()
{
    int parent = GetOwner(SELF);

    if (!CurrentHealth(GetOwner(parent)))
        return;

    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (GetUnit1C(OTHER) ^ spIdx)
    {
        if (DistanceUnitToUnit(SELF, OTHER) <= GetObjectX(parent))
        {
            Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
            if (IsPlayerUnit(OTHER))
                Enchant(OTHER, "ENCHANT_CHARMING", 0.08);
            else
                SetUnit1C(OTHER, spIdx);
        }
    }
}

void SplashDamageAtPrivate(int owner, int dam, float x, float y, float range, int sightCb)
{
    int splashsub = CreateObjectAt("InvisibleLightBlueHigh", range, y), splashIdx;

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(splashsub), GetObjectY(splashsub)), ++splashIdx);
    SetOwner(owner, splashsub);
    Raise(splashsub, ToFloat(dam));

    int splashUnits[4];
    int rep = -1;

    while ((++rep) < sizeof(splashUnits))
    {
        splashUnits[rep] = CreateObjectAt("WeirdlingBeast", x, y);
        DeleteObjectTimer(splashUnits[rep], 1);
        UnitNoCollide(splashUnits[rep]);
        LookWithAngle(splashUnits[rep], rep * 64);
        SetOwner(splashsub, splashUnits[rep]);
        SetCallback(splashUnits[rep], 3, sightCb);
    }
    DeleteObjectTimer(splashsub++, 2);
    DeleteObjectTimer(splashsub++, 2);
}

void SplashDamageAt(int owner, int damge, float xpos, float ypos, float range)
{
    SplashDamageAtPrivate(owner, damge, xpos, ypos, range, UnitVisibleSplashA);
}

void ChangePlayerRespawnLocation(float xpos, float ypos)
{
    TeleportLocation(PLAYER_RESPAWN_LOCATION, xpos, ypos);
}

void OnHearEnemy()
{
    if (MonsterGetCurrentAction(SELF) != 0)
        return;

    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);

        Attack(SELF, OTHER);
    }
}

int SwordsmanBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1919907667; arr[1] = 1634562916; arr[2] = 110; arr[17] = 150; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1108082688; arr[29] = 1; arr[57] = 5547984; arr[59] = 5542784; arr[60] = 1346; 
		arr[61] = 46900480; 
		link = arr;
	}
	return link;
}

void SwordsmanSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 150);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 150);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, SwordsmanBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CreateEnemyMeleeSoldier(float xpos, float ypos)
{
    int soldier = CreateObjectAt("Swordsman", xpos, ypos);

    SwordsmanSubProcess(soldier);
    RegistUnitStrikeHook(soldier);
    return soldier;
}

int CreateEnemyMeleeSoldierAtLocation(int locationId)
{
    int unit = CreateEnemyMeleeSoldier(LocationX(locationId), LocationY(locationId));

    SetUnitScanRange(unit, 300.0);
    if (m_swrdmanEnchant)
        Enchant(unit, "ENCHANT_HASTED", 0.0);
    return unit;
}

int FlyingGolemBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1769565254; arr[1] = 1866950510; arr[2] = 7169388; arr[17] = 300; arr[19] = 65; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; 
		arr[28] = 1133903872; arr[54] = 4; arr[59] = 5542784; arr[60] = 1316; arr[61] = 46903808; 
		link = arr;
	}
	return link;
}

void FlyingGolemSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1073322393);
		SetMemory(ptr + 0x224, 1073322393);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32769);
		SetMemory(GetMemory(ptr + 0x22c), 300);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FlyingGolemBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CreateEnemyMecaDroneAtLocation(int locationId)
{
    int drone = CreateObjectAt("FlyingGolem", LocationX(locationId), LocationY(locationId));

    FlyingGolemSubProcess(drone);
    RegistUnitStrikeHook(drone);
    SetUnitScanRange(drone, 300.0);
    return drone;
}

int HorrendousBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 400; arr[19] = 95; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 9; 
		arr[27] = 5; arr[28] = 1125515264; arr[29] = 100; arr[54] = 4; arr[59] = 5542784; 
		arr[60] = 1386; arr[61] = 46907648; 
	pArr = arr;
	return pArr;
}

void HorrendousSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077306982;		ptr[137] = 1077306982;
	int *hpTable = ptr[139];
	hpTable[0] = 400;	hpTable[1] = 400;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = HorrendousBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateEnemyHorrendousAtLocation(int locationId)
{
    int horr = CreateObjectAt("Horrendous", LocationX(locationId), LocationY(locationId));

    HorrendousSubProcess(horr);
    RegistUnitStrikeHook(horr);
    SetUnitMaxHealth(horr, 600);
    return horr;
}

int WizardWhiteBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1750557810; arr[2] = 6648937; arr[17] = 200; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1067869798; arr[26] = 4; arr[28] = 1133903872; 
		arr[32] = 14; arr[33] = 20; arr[53] = 1128792064; arr[54] = 4; arr[59] = 5542784; 
		arr[60] = 1332; arr[61] = 46909952; 
		link = arr;
	}
	return link;
}

void WizardWhiteSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1072064102);
		SetMemory(ptr + 0x224, 1072064102);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 300);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardWhiteBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CreateEnemyLaserGunnerAtLocation(int locationId)
{
    int unit = CreateObjectAt("WizardWhite", LocationX(locationId), LocationY(locationId));

    WizardWhiteSubProcess(unit);
    RegistUnitStrikeHook(unit);
    SetUnitScanRange(unit, 300.0);
    return unit;
}

int TrollBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1819243092; arr[1] = 108; arr[17] = 450; arr[19] = 70; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 7; arr[27] = 4; 
		arr[28] = 1133904200; arr[29] = 1; arr[31] = 14; arr[32] = 25; arr[33] = 35;
		arr[59] = 5542784; arr[60] = 2273; arr[61] = 46912256; 
	pArr = arr;
	return pArr;
}

void TrollSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 450;	hpTable[1] = 450;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = TrollBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateEnemyForestGunnerAtLocation(int locationId)
{
    int forest = CreateObjectAt("Troll", LocationX(locationId), LocationY(locationId));

    TrollSubProcess(forest);
    RegistUnitStrikeHook(forest);
    SetUnitScanRange(forest, 300.0);

    return forest;
}

int WoundedWarriorBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1466197348; arr[2] = 1769108065; arr[3] = 29295; arr[17] = 100; 
		arr[26] = 4; arr[27] = 5; arr[60] = 2272; arr[61] = 46912512; 
		link = arr;
	}
	return link;
}

void WoundedWarriorSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 0);
		SetMemory(ptr + 0x224, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 100);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 100);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedWarriorBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 0);
	}
}

int CreateEnemySleepy(float xpos, float ypos, int wakeAction)
{
    int slp = CreateObjectAt("WoundedWarrior", xpos, ypos);

    WoundedWarriorSubProcess(slp);
    if (wakeAction != 0)
    {
        SetCallback(slp, 5, wakeAction);
        SetCallback(slp, 7, wakeAction);
    }
    AggressionLevel(slp, 0.0);
    return slp;
}

int ArcherBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1751347777; arr[1] = 29285; arr[17] = 200; arr[19] = 70; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[37] = 1751347777; arr[38] = 1916891749; 
		arr[39] = 7827314; arr[53] = 1128792064; arr[54] = 4; arr[57] = 5548176; arr[60] = 1345; 
		arr[61] = 46904320; 
		link = arr;
	}
	return link;
}

void ArcherSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074161254);
		SetMemory(ptr + 0x224, 1074161254);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 200);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 200);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, ArcherBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CreateEnemyGunner(float xpos, float ypos)
{
    int gunner = CreateObjectAt("Archer", xpos, ypos);

    ArcherSubProcess(gunner);
    SetUnitScanRange(gunner, 300.0);
    return gunner;
}

int CreateEnemyGunnerAtLocation(int locationId)
{
    return CreateEnemyGunner(LocationX(locationId), LocationY(locationId));
}

int ShadeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684105299; arr[1] = 101; arr[17] = 100; arr[19] = 120; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; 
		arr[29] = 100; arr[31] = 4; arr[32] = 10; arr[33] = 20; arr[59] = 5542784; 
		arr[60] = 1362; arr[61] = 46905088; 
	pArr = arr;
	return pArr;
}

void ShadeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 100;	hpTable[1] = 100;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = ShadeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateEnemyShadowMan(int locationId)
{
    int shd = CreateObjectAt("Shade", LocationX(locationId), LocationY(locationId));

    ShadeSubProcess(shd);
    SetUnitScanRange(shd, 300.0);
    return shd;
}

int AirshipCaptainBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 600; 
		arr[19] = 75; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; 
		arr[27] = 5; arr[37] = 1701996367; arr[38] = 1920297043; arr[39] = 1852140393; arr[53] = 1133903872; 
		arr[55] = 35; arr[56] = 50; arr[58] = 5546320; arr[60] = 1387; arr[61] = 46915328; 
		link = arr;
	}
	return link;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074790400);
		SetMemory(ptr + 0x224, 1074790400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 300);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CreateEnemySMaster(float xpos, float ypos)
{
    int master = CreateObjectAt("AirshipCaptain", xpos, ypos);

    AirshipCaptainSubProcess(master);
    SetUnitScanRange(master, 300.0);
    return master;
}

int CreateEnemyWithFunction(int createfunction, int locationId)
{
    if (!createfunction)
        createfunction = CreateEnemyMeleeSoldierAtLocation;
    
    int enemy = CallFunctionWithArgInt(createfunction, locationId);

    SetCallback(enemy, 10, OnHearEnemy);
    return enemy;
}

int Bear2BinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50; arr[17] = 200; arr[19] = 75; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1067450368; arr[27] = 1; arr[28] = 1133904200; arr[31] = 2; 
		arr[32] = 20; arr[33] = 30; arr[58] = 5547856; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

void Bear2SubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074790400);
		SetMemory(ptr + 0x224, 1074790400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 200);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 200);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, Bear2BinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CreateSingleColorMaidenAt(int red, int grn, int blue, float xpos, float ypos)
{
    int unit = CreateObjectAt("Bear2", xpos, ypos);
    int *ptr = UnitToPtr(unit);

    if (ptr == NULLPTR)
        return NULLPTR;
    ptr[1] = 1385;

    int u = 0;
    while (u < 32)
        ptr[140 + (u++)] = 0x400;

    int *ecptr = ptr[187];

    ecptr[94] = 0xa0;
    ecptr[122] = VoiceList(7);

    int *colrarr = ecptr + 0x81c;

    colrarr[0] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[1] = grn | (blue << 8) | (red << 16) | (grn << 24);
    colrarr[2] = blue | (red << 8) | (grn << 16) | (blue << 24);
    colrarr[3] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[4] = grn | (blue << 8);

    return unit;
}

int CreateEnemyMedicAtLocation(int locationId)
{
    int medic = CreateSingleColorMaidenAt(0, 255, 0, LocationX(locationId), LocationY(locationId));

    Bear2SubProcess(medic);
    SetUnitScanRange(medic, 300.0);
    RegistUnitStrikeHook(medic);
    return medic;
}

void TeleportWhenClick()
{
    int destination = GetTrigger() + 1;

    MoveObject(OTHER, GetObjectX(destination), GetObjectY(destination));
    Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
}

void PlaceTeleportWhenClick(int putLocation, int destLocation, int *pResult)
{
    int dam = DummyUnitCreate("Bomber", LocationX(putLocation), LocationY(putLocation));

    CreateObjectAtEx("PlayerWaypoint", LocationX(destLocation), LocationY(destLocation), NULLPTR);
    SetDialog(dam, "NORMAL", TeleportWhenClick, NULLPTR);
    if (pResult != NULLPTR)
        pResult[0] = dam;
}

int AreaRoom11Sleepy()
{
    int mob[5];

    return mob;
}

void SoliderTalkingEntryStartroom()
{
    string says[] = {
        "뭔일인데?? 야 너 뭐야!!",
        "무슨소리지? 저새끼!! 총들었다!!",
        "이게무슨...!!",
        "당장 지휘통제실에 보고하자"
    };

    UniChatMessage(SELF, says[Random(0, sizeof(says)-1)], 120);
}

void SoldierTalkingWhenSee()
{
    string takrev[] = {
        "이 새끼가 미쳤나?",
        "그 총 뭐야? 내려놔 당장! 미쳤냐",
        "쏘지마 제발!! 살려줘...",
        "모두 일어나봐! 임병장 빡쳤닼ㅋ ㅋㅋ"
    };

    UniChatMessage(SELF, takrev[Random(0, sizeof(takrev)-1)], 120);
}

void SoldierComeToStartZone(int *enmy)
{
    int i;

    while (i < 3)
    {
        Move(enmy[i ++], 29);
    }
}

void ComeonEnemiesToStartroom()
{
    int enemy[3] = {
        CreateEnemyMeleeSoldier(LocationX(30), LocationY(30)),
        CreateEnemyMeleeSoldier(LocationX(31), LocationY(31)),
        CreateEnemyMeleeSoldier(LocationX(32), LocationY(32))
    }, i;

    for (i = 0 ; i < 3 ; ++i)
    {
        SetCallback(enemy[i], 3, SoliderTalkingEntryStartroom);
    }
    FrameTimerWithArg(1, enemy, SoldierComeToStartZone);
}

void StartroomUnLocking()
{
    int killcount;

    if ((++killcount) == 4)
    {
        UniChatMessage(SELF, "살려줘!!! 저 임병장 미친새끼가 총들고 난사한다고!! 으얽.. 헉;..", 120);
        UnlockDoor(Object("firstg1"));
        UnlockDoor(Object("firstg2"));
        UniPrintToAll("아직 분이 풀리지 않았다!! 이제 시작이다!! 모두 다 쓸어버릴거야");
        FrameTimer(1, ComeonEnemiesToStartroom);
    }
}

void WakeupSleepy(int *mop)
{
    int u;

    for (u = 0 ; u < 5 ; Nop(++u))
    {
        if (!CurrentHealth(mop[u]))
            continue;
        
        int sleepy = mop[u];

        UnitNoCollide(sleepy);
        mop[u] = CreateEnemyMeleeSoldier(GetObjectX(sleepy), GetObjectY(sleepy));

        SetCallback(mop[u], 3, SoldierTalkingWhenSee);
        SetCallback(mop[u], 5, StartroomUnLocking);
        Effect("SMOKE_BLAST", GetObjectX(sleepy), GetObjectY(sleepy), 0.0, 0.0);
        Delete(sleepy);
    }
}

void WakeSleepyAll()
{
    if (CurrentHealth(SELF))
    {
        PlaySoundAround(SELF, SOUND_HorrendousHurt);
        UniChatMessage(SELF, "뭐야? 갑자기 왜때려? 이 세끼가 미쳤나", 90);
    }
    int *mop = AreaRoom11Sleepy();

    FrameTimerWithArg(1, mop, WakeupSleepy);
}

void PlaceArea1Room1Enemies()
{
    int *mop = AreaRoom11Sleepy();

    if (mop == NULLPTR)
        return;
    int u;
    for (u = 0 ; u < 5 ; Nop(++u))
        mop[u] = CreateEnemySleepy(LocationX(24 + u), LocationY(24 + u), WakeSleepyAll);
}

int PlaceRoomSolider(int locationId, int targetunit)
{
    int mob = CreateEnemyMeleeSoldier(LocationX(locationId), LocationY(locationId));

    SetUnitScanRange(mob, 300.0);
    if (CurrentHealth(targetunit))
    {
        LookAtObject(mob, targetunit);
    }
    return mob;
}

void StartThisArea(int *condvar, int execFunctionId)
{
    ObjectOff(SELF);

    if (!condvar[0])
    {
        condvar[0] = TRUE;
        FrameTimerWithArg(1, GetCaller(), execFunctionId);
    }
}

void PlaceArea1Room2Enemies(int targetunit)
{
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 33);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 34);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 35);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 36);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 37);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 38);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 39);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 40);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 41);
}

void OnPlaceArea1R2()
{
    int once;

    StartThisArea(&once, PlaceArea1Room2Enemies);
}

void Area1GunnerSayOnVision()
{
    UniChatMessage(SELF, "당직부사관: 임병장 너 이세끼...!!\n총 안내려놔!!", 120);
    SetCallback(SELF, 3, Nothing);
}

void Area1GunnerWalkTo(int gunner)
{
    if (CurrentHealth(gunner))
    {
        Move(gunner, 88);
        AggressionLevel(gunner, 1.0);
    }
}

void PlaceArea1GunnerEvent()
{
    int gunner = CreateEnemyGunner(LocationX(87), LocationY(87));

    SetCallback(gunner, 3, Area1GunnerSayOnVision);
    UniChatMessage(gunner, "당직부사관: 무슨일인데 이렇게 시끄러워?!!", 120);
    FrameTimerWithArg(1, gunner, Area1GunnerWalkTo);

    int u;

    for (u = 0 ; u < 6 ; Nop(++u))
        SetUnitScanRange(CreateEnemyMeleeSoldier(LocationX(93 + u), LocationY(93 + u)), 300.0);
}

void OnPlaceArea1E1()
{
    int once;

    StartThisArea(&once, PlaceArea1GunnerEvent);
}

void PlaceArea1Room3Enemies(int targetunit)
{
    int u;

    for (u = 0 ; u < 10 ; Nop(++u))
    {
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 54 + u), targetunit);
    }
}

void OnPlaceArea1R3()
{
    int once;

    StartThisArea(&once, PlaceArea1Room3Enemies);
}

void PlaceArea1Room4Enemies(int targetunit)
{
    int u;

    for (u = 0 ; u < 12 ; Nop(++u))
    {
        Enchant(PlaceRoomSolider(u + 75, targetunit), "ENCHANT_HASTED", 0.0);
    }
}

void OnPlaceArea1R4()
{
    int once;

    StartThisArea(&once, PlaceArea1Room4Enemies);
}

void PlaceArea1Room5Enemies(int targetunit)
{
    int u;

    for (u = 0 ; u < 11 ; Nop(++u))
        Enchant(PlaceRoomSolider(u + 64, targetunit), "ENCHANT_HASTED", 0.0);
}

void OnPlaceArea1R5()
{
    int once;

    StartThisArea(&once, PlaceArea1Room5Enemies);
}

void Area1PassageOpen()
{
    int u;

    for (u = 0 ; u < 3 ; Nop(++u))
    {
        WallUtilOpenWallAtObjectPosition(99);
        WallUtilOpenWallAtObjectPosition(100);
        TeleportLocationVector(99, 23.0, 23.0);
        TeleportLocationVector(100, 23.0, 23.0);
    }

    UniPrintToAll("위이잉~ 진돗개 발령!! 현재 대대 내에서 병사 1명이 총격전을 벌이고 있음! 전원 총기 휴대 및 실탄 장착바람");
    UniPrintToAll("적이 보이는 즉시 사살할 것!!");
}

void Area1MasterSay()
{
    SetCallback(SELF, 3, Nothing);
    UniChatMessage(SELF, "당직사관: 임병장 너가 드디어 미쳤구나.\n얘들아 대대에 보고 할테니 선 조치해라!\n저 놈을 죽여!", 150);
}

void Area1MasterDeath()
{
    UniChatMessage(SELF, "당직사관: 대대를 위하여...! 이 한몸 바치는 것 쯤은.....", 150);

    FrameTimer(30, Area1PassageOpen);
}

void PlaceArea1Room6Enemies(int targetunit)
{
    LookAtObject(CreateEnemyGunner(LocationX(90), LocationY(90)), targetunit);
    LookAtObject(CreateEnemyGunner(LocationX(91), LocationY(91)), targetunit);
    LookAtObject(CreateEnemyGunner(LocationX(92), LocationY(92)), targetunit);

    int master = CreateEnemySMaster(LocationX(89), LocationY(89));

    SetCallback(master, 3, Area1MasterSay);
    SetCallback(master, 5, Area1MasterDeath);
    SetUnitMaxHealth(master, 600);
}

void OnPlaceArea1R6()
{
    int once;

    StartThisArea(&once, PlaceArea1Room6Enemies);
}

void PlaceArea1Room7Enemies(int targetunit)
{
    int u;

    for (u = 0 ; u < 5 ; Nop(++u))
    {
        Enchant(PlaceRoomSolider(101, targetunit), "ENCHANT_HASTED", 0.0);
        Enchant(PlaceRoomSolider(102, targetunit), "ENCHANT_HASTED", 0.0);
        TeleportLocationVector(101, -46.0, 46.0);
        TeleportLocationVector(102, -46.0, 46.0);
    }
}

void OnPlaceArea1R7()
{
    int once;

    StartThisArea(&once, PlaceArea1Room7Enemies);
}

void PlaceArea1LeftStreet(int targetunit)
{
    int u;

    for (u = 0 ; u < 12 ; Nop(++u))
    {
        Enchant(PlaceRoomSolider(u + 42, targetunit), "ENCHANT_HASTED", 0.0);
    }
}

void OnPlaceArea1LSt()
{
    int once;

    StartThisArea(&once, PlaceArea1LeftStreet);
}

void PlaceArea2LeftStreet()
{
    int lookcenter = CreateObjectAt("InvisibleLightBlueHigh", LocationX(109), LocationY(109));

    LookAtObject(CreateEnemyMeleeSoldier(LocationX(103), LocationY(103)), lookcenter);
    LookAtObject(CreateEnemyMeleeSoldier(LocationX(104), LocationY(104)), lookcenter);
    LookAtObject(CreateEnemyMeleeSoldier(LocationX(107), LocationY(107)), lookcenter);
    LookAtObject(CreateEnemyMeleeSoldier(LocationX(108), LocationY(108)), lookcenter);

    CreateEnemyGunner(LocationX(105), LocationY(105));
    CreateEnemyGunner(LocationX(106), LocationY(106));

    LookAtObject(CreateEnemyGunner(LocationX(122), LocationY(122)), lookcenter);
    LookAtObject(CreateEnemyGunner(LocationX(123), LocationY(123)), lookcenter);
    CreateEnemyMeleeSoldier(LocationX(124), LocationY(124));
    CreateEnemyMeleeSoldier(LocationX(125), LocationY(125));

    CreateEnemyGunner(LocationX(144), LocationY(144));
    CreateEnemyGunner(LocationX(145), LocationY(145));
    CreateEnemyGunner(LocationX(146), LocationY(146));
    CreateEnemyGunner(LocationX(147), LocationY(147));

    ChangePlayerRespawnLocation(LocationX(335), LocationY(335));
}

void Area2OnEntry()
{
    int once;

    StartThisArea(&once, PlaceArea2LeftStreet);
}

void PlaceArea2RightStreet()
{
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 141);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 142);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 143);

    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 135);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 136);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 137);

    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 138);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 139);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 140);

    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 131);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 132);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 133);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 134);

    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 126);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 127);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 128);
    CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 129);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 130);
}

void AreaR2OnEntry()
{
    int once;

    StartThisArea(&once, PlaceArea2RightStreet);
}

void PlaceArea2Room1Enemies(int targetunit)
{
    int u;

    for (u = 0 ; u < 5 ; Nop(++u))
    {
        Enchant(PlaceRoomSolider(110 + u, targetunit), "ENCHANT_HASTED", 0.0);
        LookAtObject(CreateEnemyGunner(LocationX(115 + u), LocationY(115 + u)), targetunit);
    }
}

void OnPlaceArea2R1()
{
    int once;

    StartThisArea(&once, PlaceArea2Room1Enemies);
}

void PlaceArea2Room2Enemies(int targetunit)
{
    LookAtObject(CreateEnemySMaster(LocationX(120), LocationY(120)), targetunit);
    LookAtObject(CreateEnemySMaster(LocationX(121), LocationY(121)), targetunit);
}

void OnPlaceArea2R2()
{
    int once;

    StartThisArea(&once, PlaceArea2Room2Enemies);
}

void PlaceArea2Room3Enemies(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 151), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 152), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 153), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 154), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMedicAtLocation, 148), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMedicAtLocation, 149), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMedicAtLocation, 150), targetunit);

    CreateObject("GoldKey", 165);
    CreateObject("RedPotion", 177);
    CreateObject("RedPotion", 178);
    CreateObject("RedPotion", 179);
}

void OnPlaceArea2R3()
{
    int once;

    StartThisArea(&once, PlaceArea2Room3Enemies);
}

void DropArea2Room4Key()
{
    CreateObjectAt("SilverKey", GetObjectX(SELF), GetObjectY(SELF));
    PlaySoundAround(SELF, SOUND_KeyDrop);
}

void PlaceArea2Room4Enemies(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 155), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 156), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 157), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 158), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 159), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 160), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 161), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 162), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 163), targetunit);

    int keyowner = CreateEnemySMaster(LocationX(121), LocationY(121));

    LookAtObject(keyowner, targetunit);
    SetCallback(keyowner, 5, DropArea2Room4Key);
}

void OnPlaceArea2R4()
{
    int once;

    StartThisArea(&once, PlaceArea2Room4Enemies);
}

void PlaceArea2Room5Enemies(int targetunit)
{
    int u;

    for (u = 0 ; u < 9 ; Nop(++u))
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 166 + u), targetunit);
}

void OnPlaceArea2R5()
{
    int once;
    StartThisArea(&once, PlaceArea2Room5Enemies);
}

void PlaceArea2Guard()
{
    int mobtypes[] = {CreateEnemyGunnerAtLocation, CreateEnemyMeleeSoldierAtLocation, CreateEnemyMedicAtLocation};
    int count;

    if ((count++) < 12)
    {
        FrameTimer(1, PlaceArea2Guard);
        int mob = CreateEnemyWithFunction(mobtypes[Random(0, 2)], 175);

        SetUnitScanRange(mob, 300.0);
    }
}

void PlaceArea2OpenwallPlaceGuard()
{
    FrameTimer(15, Area2OpenWalls);
    PlaceArea2Guard();
}

void Area2OpenWalls()
{
    int u;

    while ((u++) < 6)
    {
        WallUtilOpenWallAtObjectPosition(176);
        TeleportLocationVector(176, 23.0, 23.0);
    }
}

void OnPlaceArea2Gf()
{
    int once;

    StartThisArea(&once, PlaceArea2OpenwallPlaceGuard);
}

void Area2BossSay()
{
    string chats[] = {
        "놈이 저기에 있다! 그를 죽여도 무죄다",
        "어서 그를 잡아 죽여!",
        "뭣들하고 있어 서둘러!",
        "저놈은 총을 들고있어!"
    };
    char buff[128], *p=StringUtilGetScriptStringPtr(chats[Random(0, 3)]);

    NoxSprintfString(buff, "행정보급관: %s", &p, 1);
    UniChatMessage(SELF, ReadStringAddressEx(buff), 120);
}

void Area2BossDeath()
{
    UniChatMessage(SELF, "끄아ㅏ..", 120);
    UnlockDoor(Object("mercg1"));
    UnlockDoor(Object("mercg2"));
}

void RemoveWhenDrop()
{
    Delete(SELF);
}

void Area2BossItemProcess(int boss)
{
    int inv = GetLastItem(boss);

    while (inv)
    {
        SetUnitCallbackOnDiscard(inv, RemoveWhenDrop);
        Frozen(inv, TRUE);
        inv = GetPreviousItem(inv);
    }
}

void PlaceArea2R6(int targetunit)
{
    int a2boss = CreateEnemyLaserGunnerAtLocation(180);

    SetUnitMaxHealth(a2boss, 600);
    LookAtObject(a2boss, targetunit);
    SetCallback(a2boss, 3, Area2BossSay);
    SetCallback(a2boss, 5, Area2BossDeath);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 181), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 182), targetunit);
}

void OnPlaceArea2R6()
{
    int once;
    StartThisArea(&once, PlaceArea2R6);
}

void PlaceArea2R7(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 337), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 338), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 339), targetunit);
}

void OnPlaceArea2R7()
{
    int once;
    StartThisArea(&once, PlaceArea2R7);
}

void Area2NextLever()
{
    ObjectOff(SELF);

    UnlockDoor(Object("area3e1"));
    UnlockDoor(Object("area3e2"));
    UniPrintToAll("1층 문의 잠금이 해제되었습니다");
}

void PlaceArea3R1(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 184), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 185), targetunit);

    ChangePlayerRespawnLocation(GetObjectX(targetunit), GetObjectY(targetunit));
}

void OnPlaceArea3R1()
{
    int once;
    StartThisArea(&once, PlaceArea3R1);
}

void PlaceArea3R2(int targetunit)
{
    m_swrdmanEnchant = TRUE;
    int enemy[3] = {
        CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 186),
        CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 187),
        CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 188)
    };

    UniChatMessage(enemy[0], "손들어! 움직이면 쏜다!!", 120);
    int u;
    for (u = 0 ; u < 3 ; Nop(++u))
    {
        LookAtObject(enemy[u], targetunit);
    }

    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 209), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 210), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 211), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 212), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 213), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 214), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 217), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 218), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 219), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 220), targetunit);
}

void OnPlaceArea3R2()
{
    int once;
    StartThisArea(&once, PlaceArea3R2);
}

void PlaceArea3R3(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 192), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 193), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 196), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMedicAtLocation, 191), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMedicAtLocation, 195), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 194), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 189), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 190), targetunit);
}

void OnPlaceArea3R3()
{
    int once;
    StartThisArea(&once, PlaceArea3R3);
}

void PlaceArea3R4(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 201), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 203), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 205), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 207), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 202), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 204), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 206), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 208), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 197), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 198), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 199), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 200), targetunit);
}

void OnPlaceArea3R4()
{
    int once;

    StartThisArea(&once, PlaceArea3R4);
}

void Area3MecaDroneWallclear()
{
    int wallx1 = 124, wally1 = 66;

    while (wallx1 <= 130)
        WallOpen(Wall(wallx1++, wally1++));

    int wallx2 = 115, wally2 = 75;

    while (wallx2 <= 121)
        WallOpen(Wall(wallx2++, wally2 ++));
}

void PlaceArea3R5(int targetunit)
{
    FrameTimer(1, Area3MecaDroneWallclear);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 215), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 216), targetunit);
}

void OnPlaceArea3R5()
{
    int once;

    StartThisArea(&once, PlaceArea3R5);
}

void PlaceArea3R6(int targetunit)
{
    int rep = 221;

    while (rep <= 225)
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, rep++), targetunit);
}

void OnPlaceArea3R6()
{
    int once;

    StartThisArea(&once, PlaceArea3R6);
}

void PlaceArea3R7(int targetunit)
{
    int rep = 226;

    while (rep <= 230)
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, rep++), targetunit);
}

void OnPlaceArea3R7()
{
    int once;

    StartThisArea(&once, PlaceArea3R7);
}

void Area3BossOnDeath()
{
    DeleteObjectTimer(SELF, 90);
    PlaceTeleportWhenClick(236, 238, NULLPTR);
    PlaceTeleportWhenClick(237, 239, NULLPTR);
    UniChatMessage(SELF, "슈우발...!!", 90);

    UniPrintToAll("대대 출입문 통제!! 힌트. 막사 2층 창문으로 탈출하세요");
}

void PlaceArea3R8(int targetunit)
{
    int rep = 232;

    while (rep <= 235)
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, rep++), targetunit);

    int bossunit = CreateEnemyWithFunction(CreateEnemyHorrendousAtLocation, 231);

    LookAtObject(bossunit, targetunit);
    SetCallback(bossunit, 5, Area3BossOnDeath);
    UniChatMessage(bossunit, "대대장: 저놈을 죽여라!", 150);
}

void OnPlaceArea3R8()
{
    int once;

    StartThisArea(&once, PlaceArea3R8);
}

void PlaceArea3R9(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 326), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 327), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 328), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 329), targetunit);
    WallOpen(Wall(114, 102));
    WallOpen(Wall(115, 103));
}

void OnPlaceArea3R9()
{
    int once;
    StartThisArea(&once, PlaceArea3R9);
}

void PlaceArea3RA(int targetunit)
{
    int base = 330;

    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, base++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, base++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, base++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, base++), targetunit);

    FrameTimer(15, PlaceRowLaserRifleGun);
}

void OnPlaceArea3RA()
{
    int once;
    StartThisArea(&once, PlaceArea3RA);
}

int Area4R1MobFunctions(int order)
{
    int functions[] = {CreateEnemyGunnerAtLocation, CreateEnemyMeleeSoldierAtLocation};

    return functions[order & 1];
}

void PlaceArea4R1(int targetunit)
{
    int cunit, rep = -1;

    while ((++rep) < 8)
    {
        cunit = CreateEnemyWithFunction(Area4R1MobFunctions(rep), 240 + rep);

        LookAtObject(cunit, targetunit);
        UniChatMessage(cunit, "꼼짝마라! 넌 포위되었다", 120);
    }
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 248), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 249), targetunit);

    WallUtilOpenWallAtObjectPosition(236);
    WallUtilOpenWallAtObjectPosition(237);

    ChangePlayerRespawnLocation(LocationX(334), LocationY(334));
}

void OnPlaceArea4R1()
{
    int once;

    StartThisArea(&once, PlaceArea4R1);
}

void A4R2Walls()
{
    int wallx1 = 129, wallx2 = 130;
    int wally1 = 161, wally2 = 162;
    int rep = -1;

    while ((++rep) < 5)
    {
        WallOpen(Wall(wallx1, wally1));
        WallOpen(Wall(wallx2, wally2));
        wally1 += 2;
        wally2 += 2;
    }
    WallUtilOpenWallAtObjectPosition(265);
    WallUtilOpenWallAtObjectPosition(266);
    WallUtilOpenWallAtObjectPosition(267);
    WallUtilOpenWallAtObjectPosition(268);
}

void PlaceArea4R2(int targetunit)
{
    A4R2Walls();

    int rep = -1;

    while ((++rep) < 8)
    {
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 262), targetunit);
        TeleportLocationVector(262, 0.0, 23.0);
    }

    LookAtObject(CreateEnemyMeleeSoldierAtLocation(263), targetunit);
    LookAtObject(CreateEnemyMeleeSoldierAtLocation(264), targetunit);

    Effect("JIGGLE", GetObjectX(targetunit), GetObjectY(targetunit), 0.0, 0.0);
}

void OnPlaceArea4R2()
{
    int once;

    StartThisArea(&once, PlaceArea4R2);
}

void PlaceArea4R3(int targetunit)
{
    int rep = 10;

    while ((rep--))
    {
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 269), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 270), targetunit);
        TeleportLocationVector(269, 23.0, 23.0);
        TeleportLocationVector(270, 23.0, 23.0);
    }
    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 271), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 272), targetunit);
}

void OnPlaceArea4R3()
{
    int once;

    StartThisArea(&once, PlaceArea4R3);
}

void PlaceArea5R1(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 273), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 274), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 275), targetunit);
    ChangePlayerRespawnLocation(LocationX(336), LocationY(336));
}

void OnPlaceArea5R1()
{
    int once;
    StartThisArea(&once, PlaceArea5R1);
}

void PlaceArea5R2(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 276), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 277), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 278), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 279), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 280), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 281), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 282), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 283), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 284), targetunit);
}

void OnPlaceArea5R2()
{
    int once;
    StartThisArea(&once, PlaceArea5R2);
}

void PlaceArea5R3(int targetunit)
{
    int count = 14;

    while (count--)
    {
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 285), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 286), targetunit);
        TeleportLocationVector(285, 23.0, 23.0);
        TeleportLocationVector(286, 23.0, 23.0);
    }
}

void OnPlaceArea5R3()
{
    int once;
    StartThisArea(&once, PlaceArea5R3);
}

void PlaceArea5R4(int targetunit)
{
    int count = 12;

    while (count  --)
    {
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 287), targetunit);
        TeleportLocationVector(287, -23.0, 23.0);
    }
}

void OnPlaceArea5R4()
{
    int once;
    StartThisArea(&once, PlaceArea5R4);
}

void PlaceArea5R5(int targetunit)
{
    int locationId= 288;
    int rep=-1;

    while ((++rep) < 4)
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, locationId++), targetunit);
}

void OnPlaceArea5R5()
{
    int once;
    StartThisArea(&once, PlaceArea5R5);
}

void PlaceArea5R6(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 292), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 293), targetunit);

    int locationId= 294;
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, locationId++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, locationId++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, locationId++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, locationId++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, locationId++), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, locationId++), targetunit);
}

void OnPlaceArea5R6()
{
    int once;
    StartThisArea(&once, PlaceArea5R6);
}

void PlaceArea5R7(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyShadowMan, 300), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyShadowMan, 301), targetunit);
}

void OnPlaceArea5R7()
{
    int once;
    StartThisArea(&once, PlaceArea5R7);
}

void PlaceArea5R8(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 302), targetunit);
}

void OnPlaceArea5R8()
{
    int once;
    StartThisArea(&once, PlaceArea5R8);
}

void PlaceArea5R9(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 340), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 341), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 342), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 343), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 345), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 344), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyMedicAtLocation, 346), targetunit);
}

void OnPlaceArea5R9()
{
    int once;
    StartThisArea(&once, PlaceArea5R9);
}

void PlaceArea5RA(int targetunit)
{
    int rep = -1;

    while ((++rep) < 6)
    {
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 347), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 348), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 349), targetunit);
    }

    int wallrep = -1;

    while ((++wallrep) < 11)
    {
        WallUtilOpenWallAtObjectPosition(350);
        TeleportLocationVector(350, -23.0, 23.0);
        if (wallrep < 7)
        {
            WallUtilOpenWallAtObjectPosition(351);
            TeleportLocationVector(351, -23.0, 23.0);
        }
    }
}

void OnPlaceArea5RA()
{
    int once;
    StartThisArea(&once, PlaceArea5RA);
}

void PlaceArea5RB(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyShadowMan, 352), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyShadowMan, 353), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyShadowMan, 354), targetunit);
}

void OnPlaceArea5RB()
{
    int once;
    StartThisArea(&once, PlaceArea5RB);
}

void OpenBaseSecretWall()
{
    ObjectOff(SELF);

    int wallx1 = 117, wally1 = 237;

    while (wallx1 <= 125)
        WallOpen(Wall(wallx1++, wally1++));

    int wallx2 = 113, wally2 = 239;

    while (wallx2 <= 116)
        WallOpen(Wall(wallx2++, wally2--));

    int rep = -1;

    while ((++rep) < 8)
    {
        PlaceMachineGun(LocationX(355), LocationY(355));
        TeleportLocationVector(355, 23.0, 23.0);
    }
    UniPrint(OTHER, "지하 탄약고 무기고 비밀벽이 열립니다");
}

void BasePartWall()
{
    int wallx = 44, wally = 228;

    while (wallx <= 47)
        WallToggle(Wall(wallx++, wally--));
}

void PlaceArea4R4(int targetunit)
{
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 303), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 304), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 305), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 306), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 307), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 308), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 309), targetunit);

    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 383), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 384), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 385), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 386), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 387), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 388), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 389), targetunit);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 390), targetunit);
}

void OnPlaceArea4R4()
{
    int once;
    StartThisArea(&once, PlaceArea4R4);
}

void EndAllScenario()
{
    CreateObjectAtEx("PlayerStart", LocationX(321), LocationY(321), NULLPTR);
    CreateObjectAtEx("PlayerStart", LocationX(322), LocationY(322), NULLPTR);
    CreateObjectAtEx("PlayerStart", LocationX(323), LocationY(323), NULLPTR);
    CreateObjectAtEx("PlayerStart", LocationX(324), LocationY(324), NULLPTR);
    Delete(Object("splayerstart"));
}

void PlaceArea4R5(int targetunit)
{
    int first = 310;

    while (first <= 315)
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, first++), targetunit);

    int count = 15;

    while (count--)
    {
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 316), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 317), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMeleeSoldierAtLocation, 318), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 319), targetunit);
        LookAtObject(CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 320), targetunit);

        TeleportLocationVector(316, 27.0, -27.0);
        TeleportLocationVector(317, 27.0, -27.0);
        TeleportLocationVector(318, 27.0, -27.0);
        TeleportLocationVector(319, 27.0, -27.0);
        TeleportLocationVector(320, 27.0, -27.0);
    }

    FrameTimer(30, EndAllScenario);
}

void OnPlaceArea4R5()
{
    int once;
    StartThisArea(&once, PlaceArea4R5);
}

void WeaponCollideHandlerM16Bullet()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 150, 1);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.2);
        }
        else if (!GetCaller())
        {
            if (WallUtilGetWallAtObjectPosition(SELF))
                WallUtilDestroyWallAtObjectPosition(SELF);
        }
        else
            break;
        Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        PlaySoundAround(SELF, SOUND_DiamondHitStone);
        Delete(SELF);
        break;
    }
}

void WeaponUseHandlerM16Gun()
{
    int *cfps = 0x84ea04;
    int cTime = GetUnit1C(SELF);

    if (ABS(cfps[0] - cTime) < 45)
    {
        UniPrint(OTHER, "재장전 중입니다... 쿨다운 1.5초");
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cfps[0]);
        float xVect = UnitAngleCos(OTHER, 17.0), yVect = UnitAngleSin(OTHER, 17.0);
        int bullet = CreateObjectAt("SpiderSpit", GetObjectX(OTHER) + xVect, GetObjectY(OTHER) + yVect);

        DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 12);
        LookWithAngle(bullet, GetDirection(OTHER));
        SetOwner(OTHER, bullet);
        SetUnitCallbackOnCollide(bullet, WeaponCollideHandlerM16Bullet);
        PushObjectTo(bullet, xVect * 2.0, yVect * 2.0);
        PlaySoundAround(OTHER, SOUND_PowderBarrelExplode);
        PlaySoundAround(OTHER, SOUND_CrossBowShoot);
    }
}

int PlaceM16Gun(float xpos, float ypos)
{
    int m16 = CreateObjectAt("MissileWand", xpos, ypos);

    SetUnitCallbackOnUseItem(m16, WeaponUseHandlerM16Gun);
    Frozen(m16, TRUE);
    SetUnitSubclass(m16, 0x800000);
    return m16;
}

void WeaponCollideHandlerMachineGunBullet()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 300, DAMAGE_TYPE_IMPACT);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.2);
        }
        else if (!GetCaller())
        {
            if (WallUtilGetWallAtObjectPosition(SELF))
                WallUtilDestroyWallAtObjectPosition(SELF);
        }
        else
            break;
        Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        PlaySoundAround(SELF, SOUND_DiamondHitStone);
        Delete(SELF);
        break;
    }
}

void ShotMachineGun(int caster)
{
    float xvect = UnitAngleCos(caster, 9.0), yvect = UnitAngleSin(caster, 9.0);
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(caster) + xvect -(yvect*3.0), GetObjectY(caster) + yvect +(xvect*3.0));

    int rep = -1;
    int single, dir = GetDirection(caster);
    while ((++rep) < 7)
    {
        FxCreateHarpoon(GetObjectX(posUnit), GetObjectY(posUnit), &single);

        SetOwner(caster, single);
        PushObjectTo(single, xvect*4.0, yvect*4.0);
        SetUnitCallbackOnCollide(single, WeaponCollideHandlerMachineGunBullet);
        MoveObjectVector(posUnit, yvect * 3.0, -xvect * 3.0);
        LookWithAngle(single, dir);
    }
    Delete(posUnit);
}

void WeaponUseHandlerMachineGun()
{
    int *cfps = 0x84ea04;
    int cTime = GetUnit1C(SELF);

    if (ABS(cfps[0] - cTime) < 24)
    {
        UniPrint(OTHER, "재장전 중입니다... 쿨다운 0.8초");
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cfps[0]);
        ShotMachineGun(OTHER);

        int fx = CreateObjectAt("FireGrateFlame", GetObjectX(OTHER), GetObjectY(OTHER) + 2.0);

        DeleteObjectTimer(fx, 12);
        UnitNoCollide(fx);
        PlaySoundAround(OTHER, SOUND_PowderBarrelExplode);
        PlaySoundAround(OTHER, SOUND_CrossBowShoot);
    }
}

int PlaceMachineGun(float xpos, float ypos)
{
    int machgun = CreateObjectAt("OblivionOrb", xpos, ypos);

    SetUnitCallbackOnUseItem(machgun, WeaponUseHandlerMachineGun);
    SetItemPropertyAllowAllDrop(machgun);
    DisableOblivionItemPickupEvent(machgun);
    Frozen(machgun, TRUE);

    return machgun;
}

void ManaBombCancelFx(float xpos, float ypos)
{
    int caster = CreateObjectAt("ImaginaryCaster", xpos, ypos);

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}

void GreenSparkFx(float x, float y)
{
    int fxUnit = CreateObjectAt("MonsterGenerator", x, y);

    Damage(fxUnit, 0, 10, 100);
    Delete(fxUnit);
}

void BerserkerEnergyTouched()
{
    if (!CurrentHealth(OTHER))
        return;

    int owner = GetOwner(GetTrigger() - 1);

    if (IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 500, DAMAGE_TYPE_PLASMA);
        Enchant(OTHER, "ENCHANT_CHARMING", 1.0);
        GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
    }
}

void BerserkerEnergyLoop(int sUnit)
{
    int owner = GetOwner(sUnit), durate = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(owner) && durate)
        {
            if (IsVisibleTo(sUnit, sUnit + 1))
            {
                DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(sUnit + 1), GetObjectY(sUnit + 1)), 18);
                MoveObjectVector(sUnit + 1, UnitAngleCos(sUnit + 1, 19.0), UnitAngleSin(sUnit + 1, 19.0));
                FrameTimerWithArg(1, sUnit, BerserkerEnergyLoop);
                LookWithAngle(sUnit, --durate);
                break;
            }
        }
        Delete(sUnit++);
        Delete(sUnit++);
        break;
    }
}

void CastBerserkerEnergyPar(int sOwner)
{
    if (CurrentHealth(sOwner))
    {
        int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sOwner) + UnitAngleCos(sOwner, 21.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 21.0));

        SetOwner(sOwner, DummyUnitCreate("LichLord", GetObjectX(unit), GetObjectY(unit)) - 1);
        LookWithAngle(unit, 31);
        LookWithAngle(unit + 1, GetDirection(sOwner));
        SetCallback(unit + 1, 9, BerserkerEnergyTouched);
        FrameTimerWithArg(1, unit, BerserkerEnergyLoop);
    }
}

void OnUseLaserRifle()
{
    if (!CurrentHealth(OTHER))
        return;

    int *cFps = 0x84ea04;
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps[0] - cTime) < 35)
    {
        UniPrint(OTHER, "재사용 대기시간이에요");
        return;
    }

    SetUnit1C(SELF, cFps[0]);
    FrameTimerWithArg(3, GetCaller(), CastBerserkerEnergyPar);
}

void OnPickupLaserRifle()
{
    int *cFps = 0x84ea04;
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps[0] - cTime) < 10)
        return;

    if (!CurrentHealth(OTHER))
        return;

    SetUnit1C(SELF, cFps[0]);
    AbsolutelyWeaponPickupAndEquip(GetCaller(), GetTrigger());
    UniPrint(OTHER, "이 지팡이를 다시 장착하려면 바닥에 버린 후 다시 주우면 됩니다");
}

int PlaceLaserRifle(float xpos, float ypos)
{
    int unit = CreateObjectAt("SulphorousFlareWand", xpos, ypos);

    SetUnitCallbackOnPickup(unit, OnPickupLaserRifle);
    SetUnitCallbackOnUseItem(unit, OnUseLaserRifle);
    return unit;
}

void PlaceRowLaserRifleGun()
{
    int base = CreateObjectAt("ImaginaryCaster", LocationX(325), LocationY(325)), u = -1;

    while ((++u) < 10)
        Enchant(PlaceLaserRifle(GetObjectX(base + u) + 23.0, GetObjectY(base + u) + 23.0), "ENCHANT_ANCHORED", 0.0);
}

void PlaceRowForM16Gun()
{
    int base = CreateObjectAt("ImaginaryCaster", LocationX(23), LocationY(23)), u;

    for (u = 0 ; u < 10 ; Nop(++u))
        Enchant(PlaceM16Gun(GetObjectX(base + u) + 23.0, GetObjectY(base + u) - 23.0), "ENCHANT_ANCHORED", 0.0);
}

void WeaponCollideHandlerK2Bullet()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 200, 1);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.2);
        }
        else if (!GetCaller())
        {
            if (WallUtilGetWallAtObjectPosition(SELF))
                WallUtilDestroyWallAtObjectPosition(SELF);
        }
        else
            break;
        Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        PlaySoundAround(SELF, SOUND_DiamondHitStone);
        Delete(SELF);
        break;
    }
}

void WeaponUseHandlerK2Gun()
{
    int *cfps = 0x84ea04;
    int cTime = GetUnit1C(SELF);

    if (ABS(cfps[0] - cTime) < 45)
    {
        UniPrint(OTHER, "재장전 중입니다... 쿨다운 1.5초");
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cfps[0]);
        float xvect = UnitAngleCos(OTHER, 19.0), yvect = UnitAngleSin(OTHER, 19.0);
        int bullet = CreateObjectAt("WeakArcherArrow", GetObjectX(OTHER) + xvect, GetObjectY(OTHER) + yvect);

        DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 12);
        SetOwner(OTHER, bullet);
        SetUnitCallbackOnCollide(bullet, WeaponCollideHandlerK2Bullet);
        LookWithAngle(bullet, GetDirection(OTHER));
        PushObjectTo(bullet, xvect * 2.0, yvect * 2.0);
        PlaySoundAround(OTHER, SOUND_PowderBarrelExplode);
        PlaySoundAround(OTHER, SOUND_CrossBowShoot);
    }
}

int PlaceK2Gun(float xpos, float ypos)
{
    int k2 = CreateObjectAt("DemonsBreathWand", xpos, ypos);

    SetUnitCallbackOnUseItem(k2, WeaponUseHandlerK2Gun);
    Frozen(k2, TRUE);
    return k2;
}

void PlaceRowForK2Gun(int locationId)
{
    int base = CreateObjectAt("ImaginaryCaster", LocationX(locationId), LocationY(locationId)), u;

    for (u = 0 ; u < 10 ; Nop(++u))
        Enchant(PlaceK2Gun(GetObjectX(base + u) - 23.0, GetObjectY(base + u) + 23.0), "ENCHANT_ANCHORED", 0.0);
}

void OpenArmouryWalls()
{
    int u;

    for (u = 0 ; u < 11 ; Nop(++u))
    {
        WallUtilOpenWallAtObjectPosition(22);
        TeleportLocationVector(22, 23.0, -23.0);
    }
    FrameTimer(10, PlaceRowForM16Gun);
    FrameTimerWithArg(10, 183, PlaceRowForK2Gun );
    UniPrintToAll("좌측에 무기고가 있다. 무기고에서 총기를 챙긴 후 사살을 시작하자");
}

void MainGameStart()
{
    FrameTimer(1, PlaceArea1Room1Enemies);
    FrameTimer(30, OpenArmouryWalls);
    FrameTimer(50, LoopSearchIndex);
}

int PromotionClassEnemies()
{
    int npc[4];

    return npc;
}

void PromotionClassStartAttack(int *npc)
{
    string npcment[] =
    {
        "이 씹세끼 여기 있었네?ㅋㅋ",
        "뒤질래 씹세끼야?",
        "인사 똑바로 안해 이 세끼야!",
        "차려 열중쉬어 경례해 새끼야"
    };
    int u;

    for (u = 0 ; u < 4 ; Nop(++u))
    {
        ObjectOn(npc[u]);
        AggressionLevel(npc[u], 1.0);
        UniChatMessage(npc[u], npcment[u], 108);
    }
}

void PromotionClassDecayEnemies(int *npc)
{
    int u;

    for (u = 0 ; u < 4 ; Nop(++u))
    {
        Delete(GetLastItem(npc[u]));
        Effect("SMOKE_BLAST", GetObjectX(npc[u]), GetObjectY(npc[u]), 0.0, 0.0);
        Delete(npc[u]);
    }
}

void PromotionClassReportAttackComplete(int *npc)
{
    int u;

    for (u = 0 ; u < 4 ; Nop(++u))
        UniChatMessage(npc[u], "ㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋ 병신", 90);
    FrameTimerWithArg(45, npc, PromotionClassDecayEnemies);
}

void PromotionClassEnd()
{
    g_viewpromotion = FALSE;
    int u;

    for (u = 9 ; u >= 0 ; Nop(--u))
    {
        PlayerClassRemoveCamera(u);
    }
    FrameTimer(50, MainGameStart);
}

void PromotionClassLastMent(int mrim)
{
    UniPrintToAll("죽일거다... 모두 죽여버리겠어!!");

    DeleteObjectTimer(mrim, 3);
    FrameTimer(50, PromotionClassEnd);
}

void PromotionClassMrImDead()
{
    int *npc = PromotionClassEnemies();

    FrameTimerWithArg(20, npc, PromotionClassReportAttackComplete);
    FrameTimerWithArg(128, GetTrigger(), PromotionClassLastMent);
}

void PromotionClassInitEnemies()
{
    int *npcptr = PromotionClassEnemies();

    if (npcptr == NULLPTR)
        return;
    int u, val;
    char buff[32];

    for (u = 0 ; u < 4 ; Nop(++u))
    {
        val=u+1;
        NoxSprintfString(buff, "enm%d", &val, 1);
        npcptr[u] = Object(ReadStringAddressEx(buff));
    }
}

void PromotionClassRespectEnemies(int unit)
{
    int *npc = PromotionClassEnemies(), u;

    for (u = 0 ; u < 4 ; Nop(++u))
    {
        MoveObject(npc[u], LocationX(17 + u), LocationY(17 + u));
        LookAtObject(npc[u], unit);
        Effect("SMOKE_BLAST", GetObjectX(npc[u]), GetObjectY(npc[u]), 0.0, 0.0);
    }
    FrameTimerWithArg(30, npc, PromotionClassStartAttack);
}

void PromotionClassReason(int unit)
{
    UniChatMessage(unit, "자대 배치 이후부터 나는 꾸준히 상관 뿐만 아니라 동기들 에게 까지도 따돌림 받고 있었다...", 120);
    SetOwner(GetHost(), unit);
    ObjectOn(unit);
    SetUnitMass(unit, 9999.0);
}

void PromotionClassMynameis(int unit)
{
    UniChatMessage(unit, "내가 바로 임병장이다", 120);
    LookWithAngle(unit, 64);
}

void PromotionClassEndMent()
{
    int mrim = Object("mrim");

    MoveObject(mrim, GetObjectX(g_blueflame), GetObjectY(g_blueflame));
    Delete(g_blueflame);
    Effect("SMOKE_BLAST", GetObjectX(mrim), GetObjectY(mrim), 0.0, 0.0);
    Effect("TELEPORT", GetObjectX(mrim), GetObjectY(mrim), 0.0, 0.0);
    FrameTimerWithArg(30, mrim, PromotionClassMynameis);
    FrameTimerWithArg(90, mrim, PromotionClassReason);
    FrameTimerWithArg(155, mrim, PromotionClassRespectEnemies);

    SetCallback(mrim, 5, PromotionClassMrImDead);
}

void PromotionClassStartMent()
{
    string mentTb[] =
    {
        "임병장의 살인 회고록...                  제작. 녹스게임리마스터",
        "이 게임은 실화를 바탕으로 재구성된 스토리로 진행됩니다  ",
        "2014년 6월 21일 오후 8시 15분 경 강원도 고성군에 위치한 육군 제22 보병사단 55연대 GOP에서 총기난사 사건이 발생했다",
        "가해자는 전역을 불과 3개월 앞두고 사상 최악의 총기테러를 일으켜 당시 사회는 큰 충격에 휩싸였다",
        "비슷한 시기에 일어났던 제28보병사단 의무병 살인사건(윤 일병 사건), 김 일병 자살 사건과 더불어서",
        "대한민국 육군의 고질적인 병영 문화의 폐해가 만들어낸 비극이자, 사회에서 피해를 입고 당하기만 했던 사람이 참지 못하고 벌인 살인극"
    };
    int mentOrder;

    if (mentOrder < sizeof(mentTb))
    {
        UniPrintToAll(mentTb[mentOrder++]);
        FrameTimer(90, PromotionClassStartMent);
    }
    else
    {
        FrameTimer(36, PromotionClassEndMent);
    }
    
}

void StartPromotionOnce()
{
    int once;

    if (!once)
    {
        m_promoteStarted = TRUE;
        once = TRUE;
        g_blueflame = CreateObjectAt("LargeBlueFlame", LocationX(15), LocationY(15));
        PromotionClassInitEnemies();

        FrameTimer(30, PromotionClassStartMent);
    }
}

int PlayerClassCreateCamera(int plr, float xpos, float ypos)
{
    int cam = DummyUnitCreate("Bomber", xpos, ypos);
    int pUnit = g_player[plr];

    UnitNoCollide(cam);
    Enchant(pUnit, "ENCHANT_INVULNERABLE", 0.0);
    MoveObject(pUnit, LocationX(16), LocationY(16));
    return cam;
}

void PlayerClassEntryPromotion(int plr, int pUnit)
{
    if (!g_camera[plr])
    {
        g_camera[plr] = PlayerClassCreateCamera(plr, LocationX(15), LocationY(15));
    }

    StartPromotionOnce();
}

void PleaseDontShowPromote()
{
    if (m_promoteStarted)
    {
        UniPrint(OTHER, "이미 프로모션이 진행중입니다");
        return;
    }
    if (g_viewpromotion)
    {
        PromotionClassEnd();
        UniPrint(OTHER, "프로모션을 보지 않도록 설정하였습니다");
    }
    else
        UniPrint(OTHER, "프로모션을 보지 않도록 하기 위한 설정이 이미 완료되어 있습니다");
}

void PlaceDontShowPromote()
{
    int dshowPromote = DummyUnitCreate("Horrendous", LocationX(356), LocationY(356));

    SetDialog(dshowPromote, "NORMAL", PleaseDontShowPromote, Nop);
}

void EmptyAll(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void DelayAbpick(int *pItemInfo)
{
    int owner = pItemInfo[0];
    int equipment = pItemInfo[1];
    int memClear = pItemInfo[2];

    if (!CurrentHealth(owner))
        return;

    if (!MaxHealth(equipment))
        return;

    AbsolutelyWeaponPickupAndEquip(owner, equipment);
    if (memClear == TRUE)
        MemFree(pItemInfo);
}

void ArrayCopy(int *src, int *dest, int size)
{
    int rep = -1;

    while ((++rep) < size)
        dest[rep] = src[rep];
}

void SetPendantSpecIceAge(int pendant)
{
    SetUnitCallbackOnUseItem(pendant, PlayerUseItem1);
    SetItemPropertyAllowAllDrop(pendant);
}

void SetItemSpecSozu(int potion)
{
    SetUnitCallbackOnUseItem(potion, PlayerDrinkSozu);
}

void SetItemSpecCastFON(int pendant)
{
    SetUnitCallbackOnUseItem(pendant, PlayerCastFON);
}

void SetItemSpecSummonStone(int pendant)
{
    SetUnitCallbackOnUseItem(pendant, PlayerUseItemSummonGolem);
}

void SetItemSpecPoisonTrap(int trap)
{
    SetUnitCallbackOnDiscard(trap, GasTrapOnDiscard);
}

void SetItemSpecNinjaSword(int pendant)
{
    SetUnitCallbackOnUseItem(pendant, UseNinjaSwordPendant);
}

void SetItemSpecCInstall(int paper)
{
    SetUnitCallbackOnDiscard(paper, CInstallWhenDiscard);
}

void SetItemSpecBucherCandle(int candle)
{
    SetUnitCallbackOnUseItem(candle, BucherCandleOnUse);
}

void SetItemSpecRottenMeat(int meat)
{
    SetUnitCallbackOnUseItem(meat, RottenMeatOnUse);
}

void SetItemSpecWindRun(int item)
{
    SetUnitCallbackOnUseItem(item, WindRunBootsOnUse);
    SetItemPropertyAllowAllDrop(item);
}

void FindOutAllItem(int lastitem, int findAction)
{
    int thingid = GetUnitThingID(lastitem);
    int cItem = GetPreviousItem(lastitem);

    CallFunctionWithArg(findAction, lastitem);

    while (cItem)
    {
        if (GetUnitThingID(cItem) == thingid)
            CallFunctionWithArg(findAction, cItem);
        else
        {
            break;
        }
        
        cItem = GetPreviousItem(cItem);
    }
}

void SubstitutionArmorPackToBreasplate(int pUnit, int lastitem)
{
    Delete(lastitem);

    int armor = CreateObjectAt("Breastplate", GetObjectX(pUnit), GetObjectY(pUnit));

    SetArmorPropertiesDirect(armor, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Regeneration3);
    Frozen(armor, TRUE);
    int szParam[]={pUnit, armor, TRUE};
    int *pParam = MemAlloc(sizeof(szParam) * 4);

    ArrayCopy(szParam, pParam, sizeof(szParam));
    DelayAbpick(pParam);
}

void SubstitutionArmorPackToArmLeg(int pUnit, int lastitem)
{
    Delete(lastitem);

    int armor = CreateObjectAt("PlateArms", GetObjectX(pUnit), GetObjectY(pUnit));

    SetArmorPropertiesDirect(armor, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Regeneration3);
    Frozen(armor, TRUE);
    int szParam[]={pUnit, armor, TRUE};
    int *pParam = MemAlloc(sizeof(szParam) * 4);

    ArrayCopy(szParam, pParam, sizeof(szParam));
    DelayAbpick(pParam);
}

void SubstitutionArmorPackToCloak(int pUnit, int lastitem)
{
    Delete(lastitem);

    int armor = CreateObjectAt("MedievalCloak", GetObjectX(pUnit), GetObjectY(pUnit));

    SetArmorPropertiesDirect(armor, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Speed3, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_Speed3);
    Frozen(armor, TRUE);
    int szParam[]={pUnit, armor, TRUE};
    int *pParam = MemAlloc(sizeof(szParam) * 4);

    ArrayCopy(szParam, pParam, sizeof(szParam));
    DelayAbpick(pParam);
}

void OnChangeToBootsOfSpeed(int lastitem, int pUnit)
{
    Delete(lastitem);

    int speedBoot = CreateObjectAt("PlateBoots", GetObjectX(pUnit), GetObjectY(pUnit));

    SetArmorPropertiesDirect(speedBoot, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_Speed3);
    Frozen(speedBoot, TRUE);
    int szParam[] = {pUnit, speedBoot, TRUE};
    int *pParam = MemAlloc(12);

    ArrayCopy(szParam, pParam, sizeof(szParam));
    DelayAbpick(pParam);
}

void OnChangeToAmuletOfClarity(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetPendantSpecIceAge);
}

void OnChangeToCider(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecSozu);
}

void OnChangeToBookOfOblivion(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecCastFON);
}

void OnChangeToAmuletofCombat(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecSummonStone);
}

void OnChangeToTraderArmorRack1(int lastitem, int pUnit)
{
    SubstitutionArmorPackToBreasplate(pUnit, lastitem);
}

void OnChangeToAmuletofNature(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecPoisonTrap);
}

void OnChangeToAmuletofManipulation(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecNinjaSword);
}

void OnChangeToBraceletofHealth(int lastitem, int pUnit)
{
    SubstitutionArmorPackToArmLeg(pUnit, lastitem);
}

void OnChangeToMatildasCloak(int lastitem, int pUnit)
{
    SubstitutionArmorPackToCloak(pUnit, lastitem);
}

void OnChangeToSponsorshipLetter(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecCInstall);
}

void OnChangeToBottleCandle(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecBucherCandle);
}

void OnChangeToRottenMeat(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecRottenMeat);
}

void OnChangeToBridgeGuardsBoots(int lastitem, int pUnit)
{
    FindOutAllItem(lastitem, SetItemSpecWindRun);
}

void OnChangeProto(int functionId, int lastitem, int pUnit)
{
    Bind(functionId, &functionId+4);
}

void PlayerClassOnLastItemChanged(int pUnit, int lastitem)
{
    int action = 0;

    if (HashGet(m_changedItemHashInstance, GetUnitThingID(lastitem), &action, FALSE))
        OnChangeProto(action, lastitem, pUnit);
}

void InitChangedItemFunctions()
{
    HashCreateInstance(&m_changedItemHashInstance);

    HashPushback(m_changedItemHashInstance, 622, OnChangeToBootsOfSpeed);
    HashPushback(m_changedItemHashInstance, 646, OnChangeToRottenMeat);
    HashPushback(m_changedItemHashInstance, 650, OnChangeToCider);
    HashPushback(m_changedItemHashInstance, 1183, OnChangeToAmuletofCombat);
    HashPushback(m_changedItemHashInstance, 1184, OnChangeToAmuletofManipulation);
    HashPushback(m_changedItemHashInstance, 1185, OnChangeToAmuletofNature);
    HashPushback(m_changedItemHashInstance, 1188, OnChangeToBraceletofHealth);
    HashPushback(m_changedItemHashInstance, 1197, OnChangeToBottleCandle);
    HashPushback(m_changedItemHashInstance, 1637, OnChangeToTraderArmorRack1);
    HashPushback(m_changedItemHashInstance, 2177, OnChangeToBookOfOblivion);
    HashPushback(m_changedItemHashInstance, 2178, OnChangeToAmuletOfClarity);
    HashPushback(m_changedItemHashInstance, 2184, OnChangeToBridgeGuardsBoots);
    HashPushback(m_changedItemHashInstance, 2207, OnChangeToMatildasCloak);
    HashPushback(m_changedItemHashInstance, 2295, OnChangeToSponsorshipLetter);
}

int WarAbilityTable(int aSlot, int pIndex)
{
    return GetMemory(0x753600 + (pIndex * 24) + (aSlot * 4));
}

void WarAbilityUse(int pUnit, int aSlot, int actionFunction)
{
    int chk[160], pIndex = GetPlayerIndex(pUnit), cTime;
    int arrPic;

    if (!(pIndex >> 0x10))
    {
        arrPic = pIndex * 5 + aSlot; //EyeOf=5, harpoon=3, sneak=4, berserker=1
        cTime = WarAbilityTable(aSlot, pIndex);
        if (cTime ^ chk[arrPic])
        {
            if (!chk[arrPic])
            {
                CallFunctionWithArg(actionFunction, pUnit);
            }
            chk[arrPic] = cTime;
        }
    }
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; Nop(i --))
    {
        if (IsCaller(g_player[i]))
            return i;
    }
    return -1;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    g_player[plr] = pUnit;
    g_player[plr + 10] = 1;

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
        {
            ClientsideProcess();
        }
        FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);
    }
    EmptyAll(pUnit);
    SelfDamageClassEntry(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));

    return plr;
}

void PlayerClassJoin(int plr, int pUnit)
{
    if (PlayerClassGetDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    
    Enchant(pUnit, "ENCHANT_SLOWED", 0.0);
    Enchant(pUnit, "ENCHANT_HASTED", 0.0);
    
    if (g_viewpromotion)
    {
        PlayerClassEntryPromotion(plr, pUnit);
        return;
    }
    DiePlayerHandlerEntry(pUnit);
    MoveObject(pUnit, LocationX(PLAYER_RESPAWN_LOCATION), LocationY(PLAYER_RESPAWN_LOCATION));
}

void PlayerClassFailToJoin(int pUnit)
{
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    MoveObject(pUnit, LocationX(13), LocationY(13));
    UniPrint(pUnit, "이 맵에 참가하실 수 없습니다. 잠시 후 다시 시도해 보세요");
}

void PlayerClassEntryPrivate(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        while (TRUE)
        {
            int u;

            for (u = 9 ; u >= 0 && plr < 0 ; Nop(--u))
            {
                if (!MaxHealth(g_player[u]))
                {
                    plr = PlayerClassOnInit(u, pUnit);
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerClassJoin(plr, pUnit);
                break;
            }
            PlayerClassFailToJoin(pUnit);
            break;
        }
    }
}

void PlayerClassEntry()
{
    PlayerClassEntryPrivate(CheckPlayer(), GetCaller());
}

void PlayerClassFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        Enchant(OTHER, EnchantList(ENCHANT_ANTI_MAGIC), 0.0);
        int plr = CheckPlayer();

        if (plr >= 0)
        {
            MoveObject(g_player[plr], LocationX(11), LocationY(11));
            UniPrint(g_player[plr], "패스트 조인 되셨습니다");
        }
        else
        {
            MoveObject(OTHER, LocationX(12), LocationY(12));
        }
    }
}

void PlayerClassOnVision(int plr, int pUnit)
{
    if (CheckWatchFocus(pUnit))
    {
        PlayerLook(pUnit, g_camera[plr]);
    }
}

void PlayerClassOnCastBerserker(int pUnit)
{
    UniPrint(pUnit, "버저커 차지 금지!");
    Enchant(pUnit, EnchantList(ENCHANT_FREEZE), 5.0);
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (MaxHealth(g_camera[plr]))
        PlayerClassOnVision(plr, pUnit);

    int curLastItem = GetLastItem(pUnit);

    if (curLastItem != m_lastItemArr[plr])
    {
        PlayerClassOnLastItemChanged(pUnit, curLastItem);
        m_lastItemArr[plr] = curLastItem;
    }

    WarAbilityUse(pUnit, 1, PlayerClassOnCastBerserker);
}

void PlayerClassOnDeath(int plr)
{
    int pUnit = g_player[plr];
    char buff[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(pUnit));

    NoxSprintfString(buff, "%s 님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

void PlayerClassOnExit(int plr)
{
    g_player[plr] = 0;
    g_player[plr + 10] = 0;
    if (g_camera[plr])
    {
        Delete(g_camera[plr]);
        g_camera[plr] = 0;
    }
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; Nop(--i))
    {
        while (TRUE)
        {
            if (MaxHealth(g_player[i]))
            {
                if (GetUnitFlags(g_player[i]) & 0x40)
                    1;
                else if (CurrentHealth(g_player[i]))
                {
                    PlayerClassOnAlive(i, g_player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassGetDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (g_player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
    ResetChangedWorth();
}

void GunnerBulletCollide()
{
    while (GetTrigger())
    {
        int owner = GetOwner(SELF);

        if (CurrentHealth(owner))
        {
            if (IsAttackedBy(OTHER, owner))
                Damage(OTHER, owner, 150, DAMAGE_TYPE_BLADE);
        }
        Delete(SELF);
        break;
    }
}

void GunnerShotBullet(int curid)
{
    int owner = GetOwner(curid);

    if (CurrentHealth(owner))
    {
        DeleteObjectTimer(CreateObjectAt("Smoke", GetObjectX(owner), GetObjectY(owner)), 12);
        PlaySoundAround(owner, SOUND_PowderBarrelExplode);

        int mis = CreateObjectAt("LightningBolt", GetObjectX(curid), GetObjectY(curid));

        SetOwner(owner, mis);
        SetUnitCallbackOnCollide(mis, GunnerBulletCollide);
        LookAtObject(mis, owner);
        LookWithAngle(mis, GetDirection(mis) + 128);
        PushObject(mis, 23.0, GetObjectX(owner), GetObjectY(owner));
    }
    Delete(curid);
}

void MasterBulletCollide()
{
    while (GetTrigger())
    {
        int owner = GetOwner(SELF);

        if (CurrentHealth(owner))
        {
            if (CurrentHealth(OTHER))
            {
                if (IsAttackedBy(OTHER, owner))
                    Damage(OTHER, owner, 150, DAMAGE_TYPE_BLADE);
            }
            else if (!GetCaller())
            {
                if (WallUtilGetWallAtObjectPosition(SELF))
                    WallUtilDestroyWallAtObjectPosition(SELF);
            }
            else
            {
                break;
            }
            
        }
        Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Delete(SELF);
        break;
    }
}

void MasterShotBullet(int curid)
{
    int owner = GetOwner(curid);

    if (CurrentHealth(owner))
    {
        DeleteObjectTimer(CreateObjectAt("Smoke", GetObjectX(owner), GetObjectY(owner)), 12);
        PlaySoundAround(owner, SOUND_PowderBarrelExplode);

        int mis = CreateObjectAt("ArcherBolt", GetObjectX(curid), GetObjectY(curid));

        SetOwner(owner, mis);
        SetUnitCallbackOnCollide(mis, MasterBulletCollide);
        LookAtObject(mis, owner);
        LookWithAngle(mis, GetDirection(mis) + 128);
        PushObject(mis, 38.0, GetObjectX(owner), GetObjectY(owner));
    }
    Delete(curid);
}

void ExceptionPosOver(int *pPos)
{
    if (pPos[0] & 0x8000)  //isMinus
        pPos[0] = 0;

    else if (pPos[0] > 5800)
        pPos[0] = 5800;
}

void ExplosionBomb(int subsub)
{
    int caster = GetOwner(subsub);
    float xpos = GetObjectX(subsub + 1), ypos = GetObjectY(subsub + 1);

    SplashDamageAt(caster, 200, xpos, ypos, 130.0);
    Effect("EXPLOSION", xpos, ypos, 0.0, 0.0);
    PlaySoundAround(subsub, SOUND_FireExtinguish);
}

void FlyBomb(int subsub)
{
    while (IsObjectOn(subsub))
    {
        int durate = GetDirection(subsub);

        if (durate)
        {
            FrameTimerWithArg(1, subsub, FlyBomb);
            MoveObjectVector(subsub+1, GetObjectZ(subsub), GetObjectZ(subsub+1));
            MoveObject(subsub+2, GetObjectX(subsub+1), GetObjectY(subsub+1));
            Raise(subsub+2, MathSine(GetDirection(subsub+1)*5, 220.0));
            LookWithAngle(subsub, --durate);
            LookWithAngle(subsub+1, GetDirection(subsub+1)+1);
            break;
        }
        else
        {
            ExplosionBomb(subsub);
        }
        
        Delete(subsub++);
        Delete(subsub++);
        Delete(subsub++);
        break;
    }
}

//an array must be a size two only!!
int SummonBombPrivate(int caster, float *pXypos, int *pGetSub)
{
    int subUnit = CreateObjectAt("ImaginaryCaster", pXypos[0], pXypos[1]);

    if (!IsVisibleTo(caster, subUnit) && !IsVisibleTo(subUnit, caster))
    {
        Delete(subUnit);
        return FALSE;
    }

    CreateObjectAtEx("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster), NULLPTR);
    int barrel = CreateObjectAt("BlackPowderBarrel", GetObjectX(caster), GetObjectY(caster));

    UnitNoCollide(barrel);
    Frozen(barrel, TRUE);
    float gap = DistanceUnitToUnit(subUnit, subUnit+1);
    LookWithAngle(subUnit, 32);
    SetOwner(caster, subUnit);
    Raise(subUnit, UnitRatioX(subUnit, subUnit+1, gap/32.0));
    Raise(subUnit+1, UnitRatioY(subUnit, subUnit+1, gap/32.0));

    if (pGetSub != NULLPTR)
        pGetSub[0] = subUnit;
    return TRUE;
}

int ThrowBomb(int caster)
{
    if (!CurrentHealth(caster))
        return FALSE;

    int csorX, csorY;

    if (!GetPlayerMouseXY(caster, &csorX, &csorY))
        return FALSE;

    ExceptionPosOver(&csorX);
    ExceptionPosOver(&csorY);

    float xypos[] = {IntToFloat(csorX), IntToFloat(csorY)};
    int subsub = 0;

    if (!SummonBombPrivate(caster, xypos, &subsub))
    {
        UniPrint(caster, "타겟 위치는 캐릭터가 볼 수 없는 지역입니다!");
        return FALSE;
    }
    return FrameTimerWithArg(1, subsub, FlyBomb);
}

int HasItemWithThingId(int owner, int thingId, int *pDestItem)
{
    int item = GetLastItem(owner);

    while (item)
    {
        if (GetUnitThingID(item) == thingId)
        {
            if (pDestItem)
                pDestItem[0] = item;
            return TRUE;
        }

        item = GetPreviousItem(item);
    }
    return FALSE;
}

int IsRealHarpoon(int curId)
{
    int *ptr = UnitToPtr(curId);

    if (!ptr)
        return FALSE;
    return ptr[186] == 5567360;
}

void HarpoonEvent(int curid)
{
    int owner = GetOwner(curid), item;

    Delete(curid);
    if (HasItemWithThingId(owner, 1190, &item))
    {
        if (ThrowBomb(owner))
            Delete(item);
    }
    else
    {
        UniPrint(owner, "소유하고 계신 수류탄이 없습니다");
    }    
}

void DetectedSpecificIndex(int curId)
{
    if (!IsMissileUnit(curId))
        return;

    int thingId = GetUnitThingID(curId);

    if (thingId == 527)
        GunnerShotBullet(curId);
    else if (thingId == 1180)
        MasterShotBullet(curId);
    else if (thingId == 526)
    {
        if (IsRealHarpoon(curId))
            HarpoonEvent(curId);
    }
}

void LoopSearchIndex()
{
    int curId, tempId;
    int *ptr = 0x750710;

    ptr = ptr[0];
    while (ptr != NULLPTR)
    {
        tempId = ptr[11];
        if (curId)
        {
            while (curId < tempId)
                DetectedSpecificIndex(++curId);
            break;
        }
        curId = tempId;
        break;
    }
    FrameTimer(1, LoopSearchIndex);
}

void InitMapSign()
{
    string part21 = "팻말에는 \"2층 행정반\" 이라고 적혀있네요~";

    RegistSignMessage(Object("2FPIC1"), part21);
    RegistSignMessage(Object("2FPIC11"), part21);

    RegistSignMessage(Object("2FPIC2"), "팻말에는 \"체력 단련실을 포함한 다용도 공간\" 이라고 되어있네요~");

    RegistSignMessage(Object("3FPIC1"), "팻말에는 \"3층 휴게실\" 이라고 되어있네요~");
    RegistSignMessage(Object("3FPIC2"), "팻말에는 \"의무실\" 이라고 되어있네요~");
    RegistSignMessage(Object("3FPIC3"), "팻말에는 \"3층 통합 무기고\" 이라고 되어있네요~");

    RegistSignMessage(Object("1FPIC1"), "팻말에는 \"1층 병영 도서관\" 이라고 되어있네요~");
    RegistSignMessage(Object("1FPIC2"), "팻말에는 \"지휘 통제실\" 이라고 되어있네요~");
    RegistSignMessage(Object("1FPIC3"), "1층 궁국의 무기고- 대대 지휘자 외에는 출입을 금합니다");

    RegistSignMessage(Object("A1PIC1"), "이곳은 지하 탄약고 입구이다. 용무가 있을 시 초병을 통해 대대에 보고할 것");
    RegistSignMessage(Object("A1PIC2"), "-초전필승 1183 부대- 부대 방문을 환영합니다");
}

int PrevWorthTable()
{
    int arr[100];   //first= count

    return arr;
}

void ChangeObjectWorth(int thingId, int worth)
{
    int *pInfo = ThingDbEditGetArray(thingId);
    int *pTable = PrevWorthTable();

    pTable[++pTable[0]] = pInfo;
    pTable[++pTable[0]] = pInfo[12];
    
    pInfo[12] = worth;
}

void ResetChangedWorth()
{
    int *pTable = PrevWorthTable();
    int *pCount = pTable;

    while (TRUE)
    {
        if (!pCount[0])
            break;

        int value = pTable[pTable[0]--];
        int *ptr = pTable[pTable[0]--];

        ptr[12] = value;
    }
}

void InitBeforeExec()
{
    ChangeObjectWorth(1190, 100);
    ChangeObjectWorth(1184, 90);
    ChangeObjectWorth(622, 500);
    ChangeObjectWorth(650, 17);
    ChangeObjectWorth(2178, 300);
    ChangeObjectWorth(2177, 200);
    ChangeObjectWorth(1183, 230);
    ChangeObjectWorth(1637, 366);
    ChangeObjectWorth(1185, 100);
    ChangeObjectWorth(1188, 373);
    ChangeObjectWorth(2207, 369);
    ChangeObjectWorth(2295, 80);
    ChangeObjectWorth(1197, 400);
    ChangeObjectWorth(646, 400);
    ChangeObjectWorth(2184, 500);

    FixColorPotionThingDbSection(639, 125);
    FrameTimer(1, InitMapSign);

    PlaceDontShowPromote();
}

void InitEndSign()
{
    string signmessage = "사형수는 사형이 집행되기 전 까지 이곳에 머물게 된다. 00년대 이후로 사형이 집행된 적은 없으므로 사실 상 무기징역인 것이다";
    int rep = -1, u;
    char buff[32];
    
    while ((++rep) < 4)
    {
        u=rep+1;
        NoxSprintfString(buff, "endsign%d", &u, 1);
        RegistSignMessage(Object(ReadStringAddressEx(buff)), signmessage);
    }
}

void MapInitialize()
{
    MusicEvent();
    
    m_lastUnitId = CreateObjectAt("SpellBook", 100.0, 100.0);

    Delete(m_lastUnitId);
    FrameTimer(30, InitEndSign);
    FrameTimer(1, MakeCoopTeam);

    FrameTimer(10, PlayerClassOnLoop);

    RegistSignMessage(Object("mapinfo1f1"), "총기시건함- 사용 후 열쇠는 반드시 행정반에 반납할 것! 총기 사용대장 작성 필수");

    LockDoor(Object("mercg1"));
    LockDoor(Object("mercg2"));

    LockDoor(Object("area3e1"));
    LockDoor(Object("area3e2"));

    FrameTimer(3, InitBeforeExec);
    //mapfirstscan
    FrameTimerWithArg(1, Object("mapfirstscan"), MapUnitScan);

    InitStrikeFunctions();
    InitChangedItemFunctions();
}

static void ImageRemoveMe()
{ }

void ImageSpiderMine()
{ }

void ImageKorSozu()
{ }

void ImageVendingMachine()
{ }

void ImageBombBomb()
{ }

void ImageDangous()
{ }

void ImageMoney()
{ }

void ResourceBucherImage()
{ }

void MapBgmResource()
{ }

void ImageCustomHpSlot()
{ }

void ClientsideProcess()
{
    AppendAllDummyStaffsToWeaponList();

    m_pImgVector = CreateImageVector(16);

    InitializeImageHandlerProcedure(m_pImgVector);
    int index = 0;

    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageSpiderMine) + 4, 134390, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ResourceBucherImage) + 4, 17457, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageVendingMachine) + 4, 132289, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBombBomb) + 4, 112974, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageDangous) + 4, 112960, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageKorSozu) + 4, 17866, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageMoney) + 4, 14411, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageCustomHpSlot) + 4, 14505, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageRemoveMe)+4,17828, index++);

    ExtractMapBgm("..\\blood.mp3", MapBgmResource);
}

static int NetworkUtilClientTimerEnabler()
{ return TRUE; }

static void NetworkUtilClientMain()
{
    ClientsideProcess();
}

void MeleeSoliderStrike(int me, int you)
{
    Damage(you, me, 150, DAMAGE_TYPE_BLADE);
}

void EndThrowingPotion(int potionfx)
{
    int owner = GetOwner(potionfx);

    CreateObjectAt("BreakingPotion", GetObjectX(potionfx), GetObjectY(potionfx));
    SplashDamageAt(owner, 100, GetObjectX(potionfx), GetObjectY(potionfx), 50.0);
}

void ThrowingPotionLoop(int subunit)
{
    while (IsObjectOn(subunit))
    {
        int owner = GetOwner(subunit + 2), durate = GetDirection(subunit);

        if (CurrentHealth(owner))
        {
            if (durate)
            {
                FrameTimerWithArg(1, subunit, ThrowingPotionLoop);
                MoveObjectVector(subunit + 2, GetObjectZ(subunit), GetObjectZ(subunit + 1));
                
                int angle = GetDirection(subunit + 2);
                Raise(subunit + 2, MathSine(angle << 2, 240.0));
                LookWithAngle(subunit + 2, ++angle);
                LookWithAngle(subunit, --durate);
                break;
            }
            else
            {
                EndThrowingPotion(subunit + 2);
            }
            
        }
        Delete(subunit++);
        Delete(subunit++);
        Delete(subunit++);
        break;
    }
}

void ThrowingPotion(int caster, int target)
{
    float cdist = DistanceUnitToUnit(caster, target) / 45.0;
    float xvect = UnitRatioX(target, caster, cdist), yvect = UnitRatioY(target, caster, cdist);
    int subunit = CreateObjectAt("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster));

    Raise(CreateObjectAt("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster)), yvect);
    UnitNoCollide(CreateObjectAt("BottleCandleUnlit", GetObjectX(subunit), GetObjectY(subunit)));
    Raise(subunit, xvect);
    SetOwner(caster, subunit + 2);
    LookWithAngle(subunit, 45);
    FrameTimerWithArg(1, subunit, ThrowingPotionLoop);
}

void LaserCollide()
{
    if (GetTrigger())
    {
        int owner = GetOwner(SELF);

        if (CurrentHealth(OTHER))
        {
            if (CurrentHealth(owner) && IsAttackedBy(OTHER, owner))
            {
                MoveObject(SELF, GetObjectX(OTHER) - UnitAngleCos(OTHER, 1.0), GetObjectY(OTHER) - UnitAngleSin(OTHER, 1.0));
                Damage(OTHER, SELF, 100, 16);
                Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
                Delete(SELF);
            }
        }
    }
}

void LaserFlying(int subunit)
{
    while (IsObjectOn(subunit))
    {
        int owner = GetOwner(subunit);
        if (CurrentHealth(owner) && MaxHealth(subunit + 2))
        {
            int durate = GetDirection(subunit);
            if (IsVisibleTo(subunit, subunit + 1) && durate--)
            {
                FrameTimerWithArg(1, subunit, LaserFlying);
                LookWithAngle(subunit, durate);
                float xvect = GetObjectZ(subunit), yvect = GetObjectZ(subunit + 1);

                MoveObjectVector(subunit, xvect, yvect);
                MoveObjectVector(subunit + 1, xvect, yvect);
                MoveObjectVector(subunit + 2, xvect, yvect);
                Effect("SENTRY_RAY", GetObjectX(subunit), GetObjectY(subunit), GetObjectX(subunit + 1), GetObjectY(subunit + 1));
                break;
            }
        }
        Delete(subunit++);
        Delete(subunit++);
        Delete(subunit++);
        break;
    }
}

void ShotLaserBeam(int caster, int target)
{
    float xvect = UnitRatioX(target, caster, 13.0), yvect = UnitRatioY(target, caster, 13.0);
    int subunit = CreateObjectAt("ImaginaryCaster", GetObjectX(caster) - xvect, GetObjectY(caster) - yvect);
    
    Raise(CreateObjectAt("ImaginaryCaster", GetObjectX(caster) + xvect, GetObjectY(caster) + yvect), yvect);
    Raise(subunit, xvect);
    SetOwner(caster, DummyUnitCreate("StoneGolem", GetObjectX(subunit + 1), GetObjectY(subunit + 1)));
    SetCallback(subunit + 2, 9, LaserCollide);
    SetOwner(caster, subunit);
    LookWithAngle(subunit, 48);
    SetUnitFlags(subunit + 1, GetUnitFlags(subunit + 1) ^ 0x2000);
    FrameTimerWithArg(1, subunit, LaserFlying);
    MonsterForceCastSpell(caster, 0, GetObjectX(caster) + UnitRatioX(target, caster, 10.0), GetObjectY(caster) + UnitRatioY(target, caster, 10.0));
}

int MinusDegree(int degree)
{
    if (degree < 0)
        return 360 + degree;
    return degree;
}

void TripleShotOnCollide()
{
    while (GetTrigger())
    {
        int owner = GetOwner(SELF);

        if (CurrentHealth(owner))
        {
            if (IsAttackedBy(OTHER, owner))
                Damage(OTHER, owner, 100, DAMAGE_TYPE_BLADE);
        }
        Delete(SELF);
        break;
    }
}

void TripleShotTriggered(int caster)
{
    float xpos = GetObjectX(caster), ypos = GetObjectY(caster);
    int degree = MinusDegree(MathDirToDegree(GetDirection(caster)) - 36), u;

    for (u = 0 ; u < 9 ; Nop(++u))  //must be odd
    {
        int mis = CreateObjectAt("SpiderSpit", xpos + MathSine(degree + 90 + (u * 9), 17.0), ypos + MathSine(degree +(u * 9), 17.0));

        SetOwner(caster, mis);
        SetUnitCallbackOnCollide(mis, TripleShotOnCollide);
        LookAtObject(mis, caster);
        LookWithAngle(mis, GetDirection(mis) + 128);
        PushObject(mis, 15.0, xpos, ypos);
    }
}

void MecaDroneStrike(int caster, int target)
{
    LookAtObject(caster, target);
    TripleShotTriggered(caster);
}

void TrollThunderCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
            Damage(OTHER, owner, 200, DAMAGE_TYPE_BLADE);
    }
}

void TrollAfterThunder(int mark)
{
    int owner = GetOwner(mark);
    float xpos = GetObjectX(mark), ypos = GetObjectY(mark);

    Delete(mark);
    if (!CurrentHealth(owner))
        return;
    
    int subunit = DummyUnitCreate("Demon", xpos, ypos);

    SetUnitFlags(subunit, GetUnitFlags(subunit) ^ UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetOwner(owner, subunit);
    SetCallback(subunit, 9, TrollThunderCollide);
    DeleteObjectTimer(subunit, 1);

    Effect("LIGHTNING", xpos, ypos, xpos, ypos - 180.0);
    Effect("BLUE_SPARKS", xpos, ypos, 0.0, 0.0);
}

void TrollStrikePrivate(int caster, int target)
{
    int mark = CreateObjectAt("BlueSummons", GetObjectX(target), GetObjectY(target));

    SetOwner(caster, mark);
    FrameTimerWithArg(13, mark, TrollAfterThunder);
    Effect("CYAN_SPARKS", GetObjectX(mark), GetObjectY(mark), NULLPTR_F, NULLPTR_F);
}

void OnTrollStrike(int caster, int target)
{
    LookAtObject(caster, target);

    TrollStrikePrivate(caster, target);
}

void DrawGeometryRingAt(string orbName, float xProfile, float yProfile, int angleAdder)
{
    float speed = 2.3;
    int u = -1;

    while ((++u) < 60)
        LinearOrbMove(CreateObjectAt(orbName, xProfile, yProfile), MathSine((u * 6) + 90 + angleAdder, -12.0), MathSine((u * 6) + angleAdder, -12.0), speed, 10);
}

void OnStoneGiantStrike(int me, int you)
{
    RestoreHealth(me, 42);
    Damage(you, me, 100, DAMAGE_TYPE_FLAME);
    DrawGeometryRingAt("HealOrb", GetObjectX(you), GetObjectY(you), 0);
}

void OnHorrendousStrike(int me, int you)
{
    DrawGeometryRingAt("DrainManaOrb", GetObjectX(you), GetObjectY(you), 10);
    Damage(you, me, 50, DAMAGE_TYPE_IMPACT);
}

void StrikeFunctionProto(int functionId, int attacker, int victim)
{
    Bind(functionId, &functionId+4);
}

void MonsterStrikeCallback()
{
    if (!CurrentHealth(OTHER))
        return;

    int action = 0;

    if (HashGet(m_strikeFunctionHashInstance, GetUnitThingID(SELF), &action, FALSE))
        StrikeFunctionProto(action, GetTrigger(), GetCaller());
}

void InitStrikeFunctions()
{
    HashCreateInstance(&m_strikeFunctionHashInstance);

    HashPushback(m_strikeFunctionHashInstance, 1346, MeleeSoliderStrike);
    HashPushback(m_strikeFunctionHashInstance, 1385, ThrowingPotion);
    HashPushback(m_strikeFunctionHashInstance, 1332, ShotLaserBeam);
    HashPushback(m_strikeFunctionHashInstance, 1316, MecaDroneStrike);
    HashPushback(m_strikeFunctionHashInstance, 2273, OnTrollStrike);
    HashPushback(m_strikeFunctionHashInstance, 1324, OnStoneGiantStrike);
    HashPushback(m_strikeFunctionHashInstance, 1386, OnHorrendousStrike);
}

static int MonsterMeleeAttackRegistCallback()
{ 
    return MonsterStrikeCallback;
}

void OpenForestWallPrivate()
{
    int x=171, y=49;

    while(x >= 163)
        WallOpen(Wall(x--, y++));
}

void OpenForestWallBack()
{
    int x = 180, y = 54;

    while (x >= 171)
        WallOpen(Wall(x--, y++));
}

void ClearOldForestWall()
{
    int once;

    ObjectOff(SELF);
    if (once)
        return;

    int target = GetCaller();
    
    once = TRUE;
    OpenForestWallPrivate();

    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 250), target);
    LookAtObject(CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 251), target);
    UniPrint(OTHER, "전방에 있는 벽이 사라집니다~ !");
}

void PressOld2()
{
    int once;

    ObjectOff(SELF);
    if (once)
        return;
    
    once = TRUE;
    OpenForestWallBack();

    CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 252);
    CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 253);
    CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 254);
    CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 255);

    CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 256);
    CreateEnemyWithFunction(CreateEnemyLaserGunnerAtLocation, 257);

    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 358);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 359);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 360);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 361);
    CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, 362);

    int start = 363;
    while (start <= 380)
        CreateEnemyWithFunction(CreateEnemyGunnerAtLocation, start++);
}

void SummonedGunnerGoAttack(int *pInfo)
{
    if (CurrentHealth(pInfo[0]))
    {
        CreatureFollow(pInfo[0], pInfo[1]);
        AggressionLevel(pInfo[0], 1.0);
    }
}

void SummonGunners(int subunit)
{
    while (IsObjectOn(subunit))
    {
        int durate = GetDirection(subunit);

        if (durate)
        {
            int arr[] =
            {
                CreateEnemyGunnerAtLocation(258),
                subunit
            };
            FrameTimerWithArg(1, arr, SummonedGunnerGoAttack);
            LookWithAngle(subunit, --durate);
            FrameTimerWithArg(3, subunit, SummonGunners);
            break;
        }
        Delete(subunit);
        break;

    }
}

void OpenMainHallGate()
{
    int once;

    ObjectOff(SELF);
    if (once)
        return;

    once = TRUE;
    Part3GateUnlock();

    int target = CreateObjectAt("Leaves", GetObjectX(OTHER), GetObjectY(OTHER));
    FrameTimerWithArg(10, target, SummonGunners);
    Frozen(target, TRUE);
    LookWithAngle(target, 32);

    ObjectOn(Object("part3lev1"));

    ChangePlayerRespawnLocation(LocationX(258), LocationY(258));
}

void OpenDownForestWall()
{
    ObjectOff(SELF);

    int x = 172, y = 132;

    while (x >= 169)
        WallOpen(Wall(x--, y++));

    CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 259);
    CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 260);
    CreateEnemyWithFunction(CreateEnemyForestGunnerAtLocation, 261);

    CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 381);
    CreateEnemyWithFunction(CreateEnemyMecaDroneAtLocation, 382);
    UniPrintToAll("병영 외부 위병소 방향 폐쇄로가 개방되었습니다 - 6시방향");
}