
#include "demono_gvar.h"
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/wandpatch.h"
#include "libs/clientside.h"
#include "libs/objectIDdefines.h"
#include "libs/abilitydb.h"
#include "libs/sound_define.h"

void MyImageResource1()
{ }

void MyImageResourceDanger()
{ }

void MyImageResource2()
{ }

void MyImageResource3()
{ }

void MyImageResource4()
{ }

void GRPDumpNumberOutput(){}
//12
#define NUMBER_IMAGE_START_AT 113042
void initializeImageNumber(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpNumberOutput)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],NUMBER_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],NUMBER_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],NUMBER_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],NUMBER_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],NUMBER_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],NUMBER_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],NUMBER_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],NUMBER_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],NUMBER_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],NUMBER_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,0,NUMBER_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,1,NUMBER_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,2,NUMBER_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,3,NUMBER_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,4,NUMBER_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,5,NUMBER_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,6,NUMBER_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,7,NUMBER_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,8,NUMBER_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,9,NUMBER_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,10,NUMBER_IMAGE_START_AT+9);
}

void changeItemProperty(int obj, string name, string desc){
    ChangeSpriteItemNameAsString(obj, name);
    ChangeSpriteItemDescriptionAsString(obj, desc);
}

void ImageResourceInit(int imgVector)
{    
    AppendImageFrame(imgVector, GetScrCodeField(MyImageResource1) + 4, 17456);
    AppendImageFrame(imgVector, GetScrCodeField(MyImageResourceDanger) + 4, 17699);
    AppendImageFrame(imgVector, GetScrCodeField(MyImageResource2) + 4, 17457);
    AppendImageFrame(imgVector, GetScrCodeField(MyImageResource3) + 4, 17827);
    AppendImageFrame(imgVector, GetScrCodeField(MyImageResource4) + 4, 132048);
    AppendImageFrame(imgVector, GetScrCodeField(MyImageResource4) + 4, 132053);
    initializeImageNumber(imgVector);
}

void InitialChangeItemName(){
    changeItemProperty(OBJ_BRACELETOF_ACCURACY, "금고 열쇠", "잠긴 금고를 열 수 있는 금고열쇠입니다");
    changeItemProperty(OBJ_FEAR, "패스트 힐링", "잠시동안 회복속도 업!");
    changeItemProperty(OBJ_AMULETOF_MANIPULATION, "운석소나기 목걸이", "자기 자신 범위에 운석소나기를 시전한다");
    changeItemProperty(OBJ_BRACELETOF_HEALTH, "전기 팬던트", "쇼크 엔첸트를 걸어준다");
    changeItemProperty(OBJ_YELLOW_POTION, "황금 엘릭서", "체력을 125 만큼이나 회복시켜준다");
}

void PlayerClassCommonWhenEntry()
{
    int imgVector=CreateImageVector(1024);

    AppendAllDummyStaffsToWeaponList();
    ImageResourceInit(imgVector);
    InitialChangeItemName();
    ShowMessageBox("건틀렛", "어서와 이곳은 처음이지?");
    DoImageDataExchange(imgVector);
    AbilityDbSetCooldown(ABILITY_DB_TREAD_LIGHTLY_ID, WAR_SKILL_COOLDOWN_SHADOW_ATTACK);
    AbilityDbSetName(ABILITY_DB_TREAD_LIGHTLY_ID, "쉐도우 어택");
    AbilityDbSetExplanation(ABILITY_DB_TREAD_LIGHTLY_ID, "전방을 순식간에 가로지르며 적을 배어낸 후 되돌아 옵니다");
    AbilityDbChangeCastSound(ABILITY_DB_TREAD_LIGHTLY_ID, SOUND_MagicMissileCast);
}

