
#include "crazy_utils.h"
#include "crazy_resource.h"
#include "libs\printutil.h"
#include "libs\waypoint.h"
#include "libs\mathlab.h"
#include "libs\clientside.h"
#include "libs\network.h"
#include "libs/format.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/game_flags.h"
#include "libs/buff.h"
#include "libs/reaction.h"
#include "libs/thingdbServerside.h"

int player[20], m_length[10];
int count_bomb[10], STOP = 0; //, BLOCKS[360];

#define PLAYER_DEATH_FLAG 0x80000000

#define THING_DB_ID_REWARDMARKER    2672

void DoStartScan(int *pResult)
{
    if (pResult == NULLPTR)
        return;

    int firstunit = Object("mapstartscan");

    if (!firstunit)
    {
        pResult[0] = FALSE;
        return;
    }

    int lastunit = CreateObjectAt("RedPotion", 100.0, 100.0);

    Delete(lastunit);

    int startEndArr[] = {firstunit, lastunit};

    FrameTimerWithArg(1, &startEndArr, initSearchIndex);
    pResult[0] = TRUE;
}

void initializeSome()
{
    CreateRecoveryDestructorInstance(NULLPTR);

    ChangeThingDbCircle(OBJ_CARNIVOROUS_PLANT,34.0);
    ChangeThingDbCircle(OBJ_MAIDEN,24.0);
}

void MapInitialize()
{
    MusicEvent();
    
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    int pResult = FALSE;

    TeleportLocation(6, 100.0, 300.0);
    DoStartScan(&pResult);
    
    if (pResult)
    {
        FrameTimer(1, PlayerClassOnLoop);
        FrameTimer(120, startupMent);
        FrameTimer(3, initializeSome);
    }
}

void MapExit()
{
    MusicEvent();
}

void startupMent()
{
    UniPrintToAll("크레이지 아케이드 게임입니다. -HappySoft 팀");
    UniPrintToAll("제작자의 공간: blog.daum.net/ky10613");
    UniPrintToAll("폭탄 설치법: 클릭 하시면 그 자리에 폭탄이 생성됩니다.");
    UniPrintToAll("폭탄을 일정시간이 지나면 폭발합니다.");
    UniPrintToAll("폭탄은 스플래쉬를 가지고 있기 때문에 자신도 폭탄에 죽을 수 있습니다. 조심하세요.");
    UniPrintToAll("지금부터 제한시간 5 분이 주어집니다. 5 분내로 폭탄을 이용해 최대한 많은 적을 죽여보세요 +_+!");
    UniPrintToAll("자세한 정보를 확인하시려면 콘솔 키(F1) 를 눌러서 확인하세요.");
    FrameTimerWithArg(90, 300, countTimer);
}

void BnbCreateBreakableBox(float xpos, float ypos, int *pResult)
{
    int box = CreateObjectAt("MonsterGenerator", xpos, ypos);

    ObjectOff(box);
    Frozen(box, TRUE);
    if (pResult != NULLPTR)
        pResult[0] = box;
}

void ReplaceMarkerToBnb(int cunit, int *count)
{
    if (GetUnitThingID(cunit) != THING_DB_ID_REWARDMARKER)
        return;
    
    ++count[0];
    if (Random(0, 16))
        BnbCreateBreakableBox(GetObjectX(cunit), GetObjectY(cunit), NULLPTR);
    Delete(cunit);
}

void EndSearch(int *pcount)
{
    UniPrintToAll("endsearch::pcount[0]::" + IntToString(pcount[0]));
}

void initSearchIndex(int *startEndArr)
{
    int rep = 0, count;

    while ((rep++) < 50)
    {
        if (startEndArr[0] < startEndArr[1])
        {
            ReplaceMarkerToBnb(startEndArr[0], &count);

            ++startEndArr[0];
            ++startEndArr[0];
        }
        else
        {
            EndSearch(&count);
            return;
        }
        
    }
    FrameTimerWithArg(1, startEndArr, initSearchIndex);
}

int getTimer()
{
    int master;

    if (!master)
    {
        CreateObjectAtEx("Hecubah", 5500.0, 100.0, &master);
        Frozen(master, 1);
    }
    return master;
}

void countTimer(int time)
{
    char message[128];

    if (time > 0)
    {
        NoxSprintfString(message, "게임종료 까지 남은시간: %d초", &time, 1);
        UniChatMessage(getTimer(), ReadStringAddressEx(message), 40);
        SecondTimerWithArg(1, time - 1, countTimer);
        return;
    }
    UniPrintToAll("제한시간 경과__!! 게임을 종료하고 결과를 발표합니다!!");
    // removeAllBlocks();
    STOP = 1;
    teleportAllPlayers(0);
    FrameTimer(130, StrCheckWinner);
    FrameTimer(120, resultGame);
}

int PlayerClassCheckFlag(int plr, int flagone)
{
    return player[plr + 10] & flagone;
}

void PlayerClassSetFlag(int plr, int flagone)
{
    player[plr + 10] ^= flagone;
}

int getHighScorePlayer()
{
    int compare = -99, i, res = -1;

    for (i = 0 ; i < 10 ; i+=1)
    {
        if (CurrentHealth(player[i]))
        {
            if (GetScore(player[i]) > compare)
            {
                compare = GetScore(player[i]);
                res = i;
            }
        }
    }
    return res;
}

void setCrown(int pUnit)
{
    if (!CurrentHealth(pUnit))
        return;

    PlaySoundAround(pUnit, SOUND_BigGong);
    PlaySoundAround(pUnit, SOUND_LongBellsDown);
    UniPrintToAll("최고의 플레이어 에게는 승리의 왕관이 수여됩니다 !");
    UniChatMessage(pUnit, "우와앙!! 씐난다!! ^^", 150);
    Enchant(pUnit, "ENCHANT_CROWN", 0.0);
}

void resultGame()
{
    int plr = getHighScorePlayer();
    int pUnit = player[plr];
    char message[256];

    teleportAllPlayers(42);
    AudioEvent("HecubahDieFrame283", 42);
    AudioEvent("HecubahDieFrame439", 42);
    if (plr >= 0)
    {
        NoxSprintfOne(message, "오늘의 승리자는 이번게임에서 최고 점수를 획득하신 %s 님 입니다!! 축하합니다.", StringUtilGetScriptStringPtr(PlayerIngameNick(pUnit)));
        UniPrintToAll(ReadStringAddressEx(message));
        FrameTimerWithArg(90, pUnit, setCrown);
    }
    else
        UniPrintToAll("이번 게임에서는 최고 점수를 획득하신 플레이어가 없습니다 ㅠㅠ...");
}

void teleportAllPlayers(int wp)
{
    int i, inc;

    for (i = 0 ; i < 10 ; inc = i ++)
    {
        if (CurrentHealth(player[i]) > 0)
        {
            if (!wp)
            {
                SetUnitEnchantCopy(player[i], GetLShift(ENCHANT_FREEZE) | GetLShift(ENCHANT_INVULNERABLE));
                PlaySoundAround(player[i],SOUND_HecubahDieFrame0B);
                PlaySoundAround(player[i],SOUND_HecubahDieFrame0A);
            }
            else
                MoveObject(player[i], LocationX(wp), LocationY(wp));
        }
    }
}

// void removeAllBlocks()
// {
//     int rep = -1;

//     while ((++rep) < 360)
//     {
//         if (CurrentHealth(BLOCKS[rep]))
//             Delete(BLOCKS[rep]);
//     }
// }

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;
    ChangeGold(pUnit, -GetGold(pUnit));
    m_length[plr] = 2;
    count_bomb[plr] = 1;

    if (ValidPlayerCheck(pUnit))
    {
        if (pUnit ^ GetHost())
        {
            NetworkUtilClientEntry(pUnit);
        }
        else
        {
            InitializeMapResources();
        }
        FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);
    }

    return plr;
}

void PlayerClassOnJoin(int plr, int pUnit)
{
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    if (MaxHealth(player[plr]) == 150)
        Delete(GetLastItem(player[plr]));
    EnchantOff(player[plr], "ENCHANT_VILLAIN");
    Enchant(player[plr], "ENCHANT_FREEZE", 4.0);
    MoveObject(player[plr], LocationX(71), LocationY(71));
    UniChatMessage(player[plr], "입장 대기중입니다, 4 초만 기다리세요!", 120);
    AudioEvent("FlagRespawn", 71);
    FrameTimerWithArg(120, plr, respawnPlayer);
}

void playerEntry()
{
    int i;

    if (CurrentHealth(OTHER))
    {
        int plr = checkPlayer();
        for (i = 9 ; i >= 0 && plr < 0 ; Nop(i --))
        {
            if (!MaxHealth(player[i]))
            {
                plr = PlayerClassOnInit(i, GetCaller());
                break;
            }
        }
        if (plr >= 0)
        {
            PlayerClassOnJoin(plr, OTHER);
        }
        else
            playerIsFull();
    }
}

void respawnPlayer(int plr)
{
    if (CurrentHealth(player[plr]))
    {
        int locationId = Waypoint("playerWarpZone" + IntToString(Random(1, 10)));

        // TeleportLocation(22 + plr, LocationX(locationId), LocationY(locationId));

        Enchant(player[plr], "ENCHANT_ANTI_MAGIC", 0.0);
        Enchant(player[plr], "ENCHANT_INVULNERABLE", 0.0);
        Enchant(player[plr], "ENCHANT_VILLAIN", 0.0);
        MoveObject(player[plr], LocationX(locationId), LocationY(locationId));
        AudioEvent("BlindOff", locationId);
        DeleteObjectTimer(CreateObject("BlueRain", locationId), 7);
    }
}

void playerIsFull()
{
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    MoveObject(OTHER, LocationX(52), LocationY(52));
    UniPrintToAll("정원초과!- 현재 10명 모두 맵에 투입되어 있습니다.");
    UniPrintToAll("잠시만 기다리셨다가 다른 분 나가시면 입장을 시도하세요.");
}

void PlayerClassOnExit(int plr)
{
    EnchantOff(player[plr], "ENCHANT_VILLAIN");
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnDeath(int plr)
{ }

void boxCheckStorage(int **pGet)
{
    int arr[32];
    pGet[0]=arr;
}

void checkBoxAround()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (GetUnitClass(OTHER)&UNIT_CLASS_MONSTERGENERATOR)
        {
            int pIndex=GetPlayerIndex(SELF),*p;

            boxCheckStorage(&p);
            p[pIndex]=TRUE;
        }        
    }
}

int getPlayerScriptIndex(int pUnit)
{
    int r = 10;

    while (--r>=0)
    {
        if (pUnit == player[r])
            return r;
    }
    return -1;
}

void tryPutBomb(int pUnit)
{
    float xy[2];

    computeGridSnapCoor(GetObjectX(pUnit),GetObjectY(pUnit),xy);
    int *p, pIndex=GetPlayerIndex(pUnit);
    boxCheckStorage(&p);
    p[pIndex]=FALSE;
    SplashDamageAtEx(pUnit, xy[0],xy[1],11.0,checkBoxAround);
    if (p[pIndex])
    {
        UniPrint(pUnit, "위치선정 실패- 구석지가 아닌 곳에 다시 시도해 보세요");
        return;
    }
    putBomb(getPlayerScriptIndex(pUnit), xy[0],xy[1]);
}

void PlayerClassProc(int plr, int pUnit)
{
    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_VILLAIN)))
    {
        if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_INVULNERABLE)))
        {
            SetUnitEnchantCopy(pUnit,GetLShift(ENCHANT_INVULNERABLE));
            // float xy[2];

            // computeGridSnapCoor(GetObjectX(pUnit),GetObjectY(pUnit),xy);
            // putBomb(plr, xy[0],xy[1]); //LocationX(plr + 22), LocationY(plr + 22));
            tryPutBomb(pUnit);
        }
    }
    // if (Distance(GetObjectX(pUnit), GetObjectY(pUnit), LocationX(plr + 22), LocationY(plr + 22)) > 46.0)
    //     TeleportLocationVector(plr + 22, UnitWpRatioXY(pUnit, plr + 22, 0, 46.0), UnitWpRatioXY(pUnit, plr + 22, 1, 46.0));
    if (UnitCheckEnchant(pUnit,GetLShift(ENCHANT_AFRAID)))
    {
        int act=GetPlayerAction(pUnit);
        if (act==5 || act==0)
        {
            PushObjectTo(pUnit, UnitAngleCos(pUnit, -10.0),UnitAngleSin(pUnit,-10.0));
        }
    }
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; Nop(--i))
    {
        while (TRUE)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassProc(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(i, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(i, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    if (!STOP)
        FrameTimer(1, PlayerClassOnLoop);
}

void slowAll(int arg_0)
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (CurrentHealth(player[arg_0]) > 0 && i != arg_0)
        {
            Enchant(player[i], "ENCHANT_SLOWED", 5.0);
            UniChatMessage(player[i], "저주: 이속 저하.", 120);
        }
    }
}

void eyeOfTheMedusa(int arg_0)
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (IsVisibleTo(player[i], player[arg_0]) && CurrentHealth(player[arg_0]) > 0 && i != arg_0)
        {
            UniChatMessage(player[i], "마비가 되었습니다...", 90);
            Enchant(player[i], "ENCHANT_FREEZE", 3.0);
        }
    }
}

void putBomb(int plr, float x, float y)
{
    int wisp[4];

    if (count_bomb[plr] > 0)
    {
        --count_bomb[plr];
        int i, col;

        for (i = 3 ; i >= 0 ; Nop(i --))
        {
            wisp[i] = CreateObject("CarnivorousPlant", 3);
            
            SetUnitFlags(wisp[i], GetUnitFlags(wisp[i]) ^ 0x2000);
            if (LocationX(6) >= 5000.0)
                TeleportLocation(6, 100.0, 300.0);
            CreateObjectAtEx("InvisibleLightBlueLow", LocationX(6), LocationY(6), &col);
            UnitNoCollide(col);
            TeleportLocationVector(6, 10.0, 0.0);
            SetUnitMaxHealth(wisp[i], plr + 200);
            Frozen(wisp[i], TRUE);
            MoveObject(wisp[i], x,y);
            LookWithAngle(wisp[i], m_length[plr]);
            SetCallback(wisp[i], 9, touchedGenerator);
        }
        PlaySoundAround(wisp[0],SOUND_GlyphCast);
        FrameTimerWithArg(1, wisp[0], eastExplosion);
        FrameTimerWithArg(1, wisp[1], westExplosion);
        FrameTimerWithArg(1, wisp[2], northExplosion);
        FrameTimerWithArg(1, wisp[3], southExplosion);
        FrameTimerWithArg(30, wisp[0], EnableUnitCollide);
    }
}

void EnableUnitCollide(int ws)
{
    int *ptr = UnitToPtr(ws);

    if (ptr == NULLPTR)
        return;

    if (CurrentHealth(ws))
        SetUnitFlags(ws, GetUnitFlags(ws) ^ 0x2000);
}

int checkPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

void eastExplosion(int unit)
{
    int var_0 = GetDirection(unit), var_1;

    if (GetDirection(unit + 1) < 120)
    {
        LookWithAngle(unit + 1, GetDirection(unit + 1) + 1);
        FrameTimerWithArg(1, unit, eastExplosion);
    }
    else if (var_0 > 0)
    {
        LookWithAngle(unit, var_0 - 1);
        MoveWaypoint(2, GetObjectX(unit), GetObjectY(unit));
        var_1 = CreateObject("CarnivorousPlant", 2);
        LookWithAngle(var_1, CurrentHealth(unit));
        Frozen(var_1, 1);
        SetCallback(var_1, 9, deathTouched);
        DeleteObjectTimer(var_1, 1);
        Effect("EXPLOSION", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        MoveObject(unit, GetObjectX(unit) + 46.0, GetObjectY(unit) - 46.0);
        FrameTimerWithArg(1, unit, eastExplosion);
    }
    else
    {
        count_bomb[CurrentHealth(unit) - 200] ++;
        Delete(unit + 1);
        Delete(unit);
    }
}

void northExplosion(int unit)
{
    int var_0 = GetDirection(unit);
    int var_1;

    if (GetDirection(unit + 1) < 120)
    {
        LookWithAngle(unit + 1, GetDirection(unit + 1) + 1);
        FrameTimerWithArg(1, unit, northExplosion);
    }
    else if (var_0 > 0)
    {
        LookWithAngle(unit, var_0 - 1);
        MoveWaypoint(2, GetObjectX(unit), GetObjectY(unit));
        var_1 = CreateObject("CarnivorousPlant", 2);
        LookWithAngle(var_1, CurrentHealth(unit));
        Frozen(var_1, 1);
        SetCallback(var_1, 9, deathTouched);
        DeleteObjectTimer(var_1, 1);
        Effect("EXPLOSION", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        MoveObject(unit, GetObjectX(unit) - 46.0, GetObjectY(unit) - 46.0);
        FrameTimerWithArg(1, unit, northExplosion);
    }
    else
    {
        Delete(unit + 1);
        Delete(unit);
    }
}

void southExplosion(int unit)
{
    int time = GetDirection(unit), ptr;

    if (GetDirection(unit + 1) < 120)
    {
        LookWithAngle(unit + 1, GetDirection(unit + 1) + 1);
        FrameTimerWithArg(1, unit, southExplosion);
    }
    else if (time > 0)
    {
        LookWithAngle(unit, time - 1);
        ptr = CreateObjectAt("CarnivorousPlant", GetObjectX(unit), GetObjectY(unit));
        LookWithAngle(ptr, CurrentHealth(unit));
        Frozen(ptr, TRUE);
        SetCallback(ptr, 9, deathTouched);
        DeleteObjectTimer(ptr, 1);
        Effect("EXPLOSION", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        MoveObject(unit, GetObjectX(unit) - 46.0, GetObjectY(unit) + 46.0);
        FrameTimerWithArg(1, unit, southExplosion);
    }
    else
    {
        Delete(unit + 1);
        Delete(unit);
    }
}

void westExplosion(int unit)
{
    int time = GetDirection(unit), ptr;

    if (GetDirection(unit + 1) < 120)
    {
        LookWithAngle(unit + 1, GetDirection(unit + 1) + 1);
        FrameTimerWithArg(1, unit, westExplosion);
    }
    else if (time > 0)
    {
        LookWithAngle(unit, time - 1);
        MoveWaypoint(2, GetObjectX(unit), GetObjectY(unit));
        ptr = CreateObject("CarnivorousPlant", 2);
        LookWithAngle(ptr, CurrentHealth(unit));
        Frozen(ptr, 1);
        SetCallback(ptr, 9, deathTouched);
        DeleteObjectTimer(ptr, 1);
        Effect("EXPLOSION", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        MoveObject(unit, GetObjectX(unit) + 46.0, GetObjectY(unit) + 46.0);
        FrameTimerWithArg(1, unit, westExplosion);
    }
    else
    {
        Delete(unit + 1);
        Delete(unit);
    }
}

void deathTouched()
{
    int plr = GetDirection(SELF) - 200;

    if (CurrentHealth(OTHER))
    {
        if (IsBombUnit(OTHER))  // if (HasClass(OTHER, "MONSTER") && HasSubclass(OTHER, "LARGE_MONSTER"))
        {
            if (CheckBombDistance(SELF,OTHER))
                LookWithAngle(GetCaller() + 1, 120);
        }
        else if (GetUnitClass(OTHER)&UNIT_CLASS_MONSTERGENERATOR)
            return;
        
        if (IsItemUnit(OTHER))
        {
            RemoveItemUnit(GetCaller());
        }
        else if (IsPlayerUnit(OTHER))
        {
            if (UnitCheckEnchant(OTHER,GetLShift(ENCHANT_REFLECTIVE_SHIELD)))
            {
                Enchant(OTHER, EnchantList(ENCHANT_REFLECTIVE_SHIELD), 0.5);
                return;
            }
            EnchantOff(OTHER, "ENCHANT_INVULNERABLE");
            Damage(OTHER, player[plr], 255, DAMAGE_TYPE_PLASMA);
        }
    }
}

void itemPickupNoEffect()
{
    UniPrint(OTHER,"이 아이템은 주울 수 없음");
}

void callDestroyFunc(float x, float y)
{
    int pic = Random(1, 12), unit = 0;

    if (!GetDirection(OTHER))
    {
        LookWithAngle(OTHER, 1);
        if (pic < 8)
        {
            unit = CreateObjectAt(ItemTable(pic), x,y);
            CreateObjectAt("Maiden", x,y);
            LookWithAngle(unit, pic);
            Frozen(unit, TRUE);
            SetCallback(unit + 1, 9, GiveItemWhenTouched);
            SetUnitCallbackOnPickup(unit, itemPickupNoEffect);
            Frozen(unit + 1, TRUE);
        }
    }
}

void touchedGenerator() //here
{
    if (!CheckBombDistance(SELF,OTHER))
        return;

    if (HasClass(OTHER, "MONSTERGENERATOR") && GetDirection(SELF) && GetDirection(GetTrigger() + 1) >= 120)
    {
        PlaySoundAround(OTHER,SOUND_ElectricalArc1);
        PlaySoundAround(OTHER,SOUND_LightningBolt);
        LookWithAngle(SELF, 0);
        DeleteObjectTimer(OTHER, 3);
        callDestroyFunc(GetObjectX(OTHER),GetObjectY(OTHER));
    }
}

string ItemTable(int num)
{
    string table[] = {
        "None", "AmuletOfClarity", "YellowPotion", "ConjurerSpellBook", "BootsOfSpeed", "AmuletofManipulation",
        "AmuletofCombat", "AmuletofNature",
    };

    return table[num];
}

void GiveItemWhenTouched()
{
    int ptr = GetTrigger() - 1, idx = GetDirection(ptr);

    if (IsPlayerUnit(OTHER))
    {
        int plr = checkPlayer();

        if (plr >= 0)
        {
            if (!CurrentHealth(player[plr]))
                return;
            MoveWaypoint(8, GetObjectX(player[plr]), GetObjectY(player[plr]));
            AudioEvent("LongBellsUp", 8);
            CallFunctionWithArg(ItemEffectFunc(idx), plr);
        }
        Delete(SELF);
        Delete(ptr);
    }
}

int ItemEffectFunc(int actionType)
{
    int *fptr;

    if (fptr == NULLPTR)
    {
        int ftable[] = {Nop, BonusAddBomb, BonusIncreaseLength, BonusGodmode, BonusSpeedBoots, BonusMedusa, BonusSlowdown, Moonwalk,};

        fptr = ftable;
    }
    return fptr[actionType];
}

void BonusAddBomb(int plr)
{
    if (count_bomb[plr] < 6 && CurrentHealth(player[plr]))
    {
        Effect("BLUE_SPARKS", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
        UniPrint(player[plr], "폭탄의 최대개수가 1 증가 됩니다.");
        count_bomb[plr] ++;
    }
}

void BonusIncreaseLength(int plr)
{
    if (m_length[plr] < 7 && CurrentHealth(player[plr]))
    {
        Effect("VIOLET_SPARKS", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
        UniPrint(player[plr], "폭탄의 길이가 1 증가 됩니다.");
        m_length[plr] ++;
    }
}

void BonusGodmode(int plr)
{
    if (!HasEnchant(player[plr], "ENCHANT_REFLECTIVE_SHIELD") && CurrentHealth(player[plr]))
    {
        UniPrint(player[plr], "지금부터 9 초동안 무적상태가 됩니다.");
        Enchant(player[plr], "ENCHANT_REFLECTIVE_SHIELD", 9.0);
    }
}

void BonusSpeedBoots(int plr)
{
    if (!HasEnchant(player[plr], "ENCHANT_HASTED") && CurrentHealth(player[plr]))
    {
        UniPrint(player[plr], "10 초 동안 이동속도가 향상됩니다.");
        Enchant(player[plr], "ENCHANT_HASTED", 10.0);
    }
}

void BonusMedusa(int plr)
{
    if (CurrentHealth(player[plr]))
    {
        UniPrint(player[plr], "당신이 화면에 보이는 플레이어들은 마비가 됩니다.");
        eyeOfTheMedusa(plr);
    }
}

void Moonwalk(int plr)
{
    int pUnit=player[plr];

    if (CurrentHealth(pUnit))
    {
        UniPrint(pUnit, "잠시동안 반대로 걷습니다");
        Enchant(pUnit, "ENCHANT_AFRAID", 8.0);
    }
}

void BonusSlowdown(int plr)
{
    if (CurrentHealth(player[plr]))
    {
        UniPrint(player[plr], "자신을 제외한 모든 플레이어의 이동속도가 감소됩니다.");
        slowAll(plr);
    }
}

float UnitWpRatioXY(int unit, int wp, int mode, float size)
{
    if (!mode)
        return (GetObjectX(unit) - LocationX(wp)) * size / Distance(GetObjectX(unit), 0.0, LocationX(wp), 0.0);
    else
        return (GetObjectY(unit) - LocationY(wp)) * size / Distance(0.0, GetObjectY(unit), 0.0, LocationY(wp));
}

void DrawStampStringProc(int singleset, string unitname, int *count, int *stampinfo, float *vectinfo)
{
    int totalcount = stampinfo[0], locationId = stampinfo[1];
    int prgress = stampinfo[2];
    int i;

    for (i = 1 ; i > 0 && count[0] < totalcount ; i <<= 1)
    {
        if (i & singleset)
            CreateObjectAtEx(unitname, LocationX(locationId), LocationY(locationId), NULLPTR);
		if ((count[0]++) % prgress == prgress - 1)
            TeleportLocationVector(locationId, -vectinfo[0], vectinfo[1]);
		else
            TeleportLocationVector(locationId, vectinfo[2], 0.0);
    }
}

void DrawStampString(string unitname, int *pMatrix, int matrixSize, int *stampinfo, float *vectinfo)
{
    int locationId = stampinfo[1];
    float xpos = LocationX(locationId), ypos = LocationY(locationId);
    int rep = 0, count = 0;

    while (rep < matrixSize)
        DrawStampStringProc(pMatrix[rep++], unitname, &count, stampinfo, vectinfo);
    TeleportLocation(locationId, xpos, ypos);
}

void StrCheckWinner()
{
    int locationId = 37;
    int matrx[] = {
        1010811448, 1405614084, 9373936, 303104016, 579864832, 1606714440, 9472000, 554992126, 34612258, 
        296000, 151561216, 135299361, 2145395650, 71845372, 138447375, 268435712, 606224936, 4326796,
        546304008, 18944529, 536940577, 1157906432, 67700880, 33489025, 1109926908, 1174161476, 268960255,
        299982592, 2097724, 2139111424, 8391168, 16
    };
    int info[] = {992, locationId, 88};
    float vectarr[] = {174.0, 2.0, 2.0};

	DrawStampString("CharmOrb", &matrx, 28, &info, &vectarr);
}

static void NetworkUtilClientMain()
{
    InitializeMapResources();
}

void PlayerClassFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        int plr = checkPlayer();
        int destination = 73;

        if (plr >= 0)
            destination = 72;

        MoveObject(OTHER, LocationX(destination), LocationY(destination));
    }
}
