






#include "fov_popup.h"
#include "fov_player.h"
#include "fov_specialweapon.h"
// #include "fov_mon.h"
#include "libs/fixtellstory.h"
#include "libs/networkRev.h"
#include "libs/voiceList.h"

void onPopupMessageChanged(int messageId)
{
    int ctx,*pMsg;
    QueryDialogCtx(&ctx,NULLPTR);
    queryPopMessage(&pMsg);
    string message = pMsg[messageId];
    GUISetWindowScrollListboxText(ctx, message, NULLPTR);
}

void deferredChangeDialogContext(int *pParams) //override
{
    int user=pParams[0];
    int message=pParams[1];

    FreeSmartMemEx(pParams);
    if (!CurrentHealth(user))
        return;

    if (user==GetHost())
    {
        onPopupMessageChanged(message);
        return;
    }
    ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, message);
}

void sendChangeDialogCtx(int user, int param)
{
    int params[]={
        user,
        param,
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
}
int placingItemShop(short location, int descFn, int tradeFn){
    int s=DummyUnitCreateById(OBJ_URCHIN_SHAMAN, LocationX(location),LocationY(location));
    SetDialog(s,"YESNO",descFn,tradeFn);
    StoryPic(s,"WarriorPic");
    LookWithAngle(s,192);
    return s;
}

int SetInvulnerabilityItem(int unit)
{
    int count = 0, inv = GetLastItem(unit);

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void tradeInvincibleItem()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=5000)
    {
        ChangeGold(OTHER, -5000);
        int count = SetInvulnerabilityItem(OTHER);
        char buff[128];

        NoxSprintfString(buff, "%d개 아이템이 처리되었습니다", &count,1);
        UniPrint(OTHER,ReadStringAddressEx(buff));
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descInvincibleItem()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "인벤토리 무적화");
}

void descAwardWindboost(){
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_MESSAGE_SKILL_CRITICAL_HIT);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "조심스럽게\n걷기 강화");
}

void tradeAwardWindboost()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=20000)
    {
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WINDBOOST))
        {
            UniPrint(OTHER, "이미 그 능력을 가졌습니다!");
            return;
        }
        ChangeGold(OTHER, -20000);
        PlayerClassSetFlag(pIndex,PLAYER_FLAG_WINDBOOST);
        GreenExplosion(GetObjectX(OTHER),GetObjectY(OTHER));
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

// void descAwardBerserker(){
//     sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_AWARD_BERSERKER);
//     TellStoryUnitName("null", "MainBG.wnd:Loading", "버저커차지\n배우기");
// }
// void tradeAwardBerser(){
//     if (GetAnswer(SELF)^1)
//         return;

//     if (GetGold(OTHER)>=20000){
//         int pIndex=GetPlayerIndex(OTHER);
//         if (pIndex<0)
//             return;
//         if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_BERSERKER_CHARGE))        {
//             UniPrint(OTHER, "이미 그 능력을 가졌습니다!");
//             return;        }
//         ChangeGold(OTHER, -20000);
//         PlayerClassSetFlag(pIndex,PLAYER_FLAG_BERSERKER_CHARGE);
//         GreenExplosion(GetObjectX(OTHER),GetObjectY(OTHER));
//         return;    }
//     UniPrint(OTHER, "골드가 부족합니다");
// }

// void descAwardWarcry(){
//     sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_AWARD_WARCRY);
//     TellStoryUnitName("null", "MainBG.wnd:Loading", "전사의함성\n배우기");
// }
// void tradeAwardWarcry(){
//     if (GetAnswer(SELF)^1)
//         return;

//     if (GetGold(OTHER)>=20000){
//         int pIndex=GetPlayerIndex(OTHER);
//         if (pIndex<0)
//             return;
//         if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WARCRY))        {
//             UniPrint(OTHER, "이미 그 능력을 가졌습니다!");
//             return;        }
//         ChangeGold(OTHER, -20000);
//         PlayerClassSetFlag(pIndex,PLAYER_FLAG_WARCRY);
//         GreenExplosion(GetObjectX(OTHER),GetObjectY(OTHER));
//         return;    }
//     UniPrint(OTHER, "골드가 부족합니다");
// }

void desTripleArrowAxe(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_WEAPON_ARROW_AXE);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "트리플화살 손도끼");
}void tradeTripleArrowAxe(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=20000){
        ChangeGold(OTHER, -20000);
        CreateTripleArrowAxe(GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER,"무기를 구입하였고, 당신 아래에 무기가 있습니다");
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descFONAxe(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_WEAPON_FON_AXE);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "포스오브네이쳐\n손도끼");
}void tradeFONAxe(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=45000){
        ChangeGold(OTHER, -45000);
        CreateFONAxe(GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER,"무기를 구입하였고, 당신 아래에 무기가 있습니다");
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descMoneyExchanger(){
sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_MONEY_EXCHANGER);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "코인교환기");
}
int SellGerm(int inv)
{
    int thingId = GetUnitThingID(inv), pic;

    if (thingId >= OBJ_DIAMOND && thingId <= OBJ_RUBY)
    {
        Delete(inv);

        int pay[] = {1000, 5000, 10000};
        return pay[OBJ_RUBY - thingId];
    }
    else
        return 0;
}

int FindItemGerm(int holder)
{
    int inv = GetLastItem(holder), res = 0, nextInv;

    while (inv)
    {
        nextInv = GetPreviousItem(inv);
        res += SellGerm(inv);
        inv = nextInv;
    }
    return res;
}
void tradeMoneyExchanger(){
    if (GetAnswer(SELF)^1)
        return;
    int trdRes = FindItemGerm(OTHER);
    char msg[128];

    if (trdRes)
    {
        ChangeGold(OTHER, trdRes);
        NoxSprintfString(msg, "가지고 있던 모든 보석을 팔아서 %d골드를 추가했습니다", &trdRes, 1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
    else
        UniPrint(OTHER, "당신은 보석을 하나도 가지고 있지 않아요. 거래를 계속할 수 없습니다");
}
int placingMoneyExchanger(short location){
    int s=DummyUnitCreateById(OBJ_URCHIN_SHAMAN, LocationX(location),LocationY(location));
    SetDialog(s,"YESNO",descMoneyExchanger,tradeMoneyExchanger);
    StoryPic(s,"WarriorPic");
    LookWithAngle(s,160);
}
void descThunderBoltSword(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_WEAPON_THUNDER_SWORD);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "백만벌트서드");
}void tradeThunderboltsword(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=25000){
        ChangeGold(OTHER, -25000);
        CreateThunderSword(GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER,"무기를 구입하였고, 당신 아래에 무기가 있습니다");
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}

//Creatures//

void onBodyGuardLoopInterval(int *d){
    int cre=d[0];

    if (CurrentHealth(cre))
    {
        int owner=GetOwner(cre);
        if (MaxHealth(owner)){
            PushTimerQueue(13,d,onBodyGuardLoopInterval);
            if (!IsVisibleOr(cre,owner))
            {
                MoveObject(cre,GetObjectX(owner)+UnitAngleCos(owner, 13.0),GetObjectY(owner)+UnitAngleSin(owner,13.0));
                AggressionLevel(cre,1.0);
                CreatureFollow(cre,owner);
            }
            return;
        }
        Delete(cre);
    }
    d[0]=0;
}

void onBodyGuardDeath(){
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    Delete(SELF);
    DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION,x,y), 12);
}
void onBodyGuardHurt(){
    Enchant(SELF,EnchantList(ENCHANT_INVULNERABLE),3.0);
    RestoreHealth(SELF, 40);
}

void SummonBodyGuard(int owner, int *credata, int spawnFn){
    float xy[]={GetObjectX(owner)+UnitAngleCos(owner, 23.0),GetObjectY(owner)+UnitAngleSin(owner, 23.0)};
    int *arg=xy;
    int s= Bind(spawnFn,arg);

    credata[0]=s;
    SetOwner(owner,s);
    AttachHealthbar(s);
    SetUnitFlags(s,GetUnitFlags(s)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(s,5,onBodyGuardDeath);
    SetCallback(s,7,onBodyGuardHurt);
    RetreatLevel(s, 0.0);
    PushTimerQueue(1,credata,onBodyGuardLoopInterval);
}


int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 1500; arr[19] = 98; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1110704128; arr[29] = 130; arr[31] = 4; 
		arr[32] = 3; arr[33] = 3; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077684469;		ptr[137] = 1077684469;
	int *hpTable = ptr[139];
	hpTable[0] = 1500;	hpTable[1] = 1500;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createCreatureGoon(float x, float y)
{
    int m=CreateObjectById(OBJ_GOON, x,y);

    GoonSubProcess(m);
    SetUnitMass(m, 1000.0);
    SetUnitVoice(m,MONSTER_VOICE__MaleNPC1);
    return m;
}

void onHosungLeeAttackSplash(){
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 200, DAMAGE_TYPE_PLASMA);
        }
    }
}

void onHosungLeeAttack(){
    if (!GetCaller()) return;

    float x=GetObjectX(OTHER),y=GetObjectY(OTHER);

    SplashDamageAtEx(SELF, x,y,60.0, onHosungLeeAttackSplash );
    DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE, x,y), 9);
    PlaySoundAround(SELF,SOUND_CrushHard);
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 1800; arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; 
		arr[26] = 4; arr[28] = 1112014848; arr[29] = 10; arr[30] = ToInt(150.0);
        arr[31] = 4; 
		arr[32] = 19; arr[33] = 27; //min , max delay
        arr[59] = 5542784; 
	pArr = arr;
    CustomMeleeAttackCode(arr,onHosungLeeAttack);
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 1800;	hpTable[1] = 1800;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int createHosungLee(float x,float y)
{
    int m=CreateObjectById(OBJ_WOUNDED_APPRENTICE,x,y);
    WoundedApprenticeSubProcess(m);
    SetUnitVoice(m,MONSTER_VOICE__FireKnight2);
    return m;
}

void onRunningOniAttack(){
    if (!GetCaller()) return;

    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(SELF, GetLShift(ENCHANT_SHOCK));
        float x=GetObjectX(OTHER),y=GetObjectY(OTHER);

        if (!ToInt(x))
        {
            UniPrintToAll("ERROR x");
            return;
        }
        if (!ToInt(y))
        {
            UniPrintToAll("ERROR y");
            return;
        }

        x-=UnitAngleCos(OTHER,7.0);
        y-=UnitAngleSin(OTHER,7.0);
        Effect("CHARM", GetObjectX(SELF),GetObjectY(SELF),x,y);
        MoveObject(SELF, x,y);
        LookAtObject(SELF,OTHER);
        AggressionLevel(SELF, 1.0);
    }
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 2000; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1118175232; arr[29] = 10; arr[32] = 10; arr[33] = 18; arr[59] = 5542784; 
		arr[60] = 1388; arr[61] = 46915072; 
	pArr = arr;
    CustomMeleeAttackCode(arr, onRunningOniAttack);
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 2000;	hpTable[1] = 2000;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onRunnerOniCollide(){
    if (CurrentHealth(OTHER)){
        if (IsAttackedBy(OTHER,SELF)){
            if (UnitCheckEnchant(SELF,GetLShift(ENCHANT_SHOCK)))
                Damage(OTHER,SELF,200,DAMAGE_TYPE_ELECTRIC);
        }
    }
}

int createRunnerOni(float x,float y){
    int m=CreateObjectById(OBJ_WEIRDLING_BEAST,x,y);
    WeirdlingBeastSubProcess(m);
    SetUnitVoice(m,MONSTER_VOICE_Necromancer);
    SetUnitMass(m, 1200.0);
    SetUnitFlags(m, GetUnitFlags(m)^UNIT_FLAG_SHORT);
    SetCallback(m,9,onRunnerOniCollide);
    return m;
}


int placingCreatShop(short monId, short location, int descFn, int tradeFn){
    int s=CreateObjectById(monId, LocationX(location),LocationY(location));

    Frozen(s,TRUE);
    SetOwner(GetHost(),s);
    SetDialog(s,"YESNO",descFn,tradeFn);
    StoryPic(s,"WarriorPic");
    LookWithAngle(s,160);
    return s;
}

void descArmy(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_CREATURE_ARMY);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "육군병사 구입");
}
void tradeArmy(){
    if (GetAnswer(SELF)^1)
        return;
    if (GetGold(OTHER)>=40000){
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        int data[MAX_PLAYER_COUNT];
        
        if (data[pIndex])        {
            UniPrint(OTHER, "이미 육군병사를 소유중입니다");
            return;        }
        ChangeGold(OTHER, -40000);
        SummonBodyGuard(GetCaller(),&data[pIndex],createCreatureGoon);
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descHosungLee(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_CREATURE_HOSUNGLEE);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "이호성 구입");
}
void tradeHosungLee(){
    if (GetAnswer(SELF)^1)
        return;
    if (GetGold(OTHER)>=60000){
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        int data[MAX_PLAYER_COUNT];
        
        if (data[pIndex])        {
            UniPrint(OTHER, "이미 이호성를 소유중입니다");
            return;        }
        ChangeGold(OTHER, -60000);
        SummonBodyGuard(GetCaller(),&data[pIndex],createHosungLee);
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descRunnerOni(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_CREATURE_RUNNER_ONI);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "러너오니");
}
void tradeRunnerOni(){
    if (GetAnswer(SELF)^1)
        return;
    if (GetGold(OTHER)>=40000){
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        int data[MAX_PLAYER_COUNT];
        
        if (data[pIndex])        {
            UniPrint(OTHER, "이미 러너오니를 소유중입니다");
            return;        }
        ChangeGold(OTHER, -40000);
        SummonBodyGuard(GetCaller(),&data[pIndex],createRunnerOni);
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void InitializeShopSystem(){
    placingItemShop(260,descInvincibleItem,tradeInvincibleItem);
    placingItemShop(215,descAwardWindboost,tradeAwardWindboost);
    placingItemShop(265,descFONAxe,tradeFONAxe);
    // placingItemShop(75,descAwardWarcry,tradeAwardWarcry);
    placingItemShop(227,desTripleArrowAxe,tradeTripleArrowAxe);
    placingItemShop(214,descThunderBoltSword,tradeThunderboltsword);
    // placingItemShop(72,descBackstepHammer,tradeBackstepHammer);
    // placingItemShop(69,descAllEnchantment,tradeAllEnchant);
    // placingItemShop(74,speedShopDesc,speedShopTrade);
    placingCreatShop(OBJ_GOON,261,descArmy,tradeArmy);
    placingCreatShop(OBJ_WOUNDED_APPRENTICE,263,descHosungLee,tradeHosungLee);
    placingCreatShop(OBJ_WEIRDLING_BEAST,264,descRunnerOni,tradeRunnerOni);
    // placingCreatShop(OBJ_HECUBAH,134,descBlueGiant,tradeBlueGiant);
    // placingCreatShop(OBJ_EVIL_CHERUB,136,descStelsShip,tradeStelsShip);
    placingMoneyExchanger(213);
    InitializePopupMessages();
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[1])
    {
        onPopupMessageChanged(type[1]);
        type[1]=0;
    }
    if (type[0])
    {
        type[1]=type[0];
        type[0]=0;
    }
    FrameTimer(1, ClientProcLoop);
}

