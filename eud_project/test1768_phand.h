
#include "test1768_sub.h"
#include "libs/playerupdate.h"
#include "libs/buff.h"

void OnUserInitialized(int user) //virtual
{ }

int NetClassUserPtrTable()
{
    int userArr[32];

    return userArr;
}

int GetPlayerCount()
{
    int ptr = GetMemory(0x97ec60);

    if (ptr)
        return GetMemory(ptr + 84) + 1;
    return 0;
}

int NetClassUserCountCheck(int userTablePtr)
{
    int userCount, realCount = GetPlayerCount(), prevCount;

    if ((realCount ^ userCount) && realCount)
    {
        prevCount = userCount;
        userCount = realCount;
        return ABS(prevCount - realCount);
    }
    return 0;
}

int NetClassFlags(int pIndex, int setValue)
{
    int flags[32];

    if (setValue)
    {
        if (setValue & (1 << 0x1c))
            flags[pIndex] = 0;
        else
            flags[pIndex] = setValue;
    }
    return flags[pIndex];
}

int NetClassObserverFlagCheck(int pIndex)
{
    return NetClassFlags(pIndex, 0) & 0x02;
}

void NetClassObserverFlagSet(int pIndex)
{
    NetClassFlags(pIndex, NetClassFlags(pIndex, 0) ^ 0x02);
}

int NetClassDuelFlagCheck(int pIndex)
{
    return NetClassFlags(pIndex, 0) & 0x04;
}

void NetClassDuelFlagSet(int pIndex)
{
    NetClassFlags(pIndex, NetClassFlags(pIndex, 0) ^ 0x04);
}

int NetClassCamUserFlagCheck(int pIndex)
{
    return NetClassFlags(pIndex, 0) & 0x08;
}

void NetClassCamUserFlagSet(int pIndex)
{
    NetClassFlags(pIndex, NetClassFlags(pIndex, 0) ^ 0x08);
}

int NetClassDeathFlagCheck(int pIndex)
{
    return NetClassFlags(pIndex, 0) & 0x80;
}

void NetClassDeathFlagSet(int pIndex)
{
    NetClassFlags(pIndex, NetClassFlags(pIndex, 0) ^ 0x80);
}

void initialUserLoop(int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_INVULNERABLE)))
        {
            PushTimerQueue(3, pUnit, initialUserLoop);
            return;
        }
        EnchantOff(pUnit, "ENCHANT_INVULNERABLE");
        EnchantOff(pUnit, "ENCHANT_HELD");
        EnchantOff(pUnit, "ENCHANT_BLINDED");
        OnUserInitialized(pUnit);
    }
}

void NetClassOnInit(int plrUnit, int pIndex)
{
    NetClassFlags(pIndex, 1);
    // if (plrUnit ^ GetHost())
    //     NetworkUtilClientEntry(plrUnit);
    // else
    //     PlayerClassCommonWhenEntry();
    // SetUnitEnchantCopy(plrUnit, GetLShift(ENCHANT_BLINDED) | GetLShift(ENCHANT_HELD) | GetLShift(ENCHANT_INVULNERABLE));
    // PushTimerQueue(1, plrUnit, initialUserLoop);
    // DiePlayerHandlerEntry(plrUnit);
    // SelfDamageClassEntry(plrUnit);
    PrintMessageFormatOne(PRINT_ALL, "%s 님께서 지도에 입장하셨어요", StringUtilGetScriptStringPtr(PlayerIngameNick(plrUnit)));
    // UniPrint(plrUnit, "계속하려면 클릭하세요");
}

void NetClassOnExit(int pIndex)
{
    NetClassFlags(pIndex, 1 << 0x1c);
}

int NetClassAssign(int *destPtr, int *plrPtr, int pIndex)
{
    int unitId;

    if (plrPtr)
    {
        unitId = plrPtr[11];
        if (destPtr[0] ^ unitId)
        {
            NetClassOnInit(unitId, pIndex);
            destPtr[0]=unitId;
            return 1;
        }
    }
    else if (destPtr[0])
    {
        NetClassOnExit(pIndex);
        destPtr[0]=0;
        return 1;
    }
    return 0;
}

// int NetClassMapDownloading(int pIndex, int plrPtr)
// {
//     if (pIndex ^ 31)
//     {
//         if ((GetMemory(plrPtr + 128) & 0xff) || (GetMemory(plrPtr + 0x648) == 0xdeadface))
//             return 1;
//         return (GetMemory(0x81b260 + (pIndex * 0x30)) >> 0x10);
//     }
//     return 0;
// }

void NetClassMakeTable(int *userTablePtr)
{
    int *plrPtr = 0x62f9e0, i;

    for (i = 0 ; i < 32 ; i ++)
    {
        // if (!NetClassMapDownloading(i, plrPtr))
        NetClassAssign(&userTablePtr[i], plrPtr[0], i);
        plrPtr += 0x12dc;
    }
}

void NetClassOnDeath(int plrUnit, int pIndex)
{
    PrintMessageFormatOne(PRINT_ALL, "%s 님께서 적에게 격추되었습니다", StringUtilGetScriptStringPtr(PlayerIngameNick(plrUnit)));
}

void NetClassOnRespawn(int plrUnit, int pIndex)
{
    return;
}

void NetClassOnGoObserver(int plrUnit, int pIndex)
{
    if (NetClassDeathFlagCheck(pIndex))
    {
        NetClassDeathFlagSet(pIndex);
    }
}

void NetClassOnAlive(int plrUnit, int pIndex)
{
    if (MaxHealth(plrUnit))
    {
        if (GetUnitFlags(plrUnit) & 0x40)
        {
            if (!NetClassObserverFlagCheck(pIndex))
            {
                NetClassObserverFlagSet(pIndex);
                NetClassOnGoObserver(plrUnit, pIndex);
            }
        }
        else if (CurrentHealth(plrUnit))
        {
            if (NetClassDeathFlagCheck(pIndex))
            {
                NetClassDeathFlagSet(pIndex);
                NetClassOnRespawn(plrUnit, pIndex);
            }
        }
        else
        {
            if (NetClassDeathFlagCheck(pIndex)) 1;
            else
            {
                NetClassDeathFlagSet(pIndex);
                NetClassOnDeath(plrUnit, pIndex);
            }
        }
    }
}

void NetClassUserHandle(int *userTablePtr)
{
    int i = 32;

    while (--i>=0)
    {
        if (userTablePtr[i])
            NetClassOnAlive(userTablePtr[i], i);
    }
}

void NetClassLoop(int userTablePtr)
{
    NetClassMakeTable(userTablePtr);
    NetClassUserHandle(userTablePtr);
    PushTimerQueue(1, userTablePtr, NetClassLoop);
}

void LoopRun()
{
    PushTimerQueue(1, NetClassUserPtrTable(), NetClassLoop);
}
