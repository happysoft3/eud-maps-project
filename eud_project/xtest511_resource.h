
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"

void DungeonNameLeft() {}
void DungeonNameRight() {}


void BossHereImage() {}
void DungeonPicketImage() {}
void ProtossNexusImage() {}
void AnimateCent1(){}
void AnimateCent2(){}
void AnimateCent3(){}
void AnimateCent4(){}
void AnimateCent5(){}
void AnimateCent6(){}
void ResourcePortraitSpare()
{}

void MyImagePUOTDR01()
{ }
void ImageAmulet(){}
void ImagePortrait1(){}
void ImagePortrait2(){}

void MoneyImage() {}


void GRPDump_cent1_output(){}
void initializeCentAnimation(int imgVec)
{
    int *frames, *sizes, thingId=OBJ_TREASURE_CHEST;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_cent1_output)+4, thingId, xyInc, &frames, &sizes);
    AppendImageFrame(imgVec,frames[0], 112596);
    AppendImageFrame(imgVec,frames[0], 112597);
    AppendImageFrame(imgVec,frames[1], 112598);
    AppendImageFrame(imgVec,frames[2], 112599);
    AppendImageFrame(imgVec,frames[3], 112600);
    AppendImageFrame(imgVec,frames[4], 112601);
    AppendImageFrame(imgVec,frames[5], 112602);
    AppendImageFrame(imgVec,frames[0], 112603);
}

void GRPDump_RedmistOutput(){}
#define REDMIST_OUTPUT_BASE_IMAGE_ID 133955
void initializeImageFrameRedmistOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_RedmistOutput)+4, thingId, xyinc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1, USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_MaidenGhostOutput(){}
#define MAIDENGHOST_OUTPUT_BASE_IMAGE_ID 114546
void initializeImageFrameMaidenGhost(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GHOST;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MaidenGhostOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign8Directions(10, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign8Directions(15, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign8Directions(20, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);
    AnimFrameAssign8Directions(25, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_IDLE, 5);
    AnimFrameAssign8Directions(30, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_IDLE, 6);
    AnimFrameAssign8Directions(35, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_IDLE, 7);

    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
}

void InitializeResources()
{
    int vec=CreateImageVector(2048);

    initializeCentAnimation(vec);
    initializeImageFrameRedmistOutput(vec);
    initializeImageFrameMaidenGhost(vec);
    initializeImageFrameSoldier(vec);
    AppendImageFrame(vec, GetScrCodeField(ResourcePortraitSpare) + 4, 14822);
    AppendImageFrame(vec, GetScrCodeField(MyImagePUOTDR01) + 4, 132780);
    AppendImageFrame(vec, GetScrCodeField(ImageAmulet) + 4, 112960);
    AppendImageFrame(vec, GetScrCodeField(ImagePortrait1) + 4, 14922);
    AppendImageFrame(vec, GetScrCodeField(ImagePortrait2) + 4, 14983);

    AppendImageFrame(vec, GetScrCodeField(DungeonPicketImage) + 4, 17698);     //MovableSign1
    AppendImageFrame(vec, GetScrCodeField(ProtossNexusImage) + 4, 17464);      //MovableStatue2a
    AppendImageFrame(vec, GetScrCodeField(DungeonNameLeft) + 4, 17515);        //MovableStatueVictory1E
    AppendImageFrame(vec, GetScrCodeField(DungeonNameRight) + 4, 17513);       //MovableStatueVictory1N
    AppendImageFrame(vec, GetScrCodeField(BossHereImage) + 4, 17514);          //MovableStatueVictory1NE
    AppendImageFrame(vec, GetScrCodeField(MoneyImage) + 4, 14411);             //Gold
    DoImageDataExchange(vec);
}
