
#include "ak250624_main.h"

void MapInitialize(){
    OnInitializeMap();
}
void MapExit(){
    OnShutdownMap();
}

int m_player[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex];}

static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user;}

int m_playerFlags[MAX_PLAYER_COUNT];

static int GetPlayerFlags(int pIndex) //virtual
{ return m_playerFlags[pIndex];}

static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_playerFlags[pIndex]=flags;}

int m_userCamera[MAX_PLAYER_COUNT];
static int GetUserCamera(int pIndex){return m_userCamera[pIndex];}//virtual
static void SetUserCamera(int pIndex, int cam){m_userCamera[pIndex]=cam;}//virtual

static void NetworkUtilClientMain(){ InitializeResources(); }
static void InitServerResource(){InitializeResources();}//virtual

int m_yours[MAX_PLAYER_COUNT];

static int GetYOU(int pIndex){return m_yours[pIndex];}//virtual
static void SetYOU(int pIndex,int unit){m_yours[pIndex]=unit;}//virtual

