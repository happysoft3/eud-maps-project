
#include "r240503_shop.h"
#include "r240503_player.h"
#include "r240503_mon.h"
#include "r240503_initscan.h"
#include "r240503_resource.h"
#include "libs/coopteam.h"
#include "libs/winapi.h"
#include "libs/game_flags.h"

void initUserRespawn()
{
    short points[]={
        16,17,18,19,20,21,
    };
    SetRespawnPointSettings(points,sizeof(points));
}

void initReadables(){
    RegistSignMessage(Object("read1"), "**늪지의 수호자 -- 제작. 237 **");
    RegistSignMessage(Object("read2"), "**늪지의 수호자 -- 제작. 237 **시작하려면은 전진 후 오른쪽 통로로 가세요");
    RegistSignMessage(Object("read3"), "이곳은 매점입니다, 구입하고 싶은 품목을 클릭하여 거래를 진행하세요");
    RegistSignMessage(Object("read4"), "이 침대는 아주 오래전에 오우거의 원시부족이 잠을 자는 데 사용하였다고 전해져 충격...");
    RegistSignMessage(Object("read5"), "--징징이의 꿈과 희망이 묻힌 곳--");
    RegistSignMessage(Object("read6"), "제한구역- 관계자 외 출입금지- 길 없어, 돌아가");
    RegistSignMessage(Object("read7"), "그냥 장식용 표지판.. 밋밋한 것 보다는 나으니깐!");
}

void ggoverEvent(int limit){
    char msg[128];

    NoxSprintfString(msg,"게임오버 입니다! 괴물 수가 %d을 넘었습니다", &limit, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
    PlayerSetGGOver();
    CreateObjectById(OBJ_FOIL_CANDLE_UNLIT,LocationX(PLAYER_GGOVER_POS),LocationY(PLAYER_GGOVER_POS));
}

void displayEverytime(int master){
    if (MaxHealth(master))
    {
        char msg[256];
        int max=LIMIT_MON_COUNT, count= GetMonsterCount(),total=GetMonsterTotalCount();

        NoxSprintfString(msg,"괴물 수 %d개 넘으면 게임오버!!\n생존중인 괴물 수: %d\n누적 괴물 수: %d", &max,3);
        UniChatMessage(master,ReadStringAddressEx(msg), 60);
        if (count>max)
        {
            StopSpawnMonsterLoop();
            ggoverEvent(max);
            return;
        }
        PushTimerQueue(45, master, displayEverytime);
    }
}

void placeTeleporting(int pos){
    float x=GetObjectX(pos),y=GetObjectY(pos);

    Delete(pos);
    float dx=LocationX(105),dy=LocationY(105);
    DispositionTransport(x,y,dx+RandomFloat(-100.0,100.0),dy+RandomFloat(-100.0,100.0));
}

void goPlayerToField(){
    if (CurrentHealth(OTHER)){
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0) return;
        PlayerClassOnJoin(GetCaller(),pIndex);
    }
}

void initialPlaceDeco(){
    int home=CreateObjectById(OBJ_CANDLE_1,LocationX(106),LocationY(106));
    SetUnitCallbackOnPickup(home,goPlayerToField);
    UnitNoCollide(home);
}

void initGame()
{
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}

void teleportAllPlayerBossZone(){
    int r=MAX_PLAYER_COUNT;
    while (--r>=0){
        if(CurrentHealth(GetPlayer(r)))
            playerJoinBoss(GetPlayer(r));
    }
}

void tryTrophyPick(){
    UniPrint(OTHER,"당신이 승리하였어요!!");
}

#define WINNER_SUB 0
#define WINNER_COUNT 1
#define WINNER_MAX 2

void putWinnerTrophy(int *d){
    int sub=d[WINNER_SUB];
    if (ToInt(GetObjectX( sub))){
        if (--d[WINNER_COUNT]>=0){
            PushTimerQueue(1,d,putWinnerTrophy);
            int trophy= CreateObjectById(OBJ_LANTERN_2,GetObjectX(sub),GetObjectY(sub));
            SetUnitCallbackOnPickup(trophy,tryTrophyPick);
            return;
         }
        Delete(sub);
    }
    FreeSmartMemEx(d);
}

void onFinalbossDead(){
    float xy[]={GetObjectX(SELF),GetObjectY(SELF),};
    Delete(SELF);
    int *d;
    AllocSmartMemEx(WINNER_MAX*4,&d);
    PushTimerQueue(1,d,putWinnerTrophy);
    d[WINNER_SUB]=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,xy[0],xy[1]);
    d[WINNER_COUNT]=16;
    UniChatMessage(SELF, "아이 내가 죽다니...!", 90);
    UniPrintToAll("최종보스가 죽었습니다- 승리!!");
}

void onFinalBossSight(){
    UniChatMessage(SELF, "야이씨, 벌래들 그만 괴롭혀", 90);
}

void startBossZone(){
    teleportAllPlayerBossZone();
    float x=LocationX(113)+RandomFloat(-200.0,200.0),y=LocationY(113)+RandomFloat(-200.0,200.0);
    int boss=SpawnFinalBoss(x,y);
    GreenExplosion(x,y);
    DeleteObjectTimer( CreateObjectById(OBJ_GREEN_SMOKE,x,y), 12 );
    SetCallback(boss,5,onFinalbossDead);
    SetCallback(boss,3,onFinalBossSight);
    AggressionLevel(boss,1.0);
    SetUnitScanRange(boss,400.0);
}

void onMonsterSpawnFinished(){
    UniPrintToAll("***웨이브가 전부 끝났습니다, 잠시 후 보스존으로 이동하게 됩니다***");
    FrameTimer(180, startBossZone);
    QueryPlayerJoinFunction(NULLPTR,playerJoinBoss);
}

void showStartGameMentAll(){
    UniPrintToAll("<<-------------늪지의 수호자----------제작. 237---------------------------<<<");
    UniPrintToAll("괴물 수가 200개가 넘으면 게임오버! 열심히 괴물을 사냥해 보세요!! 마지막 보스도 잡아 보세요!!");
}

void onInitialUserJoin(int user){
    playerJoinNormal(user);
    QueryPlayerJoinFunction(NULLPTR,playerJoinNormal);
    InitMonsters(onMonsterSpawnFinished);
    PushTimerQueue(300, GetMasterUnit(),displayEverytime);
    FrameTimer(180, showStartGameMentAll);
}

void initPlayerSys(){
    QueryPlayerJoinFunction(NULLPTR,onInitialUserJoin);
}

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

void OnInitializeMap()
{
    MusicEvent();
    CreateLogFile("maplogging_r240503.txt");
    MakeCoopTeam();
    blockObserverMode();
    int initScanHash, lastUnit=GetMasterUnit();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_VANDEGRAF_LARGE_MOVABLE, placeTeleporting);
    // HashPushback(initScanHash, OBJ_HECUBAH_MARKER, placeSoulGate);
    // HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, CreateRandomMonster);
    StartUnitScan(Object("firstscan"), lastUnit, initScanHash);
    initUserRespawn();
    initReadables();
    initPlayerSys();
    initialPlaceDeco();
    InitializeShopSystem();
    initGame();
}

void OnShutdownMap()
{
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}

void InitializeClientOnly(){
    InitializePopupMessages();
    InitializeResource();

    ClientProcLoop();
}

void UseWishwell(){
    if (CurrentHealth(OTHER)==MaxHealth(OTHER))
        return;

    RestoreHealth(OTHER, 30);
    PlaySoundAround(OTHER,SOUND_PotionUse);
}


void OnPlayerEntryMapProc(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);
    
    if (pUnit)
        ShowQuestIntroOne(pUnit, 9999, "WarriorChapterBegin9", "Noxworld.wnd:DismalSwamp");

    if (pInfo==0x653a7c)
    {
        InitializePopupMessages();
        InitializeResource();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

