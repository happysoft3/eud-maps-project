
#include "test1129_stamps.h"
#include "test1129_player.h"
#include "libs/waypoint.h"
#include "libs/objectIDdefines.h"
#include "libs/coopteam.h"
#include "libs/observer.h"
#include "libs/logging.h"
#include "libs/clientside.h"

int ARRAY[2];
int PLAY_BGM = 0;
int m_DRAWER;

static int CheckIsDrawer(int drawer) //virtual
{
    return drawer==m_DRAWER;
}

int m_COLOR;
int DOTS[2001];

#define MAX_PLAYER_COUNT 32

int m_player[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{
    return m_player[pIndex];
}

static void SetPlayer(int pIndex, int user) //virtual
{
    m_player[pIndex]=user;
}

int m_pFlags[MAX_PLAYER_COUNT];

static int GetPlayerFlags(int pIndex) //virtual
{
    return m_pFlags[pIndex];
}

static void SetPlayerFlags(int pIndex, int flags) //virtual
{
    m_pFlags[pIndex]=flags;
}

int m_watchPoint[MAX_PLAYER_COUNT];

void initWatchPoint()
{
    int pIndex=MAX_PLAYER_COUNT;

    while (--pIndex>=0)
    {
        m_watchPoint[pIndex] = DummyUnitCreateById(OBJ_BOMBER, LocationX(7), LocationY(7));
        UnitNoCollide(m_watchPoint[pIndex]);
    }
}

void initReadables()
{
    RegistSignMessage(Object("read1"), "캐치마인드- 제작자. 패닉");
    RegistSignMessage(Object("read2"), "<--캐치마인드----- 열심히 그림을 그려보세요----------");
    RegistSignMessage(Object("read3"), "<--캐치마인드-----맵 입장하는 곳입니다 -------------");
    RegistSignMessage(Object("read4"), "<--캐치마인드-----그림을 그리려면 누르세요. 1명만 그리기 가능합니다 -------------");
    RegistSignMessage(Object("read1A"), "<--캐치마인드-----그림을 구경하려면 누르세요 -------------");
    RegistSignMessage(Object("read1B"), "<--캐치마인드-----제작자- 패닉 -------------");
    RegistSignMessage(Object("read2A"), "<--캐치마인드-----내 그림을 맞춰봐! -------------");
}

void InitMap()
{
	int unit = CreateObject("MovableStatueVictory3NE", 31);
	CreateObject("MovableStatueVictory3N", 32);
	CreateObject("MovableStatueVictory3NW", 33);
	MasterUnit();
	PushTimerQueue(30, MasterUnit(), ShowGameInfo);
    MakeCoopTeam();
    initReadables();
}

void MapInitialize()
{
    CreateLogFile("test1129-log.txt");
	MusicEvent();
    initWatchPoint();
	StrChangeColor();
	StrShutdown();
	FrameTimer(1, StrDrawPicture);
	FrameTimer(1, StrEraiser);
	FrameTimer(2, StrWatch);
	FrameTimer(1, InitMap);
    WriteLog("end map init");
}

void MapExit()
{
	MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void GoDrawing()
{
    if (!IsPlayerUnit(OTHER))
        return;

    if (CurrentHealth(m_DRAWER))
    {
        UniPrint(OTHER, "이미 누군가 그림을 그리고 있네요, 구경하기 스위치를 이용해 무슨 그림을 그리는지 구경해볼까요");
        return;
    }
    m_DRAWER = GetCaller();
    m_COLOR = 0;
    DOTS[2000] = 1999;
    Enchant(m_DRAWER, "ENCHANT_INVULNERABLE", 0.0);
    Enchant(m_DRAWER, "ENCHANT_HELD", 0.0);
    MoveObject(m_DRAWER, LocationX(7), LocationY(7));
    PushTimerQueue(1, m_DRAWER, DrawerLoop);
}

#define GO_CENTER 5

void GoObserverMode()
{
    Enchant(OTHER, EnchantList(ENCHANT_FREEZE), 2.0);
    MoveObject(OTHER,LocationX(GO_CENTER),LocationY(GO_CENTER));
    PlayerLook(OTHER, m_watchPoint[GetPlayerIndex(OTHER)]);
}

void DrawerLoop(int drawer)
{
	int glow;
	while (CurrentHealth(drawer))
	{
		if (!IsObjectOn(glow))
		{
			glow = CreateObject("Moonglow", 7);
			SetOwner(drawer, glow);
		}
		else
		{
			if (!HasEnchant(drawer, "ENCHANT_INVULNERABLE"))
			{
				DeleteObjectTimer(CreateObjectById(OBJ_MAGIC_SPARK, GetObjectX(glow), GetObjectY(glow)), 9);
				Enchant(drawer, "ENCHANT_INVULNERABLE", 0.0);
				if (Distance(GetObjectX(glow), GetObjectY(glow), LocationX(31), LocationY(31)) < 30.0)
				{
					UniPrint(drawer, "모든 점들을 지웁니다");
					EraiseAllDots();
				}
				else if (Distance(GetObjectX(glow), GetObjectY(glow), GetWaypointX(32), GetWaypointY(32)) < 30.0)
				{
					m_COLOR = (m_COLOR + 1) % 4;
                    PrintMessageFormatOne(drawer, "점 색상을 변경합니다 [Color: %s]", StringUtilGetScriptStringPtr(ColorNotify(m_COLOR)));
				}
				else if (Distance(GetObjectX(glow), GetObjectY(glow), GetWaypointX(33), GetWaypointY(33)) < 30.0)
				{
					UniPrint(drawer, "그림을 그만 그립니다");
					EnchantOff(drawer, "ENCHANT_HELD");
					MoveObject(drawer, GetWaypointX(4), GetWaypointY(4));
					m_DRAWER = 0;
                    break;
				}
			}
			else if (CheckRightClick(drawer))
			{
				if (DOTS[2000] > 0 && ToInt(Distance(GetObjectX(glow), GetObjectY(glow) + 18.0, LocationX(8), LocationY(8))))
				{
                    TeleportLocation(8, GetObjectX(glow), GetObjectY(glow) + 18.0);
					DOTS[DOTS[2000]] = CreateObjectAt(ColorString(m_COLOR), GetObjectX(glow), GetObjectY(glow) + 18.0);
					DOTS[2000] --;
				}
			}
		}
        PushTimerQueue(1, drawer, DrawerLoop);
        return;
	}
    Delete(glow);
    EraiseAllDots();
}

int MasterUnit()
{
	int unit;

	if (!unit)
	{
		unit = CreateObject("Hecubah", 34);
		Frozen(unit, 1);
	}
	return unit;
}

void ShowGameInfo(int master)
{
    char buff[128];
    int args[]={StringUtilGetScriptStringPtr(ColorNotify(m_COLOR)), DOTS[2000]};

    NoxSprintfString(buff, "COLOR: %s\n남은 도트 수: %d", args, sizeof(args));
	UniChatMessage(master, ReadStringAddressEx(buff), 60);
	PushTimerQueue(30, master, ShowGameInfo);
}

// void RegistPlayer()
// {
// 	int k;
// 	int plr;

// 	while (1)
// 	{
// 		if (CurrentHealth(OTHER))
// 		{
// 			if (MaxHealth(OTHER) == 150)
// 			{
// 				plr = CheckPlayer();
// 				for (k = 9 ; k >= 0 && plr < 0 ; k --)
// 				{
// 					if (!MaxHealth(player[k]))
// 					{
// 						player[k] = GetCaller();
// 						player[k + 10] = 1;
// 						plr = k;
// 						break;
// 					}
// 				}
// 				if (plr >= 0)
// 				{
// 					PlayerHasJoin(plr);
// 					break;
// 				}
// 			}
// 		}
// 		CantJoinPlayer();
// 		break;
// 	}
// }

// void PlayerHasJoin(int plr)
// {
// 	Enchant(player[plr], "ENCHANT_ANCHORED", 0.0);
// 	MoveObject(player[plr], GetWaypointX(4), GetWaypointY(4));
// 	DeleteObjectTimer(CreateObject("BlueRain", 4), 10);
// 	RemoveAllItems(player[plr]);
// }

// void CantJoinPlayer()
// {
// 	MoveObject(OTHER, GetWaypointX(30), GetWaypointY(30));
// 	Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
// 	Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
// 	UniPrint(OTHER,"이 맵은 전사만 입장할 수 있습니다");
// 	UniPrint(OTHER,"맵에 플레이어가 10명이 넘어도 입장이 제한될 수 있습니다");
// }

// void LoopPreservePlayers()
// {
// 	int k;

// 	for (k = 9 ; k >= 0 ; k --)
// 	{
// 		if (player[k + 10] && !CurrentHealth(player[k]))
// 		{
// 			player[k] = 0;
// 			player[k + 10] = 0;
// 			PrintToAll("플레이어" + IntToString(k + 1) + " 님이 격추되었습니다");
// 		}
// 		else if (CurrentHealth(player[k]))
// 		{
// 			if (HasEnchant(player[k], "ENCHANT_INVISIBLE"))
//             {
//                 RestoreHealth(player[k], 200);
//                 EnchantOff(player[k], "ENCHANT_INVISIBLE");
//                 EnchantOff(player[k], "ENCHANT_INVULNERABLE");
//                 Enchant(player[k], "ENCHANT_SHIELD", 0.0);
//                 Damage(player[k],0,100,13);
//                 EnchantOff(player[k], "ENCHANT_SHIELD");
//                 if (CurrentHealth(player[k]) == MaxHealth(player[k]))
//                 {
// 					if (player[k] == DRAWER)
// 						DRAWER = 0;
//                     player[k] = 0;
//                     continue;
//                 }
//                 RestoreHealth(player[k], 100);
//             }
// 			else if (HasEnchant(player[k], "ENCHANT_SNEAK"))
// 			{
// 				EnchantOff(player[k], "ENCHANT_SNEAK");
// 				if (HasEnchant(player[k], "ENCHANT_AFRAID"))
// 				{
// 					EnchantOff(player[k], "ENCHANT_AFRAID");
// 					EnchantOff(player[k], "ENCHANT_FREEZE");
// 					MoveObject(player[k], GetWaypointX(10), GetWaypointY(10));
// 				}
// 			}
// 		}
// 	}
// 	FrameTimer(1, LoopPreservePlayers);
// }


void RemoveAllItems(int unit)
{
	while (IsObjectOn(GetLastItem(unit)))
		Delete(GetLastItem(unit));
}

void EraiseAllDots()
{
	RemoveAllDots();
	FrameTimer(1, RemoveAllDots2);
	DOTS[2000] = 1999;
}

void RemoveAllDots()
{
	int r=1000;

    while (--r>=0)
		Delete(DOTS[r]);
}

void RemoveAllDots2()
{
	int r=1000;

    while (--r>=0)
		Delete(DOTS[r + 1000]);
}

string ColorString(int index)
{
	string table[] = {"ManaBombOrb", "HealOrb", "CharmOrb", "DrainManaOrb"};

	return table[index];
}

string ColorNotify(int n)
{
	string table[] = {"WHITE", "RED", "GREEN", "BLUE"};

	return table[n];
}

int CheckRightClick(int plr_unit)
{
    int ptr = UnitToPtr(plr_unit), temp;

    if (ptr)
    {
        temp = GetMemory(GetMemory(ptr + 0x2ec) + 0x114);
        if (temp)
            return (!(GetMemory(0x81b858 + (GetMemory(temp + 0x810) * 4)) ^ 0x03));
    }
    return 0;
}

void MapBgmData()
{ }

static void commonServerClientProcedure() //virtual
{
    ExtractMapBgm("..\\catch.mp3", MapBgmData);
}

static void NetworkUtilClientMain()
{
    commonServerClientProcedure();
}

