

#include "libs/define.h"
#include "libs/queueTimer.h"
#include "libs/unitstruct.h"
#include "libs/hash.h"
#include "libs/format.h"
#include "libs/bind.h"
#include "libs/printutil.h"
#include "libs/logging.h"

#define UNITSCAN_PARAM_CUR 0
#define UNITSCAN_PARAM_LASTUNIT 1
#define UNITSCAN_PARAM_HASH 2
#define UNITSCAN_PARAM_COUNT 3

void OnEndedUnitScan(int *pParams)
{
    char buff[92];
    int count=pParams[UNITSCAN_PARAM_COUNT];

    NoxSprintfString(buff,"OnEndedUnitScan:%d", &count,1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

void UnitScanProcessProto(int functionId, int posUnit)
{
    Bind(functionId, &functionId + 4);
}

void UnitScanLoop(int *pParams)
{
    int cur=pParams[UNITSCAN_PARAM_CUR];
    int rep = 30, action, thingId;

    while (rep--)
    {
        if (++cur >= pParams[UNITSCAN_PARAM_LASTUNIT])
        {
            OnEndedUnitScan(pParams);
            return;
        }
        thingId=GetUnitThingID(cur);
        if (thingId)
        {
            if (HashGet(pParams[UNITSCAN_PARAM_HASH], thingId, &action, FALSE))
            {
                WriteLog("unitscan-thingId");
                UnitScanProcessProto(action, cur);
                ++pParams[UNITSCAN_PARAM_COUNT];
                WriteLog("unitscan-thingId-end");
            }
        }
    }
    pParams[UNITSCAN_PARAM_CUR]=cur;
    PushTimerQueue(1, pParams, UnitScanLoop);
}

void StartUnitScan(int cur, int lastCreatedUnit, int unitScanHash)
{
    int params[]={
        cur,
        lastCreatedUnit,
        unitScanHash,
        0,
    };
    PushTimerQueue(1, params, UnitScanLoop);
    UniPrintToAll("mansion2.c::StartUnitScan");
}

#undef UNITSCAN_PARAM_CUR
#undef UNITSCAN_PARAM_LASTUNIT
#undef UNITSCAN_PARAM_HASH
#undef UNITSCAN_PARAM_COUNT


