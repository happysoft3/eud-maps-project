
#include "libs/define.h"
#include "libs/potionex.h"
#include "libs/waypoint.h"
#include "libs/sound_define.h"
#include "libs/fxeffect.h"
#include "libs/itemproperty.h"
#include "libs/indexloop.h"
#include "libs/weaponcapacity.h"
#include "libs/coopteam.h"
#include "libs/playerupdate.h"
#include "libs/printutil.h"
#include "libs/buff.h"
#include "libs/reaction.h"
#include "libs/observer.h"
#include "libs/spellutil.h"

int Wisp;
int Array[70];
int player[30], PlrCam[10], Life = 3, GlobalClassRev = 1;


int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);

    return x;
}

int Bear2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50;
		
		arr[16] = 20000; arr[17] = 90; arr[18] = 100; arr[19] = 40; 
		arr[21] = 1065353216; arr[23] = 65545; arr[24] = 1067450368; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30;
		arr[58] = 5547856; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

string StartMessage(int num)
{
    string msg[] = {
    "MAZE RUNNER__ (메이즈 러너)                                                                             -제작. 237",
    "지금 여러분은 미로에 갇혔습니다, 이 미로 속에는 괴물들이 존재하는데 이 괴물들은 인간을 제거하기 위해 끊임없이 쫓아다닙니다",
    "미로를 나갈 수 있는 유일한 방법은 미로 곳곳에 있는 위스프들을 모두 없애는 것입니다, 그러면 자동으로 미로에서 나가집니다   ",
    "게임 팁_ 조심스럽게 걷기 기술을 시전하면 일정거리를 빠르게 이동할 수 있는 대쉬가 발동됩니다 (쿨다운 20초)               ",
    "게임 팁_ [ 횃불 ] 아이템을 소지한 상태에서 작살을 시전하면 석궁이 발사됩니다 - 데미지 255                              ",
    "게임 팁_ 미로에는 여러 무기와 갑옷들이 준비되어 있습니다, 괴물은 무적상태가 아니므로 괴물을 격추시킬 수 있습니다         ",
    "게임 팁_ 미로 일부 구간은 막혀있는 곳이나 비밀벽도 있습니다, 게임 이용에 참고하시기 바랍니다                           ",
    "게임 팁_ 모든 플레이어의 죽음 수가 3을 넘으면 리스폰 불가 모드가 됩니다, 다만 맵에 있는 앵크를 획득하면 죽은 플레이어가 부활됩니다",
    "버그 리포트 문의 및 최신 맵 조회는 blog.daum.net/ky10613 을 방문하시기 바랍니다                                      "};

    return msg[num];
}

void StartGameMessage()
{
    int count;

    if (count < 9)
    {
        UniPrintToAll(StartMessage(count++));
        SecondTimer(5, StartGameMessage);
    }
    else
        FrameTimer(3, DecorationsToMap);
}

int GetMasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 1);
        MoveObject(unit, 5050.0, 100.0);
        Frozen(unit, 1);
    }
    return unit;
}

void ActiveObserverMode(int plr)
{
    int pic = GetActivatePlayer(plr);

    EnchantOff(player[plr], "ENCHANT_CROWN");
    ObjectOff(player[plr]);
    MoveObject(player[plr], GetWaypointX(223), GetWaypointY(223));
    MoveWaypoint(224, GetObjectX(player[pic]), GetObjectY(player[pic]));
    PlrCam[plr] = SpawnPlayerCamera(plr, 224);
    LookWithAngle(PlrCam[plr], pic);
}

int SpawnPlayerCamera(int plr, int wp)
{
    int unit = CreateObject("Maiden", wp);

    UnitNoCollide(unit);
    LookWithAngle(CreateObject("InvisibleLightBlueLow", wp), plr);
    SetOwner(player[plr], unit);
    GiveCreatureToPlayer(player[plr], unit);
    PlayerLook(player[plr], unit);
    ObjectOff(unit);
    Damage(unit, 0, 999, -1);

    return unit;
}

void MapInitialize()
{
    MusicEvent();
    GetMasterUnit();
    InitArrays();
    FrameTimer(30, LoopPreservePlayers);
    SecondTimer(5, StartGameMessage);
    SecondTimer(15, PlaceAllGolems);
    FrameTimer(30, REVSetToZero);
    FrameTimer(1, MakeCoopTeam);
}

void REVSetToZero()
{
    GlobalClassRev = 0;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;
    SelfDamageClassEntry(pUnit);
    return plr;
}

void PlayerRegist()
{
    int k, plr;

    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            if (MaxHealth(OTHER) >= 150)
            {
                plr = CheckPlayer();
                for (k = 9 ; k >= 0 && plr < 0 ; k -=1)
                {
                    if (!MaxHealth(player[k]))
                    {
                        plr = PlayerClassOnInit(k, GetCaller());
                        break;
                    }
                }
                if (plr >= 0)
                {
                    PlayerEntry(plr);
                    break;
                }
            }
            else
                UniPrint(OTHER, "죄송합니다, [ 전사 ] 만 참가할 수 있는 맵입니다");
        }
        CantJoin();
        break;
    }
}

void PlayerEntry(int plr)
{
    int wp = Random(1, 4);

    if (PlayerClassDeathFlagCheck(plr))
        PlayerClassDeathFlagSet(plr);
    if (GlobalClassRev)
    {
        SetUnitMaxHealth(player[plr], 250);
        Enchant(player[plr], "ENCHANT_CROWN", 0.0);
        EmptyInventory(player[plr]);
        MoveObject(player[plr], GetWaypointX(wp), GetWaypointY(wp));
        AudioEvent("DeathOff", wp);
        DeleteObjectTimer(CreateObject("OblivionUp", wp), 12);
        ObjectOn(player[plr]);
    }
    else
    {
        ActiveObserverMode(plr);
    }
}

void CantJoin()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    UniPrint(OTHER, "sorry, this map can not be loaded in your nox version, you are visit here: blog.daum.net/ky10613/178");
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k + 1 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void PlayerOnDeath(int plr)
{
    int pUnit = player[plr];
    if (!PlayerClassCamFlagCheck(plr))
        PlayerClassCamFlagSet(plr);
    UniPrint(pUnit, "당신은 죽었습니다");
}

void PlayerHandlerProcess(int plr, int pUnit)
{
    if (UnitCheckEnchant(pUnit, GetLShift(31)))
    {
        RemoveTreadLightly(pUnit);
        EnchantOff(pUnit, EnchantList(31));
        if (UnitCheckEnchant(pUnit, GetLShift(30)))
        {
            EnchantOff(pUnit, EnchantList(30));
            Enchant(pUnit, EnchantList(10), 20.0);
            FrameTimerWithArg(1, plr, FastMoveEvent);
            FrameTimerWithArg(1, plr, CooldownCount);
        }
    }
}

int PlayerClassDeathFlagCheck(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassDeathFlagSet(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

int PlayerClassCamFlagCheck(int plr)
{
    return player[plr + 10] & 0x40;
}

void PlayerClassCamFlagSet(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x40;
}

void LoopPreservePlayers()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    if (PlayerClassCamFlagCheck(i))
                        ObserverModeHandler(i);
                    else
                        PlayerHandlerProcess(i, player[i]);
                    break;
                }
                else
                {
                    if (!PlayerClassDeathFlagCheck(i))
                    {
                        PlayerClassDeathFlagSet(i);
                        PlayerOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerGoOut(i);
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayers);
}

void PlayerGoOut(int plr)
{
    if (MaxHealth(player[plr]))
    {
        SetUnitMaxHealth(player[plr], 150);
    }
    if (MaxHealth(PlrCam[plr]))
    {
        Delete(PlrCam[plr]);
        Delete(PlrCam[plr] + 1);
    }
    player[plr] = 0;
}

void CooldownCount(int plr)
{
    if (CurrentHealth(player[plr]))
    {
        if (HasEnchant(player[plr], "ENCHANT_VILLAIN"))
            SecondTimerWithArg(1, plr, CooldownCount);
        else
            Enchant(player[plr], "ENCHANT_CROWN", 0.0);
    }
}

void FastMoveEvent(int plr)
{
    if (CurrentHealth(player[plr]))
    {
        TeleportLocation(202, GetObjectX(player[plr]) - UnitAngleCos(player[plr], 13.0), GetObjectY(player[plr]) - UnitAngleSin(player[plr], 13.0));
        AroundStunEffect(player[plr], 142.0, 202);
        int ptr = CreateObject("InvisibleLightBlueHigh", 202);
        Frozen(CreateObject("Maiden", 202), TRUE);
        SetOwner(player[plr], ptr);
        FrameTimerWithArg(1, ptr, PlayerWindBooster);
    }
}

void PlayerWindBooster(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(owner) && count < 30)
    {
        TeleportLocation(202, GetObjectX(owner), GetObjectY(owner));
        PlaySoundAround(owner, SOUND_ElevLOTDUp);
        MoveObject(ptr + 1, GetObjectX(owner) - UnitAngleCos(owner, 13.0), GetObjectY(owner) - UnitAngleSin(owner, 13.0));
        Effect("YELLOW_SPARKS", GetObjectX(ptr + 1), GetObjectY(ptr + 1), 0.0, 0.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, PlayerWindBooster);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void ObserverModeHandler(int plr)
{
    int watch;

    if (MaxHealth(PlrCam[plr]))
    {
        watch = GetDirection(PlrCam[plr]);
        MoveObject(PlrCam[plr], GetObjectX(player[watch]), GetObjectY(player[watch]));
        if (CheckWatchFocus(player[plr]))
        {
            PlayerLook(player[plr], PlrCam[plr]);
            LookWithAngle(PlrCam[plr], GetActivatePlayer(plr));
        }
    }
}

void AllPlayersRevive()
{
    int k;

    GlobalClassRev = 1;
    for (k = 9 ; k >= 0 ; k --)
        DisableObserverMode(k);
    FrameTimer(30, REVSetToZero);
}

void DisableObserverMode(int plr)
{
    if (CurrentHealth(player[plr]) && PlayerClassCamFlagCheck(plr))
    {
        if (MaxHealth(PlrCam[plr]))
        {
            Delete(PlrCam[plr]);
            Delete(PlrCam[plr] + 1);
            MoveObject(player[plr], GetWaypointX(225), GetWaypointY(225));
        }
        PlayerClassCamFlagSet(plr);
    }
}

void EmptyInventory(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void ControlEntranceSwitch(int stat)
{
    if (stat)
    {
        ObjectOn(Object("EntranceSwitch"));
    }
    else
    {
        ObjectOff(Object("EntranceSwitch"));
    }
}

void HarpoonEvent(int cur, int owner)
{
    if (CurrentHealth(owner))
    {
        int re = CheckBullet(owner, 1);

        if (re)
        {
            int mis = CreateObjectAt("ArcherBolt", GetObjectX(cur), GetObjectY(cur));

            Enchant(mis, "ENCHANT_SHOCK", 0.0);
            PlaySoundAround(mis, SOUND_CrossBowShoot);
            LookWithAngle(mis, GetDirection(owner));
            SetOwner(owner, mis);
            PushObjectTo(mis, UnitRatioX(cur, owner, 32.0), UnitRatioY(cur, owner, 32.0));
            Delete(re);
        }
    }
    Delete(cur);
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 526, HarpoonEvent);
    HashPushback(pInstance, 1179, ShurikenEvent);
}

int GetNealryPlayer(int unit)
{
    int k, res = -1;
    float cmp = 9999.0;

    for (k = 9 ; k >= 0 ; k -= 1)
    {
        if (CurrentHealth(player[k]) && IsObjectOn(player[k]))
        {
            float cur = DistanceUnitToUnit(unit, player[k]);

            if (cur < cmp)
            {
                cmp = cur;
                res = k;
            }
        }
    }
    return res;
}

void GolemUnitAI(int unit)
{
    if (CurrentHealth(unit))
    {
        int target = GetNealryPlayer(unit);

        if (target + 1)
        {
            CreatureFollow(unit, player[target]);
            AggressionLevel(unit, 1.0);
            if (!HasEnchant(unit, "ENCHANT_ETHEREAL"))
            {
                if (!TeleportUnitAt(unit))
                    SummonUnit(unit);
            }
        }
        FrameTimerWithArg(42, unit, GolemUnitAI);
    }
}

void SummonUnit(int unit)
{
    int mobFunctions[] = {SummonSkeletonLord, SummonCarnPlant, SummonOgreWarlord, SummonBear, SummonLich, SummonLittleGirl};
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));

    SetOwner(unit, ptr);
    FrameTimerWithArg(17, ptr, mobFunctions[Random(0, 5)]);
    Effect("TELEPORT", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
}

void SummonSkeletonLord(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        int unit = CreateObjectAt("SkeletonLord", GetObjectX(ptr), GetObjectY(ptr));

        SetUnitMaxHealth(unit, 250);
        SetOwner(GetMasterUnit(), unit);
        SetCallback(unit, 3, SummonedUnitSightEvent);
    }
    Delete(ptr);
}

void SummonCarnPlant(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        int unit = CreateObjectAt("CarnivorousPlant", GetObjectX(ptr), GetObjectY(ptr));
        SetUnitMaxHealth(unit, 308);
        SetUnitSpeed(unit, 1.3);
        AggressionLevel(unit, 1.0);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetOwner(GetMasterUnit(), unit);
        SetCallback(unit, 3, SummonedUnitSightEvent);
    }
    Delete(ptr);
}

void SummonOgreWarlord(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        int unit = CreateObjectAt("OgreWarlord", GetObjectX(ptr), GetObjectY(ptr));
        SetUnitMaxHealth(unit, 295);
        SetOwner(GetMasterUnit(), unit);
        SetCallback(unit, 3, SummonedUnitSightEvent);
    }
    Delete(ptr);
}

void SummonBear(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        int unit = CreateObjectAt("Bear", GetObjectX(ptr), GetObjectY(ptr));
        SetUnitMaxHealth(unit, 310);
        SetOwner(GetMasterUnit(), unit);
        SetCallback(unit, 3, SummonedUnitSightEvent);
    }
    Delete(ptr);
}

void SummonLich(int ptr)
{
    int owner = GetOwner(ptr), unit, addr;

    if (CurrentHealth(owner))
    {
        MoveWaypoint(56, GetObjectX(ptr), GetObjectY(ptr));
        unit = CreateObject("GruntAxe", 56);
        addr = GetMemory(0x750710);
        SetMemory(addr + 4, 1342); //Lich image
        SetUnitMaxHealth(unit, 225);
        Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
        SetMemory(GetMemory(addr + 0x2ec) + 0x1e8, VoiceList(32));
        SetOwner(GetMasterUnit(), unit);
        SetCallback(unit, 3, SummonedUnitSightEvent);
    }
    Delete(ptr);
}

void SummonLittleGirl(int ptr)
{
    int owner = GetOwner(ptr), unit;

    if (CurrentHealth(owner))
    {
        MoveWaypoint(56, GetObjectX(ptr), GetObjectY(ptr));
        unit = ColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), 56);
        SetOwner(GetMasterUnit(), unit);
        SetCallback(unit, 3, SummonedUnitSightEvent);
    }
    Delete(ptr);
}

int DrawMagicIconAtLocation(int locationNumber)
{
    int unit = CreateObject("AirshipBasketShadow", locationNumber);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, 2616);
    return unit;
}

void DecorationsToMap()
{
    int k;

    for (k = 203 ; k <= 220 ; TRUE)
        DrawMagicIconAtLocation(k++);
}

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr1 = GetMemory(0x750710), ptr2, k;

    SetMemory(ptr1 + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    UnitLinkBinScript(unit, Bear2BinTable());
    SetUnitMaxHealth(unit, 260);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));

    return unit;
}

void SummonedUnitSightEvent()
{
    CreatureFollow(SELF, OTHER);
    AggressionLevel(SELF, 1.0);
}

int TeleportUnitAt(int unit)
{
    int loc = Random(8, 55);

    if (CheckPlayerSafeZone(loc))
    {
        GreenSparkAtFx(LocationX(loc), LocationY(loc));
        MoveObject(unit, LocationX(loc), LocationY(loc));
        Enchant(unit, "ENCHANT_ETHEREAL", 20.0);
        Enchant(unit, "ENCHANT_FREEZE", 1.0);
        Enchant(unit, "ENCHANT_VILLAIN", 1.0);
        AudioEvent("TripleChime", loc);
        return 1;
    }
    return 0;
}

int CheckPlayerSafeZone(int wp)
{
    float x = GetWaypointX(wp), y = GetWaypointY(wp);
    int k;

    for (k = 9 ; k >= 0 ; k -= 1)
    {
        if (CurrentHealth(player[k]))
        {
            if (Distance(x, y, GetObjectX(player[k]), GetObjectY(player[k])) < 490.0)
                return 0;
        }
    }
    return 1;
}

//110~200

void PutRewardMarker()
{
    int pic, count;

    if ((count) < 90)
    {
        int itemTable[] = {PutArmor, PutItem, PutPotions, PutBullet};
        FrameTimerWithArg(1, CreateObject("RedPotion", count + 110), itemTable[Random(0, 3)]);
        ++count;
        FrameTimer(1, PutRewardMarker);
    }
}

string WeaponName(int num)
{
    string name[] = {"FanChakram", "WarHammer", "GreatSword", "OblivionHalberd", "OblivionHeart", "OblivionWierdling"};

    return name[num];
}

string Potions(int num)
{
    string name[] = {"RedPotion", "YellowPotion", "BlackPotion", "HastePotion"};

    return name[num];
}

string ArmorTable(int num)
{
    string name[] = {"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "MedievalCloak"};
    return name[num];
}

void PutArmor(int sUnit)
{
    int armor = CreateObjectAt(ArmorTable(Random(0, 5)), GetObjectX(sUnit), GetObjectY(sUnit));

    Delete(sUnit);
    Frozen(armor, TRUE);
    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
}

void PutItem(int sUnit)
{
    int weapon = CreateObjectAt(WeaponName(Random(0, 5)), GetObjectX(sUnit), GetObjectY(sUnit));

    Delete(sUnit);
    Enchant(weapon, EnchantList(25), 0.0);
    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    SetSpecialWeapon(weapon);
}

void PutPotions(int sUnit)
{
    int potion = CheckPotionThingID(CreateObjectAt(Potions(Random(0, 3)), GetObjectX(sUnit), GetObjectY(sUnit)));

    Delete(sUnit);
}

void PutBullet(int ptr)
{
    int k;
    
    Delete(ptr);
    for (k = 0 ; k < 6 ; k += 1)
    {
        LookWithAngle(CreateObjectAt("TorchInventory", GetObjectX(ptr), GetObjectY(ptr)), 1);
    }
}

int CheckBullet(int unit, int type)
{
    int inv = GetLastItem(unit);

    while (IsObjectOn(inv))
    {
        if (GetDirection(inv) == type)
            return inv;
        inv = GetPreviousItem(inv);
    }
    return 0;
}

void SetSpecialWeapon(int weapon)
{
    int id = GetUnitThingID(weapon);

    if (id >= 222 && id <= 225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (id == 1178)
        SetConsumablesWeaponCapacity(weapon, 100, 100);
    else if (id == 1168)
        SetConsumablesWeaponCapacity(weapon, 50, 50);
}

void PutWisps()
{
    int count;

    if (count < 30)
    {
        SpawnWisp(59 + Array[count++]);
        FrameTimer(1, PutWisps);
    }
}

void SpawnWisp(int wp)
{
    int unit = CreateObject("WillOWisp", wp);

    WispPtrProperty(unit);
    SetOwner(GetMasterUnit(), unit);
    SetCallback(unit, 5, WispDeath);
    Enchant(unit, "ENCHANT_BLINDED", 0.0);
    AggressionLevel(unit, 0.0);
    Wisp ++;
}

void PlayerDeath()
{
    if (!CountLiveHuman())
    {
        Life --;
        if (Life)
        {
            FrameTimer(30, AllPlayersRevive);
            UniPrintToAll("플레이어가 전멸되었습니다, 라이프를 차감하여 모든 플레이어를 다시 부활시킵니다 (남은 라이프: " + IntToString(Life) + " )");
        }
        else
        {
            ControlEntranceSwitch(0);
            UniPrintToAll("게임오버, 라이프 0!!");
        }
    }
    else
    {
        UniPrintToAll("방금 누군가 죽었습니다");
    }
}

void WispDeath()
{
    if (--Wisp)
        UniPrintToAll("남은 위스프 수: " + IntToString(Wisp));
    else
        VictoryEvent();
}

void VictoryEvent()
{
    ControlEntranceSwitch(0);
    MoveObject(Object("PlayerStartLocation"), LocationX(201), LocationY(201));
    TeleportAllPlayers(201);
    FrameTimer(30, YoureWinner);
}

void YoureWinner()
{
    Effect("WHITE_FLASH", LocationX(201), LocationY(201), 0.0, 0.0);
    AudioEvent("StaffOblivionAchieve2", 201);
    FrameTimer(1, StrVictory);
    UniPrintToAll("승리: 미로를 탈출하셨습니다                                         ");
}

void TeleportAllPlayers(int wp)
{
    float xpos = LocationX(wp), ypos = LocationY(wp);
    int k;

    for (k = 9 ; k >= 0 ; k -= 1)
    {
        if (CurrentHealth(player[k]))
        {
            MoveObject(player[k], xpos, ypos);
            player[k] = 0;
        }
    }
}

void WispPtrProperty(int unit)
{
    SetUnitMaxHealth(unit, 400);
    SetUnitMass(unit, 99999.0);
}

void InitArrays()
{
    int k;

    for (k = 0 ; k < 50 ; k += 1)
    {
        Array[k] = k;
    }
    MixArrayTable();
    FrameTimer(1, MixArrayTable);
    FrameTimer(2, MixArrayTable);
    FrameTimer(3, MixArrayTable);
    FrameTimer(4, MixArrayTable);
}

void MixArrayTable()
{
    int k;

    for (k = 0 ; k < 50 ; k += 1)
    {
        int pic = Random(0, 50);
        if (k == pic) continue;
        Array[k] ^= Array[pic];
        Array[pic] ^= Array[k];
        Array[k] ^= Array[pic];
    }
}

void PlaceAllGolems()
{
    int count;

    if (count < 10)
    {
        count ++;
        SpawnMonster(Random(8, 55));
        FrameTimer(4, PlaceAllGolems);
    }
    else
    {
        UniPrintToAll("또 그들이 몰려온다...!");
        SpawnReviveItem();
        FrameTimer(1, PutRewardMarker);
        FrameTimer(30, PutWisps);
    }
}

int SpawnMonster(int wp) //8~55
{
    int unit = CreateObject("StoneGolem", wp);

    UnitPtrProperties(GetMemory(0x750710), unit);
    SetOwner(GetMasterUnit(), CreateObject("InvisibleLightBlueLow", wp) - 1);
    SetCallback(unit, 3, GolemDetectEvent);
    SetCallback(unit, 9, UnitTouchedEvent);
    SetCallback(unit, 10, UnitHearEnemy);
    FrameTimerWithArg(15, unit, GolemUnitAI);
    Enchant(unit, "ENCHANT_VILLAIN", 1.5);
    Enchant(unit, "ENCHANT_ETHEREAL", 25.0);

    return unit;
}

void UnitTouchedEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_VILLAIN") && CurrentHealth(SELF))
    {
        if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
        {
            Enchant(SELF, "ENCHANT_VILLAIN", 0.3);
            BloodingFX(GetCaller());
            PlaySoundAround(OTHER, SOUND_EvilCherubRecognize);
            EnchantOff(OTHER, "ENCHANT_INVULNERABLE");
            Damage(OTHER, SELF, 30, 2);
        }
    }
}

void UnitHearEnemy()
{
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        MoveWaypoint(7, GetObjectX(SELF), GetObjectY(SELF));
        Enchant(SELF, "ENCHANT_ETHEREAL", 10.0);
        AudioEvent("BeholderMove", 7);
    }
}

void BloodingFX(int unit)
{
    int k, ptr = CreateObject("InvisibleLightBlueLow", 57) + 1;

    for (k = 0 ; k < 36 ; k += 1)
        CreateObjectAt("PlayerWaypoint", GetObjectX(unit) + MathSine(k * 10 + 90, 42.0), GetObjectY(unit) + MathSine(k * 10, 42.0));
    Delete(ptr - 1);
    FrameTimerWithArg(9, ptr, RemoveBloodTrace);
}

void RemoveBloodTrace(int ptr)
{
    int k;

    for (k = 35 ; k >= 0 ; k -= 1)
        Delete(ptr + k);
}

void UnitPtrProperties(int ptr, int unit)
{
    SetMemory(ptr + 4, 1383); //TODO: hecubah
    SetUnitVoice(unit, 27);
    SetUnitMaxHealth(unit, 325);
}

void GolemDetectEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
        PlaySoundAround(SELF, SOUND_DemonDie);
        Enchant(SELF, "ENCHANT_BURNING", 0.0);
        Enchant(SELF, "ENCHANT_ETHEREAL", 15.0);
        Raise(GetTrigger() + 1, ToFloat(GetCaller()));
        FrameTimerWithArg(1, GetTrigger(), EnemyInSight);
    }
}

void EnemyInSight(int ptr)
{
    int target = ToInt(GetObjectZ(ptr + 1));

    if (CurrentHealth(target) && CurrentHealth(ptr))
    {
        if (IsVisibleTo(ptr, target))
        {
            LookAtObject(ptr, target);
            if (!HasEnchant(ptr, "ENCHANT_CHARMING"))
            {
                TeleportLocation(7, GetObjectX(ptr) + UnitRatioX(ptr, target, 10.5) , GetObjectY(ptr) + UnitRatioY(ptr, target, 10.5));
                WispExplosionAtFX(LocationX(7), LocationY(7));
                AudioEvent("WillOWispMove", 7);
                DeleteObjectTimer(CreateObject("Shopkeeper", 7), 1);
            }
        }
        else
            Raise(ptr + 1, 0.0);
        FrameTimerWithArg(1, ptr, EnemyInSight);
    }
    else
    {
        if (CurrentHealth(ptr))
            EnchantOff(ptr, "ENCHANT_BURNING");
    }
}

void WispExplosionAtFX(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 3);
}

void GreenSparkAtFx(float xpos, float ypos)
{
    int genFx = CreateObjectAt("MonsterGenerator", xpos, ypos);

    Damage(genFx, 0, 10, 100);
    Delete(genFx);
}

void ToggleSecretWall()
{
    UniPrint(OTHER, "주변 어딘가에 있는 벽이 움직입니다");
    WallToggle(Wall(205, 83));
}

void SpawnReviveItem()
{
    int wp = Random(8, 55);
    int unit = CreateObject("AnkhTradable", wp);

    UniChatMessage(unit, "부활 앵크 생성됨", 210);
    RegistItemPickupCallback(unit, GoReviveHuman);
}

void GoReviveHuman()
{
    GreenSparkAtFx(GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(OTHER, SOUND_AwardSpell);
    UniPrintToAll("리바이브가 사용되었습니다, 3 초동안 죽은 플레이어가 부활할 수 있습니다");
    UniPrint(OTHER, "라이프 +1 추가");
    Life ++;
    AllPlayersRevive();
    FrameTimer(120, SpawnReviveItem);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
    
    int k;

    for (k = 9 ; k >= 0 ; k -= 1)
        PlayerGoOut(k);
        
    UniPrintToAll("맵 종료기능이 정상 처리 되었습니다");
}

void ShurikenEvent(int cur, int owner)
{
    if (CurrentHealth(owner) && IsPlayerUnit(owner))
    {
        int mis = CreateObjectAt("WeakFireball", GetObjectX(cur), GetObjectY(cur));

        SetOwner(owner, mis);
        PushObjectTo(mis, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
    }
}

void StrVictory()
{
	int arr[13];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
	{
		drawStrVictory(arr[i++], name);
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(201);
		pos_y = GetWaypointY(201);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 201);
		if (count % 38 == 37)
			MoveWaypoint(201, GetWaypointX(201) - 74.000000, GetWaypointY(201) + 2.000000);
		else
			MoveWaypoint(201, GetWaypointX(201) + 2.000000, GetWaypointY(201));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(201, pos_x, pos_y);
	}
}

void FastMovingWalk()
{
    int unit;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(OTHER, "ENCHANT_LIGHT"))
        {
            Enchant(OTHER, "ENCHANT_LIGHT", 6.0);
            if (GetObjectX(OTHER) > GetObjectX(SELF) && GetObjectY(OTHER) > GetObjectY(SELF))
            {
                EnchantOff(OTHER, "ENCHANT_PROTECT_FROM_MAGIC");
                Enchant(OTHER, "ENCHANT_DETECTING", 0.0);
            }
            else if (GetObjectX(OTHER) < GetObjectX(SELF) && GetObjectY(OTHER) < GetObjectY(SELF))
            {
                EnchantOff(OTHER, "ENCHANT_DETECTING");
                Enchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
            }
        }
        else if (HasEnchant(OTHER, "ENCHANT_DETECTING"))
        {
            unit = CreateObjectAt("CarnivorousPlant", GetObjectX(OTHER) + 1.0, GetObjectY(OTHER) + 1.0);
            GreenSparkAtFx(GetObjectX(unit), GetObjectY(unit));
            PlaySoundAround(unit, SOUND_BeholderMove);
            Frozen(unit, TRUE);
            DeleteObjectTimer(unit, 1);
        }
        else if (HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC"))
        {
            unit = CreateObjectAt("CarnivorousPlant", GetObjectX(OTHER) - 1.0, GetObjectY(OTHER) - 1.0);
            GreenSparkAtFx(GetObjectX(unit), GetObjectY(unit));
            PlaySoundAround(unit, SOUND_BeholderMove);
            Frozen(unit, TRUE);
            DeleteObjectTimer(unit, 1);
        }
    }
}

void AroundStunEffect(int owner, float range, int wp)
{
    int tempUnit = CreateObject("InvisibleLightBlueHigh", wp) + 1, k;

    SetOwner(owner, tempUnit - 1);
    MoveObject(tempUnit - 1, range, GetObjectX(tempUnit - 1));
    for (k = 0 ; k < 8 ; k += 1)
    {
        DeleteObjectTimer(CreateObject("WeirdlingBeast", wp), 1);
        SetUnitMaxHealth(tempUnit + k, 100);
        UnitLinkBinScript(tempUnit + k, WeirdlingBeastBinTable());
        UnitZeroFleeRange(tempUnit + k);
        UnitNoCollide(tempUnit + k);
        LookWithAngle(tempUnit + k, 32 * k);
        SetOwner(tempUnit - 1, tempUnit + k);
        SetCallback(tempUnit + k, 3, Splash);
    }
    DeleteObjectTimer(tempUnit - 1, 2);
}

void Splash()
{
    if (!HasEnchant(OTHER, "ENCHANT_CHARMING"))
    {
        if (Distance(GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(SELF), GetObjectY(SELF)) <= GetObjectX(GetOwner(SELF)))
        {
            Enchant(OTHER, "ENCHANT_CHARMING", 1.7);
        }
    }
}

int GetActivatePlayer(int cur)
{
    int k, res = cur;

    for (k = 9 ; k >= 0 ; k --)
    {
        res = (res + 1) % 10;
        if (IsObjectOn(player[res]))
            return res;
    }
    return cur;
}

int CountLiveHuman()
{
    int k, res = 0;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]) && IsObjectOn(player[k]))
        {
            res ++;
        }
    }
    return res;
}

