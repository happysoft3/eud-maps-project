
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"

void GRPDumpBlueShell(){}

void changeFistSImage(int imgvec)
{
    int *frames, *sizes, thingId=OBJ_STONE_FIST_S;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDumpBlueShell)+4, thingId, xyInc, &frames,&sizes);

    AppendImageFrame(imgvec,frames[0],111838);
    AppendImageFrame(imgvec,frames[1],111839);
    AppendImageFrame(imgvec,frames[2],111840);
    AppendImageFrame(imgvec,frames[3],111841);
    AppendImageFrame(imgvec,frames[4],111842);
    AppendImageFrame(imgvec,frames[5],111843);
    AppendImageFrame(imgvec,frames[6],111844);
    AppendImageFrame(imgvec,frames[7],111845);
}

void GRPDump_TalesweaverWallensteinOutput(){}

#define TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT 127714

void initializeImageFrameTalesweavergirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH;
    int xyInc[]={0,-13};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_TalesweaverWallensteinOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,7,2,2);
    AnimFrameAssign8Directions(0,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(30, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameAssign8Directions(35, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameAssign8Directions(40, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameAssign8Directions(75, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 0);
    AnimFrameAssign8Directions(70, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 1);
    AnimFrameAssign8Directions(65, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 2);
    AnimFrameAssign8Directions(60, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+96, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 3);
    AnimFrameAssign8Directions(55, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 4);
    AnimFrameAssign8Directions(50, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+112, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 5);
    AnimFrameAssign8Directions(45, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+120, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);
}

void GRPDump_MaidenGhostOutput(){}
#define MAIDENGHOST_OUTPUT_BASE_IMAGE_ID 114546
void initializeImageFrameMaidenGhost(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GHOST;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MaidenGhostOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign8Directions(10, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign8Directions(15, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign8Directions(20, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);
    AnimFrameAssign8Directions(25, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_IDLE, 5);
    AnimFrameAssign8Directions(30, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_IDLE, 6);
    AnimFrameAssign8Directions(35, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_IDLE, 7);

    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void InitializeResources()
{
    int imgVector = CreateImageVector(2048);
    
    changeFistSImage(imgVector);
    initializeImageFrameTalesweavergirl(imgVector);
    initializeImageFrameMaidenGhost(imgVector);
    DoImageDataExchange(imgVector);
}

