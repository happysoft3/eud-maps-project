
#include "fov_gen.h"

void PlaceGensPart1A() //poison
{
    Generator(5, 20, 37, 10, 4);
    Generator(27, 20, 37, 10, 4);
    Generator(1, 14, 23, 10, 4);
    Generator(3, 20, 37, 10, 4);
    Generator(2, 40, 18, 15, 4);
    Generator(4, 11, 0, 15, 4);
    FrameTimer(1, PlaceGensPart2A);
}

void PlaceGensPart1B() //Undead
{
    Generator(5, 4, 41, 12, 4);
    Generator(27, 4, 41, 12, 4);
    Generator(1, 4, 41, 12, 4);
    Generator(3, 4, 41, 12, 4);
    Generator(2, 27, 31, 10, 4);
    Generator(4, 27, 31, 10, 4);
    FrameTimer(1, PlaceGensPart2B);
}

void PlaceGensPart1C() //human
{
    Generator(5, 26, 33, 10, 4);
    Generator(27, 0, 1, 7, 8);
    Generator(1, 26, 33, 10, 4);
    Generator(3, 26, 33, 10, 4);
    Generator(2, 25, 3, 15, 4);
    Generator(4, 32, 21, 15, 4);
    FrameTimer(1, PlaceGensPart2C);
}

void PlaceGensPart2A()
{
    Generator(6, 20, 37, 10, 4);
    Generator(7, 11, 0, 15, 4);
    Generator(8, 20, 37, 10, 4);
    Generator(9, 11, 0, 15, 4);
    Generator(13, 40, 18, 15, 4);
    Generator(12, 20, 37, 10, 4);
    Generator(14, 20, 37, 10, 4);
    Generator(15, 20, 37, 10, 4);
    Generator(16, 20, 37, 10, 4);
    Generator(17, 20, 37, 10, 4);
    Generator(19, 15, 19, 15, 4);

    Generator(18, 20, 37, 10, 4);
    Generator(20, 12, 30, 10, 4);
    Generator(21, 20, 37, 10, 4);
    Generator(22, 12, 30, 10, 4);
    FrameTimer(1, PlaceGensPart3A);
}

void PlaceGensPart2B()
{
    Generator(6, 4, 41, 10, 4);
    Generator(7, 4, 41, 10, 4);
    Generator(8, 4, 41, 10, 4);
    Generator(9, 4, 41, 10, 4);
    Generator(13, 2, 14, 10, 4);
    Generator(12, 2, 14, 10, 4);
    Generator(14, 2, 14, 10, 4);
    Generator(15, 4, 41, 10, 4);
    Generator(16, 2, 14, 10, 4);
    Generator(17, 4, 41, 10, 4);
    Generator(19, 3, 15, 15, 4);

    Generator(18, 6, 17, 15, 4);
    Generator(20, 2, 14, 15, 4);
    Generator(21, 2, 14, 15, 4);
    Generator(22, 27, 31, 15, 4);
    FrameTimer(1, PlaceGensPart3B);
}

void PlaceGensPart2C()
{
    Generator(6, 26, 33, 10, 4);
    Generator(7, 25, 3, 10, 4);
    Generator(8, 25, 3, 10, 4);
    Generator(9, 26, 33, 10, 4);

    Generator(13, 24, 4, 10, 4);
    Generator(12, 24, 4, 10, 4);
    Generator(14, 24, 4, 10, 4);
    Generator(15, 24, 4, 10, 4);
    Generator(16, 24, 4, 10, 4);
    Generator(17, 24, 4, 10, 4);
    Generator(19, 24, 4, 10, 4);

    Generator(18, 24, 4, 10, 4);
    Generator(20, 25, 3, 10, 4);
    Generator(21, 25, 3, 10, 4);
    Generator(22, 24, 4, 10, 4);
    FrameTimer(1, PlaceGensPart3C);
}

void PlaceGensPart3A()
{
    Generator(28, 15, 19, 15, 4);
    Generator(29, 15, 19, 15, 4);
    Generator(30, 20, 37, 10, 4);
    Generator(31, 20, 37, 10, 4);
    Generator(32, 20, 37, 10, 4);
    Generator(33, 42, 22, 10, 4);

    Generator(34, 42, 22, 10, 4);
    Generator(35, 41, 40, 5, 16);
    Generator(36, 33, 20, 10, 4);
    Generator(37, 20, 37, 5, 16);
    Generator(38, 21, 38, 15, 2);
    Generator(39, 14, 23, 5, 16);
    Generator(40, 1, 32, 15, 4);

    Generator(216, 17, 2, 10, 4);
    Generator(217, 11, 0, 10, 4);
    Generator(218, 15, 19, 10, 4);
    Generator(219, 21, 38, 10, 4);
    Generator(220, 20, 37, 5, 16);
    Generator(221, 7, 27, 10, 4);
    Generator(222, 8, 28, 10, 4);
    Generator(223, 19, 25, 15, 1);
    FrameTimer(1, PlaceGensPart4A);
}

void PlaceGensPart3B()
{
    Generator(28, 2, 14, 10, 4);
    Generator(29, 2, 14, 10, 4);
    Generator(30, 6, 17, 10, 4);
    Generator(31, 2, 14, 10, 4);
    Generator(32, 27, 31, 10, 4);
    Generator(33, 2, 14, 10, 4);

    Generator(34, 2, 14, 10, 4);
    Generator(35, 27, 31, 10, 4);
    Generator(36, 3, 15, 10, 4);
    Generator(37, 18, 5, 10, 2);
    Generator(38, 2, 15, 10, 4);
    Generator(39, 27, 31, 10, 4);
    Generator(40, 2, 14, 10, 4);

    Generator(216, 3, 15, 15, 4);
    Generator(217, 2, 14, 10, 4);
    Generator(218, 27, 31, 10, 4);
    Generator(219, 2, 14, 10, 4);
    Generator(220, 27, 31, 10, 16);
    Generator(221, 18, 5, 15, 4);
    Generator(222, 3, 15, 15, 4);
    Generator(223, 5, 39, 5, 10);
    FrameTimer(1, PlaceGensPart4B);
}

void PlaceGensPart3C()
{
    Generator(28, 24, 4, 10, 4);
    Generator(29, 24, 4, 10, 4);
    Generator(30, 26, 33, 10, 4);
    Generator(31, 25, 3, 10, 4);
    Generator(32, 26, 33, 10, 4);
    Generator(33, 25, 3, 10, 4);

    Generator(34, 24, 4, 10, 4);
    Generator(35, 25, 3, 10, 4);
    Generator(36, 25, 3, 10, 4);
    Generator(37, 43, 13, 15, 2);
    Generator(38, 24, 4, 10, 4);
    Generator(39, 24, 4, 10, 4);
    Generator(40, 24, 4, 10, 4);

    Generator(216, 26, 33, 5, 16);
    Generator(217, 24, 4, 10, 4);
    Generator(218, 24, 4, 10, 4);
    Generator(219, 24, 4, 10, 4);
    Generator(220, 31, 21, 10, 4);
    Generator(221, 25, 3, 10, 4);
    Generator(222, 25, 3, 10, 4);
    Generator(223, 30, 21, 10, 4);
    FrameTimer(1, PlaceGensPart4C);
}

void PlaceGensPart4A()
{
    Generator(42, 7, 27, 10, 4);
    Generator(43, 15, 19, 10, 4);
    Generator(44, 7, 27, 10, 4);
    Generator(45, 33, 20, 10, 4);
    Generator(46, 8, 28, 15, 4);
    Generator(47, 33, 20, 10, 4);
    Generator(49, 20, 37, 5, 16);
    Generator(50, 20, 37, 5, 16);
    
    Generator(59, 12, 30, 10, 4);
    Generator(58, 1, 32, 10, 4);

    Generator(60, 8, 28, 10, 4);
    Generator(61, 8, 28, 10, 4);
    Generator(62, 41, 40, 5, 8);
    Generator(63, 1, 32, 5, 8);
    FrameTimer(1, PlaceGensPart5A);
}

void PlaceGensPart4B()
{
    Generator(42, 4, 41, 5, 16);
    Generator(43, 4, 41, 5, 16);
    Generator(44, 4, 41, 5, 16);
    Generator(45, 2, 14, 10, 4);
    Generator(46, 3, 15, 10, 4);
    Generator(47, 2, 14, 10, 4);
    Generator(49, 6, 17, 5, 4);
    Generator(50, 27, 31, 10, 4);

    Generator(59, 2, 14, 5, 16);
    Generator(58, 27, 31, 10, 4);

    Generator(60, 3, 15, 10, 4);
    Generator(61, 3, 15, 10, 4);
    Generator(62, 27, 31, 5, 8);
    Generator(63, 27, 31, 5, 8);
    FrameTimer(1, PlaceGensPart5B);
}

void PlaceGensPart4C()
{
    Generator(42, 24, 4, 5, 8);
    Generator(43, 24, 4, 5, 8);
    Generator(44, 24, 4, 5, 8);
    Generator(45, 43, 13, 10, 4);
    Generator(46, 24, 4, 5, 4);
    Generator(47, 43, 13, 10, 4);
    Generator(49, 25, 3, 5, 4);
    Generator(50, 26, 33, 5, 4);

    Generator(59, 24, 4, 5, 16);
    Generator(58, 26, 33, 10, 4);

    Generator(60, 43, 13, 15, 2);
    Generator(61, 24, 4, 10, 4);
    Generator(62, 26, 33, 5, 8);
    Generator(63, 26, 33, 5, 8);
    FrameTimer(1, PlaceGensPart5C);
}

void PlaceGensPart5A()
{
    Generator(64, 7, 27, 10, 4);
    Generator(65, 9, 29, 15, 4);
    Generator(66, 7, 27, 10, 4);
    Generator(67, 9, 29, 15, 4);
    Generator(68, 16, 2, 15, 4);
    Generator(69, 15, 19, 15, 4);

    Generator(70, 22, 6, 15, 4);

    Generator(71, 28, 9, 20, 4);
    Generator(72, 12, 30, 10, 4);

    Generator(73, 9, 29, 20, 4);
    Generator(74, 7, 27, 10, 4);
    FrameTimer(1, PlaceMovingGenA);
}

void PlaceGensPart5B()
{
    Generator(64, 2, 14, 10, 4);
    Generator(65, 2, 14, 10, 4);
    Generator(66, 5, 39, 5, 4);
    Generator(67, 6, 17, 5, 4);
    Generator(68, 27, 31, 10, 4);
    Generator(69, 3, 15, 10, 4);

    Generator(70, 18, 5, 10, 4);

    Generator(71, 27, 31, 10, 4);
    Generator(72, 27, 31, 10, 4);

    Generator(73, 6, 17, 10, 4);
    Generator(74, 3, 15, 10, 4);
    FrameTimer(1, PlaceMovingGenB);
}

void PlaceGensPart5C()
{
    Generator(64, 24, 4, 10, 8);
    Generator(65, 24, 4, 10, 8);
    Generator(66, 24, 4, 10, 8);
    Generator(67, 25, 3, 5, 8);
    Generator(68, 25, 3, 5, 8);
    Generator(69, 25, 3, 5, 8);

    Generator(70, 23, 13, 15, 4);

    Generator(71, 25, 3, 5, 8);
    Generator(72, 26, 33, 5, 8);

    Generator(73, 43, 13, 10, 4);
    Generator(74, 24, 4, 10, 8);
    FrameTimer(1, PlaceMovingGenC);
}

void PlaceGensPart6A()
{
    Generator(108, 7, 27, 10, 4);
    Generator(109, 16, 2, 10, 4);
    Generator(110, 21, 38, 15, 4);

    Generator(111, 1, 32, 5, 16);
    Generator(112, 1, 32, 5, 16);

    Generator(113, 9, 29, 15, 4);

    Generator(114, 15, 19, 10, 4);
    Generator(115, 8, 27, 10, 4);
    Generator(116, 12, 35, 5, 32);

    Generator(117, 17, 2, 10, 4);
    Generator(118, 31, 21, 10, 4);
    Generator(119, 33, 20, 10, 4);

    Generator(120, 39, 24, 10, 4);
    Generator(121, 42, 22, 10, 4);
    Generator(122, 8, 28, 10, 4);
}

void PlaceGensPart6B()
{
    Generator(108, 6, 17, 10, 4);
    Generator(109, 2, 14, 10, 4);
    Generator(110, 3, 15, 10, 4);

    Generator(111, 27, 31, 5, 8);
    Generator(112, 27, 31, 5, 8);

    Generator(113, 18, 5, 10, 4);

    Generator(114, 6, 17, 10, 4);
    Generator(115, 5, 39, 5, 8);
    Generator(116, 4, 41, 5, 8);

    Generator(117, 2, 14, 10, 4);
    Generator(118, 3, 15, 10, 4);
    Generator(119, 10, 22, 10, 4);

    Generator(120, 28, 9, 15, 4);
    Generator(121, 33, 20, 15, 4);
    Generator(122, 15, 19, 10, 4);
}

void PlaceGensPart6C()
{
    Generator(108, 25, 3, 5, 8);
    Generator(109, 24, 4, 5, 8);
    Generator(110, 25, 3, 5, 8);

    Generator(111, 26, 33, 5, 8);
    Generator(112, 26, 33, 5, 8);

    Generator(113, 23, 13, 10, 4);

    Generator(114, 25, 3, 5, 8);
    Generator(115, 24, 4, 5, 8);
    Generator(116, 25, 3, 5, 8);

    Generator(117, 26, 33, 5, 8);
    Generator(118, 25, 3, 5, 8);
    Generator(119, 30, 21, 10, 4);

    Generator(120, 36, 12, 15, 1);
    Generator(121, 25, 3, 10, 8);
    Generator(122, 43, 13, 10, 4);
}

void PlaceGensPart7A()
{
    Generator(123, 19, 25, 10, 1);
    Generator(124, 11, 0, 10, 4);
    Generator(125, 33, 20, 10, 4);
    Generator(126, 20, 37, 8, 8);
    Generator(127, 41, 40, 8, 8);
    Generator(128, 36, 12, 20, 1);
    Generator(129, 37, 36, 18, 4);
    Generator(130, 17, 2, 18, 4);
    Generator(131, 16, 2, 18, 4);
    Generator(132, 39, 24, 15, 4);

    Generator(133, 31, 21, 10, 4);
    Generator(134, 33, 20, 10, 4);
    Generator(135, 9, 29, 10, 4);
    FrameTimer(1, PlaceGensPart8A);
}

void PlaceGensPart7B()
{
    Generator(123, 2, 14, 10, 1);
    Generator(124, 3, 15, 10, 4);
    Generator(125, 15, 19, 10, 4);
    Generator(126, 16, 2, 15, 4);
    Generator(127, 18, 5, 15, 4);
    Generator(128, 27, 31, 10, 4);
    Generator(129, 27, 31, 10, 4);
    Generator(130, 27, 31, 10, 4);
    Generator(131, 12, 30, 10, 4);
    Generator(132, 5, 39, 5, 8);

    Generator(133, 2, 14, 10, 4);
    Generator(134, 3, 15, 10, 4);
    Generator(135, 6, 17, 10, 4);
    FrameTimer(1, PlaceGensPart8B);
}

void PlaceGensPart7C()
{
    Generator(123, 24, 4, 10, 4);
    Generator(124, 25, 3, 10, 4);
    Generator(125, 43, 13, 10, 4);
    Generator(126, 26, 33, 10, 4);
    Generator(127, 47, 34, 15, 2);
    Generator(128, 25, 3, 10, 4);
    Generator(129, 24, 4, 10, 4);
    Generator(130, 23, 13, 10, 4);
    Generator(131, 25, 3, 10, 4);
    Generator(132, 24, 4, 10, 4);

    Generator(133, 44, 7, 15, 1);
    Generator(134, 43, 13, 10, 4);
    Generator(135, 25, 3, 5, 8);
    FrameTimer(1, PlaceGensPart8C);
}

void PlaceGensPart8A()
{
    Generator(136, 10, 22, 10, 4);
    Generator(137, 9, 29, 10, 4);
    Generator(138, 12, 30, 10, 4);
    Generator(139, 21, 38, 10, 1);
    Generator(140, 15, 19, 10, 4);
    Generator(141, 8, 28, 10, 4);
    Generator(147, 1, 32, 5, 8);
    Generator(148, 20, 37, 5, 8);
    Generator(149, 19, 25, 5, 1);
    Generator(150, 16, 2, 15, 4);
    Generator(151, 20, 37, 10, 16);
    Generator(152, 11, 0, 10, 4);
    Generator(153, 37, 36, 15, 4);
    Generator(154, 37, 36, 10, 4);
    Generator(155, 8, 28, 10, 4);
    FrameTimer(1, PlaceGensPart9A);
}

void PlaceGensPart8B()
{
    Generator(136, 33, 20, 10, 4);
    Generator(137, 31, 21, 10, 4);
    Generator(138, 34, 26, 10, 1);
    Generator(139, 39, 24, 10, 4);
    Generator(140, 44, 7, 15, 2);
    Generator(141, 18, 5, 10, 4);
    Generator(147, 27, 31, 5, 8);
    Generator(148, 27, 31, 5, 8);
    Generator(149, 27, 31, 5, 8);
    Generator(150, 2, 14, 10, 4);
    Generator(151, 3, 15, 10, 4);
    Generator(152, 6, 17, 10, 4);
    Generator(153, 35, 11, 15, 1);
    Generator(154, 2, 14, 5, 32);
    Generator(155, 27, 31, 5, 32);
    FrameTimer(1, PlaceGensPart9B);
}

void PlaceGensPart8C()
{
    Generator(136, 24, 4, 3, 16);
    Generator(137, 24, 4, 3, 16);
    Generator(138, 25, 3, 3, 16);
    Generator(139, 25, 3, 3, 16);
    Generator(140, 23, 13, 10, 4);
    Generator(141, 24, 4, 5, 18);
    Generator(147, 25, 3, 5, 8);
    Generator(148, 25, 3, 5, 8);
    Generator(149, 26, 33, 5, 8);
    Generator(150, 43, 13, 10, 4);
    Generator(151, 47, 34, 15, 1);
    Generator(152, 23, 13, 10, 4);
    Generator(153, 24, 4, 3, 32);
    Generator(154, 24, 4, 5, 32);
    Generator(155, 25, 3, 5, 32);
    FrameTimer(1, PlaceGensPart9C);
}

void PlaceGensPart9A()
{
    Generator(157, 16, 2, 10, 4);
    Generator(158, 33, 20, 10, 4);
    Generator(159, 41, 40, 5, 16);
    Generator(160, 26, 33, 5, 16);
    Generator(161, 10, 22, 15, 4);
    Generator(162, 12, 30, 10, 4);
    Generator(165, 8, 28, 10, 4);
    Generator(166, 9, 29, 10, 4);
    Generator(168, 7, 27, 10, 4);
    Generator(169, 12, 30, 10, 4);
    Generator(172, 21, 38, 15, 4);
    Generator(173, 16, 2, 10, 4);
    Generator(174, 15, 19, 10, 4);
    Generator(175, 8, 28, 10, 4);
    FrameTimer(1, PlaceGensLastPart);
}

void PlaceGensPart9B()
{
    Generator(157, 2, 14, 10, 4);
    Generator(158, 3, 15, 10, 4);
    Generator(159, 27, 31, 5, 16);
    Generator(160, 27, 31, 5, 16);
    Generator(161, 31, 21, 10, 4);
    Generator(162, 33, 20, 10, 4);
    Generator(165, 19, 25, 15, 1);
    Generator(166, 17, 2, 10, 4);
    Generator(168, 29, 9, 10, 4);
    Generator(169, 42, 22, 10, 4);

    Generator(172, 12, 30, 10, 4);
    Generator(173, 4, 41, 1, 32);
    Generator(174, 5, 39, 1, 32);
    Generator(175, 6, 17, 5, 8);
    FrameTimer(1, PlaceGensLastPart);
}

void PlaceGensPart9C()
{
    Generator(157, 24, 4, 5, 16);
    Generator(158, 24, 4, 5, 16);
    Generator(159, 25, 3, 5, 16);
    Generator(160, 25, 3, 5, 16);
    Generator(161, 24, 4, 5, 16);
    Generator(162, 24, 4, 5, 16);
    Generator(165, 24, 4, 5, 16);
    Generator(166, 24, 4, 5, 16);
    Generator(168, 25, 3, 5, 16);
    Generator(169, 25, 3, 5, 16);
    Generator(172, 25, 3, 5, 16);
    Generator(173, 24, 4, 5, 16);
    Generator(174, 24, 4, 5, 16);
    Generator(175, 24, 4, 5, 16);
    FrameTimer(1, PlaceGensLastPart);
}

void PlaceGensLastPart()
{
    Generator(176, 35, 11, 15, 4);
}

void PlaceMovingGenA()
{
    int ptr = CreateObject("AmbBeachBirds", 75);
    CreateObject("AmbBeachBirds", 76);
    CreateObject("AmbBeachBirds", 77);
    CreateObject("AmbBeachBirds", 78);
    SetOwner(Generator(75, 9, 29, 10, 4), ptr);
    SetOwner(Generator(76, 8, 28, 10, 4), ptr + 1);
    SetOwner(Generator(77, 33, 20, 10, 4), ptr + 2);
    SetOwner(Generator(78, 34, 26, 10, 2), ptr + 3);
    LookWithAngle(ptr + 3, 0);
    LookWithAngle(ptr + 2, 45);
    LookWithAngle(ptr + 1, 90);
    LookWithAngle(ptr, 135);
    FrameTimerWithArg(1, ptr, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 1, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 2, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 3, MovingCircleGen);
    Generator(93, 7, 27, 10, 4);
    Generator(94, 15, 19, 10, 4);
    Generator(95, 33, 20, 10, 4);
    Generator(96, 33, 20, 10, 4);
    Generator(97, 19, 25, 10, 1);
    Generator(98, 42, 22, 10, 4);
    FrameTimer(1, PlaceGensPart6A);
}

void PlaceMovingGenB()
{
    int ptr = CreateObject("AmbBeachBirds", 75);
    CreateObject("AmbBeachBirds", 76);
    CreateObject("AmbBeachBirds", 77);
    CreateObject("AmbBeachBirds", 78);
    SetOwner(Generator(75, 18, 5, 10, 4), ptr);
    SetOwner(Generator(76, 3, 15, 10, 4), ptr + 1);
    SetOwner(Generator(77, 18, 5, 10, 4), ptr + 2);
    SetOwner(Generator(78, 3, 15, 10, 4), ptr + 3);
    LookWithAngle(ptr + 3, 0);
    LookWithAngle(ptr + 2, 45);
    LookWithAngle(ptr + 1, 90);
    LookWithAngle(ptr, 135);
    FrameTimerWithArg(1, ptr, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 1, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 2, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 3, MovingCircleGen);
    Generator(93, 31, 21, 10, 4);
    Generator(94, 31, 21, 10, 4);
    Generator(95, 3, 15, 10, 4);
    Generator(96, 27, 31, 10, 4);
    Generator(97, 10, 22, 10, 4);
    Generator(98, 5, 39, 10, 4);
    FrameTimer(1, PlaceGensPart6B);
}

void PlaceMovingGenC()
{
    int ptr = CreateObject("AmbBeachBirds", 75);
    CreateObject("AmbBeachBirds", 76);
    CreateObject("AmbBeachBirds", 77);
    CreateObject("AmbBeachBirds", 78);
    SetOwner(Generator(75, 23, 13, 10, 4), ptr);
    SetOwner(Generator(76, 24, 4, 5, 16), ptr + 1);
    SetOwner(Generator(77, 25, 3, 5, 16), ptr + 2);
    SetOwner(Generator(78, 26, 33, 5, 16), ptr + 3);
    LookWithAngle(ptr + 3, 0);
    LookWithAngle(ptr + 2, 45);
    LookWithAngle(ptr + 1, 90);
    LookWithAngle(ptr, 135);
    FrameTimerWithArg(1, ptr, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 1, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 2, MovingCircleGen);
    FrameTimerWithArg(1, ptr + 3, MovingCircleGen);
    Generator(93, 25, 3, 10, 4);
    Generator(94, 25, 3, 10, 4);
    Generator(95, 43, 13, 10, 4);
    Generator(96, 24, 4, 5, 16);
    Generator(97, 26, 33, 5, 16);
    Generator(98, 24, 4, 5, 16);
    FrameTimer(1, PlaceGensPart6C);
}

void MovingCircleGen(int subUnit)
{
    int gen = GetOwner(subUnit), angle = GetDirection(subUnit) * 2;

    if (CurrentHealth(gen))
    {
        MoveObject(gen, LocationX(80) + MathSine(angle + 90, 169.0), LocationY(80) + MathSine(angle, 169.0));
        LookWithAngle(subUnit, (GetDirection(subUnit) + 1) % 180);
        FrameTimerWithArg(1, subUnit, MovingCircleGen);
    }
}
