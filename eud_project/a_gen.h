
#include "lzz99mh_utils.h"
#include "libs/unitstruct.h"
#include "libs/queueTimer.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"
#include "libs/bind.h"

#define SPAWN_RATE_HIGH 0
#define SPAWN_RATE_NORMAL 1
#define SPAWN_RATE_LOW 2
#define SPAWN_RATE_VERY_LOW 3
#define SPAWN_RATE_VERYVERY_LOW 4

#define SPAWN_LIMIT_HIGH 0
#define SPAWN_LIMIT_NORMAL 1
#define SPAWN_LIMIT_LOW 2
#define SPAWN_LIMIT_SINGULAR 3

#define GROUP_thirdPartWalls3 19
#define GROUP_thirdPartWalls4 20

void MonsterGeneratorIdTable(short input, short *dest)
{
    short *pTable;
    short idTable[97];

    if (!pTable)
    {
        pTable=idTable;
        
        idTable[OBJ_URCHIN%97]=OBJ_URCHIN_GENERATOR;
        idTable[OBJ_BAT%97]=OBJ_BAT_GENERATOR;
        idTable[OBJ_GIANT_LEECH%97]=OBJ_GIANT_LEECH_GENERATOR;
        idTable[OBJ_ALBINO_SPIDER%97]=OBJ_ALBINO_SPIDER_GENERATOR;
        idTable[OBJ_GHOST%97]=OBJ_GHOST_GENERATOR;
        idTable[OBJ_WOLF%97]=OBJ_WOLF_GENERATOR;
        idTable[OBJ_ARCHER%97]=OBJ_ARCHER_GENERATOR;
        idTable[OBJ_SWORDSMAN%97]=OBJ_SWORDSMAN_GENERATOR;
        idTable[OBJ_SCORPION%97]=OBJ_SCORPION_GENERATOR;
        idTable[OBJ_SPIDER%97]=OBJ_SPIDER_GENERATOR;
        idTable[OBJ_BLACK_BEAR%97]=OBJ_BEAR_GENERATOR;
        idTable[OBJ_BEAR%97]=OBJ_BEAR_GENERATOR;
        idTable[OBJ_WIZARD%97]=OBJ_WIZARD_GENERATOR;
        idTable[OBJ_EVIL_CHERUB%97]=OBJ_EVIL_CHERUB_GENERATOR;
        idTable[OBJ_SHADE%97]=OBJ_SHADE_GENERATOR;
        idTable[OBJ_OGRE_WARLORD%97]=OBJ_OGRE_WARLORD_GENERATOR;
        idTable[OBJ_GRUNT_AXE%97]=OBJ_GRUNT_AXE_GENERATOR;
        idTable[OBJ_OGRE_BRUTE%97]=OBJ_OGRE_BRUTE_GENERATOR;
        idTable[OBJ_SKELETON%97]=OBJ_SKELETON_GENERATOR;
        idTable[OBJ_HORRENDOUS%97]=OBJ_HORRENDOUS_GENERATOR;
        idTable[OBJ_TROLL%97]=OBJ_TROLL_GENERATOR;
        idTable[OBJ_VILE_ZOMBIE%97]=OBJ_VILE_ZOMBIE_GENERATOR;
        idTable[OBJ_FLYING_GOLEM%97]=OBJ_FLYING_GOLEM_GENERATOR;
        idTable[OBJ_SKELETON_LORD%97]=OBJ_SKELETON_LORD_GENERATOR;
        idTable[OBJ_LICH%97]=OBJ_LICH_GENERATOR;
        idTable[OBJ_BOMBER_BLUE%97]=OBJ_BOMBER_GENERATOR;
        idTable[OBJ_DEMON%97]=OBJ_DEMON_GENERATOR;
        idTable[OBJ_MECHANICAL_GOLEM%97]=OBJ_MECHANICAL_GOLEM_GENERATOR;
        idTable[OBJ_HECUBAH_WITH_ORB%97]=OBJ_WIZARD_GREEN_GENERATOR;
        idTable[OBJ_BEHOLDER%97]=OBJ_BEHOLDER_GENERATOR;
        idTable[OBJ_EMBER_DEMON%97]=OBJ_EMBER_DEMON_GENERATOR;
        idTable[OBJ_SPITTING_SPIDER%97]=OBJ_SPITTING_SPIDER_GENERATOR;
    }
    dest[0]=idTable[input%97];
}

void ProduceMonsterGenerator(float xpos, float ypos, int unitSlot1, int unitSlot2, int unitSlot3, int *pDestGen)
{
    int gen = CreateObjectAt("MonsterGenerator", xpos, ypos);
    int *ptr = UnitToPtr(gen);

    if (pDestGen)
        pDestGen[0] = gen;
    int *uec = ptr[187];

    int *slotPtr;
    AllocateXtraObjectSpec(unitSlot1, &slotPtr);
    if (slotPtr[4]&UNIT_FLAG_RESPAWN)
        slotPtr[4]^=UNIT_FLAG_RESPAWN;
    uec[0] = slotPtr;
    AllocateXtraObjectSpec(unitSlot2, &slotPtr);
    uec[4] = slotPtr;
    AllocateXtraObjectSpec(unitSlot3, &slotPtr);
    uec[8] = slotPtr;
    uec[23] = 1;

    short genmodel;

    MonsterGeneratorIdTable(unitSlot1, &genmodel);
    if (genmodel)
        ptr[1]=genmodel;
}

void ModificationMonsterGeneratorSummonInfo(int mobgen, int delayLv, int capaLv)
{
    int *ptr = UnitToPtr(mobgen);
    int *uec = ptr[187];

    delayLv &= 7;
    capaLv &= 7;

    uec[20] = delayLv|(delayLv<<8)|(delayLv<<16)|(capaLv<<24);
    uec[21] = capaLv|(capaLv<<8);
}

void SetMonsterGeneratorCallbackOnProduce(int mobgen, int functionId)
{
    int *ptr=UnitToPtr(mobgen);
    int *uec=ptr[187];

    uec[17] = functionId;
}

void ProduceTestGenerator(int locationId)
{
    int mobgen;

    ProduceMonsterGenerator(LocationX(locationId), LocationY(locationId), 1346, 1346, 1346, &mobgen);
    ModificationMonsterGeneratorSummonInfo(mobgen, 3, 3);
    SetMonsterGeneratorCallbackOnProduce(mobgen, GenOnProduce);
}

void OnGeneratorMonsterDeath(int mob) //virtual
{
    DeleteObjectTimer(mob, 150);
}

void GetUnitOnDeath()
{
    int *spawnClass = GetMemory( 0x81aebc );
    
    --spawnClass[35];

    //ec+0x890 = obelisk ptr
    //ec+0x894 = dwordptr->ptr that it's me
    int *ptr = UnitToPtr(SELF);
    int *ec = ptr[187];
    int *mobgenPtr = ec[548];

    if (mobgenPtr)
    {
        int *mobgenEc = mobgenPtr[187];
        int *curCount = mobgenEc + 86;

        if (curCount[0] & 0xff)
            --curCount[0];
    }
    OnGeneratorMonsterDeath(GetTrigger());

    // UniPrintToAll(IntToString(ec[548]));
    // UniPrintToAll(IntToString(ec[549]));
}

void GenProduceProcPkModeOnly()
{
    int *pSpawnClassOff = 0x81aebc;
    int *spawnClass = pSpawnClassOff[0];
    int *produceSlot = spawnClass[28];

    produceSlot[2] = produceSlot-28;
    produceSlot[4] = 0xacacacac;
    produceSlot[5] = 0xacacacac;
    produceSlot[6] = 0xacacacac;

    spawnClass[24] = produceSlot;
    spawnClass[28] = 0;
}

void OnGeneratorSummoned(int gen, int mob) //virtual
{
    Effect("SENTRY_RAY", GetObjectX(gen),GetObjectY(gen),GetObjectX(mob),GetObjectY(mob));
}

void GenOnProduce()
{
    SetCallback(OTHER, 5, GetUnitOnDeath);
    GenProduceProcPkModeOnly();
    OnGeneratorSummoned(GetTrigger(), GetCaller());
}

void OnGeneratorDestroyed(int gen)
{
    GreenExplosion(GetObjectX(gen), GetObjectY(gen));
    PlaySoundAround(gen, SOUND_MonsterGeneratorDie);
}

void GenOnDestroyed()
{
    OnGeneratorDestroyed(GetTrigger());
    Delete(SELF);
}

int CreateMonsterGenerator(short locationId, short mobId, int delayLv, int capaLv)
{
    int mobgen;

    ProduceMonsterGenerator(LocationX(locationId), LocationY(locationId), mobId, mobId, mobId, &mobgen);
    ModificationMonsterGeneratorSummonInfo(mobgen, delayLv, capaLv);
    SetMonsterGeneratorCallbackOnProduce(mobgen, GenOnProduce);
    SetUnitCallbackOnDeath(mobgen, GenOnDestroyed);
    SetUnitMaxHealth(mobgen, 475);
    return mobgen;
}

void InitGenSetting()
{
    int *pSpawnClassOff = 0x81aebc;
    int *spawnClass = pSpawnClassOff[0];

    spawnClass[24] = spawnClass[29] + 0xa64;
    spawnClass[25] = spawnClass[29];
}

int FishBigBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1752394054; arr[1] = 6777154; arr[17] = 2600; arr[19] = 80; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1073741824; arr[27] = 2; arr[28] = 1116471296; arr[29] = 65; 
		arr[31] = 10; arr[32] = 3; arr[33] = 6; arr[59] = 5544288; arr[60] = 1329; 
		arr[61] = 46905600; 
	pArr = arr;
	return pArr;
}

void FishBigSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 2600;	hpTable[1] = 2600;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = FishBigBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

