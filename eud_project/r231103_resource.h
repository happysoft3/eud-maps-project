
#include "libs/imageutil.h"
#include "libs/clientside.h"

#define IMAGE_VECTOR_SIZE 32

void ImageResourceRucciL()
{ }
void ImageResourceRucciR()
{ }
void ImageResourceArcherL()
{ }
void ImageResourceArcherR()
{ }

void ImageResourceML()
{ }
void ImageResourceMR()
{ }

void ImageResourceAmulet()
{ }

void SoundData()
{ }

void ImageResourceExitMarkIN()
{ }
void ImageResourceExitMarkOUT()
{ }

void ImageResourceMob4L()
{ }
void ImageResourceMob4R()
{ }
void ImageResourceMob5L()
{ }
void ImageResourceMob5R()
{ }
void ImageResourceMob6L()
{ }
void ImageResourceMob6R()
{ }

void ImageResourceVictory()
{ }

void introduceCustomImage(int *pImgVector)
{
    int index;

    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceRucciL)+4, 119558, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceRucciR)+4, 119570, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceArcherL)+4, 119137, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceArcherR)+4, 119148, index++);    
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceML)+4, 120608, index++);    
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMR)+4, 120619, index++);    
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceAmulet)+4, 112960, index++);
    
    short exitMessage[64];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("!!비상 탈출구- 클릭 시 이동!!"), exitMessage);
    AddStringSprite(pImgVector, 0xF814, exitMessage, 16096, index++);
    AddStringSprite(pImgVector, 0xF814, exitMessage, 16097, index++);
    AddStringSprite(pImgVector, 0x021F, exitMessage, 16098, index++);
    AddStringSprite(pImgVector, 0x021F, exitMessage, 16099, index++);
    AddStringSprite(pImgVector, 0x54AC, exitMessage, 16100, index++);
    AddStringSprite(pImgVector, 0x54AC, exitMessage, 16101, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceExitMarkIN)+4, 16090, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceExitMarkIN)+4, 16091, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceExitMarkOUT)+4, 16092, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceExitMarkOUT)+4, 16093, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceExitMarkOUT)+4, 16094, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceExitMarkIN)+4, 16095, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMob4L)+4, 113854, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMob4R)+4, 113870, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMob5L)+4, 120768, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMob5R)+4, 120780, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMob6L)+4, 119746, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMob6R)+4, 119754, index++);
    ExtractMapBgm("..\\bgm444.mp3", SoundData);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceVictory)+4, 112992, index++);
}

void InitializeCommonImage()
{
    int imgVector = CreateImageVector(IMAGE_VECTOR_SIZE);

    InitializeImageHandlerProcedure(imgVector);
    introduceCustomImage(imgVector);
}



