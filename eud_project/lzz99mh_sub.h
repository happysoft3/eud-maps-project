
#include "lzz99mh_gvar.h"
#include "lzz99mh_utils.h"
#include "lzz99mh_gen.h"
#include "libs/printutil.h"
#include "libs/waypoint.h"
#include "libs/playerinfo.h"
#include "libs/buff.h"
#include "libs/bind.h"
#include "libs/format.h"

void onSoulGateTriggered()
{
    if (!CurrentHealth(OTHER))
        return;

    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int respawn=GetUserRespawnMark(pIndex);

    if (DistanceUnitToUnit(SELF, respawn) > 23.0)
    {
        MoveObject(respawn, GetObjectX(SELF), GetObjectY(SELF));
        Effect("YELLOW_SPARKS", GetObjectX(SELF),GetObjectY(SELF), 0.0, 0.0);
        PlaySoundAround(SELF, SOUND_SoulGateTouch);
        UniPrint(OTHER, "이 영혼의 문 위치로 연결되었습니다");
    }
}

void explainWhatSoulgateDoes()
{
    if (IsPlayerUnit(OTHER))
        UniPrint(OTHER, "이것은 영혼의 문 입니다. 이것을 만지면, 마을에서 이곳으로 빠르게 이동할 수 있습니다");
}

void createSoulGate(short location)
{
    float x=LocationX(location),y=LocationY(location);
    int gate=DummyUnitCreateById(OBJ_BOMBER_GREEN, x,y);

    SetCallback(gate,9,onSoulGateTriggered);
    int deco=CreateObjectById(OBJ_TORCH_INVENTORY, x,y);

    UnitNoCollide(deco);
    Frozen(deco, TRUE);
    SetUnitCallbackOnPickup(deco, explainWhatSoulgateDoes);
}

void DelayObjectOff(int obj)
{
    if (IsObjectOn(obj))
    {
        ObjectOff(obj);
    }
}

void ElevatorTurnOn(int elev)
{
    ObjectOn(elev);
    PushTimerQueue(24, elev, DelayObjectOff);
}

void ElevatorVisibleOn(int elev)
{
    Enchant(elev, EnchantList(ENCHANT_INVISIBLE), 0.0);
}

void ElevatorVisibleOff(int elev)
{
    EnchantOff(elev, EnchantList(ENCHANT_INVISIBLE));
}

void ElevAction1(int *pElev)
{
    ElevatorTurnOn(pElev[0]);
}

void ElevAction2(int *pElev)
{
    UnitNoCollide(pElev[0]);
    UnitNoCollide(pElev[1]);
    UnitNoCollide(pElev[2]);
    ElevatorVisibleOff(pElev[1]);
    ElevatorVisibleOn(pElev[2]);
    ElevatorTurnOn(pElev[1]);
    
}

void ElevAction3(int *pElev)
{
    ElevatorTurnOn(pElev[1]);
}

void ElevAction4(int *pElev)
{
    UnitNoCollide(pElev[0]);
    UnitNoCollide(pElev[1]);
    UnitNoCollide(pElev[2]);
    ElevatorVisibleOff(pElev[2]);
    ElevatorVisibleOn(pElev[1]);
    ElevatorTurnOn(pElev[0]);
}

void invokeElevatorAction(int fn, int *pElev)
{
    Bind(fn, &fn + 4);
}

int ThreeElevState(int *set)
{
    int state;

    if (set)
        state=set[0]&TRUE;
    return state;
}

void onDistributeElevOnLoop(int *pElev)
{
    int order;
    int actions[] = {ElevAction1, ElevAction2, ElevAction3, ElevAction4};

    if (ThreeElevState(0))
    {
        invokeElevatorAction(actions[order], pElev);
        order = (++order) % 4;
    }
    PushTimerQueue(60, pElev, onDistributeElevOnLoop);
}

void ResumeThreeElevator()
{
    int on = TRUE;

    ThreeElevState(&on);
}

void threeElevShutdown()
{
    int off=FALSE;

    ThreeElevState(&off);
}

void InitializeThreeFloorElevators()
{
    int eleves[]={Object("area1baseElev"), Object("area1elev"), Object("area1mid"), };

    UnitNoCollide(eleves[1]);
    Enchant(eleves[1], EnchantList(ENCHANT_INVISIBLE), 0.0);
    PushTimerQueue(60, eleves, onDistributeElevOnLoop);
    ResumeThreeElevator();
    PushTimerQueue(70, 0, threeElevShutdown);
}

int getArrowTrap(int *p)
{
    int *t;

    if (!t)
        t=p;
    return t;
}

void StopArrowTrap(int *p)
{
    while (p[0])
    {
        ObjectOff(p[0]);
        p=&p[1];
    }
}

void StartArrowTrap03()
{
    int *src = getArrowTrap(0);
    int *p = &src[20];
    
    PushTimerQueue(1, p, StopArrowTrap);
    while (p[0])
    {
        ObjectOn(p[0]);
        p=&p[1];
    }
}

void StartArrowTrap02()
{
    int *src = getArrowTrap(0);
    int *p = &src[10];
    
    PushTimerQueue(1, p, StopArrowTrap);
    while (p[0])
    {
        ObjectOn(p[0]);
        p=&p[1];
    }
}

void StartArrowTrap01()
{
    int *p = getArrowTrap(0);
    
    PushTimerQueue(1, p, StopArrowTrap);
    while (p[0])
    {
        ObjectOn(p[0]);
        p=&p[1];
    }
}

void initialTraps(char *name, int count, int *pDest)
{
    int u = 0;
    char buff[16];

    for (u ; u < count ; u+=1)
    {
        int args[]={name, u+1};
        NoxSprintfString(buff, "%s%d", args, sizeof(args));
        pDest[0]=Object(ReadStringAddressEx(buff));
        pDest=&pDest[1];
    }
}

void InitialSubPart()
{
    createSoulGate(192); //test
    createSoulGate(190);
    createSoulGate(191);
    createSoulGate(198);
    createSoulGate(199);
    createSoulGate(222);
    createSoulGate(224);
    createSoulGate(226);
    createSoulGate(227);
    createSoulGate(229);
    createSoulGate(231);
    createSoulGate(288);
    createSoulGate(93);
    createSoulGate(307);
    createSoulGate(333);
    createSoulGate(335);
    createSoulGate(337);
    createSoulGate(395);

    int traps[30];

    initialTraps(StringUtilGetScriptStringPtr("awt1"), 7, &traps[0]);
    initialTraps(StringUtilGetScriptStringPtr("awt2"), 8, &traps[10]);
    initialTraps(StringUtilGetScriptStringPtr("awt3"), 8, &traps[20]);

    getArrowTrap(traps);
}

void tryLanternPickup()
{
    UniPrint(OTHER, "랜턴이 바닥에 떨어져있네요...?!");
}

int dispositionLanternSingle(float x, float y)
{
    int o=CreateObjectById(OBJ_LANTERN,x,y);

    Frozen(o,TRUE);
    SetUnitCallbackOnPickup(o, tryLanternPickup);
    return o;
}

void multiplePlaceLantern(int prev)
{
    int o=dispositionLanternSingle(GetObjectX(prev)-23.0,GetObjectY(prev)+23.0);

    if (!IsVisibleTo(o,prev))
    {
        Delete(o);
        return;
    }
    PushTimerQueue(1,o,multiplePlaceLantern);
}

void PlaceGenComplex() //virtual
{ }

#define GROUP_genComplexFrontWalls 21
#define GROUP_genComplexBackWalls 22

void OpenGenComplex()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_genComplexFrontWalls);
    PlaceGenComplex();
    multiplePlaceLantern(dispositionLanternSingle(LocationX(339),LocationY(339)));
    multiplePlaceLantern(dispositionLanternSingle(LocationX(340),LocationY(340)));
}

void OpenExitComplex()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_genComplexBackWalls);
}

void startSBZone()
{
    int dup;

    ObjectOff(SELF);
    if (dup)
        return;
    dup=TRUE;
    PlaySoundAround(SELF, SOUND_SpikeBlockMove);
    Move(Object("sb11"),353);
    Move(Object("sb12"),356);
    Move(Object("sb21"),345);
    Move(Object("sb22"),346);
    Move(Object("sb31"),352);
    Move(Object("sb32"),351);
}

int FonTrapControl(int **get)
{
    int fonTimer;

    if (get)
        get[0]=&fonTimer;
    return fonTimer;
}

void loopFonTrap(int timer)
{
    if (!timer)
        return;

    int fon=CreateObjectById(OBJ_DEATH_BALL, LocationX(357),LocationY(357));

    PushObjectTo(fon, 7.0, 7.0);
    PlaySoundAround(fon, SOUND_ForceOfNatureRelease);
    PushTimerQueue(timer, FonTrapControl(NULLPTR), loopFonTrap);
}

void StartFonTrap()
{
    ObjectOff(SELF);
    int *pTimer, val=81;

    FonTrapControl(&pTimer);
    pTimer[0]=val;
    PushTimerQueue(45, val, loopFonTrap);

    int trap=Object("fonSkull");

    ObjectOff(trap);
    LookWithAngle(trap, GetDirection(trap) + 128);
    ObjectOn(trap);

    CreateMonsterGenerator(364, OBJ_SPIDER, SPAWN_RATE_HIGH, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(365, OBJ_SPIDER, SPAWN_RATE_HIGH, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(366, OBJ_SPIDER, SPAWN_RATE_HIGH, SPAWN_LIMIT_NORMAL);
}

#define GROUP_fonGenWalls 24

void StopFonTrap()
{
    ObjectOff(SELF);
    int *pTimer;
    FonTrapControl(&pTimer);
    pTimer[0]=0;
    ObjectOff(Object("fonSkull"));
    WallGroupOpen(GROUP_fonGenWalls);
    CreateMonsterGenerator(358, OBJ_BOMBER_BLUE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(359, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(360, OBJ_BOMBER_BLUE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(361, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void openNorthSeWalls()
{
    ObjectOff(SELF);
    WallOpen(Wall(184,218));
    WallOpen(Wall(183,219));
}
#define GROUP_genDestroyWallEx2 26
void openGenDestw2()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_genDestroyWallEx2);
}

void openGenDestw1()
{
    ObjectOff(SELF);
    WallOpen(Wall(169,245));
    WallOpen(Wall(170,246));
    WallOpen(Wall(171,247));
    WallOpen(Wall(172,248));
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

