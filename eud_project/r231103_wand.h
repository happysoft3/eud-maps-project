
#include "r231103_gvar.h"
#include "r231103_oblivion.h"
#include "r231103_utils.h"
#include "libs/fixtellstory.h"
#include "libs/waypoint.h"
#include "libs/gui_window.h"
#include "libs/hash.h"
#include "libs/itemproperty.h"
#include "libs/buff.h"
#include "libs/network.h"

int getUpgradePay(int curLv)
{
    return 9000+(curLv*1500);
}

#define GUI_DIALOG_OBLIVION_BUY 1
#define GUI_DIALOG_SELL_ALL_GERM 2
#define GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM 3
#define GUI_DIALOG_MESSAGE_UPGRADE_WAND 4
#define GUI_DIALOG_MESSAGE_GOD_MODE 5

void popupMessageUpgradeWand(int messageId)
{
    char *p=_CLIENT_OPTION_TYPE_OFF_;
    int curLv=p[2];
    char message[192];
    int args[]={curLv, getUpgradePay(curLv)};

    NoxSprintfString(message, GetPopupMessage( messageId), args, sizeof(args));
    GUISetWindowScrollListboxText(GetDialogCtx(), ReadStringAddressEx(message), NULLPTR);
}

void onPopupMessageChanged(int messageId)
{
    if (messageId==GUI_DIALOG_MESSAGE_UPGRADE_WAND)
    {
        popupMessageUpgradeWand(messageId);
        return;
    }
    GUISetWindowScrollListboxText(GetDialogCtx(), GetPopupMessage(messageId), NULLPTR);
}

void deferredChangeDialogContext(int *pParams) //override
{
    int user=pParams[0];
    int message=pParams[1];

    FreeSmartMemEx(pParams);
    if (!CurrentHealth(user))
        return;
    char *ptr = _CLIENT_OPTION_TYPE_OFF_;

    if (user==GetHost())
    {
        ptr[2]=pParams[2];
        onPopupMessageChanged(message);
        return;
    }
    ClientSetMemory(user, &ptr[2], pParams[2]);
    ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, message);
}

void sendDialogMessage(int user, int messageId, int arg)
{
    int params[]={
        user,
        messageId,
        arg,
    };
    int *pMsg;

    AllocSmartMemEx(12, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[1])
    {
        // if (type[2])
        onPopupMessageChanged(type[1]);
        type[1]=0;
    }
    if (type[0])
    {
        type[1]=type[0];
        type[0]=0;
    }
    FrameTimer(3, ClientProcLoop);
}

//TODO. 망각의 지팡이를 구입, 강화를 위한 로직을 만듭니다//
void tradeOblivionWand()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=5000)
    {
        ChangeGold(OTHER, -5000);
        int count = SetInvulnerabilityItem(OTHER);
        char buff[128];

        NoxSprintfString(buff, "%d개 아이템이 처리되었습니다", &count,1);
        UniPrint(OTHER,ReadStringAddressEx(buff));
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descOblivionWand()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM,0);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "인벤토리 무적화");
}

void setOblivionUserHash(int pIndex, int shop, int bitState)
{
    int c;

    if (!HashGet(GenericHash(), shop, &c, FALSE))
        return;

    int shift = 1<<(pIndex+1);
    int *pBit =c;

    while (TRUE)
    {
        if (pBit[0] & shift)
        {
            if (bitState)
                break;
        }
        else
        {
            if (!bitState)
                break;
        }        
        pBit[0]^=shift;
        break;
    }
}

int getOblivionUserHashFlags(int pIndex, int shop)
{
    int c;

    if (!HashGet(GenericHash(), shop, &c, FALSE))
        return 0;
    int *flags=c;
    return flags[0]&(1<<(pIndex+1));
}

void onOblivionOrbUse()
{
    char *ptr=UnitToPtr(SELF);

    if (!ptr)
        return;

    char *pData=GetMemory(&ptr[0x2e0]);

    if (!pData)
        return;

    if (!CheckSignDelay(&pData[0x68], 24))
        return;

    ShotOblivionStaff(GetTrigger(), GetCaller());
}

int createOblivionWand(int owner)
{
    int orb=CreateObjectById(OBJ_OBLIVION_ORB, GetObjectX(owner),GetObjectY(owner));

    SetItemPropertyAllowAllDrop(orb);
    DisableOblivionItemPickupEvent(orb);
    SafetyPickup(owner,orb);
    SetUnitCallbackOnUseItem(orb,onOblivionOrbUse);
    return orb;
}

void onBuyOblivionResult()
{
    if (GetGold(OTHER)>=10000)
    {
        ChangeGold(OTHER,-10000);
        createOblivionWand(OTHER);
        UniPrint(OTHER, "결제가 완료되었습니다");
        return;
    }
    UniPrint(OTHER,"결제가 취소되었습니다- 잔액이 부족합니다");
}

void onUpgradeOblivionResult()
{
    int eq=PlayerGetEquipedWeapon(OTHER);

    if (GetUnitThingID(eq)!=OBJ_OBLIVION_ORB)
    {
        UniPrint(OTHER,"강화에 실패했습니다. 착용중인 무기가 망각의 지팡이 인지 다시 확인해 보세요");
        return;
    }

    int curLv=GetUnit1C(eq);

    if (curLv >= 15)
    {
        UniPrint(OTHER, "이미 최대로 강화되었습니다");
        return;
    }

    if (GetGold(OTHER)>=getUpgradePay(curLv))
    {
        ChangeGold(OTHER, -getUpgradePay(curLv));
        SetUnit1C(eq, ++curLv);
        PrintMessageFormatOne(GetCaller(), "강화성공! 망각의 지팡이 강화레밸 %d 이 되었습니다", curLv);
        return;
    }
    PrintMessageFormatOne(GetCaller(), "결제가 취소되었습니다- %d 골드가 필요합니다", getUpgradePay(curLv));
}

void onOblivionWandDialogEnd()
{
    if (GetAnswer(SELF)^1)
        return;

    if (getOblivionUserHashFlags(GetPlayerIndex(OTHER), GetTrigger()))
    {
        onUpgradeOblivionResult();
        return;
    }
    onBuyOblivionResult();
}

void onOblivionWandDialogStart()
{
    int eq=PlayerGetEquipedWeapon(OTHER);
    int eqCheck=GetUnitThingID(eq)==OBJ_OBLIVION_ORB;

    setOblivionUserHash(GetPlayerIndex(OTHER), GetTrigger(), eqCheck);
    if (!eqCheck)
    {
        sendDialogMessage(GetCaller(), GUI_DIALOG_OBLIVION_BUY,0);
        TellStoryUnitName("null", "MainBG.wnd:Loading", "망각의 지팡이 구입");
        return;
    }
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_UPGRADE_WAND, GetUnit1C(eq)+1);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "망각의 지팡이 강화");
}

void onSellGermDialogComplete()
{
    if (GetAnswer(SELF)^1)
        return;

    int pay=GermToMoney(OTHER);

    if (pay)
    {
        ChangeGold(OTHER, pay);
        PrintMessageFormatOne(GetCaller(), "소지하고 계신 보석을 모두 판매하여 %d 의 수익을 얻었습니다", pay);
        return;
    }
    UniPrint(OTHER, "소지하고 계신 보석이 아무것도 없습니다");
}

void onSellGermDialogStart()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_SELL_ALL_GERM,0);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "모든 보석 판매하기");
}

int SetInvulnerabilityItem(int unit)
{
    int count = 0, inv = GetLastItem(unit);

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void onInvincibleItemDialogEnd()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=3000)
    {
        ChangeGold(OTHER, -3000);
        int count = SetInvulnerabilityItem(OTHER);
        PrintMessageFormatOne(GetCaller(), "%d개 아이템이 처리되었습니다", count);
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void onInvincibleItemDialogStart()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM,0);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "아이템 무적화");
}

void onGodModeDialogEnd()
{
    if (GetAnswer(SELF)^1)
        return;

    int pIndex=GetPlayerIndex(OTHER);

    if (pIndex<0)
        return;

    if (GetGold(OTHER)>=50000)
    {
        if (PlayerClassCheckFlag(pIndex,PLAYER_FLAG_GOD_MODE))
        {
            UniPrint(OTHER, "이미 당신은 무적입니다");
            return;
        }
        ChangeGold(OTHER, -50000);
        GreenSparkFx(GetObjectX(OTHER),GetObjectY(OTHER));
        PlayerClassSetFlag(pIndex,PLAYER_FLAG_GOD_MODE);
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void onGodModeDialogStart()
{
    sendDialogMessage(GetCaller(), GUI_DIALOG_MESSAGE_GOD_MODE,0);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "무적 치트키 사용");
}

void InitialPlaceWandShop()
{
    int flags;
    int 
    unit=DummyUnitCreateById(OBJ_BOMBER, LocationX(13), LocationY(13));
    SetDialog(unit, "YESNO", onOblivionWandDialogStart, onOblivionWandDialogEnd);
    StoryPic(unit, "ShopKeeperMagicShopPic");
    HashPushback(GenericHash(), unit, &flags);

    unit=DummyUnitCreateById(OBJ_SWORDSMAN, LocationX(46), LocationY(46));
    SetDialog(unit, "YESNO", onInvincibleItemDialogStart, onInvincibleItemDialogEnd);
    StoryPic(unit, "FentonPic");
    LookWithAngle(unit,64);

    unit=DummyUnitCreateById(OBJ_WIZARD_WHITE, LocationX(47), LocationY(47));
    SetDialog(unit, "YESNO", onSellGermDialogStart, onSellGermDialogComplete);
    StoryPic(unit, "GlyndaPic");
    LookWithAngle(unit,64);

    unit=DummyUnitCreateById(OBJ_WIZARD, LocationX(48), LocationY(48));
    SetDialog(unit, "YESNO", onGodModeDialogStart, onGodModeDialogEnd);
    StoryPic(unit, "GlyndaPic");
    LookWithAngle(unit,64);
}

void initFillPopupMessageArray(string *pPopupMessage)
{
    pPopupMessage[GUI_DIALOG_OBLIVION_BUY]="망각의 지팡이를 구입하시겠습니까? 가격은 10,000 골드 입니다";
    pPopupMessage[GUI_DIALOG_SELL_ALL_GERM]="소지하고 계신 모든 보석을 저에게 판매 하시겠습니까?";
    pPopupMessage[GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM]="소지하고 계신 모든 아이템을 무적화 하시겠습니까? 가격은 3,000 골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_UPGRADE_WAND]="장착중인 망각의 지팡이를 강화하시겠습니까? [다음 강화레밸: %d, 가격: %d 골드]";
    pPopupMessage[GUI_DIALOG_MESSAGE_GOD_MODE]="무적 치트키를 사용하시겠어요? 가격은 50,000 골드 입니다";
}

void initDialog()
{
    int *wndptr=0x6e6a58;
    int ctx=NULLPTR;
    GUIFindChild(wndptr[0], 3901, &ctx);
    if (ctx!=NULLPTR)
        SetDialogCtx(ctx);
}

void InitPopupMessage(string *popupMessages)
{
    initDialog();
    // initspecialWeaponShop();
    initFillPopupMessageArray(popupMessages);
}



