
#include "libs/define.h"
#include "libs/potionex.h"
#include "libs/spellutil.h"
#include "libs/objectIDdefines.h"
#include "libs/queueTimer.h"
#include "libs/waypoint.h"
#include "libs/recovery.h"

int GetLastUnitID(){}//virtual

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void EmptyAll(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void CreateMoverFixEx(int targetUnit, int destLocation, float speed, int *result)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed);

    if (unitMover)
    {
        int *movPtr = UnitToPtr(unitMover), *unitPtr = UnitToPtr(targetUnit);

        if (movPtr != NULLPTR && unitPtr != NULLPTR)
        {
            unitPtr[10] = GetLastUnitID();
            int *ec = movPtr[187];
            ec[8] = unitPtr[10];
        }
    }
    if (result != NULLPTR)
        result[0] = unitMover;
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int *ptr = UnitToPtr(unit);

    if (ptr == NULLPTR)
        return;
    ptr[1] = 1385;

    int u = 0;
    while (u < 32)
        ptr[140 + (u++)] = 0x400;

    int *ecptr = ptr[187];

    ecptr[94] = 0xa0;
    ecptr[122] = VoiceList(7);

    int *colrarr = ecptr + 0x81c;

    colrarr[0] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[1] = grn | (blue << 8) | (red << 16) | (grn << 24);
    colrarr[2] = blue | (red << 8) | (grn << 16) | (blue << 24);
    colrarr[3] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[4] = grn | (blue << 8);
}

int LichLordBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 20; arr[19] = 100; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; 
		arr[29] = 50; arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; arr[57] = 5548288; 
		arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int HecubahWithOrbBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 250; 
		arr[18] = 100; arr[19] = 90; arr[21] = 1065353216; arr[24] = 1066192077; arr[25] = 1; 
		arr[26] = 6; arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; 
		arr[40] = 1852140903; arr[41] = 116; arr[53] = 1133903872; arr[55] = 22; arr[56] = 28; 
		link = &arr;
	}
	return link;
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4;
		arr[53] = 1128792064; arr[54] = 4; 
		link = &arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
        arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743;
        arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 15; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 1; 
		arr[35] = 2; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = &arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146;
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = &arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link = &arr;
	}
	return link;
}

void CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);

    if (!init)
    {
        init = 1;
        arr[5] = MonsterGoonProcess; arr[72] = MonsterStrongWhiteWizProcess; arr[30] = MonsterWeirdlingBeastProcess; arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process;
        arr[12] = MonsterFireSpriteProcess; arr[73] = MonsterWizardRedProcess; arr[29] = MonsterAirshipCaptainProcess;
    }
    int key = thingID % 97;

    if (arr[key])
        CallFunctionWithArg(arr[key], unit);
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}
#include "libs/winapi.h"
#define OBLIVION_TOOLTIP_BASE_OFF  0x4bf100
#define OBLIVION_TOOLTIP_OFF 0x4bf110
void EnableOblivionTooltipDetail()
{
    int oldProtect;

    WinApiVirtualProtect(OBLIVION_TOOLTIP_BASE_OFF, 1024, 0x40, &oldProtect);
    char *p = OBLIVION_TOOLTIP_OFF;

    p[1]=0x90;
    p[2]=0x90;
    p[65]=0x90;
    p[66]=0x90;
    WinApiVirtualProtect(OBLIVION_TOOLTIP_BASE_OFF, 1024, oldProtect, NULLPTR);
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}

void DrawStampStringProc(int singleset, string unitname, int *count, int *stampinfo, float *vectinfo)
{
    int totalcount = stampinfo[0], locationId = stampinfo[1];
    int prgress = stampinfo[2];
    int i;

    for (i = 1 ; i > 0 && count[0] < totalcount ; i <<= 1)
    {
        if (i & singleset)
            CreateObjectAtEx(unitname, LocationX(locationId), LocationY(locationId), NULLPTR);
		if ((count[0]++) % prgress == prgress - 1)
            TeleportLocationVector(locationId, -vectinfo[0], vectinfo[1]);
		else
            TeleportLocationVector(locationId, vectinfo[2], 0.0);
    }
}

void DrawStampString(string unitname, int *pMatrix, int matrixSize, int *stampinfo, float *vectinfo)
{
    int locationId = stampinfo[1];
    float xpos = LocationX(locationId), ypos = LocationY(locationId);
    int rep = 0, count = 0;

    while (rep < matrixSize)
        DrawStampStringProc(pMatrix[rep++], unitname, &count, stampinfo, vectinfo);
    TeleportLocation(locationId, xpos, ypos);
}

void StrTeleportRoom()
{
    int locationId = 219;
	int matrix[] = {1009247228, 75005828, 16779528, 4342308, 75760772, 554828048, 37879874, 4719868,
	    534847858, 239206978, 277381128, 18743809, 553632322, 536383624, 20906240, 1212423168, 
	    8388608, 2064384, 66078788, 545128576, 606077984, 8373256, 34611329, 33825728, 
	    1614839872, 268566031, 2097656, 63};
    int info[] = {868, locationId, 77};
    float vectarr[] = {152.0, 1.5, 2.0};
    
    DrawStampString("ManaBombOrb", matrix, sizeof(matrix), &info, &vectarr);
}

void StrBuySoldier()
{
    int locationId = 220;
	int matrix[] = {285476800, 551567344, 2081448128, 71368704, 272760961, 545521666, 2131102724, 34603016,
	    68149265, 138412094, 270565444, 1082130568, 134201608, 268403231, 1052, 8390656,
	    130023424, 33556352, 1623212064, 134242688, 8454272, 537002244, 34078208, 525328,
	    806356993, 1597488, 4202500, 1966142, 16711696 };
    int info[] = {899, locationId, 64};
    float vectarr[] = {126.0, 2.0, 2.0};

    DrawStampString("ManaBombOrb", matrix, sizeof(matrix), &info, &vectarr);
}

void StrGameOption()
{
	int locationId = 221;
    int matrix[] = { 275784704, 603463744, 1109430768, 16810240, 272779265, 67241474, 1091117060, 672081928,
	    69500958, 539037730, 547225666, 1082663044, 38273296, 37765646, 152044673, 2048,
	    608178178, 535822336, 276827904, 65473, 1090568961, 2114060548, 58982915, 67634194,
	    1050624, 270536776, 3194880, 8356128, 3932671};
	int info[] = {899, locationId, 64};
    float vectarr[] = {126.0, 2.0, 2.0};
		
    DrawStampString("ManaBombOrb", matrix, sizeof(matrix), &info, &vectarr);
}

void StrNewSkills()
{
    int locationId = 222;
    int matrix[] = {
	    1378345024, 540420, 1052656, 270608658, 33554436, 1246034952, 16713744, 268763408,
	    33826082, 539230208, 158893956, 252, 537267202, 2080376740, 2095135, 19472352, 
	    0, 1140850688, 2095252, 8192, 1076175868, 134217856, 2013339640, 2105362,
	    1073748736, 134512767, 405802992};
    int info[] = {837, locationId, 76};
    float vectarr[] = {150.0, 2.0, 2.0};

	DrawStampString("ManaBombOrb", matrix, sizeof(matrix), &info, &vectarr);
}

void StrMonsterHuntLocation()
{
    int locationId = 223;
    int matrix[] = {
	    0, 8192, 524296, 2095120, 2097152, 134350864, 268963840, 603456000,
	    34082816, 135396592, 134348832, 135266368, 268701700, 33562640, 268451856, 68158480,
	    4190240, 4198410, 2080641056, 33562633, 2079330560, 2081431552, 4061223, 274776068,
	    1884291200, 537403393, 3146752, 1342210083, 136314912, 33552448, 8396800, 260055312,
	    16400, 2097152, 1075972097, 541069344, 1610612736, 604242239, 1341145152, 64,
	    65536, 4210752, 1879064704, 16779263, 545275904, 2143322112, 1, 4194306,
	    8388670};
	int info[] = {1519, locationId, 101};
    float varr[] = {200.0, 2.0, 2.0};
	
    DrawStampString("ManaBombOrb", matrix, sizeof(matrix), &info, &varr);
}

void PutStampStrings()
{
    StrNewSkills();
    StrGameOption();
    FrameTimer(1, StrBuySoldier);
    FrameTimer(1, StrTeleportRoom);
    FrameTimer(1, StrMonsterHuntLocation);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

void SendPacketAll(char *packet, int packetLength)
{
    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x90, 0x53, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x1C, 0x31, 0xC0, 0xC3,
    };
    int args[]={
        packet,0xff,packetLength,
    }; //0xff or 0x9f
    invokeRawCode(code, args);
}

void SendPacketSpecifyOne(int user, char *packet, int packetLength)
{
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0)
        return;
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
    };
    int args[]={
        packet,pIndex,packetLength,
    };
    invokeRawCode(code, args);
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}
