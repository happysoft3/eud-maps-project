
#include "pam00753_utils.h"
#include "libs/bind.h"
#include "libs/objectIDdefines.h"

int BatBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 7627074; arr[17] = 80; arr[18] = 1; arr[19] = 105; arr[24] = 1067869798; 
		arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1097859072; arr[29] = 4; 
		arr[31] = 8; arr[59] = 5542784; arr[60] = 1357; arr[61] = 46906368; 
	pArr = arr;
	return pArr;
}

void BatSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1078565273;		ptr[137] = 1078565273;
	int *hpTable = ptr[139];
	hpTable[0] = 80;	hpTable[1] = 80;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = BatBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}


int WaspBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1886609751; arr[17] = 65; arr[19] = 100; arr[21] = 1065353216; arr[23] = 32769; 
		arr[24] = 1065353216; arr[27] = 5; arr[28] = 1097859072; arr[29] = 5; arr[31] = 3; 
		arr[34] = 15; arr[35] = 1; arr[36] = 1; arr[59] = 5544320; arr[60] = 1331; 
		arr[61] = 46900736; 
	pArr = arr;
	return pArr;
}

void WaspSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 65;	hpTable[1] = 65;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = WaspBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int UrchinBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 28265; arr[17] = 80; arr[19] = 75; arr[21] = 1056964608; 
		arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[37] = 1869768788; arr[38] = 1735289207; 
		arr[39] = 1852798035; arr[40] = 101; arr[53] = 1128792064; arr[54] = 4; arr[55] = 20; 
		arr[56] = 30; arr[60] = 1339; arr[61] = 46904832; 
	pArr = arr;
	return pArr;
}

void UrchinSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074790400;		ptr[137] = 1074790400;
	int *hpTable = ptr[139];
	hpTable[0] = 80;	hpTable[1] = 80;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = UrchinBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1056964608;
}

int SmallAlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1651261804; arr[2] = 1399811689; arr[3] = 1701079408; arr[4] = 114; 
		arr[17] = 100; arr[19] = 85; arr[21] = 1065353216; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[28] = 1103626240; arr[29] = 3; arr[31] = 8; arr[59] = 5542784; 
		arr[60] = 1356; arr[61] = 46906624; 
	pArr = arr;
	return pArr;
}

void SmallAlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 100;	hpTable[1] = 100;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = SmallAlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void simplySpecArcher(int unit)
{
	SetUnitMaxHealth(unit,98);
}

int SwordsmanBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919907667; arr[1] = 1634562916; arr[2] = 110; arr[17] = 325; arr[19] = 55; 
		arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[27] = 3; arr[28] = 1106247680; 
		arr[29] = 18; arr[57] = 5547984; arr[59] = 5542784; arr[60] = 1346; arr[61] = 46900480; 
	pArr = arr;
	return pArr;
}

void SwordsmanSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = SwordsmanBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

int WolfBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1718382423; arr[17] = 225; arr[19] = 65; arr[21] = 1056964608; arr[23] = 32768; 
		arr[24] = 1069211976; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1097859072; 
		arr[29] = 30; arr[31] = 8; arr[59] = 5542784; arr[60] = 1369; arr[61] = 46902528; 
	pArr = arr;
	return pArr;
}

void WolfSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073322393;		ptr[137] = 1073322393;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WolfBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1056964608;
}

void invokeMonSpec(int fn, int unit)
{
	Bind(fn,&fn + 4);
}

#define UNIQ_KEY_MOB_ONLY 97
void LoadMonsterAlternativeSpec(int unit)
{
	short *p;
	
	if (!p)
	{
		short tb[UNIQ_KEY_MOB_ONLY];
		
		//area-1
		tb[OBJ_WASP%UNIQ_KEY_MOB_ONLY]=WaspSubProcess; tb[OBJ_BAT%UNIQ_KEY_MOB_ONLY]=BatSubProcess; tb[OBJ_URCHIN%UNIQ_KEY_MOB_ONLY]=UrchinSubProcess;
		tb[OBJ_SMALL_ALBINO_SPIDER%UNIQ_KEY_MOB_ONLY]=SmallAlbinoSpiderSubProcess;
		
		//area-2
		tb[OBJ_ARCHER%UNIQ_KEY_MOB_ONLY]=simplySpecArcher; tb[OBJ_SWORDSMAN%UNIQ_KEY_MOB_ONLY]=SwordsmanSubProcess;
		tb[OBJ_WOLF%UNIQ_KEY_MOB_ONLY]=WolfSubProcess;
	}
	int t=p[GetUnitThingID(unit)%UNIQ_KEY_MOB_ONLY];
	
	if (t)
		invokeMonSpec(t, unit);
}


















