
#include "generate_resource.h"
#include "libs\printutil.h"

#include "libs\waypoint.h"
#include "libs\buff.h"
#include "libs\mathlab.h"

#include "libs\reaction.h"
#include "libs\itemproperty.h"

#include "libs\potionex.h"
#include "libs\playerupdate.h"
#include "libs\coopteam.h"
#include "libs/networkRev.h"
#include "libs/game_flags.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"

int UserStrCount;
int PlrLastLoc;
int LastUnitID = 1280;

static int GetLastUnitID(){
    return LastUnitID++;
}//virtual
int player[20];
int AUTO_CHAK, HellMod;
float HARD_CORE = 1.0;
int PlrInven[10], PlrNodePtr[10];
int m_langmode;

int m_descTotalCount;


#define PLAYER_SKILL_FLAG1 2
#define PLAYER_SKILL_FLAG2 4
#define PLAYER_DEATH_FLAG 0x80000000

int PlayerClassCheckFlag(int plr, int flagone)
{
    return player[plr + 10] & flagone;
}

void PlayerClassSetFlag(int plr, int flagone)
{
    player[plr + 10] ^= flagone;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);
    return x;
}

int CreateMagicPotion(int location)
{
    return CheckPotionThingID(CreateObject(RewardPotion(Random(0, 12)), location));
}

int InitPlayerLastLocMark()
{
    int unit = CreateObject("InvisibleLightBlueLow", 1), i;

    for (i = 0 ; i < 10 ; Nop(++i))
        CreateObjectAtEx("InvisibleLightBlueLow", GetObjectX(unit + i), GetObjectY(unit + i) + 10.0, NULLPTR);
    return unit + 1;
}

void MapSignInit()
{
    RegistSignMessage(Object("UniSign1"), MapDescTable(28));
    RegistSignMessage(Object("UniSign2"), MapDescTable(29));
    RegistSignMessage(Object("UniSign3"), MapDescTable(29));
    RegistSignMessage(Object("UniSign4"), MapDescTable(30));
    RegistSignMessage(Object("UniSign5"), MapDescTable(31));
    RegistSignMessage(Object("UniSign6"), MapDescTable(32));
    RegistSignMessage(Object("UniSign7"), MapDescTable(33));
    RegistSignMessage(Object("UniSign8"), MapDescTable(34));
    RegistSignMessage(Object("UniSign9"), MapDescTable(35));
    RegistSignMessage(Object("UniSign10"), MapDescTable(36));
    RegistSignMessage(Object("UniSign11"), "갑옷 무적화 10,000 골드입니다");
}

void blockPickup()
{
    PlaySoundAround(OTHER,SOUND_JournalEntryAdd);
    UniPrint(OTHER,"~~~~~~~미니 퀘스트 던전~~~~ 제작자. panic ~~~~~~~~~~~즐겁고 매너있는 게임하세요~~~~");
}

void DelayMapInit()
{
    MakeCoopTeam();
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    int unit=CreateObjectById(OBJ_BOTTLE_CANDLE,LocationX(223),LocationY(223));
    UnitNoCollide(unit);
    Frozen(unit,TRUE);
    SetUnitCallbackOnPickup(unit,blockPickup);
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("generate-log.txt");
    UnitFlags(-1);
    UnitHP(-1);
    PlayerFlag(-1);
    SummonInfo(-1);
    MasterUnit();
    PlrLastLoc = InitPlayerLastLocMark();
    FrameTimer(1, DelayMapInit);
    FrameTimer(30, PutRewardMarker);
    FrameTimer(30, MainGameLoop);
    FrameTimer(4, MapSignInit);
    UserStrCount = GetUserStringCount(MapDescTable(0));
    if (CheckGameKorLanguage())
        m_langmode = UserStrCount;
    blockObserverMode();
}

void MapExit()
{
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}

void MainGameLoop()
{
    ObjectOn(Object("MainSwitchOn"));
    KeepLockingDoors();
    InitCreatures();
    FrameTimer(10, PutGeneratorAtRoom1);
    FrameTimer(15, PlayerClassOnLoop);
    FrameTimer(20, LoopSearchIndex);
    FrameTimerWithArg(1, 3, GuideLine); //error
}

void GuideLine(int count)
{
    if (count)
    {
        int fxunit = CreateObject("Magic", 215);

        SetUnitSubclass(fxunit, GetUnitSubclass(fxunit) ^ 1);
        Frozen(fxunit, TRUE);
        CreateMoverFixEx(fxunit, 0, 30.0, NULLPTR);
        FrameTimerWithArg(1, fxunit, DelayMoveUnit);
        FrameTimerWithArg(12, count - 1, GuideLine);
    }
    else
    {
        PutSkillShop();
        // FrameTimer(1, PutStampStrings);
    }
}

void DelayMoveUnit(int moveunit)
{
    Move(moveunit, 213);
}

void InitCreatures()
{
    int unit = CreateObject("InvisibleLightBlueHigh", 160), k;

    for (k = 0 ; k < 4 ; k ++)
    {
        CreateObject(SummonCreature(3 - k), k + 156);
        Frozen(unit + k+1, 1);
        LookWithAngle(unit + k+1, 224 - (64*k));
        SetOwner(GetHost(), unit + k + 1);
        SetDialog(unit + k+1, "NORMAL", CreatureClickForBuy, DummyFunction);
    }
    CreateObject("Maiden", 212);
    Frozen(unit + k+1, 1);
    SetOwner(GetHost(), unit + k+1);
    SetDialog(unit + k + 1, "NORMAL", PayPlasmaStaff, DummyFunction);
}

void PayPlasmaStaff()
{
    int pic;

    if (GetGold(OTHER) >= 50000)
    {
        UniPrint(OTHER, MapDescTable(19));
        MoveWaypoint(160, GetObjectX(OTHER), GetObjectY(OTHER));
        Effect("CYAN_SPARKS", GetWaypointX(160), GetWaypointY(160), 0.0, 0.0);
        pic = CreateObject("OblivionOrb", 160);
        CreateObject("InvisibleLightBlueHigh", 160);
        Raise(pic + 1, ToFloat(GetCaller()));
        ChangeGold(OTHER, -50000);
        FrameTimerWithArg(3, pic, DelayGiveToStaff);
    }
    else
        UniPrint(OTHER, MapDescTable(20));
}

void DelayGiveToStaff(int unit)
{
    int owner = ToInt(GetObjectZ(unit + 1));

    Pickup(owner, unit);
    Delete(unit + 1);
}

string SummonCreature(int num)
{
    string table[] = {"WizardRed", "WeirdlingBeast", "AirshipCaptain", "BlackWidow"};

    return table[num];
}

int CreatureHP(int num)
{
    int arr[4] = {1450, 1600, 1700, 1800};
    
    return arr[num];
}

void CreatureClickForBuy()
{
    int unit, idx = (GetDirection(SELF) - 32) / 64;

    if (GetGold(OTHER) >= 30000)
    {
        unit = CreateObjectAt(SummonCreature(idx), GetObjectX(OTHER), GetObjectY(OTHER));
        CheckMonsterThing(unit);
        SetUnitMaxHealth(unit, CreatureHP(idx));
        SetOwner(OTHER, unit);
        SetDialog(unit, "NORMAL", CreatureClickEvent, DummyFunction);
        SetCallback(unit, 5, CreatureUnitDead);
        SetCallback(unit, 7, CreatureHurtHandler);
        // StartDisplayHealthBar(unit);
        AttachHealthbar(unit);
        Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
        if (!idx)
            SetCallback(unit, 3, RWizWeapon);
        else if (idx == 1)
            SetCallback(unit, 3, BeastWeapon);
        else if (idx == 2)
        {
            SetCallback(unit, 3, JandorWeapon);
            SetCallback(unit, 13, EnableUnitSight);
        }
        else if (idx == 3)
        {
            SetCallback(unit, 3, SpiderWeapon);
            SetCallback(unit, 13, EnableUnitSight);
        }
        ChangeGold(OTHER, -30000);
        UniChatMessage(unit, MapDescTable(23), 150);
        UniPrint(OTHER, MapDescTable(21));
    }
    else
        UniPrint(OTHER, MapDescTable(22));
}

void DummyFunction()
{ }

int CheckPlayerMonster(int unit)
{
    int owner = GetOwner(unit);

    if (CurrentHealth(owner))
        return IsPlayerUnit(owner);
    return 0;
}

void MonsterLocation()
{
    if (IsPlayerUnit(OTHER))
        return;
    if (CheckPlayerMonster(OTHER))
    {
        CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
        CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
        CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
        if (MaxHealth(OTHER) ^ CurrentHealth(OTHER))
            RestoreHealth(OTHER, MaxHealth(OTHER) - CurrentHealth(OTHER));
    }
    else
    {
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("BallBounce", 1);
        DeleteObjectTimer(CreateObject("MagicEnergy", 1), 9);
        PushObjectTo(OTHER, 200.0, -200.0);
    }
}

void CreatureHurtHandler()
{
    if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_INVULNERABLE)))
    {
        Enchant(SELF, EnchantList(ENCHANT_INVULNERABLE), 3.0);
        RestoreHealth(SELF, 2);
    }
}

void EnableUnitSight()
{
    if (HasEnchant(SELF, "ENCHANT_BLINDED"))
        EnchantOff(SELF, "ENCHANT_BLINDED");
}

void BeastWeapon()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(CreateObject("MagicEnergy", 1), 9);
    MoveObject(SELF, GetObjectX(OTHER) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(OTHER) + UnitRatioY(OTHER, SELF, 13.0));
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(CreateObject("MagicEnergy", 1), 9);
    Damage(OTHER, SELF, 20, DAMAGE_TYPE_IMPALE);
    AudioEvent("PushCast", 1);
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
}

int DeadSmallSpiderCreate(int wp)
{
    int unit = CreateObject("SmallAlbinoSpider", wp);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    return unit;
}

void DeadSmallSpiderFly(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    while (1)
    {
        if (CurrentHealth(owner) && CurrentHealth(target))
        {
            if (count && IsVisibleTo(ptr + 1, target))
            {
                if (DistanceUnitToUnit(ptr + 1, target) < 48.0)
                {
                    MoveWaypoint(1, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
                    DeleteObjectTimer(CreateObject("GreenPuff", 1), 9);
                    AudioEvent("EggBreak", 1);
                    Damage(target, owner, 40, DAMAGE_TYPE_DRAIN);
                }
                else
                {
                    MoveObject(ptr + 1, GetObjectX(ptr + 1) + UnitRatioX(target, ptr + 1, 16.0), GetObjectY(ptr + 1) + UnitRatioY(target, ptr + 1, 16.0));
                    LookWithAngle(ptr, count - 1);
                    LookAtObject(ptr + 1, target);
                    FrameTimerWithArg(1, ptr, DeadSmallSpiderFly);
                    break;
                }
            }
        }
        Delete(ptr);
        Delete(ptr + 1);
        break;
    }
}

void SpiderWeapon()
{
    int unit;

    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        MoveWaypoint(1, GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 23.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 23.0));
        unit = CreateObject("InvisibleLightBlueLow", 1);
        LookAtObject(DeadSmallSpiderCreate(1), OTHER);
        SetOwner(SELF, unit);
        Raise(unit, GetCaller());
        LookWithAngle(unit, 20);
        FrameTimerWithArg(1, unit, DeadSmallSpiderFly);
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.1);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void JandorWeapon()
{
    if (DistanceUnitToUnit(SELF, OTHER) < 98.0)
    {
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        DeleteObjectTimer(CreateObject("MeteorExplode", 1), 9);
		Damage(OTHER, SELF, 30, 1);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void RWizWeapon()
{
    if (CurrentHealth(SELF))
    {
        if (CurrentHealth(OTHER))
        {
            Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, SELF, 35, 16);
        }
        if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
        {
            Enchant(SELF, "ENCHANT_VILLAIN", 0.0);
            FrameTimerWithArg(22, GetTrigger(), RWizResetSight);
        }
    }
}

void RWizResetSight(int unit)
{
    EnchantOff(unit, "ENCHANT_VILLAIN");
    Enchant(unit, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(unit, 1.0);
}

void CreatureClickEvent()
{
    if (CurrentHealth(SELF) && CurrentHealth(OTHER))
    {
        MoveWaypoint(160, GetObjectX(SELF), GetObjectY(SELF));
        Effect("CHARM", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("FlagRespawn", 160);
        UniChatMessage(SELF, MapDescTable(27) + IntToString(CurrentHealth(SELF)) + " / " + IntToString(MaxHealth(SELF)), 120);
        CreatureFollow(SELF, OTHER);
        AggressionLevel(SELF, 1.0);
    }
}

void CreatureUnitDead()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner))
        UniPrint(owner, MapDescTable(25));
    UniChatMessage(SELF, MapDescTable(26), 150);
    DeleteObjectTimer(SELF, 30);
}

void PutGeneratorAtRoom1()
{
    //area1
    SpawnGenerator(6, 7);
    SpawnGenerator(6, 8);
    SpawnGenerator(21, 14);
    SpawnGenerator(21, 15);
    SpawnGenerator(11, 13);
    //area1-1
    SpawnGenerator(10, 16);
    SpawnGenerator(15, 17);
    SpawnGenerator(15, 18);
    SpawnGenerator(30, 19);
    SpawnGenerator(38, 20);
}

void OpenGateArea2()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 5)
    {
        UnlockDoor(Object("NormalGate1"));
        UnlockDoor(Object("NormalGate2"));
        //area2
        SpawnGenerator(9, 21);
        SpawnGenerator(8, 22);
        SpawnGenerator(8, 23);
        SpawnGenerator(20, 24);
        SpawnGenerator(31, 25);
        SpawnGenerator(33, 26);
    }
}

void OpenGateArea21()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 5)
    {
        UnlockDoor(Object("HardGate1"));
        UnlockDoor(Object("HardGate2"));
        //area2-1
        SpawnGenerator(17, 28);
        SpawnGenerator(16, 27);
        SpawnGenerator(16, 29);
        SpawnGenerator(18, 30);
        SpawnGenerator(18, 31);
        SpawnGenerator(37, 32);
    }
}

void OpenGateArea3()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 6)
    {
        UnlockDoor(Object("NormalGate3"));
        UnlockDoor(Object("NormalGate4"));
        //area3
        SpawnGenerator(32, 33);
        SpawnGenerator(20, 34);
        SpawnGenerator(32, 35);
        SpawnGenerator(20, 36);
        SpawnGenerator(7, 37);
        SpawnGenerator(7, 38);
    }
}

void OpenGateArea31()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 6)
    {
        UnlockDoor(Object("HardGate3"));
        UnlockDoor(Object("HardGate4"));
        //area3
        SpawnGenerator(14, 39);
        SpawnGenerator(12, 40);
        SpawnGenerator(14, 41);
        SpawnGenerator(12, 42);
        SpawnGenerator(41, 43);
        SpawnGenerator(5, 44);
    }
}

void OpenGateArea4()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 6)
    {
        UnlockDoor(Object("NormalGate5"));
        UnlockDoor(Object("NormalGate6"));
        //area4
        SpawnGenerator(1, 45);
        SpawnGenerator(25, 46);
        SpawnGenerator(1, 47);
    }
}

void OpenGateArea41()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 6)
    {
        UnlockDoor(Object("HardGate5"));
        UnlockDoor(Object("HardGate6"));
        //area4
        SpawnGenerator(22, 48);
        SpawnGenerator(2, 49);
        SpawnGenerator(22, 50);
    }
}

void OpenGateArea5()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 3)
    {
        WallOpen(Wall(178, 34));
        WallOpen(Wall(177, 35));
        WallOpen(Wall(176, 36));
        SpawnGenerator(19, 51);
        SpawnGenerator(19, 52);
        SpawnGenerator(27, 53);
        SpawnGenerator(27, 54);
    }
}

void OpenGateArea6()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 4)
    {
        UnlockDoor(Object("NormalGate7"));
        UnlockDoor(Object("NormalGate8"));
        SpawnGenerator(27, 55);
        SpawnGenerator(28, 56);
        SpawnGenerator(28, 57);
        SpawnGenerator(27, 58);
        SpawnGenerator(4, 59);
        SpawnGenerator(4, 60);
        SpawnGenerator(0, 61);
    }
}

void OpenGateArea7()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 7)
    {
        UnlockDoor(Object("NormalGate9"));
        UnlockDoor(Object("NormalGate10"));
        SpawnGenerator(28, 62);
        SpawnGenerator(28, 63);
        SpawnGenerator(4, 64);
        SpawnGenerator(4, 65);
        SpawnGenerator(25, 66);
    }
}

void OpenGateArea8()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 5)
    {
        UnlockDoor(Object("NormalGate11"));
        UnlockDoor(Object("NormalGate12"));
        SpawnGenerator(7, 67);
        SpawnGenerator(7, 68);
        SpawnGenerator(13, 69);
        SpawnGenerator(13, 70);
        SpawnGenerator(13, 71);
        SpawnGenerator(13, 72);
        SpawnGenerator(23, 73);
        SpawnGenerator(23, 74);
    }
}

void OpenGateArea9()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 8)
    {
        UnlockDoor(Object("AnnexGate1"));
        UnlockDoor(Object("AnnexGate2"));
        SpawnGenerator(3, 161);
        SpawnGenerator(3, 162);
        SpawnGenerator(1, 163);
        SpawnGenerator(12, 164);
        SpawnGenerator(12, 165);
        SpawnGenerator(1, 166);
    }
}

void OpenGateArea10()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 6)
    {
        UnlockDoor(Object("AnnexGate3"));
        UnlockDoor(Object("AnnexGate4"));
        SpawnGenerator(34, 167);
        SpawnGenerator(34, 168);
        SpawnGenerator(9, 169);
        SpawnGenerator(9, 170);
        SpawnGenerator(28, 171);
        SpawnGenerator(28, 172);
        SpawnGenerator(20, 173);
        SpawnGenerator(20, 174);
    }
}

void OpenGateArea11()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 8)
    {
        UnlockDoor(Object("AnnexGate5"));
        UnlockDoor(Object("AnnexGate6"));
        SpawnGenerator(25, 175);
        SpawnGenerator(25, 176);
        SpawnGenerator(25, 177);
        SpawnGenerator(25, 178);
    }
}

void OpenGateArea12()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 4)
    {
        UnlockDoor(Object("AnnexGate7"));
        UnlockDoor(Object("AnnexGate8"));
        SpawnGenerator(20, 183);
        SpawnGenerator(0, 184);
        SpawnGenerator(20, 185);
        SpawnGenerator(0, 186);

        SpawnGenerator(13, 179);
        SpawnGenerator(32, 180);
        SpawnGenerator(13, 181);
        SpawnGenerator(32, 182);
    }
}

void OpenGateArea13()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 8)
    {
        UnlockDoor(Object("AnnexGate9"));
        UnlockDoor(Object("AnnexGate10"));
        SpawnGenerator(38, 187);
        SpawnGenerator(19, 188);
        SpawnGenerator(27, 189);
        SpawnGenerator(5, 190);
        SpawnGenerator(13, 191);
        SpawnGenerator(14, 192);
        SpawnGenerator(1, 193);
        SpawnGenerator(23, 194);
        SpawnGenerator(25, 195);
        SpawnGenerator(3, 196);
    }
}

void RemoveLeftLastWalls()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 10)
    {
        VictoryEvent();
        WallOpen(Wall(121, 91));
        WallOpen(Wall(120, 92));
        WallOpen(Wall(119, 93));
        UnlockDoor(Object("AnnexGate11"));
        UnlockDoor(Object("AnnexGate12"));
    }
}

void OpenGateArea51()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 3)
    {
        UnlockDoor(Object("HardGate7"));
        UnlockDoor(Object("HardGate8"));
        SpawnGenerator(0, 75);
        SpawnGenerator(19, 76);
        SpawnGenerator(12, 77);
        SpawnGenerator(12, 78);
        SpawnGenerator(19, 79);
        SpawnGenerator(14, 80);
        SpawnGenerator(18, 81);
        SpawnGenerator(18, 82);
        SpawnGenerator(14, 83);
    }
}

void OpenGateArea61()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 9)
    {
        UnlockDoor(Object("HardGate9"));
        UnlockDoor(Object("HardGate10"));
        SpawnGenerator(15, 84);
        SpawnGenerator(15, 85);
        SpawnGenerator(16, 86);
        SpawnGenerator(16, 91);
        SpawnGenerator(13, 90);
        SpawnGenerator(13, 89);
        SpawnGenerator(33, 87);
        SpawnGenerator(33, 88);
    }
}

void OpenGateArea71()
{
    int destroy;
    int k;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 8)
    {
        for (k = 0 ; k < 7 ; k ++)
            WallOpen(Wall(193 - k, k + 79));
        SpawnGenerator(10, 92);
        SpawnGenerator(11, 98);
        SpawnGenerator(21, 93);
        SpawnGenerator(22, 99);
        SpawnGenerator(24, 94);
        SpawnGenerator(12, 100);
        SpawnGenerator(14, 95);
        SpawnGenerator(5, 101);
        SpawnGenerator(17, 96);
        SpawnGenerator(13, 102);
        SpawnGenerator(18, 97);
        SpawnGenerator(36, 103);
    }
}

void OpenGateArea81()
{
    int destroy;
    int k;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 12)
    {
        for (k = 0 ; k < 5 ; k ++)
            WallOpen(Wall(k + 188, k + 92));
        SpawnGenerator(39, 104);
        SpawnGenerator(40, 105);
        SpawnGenerator(17, 106);
        SpawnGenerator(12, 107);
        SpawnGenerator(2, 108);
        SpawnGenerator(17, 109);
    }
}

void OpenGateArea91()
{
    int destroy;
    int k;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 13)
    {
        for (k = 0 ; k < 6 ; k ++)
            WallOpen(Wall(177 - k, k + 95));
        SpawnGenerator(24, 110);
        SpawnGenerator(5, 111);
        SpawnGenerator(18, 112);
        SpawnGenerator(14, 113);
        SpawnGenerator(40, 114);
        SpawnGenerator(39, 115);
        SpawnGenerator(41, 116);
        SpawnGenerator(29, 117);
    }
}

void OpenGateArea101()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 8)
    {
        UnlockDoor(Object("HardGate11"));
        UnlockDoor(Object("HardGate12"));
        SpawnGenerator(30, 118);
        SpawnGenerator(21, 119);
        SpawnGenerator(10, 120);
        SpawnGenerator(33, 121);

        SpawnGenerator(24, 122);
        SpawnGenerator(41, 123);
        SpawnGenerator(16, 124);
        SpawnGenerator(0, 125);

        SpawnGenerator(35, 126);
        SpawnGenerator(37, 127);
        SpawnGenerator(11, 128);
        SpawnGenerator(15, 129);

        SpawnGenerator(26, 130);
        SpawnGenerator(41, 131);
        SpawnGenerator(17, 132);
        SpawnGenerator(29, 133);
    }
}

void OpenGateAreaP91()
{
    int destroy;
    int k;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 6)
    {
        for (k = 0 ; k < 7 ; k ++)
            WallOpen(Wall(k + 171, k + 107));
        SpawnGenerator(2, 197);
        SpawnGenerator(26, 198);
        SpawnGenerator(22, 199);
        SpawnGenerator(29, 200);
        SpawnGenerator(12, 201);
        SpawnGenerator(19, 202);
        SpawnGenerator(21, 203);
        SpawnGenerator(22, 204);
        SpawnGenerator(0, 205);
        SpawnGenerator(39, 206);
        SpawnGenerator(41, 207);
        SpawnGenerator(5, 208);
        SpawnGenerator(17, 209);
    }
}

void RemoveRightLastWalls()
{
    int destroy;

    destroy ++;
    ObjectOff(SELF);
    if (destroy >= 16)
    {
        VictoryEvent();
        WallOpen(Wall(139, 109));
        WallOpen(Wall(138, 110));
        WallOpen(Wall(137, 111));
    }
}

void VictoryEvent()
{
    int win;

    win ++;
    if (win == 2)
    {
        UniPrintToAll(MapDescTable(16));
        ObjectOff(Object("MainSwitchOn"));
        MoveObject(Object("StartLocation"), GetWaypointX(210), GetWaypointY(210));
        FrameTimer(1, StrVictory);
        FrameTimerWithArg(120, 210, TeleportAllPlayer);
    }
}

void TeleportAllPlayer(int wp)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
            MoveObject(player[k], GetWaypointX(wp), GetWaypointY(wp));
    }
}

void StrVictory()
{
	int arr[13];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
	{
		drawStrVictory(arr[i], name);
		i ++;
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(211);
		pos_y = GetWaypointY(211);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 211);
		if (count % 38 == 37)
			MoveWaypoint(211, GetWaypointX(211) - 74.000000, GetWaypointY(211) + 2.000000);
		else
			MoveWaypoint(211, GetWaypointX(211) + 2.000000, GetWaypointY(211));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(211, pos_x, pos_y);
	}
}

void KeepLockingDoors()
{
    int k;

    for (k = 11 ; k >= 0 ; Nop(k --))
    {
        LockDoor(Object("NormalGate" + IntToString(k + 1)));
        LockDoor(Object("HardGate" + IntToString(k + 1)));
        LockDoor(Object("AnnexGate" + IntToString(k + 1)));
    }
}

static void NetworkUtilClientMain()
{
    OnInitializeResources();
}

void PlayerInit(int plr)
{
    ChangeGold(player[plr], -GetGold(player[plr]));
    MoveObject(PlrLastLoc + plr, GetWaypointX(3), GetWaypointY(3));
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        int plr = CheckPlayer();

        Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
        if (plr < 0)
            MoveObject(OTHER, LocationX(224), LocationY(224));
        else
            RegistPlayer();
    }
}

void PlayerClassOnInit(int plr, int pUnit, int *result)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    ChangeGold(pUnit, -GetGold(pUnit));
    MoveObject(PlrLastLoc + plr, LocationX(3), LocationY(3));
    EmptyAll(pUnit);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    if (result != NULLPTR)
        result[0] = plr;
}

void RegistPlayer()
{
    int plr, k;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            plr = CheckPlayer();

            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    PlayerClassOnInit(k, GetCaller(), &plr);
                    break;
                }
            }
            if (plr >= 0)
            {
                JoinTheMap(plr);
                break;
            }
        }
        CantJoin();
        break;
    }
}

void JoinTheMap(int plr)
{
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);

    Enchant(player[plr], "ENCHANT_ANCHORED", 0.0);
    MoveObject(player[plr], GetWaypointX(3), GetWaypointY(3));
    DeleteObjectTimer(CreateObject("RainOrbBlue", 3), 15);
    AudioEvent("QuestPlayerJoinGame", 3);
}

void CantJoin()
{
    MoveObject(OTHER, GetWaypointX(2), GetWaypointY(2));
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    UniPrint(OTHER, MapDescTable(0));
    UniPrint(OTHER, MapDescTable(1));
}

void TeleportPlayerToLastLoc()
{
    int plr = GetPlayerScrIndex(GetCaller()), dest;

    if (plr + 1)
    {
        dest = PlrLastLoc + plr;
        Enchant(player[plr], "ENCHANT_INVULNERABLE", 2.0);
        Effect("SMOKE_BLAST", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
        MoveObject(player[plr], GetObjectX(dest), GetObjectY(dest));
        Effect("TELEPORT", GetObjectX(dest), GetObjectY(dest), 0.0, 0.0);
        UniPrint(OTHER, MapDescTable(2));
    }
}

int GetPlayerScrIndex(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(unit))
        {
            if (player[i] ^ unit)
                continue;
            else
                return i;
        }
    }
    return -1;
}

void PlayerEnchantment(int unit)
{
    if (HasEnchant(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY"))
    {
        EnchantOff(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY");
        Enchant(unit, "ENCHANT_SHOCK", 70.0);
    }
    else if (HasEnchant(unit, "ENCHANT_PROTECT_FROM_POISON"))
    {
        EnchantOff(unit, "ENCHANT_PROTECT_FROM_POISON");
        MoveObject(PlrLastLoc + GetPlayerScrIndex(unit), GetObjectX(unit), GetObjectY(unit));
        MoveObject(unit, GetWaypointX(3), GetWaypointY(3));
        DeleteObjectTimer(CreateObject("BlueRain", 3), 15);
        AudioEvent("BlindOff", 3);
        UniPrint(unit, MapDescTable(3));
        UniPrint(unit, MapDescTable(4));
    }
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    PlayerEnchantment(pUnit);
    CheckUseSkill(plr);
}

void PlayerClassOnDeath(int plr)
{ }

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; Nop(--i))
    {
        while (TRUE)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(i, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(i, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void CheckUseSkill(int plr)
{
    int flag = player[plr + 10], plrUnit = player[plr];

    if (UnitCheckEnchant(plrUnit, GetLShift(31)))
    {
        EnchantOff(plrUnit, EnchantList(31));
        RemoveTreadLightly(plrUnit);
        if (player[plr + 10] & 0x02)
            EnergyPar(plr);
    }
    else if (player[plr + 10] & 0x04)
    {
        if (!(CheckPlayerInput(plrUnit) ^ 48))
        {
            if (!UnitCheckEnchant(plrUnit, GetLShift(15)))
            {
                Firewalk(plr);
                Enchant(plrUnit, EnchantList(15), 10.0);
            }
        }
    }
}

void EnergyPar(int plr)
{
    float pos_x, pos_y;
    int ptr;

    if (!HasEnchant(player[plr], "ENCHANT_DETECTING"))
    {
        Enchant(player[plr], "ENCHANT_DETECTING", 10.0);
        pos_x = UnitAngleCos(player[plr], 30.0);
        pos_y = UnitAngleSin(player[plr], 30.0);
        MoveWaypoint(plr + 146, pos_x, pos_y);
        MoveWaypoint(plr + 136, GetObjectX(player[plr]) + pos_x, GetObjectY(player[plr]) + pos_y);
        ptr = CreateObject("InvisibleLightBlueHigh", plr + 136);
        CreateObject("InvisibleLightBlueHigh", plr + 136);
        LookWithAngle(ptr, plr);
        EnergyParLoop(ptr);
    }
}

void EnergyParLoop(int ptr)
{
    int plr = GetDirection(ptr), unit;

    while (1)
    {
        if (CurrentHealth(player[plr]))
        {
            if (GetDirection(ptr + 1) < 20 && checkMapBoundary(plr + 136))
            {
                MoveWaypoint(plr + 136, GetWaypointX(plr + 136) + GetWaypointX(plr + 146), GetWaypointY(plr + 136) + GetWaypointY(plr + 146));
                Effect("LIGHTNING", GetObjectX(ptr), GetObjectY(ptr), GetWaypointX(plr + 136), GetWaypointY(plr + 136));
                unit = CreateObject("ShopkeeperConjurerRealm", plr + 136);
                LookWithAngle(unit, plr);
                Frozen(unit, 1);
                SetCallback(unit, 9, DeathTouched);
                DeleteObjectTimer(unit, 1);
                LookWithAngle(ptr + 1, GetDirection(ptr + 1) + 1);
                FrameTimerWithArg(1, ptr, EnergyParLoop);
                break;
            }
        }
        Delete(ptr);
        Delete(ptr + 1);
        break;
    }
}

void DeathTouched()
{
    int plr = GetDirection(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, player[plr]) && IsVisibleTo(player[plr], OTHER))
    {
        Damage(OTHER, player[plr], 150, DAMAGE_TYPE_PLASMA);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.7);
    }
}

void Firewalk(int k)
{
    int unit;
    float pos_x = UnitAngleCos(player[k], 20.0), pos_y = UnitAngleSin(player[k], 20.0);

    MoveWaypoint(1, GetObjectX(player[k]), GetObjectY(player[k]));
    unit = CreateObject("WeirdlingBeast", 1);
    CreateObject("InvisibleLightBlueHigh", 1);
    UnitNoCollide(unit);
    DeleteObjectTimer(unit, 1);
    DeleteObjectTimer(unit + 1, 3);
    SetOwner(player[k], unit);
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit) + pos_x, GetObjectY(unit) + pos_y, 450.0);
    LookWithAngle(unit, GetDirection(player[k]));
    LookWithAngle(unit + 1, k);
	AggressionLevel(unit, 1.0);

	SetCallback(unit, 3, ShotDeathray);
}

void ClickWishWellEvent()
{
    UniPrint(OTHER, MapDescTable(5));
    Effect("GREATER_HEAL", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    RestoreHealth(OTHER, 50);
    CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
    MoveWaypoint(9, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("RestoreHealth", 9);
}

void SetAutoChakrum()
{
    string stat;

    if (!AUTO_CHAK)
    {
        stat = "ON";
        AUTO_CHAK = 1;
    }
    else
    {
        stat = "OFF";
        AUTO_CHAK = 0;
    }
    UniPrintToAll(MapDescTable(6) + stat);
}

void WeaponEnchant()
{
    int weapon;
    int count = 0;

    if (GetGold(OTHER) >= 10000)
    {
        weapon = GetLastItem(OTHER);
        while (IsObjectOn(weapon))
        {
            if (HasClass(weapon, "WEAPON") && !HasEnchant(weapon, "ENCHANT_INVULNERABLE"))
            {
                count ++;
                Frozen(weapon, 1);
            }
            weapon = GetPreviousItem(weapon);
        }
        if (count > 0)
        {
            UniPrint(OTHER, MapDescTable(7) + IntToString(count) + MapDescTable(8));
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
            ChangeGold(OTHER, -10000);
        }
    }
    else
        UniPrint(OTHER, MapDescTable(9));
}

void ArmorEnchant()
{
    int armor;
    int count = 0;

    if (GetGold(OTHER) >= 10000)
    {
        armor = GetLastItem(OTHER);
        while(IsObjectOn(armor))
        {
            if (HasClass(armor, "ARMOR") && !HasEnchant(armor, "ENCHANT_INVULNERABLE"))
            {
                count ++;
                Frozen(armor, 1);
            }
            armor = GetPreviousItem(armor);
        }
        if (count > 0)
        {
            UniPrint(OTHER, MapDescTable(7) + IntToString(count) + MapDescTable(8));
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
            ChangeGold(OTHER, -10000);
        }
    }
    else
        UniPrint(OTHER, MapDescTable(10));
}

void PutRewardMarker()
{
    int line;

    if (line++ < 24)
    {
        TeleportLocation(12, LocationX(10), LocationY(10));
        int rep = 0;

        while (rep++ < 15)
        {
            SpawnRewardMarker(12, NULLPTR);
            TeleportLocationVector(12, 30.0, 30.0);
        }
        TeleportLocationVector(10, -30.0, 30.0);
        FrameTimer(1, PutRewardMarker);
    }
}

void CheckSpecialItem(int weapon)
{
    int thingId = GetUnitThingID(weapon);
    int ptr = UnitToPtr(weapon);

    if (thingId >= 222 && thingId <= 225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId == 1178)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
    else if (thingId == 1168)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
}

int CallRewardWeapon(int locationId)
{
    int weapon = CreateObject(RewardWeapon(Random(0, 15)), locationId);

    CheckSpecialItem(weapon);
    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    return weapon;
}

int CallRewardArmor(int locationId)
{
    int armor = CreateObject(RewardArmor(Random(0, 21)), locationId);

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return armor;
}

int CallRewardPotion(int locationId)
{
    return CreateMagicPotion(locationId);
}

int CallRewardStaff(int locationId)
{
    return CreateObject(RewardStaff(Random(0, 6)), locationId);
}

void SpawnRewardMarker(int wp, int *destunit)
{
    int actions[] = {CallRewardWeapon, CallRewardPotion, CallRewardArmor, CallRewardStaff};
    int unit = CallFunctionWithArgInt(actions[Random(0, 3)], wp);

    if (destunit != NULLPTR)
        destunit[0] = unit;
}

void SpawnRewardMarkerPlus(int wp, int *destunit)
{
    int actions[] = {CallRewardWeapon, CallRewardPotion, CallRewardArmor, CallRewardStaff, CreateGold};
    int unit = CallFunctionWithArgInt(actions[Random(0, 4)], wp);

    if (destunit != NULLPTR)
        destunit[0] = unit;
}

void DetectedSpecificIndex(int curId)
{
    if (!IsMissileUnit(curId))
        return;

    int thingID = GetUnitThingID(curId);

    if (thingID == 709)
        ShotMagicMissile(GetOwner(curId), curId);
    else if (thingID == 706)
        ShootFireball(GetOwner(curId), curId);
    else if (thingID == 1177)
        ChakramEvent(GetOwner(curId), curId);
}

void LoopSearchIndex()
{
    int curId, tempId;
    int *ptr = 0x750710;

    ptr = ptr[0];
    while (ptr != NULLPTR)
    {
        tempId = ptr[11];
        if (curId)
        {
            while (curId < tempId)
                DetectedSpecificIndex(++curId);
            break;
        }
        curId = tempId;
        break;
    }
    FrameTimer(1, LoopSearchIndex);
}

void ChakramEvent(int owner, int cur)
{
    Enchant(cur, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
    Enchant(cur, "ENCHANT_INFRAVISION", 0.0);
}

void ShootFireball(int owner, int cur)
{
    if (CurrentHealth(owner))
    {
        int mis = CreateObjectAt("TitanFireball", GetObjectX(owner) + UnitAngleCos(owner, 11.0), GetObjectY(owner) + UnitAngleSin(owner, 11.0));

        SetOwner(owner, mis);
        LookWithAngle(mis, GetDirection(owner));
        PushObject(mis, 40.0, GetObjectX(owner), GetObjectY(owner));
    }
    Delete(cur);
}

void ShotMagicMissile(int owner, int cur)
{
    if (CurrentHealth(owner))
    {
        int subunit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(cur), GetObjectY(cur));

        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", owner, GetObjectX(owner) + UnitAngleCos(owner, 23.0), GetObjectY(owner) + UnitAngleSin(owner, 23.0));
        Delete(cur);
        Delete(subunit);
        Delete(subunit + 2);
        Delete(subunit + 3);
        Delete(subunit + 4);
    }
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int PlayerFlag(int num)
{
    int flag[10];

    if (num < 0)
    {
        flag[0] = Object("flagBase");
        int k;
        for (k = 1 ; k < 10 ; k ++)
            flag[k] = flag[0] + (k * 2);
        return 0;
    }
    return flag[num];
}

int MasterUnit()
{
    int master;

    if (!master)
    {
        master = CreateObject("Hecubah", 6);
        Frozen(master, 1);
    }
    return master;
}

int SpawnGenerator(int type, int wp)
{
    int id = CreateObject(UnitType(type) + "Generator", wp);

    SetUnitMaxHealth(id, 600);
    CreateObject("InvisibleLightBlueHigh", wp);
    Enchant(id, "ENCHANT_FREEZE", 0.0);
    LookWithAngle(id + 1, type); //push unit_type
    ObjectOff(id);
    
    SetOwner(MasterUnit(), id);
    FrameTimerWithArg(1, id, DelayChangeLook);
    FrameTimerWithArg(30, id, GeneratorLoop);
    return id;
}

void DelayChangeLook(int gen)
{
    LookWithAngle(gen, 0);
}

void CheckInvisibleUnit(int unit)
{
    if (HasEnchant(unit, "ENCHANT_INVISIBLE"))
        EnchantOff(unit, "ENCHANT_INVISIBLE");
}

void GeneratorLoop(int gen)
{
    int type = GetDirection(gen + 1), plr;

    if (CurrentHealth(gen))
    {
        if (!HasEnchant(gen, "ENCHANT_VILLAIN"))
        {
            if (GetDirection(gen) < SummonInfo(type) & 0xff)
            {
                plr = CheckNealyPlayer(gen);
                if (plr + 1)
                {
                    MoveWaypoint(5, GetObjectX(gen) - UnitRatioX(gen, player[plr], 35.0), GetObjectY(gen) - UnitRatioY(gen, player[plr], 35.0));
                    Effect("LIGHTNING", GetObjectX(gen), GetObjectY(gen), GetWaypointX(5), GetWaypointY(5));
                    Effect("RICOCHET", GetWaypointX(5), GetWaypointY(5), 0.0, 0.0);
                    AudioEvent("MonsterGeneratorSpawn", 5);
                    SummonedUnit(gen, type, 5);
                    Enchant(gen, "ENCHANT_VILLAIN", IntToFloat((SummonInfo(type) >> 0x8) & 0xff) / HARD_CORE);
                    LookWithAngle(gen, GetDirection(gen) + 1);
                    FrameTimerWithArg(1, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(gen), GetObjectY(gen)), YellowThunderFx);
                    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(gen) + UnitRatioX(player[plr], gen, 75.0), GetObjectY(gen) + UnitRatioY(player[plr], gen, 75.0));
                    FrameTimerWithArg(3, player[plr], CheckInvisibleUnit);
                }
            }
        }
        FrameTimerWithArg(6, gen, GeneratorLoop);
    }
}

void YellowThunderFx(int ptr)
{
    CastSpellObjectObject("SPELL_LIGHTNING", ptr, ptr + 1);
    FrameTimerWithArg(30, ptr, DelayRemoveYellowPtr);
}

void DelayRemoveYellowPtr(int ptr)
{
    Delete(ptr);
    Delete(ptr + 1);
}

int CheckVisibleUnitToUnit(int unit1, int unit2)
{
    return (IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1));
}

int CheckNealyPlayer(int unit)
{
    float temp = 9999.0, r;
    int i, res = -1;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]) && CheckVisibleUnitToUnit(player[i], unit))
        {
            r = Distance(GetObjectX(unit), GetObjectY(unit), GetObjectX(player[i]), GetObjectY(player[i]));
            if (r < temp)
            {
                temp = r;
                res = i;
            }
        }
    }
    return res;
}

void SummonedUnit(int owner, int type, int wp)
{
    int unit = CreateObject(UnitType(type), wp);

    UnitSpecialProperty(unit, type);
    CreateObject("InvisibleLightBlueHigh", wp);
    Raise(unit + 1, ToFloat(owner));
    SetOwner(MasterUnit(), unit);
    SetCallback(unit, 5, UnitDeaths);
    SetCallback(unit, 7, UnitHit);
    LookAtObject(unit, owner);
    LookWithAngle(unit, GetDirection(unit) + 128);
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(owner), GetObjectY(owner), 450.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    AggressionLevel(unit, 1.0);
}

void UnitSpecialProperty(int unit, int type)
{
    int func;

    if (!func)
    {
        CancelTimer(FrameTimerWithArg(10, UnitNothing, UnitNothing));
        func = GetMemory(GetMemory(0x83395c) + 8);
    }
    if (HellMod)
        SetUnitMaxHealth(unit, UnitHP(type) * 36 / 10);
    else
        SetUnitMaxHealth(unit, UnitHP(type) * 32 / 10);
    CallFunctionWithArg(func + UnitFlags(type), unit);
}

void UnitHit()
{
    if (GetCaller())
    {
        if (AUTO_CHAK)
        {
            if (HasClass(OTHER, "WEAPON") && HasSubclass(OTHER, "CHAKRAM"))
                MoveObject(OTHER, GetObjectX(SELF), GetObjectY(SELF));
        }
    }
    else
    {
        if (IsPoisonedUnit(SELF))
        {
            Damage(SELF, 0, IsPoisonedUnit(SELF) * 2, 5);
            DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 15);
        }
    }
}

void UnitDeaths()   //here
{
    int ptr = GetTrigger() + 1;
    int owner = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner))
        LookWithAngle(owner, GetDirection(owner) - 1);

    TeleportLocation(9, GetObjectX(SELF), GetObjectY(SELF));

    int item;

    SpawnRewardMarkerPlus(9, &item); //todo CreateGold(9);

    Frozen(item, TRUE);
    
    Delete(ptr);
    DeleteObjectTimer(SELF, 60);
}

int CreateGold(int wp)
{
    int money = CreateObject("QuestGoldChest", wp);

    UnitStructSetGoldAmount(money, Random(3000, 5000));
    return money;
}

int UnitHP(int num)
{
    int arr[42];
    if (num < 0)
    {
        arr[0] = 120; arr[1] = 75; arr[2] = 60;
        arr[3] = 80; arr[4] = 35; arr[5] = 140;
        arr[6] = 10; arr[7] = 35; arr[8] = 50;
        arr[9] = 75; arr[10] = 30; arr[11] = 40;
        arr[12] = 25; arr[13] = 32; arr[14] = 75;
        arr[15] = 20; arr[16] = 125; arr[17] = 130;
        arr[18] = 75; arr[19] = 10; arr[20] = 20;
        arr[21] = 15; arr[22] = 30; arr[23] = 200;
        arr[24] = 300; arr[25] = 400; arr[26] = 125;
        arr[27] = 100; arr[28] = 105; arr[29] = 100;
        arr[30] = 10; arr[31] = 30; arr[32] = 60;
        arr[33] = 30; arr[34] = 150; arr[35] = 10;
        arr[36] = 10; arr[37] = 50; arr[38] = 11;
        arr[39] = 250; arr[40] = 150; arr[41] = 200;
        return 0;
    }
    return arr[num];
}

string UnitType(int num)
{
    string name[] = {
        "Beholder", "Wizard", "WizardGreen", 
        "WizardWhite", "Archer", "Bear",
        "Imp", "EmberDemon", "Skeleton",
        "SkeletonLord", "AlbinoSpider", "Spider",
        "SpittingSpider", "Shade", "Scorpion",
        "GiantLeech", "OgreBrute", "OgreWarlord",
        "GruntAxe", "FlyingGolem", "EvilCherub",
        "Urchin", "UrchinShaman", "Demon",
        "StoneGolem", "MechanicalGolem", "WillOWisp",
        "Swordsman", "Horrendous", "Necromancer",
        "Bat", "Zombie", "VileZombie",
        "Ghost", "Lich", "SmallAlbinoSpider",
        "SmallSpider", "Wolf", "Wasp",
        "Mimic", "Troll", "CarnivorousPlant"
    };
    return name[num];
}

int UnitFlags(int num)
{
    int flag[42];

    if (num < 0)
    {
        flag[0] = 1; flag[1] = 1; flag[2] = 1;
        flag[3] = 1; flag[34] = 6;
        flag[28] = 2; flag[29] = 5;
        flag[32] = 3; flag[41] = 3; flag[33] = 4;
        return 0;
    }
    return flag[num];
}

void UnitNothing(int unit)
{
    return;
}

void SpellUnitAnchored(int unit)
{
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
}

void HorrendousSlowly(int unit)
{
    SetUnitSpeed(unit, 1.2);
}

void CarnPlant(int unit)
{
    SetUnitSpeed(unit, 1.22);
}

void GhostHandler(int unit)
{
    Enchant(unit, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
    Enchant(unit, "ENCHANT_INFRAVISION", 0.0);
    SetCallback(unit, 3, GhostStrike);
}

void NecroHandler(int unit)
{
    UnitZeroFleeRange(unit);
    UnitLinkBinScript(unit, HecubahWithOrbBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
}

void LichHandler(int unit)
{
    UnitZeroFleeRange(unit);
    UnitLinkBinScript(unit, LichLordBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
}

int SummonInfo(int num)
{
    int data[42];

    if (num < 0)
    {
        //Fast = 0xa(10), Medium = 0xf(15), Slow = 0x14(20)
        data[0] = 0x1401; data[1] = 0xf01; data[2] = 0xf01;
        data[3] = 0xf04; data[4] = 0xf04; data[5] = 0xf04;
        data[6] = 0xa04; data[7] = 0xf02; data[8] = 0xf04;
        data[9] = 0xf04; data[10] = 0xa04; data[11] = 0xf04;
        data[12] = 0xf04; data[13] = 0xf04; data[14] = 0xa04;
        data[15] = 0xa04; data[16] = 0xf04; data[17] = 0x1402;
        data[18] = 0xa04; data[19] = 0xa04; data[20] = 0xf04;
        data[21] = 0xa08; data[22] = 0xf01; data[23] = 0x1901;
        data[24] = 0x1901; data[25] = 0x1901; data[26] = 0x1401;
        data[27] = 0x1401; data[28] = 0xf02; data[29] = 0x1402;
        data[30] = 0xa08; data[31] = 0xa08; data[32] = 0xf04;
        data[33] = 0xf04; data[34] = 0xf01; data[35] = 0xa08;
        data[36] = 0xf08; data[37] = 0xf04; data[38] = 0xa08;
        data[39] = 0x1401; data[40] = 0x1804; data[41] = 0x1802;
        return 0;
    }
    return data[num];
}

void NecroWeapon()
{
    int mis;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.1);
            LookWithAngle(SELF, OTHER);
            MoveWaypoint(9, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 20.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 20.0));
            AudioEvent("ForceOfNatureRelease", 9);
            mis = CreateObject("DeathBallFragment", 9);
            SetOwner(SELF, mis);
            PushObject(mis, -20.0, GetObjectX(OTHER), GetObjectY(OTHER));
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(25, GetTrigger(), ResetUnitSight);
        }
    }
}

void GhostHiting()
{
    if (HasEnchant(OTHER, "ENCHANT_HELD"))
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < 60.0)
        {
            ResumeLevel(SELF, 1.0);
            HitLocation(SELF, GetObjectX(OTHER), GetObjectY(OTHER));
        }
    }
}

string RewardArmor(int num)
{
    string name[] = {
        "OrnateHelm", "Breastplate", "PlateArms",
        "PlateBoots", "PlateLeggings", "ChainCoif",
        "ChainLeggings", "ChainTunic", "ConjurerHelm",
        "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots",
        "LeatherBoots", "LeatherHelm", "LeatherLeggings",
        "WizardHelm", "WizardRobe", "MedievalCloak",
        "MedievalPants", "MedievalShirt", "SteelShield",
        "WoodenShield"
    };
    return name[num];
}

string RewardWeapon(int num)
{
    string name[] = {
        "StaffWooden", "CrossBow", "Quiver",
        "Bow", "RoundChakram", "FanChakram",
        "BattleAxe", "OgreAxe", "WarHammer",
        "GreatSword", "MorningStar", "Sword",
        "Longsword", "OblivionHalberd", "OblivionHeart",
        "OblivionWierdling"
    };
    return name[num];
}

string RewardPotion(int num)
{
    string name[] = {
        "RedPotion", "BluePotion", "CurePoisonPotion",
        "HastePotion", "VampirismPotion", "InfravisionPotion",
        "InvulnerabilityPotion", "FireProtectPotion", "ShockProtectPotion",
        "PoisonProtectPotion", "YellowPotion", "BlackPotion", "WhitePotion"
    };
    return name[num];
}

string RewardStaff(int num)
{
    string name[] = {
        "DeathRayWand", "ForceWand", "FireStormWand",
        "LesserFireballWand", "InfinitePainWand", "SulphorousFlareWand",
        "SulphorousShowerWand"
    };
    return name[num];
}

int checkMapBoundary(int location)
{
    float pos_x = GetWaypointX(location);
    float pos_y = GetWaypointY(location);

    if (pos_x > 100.0 && pos_y > 100.0 && pos_x < 5532.0 && pos_y < 5600.0)
        return 1;
    else
        return 0;
}

void ShotDeathray()
{
    int ptr = GetTrigger() + 1;
    int plr = GetDirection(ptr);

    CastSpellObjectObject("SPELL_DEATH_RAY", player[plr], OTHER);
}

void SetHardcoreMode()
{
    HellMod = HellMod ^ 1;

    if (HellMod)
    {
        UniPrintToAll(MapDescTable(17));
        HARD_CORE = 5.0;
    }
    else
    {
        UniPrintToAll(MapDescTable(18));
        HARD_CORE = 1.0;
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.1);
    if (HasEnchant(unit, "ENCHANT_ANTI_MAGIC"))
        EnchantOff(unit, "ENCHANT_ANTI_MAGIC");
}

void GhostStrike()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.1);
        int subsub = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0));
        Enchant(subsub, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
        Enchant(subsub, "ENCHANT_SHIELD", 0.0);
        SetOwner(SELF, subsub);
        Raise(subsub, ToFloat(GetCaller()));
        LookWithAngle(subsub, 28);
        FrameTimerWithArg(1, subsub, ProjectileBlueRing);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    CreatureFollow(SELF, OTHER);
    AggressionLevel(SELF, 1.0);
}

void ProjectileBlueRing(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    while (1)
    {
        if (CurrentHealth(owner) && CurrentHealth(target))
        {
            if (count && IsVisibleTo(ptr, target))
            {
                if (DistanceUnitToUnit(ptr, target) < 30.0)
                {
                    Damage(target, owner, 20, 0);
                    Effect("RICOCHET", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
                }
                else
                {
                    MoveObject(ptr, GetObjectX(ptr) + UnitRatioX(target, ptr, 16.0), GetObjectY(ptr) + UnitRatioY(target, ptr, 16.0));
                    LookWithAngle(ptr, count - 1);
                    FrameTimerWithArg(1, ptr, ProjectileBlueRing);
                    break;
                }
            }
        }
        Delete(ptr);
        break;
    }
}

void AwardEnergyPar()
{
    int plr = CheckPlayer();

    if (plr + 1)
    {
        if (GetGold(OTHER) >= 8000)
        {
            if (PlayerClassCheckFlag(plr, PLAYER_SKILL_FLAG1))
                UniPrint(OTHER, MapDescTable(11));
            else
            {
                PlayerClassSetFlag(plr, PLAYER_SKILL_FLAG1);
                ChangeGold(OTHER, -8000);
                UniPrint(OTHER, MapDescTable(12));
            }
        }
        else
        {
            UniPrint(OTHER, MapDescTable(13));
        }
    }
}

void AwardAutoDeathray()
{
    int plr = CheckPlayer();

    if (plr + 1)
    {
        if (GetGold(OTHER) >= 10000)
        {
            if (PlayerClassCheckFlag(plr, PLAYER_SKILL_FLAG2))
                UniPrint(OTHER, MapDescTable(11));
            else
            {
                PlayerClassSetFlag(plr, PLAYER_SKILL_FLAG2);
                ChangeGold(OTHER, -10000);
                UniPrint(OTHER, MapDescTable(14));
            }
        }
        else
            UniPrint(OTHER, MapDescTable(15));
    }
}

void PutSkillShop()
{
    int unit = DeadUnit("Wizard", 217, 96);

    DeadUnit("WizardGreen", 218, 96);

    SetDialog(unit, "A", AwardEnergyPar, DummyFunction);
    SetDialog(unit + 1, "A", AwardAutoDeathray, DummyFunction);
}

int DeadUnit(string name, int wp, int dir)
{
    int unit = CreateObject(name, wp);

    LookWithAngle(unit, dir);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    return unit;
}

int GetUserStringCount(string pick)
{
    return m_descTotalCount >> 1;
}

string MapDescTable(int sIndex)
{
    string *ptr;

    if (SToInt(ptr) == NULLPTR)
    {
        string desc[] = {
            "You can't join this map. please read the follow some cause.",
            "This map only allow 2 class. warrior and conjurer. and user counts is over at 10",
            "did teleporting",
            "save previous position. teleport to startLocation",
            "if you want go to back, try using teleport potal",
            "this well healing your health at 50",
            "auto charging chakram Mode Status: ",
            "MISSING:007",
            "MISSING:008",
            "MISSING:009",
            "MISSING:010",
            "already you had this ability! trade failed",
            "now you have a new ability! try use TreadLightly",
            "not enough gold you need 8000 gold",
            "new ability is done. try press L key",
            "not enough gold you need 10000 gold",
            "You Win! You have been passed in all sections",
            "HardCoreMode does working. more fasted speed for summon to mob and increase all monster's stat",
            "back to Easy mode",
            "Trade is complete! OblivionOrb Staff here",
            "not enough gold. you need 50000 gold for buy oblivion orb staff!",
            "MISSING:021",
            "MISSING:022",
            "MISSING:023",
            "MISSING:024",
            "MISSING:025",
            "MISSING:026",
            "Escorting\nHP: ",
            "MISSING:028",
            "MISSING:029",
            "MISSING:030",
            "MISSING:031",
            "MISSING:032",
            "MISSING:033",
            "MISSING:034",
            "MISSING:035",
            "MISSING:036",
            //영문 여기부터 추가
            // "0000END",

            //0: 
            "이 맵에 참가하실 수 없습니다. 아래의 사항을 참고해 주십시오",
            //1:
            "기술적으로 이 맵은 '전사, 소환술사' 만 입장가능합니다, 또는 이 맵이 수용가능한 인원 10명이 초과되었습니다",
            //2: 
            "이동했습니다",
            //3: 
            "공간이동 직전 위치가 저장되었습니다. 시작지점으로 이동되었습니다",
            //4:
            "저장된 위치로 돌아가려면 공간이동 포탈을 이용하세요",
            //5: 
            "이 우물이 당신의 체력을 50 회복 시켜줍니다",
            //6: 
            "자동 박돌모드: ",
            //7: 
            "처리결과: 총 ",
            //8: 
            " 개의 아이템을 무적화 했습니다",
            //9: 
            "금화가 부족합니다. 무기 내구도 무한의 가격은 10000골드 입니다",
            //10: 
            "금화가 부족합니다. 갑옷 내구도 무한의 가격은 10000골드 입니다",
            //11: 
            "이미 이 스킬을 배우셨습니다",
            //12: 
            "스킬 습득완료! -조심스럽게 걷기를 사용하시면 습득한 스킬이 발동됩니다",
            //13: 
            "골드가 부족합니다, 스킬을 배우려면 8천원이 필요해요",
            //14:,
            "스킬 습득완료! -웃기 버튼을 누르시면 스킬이 발동됩니다",
            //15: 
            "골드가 부족하군요, 이 스킬을 배우려면 만원이 필요해요",
            //16: 
            "승리__!! 모든 구역을 통과하셨습니다",
            //17: 
            "하드코어 모드가 작동되었습니다. 소환속도가 70% 증가되고 몬스터가 더 강해집니다",
            //18: 
            "이지 모드로 돌아갑니다. 소환속도를 원래상태로 되돌립니다",
            //19: 
            "망각의 지팡이를 구입했습니다",
            //20: 
            "금화가 부족합니다, 망각의 지팡이는 5만 골드 입니다",
            //21: 
            "크리쳐 1 개를 구입했습니다. 크리쳐를 클릭하면 당신을 경호합니다",
            //22: 
            "이런, 금화가 부족하군요... 소환 수 하나당 3만 골드이라는 거금이 필요하죠",
            //23: 
            "크리쳐 소환완료",
            //24: 
            "유저가 소환한 크리쳐\n남은 체력:",
            //25: 
            "당신이 소환한 크리쳐가 방금 전 사망했습니다",
            //26: 
            "흐앗... 죽어버렸다",
            //27: 
            "경호중...\n현재체력: ",
            //28: 
            "왔던 곳으로 되돌아가는 공간이동 포탈입니다",
            //29: 
            "크리쳐 구입하는 곳. 크리쳐를 구입하려면 원하는 크리쳐를 클릭하세요. 3만골드 필요",
            //30: 
            "망각의 지팡이 구입하는 곳. 5만 골드가 필요합니다",
            //31: 
            "루트A 던전_ 주로 언데드 계열의 몬스터가 나옵니다",
            //32: 
            "루트B 외곽던전_ 주로 생체, 독성 계열 몬스터가 나옵니다",
            //33: 
            "자동 박돌 제어 스위치",
            //34: 
            "게임 플레이 난이도 제어 스위치 (이지- 하드 모드)",
            //35: 
            "스킬 구입- 라이트닝 스톰(8000 골드)",
            //36: 
            "스킬 구입- 자동 타게팅 데스레이(10000 골드)"
        };
        m_descTotalCount = sizeof(desc);
        ptr = &desc;
    }
    //한글 여기부터 추가
    return ptr[sIndex + m_langmode];
}


static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);
    
    if (pUnit)
        ShowQuestIntroOne(pUnit, 237, "NoxWorldMap", "Noxworld.wnd:NotReady");

    if (pInfo==0x653a7c)
    {
        OnInitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}


