
#include "test1768_sub.h"
#include "test1768_phand.h"
#include "test1768_net.h"
#include "test1768_utils.h"
#include "test1768_resource.h"
#include "libs/coopteam.h"
#include "libs/indexloop.h"
#include "libs/reaction.h"
#include "libs/chatevent.h"
#include "libs/logging.h"
#include "libs/wallutil.h"
#include "libs/cmdline.h"
#include "libs/network.h"
#include "libs/game_flags.h"

int *m_pImgVector, m_pShootFuncs[MAX_UPGRADE_LEVEL];
int m_player[10];
int ArrTrp1, LastUnit, ArrTrp2, ArrTrp3, ArrTrp4, ArrTrp5, ArrTrp6;
int m_Kills[10], MaxLv = 40, m_FrogAtkFunc = 0, CampArea;
float FGap[2];

static int GetPlayer(int index) //virtual
{
    return m_player[index];
}
static int GetPlayerKills(int index) //virtual
{
    return m_Kills[index];
}

int PlrStruct(int plr)
{
    int arr[10], i;

    if (!arr[0])
    {
        for (i = 0 ; i < 10 ; i ++)
            arr[i] = CreateObject("InvisibleLightBlueHigh", 1);
    }
    return arr[plr];
}

int GetPlayerLevel(int plr)
{
    return GetDirection(PlrStruct(plr));
}

void SetPlayerLevel(int plr, int lv)
{
    LookWithAngle(PlrStruct(plr), lv);
}

int GetPlayerHealRate(int plr)
{
    return ToInt(GetObjectZ(PlrStruct(plr)));
}

void SetPlayerHealRate(int plr, int rate)
{
    Raise(PlrStruct(plr), ToFloat(rate));
}

void PlayerAutoHeal(int plr)
{
    int rate[10];

    if (rate[plr]) rate[plr] --;
    else
    {
        RestoreHealth(m_player[plr], 1);
        rate[plr] = GetPlayerHealRate(plr);
    }
}

void EnableLavaFireballTrap()
{
    int trp = Object("LavaFireTrp");

    ObjectOn(trp);
    ObjectOn(trp + 2);
    FrameTimerWithArg(1, trp, DelayDisableUnit);
    FrameTimerWithArg(1, trp + 2, DelayDisableUnit);
}

void LowerRightWalls()
{
    int i;

    if (!i)
    {
        for (i = 0 ; i < 4 ; i ++)
            WallOpen(Wall(67 - i, 221 + i));
        UniPrint(OTHER, "벽이 낮아졌습니다");
    }
}

void BossRoomKeyWallsOpen()
{
    int i;
    ObjectOff(SELF);
    for (i = 0 ; i < 4 ; i ++)
    {
        if (i < 3)
            WallOpen(Wall(79 + i, 215 + i));
        WallClose(Wall(67 - i, 221 + i));
    }
}

void LowerKeyElevator()
{
    ObjectOff(SELF);
    TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
    BossRoomCardKey();
    ObjectOn(Object("CardKeyElev"));
    FrameTimerWithArg(22, Object("CardKeyElev"), DelayDisableUnit);
    AudioEvent("CreatureCageAppears", 1);
    FrameTimer(20, BossRoomKeyWallsOpen);
}

int BossRoomCardKey()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectAt("BlueOrbKeyOfTheLich", 2151.0, 5018.0);
    }
    return unit;
}

int UnderfootSentry(int idx)
{
    int arr[6];

    if (!arr[0])
    {
        int i;
        arr[0] = Object("BossRoomSentryTrp");
        for (i = 1 ; i < 6 ; i ++)
            arr[i] = arr[0] + (i * 2);
        RayController(0);
        Raise(RayController(0), ToFloat(arr[0]));
        Raise(RayController(1), ToFloat(arr[2]));
        Raise(RayController(2), ToFloat(arr[4]));
        ObjectOff(RayController(1) + 3);
        FrameTimer(3, PlaceRayBeacons);
        return 0;
    }
    return arr[idx];
}

int RayController(int idx)
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("InvisibleLightBlueHigh", 1);
        CreateObject("InvisibleLightBlueHigh", 1);
        CreateObject("InvisibleLightBlueHigh", 1);
        CreateObject("InvisibleLightBlueHigh", 1);
        CreateObject("InvisibleLightBlueHigh", 1);
        CreateObject("InvisibleLightBlueHigh", 1);
        return 0;
    }
    return unit + idx;
}

void PlaceRayBeacons()
{
    int i;

    for (i = 0 ; i < 5 ; i ++)
    {
        TeleportLocationVector(113, -23.0, 23.0);
        TeleportLocationVector(114, -23.0, 23.0);
        TeleportLocationVector(115, -23.0, 23.0);
        PlaceUnderfootRayTrapBeacon(LocationX(113), LocationY(113), 0);
        PlaceUnderfootRayTrapBeacon(LocationX(114), LocationY(114), 1);
        PlaceUnderfootRayTrapBeacon(LocationX(115), LocationY(115), 2);
    }
}

int PlaceUnderfootRayTrapBeacon(float x, float y, int idx)
{
    int unit = CreateObjectAt("WeirdlingBeast", x, y);

    SetUnitMaxHealth(unit, 10);
    LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", x, y), idx);
    Enchant(unit + 1, "ENCHANT_ANCHORED", 0.0);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, StartUnderfootRayTrap);
    return unit;
}

void StartUnderfootRayTrap()
{
    if (CurrentHealth(OTHER))
    {
        int ptr = RayController(GetDirection(GetTrigger() + 1));
        if (!GetDirection(ptr))
        {
            LookWithAngle(ptr, 1);
            SetUnderfootRayTrapStatus(ptr, 1);
            FrameTimerWithArg(6, ptr, ControlUnderfootRayTrp);
        }
    }
}

void SetUnderfootRayTrapStatus(int ptr, int status)
{
    if (status)
    {
        ObjectOn(ToInt(GetObjectZ(ptr)));
        ObjectOn(ToInt(GetObjectZ(ptr)) + 2);
    }
    else
    {
        ObjectOff(ToInt(GetObjectZ(ptr)));
        ObjectOff(ToInt(GetObjectZ(ptr)) + 2);
    }
}

void ControlUnderfootRayTrp(int ptr)
{
    if (IsObjectOn(ptr + 3))
        Raise(ptr + 3, 1.0);
    else
        Raise(ptr + 3, -1.0);
    MovingUnderfootSentry(ptr);
}

void MovingUnderfootSentry(int ptr)
{
    int count = GetDirection(ptr + 3), trp = ToInt(GetObjectZ(ptr));
    float vect = GetObjectZ(ptr + 3);

    if (count < 69)
    {
        MoveObject(trp, GetObjectX(trp) + vect, GetObjectY(trp) + vect);
        MoveObject(trp + 2, GetObjectX(trp + 2) - vect, GetObjectY(trp + 2) - vect);
        LookWithAngle(ptr + 3, count + 1);
        FrameTimerWithArg(1, ptr, MovingUnderfootSentry);
    }
    else
    {
        SetUnderfootRayTrapStatus(ptr, 0);
        FrameTimerWithArg(15, ptr, ResetUnderfootRay);
    }
}

void ResetUnderfootRay(int ptr)
{
    ObjectToggle(ptr + 3);
    LookWithAngle(ptr, 0);
}

int PlayerExperienceTable(int lv)
{
    int arr[50], k;

    if (!arr[0])
    {
        arr[0] = lv;
        for (k = 1 ; k < 50 ; k ++)
            arr[k] = (arr[k - 1] * 2) + (k * 2);
        return 0;
    }
    return arr[lv];
}

void OpenPassageWalls()
{
    int i;

    ObjectOff(SELF);
    for (i = 0 ; i < 6 ; i ++)
    {
        if (i < 5)
            WallOpen(Wall(106 - i, 202 + i));
        WallOpen(Wall(101 + i, 207 + i));
    }
    StartSummonFrog(109, 10);
    UniPrint(OTHER, "비밀의 벽이 열립니다");
}

void InitUnderground()
{
    UnderfootSentry(0);
    PutLightGenerator(102);
    PutLightGenerator(103);
    PutLightGenerator(104);
    SetCallback(PutUnderPartBeacon(LocationX(127), LocationY(127), 0), 9, RemoveUnderfootWalls);
    SetCallback(PutUnderPartBeacon(LocationX(128), LocationY(128), 1), 9, RemoveUnderfootWalls);
    SetCallback(PutUnderPartBeacon(LocationX(129), LocationY(129), 2), 9, RemoveUnderfootWalls);
    StartItemRing("RedPotion", 110, 18);
    FrameTimer(1, PutUnderfootMovingWalk);
    CreateObjectById(OBJ_RUBY_KEY, LocationX(126), LocationY(126));
}

void PutUnderfootMovingWalk()
{
    PutMovingWalkToSouth(LocationX(105), LocationY(105), 26);
    PutMovingWalkToSouth(LocationX(106), LocationY(106), 26);
    PutMovingWalkToWest(LocationX(107), LocationY(107), 22);
    PutMovingWalkToWest(LocationX(108), LocationY(108), 22);
}

void RemoveWideRoomWalls(int arg)
{
    int run, arr[3], val = arg & 0xff, idx = arg >> 8, i;

    if (run) return;
    arr[idx] = val;
    if (arr[0] && arr[1] && arr[2])
    {
        run = 1;
        for (i = 0 ; i < 11 ; i ++)
        {
            WallOpen(Wall(138 - i, 198 + i));
            WallOpen(Wall(147 - i, 207 + i));
        }
    }
}

int SlideBeacon(float x, float y, int dir)
{
    int unit = CreateObjectAt("WeirdlingBeast", x, y);

    SetUnitMaxHealth(unit, 10);
    LookWithAngle(unit, dir);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, SlideOnBeacon);
    SetUnitFlags(unit, GetUnitFlags(unit)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    return unit;
}

void SlideOnBeacon()
{
    int unit;
    float vect = -1.0;

    if (CurrentHealth(OTHER))
    {
        if (GetDirection(SELF))
            vect = -vect;
        unit = CreateObjectAt("NecromancerMarker", GetObjectX(OTHER) + vect, GetObjectY(OTHER) - 1.0);
        Frozen(unit, 1);
        DeleteObjectTimer(unit, 1);
    }
}

int PutMovingWalkToSouth(float x, float y, int count)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", x, y), i;

    for (i = 0 ; i < count ; i ++)
    {
        SlideBeacon(GetObjectX(unit), GetObjectY(unit), 0);
        MoveObjectVector(unit, 23.0, 23.0);
    }
    Delete(unit);
    return unit + 1;
}

int PutMovingWalkToWest(float x, float y, int count)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", x, y), i;

    for (i = 0 ; i < count ; i ++)
    {
        SlideBeacon(GetObjectX(unit), GetObjectY(unit), 1);
        MoveObjectVector(unit, -23.0, 23.0);
    }
    Delete(unit);
    return unit + 1;
}

void RemoveWaterParkWall1()
{
    ObjectOff(SELF);
    WallToggle(Wall(143, 187));
    WallToggle(Wall(144, 188));
    int arg = (arg + 1) % 2;
    RemoveWideRoomWalls(arg);
    FrameTimerWithArg(105, GetTrigger(), DelayEnableUnit);
}

void RemoveWaterParkWall2()
{
    ObjectOff(SELF);
    WallToggle(Wall(150, 194));
    WallToggle(Wall(151, 195));
    int arg = (arg + 1) % 2;
    RemoveWideRoomWalls(arg | (1 << 8));
    FrameTimerWithArg(105, GetTrigger(), DelayEnableUnit);
}

void RemoveWaterParkWall3()
{
    ObjectOff(SELF);
    WallToggle(Wall(158, 202));
    WallToggle(Wall(159, 203));
    int arg = (arg + 1) % 2;
    RemoveWideRoomWalls(arg | (2 << 8));
    FrameTimerWithArg(105, GetTrigger(), DelayEnableUnit);
}

int PutUnderPartBeacon(float x, float y, int flag)
{
    int unit = CreateObjectAt("WeirdlingBeast", x, y), i;

    SetUnitMaxHealth(CreateObjectAt("InvisibleLightBlueHigh", x, y) - 1, 10);
    LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", x, y), flag);
    for (i = 1 ; i <= 12 ; i ++)
        Enchant(unit + 1, ChakramPowerLevel(i), 0.0);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, BeaconDefaultCollide);

    return unit;
}

void BeaconDefaultCollide()
{
    if (CurrentHealth(OTHER))
    {
        Delete(GetTrigger() + 2);
        Delete(GetTrigger() + 1);
        Delete(SELF);
    }
}

void RemoveUnderfootWalls()
{
    int i, flag, x, y;

    if (!CurrentHealth(OTHER))
        return;

    if (GetUnitClass(OTHER) & UNIT_CLASS_MONSTERGENERATOR)
    {
        flag = GetDirection(GetTrigger()+2);
        GeneratorAllign();
        if (flag)
        {
            if (flag ^ 1)
            {
                x = 152;
                y = 208;
            }
            else
            {
                x = 143;
                y = 199;
            }
        }
        else
        {
            x = 134;
            y = 190;
        }
        for (i = 0 ; i < 4 ; i ++)
            WallOpen(Wall(x + i, y + i));
    }
}

void GeneratorAllign()
{
    MoveObject(OTHER, GetObjectX(SELF), GetObjectY(SELF));
    Frozen(OTHER, TRUE);
    BeaconDefaultCollide();
}

int PutLightGenerator(int wp)
{
    int unit = CreateObject("WillOWispGenerator", wp);

    ObjectOff(unit);
    Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
    SetUnitMass(unit, 22.0);
    return unit;
}

void StartUndergroundRot()
{
    int ptr = Object("UnderLeftRot1"), i;

    for (i = 0 ; i < 10 ; i ++)
        Move(ptr + (i*2), 86 + i);
}

void BasecampInit()
{
    int ptr = CreateObject("RedPotion", 1);
    
    MagicalItem();
    SpawnNest(2151.0, 2935.0, MyNestDestroy);
    SpawnNest(LocationX(119), LocationY(119), PotionNestDestroy);
    SpawnNest(LocationX(120), LocationY(120), PotionNestDestroy);
    SpawnNest(LocationX(121), LocationY(121), PotionNestDestroy);
    SpawnNest(LocationX(122), LocationY(122), PotionNestDestroy);
    SpawnNest(LocationX(123), LocationY(123), PotionNestDestroy);
    SpawnNest(3687.0, 2812.0, DefaultNestDestroy);
    SpawnNest(3417.0, 3724.0, DefaultNestDestroy);
    SpawnNest(3748.0, 1912.0, DefaultNestDestroy);
    SpawnNest(3923.0, 2351.0, DefaultNestDestroy);
    SpawnNest(4401.0, 3518.0, DefaultNestDestroy);
    SpawnNest(4420.0, 3558.0, DefaultNestDestroy);
    SpawnNest(4440.0, 3601.0, DefaultNestDestroy);
    SpawnNest(4909.0, 2858.0, DefaultNestDestroy);
    Delete(ptr);
    FrameTimer(1, InitUnderground);
}

void NestDestroyFx(int hive)
{
    UnitNoCollide(CreateObjectAt("WaspNestDestroy", GetObjectX(hive), GetObjectY(hive)));
}

void PotionNestDestroy()
{
    TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
    NestDestroyFx(SELF);
    Delete(SELF);
    DeleteObjectTimer(CreateObject("GreenSmoke", 1), 9);
    if (Random(0, 1))
    {
        CreateYellowPotion(125, LocationX(1), LocationY(1));
        CreateYellowPotion(125, LocationX(1), LocationY(1));
        CreateYellowPotion(125, LocationX(1), LocationY(1));
    }
    else
    {
        CreateObject("RedPotion", 1);
        CreateObject("RedPotion", 1);
        CreateObject("RedPotion", 1);
    }
    AudioEvent("PotionDrop", 1);
}

void DefaultNestDestroy()
{
    TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
    NestDestroyFx(SELF);
    Delete(SELF);
    DeleteObjectTimer(CreateObject("GreenSmoke", 1), 9);
    FieldRewardPic(CreateObjectAt("InvisibleLightBlueHigh", LocationX(1), LocationY(1)));
    AudioEvent("TrollFlatus", 1);
}

void MyNestDestroy()
{
    TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
    NestDestroyFx(SELF);
    Delete(SELF);
    DeleteObjectTimer(CreateObject("GreenSmoke", 1), 9);
    MoveObject(MagicalItem(), LocationX(1), LocationY(1));
    AudioEvent("KeyDrop", 1);
}

int MagicalItem()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectAt("RedOrbKeyOfTheLich", 2238.0, 2907.0);
        Enchant(unit, "ENCHANT_FREEZE", 0.0);
    }
    return unit;
}

void FirstGateDisableLock()
{
    if (HasItem(OTHER, MagicalItem()))
    {
        ObjectOff(SELF);
        UnlockDoor(Object("FstGate1"));
        UnlockDoor(Object("FstGate10"));

        UniPrint(OTHER, "게이트의 잠금이 해제되었습니다, 이 보안카드는 더 이상 필요하지 않으므로 제거됩니다");
        Delete(MagicalItem());
        SpawnTeleportMark(1598.0, 2748.0, 1654.0, 2696.0);
        SpawnTeleportMark(1644.0, 2794.0, 1696.0, 2746.0);
        FrameTimer(30, ChatMessageLoop);
    }
    else
        UniPrint(OTHER, "이 스위치를 조작하려면 보안카드가 필요합니다");
}

void DestinationBeaconSpawn(float x, float y)
{
    int unit = CreateObjectAt("Horrendous", x, y);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, DestinationEntry);
}

void DestinationEntry()
{
    int check;

    if (IsPlayerUnit(OTHER) && CurrentHealth(OTHER))
    {
        Delete(SELF);
        if (!check)
        {
            check = 1;
            UniPrint(OTHER, "미션완료!! 최종 목적지 도착!! -_-+ ㅊㅋㅊㅋ");
            UniPrint(OTHER, "사실 아직 미완성이지만 어쨌거나 다 깬거임!! ㅠㅠ");
            Effect("WHITE_FLASH", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            TeleportLocation(1, LocationX(85), LocationY(85));
            AudioEvent("JournalEntryAdd", 1);
            AudioEvent("AmuletDrop", 1);
            StrNotComplete();
        }
    }
}

int UndergroundGoldKey()
{
    int key;
    if (!IsObjectOn(key)) key = CreateObject("GoldKey", 84);
    return key;
}

void UnderPartLeftWallsBreak()
{
    int i;

    ObjectOff(SELF);
    UndergroundGoldKey();
    NoWallSound(1);
    for (i = 0 ; i < 5 ; i ++)
        WallOpen(Wall(84 + i, 160 + i));
    NoWallSound(0);
    DestroyWallFx(1941.0, 3691.0, 23.0, 23.0, 5, 9);
    TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("WallDestroyed", 1);
    AudioEvent("GolemHitting", 1);
    Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 21.0, 0.0);
}

void UnderPartRightWallsBreak()
{
    int i;

    ObjectOff(SELF);
    NoWallSound(1);
    for (i = 0 ; i < 6 ; i ++)
        WallOpen(Wall(81 + i, 153 + i));
    NoWallSound(0);
    DestroyWallFx(1874.0, 3529.0, 23.0, 23.0, 6, 9);
    TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("WallDestroyed", 1);
    AudioEvent("GolemHitting", 1);
    Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 21.0, 0.0);
}

void DestroyWallFx(float x, float y, float xVect, float yVect, int count, int delay)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", x, y);

    Raise(unit, xVect);
    Raise(CreateObjectAt("InvisibleLightBlueHigh", x, y), yVect);
    LookWithAngle(unit, count);
    FrameTimerWithArg(delay, unit, DestroyWallProc);
}

void DestroyWallProc(int ptr)
{
    int i, count = GetDirection(ptr);
    float xVect = GetObjectZ(ptr), yVect = GetObjectZ(ptr + 1);

    for (i = 0 ; i < count ; i ++)
    {
        DeleteObjectTimer(CreateObjectAt("Smoke", GetObjectX(ptr), GetObjectY(ptr)), 12);
        MoveObject(ptr, GetObjectX(ptr) + xVect, GetObjectY(ptr) + yVect);
    }
    Delete(ptr + 1);
    Delete(ptr);
}

void StartRotPusher()
{
    int ptr = MiniRotStrike();

    ObjectOff(SELF);
    if (!GetDirection(ptr + 1))
    {
        LookWithAngle(ptr + 1, 1);
        FrameTimerWithArg(3, ptr, PusherToEast);
        UniPrint(OTHER, "함정카드 발동!");
    }
    FrameTimerWithArg(60, GetTrigger(), DelayEnableUnit);
}

void PusherToEast(int ptr)
{
    int count = GetDirection(ptr);
    if (count < 69)
    {
        MoveObject(ptr, GetObjectX(ptr) + 1.0, GetObjectY(ptr) - 1.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 1.0, GetObjectY(ptr + 1) - 1.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + 1.0, GetObjectY(ptr + 2) - 1.0);
        RotPusherUpdate(ptr);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, PusherToEast);
    }
    else
    {
        LookWithAngle(ptr, 0);
        FrameTimerWithArg(1, ptr, PusherToSouth);
    }
}

void PusherToWest(int ptr)
{
    int count = GetDirection(ptr);
    if (count < 69)
    {
        MoveObject(ptr, GetObjectX(ptr) - 1.0, GetObjectY(ptr) + 1.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - 1.0, GetObjectY(ptr + 1) + 1.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) - 1.0, GetObjectY(ptr + 2) + 1.0);
        RotPusherUpdate(ptr);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, PusherToWest);
    }
    else
    {
        LookWithAngle(ptr, 0);
        LookWithAngle(ptr + 1, 0);
    }
}

void PusherToSouth(int ptr)
{
    int count = GetDirection(ptr);
    if (count < 92)
    {
        MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) + 2.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 2.0, GetObjectY(ptr + 1) + 2.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + 2.0, GetObjectY(ptr + 2) + 2.0);
        RotPusherUpdate(ptr);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, PusherToSouth);
    }
    else
    {
        LookWithAngle(ptr, 0);
        FrameTimerWithArg(105, ptr, PusherToNorth);
    }
}

void PusherToNorth(int ptr)
{
    int count = GetDirection(ptr);
    if (count < 92)
    {
        MoveObject(ptr, GetObjectX(ptr) - 2.0, GetObjectY(ptr) - 2.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - 2.0, GetObjectY(ptr + 1) - 2.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) - 2.0, GetObjectY(ptr + 2) - 2.0);
        RotPusherUpdate(ptr);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, PusherToNorth);
    }
    else
    {
        LookWithAngle(ptr, 0);
        FrameTimerWithArg(1, ptr, PusherToWest);
    }
}

void RotPusherUpdate(int ptr)
{
    MoveObject(ptr + 3, GetObjectX(ptr), GetObjectY(ptr));
    MoveObject(ptr + 4, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
    MoveObject(ptr + 5, GetObjectX(ptr + 2), GetObjectY(ptr + 2));
}

int MiniRotStrike()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("InvisibleLightBlueLow", 74);
        CreateObject("InvisibleLightBlueLow", 75);
        CreateObject("InvisibleLightBlueLow", 76);
        CreateObject("RotatingSpikes", 74);
        CreateObject("RotatingSpikes", 75);
        CreateObject("RotatingSpikes", 76);
    }
    return unit;
}

int WestShotFireTraps(int wp, int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", wp), k;
    LookWithAngle(unit, count);
    for (k = 0 ; k < count ; k ++)
    {
        ObjectOff(CreateObjectAt("Skull3", GetObjectX(unit), GetObjectY(unit)));
        MoveObject(unit, GetObjectX(unit) + 23.0, GetObjectY(unit) + 23.0);
    }
    return unit;
}

void EntrySwampHouse()
{
    ObjectOff(SELF);

    StartSummonFrog(72, 10);
    StartSummonFrog(71, 10);
    StartSummonFrog(70, 10);
    RoundingTrap(68);
    RoundingTrap(69);
}

void deferredEnableObject(int obj)
{
    if (ToInt(GetObjectX(obj)))
    {
        ObjectOn(obj);
    }
}

void StartOgreElevator()
{
    int unit = Object("OgreElevatorUp");

    if (!IsObjectOn(unit))
    {
        ObjectOff(SELF);
        TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("Gear3", 1);
        ObjectOn(unit);
        PushTimerQueue(24, unit, DelayDisableUnit);
        PushTimerQueue(26, GetTrigger(), deferredEnableObject);
    }
}

void StartOgrePart()
{
    ObjectOff(SELF);
    // TeleportLocation(9, 2954.0, 2519.0);
    SummonFrogAmount(63, 4);
    SummonFrogAmount(64, 4);
    UnlockDoor(Object("OgreLocked1"));
    UnlockDoor(Object("OgreLocked2"));
    StartItemRing("RedPotion", 81, 18);
    StartItemRing("RedPotion", 82, 18);
    StartItemRing("RedPotion", 83, 18);
    StartItemRing("RedPotion", 134, 18);
    StartItemRing("RedPotion", 135, 18);
    StartItemRing("RedPotion", 136, 18);
    StartItemRing("RedPotion", 137, 18);
    StartItemRing("RedPotion", 138, 18);
    StartItemRing("RedPotion", 139, 18);
    StartItemRing("RedPotion", 140, 18);
    StartItemRing("RedPotion", 141, 18);
    StartItemRing("RedPotion", 142, 18);
    MiniRotStrike();
    m_FrogAtkFunc = 1; //2;
    // DestinationBeaconSpawn(2641.0, 4260.0);
}

void RemovePart2Wall()
{
    int count, i;

    count ++;
    if (count == 2)
    {
        SecondGoldKey();
        SecondRubyKey();
        StartSummonFrog(65, 5);
        StartSummonFrog(66, 5);
        StartSummonFrog(67, 5);
        for (i = 0 ; i < 4 ; i ++) WallOpen(Wall(154 + i, 138 - i));
        UniPrintToAll("벽이 낮아졌습니다 ...");
    }
    ObjectOff(SELF);
}

int BridgeRow()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("IronBlock", 60);
        LookWithAngle(CreateObject("InvisibleLightBlueHigh", 60), 1);
        Frozen(ptr, 1);
    }
    return ptr;
}

void StartBridgeBlock()
{
    int ptr = BridgeRow();

    if (!GetDirection(ptr))
    {
        TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("SpikeBlockMove", 1);
        LookWithAngle(ptr, 94);
        LookWithAngle(ptr + 1, GetDirection(ptr + 1) ^ 1);
        FrameTimerWithArg(3, ptr, BridgeBlockToggleMove);
    }
}

void BridgeBlockToggleMove(int ptr)
{
    int count = GetDirection(ptr), idx = GetDirection(ptr + 1);
    if (count)
    {
        MoveObject(ptr, GetObjectX(ptr) - FGap[idx], GetObjectY(ptr) - FGap[idx]);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, BridgeBlockToggleMove);
    }
}

int SwampRow()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("SpikeBlock", 57);
        Frozen(CreateObject("SpikeBlock", 58), 1);
        Frozen(ptr, 1);
    }
    return ptr;
}

void StartSwampRow()
{
    ObjectOff(SELF);
    TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("CreatureCageAppears", 1);
    AudioEvent("SpikeBlockMove", 57);
    AudioEvent("SpikeBlockMove", 58);
    FrameTimerWithArg(9, SwampRow(), SwampBlockMoveToEast);
    UniPrint(OTHER, "주변 어딘가에서 동력장치가 움직이기 시작했습니다...");
}

void SwampBlockMoveToEast(int ptr)
{
    if (GetObjectX(ptr) < 4244.0)
    {
        MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) - 2.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 2.0, GetObjectY(ptr + 1) - 2.0);
        FrameTimerWithArg(1, ptr, SwampBlockMoveToEast);
    }
    else
    {
        TeleportLocation(1, GetObjectX(ptr), GetObjectY(ptr));
        AudioEvent("SpikeBlockMove", 1);
        FrameTimerWithArg(6, ptr, SwampBlockMoveToSouth);
    }
}

void SwampBlockMoveToSouth(int ptr)
{
    int k;
    if (GetObjectX(ptr) < 4612.0)
    {
        MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) + 2.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 2.0, GetObjectY(ptr + 1) + 2.0);
        FrameTimerWithArg(1, ptr, SwampBlockMoveToSouth);
    }
    else
    {
        for (k = 0 ; k < 6 ; k ++)
        {
            if (k < 4) WallOpen(Wall(195 + k, 119 + k));
            WallOpen(Wall(191 + k, 119 + k));
        }
    }
}

void InitMapArrowTraps()
{
    FGap[0] = 2.0;
    FGap[1] = -2.0;
    ArrTrp1 = WestShotArrowTraps(56, 8);
    ArrTrp2 = SouthShotArrowTraps(59, 8);
    ArrTrp3 = WestShotFireTraps(73, 3);
    ArrTrp4 = SouthShotArrowTraps(77, 8);
    ArrTrp5 = WestShotArrowTraps(78, 5);
    ArrTrp6 = WestShotArrowTraps(79, 5);
    SwampRow();
    BridgeRow();
}

void Part2WestArrowTrp1()
{
    if (ArrTrp5) ShotArrowTraps(ArrTrp5);
}

void Part2WestArrowTrp2()
{
    if (ArrTrp6) ShotArrowTraps(ArrTrp6);
}

void Part2SouthArrowTrp()
{
    if (ArrTrp4) ShotArrowTraps(ArrTrp4);
}

void Part2WestFireTrap()
{
    if (ArrTrp3) ShotArrowTraps(ArrTrp3);
}

void Part1LastSectArrowTrps()
{
    if (ArrTrp1) ShotArrowTraps(ArrTrp1);
}

void Part2ArrowTraps()
{
    if (ArrTrp2) ShotArrowTraps(ArrTrp2);
}

int WestShotArrowTraps(int wp, int count) //direct: shoot to west
{
    //8 11.0
    int ptr = CreateObject("InvisibleLightBlueHigh", wp), k;

    LookWithAngle(ptr, count);
    for (k = 0 ; k < count ; k ++)
    {
        ObjectOff(CreateObjectAt("ArrowTrap2", GetObjectX(ptr), GetObjectY(ptr)));
        MoveObject(ptr, GetObjectX(ptr) + 11.0, GetObjectY(ptr) + 11.0);
    }
    return ptr;
}

int SouthShotArrowTraps(int wp, int count)
{
    int ptr = CreateObject("InvisibleLightBlueLow", wp), k;

    LookWithAngle(ptr, count);
    for (k = 0 ; k < count ; k ++)
    {
        ObjectOff(CreateObjectAt("ArrowTrap1", GetObjectX(ptr), GetObjectY(ptr)));
        MoveObject(ptr, GetObjectX(ptr) - 11.0, GetObjectY(ptr) + 11.0);
    }
    return ptr;
}

void ShotArrowTraps(int ptr)
{
    int k, max = GetDirection(ptr);

    for (k = 1 ; k <= max ; k ++)
        ObjectOn(ptr + k);
    FrameTimerWithArg(1, ptr, DisableArrowTraps);
}

void DisableArrowTraps(int ptr)
{
    int k, max = GetDirection(ptr);

    for (k = 1 ; k <= max ; k ++) ObjectOff(ptr + k);
}

void EntryPart1LastRoom()
{
    int unit;

    if (!unit)
    {
        unit = StartSummonFrog(55, 50);
        UniChatMessage(unit, "개구리 언버로우 중...", 150);
    }
    ObjectOff(SELF);
}

int StartSummonFrog(int wp, int count)
{
    int unit = CreateObject("StormCloud", wp);

    LookWithAngle(unit, count);
    SummonFrog(wp);
    FrameTimerWithArg(1, unit, SummonFrogAtFrames);
    return unit + 1;
}

void SummonFrogAtFrames(int ptr)
{
    int count = GetDirection(ptr);
    if (count && IsObjectOn(ptr))
    {
        TeleportLocation(1, GetObjectX(ptr), GetObjectY(ptr));
        SummonFrog(1);
        Effect("SMOKE_BLAST", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(4, ptr, SummonFrogAtFrames);
    }
    else
        Delete(ptr);
}

void NeedSecurityKeyForOpen()
{
    int key = SecurityKey(52), i;
    if (IsObjectOn(key))
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(key), GetObjectY(key)) < 30.0)
        {
            Effect("VIOLET_SPARKS", GetObjectX(key), GetObjectY(key), 0.0, 0.0);
            Delete(key);
            ObjectOff(SELF);

            SummonFrogAmount(53, 10);
            for (i = 0 ; i < 7 ; i ++) WallOpen(Wall(96 + i, 30 - i));
            UniPrint(OTHER, "비밀벽이 열립니다");
        }
        else
        {
            UniPrint(OTHER, "이 스위치를 조작하기 위해서는 보안카드가 필요하다");
        }
    }
}

int SecurityKey(int wp)
{
    int key;

    if (!IsObjectOn(key)) key = CreateObject("ProtectionEnchantments", wp);
    return key;
}

void EastSecretWallOpen()
{
    int k;
    ObjectOff(SELF);
    for (k = 0 ; k < 4 ; k ++) WallOpen(Wall(116 + k, 22 + k));
    UniPrint(OTHER, "비밀벽이 열렸습니다");
}

void ExplainBurningRoomKey()
{
    UniPrint(OTHER, "이 열쇠를 사용하려면, 자물쇠 앞에서 이것을 내려놓은 후 스위치를 누르세요");
}

void InitBurningRoom()
{
    int unit = CreateObject("Maiden", 35);
    Frozen(CreateObject("Maiden", 36), 1);
    Frozen(unit, 1);
    Frozen(CreateObject("BlackPowderBarrel2", 35), 1);
    Frozen(CreateObject("BlackPowderBarrel2", 36), 1);
    SetCallback(unit, 9, BurnBucketHandler);
    int key = SecurityKey(52);

    SetUnitCallbackOnUseItem(key, ExplainBurningRoomKey);
}

void BurnBucketHandler()
{
    if (HasClass(OTHER, "FIRE") && MaxHealth(SELF))
    {
        Effect("JIGGLE", GetObjectX(SELF), GetObjectY(SELF), 38.0, 0.0);
        Frozen(GetTrigger() + 3, 0);
        Frozen(GetTrigger() + 2, 0);
        Damage(GetTrigger() + 3, 0, 999, 14);
        Damage(GetTrigger() + 2, 0, 999, 14);
        Delete(GetTrigger() + 1);
        Delete(SELF);
        FrameTimer(1, Part2WallDestroy);
    }
}

void Part2WallDestroy()
{
    int k;
    NoWallSound(1);
    for (k = 0 ; k < 3 ; k ++) WallOpen(Wall(118 + k, 38 + k));
    DeleteObjectTimer(CreateObject("BigSmoke", 35), 9);
    DeleteObjectTimer(CreateObject("BigSmoke", 36), 9);
    DeleteObjectTimer(CreateObject("BigSmoke", 42), 9);
    DeleteObjectTimer(CreateObject("BigSmoke", 43), 9);
    SummonFrogAmount(45, 10);
    SummonFrog(44);
    SummonFrog(46);
    SummonFrog(47);
    SummonFrog(48);
    SummonFrog(49);
    SummonFrog(50);
    SummonFrog(51);
    NoWallSound(0);
}

void spawnSecondGoldKeyFrog()
{
    int f = SummonFrog(125);

    SummonFrog(125);
    SummonFrog(125);
    UniChatMessage(f, "개굴~~ 딱 걸렸다 개굴~!", 120);
}

int SecondGoldKey()
{
    int key;
    if (!IsObjectOn(key))
    {
        key = CreateObject("GoldKey", 61);
        SetUnitPickEvent(key, spawnSecondGoldKeyFrog);
    }
    return key;
}

int SecondRubyKey()
{
    int key;
    if (!IsObjectOn(key)) key = CreateObject("RubyKey", 62);
    return key;
}

void SecondRubyDoor()
{
    if (IsLocked(Object("Part2RubyKeyGate"))) SecondRubyKey();
    else ObjectOff(SELF);
}

void SecondGoldDoor()
{
    if (IsLocked(Object("Part2GoldKeyGate"))) SecondGoldKey();
    else ObjectOff(SELF);
}

int FirstSaphahKey()
{
    int key;
    if (!IsObjectOn(key)) key = CreateObject("SapphireKey", 54);
    return key;
}

int FirstKeyRuby()
{
    int key;

    if (!IsObjectOn(key))
        key = CreateObject("RubyKey", 43);
    return key;
}

void FirstSaphahDoor()
{
    if (IsLocked(Object("SpihDoor"))) FirstSaphahKey();
    else                            ObjectOff(SELF);
}

void FirstRubyDoor()
{
    if (IsLocked(Object("FirstRubyGate")))
        FirstKeyRuby();
    else
        ObjectOff(SELF);
}

void StartFirewalkTrap()
{
    int unit;

    if (!IsObjectOn(unit))
    {
        TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
        unit = CreateObject("InvisibleLightBlueHigh", 1);
        CreateObject("InvisibleLightBlueLow", 37);
        CreateObject("InvisibleLightBlueLow", 38);
        CreateObject("InvisibleLightBlueLow", 39);
        AudioEvent("FireballCast", 1);
        AudioEvent("FirewalkCast", 1);
        FrameTimerWithArg(1, unit, FirewalkTrapHandler);
    }
}

void FirewalkTrapHandler(int ptr)
{
    int count = GetDirection(ptr), unit;

    if (count < 20)
    {
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        CreateObjectAt("SmallFlame", GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr + 2), GetObjectY(ptr + 2)), 15);
        CreateObjectAt("SmallFlame", GetObjectX(ptr + 2), GetObjectY(ptr + 2));
        LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr + 3), GetObjectY(ptr + 3)), 15);
        CreateObjectAt("SmallFlame", GetObjectX(ptr + 3), GetObjectY(ptr + 3));
        LookWithAngle(unit, 21);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 23.0, GetObjectY(ptr + 1) + 23.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + 23.0, GetObjectY(ptr + 2) + 23.0);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) + 23.0, GetObjectY(ptr + 3) + 23.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, unit, SmallFireNext);
        FrameTimerWithArg(1, unit + 2, SmallFireNext);
        FrameTimerWithArg(1, unit + 4, SmallFireNext);
        FrameTimerWithArg(2, ptr, FirewalkTrapHandler);
    }
    else
        Delete(ptr);
}

void SmallFireNext(int ptr)
{
    int count = GetDirection(ptr), unit;

    if (count)
    {
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, SmallFireNext);
    }
    else
    {
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr), GetObjectY(ptr));
        CreateObjectAt("MediumFlame", GetObjectX(unit), GetObjectY(unit));
        LookWithAngle(unit, 12);
        Delete(ptr);
        Delete(ptr + 1);
        FrameTimerWithArg(1, unit, MedFireNext);
    }
}

void MedFireNext(int ptr)
{
    int count = GetDirection(ptr), unit;

    if (count)
    {
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, MedFireNext);
    }
    else
    {
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr));
        CreateObjectAt("Flame", GetObjectX(unit), GetObjectY(unit));
        LookWithAngle(unit, 9);
        Delete(ptr);
        Delete(ptr + 1);
        FrameTimerWithArg(1, unit, HugeFireNext);
    }
}

void HugeFireNext(int ptr)
{
    int count = GetDirection(ptr), unit;
    if (count)
    {
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, HugeFireNext);
    }
    else
    {
        unit = CreateObjectAt("InvisibleLightBlueMed", GetObjectX(ptr), GetObjectY(ptr));
        CreateObjectAt("MediumFlame", GetObjectX(unit), GetObjectY(unit));
        LookWithAngle(unit, 12);
        Delete(ptr);
        Delete(ptr + 1);
        FrameTimerWithArg(1, unit, MedFireTail);
    }
}

void MedFireTail(int ptr)
{
    int count = GetDirection(ptr), unit;
    if (count)
    {
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, MedFireTail);
    }
    else
    {
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr), GetObjectY(ptr));
        CreateObjectAt("SmallFlame", GetObjectX(unit), GetObjectY(unit));
        LookWithAngle(unit, 15);
        Delete(ptr);
        Delete(ptr + 1);
        FrameTimerWithArg(1, unit, SmallFireTail);
    }
}

void SmallFireTail(int ptr)
{
    int count = GetDirection(ptr), unit;
    if (count)
    {
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, SmallFireTail);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void Area2PutFrogs()
{
    int ptr;
    ObjectOff(SELF);
    FirstKeyRuby();
    if (!ptr)
        ptr = SummonFrogAmount(40, 7);
}

void DisableWizWorkRoomDoorLock()
{
    int chk;

    if (MaxHealth(SELF))
    {
        if (!chk)
        {
            chk = 1;
            UnlockDoor(Object("WizWorkRoomGate11"));
            UnlockDoor(Object("WizWorkRoomGate12"));
        }
        Delete(SELF);
    }
}

void DisableWizWorkRoomGoldRoomLock()
{
    ObjectOff(SELF);
    FirstGoldKey();
    UnlockDoor(Object("WizWorkRoomGate1"));
    UnlockDoor(Object("WizWorkRoomGate2"));
    SummonFrogAmount(30, 12);
    SummonFrogAmount(32, 5);
    UniPrint(OTHER, "맞은편 방 문의 잠금이 해제되었습니다");
}

void KeyFirstGoldGate()
{
    if (IsLocked(Object("FirstGoldGate")))
    {
        FirstGoldKey();
    } else ObjectOff(SELF);
}

int FirstGoldKey()
{
    int key;

    if (!IsObjectOn(key))
        key = CreateObject("GoldKey", 31);
    return key;
}

void KeyFirstSilverGate()
{
    if (IsLocked(Object("FirstSilverKeyDoor")))
    {
        FirstSilverKey();
    } else ObjectOff(SELF);
}

int FirstSilverKey()
{
    int key;

    if (!IsObjectOn(key))
    {
        key = CreateObject("SilverKey", 22);
        Raise(CreateObject("InvisibleLightBlueHigh", 22), FirstSilverKeyPickEvent);
        SetUnitPickEvent(key, ToInt(GetObjectZ(key + 1)));
        Delete(key + 1);
    }
    return key;
}

void FirstSilverKeyPickEvent()
{
    TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("JournalEntryAdd", 1);
    SummonFrogAmount(23, 8);
    UniPrint(OTHER, "실버열쇠를 얻었돠.. . ㅇㅅㅇ");
}

void SurpriseRespectFrogs()
{
    int ptr, k, i;

    ObjectOff(SELF);
    if (!ptr)
    {
        KeyFirstSilverGate();
        ptr = 1;
        for (k = 0 ; k < 6 ; k ++)
            SummonFrogAmount(16 + k, 2);
    }
}

void WizWorkRoomFrogs()
{
    int ptr;
    
    ObjectOff(SELF);
    if (!ptr)
    {
        ptr = CreateObject("RedPotion", 1);
        Raise(ptr, DisableWizWorkRoomDoorLock);
        DeadWizBeacon(26, ToInt(GetObjectZ(ptr)));
        DeadWizBeacon(27, ToInt(GetObjectZ(ptr)));
        DeadWizBeacon(28, ToInt(GetObjectZ(ptr)));
        DeadWizBeacon(29, ToInt(GetObjectZ(ptr)));
        SummonFrogAmount(24, 10);
    }
}

int DeadWizBeacon(int wp, int func)
{
    int unit = CreateObject("Wizard", wp);

    SetCallback(unit, 9, func);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    return unit;
}

int SummonFrogAmount(int wp, int amount)
{
    int unit = CreateObject("AmbBeachBirds", wp) + 1, i;
    Delete(unit - 1);

    for (i = 0 ; i < amount ; i ++)
        SummonFrog(wp);
    Effect("SMOKE_BLAST", LocationX(wp), LocationY(wp), 0.0, 0.0);
    AudioEvent("PoisonTrapTriggered", wp);
    return unit;
}

void MakeBeFrog(int target)
{
    int unit = GetOwner(target), ptr;

    if (IsObjectOn(target) && CurrentHealth(unit))
    {
        ptr = UnitToPtr(unit);
        if (ptr)
        {
            SetMemory(ptr + 4, 1313); //TODO: Green Frog Thing ID
            SetUnitVoice(unit, 49);     //TODO: Frog's voice
            MoveObject(unit, GetObjectX(target), GetObjectY(target));
        }
        Delete(target);
    }
}

int SummonFrog(int wp)
{
    int unit = CreateObject("GreenFrog", wp);

    Enchant(unit, "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    UnitLinkBinScript(unit, GreenFrogBinTable());
    SetUnitFlags (unit, GetUnitFlags(unit) ^ UNIT_FLAG_SHORT);
    SetUnitStatus(unit, GetUnitStatus(unit)^MON_STATUS_ALWAYS_RUN);
    UnitZeroFleeRange(unit);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    AggressionLevel(unit, 1.0);
    //SetUnitScanRange(unit, 500.0);
    SetUnitMaxHealth(unit, 295);
    SetCallback(unit, 3, FrogDetectHandler);
    SetCallback(unit, 7, FrogHurtHandler);
    SetCallback(unit, 5, FrogDeathHandler);
    SetCallback(unit, 10, FrogHearEnemy);
    SetCallback(unit, 13, LostEnemyOnSight);
    //SetOwner(unit, CreateObject("InvisibleLightBlueLow", wp));
    //FrameTimerWithArg(3, unit + 2, MakeBeFrog);

    return unit;
}

void FrogHearEnemy()
{
    if (IsVisibleTo(SELF, OTHER) || IsVisibleTo(OTHER, SELF))
    {
        if (CurrentHealth(OTHER))
        {
            LookAtObject(SELF, OTHER);
            if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_ETHEREAL)))
            {
                Enchant(SELF, EnchantList(ENCHANT_ETHEREAL), 6.0);
                Attack(SELF, OTHER);
            }
        }
        UniChatMessage(SELF, "개굴~", 150);
    }
}

void FrogDeathHandler()
{
    int kill = GetKillCredit();

    Delete(GetTrigger() + 1);
    GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
    if (!Random(0, 4))
        FieldRewardPic(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF)));
    PlayerKillHandler(kill);
    Delete(SELF);
}

void PlayerKillHandler(int unit)
{
    if (!IsPlayerUnit(unit))
        unit = GetOwner(unit);
    if (CurrentHealth(unit) && HasClass(unit, "PLAYER"))
    {
        int plr = CheckPlayerIndex(unit);
        if (plr + 1)
        {
            ChangeGold(m_player[plr], Random(8, 16));
            AddPlayerKillScore(plr, Random(1, 2));
        }
    }
}

void AddPlayerKillScore(int plr, int amount)
{
    m_Kills[plr] += amount;
    if (GetPlayerLevel(plr) < MaxLv)
    {
        if (m_Kills[plr] >= PlayerExperienceTable(GetPlayerLevel(plr)))
            PlayerLevelUp(plr);
    }
}

static void revealYourLevel(int plr, int pUnit)
{
    char buff[128];
    int lv=GetPlayerLevel(plr);

    NoxSprintfString(&buff, "___LEVEL UP_!! 레밸 업! 레밸 %d 이 되셨습니다___", &lv, 1);
    UniPrint(pUnit, ReadStringAddressEx(&buff));
    string userName = PlayerIngameNick(pUnit);
    int args[]={StringUtilGetScriptStringPtr(userName), lv, 90 + (lv*10)};
    ChangeGold(pUnit, args[2]);
    NoxSprintfString(&buff, "%s 님께서 레밸 %d 에 달성하셨습니다. 금화 %d이 지급되었습니다", &args, sizeof(args));
    UniPrintToAll(ReadStringAddressEx(&buff));
}

void PlayerLevelUp(int plr)
{
    SetPlayerLevel(plr, GetPlayerLevel(plr) + 1);
    if (GetPlayerHealRate(plr) > 10)
        SetPlayerHealRate(plr, GetPlayerHealRate(plr) - 1);
    TeleportLocation(1, GetObjectX(m_player[plr]), GetObjectY(m_player[plr]));
    AudioEvent("LevelUp", 1);
    DisplayLevelUpText(LocationX(1) - 30.0, LocationY(1) - 10.0);
    DeleteObjectTimer(CreateObjectAt("LevelUp", GetObjectX(m_player[plr]), GetObjectY(m_player[plr])), 30);
    Effect("WHITE_FLASH", GetObjectX(m_player[plr]), GetObjectY(m_player[plr]), 0.0, 0.0);
    RestoreHealth(m_player[plr], MaxHealth(m_player[plr]));
    revealYourLevel(plr, m_player[plr]);
}

void FrogHurtHandler()
{
    int plr = CheckPlayerIndex(GetCaller());

    if (plr + 1)
    {
        Damage(SELF, m_player[plr], GetPlayerLevel(plr) * 2, 14);
    }
}

void FrogDetectHandler()
{
    int attHand[]={FrogAttackTrigger1, FrogAttackTrigger2};

    CallFunctionWithArg(attHand[ m_FrogAtkFunc ], 0);  //here
}

void FrogAttackTrigger1(int arg)
{
    int unit, ptr = GetTrigger() + 1;
    int enemy = ToInt(GetObjectZ(ptr));

    if (!HasEnchant(SELF, "ENCHANT_VILLAIN") && IsObjectOn(OTHER))
    {
        Enchant(SELF, "ENCHANT_VILLAIN", 1.2);
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        SetOwner(SELF, unit);
        Raise(unit, ToFloat(GetCaller()));
        FrameTimerWithArg(35, unit, FrogForceStrike);
    }
    CheckResetSight(GetTrigger(), 21);
    if (enemy ^ GetCaller())
    {
        CreatureFollow(SELF, OTHER);
        Raise(ptr, ToFloat(GetCaller()));
    }
    AggressionLevel(SELF, 1.0);
}

void FrogAttackTrigger2(int arg)
{
    int unit, ptr = GetTrigger() + 1;
    int enemy = ToInt(GetObjectZ(ptr));

    if (!HasEnchant(SELF, "ENCHANT_VILLAIN") && IsObjectOn(OTHER))
    {
        Enchant(SELF, "ENCHANT_VILLAIN", 1.2);
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        SetOwner(SELF, unit);
        Raise(unit, ToFloat(GetCaller()));
        FrameTimerWithArg(35, unit, PowerFrogForceStrike);
    }
    CheckResetSight(GetTrigger(), 21);
    if (enemy ^ GetCaller())
    {
        CreatureFollow(SELF, OTHER);
        Raise(ptr, ToFloat(GetCaller()));
    }
    AggressionLevel(SELF, 1.0);
}

void LostEnemyOnSight()
{
    int enemy = ToInt(GetObjectZ(GetTrigger() + 1));

    if (CurrentHealth(enemy))
    {
        if (HasEnchant(enemy, "ENCHANT_LIGHT"))
            ClearUnitTarget(GetTrigger());
        else
            Effect("THIN_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    }
    else
        ClearUnitTarget(GetTrigger());
}

void ClearUnitTarget(int unit)
{
    CreatureIdle(unit);
    Raise(unit + 1, ToFloat(0));
}

void FrogForceStrike(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), unit;
    
    if (CurrentHealth(owner))
    {
        if (CurrentHealth(target))
        {
            if (Distance(GetObjectX(owner), GetObjectY(owner), GetObjectX(target), GetObjectY(target)) < 150.0)
            {
                LinearOrbMove(CreateObjectAt("CharmOrb", GetObjectX(owner), GetObjectY(owner)), UnitRatioX(owner, target, -1.0), UnitRatioY(owner, target, -1.0), 3.0, 3);
                unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target) - UnitRatioX(owner, target, 9.0), GetObjectY(target) - UnitRatioY(owner, target, 9.0));
                if (IsVisibleTo(owner, unit))
                    MoveObject(owner, GetObjectX(unit), GetObjectY(unit));
                else
                    MoveObject(owner, GetObjectX(target) - UnitRatioX(owner, target, 2.0), GetObjectY(target) - UnitRatioY(owner, target, 2.0));
                Delete(unit);
                TeleportLocation(1, GetObjectX(owner), GetObjectY(owner));
                DeleteObjectTimer(CreateObject("MagicEnergy", 1), 9);
                Damage(target, owner, 11, -1);
                AudioEvent("PushCast", 1);
            }
        }
    }
    Delete(ptr);
}

void PowerFrogForceStrike(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), unit;
    
    if (CurrentHealth(owner))
    {
        if (CurrentHealth(target))
        {
            if (Distance(GetObjectX(owner), GetObjectY(owner), GetObjectX(target), GetObjectY(target)) < 150.0)
            {
                LinearOrbMove(CreateObjectAt("CharmOrb", GetObjectX(owner), GetObjectY(owner)), UnitRatioX(owner, target, -1.0), UnitRatioY(owner, target, -1.0), 3.0, 3);
                unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target) - UnitRatioX(owner, target, 9.0), GetObjectY(target) - UnitRatioY(owner, target, 9.0));
                if (IsVisibleTo(owner, unit))
                    MoveObject(owner, GetObjectX(unit), GetObjectY(unit));
                else
                    MoveObject(owner, GetObjectX(target) - UnitRatioX(owner, target, 2.0), GetObjectY(target) - UnitRatioY(owner, target, 2.0));
                Delete(unit);
                TeleportLocation(1, GetObjectX(owner), GetObjectY(owner));
                DeleteObjectTimer(CreateObject("MagicEnergy", 1), 9);
                Damage(target, owner, 18, -1);
                AudioEvent("PushCast", 1);
            }
        }
    }
    Delete(ptr);
}

void OpenFirstWalls()
{
    int k;

    for (k = 0 ; k < 12 ; k ++)
    {
        WallOpen(Wall(67 + k, 109 - k));
        WallOpen(Wall(79 + k, 99 + k));
        if (k < 3)
        {
            LookAtObject(SummonFrog(12), OTHER);
            LookAtObject(SummonFrog(13), OTHER);
        }
    }
    TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
    StartItemRing("RedPotion", 15, 12);
    StartItemRing("RedPotion", 80, 12);
    StartItemRing("RedPotion", 30, 12);
    AudioEvent("Gear3", 1);
    ObjectOff(SELF);
    UniPrint(OTHER, "벽이 열렸습니다...");
}

static void initShootFunctions()
{
    m_pShootFuncs[0]=ThrowingChakramLv1;
    m_pShootFuncs[1]=ThrowingChakramLv2;
    m_pShootFuncs[2]=ThrowingChakramLv3;
    m_pShootFuncs[3]=ThrowingChakramLv4;
    m_pShootFuncs[4]=ThrowingChakramLv5;
    m_pShootFuncs[5]=ThrowingChakramLv6;
    m_pShootFuncs[6]=ThrowingChakramLv7;
    m_pShootFuncs[7]=ThrowingChakramLv8;
    m_pShootFuncs[8]=ThrowingChakramLv9;
    m_pShootFuncs[9]=ThrowingChakramLv10;
    m_pShootFuncs[10]=ThrowingChakramLv11;
    m_pShootFuncs[11]=ThrowingChakramLv12;
    m_pShootFuncs[12]=ThrowingChakramLv13;
    m_pShootFuncs[13]=ThrowingChakramLv14;
}

void DelayInitRun()
{
    ReventBerserker(0);
    GetItemColors(0);
    PlayerExperienceTable(5);
    FieldReward(0);
    GetOwner(PlrStruct(0));
    FrameTimerWithArg(1, Object("FstUnitPtr"), StartInitSearch);
    FrameTimer(1, InitMapArrowTraps);
    FrameTimer(1, InitBurningRoom);
    FrameTimer(2, BasecampInit);
    FrameTimer(38, WaitForLoading);
    FrameTimer(1, SetHostileCritter);
    ChatMessageEventPushback("//t", PlayerTeleportHandler);
    FirstSaphahKey();
    InitializeReadables();
}

void MapInitialize()
{
    CreateLogFile("test1768-log.txt");
    LastUnit = CreateObject("RedPotion", 1);
    Delete(LastUnit);
    MusicEvent();
    FrameTimer(1, DelayInitRun);
    FrameTimerWithArg(22, Object("OgreElevatorUp"), DelayDisableUnit);
    FrameTimerWithArg(22, Object("CardKeyElev"), DelayDisableUnit);
    FrameTimer(3, LoopRun);
    FrameTimer(2, MakeCoopTeam);
    initShootFunctions();
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    if (CheckGameFlags(GAME_FLAG_ENABLE_CAMPER_ALARM))
        SetGameFlags(GAME_FLAG_ENABLE_CAMPER_ALARM);
}


void WaitForLoading()
{
    UniPrintToAll("맵 내부 데이터를 로드중 입니다, 이 작업은 최대 20초 까지 소요될 수 있으니 잠시만 기다려 주시기 바랍니다");
    AudioEvent("SmallGong", 9);
}

void MapExit()
{
    MusicEvent();
    ResetHostileCritter();
    FixPlayerDialogBug();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void FixPlayerDialogBug()
{
    int k, temp;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (MaxHealth(m_player[k]))
        {
            temp = UnitToPtr(m_player[k]);
            if (GetMemory(GetMemory(temp + 0x2ec)) + 0x11c)
            {
                SetMemory(GetMemory(temp + 0x2ec) + 0x11c, 0);
                SetMemory(GetMemory(GetMemory(temp + 0x2ec) + 0x114) + 0xe60, 0x10);
            }
        }
    }
}

void PlayerEntryPoint()
{
    int plr, k;

    if (CurrentHealth(OTHER))
    {
        plr = CheckPlayer();
        for (k = 9 ; k >= 0 && plr < 0 ; k --)
        {
            if (!MaxHealth(m_player[k]))
            {
                m_player[k] = GetCaller();
                plr = k;
                PlayerIniti(plr);
                break;
            }
        }
        if (plr + 1)    PlayerJoin(plr);
        else            CantJoinThisMap();
    }
}

void FastJoin()
{
    if (CurrentHealth(OTHER))
    {
        int plr = CheckPlayer();

        if (plr < 0)
        {
            MoveObject(OTHER, LocationX(132), LocationY(132));
            return;
        }
        MoveObject(OTHER, LocationX(133), LocationY(133));
    }
}

void commonServerClientProcedure()
{
    CommonInitializeImage();
}

void PlayerIniti(int plr)
{
    int user=m_player[plr];
    m_Kills[plr] = 0;
    if (ValidPlayerCheck(user))
    {
        if (GetHost() ^ user)
            NetworkUtilClientEntry(user);
        else
            commonServerClientProcedure();
        // PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    }
    // if (user ^ GetHost())
    //     NetworkUtilClientEntry(user);
    // else
    //     PlayerClassCommonWhenEntry();
    SetPlayerLevel(plr, 0);
    SetPlayerHealRate(plr, 60);
    if (GetGold(user))
        ChangeGold(user, -GetGold(user));
    EmptyInventory(user);
    DiePlayerHandlerEntry(user);
    SelfDamageClassEntry(user);
    ShowQuestIntroOne(user, 999, "FrogPic", "NPC:FrogBoy");
}

void PlayerJoin(int plr)
{
    Enchant(m_player[plr], "ENCHANT_ANTI_MAGIC", 0.0);
    SetOwner(m_player[plr], ReventBerserker(plr));
    MoveObject(m_player[plr], LocationX(9), LocationY(9));
    Effect("SMOKE_BLAST", LocationX(9), LocationY(9), 0.0, 0.0);
    DeleteObjectTimer(CreateObjectAt("BlueRain", GetObjectX(m_player[plr]), GetObjectY(m_player[plr])), 30);
    AudioEvent("BlindOff", 9);
    UniPrintToAll(PlayerIngameNick(m_player[plr]) + " 님께서 입장하셨습니다");
}

void CantJoinThisMap()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
    MoveObject(OTHER, LocationX(10), LocationY(10));
    UniPrint(OTHER, "맵 입장실패");
}

void PreservePlayersHandler()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (MaxHealth(m_player[k]))
        {
            if (GetUnitFlags(m_player[k]) & 0x40) m_player[k] = 0;
            else if (CurrentHealth(m_player[k]))
            {
                MoveObject(ReventBerserker(k), GetObjectX(m_player[k]), GetObjectY(m_player[k]));
                if (GetPlayerAction(m_player[k]) == 5)
                    PushObjectTo(m_player[k], UnitAngleCos(m_player[k], -1.3), UnitAngleSin(m_player[k], -1.3));
                else if (!(CheckPlayerInput(m_player[k]) ^ 0x07))
                    PlayerJumpEvent(k);
                if (HasEnchant(m_player[k], "ENCHANT_SNEAK"))
                {
                    EnchantOff(m_player[k], "ENCHANT_SNEAK");
                    SetPlayerAction(m_player[k], 1);
                }
                PlayerAutoHeal(k);
            }
        }
        else if (m_player[k])
        {
            m_player[k] = 0;
        }
    }
    FrameTimer(1, PreservePlayersHandler);
}

void CheckMagicalPotion(int unit)
{
    int ptr;

    if (HasEnchant(unit, "ENCHANT_PROTECT_FROM_POISON"))
    {
        EnchantOff(unit, "ENCHANT_PROTECT_FROM_POISON");
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
        Raise(ptr, ToFloat(12));
        SetOwner(unit, ptr);
        FrameTimerWithArg(1, ptr, HealingPotion);
    }
    else if (HasEnchant(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY"))
    {
        EnchantOff(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY");
        Enchant(unit, "ENCHANT_SHOCK", 15.0);
    }
    else if (HasEnchant(unit, "ENCHANT_CONFUSED"))
    {
        EnchantOff(unit, "ENCHANT_CONFUSED");
        PutGasTrap(unit);
    }
}

void AlwaysRunning(int unit)
{
    if (!GetPlayerAction(unit))
        Enchant(unit, "ENCHANT_RUN", 0.0);
    else if (HasEnchant(unit, "ENCHANT_RUN"))
        EnchantOff(unit, "ENCHANT_RUN");
}

void PutGasTrap(int unit)
{
    int trp = CreateObjectAt("PoisonGasTrap", GetObjectX(unit), GetObjectY(unit));

    SetOwner(unit, trp);
    UniPrint(unit, "독가스 함정을 설치했습니다");
}

void PlayerJumpEvent(int plr)
{
    Enchant(m_player[plr], "ENCHANT_SLOWED", 0.7);
}

int ReventBerserker(int idx)
{
    int arr[10], ptr;
    if (!ptr)
    {
        ptr = CreateObject("RedPotion", 1);
        for (0 ; GetDirection(ptr) < 10 ; LookWithAngle(ptr, GetDirection(ptr) + 1))
        {
            arr[GetDirection(ptr)] = CreateObject("Flag", 1);
            SetUnitFlags(arr[GetDirection(ptr)], GetUnitFlags(arr[GetDirection(ptr)]) ^ 0x2000);
            SetFlagColor(arr[GetDirection(ptr)], GetDirection(ptr));
        }
        Delete(ptr);
    }
    return arr[idx];
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(m_player[k]))
            return k;
    }
    return -1;
}



void CheckResetSight(int unit, int delay)
{
    if (!HasEnchant(unit, "ENCHANT_DETECTING"))
    {
        Enchant(unit, "ENCHANT_DETECTING", 0.0);
        FrameTimerWithArg(delay, unit, ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.06);
    AggressionLevel(unit, 1.0);
}

void OnTrackedHarpoon(int cur, int owner)
{
    if (IsPlayerUnit(owner))
    {
        // CastPowerShooter(owner);
        Delete(cur);
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 1177, CheckChakramTracking);
    HashPushback(pInstance, 526, OnTrackedHarpoon);    
}

void CheckChakramTracking(int cur, int owner)
{
    int inv = GetLastItem(cur), ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(cur), GetObjectY(cur));

    Delete(cur);
    SetOwner(owner, ptr);
    Raise(ptr, ToFloat(inv));
    FrameTimerWithArg(2, ptr, DelayGiveUnit);
}

void invokeShootFunction(int functionId, int cur, int owner)
{
    Bind(functionId, &functionId + 4);
}

void DelayGiveUnit(int ptr)
{
    int inv = ToInt(GetObjectZ(ptr)), owner = GetOwner(ptr);

    if (CurrentHealth(owner) && IsObjectOn(inv))
    {
        Pickup(owner, inv);
        invokeShootFunction(m_pShootFuncs[ GetDirection(inv) ], ptr, owner);
    }
    Delete(ptr);
}

void ThrowingChakramLv1(int cur, int owner)
{
    int mis = CreateObjectAt("ThrowingStone", GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));
    UserDamageArrowCreate(owner, GetObjectX(mis), GetObjectY(mis), 20);
    Enchant(mis, "ENCHANT_SLOWED", 0.0);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur, owner, 23.0), UnitRatioY(cur, owner, 23.0));
    PushObjectTo(mis + 1, UnitRatioX(cur, owner, 23.0), UnitRatioY(cur, owner, 23.0));
}

void ThrowingChakramLv2(int cur, int owner)
{
    int mis = CreateObjectAt("ImpShot", GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));
    UserDamageArrowCreate(owner, GetObjectX(mis), GetObjectY(mis), 40);
    Enchant(mis, "ENCHANT_SLOWED", 0.0);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
    PushObjectTo(mis + 1, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
}

void ThrowingChakramLv3(int cur, int owner)
{
    int mis = CreateObjectAt("PitifulFireball", GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));
    UserDamageArrowCreate(owner, GetObjectX(mis), GetObjectY(mis), 52);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur, owner, 27.0), UnitRatioY(cur, owner, 27.0));
    PushObjectTo(mis + 1, UnitRatioX(cur, owner, 27.0), UnitRatioY(cur, owner, 27.0));
}

void ThrowingChakramLv4(int cur, int owner)
{
    int mis = CreateObjectAt("OgreShuriken", GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));

    UserDamageArrowCreate(owner, GetObjectX(mis), GetObjectY(mis), 50);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur, owner, 30.0), UnitRatioY(cur, owner, 30.0));
    PushObjectTo(mis + 1, UnitRatioX(cur, owner, 30.0), UnitRatioY(cur, owner, 30.0));
}

void ThrowingChakramLv5(int cur, int owner)
{
    int mis = CreateObjectAt("WeakArcherArrow", GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));

    UserDamageArrowCreate(owner, GetObjectX(mis), GetObjectY(mis), 60);
    Enchant(mis, "ENCHANT_HASTED", 0.0);
    SetOwner(owner, mis);
    LookAtObject(mis, owner);
    LookWithAngle(mis, GetDirection(mis) + 128);
    PushObjectTo(mis, UnitRatioX(cur, owner, 33.0), UnitRatioY(cur, owner, 33.0));
    PushObjectTo(mis + 1, UnitRatioX(cur, owner, 33.0), UnitRatioY(cur, owner, 33.0));
}

void onMagicMissileCollide()
{
    int owner = GetOwner(SELF);

    while (IsObjectOn(SELF))
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 72, DAMAGE_TYPE_FLAME);
            Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void ThrowingChakramLv6(int cur, int owner)
{
    int mis = CreateObjectById(OBJ_MAGIC_MISSILE, GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0)); //72
    int ptr = GetMemory(0x750710);

    SetOwner(owner, mis);
    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, onMagicMissileCollide);
    // LookAtObject(mis, owner);
    // LookWithAngle(mis, GetDirection(mis) + 128);
    PushObjectTo(mis, UnitRatioX(cur, owner, 34.0), UnitRatioY(cur, owner, 34.0));
}

void ThrowingChakramLv7(int cur, int owner)
{
    int unit = DummyUnitSmall(GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));
    
    LookWithAngle(unit, 30);
    SetCallback(unit, 9, TouchShockWave);
    SetUnitFlags(unit, GetUnitFlags(unit) ^ UNIT_FLAG_NO_PUSH_CHARACTERS);
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), UnitRatioX(cur, owner, 17.0));
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), UnitRatioY(cur, owner, 17.0));
    SetOwner(owner, unit + 1);
    PushTimerQueue(1, unit, ShockWavePar);
}

void onLightningBoltCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, SELF, 75, DAMAGE_TYPE_PLASMA);
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
            Delete(SELF);
        }
    }
}

void ThrowingChakramLv8(int cur, int owner)
{
    int mis = CreateObjectAt("LightningBolt", GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));

    SetOwner(owner, mis);
    Enchant(mis, "ENCHANT_FREEZE", 0.0);
    LookAtObject(mis, owner);
    LookWithAngle(mis, GetDirection(mis) + 128);
    PushObjectTo(mis, UnitRatioX(cur, owner, 42.0), UnitRatioY(cur, owner, 42.0));
    SetUnitCallbackOnCollide(mis, onLightningBoltCollide);
}

void ThrowingChakramLv9(int cur, int owner)
{
    int mis = UserDamageArrowCreate(owner, GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0), 80);

    SetOwner(owner, CreateObjectAt("SpiderSpit", GetObjectX(mis), GetObjectY(mis)));
    Enchant(mis + 1, "ENCHANT_SLOWED", 0.0);
    LookAtObject(mis + 1, owner);
    LookWithAngle(mis + 1, GetDirection(mis + 1) + 128);
    PushObjectTo(mis, UnitRatioX(cur, owner, 37.0), UnitRatioY(cur, owner, 37.0));
    PushObjectTo(mis + 1, UnitRatioX(cur, owner, 37.0), UnitRatioY(cur, owner, 37.0));
}

void ThrowingChakramLv10(int cur, int owner)
{
    int unit = DummyUnitSmall(GetObjectX(owner) + UnitRatioX(cur, owner, 9.0), GetObjectY(owner) + UnitRatioY(cur, owner, 9.0));

    LookWithAngle(unit, 30);
    SetCallback(unit, 9, RifleTouch);
    SetUnitFlags(unit, GetUnitFlags(unit) ^ UNIT_FLAG_NO_PUSH_CHARACTERS);
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), UnitRatioX(cur, owner, 17.0));
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), UnitRatioY(cur, owner, 17.0));
    SetOwner(owner, unit + 1);
    PushTimerQueue(1, unit, LaiserRifle);
}

void onHorrendousHarpoonCollide()
{
    int owner = GetOwner(SELF);

    while (IsObjectOn(SELF))
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 103, DAMAGE_TYPE_FLAME);
            Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void ThrowingChakramLv11(int cur, int owner)
{
    int mis = CreateObjectAt("HarpoonBolt", GetObjectX(owner) + UnitRatioX(cur, owner, 13.0), GetObjectY(owner) + UnitRatioY(cur, owner, 13.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, onHorrendousHarpoonCollide);
    LookAtObject(mis, owner);
    LookWithAngle(mis,GetDirection(mis)+128);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur,owner, 42.0),UnitRatioY(cur,owner,42.0));
}

void onFonTinyCollide()
{
    int owner = GetOwner(SELF);

    while (IsObjectOn(SELF))
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 120, DAMAGE_TYPE_FLAME);
            GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void onFonLargeCollide()
{
    int owner = GetOwner(SELF);

    while (IsObjectOn(SELF))
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 160, DAMAGE_TYPE_FLAME);
            GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void ThrowingChakramLv12(int cur, int owner)
{
    int mis=CreateObjectById(OBJ_DEATH_BALL_FRAGMENT, GetObjectX(owner) + UnitRatioX(cur, owner, 13.0), GetObjectY(owner) + UnitRatioY(cur, owner, 13.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, onFonTinyCollide);
    LookAtObject(mis, owner);
    LookWithAngle(mis,GetDirection(mis)+128);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur,owner, 42.0),UnitRatioY(cur,owner,42.0));
}

void ThrowingChakramLv13(int cur, int owner)
{
    int mis=CreateObjectById(OBJ_ARCHER_ARROW, GetObjectX(owner) + UnitRatioX(cur, owner, 13.0), GetObjectY(owner) + UnitRatioY(cur, owner, 13.0));

    LookAtObject(mis, owner);
    LookWithAngle(mis,GetDirection(mis)+128);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur,owner, 46.0),UnitRatioY(cur,owner,46.0));
}

void ThrowingChakramLv14(int cur, int owner)
{
    int mis=CreateObjectById(OBJ_DEATH_BALL, GetObjectX(owner) + UnitRatioX(cur, owner, 13.0), GetObjectY(owner) + UnitRatioY(cur, owner, 13.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, onFonLargeCollide);
    LookAtObject(mis, owner);
    LookWithAngle(mis,GetDirection(mis)+128);
    SetOwner(owner, mis);
    PushObjectTo(mis, UnitRatioX(cur,owner, 44.0),UnitRatioY(cur,owner,44.0));
}

void ShockWavePar(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr + 1);

    if (CurrentHealth(owner) && count)
    {
        if (IsVisibleTo(ptr + 1, ptr))
        {
            MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr + 1), GetObjectY(ptr) + GetObjectZ(ptr + 2));
            DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(ptr), GetObjectY(ptr)), 9);
            LookWithAngle(ptr, count - 1);
        }
        else
            LookWithAngle(ptr, 0);
        PushTimerQueue(1, ptr, ShockWavePar);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void TouchShockWave()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER) && CurrentHealth(owner) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 64, 14);
        Effect("BLUE_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Delete(SELF);
    }
}

void LaiserRifle(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr + 1);

    if (CurrentHealth(owner) && count)
    {
        if (IsVisibleTo(ptr + 1, ptr))
        {
            Effect("SENTRY_RAY", GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr) - (GetObjectZ(ptr + 1) * 2.0), GetObjectY(ptr) - (GetObjectZ(ptr + 2) * 2.0));
            MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr + 1), GetObjectY(ptr) + GetObjectZ(ptr + 2));
            LookWithAngle(ptr, count - 1);
        }
        else
            LookWithAngle(ptr, 0);
        PushTimerQueue(1, ptr, LaiserRifle);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void RifleTouch()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER) && CurrentHealth(owner) && IsAttackedBy(OTHER, owner))
    {
        TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("SentryRayHit", 1);
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, owner, 92, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.8);
    }
}

void StartItemRing(string name, int wp, int count)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", wp), str = SToInt(name);

    Raise(CreateObject("InvisibleLightBlueLow", wp), ToFloat(str));
    Raise(ptr, ToFloat(count));
    FrameTimerWithArg(1, ptr, MakeItemRingAtFrame);
}

void MakeItemRingAtFrame(int ptr)
{
    int count = GetDirection(ptr), max = ToInt(GetObjectZ(ptr)), str = ToInt(GetObjectZ(ptr + 1));
    int angle = (360 / max) * count;

    if (count < max)
    {
        if (IsObjectOn(ptr))
        {
            CreateObjectAt(ToStr(str), GetObjectX(ptr) + MathSine(angle + 90, 90.0), GetObjectY(ptr) + MathSine(angle, 90.0));
            LookWithAngle(ptr, count + 1);
        }
        FrameTimerWithArg(1, ptr, MakeItemRingAtFrame);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void SetUnitPickEvent(int unit, int func)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(ptr + 0x300, func);
    }
}

void EmptyInventory(int unit)
{
    while (IsObjectOn(GetLastItem(unit))) Delete(GetLastItem(unit));
}

int UserDamageArrowCreate(int owner, float x, float y, int dam)
{
    int unit = CreateObjectAt("MercArcherArrow", x, y);
    int ptr = GetMemory(0x750710);

    SetOwner(owner, unit);
    SetMemory(ptr + 0x14, 0x32);
    SetMemory(GetMemory(ptr + 0x2bc) + 4, dam);
    Enchant(unit, "ENCHANT_INVISIBLE", 0.0);
    return unit;
}

void StartInitSearch(int cur)
{
    int i, count;

    if (cur < LastUnit)
    {
        for (i = 0 ; i < 30 ; i ++)
        {
            if (GetUnitThingID(cur + (i*2)) == 2675)
            {
                FrogSpawn(cur + (i*2));
                count ++;
            }
            else if (GetUnitThingID(cur + (i*2)) == 2672)
            {
                FieldRewardPic(cur + (i*2));
                count ++;
            }
        }
        FrameTimerWithArg(1, cur + 60, StartInitSearch);
    }
    else
        FinishLoadTheMap(count);
}

void FinishLoadTheMap(int arg)
{
    int k;

    for (k = 0 ; k < 5 ; k ++) WallOpen(Wall(72 + k, 138 + k));
    StartItemRing("RoundChakram", 14, 36);
    FrameTimer(1, PreservePlayersHandler);
    SecondTimer(1, StartBgmLoop);
    GetOwner(MasterUnit());
    InitiMapTrader();
    CampArea = CreateObjectAt("InvisibleLightBlueHigh", LocationX(14), LocationY(14));
    StartItemRing("RedPotion", 53, 16);
    BossRoomEntranceWall(0);
    UniPrintToAll("맵 로딩이 완료되었습니다, 게임을 시작합니다");
}

void FrogSpawn(int cur)
{
    TeleportLocation(1, GetObjectX(cur), GetObjectY(cur));
    SummonFrog(1);
    Delete(cur);
}

int GetKillCredit()
{
    int ptr = GetMemory(0x979724), ptr2;

    if (ptr)
    {
        ptr2 = GetMemory(ptr + 0x208);
        if (ptr2)
        {
            return GetMemory(ptr2 + 0x2c);
        }
    }
    return 0;
}

void DelayDisableUnit(int unit)
{
    ObjectOff(unit);
}

void RoundingTrap(int wp)
{
    int unit = CreateObject("Maiden", wp);
    Frozen(CreateObject("LargeFist", wp), 1);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    SetCallback(unit, 9, TouchedTrap);
    FrameTimerWithArg(1, unit, MovingCallback);
}

void TouchedTrap()
{
    if (CurrentHealth(OTHER))
    {
        Damage(OTHER, 0, 3, 14);
    }
}

void MovingCallback(int unit)
{
    MoveObject(unit, GetObjectX(unit) + UnitAngleCos(unit, 6.0), GetObjectY(unit) + UnitAngleSin(unit, 6.0));
    MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
    LookWithAngle(unit, GetDirection(unit) + 6);
    FrameTimerWithArg(1, unit, MovingCallback);
}

void DelayEnableUnit(int unit)
{
    ObjectOn(unit);
}

void GreenSparkFx(float x, float y)
{
    int unit = CreateObjectAt("MonsterGenerator", x, y);
    Damage(unit, 0, 1, -1);
    Delete(unit);
}

int CheckPlayerIndex(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (m_player[i] ^ unit) continue;
        else return i;
    }
    return -1;
}

void HealingPotion(int ptr)
{
    int time = GetDirection(ptr), count = ToInt(GetObjectZ(ptr)), owner = GetOwner(ptr);

    if (!IsObjectOn(ptr)) return;
    if (CurrentHealth(owner) && count)
    {
        if (!time)
        {
            Effect("GREATER_HEAL", GetObjectX(owner), GetObjectY(owner), GetObjectX(owner), GetObjectY(owner) - 100.0);
            RestoreHealth(owner, 1);
            Raise(ptr, ToFloat(count - 1));
        }
        LookWithAngle(ptr, (time + 1) % 4);
        FrameTimerWithArg(1, ptr, HealingPotion);
    }
    else
        Delete(ptr);
}

void DisplayLevelUpText(float x, float y)
{
    TeleportLocation(1, x, y);
    int ptr = CreateObject("InvisibleLightBlueHigh", 1);
    StrLevelUp();
    int end = CreateObject("InvisibleLightBlueHigh", 1);
    Raise(ptr, ToFloat(end - ptr + 1));
    FrameTimerWithArg(41, ptr, RemoveDrawedText);
}

void RemoveDrawedText(int ptr)
{
    int max = ToInt(GetObjectZ(ptr)), i;
    
    for (i = 0 ; i < max ; i ++)
        Delete(ptr + i);
}

void StrLevelUp()
{
	int arr[18], i = 0;
	string name = "ManaBombOrb";
	
	arr[0] = 268435458; arr[1] = 32801; arr[2] = 537413632; arr[3] = 301989888; arr[4] = 1148194820; arr[5] = 75041052; arr[6] = 1112056388; arr[7] = 680591940; arr[8] = 1083253028; arr[9] = 1212805775; 
	arr[10] = 77602888; arr[11] = 135406628; arr[12] = 151593505; arr[13] = 1191721465; arr[14] = 15480; arr[15] = 16777216; arr[16] = 0; arr[17] = 128; 
	while(i < 18)
	{
		drawStrLevelUp(arr[i], name);
		i ++;
	}
}

void drawStrLevelUp(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = LocationX(1);
		pos_y = LocationY(1);
	}
	for (i = 1 ; i > 0 && count < 558 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 1);
		if (count % 45 == 44)
            TeleportLocationVector(1, -88.0, 2.0);
		else
            TeleportLocationVector(1, 2.0, 0.0);
		count ++;
	}
	if (count >= 558)
	{
		count = 0;
		TeleportLocation(1, pos_x, pos_y);
	}
}

void FieldRewardPic(int cur)
{
    TeleportLocation(1, GetObjectX(cur), GetObjectY(cur));
    Delete(cur);

    int unit = CallFunctionWithArgInt(FieldReward(Random(0, 11)), 1);

    if (unit)
    {
        CheckPotionThingID(unit);
    }
}

int FieldReward(int idx)
{
    int *ret;

    if (!ret)
    {
        int arr[]={
            CheatGold, CheatGold, CheatGold, DefaultPotion, DefaultPotion,
            DefaultPotion, PotionPic, ExpBook, DefaultPotion, SpecialItemSpawn,
            SpecialItemSpawn, DefaultPotion
        };
        ret=arr;
    }
    return ret[idx];
}

int CheatGold(int wp)
{
    int unit = CreateObject("QuestGoldChest", wp);

    UnitNoCollide(unit);
    UnitStructSetGoldAmount(unit, Random(100, 200));
    return unit;
}

int PotionPic(int wp)
{
    string name[] = {"RedPotion", "BlackPotion", "YellowPotion", "PoisonProtectPotion", "VampirismPotion", "ShieldPotion", "ShockProtectPotion", "RoundChakram",
        "Cider", "CurePoisonPotion"};
    int unit = CreateObject(name[ Random(0, 9)], wp);

    return unit;
}

int ExpBook(int wp)
{
    int unit = CreateObject("AbilityBook", wp);

    SetUnitPickEvent(unit, ExpBookPic);
    return unit;
}

int DefaultPotion(int wp)
{
    return CreateObject("RedPotion", wp);
}

int SpecialItemSpawn(int wp)
{
    int sp[]={ShockItemCreate, HealingItemCreate, TrapItemCreate };
    CallFunctionWithArgInt(sp[ Random(0, 2) ], wp);
}

void ExpBookPic()
{
    int rnd = Random(3, 10), plr = CheckPlayerIndex(GetCaller());

    if (plr + 1)
    {
        TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("AwardSpell", 1);
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);

        char buff[128];

        NoxSprintfString(&buff, "킬 스코어 %d 점이 적립되었습니다", &rnd, 1);
        UniPrint(OTHER, ReadStringAddressEx(&buff));
        AddPlayerKillScore(plr, rnd);
    }
    Delete(SELF);
}

int CheckWeaponLevel(int unit)
{
    int k;

    for (k = 1 ; k <= 10 ; k ++)
    {
        if (HasEnchant(unit, ChakramPowerLevel(k)))
            return k;
    }
    return 0;
}

void InitiMapTrader()
{
    LookWithAngle(MakeTraderChakramPowerUp(1408.0, 2820.0), 32);
    LookWithAngle(MakeSCV(1447.0, 2781.0), 32);
    LookWithAngle(GrenadeShop(1414.0, 2994.0), 225);
    LookWithAngle(ArrowShop(LocationX(124), LocationY(124)), 225);
    MakeTraderPixieMaster(LocationX(131), LocationY(131));
    CreateObject("RedPotion", 11);
    CreateObject("RedPotion", 11);
    CreateObject("RedPotion", 11);
    CreateObject("RedPotion", 11);
    CreateObject("RedPotion", 11);
}

int ChakRealLevel(int unit)
{
    int i, res = 0, ptr = UnitToPtr(unit);

    if (ptr)
    {
        ptr = GetMemory(GetMemory(ptr + 0x2b4));
        for (i = 1 ; i <= 10 ; i ++)
        {
            if (ptr == GetItemColors(i))
                return i;
        }
    }
    return 0;
}

void PlayerTeleportHandler(int unit)
{
    int arr[32], pIndex = GetPlayerIndex(unit);

    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit, "ENCHANT_LIGHT"))
        {
            UniPrint(unit, "공간이동 명령어 쿨다운 입니다... 몇초 후 다시 시도해보세요");
            return;
        }
        if (IsVisibleTo(CampArea, unit))
        {
            if (IsObjectOn(arr[pIndex]))
            {
                PlayerTeleportAt(unit, GetObjectX(arr[pIndex]), GetObjectY(arr[pIndex]));
                UniPrint(unit, "저장된 위치로 이동되었습니다");
            }
            else
                UniPrint(unit, "아직 저장된 위치가 없습니다");
        }
        else
        {
            if (IsObjectOn(arr[pIndex]))
            {
                MoveObject(arr[pIndex], GetObjectX(unit), GetObjectY(unit));
            }
            else
            {
                arr[pIndex] = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
                Enchant(arr[pIndex], "ENCHANT_ANCHORED", 0.0);
            }
            PlayerTeleportAt(unit, GetObjectX(CampArea), GetObjectY(CampArea));
            UniPrint(unit, "이동 전 위치를 저장 후 시작지점으로 이동되었습니다");
            UniPrint(unit, "시작지점 주변에서 이탈하여 공간이동 명령어를 사용할 경우 위치가 다시 저장되므로, 원래위치로 가려면 반드시 시작지점 주변에서 사용하세요");
        }
    }
}

void PlayerTeleportAt(int unit, float destX, float destY)
{
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    MoveObject(unit, destX, destY);
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    DeleteObjectTimer(CreateObjectAt("BlueRain", destX, destY), 25);
    Enchant(unit, "ENCHANT_LIGHT", 5.0);
}

int PlacePullLocation(float x, float y, float x2, float y2)
{
    int unit = CreateObjectAt("CarnivorousPlant", x, y);
    SetUnitMaxHealth(CreateObjectAt("WeirdlingBeast", x2, y2), 10);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Damage(unit + 1, 0, MaxHealth(unit + 1) + 1, -1);
    SetCallback(unit, 9, PullAtLocation);
    return unit;
}

void PullAtLocation()
{
    if (CurrentHealth(OTHER) && HasClass(OTHER, "MONSTER"))
    {
        if (HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC"))
        {
            TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
            AudioEvent("BallBounce", 1);
            DeleteObjectTimer(CreateObject("MagicEnergy", 1), 9);
            Effect("LIGHTNING", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(GetTrigger() + 1), GetObjectY(GetTrigger() + 1));
            MoveObject(OTHER, GetObjectX(GetTrigger() + 1), GetObjectY(GetTrigger() + 1));
        }
    }
}

int BossMonster(string name, float x, float y) //Way: 111
{
    int unit = CreateObjectAt(name, x, y);
    CreateObjectAt("InvisibleLightBlueHigh", x, y);

    SetUnitMaxHealth(unit, 900);
    SetUnitScanRange(unit, 450.0);
    SetCallback(unit, 3, BossDetectHandler);
    SetCallback(unit, 5, BossUnitDeathEvent);
    SetCallback(unit, 13, LostEnemyOnSight);

    return unit;
}

void BossUnitDeathEvent()
{
    int kill = GetKillCredit();

    Effect("WHITE_FLASH", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(GetTrigger() + 1);
    UniPrintToAll("방금 보스가 격추되었습니다");

    Delete(GetTrigger() + 1);
    GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
    PlayerKillToBossHandler(kill);
    FrameTimerWithArg(60, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF)), VictoryEvent);
}

void PlayerKillToBossHandler(int unit)
{
    int plr;

    if (!IsPlayerUnit(unit))
        unit = GetOwner(unit);
    if (CurrentHealth(unit) && HasClass(unit, "PLAYER"))
    {
        plr = CheckPlayerIndex(unit);
        if (plr + 1)
        {
            UniPrintToAll(PlayerIngameNick(unit) + " 님께서 보스를 처치하셨습니다");
            Enchant(unit, "ENCHANT_CROWN", 0.0);
            AddPlayerKillScore(plr, 100);
        }
    }
}

void VictoryEvent(int ptr)
{
    TeleportLocation(1, GetObjectX(ptr), GetObjectY(ptr));
    StrSuccessMission();
    DeleteObjectTimer(CreateObject("LevelUp", 1), 600);
    AudioEvent("StaffOblivionAchieve1", 1);
    AudioEvent("AmuletDrop", 1);
    Delete(ptr);
}

void BossDetectHandler()
{
    int ptr = GetTrigger() + 1;
    int enemy = ToInt(GetObjectZ(ptr));

    MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
    ShotSingleMagicMissile(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        BossCastRandomSkill(GetTrigger(), GetCaller());
    }
    LookAtObject(SELF, OTHER);
    if (enemy ^ GetCaller())
    {
        CreatureFollow(SELF, OTHER);
        Raise(ptr, ToFloat(GetCaller()));
    }
    CheckResetSight(GetTrigger(), 20);
}

void ShotSingleMagicMissile(int unit, int target)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));

    CastSpellObjectObject("SPELL_MAGIC_MISSILE", unit, target);
    Delete(ptr);
    Delete(ptr + 2);
    Delete(ptr + 3);
    Delete(ptr + 4);
}

void BossCastRandomSkill(int unit, int target)
{
    int ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit));

    Enchant(unit, "ENCHANT_BURNING", 7.0);
    SetOwner(unit, ptr);
    Raise(ptr, ToFloat(target));
    int exec[]={CastEnergyPar, ThrowMeteor, SummonThreeHok, ShotTripleMissile, ShotMultiDeathray};
    FrameTimerWithArg(3, ptr, exec[Random(0, 4)]);
}

void CastEnergyPar(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), unit;
    float vectX, vectY;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        vectX = UnitRatioX(target, owner, 21.0);
        vectY = UnitRatioY(target, owner, 21.0);
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner) + vectX, GetObjectY(owner) + vectY);
        SetOwner(owner, unit);
        Raise(unit, vectX);
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit) + (vectX * 4.0), GetObjectY(unit) + (vectY * 4.0)), vectY);
        FrameTimerWithArg(1, unit, EnergyParHandler);
    }
    Delete(ptr);
}

void ThrowMeteor(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), unit;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target), GetObjectY(target));
        CastSpellObjectLocation("SPELL_METEOR", owner, GetObjectX(target), GetObjectY(target));
        SetOwner(owner, unit);
        FrameTimerWithArg(17, unit, HitMeteor);
    }
    Delete(ptr);
}

void SummonThreeHok(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), unit;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        LookAtObject(owner, target);
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));
        SetOwner(owner, unit);
        LookWithAngle(unit, GetDirection(owner) - 21);
        FrameTimerWithArg(1, SpawnBlackHok(GetObjectX(owner) + UnitAngleCos(unit, 23.0), GetObjectY(owner) + UnitAngleSin(unit, 23.0), unit, target), BlackHokMovingHandler);
        LookWithAngle(unit, GetDirection(owner));
        FrameTimerWithArg(1, SpawnBlackHok(GetObjectX(owner) + UnitAngleCos(unit, 23.0), GetObjectY(owner) + UnitAngleSin(unit, 23.0), unit, target), BlackHokMovingHandler);
        LookWithAngle(unit, GetDirection(owner) + 21);
        FrameTimerWithArg(1, SpawnBlackHok(GetObjectX(owner) + UnitAngleCos(unit, 23.0), GetObjectY(owner) + UnitAngleSin(unit, 23.0), unit, target), BlackHokMovingHandler);
        FrameTimerWithArg(1, unit, BlackHokLifeTimeCounter);
    }
    Delete(ptr);
}

void ShotTripleMissile(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), unit;
    float vectX, vectY;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        vectX = UnitRatioX(target, owner, 13.0);
        vectY = UnitRatioY(target, owner, 13.0);
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner) + (vectX * 2.0), GetObjectY(owner) + (vectY * 2.0));
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), vectY);
        Raise(unit, vectX);
        LookAtObject(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), target);
        CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit));
        SetOwner(owner, unit);
        FrameTimerWithArg(1, unit, RainArrowShotHandler);
    }
    Delete(ptr);
}

void ShotMultiDeathray(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), unit;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        TeleportLocation(1, GetObjectX(owner), GetObjectY(owner));
        AudioEvent("SpellPhonemeDownRight", 1);
        unit = CreateObjectAt("WeirdlingBeast", GetObjectX(owner) + UnitRatioX(target, owner, 3.0), GetObjectY(owner) + UnitRatioY(target, owner, 3.0));
        UnitNoCollide(unit);
        SetOwner(owner, unit);
        LookAtObject(unit, target);
        SetUnitScanRange(unit, 600.0);
        SetCallback(unit, 3, DetectingMultiDeathRay);
        DeleteObjectTimer(unit, 1);
    }
    Delete(ptr);
}

void EnergyParHandler(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && count < 40)
    {
        if (IsVisibleTo(ptr, ptr + 1))
        {
            MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr), GetObjectY(ptr) + GetObjectZ(ptr + 1));
            MoveObject(ptr + 1, GetObjectX(ptr + 1) + GetObjectZ(ptr), GetObjectY(ptr + 1) + GetObjectZ(ptr + 1));
            Effect("SENTRY_RAY", GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr) - GetObjectZ(ptr), GetObjectY(ptr) - GetObjectZ(ptr + 1));
            unit = CreateObjectAt("ShopkeeperConjurerRealm", GetObjectX(ptr), GetObjectY(ptr));
            SetOwner(ptr + 1, unit);
            Frozen(unit, 1);
            DeleteObjectTimer(unit, 1);
            SetCallback(unit, 9, CollideEnergyPar);
        }
        else
            count = 100;
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, EnergyParHandler);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void CollideEnergyPar()
{
    int owner = GetOwner(GetOwner(SELF) - 1);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("SentryRayHit", 1);
        Effect("VIOLET_SPARKS", LocationX(1), LocationY(1), 0.0, 0.0);
        Damage(OTHER, owner, 100, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.8);
    }
}

void onHitMeteorSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 105, DAMAGE_TYPE_PLASMA);
        }
    }
}

void HitMeteor(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        float x=GetObjectX(ptr), y=GetObjectY(ptr);
        Effect("SPARK_EXPLOSION", x,y, 0.0, 0.0);
        SplashDamageAt(owner, x,y, 125.0, onHitMeteorSplash);
        DeleteObjectTimer(CreateObjectAt("MeteorExplode", x,y), 9);
        DeleteObjectTimer(CreateObjectAt("Explosion", x,y), 9);
        PlaySoundAround(ptr, SOUND_MeteorHit);
        PlaySoundAround(ptr, SOUND_FireballExplode);
    }
    Delete(ptr + 1);
    Delete(ptr);
}

int SpawnBlackHok(float x, float y, int ptr, int target)
{
    int unit = CreateObjectAt("BlackWolf", x, y);

    Raise(CreateObjectAt("InvisibleLightBlueLow", x, y), ToFloat(target));

    LookAtObject(unit, target);
    SetOwner(ptr, unit);
    SetOwner(ptr, unit + 1);
    AggressionLevel(unit, 0.0);
    ObjectOff(unit);
    return unit;
}

void onBlackHokSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 50, DAMAGE_TYPE_PLASMA);
        }
    }
}

void BlackHokMovingHandler(int arg)
{
    int ptr = GetOwner(arg + 1), target = ToInt(GetObjectZ(arg + 1));
    int owner = GetOwner(ptr);
    float x=GetObjectX(arg), y=GetObjectY(arg);

    if (CurrentHealth(arg) && CurrentHealth(owner) && CurrentHealth(target))
    {
        if (IsVisibleTo(arg, target))
        {
            if (DistanceUnitToUnit(arg,target) > 40.0)
            {
                MoveObjectVector(arg, UnitRatioX(target, arg, 7.0), UnitRatioY(target, arg, 7.0));
                LookAtObject(arg, target);
                Walk(arg, GetObjectX(arg), GetObjectY(arg));
            }
            else
            {
                // TeleportLocation(1, );
                GreenExplosion(x,y);
                Damage(arg, 0, MaxHealth(arg) + 1, 14);
                SplashDamageAt(owner, x,y, 85.0,onBlackHokSplash);
            }
        }
        else
            Damage(arg, 0, MaxHealth(arg) + 1, 14);
        FrameTimerWithArg(1, arg, BlackHokMovingHandler);
    }
    else
    {
        if (MaxHealth(arg))
            Effect("SMOKE_BLAST", x,y, 0.0, 0.0);
        DeleteObjectTimer(arg, 1);
        DeleteObjectTimer(arg + 1, 1);
    }
}

void BlackHokLifeTimeCounter(int ptr)
{
    int count = ToInt(GetObjectZ(ptr)), owner = GetOwner(ptr), i;

    if (CurrentHealth(owner) && count < 85)
    {
        Raise(ptr, ToFloat(count + 1));
        FrameTimerWithArg(1, ptr, BlackHokLifeTimeCounter);
    }
    else
    {
        for (i = 0 ; i < 3 ; i ++)
        {
            if (CurrentHealth(ptr + 1 + (i*2)))
                Damage(ptr + 1 + (i*2), 0, MaxHealth(ptr + 1 + (i*2)) + 1, 14);
        }
        Delete(ptr);
    }
}

void RainArrowShotHandler(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), mis;

    if (CurrentHealth(owner) && count < 16)
    {
        mis = UserDamageArrowCreate(owner, GetObjectX(ptr), GetObjectY(ptr), 35);
        LookWithAngle(mis, GetDirection(ptr + 2));
        LookWithAngle(UserDamageArrowCreate(owner, GetObjectX(ptr + 2), GetObjectY(ptr + 2), 75), GetDirection(ptr + 2));
        Enchant(mis, "ENCHANT_SLOWED", 0.0);
        Enchant(mis + 1, "ENCHANT_SLOWED", 0.0);
        PushObjectTo(mis, GetObjectZ(ptr) * 2.5, GetObjectZ(ptr + 1) * 2.5);
        PushObjectTo(mis + 1, GetObjectZ(ptr) * 2.5, GetObjectZ(ptr + 1) * 2.5);
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr + 1), GetObjectY(ptr) + GetObjectZ(ptr));
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + GetObjectZ(ptr + 1), GetObjectY(ptr + 2) - GetObjectZ(ptr));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, RainArrowShotHandler);
        GreenLightningEffect(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr + 2), GetObjectY(ptr + 2));
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
        Delete(ptr + 3);
    }
}

void GreenLightningEffect(float x1, float y1, float x2, float y2)
{
    GreenLightningFx(FloatToInt(x1), FloatToInt(y1), FloatToInt(x2), FloatToInt(y2), 12);
}

void DetectingMultiDeathRay()
{
    int owner = GetOwner(SELF), ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));

    SetOwner(owner, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER)));

    Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    SetOwner(owner, ptr);
    SetOwner(owner, ptr + 1);
    FrameTimerWithArg(3, ptr, RealDeathrayTrigger);
}

void RealDeathrayTrigger(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        CastSpellObjectLocation("SPELL_DEATH_RAY", ptr, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        CastSpellObjectLocation("SPELL_DEATH_RAY", ptr + 1, GetObjectX(ptr + 1) + 0.1, GetObjectY(ptr + 1) + 0.1);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void BossRoomEntranceWall(int status)
{
    int i;

    for (i = 0 ; i < 4 ; i ++)
    {
        if (status)
        {
            WallOpen(Wall(63 - i, 217 + i));
            if (i < 3)
                WallClose(Wall(66 + i, 216 + i));
        }
        else
        {
            WallClose(Wall(63 - i, 217 + i));
            if (i < 3)
                WallOpen(Wall(66 + i, 216 + i));
        }
    }
}

void StartBossRoom()
{
    if (HasItem(OTHER, BossRoomCardKey()))
    {
        ObjectOff(SELF);
        BossRoomEntranceWall(1);
        ObjectOn(Object("BossRoomTeleport"));
        Frozen(CreateObjectAt("TraderArmorRack1", 208.0, 169.0), 1);
        BossMonster("WizardRed", LocationX(111), LocationY(111));
        SpawnTeleportMark(1599.0, 4749.0, 1234.0, 5374.0);
        RemoveFrogInArea(GetObjectX(SELF), GetObjectY(SELF), OTHER);
        StartItemRing("RedPotion", 112, 20);
        UniPrintToAll("보스가 등장했습니다!!");
    }
    else
    {
        LowerRightWalls();
        UniPrint(OTHER, "이 스위치를 조작하려면 보안 카드키가 필요합니다");
    }
}

int SpawnTeleportMark(float x, float y, float destX, float destY)
{
    int unit = CreateObjectAt("WeirdlingBeast", x, y);
    SetUnitMaxHealth(CreateObjectAt("WeirdlingBeast", destX, destY), 10);

    SetCallback(unit, 9, TakeMeToTargetLocation);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Damage(unit + 1, 0, MaxHealth(unit + 1) + 1, -1);
    return unit;
}

void TakeMeToTargetLocation()
{
    int ptr;

    if (CurrentHealth(OTHER) && HasClass(OTHER, "PLAYER"))
    {
        if (!HasEnchant(OTHER, "ENCHANT_BURNING"))
        {
            UniPrint(OTHER, "지정된 장소로 공간이동을 시작합니다, 취소하려면 움직이세요");
            TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
            ptr = CreateObject("InvisibleLightBlueHigh", 1);
            CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(GetTrigger() + 1), GetObjectY(GetTrigger() + 1));
            CreateObject("VortexSource", 1);
            Enchant(ptr, "ENCHANT_ANCHORED", 0.0);
            Raise(ptr, ToFloat(GetCaller()));
            Enchant(OTHER, "ENCHANT_BURNING", 2.0);
            FrameTimerWithArg(48, ptr, DelayTeleportUnit);
        }
    }
}

void DelayTeleportUnit(int ptr)
{
    int unit = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(unit))
    {
        if (Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(unit), GetObjectY(ptr)) < 50.0)
            PlayerTeleportAt(unit, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
    }
    Delete(ptr);
    Delete(ptr + 1);
    Delete(ptr + 2);
}

void RemoveFrogInArea(float x, float y, int owner)
{
    int i, unit = CreateObjectAt("RedPotion", x, y) + 1;

    for (i = 0 ; i < 8 ; i ++)
    {
        SetOwner(owner, CreateObjectAt("WeirdlingBeast", x, y));
        UnitNoCollide(unit + i);
        SetUnitScanRange(unit + i, 450.0);
        LookWithAngle(unit + i, 32 * i);
        SetCallback(unit + i, 3, RemoveInVisibledSight);
    }
    Delete(unit - 1);
}

void RemoveInVisibledSight()
{
    if (HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC") && HasClass(OTHER, "MONSTER"))
    {
        Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
        Damage(OTHER, GetOwner(SELF), MaxHealth(OTHER) + 1, 14);
    }
}

void DescWarpCmdInfo()
{
    UniPrint(OTHER, "$$프로 개구리 사냥꾼$$ 제작. panic");
    UniPrint(OTHER, "게임 팁- 채팅창에 //t 라고 입력하시면 마을로 되돌아 갈 수 있습니다");
}

void HealingItemUse()
{
    int unit;

    if (!HasEnchant(OTHER, "ENCHANT_DETECTING"))
    {
        Enchant(OTHER, "ENCHANT_DETECTING", 15.0);
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));
        Raise(unit, 14);
        SetOwner(OTHER, unit);
        FrameTimerWithArg(1, unit, HealingPotion);
    }
    Delete(SELF);
}

void ShockItemUse()
{
    Delete(SELF);
    Enchant(OTHER, "ENCHANT_SHOCK", 30.0);
}

void PoisonGasUse()
{
    Delete(SELF);
    PutGasTrap(OTHER);
}

int ShockItemCreate(int location)
{
    int item = CreateObject("Fear", location);
    
    SetUnitCallbackOnUseItem(item, ShockItemUse);
    return item;
}

int HealingItemCreate(int location)
{
    int item = CreateObject("AmuletofManipulation", location);
    
    SetUnitCallbackOnUseItem(item, HealingItemUse);
    return item;
}

int TrapItemCreate(int location)
{
    int item = CreateObject("AmuletofNature", location);
    
    SetUnitCallbackOnUseItem(item, PoisonGasUse);
    return item;
}

static void OnUserInitialized(int user) //virtual
{
    int pIndex = GetPlayerIndex(user);

    if (pIndex<0)
        return;

    char print[64];

    NoxSprintfString(print, "user %d- initialized", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(print), CONSOLE_COLOR_SKYBLUE);
    // if (user ^ GetHost())
    //     NetworkUtilClientEntry(user);
    // else
    //     PlayerClassCommonWhenEntry();
}

#define GROUP_caveFrogWalls 0
void openCaveFrogWalls()
{
    int already;

    ObjectOff(SELF);
    if (!already)
    {
        already=TRUE;
        WallGroupOpen(GROUP_caveFrogWalls);
        Effect("JIGGLE", GetObjectX(SELF), GetObjectY(SELF), 20.0, 0.0);
    }
}

void endThisMap()
{
    // ObjectOff(SELF);
    Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER), 0.0,0.0);
    MoveObject(OTHER, LocationX(143),LocationY(143));
    Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER), 0.0,0.0);
    UniPrint(OTHER, "왕개구리 서식처로 이동했습니다");
}

#define GROUP_caveWalls2 1

void openCaveWalls2()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_caveWalls2);
}

void Surprising2()
{
    ObjectOff(SELF);
    SummonFrogAmount(130, 5);
}

static void NetworkUtilClientMain()
{
    commonServerClientProcedure();
}
