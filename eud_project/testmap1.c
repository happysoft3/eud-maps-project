
#include "r240503_utils.h"
#include "libs/printutil.h"
#include "libs/objectIDdefines.h"
#include "libs/waypoint.h"
#include "libs/unitstruct.h"

void onStoneGiantCollide(){
    if (GetUnitClass(OTHER)&UNIT_CLASS_MISSILE){
        if (GetUnitThingID(OTHER)==OBJ_HARPOON_BOLT){
            int owner=GetOwner(OTHER);
            if (IsPlayerUnit(owner)){
                int ptr=UnitToPtr(owner);
                int ec=GetMemory(ptr+0x2ec);

                UniPrintToAll(IntToString(GetMemory(ec+0x84)));
                UniPrintToAll(IntToString(GetMemory(ec+0x88)));
            }
        }
    }
}

int createObstacleWallReal(float x,float y){
    int wall =CreateObjectById(OBJ_LARGE_BLUE_FLAME,x,y);
    
    // SetUnitCallbackOnCollide(wall,onWallCollide);
    return wall;
}

#define PATROL_BLOCK_UNIT 0
#define PATROL_BLOCK_COUNT 1
#define PATROL_BLOCK_XVECT 2
#define PATROL_BLOCK_YVECT 3
#define PATROL_BLOCK_INIT_VALUE 4
#define PATROL_BLOCK_MAX 5

void onBlockPatrolBackward(int *d){
    int b=d[PATROL_BLOCK_UNIT];
    if (ToInt(GetObjectX(b))){
        if (--d[PATROL_BLOCK_COUNT]>=0){
            PushTimerQueue(1,d,onBlockPatrolBackward);
            float *pv=&d[PATROL_BLOCK_XVECT];
            MoveObjectVector(b,-pv[0],-pv[1]);
            return;
        }
        d[PATROL_BLOCK_COUNT]=d[PATROL_BLOCK_INIT_VALUE];
        PushTimerQueue(1,d,onBlockPatrolForward);
        return;
    }
    FreeSmartMemEx(d);
}

void onBlockPatrolForward(int *d){
    int b=d[PATROL_BLOCK_UNIT];
    if (ToInt(GetObjectX(b))){
        if (--d[PATROL_BLOCK_COUNT]>=0){
            PushTimerQueue(1,d,onBlockPatrolForward);
            float *pv=&d[PATROL_BLOCK_XVECT];
            MoveObjectVector(b,pv[0],pv[1]);
            return;
        }
        d[PATROL_BLOCK_COUNT]=d[PATROL_BLOCK_INIT_VALUE];
        PushTimerQueue(1,d,onBlockPatrolBackward);
        return;
    }
    FreeSmartMemEx(d);
}

void placingPatrolBlock(short loc, short destLoc, float speed){
    int b=createObstacleWallReal(LocationX(loc),LocationY(loc));
    int *d;
    float points[]={LocationX(loc),LocationY(loc),LocationX(destLoc),LocationY(destLoc)},vect[2];
    float dist=Distance(points[0],points[1],points[2],points[3]);
    // float vc=vect[1];

    ComputePointRatio(&points[2],points,vect,speed);
    // if (ToInt(vect[0])) vc=vect[0];
    AllocSmartMemEx(PATROL_BLOCK_MAX*4,&d);
    d[PATROL_BLOCK_UNIT]=b;
    d[PATROL_BLOCK_INIT_VALUE]=FloatToInt( dist/speed ); //FloatToInt(dist/vc)+1;
    d[PATROL_BLOCK_COUNT]=d[PATROL_BLOCK_INIT_VALUE];
    d[PATROL_BLOCK_XVECT]=vect[0];
    d[PATROL_BLOCK_YVECT]=vect[1];
    PushTimerQueue(1,d,onBlockPatrolForward);
}

void MapInitialize(){
    MusicEvent();
    int s=CreateObjectById(OBJ_STONE_GOLEM,LocationX(35),LocationY(35));
    ObjectOff(s);
    Enchant(s,"ENCHANT_INVULNERABLE",0.0);
    SetCallback(s,9,onStoneGiantCollide);
    placingPatrolBlock(27,38,2.0);
}

void onBackstepHammerSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
            Damage(OTHER, SELF, 200, DAMAGE_TYPE_PLASMA);
    }
}

#define BACKSTEP_DISTANCE 100.0
void earthQuakeAxeTriggered(int unit){
    float backVectX = UnitAngleCos(unit, -BACKSTEP_DISTANCE), backVectY = UnitAngleSin(unit, -BACKSTEP_DISTANCE);
    SplashDamageAtEx(unit, GetObjectX(unit), GetObjectY(unit), 180.0, onBackstepHammerSplash);
    PushObjectTo(unit, backVectX, backVectY);
    DeleteObjectTimer(CreateObjectById(OBJ_FORCE_OF_NATURE_CHARGE, GetObjectX(unit), GetObjectY(unit)), 18);
    PlaySoundAround(unit, SOUND_FireballExplode);
    Effect("JIGGLE", GetObjectX(unit), GetObjectY(unit), 33.0, 0.0);
}

void dialogTest(){
    UniChatMessage(SELF,"말을 하지마", 60);
}

void DemonSightEvent()
{
    if (DistanceUnitToUnit(SELF, OTHER) < 98.0)
	{
		DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
		Damage(OTHER, SELF, 35, 0);
	}
	if (GetCaller() ^ ToInt(GetObjectZ(GetTrigger() + 1)))
	{
		CreatureFollow(SELF, OTHER);
		Raise(GetTrigger() + 1, ToFloat(GetCaller()));
	}
	MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void DemonResetSight()
{
	int target = ToInt(GetObjectZ(GetTrigger() + 1));

	EnchantOff(SELF, "ENCHANT_BLINDED");
	if (!CurrentHealth(target))
	{
		Raise(GetTrigger() + 1, ToFloat(0));
		CreatureIdle(SELF);
	}
}

int SpawnDragon(int ptr)
{
    int unit = CreateObjectAt("Demon", GetObjectX(ptr), GetObjectY(ptr));

    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    SetUnitScanRange(unit, 480.0);
    // monsterCommonSettings(unit);
    SetUnitMaxHealth(unit, 1250);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020); //ALWAYS_RUN
    SetCallback(unit, 3, DemonSightEvent);
    // SetCallback(unit, 5, DemonDeadHandler);
    SetCallback(unit, 13, DemonResetSight);
    return unit;
}

void memTest2(){
    SpawnDragon(SELF);
}


