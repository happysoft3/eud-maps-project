
#include "pam00755_gvar.h"
#include "pam00755_fireway.h"
#include "pam00755_initscan.h"
#include "pam00755_reward.h"
#include "pam00755_mob.h"
#include "pam00755_shop.h"
#include "pam00755_utils.h"
#include "libs/coopteam.h"
#include "libs/playerupdate.h"
#include "libs/mathlab.h"
#include "libs/waypoint.h"
#include "libs/itemproperty.h"
#include "libs/bind.h"
#include "libs/printutil.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/game_flags.h"
#include "libs/groupUtils.h"
#include "libs/networkRev.h"

#define INIT_RESPAWN_MARK_LOCATION 13

void commonItemProcedure(int item)
{
    if (MaxHealth(item))
    {
        SetUnitMaxHealth(item,MaxHealth(item)*2);
        if (GetUnitFlags(item)&UNIT_FLAG_NO_COLLIDE)
            UnitNoCollide(item);
    }
}

#define GROUP_urchinWalls 0
#define GROUP_part2Walls 1
#define GROUP_southSentryTraps 3
#define GROUP_westSentryTraps 2
#define GROUP_warBossWalls 4

void initializePlayerRespawnMark()
{
    int r=MAX_PLAYER_COUNT;
    float x=LocationX(INIT_RESPAWN_MARK_LOCATION), y=LocationY(INIT_RESPAWN_MARK_LOCATION);
    int mark;

    while(--r>=0)
    {
        mark=CreateObjectById(OBJ_SPINNING_CROWN, x+MathSine(r*10 +90, 11.0), y+MathSine(r*10, 11.0));
        UnitNoCollide(mark);
        CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(mark), GetObjectY(mark));
        SetUserRespawnMark(r, mark);
    }
}

void onSoulGateTouched()
{
    int pIndex=GetPlayerIndex(OTHER);

    if (pIndex<0)
        return;
    
    int mark=GetUserRespawnMark(pIndex);
    float x=GetObjectX(SELF), y=GetObjectY(SELF);

    if (DistanceUnitToUnit(SELF, mark) > 200.0)
    {
        MoveObject(mark,x,y);
        GreenSparkAt(x,y);
        PlaySoundAround(mark, SOUND_SoulGateTouch);
        UniPrint(OTHER, "이 영혼의 문에 연결이 되었어요. 죽게 될 경우, 이곳에서 다시 부활을 하게 됩니다");
    }
}

void placeSoulGate(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);

    Delete(posUnit);
    int gate=DummyUnitCreateById(OBJ_WILL_O_WISP, x,y);

    SetDialog(gate,"NORMAL",onSoulGateTouched, onSoulGateTouched);
    int fx= CreateObjectById(OBJ_MAGIC_MISSILE,x,y);
    SetUnitSubclass(fx,GetUnitSubclass(fx)^1);
    Frozen(fx,TRUE);
}

#define HOME_FIELD_LOCATION 12
void onFieldGuideUse()
{
    int fps[MAX_PLAYER_COUNT], *cfps=0x84ea04;

    if (CurrentHealth(OTHER))
    {
        int pIndex=GetPlayerIndex(OTHER);

        if ((ABS(cfps[0]- fps[pIndex]) )>15)
            UniPrint(OTHER, "이 비스롤을 더블클릭하면, 마을로 공간이동을 하게 됩니다");
        else
        {
            MoveObject(OTHER, LocationX(HOME_FIELD_LOCATION),LocationY(HOME_FIELD_LOCATION));
            UniPrint(OTHER, "마을로 이동했습니다");
        }
        fps[pIndex]=cfps[0];
    }
}

void placeHomeFieldGuide(float x, float y)
{
    int guide=CreateObjectById(OBJ_FIELD_GUIDE, x,y);

    UnitNoCollide(guide);
    SetUnitCallbackOnUseItem(guide, onFieldGuideUse);
}

void placeInitialWeapon(float x, float y)
{
    int w= CreateObjectById(OBJ_SWORD,x,y);

    SetWeaponPropertiesDirect(w, ITEM_PROPERTY_weaponPower2, ITEM_PROPERTY_Matrial3, ITEM_PROPERTY_fire1, NULLPTR);
    SetUnitMaxHealth(w, MaxHealth(w)*2);
    UnitNoCollide(w);
}

void placeInitialArmor(float x, float y)
{
    int a=CreateObjectById(OBJ_BREASTPLATE,x,y);

    SetArmorPropertiesDirect(a,ITEM_PROPERTY_armorQuality2, ITEM_PROPERTY_Matrial3, ITEM_PROPERTY_FireProtect1, NULLPTR);
    SetUnitMaxHealth(a, MaxHealth(a)*2);
    UnitNoCollide(a);
}

void placeInitialPotions(float x, float y)
{
    int pot=CreateObjectById(OBJ_RED_POTION, x,y);

    UnitNoCollide(pot);
}

void invokeInitialCreateItem(int fn, float x, float y)
{
    Bind(fn, &fn + 4);
}

void placeInitialItems(int locationId, int fn,float gap)
{
    int r=15;
    float baseX=LocationX(locationId),baseY=LocationY(locationId);

    while (--r>=0)
        invokeInitialCreateItem(fn, baseX + MathSine(r*24 + 90,gap),baseY+MathSine(r*24, gap));
}

void initializeReadables()
{
    RegistSignMessage(Object("read1"), "<<--전사의 모험-------- 제작. 237-----------------<");
    RegistSignMessage(Object("read2"), "게임을 시작하려면 레드카펫 위에 놓인 비콘을 밟으세요");
    RegistSignMessage(Object("read3"), "이 맵은 전사의 모험을 위한 지도로서 237 에 의하여 제작되었습니다");
    RegistSignMessage(Object("read4"), "이 표지판 뒤에 있는 벽을 만져보세요. 그러면 열릴지어다");
    RegistSignMessage(Object("read5"), "전사 기술 구입하는 곳입니다. npc에게 말을 걸어서 돈을 지불하여 배우세요");
    RegistSignMessage(Object("read6"), "마지막에 저장된 위치로 공간이동 시켜주는 npc 입니다. 많이 이용해주세요~");
    RegistSignMessage(Object("read7"), "보석 수집가- 이 할배가 보석을 금화로 교환해 줄 것입니다");
    RegistSignMessage(Object("read8"), "!!-- - 무기 및 갑옷의 내구도 무한 기능 - --!!");
    RegistSignMessage(Object("read9"), "축하합니다, 당신은 이 맵을 모두 클리어 하셨어요 ^^");
}

void onHarpoonPulling()
{
    if (CurrentHealth(OTHER))
        PushObjectTo(SELF, UnitRatioX(SELF,OTHER, -4.0), UnitRatioY(SELF,OTHER, -4.0));
}

void harpoonPreserve(int unit)
{
    char *pcode;

    if (!pcode)
    {
        char code[]={
            0x56, 0x57, 0x52, 0x55, 0x8B, 0xEC, 0x83, 0xEC, 0x20, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0x38, 0x8B, 0xB7, 0xEC, 0x02, 
            0x00, 0x00, 0x8B, 0x40, 0x04, 0x89, 0x45, 0xFC, 0x8B, 0x86, 0x84, 0x00, 0x00, 0x00, 0x85, 0xC0, 0x74, 0x2E, 0x8B, 0x86, 0x84, 0x00, 
            0x00, 0x00, 0xF6, 0x40, 0x10, 0x20, 0x74, 0x0B, 0x57, 0xB8, 0x20, 0x75, 0x53, 0x00, 0xFF, 0xD0, 0x58, 0xEB, 0x17, 0x8B, 0x96, 0x84, 
            0x00, 0x00, 0x00, 0x52, 0x57, 0x68, 0x00, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x83, 0xC4, 
            0x20, 0x8B, 0xE5, 0x5D, 0x5A, 0x5F, 0x5E, 0x31, 0xC0, 0xC3, 0x90, 0x90,
        };
        pcode=code;
        int *n=&code[72];
        n[0]=onHarpoonPulling;
    }
    int *ptr=UnitToPtr(unit);

    if(!ptr) return;
    int *pec=ptr[187];

    if (pec[34])
    {
        int args[]={ptr, -10.0};
        invokeRawCode(pcode, args);
        PushTimerQueue(1,unit,harpoonPreserve);
        return;
    }
    if (pec)
        SetMemory(pec+0x2c0, 0);
}

int harpoonCodeCopied()
{
    char *code;

    if (code)
        return code;

    char copiedCode[980];//2ec (+5b4)
    int calls[]={
0x0054f3b9,
0x0054f3c9,
0x0054f3d9,
0x0054f3e9,
0x0054f41a,
0x0054f437,
0x0054f480,
0x0054f49c,
0x0054f4d8,
0x0054f502,
0x0054f530,
0x0054f554,
0x0054f592,
0x0054f5e9,
0x0054f630,
0x0054f643,
0x0054f669,
0x0054f6e2,
0x0054f6fa,
0x0054f729,
0,
    };
    OpcodeCopiesAdvance(copiedCode, calls, 0x54f380, 0x54f738);
    SetMemory(copiedCode+215, 0x5b4);
    SetMemory(copiedCode+228, 0x100);
    SetMemory(copiedCode+238, 0x104);
    code=copiedCode;
    return code;
}

int attachExtendedUnitFieldFix114To5b4(int unit)
{
    int *ptr=UnitToPtr(unit);
    int *pEC = ptr[187];

    if (!pEC[365])
    {
        int c;
        AllocSmartMemEx(0x1000, &c);
        int *newField114 = c;
        NoxDwordMemset(newField114, 1024, 0);
        pEC[365]=newField114;
        return c;
    }
    return pEC[365];
}

void castHarpoonForcedFix(int unit, float gap)
{
    char code[]={0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x50, 
    0xB8, 0x90, 0x88, 0x53, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3, 0x90,};
    int *p=UnitToPtr(unit);    
    int *pEC=GetMemory(p+0x2ec);

    if (GetMemory(pEC+0x2c0))
        return;
    
    int *field = attachExtendedUnitFieldFix114To5b4(unit);
    float xvect=UnitAngleCos(unit,gap),yvect=UnitAngleSin(unit,gap);

    field[0x100]=FloatToInt(GetObjectX(unit)+xvect);
    field[0x104]=FloatToInt(GetObjectY(unit)+yvect);
    invokeRawCode(code, p);
    int *ptr=GetMemory(0x750710);

    SetMemory(ptr+0x2e8, harpoonCodeCopied());
    SetMemory(pEC+0x2c0, ptr);
    PushTimerQueue(1,unit,harpoonPreserve);
}

void initializeWarBoss()
{
    int boss = Object("FinalBoss");

    SetWarBoss(boss);
    LookWithAngle(boss, 64);

    int trg=DummyUnitCreateById(OBJ_BOMBER_BLUE, LocationX(140),LocationY(140));

    SetDialog(trg,"YESNO", StartLizardTalk, enterPlace6);
    StoryPic(trg,"FrogPic");
}

int UrchinBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 28265; arr[16] = 11000; arr[17] = 40; arr[18] = 10; 
		arr[19] = 75; arr[21] = 1056964608; arr[23] = 34816; arr[24] = 1067869798; arr[26] = 4; 
		arr[37] = 1769236816; arr[38] = 1181513062; arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1128792064; 
		arr[54] = 4; arr[55] = 20; arr[56] = 30; arr[60] = 1339; arr[61] = 46904832; 
	pArr = arr;
	return pArr;
}

void UrchinSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074790400;		ptr[137] = 1074790400;
	int *hpTable = ptr[139];
	hpTable[0] = 40;	hpTable[1] = 40;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = UrchinBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1056964608;
}

void gauntlet1UrchinCountCore(int **pp)
{
    int count;

    pp[0]=&count;
}

void onUrchinDie()
{
    int *pCount;

    gauntlet1UrchinCountCore(&pCount);
    if (--pCount[0]>0)
        return;

    WallOpen(Wall(60,194));
    UniPrintToAll( "다음 구간으로 향하는 벽이 열렸습니다");
}

void spawnGauntletUrchin(int locationId)
{
    int mon=CreateObjectById(OBJ_URCHIN, LocationX(locationId),LocationY(locationId));

    UrchinSubProcess(mon);
    SetCallback(mon,5,onUrchinDie);
}

void startGauntletUrchin()
{
    int dup;

    if (GetAnswer(SELF)^1)
        return;

    if (dup)
        return;

    int count=5;
    int *pCount = NULLPTR;

    gauntlet1UrchinCountCore(&pCount);
    while (--count>=0)
    {
        spawnGauntletUrchin(18);
        spawnGauntletUrchin(19);
        pCount[0]+=2;
    }
    dup=TRUE;
    WallGroupSetting(GROUP_urchinWalls, WALL_GROUP_ACTION_OPEN);
}

void placeGateNpc1()
{
    int keeper=DummyUnitCreateById(OBJ_SWORDSMAN, LocationX(16),LocationY(16));

    LookWithAngle(keeper, 32);
    SetDialog(keeper,"YESNO", StartLuckyBoyTalk, startGauntletUrchin);
    StoryPic(keeper, "Warrior9Pic");
}

void openGate1()
{
    int dup;

    if (GetAnswer(SELF)^1)
        return;

    if (dup)
        return;

    dup=TRUE;
    UniChatMessage(SELF, "어서 가게나! 우리에게는 시간이 별로 없다 이말이야!", 120);
    placeGateNpc1();
    WallOpen(Wall(36,218));
}

void placeGuideAtStartingPoint()
{
    int guide=DummyUnitCreateById(OBJ_HORRENDOUS, LocationX(17),LocationY(17));

    SetDialog(guide,"YESNO",StartHorrendousTalk, openGate1);
    StoryPic(guide, "HorrendousPic");
    LookWithAngle(guide, 92);
}

void elevMobScorpion(int location)
{
    int m=CreateScorpionMonster( LocationX(location),LocationY(location));

    ScorpionSubProcess(m);
}

void elevMobMystic(int location)
{
    int my=CreateObjectById(OBJ_WIZARD, LocationX(location),LocationY(location));

    Enchant(my,"ENCHANT_ANCHORED", 0.0);
    SetUnitMaxHealth(my,225);
}

void monsterElevatorOff(int n)
{
    int ev;
    MonsterElevator(n, 0, &ev);
    // char msg[128];

    // NoxSprintfString(msg,"monsterElevatorOff-%d", &ev, 1);
    // UniPrintToAll(ReadStringAddressEx(msg));
    if (IsObjectOn(ev))
        ObjectOff(ev);
}

void putMonsterOnElevator()
{
    int m= gauntletMon1(51);

    RetreatLevel(m,0.0);
    elevMobScorpion(52);
    elevMobMystic(53);
    m=CreateObjectById(OBJ_EMBER_DEMON,LocationX(54),LocationY(54));
    SetUnitMaxHealth(m,225);
}

void initMonsterElevators()
{
    int evs[]={Object("mobEv1"),Object("mobEv2"),Object("mobEv3"),Object("mobEv4"),};

    MonsterElevator(0, evs[0], NULLPTR);
    MonsterElevator(1, evs[1], NULLPTR);
    MonsterElevator(2, evs[2], NULLPTR);
    MonsterElevator(3, evs[3], NULLPTR);
    ObjectOn(evs[0]);
    ObjectOn(evs[1]);
    ObjectOn(evs[2]);
    ObjectOn(evs[3]);
    FrameTimerWithArg(48, 0, monsterElevatorOff);
    FrameTimerWithArg(48, 1, monsterElevatorOff);
    FrameTimerWithArg(48, 2, monsterElevatorOff);
    FrameTimerWithArg(48, 3, monsterElevatorOff);
    FrameTimer(120,putMonsterOnElevator);
}

void initGame()
{
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}

void onlanternPickup()
{
    PlaySoundAround(OTHER,SOUND_JournalEntryAdd);
    UniPrint(OTHER,"전사의 모험! - 그런데 그 모험이 위험하고 힘들것이다, 그것을 극복해 보자고요~ 할 수 있잖아요? -제작.237");
}

void putUsefulItems()
{
    int item;

    item=CreateObjectById(OBJ_PLATE_ARMS, LocationX(56),LocationY(56));
    SetArmorPropertiesDirect(item,ITEM_PROPERTY_armorQuality2,ITEM_PROPERTY_Matrial3, ITEM_PROPERTY_Speed1, ITEM_PROPERTY_FireProtect1);
    commonItemProcedure(item);
    item=CreateObjectById(OBJ_WOODEN_SHIELD, LocationX(57),LocationY(57));
    SetArmorPropertiesDirect(item,ITEM_PROPERTY_armorQuality2,ITEM_PROPERTY_Matrial3, ITEM_PROPERTY_FireProtect1,NULLPTR);
    commonItemProcedure(item);
    CreateObjectById(OBJ_RED_POTION, LocationX(58),LocationY(58));
    CreateObjectById(OBJ_RED_POTION, LocationX(59),LocationY(59));
    CreateObjectById(OBJ_RED_POTION, LocationX(60),LocationY(60));
    CreateObjectById(OBJ_RED_POTION, LocationX(61),LocationY(61));

    int lt= CreateObjectById(OBJ_LANTERN_2, LocationX(260),LocationY(260));

    SetUnitCallbackOnPickup(lt,onlanternPickup);
}

#include "pam00755_resource.h"

void InitializeClientside(int serverSide)
{
    initDialog();
    initPopupMessages();
    InitializeCommonImage();
    if (!serverSide)
        FrameTimer(1, ClientProcLoop);
}

void OnEnterMap()
{
    MakeCoopTeam();
    int lastUnit=CreateObjectById(OBJ_RED_POTION, 100.0,100.0);
    initializePlayerRespawnMark();
    initializeReadables();
    placeInitialItems(14, placeInitialWeapon, 66.0);
    placeInitialItems(15, placeInitialPotions, 50.0);
    placeInitialItems(163, placeHomeFieldGuide, 50.0);
    placeInitialItems(55, placeInitialArmor, 60.0);
    placeGuideAtStartingPoint();
    initMonsterElevators();
    initGame();
    FrameTimer(1,putUsefulItems);
    
    int initScanHash;
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_HECUBAH_MARKER, placeSoulGate);
    HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, CreateRandomMonster);
    StartUnitScan(Object("firstscan"), lastUnit, initScanHash);

    int trap = Object("decoSkull");

    ObjectOff(trap);
    LookWithAngle(trap, GetDirection(trap) + 128);
    ObjectOn(trap);
    CreateObjectById(OBJ_TARGET_BARREL_1,LocationX(154),LocationY(154));
    FrameTimer(30, InitializeShop);
    FrameTimer(30, initializeWarBoss);
    FrameTimerWithArg(3, TRUE, InitializeClientside); //server side
}

void OnShutdownMap()
{
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void OnFirewayTriggered()
{
    int *pControl = FirewayControlPtr();

    ObjectOff(SELF);
    StartFireway(20,9,pControl,1);
    StartFireway(21,9,pControl,4);
    StartFireway(22,9,pControl,7);
    StartFireway(23,9,pControl,10);
    StartFireway(24,9,pControl,13);
}

void clearRoom2()
{
    WallOpen(Wall(21,157));
    WallOpen(Wall(22,158));
    UniPrintToAll("다음 구간으로 향하는 비밀벽이 열렸습니다");
}

int gauntletMon1(int locationId)
{
    int mon=CreateObjectById(OBJ_SWORDSMAN, LocationX(locationId), LocationY(locationId));

    SetUnitMaxHealth(mon, 250);
    SetUnitStatus(mon,GetUnitStatus(mon)^MON_STATUS_ALWAYS_RUN);
    return mon;
}

int gauntletMon2(int locationId)
{
    int mon=CreateObjectById(OBJ_ARCHER, LocationX(locationId), LocationY(locationId));

    SetUnitMaxHealth(mon, 130);
    return mon;
}

int gauntletMon3(int locationId)
{
    int mon=CreateObjectById(OBJ_FLYING_GOLEM,LocationX(locationId),LocationY(locationId));

    SetUnitMaxHealth(mon, 98);
    return mon;
}

void deathCountRoom2(int **pp)
{
    int count;
    pp[0]=&count;
}

int invokeGauntletMonRoom2(int *fn, int locationId)
{
    return Bind(fn, &locationId);
}

void onRoom2Death()
{
    int *pCount;

    deathCountRoom2(&pCount);
    if (--pCount[0]>0)
        return;

    clearRoom2();
}

void mobSpawnAtRoom2(short locationId, int caller)
{
    int fn[]={gauntletMon3,gauntletMon2,gauntletMon1, };
    int m = invokeGauntletMonRoom2(fn[Random(0,sizeof(fn)-1)], locationId);

    RetreatLevel(m,0.0);
    ResumeLevel(m,0.0);
    AggressionLevel(m,1.0);
    LookWithAngle(m,caller);
    SetCallback(m,5,onRoom2Death);

    int *pCount;

    deathCountRoom2(&pCount);
    ++pCount[0];
}

void progressRoom2(short *locations, int caller)
{
    while (locations[0])
    {
        mobSpawnAtRoom2(locations[0], caller);
        locations=&locations[1];
    }
}

void startGauntlet2()
{
    int dup;

    if (dup)
        return;
    dup=TRUE;
    short locations[]={25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,
    0,};

    progressRoom2(locations, GetCaller());
}

void FirewayOffTriggered()
{
    int *pControl=FirewayControlPtr();

    ObjectOff(SELF);
    pControl[0]=FALSE;
}

void StartPart2()
{
    ObjectOff(SELF);
    WallGroupSetting(GROUP_part2Walls, WALL_GROUP_ACTION_OPEN);
    startGauntlet2();
}

void movingRoom2Blocks()
{
    int dup;

    ObjectOff(SELF);
    if (dup)
        return;
    dup=Object("room2b1");
    Move(dup, 44);
    PlaySoundAround(dup, SOUND_SpikeBlockMove);
    Move(Object("room2b2"), 45);
}

void togglingRoom2Blocks()
{
    ObjectToggle(Object("room2mv1"));
    ObjectToggle(Object("room2mv2"));
}

void loopFonTrapRoom3(int *pControl)
{
    int fon=CreateObjectById(OBJ_DEATH_BALL, LocationX(46),LocationY(46));

    PushObjectTo(fon, 7.0, 7.0);
    PlaySoundAround(fon, SOUND_ForceOfNatureRelease);
    if (pControl[0])
        PushTimerQueue(pControl[0], pControl, loopFonTrapRoom3);
}

void endRoom3()
{
    ObjectOff(SELF);
    int *pControl;

    if (pControl)
        return;

    UnlockDoor(Object("outRoom31"));
    UnlockDoor(Object("outRoom32"));
    pControl = FonTrapControlPtr();
    pControl[0]=0;
    ObjectOff(Object("decoSkull"));
    UniPrint(OTHER, "함정이 작동을 중단했고, 문의 잠금이 해제되었습니다");
}

void startRoom3()
{
    int dup;

    ObjectOff(SELF);
    if (dup)
        return;

    dup=TRUE;
    PushTimerQueue(45, FonTrapControlPtr(), loopFonTrapRoom3);
    createMobSpider( LocationX(47), LocationY(47));
    createMobSpider( LocationX(48), LocationY(48));
    createMobSpider( LocationX(49), LocationY(49));
    createMobSpider( LocationX(50), LocationY(50));
}

#define SENTRY_WALL_PTR 0
#define SENTRY_WALL_COUNT 1
#define SENTRY_WALL_CURRENT_COUNT 2
#define SENTRY_WALL_MAX 3

int nextWallIndex(int *pcur, int max)
{
    int ret = pcur[0];

    pcur[0] = (pcur[0]+1)%max;
    return ret;
}

void togglingSentryWalls(int *p)
{
    int *pctl=SentryWallsControlPtr();

    if (!pctl[0])
        return;

    int *pWalls = p[SENTRY_WALL_PTR];
    int max=p[SENTRY_WALL_COUNT];
    int *pcur=&p[SENTRY_WALL_CURRENT_COUNT];

    WallToggle(pWalls[nextWallIndex(pcur,max)]);
    WallToggle(pWalls[nextWallIndex(pcur,max)]);
    WallToggle(pWalls[nextWallIndex(pcur,max)]);
    PushTimerQueue(13, p,togglingSentryWalls);
}

void startSentryWalls(int *pWalls, int count)
{
    int *p;

    AllocSmartMemEx(SENTRY_WALL_MAX*4, &p);
    p[SENTRY_WALL_PTR]=pWalls;
    p[SENTRY_WALL_COUNT]=count;
    p[SENTRY_WALL_CURRENT_COUNT]=0;
    PushTimerQueue(10, p, togglingSentryWalls);
}

void standbySentryTrapWalls()
{
    int lineA[14];
    int xoff=89,yoff=185;
    int 
    r=sizeof(lineA);

    while (--r>=0)
    {
        lineA[r]=Wall(xoff+r,yoff+r);
        WallOpen(lineA[r]);
    }

    int lineB[16];
    int xoff2=98,yoff2=204;
    
    r=sizeof(lineB);

    while (--r>=0)
    {
        lineB[r]=Wall(xoff2-r,yoff2+r);
        WallOpen(lineB[r]);
    }
    startSentryWalls(lineA, sizeof(lineA));
    startSentryWalls(lineB, sizeof(lineB));
}

void startSentryTraps()
{
    int dup;

    ObjectOff(SELF);
    if (dup)
        return;

    dup=TRUE;
    ObjectGroupOn(GROUP_southSentryTraps);
    ObjectGroupOn(GROUP_westSentryTraps);
    standbySentryTrapWalls();
    UniPrint(OTHER, "감시광선 함정이 작동을 시작하였습니다");
}

void disableSentryTraps()
{
    int *p;

    ObjectOff(SELF);
    if (p)
        return;

    p=SentryWallsControlPtr();
    p[0]=FALSE;
    ObjectGroupOff(GROUP_southSentryTraps);
    ObjectGroupOff(GROUP_westSentryTraps);
}

void loadMobElevator(int n)
{
    int ev;

    MonsterElevator(n,0,&ev);
    ObjectOn(ev);
    PushTimerQueue(18, n, monsterElevatorOff);
    ObjectOff(SELF);
}

void startMobEv1()
{
    loadMobElevator(0);
}

void startMobEv2()
{
    loadMobElevator(1);
}

void startMobEv3()
{
    loadMobElevator(2);
}

void startMobEv4()
{
    loadMobElevator(3);
}

void enterPlace1()
{
    ObjectOff(SELF);
    int item = CreateObjectById(OBJ_MORNING_STAR, LocationX(66),LocationY(66));
    SetWeaponPropertiesDirect(item,ITEM_PROPERTY_weaponPower3,ITEM_PROPERTY_Matrial4, ITEM_PROPERTY_fire1,NULLPTR);
    commonItemProcedure(item);
    CreateObjectById(OBJ_RED_POTION,LocationX(67),LocationY(67));
    item=CreateObjectById(OBJ_PLATE_BOOTS,LocationX(68),LocationY(68));
    SetArmorPropertiesDirect(item,ITEM_PROPERTY_armorQuality2,ITEM_PROPERTY_Matrial3,ITEM_PROPERTY_FireProtect1,NULLPTR);
    commonItemProcedure(item);
    SetUnitMaxHealth(CreateObjectById(OBJ_SPITTING_SPIDER, LocationX(62),LocationY(62)), 128);
    SetUnitMaxHealth(CreateObjectById(OBJ_SPITTING_SPIDER, LocationX(63),LocationY(63)), 128);
    SetUnitMaxHealth(CreateObjectById(OBJ_SMALL_SPIDER, LocationX(64),LocationY(64)), 64);
    SetUnitMaxHealth(CreateObjectById(OBJ_SMALL_SPIDER, LocationX(65),LocationY(65)), 64);
    WallGroupSetting(WallGroup("blueDoomWalls"), WALL_GROUP_ACTION_OPEN);
}

void enterPlace2()
{
    int dup;

    ObjectOff(SELF);
    if (dup)
        return;

    dup=TRUE;
    CreateObjectById(OBJ_BEAR_TRAP,LocationX(69),LocationY(69));
    CreateObjectById(OBJ_BEAR_TRAP,LocationX(70),LocationY(70));
    CreateObjectById(OBJ_BEAR_TRAP,LocationX(71),LocationY(71));
    CreateObjectById(OBJ_BEAR_TRAP,LocationX(72),LocationY(72));
    CreateObjectById(OBJ_BEAR_TRAP,LocationX(73),LocationY(73));
    int item=CreateObjectById(OBJ_MEDIEVAL_CLOAK, LocationX(74),LocationY(74));
    SetArmorPropertiesDirect(item,ITEM_PROPERTY_armorQuality2,NULLPTR,ITEM_PROPERTY_Regeneration1,NULLPTR);
    commonItemProcedure(item);
    CreateObjectById(OBJ_RED_POTION,LocationX(75),LocationY(75));
    CreateObjectById(OBJ_RED_POTION,LocationX(78),LocationY(78));
    CreateObjectById(OBJ_RED_POTION,LocationX(79),LocationY(79));
    CreateObjectById(OBJ_RED_POTION,LocationX(80),LocationY(80));
    CreateObjectById(OBJ_RED_POTION,LocationX(81),LocationY(81));
    CreateObjectById(OBJ_RED_POTION,LocationX(82),LocationY(82));
    item=CreateObjectById(OBJ_BREASTPLATE,LocationX(76),LocationY(76));
    SetArmorPropertiesDirect(item,ITEM_PROPERTY_armorQuality2,ITEM_PROPERTY_Matrial5,ITEM_PROPERTY_LightningProtect2,NULLPTR);
    commonItemProcedure(item);
    item=CreateObjectById(OBJ_LONGSWORD,LocationX(77),LocationY(77));
    SetWeaponPropertiesDirect(item,ITEM_PROPERTY_weaponPower3,ITEM_PROPERTY_Matrial4,ITEM_PROPERTY_venom2,ITEM_PROPERTY_stun1);
    commonItemProcedure(item);
    CreateObjectById(OBJ_CURE_POISON_POTION,LocationX(83),LocationY(83));
    CreateObjectById(OBJ_CURE_POISON_POTION,LocationX(84),LocationY(84));
}

void spawnFarUrchin(int locationId)
{
    int mon=CreateObjectById(OBJ_URCHIN,LocationX(locationId),LocationY(locationId));

    SetUnitMaxHealth(mon, 64);
    SetUnitStatus(mon, GetUnitStatus(mon)^MON_STATUS_HOLD_YOUR_GROUND);
    UnitZeroFleeRange(mon);
    RetreatLevel(mon,0.0);
    SetUnitScanRange(mon, 400.0);
}

void enterPlace3()
{
    ObjectOff(SELF);

    spawnFarUrchin(85);
    spawnFarUrchin(86);
    spawnFarUrchin(87);
    spawnFarUrchin(88);
    createMobSpider( LocationX(89), LocationY(89));
    createMobSpider( LocationX(90), LocationY(90));
    WallGroupSetting(WallGroup("fountainWalls"),WALL_GROUP_ACTION_OPEN);
}

void toggleUnlockGate()
{
    int *p, lval, uval;

    if (!p)
    {
        int gates[]={Object("upgate1"), Object("upgate11"),Object("downgate1"), Object("downgate11"),};
        p=gates;
        uval=2;
    }
    lval^=2;
    uval^=2;
    UnlockDoor(p[uval]);
    UnlockDoor(p[uval+1]);
    LockDoor(p[lval]);
    LockDoor(p[lval+1]);
}

void onLizardDeath()
{
    float x=GetObjectX(SELF), y=GetObjectY(SELF);

    CreateObjectById(OBJ_BREAKING_SOUP, x,y);
    CreateRandomItemCommon(CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y));
}

void putLizardOnHive(int hive)
{
    int mon=createMobLizard( GetObjectX(hive),GetObjectY(hive));

    SetCallback(mon, 5, onLizardDeath);
    UniChatMessage(mon, "나의 소중한 둥지를 파괴하디니, 각오하여라", 90);
}

void onHiveDestroyedDefault(int hive)
{
    float x=GetObjectX(hive),y=GetObjectY(hive);

    CreateObjectById(OBJ_WASP_NEST_DESTROY, x,y);
    DeleteObjectTimer( CreateObjectById(OBJ_BIG_SMOKE, x,y), 18);
    PlaySoundAround(hive, SOUND_WaspHiveBreak);
}

void onHiveDestroyed()
{
    onHiveDestroyedDefault(SELF);
    putLizardOnHive(SELF);
    putLizardOnHive(SELF);
    putLizardOnHive(SELF);
    Delete(SELF);
}

void onHiveDestoryedWithKey()
{
    onHiveDestroyedDefault(SELF);
    CreateObjectById(OBJ_SILVER_KEY,GetObjectX(SELF),GetObjectY(SELF));
    Delete(SELF);
}

int putHiveNest(int locationId)
{
    int hive=CreateObjectById(OBJ_WASP_NEST, LocationX(locationId),LocationY(locationId));

    SetUnitMaxHealth(hive, 485);
    SetUnitCallbackOnDeath(hive, onHiveDestroyed);
    return hive;
}

int putHiveNestGroup(short *pLoc, int *pCount)
{
    int arr[32], count;

    while (pLoc[0])
    {
        arr[count] = putHiveNest(pLoc[0]);
        pLoc=&pLoc[1];
        count+=1;
    }
    if (pCount)
        pCount[0]=count;
    return arr;
}

void enterPlace4()
{
    ObjectOff(SELF);
    short locations[]={91,92,93,94,95,96,97,98,99,100,101,102,103,104,0,};
    int count;
    int *hives = putHiveNestGroup(locations, &count);

    SetUnitCallbackOnDeath( hives[Random(0, count-1)], onHiveDestoryedWithKey);
}

int DeathRayShooter()
{
    int st;

    if (!st)
        st=DummyUnitCreateById(OBJ_WIZARD_GREEN,LocationX(105),LocationY(105));
    return st;
}

int shooterHash()
{
    int hash;

    if (!hash)
    {
        HashCreateInstance(&hash);
    }
    return hash;
}

void deferredEraseEnemyFromHash(int enemy)
{
    int hash=shooterHash(), val;

    HashGet(hash, enemy, &val, TRUE);
}

void pushEnemy(int enemy)
{
    int hash=shooterHash();
    int val;

    if (HashGet(hash, enemy, &val, FALSE))
        return;

    int st=DeathRayShooter();

    if (IsVisibleOr(st,enemy))
    {
        LookAtObject(st,enemy);
        Damage(enemy, 0, 100, DAMAGE_TYPE_ZAP_RAY);
        PlaySoundAround(st,SOUND_DeathRayCast);
        PlaySoundAround(enemy,SOUND_DeathRayKill);
        Effect("DEATH_RAY", GetObjectX(st),GetObjectY(st),GetObjectX(enemy),GetObjectY(enemy));
        HashPushback(hash,enemy, TRUE);
        PushTimerQueue(60, enemy, deferredEraseEnemyFromHash);
    }
}

void shooterHit()
{
    if (CurrentHealth(OTHER))
    {
        pushEnemy(GetCaller());
    }
}

void deferredMovingPad(int *p)
{
    int *pads=p[0];
    short *destinations=p[1];

    while (destinations[0])
    {
        SetUnitFlags(pads[0],GetUnitFlags(pads[0])^UNIT_FLAG_NO_PUSH_CHARACTERS);
        SetUnitCallbackOnCollide(pads[0],shooterHit);

        Move(pads[0], destinations[0]);
        destinations=&destinations[1];
        pads=&pads[1];
    }
}

void startMovingShooterPad()
{
    short destinations[]={
        134,
        109,
        110,
        112,
        129,
        114,
        126,
        115,
        124,
        118,
        122,
        120,
        134,
        132,
        116,
    };
    int u= sizeof(destinations);
    int pic=u;
    char nameBuff[64];
    int pads[32];

    while (--u>=0)
    {
        NoxSprintfString(nameBuff, "rayHit%d", &pic, 1);
        pic-=1;
        pads[u]=Object(ReadStringAddressEx(nameBuff));
    }
    int params[]={pads,destinations,};

    PushTimerQueue(3, params, deferredMovingPad);
}

void enterPlace5()
{
    ObjectOff(SELF);
    DeathRayShooter();
    startMovingShooterPad();
}

void moveIxBlocks()
{
    ObjectOff(SELF);
    PlaySoundAround(OTHER, SOUND_SpikeBlockMove);
    UniPrint(OTHER, "블럭이 작동을 시작합니다");
    Move(Object("IxBlock1"),136);
    Move(Object("IxBlock11"),138);
}

void removeEquipments(int unit)
{
	int inven = unit + 2;
	
	while (ToInt(GetObjectX(inven)))
    {
        if (GetUnitClass(inven) & (UNIT_CLASS_WEAPON | UNIT_CLASS_ARMOR))
		    Delete(inven);
		inven += 2;
	}
}

void onWarBossDeath()
{
    int elv=Object("npcElev"),npc=GetTrigger();

    ObjectOn(elv);
    removeEquipments(npc);
    UniChatMessage(npc, "윽, 주금...", 60);
    DeleteObjectTimer(npc, 1);
}

int GetMonsterTargetEnemy(int mob)
{
    char *ptr=UnitToPtr(mob);
    
    if (!ptr)
        return 0;
    char *pEC = GetMemory(&ptr[0x2ec]);
    if (!pEC)
        return 0;
    
    int *pTarget = &pEC[0x30c];

    if (!pTarget[0])
        return 0;
    return GetMemory(pTarget[0]+0x2c);
}

#define WAR_BOSS_UNIT 0
#define WAR_BOSS_COOLTIME 1
#define WAR_BOSS_MAX 2

void onWarBossHandler(int *pData)
{
    int boss = pData[WAR_BOSS_UNIT];

    if (CurrentHealth(boss))
    {
        PushTimerQueue(1, pData,onWarBossHandler);
        int cool=pData[WAR_BOSS_COOLTIME];

        if (cool>0)
        {
            --pData[WAR_BOSS_COOLTIME];
            return;
        }

        int target = GetMonsterTargetEnemy(boss);

        if (CurrentHealth(target) && IsVisibleOr(boss,target))
        {
            float dist=DistanceUnitToUnit(target,boss);
            if (dist>=60.0 && dist<=350.0)
            {
                LookAtObject(boss,target);
                castHarpoonForcedFix(boss, 48.0);
            }
            pData[WAR_BOSS_COOLTIME]=150;
        }
        return;
    }
    FreeSmartMemEx(pData);
}

void enterPlace6()
{
    int dup;

    if (GetAnswer(SELF)^1) return;
    if (dup) return;

    dup=TRUE;
    WallGroupSetting(GROUP_warBossWalls,WALL_GROUP_ACTION_OPEN);
    int boss=GetWarBoss();

    if (CurrentHealth(boss))
    {
        ObjectOn(boss);
        AggressionLevel(boss,1.0);
        SetCallback(boss,5,onWarBossDeath);
        NPCSetInventoryCount(boss);
        MoveObject(boss,LocationX(140),LocationY(140));
        int *data;

        AllocSmartMemEx(WAR_BOSS_MAX*4, &data);
        data[WAR_BOSS_UNIT]=boss;
        data[WAR_BOSS_COOLTIME]=0;

        PushTimerQueue(1, data, onWarBossHandler);
        UniChatMessage(boss, "그 자신감, 내가 부수어 버리겠어!", 90);
    }
}

#define SKULLTRAP_GAP 6.0
#define TRAP_INFO_UNIT 0
#define TRAP_INFO_DIR 1
#define TRAP_INFO_MAX 2

void deferredTrapDirChange(int *p)
{
    int trap=p[TRAP_INFO_UNIT];

    if (ToInt(GetObjectX(trap)))
    {
        LookWithAngle(trap, p[TRAP_INFO_DIR]);
        HookFireballTraps(trap,OBJ_LIGHTNING_BOLT);
        ObjectOn(trap);
    }
    FreeSmartMemEx(p);
}

void placeFireballTrapCommon(short decoTy, float xVect, float yVect, char angle,float *pos)
{
    int vinus=CreateObjectById(decoTy,pos[0],pos[1]);
    int trpReal=CreateObjectById(OBJ_ARROW_TRAP_2,pos[0]+xVect,pos[1]+yVect);

    ObjectOff(trpReal);
    int *p;

    AllocSmartMemEx(TRAP_INFO_MAX*4,&p);
    p[TRAP_INFO_UNIT]=trpReal;
    p[TRAP_INFO_DIR]=angle;
    PushTimerQueue(1, p, deferredTrapDirChange);
}

void placeFireballTrapNorth(float x,float y)
{
    placeFireballTrapCommon(OBJ_MOVABLE_STATUE_VICTORY_4_NW,-SKULLTRAP_GAP,-SKULLTRAP_GAP,160,&x);
}

void placeFireballTrapSouth(float x,float y)
{
    placeFireballTrapCommon(OBJ_MOVABLE_STATUE_VICTORY_4_SE,SKULLTRAP_GAP,SKULLTRAP_GAP,32,&x);
}

void placeFireballTraps(int count)
{
    while (--count>=0)
    {
        placeFireballTrapSouth(LocationX(141),LocationY(141));
        TeleportLocationVector(141,46.0,-46.0);
        placeFireballTrapNorth(LocationX(142),LocationY(142));
        TeleportLocationVector(142,46.0,-46.0);
    }
}

void enterPlace7()
{
    ObjectOff(SELF);
    placeFireballTraps(10);
}

#define GROUP_ixBlockPits 6
#define GROUP_ixBlockWalls 5

void enterPlace8()
{
    ObjectOff(SELF);
    ObjectGroupOn(GROUP_ixBlockPits);
    WallGroupSetting(GROUP_ixBlockWalls,WALL_GROUP_ACTION_OPEN);
}

#define GROUP_ixWindowWalls 7

void removeIxFence()
{
    int dup;

    ObjectOff(SELF);
    if (dup)
        return;

    dup=TRUE;
    WallGroupSetting(GROUP_ixWindowWalls,WALL_GROUP_ACTION_OPEN);
}

#define GROUP_surpriseRoomOutWalls 9

void tryOpenSupExit()
{
    int count;

    ObjectOff(SELF);
    if (++count == 3)
    {
        WallGroupSetting(GROUP_surpriseRoomOutWalls,WALL_GROUP_ACTION_OPEN);
        UniPrintToAll("방금 비밀벽이 열렸습니다-!");
    }
}

void spawnLoop(int sub)
{
    if (!ToInt(GetObjectX(sub)))
        return;

    int count=GetDirection(sub);

    if (count)
    {
        SettingsMonsterCommon( CreateMobPlant(GetObjectX(sub),GetObjectY(sub)) );
        LookWithAngle(sub,--count);
        PushTimerQueue(3,sub,spawnLoop);
    }
}

void spawnSurprisePlant(short locationId, int count)
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,LocationX(locationId),LocationY(locationId));

    PushTimerQueue(1, sub, spawnLoop);
    LookWithAngle(sub,count);
}

#define GROUP_surprisingWalls 8

void enterPlace9()
{
    ObjectOff(SELF);
    WallGroupSetting(GROUP_surprisingWalls,WALL_GROUP_ACTION_OPEN);
    spawnSurprisePlant(143, 10);
    spawnSurprisePlant(144, 10);
    spawnSurprisePlant(145, 10);
}

void startIxBlocks2()
{
    ObjectOff(SELF);
    UniPrintToAll("주변 어디에선가 동력장치가 움직입니다");
    Move(Object("ixBlock2"),150);
    Move(Object("ixBlock21"),152);
    short locations[]={161,155,156,157,158,159,160,};
    int count=sizeof(locations);
    int mob;

    while (--count>=0)
    {
        mob= CreateMobGargoyle(LocationX(locations[count]), LocationY(locations[count]));
        LookAtObject(mob,OTHER);
    }
}

#define GROUP_zoneAWalls 10

void enterPlaceA()
{
    ObjectOff(SELF);
    WallGroupSetting(GROUP_zoneAWalls,WALL_GROUP_ACTION_OPEN);
    int wiz[]={
         CreateMobStrongWizard(LocationX(146),LocationY(146)),
        CreateMobStrongWizard(LocationX(147),LocationY(147)),
        CreateMobStrongWizard(LocationX(148),LocationY(148)),
        CreateMobStrongWizard(LocationX(149),LocationY(149)),
    };
    SettingsMonsterCommon(wiz[0]);
    SettingsMonsterCommon(wiz[1]);
    SettingsMonsterCommon(wiz[2]);
    SettingsMonsterCommon(wiz[3]);
    LookAtObject(wiz[0], SELF);
    LookAtObject(wiz[1], SELF);
    LookAtObject(wiz[2], SELF);
    LookAtObject(wiz[3], SELF);
}

#define GROUP_zoneANextWalls 11
void removeWallsZoneA()
{
    ObjectOff(SELF);
    WallGroupSetting(GROUP_zoneANextWalls,WALL_GROUP_ACTION_OPEN);
}
#define GROUP_ixArrowTraps 12

void disableIxArrow(int gid)
{
    ObjectGroupOff(gid);
}

void ixArrowTriggered()
{
    PushTimerQueue(1,GROUP_ixArrowTraps,disableIxArrow);
    ObjectGroupOn(GROUP_ixArrowTraps);
}

void startMovingLaiser()
{
    int dup;

    ObjectOff(SELF);
    if (dup)
        return;
    dup=TRUE;

    int sens[]={Object("sen1"),Object("sen2")};

    ObjectOn(sens[0]);
    ObjectOn(sens[1]);
    Move(sens[0], 170);
    Move(sens[1], 172);
}

void startSpinBlocks()
{
    ObjectOff(SELF);
    Move(Object("dangerA1"), 190);
    Move(Object("dangerA2"), 188);
    Move(Object("dangerB1"), 184);
    Move(Object("dangerB2"), 186);
    Move(Object("dangerC1"), 182);
    Move(Object("dangerC2"), 180);
    Move(Object("dangerD1"), 181);
    Move(Object("dangerD2"), 191);
}

void onGolemGeneratorDeath()
{
    int meca=CreateObjectById(OBJ_MECHANICAL_GOLEM,GetObjectX(SELF),GetObjectY(SELF));

    SetUnitMaxHealth(meca, 600);
    Delete(SELF);
}

void placeGolemGenerator(short locationId)
{
    int mg=CreateObjectById(OBJ_MECHANICAL_GOLEM_GENERATOR,LocationX(locationId),LocationY(locationId));

    ObjectOff(mg);
    SetUnitMaxHealth(mg, 480);
    Enchant(mg,"ENCHANT_FREEZE",0.0);
    SetUnitCallbackOnDeath(mg, onGolemGeneratorDeath);
}

void initialPlaceGenerators(int rowCount)
{
    while (--rowCount>=0)
    {
        placeGolemGenerator(174);
        placeGolemGenerator(175);
        placeGolemGenerator(176);
        placeGolemGenerator(177);
        TeleportLocationVector(174,46.0,46.0);
        TeleportLocationVector(175,46.0,46.0);
        TeleportLocationVector(176,46.0,46.0);
        TeleportLocationVector(177,46.0,46.0);
    }
}

int venusGuardian()
{
    int ret;

    if (!ret)
    {
        ret=
        CreateObjectById(OBJ_MOVABLE_STATUE_VICTORY_3_S,LocationX(201),LocationY(201));
        CreateObjectById(OBJ_MOVABLE_STATUE_VICTORY_3_E,LocationX(202),LocationY(202));
        CreateObjectById(OBJ_MOVABLE_STATUE_VICTORY_3_N,LocationX(203),LocationY(203));
        CreateObjectById(OBJ_MOVABLE_STATUE_VICTORY_3_W,LocationX(204),LocationY(204));
    }
    return ret;
}

void initialPlaceVenus()
{
    venusGuardian();
}

// #define GROUP_venusWalls 17

void venusDeathCount(int *get, int*set)
{
    int count;
    if (get)get[0]= count;
    if (set)count=set[0];
}

void onVenusDeath()
{
    int count;

    venusDeathCount(&count,0);
    if (--count>0)
    {
        venusDeathCount(0,&count);
        return;
    }
    WallGroupSetting(WallGroup("dryadWalls"),WALL_GROUP_ACTION_OPEN);
}

void venusTransform(int v)
{
    int count=4;
    float x,y;
    int wiz[4];

    venusDeathCount(0,&count);
    while (--count>=0)
    {
        x=GetObjectX(v+count);
        y=GetObjectY(v+count);
        Delete(v+count);
        Effect("SMOKE_BLAST",x,y,0.0,0.0);
        wiz[count]=createMobDryad(x,y);
        SettingsMonsterCommon(wiz[count]);
        SetCallback(wiz[count],5,onVenusDeath);
    }
}

void encounterVenus()
{
    ObjectOff(SELF);
    PushTimerQueue(24, venusGuardian(), venusTransform);
}

void startMovingGenerator()
{
    ObjectOff(SELF);
    Move(Object("mg1"),197);
    Move(Object("mg2"),197);
    Move(Object("mg3"),197);
    Move(Object("mg4"),195);
    Move(Object("mg5"),195);
    Move(Object("mg6"),195);
    initialPlaceGenerators(8);
    initialPlaceVenus();
    ObjectOn(Object("mgsen1"));
    ObjectOn(Object("mgsen2"));
    ObjectOn(Object("mgsen3"));
}

void dun1MonDeathCount(int *get, int *set)
{
    int count;

    if (get)
        get[0]=count;
    if (set)
        count=set[0];
}

void spawnDun1MonDeath()
{
    int count;

    dun1MonDeathCount(&count, NULLPTR);
    if (--count>0)
    {
        dun1MonDeathCount(NULLPTR, &count);
        return;
    }
    ObjectOn(Object("dmpass1"));
    UniPrintToAll("벽에 있는 버튼에 전원이 공급되었습니다, 이제 버튼을 누르십시오");
}

#define DUN_MON_SUB_UNIT 0
#define DUN_MON_MOB_FUNCTION 1
#define DUN_MON_DEATH_FUNCTION 2
#define DUN_MON_MAX 3

void spawnDunMonsCommon(int *p)
{
    int sub=p[DUN_MON_SUB_UNIT];
    int count = GetDirection(sub);

    if (count)
    {
        PushTimerQueue(1, p, spawnDunMonsCommon);
        LookWithAngle(sub, count-1);
        int mob =invokeMobCreator(p[DUN_MON_MOB_FUNCTION], GetObjectX(sub),GetObjectY(sub));
        SettingsMonsterCommon(mob);
        SetCallback( mob, 5, p[DUN_MON_DEATH_FUNCTION] );
        return;
    }
    FreeSmartMemEx(p);
}

// #define GROUP_dungMonWalls1 13

void openDun1()
{
    ObjectOff(SELF);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, LocationX(198),LocationY(198));
    int count=32;
    int *p;

    AllocSmartMemEx(DUN_MON_MAX*4,&p);
    p[DUN_MON_SUB_UNIT]=sub;
    p[DUN_MON_MOB_FUNCTION]=createMobOgreLord;
    p[DUN_MON_DEATH_FUNCTION]=spawnDun1MonDeath;
    PushTimerQueue(1,p,spawnDunMonsCommon);
    LookWithAngle(sub,count);
    dun1MonDeathCount(NULLPTR, &count);
    WallGroupSetting(WallGroup("dunmirMobWalls1"), WALL_GROUP_ACTION_OPEN);
}

void dun2MonDeathCount(int *get, int *set)
{
    int count;

    if (get)
        get[0]=count;
    if (set)
        count=set[0];
}

void spawnDun2MonDeath()
{
    int count;

    dun2MonDeathCount(&count, NULLPTR);
    if (--count>0)
    {
        dun2MonDeathCount(NULLPTR, &count);
        return;
    }
    ObjectOn(Object("dmpass2"));
    UniPrintToAll("벽에 있는 버튼에 전원이 공급되었습니다, 이제 버튼을 누르십시오");
}

// #define GROUP_dungMonWalls2 14

void openDun2()
{
    ObjectOff(SELF);
    WallGroupSetting(WallGroup("secondDunmirMobWalls"),WALL_GROUP_ACTION_OPEN);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, LocationX(199),LocationY(199));
    int count=24;
    
    int *p;

    AllocSmartMemEx(DUN_MON_MAX*4,&p);
    p[DUN_MON_SUB_UNIT]=sub;
    p[DUN_MON_MOB_FUNCTION]=createMobBeholder;
    p[DUN_MON_DEATH_FUNCTION]=spawnDun2MonDeath;
    PushTimerQueue(1,p,spawnDunMonsCommon);

    LookWithAngle(sub,count);
    dun2MonDeathCount(NULLPTR, &count);
}

void dun3MonDeathCount(int *get, int *set)
{
    int count;

    if (get)
        get[0]=count;
    if (set)
        count=set[0];
}

void spawnDun3MonDeath()
{
    int count;

    dun3MonDeathCount(&count, NULLPTR);
    if (--count>0)
    {
        dun3MonDeathCount(NULLPTR, &count);
        return;
    }
    ObjectOn(Object("dmpass3"));
    UniPrintToAll("벽에 있는 버튼에 전원이 공급되었습니다, 이제 버튼을 누르십시오");
}

// #define GROUP_dungMonWalls3 15

void openDun3()
{
    ObjectOff(SELF);
    WallGroupSetting(WallGroup("thirdDunMobWalls"),WALL_GROUP_ACTION_OPEN);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, LocationX(200),LocationY(200));
    int count=19;
    
    int *p;

    AllocSmartMemEx(DUN_MON_MAX*4,&p);
    p[DUN_MON_SUB_UNIT]=sub;
    p[DUN_MON_MOB_FUNCTION]=createEmberDemon;
    p[DUN_MON_DEATH_FUNCTION]=spawnDun3MonDeath;
    PushTimerQueue(1,p,spawnDunMonsCommon);

    LookWithAngle(sub,count);
    dun3MonDeathCount(NULLPTR, &count);
}

// #define GROUP_dungMonWalls4 16

void openDunFinal()
{
    ObjectOff(SELF);
    WallGroupSetting(WallGroup("dunmirMobFinalWalls"),WALL_GROUP_ACTION_OPEN);
}

void deferredArrowOff(int arw)
{
    if (IsObjectOn(arw))
        ObjectOff(arw);
}

void arrowTrapOn(char *name, int count)
{
    char lbuff[64];
    int args[]={name,0,}, unit;

    while (--count>=0)
    {
        args[1]=count+1;
        NoxSprintfString(lbuff,"%s%d",args,sizeof(args));
        unit=Object(ReadStringAddressEx(lbuff));
        PushTimerQueue(1,unit,deferredArrowOff);
        ObjectOn(unit);
    }
}

void ActivateMidArrows()
{
    arrowTrapOn(StringUtilGetScriptStringPtr("midArrow"), 6);
}

void ActivateArwT1()
{
    arrowTrapOn(StringUtilGetScriptStringPtr("arwtrp1"), 5);
}

void ActivateArwT2()
{
    arrowTrapOn(StringUtilGetScriptStringPtr("arwtrp2"), 5);
}
void ActivateArwT3()
{
    arrowTrapOn(StringUtilGetScriptStringPtr("arwtrp3"), 5);
}

void onBlackManDeath()
{
    int count;

    if (++count==3)
    {
        WallGroupSetting(WallGroup("pushEnterWalls"),WALL_GROUP_ACTION_OPEN);
        UniPrintToAll("아래쪽에 벽이 열렸는데요..?");
    }
    removeEquipments(GetTrigger());
    UniChatMessage(SELF, "앙~ 죽어버렸네!", 90);
}

void stoneArrayStorage(int *set, int **get)
{
    int *p;

    if (set) p=set;
    if (get) get[0]=p;
}
void redWizardDeathCount(int *set, int *get)
{
    int count;

    if (set) count=set[0];
    if (get) get[0]=count;
}

// #define GROUP_blackManWalls 18

void onRedWizardDeath()
{
    //pushEnterWalls
    int count=0;

    redWizardDeathCount(NULLPTR, &count);
    if (--count>0)
    {
        redWizardDeathCount(&count,NULLPTR);
        return;
    }

    WallGroupSetting(WallGroup("threeBlackmanWalls"),WALL_GROUP_ACTION_OPEN);
    int bkman1=Object("bkman1");
    int bkman2=Object("bkman2");
    int bkman3=Object("bkman3");
    ObjectOn(bkman1);
    ObjectOn(bkman2);
    ObjectOn(bkman3);
    SetCallback(bkman1,5,onBlackManDeath);
    SetCallback(bkman2,5,onBlackManDeath);
    SetCallback(bkman3,5,onBlackManDeath);
    CreateObjectById(OBJ_LEVEL_UP,LocationX(218),LocationY(218));
    CreateObjectById(OBJ_LEVEL_UP,LocationX(219),LocationY(219));
    UniPrintToAll("!!전설의 껌둥쓰 3인이 등장했다!!전설의 껌둥쓰 3인이 등장했다!!전설의 껌둥쓰 3인이 등장했다!!전설의 껌둥쓰 3인이 등장했다!!");
}

void transformRedWizard()
{
    int dup;

    if (GetAnswer(SELF)^1) return;
    if (dup) return;

    dup=TRUE;
    int *stones, reader=0;
    stoneArrayStorage(0,&stones);
    if (stones)
    {
        while (stones[reader])
        {
            float x=GetObjectX(stones[reader]),y=GetObjectY(stones[reader]);
            Delete(stones[reader++]);
            int wiz =createMobRedWizard(x,y);
            SettingsMonsterCommon(wiz);
            SetCallback(wiz , 5, onRedWizardDeath);
            Effect("SMOKE_BLAST",x,y,0.0,0.0);
            redWizardDeathCount(&reader, NULLPTR);
        }
    }
}

void enterPlaceB()
{
    ObjectOff(SELF);
    int stones[]={
    CreateObjectById(OBJ_MOVABLE_STATUE_2_A,LocationX(209),LocationY(209)),
    CreateObjectById(OBJ_MOVABLE_STATUE_2_H,LocationX(210),LocationY(210)),
    CreateObjectById(OBJ_MOVABLE_STATUE_2_G,LocationX(211),LocationY(211)),
    CreateObjectById(OBJ_MOVABLE_STATUE_2_F,LocationX(212),LocationY(212)),
    CreateObjectById(OBJ_MOVABLE_STATUE_2_E,LocationX(213),LocationY(213)),
    CreateObjectById(OBJ_MOVABLE_STATUE_2_D,LocationX(214),LocationY(214)),
    CreateObjectById(OBJ_MOVABLE_STATUE_2_C,LocationX(215),LocationY(215)),
    CreateObjectById(OBJ_MOVABLE_STATUE_2_B,LocationX(216),LocationY(216)),
    0,
    };
    stoneArrayStorage(stones,0);
    int interact = DummyUnitCreateById(OBJ_WILL_O_WISP,LocationX(217),LocationY(217));

    SetDialog(interact,"YESNO",StartWispTalk,transformRedWizard);
    StoryPic(interact, "WolfPic");
}

void dunmirPusherCounter(int *get, int set)
{
    int counter;

    if (get)
    {
        get[0]=counter;
        return;
    }
    counter=set;
}

void dunmirPusherCounterLoop(int bl)
{
    int counter;

    dunmirPusherCounter(&counter,0);
    if (counter>0)
    {
        PushTimerQueue(1,bl,dunmirPusherCounterLoop);
        dunmirPusherCounter(NULLPTR,counter-1);
        return;
    }
    PlaySoundAround(bl,SOUND_CreatureCageAppears);
}

void GoDunmirPusher()
{
    int counter;

    dunmirPusherCounter(&counter,0);
    if (counter)
        return;

    int bls[]={Object("dnPusher1"),Object("dnPusher2")};

    Move(bls[0], 223);
    Move(bls[1], 222);
    PlaySoundAround(OTHER,SOUND_Clank2);
    PlaySoundAround(bls[0],SOUND_SpikeBlockMove);
    dunmirPusherCounter(NULLPTR, 120);
    PushTimerQueue(1,bls[0],dunmirPusherCounterLoop);
}

void removePushWalls()
{
    ObjectOff(SELF);
    WallOpen(Wall(163,207));
    WallOpen(Wall(162,208));
    WallOpen(Wall(161,209));
    UniPrint(OTHER, "주변 어딘가에서 비밀벽이 열렸습니다~");
}
void onHorrenDeath()
{
    Raise( CreateObjectById(OBJ_RUBY_KEY,GetObjectX(SELF),GetObjectY(SELF)), 200.0);
    PlaySoundAround(SELF, SOUND_KeyDrop);
}

void deferredSummonHorr(int sub)
{
    float x=GetObjectX(sub),y=GetObjectY(sub);
    int m=CreateHorrendousMonster(x,y);

    SettingsMonsterCommon(m);
    SetCallback(m,5,onHorrenDeath);
    DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE,x,y),12);
    Delete(sub);
}

void onCoffinDestoryed()
{
    float x=GetObjectX(SELF),y=GetObjectY(SELF);

    Delete(SELF);
    PlaySoundAround( CreateObjectById(OBJ_COFFIN_BREAKING_1,x,y), SOUND_CoffinBreak);
    PushTimerQueue(3, CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y),deferredSummonHorr);
}

void placeCoffin(short locationId)
{
    int cof=CreateObjectById(OBJ_COFFIN_1, LocationX(locationId),LocationY(locationId));

    SetUnitMaxHealth(cof, 600);
    SetUnitCallbackOnDeath(cof,onCoffinDestoryed);
}

void enterPlaceC()
{
    ObjectOff(SELF);
    short locations[]={
        229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,
    };
    int sz=sizeof(locations);
    while(--sz>=0)
        placeCoffin(locations[sz]);
}

void wakeGolemNop()
{
    return;
}

void wakeGolem()
{
    UniChatMessage(SELF,"감히!? 나를 건들였어? 이제, 네는 뒈졌어", 90);
    AggressionLevel(SELF,1.0);
    SetCallback(SELF,7,wakeGolemNop);
    SetCallback(SELF,9,wakeGolemNop);
}

void passiveGolemSingle(short locationId, int dir)
{
    int g=createMobStoneGiant(LocationX(locationId),LocationY(locationId));

    SettingsMonsterCommon(g);
    LookWithAngle(g,dir);
    AggressionLevel(g,0.0);
    SetCallback(g,9,wakeGolem);
    SetCallback(g,7,wakeGolem);
}

void placePassiveGolem()
{
    //13
    int count=13;
    while (--count>=0)
    {
        passiveGolemSingle(246,32);
        passiveGolemSingle(247,160);
        TeleportLocationVector(246,-46.0,46.0);
        TeleportLocationVector(247,-46.0,46.0);
    }
}

int necroWalls()
{
    int *pWalls;

    if (pWalls==NULLPTR)
    {
        int walls[]={
            Wall(79,41),
            Wall(80,42),
            Wall(81,43),
            Wall(82,44),
            Wall(83,45),
            Wall(84,46),
            Wall(85,47),
            Wall(86,48),
            Wall(87,49),
            Wall(88,50),
            Wall(89,51),
            Wall(90,52),
            Wall(91,53),
            Wall(92,54),
            Wall(93,55),
            Wall(94,56),
            Wall(95,57),
            Wall(96,58),
            Wall(97,59),
            Wall(98,60),
            Wall(99,59),
            Wall(100,58),
            Wall(101,57),
            Wall(102,56),
            Wall(103,55),
            Wall(104,54),
            0,
        };
        pWalls=walls;
    }
    return pWalls;
}

#define ALIGN_GLYPH_UNIT 0
#define ALIGN_GLYPH_X 1
#define ALIGN_GLYPH_Y 2
#define ALIGN_GLYPH_MAX 3

void alignmentGlyph(int *p)
{
    float *pxy=&p[ALIGN_GLYPH_X];
    int gly=p[ALIGN_GLYPH_UNIT];

    if (ToInt( GetObjectX(gly)))
        MoveObject(gly,pxy[0],pxy[1]);
    FreeSmartMemEx(p);
}

int placeGlyph(float x, float y, string spell1, string spell2, string spell3)
{
    int bo=CreateObjectById(OBJ_BOMBER,x,y);
    int *pArg;

    AllocSmartMemEx(ALIGN_GLYPH_MAX*4, &pArg);
    TrapSpells(bo,spell1,spell2,spell3);
    int gly=GetLastItem(bo);
    PushTimerQueue(2,pArg,alignmentGlyph);
    pArg[0]=gly;
    pArg[1]=x;
    pArg[2]=y;
    Drop(bo,gly);
    Delete(bo);
    return gly;
}

void enterPlaceD()
{
    ObjectOff(SELF);
    placePassiveGolem();
    int *pWalls=necroWalls();

    while (pWalls[0])
    {
        WallOpen(pWalls[0]);
        pWalls=&pWalls[1];
    }
    placeGlyph(LocationX(261),LocationY(261),"SPELL_CLEANSING_FLAME", "null", "null");
    placeGlyph(LocationX(262),LocationY(262),"SPELL_METEOR", "SPELL_BURN", "null");
    placeGlyph(LocationX(263),LocationY(263),"SPELL_CLEANSING_FLAME", "null", "null");
}

void finalWallLoop(int *pWalls)
{
    if (pWalls[0])
    {
        WallClose(pWalls[0]);
        PushTimerQueue(4,&pWalls[1],finalWallLoop);
    }
}

void startWallsAreClosing()
{
    int *pWalls;

    ObjectOff(SELF);
    if (pWalls) return;
    pWalls=necroWalls();
    PushTimerQueue(1,pWalls, finalWallLoop);
}

void deferredPushing(int unit)
{
    if (CurrentHealth(unit))
        PushObjectTo(unit,UnitAngleCos(unit,-8.0),UnitAngleSin(unit,-8.0));
}

void onSlideCollide()
{
    if (CurrentHealth(OTHER))
    {
        Effect("DAMAGE_POOF",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
        PushTimerQueue(1,GetCaller(),deferredPushing);
    }
}

void placeSlide(short locationId)
{
    int sl=CreateObjectById(OBJ_WIZARD,LocationX(locationId),LocationY(locationId));

    Damage(sl,0,9999,DAMAGE_TYPE_PLASMA);
    SetCallback(sl,9,onSlideCollide);
}

void initialPlaceSlide(short locationId, int count)
{
    while (--count>=0)
    {
        placeSlide(locationId);
        TeleportLocationVector(locationId, 23.0, -23.0);
    }
}

void enterPlaceE()
{
    ObjectOff(SELF);
    initialPlaceSlide(249,20);
    initialPlaceSlide(250,20);
    initialPlaceSlide(251,20);

    initialPlaceSlide(252,20);
    initialPlaceSlide(253,20);
    initialPlaceSlide(254,20);

    initialPlaceSlide(255,20);
    initialPlaceSlide(256,20);
    initialPlaceSlide(257,20);
}

void summonNecroLoop(int sub)
{
    int count=GetDirection(sub);

    if (ToInt(GetObjectX(sub)))
    {
        if (count)
        {
            PushTimerQueue(3,sub,summonNecroLoop);
            LookWithAngle(sub,count-1);
            float x=GetObjectX(sub),y=GetObjectY(sub);

            UniChatMessage( createMobNecromancer(x,y), "흐하하하하하하핳", 60);
            Effect("SMOKE_BLAST",x,y,0.0,0.0);
            return;
        }
        Delete(sub);
    }
}

void startNecros()
{
    int dup;

    if (dup) return;
    dup=TRUE;
    int sub=CreateObjectById(OBJ_REWARD_MARKER_PLUS,GetObjectX(SELF),GetObjectY(SELF));

    LookWithAngle(sub,24);
    PushTimerQueue(1, sub,summonNecroLoop);
}

void onTransformWalls(int wall)
{
    float xy[2];

    WallCoorToUnitCoor(wall,xy);
    int mob = createMobLichLord(xy[0],xy[1]);
    SettingsMonsterCommon( mob );    
    LookWithAngle(mob,96);
    Effect("SMOKE_BLAST",xy[0],xy[1],0.0,0.0);
    UniChatMessage(mob, "크캬캬캬캬캬컄캬캬캬컄캬캬캬크캬크캬캬캬컄캬캬캬캬캬캬캬캬컄ㅋ캬캬캬컄ㅋ컄", 90);
    WallOpen(wall);
}

void enterPlaceF()
{
    ObjectOff(SELF);
    RegistWallGroupCallback(onTransformWalls);
    WallGroupSetting(WallGroup("transformLichWalls"), WALL_GROUP_ACTION_CALLBACK);
}
void OnPlayerEntryMapProc(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    if (pInfo==0x653a7c)
    {
        initDialog();
        initPopupMessages();
        InitializeCommonImage();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
