
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/networkEx.h"
#include "libs/fxeffect.h"
#include "libs/waypoint.h"
#include "libs/buff.h"

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectById(OBJ_HECUBAH, 5500.0, 100.0);
        Frozen(unit, TRUE);
    }
    return unit;
}

void ManaBombCancelFx(int sUnit)
{
    int caster = CreateObjectById(OBJ_IMAGINARY_CASTER, GetObjectX(sUnit), GetObjectY(sUnit));

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int GetPlayerUnit(int pIndex){
    int *pInfo=0x62f9e0 + (0x12dc*pIndex);

    if (!pInfo[0])
        return 0;

    int *ptr=pInfo[0];
    return ptr[11];
}

void CustomMeleeAttackCode(char *bin, int fn)
{
    char code[]={
        0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
        0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
        0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
        0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
    };
    
    int *pStrike = &bin[0xEC];

    pStrike[0] = code;
    int *hack = &bin[0x94];

    hack[15]=fn;
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

void DisableObject(int unit)
{
    if (IsObjectOn(unit))
        ObjectOff(unit);
}

int ImportSetPlayerActionFunc()
{
    int *codestream;

    if (codestream == NULLPTR)
    {
        int codebyte[] = {0x4FA02068, 0x72506800, 0x14FF0050, 0x54FF5024, 0xFF500424,
            0x830C2454, 0x90C310C4, 0x9090C390, 0xC03108C4, 0x909090C3};
        
        codestream = codebyte;
    }
    return codestream;
}

void SetPlayerActionFunc(int pUnit, int val)
{
    if (!IsPlayerUnit(pUnit))
        return;

    int *ptr = UnitToPtr(pUnit);

    if (ptr == NULLPTR)
        return;
    int *btbase = 0x5c308c;
    int *target = btbase[0x5a];
    int prev = target;

    target[0] = ImportSetPlayerActionFunc();
    Unused5a(ptr, val);
    target[0] = prev;
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

int DrawMagicIcon(float x, float y, short unitId)
{
    int unit = CreateObjectById(OBJ_AIRSHIP_BASKET_SHADOW, x, y);
    int *ptr = UnitToPtr(unit);

    ptr[1]= unitId;
    return unit;
}

void DrawGeometryRing(short orbType, float x, float y)
{
    int u=60;
    float speed = 2.3;

    while (--u>=0)
        LinearOrbMove(CreateObjectById(orbType, x,y), MathSine((u * 6) + 90, -12.0), MathSine(u * 6, -12.0), speed, 10);
}

void GreenLightningEffect(float x1, float y1, float x2, float y2)
{
    GreenLightningFx(FloatToInt(x1), FloatToInt(y1), FloatToInt(x2), FloatToInt(y2), 25);
}
void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}

void RiseBlueSpark(float x, float y)
{
    int fx = CreateObjectById(OBJ_TELEPORT_WAKE, x, y);
    Frozen(fx, TRUE);
    UnitNoCollide(fx);
}

void onCollideInvisbleTelepo(){
    if (CurrentHealth(OTHER))
    {
        int dest=GetTrigger()+1;

        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
        MoveObject(OTHER, GetObjectX(dest),GetObjectY(dest));
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
    }
}

void DeleteInvisibleTelpo(int t){
    if (GetUnitThingID(t) == OBJ_STORM_CLOUD)
    {
        if (GetUnitThingID(t+2)==OBJ_TELEPORT_WAKE){
        Delete(t++);
        Delete(t++);
        Delete(t++);
        }
    }
}

int PlacingInvisibleTeleporting(short srcLoc, short destLoc){
    int s=CreateObjectById(OBJ_STORM_CLOUD, LocationX(srcLoc),LocationY(srcLoc));

    SetUnitCallbackOnCollide(s, onCollideInvisbleTelepo);
    SetUnitFlags(s, GetUnitFlags(s) ^ (UNIT_FLAG_NO_COLLIDE|UNIT_FLAG_NO_PUSH_CHARACTERS));
    Frozen(s, TRUE);

    DrawMagicIcon(LocationX(destLoc),LocationY(destLoc),OBJ_SPELL_ICONS);
    RiseBlueSpark(GetObjectX(s),GetObjectY(s));
    return s;
}

void CreateVoicesetInstance(int *pDest, string voiceName)
{
    if (!pDest)
        return;

    int *pAlloc;

    AllocSmartMemEx(80, &pAlloc);
    pDest[0] = pAlloc;
    NoxDwordMemset(pAlloc, 20, 0);
    int length = StringUtilGetLength(StringUtilGetScriptStringPtr( voiceName) );
    char *pName;

    AllocSmartMemEx(++length, &pName);
    StringUtilCopy(StringUtilGetScriptStringPtr(voiceName), pName);
    pAlloc[0] = pName;
}

#define VOICEOFFSET_ON_TALKABLE 1  //말걸때
#define VOICEOFFSET_ON_IDLE 4   //가만히 있을 때(NPC 유닛이 기침하거나 한숨쉴 때 여기를 참조합니다)
#define VOICEOFFSET_ON_ATTACKINIT 6  //공격직전?으로 알려짐
#define VOICEOFFSET_ON_RETREAT 13   //후퇴 시
#define VOICEOFFSET_ON_DIE 15   //죽을 때
#define VOICEOFFSET_ON_HURT 16   //피격 시
#define VOICEOFFSET_ON_RECOGNIZE 17   //적군 발견
#define VOICEOFFSET_ON_MOVE 18   //발자국 소리

void ModificationVoiceData(int *pVoice, int voiceOffsetMacro, int soundId)
{
    if (!pVoice)
        return;

    pVoice[voiceOffsetMacro] = soundId;
}

void ChangeUnitCustomVoice(int unit, int *pVoice)
{
    int *uptr = UnitToPtr(unit);
    int *uc = uptr[187];

    uc[122] = pVoice;
}
void RemoveNPCEquipmentsByCount(int npc, int count, int uClass)
{
	int inven = npc + 2;
	
    if (!uClass)
        uClass=UNIT_CLASS_ARMOR|UNIT_CLASS_WEAPON;
    while (--count>=0){
        if (GetUnitClass(inven) & uClass)
            Delete(inven);
        inven += 2;
    }
}
int InvincibleInventoryItem(int unit)
{
    int inv = GetLastItem(unit), count = 0;

    while (inv)
    {
        if (UnitCheckEnchant(inv, GetLShift(11)))
            1;
        else
        {
            count ++;
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}
void SetNPCAntiFumble(int npc)
{
    int inv = GetLastItem(npc);

    while (inv)
    {
        if (GetUnitClass(inv) & UNIT_CLASS_WEAPON)
        {
            int *ptr=UnitToPtr(inv);
            ptr[178]=0x408fdb;
        }
        inv = GetPreviousItem(inv);
    }
}
int MakePoisoned(int victim, int attacker)
{
    char *pcode;

    if (!pcode)
    {
        char code[]={0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
            0xB8, 0x90, 0x96, 0x54, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x50, 0xB8, 0x30, 
            0x72, 0x50, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3, 0x90, 0x90};
        pcode=code;
    }
    int params[]={UnitToPtr(victim), UnitToPtr(attacker)};
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=pcode;
    int ret = Unused5e(params);
    pExec[0x5e]=pOld;
    return ret;
}
