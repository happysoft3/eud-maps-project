
#include "r231103_utils.h"
#include "r231103_readable.h"
#include "libs/queueTimer.h"
#include "libs/sound_define.h"
#include "libs/fxeffect.h"
#include "libs/buff.h"
#include "libs/waypoint.h"
#include "libs/game_flags.h"
#include "libs/printutil.h"

#define TRANSPORT_SUB 0
#define TRANSPORT_COUNTER 1
#define TRANSPORT_CUSTOMER 2
#define TRANSPORT_EFFECT 3
#define TRANSPORT_MAX 4

void onTransportProcedure(int *pTransp)
{
    int owner = pTransp[TRANSPORT_CUSTOMER];

    if (CurrentHealth(owner))
    {
        int count = pTransp[TRANSPORT_COUNTER];

        if (count)
        {
            int eff=pTransp[TRANSPORT_EFFECT];

            if (DistanceUnitToUnit(owner, eff) < 23.0)
            {
                pTransp[TRANSPORT_COUNTER]-=1;
                PushTimerQueue(1, pTransp, onTransportProcedure);
                return;
            }
        }
        else
        {
            Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            int destination = pTransp[TRANSPORT_SUB]+1;

            MoveObject(owner, GetObjectX(destination), GetObjectY(destination));
            PlaySoundAround(owner, SOUND_BlindOff);
            Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
        }
        EnchantOff(owner, "ENCHANT_BURNING");
    }
    Delete(pTransp[TRANSPORT_EFFECT]);
    FreeSmartMemEx(pTransp);
}

void onTransportCollide()
{
    if (CurrentHealth(OTHER))
    {
        if (!UnitCheckEnchant(OTHER, GetLShift(ENCHANT_BURNING)))
        {
            int c;
            AllocSmartMemEx(TRANSPORT_MAX*4, &c);
            int *pTransp=c;
            pTransp[TRANSPORT_SUB]=GetTrigger();
            pTransp[TRANSPORT_COUNTER] = 48;
            pTransp[TRANSPORT_CUSTOMER]=GetCaller();
            pTransp[TRANSPORT_EFFECT]=CreateObjectAt("VortexSource", GetObjectX(SELF), GetObjectY(SELF));
            PushTimerQueue(1, pTransp, onTransportProcedure);
            GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
            Enchant(OTHER, "ENCHANT_BURNING", 4.0);
        }
    }
}

#undef TRANSPORT_SUB
#undef TRANSPORT_COUNTER
#undef TRANSPORT_CUSTOMER
#undef TRANSPORT_EFFECT
#undef TRANSPORT_MAX

int dispositionTransport(int srcLocation, int destLocation)
{
    int tp=CreateObject("WeirdlingBeast", srcLocation);

    CreateObject("InvisibleLightBlueLow", destLocation);
    UnitNoCollide(CreateObjectAt("TeleportWake", GetObjectX(tp), GetObjectY(tp)));
    Frozen(tp+2, TRUE);
    SetCallback(tp, 9, onTransportCollide);
    Damage(tp, 0, MaxHealth(tp)+1, -1);
    return tp;
}

#define EXIT_LOCATION 38
void tryExitPickup()
{
    if (IsPlayerUnit(OTHER))
    {
        Effect("SMOKE_BLAST", GetObjectX(OTHER),GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER,LocationX(EXIT_LOCATION),LocationY(EXIT_LOCATION));
        Effect("TELEPORT", GetObjectX(OTHER),GetObjectY(OTHER), 0.0, 0.0);
        UniPrint(OTHER, "필드에서 나갑니다");
    }
}

void putExit(int locationId)
{
    int e=CreateObjectById(OBJ_GREEN_ORB, LocationX(locationId),LocationY(locationId));

    Frozen(e, TRUE);
    SetUnitFlags(e,GetUnitFlags(e)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetUnitCallbackOnPickup(e,tryExitPickup);
}

void InitializeSubPart()
{
    dispositionTransport(14,16);
    dispositionTransport(22,18);
    dispositionTransport(23,19);
    dispositionTransport(24,17);
    dispositionTransport(25,21);
    dispositionTransport(26,20);
    dispositionTransport(30,31);
    dispositionTransport(32,33);

    DummyUnitCreateById(OBJ_FLYING_GOLEM, LocationX(34), LocationY(34));
    LookWithAngle(
         DummyUnitCreateById(OBJ_FLYING_GOLEM, LocationX(35), LocationY(35)), 128);
    DummyUnitCreateById(OBJ_SMALL_SPIDER, LocationX(36), LocationY(36));
    LookWithAngle( DummyUnitCreateById(OBJ_SMALL_SPIDER, LocationX(37), LocationY(37)), 128);
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    DummyUnitCreateById(OBJ_DEMON, LocationX(44), LocationY(44));
    LookWithAngle( DummyUnitCreateById(OBJ_DEMON, LocationX(45), LocationY(45)), 128);
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);

    putExit(39);
    putExit(40);
    putExit(41);
    putExit(42);
    putExit(43);
    InitializeReadables();
    CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT, LocationX(49),LocationY(49));
}

