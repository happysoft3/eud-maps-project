
#include "fkjapan_utils.h"
#include "fkjapan_reward.h"
#include "fkjapan_desc.h"
#include "libs/voiceList.h"

void DefaultMobSight()
{
    return;
}

void DefaultMobInitSight()
{
    AggressionLevel(SELF, 1.0);
    SetCallback(SELF, 3, DefaultMobSight);
}

void DefaultMobDead()
{
    CreateRandomItemCommon(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF)));
    DeleteObjectTimer(SELF, 90);
}

void PlantMobInitSight()
{
    AggressionLevel(SELF, 1.0);
    SetCallback(SELF, 3, DefaultMobSight);
    SetCallback(SELF, 7, FieldMobHurtHandler);
    UniChatMessage(SELF, MessageDesc(13), 180);
}

void FieldMobHurtHandler()
{
    int propertyLv;

    if (GetCaller())
    {
        if (UnitCheckEnchant(SELF, GetLShift(5)))
        {
            EnchantOff(SELF, EnchantList(5));
            propertyLv = GetWeaponEnchantLevel(OTHER, 0x5BA1BC);
            if (propertyLv)
                UnitSetEnchantTime(SELF, 4, 15 + (propertyLv * 5));
        }
        else if (UnitCheckEnchant(SELF, GetLShift(3)))
        {
            EnchantOff(SELF, EnchantList(3));
            propertyLv = GetWeaponEnchantLevel(OTHER, 0x5BA2DC);
            if (propertyLv)
                UnitSetEnchantTime(SELF, 11, 10 + (propertyLv * 4));
        }
    }
    else
    {
        if (IsPoisonedUnit(SELF))
        {
            Damage(SELF, 0, IsPoisonedUnit(SELF) + 1, 5);
            DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 15);
        }
    }
}

void DefaultListenEnemy()
{
    return;
}

void HearAroundEnemy()
{
    int target = LastParentUnit(OTHER);

    if (IsVisibleTo(SELF, target) || IsVisibleTo(target, SELF))
    {
        Attack(SELF, target);
        AggressionLevel(SELF, 1.0);
        SetCallback(SELF, 10, DefaultListenEnemy);
    }
}

void commonMonsterSettings(int unit){
    SetCallback(unit, 3, DefaultMobInitSight);
    SetCallback(unit, 5, DefaultMobDead);
    SetCallback(unit, 7, FieldMobHurtHandler);
    SetCallback(unit, 10, HearAroundEnemy);
    SetUnitScanRange(unit, 450.0);
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
}

int WoundedConjurerBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1130653028; arr[2] = 1969909359; arr[3] = 7497074; arr[17] = 260; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1106247680; arr[29] = 40; arr[30] = 1117782016; arr[31] = 4; arr[32] = 13; 
		arr[33] = 21; arr[59] = 5542784; arr[60] = 2271; arr[61] = 46912768; 
	pArr = arr;
	return pArr;
}

void WoundedConjurerSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = WoundedConjurerBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int SummonDefaultMob(string name, int wp)
{
    int unit = CreateObject(name, wp);

    CheckMonsterThing(unit);
    commonMonsterSettings(unit);
    return unit;
}

int SummonBranchOni(short locationId){
	int mob=CreateObjectById(OBJ_WOUNDED_CONJURER, LocationX(locationId),LocationY(locationId));

    WoundedConjurerSubProcess(mob);
    commonMonsterSettings(mob);
    SetUnitVoice(mob,MONSTER_VOICE_CarnivorousPlant);
    return mob;
}

int SummonHecubah(int wp)
{
    int unit = CreateObject("Hecubah", wp);
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 750);
    SetCallback(unit, 3, PlantMobInitSight);
    SetCallback(unit, 5, DefaultMobDead);
    SetCallback(unit, 7, FieldMobHurtHandler);
    SetCallback(unit, 10, HearAroundEnemy);
    AggressionLevel(unit, 0.0);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(450.0));
        SetMemory(uec + 0x5a8, 0x0f0000);
        SetMemory(uec + 0x5b0, 0x0f0000);
        SetMemory(uec + 0x5c0, 0x0f0000);
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_CHAIN_LIGHTNING"), 0x40000000);
    }
    return unit;
}

int SummonNecromancer(int wp)
{
    int unit = CreateObject("Necromancer", wp);
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 350);
    SetCallback(unit, 5, DefaultMobDead);
    SetCallback(unit, 3, DefaultMobInitSight);
    SetCallback(unit, 10, HearAroundEnemy);
    AggressionLevel(unit, 0.0);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(450.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_LIGHTNING"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x8000000);
    }
    SetUnitVoice(unit, MONSTER_VOICE__Maiden1);
    return unit;
}
