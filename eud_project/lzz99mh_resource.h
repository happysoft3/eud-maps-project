
#include "libs/imageutil.h"

#define IMAGE_VECTOR_SIZE 1024

void ImageResourceKodi1()
{ }
void ImageResourceKodi2()
{ }
void ImageResourceKodi3()
{ }
void ImageResourceAmulet(){}

void ImageTelevisionGirl() { }
void ImageProtraitTvGirl(){}
void ImageKoniMark(){}
void ImageTeacher(){}

void introduceCustomImage(int *pImgVector)
{
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138235);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138236);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138237);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138238);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138239);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138240);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138241);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138242);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138243);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138244);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138245);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138246);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138247);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138248);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138249);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138250);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138251);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138252);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138253);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi1)+4, 138254);

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138256);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138257);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138258);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138259);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138260);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138261);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138262);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138263);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138264);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138265);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138266);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138267);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138268);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138269);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138270);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138271);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138272);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138273);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138274);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi2)+4, 138275);

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138276);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138277);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138278);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138279);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138280);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138281);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138282);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138283);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138284);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138285);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138286);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138287);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138288);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138289);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138290);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138291);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138292);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138293);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138294);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceKodi3)+4, 138295);

    AppendImageFrame(pImgVector, GetScrCodeField(ImageResourceAmulet) + 4, 112960);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageTelevisionGirl) + 4, 17695);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageProtraitTvGirl) + 4, 14868);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageKoniMark) + 4, 132293);
    AppendImageFrame(pImgVector, GetScrCodeField(ImageTeacher) + 4, 115510);
}

void InitializeCommonImage()
{
    int imgVector = CreateImageVector(IMAGE_VECTOR_SIZE);

    // InitializeImageHandlerProcedure(imgVector);
    introduceCustomImage(imgVector);
    DoImageDataExchange(imgVector);
}

