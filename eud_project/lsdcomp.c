
#include "lsdcomp_gvar.h"
#include "lsdcomp_player.h"
#include "lsdcomp_input.h"
#include "lsdcomp_item.h"
#include "lsdcomp_sub.h"
#include "lsdcomp_mob.h"
#include "lsdcomp_utils.h"
#include "lsdcomp_initscan.h"
#include "lsdcomp_resource.h"
#include "libs/fxeffect.h"
#include "libs/playerinfo.h"
#include "libs/observer.h"
#include "libs/reaction.h"
#include "libs/coopteam.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"
#include "libs/buff.h"
#include "libs/potionex.h"
#include "libs/chatevent.h"

int m_itemMaster;
int m_initScanHash;

#define MAX_LEVEL 30

static int GetItemMaster() //virtual
{
    return m_itemMaster;
}

int m_clientNetIdNumber;

static int IsPickupableItem(int item) //virtual
{
    return GetOwner(item)==m_itemMaster;
}

int m_player[MAX_PLAYER_COUNT];
int m_creature[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{
    return m_player[pIndex];
}

static void SetPlayer(int pIndex, int user) //virtual
{
    m_player[pIndex] = user;
}

static int GetCreature(int pIndex) //virtual
{
    return m_creature[pIndex];
}

static void SetCreature(int pIndex, int cre) //virtual
{
    m_creature[pIndex]=cre;
}

short m_statExp[MAX_PLAYER_COUNT];
char m_petLevel[MAX_PLAYER_COUNT];

static int GetCreatureStatExp(int pIndex) //virtual
{ return m_statExp[pIndex]; }

static void SetCreatureStatExp(int pIndex, int exp) //virtual
{
    m_statExp[pIndex]=exp;
}

static int GetPetLevel(int pIndex) //virtual
{ return m_petLevel[pIndex]; }

static void SetPetLevel(int pIndex, int lv) //virtual
{ m_petLevel[pIndex]=lv; }

int m_pFlags[MAX_PLAYER_COUNT];

static int GetPlayerFlags(int pIndex) //virtual
{
    return m_pFlags[pIndex];
}

static void SetPlayerFlags(int pIndex, int flags) //virtual
{
    m_pFlags[pIndex]=flags;
}

int m_respSpot[MAX_PLAYER_COUNT];

static int GetRespawnSpot(int pIndex) //virtual
{
    return m_respSpot[pIndex];
}

static void SetRespawnSpot(int pIndex, int spot) //virtual
{ m_respSpot[pIndex]=spot; }

int *m_pClientKeyState;

static int GetClientKeyState(int pIndex) //virtual
{ return m_pClientKeyState[pIndex]; }

static void SetClientKeyState(int pIndex, int value) //virtual
{ m_pClientKeyState[pIndex]=value; }

short m_levelBuffer[64];

static int GetLevelBuffer() //virtual
{
    return m_levelBuffer;
}

int m_dropCodePtr;

static int GetDropCode() //virtual
{
    return m_dropCodePtr;
}

static void SetDropCode(int p) //virtual
{ m_dropCodePtr=p; }

void MonsterLich(int unit)
{
    UnitLinkBinScript(unit, LichLordBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    UnitZeroFleeRange(unit);
}

void EnableObject(int unit)
{
    ObjectOn(unit);
}

int DecimalTable(int num)
{
    int arr[100];

    if (num < 0)
    {
        arr[0] = 2; arr[1] = 3; arr[2] = 5; arr[3] = 7; arr[4] = 11;
        arr[5] = 13; arr[6] = 17; arr[7] = 19; arr[8] = 23; arr[9] = 29;
        arr[10] = 31; arr[11] = 37; arr[12] = 41; arr[13] = 43; arr[14] = 47;
        arr[15] = 53; arr[16] = 59; arr[17] = 61; arr[18] = 67; arr[19] = 71;
        arr[20] = 73; arr[21] = 79; arr[22] = 83; arr[23] = 89; arr[24] = 97;
        arr[25] = 101; arr[26] = 103; arr[27] = 107; arr[28] = 109; arr[29] = 113;
        arr[30] = 127; arr[31] = 127; arr[32] = 131; arr[33] = 137; arr[34] = 139;
        arr[35] = 149; arr[36] = 151; arr[37] = 157; arr[38] = 163; arr[39] = 167;
        arr[40] = 173; arr[41] = 179; arr[42] = 181; arr[43] = 191; arr[44] = 193;
        arr[45] = 197; arr[46] = 199; arr[47] = 211; arr[48] = 223; arr[49] = 227;
        arr[50] = 229; arr[51] = 233; arr[52] = 239; arr[53] = 241; arr[54] = 251;
        arr[55] = 257; arr[56] = 263; arr[57] = 269; arr[58] = 271; arr[59] = 277;
        arr[60] = 281; arr[61] = 283; arr[62] = 293; arr[63] = 307; arr[64] = 311;
        arr[65] = 313; arr[66] = 317; arr[67] = 331; arr[68] = 337; arr[69] = 347;
        arr[70] = 349; arr[71] = 353; arr[72] = 359; arr[73] = 367; arr[74] = 373;
        arr[75] = 379; arr[76] = 383; arr[77] = 389; arr[78] = 397; arr[79] = 401;
        arr[80] = 409; arr[81] = 419; arr[82] = 421; arr[83] = 431; arr[84] = 433;
        arr[85] = 439; arr[86] = 443; arr[87] = 449; arr[88] = 457; arr[89] = 461;
        arr[90] = 463; arr[91] = 467; arr[92] = 479; arr[93] = 487; arr[94] = 491;
        arr[95] = 499; arr[96] = 503; arr[97] = 509; arr[98] = 521; arr[99] = 523;
        return 0;
    }
    return arr[num];
}

int NextLevelExpTable(int lv)
{
    int arr[MAX_LEVEL], i;

    if (lv < 0)
    {
        arr[0] = 15;
        for (i = 1 ; i < MAX_LEVEL ; i ++)
        {
            arr[i] = arr[i - 1] + (DecimalTable((i * 3) + 5) * (16/10));
        }
        return 0;
    }
    return arr[lv];
}

int CheckLevelUp(int pIndex)
{
    return (m_statExp[pIndex] >= NextLevelExpTable(GetPetLevel(pIndex)));
}

void LevelUpFx(int unit)
{
    DeleteObjectTimer(CreateObjectById(OBJ_LEVEL_UP, GetObjectX(unit), GetObjectY(unit)), 28);
    PlaySoundAround(unit, SOUND_LevelUp);
}

#define FIRST_SKILL_LEVEL 2

int LevelNotify(int lv)
{
    int arr[MAX_LEVEL];

    if (lv < 0)
    {
        arr[FIRST_SKILL_LEVEL] = NotifyHandler1; arr[8] = NotifyHandler2; arr[14] = NotifyHandler3;
        arr[19]=NotifyBonusSkill; //테스트
        return 0;
    }
    return arr[lv];
}

void NotifyHandlerNothing(int arg)
{
    return;
}

void NotifyHandler1(int pIndex)
{
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_FIRST_SKILL))
        return;
        
    PlayerClassSetFlag(pIndex, PLAYER_FLAG_FIRST_SKILL);
    UniPrint(GetPlayer(pIndex), "이제부터 첫번째 스킬을 사용할 수 있습니다! 사용하려면 A 키를 누르세요");
    UpdateCooldown(pIndex);
}

void NotifyHandler2(int pIndex)
{
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_SECOND_SKILL))
        return;
        
    PlayerClassSetFlag(pIndex, PLAYER_FLAG_SECOND_SKILL);
    UniPrint(GetPlayer(pIndex), "이제부터 두번째 스킬을 사용할 수 있습니다! 사용하려면 S 키를 누르세요");
    UpdateCooldown(pIndex);
}

void NotifyHandler3(int pIndex)
{
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_LAST_SKILL))
        return;
        
    PlayerClassSetFlag(pIndex, PLAYER_FLAG_LAST_SKILL);
    UniPrint(GetPlayer(pIndex), "이제부터 마지막 스킬을 사용할 수 있습니다! 사용하려면 D 키를 누르세요");
    UpdateCooldown(pIndex);
}

void NotifyBonusSkill(int pIndex)
{
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_BONUS_SKILL))
        return;
        
    PlayerClassSetFlag(pIndex, PLAYER_FLAG_BONUS_SKILL);
    UniPrint(GetPlayer(pIndex), "이제부터 보너스 스킬을 사용할 수 있습니다! 사용하려면 F 키를 누르세요");
    UpdateCooldown(pIndex);
}

void AddPetExp(int pIndex, int amount)
{
    m_statExp[pIndex]+=amount;
    if (CheckLevelUp(pIndex) && GetPetLevel(pIndex) < 20)
    {
        SetPetLevel(pIndex, GetPetLevel(pIndex) + 1);
        int pet = GetCreature(pIndex);
        PetMakeHealth(pet, GetPetLevel(pIndex));
        LevelUpFx(pet);
        PushTimerQueue(1, pIndex, LevelNotify(GetPetLevel(pIndex)));
        PrintMessageFormatOne(GetPlayer(pIndex), "레밸 업! 레밸 %d 이 되셨습니다", GetPetLevel(pIndex));
        UpdateLevelMeter(pIndex);
    }
}

void FastWayLowerWalls()
{
    int owner = GetOwner(OTHER), i;

    if (CurrentHealth(OTHER))
    {
        ObjectOff(SELF);
        if (CurrentHealth(owner) && HasClass(owner, "PLAYER"))
        {
            for (i = 0 ; i < 5 ; i ++)
                WallOpen(Wall(126 + i, 78 + i));
            UniPrint(owner, "마법벽이 낮아졌습니다");
        }
        else
        {
            FrameTimerWithArg(120, GetTrigger(), EnableObject);
        }
    }
}

void OpenFinalWalls()
{
    int owner = GetOwner(OTHER);

    if (CurrentHealth(OTHER))
    {
        ObjectOff(SELF);
        if (CurrentHealth(owner) && HasClass(owner, "PLAYER"))
        {
            WallOpen(Wall(116, 14));
            WallOpen(Wall(117, 15));
            WallOpen(Wall(118, 16));
            FrameTimer(1, FieldMonsterLastPart);
            UniPrint(owner, "주변 어딘가에서 비밀 벽이 열렸습니다");
        }
        else
            FrameTimerWithArg(120, GetTrigger(), EnableObject);
    }
}

void NotificationChatCommand()
{
    int owner = GetOwner(OTHER);

    if (CurrentHealth(OTHER))
    {
        ObjectOff(SELF);
        if (CurrentHealth(owner) && HasClass(owner, "PLAYER"))
        {
            MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
            AudioEvent("JournalEntryAdd", 1);
            UniPrintToAll("게임 팁 - 채팅창에 '/?' 을 입력하면 이 맵에서 사용되는 모든 명령어를 확인하실 수 있습니다");
        }
        else
            SecondTimerWithArg(5, GetTrigger(), EnableObject);
    }
}

void StartPart2()
{
    ObjectOff(SELF);
    FieldMonsterPart2();
}

void StartPart3()
{
    ObjectOff(SELF);
    FieldMonsterPart3();
}

void StartPart4()
{
    ObjectOff(SELF);
    FieldMonsterPart4();
}

void SoulGateCollide()
{
    if (CurrentHealth(OTHER) && MaxHealth(SELF))
    {
        int owner = GetOwner(OTHER);
        int pIndex = GetPlayerIndex(owner);

        if (pIndex < 0)
            return;

        if (IsPlayerUnit(owner))
        {
            int resp = GetRespawnSpot(pIndex);

            if (!ToInt(GetObjectX(resp)))
                return;

            if (DistanceUnitToUnit(resp, SELF) > 250.0)
            {
                float x=GetObjectX(OTHER), y=GetObjectY(OTHER);
                MoveObject(resp, x,y);
                Effect("YELLOW_SPARKS", x,y, 0.0, 0.0);
                PlaySoundAround(resp, SOUND_SoulGateTouch);
                UniPrint(owner, "이 위치를 저장했습니다, 영웅이 죽게되면 이곳에서 다시 태어날 것입니다");
            }
        }
    }
}

int PlaceSoulGate(int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 10);
    Frozen(CreateObject("SpinningCrown", wp), 1);
    UnitNoCollide(unit + 1);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, SoulGateCollide);
    return unit;
}

void ShowCharacterInfo(int unit)
{
    int pIndex = GetPlayerIndex(unit);

    if (pIndex<0)
        return;

    char message[128];
    
    int cre = GetCreature(pIndex);
    if (CurrentHealth(cre))
    {
        DisplayHealthInfo(cre);
        int args[]={GetPetLevel(pIndex), m_statExp[pIndex], NextLevelExpTable(GetPetLevel(pIndex))};
        NoxSprintfString(message, "영웅정보 출력:: 레밸: %d, 스코어: %d / %d", args, sizeof(args));
        UniPrint(GetPlayer(pIndex), ReadStringAddressEx(message));
    }
}

void ShowCommandList(int unit)
{
    if (CurrentHealth(unit))
    {
        UniPrint(unit, "/s - 영웅 정보출력");
    }
}

void MapExit()
{
    MusicEvent();
    ResetAllPlayerResources();
    RemoveCoopTeamMode();
}

void ClearStartWalls()
{
    int i;

    for (i = 0 ; i < 5 ; i ++)
        WallOpen(Wall(25 + i, 195 + i));
}

void NotifyMoreInfo()
{
    UniPrintToAll("게임 팁- 채팅에 '/?' 를 입력하시면 게임에 사용되는 모든 명령어를 볼 수 있습니다                          ");
}

void GameDocuments()
{
    UniPrintToAll("*여신 키우기* 제작. panic======================================================================");
    UniPrintToAll("게임 팁- 몬스터를 잡을 때 마다 경험치가 누적되며, 어느정도 누적되면 레밸이 올라갑니다                    ");
    UniPrintToAll("게임 팁- 레밸이 증가되면 체력과 데미지도 증가되고, 특정 레밸 부터는 스킬을 사용할 수 있게 됩니다          ");
    SecondTimer(5, NotifyMoreInfo);
}

void ShowMapInfo()
{
    UniPrintToAll("** 여신 키우기 *** version 0.1 24 Aug 2019 -- Happy Soft LTD  **");
    UniPrintToAll("플레이 방법-- 그냥 잘 키우면 됨");
    SecondTimer(7, GameDocuments);
}

void MecroPutSoulGates(int startLocationNumber, int maxCount)
{
    int i;

    for (i = 0 ; i < maxCount ; i ++)
        PlaceSoulGate(startLocationNumber + i);
}

void MapDecorations()
{
    MecroPutSoulGates(126, 15);
    CreateObject("BlackPowderBarrel", 47);
    CreateObject("BlackPowderBarrel2", 48);
    CreateObject("BlackPowderBarrel2", 49);
    CreateObject("BlackPowderBarrel", 50);
    CreateObject("BlackPowderBarrel2", 51);
    CreateObject("BlackPowderBarrel", 52);
    CreateObject("BlackPowderBarrel2", 53);
}

void DelayMapInit()
{
    DecimalTable(-1);
    NextLevelExpTable(-1);
    PlayerInputTable(-1);
    LevelNotify(-1);
    FrameTimer(1, MapDecorations);
    FrameTimer(11, FieldMonsterPart1);
    FrameTimer(15, ClearStartWalls);
    SecondTimer(6, ShowMapInfo);
    FrameTimer(50, PutStartStampString);
    MakeCoopTeam();
    HashCreateInstance(&m_initScanHash);
    HashPushback(m_initScanHash, OBJ_REWARD_MARKER, CreateEffectiveItemUnitPos);
    StartUnitScan(Object("firstscan"), m_itemMaster, m_initScanHash);
    InitializeSubPart();
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("lsdcamp-log.txt");
    m_pClientKeyState=0x751000;
    m_itemMaster=DummyUnitCreateById(OBJ_HECUBAH, 100.0, 100.0);
    InitialServerPatchMapleOnly();
    InitializeCommonItemHash();
    FrameTimer(1, DelayMapInit);
    PushTimerQueue(60, m_pClientKeyState, ServerInputLoop);

    ChatMessageEventPushback("/?", ShowCommandList);
    ChatMessageEventPushback("/s", ShowCharacterInfo);
}

static void PlayerClassDoInitialize(int pIndex, int user) //virtual
{
    float x=GetObjectX(user), y= GetObjectY(user);
    
    SafetyPickup(user, CreateEffectiveItem(OBJ_SPELL_BOOK, x, y));
    SafetyPickup(user, CreateEffectiveItem(OBJ_BRACELETOF_HEALTH, x,y));
}

static void PlayerClassDoDeinit(int pIndex) //virtual
{ }


static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void commonServerClientProcedure() //virtual
{
    CommonInitializeImage();
    // InitPopupMessage(m_popupMessage);
}

void ClientProcLoop(){
    char *p=_CLIENT_OPTION_TYPE_OFF_;

    if (p[0])
    {
        ChangeLevelMeter(p[0]);
        p[0]=0;
    }
    FrameTimer(10, ClientProcLoop);
}

static void NetworkUtilClientMain()
{
    m_clientNetIdNumber=GetMemory(0x979720);
    commonServerClientProcedure();
    FrameTimerWithArg(30, m_clientNetIdNumber, ClientClassTimerLoop);
    FrameTimer(31, ClientProcLoop);
}

static void AddPetExpUsingAnItem(int pIndex, int amount) //virtual
{
    AddPetExp(pIndex, amount);
}

static void OnCreatureKill(int pIndex, int credit) //virtual
{
    AddPetExp(pIndex, credit);
}

static void OnFieldMobDeath(int mob) //virtual
{
    if (!Random(0, 8))
    {
        // PlaceHotPotion(GetObjectX(SELF), GetObjectY(SELF));
        CreateEffectiveItemRandomly(GetObjectX(SELF), GetObjectY(SELF));
    }
}
