
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/memutil.h"
#include "libs/objectIDdefines.h"
#include "libs/playerinfo.h"
#include "libs/queueTimer.h"

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        /*
        push ebp
        mov ebp,esp
        push [ebp+0C]
        push [ebp+08]
        push 00000001
        mov eax,game.exe+107310
        call eax
        add esp,0C
        pop ebp
        ret 
        */
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        /*
        mov eax,game.exe+107250
        call eax
        push [eax]
        push [eax+04]
        push [eax+08]
        push [eax+0C]
        mov eax,game.exe+117F90
        call eax
        add esp,10
        xor eax,eax
        ret 
        */
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

int GetUnitTopParent(int unit)
{
    int next;

    while (TRUE)
    {
        next = GetOwner(unit);
        if (next)
            unit = next;
        else
            break;
    }
    return unit;
}

int GetKillCredit()
{
    int *victim = GetMemory(0x979724);

    if (victim != NULLPTR)
    {
        int *attacker = victim[130];

        if (attacker != NULLPTR)
            return attacker[11];
    }
    return NULLPTR;
}

void UpdatePlayerGold(int user)
{
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0x6A, 0x05, 
            0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3, 0x90
        };
        pCode=code;
    }
    int pIndex=GetPlayerIndex(user);

    if (pIndex<0)
        return;

    char packet[]={0x4a, 0,0,0,0};
    int *p = &packet[1];

    p[0]=GetGold(user);
    int args[]={packet,pIndex};
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=pCode;
    Unused1f(args);
    pBuiltins[31]=pOld;
}

void InvisibleDelaySetPrivate(int *pAllocptr)
{
    int *ptr = pAllocptr[0];
    int *pec = ptr[187];

    ptr[4] = 0x0901120d;
    pec[0] = 8;
    int allowedFlag = pAllocptr[1];
    pec[11] = allowedFlag;
    if (pAllocptr[2])
        pec[6] = pAllocptr[2];
    if (pAllocptr[3])
        pec[8] = pAllocptr[3];
    if (pAllocptr[4])
        pec[4] = pAllocptr[4];

    int rep = -1;

    while ((++rep) < 32)
        ptr[rep+140] = 0x10000;

    ObjectOn(ptr[11]);
}

void CreateInvisibleBeacon(int *pGet, float *pPos, float *pSize, int allowedFlag, int *pCallbacks)
{
    int *ptr = UnitToPtr(CreateObjectById(OBJ_TRIGGER, pPos[0], pPos[1]));
    int *allocptr = NULLPTR;

    AllocSmartMemEx(20, &allocptr);
    if (allocptr == NULLPTR)
        return;

    ptr[46] = ToInt(pSize[0]);
    ptr[47] = ToInt(pSize[1]);

    allocptr[0] = ptr;
    allocptr[1] = allowedFlag;
    if (!pCallbacks)
    {
        allocptr[2] = 0;
        allocptr[3] = 0;
        allocptr[4] = 0;
    }
    else
    {
        allocptr[2] = pCallbacks[0];
        allocptr[3] = pCallbacks[1];
        allocptr[4] = pCallbacks[2];
    }
    FrameTimerWithArg(1, allocptr, InvisibleDelaySetPrivate);

    if (pGet)
        pGet[0] = ptr[11];
}
void PlaySummonEffect(float *xy, short thingId, int duration)
{
    char *p;

    if (!p)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
    0xFF, 0x70, 0x08, 0xFF, 0x70, 0x0C, 0xFF, 0x70, 0x10, 0xB8, 0xF0, 0x36, 
    0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
        };
        p=code;
    }
    int args[]={
        duration,
        thingId,
        0,
        xy,
        0,
    };
    invokeRawCode(p, args);
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}

void onHarpoonPulling()
{
    if (CurrentHealth(OTHER))
    {
        PushObjectTo(SELF, UnitRatioX(SELF,OTHER, -4.0), UnitRatioY(SELF,OTHER, -4.0));
        Damage(SELF,OTHER,3,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
    }
}

void DeferredFollowUnit(int mon)
{
    if (CurrentHealth(mon))
    {
        int owner=GetOwner(mon);
        if (owner){
            CreatureFollow(mon,owner);
            AggressionLevel(mon,1.0);
        }
    }
}

void harpoonPreserve(int unit)
{
    char *pcode;

    if (!pcode)
    {
        char code[]={
            0x56, 0x57, 0x52, 0x55, 0x8B, 0xEC, 0x83, 0xEC, 0x20, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0x38, 0x8B, 0xB7, 0xEC, 0x02, 
            0x00, 0x00, 0x8B, 0x40, 0x04, 0x89, 0x45, 0xFC, 0x8B, 0x86, 0x84, 0x00, 0x00, 0x00, 0x85, 0xC0, 0x74, 0x2E, 0x8B, 0x86, 0x84, 0x00, 
            0x00, 0x00, 0xF6, 0x40, 0x10, 0x20, 0x74, 0x0B, 0x57, 0xB8, 0x20, 0x75, 0x53, 0x00, 0xFF, 0xD0, 0x58, 0xEB, 0x17, 0x8B, 0x96, 0x84, 
            0x00, 0x00, 0x00, 0x52, 0x57, 0x68, 0x00, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x83, 0xC4, 
            0x20, 0x8B, 0xE5, 0x5D, 0x5A, 0x5F, 0x5E, 0x31, 0xC0, 0xC3, 0x90, 0x90,
        };
        pcode=code;
        int *n=&code[72];
        n[0]=onHarpoonPulling;
    }
    int *ptr=UnitToPtr(unit);

    if(!ptr) return;
    int *pec=ptr[187];

    if (pec[34])
    {
        int args[]={ptr, -10.0};
        invokeRawCode(pcode, args);
        PushTimerQueue(1,unit,harpoonPreserve);
        return;
    }
    if (pec)
        SetMemory(pec+0x2c0, 0);
}

#define HARPOON_TARGET_OFF 527
int harpoonCodeCopied()
{
    char *code;

    if (code)
        return code;

    char copiedCode[980];//2ec (+5b4)
    int calls[]={
0x0054f3b9,
0x0054f3c9,
0x0054f3d9,
0x0054f3e9,
0x0054f41a,
0x0054f437,
0x0054f480,
0x0054f49c,
0x0054f4d8,
0x0054f502,
0x0054f530,
0x0054f554,
0x0054f592,
0x0054f5e9,
0x0054f630,
0x0054f643,
0x0054f669,
0x0054f6e2,
0x0054f6fa,
0x0054f729,
0,
    };
    OpcodeCopiesAdvance(copiedCode, calls, 0x54f380, 0x54f738);
    SetMemory(copiedCode+215, HARPOON_TARGET_OFF*4);
    SetMemory(copiedCode+228, 0x100);
    SetMemory(copiedCode+238, 0x104);
    code=copiedCode;
    return code;
}

int attachExtendedUnitFieldFix114To5b4(int unit)
{
    int *ptr=UnitToPtr(unit);
    int *pEC = ptr[187];

    if (!pEC[HARPOON_TARGET_OFF])
    {
        int c;
        AllocSmartMemEx(0x1000, &c);
        int *newField114 = c;
        NoxDwordMemset(newField114, 1024, 0);
        pEC[HARPOON_TARGET_OFF]=newField114;
        return c;
    }
    return pEC[HARPOON_TARGET_OFF];
}

void castHarpoonForcedFix(int unit, float gap)
{
    char code[]={0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x50, 
    0xB8, 0x90, 0x88, 0x53, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3, 0x90,};
    int *p=UnitToPtr(unit);    
    int *pEC=GetMemory(p+0x2ec);

    if (GetMemory(pEC+0x2c0))
        return;
    
    int *field = attachExtendedUnitFieldFix114To5b4(unit);
    float xvect=UnitAngleCos(unit,gap),yvect=UnitAngleSin(unit,gap);

    field[0x100]=FloatToInt(GetObjectX(unit)+xvect);
    field[0x104]=FloatToInt(GetObjectY(unit)+yvect);
    invokeRawCode(code, p);
    int *ptr=GetMemory(0x750710);

    SetMemory(ptr+0x2e8, harpoonCodeCopied());
    SetMemory(pEC+0x2c0, ptr);
    PushTimerQueue(1,unit,harpoonPreserve);
}
