
#include "boss_gvar.h"
#include "libs/gui_window.h"
#include "libs/format.h"

void QueryDialogCtx(int *get, int *set){
    int ctx;
    if (get)get[0]=ctx;
    if (set)ctx=set[0];
}

void initDialog()
{
    int *wndptr=0x6e6a58,ctx;
    GUIFindChild(wndptr[0], 3901, &ctx);
    QueryDialogCtx(NULLPTR, &ctx);
}

void InitFillPopupMessageArray(int *pPopupMessage)
{
    pPopupMessage[GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM]="당신의 인벤토리를 무적화 하시겠어요? 필요한 금액은 5,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_ARROW_AXE]="블러드 서드를 구입하시겠어요? 필요한 금액은 40,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_THUNDER_SWORD]="피카추의 백만볼트 서드를 구입하려면 예를 누르세요, 검을 휘두르면 전방에 백만볼트를 발사합니다,  45,000골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_SKILL_CRITICAL_HIT]="윈드 부스터를 배우시겠어요? 짧은 거리를 빠르게 공간이동을 합니다, 15,000골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_MONEY_EXCHANGER]="당신이 가진 여러 보석들을 모두 금화로 환전해 드립니다, 예를 누르시면 보석을 금화로 환전해드릴게요";
    pPopupMessage[GUI_DIALOG_MESSAGE_CREATURE_STELLS]="스텔스기을 구입하시겠습니까? 당신을 따라다니면서 도와줄 것입니다,  40,000골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_CREATURE_HOSUNGLEE]="이호성을 구입하시겠습니까?, 가격은 60,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_CREATURE_RUNNER_ONI]="러너오니를 구입하시겠습니까?, 가격은 40,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_FON_AXE]="포스오브네이쳐 엑스를 구입하실래요?, 가격은 45,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_SKILL_AUTODEATHRAY]="자동 데스레이 마술을 구입하시겠어요?, 가격은 25,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_AWARD_BERSERKER]="트리플에로우 마술을 활성하려면은 가격은 20,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_SECRET_GARDEN]="비밀의 정원을 개방하실래요? 가격은 38,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_AWARD_HARPOON]="작살을 배우시겠어요? 가격은 20,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_SPEED_ARMOR]="랜덤으로 이동속도 속성의 갑옷을 드립니다, 가격은 회차 당 17,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_CURE_ARMOR]="랜덤으로 재생 속성의 갑옷을 드립니다, 가격은 회차 당 14,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_AWARD_WARCRY]="전사의 함성을 활성화 하려면 가격은 20,000골드입니다";
}

void initPopupMsg(int *pPopupMessage)
{
    initDialog();
    InitFillPopupMessageArray(pPopupMessage);
}

void queryPopMessage(int **get){
    int msg[32];

    if (get) get[0]=msg;
}

void InitializePopupMessages(){
    int once;
    if (once) return;
    once=TRUE;
    int *s;
    queryPopMessage(&s);
    initPopupMsg(s);
}

