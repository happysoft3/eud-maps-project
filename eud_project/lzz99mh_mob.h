
#include "lzz99mh_gvar.h"
#include "lzz99mh_utils.h"
#include "libs/unitstruct.h"
#include "libs/hash.h"
#include "libs/objectIDdefines.h"
#include "libs/queueTimer.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/format.h"
#include "libs/buff.h"
#include "libs/monsteraction.h"

#define HORVATH_HP 770
#define HORVATH_LAISER_RIFLE_DAMAGE 30

int computePercent(int cur, int max)
{
    return (cur*10)/max;
}

int MonsterSightHash() //virtual
{
    return 0;
}

void ResetUnitSight(int unit)
{
    int arg;

    if (HashGet(MonsterSightHash(), unit, &arg, TRUE))
    {
        Enchant(unit, "ENCHANT_BLINDED", 0.06);
        AggressionLevel(unit, 1.0);
    }
}

void CheckResetSight(int unit, int arg, int delay)
{
    if (!HashGet(MonsterSightHash(), unit, NULLPTR, TRUE))
    {
        HashPushback(MonsterSightHash(), unit, arg);
        PushTimerQueue(delay, unit, ResetUnitSight);
    }
}

void onMonsterPoisoned(int mob)
{
	int poisonLv = IsPoisonedUnit(mob);

    if (poisonLv)
    {
        Damage(SELF, 0, poisonLv, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(mob), GetObjectY(mob)), 9);
    }
}

void reportMonsterHP(int mob)
{
	char slots[11];

    NoxByteMemset(slots,sizeof(slots)-1,' ');
    int perc=computePercent(CurrentHealth(mob),MaxHealth(mob));

    while (--perc>=0)
        slots[perc]='=';

    char buff[32], *p=slots;

    NoxSprintfString(buff, "[%s]", &p, 1);
    UniChatMessage(mob, ReadStringAddressEx(buff), 90);
}

void onBossMonsterHit()
{
	onMonsterPoisoned(SELF);
    reportMonsterHP(SELF);
}

void onMonsterHit()
{
	onMonsterPoisoned(SELF);
}

int BomberBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1651339074; arr[1] = 29285; arr[17] = 98; arr[18] = 1; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1065353216; arr[26] = 4; arr[27] = 1; 
		arr[37] = 1399876937; arr[38] = 7630696; arr[53] = 1132068864; arr[55] = 13; arr[56] = 21; 
		arr[58] = 5546320; arr[60] = 1348; arr[61] = 46899968; 
	pArr = arr;
	return pArr;
}

void BomberSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 98;	hpTable[1] = 98;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = BomberBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void TinyMobBomber(int mon)
{
    int *ptr=UnitToPtr(mon);
    
    SetMemory(ptr + 0x2b8, 0x4e83b0);
    BomberSubProcess(mon);
    TrapSpells(mon,"SPELL_METEOR","NULL","NULL");
}

void OnSpawnTinyBomberLoop(int sub)
{
    int dur=GetDirection(sub);
    if (dur)
    {
        LookWithAngle(sub,dur-1);
        PushTimerQueue(1,sub,OnSpawnTinyBomberLoop);
        int bomb=CreateObjectById(OBJ_BOMBER_GREEN,GetObjectX(sub),GetObjectY(sub));
        TinyMobBomber(bomb);
        return;
    }
    Delete(sub);
}

void SpawnTinyBomber(short location, int count)
{
    int sub=CreateObjectById(OBJ_REWARD_MARKER_PLUS,LocationX(location),LocationY(location));

    PushTimerQueue(1, sub, OnSpawnTinyBomberLoop);
    LookWithAngle(sub,count);
}

int FallFist(float xpos, float ypos, int damageAmount, float speed)
{
    int ret = CreateObjectAt("SmallFist", xpos, ypos);
    int ptr=UnitToPtr(ret);
    int *ctl = GetMemory(ptr+0x2ec);

    ctl[0] = damageAmount;
    SetMemory(ptr+0x14, GetMemory(ptr+0x14) | 0x20);
    Raise(ret, 255.0);
    ptr[27]=speed;
    // SetMemory(ptr+0x6c, speed);
    return ret;
    
}

void decayingDelayRemoveFistSubUnitAndFistUnit(int fist)
{
	int owner=GetOwner(fist);

	if (owner)
		Delete(owner);
	Delete(fist);
}

void onJandorMeleeAttack()
{
	float x=GetObjectX(OTHER), y=GetObjectY(OTHER);
	int fist = FallFist(x,y, 10, 30.0);

	if (!fist)
	{
		UniPrintToAll("error-no fist ");
		return;
	}
	int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);

	SetOwner(SELF, sub);
	SetOwner(sub, fist);
	PushTimerQueue(60, fist, decayingDelayRemoveFistSubUnitAndFistUnit);
}

int AirshipCaptainBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 600; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 34817; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1109393408; arr[29] = 20; arr[30] = 1138819072; arr[31] = 11; arr[32] = 6; 
		arr[33] = 12; arr[58] = 5546320; arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328; 
	pArr = arr;
	CustomMeleeAttackCode(arr, onJandorMeleeAttack);
	return pArr;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 600;	hpTable[1] = 600;
	int *uec = ptr[187];
	uec[360] = 34817;		uec[121] = AirshipCaptainBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void OnJandorHurt()
{
    onBossMonsterHit();
    Enchant(SELF, "ENCHANT_INVULNERABLE", 1.0);
}

int SpawnBossJandor(short location)
{
	int mob=CreateObjectById(OBJ_AIRSHIP_CAPTAIN, LocationX(location),LocationY(location));

	AirshipCaptainSubProcess(mob);
	SetCallback(mob, 7,OnJandorHurt);
	return mob;
}

void SummonedUrchin(int mon)
{
    SetUnitMaxHealth(mon, 64);
	UnitZeroFleeRange(mon);
}

void SummonedBat(int mon)
{
    SetUnitMaxHealth(mon, 70);
}

void SummonedLeech(int mon)
{
    SetUnitMaxHealth(mon,128);
}

void SummonedWolf(int mon)
{
    SetUnitMaxHealth(mon,160);
}

void SummonedSwordsman(int mon)
{
    SetUnitMaxHealth(mon, 200);
}

void SummonedScorpion(int mon)
{
    SetUnitMaxHealth(mon, 220);
    Enchant(mon, "ENCHANT_VAMPIRISM", 0.0);
}

void SummonedMystic(int mon)
{
    SetUnitMaxHealth(mon, 275);
    Enchant(mon, EnchantList(ENCHANT_ANCHORED), 0.0);
}

int BlackWidowBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 200; arr[18] = 45; 
		arr[19] = 85; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; 
		arr[27] = 3; arr[28] = 1097859072; arr[29] = 15; arr[31] = 8; arr[32] = 13; 
		arr[33] = 21; arr[34] = 1; arr[35] = 1; arr[36] = 1; arr[37] = 1684631635; 
		arr[38] = 1884516965; arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; 
		arr[59] = 5544896; arr[61] = 45071360; 
	pArr = &arr;
	return pArr;
}

void BlackWidowSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = BlackWidowBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int BlackBearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1667329090; arr[1] = 1634026091; arr[2] = 114; arr[17] = 260; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1106247680; arr[29] = 18; arr[30] = 1114636288; arr[31] = 2; 
		arr[59] = 5542784; arr[60] = 1366; arr[61] = 46902784; 
	pArr = &arr;
	return pArr;
}

void BlackBearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = BlackBearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int VileZombieBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701603670; arr[1] = 1651339098; arr[2] = 25961; arr[17] = 295; arr[19] = 105; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1110704128; arr[29] = 30; arr[31] = 10; arr[34] = 10; 
		arr[35] = 2; arr[36] = 10; arr[59] = 5543680; arr[60] = 1361; arr[61] = 46895184; 
	pArr = &arr;
	return pArr;
}

void VileZombieSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1078565273;		ptr[137] = 1078565273;
	int *hpTable = ptr[139];
	hpTable[0] = 295;	hpTable[1] = 295;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = VileZombieBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int LichBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[16] = 120000; arr[17] = 415; arr[19] = 55; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; arr[28] = 1108082688; 
		arr[29] = 50; arr[53] = 1128792064; arr[54] = 4; arr[59] = 5542784; arr[60] = 1342; 
		arr[61] = 46909440; 
	pArr = &arr;
	return pArr;
}

void LichSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 415;	hpTable[1] = 415;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void tryBlueOrbPickup()
{
    GreenSparkFx(GetObjectX(SELF),GetObjectY(SELF));
}

void onBlueOrbCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 10, DAMAGE_TYPE_EXPLOSION);
            Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            Delete(SELF);
        }
    }
}

#define ORB_BLUE_DURATION 0
#define ORB_BLUE_XVECT 1
#define ORB_BLUE_YVECT 2
#define ORB_BLUE_UNIT 3
#define ORB_BLUE_MAX 4

int createOrbForBomber(int owner)
{
    int orb=CreateObjectById(OBJ_BLUE_ORB, GetObjectX(owner), GetObjectY(owner));

    Frozen(orb,TRUE);
    SetUnitFlags(orb,GetUnitFlags(orb)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetUnitCallbackOnPickup(orb,tryBlueOrbPickup);
    SetOwner(owner,orb);
    SetUnitCallbackOnCollide(orb, onBlueOrbCollide);
    return orb;
}

void persistBomberAttack(int *pData)
{
    int sub=pData[ORB_BLUE_UNIT];

    if (ToInt(GetObjectX( sub )) )
    {
        int attacker=GetOwner(sub);

        if (IsVisibleTo(sub, attacker))
        {
            float *pVect = &pData[ORB_BLUE_XVECT];
            
            MoveObjectVector(sub,pVect[0],pVect[1]);
            PushTimerQueue(1, pData, persistBomberAttack);
            return;
        }
        Delete(sub);
    }
    FreeSmartMemEx(pData);
}

void startBomberAttack(int attacker, int victim)
{
    int sub=createOrbForBomber(attacker);
    int c;

    AllocSmartMemEx(ORB_BLUE_MAX*4, &c);
    PushTimerQueue(1, c, persistBomberAttack);
    int *pData=c;

    pData[ORB_BLUE_UNIT]=sub;
    pData[ORB_BLUE_DURATION]=30;
    pData[ORB_BLUE_XVECT]=UnitRatioX(victim,attacker,7.0);
    pData[ORB_BLUE_YVECT]=UnitRatioY(victim,attacker,7.0);
}

void onBomberMeleeAttack()
{
	startBomberAttack(SELF, OTHER);
    CreatureIdle(SELF);
}

int BomberBlueBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1651339074; arr[1] = 1816293989; arr[2] = 25973; arr[17] = 165; arr[19] = 110; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1065353216; arr[26] = 4; arr[27] = 1; 
		arr[28] = 1128792064; arr[29] = 1; arr[32] = 19; arr[33] = 25; arr[58] = 5546320; 
		arr[59] = 5542784; arr[60] = 1349; arr[61] = 46899456; 
	pArr = arr;
    CustomMeleeAttackCode(arr, onBomberMeleeAttack);
	return pArr;
}

void BomberBlueSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1079194419;		ptr[137] = 1079194419;
	int *hpTable = ptr[139];
	hpTable[0] = 165;	hpTable[1] = 165;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = BomberBlueBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
    SetMemory(ptr + 0x2b8, 0x4e83b0);
}

void onKillerianAttack()
{
    DeleteObjectTimer( CreateObjectById(OBJ_METEOR_EXPLODE, GetObjectX(OTHER), GetObjectY(OTHER)), 9);
    Damage(OTHER, SELF, 10, DAMAGE_TYPE_FLAME);
}

int DemonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869440324; arr[1] = 110; arr[17] = 500; arr[18] = 200; arr[19] = 96; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; 
		arr[28] = 1106247680; arr[29] = 10; arr[31] = 1; arr[53] = 1128792064; arr[54] = 4; 
		arr[58] = 5545472; arr[59] = 5542784; arr[60] = 1347; arr[61] = 46910976; 
	pArr = arr;
    CustomMeleeAttackCode(arr, onKillerianAttack);
	return pArr;
}

void DemonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077432811;		ptr[137] = 1077432811;
	int *hpTable = ptr[139];
	hpTable[0] = 500;	hpTable[1] = 500;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = DemonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int HecubahWithOrbBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 600; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; arr[40] = 1852140903; 
		arr[41] = 116; arr[53] = 1128792064; arr[55] = 16; arr[56] = 26; arr[60] = 1384; 
		arr[61] = 46914560; 
	pArr = arr;
	return pArr;
}

void HecubahWithOrbSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 600;	hpTable[1] = 600;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = HecubahWithOrbBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

#define SPELL_BLINK 4
#define SPELL_INVISIBILITY 39

void onBeholderSummoned(int mon)
{
	char *ptr=UnitToPtr(mon);
	char *pSpellset = GetMemory(&ptr[0x2ec])+0x5d0;

	pSpellset[SPELL_INVISIBILITY]=pSpellset[SPELL_BLINK];
	pSpellset[SPELL_BLINK] = 0;
		
        // SetMemory(uec + (16*4)// GetSpellNumber("SPELL_DEATH_RAY")
        // , 0x40000000);
		// SetMemory(uec +(51*4)// GetSpellNumber("SPELL_SHIELD")
        // , 0x10000000);
        // SetMemory(uec +(72*4)// GetSpellNumber("SPELL_SLOW")
        // , 0x20000000);
		// SetMemory(uec +(39*4)// GetSpellNumber("SPELL_INVISIBILITY")
        // , 0x10000000);
        // SetMemory(uec +(38*4) //GetSpellNumber("SPELL_INVERSION")
        // , 0x8000000);
        // SetMemory(uec +(43*4) //GetSpellNumber("SPELL_CHAIN_LIGHTNING")
        // , 0x40000000);
	SetUnitMaxHealth(mon, 485);
}

void InitializeSummonMobHash(int hash)
{
    HashPushback(hash,OBJ_URCHIN,SummonedUrchin);
    HashPushback(hash,OBJ_BAT,SummonedBat);
    HashPushback(hash,OBJ_GIANT_LEECH,SummonedLeech);
    HashPushback(hash,OBJ_WOLF,SummonedLeech);
    HashPushback(hash,OBJ_ALBINO_SPIDER,SummonedLeech);
    HashPushback(hash,OBJ_ARCHER,SummonedLeech);
    HashPushback(hash,OBJ_SWORDSMAN,SummonedSwordsman);
    HashPushback(hash,OBJ_SCORPION,SummonedScorpion);
    HashPushback(hash,OBJ_SPIDER,BlackWidowSubProcess);
    HashPushback(hash,OBJ_BLACK_BEAR,BlackBearSubProcess);
    HashPushback(hash,OBJ_EVIL_CHERUB,SummonedLeech);
    HashPushback(hash,OBJ_WIZARD,SummonedMystic);
    HashPushback(hash,OBJ_VILE_ZOMBIE,VileZombieSubProcess);
    HashPushback(hash,OBJ_LICH,LichSubProcess);
	HashPushback(hash,OBJ_BOMBER_BLUE,BomberBlueSubProcess);
	HashPushback(hash,OBJ_DEMON,DemonSubProcess);
	HashPushback(hash,OBJ_HECUBAH_WITH_ORB,HecubahWithOrbSubProcess);
	HashPushback(hash,OBJ_BEHOLDER,onBeholderSummoned);
}

int MysticSludgeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 225; arr[19] = 87; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1113325568; 
		arr[29] = 10;
		arr[30] = 1123024896; arr[31] = 11; arr[58] = 5546320; arr[59] = 5542784; arr[60] = 2303; 
		arr[61] = 46917632; 
	pArr = arr;
	return pArr;
}

void MysticSludgeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076300349;		ptr[137] = 1076300349;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = MysticSludgeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void OnMysticSludgeLoop(int sub)
{
	if (MaxHealth(sub))
	{
		int owner=GetOwner(sub);

		if (CurrentHealth(owner))
		{
			if (ToInt( DistanceUnitToUnit(sub,owner)) )
				MoveObject(sub,GetObjectX(owner),GetObjectY(owner));
			int data;
			if (HashGet(GenericHash(), sub, &data, FALSE))
			{
				PushTimerQueue(1, sub, OnMysticSludgeLoop);
				int *pImage=data, *ptr=UnitToPtr(sub);

				SetMemory(GetMemory(ptr+0x2ec)+0x1e0, pImage[GetUnit1C(sub)]);
				SetUnit1C(sub, (GetUnit1C(sub)+1)%21);
				return;
			}
		}
		HashGet(GenericHash(), sub, NULLPTR, TRUE);
		Delete(sub);
	}
}

int CreateMysticSludge(float x,float y)
{
	int mob=CreateObjectById(OBJ_TALKING_SKULL,x,y);

	MysticSludgeSubProcess(mob);
	int mystic=DummyUnitCreateById(OBJ_WIZARD,x,y);
	UnitNoCollide(mystic);	
	SetOwner(mob, mystic);
	LookWithAngle(mystic, 64);
	PushTimerQueue(1, mystic, OnMysticSludgeLoop);
	int img[]={
		68364,
		68108,
		67852,
		67596,
		67340,
		67084,
		66828,
		66572,
		66316,
		66060,
		65804,
		66060,
		66316,
		66572,
		66828,
		67084,
		67340,
		67596,
		67852,
		68108,
		68364,
	};
	HashPushback(GenericHash(), mystic, img);
	SetUnit1C(mystic,0);
	return mob;
}

int HorrendousBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[16] = 100000; arr[17] = 400; 
		arr[18] = 400; arr[19] = 100; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 9; arr[27] = 5; arr[28] = 1120403456; arr[29] = 34; 
		arr[54] = 4; arr[59] = 5542784; arr[60] = 1386; arr[61] = 46907648; 
	pArr = arr;
	return pArr;
}

void onHorrendousSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 60, DAMAGE_TYPE_PLASMA);
            // PlaySphericalEffect(OTHER);
            // NoxRestoreHealth(SELF, 100);
        }
    }
}

#define FALL_GAP 20.0
void fallingHorrendousStuff(int sub)
{
    float z=GetObjectZ(sub);
    if (ToInt(z))
    {
        if (z > FALL_GAP)
        {
            Raise(sub, z - FALL_GAP);
            PushTimerQueue(1, sub, fallingHorrendousStuff);
            return;
        }
        float x= GetObjectX(sub), y=GetObjectY(sub);
        SplashDamageAtEx(GetOwner(sub), x,y, 75.0, onHorrendousSplash);
        CreateObjectById(OBJ_CRATE_BREAKING_1, x,y);
        Delete(sub);
    }
}

void startHorrendousAttack(int mon, float x,float y)
{
    int sub=CreateObjectById(OBJ_CRATE_1, x, y);

    UnitNoCollide(sub);
    Frozen(sub, TRUE);
    Raise(sub, 200.0);
    SetOwner(mon, sub);
    PushTimerQueue(1, sub, fallingHorrendousStuff);
}

void onHorrendousAttack()
{
    // Damage(OTHER, SELF, 50, DAMAGE_TYPE_BLADE);
    startHorrendousAttack(GetTrigger(), GetObjectX(OTHER), GetObjectY(OTHER));
    // DeleteObjectTimer( CreateObjectById(OBJ_METEOR_EXPLODE, GetObjectX(OTHER), GetObjectY(OTHER)), 24);
}

void HorrendousSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 600;	hpTable[1] = 600;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = HorrendousBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
    CustomMeleeAttackCode(uec[121], onHorrendousAttack);
}

int CreateBossHorrendous(float x,float y)
{
	int mob=CreateObjectById(OBJ_HORRENDOUS, x,y);

	HorrendousSubProcess(mob);
	RetreatLevel(mob, 0.0);
	return mob;
}

int StrongWizardWhiteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[17] = 200; arr[18] = 55; arr[19] = 60; arr[21] = 1065353216; arr[23] = 34816; 
		arr[24] = 1069547520; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; 
	pArr = arr;
	return pArr;
}

void StrongWizardWhiteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = HORVATH_HP;	hpTable[1] = HORVATH_HP;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = StrongWizardWhiteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onLaiserCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            int hash=GetUnit1C(SELF);

            if (!hash)
                return;

            if (HashGet(hash, GetCaller(), NULLPTR, FALSE))
                return;

            HashPushback(hash, GetCaller(), TRUE);
            Damage(OTHER, GetOwner(SELF), HORVATH_LAISER_RIFLE_DAMAGE, DAMAGE_TYPE_ZAP_RAY);
        }
    }
}

void spawnLaiserDamageHelper(int posUnit, int owner, int hash)
{
    if (!CurrentHealth(owner))
        return;

    int dam=DummyUnitCreateById(OBJ_DEMON, GetObjectX(posUnit),GetObjectY(posUnit));

    SetOwner(owner, dam);
    SetUnitFlags(dam, GetUnitFlags(dam)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    DeleteObjectTimer(dam,1);
    SetCallback(dam, 9, onLaiserCollide);
    SetUnit1C(dam, hash);
}

void onLaiserMove(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int hash=GetUnit1C(sub);
        int dur=GetDirection(sub);
        if (dur)
        {
            if (IsVisibleTo(sub,sub+1))
            {
                PushTimerQueue(1, sub, onLaiserMove);
                LookWithAngle(sub,dur-1);
                MoveObjectVector(sub, GetObjectZ(sub), GetObjectZ(sub+1));
                MoveObjectVector(sub+1, GetObjectZ(sub), GetObjectZ(sub+1));
                spawnLaiserDamageHelper(sub, GetOwner(sub), hash);
                Effect("SENTRY_RAY", GetObjectX(sub), GetObjectY(sub),GetObjectX(sub+1),GetObjectY(sub+1));
                return;
            }
        }

        if (hash)
            HashDeleteInstance(hash);
        Delete(sub);
        Delete(sub+1);
    }
}

void shootLaiserBolt(int caster, int target)
{
    float point[]={GetObjectX(caster),GetObjectY(caster)};
    float vect[]={UnitRatioX(target,caster,13.0),UnitRatioY(target,caster,13.0)};
    int unit=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,point[0]+vect[0],point[1]+vect[1]);
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,point[0]-vect[0],point[1]-vect[1]);
    LookWithAngle(unit, 32);
    Raise(unit, vect[0]);
    Raise(unit+1,vect[1]);
    SetOwner(caster, unit);
    int hash;

    HashCreateInstance(&hash);
    SetUnit1C(unit, hash);
    PushTimerQueue(1, unit, onLaiserMove);
}

void onWhiteWizSight()
{
    if (!CurrentHealth(SELF))
        return;

	if (CurrentHealth(OTHER))
    {
		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
        {
			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
            shootLaiserBolt(GetTrigger(), GetCaller());
		}
	}
    CheckResetSight(GetTrigger(), 0, 60);
}

int Part1FinalBoss(float x, float y)
{
    int unit = CreateObjectById(OBJ_STRONG_WIZARD_WHITE, x,y);

    StrongWizardWhiteSubProcess(unit);
    SetCallback(unit, 3, onWhiteWizSight);
	SetCallback(unit,7,onBossMonsterHit);
    return unit;
}

int UrchinShamanBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 1750298217; arr[2] = 1851878753; arr[17] = 180; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[37] = 1869768788; 
		arr[38] = 1735289207; arr[39] = 1852798035; arr[40] = 101; arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 48; arr[56] = 60; arr[60] = 1340; arr[61] = 46904576; 
	pArr = &arr;
	return pArr;
}

#define SHAMAN_MAX_HP 600
#define SHAMAN_DAMAGE 10

void UrchinShamanSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = SHAMAN_MAX_HP;	hpTable[1] = SHAMAN_MAX_HP;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = UrchinShamanBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onGasCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER,SELF))
    {
        int hash=GetUnit1C(SELF);

        if (HashGet(hash,GetCaller(),NULLPTR,FALSE))
            return;

        Damage(OTHER,SELF,SHAMAN_DAMAGE,DAMAGE_TYPE_POISON);
        HashPushback(hash,GetCaller(),TRUE);
    }
}

void onSmokingGunLoop(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int hash=GetUnit1C(sub);
        int owner=GetOwner(sub);
        if(CurrentHealth(owner)){
            if(IsVisibleTo(sub,sub+1))
            {
                int dur=GetDirection(sub); 
                if (dur)
                {
                    PushTimerQueue(1,sub,onSmokingGunLoop);
                    LookWithAngle(sub,dur-1);
                    MoveObjectVector(sub,GetObjectZ(sub),GetObjectZ(sub+1));
                    MoveObjectVector(sub+1,GetObjectZ(sub),GetObjectZ(sub+1));
                    MoveObjectVector(sub+2,GetObjectZ(sub),GetObjectZ(sub+1));
                    return;
                }
            }
        }
        Delete(sub);
        Delete(sub+1);
        Delete(sub+2);
        if(hash)
            HashDeleteInstance(hash);
    }
}

void startSmokingGun(int caster, int target)
{
    float vect[]={UnitRatioX(target,caster,13.0),UnitRatioY(target,caster,13.0)};
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,GetObjectX(caster)+vect[0],GetObjectY(caster)+vect[1]);
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(caster)-vect[0],GetObjectY(caster)-vect[1]);
    int gas= CreateObjectById(OBJ_BIG_SMOKE, GetObjectX(sub),GetObjectY(sub));

    UnitNoCollide(gas);
    Frozen(gas, TRUE);
    SetUnitFlags(gas,GetUnitFlags(gas)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetUnitCallbackOnCollide(gas, onGasCollide);
    Raise(sub,vect[0]);
    Raise(sub+1,vect[1]);
    SetOwner(caster,sub);
    SetOwner(caster,gas);
    LookWithAngle(sub,21);
    PushTimerQueue(1,sub,onSmokingGunLoop);
    int hash;
    HashCreateInstance(&hash);
    SetUnit1C(sub,hash);
    SetUnit1C(gas,hash);
    MonsterForceCastSpell(caster,0,GetObjectX(caster)+vect[0],GetObjectY(caster)+vect[1]);
}

void onShamanSight()
{
    if (!CurrentHealth(SELF))
        return;

	if (CurrentHealth(OTHER))
    {
		if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_PROTECT_FROM_MAGIC)) )
        {
			Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
            startSmokingGun(GetTrigger(),GetCaller());
		}
	}
    CheckResetSight(GetTrigger(),0,48);
}

int Part2BossShaman(float x, float y)
{
    int unit = CreateObjectById(OBJ_URCHIN_SHAMAN, x,y);

    UrchinShamanSubProcess(unit);
    SetCallback(unit,3,onShamanSight);
    AggressionLevel(unit, 1.0);
    return unit;
}

int GetGeneratorMonsterSpecHP(short ty, int alt)
{
	short *pHP;

	if (!pHP)
	{
		short hpTable[97];

		hpTable[OBJ_SWORDSMAN%97] = 325;
		hpTable[OBJ_BEAR%97] = 380;
		hpTable[OBJ_BLACK_BEAR%97] = 325;
		hpTable[OBJ_SKELETON%97] = 225;
		hpTable[OBJ_SKELETON_LORD%97] = 295;
		hpTable[OBJ_GRUNT_AXE%97] = 230;
		hpTable[OBJ_OGRE_BRUTE%97] = 325;
		hpTable[OBJ_OGRE_WARLORD%97] = 360;
		hpTable[OBJ_SHADE%97] = 128;
		hpTable[OBJ_HORRENDOUS%97] = 295;
		hpTable[OBJ_MECHANICAL_GOLEM%97] = 800;
		hpTable[OBJ_EMBER_DEMON%97] = 225;
		hpTable[OBJ_TROLL%97] = 385;
		pHP=hpTable;
	}
	int res =pHP[ty%97];
	if (!res)
		return alt;
	
	return res;
}

void onWhiteExplosionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 70, DAMAGE_TYPE_BLADE);
        }
    }
}

void whiteExplosion(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int dur=GetDirection(sub);
        if (dur)
        {
            PushTimerQueue(1,sub, whiteExplosion);
            LookWithAngle(sub,dur-1);
            return;
        }
        int owner=GetOwner(sub);
        if (CurrentHealth(owner))
        {
            SplashDamageAtEx(owner, GetObjectX(sub),GetObjectY(sub), 130.0, onWhiteExplosionSplash);
            CastSpellObjectObject("SPELL_TURN_UNDEAD", sub,sub);
        }
        Delete(sub);
    }
}

void onWizRedAttack()
{
    float x=GetObjectX(OTHER), y=GetObjectY(OTHER);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);

    PushTimerQueue(1, sub, whiteExplosion);
    LookWithAngle(sub, 21);
    Effect("COUNTERSPELL_EXPLOSION", x,y,0.0,0.0);
    SetOwner(SELF, sub);
}

int WizardRedBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[17] = 325; arr[18] = 100; 
		arr[19] = 80; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; 
		arr[28] = 1128792064; arr[29] = 1; arr[32] = 16; arr[33] = 26; arr[53] = 1128792064; 
		arr[54] = 4; arr[59] = 5542784; 
	pArr = arr;
    CustomMeleeAttackCode(arr, onWizRedAttack);
	return pArr;
}

void WizardRedSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 1325;	hpTable[1] = 1325;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WizardRedBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateWizardRed(float x, float y)
{
	int wiz=CreateObjectById(OBJ_WIZARD_RED,x,y);

	WizardRedSubProcess(wiz);
	return wiz;
}
