
#include "subway_utils.h"
#include "subway_reward.h"
#include "libs/spellutil.h"
#include "libs/voiceList.h"

void onBodyGuardLoopInterval(int *d){
    int cre=d[0];

    if (CurrentHealth(cre))
    {
        int owner=GetOwner(cre);
        if (MaxHealth(owner)){
            PushTimerQueue(13,d,onBodyGuardLoopInterval);
            if (!IsVisibleOr(cre,owner))
            {
                MoveObject(cre,GetObjectX(owner)+UnitAngleCos(owner, 13.0),GetObjectY(owner)+UnitAngleSin(owner,13.0));
                AggressionLevel(cre,1.0);
                CreatureFollow(cre,owner);
            }
            return;
        }
        Delete(cre);
    }
    d[0]=0;
}

void onBodyGuardDeath(){
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    Delete(SELF);
    DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION,x,y), 12);
}
void onBodyGuardHurt(){
    Enchant(SELF,EnchantList(ENCHANT_INVULNERABLE),3.0);
    RestoreHealth(SELF, 40);
}

void SummonBodyGuard(int owner, int *credata, int spawnFn){
    float xy[]={GetObjectX(owner),GetObjectY(owner)};
    int *arg=xy;
    int s= Bind(spawnFn,arg);

    credata[0]=s;
    SetOwner(owner,s);
    AttachHealthbar(s);
    SetUnitFlags(s,GetUnitFlags(s)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(s,5,onBodyGuardDeath);
    SetCallback(s,7,onBodyGuardHurt);
    PushTimerQueue(1,credata,onBodyGuardLoopInterval);
}

void onMedSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
            Damage(OTHER, SELF, 135, DAMAGE_TYPE_PLASMA);
    }
}

void onMedAttack(){
    if (!GetCaller())
        return;
    float x=GetObjectX(OTHER),y=GetObjectY(OTHER);
    DeleteObjectTimer( CreateObjectById(OBJ_METEOR_EXPLODE, x,y), 12);
    SplashDamageAtEx(SELF,x,y,85.0, onMedSplash);
}

int WoundedWarriorBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1466197348; arr[2] = 1769108065; arr[3] = 29295; arr[17] = 1500; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1118437376; arr[29] = 30; arr[32] = 15; arr[33] = 15; arr[57] = 5545344; 
		arr[59] = 5542784; arr[60] = 2272; arr[61] = 46912512; 
    CustomMeleeAttackCode(arr, onMedAttack);
	pArr = arr;
	return pArr;
}

void WoundedWarriorSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 3900;	hpTable[1] = 3900;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedWarriorBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CreateThoothOni(float x,float y){
    int s=CreateObjectById(OBJ_WOUNDED_WARRIOR,x,y);
    WoundedWarriorSubProcess(s);
    return s;
}

void SingleMagicMissileShot(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));
    float dt = Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(target), GetObjectY(target));
    float vectX = UnitRatioX(target, ptr, dt), vectY = UnitRatioY(target, ptr, dt);

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        MoveObject(ptr, GetObjectX(target) + (vectX * GetObjectZ(ptr + 1)), GetObjectY(target) + (vectY * GetObjectZ(ptr + 1)));
        if (IsVisibleTo(ptr, owner))
            SingleMissileShooter(owner, ptr);
        else
            SingleMissileShooter(owner, target);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void SingleMissileShooter(int owner, int target)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));

    SetOwner(owner, unit);
    ShotSingleMagicMissile(unit, target);
    DeleteObjectTimer(unit, 60);
}

void ShotSingleMagicMissile(int unit, int target)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));

    CastSpellObjectLocation("SPELL_MAGIC_MISSILE", unit, GetObjectX(target), GetObjectY(target));
    Delete(ptr);
    Delete(ptr + 2);
    Delete(ptr + 3);
    Delete(ptr + 4);
}

void MonsterCommonHitEvent()
{
    if (GetCaller())
        return;

    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 3, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectById(OBJ_GREEN_PUFF, GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void UnitDeathHandler()
{
    float x= GetObjectX(SELF),y= GetObjectY(SELF);

    if (Random(0,2))
        CreateRandomItemCommon(CreateObjectById(OBJ_MOVER, x,y));
    DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION,x,y), 6);
    DeleteObjectTimer(SELF, 3);
}

void monsterCommonSettings(int mon){
    AggressionLevel(mon,1.0);
    RetreatLevel(mon,0.0);
    LookWithAngle(mon,Random(0,255));
    SetCallback(mon,5,UnitDeathHandler);
    SetCallback(mon,7,MonsterCommonHitEvent);
    SetOwner(GetMasterUnit(),mon);
}

/*
MonsterSet
num, name
0   SmallSpider
1   Imp
2   Swordsman
3   GiantLeech
4   Wolf
5   Archer
6   BlackWidow
7   PurpleMaiden
8   Mystic
9   RWiz
10  Necromancer
11  Lich
12  GWiz
13  DarkBear
14  Goon
15  Bear
16  OgreBrute
17  Ogreaxe
18  OgreLord
19  FireSprite
20  EmberDemon
21  Bat
22  MecaFly
23  EvilCherub
24  Shade
25  Wisp
26  BlackWolf
27  Urchin
28 shaman
29  Skeleton
30  SkeletonLord
31  Ghost
32  Scorpion
33  Spider
34  AlbinoSpider
35    SmallAlbinoSpider
36  WeirdlingBeast
37    Mimic
38  StoneGolem
39  MechanicalGolem
40  Troll
41  Beholder
42  CarnivorousPlant
43  MeleeDemon
44  Wasp
45  SpittingSpider
46  VileZombie
47 Zombie
48 AirshipCaptain
49 Horrendous
50 hecubah
*/

int SpawnSmallSpider(int ptr)
{
    int unit = CreateObjectById(OBJ_SMALL_SPIDER, GetObjectX(ptr), GetObjectY(ptr));

    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    SetUnitMaxHealth(unit, 68);
    monsterCommonSettings(unit);
    SetUnitVoice(unit,MONSTER_VOICE__MaleNPC2);
    return unit;
}

int SpawnImp(int ptr)
{
    int unit = CreateObjectAt("Swordsman", GetObjectX(ptr), GetObjectY(ptr));

    Enchant(unit, "ENCHANT_SLOWED", 0.0);    
    SetUnitMaxHealth(unit, 64);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnSwordman(int ptr)
{
    int unit = CreateObjectAt("Swordsman", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 325);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnLeech(int ptr)
{
    int unit = CreateObjectAt("GiantLeech", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 96);
    monsterCommonSettings(unit);
    SetUnitVoice(unit,MONSTER_VOICE_MechanicalGolem);
    return unit;
}

int SpawnWolf(int ptr)
{
    int unit = CreateObjectAt("Wolf", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 128);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnArcher(int ptr)
{
    int unit = CreateObjectAt("Archer", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 96);
    monsterCommonSettings(unit);
    return unit;
}

void onminiSpiderDeath(){
    DeleteObjectTimer(SELF,3);
}

void spreadMiniSpider(int sub){
    if (ToInt(GetObjectX(sub))){
        int dur=GetDirection(sub);
        if (--dur>=0){
            PushTimerQueue(3,sub,spreadMiniSpider);
            LookWithAngle(sub,dur);
            int mini=CreateObjectById(OBJ_SMALL_SPIDER,GetObjectX(sub),GetObjectY(sub));
            monsterCommonSettings(mini);
            SetCallback(mini,5,onminiSpiderDeath);
            return;
        }
        Delete(sub);
    }
}

void BlackSpiderDead()
{
    float x= GetObjectX(SELF),y= GetObjectY(SELF);
    PlaySoundAround(SELF,SOUND_BeholderDie);
    DeleteObjectTimer(CreateObjectById(OBJ_WATER_BARREL_BREAKING,x,y ), 12);
    int sub=CreateObjectById(OBJ_BIG_SMOKE,x,y);
    LookWithAngle(sub,3);
    PushTimerQueue(1,sub,spreadMiniSpider);
    UnitDeathHandler();
}

int SpawnBlackSpider(int ptr)
{
    int unit = CreateObjectAt("BlackWidow", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, BlackWidowBinTable());
    SetUnitMaxHealth(unit, 295);
    monsterCommonSettings(unit);
    SetCallback(unit, 5, BlackSpiderDead);
    return unit;
}

void PurpleGirlDetectHandler()
{
    AbsoluteTargetStrike(GetTrigger(), GetCaller(), DistanceUnitToUnit(SELF, OTHER) / 30.0, SingleMagicMissileShot);
    Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void PurpleGirlLostEnemyOnSight()
{
    EnchantOff(SELF, "ENCHANT_BLINDED");
}

int SpawnVioletMaiden(int ptr)
{
    int unit= ColorMaidenAt(240, 0, 225, GetObjectX(ptr), GetObjectY(ptr));

    SetUnitScanRange(unit, 400.0);
    SetUnitMaxHealth(unit, 260);
    monsterCommonSettings(unit);
    // SetCallback(unit, 3, PurpleGirlDetectHandler);
    // SetCallback(unit, 13, PurpleGirlLostEnemyOnSight);
    return unit;
}

int SummonMystic(int target)
{
    int unit = CreateObjectAt("Wizard", GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 237);
    monsterCommonSettings(unit);    
    //SetCallback(unit, 8, WizRunAway);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_BLINK"), 0);
    }
    return unit;
}

int SummonRedWiz(int target)
{
    int unit = CreateObjectAt("WizardRed", GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    UnitLinkBinScript(unit, WizardRedBinTable());
    SetUnitMaxHealth(unit, 435);
    SetCallback(unit, 8, WizRunAway);
    UnitZeroFleeRange(unit);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ MON_STATUS_CAN_CAST_SPELLS);
    monsterCommonSettings(unit);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
		SetMemory(uec + 0x520, ToInt(400.0));
		uec += 0x5d0;
		SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
    }
    return unit;
}

int SummonNecromancer(int target)
{
    int unit = CreateObjectAt("Necromancer", GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 308);
    SetCallback(unit, 8, WizRunAway);
    monsterCommonSettings(unit);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_LIGHTNING"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x8000000);
    }
    return unit;
}

int SummonLich(int target)
{
    int unit = CreateObjectAt("Lich", GetObjectX(target), GetObjectY(target));

    UnitLinkBinScript(unit, LichLordBinTable());
    UnitZeroFleeRange(unit);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    SetUnitMaxHealth(unit, 405);
    monsterCommonSettings(unit);
    return unit;
}

int SummonGreenWiz(int target)
{
    int unit = CreateObjectAt("WizardGreen", GetObjectX(target), GetObjectY(target));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 265);
    SetCallback(unit, 8, WizRunAway);
    monsterCommonSettings(unit);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_METEOR"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_CONFUSE"), 0x20000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
        SetMemory(uec + GetSpellNumber("SPELL_POISON"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_PIXIE_SWARM"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SUMMON_URCHIN_SHAMAN"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_FIST"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        SetMemory(uec + GetSpellNumber("SPELL_BLINK"), 0);
    }
    return unit;
}

int SpawnDarkbear(int ptr)
{
    int unit = CreateObjectAt("BlackBear", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 325);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnGoons(int ptr)
{
    int unit = CreateObjectAt("Goon", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, GoonBinTable());
    SetUnitMaxHealth(unit, 225);
    monsterCommonSettings(unit);
    SetUnitVoice(unit,MONSTER_VOICE__FireKnight1);
    return unit;
}

int SpawnBear(int ptr)
{
    int unit = CreateObjectAt("Bear", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 360);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnOgre(int ptr)
{
    int unit = CreateObjectAt("OgreBrute", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 325);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnOgreAxe(int ptr)
{
    int unit = CreateObjectAt("GruntAxe", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 225);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnOgreLord(int ptr)
{
    int unit = CreateObjectAt("OgreWarlord", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 360);
    monsterCommonSettings(unit);
    return unit;
}

int FireSpriteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 185; arr[19] = 82; 
		arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 13; arr[56] = 21; 
		arr[58] = 5545472; 
	pArr = arr;
	return pArr;
}

void FireSpriteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075671204;		ptr[137] = 1075671204;
	int *hpTable = ptr[139];
	hpTable[0] = 185;	hpTable[1] = 185;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = FireSpriteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpawnFireSprite(int ptr)
{
    int unit = CreateObjectById(OBJ_FIRE_SPRITE, GetObjectX(ptr), GetObjectY(ptr));

    FireSpriteSubProcess(unit);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnEmberDemon(int ptr)
{
    int unit = CreateObjectAt("EmberDemon", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 128);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnBat(int ptr)
{
    int unit = CreateObjectAt("Bat", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 64);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnMecaFly(int ptr)
{
    int unit = CreateObjectAt("FlyingGolem", GetObjectX(ptr), GetObjectY(ptr));

    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    SetUnitMaxHealth(unit, 64);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnGargoyle(int ptr)
{
    int unit = CreateObjectAt("EvilCherub", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 64);monsterCommonSettings(unit);

    return unit;
}

int SpawnShade(int ptr)
{
    int unit = CreateObjectAt("Shade", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 160);monsterCommonSettings(unit);

    return unit;
}

int SpawnWisp(int ptr)
{
    int unit = CreateObjectAt("WillOWisp", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 420);monsterCommonSettings(unit);
    SetCallback(unit, 8, WizRunAway);

    return unit;
}

int SpawnBlackWolf(int ptr)
{
    int unit = CreateObjectAt("BlackWolf", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 225);monsterCommonSettings(unit);

    return unit;
}

int UrchinBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 28265; arr[17] = 160; arr[19] = 85; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[37] = 1751607628; arr[38] = 1852403316; 
		arr[39] = 1819230823; arr[40] = 116; arr[53] = 1128792064; arr[54] = 4; arr[55] = 20; 
		arr[56] = 30; arr[60] = 1339; arr[61] = 46904832; 
	pArr = arr;
	return pArr;
}

void UrchinSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 160;	hpTable[1] = 160;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = UrchinBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpawnUrchin(int ptr)
{
    int unit = CreateObjectById(OBJ_URCHIN, GetObjectX(ptr), GetObjectY(ptr));

    UrchinSubProcess(unit);
    monsterCommonSettings(unit);
    SetUnitVoice(unit,MONSTER_VOICE_Beholder);
    return unit;
}

int SpawnShaman(int ptr)
{
    int unit = CreateObjectAt("UrchinShaman", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 108);monsterCommonSettings(unit);
    SetCallback(unit, 8, WizRunAway);

    return unit;
}

int SpawnSkeleton(int ptr)
{
    int unit = CreateObjectAt("Skeleton", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 225);monsterCommonSettings(unit);

    return unit;
}

int SpawnSkeletonLord(int ptr)
{
    int unit = CreateObjectAt("SkeletonLord", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 280);monsterCommonSettings(unit);

    return unit;
}

int SpawnGhost(int ptr)
{
    int unit = CreateObjectAt("Ghost", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 250);monsterCommonSettings(unit);

    return unit;
}

int SpawnScorpion(int ptr)
{
    int unit = CreateObjectAt("Scorpion", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 250);monsterCommonSettings(unit);

    return unit;
}

int SpawnNormalSpider(int ptr)
{
    int unit = CreateObjectAt("Spider", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 192);monsterCommonSettings(unit);

    return unit;
}

int SpawnWhiteSpider(int ptr)
{
    int unit = CreateObjectAt("AlbinoSpider", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 128);
    monsterCommonSettings(unit);
    SetUnitVoice(unit, MONSTER_VOICE__Maiden1);
    return unit;
}

int SpawnMiniWhiteSpider(int ptr)
{
    int unit = CreateObjectAt("SmallAlbinoSpider", GetObjectX(ptr), GetObjectY(ptr));

    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    SetUnitMaxHealth(unit, 130);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnBeast(int ptr)
{
    int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(ptr), GetObjectY(ptr));

    UnitZeroFleeRange(unit);
    UnitLinkBinScript(unit, WeirdlingBeastBinTable());
    SetUnitMaxHealth(unit, 256);
    monsterCommonSettings(unit);
    SetUnitVoice(unit,MONSTER_VOICE_Mimic);
    return unit;
}

int SpawnMimic(int ptr)
{
    int unit = CreateObjectAt("Mimic", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 700);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnRockGolem(int ptr)
{
    int unit = CreateObjectAt("StoneGolem", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 800);monsterCommonSettings(unit);

    return unit;
}

int SpawnMecaGolem(int ptr)
{
    int unit = CreateObjectAt("MechanicalGolem", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 900);monsterCommonSettings(unit);

    return unit;
}

int SpawnTroll(int ptr)
{
    int unit = CreateObjectAt("Troll", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 325);monsterCommonSettings(unit);

    return unit;
}

int SpawnBeholder(int ptr)
{
    int unit = CreateObjectAt("Beholder", GetObjectX(ptr), GetObjectY(ptr));

    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    SetUnitMaxHealth(unit, 425);monsterCommonSettings(unit);
    return unit;
}

int CarnivorousPlantBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 480; 
		arr[19] = 85; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[27] = 5; 
		arr[28] = 1109393408; arr[29] = 100; arr[31] = 8; arr[59] = 5542784; arr[60] = 1371; 
		arr[61] = 46901760; 
	pArr = arr;
	return pArr;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 680;	hpTable[1] = 680;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = CarnivorousPlantBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpawnPlant(int ptr)
{
    int unit = CreateObjectAt("CarnivorousPlant", GetObjectX(ptr), GetObjectY(ptr));

    CarnivorousPlantSubProcess(unit);
    SetUnitScanRange(unit, 420.0);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnMeleeDemon(int ptr)
{
    int unit = CreateObjectAt("MeleeDemon", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 225);monsterCommonSettings(unit);

    return unit;
}

int SpawnWasp(int ptr)
{
    int unit = CreateObjectAt("Wasp", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 225);monsterCommonSettings(unit);

    return unit;
}

int SpawnWebSpider(int ptr)
{
    int unit = CreateObjectAt("SpittingSpider", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitMaxHealth(unit, 120);monsterCommonSettings(unit);

    return unit;
}

int SpawnVileZombie(int ptr)
{
    int unit = CreateObjectAt("VileZombie", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitSpeed(unit, 1.28);
    SetUnitMaxHealth(unit, 325);
    monsterCommonSettings(unit);
    return unit;
}

int SpawnZombie(int ptr)
{
    int unit = CreateObjectAt("Zombie", GetObjectX(ptr), GetObjectY(ptr));

    monsterCommonSettings(unit);
    SetUnitMaxHealth(unit, 225);
    return unit;
}

void JandorShotGun(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), mis;
    float dt = DistanceUnitToUnit(ptr, target);
    float vectX = UnitRatioX(target, ptr, dt), vectY = UnitRatioY(target, ptr, dt);

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        mis = CreateObjectAt("OgreShuriken", GetObjectX(owner) + UnitRatioX(target, owner, 14.0), GetObjectY(owner) + UnitRatioY(target, owner, 14.0));
        SetOwner(owner, mis);
        MoveObject(ptr, GetObjectX(target) + (vectX * GetObjectZ(ptr + 1)), GetObjectY(target) + (vectY * GetObjectZ(ptr + 1)));
        if (IsVisibleTo(ptr, owner))
        {
            PushObject(mis, -40.0, GetObjectX(ptr), GetObjectY(ptr));
        }
        else
        {
            PushObject(mis, -40.0, GetObjectX(target), GetObjectY(target));
        }
        Enchant(mis, "ENCHANT_SHOCK", 0.0);
    }
    Delete(ptr);
}

void CaptainDetectHandler()
{
	if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
	{
		Enchant(SELF, "ENCHANT_VILLAIN", 0.8);
		AbsoluteTargetStrike(GetTrigger(), GetCaller(), DistanceUnitToUnit(SELF, OTHER) / 66.0, JandorShotGun);
	}
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

int SpawnJandor(int ptr)
{
    int unit = CreateObjectAt("AirshipCaptain", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, AirshipCaptainBinTable());
    SetUnitScanRange(unit, 450.0);
    monsterCommonSettings(unit);
    SetUnitMaxHealth(unit, 550);
    SetCallback(unit, 3, CaptainDetectHandler);
    SetCallback(unit, 13, PurpleGirlLostEnemyOnSight);
    return unit;
}

int SpawnHorrendous(int ptr)
{
    int unit = CreateObjectAt("Horrendous", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitSpeed(unit, 1.1);
    monsterCommonSettings(unit);
    SetUnitMaxHealth(unit, 500);
    return unit;
}

int SpawnOrbHecubah(int ptr)
{
    int unit = CreateObjectAt("Hecubah", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, HecubahBinTable());
    SetUnitScanRange(unit, 480.0);
    monsterCommonSettings(unit);
    SetUnitMaxHealth(unit, 900);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ MON_STATUS_CAN_CAST_SPELLS);
    UnitZeroFleeRange(unit);
    return unit;
}

void DemonDeadHandler()
{
    int lord;
    QueryDLord(&lord,0,DEMON_MAX_COUNT);
    CheckDemonDeathCount(++lord);
    QueryDLord(0,lord,DEMON_MAX_COUNT);
    Delete(GetTrigger() + 1);
    UnitDeathHandler();
}

void DemonSightEvent()
{
    if (DistanceUnitToUnit(SELF, OTHER) < 98.0)
	{
		DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
		Damage(OTHER, SELF, 35, 0);
	}
	if (GetCaller() ^ ToInt(GetObjectZ(GetTrigger() + 1)))
	{
		CreatureFollow(SELF, OTHER);
		Raise(GetTrigger() + 1, ToFloat(GetCaller()));
	}
	MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void DemonResetSight()
{
	int target = ToInt(GetObjectZ(GetTrigger() + 1));

	EnchantOff(SELF, "ENCHANT_BLINDED");
	if (!CurrentHealth(target))
	{
		Raise(GetTrigger() + 1, ToFloat(0));
		CreatureIdle(SELF);
	}
}

int SpawnDragon(int ptr)
{
    int unit = CreateObjectAt("Demon", GetObjectX(ptr), GetObjectY(ptr));

    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    SetUnitScanRange(unit, 480.0);
    monsterCommonSettings(unit);
    SetUnitMaxHealth(unit, 1250);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020); //ALWAYS_RUN
    SetCallback(unit, 3, DemonSightEvent);
    SetCallback(unit, 5, DemonDeadHandler);
    SetCallback(unit, 13, DemonResetSight);
    return unit;
}

void QueryMonsterFunctions(int *getFn, int n, int *setFnArray){
    int *pfn;

    if (getFn){
        getFn[0]=pfn[n];
        return;
    }
    pfn=setFnArray;
}

void InitializeMonsterFunctions(){
    int arr[]={
        SpawnSmallSpider,        SpawnImp,        SpawnSwordman,        SpawnLeech,        SpawnWolf,
        SpawnArcher,        SpawnBlackSpider,        SpawnVioletMaiden,        SummonMystic,        SummonRedWiz,
        SummonNecromancer,        SummonLich,        SummonGreenWiz,        SpawnDarkbear,        SpawnGoons,
        SpawnBear,        SpawnOgre,        SpawnOgreAxe,        SpawnOgreLord,        SpawnFireSprite,
        SpawnEmberDemon,        SpawnBat,        SpawnMecaFly,        SpawnGargoyle,        SpawnShade,
        SpawnWisp,        SpawnBlackWolf,        SpawnUrchin,        SpawnShaman,        SpawnSkeleton,
        SpawnSkeletonLord,        SpawnGhost,        SpawnScorpion,        SpawnNormalSpider,        SpawnWhiteSpider,
        SpawnMiniWhiteSpider,        SpawnBeast,        SpawnMimic,        SpawnRockGolem,        SpawnMecaGolem,
        SpawnTroll,        SpawnBeholder,        SpawnPlant,        SpawnMeleeDemon,        SpawnWasp,
        SpawnWebSpider,        SpawnVileZombie,        SpawnZombie,        SpawnJandor,        SpawnHorrendous,
        SpawnOrbHecubah,        SpawnDragon,
            };
    QueryMonsterFunctions(0,0,arr);
}
