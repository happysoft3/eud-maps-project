
#include "libs/define.h"
#include "libs/unitutil.h"
#include "libs/mathlab.h"

static void OnCircleRotCollide()
{
    if (IsCaller(GetTrigger()-1))
    {
        int angle=GetDirection(SELF)*2;

        MoveObjectVector(OTHER, MathSine(angle+90, 3.2), MathSine(angle , 3.2));
        LookWithAngle(SELF, (GetDirection(SELF)+1)%180);
        MoveObject(SELF, GetObjectX(OTHER), GetObjectY(OTHER));
    }
}

int CreateCircleRot(float xpos, float ypos)
{
    int rot = CreateObjectAt("RotatingSpikes", xpos, ypos);
    int u= CreateObjectAt("Bomber", GetObjectX(rot), GetObjectY(rot));

    Damage(u, 0, 999, -1);
    Frozen(u, TRUE);
    LookWithAngle(u, 0);
    SetCallback(u, 9, OnCircleRotCollide);
    return u;
}

static void deferredInit()
{
    CreateCircleRot(GetWaypointX(10), GetWaypointY(10));
}

void MapInitialize()
{
    FrameTimer(1, deferredInit);
}
