
#include "r231103_gvar.h"
#include "r231103_utils.h"
#include "libs/queueTimer.h"
#include "libs/wallutil.h"
#include "libs/printutil.h"
#include "libs/waypoint.h"
#include "libs/bind.h"

void OnEntityDead(int entity, float x, float y) //virtual
{ }

// #define ENTITY_ACTION_NOTHING 0
// #define ENTITY_ACTION_MOVE 1
// #define ENTITY_ACTION_ATTACK 2
// #define ENTITY_ACTION_COMPLETE 3

void removeEntity(int entity)
{
    Delete(entity++);
    Delete(entity++);
}

void OnEntityCoreDead()
{
    if (MaxHealth(SELF))
    {
        if (CurrentHealth(SELF))
            return;
        float x=GetObjectX(SELF),y=GetObjectY(SELF);

        OnEntityDead(GetTrigger(), x, y);
        removeEntity(GetTrigger());
        DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION, x,y), 9);
    }
}

int onEntityMoving(int mon)
{
    if (!MaxHealth(mon))
        return FALSE;

    int target = GetUnit1C(mon);

    if (target)
        return FALSE;

    int sub=mon+1;
    int dur=GetUnit1C(sub);

    if (dur)
    {
        float force = RandomFloat(2.0, 4.0);
        SetUnit1C(sub, dur-1);
        PushObjectTo(mon, UnitAngleCos(sub, force), UnitAngleSin(sub, force));
        MoveObject(sub, GetObjectX(mon), GetObjectY(mon));
        return TRUE;
    }
    return FALSE;
}

void onEntityDefaultCollide()
{
    return;
}

void onEntityAttackCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 12, 1);
            Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        if (GetUnit1C(SELF))
            SetUnit1C(SELF, 0);
        break;
    }
}

void onEntityAttack(int mon)
{
    if (!MaxHealth(mon))
        return;

    if (GetUnit1C(mon))
    {
        int sub=mon+1;
        int dur=GetUnit1C(sub);

        if (--dur>=0)
        {
            PushTimerQueue(1, mon, onEntityAttack);
            SetUnit1C(sub, dur);
            PushObjectTo(mon, UnitAngleCos(sub, 5.0), UnitAngleSin(sub, 5.0));
            return;
        }
        SetUnit1C(mon, 0);
    }
    SetCallback(mon, 9, onEntityDefaultCollide);
}

void entityAttackMelee(int mon, int target, int sub)
{
    LookAtObject(sub, target);
    SetUnit1C(sub, 42);
    SetCallback(mon, 9, onEntityAttackCollide);
    PushTimerQueue(1, mon, onEntityAttack);
}

void onEntityPauseDuration(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int mon=GetOwner(sub);

        if (MaxHealth(mon))
        {
            int dur=GetDirection(sub);

            if (dur)
            {
                PushTimerQueue(1, sub, onEntityPauseDuration);
                LookWithAngle(sub,dur-1);
                return;
            }
            SetUnit1C(mon, 0);
        }
        Delete(sub);
    }
}

void entityMeleePause2(int mon, int duration)
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(mon),GetObjectY(mon));

    PushTimerQueue(1, sub, onEntityPauseDuration);
    LookWithAngle(sub,duration);
    SetOwner(mon,sub);
}

void onEntityMeleeAttack2(int mon)
{
    if (!MaxHealth(mon))
        return;
    int target=GetUnit1C(mon);

    while (CurrentHealth(target))
    {
        if (DistanceUnitToUnit(mon, target) < 81.0)
        {
            Damage(target, mon, 30, DAMAGE_TYPE_BLADE);
            DeleteObjectTimer(CreateObjectById(OBJ_METEOR_EXPLODE, GetObjectX(target), GetObjectY(target)), 9);
            entityMeleePause2(mon, 30);
            break;
        }
        PushTimerQueue(1, mon, onEntityMeleeAttack2);
        PushObjectTo(mon, UnitRatioX(target,mon, 5.0), UnitRatioY(target,mon, 5.0));
        return;
    }
    SetUnit1C(mon, 0);
}

void entityMeleeAttack2(int mon, int target, int sub)
{
    PushTimerQueue(1, mon, onEntityMeleeAttack2);
}

void tryBlueOrbPickup()
{
    GreenSparkFx(GetObjectX(SELF), GetObjectY(SELF));
}

void onBlueOrbMoving(int orb)
{
    if (ToInt(GetObjectX(orb)))
    {
        int mob = GetOwner(orb);

        if (mob)
        {
            int target=GetUnit1C(orb);

            while (CurrentHealth(target))
            {
                if (DistanceUnitToUnit(orb,target)<26.0)
                {
                    Damage(target, mob, 10, DAMAGE_TYPE_FLAME);
                    GreenSparkFx(GetObjectX(orb), GetObjectY(orb));
                    break;
                }
                int dur=GetDirection(orb);

                if (dur)
                {
                    PushTimerQueue(1, orb, onBlueOrbMoving);
                    LookWithAngle(orb, dur-1);
                    MoveObjectVector(orb, UnitRatioX(target, orb, 11.0), UnitRatioY(target, orb, 11.0));
                    return;
                }
                break;
            }
            SetUnit1C(mob, 0);
        }
        Delete(orb);
    }
}

void entityShooting(int mon, int target, int sub)
{
    int orb=CreateObjectById(OBJ_BLUE_ORB, GetObjectX(mon), GetObjectY(mon));

    PushTimerQueue(1, orb, onBlueOrbMoving);
    SetUnitCallbackOnPickup(orb,tryBlueOrbPickup);
    LookWithAngle(orb, 32);
    SetUnit1C(orb, target);
    SetOwner(mon, orb);
    SetUnitFlags(orb, GetUnitFlags(orb)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    Frozen(orb, TRUE);
}

void onThunderStrikeSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(OTHER, SELF))
                Damage(OTHER, SELF, 8, DAMAGE_TYPE_ELECTRIC);
        }
    }
}

void onEntityThunderStrike(int mark)
{
    if (ToInt(GetObjectX(mark)))
    {
        int mon = GetUnit1C(mark);

        if (MaxHealth(mon))
        {
            int target=GetUnit1C(mon);

            if (CurrentHealth(target))
            {
                int dur=GetDirection(mark);

                if (dur)
                {
                    LookWithAngle(mark,dur-1);
                    PushTimerQueue(1,mark,onEntityThunderStrike);
                    if (DistanceUnitToUnit(mon,target)>108.0)
                        PushObjectTo(mon, UnitRatioX(target,mon,4.0),UnitRatioY(target,mon, 4.0));
                    return;
                }
            }
            float x=GetObjectX(mark),y=GetObjectY(mark);

            SplashDamageAtEx(mon,x,y,46.0,onThunderStrikeSplash);
            Effect("LIGHTNING", x,y,x,y-160.0);
            Effect("RICOCHET",x,y,0.0,0.0);
            SetUnit1C(mon,0);
        }
        Delete(mark);
    }
}

void entityThunderStrike(int mon, int target, int sub)
{
    int mark=CreateObjectById(OBJ_BLUE_SUMMONS, GetObjectX(target),GetObjectY(target));

    PushTimerQueue(1,mark,onEntityThunderStrike);
    SetUnit1C(mark, mon);
    LookWithAngle(mark,18);
}
#define IMPACT_FORCE 98.0
void afterTeleportingAttack(int wake)
{
    int mon=GetUnit1C(wake);

    if (!MaxHealth(mon))
        return;

    int target=GetUnit1C(mon);
    int sub=mon+1;

    while (CurrentHealth(target))
    {
        if (!IsVisibleOr(target,mon))
        {
            Effect("SMOKE_BLAST",GetObjectX(mon),GetObjectY(mon),0.0,0.0);
            MoveObject(mon,GetObjectX(wake),GetObjectY(wake));
        }

        if (DistanceUnitToUnit(target,mon)<46.0)
        {
            Damage(target,mon,10,DAMAGE_TYPE_BLADE);
            Effect("COUNTERSPELL_EXPLOSION",GetObjectX(target),GetObjectY(target),0.0,0.0);
            PushObjectTo(target,UnitRatioX(target,mon,IMPACT_FORCE), UnitRatioY(target,mon,IMPACT_FORCE));
        }
        break;
    }
    SetUnit1C(mon, 0);
    Delete(wake);
}

void entityTeleportingAttack(int mon, int target, int sub)
{
    int wake=CreateObjectById(OBJ_TELEPORT_WAKE,GetObjectX(mon),GetObjectY(mon));
    int ptr=UnitToPtr(wake);
    float *xy=GetMemory(ptr+0x2bc);

    xy[0]=GetObjectX(target)+UnitAngleCos(target,21.0);
    xy[1]=GetObjectY(target)+UnitAngleSin(target,21.0);
    SetUnit1C(wake,mon);
    PushTimerQueue(6,wake,afterTeleportingAttack);
}

void onRushCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 34, DAMAGE_TYPE_FLAME);
            Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void onRushAttack(int mark)
{
    int mon=GetUnit1C(mark);

    if (!MaxHealth(mon))
    {
        Delete(mark);
        return;
    }

    int dur=GetDirection(mark);

    if (dur)
    {
        int sub=mon+1;

        PushTimerQueue(1, mark, onRushAttack);
        LookWithAngle(mark,dur-1);
        PushObjectTo(mon, UnitAngleCos(sub,9.0),UnitAngleSin(sub,9.0));
        int dam=DummyUnitCreateById(OBJ_BOMBER,GetObjectX(mon),GetObjectY(mon));

        UnitNoCollide( CreateObjectById(OBJ_IMP_SHOT,GetObjectX(dam),GetObjectY(dam)) );
        SetCallback(dam,9,onRushCollide);
        SetUnitFlags(dam,GetUnitFlags(dam)^UNIT_FLAG_NO_PUSH_CHARACTERS);
        DeleteObjectTimer(dam,1);
        SetOwner(mon,dam);
        return;
    }
    // SetUnit1C(mon,0);
    entityMeleePause2(mon, 60);
    Effect("SMOKE_BLAST",GetObjectX(mon),GetObjectY(mon),0.0,0.0);
    MoveObject(mon,GetObjectX(mark),GetObjectY(mark));
    Delete(mark);
    Effect("COUNTERSPELL_EXPLOSION",GetObjectX(mon),GetObjectY(mon),0.0,0.0);
}

void entityRushAttack(int mon, int target, int sub)
{
    int mark=CreateObjectById(OBJ_PLAYER_WAYPOINT, GetObjectX(mon),GetObjectY(mon));

    PushTimerQueue(1,mark,onRushAttack);
    SetUnit1C(mark,mon);
    LookWithAngle(mark,31);
    LookAtObject(sub,target);
}

void onJumpSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(OTHER, SELF))
                Damage(OTHER, SELF, 50, DAMAGE_TYPE_CRUSH);
        }
    }
}

void onEntityJump(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int mon=GetUnit1C(sub);

        if (MaxHealth(mon))
        {
            int dur=GetDirection(sub);

            if (dur)
            {
                PushTimerQueue(1, sub, onEntityJump);
                MoveObjectVector(mon,GetObjectZ(sub),GetObjectZ(sub+1));
                LookWithAngle(sub, dur-1);

                int angle=GetDirection(sub+1);
                LookWithAngle(sub+1,angle+1);
                Raise(mon, MathSine(angle*5, 250.0));
                return;
            }
            float x=GetObjectX(mon), y=GetObjectY(mon);
            DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE, x,y ), 9);
            SplashDamageAtEx(mon,x,y,105.0, onJumpSplash);
            entityMeleePause2(mon, 48);
        }
        Delete(sub);
        Delete(sub+1);
    }
}

void entityJumpAttack(int mon, int target, int unused)
{
    float gap=DistanceUnitToUnit(mon,target);

    if (gap<13.0)
    {
        Damage(target,mon,20,DAMAGE_TYPE_IMPACT);
        SetUnit1C(mon, 0);
        return ;
    }
    float x=GetObjectX(mon),y=GetObjectY(mon);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);
    int sub2=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);

    LookWithAngle(sub, 35);
    Raise(sub, UnitRatioX(target,mon,gap/36.0));
    Raise(sub+1,UnitRatioY(target,mon,gap/36.0));
    SetUnit1C(sub,mon);
    PushTimerQueue(1, sub, onEntityJump);
}

void invokeEntityAttackFn(int fn, int mob, int target, int sub)
{
    Bind(fn, &fn + 4);
}

void EntityGoAttack(int mon, int target)
{
    if (GetUnit1C(mon))
        return;

    int sub=mon+1;
    int thingId = GetUnitThingID(mon);

    if (!thingId)
    {
        UniPrintToAll("***fatal error***");
        return;
    }
    
    MoveObject(sub, GetObjectX(mon), GetObjectY(mon));
    SetUnit1C(mon,target);
    // entityAttackMelee(mon, target, sub); //TODO: replace to function pointer
    invokeEntityAttackFn(GetMobAttackFunction(thingId%97), mon, target, sub);
    LookWithAngle(mon, (GetObjectX(mon)>GetObjectX(target)) * 128);
}

int mobReadyToMoving(int mon)
{
    if (!MaxHealth(mon))
        return FALSE;

    int sub=mon+1;

    if (GetUnit1C(sub))
        return TRUE;

    int dir=Random(1,255);
        
    LookWithAngle(sub, dir);
    LookWithAngle(mon, (dir>64 && dir<192) * 128);
    MoveObject(sub,GetObjectX(mon),GetObjectY(mon));    
    SetUnit1C(sub, Random(15, 30));
    return TRUE;
}

int createEntity(float x, float y)
{
    int ty=GetMobType();
    int mon=DummyUnitCreateById(ty, x,y);
    int ptr=UnitToPtr(mon);

    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);
    // createEntityCore(mon, 100);
    Frozen(mon, FALSE);
    SetCallback(mon, 9, onEntityDefaultCollide);
    SetUnit1C(mon, 0);
    SetOwner(GetMasterUnit(), mon);
    int hp=GetMobHP(ty%97);

    if (!hp)
        hp=100;
    SetUnitMaxHealth(mon, hp);
    SetUnitFlags(mon,GetUnitFlags(mon)^UNIT_FLAG_DEAD);
    // if (GetUnitFlags(mon)&UNIT_FLAG_AIRBORNE)
    //     SetUnitFlags(mon, GetUnit1C(mon)^(UNIT_FLAG_AIRBORNE|UNIT_FLAG_SHORT) );
    SetMemory(ptr+0x2cc, GetDummyDamageHandler());
    return mon;
}

#define MAX_MOB_ARRAY_SIZE 600

int monsterSubLoop(int *pMobs, int r, int init)
{
    int rep=100;
    int movingCount = 0;

    while (--rep>=0)
    {
        while (pMobs[r])
        {
            if (init)
            {
                if (!mobReadyToMoving(pMobs[r]))
                {
                    pMobs[r]=0;
                    break;
                }
            }
            movingCount += onEntityMoving(pMobs[r]);
            break;
        }
        r+=1;
    }
    if (!movingCount)
        return TRUE;
    return FALSE;
}

void OnMonsterLoop(int *pMobs)
{
    int r, c;

    if (r==MAX_MOB_ARRAY_SIZE)
        r=0;

    if (monsterSubLoop(pMobs, r, c==0))
    {
        c=0;
        r+=100;
    }
    else
        ++c;
    PushTimerQueue(1, pMobs, OnMonsterLoop);
}

void placingEntityMultiple(int *pArgs)
{
    int *pMobs = pArgs[1];
    float xRandom=RandomFloat(LocationX(27), LocationX(28));
    float yRandom=RandomFloat(LocationY(28), LocationY(29));

    if (--pArgs[0]>=0)
    {
        PushTimerQueue(1, pArgs, placingEntityMultiple);
        pMobs[pArgs[0]] = createEntity(xRandom, yRandom);
        return;
    }
    PushTimerQueue(1, pMobs, OnMonsterLoop);
}

void StartPlacingEntityMultiple()
{
    int mobArr[MAX_MOB_ARRAY_SIZE];
    int args[]={
        MAX_MOB_ARRAY_SIZE,
        mobArr,
    };
    PushTimerQueue(1, args, placingEntityMultiple);
}

void InitialSetMobData(short *pMobAttackFn, short *pMobHpTable, short *pMobTypes)
{
    pMobTypes[0]=OBJ_SWORDSMAN;
    pMobTypes[1]=OBJ_ARCHER;
    pMobTypes[2]=OBJ_SPIDER;
    pMobTypes[3]=OBJ_FLYING_GOLEM;
    pMobTypes[5]=OBJ_SMALL_SPIDER;
    pMobTypes[4]=OBJ_DEMON;
    pMobAttackFn[OBJ_SWORDSMAN%97]=entityAttackMelee;
    pMobAttackFn[OBJ_ARCHER%97]=entityShooting;
    pMobAttackFn[OBJ_SPIDER%97]=entityMeleeAttack2;
    pMobAttackFn[OBJ_FLYING_GOLEM%97]=entityThunderStrike;
    pMobAttackFn[OBJ_SMALL_SPIDER%97]=entityJumpAttack;
    pMobAttackFn[OBJ_DEMON%97]=entityRushAttack;
    pMobHpTable[OBJ_SWORDSMAN%97]=180;
    pMobHpTable[OBJ_ARCHER%97]=98;
    pMobHpTable[OBJ_SPIDER%97]=205;
    pMobHpTable[OBJ_FLYING_GOLEM%97]=138;
    pMobHpTable[OBJ_SMALL_SPIDER%97]=160;
    pMobHpTable[OBJ_DEMON%97]=220;
}
