
#include "callmethod.h"
#include "unitutil.h"
#include "stringutil.h"

int g_AntiCharmCode;
int g_AntiCharmPrivateExecutor;
int g_AntiCharmSetMonster;

void AntiCharmPrivateExecutor()
{
    CastSpellLocationLocation(ReadStringAddress(0x596fdc), GetObjectX(self), GetObjectY(self), GetObjectX(self), GetObjectY(self));
}

void AntiCharmGenCode(int *pDestCode)
{
	int gen;
	
	if (!gen)
	{
		int arr[13];
		arr[0] = 0x90868B50; arr[1] = 0x85000001; arr[2] = 0x8B1F74C0; arr[3] = 0x00015486; arr[4] = 0x00002500; arr[5] = 0xC0851000; arr[6] = 0x6A561074; arr[7] = 0xFFFF6800; arr[8] = 0xE9E80000; arr[9] = 0x83FFDB62; arr[10] = 0x68580CC4; arr[11] = 0x0050A5C0; arr[12] = 0x909090C3; 
		gen = GetScrDataField(-g_AntiCharmCode)+8;
		
		FixCallOpcode(gen+0x22, 0x507310);
		SetMemory(gen+30, -g_AntiCharmPrivateExecutor);
	}
	SetMemory(pDestCode, gen);
}

void AntiCharmSetMonster(int mon)
{
	int ptr = UnitToPtr(mon);
	
	if (!ptr)
		return;
	if (!IsMonsterUnit(mon))
		return;
	
	int code;
	
	AntiCharmGenCode(GetScrDataField(-g_AntiCharmSetMonster)+8);
	SetMemory(ptr + 0x2e8, code);
}

void NOXLibraryEntryPointFunction()
{
	g_AntiCharmCode = AntiCharmGenCode;
	g_AntiCharmPrivateExecutor = AntiCharmPrivateExecutor;
	g_AntiCharmSetMonster = AntiCharmSetMonster;
	
	"export AntiCharmPrivateExecutor";
	"export AntiCharmGenCode";
	"export AntiCharmSetMonster";
}
