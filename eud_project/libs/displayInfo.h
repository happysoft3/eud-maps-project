
#include "winapi.h"
#include "memutil.h"
#include "recovery.h"

#define CODE_LENGTH 52
int initialCreateDisplayCode()
{
    char *p;

    if (!p)
    {
        char code[CODE_LENGTH]={
            0x56, 0x55, 0xA1, 0x84, 0x71, 0x58, 0x00, 0x85, 0xC0, 0x74, 0x26, 
            0x8D, 0x70, 0x04, 0xFF, 0x76, 0x0C, 0xBD, 0x90, 0x43, 0x43, 0x00, 
            0xFF, 0xD5, 0xFF, 0x76, 0x04, 0xFF, 0x36, 0xFF, 0x76, 0x08, 0x6A, 
            0x00, 0xBD, 0xE0, 0xF6, 0x43, 0x00, 0xFF, 0xD5, 0x83, 0xC4, 0x14, 0x8B, 0x46, 0xFC, 0xEB, 0xD6, 0x5D, 0x5E, 0xC3
        };
        p=code;
    }
    char *ret=NULLPTR;
    AllocSmartMemEx(64, &ret);
    NoxByteMemset(ret,64,0);
    NoxByteMemCopy(p,ret,CODE_LENGTH);
    return ret;
}

#define SHOW_INFO_PATCH_BOUNDARY 0x436f00
#define SHOW_INFO_PATCH_SIZE 1024
void initialShowInfoPatch()
{
    int pOldProtect, code;

    if (code) return;
    code = initialCreateDisplayCode();
    SetMemory(0x587184,0);
    SetMemory(0x587228, 0x401a4d);
    SetRecoveryDataType2(0x587184, 0);
    WinApiVirtualProtect(SHOW_INFO_PATCH_BOUNDARY, SHOW_INFO_PATCH_SIZE, 0x40, &pOldProtect);
    char co[]={0xFF, 0x15, 0x28, 0x72, 0x58, 0x00, 0xC3};
    NoxByteMemCopy(co, 0x437051, sizeof(co));
    WinApiVirtualProtect(SHOW_INFO_PATCH_BOUNDARY, SHOW_INFO_PATCH_SIZE, pOldProtect, NULLPTR);
    SetRecoveryDataType2(0x587228,code);
    SetMemory(0x69a914,1);
}

#define SHOWINFO_DATA_NEXT 0
#define SHOWINFO_DATA_X 1
#define SHOWINFO_DATA_Y 2
#define SHOWINFO_DATA_MESSAGE 3
#define SHOWINFO_DATA_COLOR 4
#define SHOWINFO_DATA_MAX 5
int AddShowInfoData(int x, int y, short *uMessage, int color)
{
    int* data;

    initialShowInfoPatch();
    AllocSmartMemEx(SHOWINFO_DATA_MAX*4, &data);
    data[SHOWINFO_DATA_NEXT]=GetMemory(0x587184);
    data[SHOWINFO_DATA_COLOR]=color;
    data[SHOWINFO_DATA_MESSAGE]=uMessage;
    data[SHOWINFO_DATA_X]=x;
    data[SHOWINFO_DATA_Y]=y;
    SetMemory(0x587184, data);
    return data;
}
