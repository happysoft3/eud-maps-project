
#include "memutil.h"

//private 함수입니다-- 내부에서만 사용//
char VariantStringCheckRemovable(short *checkWidePtr, int uniqKey)
{
	short length = StringUtilGetWideLength(checkWidePtr);
	int *pCheck = checkWidePtr + (length*2)+2;

	return pCheck[0] == uniqKey;
}

//private 함수입니다-- 내부에서만 사용//
void VariantStringImpl(char *src, int *destPtr, int uniqKey)
{
	char removable = VariantStringCheckRemovable(destPtr[0], uniqKey);
	short length = (StringUtilGetLength(src)*2)+8;	//word 형 이므로 x2//
	short *newNamePt = MemAlloc(length);	//새 주소할당//

	NoxByteMemset(newNamePt, length, 0);		//새 주소, 초기화
	NoxUtf8ToUnicode(src, newNamePt);			//인코딩//

	short markPos = (StringUtilGetWideLength(newNamePt)*2)+2;
	int *mark = newNamePt+markPos;

	mark[0] = uniqKey;

	short *pOldWString = destPtr[0];
	destPtr[0] = newNamePt;
	if (removable)
	{
		MemFree(pOldWString);
	}
}

void NOXLibraryEntryPointFunction()
{
    "export VariantStringCheckRemovable";
    "export VariantStringImpl";
}