
#include "typecast.h"
#include "memutil.h"
#include "csf_ex.h"

int ImportTellStoryUniNamePartB()
{
    int *ret;

    if (!ret)
    {
        int stream[]={
            0x0424448B, 0x0824548B, 0x008CEC81, 0xC0850000, 0xEC9A8B53, 0x55000002, 0x02ECA88B,
            0x0F570000, 0x00012184, 0x0840F600, 0x17840F04, 0x8B000001, 0x00A824BC, 0xFF850000,
            0x0108840F, 0xC9830000, 0x89C033FF, 0x00011C95, 0xAEF25600, 0x748DD1F7, 0xF92B1624,
            0x7489C18B, 0xF78B1024, 0x10247C8B, 0xC602E9C1, 0xD0142444, 0x152444C6, 0x8BA5F303,
            0x5CC868C8, 0x1F6A0056, 0xF303E183, 0xC9C8B8A4, 0x4C8D0069, 0x51503E24, 0x0C2454FF,
            0xB824848B, 0x66000000, 0x842484C7, 0x00000000, 0x38938A00, 0x8D000008, 0x00083ABB,
            0x24948800, 0x000000AA, 0x86248489, 0x8A000000, 0x10C48307, 0x2374C084, 0x31FFC983,
            0xF7AEF2C0, 0x8DCF29D1, 0x8B7A2454, 0x8BF78BC1, 0x02E9C1FA, 0xC88BA5F3, 0xF303E183,
            0x8B2FEBA4, 0x5CD1640D, 0x68158B00, 0xA1005CD1, 0x005CD16C, 0x7A244C89, 0x700D8B66,
            0x89005CD1, 0x897E2454, 0x00822484, 0x89660000, 0x0086248C, 0x858B0000, 0x00000114,
            0x4E542068, 0x31016A00, 0x8A006AC9, 0x00081088, 0x24548D00, 0x00876820, 0x51520000,
            0x142454FF, 0xB424948B, 0x6A000000, 0xC4835200, 0x5D5F5E20, 0x8CC4815B, 0xC3000000};
		ret=stream;
    }
    return ret;
}

int ImportTellStoryUniNamePartA()
{
    int *ret;

    if (!ret)
    {
        int stream[]={
            0x8DE06856, 0x50680054, 0xFF005072, 0xF08B2414, 0x8B2414FF,
            0xBB40B50C, 0x148B0097, 0x97BB4085, 0x50685100, 0x520040AF,
            0x042454FF, 0x97200D8B, 0xC4830097, 0x24A15008, 0x50009797,
            0x2454FF51, 0x10C48314, 0xC483C031, 0x90C35E08};
		ret=stream;
        SetMemory(ret + 2, ImportTellStoryUniNamePartB());
    }
    return ret;
}

void TellStoryUnitName(string sAudio, string sDesc, string sUnitName)
{
    int *pStr=0x97bb40,n=sUnitName;
    int temp = GetMemory(0x5c3290), src = pStr[n];

    SetMemory(0x5c3290, ImportTellStoryUniNamePartA());
    NoxUtf8ToUnicode(src, 0x69C9C8);
    TellStory(sAudio, sDesc);
    SetMemory(0x5c3290, temp);
}

#define _tellstory_newmem_ 0
#define _tellstory_oldmem_ 2
#define _tellstory_target_ 1
#define _tellstory_max_ 3

void tellstoryDeferredReset(int *pMem)
{
    int *targ=pMem[_tellstory_target_];

    targ[_tellstory_newmem_]=pMem[_tellstory_oldmem_];
    MemFree(pMem[_tellstory_newmem_]);
    FreeSmartMemEx(pMem);
}

#define Csf_key 0

void TellStoryClientside(string str, string unitname)
{
    char*src=StringUtilGetScriptStringPtr(str);
    int len=StringUtilGetLength(src);
    short *sw=MemAlloc((len+2)*2);
    NoxUtf8ToUnicode(src,sw);
    int *keyPtr;

    CsfPrivateGetKeyPtr(Csf_key, &keyPtr);
    short *strIndex = keyPtr+50;
    int *strPtbase = 0x611c04;
    int *strTb = strPtbase[0];
    int *pOld= strTb[strIndex[0]];
    strTb[strIndex[0]]=sw;
    string nullstr=0;
    TellStoryUnitName(nullstr, CsfGetKeyStringFromId(Csf_key), unitname);
    // strTb[strIndex[0]]=pOld;

    int *mem2;
    AllocSmartMemEx(_tellstory_max_*4, &mem2);
    mem2[_tellstory_newmem_]=sw;
    mem2[_tellstory_target_]=ArrayRefN( strTb, strIndex[0]);
    mem2[_tellstory_oldmem_]=pOld;
    FrameTimerWithArg(1, mem2, tellstoryDeferredReset);
    // MemFree(sw);
}

#undef _tellstory_newmem_
#undef _tellstory_oldmem_
#undef _tellstory_target_
#undef _tellstory_max_

#undef Csf_key

void NOXLibraryEntryPointFunction()
{
    "export needinit ImportTellStoryUniNamePartB";
}
