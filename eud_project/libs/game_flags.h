
//0x5d5394 -- 1 일 때, 강제 플레이어 되살리기

#define GAME_FLAG_RESPAWN_WEAPON_ARMOR 2
#define GAME_FLAG_ENABLE_CAMPER_ALARM 0x2000

// void SetGameFlags(int flags)
// {
//     int *pFlags=0x5d5330;

//     pFlags[0]=flags;
// }

// int GetGameFlags()
// {
//     int *pFlags = 0x5d5330;
//     return pFlags[0];
// }

//GAME_FLAG_XXX 매크로 사용
int CheckGameFlags(int flags)
{
    int *pFlags = 0x5d5330;
    return pFlags[0]&flags;
}

//GAME_FLAG_XXX 매크로 사용
void SetGameFlags(int flags)
{
    int *pFlags = 0x5d5330;
    pFlags[0]^=flags;
}

//강제 플레이어 되살리기 = TRUE
void SetGameSettingForceRespawn(int tof)
{
    int*p=0x5d5394;

    p[0]=tof&1;
}

int GetSysopPassword(char *pDest, int size)
{
    if (size==0)
        return 0;

    short *pSysop = 0x5d5368;
    int u = 0;

    while (--size >= 0)
    {
        if (!pSysop[u])
        {
            pDest[u]=0;
            break;
        }
        pDest[u]=pSysop[u];
        u+=1;
    }
    return u;
}

