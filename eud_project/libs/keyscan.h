
#include "../noxscript/builtins.h"

#define VIRTUAL_KEY_ESC 1
#define VIRTUAL_KEY_LCTRL 29
#define VIRTUAL_KEY_1 2
#define VIRTUAL_KEY_2 3
#define VIRTUAL_KEY_3 4
#define VIRTUAL_KEY_4 5
#define VIRTUAL_KEY_5 6
#define VIRTUAL_KEY_6 7
#define VIRTUAL_KEY_7 8
#define VIRTUAL_KEY_8 9
#define VIRTUAL_KEY_9 10
#define VIRTUAL_KEY_0 11
#define VIRTUAL_KEY_MINUS 12
#define VIRTUAL_KEY_EQUAL 13
#define VIRTUAL_KEY_BACKSPACE 14

#define VIRTUAL_KEY_TAB 15

#define VIRTUAL_KEY_Q 16
#define VIRTUAL_KEY_W 17
#define VIRTUAL_KEY_E 18
#define VIRTUAL_KEY_R 19
#define VIRTUAL_KEY_T 20
#define VIRTUAL_KEY_Y 21
#define VIRTUAL_KEY_U 22
#define VIRTUAL_KEY_I 23
#define VIRTUAL_KEY_O 24
#define VIRTUAL_KEY_P 25

#define VIRTUAL_KEY_ENTER 28

#define VIRTUAL_KEY_A 30
#define VIRTUAL_KEY_S 31
#define VIRTUAL_KEY_D 32
#define VIRTUAL_KEY_F 33
#define VIRTUAL_KEY_G 34
#define VIRTUAL_KEY_H 35
#define VIRTUAL_KEY_J 36
#define VIRTUAL_KEY_K 37
#define VIRTUAL_KEY_L 38

#define VIRTUAL_KEY_LSHIFT 42

#define VIRTUAL_KEY_Z 44
#define VIRTUAL_KEY_X 45
#define VIRTUAL_KEY_C 46
#define VIRTUAL_KEY_V 47
#define VIRTUAL_KEY_B 48
#define VIRTUAL_KEY_N 49
#define VIRTUAL_KEY_M 50

#define VIRTUAL_KEY_RSHIFT 54
#define VIRTUAL_KEY_LALT 56
#define VIRTUAL_KEY_SPACEBAR 57

#define VIRTUAL_KEY_RCTRL 157
#define VIRTUAL_KEY_HOME 199
#define VIRTUAL_KEY_UP_ARROW 200
#define VIRTUAL_KEY_PAGEUP 201
#define VIRTUAL_KEY_LEFT_ARROW 203
#define VIRTUAL_KEY_RIGHT_ARROW 205
#define VIRTUAL_KEY_END 207
#define VIRTUAL_KEY_DOWN_ARROW 208
#define VIRTUAL_KEY_PAGEDOWN 209
#define VIRTUAL_KEY_INSERT 210
#define VIRTUAL_KEY_DELETE 211

int KeyboardIOCheckKey(int keyId)
{
    int *keyTable = 0x6950b0;
    int *selkey = keyTable+(keyId*8);
    int key = (selkey[0]>>8)&0xff;

    return key==2;
}

#define FALSE 0

//게임이 포커스를 갖지 않았으면, 무조건 false 입니다
int KeyboardIOCheckKeyEx(int keyId)
{
    if (GetMemory(0x68C74C))
        return FALSE;

    return KeyboardIOCheckKey(keyId);
}

//입력중인 상태이면, true 입니다
int KeyboardIO_IsInputMode()
{
    return GetMemory(0x6d8558)!=0;
}

int KeyboardIOGetFps(int keyId)
{
    int *selkey = 0x6950b0+(keyId*8);

	return selkey[1];
}

#undef FALSE

