
#include "define.h"
#include "array.h"
#include "stringutil.h"

int StringCompareWithPtr(int *cmp1, int *cmp2)
{
	while ((cmp1[0]&0xff) && ((cmp1[0]&0xff)==(cmp2[0]&0xff)))
	{
		Nop(++cmp1);
		Nop(++cmp2);
	}
	return (cmp1[0]&0xff)-(cmp2[0]&0xff);
}

int GameDataUtilFindTable(string keyId, int *pDest, int *pCount)
{
	string *dest;

	StringUtilGetScriptStringPtrEx(keyId, &dest);

	int tableKey = (SToInt(dest[0]) & 0xff);
	if (tableKey < 'A' || tableKey > 'Z')
		return FALSE;

	tableKey -= 'A';
	
	int *gameDbBase = 0x6552d8;
	int *tablePtr = gameDbBase[0]+(tableKey*12);

	if (!tablePtr[0])
		return FALSE;

	pDest[0] = tablePtr[0];
	pCount[0] = tablePtr[1];
	return TRUE;
}

int GameDataUtilFindSubTable(string keyId, int *subTable, int count, int *pFindRes)
{
	if (!count)
		return FALSE;

	int *cmp = StringUtilGetScriptStringPtr(keyId);

	while (count--)
	{
		if (!StringCompareWithPtr(cmp, subTable[count*2]))
		{
			pFindRes[0] = ArrayRefN(subTable, count*2);
			return TRUE;
		}
	}
	return FALSE;
}

int GameDataUtilGetValueByIndex(string keyId, int index, float *pValue)
{
	int *pTable, count;

	if (!GameDataUtilFindTable(keyId, &pTable, &count))
		return FALSE;

	int *find;
	if (!GameDataUtilFindSubTable(keyId, pTable, count, &find))
		return FALSE;

	int *valuePtr = find[1];

	if (index >= valuePtr[1])
		return FALSE;

	int *val = valuePtr[0];
	pValue[0] = ToFloat(val[index]);
	return TRUE;
}

int GameDataUtilGetValue(string keyId, float *pValue)
{
	return GameDataUtilGetValueByIndex(keyId, 0, pValue);
}

int GameDataUtilSetValueByIndex(string keyId, int index, float setvalue)
{
	int *pTable, count;

	if (!GameDataUtilFindTable(keyId, &pTable, &count))
		return FALSE;

	int *find;
	if (!GameDataUtilFindSubTable(keyId, pTable, count, &find))
		return FALSE;

	int *valuePtr = find[1];

	if (index >= valuePtr[1])
		return FALSE;

	int *val = valuePtr[0];
	val[index] = ToInt(setvalue);
	return TRUE;
}

void NOXLibraryEntryPointFunction()
{
	"export StringCompareWithPtr";
	"export GameDataUtilFindTable";
	"export GameDataUtilFindSubTable";
	"export GameDataUtilGetValueByIndex";
	"export GameDataUtilGetValue";
	"export GameDataUtilSetValueByIndex";
}
