
#include "unitutil.h"
#include "callmethod.h"
#include "stringutil.h"

int GetPlayerIndex(int plrUnit)
{
    int *ptr = UnitToPtr(plrUnit);

    if (ptr)
	{
		int *pEC = ptr[187];

		if (!pEC)
			return -1;

		int *pInfo = pEC[69];

        return pInfo[516];
	}
    return -1;
}

//플레이어가 현재 장착중인 무기의 id 을 얻습니다
int PlayerGetEquipedWeapon(int plrUnit)
{
    int *ptr = UnitToPtr(plrUnit);
    
    if (ptr)
    {
		int *pEC = ptr[187];
        int *gid = pEC[26];
        if (gid)
            return gid[11];
    }
    return 0;
}

//플레이어의 두번째 무기의 id 을 얻습니다
int PlayerGetNextWeapon(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		int *pEC = ptr[187];
		int *gid = pEC[27];

		if (gid)
			return gid[11];
	}
	return 0;
}

//플레이어의 힘 수치를 얻습니다
int PlayerGetStrength(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		int *pEC = ptr[187];
		int *pInfo = pEC[69];
		int *pStrength = pInfo+0x8bf;
		return pStrength[0];
	}
	return 0;
}

void PlayerSetStrength(int plrUnit, int strength)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		int *pEC = ptr[187];
		int *pInfo = pEC[69];
		int *pStrength = pInfo+0x8bf;

		pStrength[0] = strength;
	}
}

int PlayerGetMaxSpeed(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		int *pEC = ptr[187];
		int *pInfo = pEC[69];
		int *pSpeed = pInfo +0x8bb;
		
		return pSpeed[0];
	}
	return 0;
}

int PlayerGetMaxWeight(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		int *pEC = ptr[187];
		int *pInfo = pEC[69];
		
		return pInfo[913];
	}
	return 0;
}

float PlayerGetArmorValue(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		int *pEC = ptr[187];
		
		StopScript(pEC[58]);
	}
	return 0.0;
}

int PlayerGetPtrFromIndex(int pIndex)
{
	return GetMemory(0x62f9e0 + ((pIndex & 0x1f) * 0x12dc));
}

int GetPlayerMouseX(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);

	while (ptr)
	{
		if (!(GetMemory(ptr + 8) & 4))
			break;
		return GetMemory(GetMemory(GetMemory(ptr + 0x2ec) + 0x114) + 0x8ec);
	}
	return 0;
}

int GetPlayerMouseY(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);

	while (ptr)
	{
		if (!(GetMemory(ptr + 8) & 4))
			break;
		return GetMemory(GetMemory(GetMemory(ptr + 0x2ec) + 0x114) + 0x8f0);
	}
	return 0;
}

int GetPlayerMouseXY(int plrUnit, int *xpos, int *ypos)
{
	int *ptr = UnitToPtr(plrUnit);
	
	while (ptr)
	{
		if (!(ptr[2]&4))
			break;
		int *pEC = ptr[187];
		int *pInfo = pEC[69];
		int *xyptr = pInfo + 0x8ec;
		
		if (xpos != 0)
			xpos[0] = xyptr[0];
		if (ypos != 0)
			ypos[0] = xyptr[1];
		return TRUE;
	}
	return FALSE;
}

int GetPlayerPtrByNetCode(int netId)
{
	int rep = 32, *pPlayerOffset = 0x62f9e0;

	while (--rep>=0)
	{
		if (pPlayerOffset[1]==netId)
			return pPlayerOffset[0];
		pPlayerOffset+=0x12dc;		
	}
	return 0;


}

int ImportGiveUnitFunc()
{
    int *link;
	
    if (!link)
    {
		int impl[] = {
        	0x50725068, 0x1B606800, 0x00680051, 0xFF004E7B, 0x50082454, 0x082454FF, 0x8B04C483, 0x74FF85F8,
        	0x2454FF19, 0x54FF5008, 0xC4830824, 0x74C08504, 0xFF505709, 0x83082454, 0xC48308C4, 0x31FF310C,
        	0x0000C3C0};
		link = &impl;
    }
    return link;
}

void GiveCreatureToPlayer(int owner, int unit)
{
	int *builtins = 0x5c308c;
	int *pOld = builtins[90];

	builtins[90] = ImportGiveUnitFunc();
    Unused5a(owner, unit);
	builtins[90] = pOld;
}

//유저의 현재 마나량을 얻습니다
//유효하지 않을 경우 -1이 반환됩니다
//plrUnit: 유저의 유닛id
int PlayerGetCurrentManaAmount(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		if (ptr[2] & 4)
		{
			int *pEC = ptr[187];
			short cMana = pEC[1];
			
			return cMana;
		}
	}
	return -1;
}

//유저의 현재 마나량을 설정합니다
//plrUnit: 유저의 유닛id
//manaAmount: 0~65535까지의 설정량
void PlayerSetCurrentManaAmount(int plrUnit, short manaAmount)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		int manaWord = manaAmount & 0xffff;
		
		if (ptr[2] & 4)
		{
			int *pEC = ptr[187];
			short *pMana = pEC + 4;

			pMana[0] = manaAmount;
			pMana[1] = manaAmount;
		}
	}
}

//유저의 최대 마나량을 얻습니다
//유효하지 않은경우 -1이 반환됩니다
short PlayerGetMaximumManaAmount(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		if (ptr[2] & 4)
		{
			int *pEC = ptr[187];
			short maxMana = pEC[2];

			return maxMana;
		}
	}
	return -1;
}

//유저의 최대 마나량을 설정합니다
void PlayerSetMaximumManaAmount(int plrUnit, int manaSetTo)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		if (ptr[2] & 4)
		{
			int *pEC = ptr[187];
			short *pMana = pEC + 8;

			pMana[0] = manaSetTo;
		}
	}
}

int PlayerGetCurrentLevel(int plrUnit)
{
	int *ptr = UnitToPtr(plrUnit);
	
	if (ptr)
	{
		if (ptr[2] & 4)
		{
			int *pEC = ptr[187];
			int *pInfo = pEC[69];
			return pInfo[921];
		}
	}
	return -1;
}

string PlayerIngameNick(int sUnit)
{
	char userNickArr[1024];	
	int *ptr = UnitToPtr(sUnit);
    
    if (ptr)
    {
        if (ptr[2] & 4)
        {
            int plrIndex = GetPlayerIndex(sUnit);
			char *destPtr = &userNickArr + (plrIndex*32);
			int *pEC = ptr[187];
			int *pInfo = pEC[69];

            NoxUnicodeToUtf8(pInfo + 0x889, destPtr);
            return ReadStringAddressEx(destPtr);
        }
    }
    return ReadStringAddress(0x587a50);
}

void NOXLibraryEntryPointFunction()
{
	"export GetPlayerIndex";
	"export PlayerGetEquipedWeapon";
	"export PlayerGetNextWeapon";
	"export PlayerGetStrength";
	"export PlayerSetStrength";
	"export PlayerGetMaxSpeed";
	"export PlayerGetMaxWeight";
	"export PlayerGetArmorValue";
	"export PlayerGetPtrFromIndex";
	"export GetPlayerMouseX";
	"export GetPlayerMouseY";
	"export GetPlayerMouseXY";
	"export GetPlayerPtrByNetCode";
	"export needinit ImportGiveUnitFunc";
	"export GiveCreatureToPlayer";
	"export PlayerGetCurrentManaAmount";
	"export PlayerGetMaximumManaAmount";
	"export PlayerSetCurrentManaAmount";
	"export PlayerSetMaximumManaAmount";
	"export PlayerGetCurrentLevel";
	"export PlayerIngameNick";
}