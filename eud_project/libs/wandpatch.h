
#include "define.h"
#include "typecast.h"
#include "callmethod.h"
#include "memutil.h"

//Todo. 추후 csf 모듈로//
int CsfDbFindStringDescriptionFromId(short csfId)
{
	int *csfOff1 = 0x611c00;
	int key = GetMemory(csfOff1[0] + (52 * csfId) + 48) >> 0x10;
	
	return GetMemory(GetMemory(0x611c04) + (key * 4));
}

int WeaponListSectionFindThingId(short thingId)
{
	int page = GetMemory(0x611C64);
	
	while (page != 0)
	{
		if (GetMemory(page + 4) == thingId)
			return page;
		page=GetMemory(page + 80);
	}
	return 0;
}

void AppendMissileWand()
{
    short thingId = 213;
    int *ptr = WeaponListSectionFindThingId(thingId);

    if (ptr != 0)
    {
		if (ptr[2]==0)
		{
			short *desc = MemAlloc(24);
			
			NoxByteMemset(desc, 24, 0);
			StringUtilWideCopy(CsfDbFindStringDescriptionFromId(3651), desc);
			ptr[2]=desc;
		}
		ptr[3]=0xa1000000;
		ptr[4]=0x788c;
		ptr[10]=1;
		ptr[15]=0x70014;
		ptr[16]=0.5;
		ptr[17]=50.0;
		ptr[18]=5;

		int *weaponSpare = 0x58f260;

		weaponSpare[0]=ptr[0];
		weaponSpare[1]=thingId;
		weaponSpare[2]=131072;
    }
}

void AppendDemonsBreathWand()
{
    short thingId = 215;
    int *ptr = WeaponListSectionFindThingId(thingId);

    if (ptr != 0)
    {
		if (ptr[2]==0)
		{
			short *desc = MemAlloc(24);
			
			NoxByteMemset(desc, 24, 0);
			StringUtilWideCopy(CsfDbFindStringDescriptionFromId(3651), desc);
			ptr[2]=desc;
		}
		ptr[3]=0xa1000000;
		ptr[4]=0x788c;
		ptr[10]=1;
		ptr[15]=0x70014;
		ptr[16]=0.5;
		ptr[17]=50.0;
		ptr[18]=5;
		int *firestaffFlag = 0x58f1f0;
		firestaffFlag[0]=0x200000;
    }
}

void AppendAllDummyStaffsToWeaponList()
{
	AppendDemonsBreathWand();
	AppendMissileWand();
}

void NOXLibraryEntryPointFunction()
{
	"export CsfDbFindStringDescriptionFromId";
	"export WeaponListSectionFindThingId";
	"export AppendMissileWand";
	"export AppendDemonsBreathWand";
	"export AppendAllDummyStaffsToWeaponList";
}

