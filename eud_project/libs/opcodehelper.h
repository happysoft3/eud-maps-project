
#include "callmethod.h"

int FixCallOpcode(int curAddr, int targetAddr)
{
    SetMemory(curAddr + 1, targetAddr - curAddr - 5);
    return 0;
}

int OpcodeGetTargetAddr(int curAddr)
{
    return GetMemory(curAddr + 1) + curAddr + 5;
}

void OpcodeLoadEffectiveAddr(int loadAddr, int codeAddr, int offset)
{
    int targetAddr = OpcodeGetTargetAddr(codeAddr + offset);

    SetMemory(loadAddr + offset + 1, targetAddr - (loadAddr + offset) - 5);
}

void OpcodeCopiesAdvance(int *destPtr, int *callNodePtr, int *startAddr, int *endAddr)
{
    int destBase=destPtr, *curAddr = startAddr;

    while (curAddr <= endAddr)
    {
        destPtr[0]=curAddr[0];

        if (callNodePtr[0])
        {
            if (callNodePtr[0]+1 <= curAddr)
            {
                OpcodeLoadEffectiveAddr(destBase, startAddr, callNodePtr[0] - startAddr);
                callNodePtr=&callNodePtr[1];
            }
        }
        curAddr=&curAddr[1];
        destPtr=&destPtr[1];
    }
}

void invokeRawCode(char *pCode, int *args)
{
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=pCode;
    Unused1f(args);
    pBuiltins[31]=pOld;
}

int invokeRawCodeReturn(char *pCode, int *args)
{
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[94];
    pBuiltins[94]=pCode;
    int ret = Unused5e(args);
    pBuiltins[94]=pOld;
    return ret;
}
