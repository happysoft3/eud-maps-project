
#include "typecast.h"
#include "unitutil.h"
#include "stringutil.h"


/*
int GetMemory(int a){}
void SetMemory(int a, int b){}
int UnitToPtr(int a){}
string ReadStringAddress(int t) {}
*/

#define ENCHANT_INVISIBLE   0
#define ENCHANT_MOONGLOW    1
#define ENCHANT_BLINDED     2
#define ENCHANT_CONFUSED        3
#define ENCHANT_SLOWED          4
#define ENCHANT_HELD            5
#define ENCHANT_DETECTING       6
#define ENCHANT_ETHEREAL        7
#define ENCHANT_RUN             8
#define ENCHANT_HASTED          9
#define ENCHANT_VILLAIN         10
#define ENCHANT_AFRAID          11
#define ENCHANT_BURNING         12
#define ENCHANT_VAMPIRISM       13
#define ENCHANT_ANCHORED        14
#define ENCHANT_LIGHT           15
#define ENCHANT_DEATH           16
#define ENCHANT_PROTECT_FROM_FIRE       17
#define ENCHANT_PROTECT_FROM_POISON     18
#define ENCHANT_PROTECT_FROM_MAGIC      19  
#define ENCHANT_PROTECT_FROM_ELECTRICITY            20
#define ENCHANT_INFRAVISION         21
#define ENCHANT_SHOCK               22
#define  ENCHANT_INVULNERABLE        23
#define  ENCHANT_TELEKINESIS         24
#define ENCHANT_FREEZE              25
#define ENCHANT_SHIELD              26
#define ENCHANT_REFLECTIVE_SHIELD   27
#define ENCHANT_CHARMING            28
#define ENCHANT_ANTI_MAGIC          29
#define ENCHANT_CROWN               30
#define ENCHANT_SNEAK               31

string EnchantList(int sNumber)
{
	return ReadStringAddress(0x596f24 + (sNumber * 4));
}

int UnitCheckEnchant(int sUnit, int sMagicFlag)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
        return GetMemory(ptr + 0x154) & sMagicFlag;
    return 0;
}

int GetLShift(int sCount)
{
    return (1 << sCount);
}

void SetUnitEnchantCopy(int sUnit, int sMagicFlag)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		Enchant(sUnit, EnchantList(6), 0.0);
		SetMemory(ptr + 0x154, GetMemory(ptr + 0x154) | sMagicFlag);
	}
}

void UnitSetEnchantTime(int unit, int enchantNumber, int durateTime)
{
	int ptr = UnitToPtr(unit), temp, tempPtr;

	if (ptr)
	{
		SetUnitEnchantCopy(unit, GetLShift(enchantNumber));
		tempPtr = ptr + 0x158 + ((enchantNumber / 2) * 4);
		temp = GetMemory(tempPtr);
		if (enchantNumber % 2)
		{
			SetMemory(tempPtr, (GetMemory(tempPtr) & 0xffff) | (durateTime << 0x10));
		}
		else
			SetMemory(tempPtr, ((GetMemory(tempPtr) >> 0x10) << 0x10) | durateTime);
	}
}

void NOXLibraryEntryPointFunction()
{
	"export EnchantList";
	"export UnitCheckEnchant";
	"export GetLShift";
	"export SetUnitEnchantCopy";
	"export UnitSetEnchantTime";
}
