
// #include "stringutil.h"
#include "imageutil.h"
#include "recovery.h"

#define ABILITY_DB_BERSERKER_CHARGE_ID 	0
#define ABILITY_DB_WARCRY_ID 			1
#define ABILITY_DB_HARPOON_ID 			2
#define ABILITY_DB_TREAD_LIGHTLY_ID		3
#define ABILITY_DB_EYE_OF_WOLF_ID		4

#define ABILITY_DB_BASE_OFF 0x666a24
void abilityDbGetTable(char ability_id, int **pDest)
{
	pDest[0]= ABILITY_DB_BASE_OFF + (52 * ability_id);
}

//능력 이름을 변경합니다
//클라이언트 사이드입니다
void AbilityDbSetName(char ability_id, short *wMessage)
{
	int *target;
	
	abilityDbGetTable(ability_id, &target);
	SetRecoveryDataType2(&target[0], wMessage);
}

//능력 설명을 변경합니다
//클라이언트 사이드입니다
void AbilityDbSetExplanation(char ability_id, short*wMessage)
{
	int *target;
	
	abilityDbGetTable(ability_id, &target);
	SetRecoveryDataType2(&target[1], wMessage);
}

void AbilityDbChangeButtonSet(char ability_id, int imgId)
{
	int *target, imgptr;
	
	abilityDbGetTable(ability_id, &target);
	ImageUtilGetPtrFromID(imgId, &imgptr);
	SetRecoveryDataType2(&target[2], imgptr);
}

void AbilityDbChangeHighlightButtonSet(char ability_id, int imgId)
{
	int *target, imgptr;
	
	abilityDbGetTable(ability_id, &target);
	ImageUtilGetPtrFromID(imgId, &imgptr);
	SetRecoveryDataType2(&target[3], imgptr);
}

void AbilityDbChangeCastSound(char ability_id, int soundId)
{
	int *target;
	
	abilityDbGetTable(ability_id, &target);
	SetRecoveryDataType2(&target[10], soundId);
}

void AbilityDbChangeOnSound(char ability_id, short soundId)
{
	int *target;
	
	abilityDbGetTable(ability_id, &target);
	SetRecoveryDataType2(&target[11], soundId);
}

void AbilityDbChangeOffSound(char ability_id, short soundId)
{
	int *target;
	
	abilityDbGetTable(ability_id, &target);
	SetRecoveryDataType2(&target[12], soundId);
}

//목록에서 해당 능력을 표시할지를 결정합니다 
void AbilityDbSetActivate(char abilityId, int tof){
	int *target, once;

	abilityDbGetTable(abilityId, &target);
	if (!once){
		SetRecoveryDataType2(&target[6], tof);
		once=TRUE;
		return;
	}
	target[6]=tof;
}

//해당 능력의 쿨다운을 변경합니다, 서버 사이드, 클라이언트 사이드 혼용
void AbilityDbSetCooldown(char abilityId, int cooldown){
	int *target, once;

	abilityDbGetTable(abilityId, &target);
	if (once)
	{
		target[7]=cooldown;
		return;
	}
	once=TRUE;
	SetRecoveryDataType2(&target[7], cooldown);
}

//해당 능력의 지속시간을 변경합니다, 서버 사이드
void AbilityDbSetDuration(char abilityId, int dur){
	int *target, once;

	abilityDbGetTable(abilityId, &target);
	if (once)
	{
		target[8]=dur;
		return;
	}
	once=TRUE;
	SetRecoveryDataType2(&target[8], dur);
}


