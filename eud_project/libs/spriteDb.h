
#include "define.h"
#include "recovery.h"
#include "stringutil.h"
#include "imageutil.h"

#define SPRITEDB_NAMEPTR 0
#define SPRITEDB_ITEMNAME 1
#define SPRITEDB_ITEMDESC 2
#define SPRITEDB_IMAGE_SIZE 3
#define SPRITEDB_EXTENT_TYPE 5
#define SPRITEDB_THING_ID 7
#define SPRITEDB_FIELD24 9  //표지판 같은것은 이 값에 0x80이 들어있음
#define SPRITEDB_PROPERTIES 10
#define SPRITEDB_LIGHT_INTENSITY 11
#define SPRITEDB_LIGHT_INTENSITY_RED 12
#define SPRITEDB_LIGHT_INTENSITY_GREEN 13
#define SPRITEDB_LIGHT_INTENSITY_BLUE 14
#define SPRITEDB_EXTENT_CIRCLE 16
#define SPRITEDB_EXTENT_ZSIZE_LOW 17
#define SPRITEDB_EXTENT_ZSIZE_HIGH 18
#define SPRITEDB_EXTENT_WIDTH 19
#define SPRITEDB_EXTENT_HEIGHT 20
#define SPRITEDB_DRAW_FUNCTION 22
#define SPRITEDB_IMAGE_INFORMATION 23
#define SPRITEDB_MENU_ICON_IMAGE_NUMBER 29

#define SPRITE_FLAG_PICKUPABLE 0x10000

#define SPRITE_PROPERTY_STATIC 0x10
#define SPRITE_PROPERTY_SIGHT_DESTROY 0x200000

int getSpriteDbObject(short thingId)
{
    int *spriteBase=GetMemory(0x69f224);

    return spriteBase[thingId];
}

void allocNewImageTable(int **setTarget, int prevCount, int imgCount)
{
    if (!setTarget)
        return;

    int *src=setTarget[0];
    int *dest=MemAlloc((imgCount+1)*4);

    setTarget[0]=dest;
    dest[imgCount]=0;
    if (src)
    {
        DwordCopy(src, dest, prevCount);
        MemFree(src);
        return;
    }
}
void allocNewImageTableAdv(int *setTarget,int prevCount,int imgCount){
    char *p;

    if (!p){
        char code[]={
            0x83, 0xEC, 0x08, 0x55, 0x8B, 0xEC, 0x56, 0x53, 0x57, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0xF0, 0x8B, 0x06, 0x85, 0xC0, 0x74, 0x4A, 0x0F, 0x1F, 0x40, 0x00, 0x8B, 0x06, 0x8B, 0x00, 0x89, 0x45, 0x04, 0x8B, 0x46, 0x08, 0x40, 0xC1, 0xE0, 0x02, 0x50, 0xB8, 0x60, 0x35, 0x40, 0x00, 0xFF, 0xD0, 0x8B, 0xD8, 0x8B, 0x06, 0x89, 0x18, 0x8B, 0x46, 0x08, 0xC7, 0x04, 0x83, 0x00, 0x00, 0x00, 0x00, 0x8B, 0x45, 0x04, 0x85, 0xC0, 0x74, 0x19, 0x0F, 0x1F, 0x40, 0x00, 0x8B, 0x4E, 0x04, 0x8B, 0x75, 0x04, 0x8B, 0xFB, 0xF3, 0xA5, 0xFF, 0x75, 0x04, 0xB8, 0x5D, 0x42, 0x40, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0x5F, 0x5B, 0x5E, 0x8B, 0xE5, 0x5D, 0x83, 0xC4, 0x08, 0xC3
        };
        p=code;
    }
    invokeRawCode(p,&setTarget);
}

#define IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK 1
#define IMAGE_SPRITE_MON_ACTION_FAR_ATTACK 3
#define IMAGE_SPRITE_MON_ACTION_CAST_SPELL 7
#define IMAGE_SPRITE_MON_ACTION_IDLE 8
#define IMAGE_SPRITE_MON_ACTION_SLAIN 9
#define IMAGE_SPRITE_MON_ACTION_DUMMY 10
#define IMAGE_SPRITE_MON_ACTION_RUN 13
#define IMAGE_SPRITE_MON_ACTION_WALK 12

//공격모션 변경안될 시, 이 함수를 사용하여 값을 2으로 설정하시오(원래 값은 아마 5일 것입니다)
void ChangeMonsterSpriteImageUnkValue(int thingId, char actionType, int setTo)
{
    int *pSprite=getSpriteDbObject(thingId);
    char *pImgInfo = pSprite[SPRITEDB_IMAGE_INFORMATION];
    char *pImgTablePtrs = &pImgInfo[8 + (actionType*48)];

    SetRecoveryDataNormal(&pImgTablePtrs[40], setTo);
}

int getMonsterSpriteImageTablePtrBase(int thingId, char actionType)
{
    int *pSprite=getSpriteDbObject(thingId);
    char *pImgInfo = pSprite[SPRITEDB_IMAGE_INFORMATION];

    return &pImgInfo[8 + (actionType*48)];
}

#define USE_DEFAULT_SETTINGS -1

#define IMPL_MON_SPRITE_IMG_COUNT 0
#define IMPL_MON_SPRITE_IMG_FRAMES 1
#define IMPL_MON_SPRITE_IMG_UNK 2

void implMonsterSpriteImageChangeHeaderData(int thingId, char actionType, int *data)
{
    int *pSprite=getSpriteDbObject(thingId);
    char *pImgInfo = pSprite[SPRITEDB_IMAGE_INFORMATION];
    int *pImgTablePtrs = &pImgInfo[8 + (actionType*48)];
    short *pImgCount = &pImgTablePtrs[9];
    int uWord;

    if (data[IMPL_MON_SPRITE_IMG_FRAMES]!=USE_DEFAULT_SETTINGS)   uWord=data[IMPL_MON_SPRITE_IMG_FRAMES]<<0x10;
    else                                uWord=pImgCount[1]<<0x10;

    if (data[IMPL_MON_SPRITE_IMG_UNK]!=USE_DEFAULT_SETTINGS)
    {
        SetRecoveryDataNormal(&pImgTablePtrs[10], data[IMPL_MON_SPRITE_IMG_UNK]);
    }

    int count = data[IMPL_MON_SPRITE_IMG_COUNT];

    if (count==USE_DEFAULT_SETTINGS)
    {
        SetRecoveryDataNormal(pImgCount, count|uWord);
        return;
    }

    if (count < pImgCount[0])
    {
        SetRecoveryDataNormal(pImgCount, count|uWord);
        return;
    }
    int prevCount = pImgCount[0];
    count|=uWord;
    int allocSize=(count+1)*4;
    // allocNewImageTableAdv(&pImgTablePtrs[0], prevCount,count            , allocSize);
    // allocNewImageTableAdv(&pImgTablePtrs[1], prevCount,count            , allocSize);
    // allocNewImageTableAdv(&pImgTablePtrs[2], prevCount,count            , allocSize);
    // allocNewImageTableAdv(&pImgTablePtrs[3], prevCount,count            , allocSize);
    // allocNewImageTableAdv(&pImgTablePtrs[5], prevCount,count            , allocSize);
    // allocNewImageTableAdv(&pImgTablePtrs[6], prevCount,count            , allocSize);
    // allocNewImageTableAdv(&pImgTablePtrs[7], prevCount,count            , allocSize);
    // allocNewImageTableAdv(&pImgTablePtrs[8], prevCount,count            , allocSize);
    allocNewImageTableAdv(pImgTablePtrs, prevCount,count           );
    allocNewImageTableAdv(pImgTablePtrs+ 4, prevCount,count        );
    allocNewImageTableAdv(pImgTablePtrs+ 8, prevCount,count        );
    allocNewImageTableAdv(pImgTablePtrs+ 12, prevCount,count       );
    allocNewImageTableAdv(pImgTablePtrs+ 20, prevCount,count       );
    allocNewImageTableAdv(pImgTablePtrs+ 24, prevCount,count       );
    allocNewImageTableAdv(pImgTablePtrs+ 28, prevCount,count       );
    allocNewImageTableAdv(pImgTablePtrs+ 32, prevCount,count       );
    SetRecoveryDataNormal(pImgCount, count);
}

//actionType 인자는 IMAGE_SPRITE_MON_ACTION 키워드를 사용하세요
void ChangeMonsterSpriteImageCount(int thingId, char actionType, int count)
{
    int headerData[]={count,USE_DEFAULT_SETTINGS,USE_DEFAULT_SETTINGS};
    implMonsterSpriteImageChangeHeaderData(thingId, actionType, headerData);
}

void ChangeMonsterSpriteImageCountAndFrames(int thingId, char actionType, int count, int frames)
{
    int headerData[]={count,frames,USE_DEFAULT_SETTINGS};
    implMonsterSpriteImageChangeHeaderData(thingId, actionType, headerData);
}

void ChangeMonsterSpriteImageHeader(int thingId,char actionType,int count,int frames, int unk)
{
    implMonsterSpriteImageChangeHeaderData(thingId, actionType, &count);
}

#define IMAGE_SPRITE_DIRECTION_UP_LEFT 0
#define IMAGE_SPRITE_DIRECTION_UP 1
#define IMAGE_SPRITE_DIRECTION_UP_RIGHT 2
#define IMAGE_SPRITE_DIRECTION_LEFT 3
#define IMAGE_SPRITE_DIRECTION_RIGHT 5
#define IMAGE_SPRITE_DIRECTION_DOWN_LEFT 6
#define IMAGE_SPRITE_DIRECTION_DOWN 7
#define IMAGE_SPRITE_DIRECTION_DOWN_RIGHT 8

//actionType 인자는 IMAGE_SPRITE_MON_ACTION 키워드를 사용하세요
void ChangeMonsterSpriteImage(int thingId, char actionType, char direction, char index, int imageId)
{
    int imagePtr;

    ImageUtilGetPtrFromID(imageId, &imagePtr);
    int *pSprite=getSpriteDbObject(thingId);
    char *pImgInfo=pSprite[SPRITEDB_IMAGE_INFORMATION];
    int *pImgTablePtrs=&pImgInfo[8+(actionType*48)];
    int *pImgTable = pImgTablePtrs[direction];

    // pImgTable[index]=imagePtr;
    SetRecoveryDataNormal(&pImgTable[index], imagePtr);
}

void ChangeSpriteImageSize(int thingId, char width, char height)
{
    int *pSprite=getSpriteDbObject(thingId);
    int value = pSprite[SPRITEDB_IMAGE_SIZE];
    char *p=&value;

    p[0]=width;
    p[1]=height;
    SetRecoveryDataNormal(&pSprite[SPRITEDB_IMAGE_SIZE], value);
}

void GetSpriteImageSize(int thingId, int *width, int *height)
{
    int *pSprite=getSpriteDbObject(thingId);
    int value = pSprite[SPRITEDB_IMAGE_SIZE];
    char *p=&value;

    width[0]=p[0];
    height[0]=p[1];
}

void ChangeSpriteLightIntensity(int thingId, char intensity)
{
    int *pSprite=getSpriteDbObject(thingId);

    SetRecoveryDataNormal(&pSprite[SPRITEDB_LIGHT_INTENSITY], ToInt(IntToFloat(intensity)));
}

void ChangeSpriteLightColor(int thingId, char *rgbArray)
{
    int *pSprite=getSpriteDbObject(thingId);

    if (!rgbArray)
        return;

    SetRecoveryDataNormal(&pSprite[SPRITEDB_LIGHT_INTENSITY_RED], rgbArray[0]);
    SetRecoveryDataNormal(&pSprite[SPRITEDB_LIGHT_INTENSITY_GREEN], rgbArray[1]);
    SetRecoveryDataNormal(&pSprite[SPRITEDB_LIGHT_INTENSITY_BLUE], rgbArray[2]);
}

void ChangeSpriteItemName(int thingId, short *wName )
{
    int *pSprite=getSpriteDbObject(thingId);

    SetRecoveryDataNormal(&pSprite[SPRITEDB_ITEMNAME], wName);
}

void ChangeSpriteItemNameAsString(int thingId, string name)
{
    char *src=StringUtilGetScriptStringPtr(name);
    short *wBuff;
    int len=StringUtilGetLength(src);

    AllocSmartMemEx((len*2)+4, &wBuff);
    NoxUtf8ToUnicode(src,wBuff);
    ChangeSpriteItemName(thingId,wBuff);
}

void ChangeSpriteItemDescription(int thingId, short *wDesc)
{
    int *pSprite=getSpriteDbObject(thingId);

    SetRecoveryDataNormal(&pSprite[SPRITEDB_ITEMDESC],wDesc);
}

void ChangeSpriteItemDescriptionAsString(int thingId, string desc)
{
    char *src=StringUtilGetScriptStringPtr(desc);
    short *wBuff;
    int len=StringUtilGetLength(src);

    AllocSmartMemEx((len*2)+4, &wBuff);
    NoxUtf8ToUnicode(src,wBuff);
    ChangeSpriteItemDescription(thingId,wBuff);
}

void ChangeSpriteCircleSize(int thingId, char size)
{
    int *pSprite=getSpriteDbObject(thingId);

    if (pSprite[SPRITEDB_EXTENT_TYPE]!=2)
        return;

    SetRecoveryDataNormal(&pSprite[SPRITEDB_EXTENT_CIRCLE], ToInt(IntToFloat(size)));
}

void ChangeSpriteZSize(int thingId, float low, float high)
{
    int *pSprite=getSpriteDbObject(thingId), *args=&low;

    SetRecoveryDataNormal(&pSprite[SPRITEDB_EXTENT_ZSIZE_LOW],args[0]);
    SetRecoveryDataNormal(&pSprite[SPRITEDB_EXTENT_ZSIZE_HIGH],args[1]);
}

void ChangeSpriteRectSize(int thingId, char width, char height)
{
    int *pSprite=getSpriteDbObject(thingId);

    if (pSprite[SPRITEDB_EXTENT_TYPE]!=3)
        return;

    SetRecoveryDataNormal(&pSprite[SPRITEDB_EXTENT_WIDTH], ToInt(IntToFloat(width)));
    SetRecoveryDataNormal(&pSprite[SPRITEDB_EXTENT_HEIGHT], ToInt(IntToFloat(height)));
}

#define SPRITE_DRAW_STATIC 4967456
#define SPRITE_DRAW_ANIMATE 4963680

void ChangeSpriteDrawFunction(int thingId, int newDrawFn, int *oldDrawFn)
{
    int *pSprite=getSpriteDbObject(thingId);

    if (oldDrawFn)
        oldDrawFn[0]=pSprite[SPRITEDB_DRAW_FUNCTION];

    pSprite[SPRITEDB_DRAW_FUNCTION]=newDrawFn;
}

#define DRAW_ANIMATE_INFO_COUNT 8
#define DRAW_ANIMATE_INFO_FRAMES 9
void ChangeSpriteAnimateImage(int thingId, int frames, int *images, int imageCount)
{
    int *pSprite=getSpriteDbObject(thingId);
    char *drawInfo = pSprite[SPRITEDB_IMAGE_INFORMATION];

    if (imageCount > drawInfo[DRAW_ANIMATE_INFO_COUNT])
    {
        allocNewImageTable(&drawInfo[4], drawInfo[DRAW_ANIMATE_INFO_COUNT], imageCount);
    }
    SetRecoveryDataNormal(&drawInfo[8], imageCount|(frames<<8));

    int *p = GetMemory(&drawInfo[4]), r=imageCount, imgptr;

    while (--r>=0)
    {
        ImageUtilGetPtrFromID(images[r], &imgptr);
        SetRecoveryDataNormal(&p[r],imgptr);
    }
}
