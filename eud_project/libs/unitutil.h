
#include "typecast.h"
#include "opcodehelper.h"
#include "stringutil.h"

int ImportCreateAtFunc()
{
    int *link;

    if (!link)
    {
        int impl[] = {
            0xAA506856, 0x5068004D, 0xFF005072, 0xFF502414, 0x50042454,
            0x082454FF, 0x4085048B, 0x680097BB, 0x004E3810, 0x2454FF50,
            0x08C48304, 0xF685F08B, 0x006A0A74, 0x2454FF56, 0x08C48314,
            0x50723068, 0x54FF5600, 0xC4830424, 0x5EC03118, 0x909090C3};
        link = &impl;
    }
    return link;
}

int CreateObjectAt(string name, float x, float y)
{
    int *builtins = 0x5c308c;
    int *pOld = builtins[53];

    builtins[53] = ImportCreateAtFunc();
    int *unitPtr = CreateMover(SToInt(name), ToInt(x), y);
    builtins[53] = pOld;
    if (unitPtr) return unitPtr[11];
    else return 0;
}

int CreateObjectById(short thingId, float x, float y)
{
    int *tb = GetMemory(0x7520d4);
    int *targ=tb[thingId];
    return CreateObjectAt(ReadStringAddressEx(targ[1]), x, y);
}

void CreateObjectAtEx(string name, float x, float y, int *pResult)
{
	int tmp = CreateObjectAt(name, x, y);
	
	if (pResult != 0)
        pResult[0] = tmp;
}

void MoveObjectVector(int unit, float xvect, float yvect)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
	{
        float *xyPos = ptr + 0x38;

		MoveObject(unit, xyPos[0] + xvect, xyPos[1] + yvect);
	}
}

int ImportUnitToPtrFunc()
{
    int *link;

    if (!link)
    {
        int codeStream[] = {
            0x50725068, 0x2414FF00, 0x511B6068, 0x54FF5000, 0xC4830424,
            0x7230680C, 0xFF500050, 0x83042454, 0xC03108C4, 0x909090C3};
        link = &codeStream;
    }
    return link;
}

int UnitToPtr(int unit)
{
    int *builtins = 0x5c308c;
    int *pOld = builtins[184];

    builtins[184] = ImportUnitToPtrFunc();
    int res = Unknownb8(unit);
    builtins[184] = pOld;
	return res;
}

int ImportUseItemFuncOffset()
{
	return 0x2fc;
}

int ImportUseItemFunc()
{
    int *link;

    if (!link)
    {
        int codeStream[] = {
            0x50731068, 0x50565500, 0x1424748B, 0x18246C8B,
            0x858B | (ImportUseItemFuncOffset() << 0x10), 0x56550000, 0x2454FF50, 0x0CC48318,
            0x835D5E58, 0x90C304C4};
        link = &codeStream;
    }
    return link;
}

//아이템을 사용할 때 실행할 함수를 등록합니다
void SetUnitCallbackOnUseItem(int unit, int callback)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
	{
        ptr[183] = ImportUseItemFunc();
        ptr[ImportUseItemFuncOffset() >> 2] = callback;
	}
}

int ImportUnitCollideFuncOffset()
{
	return 0x2fc;
}

int ImportUnitCollideFunc()
{
    int *link;

    if (!link)
    {
        int codeStream[] = {
            0x50731068, 0x50565500, 0x14246C8B, 0x1824748B,
            0x858B | (ImportUnitCollideFuncOffset() << 0x10), 0x56550000, 0x2454FF50, 0x0CC48318,
            0x835D5E58, 0x90C304C4};
		
        link = &codeStream;
    }
    return link;
}

//유닛이 충돌할 때 실행할 함수를 등록합니다
void SetUnitCallbackOnCollide(int unit, int callback)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
	{
        ptr[174] = ImportUnitCollideFunc();
        ptr[ImportUnitCollideFuncOffset() >> 2] = callback;
	}
}

int ImportUnitDieFuncOffset()
{
	return 0x228;
}

int ImportUnitDieFunc()
{
    int *link;

    if (!link)
    {
        int codeStream[] = {
            0x50731068, 0x8B565000, 0x8B102474, 0x86 | (ImportUnitDieFuncOffset() << 8),
            0x006A5600, 0x2454FF50, 0x0CC48314, 0xC483585E,
            0x9090C304};
        link = &codeStream;
    }
    return link;
}

//유닛이 파괴될 때 실행할 함수를 등록합니다
void SetUnitCallbackOnDeath(int unit, int callback)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
	{
        ptr[181] = ImportUnitDieFunc();
        ptr[ImportUnitDieFuncOffset() >> 2] = callback;
	}
}

int ImportUnitDropFuncOffset()
{
	return 0x90;
}

int ImportUnitDropFunc() //0x2fc
{
    int *link;

    if (!link)
    {
        int codeStream[] = {
            0x50731068, 0x55565300, 0x14245C8B, 0x1824748B,
            0xAE8B | (ImportUnitDropFuncOffset() << 0x10), 0x53560000, 0x2454FF55, 0x0CC48318,
            0x835B5E5D, 0x90C304C4};
        link = &codeStream;
    }
    return link;
}

//유닛을 버릴 때 실행할 함수를 등록합니다
void SetUnitCallbackOnDiscard(int unit, int callback)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
	{
        ptr[178] = ImportUnitDropFunc();
        ptr[ImportUnitDropFuncOffset()>>2] = callback;
	}
}

int ImportUnitDropPassFunction()
{
	int *link;
	
	if (!link)
	{
        int codeStream[] = {
		    0x8B555653, 0x8B10245C, 0x8B142474, 0x000000AE | (ImportUnitDropFuncOffset() << 0x8),
		    0x55535600, 0xDB62F7E8, 0x0CC483FF, 0x685B5E5D, 0x004ED290, 0x909090C3};
		link = &codeStream;
        FixCallOpcode(link + 0x14, 0x507310);
	}
	return link;
}

//유닛을 버릴 때 실행할 함수를 등록합니다
//이 함수는 버리는 부분까지 재정의 할 필요는 없습니다
void SetUnitCallbackOnDiscardBypass(int unit, int callback)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
	{
        ptr[178] = ImportUnitDropPassFunction();
        ptr[ImportUnitDropFuncOffset()>>2]=callback;
	}
}

int ImportUnitPickupFuncOffset()
{
	return 0xb8;
}

int ImportUnitPickupFunc()	//ptr+144, 184,188
{
    int *link;

    if (!link)
    {
        int codeStream[] = {
            0x50731068, 0x55565300, 0x14245C8B, 0x1824748B,
            0xAE8B | (ImportUnitPickupFuncOffset() << 0x10), 0x53560000, 0x2454FF55, 0x0CC48318,
            0x835B5E5D, 0x90C304C4};
		link = &codeStream;
    }
    return link;
}

//유닛을 픽업했을 때 실행할 함수를 등록합니다
void SetUnitCallbackOnPickup(int unit, int callback)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
	{
        ptr[177] = ImportUnitPickupFunc();
        ptr[ImportUnitPickupFuncOffset()>>2] = callback;
	}
}

//아이템을 주웠을 때 실행할 함수를 등록합니다. 이 함수는 1회성입니다
void RegistItemPickupCallback(int unit, int callback)
{
	int *ptr = UnitToPtr(unit);
	
	if (ptr)
        ptr[192]=callback;
}

void NOXLibraryEntryPointFunction()
{
	// "export UnitUtilFixCallOpcode";
    "export needinit ImportCreateAtFunc";
    "export CreateObjectAt";
	"export CreateObjectAtEx";
    "export CreateObjectById";
	// "export UnitUtilGetMemoryFloat";
	"export MoveObjectVector";
    "export needinit ImportUnitToPtrFunc";
    "export UnitToPtr";
	"export ImportUseItemFuncOffset";
    "export needinit ImportUseItemFunc";
	"export SetUnitCallbackOnUseItem";
	"export ImportUnitCollideFuncOffset";
    "export ImportUnitCollideFunc";
	"export SetUnitCallbackOnCollide";
	"export ImportUnitDieFuncOffset";
    "export ImportUnitDieFunc";
	"export SetUnitCallbackOnDeath";
	"export ImportUnitDropFuncOffset";
    "export ImportUnitDropFunc";
	"export SetUnitCallbackOnDiscard";
	"export ImportUnitPickupFuncOffset";
	"export ImportUnitPickupFunc";
	"export SetUnitCallbackOnPickup";
	"export RegistItemPickupCallback";
	"export ImportUnitDropPassFunction";
	"export SetUnitCallbackOnDiscardBypass";
}