
#include "typecast.h"
#include "callmethod.h"

//Changed by Lycoris 2023/03/11 vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#define LOCAL_RUS_TRIADA			0x002000ec
#define LOCAL_RUS_FARGUS			0x00e600e1
#define LOCAL_RUS_RUSSIANPROJECT	0x00ea00f8
#define LOCAL_RUS_8BIT				0x00ed00e5
#define LOCAL_RUS_WOLF				0x00f200e5
#define LOCAL_ENG					0x00200065
#define LOCAL_KOR					0xb1080020
//Changed by Lycoris 2023/03/11 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#define MAX_LENGTH 1024

string ReadStringAddress(int t)
{
	return ToStr((t - 0x97bb40) / 4);
}

string ReadStringAddressEx(int* targetString)
{
	int ptrs[64], cursor;

    if (cursor >= sizeof(ptrs))
        cursor = 0;

    ptrs[cursor] = targetString;
	StopScript((&ptrs[cursor++] - 0x97bb40) >> 2);
}

string StringUtilFindUnitNameById(int thingId)
{
	int* thingCount = 0x69f228;

	if (thingId > 0 && thingId < thingCount[0])
	{
		int* thingDbLink = 0x7520d4;
		int* thingDbUnitBase = thingDbLink[0];

		return ReadStringAddress(thingDbUnitBase[thingId] + 4);
	}
	return ReadStringAddress(0x587a50);
}

void NoxUtf8ToUnicode(char* src, short* dest)
{
	int i = 0;

	//Changed by Lycoris 2023/03/11 vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	int localization = 0;
    int *csfBase = 0x611c04;
    int *csfTable = csfBase[0];
    if (csfTable)
    {
        int* csfIntPtr = csfTable[3];
		localization = csfIntPtr[3];
    }
	//Changed by Lycoris 2023/03/11 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	while (i < MAX_LENGTH)
	{
		if (!src[i]) break;
		if (!(src[i] & 0x80))
		{
			dest[0] = src[i];
			i += 1;
		}
		else if ((src[i] & 0xe0) == 0xc0)
		{
			//Changed by Lycoris 2023/03/11 vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			if (src[i] == 0xD0 || src[i] == 0xD1) // russian 		
			{ 
				if (localization == LOCAL_RUS_FARGUS) 
				{
					if (src[i] == 0xD0) 
					{
						if (src[i + 1] >= 0x90 && src[i + 1] <= 0xBF) // Б~Я, а~п 						
							dest[0] = src[i + 1] + 49;
						else if (src[i + 1] == 0x90)					// А					
							dest[0] = 0xFF;
						else if (src[i + 1] == 0x81) 					// Ё						
							dest[0] = 0xC6;
					} 
					else
					{
						if (src[i + 1] >= 0x80 && src[i + 1] <= 0x8E)	// р~ю				
							dest[0] = src[i + 1] + 113;
						else if (src[i + 1] == 0x8F)					// я					
							dest[0] = 0xC0;
						else if (src[i + 1] == 0x91)					// ё				
							dest[0] = 0xE6;
					}
				} 
				else 
				{
					if (src[i] == 0xD0)
					{
						if (src[i + 1] >= 0x90 && src[i + 1] <= 0xBF)	// A~Я, а~п 						
							dest[0] = src[i + 1] + 48;					
						else if (src[i + 1] == 0x81) 					// Ё						
							dest[0] = 0xC5;
					} 
					else
					{
						if (src[i + 1] >= 0x80 && src[i + 1] <= 0x8F)	// р~ю				
							dest[0] = src[i + 1] + 112;
						else if (src[i + 1] == 0x91)					// ё
						{						
							if (localization == LOCAL_RUS_WOLF || localization == LOCAL_RUS_8BIT)
								dest[0] = 0xB8;
							else if (localization == LOCAL_RUS_RUSSIANPROJECT
									 || localization == LOCAL_RUS_TRIADA)
								dest[0] = 0xE5;
						}
					}
				}
			} 
			else 
			{ // not russian		
				dest[0] = ((src[i] & 0x1f) << 6) | (src[i + 1] & 0x3f);
			}
			//Changed by Lycoris 2023/03/11 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			i += 2;
		}
		else if ((src[i] & 0xf0) == 0xe0)
		{
			dest[0] = ((src[i] & 0xf) << 12) | ((src[i + 1] & 0x3f) << 6) | (src[i + 2] & 0x3f);
			i += 3;
		}
		dest += 2;
	}
	dest[0] = 0;
}

int NoxUnicodeToUtf8(short* src, char* dest)
{
	int rep = -1;
	short front;
	char* oldDest = dest;

	while (++rep < MAX_LENGTH)
	{
		front = src[rep];
		if (!front) break;
		if (front < 0x80)
		{
			dest[0] = front;
			dest += 1;
		}
		else if (front < 0x800)
		{
			dest[0] = ((front >> 6) & 0x1f) | 0xc0;
			dest[1] = (front & 0x3f) | 0x80;
			dest += 2;
		}
		else
		{
			dest[0] = ((front >> 12) & 0xf) | 0xe0;
			dest[1] = ((front >> 6) & 0x3f) | 0x80;
			dest[2] = (front & 0x3f) | 0x80;
			dest += 3;
		}
	}
	dest[0] = 0;
	return dest - oldDest;
}

int StringUtilGetScriptStringPtr(string scrstring)
{
	int scrStrId = scrstring;
	int* scrStrBase = 0x97bb40;

	return scrStrBase[scrStrId];
}

void StringUtilGetScriptStringPtrEx(string scrstring, char* pDest)
{
	int* destImpl = pDest;
	destImpl[0] = StringUtilGetScriptStringPtr(scrstring);
}

int StringUtilGetLength(char* str)
{
	char* oldStr = str;

	for (str; str[0]; str+=1)
	{
	}
	return str - oldStr;
}

int StringUtilGetWideLength(short* wstr)
{
	short* oldWstr = wstr;

	for (wstr; wstr[0]; wstr += 2)
	{
	}
	return (wstr - oldWstr) >> 1;
}

void StringUtilCopy(char* src, char* dest)
{
	int rep = -1;

	while (++rep < MAX_LENGTH)
	{
		dest[rep] = src[rep];
		if (!src[rep])
			break;
	}
}

void StringUtilWideCopy(short* src, short* dest)
{
	int rep = -1;

	while (++rep < MAX_LENGTH)
	{
		dest[rep] = src[rep];
		if (!src[rep])
			break;
	}
}

int StringUtilCompare(char* s1, char* s2)
{
	while (s1[0] && (s1[0] == s2[0]))
	{
		s1 += 1;
		s2 += 1;
	}
	return s1[0] - s2[0];
}

//두개의 유니코드 문자열을 비교합니다. 같으면 0 가 반환됩니다
int StringUtilWideCompare(short* s1, short* s2)
{
	while (s1[0] && (s1[0] == s2[0]))
	{
		s1 += 2;
		s2 += 2;
	}
	return s1[0] - s2[0];
}

void StringUtilAppend(char* dest, char* src)
{
	int length = StringUtilGetLength(dest);

	while (src[0])
	{
		dest[length++] = src[0];
		src += 1;
	}
	dest[length] = 0;
}

#undef MAX_LENGTH
