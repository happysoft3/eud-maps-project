
#include "callmethod.h"
#include "opcodehelper.h"

int BindImpl()
{
	int *ptr;

    if (!ptr)
    {
        int impl[] = {
            0x83EC8B55, 0xC5E80CEC, 0x89F1E6F5, 0xBDE8F445, 0x89F1E6F5, 0xB5E8F845, 0x89F1E6F5, 0x3151FC45,
            0x08488AC9, 0x1474C984, 0xFEF4458B, 0x048B51C9, 0xE8905088, 0xF1E6F578, 0xE8EB5959, 0x979724B8,
            0x8D30FF00, 0x30FFFC40, 0x50F8458B, 0xE6F63FE8, 0x18C483F1, 0x90C35D59};
        ptr=&impl;
        FixCallOpcode(ptr + 6, 0x507250);
        FixCallOpcode(ptr + 0x0e, 0x507250);
        FixCallOpcode(ptr + 0x16, 0x507250);
        FixCallOpcode(ptr + 0x33, 0x507230);
        FixCallOpcode(ptr + 0x4c, 0x507310);
    }
    return ptr;
}

int BindInvoke(int *fptr, int functionId, int *args)
{
    StopScript(GroupRunAway(fptr, functionId, args));
}

int Bind(int functionId, int *args)
{
    int *builtins=0x5c308c;
    int *pOld = builtins[165];
    
    builtins[165]=BindImpl();
    int *scrBase = 0x75ae28;
    int ret= BindInvoke(scrBase[0] + (functionId*0x30), functionId, args);
    builtins[165]=pOld;

    return ret;
}

void NOXLibraryEntryPointFunction()
{
	"export needinit BindImpl";
	"export BindInvoke";
	"export Bind";
}


