
#include "define.h"
#include "callmethod.h"
#include "playerinfo.h"
#include "hash.h"
#include "stringutil.h"
#include "printutil.h"


int ChatMessageHashInstance()
{
	int instance;
	
	if (!instance)
		HashCreateInstance(&instance);
	
	return instance;
}

void ChatMessageLocalStorage(int *pArr, int *pLength)
{
	int szLocal[40];
	
	if (pArr)
		pArr[0]=szLocal;
	if (pLength)
		pLength[0]=sizeof(szLocal);
}

void ChatMessageWideStringStorage(int index, int *pDestPtr)
{
	int szWideStrArr[400];
	
	pDestPtr[0]=&szWideStrArr[index*10];
}

int ChatMessageHandler(int *pChat, int *pInstance)
{
	int netId = pChat[164];
	int *pUser = GetPlayerPtrByNetCode(netId);
	int hashkey = pChat[0];
	
	if (pUser && hashkey)
	{
		int hashval;
		
		if (HashGet(pInstance, hashkey, &hashval, FALSE))
		{
			int pLocal;
			
			ChatMessageLocalStorage(&pLocal, 0);						
			if (!StringUtilWideCompare(pChat, GetMemory(pLocal + (hashval*8))))
			{
				UniChatMessage(pUser[11], " ", 1);
				if (!CallFunctionWithArgInt(GetMemory(pLocal + (hashval<<3)+4), pUser[11]))
					return FALSE;
			}
		}
	}
	return TRUE;
}

int ChatMessageLoopFrame()
{
	return 10;
}

void ChatMessageLoop(int fps)
{
	int chatcur = GetMemory(0x6f8acc), chatnext;
	int pInstance = ChatMessageHashInstance();
	
	while (chatcur)
	{
		chatnext = GetMemory(chatcur+0x2ac);
		
		if (!ChatMessageHandler(chatcur, pInstance))
			break;
		chatcur=chatnext;
	}
	FrameTimerWithArg(fps, fps, ChatMessageLoop);
}

void ChatMessageEventPushback(string eventString, int eventCb)
{
	int index;
	int c, length;
	
	ChatMessageLocalStorage(&c, &length);
	if (index < length)
	{
		int pWideString, *pArr = c;
		
		ChatMessageWideStringStorage(index>>1, &pWideString);
		
		NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(eventString), pWideString);
		HashPushback(ChatMessageHashInstance(), GetMemory(pWideString), index>>1);
		
		pArr[index++] = pWideString;
		pArr[index++]=eventCb;
	}
}

int ChatMessageStartLoopDelay()
{
	return 10;
}

void ChatMessageInit()
{
	ChatMessageHashInstance();
	FrameTimerWithArg(ChatMessageStartLoopDelay(), ChatMessageLoopFrame(), ChatMessageLoop);
}

void NOXLibraryEntryPointFunction()
{
	"export needinit ChatMessageInit";
}
