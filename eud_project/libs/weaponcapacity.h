
#include "callmethod.h"
#include "opcodehelper.h"
#include "playerinfo.h"
#include "unitutil.h"

int g_importWeaponCapacityUpdate, g_updateConsumablesWeaponCapacity;

/*
int GetMemory(int a){}
void SetMemory(int a, int b){}
int GetScrDataField(int functionName){}
int GetPlayerIndex(int a){}
int UnitToPtr(int a){}
void FixCallOpcode(int a, int b){}
*/

int ImportWeaponCapacityUpdate()
{
    int arr[8], link;

    if (!link)
    {
        arr[0] = 0x624AE851; arr[1] = 0x488BFFDB; arr[2] = 0x488B510C; arr[3] = 0x488B5108; arr[4] = 0x30FF5104; arr[5] = 0xD87297E8; arr[6] = 0x10C483FF; arr[7] = 0x9090C359;
        link = GetScrDataField(-g_importWeaponCapacityUpdate);
        FixCallOpcode(link + 1, 0x507250);
        FixCallOpcode(link + 0x14, 0x4d82b0);
    }
    return link;
}

//지팡이의 잔량 표시정보를 갱신합니다
//plrUnit: 지팡이를 소유한 플레이어 유닛id 입니다
//staff: 지팡이 유닛 id 입니다
void UpdateConsumablesWeaponCapacity(int plrUnit, int weapon)
{
    int pIndex = GetPlayerIndex(plrUnit), staffPtr = UnitToPtr(weapon), cur, max;
	int link = GetScrDataField(-g_updateConsumablesWeaponCapacity) + 8;

    if (staffPtr && pIndex >= 0)
    {
        cur = GetMemory(GetMemory(staffPtr + 0x2e0) + 0x6c) & 0xff;
        max = (GetMemory(GetMemory(staffPtr + 0x2e0) + 0x6c) >> 8) & 0xff;

        int temp = GetMemory(0x5c3108);
        SetMemory(0x5c3108, ImportWeaponCapacityUpdate());
        Unused1f(link);
        SetMemory(0x5c3108, temp);
    }
}

//소모류 무기의 남은 잔량을 얻습니다
//올바르지 않을 경우 -1이 반환됩니다
int GetConsumablesWeaponCurrentAmount(int weapon)
{
	int ptr = UnitToPtr(weapon);
	
	if (ptr)
	{
		int amountPtr = GetMemory(ptr + 0x2e0);
		
		if (amountPtr)
		{
			if (GetMemory(ptr + 8) & 0x1000)
				return GetMemory(amountPtr + 0x6c) & 0xff;
			else if (GetMemory(ptr + 8) & 0x1000000)
				return GetMemory(amountPtr) & 0xff;
		}
	}
	return -1;
}

//소모류 무기의 최대 수용량을 얻습니다
//올바르지 않을 경우 -1이 반환됩니다
int GetConsumablesWeaponMaxCapacity(int weapon)
{
	int ptr = UnitToPtr(weapon);
	
	if (ptr)
	{
		int amountPtr = GetMemory(ptr + 0x2e0);
		
		if (amountPtr)
		{
			if (GetMemory(ptr + 8) & 0x1000)
				return (GetMemory(amountPtr + 0x6c) >> 8) & 0xff;
			else if (GetMemory(ptr + 8) & 0x1000000)
				return (GetMemory(amountPtr) >> 8) & 0xff;
		}
	}
	return -1;
}

//소모류 무기의 현재잔량 및 최대수용량을 설정합니다
//weapon: 지팡이 류를 포함한 무기의 id
//currentAmount: 0~255, -1을 주면 현재 속성은 수정되지 않습니다
//maxAmount: 0~255, -1을 주면 현재 속성은 수정되지 않습니다
void SetConsumablesWeaponCapacity(int weapon, int currentAmount, int maxAmount)
{
	int ptr = UnitToPtr(weapon);
	
	if (ptr)
	{
		int amountPtr = GetMemory(ptr + 0x2e0);
		
		if (!amountPtr)
			return;
		
		int typeOffset = 0;
		
		if (GetMemory(ptr + 8) & 0x1000)
			typeOffset = 0x6c;
		int sectionValue = GetMemory(amountPtr + typeOffset);
		int presv = sectionValue & (~0xffff);
		
		if (maxAmount == -1)
			maxAmount = (sectionValue >> 8) & 0xff;
		else
			maxAmount = maxAmount & 0xff;
		if (currentAmount == -1)
			currentAmount = sectionValue & 0xff;
		else
			currentAmount = currentAmount & 0xff;
		
		SetMemory(amountPtr + typeOffset, presv | (maxAmount << 8) | currentAmount);
	}
}

void NOXLibraryEntryPointFunction()
{
	"export ImportWeaponCapacityUpdate";
	"export UpdateConsumablesWeaponCapacity";
	"export GetConsumablesWeaponCurrentAmount";
	"export GetConsumablesWeaponMaxCapacity";
	"export SetConsumablesWeaponCapacity";
	
	g_importWeaponCapacityUpdate = ImportWeaponCapacityUpdate;
	g_updateConsumablesWeaponCapacity = UpdateConsumablesWeaponCapacity;
}
