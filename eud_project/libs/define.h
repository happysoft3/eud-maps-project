
#include "..\noxscript\builtins.h"
#include "noxmemlib.h"

#define TRUE 1
#define FALSE 0
#define OTHER -1
#define SELF -2

#define NULLPTR 0
#define NULLPTR_F 0.0

void Nop(int n)
{ }

void NOXLibraryEntryPointFunction()
{ }
