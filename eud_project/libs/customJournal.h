
#include "define.h"
#include "winapi.h"
#include "memutil.h"

#define MY_JOURNAL_OFFSET 0x5acfb4
#define MAX_TABLE_COUNT 1024
void MakeJournalTable()
{
    int *p=MY_JOURNAL_OFFSET;

    if (p[0]!=0x726f7070) //70 70 6F 72
        return;

    int *pMem = MemAlloc(MAX_TABLE_COUNT*8);

    NoxDwordMemset(pMem, MAX_TABLE_COUNT*2, 0);

    p[0]=pMem;
}

int referenceJournalIndex()
{
    int *p=MY_JOURNAL_OFFSET;
    int *pp=p[0];

    while (pp[0])
        pp=&pp[2];
    return pp;
}

void referenceJournalIndexEx(char *s)
{
    int *p=MY_JOURNAL_OFFSET;
    int *pp=p[0];
    char *res = NULLPTR;
    int *prev = pp;

    while (pp[0])
    {
        if (!StringUtilCompare(s,pp[0]))
            res=pp[0];
        prev=pp[0];
        pp=&pp[2];
    }
    pp=prev;
    if (res)
    {
        res[0]=pp[0];
        res[1]=pp[1];
        pp[0]=NULLPTR;
        pp[1]=NULLPTR;
    }
}

void AppendNewJournal(string questName, string explain)
{
    int questNameLen=StringUtilGetLength(StringUtilGetScriptStringPtr(questName))+16;
    int explainLen=StringUtilGetLength(StringUtilGetScriptStringPtr(explain))+4;
    char *pQuest = MemAlloc(questNameLen);
    short *pwExplain = MemAlloc(explainLen*2);

    int *p = referenceJournalIndex();

    string prefix = "Journal:";

    StringUtilCopy(StringUtilGetScriptStringPtr(prefix), pQuest);
    StringUtilAppend(pQuest, StringUtilGetScriptStringPtr(questName));
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(explain), pwExplain);
    p[0]=pQuest;
    p[1]=pwExplain;
}

void EraseJournalExtended(string questName)
{
    char *p=StringUtilGetScriptStringPtr(questName);

    referenceJournalIndexEx(p);
}

void InitializeExtendedJournalEnabler()
{
    int *test = 0x469ee2;

    if (test[0]!=0xfffa52ea) //EA 52 FA FF
        return;

    char code[]={
        0x56, 0x57, 0x8B, 0x74, 0x24, 0x0C, 0x85, 0xF6, 0x74, 0x18, 0x8B, 0x7C, 0x24, 0x10, 0x85, 0xFF, 0x74, 0x10, 0x8A, 0x0E,
        0x8A, 0x07, 0x38, 0xC8, 0x75, 0x08, 0x84, 0xC0, 0x74, 0x08, 0x46, 0x47, 0xEB, 0xF0, 0x31, 0xC0, 0xEB, 0x05, 0xB8, 0x01,
        0x00, 0x00, 0x00, 0x5F, 0x5E, 0xC3, 0x51, 0x53, 0x56, 0x8B, 0x74, 0x24, 0x10, 0x8B, 0x1D, 0xB4, 0xCF, 0x5A, 0x00, 0x85,
        0xDB, 0x74, 0x20, 0x8B, 0x03, 0x85, 0xC0, 0x74, 0x1A, 0x50, 0x56, 0xE8, 0xB4, 0xFF, 0xFF, 0xFF, 0x83, 0xC4, 0x08, 0x85,
        0xC0, 0x75, 0x05, 0x8D, 0x5B, 0x08, 0xEB, 0xE7, 0x8B, 0x43, 0x04, 0x5E, 0x5B, 0x59, 0xC3, 0x5E, 0x5B, 0x59, 0x68, 0xD0,
        0xF1, 0x40, 0x00, 0xC3
    };
    int oldProtect;
    WinApiVirtualProtect(0x469e00, 1024, 0x40, &oldProtect);

    char *pCode = MemAlloc(sizeof(code));
    NoxByteMemCopy(code, pCode, sizeof(code));
    FixCallOpcode(0x469ee1, pCode + 0x2e);
    WinApiVirtualProtect(0x469e00, 1024, oldProtect, NULLPTR);
    
}

void NOXLibraryEntryPointFunction()
{
    "export needinit MakeJournalTable";
    "export needinit InitializeExtendedJournalEnabler";
}
