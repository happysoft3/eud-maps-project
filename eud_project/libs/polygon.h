
#include "define.h"
#include "stringutil.h"
#include "memutil.h"
#include "array.h"

//현재 지도의 전체 폴리곤 수를 반환합니다
//맵 전체 지역을 포함하는, 기본 폴리곤 개수를 포함합니다
void PolygonUtilGetCount(short *pCount)
{
    short *pPolygons=0x595BC0;

    pCount[0]=pPolygons[0];
}

//폴리곤 포인터를 얻기 위한 함수입니다
//폴리곤의 이름으로 검색합니다
int PolygonUtilGetPolygonFromName(string polygonName, int *pDestPtr)
{
    short count;

    PolygonUtilGetCount(&count);
    if (!count)
        return FALSE;
    if (--count==0)
        return FALSE;
    
    char *src;

    StringUtilGetScriptStringPtrEx(polygonName, &src);
    int *pPoly = 0x65b344;

    while (--count>=0)
    {
        if (!StringUtilCompare(pPoly+4, src))
        {
            pDestPtr[0]=pPoly;
            return TRUE;
        }
        pPoly+=140;
    }
    return FALSE;
}

//폴리곤 포인터를 얻기 위한 함수입니다
//폴리곤의 id 으로 검색합니다
int PolygonUtilGetPolygonFromId(short id, int *pDestPtr)
{
    short count;

    PolygonUtilGetCount(&count);
    if (--count <= id)
        return FALSE;

    int *pPolybase = 0x65b344;
    pDestPtr[0] = pPolybase+(140*id);
    return TRUE;
}

//폴리곤의 이름을 얻기 위한 함수입니다
string PolygonUtilGetName(int *pPolygon)
{
    return ReadStringAddressEx(pPolygon+4);
}

//폴리곤의 색상을 얻기 위한 함수입니다
//pColor 배열은, 최소 3바이트 이상이어야 합니다//
void PolygonUtilGetColor(int *pPolygon, char *pColor)
{
    int color=pPolygon[26];
    char *pp=&color;

    NoxByteMemCopy(pp, pColor, 3);
}

void PolygonUtilSetColor(int *pPolygon, char *pSrcColor)
{
    char *pcl=ArrayRefN(pPolygon, 26);

    NoxByteMemCopy(pSrcColor, pcl, 3);
}

//폴리곤으로 부터, 점 개수 그리고, 미니맵 번호를 얻기 위한 함수입니다
void PolygonUtilGetInfo(int *pPolygon, short *pDestPoints, short *pDestMinimapId)
{
    short *pInfo = ArrayRefN(pPolygon, 32);

    if (pDestPoints) pDestPoints[0] = pInfo[0];
    if (pDestMinimapId) pDestMinimapId[0] = pInfo[1];
}

//폴리곤으로 부터, 점 배열을 얻기 위한 함수입니다
void PolygonUtilGetPointArray(int *pPolygon, int *pDestArray)
{
    pDestArray[0]=pPolygon[27];
}

//사용하지 않을 경우, -1 으로 설정하십시오
//아마도, 서버사이드
void PolygonUtilRegMonEventCallback(int *pPolygon, int mobEventFn)
{
    pPolygon[31]=mobEventFn;
}

//사용하지 않을 경우, -1 으로 설정하십시오
//아마도, 서버사이드
void PolygonUtilRegPlayerEventCallback(int *pPolygon, int playerEventFn)
{
    pPolygon[29]=playerEventFn;
}

//점을 위치 값으로 돌려줍니다//
void PolygonUtilPointToXY(int pointId, float *pDestXY)
{
    int *pPointbase= 0x6572b8;
    int *pPoint=pPointbase+(pointId*16);

    pDestXY[0]=pPoint[1];
    pDestXY[1]=pPoint[2];
}

void NOXLibraryEntryPointFunction()
{
"export PolygonUtilGetCount";
"export PolygonUtilGetPolygonFromName";
"export PolygonUtilGetPolygonFromId";
"export PolygonUtilGetName";
"export PolygonUtilGetColor";
"export PolygonUtilSetColor";
"export PolygonUtilGetInfo";
"export PolygonUtilGetPointArray";
"export PolygonUtilRegMonEventCallback";
"export PolygonUtilRegPlayerEventCallback";
"export PolygonUtilPointToXY";
}

