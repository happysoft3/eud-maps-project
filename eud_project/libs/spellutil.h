

#include "typecast.h"
#include "unitstruct.h"
#include "unitutil.h"
#include "playerinfo.h"


int g_importRemoveSneakDel, g_importgetspell;


#define SPELLFLAG_ON_OFFENSIVE 0x40000000
#define SPELLFLAG_ON_REACTION 0x08000000
#define SPELLFLAG_ON_ESCAPE 0x80000000
#define SPELLFLAG_ON_DEFFENSIVE 0x10000000
#define SPELLFLAG_ON_DISABLING 0x20000000


//reference with https://cafe.naver.com/noxfriends/1777


int ImportRemoveSneakDelay()
{
    int arr[7], link;

    if (!link)
    {
        arr[0] = 0x72506850; arr[1] = 0x14FF0050; arr[2] = 0xC3006824; arr[3] = 0x046A004F; arr[4] = 0x2454FF50; arr[5] = 0x10C48308; arr[6] = 0x9090C358;
        link = GetMemory(GetMemory(0x75ae28) + (0x30 * (-g_importRemoveSneakDel)) + 0x1c);
    }
    return link;
}

void RemoveTreadLightly(int plrUnit)
{
    int ptr = UnitToPtr(plrUnit), temp = GetMemory(0x5c336c);

    if (ptr)
    {
        if (GetMemory(ptr + 0x08) & 0x04)
        {
            SetMemory(0x5c336c, ImportRemoveSneakDelay());
            Unknownb8(ptr);
            SetMemory(0x5c336c, temp);
        }
    }
}

int ImportGetSpellNumber()
{
	int arr[11], link;

	if (!link)
	{
		arr[0] = 0x50725068; arr[1] = 0x2414FF00; arr[2] = 0x4085048B; arr[3] = 0x680097BB; arr[4] = 0x004243F0; arr[5] = 0x2454FF50;
		arr[6] = 0x08C48304; arr[7] = 0x50723068; arr[8] = 0x54FF5000; arr[9] = 0xC4830424; arr[10] = 0xC3C0310C;
        link = GetMemory(GetMemory(0x75ae28) + (0x30 * (-g_importgetspell)) + 0x1c);
	}
	return link;
}

int SpellUtilGetId(string spell)
{
	int temp = GetMemory(0x5c3204), res;

	SetMemory(0x5c3204, ImportGetSpellNumber());
	res = Unused5e(spell);
	SetMemory(0x5c3204, temp);
	return res;
}

int GetSpellNumber(string spell)
{
	return SpellUtilGetId(spell) << 2;
}

//플레이어의 전사 능력 쿨다운을 얻습니다, 실패시 -1
//plrUnit: 플레이어 유닛 id(전사만)
//aSlot: 능력번호- berserker=1, warcry=2, harpoon=3, TreadLightly=4, EyeOfWolf=5
int SpellUtilGetPlayerAbilityCooldown(int plrUnit, int aSlot)
{
	int ptr = UnitToPtr(plrUnit);
	
	while (ptr)
	{
		if (!(GetMemory(ptr + 8) & 4))
			break;
		if (aSlot > 5 || aSlot <= 0)
			break;
		
		int pIndex = GetPlayerIndex(plrUnit);
		
		if (pIndex < 0)
			break;
		
		return GetMemory(0x753600 + (pIndex * 24) + (aSlot << 2));
	}
	return -1;
}

//플레이어의 전사 능력 쿨다운을 설정합니다
//plrUnit: 플레이어 유닛 id(전사만)
//aSlot: 능력번호- berserker=1, warcry=2, harpoon=3, TreadLightly=4, EyeOfWolf=5
//setTo: 설정값
void SpellUtilSetPlayerAbilityCooldown(int plrUnit, int aSlot, int setTo)
{
	int ptr = UnitToPtr(plrUnit);
	
	while (ptr)
	{
		if (!(GetMemory(ptr + 8) & 4))
			break;
		if (aSlot > 5 || aSlot <= 0)
			break;
		
		int pIndex = GetPlayerIndex(plrUnit);
		
		if (pIndex < 0)
			break;
		
		SetMemory(0x753600 + (pIndex * 24) + (aSlot << 2), setTo);
		return;
	}
}

//마법 유닛의 마법 명중률을 설정합니다
//unit- 유닛 id
//rate- 명중률 0.0~1.0
void SpellUtilSetSpellAimRate(int unit, float rate)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x528, ToInt(rate));
}

void SpellUtilSetSpellDelayOnReaction(int unit, int min, int max)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x5a8, min | (max << 0x10));
}

void SpellUtilSetSpellDelayOnDeffensive(int unit, int min, int max)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x5b0, min | (max << 0x10));
}

void SpellUtilSetSpellDelayOnDisabling(int unit, int min, int max)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x5b8, min | (max << 0x10));
}

void SpellUtilSetSpellDelayOnOffensive(int unit, int min, int max)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x5c0, min | (max << 0x10));
}

void SpellUtilSetSpellDelayOnEscape(int unit, int min, int max)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x5c8, min | (max << 0x10));
}

//유닛의 스킬 레밸을 설정합니다 1~5
void SpellUtilSetUnitSpellLevel(int unit, int spellLevel)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	if (spellLevel < 0)
		spellLevel = 1;
	else if (spellLevel > 5)
		spellLevel = 5;
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x7f8, spellLevel);
}

//유닛에게 스킬을 제공합니다
void SpellUtilSetUnitSpell(int unit, string spellname, int spellflag)
{
	if (!IsMonsterUnit(unit))
		return;
	
	int ptr = UnitToPtr(unit);
	
	SetMemory(GetMemory(ptr + 0x2ec) + 0x5d0 + GetSpellNumber(spellname), spellflag);
}

void NOXLibraryEntryPointFunction()
{
    "export ImportRemoveSneakDelay";
    "export RemoveTreadLightly";
    "export needinit ImportGetSpellNumber";
	"export SpellUtilGetId";
    "export GetSpellNumber";
	"export SpellUtilGetPlayerAbilityCooldown";
	"export SpellUtilSetPlayerAbilityCooldown";
	"export SpellUtilSetSpellAimRate";
	"export SpellUtilSetSpellDelayOnReaction";
	"export SpellUtilSetSpellDelayOnDeffensive";
	"export SpellUtilSetSpellDelayOnDisabling";
	"export SpellUtilSetSpellDelayOnOffensive";
	"export SpellUtilSetSpellDelayOnEscape";
	"export SpellUtilSetUnitSpellLevel";
	"export SpellUtilSetUnitSpell";

    g_importRemoveSneakDel = ImportRemoveSneakDelay;
    g_importgetspell = ImportGetSpellNumber;
}