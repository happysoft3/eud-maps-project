
#include "define.h"
#include "memutil.h"
#include "mathlab.h"
#include "linked_list.h"

int GetQueueTimerList()
{
    int list;

    if (!list)
        LinkedListCreateInstance(&list);
    return list;
}

#define TIMERQ_ELEM_FPS 0
#define TIMERQ_ELEM_ARG 1
#define TIMERQ_ELEM_FN 2
#define TIMERQ_ELEM_MAX 3

void QueueTimerLoopProc()
{
    int node, next, *elem, fps=GetMemory(0x84ea04);

    LinkedListFront(GetQueueTimerList(), &node);
    while (node)
    {
        elem= LinkedListGetValue(node);
        LinkedListNext(node, &next);
        if (elem[TIMERQ_ELEM_FPS]<=fps)
        {
            LinkedListPop(GetQueueTimerList(), node);
            CallFunctionWithArg(elem[TIMERQ_ELEM_FN], elem[TIMERQ_ELEM_ARG]);
            FreeSmartMemEx(elem);
        }
        node=next;
    }
    FrameTimer(1,__this_function_id__);
}

void PushTimerQueue(int fps, int arg, int fn)
{
    int *node;

    AllocSmartMemEx(TIMERQ_ELEM_MAX*4, &node);
    node[TIMERQ_ELEM_FPS]=GetMemory(0x84ea04)+fps;
    node[TIMERQ_ELEM_ARG]=arg;
    node[TIMERQ_ELEM_FN]=fn;
    LinkedListPushback(GetQueueTimerList(), node, NULLPTR);
}

#undef TIMERQ_ELEM_FPS
#undef TIMERQ_ELEM_ARG
#undef TIMERQ_ELEM_FN
#undef TIMERQ_ELEM_MAX

void StartQueueTimerEngine()
{
    QueueTimerLoopProc();
}

void NOXLibraryEntryPointFunction()
{
    "export GetQueueTimerList";
    "export QueueTimerLoopProc";
    "export PushTimerQueue";
    "export needinit StartQueueTimerEngine";
}
