
#include "define.h"
#include "unitutil.h"
#include "unitstruct.h"
#include "stringutil.h"

#define NPC_H_NPC_ID 1343
#define NPC_H_MAIDEN_ID 1385
#define NPC_H_BEAR2_ID 1364

int NPCColorStuff(int *pColorIndex)
{
    if (pColorIndex[0] == 3)
    {
        pColorIndex[0] = 0;
        return pColorIndex[0];
    }
    return pColorIndex[0]++;
}

void CreateNewNPC(float xpos, float ypos, int *pDest)
{
    int mon = CreateObjectAt(StringUtilFindUnitNameById(NPC_H_NPC_ID), xpos, ypos);

    if (pDest)
        pDest[0] = mon;

    int *ptr = UnitToPtr(mon), rep=-1;

    while (++rep<32)
        ptr[140+rep]=0x400;
    int *pec = ptr[187];

    pec[94] = '@';
    pec[331] = 255; //Strength

    int color[] = {Random(0,255),Random(0,255),Random(0,255)}, colorIndex=0;

    pec[519]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[520]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[521]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[522]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[523]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8);
}

int CreateSingleColorMaidenAt(int red, int grn, int blue, float xpos, float ypos)
{
    int unit = CreateObjectAt(StringUtilFindUnitNameById(NPC_H_BEAR2_ID), xpos, ypos);
    int *ptr = UnitToPtr(unit);

    if (ptr == NULLPTR)
        return NULLPTR;
    ptr[1] = NPC_H_MAIDEN_ID;

    int u = 0;
    while (u < 32)
        ptr[140 + (u++)] = 0x400;

    int *ecptr = ptr[187];

    ecptr[94] = 0xa0;
    ecptr[122] = VoiceList(7);

    int *colrarr = ecptr + 0x81c;

    colrarr[0] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[1] = grn | (blue << 8) | (red << 16) | (grn << 24);
    colrarr[2] = blue | (red << 8) | (grn << 16) | (blue << 24);
    colrarr[3] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[4] = grn | (blue << 8);

    return unit;
}

int NpcUtilImportItemEquip()
{
    int *pp;

    if (!pp)
    {
        int codeStream[]={
            0x50EC8B55, 0xE804EC83, 0xFFDB6244, 0xE8240489, 0xFFDB623C,
            0x502434FF, 0xDA1F53E8, 0x0CC483FF, 0x90C35D58};
        pp=&codeStream;
        FixCallOpcode(pp + 0x7, 0x507250);
        FixCallOpcode(pp + 0xf, 0x507250);
        FixCallOpcode(pp + 0x18, 0x4f2f70);
    }
    return pp;
}

void NPCDressupEquipment(int humanUnit, int item, char equipMode)
{
    int *unitPtr = UnitToPtr(humanUnit), *itemPtr = UnitToPtr(item);

    if (unitPtr && itemPtr)
    {
        int *pExec=0x5c31f4;
        int *pOld=pExec[0],*pCode=NpcUtilImportItemEquip();
        FixCallOpcode(pCode+0x18, (equipMode==TRUE)*0x4f2f70 | (equipMode==FALSE)*0x4f2fb0);
        pExec[0]=pCode;
        Unused5a(unitPtr, itemPtr);
        pExec[0]=pOld;
    }
}

void NOXLibraryEntryPointFunction()
{
    "export NPCColorStuff";
    "export CreateNewNPC";
    "export CreateSingleColorMaidenAt";
    "export NpcUtilImportItemEquip";
    "export NPCDressupEquipment";
}
