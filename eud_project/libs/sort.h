
#include "define.h"
#include "memutil.h"
#include "array.h"

int QsortStackInt()
{
    int cur, stack[1024];
    int *pStackInfo;

    if (pStackInfo==0)
    {
        int stackInfo[] = {&cur, &stack};

        pStackInfo = &stackInfo;
    }
    return pStackInfo;
}

void QsortStackIntPush(int pushdata)
{
    int *pStackInfo = QsortStackInt();
    int *stack = pStackInfo[1], *index = pStackInfo[0];

    stack[index[0]++] = pushdata;
}

int QsortStackIntPop()
{
    int *pStackInfo = QsortStackInt();
    int *stack = pStackInfo[1], *index = pStackInfo[0];

    return stack[--index[0]];
}

void QuicksortSwapPrivate(int *first, int *second)
{
    int tmp = first[0];

    first[0] = second[0];
    second[0] = tmp;
}

void QuicksortPrivate(int *pArr, int start, int end)
{
    int left = start, right = end;
    int mid = pArr[(left + right)/2];

    if (left < right)
    {
        while (true)
        {
            while (pArr[left] < mid) left += 1;
            while (pArr[right] > mid) right -= 1;
            if (left <= right)
                QuicksortSwapPrivate(ArrayRefN(pArr, left++), ArrayRefN(pArr, right--));
            if (left < right) continue;
            else break;
        }
        QsortStackIntPush(left);
        QsortStackIntPush(end);
        QuicksortPrivate(pArr, start, right);
        QuicksortPrivate(pArr, QsortStackIntPop(), QsortStackIntPop());
    }
}

void Quicksort(int *pArr, int length)
{
    QuicksortPrivate(pArr, 0, --length);
}

void MergesortPrivate(int *pArr, int start, int mid, int end)
{
    int left = start, right = mid + 1;
    int length = end - start + 1;
    int *tmp = MemAlloc(length*4), wr = 0;

    while (1)
    {
        if (left > mid)
            tmp[wr++] = pArr[right++];
        else if (right > end)
            tmp[wr++] = pArr[left++];
        else if (pArr[left] < pArr[right])
            tmp[wr++] = pArr[left++];
        else
            tmp[wr++] = pArr[right++];
        
        if (wr < length)
            continue;
        break;
    }
    int rep = -1;

    while ((++rep) < length)
        pArr[start + rep] = tmp[rep];
    MemFree(tmp);
}

void MergesortPartion(int *pArr, int start, int end)
{
    if (end - start < 1)
        return;
    
    int mid = (start + end)/2;

    QsortStackIntPush(start);
    QsortStackIntPush(mid);
    QsortStackIntPush(end);
    QsortStackIntPush(mid + 1);
    QsortStackIntPush(end);
    MergesortPartion(pArr, start, mid);
    MergesortPartion(pArr, QsortStackIntPop(), QsortStackIntPop());
    MergesortPrivate(pArr, QsortStackIntPop(), QsortStackIntPop(), QsortStackIntPop());
}

void Mergesort(int *pArr, int length)
{
    MergesortPartion(pArr, 0, --length);
}

void NOXLibraryEntryPointFunction()
{
    "export QsortStackInt";
    "export QsortStackIntPush";
    "export QsortStackIntPop";
    "export QuicksortSwapPrivate";
    "export QuicksortPrivate";
    "export Quicksort";
    "export MergesortPrivate";
    "export MergesortPartion";
    "export Mergesort";
}
