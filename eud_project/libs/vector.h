
#include "define.h"
#include "memutil.h"

#define VECTOR_SIZE 0
#define VECTOR_VOLUME 1
#define VECTOR_PTR 2
#define VECTOR_MAX 3

int CreateVector(int size)
{
    int *p;

    if (size<0) size=0;

    AllocSmartMemEx(VECTOR_MAX*4,&p);
    size+=(4-(size%4));
    p[VECTOR_SIZE]=size;
    p[VECTOR_VOLUME]=0;
    char *ptr=0;

    AllocSmartMemEx(size,&ptr);
    p[VECTOR_PTR]=ptr;
    return p;
}

void roundUpVector(int *vector, int needsize)
{
    if (vector[VECTOR_VOLUME]+needsize>=vector[VECTOR_SIZE])
    {
        int *v;

        // needsize=(vector[VECTOR_SIZE]*2);
        needsize+=(vector[VECTOR_SIZE]*2);
        needsize+=(4-(needsize%4));
        AllocSmartMemEx(needsize,&v);
        // NoxDwordMemCopy(vector[VECTOR_PTR],v,vector[VECTOR_SIZE]>>2);
        DwordCopy(vector[VECTOR_PTR],v,vector[VECTOR_SIZE]>>2);
        FreeSmartMemEx(vector[VECTOR_PTR]);
        vector[VECTOR_SIZE]=needsize;
        vector[VECTOR_PTR]=v;
    }
}

void SetVector(int *vector, char c)
{
    roundUpVector(vector,1);
    char *ptr=vector[VECTOR_PTR];
    ptr[vector[VECTOR_VOLUME]++]=c;
}

void SetVectorWord(int *vec,short word)
{
    roundUpVector(vec,2);
    char *ptr=vec[VECTOR_PTR];
    short *p=&ptr[vec[VECTOR_VOLUME]];

    p[0]=word;
    vec[VECTOR_VOLUME]+=2;
}

void SetVectorDword(int *vec, int dw){
    roundUpVector(vec,4);
    char *ptr=vec[VECTOR_PTR];
    int *p=&ptr[vec[VECTOR_VOLUME]];

    p[0]=dw;
    vec[VECTOR_VOLUME]+=4;
}

void SetVectorBytes(int *vector, char *bytes, int length)
{
    roundUpVector(vector, length);
    char *ptr=vector[VECTOR_PTR];
    NoxByteMemCopy(bytes,&ptr[vector[VECTOR_VOLUME]],length);
    vector[VECTOR_VOLUME]+=length;
}


void ResetVector(int *vec)
{
    vec[VECTOR_VOLUME]=0;
}
