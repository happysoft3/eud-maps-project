
#include "spriteDb.h"

#define FRAME_MAX_COUNT 1600

#define ANIM_PARAM_FRAMES 0
#define ANIM_PARAM_SIZES 1
#define ANIM_PARAM_THINGID 2
#define ANIM_PARAM_IMG_VECTOR 3
#define ANIM_PARAM_ANIM 4
#define ANIM_PARAM_MAX 5
void animFrameLoaderStorage(int **p)
{
    int params[ANIM_PARAM_MAX];
    p[0]=params;
}

int AnimFrameLoaderPutParams(int *frames, int *sizes,int thingId,int imgVector)
{
    int *p;
    animFrameLoaderStorage(&p);
    p[ANIM_PARAM_FRAMES]=frames;
    p[ANIM_PARAM_SIZES]=sizes;
    p[ANIM_PARAM_THINGID]=thingId;
    p[ANIM_PARAM_IMG_VECTOR]=imgVector;
}

void AnimFrameMake8Directions(int frameCount)
{
    int *params;
    animFrameLoaderStorage(&params);
    int *frames =params[ANIM_PARAM_FRAMES], *sizes=params[ANIM_PARAM_SIZES];
    // int *anim;

    // AllocSmartMemEx(((frameCount/5)*32)+32, &anim);
    int anim[FRAME_MAX_COUNT];
    int r=0,ar=0;

    while (r+5<=frameCount)
    {
        DwordCopy(&frames[r],&anim[ar],5);
        anim[ar+5]=LRInversionGRP(frames[r+3],sizes[r+3]);
        anim[ar+6]=LRInversionGRP(frames[r+2],sizes[r+2]);
        anim[ar+7]=LRInversionGRP(frames[r+1],sizes[r+1]);
        ar+=8;
        r+=5;
    }    
    while (r<frameCount) //남은 프레임을 처리합니다
        anim[ar++]=frames[r++];
    params[ANIM_PARAM_ANIM]=anim;
}

void AnimFrameMake4Directions(int frameCount)
{
    int *params;
    animFrameLoaderStorage(&params);
    int *frames =params[ANIM_PARAM_FRAMES], *sizes=params[ANIM_PARAM_SIZES];
    // int *anim;

    // AllocSmartMemEx(((frameCount/3)*16)+16, &anim);
    int anim[FRAME_MAX_COUNT];
    int r=0,ar=0;
    while (r+3<=frameCount)
    {
        DwordCopy(&frames[r],&anim[ar],3);
        anim[ar+3]=LRInversionGRP(frames[r+1],sizes[r+1]);
        ar+=4;
        r+=3;
    }
    while (r<frameCount)
        anim[ar++]=frames[r++];
    params[ANIM_PARAM_ANIM]=anim;
}

//frameId- 반드시 5배수
void AnimFrameAssign8Directions(int frameId, int startIndex, int actionTy, int n)
{
    int *params;

    animFrameLoaderStorage(&params);
    int *anim =params[ANIM_PARAM_ANIM];
    if (frameId!=0)
        anim=&anim[(frameId/5)*8];
    int imgVec=params[ANIM_PARAM_IMG_VECTOR],thingId=params[ANIM_PARAM_THINGID];
    int *imgTables = getMonsterSpriteImageTablePtrBase(thingId,actionTy);
    int imagePtr;

    n<<=2;
    ImageUtilGetPtrFromID(startIndex, &imagePtr);
    AppendImageFrame(imgVec, anim[0], startIndex++);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    AppendImageFrame(imgVec, anim[1], startIndex++);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    AppendImageFrame(imgVec, anim[2], startIndex++);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+24);
    AppendImageFrame(imgVec, anim[3], startIndex++);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+36);
    AppendImageFrame(imgVec, anim[4], startIndex++);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+48);
    AppendImageFrame(imgVec, anim[5], startIndex++);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+60);
    AppendImageFrame(imgVec, anim[6], startIndex++);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+72);
    AppendImageFrame(imgVec, anim[7], startIndex);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr+84);
}

//frameId- 반드시 5배수
void AnimFrameAssignAsSingleImage(int imageId, int frame, int actionTy, int n)
{
    int *params;

    animFrameLoaderStorage(&params);
    int *imgTables = getMonsterSpriteImageTablePtrBase(params[ANIM_PARAM_THINGID],actionTy);
    int imagePtr;

    n<<=2;
    ImageUtilGetPtrFromID(imageId, &imagePtr);
    AppendImageFrame(params[ANIM_PARAM_IMG_VECTOR], frame, imageId);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
}

void AnimFrameCopy8Directions(int startIndex, int actionTy, int n)
{
    int *params;

    animFrameLoaderStorage(&params);
    int thingId=params[ANIM_PARAM_THINGID];
int *imgTables = getMonsterSpriteImageTablePtrBase(thingId,actionTy);
    int imagePtr;

    n<<=2;
    ImageUtilGetPtrFromID(startIndex, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+24);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+36);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+48);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+60);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+72);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr+84);
}

void AnimFrameCopyAsSingleImage(int imageId, int actionTy, int n){
    int *params;

    animFrameLoaderStorage(&params);
    int *imgTables = getMonsterSpriteImageTablePtrBase(params[ANIM_PARAM_THINGID],actionTy);
    int imagePtr;

    n<<=2;
    ImageUtilGetPtrFromID(imageId, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
}

void AnimFrameAssign4Directions(int frameId, int startIndex, int actionTy, int n)
{
int *params;

animFrameLoaderStorage(&params);
int *anim =params[ANIM_PARAM_ANIM];

if (frameId!=0)
    anim=&anim[(frameId/3)*4];
int imgVec=params[ANIM_PARAM_IMG_VECTOR], thingId=params[ANIM_PARAM_THINGID];
int *imgTables = getMonsterSpriteImageTablePtrBase(thingId,actionTy);
int imagePtr;

n<<=2;
AppendImageFrame(imgVec, anim[0],startIndex);
AppendImageFrame(imgVec, anim[1],startIndex+1);
AppendImageFrame(imgVec, anim[2],startIndex+2);
AppendImageFrame(imgVec, anim[3],startIndex+3);
ImageUtilGetPtrFromID(startIndex, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+36);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+36);
}

void AnimFrameCopy4Directions(int startIndex, int actionTy, int n)
{
    int *params;

n<<=2;
    animFrameLoaderStorage(&params);
    int thingId=params[ANIM_PARAM_THINGID],*imgTables = getMonsterSpriteImageTablePtrBase(thingId,actionTy),imagePtr;
ImageUtilGetPtrFromID(startIndex, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+36);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+36);
}
