
#include "define.h"
#include "playerinfo.h"
#include "queueTimer.h"
#include "winapi.h"
#include "netmemlib.h"
#include "recovery.h"

int ImportNetSendClient()
{
    int arr[15], link;

    if (!link)
    {
        arr[0] = 0x40EBC068; arr[1] = 0x72506800; arr[2] = 0x83500050; arr[3] = 0x54FF10EC; arr[4] = 0x44891424;
        arr[5] = 0x54FF0C24; arr[6] = 0x44891424; arr[7] = 0x54FF0824; arr[8] = 0x44891424; arr[9] = 0x54FF0424;
        arr[10] = 0x04891424; arr[11] = 0x2454FF24; arr[12] = 0x10C48318; arr[13] = 0x08C48358; arr[14] = 0x909090C3;
        link=arr;
    }
    return link;
}

void NetClientSendRaw(int pIndex, int boolDirection, int buffPtr, int buffSize)
{
    int temp = GetMemory(0x5c315c);

    SetMemory(0x5c315c, ImportNetSendClient());
    GroupDamage(pIndex, boolDirection, buffPtr, buffSize);
    SetMemory(0x5c315c, temp);
}

void NetClientSend(int plrUnit, int buffPtr, int buffSize)
{
    //netClientSend,0x0040EBC0
    int plrPtr = UnitToPtr(plrUnit), plrIdx;
    int temp = GetMemory(0x5c315c);

    if (plrPtr)
    {
        if (GetMemory(plrPtr + 0x08) & 0x04)
        {
            plrIdx = GetMemory(GetMemory(GetMemory(plrPtr + 0x2ec) + 0x114) + 0x810);
            //5c31ac
            SetMemory(0x5c315c, ImportNetSendClient());
            GroupDamage(plrIdx, 1, buffPtr, buffSize);
            SetMemory(0x5c315c, temp);
        }
    }
}

void ClientSetMemory(int user, int targetAddr, int byte)
{
    char packet[]={0xdf,0,0,0,0,byte,};

    SetMemory(&packet[1], targetAddr - 0x6d495c);
    NetClientSend(user, packet, sizeof(packet));
}

void NetPlayBgm(int user, int bgmId, int volume)
{
	if (MaxHealth(user))
	{
		char packet[]={0xe5,bgmId,volume,};
		
		NetClientSend(user, packet, 3);
	}
}
void NetPlayCustomBgm(int user){
    NetPlayBgm(user,0x66,100);
}

int ValidPlayerCheck(int plrUnit)
{
    int plrArr[32], pIndex = GetPlayerIndex(plrUnit);

    if (pIndex >= 0)
    {
        if (plrUnit ^ plrArr[pIndex])
        {
            plrArr[pIndex] = plrUnit;
            return TRUE;
        }
    }
    return FALSE;
}

//this function can be override
void NetworkUtilClientMain()
{ }

// int NetworkUtilClientTimerEnablerOffset()
// {
// 	return 0x750fc0;
// }

// int NetworkUtilClientTimerEnabler()
// {
// 	return FALSE;
// }

// int NetworkUtilClientTimerEnablerExec()
// {
// 	int offset = NetworkUtilClientTimerEnablerOffset();
// 	int uniqcmp = 0x753af0;
	
// 	SetMemory(uniqcmp, GetMemory(0x5D5350));
// 	SetMemory(offset, 0x500D8B51);
// 	SetMemory(offset + 4, 0xB8005D53);
// 	SetMemory(offset + 8, uniqcmp);
// 	SetMemory(offset + 12, 0x07750839);
// 	SetMemory(offset + 16, 0xDC9DDBE8);
// 	SetMemory(offset + 20, 0x8907EBFF);
// 	SetMemory(offset + 24, 0x9C42E808);
// 	SetMemory(offset + 28, 0xB859FFDC);
// 	SetMemory(offset + 32, 0x00000001);
// 	SetMemory(offset + 36, 0x909090C3);
	
// 	SetMemory((offset + 0x10) + 1, 0x51ADF0 - (offset + 0x10) - 5);
// 	SetMemory((offset + 0x19) + 1, 0x51ac60 - (offset + 0x19) - 5);
	
// 	return offset;
// }

void NetworkUtilClientTimerEnablerExec(int loopFn){
    char *p;

    AllocSmartMemEx(32, &p);
    char src[]={
        // 0xB8, 0xF0, 0xAD, 0x51, 0x00, 0xFF, 0xD0, 0xB8, 0x01, 0x00, 0x00, 0x00, 0xC3, 0x90
        0x6A, 0x00, 0x6A, 0x00, 0x68, 0xAC, 0xAC, 0xAC, 0xAC, 0xB8, 0x10, 0x73, 0x50, 0x00, 
        0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0xC3, 0x90
        };

    NoxByteMemCopy(src,p,sizeof(src));
    int *t=&p[5];
    t[0]=loopFn;
    SetRecoveryDataNormal (0x69ba98, p);
}

void MapEntry(){
    if (ToInt(GetWaypointX(1)) )
        return;

    EnableMemoryIONet();
    UOperatorInitNon4ByteHandler();
    SetMemory(0x69ba98, 0x43de10);
    // if (NetworkUtilClientTimerEnabler())
    // {
    //     SetRecoveryDataNormal (0x69ba98, NetworkUtilClientTimerEnablerExec());
    // }
    // else
    NetworkUtilClientMain();
    char patch[]={0x5E, 0xB8, 0x01, 0x00, 0x00, 0x00, 0xC3, 0x90};
    ModifyCodeGuardedX(0x4fc660,patch,sizeof(patch));
}

#define NETWORK_UTIL_PINDEX_OFF 0x58d7f2

void deferredSendClientXXX(int user){
    int off=0x4Fc612;
    char *p=&off;

    ClientSetMemory(user, 0x69ba98 + 0, p[0]);
    ClientSetMemory(user, 0x69ba98 + 1, p[1]);
    ClientSetMemory(user, 0x69ba98 + 2, p[2]);
    ClientSetMemory(user, 0x58d7f2, GetPlayerIndex(user));
}

void NetworkUtilClientEntry(int user){
    if (!MaxHealth(user))
        return;

    if (!ValidPlayerCheck(user))
        return;

    PushTimerQueue(1,user,deferredSendClientXXX);
}

void OnPlayerEntryMap(int pInfo)
{
    if (pInfo!=0x653a7c)
    {
        int *ptr=GetMemory(pInfo+0x808);

        if (ptr)
            NetworkUtilClientEntry(GetMemory(ptr+0x2c));
    }
}

void InitialPatchPlayerEntry()
{
    char *p = MemAlloc(64), code[]={
        0x8B, 0x44, 0x24, 0x04, 0x50, 0xE8, 0x26, 0x62, 0xDB, 0xFF, 0x58, 0x6A, 0x00, 
        0x6A, 0x00, 0x68, 0xAC, 0xAC, 0xAC, 0xAC, 0xE8, 0x17, 0x62, 0xDB, 0xFF, 0x83, 
        0xC4, 0x0C, 0x68, 0x80, 0x00, 0x00, 0x00, 0xE8, 0x99, 0x95, 0xCB, 0xFF, 0x83, 0xC4, 0x04, 0xC3};
    char *pcode=&p[4];

    SetMemory(p, &pcode[0x1c]);
    NoxByteMemCopy(code,pcode,sizeof(code));
    FixCallOpcode(&pcode[5],0x507230);
    FixCallOpcode(&pcode[0x14],0x507310);
    FixCallOpcode(&pcode[0x21],0x40A5C0);
    SetMemory(&pcode[0x10],OnPlayerEntryMap);
    char hook[]={0x57, 0xFF, 0x15, 0x00, 0x10, 0x75, 0x00, 0x5F, 0x90, 0x90, 0x90, 0x90, 0x90};
    int *fix=&hook[3];
    fix[0]=p;
    ModifyCodeGuardedX(0x4dd1b3, hook, sizeof(hook));
    SetRecoveryDataType2(p, pcode);
}

void NOXLibraryEntryPointFunction()
{
    "export needinit InitialPatchPlayerEntry";
}
