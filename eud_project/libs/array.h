

//Array 값의 주소를 얻습니다
//@param. pArr - 배열의 주소입니다
//@param. index - 배열의 번지입니다
//@returns. index 번지의 pArr 주소를 돌려줍니다
int ArrayRefN(int *pArr, int index)
{
	return pArr + (index<<2);
}

//Array 값 주소로 부터 adder 만큼 더한 값을 돌려줍니다
int ArrayRefAdd(int *pArr, int adder)
{
	return pArr+adder;
}

void NOXLibraryEntryPointFunction()
{
	"export ArrayRefN";
	"export ArrayRefAdd";
}


