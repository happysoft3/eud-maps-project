
#include "typecast.h"
#include "unitutil.h"
#include "opcodehelper.h"

int g_GetRawCodeCreateObjectSpec;


#define UNIT_FLAG_BELOW  0x1
#define UNIT_FLAG_NO_UPDATE  0x2
#define UNIT_FLAG_ACTIVE  0x4
#define UNIT_FLAG_ALLOW_OVERLAP  0x8
#define UNIT_FLAG_SHORT  0x10
#define UNIT_FLAG_DESTROYED  0x20
#define UNIT_FLAG_NO_COLLIDE  0x40
#define UNIT_FLAG_MISSILE_HIT  0x80
#define UNIT_FLAG_EQUIPPED  0x100
#define UNIT_FLAG_PARTITIONED  0x200
#define UNIT_FLAG_NO_COLLIDE_OWNER  0x400
#define UNIT_FLAG_OWNER_VISIBLE  0x800
#define UNIT_FLAG_EDIT_VISIBLE  0x1000
#define UNIT_FLAG_NO_PUSH_CHARACTERS  0x2000
#define UNIT_FLAG_AIRBORNE  0x4000
#define UNIT_FLAG_DEAD  0x8000
#define UNIT_FLAG_SHADOW  0x10000
#define UNIT_FLAG_FALLING  0x20000
#define UNIT_FLAG_IN_HOLE  0x40000
#define UNIT_FLAG_RESPAWN  0x80000
#define UNIT_FLAG_ON_OBJECT  0x100000
#define UNIT_FLAG_SIGHT_DESTROY  0x200000
#define UNIT_FLAG_TRANSIENT  0x400000
#define UNIT_FLAG_BOUNCY  0x800000
#define UNIT_FLAG_ENABLED  0x1000000
#define UNIT_FLAG_PENDING  0x2000000
#define UNIT_FLAG_TRANSLUCENT  0x4000000
#define UNIT_FLAG_STILL  0x8000000
#define UNIT_FLAG_NO_AUTO_DROP  0x10000000
#define UNIT_FLAG_FLICKER  0x20000000
#define UNIT_FLAG_SELECTED  0x40000000
#define UNIT_FLAG_MARKED  0x80000000


#define DAMAGE_TYPE_BLADE  0
#define DAMAGE_TYPE_FLAME  1
#define DAMAGE_TYPE_CRUSH  2
#define DAMAGE_TYPE_IMPALE  3
#define DAMAGE_TYPE_DRAIN  4
#define DAMAGE_TYPE_POISON  5
#define DAMAGE_TYPE_DISPEL_UNDEAD  6
#define DAMAGE_TYPE_EXPLOSION  7
#define DAMAGE_TYPE_BITE  8
#define DAMAGE_TYPE_ELECTRIC  9
#define DAMAGE_TYPE_CLAW  10
#define DAMAGE_TYPE_IMPACT  11
#define DAMAGE_TYPE_LAVA  12
#define DAMAGE_TYPE_DEATH_MAGIC  13
#define DAMAGE_TYPE_PLASMA  14
#define DAMAGE_TYPE_MANA_BOMB  15
#define DAMAGE_TYPE_ZAP_RAY  16
#define DAMAGE_TYPE_AIRBORNE_ELECTRIC  17


#define MON_STATUS_DESTROY_WHEN_DEAD  1
#define MON_STATUS_CHECK  2
#define MON_STATUS_CAN_BLOCK  4
#define MON_STATUS_CAN_DODGE  8
#define MON_STATUS_UNUSED  0x10
#define MON_STATUS_CAN_CAST_SPELLS  0x20
#define MON_STATUS_HOLD_YOUR_GROUND  0x40
#define MON_STATUS_SUMMONED  0x80
#define MON_STATUS_ALERT  0x100
#define MON_STATUS_INJURED  0x200
#define MON_STATUS_CAN_SEE_FRIENDS  0x400
#define MON_STATUS_CAN_HEAL_SELF  0x800
#define MON_STATUS_CAN_HEAL_OTHERS  0x1000
#define MON_STATUS_CAN_RUN  0x2000
#define MON_STATUS_RUNNING  0x4000
#define MON_STATUS_ALWAYS_RUN  0x8000
#define MON_STATUS_NEVER_RUN  0x10000
#define MON_STATUS_BOT  0x20000
#define MON_STATUS_MORPHED  0x40000
#define MON_STATUS_STAY_DEAD  0x80000
#define MON_STATUS_ON_FIRE  0x100000
#define MON_STATUS_FRUSTRATED  0x200000


#define UNIT_CLASS_MISSILE  0x1
#define UNIT_CLASS_MONSTER  0x2
#define UNIT_CLASS_PLAYER  0x4
#define UNIT_CLASS_OBSTACLE  0x8
#define UNIT_CLASS_FOOD  0x10
#define UNIT_CLASS_EXIT  0x20
#define UNIT_CLASS_KEY  0x40
#define UNIT_CLASS_DOOR  0x80
#define UNIT_CLASS_INFO_BOOK  0x100
#define UNIT_CLASS_TRIGGER  0x200
#define UNIT_CLASS_TRANSPORTER  0x400
#define UNIT_CLASS_HOLE  0x800
#define UNIT_CLASS_WAND  0x1000
#define UNIT_CLASS_FIRE  0x2000
#define UNIT_CLASS_ELEVATOR  0x4000
#define UNIT_CLASS_ELEVATOR_SHAFT  0x8000
#define UNIT_CLASS_DANGEROUS  0x10000
#define UNIT_CLASS_MONSTERGENERATOR  0x20000
#define UNIT_CLASS_READABLE  0x40000
#define UNIT_CLASS_LIGHT  0x80000
#define UNIT_CLASS_SIMPLE  0x100000
#define UNIT_CLASS_COMPLEX  0x200000
#define UNIT_CLASS_IMMOBILE  0x400000
#define UNIT_CLASS_VISIBLE_ENABLE  0x800000
#define UNIT_CLASS_WEAPON  0x1000000
#define UNIT_CLASS_ARMOR  0x2000000
#define UNIT_CLASS_NOT_STACKABLE  0x4000000
#define UNIT_CLASS_TREASURE  0x8000000
#define UNIT_CLASS_FLAG  0x10000000
#define UNIT_CLASS_CLIENT_PERSIST  0x20000000
#define UNIT_CLASS_CLIENT_PREDICT  0x40000000
#define UNIT_CLASS_PICKUP  0x80000000


float GetMemoryFloat(int a)
{
	StopScript(GetMemory(a));
}

void SetUnitFlags(int unit, int flag)
{
	int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(ptr + 0x10, flag);
}

int GetUnitFlags(int unit)
{
	int ptr = UnitToPtr(unit);

    if (ptr)
        return GetMemory(ptr + 0x10);
    return 0;
}

void UnitNoCollide(int unit)
{
    SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x40);
}

int GetUnitSubclass(int unit)
{
	int ptr = UnitToPtr(unit);
	
	if (ptr)
		return GetMemory(ptr + 0x0c);
	return 0;
}

void SetUnitSubclass(int unit, int subclassFlag)
{
	int ptr = UnitToPtr(unit);
	
	if (ptr)
		SetMemory(ptr + 0x0c, subclassFlag);
}

int GetUnitClass(int unit)
{
	int ptr = UnitToPtr(unit);
	
	if (ptr)
		return GetMemory(ptr + 0x08);
	return 0;
}

int GetUnitThingID(int unit)
{
    int ptr = UnitToPtr(unit);
    if (ptr)
        return GetMemory(ptr + 0x04);
    return 0;
}

void SetUnitSpeed(int unit, float amount)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(ptr + 0x224, ToInt(amount));
}

void SetUnitMass(int unit, float ms)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(ptr + 0x78, ToInt(ms));
}

int GetUnitStatus(int unit)
{
    int temp, ptr = UnitToPtr(unit);

    if (ptr)
    {
        temp = GetMemory(ptr + 0x2ec);
        if (temp)
            return GetMemory(temp + 0x5a0);
    }
    return 0;
}

void SetUnitStatus(int unit, int stat)
{
    int temp, ptr = UnitToPtr(unit);

    if (ptr)
    {
        temp = GetMemory(ptr + 0x2ec);
        if (temp)
            SetMemory(temp + 0x5a0, stat);
    }
}

void SetUnitMaxHealth(int unit, int amount)
{
    int ptr = UnitToPtr(unit);
    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x22c), amount);
        SetMemory(GetMemory(ptr + 0x22c) + 0x4, amount);
    }
}

void UnitZeroFleeRange(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
}

void SetUnitVoice(int unit, int voiceIndex)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(voiceIndex));
}

void UnitLinkBinScript(int unit, int binScrAddr)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, binScrAddr);
}

int VoiceList(int num)
{
    int list[75], addr, k;

    if (!list[0])
    {
        addr = GetMemory(0x663eec);
        for (k = 0 ; k < 75 ; k ++)
        {
            list[k] = addr;
            addr = GetMemory(addr + 0x4c);
        }
    }
    return list[num];
}

void PreloadVoiceList()
{
	VoiceList(0);
}

int GetUnit1C(int sUnit)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
        return GetMemory(ptr + 0x1c);
    return 0;
}

void SetUnit1C(int sUnit, int sData)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
        SetMemory(ptr + 0x1c, sData);
}

int IsMissileUnit(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        return GetMemory(ptr + 8) & 1;
    return false;
}

int IsPlayerUnit(int checkUnit)
{
    int ptr = UnitToPtr(checkUnit);

    if (ptr)
        return GetMemory(ptr + 0x08) & 4;
    return false;
}

int IsMonsterUnit(int checkUnit)
{
    int ptr = UnitToPtr(checkUnit);

    if (ptr)
        return GetMemory(ptr + 0x08) & 2;
    return false;
}

float DistanceUnitToUnit(int unit1, int unit2)
{
    int ptr1 = UnitToPtr(unit1);
    int ptr2 = UnitToPtr(unit2);

    if (ptr1 && ptr2)
        return Distance(GetMemoryFloat(ptr1 + 0x38), GetMemoryFloat(ptr1 + 0x3c), GetMemoryFloat(ptr2 + 0x38), GetMemoryFloat(ptr2 + 0x3c));
    return 0.0;
}

int GetOwner(int unit)
{
    int ptr = UnitToPtr(unit), res;

    if (ptr)
    {
        res = GetMemory(ptr + 0x1fc);
        if (res)
            return GetMemory(res + 0x2c);
    }
    return 0;
}

int IsPoisonedUnit(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
        return GetMemory(ptr + 0x21c) & 0xff;
    return 0;
}

void SetUnitScanRange(int unit, float range)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
		if (GetMemory(ptr + 0x08) & 2)
			SetMemory(GetMemory(ptr + 0x2ec) + 0x520, ToInt(range));
    }
}

int IsMonsterPoisonImmune(int unit)
{
	int ptr = UnitToPtr(unit);
	
	if (IsMonsterUnit(unit))
	{
		return GetMemory(ptr + 0x0c) & 0x200;
	}
	return false;
}

void SetMonsterPoisonImmune(int unit)
{
    int ptr = UnitToPtr(unit);

    if (IsMonsterUnit(unit))
        SetMemory(ptr + 0x0c, GetMemory(ptr + 0x0c) ^ 0x200);
}

int IsMonsterFireImmune(int unit)
{
	int ptr = UnitToPtr(unit);
	
	if (IsMonsterUnit(unit))
	{
		return GetMemory(ptr + 0x0c) & 0x400;
	}
	return false;
}

void SetMonsterFireImmune(int unit)
{
    int ptr = UnitToPtr(unit);

    if (IsMonsterUnit(unit))
        SetMemory(ptr + 0x0c, GetMemory(ptr + 0x0c) ^ 0x400);
}

int IsMonsterElectricityImmune(int unit)
{
	int ptr = UnitToPtr(unit);
	
	if (IsMonsterUnit(unit))
	{
		return GetMemory(ptr + 0x0c) & 0x800;
	}
	return false;
}

void SetMonsterElectricityImmune(int unit)
{
    int ptr = UnitToPtr(unit);

    if (IsMonsterUnit(unit))
        SetMemory(ptr + 0x0c, GetMemory(ptr + 0x0c) ^ 0x800);
}

int UnitStructGetUnitTeamId(int unit)
{
	int ptr = UnitToPtr(unit);
	
	if (ptr)
	{
		return GetMemory(ptr + 0x34);
	}
	return 0;
}

void UnitStructSetUnitTeamId(int unit, int teamId)
{
	int ptr = UnitToPtr(unit);
	
	if (ptr)
	{
		SetMemory(ptr + 0x34, teamId & 0x1f);
	}
}

//금화의 수량을 얻습니다
//오류 시 -1을 얻습니다
int UnitStructGetGoldAmount(int goldUnit)
{
	int ptr = UnitToPtr(goldUnit);
	
	if (ptr)
	{
		if (GetMemory(ptr + 0x2c4) == 5192288)
		{
			if (GetMemory(ptr + 0x2b4))
				return GetMemory(GetMemory(ptr + 0x2b4));
		}
	}
	return -1;
}

void UnitStructSetGoldAmount(int goldUnit, int setTo)
{
	int ptr = UnitToPtr(goldUnit);
	
	if (ptr)
	{
		if (GetMemory(ptr + 0x2c4) == 5192288)
		{
			int amountPtr = GetMemory(ptr + 0x2b4);
			
			if (amountPtr)
				SetMemory(amountPtr, setTo); 
		}
	}
}

int GetRawCodeCreateXtraObjectSpec()
{
	int codeArr[6], link;
	
	if (!link)
	{
		codeArr[0] = 0xDB624BE8; codeArr[1] = 0x45E850FF; codeArr[2] = 0x50FFD924;
		codeArr[3] = 0xDB621FE8; codeArr[4] = 0x315858FF; codeArr[5] = 0x9090C3C0;
		
		link = GetScrDataField(-g_GetRawCodeCreateObjectSpec);
		
		FixCallOpcode(link, 0x507250);
        FixCallOpcode(link+6, 0x4e3450);
        FixCallOpcode(link+0x0c, 0x507230);
	}
	return link;
}

void AllocateXtraObjectSpec(int thingId, int *pDestSpec)
{
    int pOld = GetMemory(0x5c336c);

    SetMemory(0x5c336c, GetRawCodeCreateXtraObjectSpec());
    int allocSpec = Unknownb8(thingId);
    SetMemory(0x5c336c, pOld);
    if (pDestSpec)
        SetMemory(pDestSpec, allocSpec);
}

void NOXLibraryEntryPointFunction()
{
	"export GetMemoryFloat";
	"export SetUnitFlags";
	"export GetUnitFlags";
	"export GetUnitSubclass";
	"export SetUnitSubclass";
	"export GetUnitClass";
	"export UnitNoCollide";
	"export GetUnitThingID";
	"export SetUnitSpeed";
	"export SetUnitMass";
	"export GetUnitStatus";
	"export SetUnitStatus";
	"export SetUnitMaxHealth";
	"export UnitZeroFleeRange";
	"export VoiceList";
	"export SetUnitVoice";
	"export UnitLinkBinScript";
	"export needinit PreloadVoiceList";
	"export GetUnit1C";
	"export SetUnit1C";
	"export IsMissileUnit";
	"export IsPlayerUnit";
	"export IsMonsterUnit";
	"export IsPlayerUnit";
	"export DistanceUnitToUnit";
	"export GetOwner";
	"export IsPoisonedUnit";
	"export SetUnitScanRange";
	"export IsMonsterPoisonImmune";
	"export SetMonsterPoisonImmune";
	"export IsMonsterFireImmune";
	"export SetMonsterFireImmune";
	"export IsMonsterElectricityImmune";
	"export SetMonsterElectricityImmune";
	"export UnitStructGetUnitTeamId";
	"export UnitStructSetUnitTeamId";
	"export UnitStructGetGoldAmount";
	"export UnitStructSetGoldAmount";
	"export GetRawCodeCreateXtraObjectSpec";
	"export AllocateXtraObjectSpec";
	
	g_GetRawCodeCreateObjectSpec=GetRawCodeCreateXtraObjectSpec;
}




