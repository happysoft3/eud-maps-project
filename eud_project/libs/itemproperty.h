

#include "unitutil.h"
#include "callmethod.h"
#include "array.h"
#include "memutil.h"

#define		ITEM_PROPERTY_stun1	0x5BA1BC 
#define		ITEM_PROPERTY_stun2	0x5BA1D4 
#define		ITEM_PROPERTY_stun3	0x5BA1EC 
#define		ITEM_PROPERTY_stun4	0x5BA204 

#define		ITEM_PROPERTY_fire1	0x5BA21C 
#define		ITEM_PROPERTY_fire2	0x5BA234 
#define		ITEM_PROPERTY_fire3	0x5BA24C 
#define		ITEM_PROPERTY_fire4	0x5BA264 

#define		ITEM_PROPERTY_impact1	0x5BA27C 
#define		ITEM_PROPERTY_impact2	0x5BA294 
#define		ITEM_PROPERTY_impact3	0x5BA2AC 
#define		ITEM_PROPERTY_impact4	0x5BA2C4 

#define		ITEM_PROPERTY_confuse1	0x5BA2DC 
#define		ITEM_PROPERTY_confuse2	0x5BA2F4 
#define		ITEM_PROPERTY_confuse3	0x5BA30C 
#define		ITEM_PROPERTY_confuse4	0x5BA324 

#define		ITEM_PROPERTY_lightning1	0x5BA33C 
#define		ITEM_PROPERTY_lightning2	0x5BA354 
#define		ITEM_PROPERTY_lightning3	0x5BA36C 
#define		ITEM_PROPERTY_lightning4	0x5BA384 

#define		ITEM_PROPERTY_vampirism1	0x5BA39C 
#define		ITEM_PROPERTY_vampirism2	0x5BA3B4 
#define		ITEM_PROPERTY_vampirism3	0x5BA3CC 
#define		ITEM_PROPERTY_vampirism4	0x5BA3E4 

#define		ITEM_PROPERTY_venom1	0x5BA3FC 
#define		ITEM_PROPERTY_venom2	0x5BA414 
#define		ITEM_PROPERTY_venom3	0x5BA42C 
#define		ITEM_PROPERTY_venom4	0x5BA444 

#define		ITEM_PROPERTY_readiness1	0x5BA63C 
#define		ITEM_PROPERTY_readiness2	0x5BA654 
#define		ITEM_PROPERTY_readiness3	0x5BA66C 
#define		ITEM_PROPERTY_readiness4	0x5BA684 

#define		ITEM_PROPERTY_projectileSpeed1	0x5BA69C 
#define		ITEM_PROPERTY_projectileSpeed2	0x5BA6B4 
#define		ITEM_PROPERTY_projectileSpeed3	0x5BA6CC 
#define		ITEM_PROPERTY_projectileSpeed4	0x5BA6E4 

#define		ITEM_PROPERTY_weaponPower2	0x5BA714 
#define		ITEM_PROPERTY_weaponPower3	0x5BA72C 
#define		ITEM_PROPERTY_weaponPower4	0x5BA744 
#define		ITEM_PROPERTY_weaponPower5	0x5BA75C 
#define		ITEM_PROPERTY_weaponPower6	0x5BA774 

#define		ITEM_PROPERTY_armorQuality2	0x5BA7A4 
#define		ITEM_PROPERTY_armorQuality3	0x5BA7BC 
#define		ITEM_PROPERTY_armorQuality4	0x5BA7D4 
#define		ITEM_PROPERTY_armorQuality5	0x5BA7EC 
#define		ITEM_PROPERTY_armorQuality6	0x5BA804 

#define		ITEM_PROPERTY_Matrial3	0x5ba834 
#define		ITEM_PROPERTY_Matrial4	0x5ba84c 
#define		ITEM_PROPERTY_Matrial5	0x5ba864 
#define		ITEM_PROPERTY_Matrial6	0x5ba87c 
#define		ITEM_PROPERTY_Matrial7	0x5ba894 

#define		ITEM_PROPERTY_FireProtect1 	0x5ba45c
#define		ITEM_PROPERTY_FireProtect2	0x5BA474 
#define		ITEM_PROPERTY_FireProtect3	0x5BA48C 
#define		ITEM_PROPERTY_FireProtect4	0x5BA4A4 

#define		ITEM_PROPERTY_LightningProtect1	0x5BA4BC 
#define		ITEM_PROPERTY_LightningProtect2	0x5BA4D4 
#define		ITEM_PROPERTY_LightningProtect3	0x5BA4EC 
#define		ITEM_PROPERTY_LightningProtect4	0x5BA504 

#define		ITEM_PROPERTY_PoisonProtect1	0x5BA51C 
#define		ITEM_PROPERTY_PoisonProtect2	0x5BA534 
#define		ITEM_PROPERTY_PoisonProtect3	0x5BA54C 
#define		ITEM_PROPERTY_PoisonProtect4	0x5BA564 

#define		ITEM_PROPERTY_Regeneration1	0x5BA57C 
#define		ITEM_PROPERTY_Regeneration2	0x5BA594 
#define		ITEM_PROPERTY_Regeneration3	0x5BA5AC 
#define		ITEM_PROPERTY_Regeneration4	0x5BA5C4 

#define		ITEM_PROPERTY_Speed1	0x5BA5DC 
#define		ITEM_PROPERTY_Speed2	0x5BA5F4 
#define		ITEM_PROPERTY_Speed3	0x5BA60C 
#define		ITEM_PROPERTY_Speed4	0x5BA624 


int WeaponPower(int num)
{
	int *ppp;

    if (!ppp)
    {
		int properties[]= {0x5a00a4, 0x5BA714, 0x5BA72C, 0x5BA744, 0x5BA75C, 0x5BA774};
		ppp=&properties;
        return 0;
    }
	int *pic=ppp[num];
    return pic[0];
}

int ArmorQuality(int num)
{
    int *ppp;

    if (!ppp)
    {
		int zero=0;
		int quals[]={&zero, 0x5BA7A4, 0x5BA7BC, 0x5BA7D4, 0x5BA7EC, 0x5BA804};
		ppp=&quals;
        return 0;
    }
	int *pic=ppp[num];
    return pic[0];
}

int MaterialList(int num)
{
    int *ppp;

    if (!ppp)
    {
        //Lv.3 ~ 7, null
		int zero=0;
		int mats[]={
        	&zero, 0x5ba834, 0x5ba84c, 0x5ba864, 0x5ba87c, 0x5ba894};
			ppp=&mats;
        return 0;
    }
	int*pic=ppp[num];
    return pic[0];
}

int WeaponEffect(int num)
{
    int*ppp;

    if (!ppp)
    {
		int zero=0;
		int fxs[]={
        &zero,
        0x5BA1BC, 0x5BA1D4, 0x5BA1EC, 0x5BA204, 0x5BA21C, 0x5BA234, 0x5BA24C, 0x5BA264,
        0x5BA27C, 0x5BA294, 0x5BA2AC, 0x5BA2C4, 0x5BA2DC, 0x5BA2F4, 0x5BA30C, 0x5BA324,
        0x5BA33C, 0x5BA354, 0x5BA36C, 0x5BA384, 0x5BA39C, 0x5BA3B4, 0x5BA3CC, 0x5BA3E4,
        0x5BA3FC, 0x5BA414, 0x5BA42C, 0x5BA444,
        0x5BA63C, 0x5BA654, 0x5BA66C, 0x5BA684,
        0x5BA69C, 0x5BA6B4, 0x5BA6CC, 0x5BA6E4};
		ppp=&fxs;
        return 0;
    }
	int *pic=ppp[num];
    return pic[0];
}

int ArmorEffect(int num)
{
    int *ppp;

    if (!ppp)
    {
		int zero=0;
		int ams[]={
        	&zero,
        0x5BA45C, 0x5BA474, 0x5BA48C, 0x5BA4A4, 0x5BA4BC, 0x5BA4D4, 0x5BA4EC, 0x5BA504,
        0x5BA51C, 0x5BA534, 0x5BA54C, 0x5BA564, 0x5BA57C, 0x5BA594, 0x5BA5AC, 0x5BA5C4,
        0x5BA5DC, 0x5BA5F4, 0x5BA60C, 0x5BA624};
		ppp=&ams;
        return 0;
    }
	int *pic=ppp[num];
    return pic[0];
}

void InitInvPropertiesSet()
{
    WeaponEffect(0);
    ArmorEffect(0);
    WeaponPower(0);
    ArmorQuality(0);
    MaterialList(0);
}

//itemproperty 모듈 내부 사용 전용함수
void ItemPropertyItemUpdatePrivate(int *ptr)
{
	int rep=32;
	while(--rep>=0)
		ptr[140+rep]=0x200;
}

//무기속성을 설정합니다
//weapon: 무기유닛 id 입니다
//power: 무기 퀼리티 레밸입니다. 0~5
//mt_lv: 내구력 수치 입니다. 0~5
//wfx1: 첫번째 마법효과 번호입니다. 0~36
//wfx2: 두번째 마법효과 번호입니다. 0~36
void SetWeaponProperties(int weapon, char power, char mt_lv, char wfx1, char wfx2)
{
    int* ptr = UnitToPtr(weapon);

    if (!ptr)
		return;
	int *pSlot=ptr[173];

	pSlot[0]=WeaponPower(power);
	pSlot[1]=MaterialList(mt_lv);
	pSlot[2]=WeaponEffect(wfx1);
	pSlot[3]=WeaponEffect(wfx2);
	ItemPropertyItemUpdatePrivate(ptr);
}

// item_property_... 매크로를 사용하세요
void SetWeaponPropertiesDirect(int weapon, int *item_property1, int *item_property2, int *item_property3, int *item_property4)
{
	int *ptr = UnitToPtr(weapon);
	
	if (!ptr)
		return;
	int *pSlot=ptr[173];
	
	if (item_property1) pSlot[0]=item_property1[0];
	if (item_property2) pSlot[1]=item_property2[0];
	if (item_property3) pSlot[2]=item_property3[0];
	if (item_property4) pSlot[3]=item_property4[0];
	ItemPropertyItemUpdatePrivate(ptr);
}

//갑옷속성을 설정합니다
//armor: 갑옷유닛 id 입니다
//qual: 방어구 퀼리티 레밸입니다. 0~5
//mt_lv: 내구력 수치 입니다. 0~5
//afx1: 첫번째 갑옷 마법효과 번호입니다. 0~20
//afx2: 두번째 갑옷 마법효과 번호입니다. 0~20
void SetArmorProperties(int armor, int qual, int mt_lv, int afx1, int afx2)
{
    int *ptr = UnitToPtr(armor);

    if (!ptr)
		return;
	int *pSlot=ptr[173];
	
	pSlot[0]=ArmorQuality(qual);
	pSlot[1]=MaterialList(mt_lv);
	pSlot[2]=ArmorEffect(afx1);
	pSlot[3]=ArmorEffect(afx2);
	ItemPropertyItemUpdatePrivate(ptr);
}

// item_property_... 매크로를 사용하세요
void SetArmorPropertiesDirect(int armor, int *item_property1, int *item_property2, int *item_property3, int *item_property4)
{
	int *ptr = UnitToPtr(armor);
	
	if (!ptr)
		return;
	int *pSlot=ptr[173];

	if (item_property1) pSlot[0]=item_property1[0];
	if (item_property2) pSlot[1]=item_property2[0];
	if (item_property3) pSlot[2]=item_property3[0];
	if (item_property4) pSlot[3]=item_property4[0];
	ItemPropertyItemUpdatePrivate(ptr);
}

int ImportAllowAllDrop()
{
	int *ppp;

	if (!ppp)
	{
		int codeStream[]={
		0x550CEC83, 0x14246C8B, 0x24748B56, 0xECAE391C, 0x74000001, 0xC0315E08, 0x0CC4835D,
		0x0845F6C3, 0x68207404, 0x0053EBF0, 0x2454FF56, 0x08C48304, 0x0F74C085, 0x53EC8068,
		0x56016A00, 0x082454FF, 0x680CC483, 0x004ED301, 0x909090C3};
        ppp=&codeStream;
	}
	return ppp;
}

//버릴 수 없는 아이템을 버리기 가능하도록 설정 합니다
void SetItemPropertyAllowAllDrop(int item)
{
	int *ptr = UnitToPtr(item);
	
	if (ptr)
	{
		ptr[178]=ImportAllowAllDrop();
	}
}

//퀘스트 무기류 픽업 이벤트 제거용
void DisableOblivionItemPickupEvent(int oblvItem)
{
	int *ptr = UnitToPtr(oblvItem);
	
	if (ptr)
	{
		ptr[177]=0x53a720;
	}
}

//아이템의 속성 값을 얻습니다
//equippableItem- 아이템 유닛Id
//pDestTetraArr- 크기 4이상의 배열 주소
//Returns- 함수가 실패하면 FALSE 을 반환합니다
int GetEquippableItemProperties(int equippableItem, int *pDestTetraArr)
{
	int *ptr = UnitToPtr(equippableItem);
	
	if (!ptr)
		return FALSE;
	
	int *propertySlot = ptr[173];
	
	if (!propertySlot)
		return FALSE;
	
	pDestTetraArr[0]=ArrayRefN(propertySlot,0);
	pDestTetraArr[1]=ArrayRefN(propertySlot,1);
	pDestTetraArr[2]=ArrayRefN(propertySlot,2);
	pDestTetraArr[3]=ArrayRefN(propertySlot,3);
	return TRUE;
}

//아이템 srcItem 의 속성을 아이템 destItem 으로 복사합니다
//Returns- 함수가 실패하면 FALSE 을 반환합니다
int DoCopyItemProperties(int srcItem, int destItem)
{
	int *srcptr = UnitToPtr(srcItem), *destptr = UnitToPtr(destItem);
	
	if ((!srcptr) || (!destptr))
		return FALSE;
	int *srcProperty = srcptr[173];
	int *destProperty = destptr[173];
	
	if ((!srcProperty) || (!destProperty))
		return FALSE;
	
	NoxDwordMemCopy(srcProperty, destProperty, 4);
	
	ItemPropertyItemUpdatePrivate(destptr);
	return TRUE;
}

//아이템 마법효과 속성 섹션을 로드합니다
//pDestArr - 로드할 배열의 주소값 입니다
//pLength - 로드된 항목 사이즈를 받을 주소값 입니다
void LoadItemEffectPropertiesFromLinkedList(int *pDestArr, int *pLength)
{
	int *reader = 0x611c5c;
	int *lengthOff=0x611c60;
	pLength[0]=lengthOff[0];
	
	while (reader[0])
	{
		short *pId=reader[0]+4;
		pDestArr[pId[0]]=reader[0];
		reader=reader[0]+0x88;
	}
}

/*
*아이템 마법효과 제목을 변경합니다
*pProperty - 바꿀 속성의 PTR
*newName - 새로운 이름
*/
void ChangeItemEnchantmentExplanation(int *pProperty, string newName)
{
    if (pProperty[2])
        MemFree(pProperty[2]);

    char *src = StringUtilGetScriptStringPtr(newName);
    short length = StringUtilGetLength(src);
    short *newString = MemAlloc((++length) * 2);

    NoxUtf8ToUnicode(src, newString);
    pProperty[2] = newString;
}

void NOXLibraryEntryPointFunction()
{
	"export WeaponPower";
	"export ArmorQuality";
	"export MaterialList";
	"export WeaponEffect";
	"export ArmorEffect";
	"export needinit InitInvPropertiesSet";
	"export ItemPropertyItemUpdatePrivate";
	"export SetWeaponProperties";
	"export SetWeaponPropertiesDirect";
	"export SetArmorProperties";
	"export SetArmorPropertiesDirect";
	"export ImportAllowAllDrop";
	"export SetItemPropertyAllowAllDrop";
	"export DisableOblivionItemPickupEvent";
	"export GetEquippableItemProperties";
	"export DoCopyItemProperties";
	"export LoadItemEffectPropertiesFromLinkedList";
	"export ChangeItemEnchantmentExplanation";
}

