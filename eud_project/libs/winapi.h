
#include "define.h"
#include "opcodehelper.h"
#include "stringUtil.h"
#include "memutil.h"

int WinApiGetModuleHandleA(char *modName)
{
    char code[]={
        0x51, 0xE8, 0x4A, 0x62, 0xDB, 0xFF, 0xB9, 0x14, 0x12, 0x58, 0x00, 0x50, 0xFF, 0x11, 0x50, 0xE8, 0x1C, 0x62, 0xDB, 0xFF, 0x58, 0x59, 0xC3, 0x90
        };

    FixCallOpcode(code+1, 0x507250);
    FixCallOpcode(code+0xf, 0x507230);
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=code;
    int ret = Unused5e(modName);
    pExec[0x5e]=pOld;
    return ret;
}

int WinApiCloseHandle(int handle)
{
    char code[]=
    {
        0x51, 0xE8, 0x4A, 0x62, 0xDB, 0xFF, 0xB9, 0xF8, 0x10, 0x58, 0x00, 0x50, 0xFF, 0x11, 0x50, 0xE8, 0x1C, 0x62, 0xDB, 0xFF, 0x58, 0x59, 0x31, 0xC0, 0xC3, 0x90, 0x90, 0x90
    };
    int *p=code;
    FixCallOpcode(p+1, 0x507250);
    FixCallOpcode(p+0xf, 0x507230);
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=code;
    int ret = Unused5e(handle);
    pExec[0x5e]=pOld;
    return ret;
}

int WinApiGetProcAddress(int hmod, char *fn)
{
    char code[]=
    {
        0x51, 0xE8, 0x4A, 0x62, 0xDB, 0xFF, 0xB9, 0xD8, 0x10, 0x58, 0x00, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xFF, 0x11, 0x50, 0xE8, 0x18, 0x62, 0xDB, 0xFF, 0x58, 0x59, 0xC3, 0x90
    };
    FixCallOpcode(code+1, 0x507250);
    FixCallOpcode(code +0x13, 0x507230);
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=code;
    int ret = Unused5e(&hmod);
    pExec[0x5e]=pOld;
    return ret;
}

#define KERNEL32_SYMBOL 0x583e0c

int WinApiGetUserDefaultLangID()
{
    int hmod=WinApiGetModuleHandleA(KERNEL32_SYMBOL);
    if (!hmod)
        return 0;
    int proc=WinApiGetProcAddress(hmod, StringUtilGetScriptStringPtr("GetUserDefaultLangID"));
    if (!proc)
        return 0;
    char code[]=
    {
        0xFF, 0x15, 0xD8, 0x10, 0x58, 0x00, 0x50, 0xE8, 0x24, 0x62, 0xDB, 0xFF, 0x58, 0x31, 0xC0, 0xC3
    };
    int *p=code+2;
    p[0]=&proc;
    FixCallOpcode(code +7, 0x507230);
    int *pExec=0x5c308c;
    int *pOld=pExec[0xc1];
    pExec[0xc1]=code;
    int ret = IsTrading();
    pExec[0xc1]=pOld;
    return ret;
}

void WinApiVirtualProtect(int *lpAddress, int dwSize, int flNewProtect, int *lpflOldProtect)
{
    char *apicode;

    if (!apicode)
    {
        int hmod = WinApiGetModuleHandleA(KERNEL32_SYMBOL);

        if (!hmod)
            return;

        int proc=WinApiGetProcAddress(hmod, StringUtilGetScriptStringPtr("VirtualProtect"));
        if (!proc)
            return;
        char code[]={0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70,
        0x0C, 0xFF, 0x70, 0x08, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xB8, 0xAC,
        0xAC, 0xAC, 0x00, 0xFF, 0xD0, 0x50, 0xB8, 0x30, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3, 0x90, 0x90, 0x90};
        int *p=code+19;
        p[0]=proc;
        apicode=code;
    }

    if (apicode)
    {
        int *pExec=0x5c308c;
        int *pOld=pExec[0x20];
        pExec[0x20]=apicode;
        Unused20(&lpAddress);
        pExec[0x20]=pOld;
    }
}

void WinApiOutputDebugStringRaw(char *pMessage)
{
    char *apicode;

    if (!apicode)
    {
        int hmod=WinApiGetModuleHandleA(KERNEL32_SYMBOL);

        if (!hmod)
            return;
        int proc=WinApiGetProcAddress(hmod, StringUtilGetScriptStringPtr("OutputDebugStringA"));
        if (!proc)
            return;
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x50, 0xFF, 0x15, 0xD8, 0x10, 0x58, 0x00, 0x31, 0xC0, 0xC3, 0x90, 0x90, 0x90
        };
        int *p=code+10;
        p[0]=&proc;
        apicode=code;
    }

    if (apicode)
    {
        int *pExec=0x5c308c;
        int *pOld=pExec[0x20];
        pExec[0x20]=apicode;
        Unused20(pMessage);
        pExec[0x20]=pOld;
    }
}

void WinApiOutputDebugString(string message)
{
    WinApiOutputDebugStringRaw(StringUtilGetScriptStringPtr(message));
}

#define GENERIC_READ                     0x80000000
#define GENERIC_WRITE                    0x40000000
#define GENERIC_EXECUTE                  0x20000000
#define GENERIC_ALL                      0x10000000

#define CREATE_NEW 1
#define OPEN_EXISTING 3
#define OPEN_ALWAYS 4
#define TRUNCATE_EXISTING 5
#define CREATE_ALWAYS 2

int WinApiCreateFileA(char *filename, int desiredAccess, int creationDisposition, int flagsAndAttributes)
{
    char code[]={
    0x51, 0xB9, 0x54, 0x11, 0x58, 0x00, 0xE8, 0x45, 0x62, 0xDB, 0xFF, 0xFF, 0x70, 0x18, 0xFF, 0x70, 0x14, 0xFF, 
        0x70, 0x10, 0xFF, 0x70, 0x0C, 0xFF, 0x70, 0x08, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xFF, 0x11, 0x50, 0xE8, 0x09, 0x62, 0xDB, 0xFF, 0x58, 0x59, 0x31, 0xC0, 0xC3};
    FixCallOpcode(code+6, 0x507250);
    FixCallOpcode(code+0x22, 0x507230);

    int params[]={filename, desiredAccess, 0, 0, creationDisposition, flagsAndAttributes, 0};
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=code;
    int ret = Unused5e(params);
    pExec[0x5e]=pOld;
    return ret;
}

int WinApiReadFile(int fileHandle, char *pBuffer, int nNumberOfBytesToRead, int *lpNumberOfBytesRead)
{
    char code[]={
        0x51, 0xB9, 0x50, 0x11, 0x58, 0x00, 0xE8, 0x4A, 0x62, 0xDB, 0xFF, 0xFF, 0x70, 0x10, 0xFF, 0x70, 0x0C, 0xFF,
         0x70, 0x08, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xFF, 0x11, 0x50, 0xE8, 0x0F, 0x62, 0xDB, 0xFF, 0x58, 0x59, 0x31, 0xC0, 0xC3, 0x90, 0x90
    };
    FixCallOpcode(code+6,0x507250);
    FixCallOpcode(code+0x1c, 0x507230);
    int params[]={fileHandle, pBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, 0};
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=code;
    int ret = Unused5e(params);
    pExec[0x5e]=pOld;
    return ret;
}

int WinApiWriteFile(int handle, char *pBuffer, int nNumberOfBytesToWrite, int *lpNumberOfBytesWritten)
{
    char code[]={
        0x51, 0xB9, 0xA0, 0x10, 0x58, 0x00, 0xE8, 0x4A, 0x62, 0xDB, 0xFF, 0xFF, 0x70, 0x10, 0xFF, 0x70, 0x0C, 0xFF,
         0x70, 0x08, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xFF, 0x11, 0x50, 0xE8, 0x0F, 0x62, 0xDB, 0xFF, 0x58, 0x59, 0x31, 0xC0, 0xC3, 0x90, 0x90
    };
    int *p=code;
    FixCallOpcode(p+6,0x507250);
    FixCallOpcode(p+0x1c,0x507230);
    int params[]={handle, pBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, 0};
    int *pExec=0x5c308c;
    int *pOld=pExec[0x5e];
    pExec[0x5e]=code;
    int ret = Unused5e(params);
    pExec[0x5e]=pOld;
    return ret;
}

int WritePrivateProfileStringA(char *lpAppName, char *lpKeyName, char *lpString, char *lpFileName)
{
    char *apicode;
    int proc;

    if (!apicode)
    {
        int hmod = WinApiGetModuleHandleA(KERNEL32_SYMBOL);

        if (!hmod)
            return 0;

        proc=WinApiGetProcAddress(hmod, StringUtilGetScriptStringPtr("WritePrivateProfileStringA"));
        if (!proc)
            return 0;
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x0C, 0xFF, 0x70, 0x08, 0xFF, 0x70, 
            0x04, 0xFF, 0x30, 0x8B, 0x40, 0x10, 0xFF, 0xD0, 0x50, 0xB8, 0x30, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3, 0x90
        };
        apicode=code;
    }

    if (!apicode)
        return 0;
    int args[]={lpAppName,lpKeyName,lpString,lpFileName,proc,};
    return invokeRawCodeReturn(apicode, args);
}

int GetPrivateProfileStringA(char *lpAppName, char *lpKeyName, char *lpDefault, char *lpReturnedString, int nSize, char *lpFileName)
{
    char *apicode;
    int proc;

    if (!apicode)
    {
        int hmod = WinApiGetModuleHandleA(KERNEL32_SYMBOL);

        if (!hmod)
            return 0;

        proc=WinApiGetProcAddress(hmod, StringUtilGetScriptStringPtr("GetPrivateProfileStringA"));
        if (!proc)
            return 0;
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x14, 0xFF, 0x70, 0x10, 0xFF, 0x70, 0x0C, 
            0xFF, 0x70, 0x08, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0x8B, 0x40, 0x18, 0xFF, 0xD0, 0x50, 0xB8, 0x30, 
            0x72, 0x50, 0x00, 0xFF, 0xD0, 0x58, 0x31, 0xC0, 0xC3
        };
        apicode=code;
    }

    if (!apicode)
        return 0;
    int args[]={lpAppName,lpKeyName,lpDefault,lpReturnedString,nSize,lpFileName,proc,};
    return invokeRawCodeReturn(apicode, args);
}

void ModifyCodeGuardedX(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}
