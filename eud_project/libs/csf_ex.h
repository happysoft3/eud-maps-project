
#include "define.h"
#include "wildcard_string.h"
#include "array.h"

void CsfPrivateGetKeyPtr(short csfId, int *pDestPtr)
{
    int *csfKeybase = 0x611c00;

    pDestPtr[0] = csfKeybase[0]+(csfId*52);
}

void CsfPrivateGetVoiceString(int *keyPtr, char *destString)
{
    short *strIndex = keyPtr+50;
    int *voicePtbase = 0x611c08;
    int *voiceTable = voicePtbase[0];

    int *destPt =destString;
    destPt[0] =voiceTable[strIndex[0]];
}

short CsfWholeStringCount()
{
    short *csfWholeCount = 0x611bfc;

    return csfWholeCount[0];
}

//csf 번호로 해당하는 키 문자열을 가져옵니다
//아마도 디버그 용도//
//csfId- 키 문자열
//반환- 키 문자열 값
string CsfGetKeyStringFromId(short csfId)
{
    int *csfKeyBase = 0x611c00;

    return ReadStringAddressEx(csfKeyBase[0] + (csfId*52));
}

//음성파일 이름을 얻기//
string CsfGetVoiceFileNameFromId(short csfId)
{
    int *keypt;
    CsfPrivateGetKeyPtr(csfId,&keypt);
    char *voiceFilename;
    CsfPrivateGetVoiceString(keypt, &voiceFilename);
    return ReadStringAddressEx(voiceFilename);
}

//음성파일 이름을 설정하기//
//forced == true 일 시, 음성이 없는 문자열은, 강제로 추가됩니다//
int CsfChangeVoiceFileName(short csfId, string newName, char forced)
{
    int *keyPtr;
    CsfPrivateGetKeyPtr(csfId,&keyPtr);

    short *strIndex = keyPtr+50;
    int *voicePtbase = 0x611c08;
    int *voiceTable = voicePtbase[0];
    char *src;
    StringUtilGetScriptStringPtrEx(newName, &src);

    short length = StringUtilGetLength(src);

    if (length != 8)    //must be 8 characters
        return FALSE;

    char *dest = voiceTable[strIndex[0]];

    if (dest != 0)
    {
        StringUtilCopy(src, dest);
        return FALSE;
    }
    if (forced)
    {
        char *newVoice = MemAlloc(16);

        NoxByteMemset(newVoice, 16, 0);
        StringUtilCopy(src, newVoice);
    }
    return TRUE;
}

//기존 csf 문자열 변경하기//
int CsfChangeString(short csfId, string newString)
{
    int *keyPtr;

    CsfPrivateGetKeyPtr(csfId, &keyPtr);
    short *strIndex = keyPtr+50;
    int *strPtbase = 0x611c04;
    int *strTable = strPtbase[0];

    char *src;
    StringUtilGetScriptStringPtrEx(newString, &src);

    short length = StringUtilGetLength(src);

    if (!length || (length >= 1024))
        return FALSE;

    VariantStringImpl(src, ArrayRefN(strPtbase[0], strIndex[0]), 0xd9ecba61);
    return TRUE;
}

void NOXLibraryEntryPointFunction()
{
    "export CsfPrivateGetKeyPtr";
"export CsfPrivateGetVoiceString";
"export CsfWholeStringCount";
"export CsfGetKeyStringFromId";
"export CsfGetVoiceFileNameFromId";
"export CsfChangeVoiceFileName";
"export CsfChangeString";
}

