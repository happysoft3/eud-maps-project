
#include "opcodehelper.h"
#include "stringutil.h"

int ImportMemAlloc()
{
    int *link;

    if (!link)
    {
		int codeStream[] = {
        	0x49E85055, 0x50FFDB62, 0xCB2553E8, 0x1DE850FF, 0x83FFDB62, 0x5D5808C4, 0x909090C3 };
		link = codeStream;
        FixCallOpcode(link + 2, 0x507250);
        FixCallOpcode(link + 8, 0x403560);
        FixCallOpcode(link + 0x0e, 0x507230);
    }
    return link;
}

int MemAlloc(int size)
{
	int *builtins = 0x5c308c;
	int *pOld = builtins[95];

	builtins[95] = ImportMemAlloc();
    int *ptr = GetCharacterData(size);
	
	builtins[95] = pOld;
    return ptr;
}

int ImportMemFree()
{
    int *link;

    if (!link)
    {
		int codeStream[]={
        	0x624AE850,  0xE850FFDB, 0xFFCB3251, 0x5804C483, 0x909090C3};
		link = codeStream;
        FixCallOpcode(link + 1, 0x507250);
        FixCallOpcode(link + 7, 0x40425d);
    }
    return link;
}

void MemFree(int *ptr)
{
	int *builtins = 0x5c308c;
	int *pOld = builtins[31];
	builtins[31] = ImportMemFree();
    Unused1f(ptr);
	builtins[31] = pOld;
}

// int 배열을, value 값으로 초기화 합니다 //
void NoxDwordMemset(int *destPtr, int length, int value)
{
	while (--length>=0)
		destPtr[length]=value;
}

// short 배열을, value 값으로 초기화 합니다 //
void NoxWordMemset(short *wordPtr, int length, short value)
{
	while (--length>=0)
		wordPtr[length]=value;
}

// char 배열을, value 값으로 초기화 합니다 //
void NoxByteMemset(char *bytePtr, int length, char value)
{
	while (--length>=0)
		bytePtr[length]=value;
}

void NoxDwordMemCopy(int *src, int *dest, int length)
{
	while (--length>=0)
		dest[length]=src[length];
}

void NoxWordMemCopy(short *src, short *dest, int length)
{
	while (--length>=0)
		dest[length]=src[length];
}

void NoxByteMemCopy(char *src, char *dest, int length)
{
	while (--length>=0)
		dest[length]=src[length];
}

//!Deprecated!// 할당된 메모리는 현재 맵이 종료될 때 해제됩니다
short MemUtilAppendStrsectionAllocMemory(int *allocptr)
{
	short *scriptStringCount = 0x75ae24;
	// int scrstrcount = offset[0];
	int *scriptStringTable = 0x97bb40;

	scriptStringTable[scriptStringCount[0]] = allocptr;
	return ++scriptStringCount[0];
}

void InitializeSmartMemoryDestructor()
{
	char *retarg;

	if (retarg)
		return;

	char code[]={0xA1, 0xDC, 0x56, 0x59, 0x00, 0x85, 0xC0, 0x74, 0x0E, 0xFF, 0x30, 0x50, 
		0xB8, 0x5D, 0x42, 0x40, 0x00, 0xFF, 0xD0, 0x58, 0x58, 0xEB, 0xEE, 0xC7, 0x05, 0x4C, 
		0x82, 0x59, 0x00, 0xE0, 0xA6, 0x42, 0x00, 0x68, 0xE0, 0xA6, 0x42, 0x00, 0xC3};

	SetMemory(0x5956dc, 0);
	retarg = 0x59567c;
	NoxByteMemCopy(code, retarg, sizeof(code));
	// int *t=0x59821c;
	int *t=0x59824c;
	int *c = &retarg[34];
	c[0]=t[0];
	c=&retarg[29];
	c[0]=t[0];
	t[0]=retarg;
}

/*
typedef struct _memNode
{
	struct _memNode *pNext;
	struct _memNode *pPrev;
	//userdata
};
*/

#define _NEXT 0
#define _PREV 1

void AllocSmartMemEx(int allocsize, int **pDestptr)
{
	InitializeSmartMemoryDestructor();

	int *off = 0x5956dc;
	int *firstNode = off[0];
	int *ptr = MemAlloc(allocsize + 8);

	if (firstNode)
	{
		ptr[_NEXT] = firstNode;	//ptr->next=first;
		firstNode[_PREV] = ptr;
	}
	else
		ptr[_NEXT] =0;	
	
	ptr[_PREV] = 0;	//prev
	off[0] = ptr;
	pDestptr[0] = ptr + 8;
}

void FreeSmartMemEx(int *pMem)
{
	int *ptr = pMem - 8;
	int *next = ptr[_NEXT], *prev = ptr[_PREV];
	
	if (prev)
	{
		prev[_NEXT] = next;
	}
	else
	{
		SetMemory(0x5956dc, next);
	}
	if (next)
	{
		next[_PREV] = prev;
	}
	MemFree(ptr);
}

#undef _NEXT
#undef _PREV

#define INVALID_POINTER 0

void DwordCopy(int *src, int *dest, int count)
{
    char *p;

    if (p==INVALID_POINTER)
    {
        char code[]={
            0x56, 0x57, 0x51, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 
            0x30, 0x8B, 0x78, 0x04, 0x8B, 0x48, 0x08, 0xF3, 0xA5, 0x59, 0x5F, 0x5E, 0x31, 0xC0, 0xC3,
        };
        p=code;
    }
    invokeRawCode(p,&src);
}

