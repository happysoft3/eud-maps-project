
#include "define.h"
#include "typecast.h"
#include "stringutil.h"
#include "unitutil.h"
#include "mathlab.h"

#define MAX_LENGTH 200

//Changed by Lycoris 2023/03/11 vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
int CheckGameRusLanguage()
{
    int *csfBase = 0x611c04;
    int *csfTable = csfBase[0];

    if (csfTable)
    {
        int* csfIntPtr = csfTable[3];
		int localization = csfIntPtr[3];
		return localization == LOCAL_RUS_TRIADA 
			 || localization == LOCAL_RUS_FARGUS 
			 || localization == LOCAL_RUS_RUSSIANPROJECT 
			 || localization == LOCAL_RUS_8BIT 
			 || localization == LOCAL_RUS_WOLF;
    }
    return 0;
}

//Changed by Lycoris 2023/03/11 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//사용자의 언어환경이 '한글'인지 확인합니다. 한글이 아닌 경우 0가 반환됩니다
int CheckGameKorLanguage()
{
    int *csfBase = 0x611c04;
    int *csfTable = csfBase[0];

    if (csfTable)
    {
        short cmp[] = {0xd300, 0xc774, 0x20, 0xc5c6, 0xc2b5, 0xb2c8, 0xb2e4, 0x2e, 0};

        return (StringUtilWideCompare(csfTable[0], cmp)==0);
    }
    return 0;
}

int ImportUniChatCore()
{
    int arr[10], link;

    if (!link)
    {
        arr[0] = 0xC0685657; arr[1] = 0x6800528A; arr[2] = 0x00507250; arr[3] = 0x8B2414FF; arr[4] = 0x2414FFF8;
        arr[5] = 0x14FFF08B; arr[6] = 0x56505724; arr[7] = 0x102454FF; arr[8] = 0x5E14C483; arr[9] = 0x9090C35F;
        link = arr;
    }
    return link;
}

void UniChatCore(int *plrPtr, short *wMessage, int sTime)
{
    int temp = GetMemory(0x5c3320);

    SetMemory(0x5c3320, ImportUniChatCore());
    GroupRunAway(wMessage, plrPtr, sTime);
    SetMemory(0x5c3320, temp);
}

int ImportUniPrintCore()
{
    int arr[8], link;

    if (!link)
    {
        arr[0] = 0x9EB06856; arr[1] = 0x5068004D; arr[2] = 0xFF005072; arr[3] = 0xF08B2414; arr[4] = 0x502414FF;
        arr[5] = 0x2454FF56; arr[6] = 0x10C4830C; arr[7] = 0x9090C35E;
        link = &arr[0];
    }
    return link;
}

void UniPrintCore(int *plrPtr, short *wMessage)
{
    int temp = GetMemory(0x5c31f4);

    SetMemory(0x5c31f4, ImportUniPrintCore());
    Unused5a(wMessage, plrPtr);
    SetMemory(0x5c31f4, temp);
}

//특정 플레이어에게 메시지를 표시합니다
void UniPrint(int sUnit, string sMsg)
{
    short wDest[MAX_LENGTH];
    int *ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(sMsg), wDest);
        UniPrintCore(ptr, wDest);
    }
}

//duration 프레임 초 동안 sUnit 유닛이 sMsg 메시지 내용으로 채팅창을 띄웁니다
void UniChatMessage(int sUnit, string sMsg, int duration)
{
    short wDest[MAX_LENGTH];
    int *ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(sMsg), wDest);
        UniChatCore(ptr, wDest, duration);
    }
}

//모든 플레이어의 화면에 메시지를 표시합니다
void UniPrintToAll(string sMsg)
{
    short wDest[MAX_LENGTH];
    int *plrPtr = 0x62f9e0;

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(sMsg), wDest);

    int rep = -1;
    while (++rep<32)
    {
        if (plrPtr[0])
            UniPrintCore(plrPtr[0], wDest);
        plrPtr += 0x12dc;
    }
}

int CheckSignDelay(int *sPtr, int gap)
{
    int *cFps = 0x84ea04;

    if (ABS(cFps[0] - sPtr[0]) > gap)
    {
        sPtr[0] = cFps[0];
        return 1;
    }
    return 0;
}

void SignNotification()
{
    int *otPtr = 0x979720, *sePtr = 0x979724;

    if (otPtr[0] && sePtr[0])
    {
        int *pData = sePtr[0];
        if (CheckSignDelay(pData[184] + 100, 60))
            UniPrint(OTHER, ToStr(pData[188]));
    }
}

//표지판 메시지를 등록합니다
void RegistSignMessage(int sUnit, string sMsg)
{
    int *ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        ptr[188] = sMsg;
        SetUnitCallbackOnUseItem(sUnit, SignNotification);
    }
}

int ImportUniBroadcast()
{
    int arr[6], link;

    if (!link)
    {
        arr[0] = 0x4D9FD068; arr[1] = 0x72506800; arr[2] = 0x14FF0050; arr[3] = 0x106A5024;
        arr[4] = 0x0C2454FF; arr[5] = 0xC310C483;
        link = &arr[0];
    }
    return link;
}

//모든 플레이어에게 공고 메시지를 보여줍니다.
//공고 메시지 버그로 인해 이 함수사용을 비추천합니다
void UniBroadcast(string sMsg)
{
    short wDest[MAX_LENGTH];
    int temp = GetMemory(0x5c3108);

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(sMsg), wDest);
    SetMemory(0x5c3108, ImportUniBroadcast());
    Unused1f(wDest);
    SetMemory(0x5c3108, temp);
}

#undef MAX_LENGTH

