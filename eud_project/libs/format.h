
#include "define.h"
#include "memutil.h"
#include "array.h"

// int IsValidFormatToken(char c)
// {
//     char *ptb;
//     int tbSize;

//     if (!ptb)
//     {
//         char tb[128];
//         ptb=&tb;
//         char vtok[]={'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'e', '.'};
//         int work=sizeof(vtok);
//         while(--work>=0)
//             tb[vtok[work]]=TRUE;
//         tbSize=sizeof(tb);
//     }
//     if (c>=tbSize) return FALSE;
//     return ptb[c];
// }

int IsValidFormatIdentifier(char c)
{
    char *ptb;
    int tbSize;
    if (!ptb)
    {
        char tb[256];
        ptb=&tb;
        char fmtId[]={'d', 's','l','f','x','p','i','c','u'};
        int work=sizeof(fmtId);
        while(--work>=0)
            tb[fmtId[work]]=TRUE;
    }
    return ptb[c];
}

void InvestigateFormatIdIndexes(char *fmt, char *pArr, char id)
{
    int argc=0, infmt=FALSE, workpos=-1;
    while (fmt[++workpos])
    {
        if (infmt)
        {
            // if (IsValidFormatToken(fmt[workpos]))
            // {
            //     continue;
            // }
            if (IsValidFormatIdentifier(fmt[workpos]))
            {
                pArr[argc++]=fmt[workpos]==id;
                infmt=FALSE;
            }
            if (fmt[workpos]=='%')
                infmt=FALSE;
            continue;
        }
        if (fmt[workpos]=='%')
        {
            infmt=TRUE;
            continue;
        }
    }
}

void NoxSprintfPushVariadicArg(char *fmt, char *pCodeLine, int length, int *picker, int *ptrs, int *pCount)
{
    char ids[64];
    
    InvestigateFormatIdIndexes(fmt, &ids, 'f');
    int *ptr;
    while (--length>=0)
    {
        if (ids[length])
        {
            ptr=pCodeLine+picker[0];
            ptr[0]=0xb808ec83;
            ptr[1]=ArrayRefN(ptrs, length);
            ptr[2]=0x1cdd00d9;
            picker[0]+=12;
            pCodeLine[picker[0]++]=0x24 + (++pCount[0]*0);
            continue;
        }
        pCodeLine[picker[0]++]=0x68;
        ptr=pCodeLine+picker[0];
        ptr[0]=ptrs[length];
        picker[0]+=4;
    }
}

void NoxSprintf(char *pDest, char *fmt, int *ptrs, char length)
{
    char codeLine[256];
    int cpLength=length;
    int picker=0;
    int *ptr, fCount=0;

    NoxSprintfPushVariadicArg(fmt, &codeLine, length, &picker, ptrs, &fCount);

    char postCode[]={0x68, 0xD0, 0x5A, 0x59, 0x00, 0x68, 0x90, 0x6E, 0x65, 0x00, 0xE8, 0x18, 0x4C, 0xE1, 0xFF, 0x83, 0xC4, 0x10, 0xC3, 0x90};

    ptr=&postCode+1;
    ptr[0]=fmt;
    ptr=&postCode+6;
    ptr[0]=pDest;
    postCode[17]=(length+fCount+2)*4;
    NoxByteMemCopy(&postCode, &codeLine + picker, sizeof(postCode));
    FixCallOpcode(&codeLine + picker + 0x0a, 0x565c27);

    int *pBuiltins=0x5c308c;
    int *pOld=pBuiltins[80];

    pBuiltins[80]=&codeLine;
    Unused50();
    pBuiltins[80]=pOld;
}

void NoxSprintfString(char *pDest, string fmt, int *ptrs, char length)
{
	NoxSprintf(pDest, StringUtilGetScriptStringPtr(fmt), ptrs, length);
}

void NoxSprintfOne(char *pDest, string fmt, int *ptr)
{
    NoxSprintf(pDest, StringUtilGetScriptStringPtr(fmt), &ptr, 1);
}

void NoxSprintfOneString(char *pDest, string fmt, string str)
{
    char *src = StringUtilGetScriptStringPtr(str);

    NoxSprintf(pDest, StringUtilGetScriptStringPtr(fmt), &src, 1);
}

void NOXLibraryEntryPointFunction()
{
    // "export IsValidFormatToken";
    "export IsValidFormatIdentifier";
    "export InvestigateFormatIdIndexes";
    "export NoxSprintfPushVariadicArg";
    "export NoxSprintf";
    "export NoxSprintfString";
    "export NoxSprintfOne";
    "export NoxSprintfOneString";
}
