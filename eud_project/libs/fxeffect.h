
#include "typecast.h"
#include "callmethod.h"
#include "unitutil.h"

int ImportPlaySoundAround()
{
	int *pp;

	if (!pp)
	{
		int codeStream[]={
			0x50196068, 0x72506800, 0x50560050, 0x082454FF,
			0x54FFF08B, 0x006A0824, 0x5650006A, 0x1C2454FF,
			0x5810C483, 0x08C4835E, 0x909090C3};
		pp=codeStream;
	}
	return pp;
}

void PlaySoundAround(int sUnit, short sNumber)
{
	int *unitPtr = UnitToPtr(sUnit);

	if (unitPtr)
	{
		int *pExec=0x5c325c;
		int *pOld=pExec[0];
		pExec[0]= ImportPlaySoundAround();
		Unused74(unitPtr, sNumber);
		pExec[0]=pOld;
	}
}

int ImportGreenExplosionFunc()
{
    int *pp;

    if (!pp)
    {
		int codeStream[]={
        0x32006856, 0x50680052, 0x68005072, 0x00403560, 0x54FF086A,
		0xC4830424, 0xFFF08B04, 0x89042454, 0x2454FF06, 0x04468904,
		0x0000C868, 0x54FF5600, 0xC4831024, 0x425D6814, 0xFF560040,
		0x83042454, 0xC35E08C4};
        pp=codeStream;
    }
    return pp;
}

void GreenExplosion(float x, float y)
{
	int *pExec=0x5c31f4;
	int *pOld=pExec[0];

    pExec[0]= ImportGreenExplosionFunc();
    Unused5a(ToInt(y), ToInt(x));
	pExec[0]=pOld;
}

int ImportOrbMoveFunc()
{
	int *pp;

	if (!pp)
	{
		int codeStream[]={
			0x52353068, 0x72506800, 0x14FF0050, 0x54FF5024, 0xC4830824, 0x9090C30C};
		pp=codeStream;
	}
	return pp;
}

void LinearOrbMove(int unit, float x_vect, float y_vect, float speed, int time)
{
	int *ptr = UnitToPtr(unit), temp = GetMemory(0x5c336c);

	if (ptr)
	{
		ptr[20]=x_vect;
		ptr[21]=y_vect;
		ptr[28]=speed;
		int *pExec=0x5c336c;
		int *pOld=pExec[0];
		pExec[0]= ImportOrbMoveFunc();
		Unknownb8(ptr);
		pExec[0]=pOld;
		DeleteObjectTimer(unit, time);
	}
}

int ImportGreenLightningFunc()
{
    int *pp;

    if (!pp)
    {
		int codeStream[]={
			0x5D685657, 0x68004042, 0x00523790, 0x50725068,0x35606800,
			0x106A0040, 0x042454FF, 0x8B08C483, 0x2414FFF0,0x14FFF88B,
			0x0C468924, 0x892414FF, 0x14FF0846, 0x04468924,0x892414FF,
			0xFF565706, 0x830C2454, 0xFF5608C4, 0x830C2454,0x5F5E10C4, 0x909090C3};
		pp=codeStream;
    }
    return pp;
}

void GreenLightningFx(int x1, int y1, int x2, int y2, int time)
{
	int *pExec=0x5c321c;
	int *pOld=pExec[0];

    pExec[0]= ImportGreenLightningFunc();
    Effect(ToStr(x1), ToFloat(y1), ToFloat(x2), ToFloat(y2), ToFloat(time));
	pExec[0]=pOld;
}

void NetLoadFx(int netId, int ty, int *args, int *args2)
{
    char *pcode;

    if (!pcode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x0C, 0xFF, 0x70, 0x08,
            0xFF, 0x70, 0x04, 0xFF, 0x30, 0xB8, 0x50, 0x31, 0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3, 0x90
        };
        pcode=code;
    }
    invokeRawCode(pcode, &netId);
}

void GreenSparkAt(float x, float y)
{
    int *p=&x;
    NetLoadFx(0xF0, 0x1A, p, p);
}

void PlaySummonEffect(float *xy, short thingId, int duration)
{
    char *p;

    if (!p)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
    0xFF, 0x70, 0x08, 0xFF, 0x70, 0x0C, 0xFF, 0x70, 0x10, 0xB8, 0xF0, 0x36, 
    0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
        };
        p=code;
    }
    int args[]={
        duration,
        thingId,
        0,
        xy,
        0,
    };
    invokeRawCode(p, args);
}
