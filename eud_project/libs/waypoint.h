

#include "typecast.h"
#include "callmethod.h"


int g_waypointTable;
int g_MapWaypointFill;

/*
int ToInt(float x) { StopScript(x); }
float ToFloat(int x) { StopScript(x); }

void SetMemory(int a, int v){}
int GetMemory(int a){}
int GetScrDataField(int functionName){}*/

int MapWaypointTable(int idx)
{
    int table[600];

    return table[idx - 1];
}

float LocationX(int location)
{
    StopScript(GetMemory(MapWaypointTable(location) + 8));
}

float LocationY(int location)
{
    StopScript(GetMemory(MapWaypointTable(location) + 12));
}

void TeleportLocation(int location, float xProfile, float yProfile)
{
    int wTable = MapWaypointTable(location);

    SetMemory(wTable + 8, ToInt(xProfile));
    SetMemory(wTable + 12, ToInt(yProfile));
    return;
}

void TeleportLocationVector(int location, float xVect, float yVect)
{
    int wTable = MapWaypointTable(location);

    SetMemory(wTable + 8, ToInt(ToFloat(GetMemory(wTable + 8)) + xVect));
    SetMemory(wTable + 12, ToInt(ToFloat(GetMemory(wTable + 12)) + yVect));
    return;
}

void MapWaypointFill(int *wAddr, int *tPtr)
{
	int ccc = GetScrDataField(-g_MapWaypointFill);
	
    while (wAddr)
    {
		SetMemory(tPtr + (GetMemory(wAddr)<<2), wAddr);
		
		SetMemory(ccc, GetMemory(wAddr+484));
    }
}

void MapWaypointInit()
{
    MapWaypointFill(GetMemory(0x83c7fc), GetScrDataField(-g_waypointTable));
}

void NOXLibraryEntryPointFunction()
{
	"export MapWaypointTable";
	"export LocationX";
	"export LocationY";
	"export TeleportLocation";
	"export TeleportLocationVector";
	"export MapWaypointFill";
	"export needinit MapWaypointInit";
	
    g_waypointTable = MapWaypointTable;
	g_MapWaypointFill=MapWaypointFill;
}