
#include "memutil.h"

int DrawTextImportRemoveSpriteFromThingId()
{
    int *ppp;

    if (!ppp)
    {
        int codeStream[]={
            0x5008EC83, 0x6D3DC0A1, 0x24448900, 0x623EE808, 0x4489FFDB, 0x8B560424,
            0x850C2474, 0x8B1B74F6, 0xFF082444, 0x000170B6, 0x768B5600, 0x75F0396C,
            0x94AAE805, 0x5E58FFD0, 0x585EE1EB, 0xC308C483};
        ppp=&codeStream;
        FixCallOpcode(ppp + 0x0d, 0x507250);
        FixCallOpcode(ppp + 0x31, 0x45a4e0);
    }
    return ppp;
}

void DrawTextRemoveSpriteFromThingId(int thingId)
{
    int *pExec=0x5c3108;
    int *pOld=pExec[0];

    pExec[0]=DrawTextImportRemoveSpriteFromThingId();
    Unused1f(thingId);
    pExec[0]=pOld;
}

void DrawTextMappingTextDrawCode(int *ptr)
{
    int codeStream[]={
        0x8B20EC83, 0x84EA040D,0x56555300,0x3424748B,
        0xAC3D8B57,0x8B00853B,0x00012086,0x72C83900,
        0xE03D8B06,0x570084C9,0xCE336FE8,0x988e8bff,
        0x51000000,0xCE3293E8,0x244C8BFF,0x0C468B3C,
        0x8B08C483,0x118B1059,0x2914698B,0x105E8BD8,
        0x518BD001,0x704E8B04,0x4489EA29,0xDA011024,
        0x14245489,0xA16C4E8B,0x0069F224,0x8B88048B,
        0xD2850450,0x00BA0575,0x8B0058D7,0x8B142444,
        0x5010244C,0x006A5251,0xCEE58FE8,0x10C483FF,
        0x000001B8,0x5D5E5F00,0x20C4835B,0x909090C3
    };
    NoxDwordMemCopy(&codeStream, ptr, sizeof(codeStream));
    FixCallOpcode(ptr + 40, 0x434460);
    FixCallOpcode(ptr + 52, 0x434390);
    FixCallOpcode(ptr + 136, 0x43f6e0);
}

//텍스트 그리기 함수입니다
int DrawTextBuildTextDrawFunction()
{
    int offset = 0x5cebdc;
    
    DrawTextMappingTextDrawCode(offset);
    return offset;
}

void DrawTextModifyThingClassDescription(short thingId, string desc, short textColor)
{
    int *spriteOff = 0x69f224;
    int *spriteTb=spriteOff[0];
    int *spritePtr = spriteTb[thingId];

    char *srcPtr;
    
    StringUtilGetScriptStringPtrEx(desc, &srcPtr);
    short length = StringUtilGetLength(srcPtr);

    if (!length || (length>128))
        return;

    short *dest = MemAlloc(length*2+4), *prevPtr = spritePtr[1];

    NoxUtf8ToUnicode(srcPtr, dest);
    if (textColor)
        spritePtr[12]=textColor;
    spritePtr[1]=dest;
    if (prevPtr)
        MemFree(prevPtr);
}

void DrawTextModifyThingClassDrawFunction(short thingId, int* drawFn)
{
    int *spriteOff=0x69f224;
    int *spriteTb=spriteOff[0];
    int *spritePtr=spriteTb[thingId];

    spritePtr[22]=drawFn;
}

//유닛의 모습을 글자로 바꿉니다 !주의!: IxTempleExtPiece01 와 같이 모습이 없는 유닛위주로 지정하세요
//thingId: 대상 유닛의 thingDb id 값입니다
//drawMethod: 텍스트로 유닛 그리기 함수입니다
//colorSet: RGB565 색상 값입니다
//desc: 글자 내용입니다
void DrawTextSetupBottmText(short thingId, int *drawMethod, short colorSet, string desc)
{
    DrawTextModifyThingClassDescription(thingId, desc, colorSet);
    DrawTextModifyThingClassDrawFunction(thingId, drawMethod);
    DrawTextRemoveSpriteFromThingId(thingId);
}

void NOXLibraryEntryPointFunction()
{
	"export DrawTextImportRemoveSpriteFromThingId";
	"export DrawTextRemoveSpriteFromThingId";
	"export DrawTextMappingTextDrawCode";
	"export DrawTextBuildTextDrawFunction";
	"export DrawTextModifyThingClassDescription";
	"export DrawTextModifyThingClassDrawFunction";
	"export DrawTextSetupBottmText";
}

