
#include "spriteDb.h"
#include "grplib.h"

#define FRAME_MAX_COUNT 1600

#define ANIM_CORE_IMGVECTOR 0
#define ANIM_CORE_THINGID 1
#define ANIM_CORE_FRAMES 2
#define ANIM_CORE_SIZES 3
#define ANIM_CORE_FRAME_COUNT 4
#define ANIM_CORE_ACTION_TYPE 5
#define ANIM_CORE_ANIM_NUM 6
#define ANIM_CORE_ANIM_OBJECT 7
#define ANIM_CORE_FRAME_ID 8
#define ANIM_CORE_ASSIGNFN 9
#define ANIM_CORE_COPYFN 10
#define ANIM_CORE_MAX 20
void AnimCoreData(int **get){
	int data[ANIM_CORE_MAX];
	get[0]=data;
}

void AnimLoadGRP(int resourceFn, int thingId, int imgVector, int *xyInc){
	int *frames, *sizes ;
	int frameCount = UnpackAllFromGrp(GetScrCodeField(resourceFn)+4,
		 thingId, xyInc, &frames, &sizes );
	int *p;
	AnimCoreData(&p);
	p[ANIM_CORE_THINGID]=thingId;
	p[ANIM_CORE_FRAMES]=frames;
	p[ANIM_CORE_SIZES]=sizes;
	p[ANIM_CORE_IMGVECTOR]=imgVector;
    p[ANIM_CORE_FRAME_COUNT]=frameCount;
    p[ANIM_CORE_FRAME_ID]=0;
}

void AnimAssign8(int imgId){
	int *params;

    AnimCoreData(&params);
    int *anim =params[ANIM_CORE_ANIM_OBJECT];
    int frameId=params[ANIM_CORE_FRAME_ID];
    anim=&anim[frameId];
    params[ANIM_CORE_FRAME_ID]+=8;
    int imgVec=params[ANIM_CORE_IMGVECTOR],thingId=params[ANIM_CORE_THINGID];
    int *imgTables = getMonsterSpriteImageTablePtrBase(thingId,params[ANIM_CORE_ACTION_TYPE]);
    int imagePtr, n=(params[ANIM_CORE_ANIM_NUM]++)<<2;

    ImageUtilGetPtrFromID(imgId, &imagePtr);
    AppendImageFrame(imgVec, anim[0], imgId);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    AppendImageFrame(imgVec, anim[1], imgId+1);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    AppendImageFrame(imgVec, anim[2], imgId+2);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+24);
    AppendImageFrame(imgVec, anim[3], imgId+3);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+36);
    AppendImageFrame(imgVec, anim[4], imgId+4);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+48);
    AppendImageFrame(imgVec, anim[5], imgId+5);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+60);
    AppendImageFrame(imgVec, anim[6], imgId+6);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+72);
    AppendImageFrame(imgVec, anim[7], imgId+7);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr+84);
}

void AnimCopy8(int imgId)
{
    int *params;

    AnimCoreData(&params);
    int thingId=params[ANIM_CORE_THINGID];
int *imgTables = getMonsterSpriteImageTablePtrBase(thingId,params[ANIM_CORE_ACTION_TYPE]);
    int imagePtr,n=(params[ANIM_CORE_ANIM_NUM]++)<<2;

    ImageUtilGetPtrFromID(imgId, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+24);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+36);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+48);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+60);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+72);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr+84);
}

void AnimAssign4(int imgId)
{
int *params;

AnimCoreData(&params);
int *anim =params[ANIM_CORE_ANIM_OBJECT], frameId=params[ANIM_CORE_FRAME_ID];
anim=&anim[frameId];
params[ANIM_CORE_FRAME_ID]+=4;
int imgVec=params[ANIM_CORE_IMGVECTOR], thingId=params[ANIM_CORE_THINGID];
int *imgTables = getMonsterSpriteImageTablePtrBase(thingId,params[ANIM_CORE_ACTION_TYPE]);
int imagePtr,n=(params[ANIM_CORE_ANIM_NUM]++)<<2;

AppendImageFrame(imgVec, anim[0],imgId);
AppendImageFrame(imgVec, anim[1],imgId+1);
AppendImageFrame(imgVec, anim[2],imgId+2);
AppendImageFrame(imgVec, anim[3],imgId+3);
ImageUtilGetPtrFromID(imgId, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+36);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+36);
}

void AnimCopy4(int imgId)
{
    int *params;

    AnimCoreData(&params);
    int n=(params[ANIM_CORE_ANIM_NUM]++)<<2;
    int thingId=params[ANIM_CORE_THINGID],*imgTables = getMonsterSpriteImageTablePtrBase(thingId,params[ANIM_CORE_ACTION_TYPE]),imagePtr;
ImageUtilGetPtrFromID(imgId, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr+12);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr+24);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr+36);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr+36);
}

void AnimAssignOne(int imgId, int frameId){
    int *params;

    AnimCoreData(&params);
    int *imgTables = getMonsterSpriteImageTablePtrBase(params[ANIM_CORE_THINGID],params[ANIM_CORE_ACTION_TYPE]);
    int imagePtr,n=(params[ANIM_CORE_ANIM_NUM]++)<<2;

    ImageUtilGetPtrFromID(imgId, &imagePtr);
    int *pFrames=params[ANIM_CORE_FRAMES];
    AppendImageFrame(params[ANIM_CORE_IMGVECTOR], pFrames[frameId], imgId);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
}

void AnimCopyOne(int imgId){
    int *params;

    AnimCoreData(&params);
    int *imgTables = getMonsterSpriteImageTablePtrBase(params[ANIM_CORE_THINGID],params[ANIM_CORE_ACTION_TYPE]);
    int imagePtr,n=(params[ANIM_CORE_ANIM_NUM]++)<<2;

    ImageUtilGetPtrFromID(imgId, &imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_RIGHT]+n,imagePtr);
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_RIGHT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_RIGHT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_DOWN_LEFT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_LEFT]+n,imagePtr);    
    SetRecoveryDataNormal(imgTables[IMAGE_SPRITE_DIRECTION_UP_LEFT]+n,imagePtr);
}

void AnimCreate8Dir(){
    int *params;
    AnimCoreData(&params);
    int *frames =params[ANIM_CORE_FRAMES], *sizes=params[ANIM_CORE_SIZES];
    int frameCount=params[ANIM_CORE_FRAME_COUNT];
    int anim[FRAME_MAX_COUNT];
    int r=0,ar=0;

    while (r+5<=frameCount)
    {
        DwordCopy(&frames[r],&anim[ar],5);
        anim[ar+5]=LRInversionGRP(frames[r+3],sizes[r+3]);
        anim[ar+6]=LRInversionGRP(frames[r+2],sizes[r+2]);
        anim[ar+7]=LRInversionGRP(frames[r+1],sizes[r+1]);
        ar+=8;
        r+=5;
    }    
    while (r<frameCount) //남은 프레임을 처리합니다
        anim[ar++]=frames[r++];
    params[ANIM_CORE_ANIM_OBJECT]=anim;
    params[ANIM_CORE_ASSIGNFN]=AnimAssign8;
    params[ANIM_CORE_COPYFN]=AnimCopy8;
}
void AnimCreate4Dir()
{
    int *params;
    AnimCoreData(&params);
    int *frames =params[ANIM_CORE_FRAMES], *sizes=params[ANIM_CORE_SIZES];
    int anim[FRAME_MAX_COUNT],frameCount=params[ANIM_CORE_FRAME_COUNT];
    int r=0,ar=0;
    while (r+3<=frameCount)
    {
        DwordCopy(&frames[r],&anim[ar],3);
        anim[ar+3]=LRInversionGRP(frames[r+1],sizes[r+1]);
        ar+=4;
        r+=3;
    }
    while (r<frameCount)
        anim[ar++]=frames[r++];
    params[ANIM_CORE_ANIM_OBJECT]=anim;
    params[ANIM_CORE_ASSIGNFN]=AnimAssign4;
    params[ANIM_CORE_COPYFN]=AnimCopy4;
}
void AnimSetAction(int actionId, int count, int frames){
	int *p;
	AnimCoreData(&p);
	ChangeMonsterSpriteImageHeader(p[ANIM_CORE_THINGID ],actionId,count,frames,2);
	p[ANIM_CORE_ACTION_TYPE ]=actionId;
	p[ANIM_CORE_ANIM_NUM ]=0;
}
void AnimAssign(int imgId){
    int *p;
    AnimCoreData(&p);
    Bind(p[ANIM_CORE_ASSIGNFN],&imgId);
}
void AnimCopy(int imgId){
    int *p;
    AnimCoreData(&p);
    Bind(p[ANIM_CORE_COPYFN],&imgId);
}







