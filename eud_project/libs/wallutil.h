
#include "typecast.h"
#include "callmethod.h"

int g_wallUtilCreateMagicWall;
int g_wallUtilImportAddBreakableWall;
int g_wallUtilImportCreateMagicWall;
int g_wallUtilImportGetWallAtPtr;

/*
int GetMemory(int a){}
void SetMemory(int a, int b){}
int GetScrDataField(int a){}
int FloatToInt(float a){}
*/

int WallUtilImportGetWallAtPtr()
{
    int arr[12], link;

    if (!link)
    {
        arr[0] = 0x50685056; arr[1] = 0xFF005072; arr[2] = 0xF08B2414; arr[3] = 0x832414FF; arr[4] = 0x506804C4; arr[5] = 0x56004102;
        arr[6] = 0x2454FF50; arr[7] = 0x0CC48308; arr[8] = 0x50723068; arr[9] = 0x54FF5000; arr[10] = 0xC4830424; arr[11] = 0xC35E5808;
		link = GetScrDataField(-g_wallUtilImportGetWallAtPtr);
    }
    return link;
}

//벽 id 값을 PTR 으로 변환합니다
//해당 벽이 유효한 경우 벽의 PTR 값이 반환됩니다
//벽이 존재하지 않는 경우 0 이 반환됩니다
int WallUtilWallUniqueIdToPtr(int wallId)
{
    int temp = GetMemory(0x5c325c);
	int xWall = wallId >> 0x10, yWall = wallId & 0xffff;

    SetMemory(0x5c325c, WallUtilImportGetWallAtPtr());
    CancelTimer(FrameTimerWithArg(10, Unused74(xWall, yWall), 0));
    SetMemory(0x5c325c, temp);
    return GetMemory(GetMemory(0x83395c) + 8);
}

//벽 좌표로 부터 PTR을 얻습니다
//유효하지 않으면 0 이 반환됩니다
int WallUtilGetWallPtrFromCoor(int wallX, int wallY)
{
	int temp = GetMemory(0x5c325c);

    SetMemory(0x5c325c, WallUtilImportGetWallAtPtr());
    CancelTimer(FrameTimerWithArg(10, Unused74(wallX, wallY), 0));
    SetMemory(0x5c325c, temp);
    return GetMemory(GetMemory(0x83395c) + 8);
}

//벽이 열렸는지 확인합니다
//벽이 닫혀있거나 유효하지 않은 경우 0 값이 반환 됩니다
int WallUtilWallIsOpened(int wallId)
{
	int wallPtr = WallUtilWallUniqueIdToPtr(wallId);
	
	if (wallPtr)
	{
		int details = GetMemory(wallPtr + 0x1c);
		
		if (details)
			return GetMemory(details + 0x1c) & 0x80000000;
	}
	return false;
}

//벽이 파괴되었는 지 확인합니다
int WallUtilWallIsDestroyed(int wallId)
{
	int wallPtr = WallUtilWallUniqueIdToPtr(wallId);
	
	if (wallPtr)
		return (GetMemory(wallPtr + 0x4) << 0x18) & true;
	return false;
}

int WallUtilImportCreateMagicWall()
{
    int arr[10], link;

    if (!link)
    {
        arr[0] = 0x72506856; arr[1] = 0x14FF0050; arr[2] = 0x68F08B24; arr[3] = 0x004FFD00; arr[4] = 0xFF1076FF; arr[5] = 0x76FF0C76;
        arr[6] = 0x0476FF08; arr[7] = 0x54FF36FF; arr[8] = 0xC4831424; arr[9] = 0x90C35E1C;
		link = GetScrDataField(-g_wallUtilImportCreateMagicWall);
    }
    return link;
}

//마법 벽을 생성합니다
//sPtr: unknown- set to 0
//sWallX: 벽 좌표계 x좌표
//sWallY: 벽 좌표계 y좌표
//sWallDir: 벽의 방향입니다
//sUnk2: unknown- set to 0
//성공적으로 생성되었다면 생성된 벽의 PTR이 반환됩니다
int WallUtilCreateMagicWall(int sPtr, int sWallX, int sWallY, int sWallDir, int sUnk2)
{
    int link, temp = GetMemory(0x5c3108);

    if (!link)
		link = GetScrDataField(-g_wallUtilCreateMagicWall);
    SetMemory(0x5c3108, WallUtilImportCreateMagicWall());
    Unused1f(link);
    SetMemory(0x5c3108, temp);
	
	return WallUtilGetWallPtrFromCoor(sWallX, sWallY);
}

int WallUtilImportAddBreakableWall()
{
    int arr[6], link;

    if (!link)
    {
        arr[0] = 0x08406850; arr[1] = 0x50680041; arr[2] = 0xFF005072; arr[3] = 0xFF502414; arr[4] = 0x83082454; arr[5] = 0xC3580CC4;
		link = GetScrDataField(-g_wallUtilImportAddBreakableWall);
    }
    return link;
}

//벽을 부서질 수 있는 상태로 만듦니다
void WallUtilAddBreakableWall(int wallId)
{
	int wallPtr = WallUtilWallUniqueIdToPtr(wallId);
	
	if (wallPtr)
	{
		int temp = GetMemory(0x5c3108);

		SetMemory(0x5c3108, WallUtilImportAddBreakableWall());
		Unused1f(wallPtr);
		SetMemory(0x5c3108, temp);
	}
}

int WallUtilGetWallAtObjectPosition(int anyunit)
{
	int xPos, yPos;
	
	if (anyunit >= GetHost() || anyunit == self || anyunit == other)
	{
		xPos = FloatToInt(GetObjectX(anyunit));
		yPos = FloatToInt(GetObjectY(anyunit));
	}
	else
	{
		xPos = FloatToInt(GetWaypointX(anyunit));
		yPos = FloatToInt(GetWaypointY(anyunit));
	}
    if (xPos > yPos) xPos += 23;
    else             yPos += 23;
    int spX = (xPos + yPos - 22) / 46;
    int spY = (xPos - yPos) / 46;
    int tx = spX * 46;
    int ty = spY * 46;
    int rx = (tx + ty) / 2;
    return Wall(rx / 23, (rx - ty) / 23);
}

void WallUtilCloseWallAtObjectPosition(int anyunit)
{
	int wallId = WallUtilGetWallAtObjectPosition(anyunit);
	
	if (wallId)
		WallClose(wallId);
}

void WallUtilOpenWallAtObjectPosition(int anyunit)
{
	int wallId = WallUtilGetWallAtObjectPosition(anyunit);
	
	if (wallId)
		WallOpen(wallId);
}

void WallUtilDestroyWallAtObjectPosition(int anyunit)
{
	int wallId = WallUtilGetWallAtObjectPosition(anyunit);
	
	if (wallId)
		WallBreak(wallId);
}

void WallUtilToggleWallAtObjectPosition(int anyunit)
{
	int wallId = WallUtilGetWallAtObjectPosition(anyunit);
	
	if (wallId)
		WallToggle(wallId);
}

void NOXLibraryEntryPointFunction()
{
	"export needinit WallUtilImportGetWallAtPtr";
	"export WallUtilWallUniqueIdToPtr";
	"export WallUtilGetWallPtrFromCoor";
	"export WallUtilWallIsOpened";
	"export WallUtilWallIsDestroyed";
	"export WallUtilImportCreateMagicWall";
	"export WallUtilCreateMagicWall";
	"export WallUtilImportAddBreakableWall";
	"export WallUtilAddBreakableWall";
	"export WallUtilGetWallAtObjectPosition";
	"export WallUtilCloseWallAtObjectPosition";
	"export WallUtilOpenWallAtObjectPosition";
	"export WallUtilDestroyWallAtObjectPosition";
	"export WallUtilToggleWallAtObjectPosition";
	
	g_wallUtilCreateMagicWall = WallUtilCreateMagicWall;
	g_wallUtilImportAddBreakableWall = WallUtilImportAddBreakableWall;
	g_wallUtilImportCreateMagicWall = WallUtilImportCreateMagicWall;
	g_wallUtilImportGetWallAtPtr = WallUtilImportGetWallAtPtr;
}