
#include "typecast.h"
#include "array.h"
#include "wildcard_string.h"

int ThingDbEditGetArray(short thingId)
{
	short *thingDbCount = 0x69f228;

	if (thingId >= thingDbCount[0])
		return 0;
	int *thingDbOff = 0x7520d4;
	int *thingDbTb = thingDbOff[0];
	
	return thingDbTb[thingId];
}

int ThingDbEditGetSpriteDbArray(short thingId)
{
	short *thingDbCount = 0x69f228;

	if (thingId >= thingDbCount[0])
		return 0;
	int *spriteOff = 0x69f224;
	int *spriteTb = spriteOff[0];
	
	return spriteTb[thingId];
}

// yellow, black, white 포션을 thingdb 수정을 통해 올바르게 동작하도록 고칩니다
//thingId: 해당 포션의 thing넘버
//healAmount: 체력회복 수치
//potionFlag: 마나포션으로 사용하려면 1을 입력. 그 외에는 체력포션으로 간주됩니다
void ThingDbEditFixColorPotion(short thingId, short healAmount, int potionFlag)
{
	int *thingTarget = ThingDbEditGetArray(thingId);
	
	if (!thingTarget)
		return;
	if (potionFlag == 1)
		potionFlag = 0x20;
	else
		potionFlag = 0x10;
	thingTarget[7]|=potionFlag;
	thingTarget[50] = 0x53ef70;
	if (thingTarget[51] == 0)
	{
		int *hpAlloc = MemAlloc(4);

		hpAlloc[0]=healAmount;
		thingTarget[51]=hpAlloc;
	}
	thingTarget[52]=4;
}

//thingdb에 정의된 유닛의 값어치를 변경합니다
void ThingDbEditChangeWorth(short thingId, int worth)
{
	int *thingTarget = ThingDbEditGetArray(thingId);
	
	if (!thingTarget)
		return;
	
	thingTarget[12]=worth;
}

//thingdb에 정의된 유닛의 마찰력을 변경합니다. 기본적으로 0.5입니다
void ThingDbEditChangeFriction(short thingId, float friction)
{
	int *thingTarget = ThingDbEditGetArray(thingId);
	
	if (!thingTarget)
		return;
	thingTarget[13]=friction;
}

//thingdb에 정의된 유닛의 무게를 변경합니다
void ThingDbEditChangeMass(short thingId, float mass)
{
	int *thingTarget = ThingDbEditGetArray(thingId);
	
	if (!thingTarget)
		return;
	thingTarget[14]=mass;
}

//thingdb에 정의된 유닛의 충돌크기를 변경합니다. 60을 넘지 않는것이 좋습니다
void ThingDbEditChangeExtentRadius(short thingId, float radius)
{
	int *thingTarget = ThingDbEditGetArray(thingId);
	
	if (!thingTarget)
		return;
	thingTarget[16]=radius;
}

//thingdb에 정의된 스프라이트의 선택원 크기를 변경합니다
//클라이언트 사이드입니다
void ThingDbEditSpriteSelectionRing(short thingId, float radius)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	spritedbTarget[16]=radius;
}

//thingdb에 정의된 스프라이트의 주변 조명색상을 변경합니다. 해당 색상을 변경하지 않을 경우 -1 을 전달하세요
//클라이언트 사이드입니다
void ThingDbEditSpriteLightColor(short thingId, char red, char green, char blue)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	if (red != -1)
		spritedbTarget[12]=red;
	if (green != -1)
		spritedbTarget[13]=green;
	if (blue != -1)
		spritedbTarget[14]=blue;
}

void ThingDbEditSpriteLightColorRGB(short thingId, int rgbCode)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);

	if (!spritedbTarget)
		return;
	char *rgbArr = &rgbCode;
	if (rgbArr[0])
		spritedbTarget[12]=rgbArr[0];
	if (rgbArr[1])
		spritedbTarget[13]=rgbArr[1];
	if (rgbArr[2])
		spritedbTarget[14]=rgbArr[2];
}

//thingdb에 정의된 스프라이트의 주변 조명의 강렬함을 수정합니다
//클라이언트 사이드입니다
void ThingDbEditLightIntensity(short thingId, float intensity)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	spritedbTarget[11]=intensity;
}

//thingdb에 정의된 스프라이트의 zorder을 수정합니다
//클라이언트 사이드입니다
void ThingDbEditSetZOrder(short thingId, float zorder)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	spritedbTarget[18]=zorder;
}

//thingdb에 정의된 스프라이트를 픽업가능하게 합니다
//클라이언트 사이드입니다
void ThingDbEditEnablePickup(short thingId)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	spritedbTarget[8]|=(0x80<<0x18);
}

//thingdb에 정의된 스프라이트를 픽업불가능하게 합니다
//클라이언트 사이드입니다
void ThingDbEditDisablePickup(short thingId)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	if (spritedbTarget[8] & (0x80 << 0x18))
		spritedbTarget[8]^=(0x80 << 0x18);
}

void ThingDbEditSetItemName(short thingId, int *nameptr)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	spritedbTarget[1]=nameptr;
}

int ThingDbEditChangeItemName(short thingId, string changeItemname)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return FALSE;
	char *src;
    StringUtilGetScriptStringPtrEx(changeItemname, &src);

    short length = StringUtilGetLength(src);

    if (!length || (length >= 1024))
        return FALSE;

    VariantStringImpl(src, ArrayRefN(spritedbTarget, 1), 0xa41329fb);
	return TRUE;
}

void ThingDbEditSetItemDesc(short thingId, int *descptr)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return;
	spritedbTarget[2]=descptr;
}

int ThingDbEditChangeItemDesc(short thingId, string changeExplanation)
{
	int *spritedbTarget = ThingDbEditGetSpriteDbArray(thingId);
	
	if (!spritedbTarget)
		return FALSE;
	char *src;
    StringUtilGetScriptStringPtrEx(changeExplanation, &src);

    short length = StringUtilGetLength(src);

    if (!length || (length >= 1024))
        return FALSE;

    VariantStringImpl(src, ArrayRefN(spritedbTarget, 2), 0xa41329fb);
	return TRUE;
}

void NOXLibraryEntryPointFunction()
{
	"export ThingDbEditGetArray";
	"export ThingDbEditGetSpriteDbArray";
	"export ThingDbEditFixColorPotion";
	"export ThingDbEditChangeWorth";
	"export ThingDbEditChangeFriction";
	"export ThingDbEditChangeMass";
	"export ThingDbEditChangeExtentRadius";
	"export ThingDbEditSpriteSelectionRing";
	"export ThingDbEditSpriteLightColor";
	"export ThingDbEditSpriteLightColorRGB";
	"export ThingDbEditLightIntensity";
	"export ThingDbEditSetZOrder";
	"export ThingDbEditEnablePickup";
	"export ThingDbEditDisablePickup";
	"export ThingDbEditSetItemName";
	"export ThingDbEditChangeItemName";
	"export ThingDbEditSetItemDesc";
	"export ThingDbEditChangeItemDesc";
}
