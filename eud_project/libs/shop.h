
#include "define.h"
#include "typecast.h"
#include "unitutil.h"
#include "callmethod.h"
#include "thingdbedit.h"

void ShopUtilItemBasePrivate(int shopunit, int *destptr)
{
	int *ptr = UnitToPtr(shopunit);
	
	destptr[0]=ptr[173];
}

int ShopUtilCheckAlreadyExist(int shopUnit, short thingId)
{
	int *base;
	
	ShopUtilItemBasePrivate(shopUnit, &base);
	if (!base)
		return FALSE;
	int count =base[0];
	base+=4;
	int rep=-1;
	while(++rep<count)
	{
		int *tptr=base[0];
		if (tptr[0]==thingId)
			return TRUE;
		base+=28;
	}
	return FALSE;
}

void ShopUtilFindItem(int shopunit, short thingId, int second, int *destptr)
{
	int *base;
	
	ShopUtilItemBasePrivate(shopunit, &base);
	if (!base)
		return;
	
	int count = base[0];	
	base+=4;
	int rep=-1;
	while (++rep<count)
	{
		int *tptr=base[0];
		if (tptr[0]==thingId)
		{
			if(!second)
			{
				destptr[0]=base;
				break;
			}
			else --second;
		}
		base+=28;
	}	
}

void ShopUtilGetItem(int shopUnit, int index, int *pDestPtr)
{
	if (index&(~0x1f))
		return;
	int *base;
	ShopUtilItemBasePrivate(shopUnit, &base);
	if (!base)
		return;
	int count =base[0];
	if (index>=count)
		return;
	base+=4;
	pDestPtr[0]=base+(index*28);
}

void ShopUtilAllItemCount(int shopunit, int *pCount)
{
    int *base;

    ShopUtilItemBasePrivate(shopunit, &base);
	pCount[0]=base[0];
}

void ShopUtilItemCount(int shopunit, short thingId, int second, int *pCount)
{
    int *ptr = 0;

    ShopUtilFindItem(shopunit, thingId, second, &ptr);
    if (ptr == 0)
        return;

	pCount[0]=ptr[1];
}

void ShopUtilSetItemCount(int shopunit, short thingId, int second, int setTo)
{
    int *ptr = 0;

    ShopUtilFindItem(shopunit, thingId, second, &ptr);
    if (ptr == 0)
        return;
	
	setTo = (--setTo)&0x1f;
	ptr[1]=++setTo;
}

void ShopUtilSetTradePrice(int shopunit, float buyPrice, float sellPrice)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (base == 0)
        return;

	int *priceTable = base + 0x6b4;
	
	priceTable[0]=buyPrice;
	priceTable[1]=sellPrice;
}

void ShopUtilGetTradePrice(int shopunit, float *buyPrice, float *sellPrice)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (base == 0)
        return;

    int *priceTable = base + 0x6b4;
	
    if (ToInt(buyPrice))
		buyPrice[0]=priceTable[0];
    if (ToInt(sellPrice))
		sellPrice[0]=priceTable[1];
}

void ShopUtilAppendItem(int shopunit, short thingId, int itemCount)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (!base)
        return;
	if (base[0]&(~0x1f))
		return;
	
	if (ShopUtilCheckAlreadyExist(shopunit, thingId))
		return;
    
    int *ptr = base + (28*(base[0]++))+4;
	
    int *loadThingdb = ThingDbEditGetArray(thingId);
    
	ptr[0]=loadThingdb;

    itemCount = (--itemCount) &0x1f;
	ptr[1]=++itemCount;
}

// item_property_... 매크로 사용
void ShopUtilAppendItemWithProperties(int shopunit, short thingId, int itemCount, int *item_property1, int *item_property2, int *item_property3, int *item_property4)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (!base)
        return;
	if (base[0]&(~0x1f))
		return;
    
    int *ptr = base + (28*(base[0]++))+4;	
    int *loadThingdb = ThingDbEditGetArray(thingId);
    
	ptr[0]=loadThingdb;

    itemCount = (--itemCount) &0x1f;
	ptr[1]=++itemCount;
	
	if (item_property1) ptr[3]=item_property1[0];
	if (item_property2) ptr[4]=item_property2[0];
	if (item_property3) ptr[5]=item_property3[0];
	if (item_property4) ptr[6]=item_property4[0];
}

void NOXLibraryEntryPointFunction()
{
	"export ShopUtilItemBasePrivate";
	"export ShopUtilCheckAlreadyExist";
	"export ShopUtilFindItem";
	"export ShopUtilGetItem";
	"export ShopUtilAllItemCount";
	"export ShopUtilItemCount";
	"export ShopUtilSetItemCount";
	"export ShopUtilSetTradePrice";
	"export ShopUtilGetTradePrice";
	"export ShopUtilAppendItem";
	"export ShopUtilAppendItemWithProperties";
}
