
#include "typecast.h"
#include "callmethod.h"
#include "stringutil.h"
#include "opcodehelper.h"

int NoxOpcodeCmdLine()
{
    int link;

    if (!link)
    {
        int arr[]={
            0x49E85650, 0x85FFDB62, 0x8B1174C0, 0x8D30FFF0,
            0x30FF0446, 0xCF2C68E8, 0x08C483FF, 0x90C3585E};
        link=&arr;
        FixCallOpcode(link + 2, 0x507250);
        FixCallOpcode(link + 0x14, 0x443c80);
    }
    return link;
}

void CmdLine(string commandMessage, int isShow)
{
    short wCmd[200];
    char *srcPtr = StringUtilGetScriptStringPtr(commandMessage);
    int *pExec = 0x5c3108;
    int *pOld = pExec[0];

    NoxUtf8ToUnicode(srcPtr, &wCmd);
    pExec[0] = NoxOpcodeCmdLine();
    int params[] = {isShow, &wCmd};
    Unused1f(&params);
    pExec[0]=pOld;
}

void LoadmapImpl(int *pDestCode)
{
    int *pCode;

    if (!pCode)
    {
        //E8 4B 62 DB /FF 50 E8 45/ 14 D8 FF 58/ 31 C0 C3 90
        int codes[] = {0xdb624be8, 0x45e850ff, 0x58ffd814, 0x90c3c031};

        FixCallOpcode(&codes, 0x507250);
        FixCallOpcode(&codes + 6, 0x4d2450);
        pCode = &codes;
    }
    pDestCode[0] = pCode;
}

void Loadmap(string mapFileName)
{
    int *builtins=0x5c308c;
    int *pOld = builtins[94];
    int *pCode;

    LoadmapImpl(&pCode);
    builtins[94] = pCode;
    Unused5e(StringUtilGetScriptStringPtr(mapFileName));
    builtins[94] = pOld;
}

int NoxConsolePrintImpl()
{
    int *p;

    if (!p)
    {
        char codes[]={0xE8, 0x4B, 0x62, 0xDB, 0xFF, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xE8, 0x81, 0x4B, 0xCB, 0xFF, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3, 0x90, 0x90, 0x90};
    p=&codes;
        FixCallOpcode(&codes,0x507250);
        FixCallOpcode(&codes+0xa,0x450B90);
    }
    return p;
}

#define CONSOLE_COLOR_BLACK 1
#define CONSOLE_COLOR_GREY 2
#define CONSOLE_COLOR_WHITE 3
#define CONSOLE_COLOR_WHITE_LIGHT 4
#define CONSOLE_COLOR_BROWN 5
#define CONSOLE_COLOR_RED 6
#define CONSOLE_COLOR_PINK 7
#define CONSOLE_COLOR_GREEN 8
#define CONSOLE_COLOR_LIME 10
#define CONSOLE_COLOR_DARKBLUE 11
#define CONSOLE_COLOR_BLUE 12
#define CONSOLE_COLOR_SKYBLUE 13
#define CONSOLE_COLOR_ORANGE 14
#define CONSOLE_COLOR_YELLOW 15
#define CONSOLE_COLOR_PALE_YELLOW 16


//콘솔에 글자를 출력합니다-- 클라이언트 사이드입니다
//msg- 출력할 글자
//color- CONSOLE_COLOR_ 매크로를 사용하세요
void NoxConsolePrint(string msg, char color)
{
    short uMsg[256];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(msg), &uMsg);
    int *builtins=0x5c308c;
    int *pOldExec=builtins[0x1f];
    int pamars[]={color, &uMsg};

    builtins[0x1f]=NoxConsolePrintImpl();
    Unused1f(&pamars);
    builtins[0x1f]=pOldExec;
}

void NOXLibraryEntryPointFunction()
{
    "export NoxOpcodeCmdLine";
    "export CmdLine";
    "export LoadmapImpl";
    "export Loadmap";
    "export NoxConsolePrintImpl";
    "export NoxConsolePrint";
}