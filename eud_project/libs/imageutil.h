
#include "define.h"
#include "opcodehelper.h"
#include "memutil.h"
#include "linked_list.h"
#include "bind.h"

#define IMAGE_FRAME_TABLE_HASH_KEY 31

int initializeDrawTextSprite()
{
    char code[]={
        0x83, 0xEC, 0x20, 0x8B, 0x0D, 0x04, 0xEA, 0x84, 0x00, 0x53, 0x55, 0x56, 0x8B, 0x74, 0x24, 
        0x34, 0x57, 0x8B, 0x3D, 0xAC, 0x3B, 0x85, 0x00, 0x8B, 0x86, 0x20, 0x01, 0x00, 0x00, 0x39, 
        0xC8, 0x72, 0x06, 0x8B, 0x3D, 0xE0, 0xC9, 0x84, 0x00, 0x57, 0xE8, 0xDF, 0x72, 0x00, 0xF2, 
        0x8B, 0x4C, 0x24, 0x40, 0x51, 0xE8, 0x05, 0x72, 0x00, 0xF2, 0x8B, 0x4C, 0x24, 0x3C, 0x8B, 
        0x46, 0x0C, 0x83, 0xC4, 0x08, 0x8B, 0x59, 0x10, 0x8B, 0x11, 0x8B, 0x69, 0x14, 0x29, 0xD8, 
        0x8B, 0x5E, 0x10, 0x01, 0xD0, 0x8B, 0x51, 0x04, 0x8B, 0x4E, 0x70, 0x29, 0xEA, 0x89, 0x44, 
        0x24, 0x10, 0x01, 0xDA, 0x89, 0x54, 0x24, 0x14, 0x8B, 0x4E, 0x6C, 0x8B, 0x54, 0x24, 0x40, 
        0x85, 0xD2, 0x75, 0x05, 0xBA, 0x00, 0xD7, 0x58, 0x00, 0x8B, 0x44, 0x24, 0x14, 0x8B, 0x4C, 
        0x24, 0x10, 0x50, 0x51, 0x52, 0x6A, 0x00, 0xE8, 0x08, 0x25, 0x01, 0xF2, 0x83, 0xC4, 0x10, 
        0xB8, 0x01, 0x00, 0x00, 0x00, 0x5F, 0x5E, 0x5D, 0x5B, 0x83, 0xC4, 0x20, 0xC3
    };
    FixCallOpcode(code + 0x28, 0x434460);
    FixCallOpcode(code + 0x32, 0x434390);
    FixCallOpcode(code + 0x7f, 0x43f6e0);
    return code;
}

//Custom Image Handler //

#define SPRITE_STACK_OFFSET 0x34
int createImageHandler(int *hash, char *ciTable)
{
    int *ptr;// = MemAlloc(1024);

    AllocSmartMemEx(1024, &ptr);

    char code004c7670[] = {
        0x55, 0x8B, 0xEC, 0x8B, 0x55, 0x08, 0x56, 0x31, 0xF6, 0x85, 0xD2, 0x74, 0x15, 0x8A, 
        0x42, 0x0A, 0x83, 0xE0, 0x3F, 0x83, 0xE8, 0x02, 0x83, 0xF8, 0x07, 0x77, 0x07, 0xFF, 
        0x24, 0x85, 0x38, 0x78, 0x4C, 0x00, 0x5E, 0x5D, 0xC3, 0x90
    };
    NoxByteMemCopy(&code004c7670, ptr, sizeof(code004c7670));

    int *code004c76d6 = ptr + 40;
    NoxByteMemCopy(0x4c76d6, code004c76d6, 352);

    char codePixelLoader[]={
        0x8B, 0x44, 0x24, 0x04, 0x31, 0xD2, 0x66, 0x8B, 0x50, 0x08, 0x8B, 0x0D, 0xE8, 0x56, 0x59, 0x00, 0x8B, 0x0C, 0x11, 0x84, 0xC9, 0x74, 0x5C, 
        0x50, 0x8B, 0x00, 0xB9, IMAGE_FRAME_TABLE_HASH_KEY, 0x00, 0x00, 0x00, 0x99, 0xF7, 0xF9, 0x8B, 0x0D, 0xE4, 0x56, 0x59, 0x00, 0x8B, 0x0C, 0x91, 0x58, 0x8B, 0x11, 
        0x85, 0xD2, 0x74, 0x41, 0x39, 0xD0, 0x74, 0x05, 0x8D, 0x49, 0x08, 0xEB, 0xF1, 0x8B, 0x49, 0x04, 0x8B, 0x11, 0x81, 0xFA, 0xFF, 0xFF, 0xFF, 
        0xFF, 0x75, 0x1D, 0x50, 0xFF, 0x71, 0x08, 0xFF, 0x71, 0x04, 0xFF, 0x35, 0xC0, 0x6F, 0x71, 0x00, 0x68, 0xD0, 0xA5, 0x69, 0x00, 0xE8, 0xA1, 
        0xFF, 0xFF, 0xFF, 0x83, 0xC4, 0x10, 0x58, 0x31, 0xC9, 0x83, 0xEC, 0x20, 0x53, 0x55, 0x56, 0x57, 0x50, 0x68, 0x04, 0x7A, 0x4C, 0x00, 0xC3, 
        0x83, 0xEC, 0x20, 0x53, 0x55, 0x56, 0x57, 0x50, 0xFF, 0x15, 0x58, 0x3F, 0x97, 0x00, 0x8B, 0xC8, 0x68, 0x04, 0x7A, 0x4C, 0x00, 0xC3, 0x90
    };
    int *pixelLoader = ptr + 400;
    NoxByteMemCopy(&codePixelLoader, pixelLoader, sizeof(codePixelLoader));
    FixCallOpcode(code004c76d6 + 0x14f, pixelLoader);
    FixCallOpcode(pixelLoader+0x5a, initializeDrawTextSprite());
    SetMemory(pixelLoader+0x0c, &ciTable);
    SetMemory(pixelLoader+0x24, &hash);

    int *pJumptable = pixelLoader + sizeof(codePixelLoader);

    //9A 76 4C 00/ D6 76 4C 00/ D6 76 4C 00/ D6 76 4C 00/ D6 76 4C 00/ 9A 76 4C 00/ B8 76 4C 00
    pJumptable[0] = 0x4c769a;
    // pJumptable[1] = 0x4c76d6;
    pJumptable[2]=0x4c76d6;
    pJumptable[3] = 0x4c76d6;
    pJumptable[4]=0x4c76d6;
    pJumptable[5]=0x4c769a;
    pJumptable[6]=0x4c76b8;
    pJumptable[1]=code004c76d6; //7->1
    int *doJump = ptr + 30;
    doJump[0] = pJumptable;

    char code004ea6e0[] = {
        0xC7, 0x05, 0x60, 0x3F, 0x97, 0x00, 0x70, 0x76, 0x4C, 0x00, 0xC7, 0x05, 0x1C, 
        0x82, 0x59, 0x00, 0xE0, 0xA6, 0x42, 0x00, 0x68, 0xE0, 0xA6, 0x42, 0x00, 0xC3
        };
    char *pCode004ea6e0 = pJumptable + 40;

    NoxByteMemCopy(&code004ea6e0, pCode004ea6e0, sizeof(code004ea6e0));

    int *pHook = 0x973f60;

    pHook[0] = ptr;

    pHook = 0x59821c;
    int *fixrot = &pCode004ea6e0[21];
    fixrot[0]=pHook[0];
    fixrot=&pCode004ea6e0[16];
    fixrot[0]=pHook[0];
    pHook[0] = pCode004ea6e0;
    return ptr;
}

int CreateUseCustomImageSection()
{
    int *ptr;

    AllocSmartMemEx(6720, &ptr);
    NoxDwordMemset(ptr, 6720>>2, 0);
    return ptr;
}

int createImageHashTable(short *hashCount)
{
    int *p, nil[]={0,0,};

    if (p==NULLPTR)
    {
        AllocSmartMemEx(IMAGE_FRAME_TABLE_HASH_KEY*4, &p);
        int r=IMAGE_FRAME_TABLE_HASH_KEY;
        int hcount;

        while (--r>=0)
        {
            hcount=hashCount[r];
            if (hcount)
            {
                AllocSmartMemEx((hcount*8)+8, &p[r]);
                int *table=p[r];
                table[hcount*2]=0;
                table[hcount*2+1]=0;
            }
            else                p[r]=nil;
            hashCount[r]=0;
        }
    }
    return p;
}

#include "imageTile.h"

void DoImageDataExchange(int *vector)
{
    int *count=vector, *elem=&vector[1];
    short hashCount[IMAGE_FRAME_TABLE_HASH_KEY];
    int r=count[0];

    while (--r>=0)
        hashCount[OperatorGetWordMemory( elem[r*2] )%IMAGE_FRAME_TABLE_HASH_KEY]+=1;

    int *hash;

    if (hash) FreeSmartMemEx(hash);
    hash=createImageHashTable(hashCount); //hash count set to zero here

    int pic, *table;
    char *ciEnabler= CreateUseCustomImageSection();
    r=count[0];

    while (--r>=0)
    {
        pic=(OperatorGetWordMemory(elem[r*2]))%IMAGE_FRAME_TABLE_HASH_KEY;
        table=hash[pic];
        table[hashCount[pic]++]=elem[r*2];
        table[hashCount[pic]++]=elem[r*2+1];
        ciEnabler[OperatorGetWordMemory( elem[r*2]+8)]=1;
    }
    createImageHandler(hash, ciEnabler);
    InitializeImageTileDraw(hash,ciEnabler);
}

int CreateImageVector(int size)
{
    int *ptr;

    AllocSmartMemEx((size*8)+4, &ptr);
    ptr[0]=0;
    return ptr;
}

void AppendImageFrame(int *vector, char *imgFrame, int imgId)
{
    int *cur = vector, *elem=&vector[1];
    int pic=(cur[0]++)*2;

    elem[pic]=GetMemory(0x694864)+(12*imgId);
    elem[++pic]=imgFrame;
}

#define TEXT_DATA_SIG 0
#define TEXT_DATA_COLOR 1
#define TEXT_DATA_STRING 2
#define TEXT_DATA_MAX 3

void AppendTextSprite(int *vector, int fontColor, short *wString, int imgId)
{
    int *cur = vector, *elem=&vector[1];
    int pic=(cur[0]++)*2;
    int *textData;

    AllocSmartMemEx(TEXT_DATA_MAX*4, &textData);
    textData[TEXT_DATA_SIG]=-1;
    textData[TEXT_DATA_STRING]=wString;
    textData[TEXT_DATA_COLOR]=fontColor;

    elem[pic]=GetMemory(0x694864)+(12*imgId);
    elem[++pic]=textData;
}

////end

void GrpTable(int *pDest)
{
    int table[32];

    if (pDest)
        pDest[0] = &table;
}

int GetGrpStream(int index)
{
    int *pTable;

    GrpTable(&pTable);
    return pTable[index];
}

int LoadImageResource(int *stream)
{
    int stx=stream[0];

    if (stx!=0xdeadface)
        return FALSE;
    int headerLength=stream[1];
    int grpCount=stream[2]&0xff;

    if (grpCount & (~0x1f))
        return FALSE;

    int *grpStream=stream+(stream[2]>>8);
    int *sizeTable=stream+headerLength;
    int *pTable;

    GrpTable(&pTable);
    int rep=-1;
    int accumulate=grpStream;

    while((++rep) < grpCount)
    {
        pTable[rep] = accumulate;
        accumulate+=sizeTable[rep];
    }
    return TRUE;
}

void ImageUtilGetPtrFromID(int imgId, int *pDestTable)
{
    int *imgTableOff = 0x694864;

    pDestTable[0] = imgTableOff[0] + (12*imgId);
}

void writeNone(char *ptr, char *dest, int *p)
{ }

void writeType1(char *srcStream, char *pDest, int *destCur)
{
    pDest[destCur[0]++]=1;
    pDest[destCur[0]++]=srcStream[1];
}

void writeType2(char *srcStream, char *pDest, int *destCur)
{
    char count = srcStream[1];
    pDest[destCur[0]++]=2;
    pDest[destCur[0]++]=count;
    short *sDest = &pDest[destCur[0]];
    short *pixels = &srcStream[2];

    int iCount= count;
    int wrt=0;

    destCur[0]+=(iCount*2);

    while (--iCount>=0)
        sDest[wrt++]=pixels[iCount];
}

void invokeWriteType(int fn, char *ptr, char *dest, int *cur)
{
    Bind(fn, &fn+4);
}

void writeInverseGrpFromList(char *pDest, int *indexes, int nb, int *destCur)
{
    int fn[]={writeNone, writeType1, writeType2, writeNone, };
    char *ptr;

    while (--nb>=0)
    {
        ptr=indexes[nb];
        invokeWriteType(fn[ptr[0]], ptr, pDest, destCur);
    }
}

// int LRInversionGRP(char *pStream, int size)
// {
//     int *lpStream = pStream;
//     int width = lpStream[0];
//     int height = lpStream[1];

//     if (width>1024)
//         return NULLPTR;
//     if (height>1024)
//         return NULLPTR;
        
//     if (pStream[0x10]!=3)
//         return NULLPTR;

//     int cur=0x11, destCur=0x11;
//     int accumulate=0, c;
//     char *dest;

//     AllocSmartMemEx(size, &dest);
//     int indexes[1024],count,nb=0;

//     NoxByteMemCopy(pStream, dest, cur);
//     while (cur < size)
//     {
//         indexes[nb++]=pStream+cur;
//         c=pStream[cur]; count=pStream[cur+1];

//         cur+=2;
//         accumulate+=count;
//         if (c==2)
//             cur+=(count*2);

//         if (accumulate==width)
//         {
//             writeInverseGrpFromList(dest, indexes, nb, &destCur);
//             accumulate=0;
//             nb=0;
//         }
//     }
//     return dest;
// }

int LRInversionGRP(char *p, int size)
{
	int *lp=p;
	int w=lp[0];
	int cur=0x11, destc=0x11;
	char *dest;
    if (p[0x10]!=3)
        return NULLPTR;
	AllocSmartMemEx(size+16, &dest);
	NoxByteMemCopy(p,dest,cur);
	char buff[4096],unused[8];
	int c,bc=sizeof(buff),count=0,rep;
	while (cur<size){
		c=p[cur++];
		if (c==1){
            c=p[cur++];
			count+=c;
			buff[--bc]=c;
			buff[--bc]=1;
		}
		else if (c==2){
			c=p[cur++];
            rep=c;
			count+=rep;
			while (--rep>=0){
				buff[--bc]=p[cur+1];
				buff[--bc]=p[cur];
				cur+=2;
			}
			buff[--bc]=c;
			buff[--bc]=2;
		}
		else
			break;
		if (count==w)
		{
			count=sizeof(buff)-bc;
			DwordCopy(&buff[bc], &dest[destc], ( count>>2 )+1);
			destc+=count;
            count=0;
			bc=sizeof(buff);
		}
	}
    dest[destc++]=0;
    dest[destc++]=0;
	return dest;
}

int GetInversionGrpStreamFromFunction(int fn)
{
    char *stream = GetScrCodeField(fn);

    return LRInversionGRP(&stream[4], GetMemory(stream));
}
