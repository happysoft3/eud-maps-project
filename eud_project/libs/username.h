
#include "typecast.h"
#include "unitutil.h"
#include "callmethod.h"
#include "stringutil.h"

int g_playerNick;

/*
int GetMemory(int a){}
void SetMemory(int a, int b){}
int UnitToPtr(int a){}
string ToStr(int a){}
int GetScrDataField(int functionName){}
int GetScrCodeField(int functionName){}
int NoxUnicodeToUtf8(int src, int destPtr){}
string ReadStringAddress(int t){}
*/

string PlayerIngameNick(int sUnit)
{
    int destPtr;  //0
	int userNickArr[352];
    
    int ptr = UnitToPtr(sUnit), plrIndex, srcPtr;
    int link = GetScrDataField(-g_playerNick) + 4;
    
    if (ptr)
    {
        if (GetMemory(ptr + 0x08) & 4)
        {
            plrIndex = GetMemory(GetMemory(GetMemory(ptr + 0x2ec) + 0x114) + 0x810);
			destPtr = (link + 4) + (plrIndex * 44);
            srcPtr = GetMemory(GetMemory(ptr + 0x2ec) + 0x114) + 0x889;
            NoxUnicodeToUtf8(srcPtr, destPtr);
            return ReadStringAddress(link);
        }
    }
    return ReadStringAddress(0x587a50);
}

void NOXLibraryEntryPointFunction()
{
	"export PlayerIngameNick";
	
    g_playerNick = PlayerIngameNick;
}


