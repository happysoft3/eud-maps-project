
#include "../define.h"
#include "../stringutil.h"
#include "../buff.h"
#include "dunmir2_bintable.h"
#include "../npc.h"

void BomberSetMonsterCollide(int bombUnit)
{
    int ptr = UnitToPtr(bombUnit);

    if (ptr)
        SetMemory(ptr + 0x2b8, 0x4e83b0);
}

int FieldMobSwordsman(int sUnit)
{
        //"Swordsman"
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1346), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 125);
    return mob;
}

int FieldMobBat(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1357) /*"Bat"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 64);
    return mob;
}

int FieldMobGreenFrog(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1313) /*"GreenFrog"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 64);
    UnitLinkBinScript(mob, GreenFrogBinTable());
    return mob;
}

int FieldMobArcher(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1345)/*"Archer"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 98);
    return mob;
}

int FieldMobWasp(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1331)/* "Wasp"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 64);
    return mob;
}

int FieldMobGiantLeech(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1358) /*"GiantLeech"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 120);
    return mob;
}

int FieldMobUrchin(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1339)/* "Urchin"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 98);
    return mob;
}

int FieldMobBomber(int sUnit)
{
    int bombers[] = {1348, 1349,1350,1351};
    int mob = CreateObjectAt(StringUtilFindUnitNameById( bombers[Random(0, 3)]), GetObjectX(sUnit), GetObjectY(sUnit));

    UnitLinkBinScript(mob, BomberGreenBinTable());
    BomberSetMonsterCollide(mob);
    SetUnitMaxHealth(mob, 135);
    return mob;
}

int FieldMobMystic(int sUnit)
{
    int mob = CreateObjectAt(/*"Wizard"*/StringUtilFindUnitNameById(1327), GetObjectX(sUnit), GetObjectY(sUnit));

    Enchant(mob, EnchantList(ENCHANT_ANCHORED), 0.0);
    SetUnitMaxHealth(mob, 175);
    return mob;
}

int FieldMobBlackBear(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1366)/* "BlackBear"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 275);
    return mob;
}

int FieldMobBlackWolf(int sUnit)
{
    int mob = CreateObjectAt(/*"BlackWolf"*/StringUtilFindUnitNameById(1368), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 225);
    return mob;
}

int FieldMobBear(int sUnit)
{
    int mob = CreateObjectAt(/*"Bear"*/StringUtilFindUnitNameById(1365), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 325);
    return mob;
}

int FieldMobScopion(int sUnit)
{
    int mob = CreateObjectAt(/*"Scorpion"*/StringUtilFindUnitNameById(1373), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 260);
    return mob;
}

int FieldMobShade(int sUnit)
{
    int mob = CreateObjectAt(/*"Shade"*/StringUtilFindUnitNameById(1362), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 170);
    return mob;
}

int FieldMobSpider(int sUnit)
{
    int mob = CreateObjectAt(/*"BlackWidow"*/StringUtilFindUnitNameById(1392), GetObjectX(sUnit), GetObjectY(sUnit));

    UnitLinkBinScript(mob, BlackWidowBinTable());
    SetUnitMaxHealth(mob, 170);
    SetUnitVoice(mob, 33);
    return mob;
}

int FieldMobSkeleton(int sUnit)
{
    int mob = CreateObjectAt(/*"Skeleton"*/StringUtilFindUnitNameById(1314), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 250);
    return mob;
}

int FieldMobSkeletonLord(int sUnit)
{
    int mob = CreateObjectAt(/*"SkeletonLord"*/StringUtilFindUnitNameById(1315), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 306);
    return mob;
}

int FieldMobGargoyle(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1317) /*"EvilCherub"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 108);
    return mob;
}

void ZombieWhenDead(){}

int FieldMobZombie(int sUnit)
{
    int mob = CreateObjectAt(/*"Zombie"*/StringUtilFindUnitNameById(1360), GetObjectX(sUnit), GetObjectY(sUnit));

    ObjectOff(mob);
    SetUnitMaxHealth(mob, 240);
    SetCallback(mob, 5, ZombieWhenDead);
    return mob;
}

int FieldMobVileZombie(int sUnit)
{
    int mob = CreateObjectAt(/*"VileZombie"*/StringUtilFindUnitNameById(1361), GetObjectX(sUnit), GetObjectY(sUnit));

    ObjectOff(mob);
    SetUnitMaxHealth(mob, 325);
    SetUnitSpeed(mob, 2.4);
    SetCallback(mob, 5, ZombieWhenDead);
    return mob;
}

int FieldMobGhost(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1325)/* "Ghost" */
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 98);
    return mob;
}

int FieldMobLich(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1342) //"Lich"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 360);
    SetUnitStatus(mob, GetUnitStatus(mob) ^ 0x20);
    UnitLinkBinScript(mob, LichLordBinTable());
    return mob;
}

int FieldMobLichLord(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1342)//"LichLord"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 450);
    SetUnitStatus(mob, GetUnitStatus(mob) ^ 0x20);
    UnitLinkBinScript(mob, LichLord2BinTable());
    return mob;
}

int FieldMobMecaGolem(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1318) //"MechanicalGolem"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 800);
    return mob;
}

int FieldMobNecromancer(int sUnit)
{
    int unit = CreateObjectAt(StringUtilFindUnitNameById(1391) //"Necromancer"
    , GetObjectX(sUnit), GetObjectY(sUnit));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 325);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec +(24*4) //GetSpellNumber("SPELL_LIGHTNING")
        , 0x40000000);
		SetMemory(uec +(51*4)// GetSpellNumber("SPELL_SHIELD")
        , 0x10000000);
        SetMemory(uec +(72*4) //GetSpellNumber("SPELL_SLOW")
        , 0x20000000);
		SetMemory(uec +(39*4) //GetSpellNumber("SPELL_INVISIBILITY")
        , 0x10000000);
		SetMemory(uec +(27*4) //GetSpellNumber("SPELL_FIREBALL")
        , 0x40000000);
        SetMemory(uec +(38*4) //GetSpellNumber("SPELL_INVERSION")
        , 0x8000000);
        SetMemory(uec +(13*4) //GetSpellNumber("SPELL_COUNTERSPELL")
        , 0x8000000);
    }
    return unit;
}

int FieldMobFireFairy(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1370)//"FireSprite"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 130);
    SetUnitStatus(mob, GetUnitStatus(mob) ^ 0x10000);
    UnitLinkBinScript(mob, FireSpriteBinTable());
    return mob;
}

int FieldMobJandor(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1387)//"AirshipCaptain"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 325);
    UnitLinkBinScript(mob, AirshipCaptainBinTable());
    return mob;
}

int FieldMobEmberDemon(int sUnit)
{
    // string mName[] = {"EmberDemon", "MeleeDemon"};
    int ids[]={1337,1338};
    int mob = CreateObjectAt(StringUtilFindUnitNameById( ids[Random(0, 1)] ),    GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 160);
    return mob;
}

int FieldMobWhiteWizard(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1332)// "WizardWhite"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    Enchant(mob, EnchantList(ENCHANT_ANCHORED), 0.0);
    SetUnitMaxHealth(mob, 275);
    return mob;
}

int FieldMobHecubahWithOrb(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1384) //"HecubahWithOrb"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 375);
    SetUnitStatus(mob, GetUnitStatus(mob) ^ 0x10000);
    UnitLinkBinScript(mob, HecubahWithOrbBinTable());
    return mob;
}

int FieldMobOgre(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1390) //"OgreBrute"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 295);
    return mob;
}

int FieldMobOgreAxe(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1336) //"GruntAxe"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 260);
    return mob;
}

int FieldMobOgreLord(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1389) //"OgreWarlord"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 325);
    return mob;
}

int FieldMobGoon(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1363) //"Goon"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 225);
    if (!IsMonsterPoisonImmune(mob))
        SetMonsterPoisonImmune(mob);
    UnitLinkBinScript(mob, GoonBinTable());
    return mob;
}

int FieldMobFemale(int sUnit)
{
    int mob = CreateSingleColorMaidenAt(Random(0, 255), Random(0, 255), Random(0, 255), GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 325);
    if (!IsMonsterPoisonImmune(mob))
        SetMonsterPoisonImmune(mob);
    SetUnitVoice(mob, 7);
    UnitLinkBinScript(mob, MaidenBinTable());

    return mob;
}

int FieldMobBeast(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1388) //"WeirdlingBeast"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 240);
    UnitLinkBinScript(mob, WeirdlingBeastBinTable());
    return mob;
}

int FieldMobStoneGolem(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1324) //"StoneGolem"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 700);
    return mob;
}

int FieldMobDryad(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1335)// "WizardGreen"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    Enchant(mob, EnchantList(ENCHANT_ANCHORED), 0.0);
    SetUnitMaxHealth(mob, 225);
    return mob;
}

int FieldMobShaman(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1340)//"UrchinShaman"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 128);
    return mob;
}

int FieldMobHorrendous(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1386)//"Horrendous"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 420);
    SetUnitSpeed(mob, 1.4);
    return mob;
}

int FieldMobDemon(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1347)//"Demon"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 500);
    return mob;
}

int FieldMobMimic(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1372)//"Mimic"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 600);
    return mob;
}

int FieldMobBeholder(int sUnit)
{
    int mob = CreateObjectAt( StringUtilFindUnitNameById(1382)//"Beholder"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    Enchant(mob, EnchantList(ENCHANT_ANCHORED), 0.0);
    SetUnitMaxHealth(mob, 400);
    return mob;
}

int FieldMobSmallWhiteSpider(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1356) //"SmallAlbinoSpider"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 75);
    SetUnitSpeed(mob, 1.1);
    return mob;
}

int FieldMobWhiteWolf(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1367)// "WhiteWolf"
    , GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 175);
    return mob;
}

int FieldMobAlbinoSpider(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1355) /*"AlbinoSpider"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 160);
    return mob;
}

int FieldMobMecaFlier(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1316)/* "FlyingGolem"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 75);
    return mob;
}

int FieldMobImp(int sUnit)
{
    int mob = CreateObjectAt( StringUtilFindUnitNameById(1354)/*"SmallSpider"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 50);
    SetUnitSpeed(mob, 1.3);
    return mob;
}

int FieldMobSmallSpider(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1356)/* "SmallAlbinoSpider"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 75);
    SetUnitSpeed(mob, 1.1);
    return mob;
}

int FieldMobWolf(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1369)/*"Wolf"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 140);
    return mob;
}

int FieldMobHecubah(int sUnit)
{
    int mob = CreateObjectAt(StringUtilFindUnitNameById(1383)/*"Hecubah"*/, GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitStatus(mob, GetUnitStatus(mob) ^ 0x20);
    UnitLinkBinScript(mob, HecubahBinTable());
    SetUnitMaxHealth(mob, 550);
    return mob;
}

int FieldMobTalkingSkull(int sUnit)
{
    int mob = CreateObjectAt(/*"TalkingSkull"*/StringUtilFindUnitNameById(2303), GetObjectX(sUnit), GetObjectY(sUnit));

    Enchant(mob, EnchantList(ENCHANT_VAMPIRISM), 0.0);
    SetUnitSpeed(mob, 2.7);
    UnitLinkBinScript(mob, TalkingSkullBinTable());
    SetUnitMaxHealth(mob, 240);
    return mob;
}

int FieldMobFireballWiz(int sUnit)
{
    int mob = CreateObjectAt(/*"StrongWizardWhite"*/StringUtilFindUnitNameById(1333), GetObjectX(sUnit), GetObjectY(sUnit));

    UnitLinkBinScript(mob, StrongWizardWhiteBinTable());
    SetUnitMaxHealth(mob, 260);
    return mob;
}

void StrVictory()
{
	int arr[13];
	string name = StringUtilFindUnitNameById(949); // "ManaBombOrb";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
		drawStrVictory(arr[i++], name);
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(108);
		pos_y = GetWaypointY(108);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 108);
		if (count % 38 == 37)
			MoveWaypoint(108, GetWaypointX(108) - 74.000000, GetWaypointY(108) + 2.000000);
		else
			MoveWaypoint(108, GetWaypointX(108) + 2.000000, GetWaypointY(108));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(108, pos_x, pos_y);
	}
}

void NOXLibraryEntryPointFunction()
{
    "export BomberSetMonsterCollide";
"export FieldMobSwordsman";
"export ZombieWhenDead";
"export FieldMobBat";
"export FieldMobGreenFrog";
"export FieldMobArcher";
"export FieldMobWasp";
"export FieldMobGiantLeech";
"export FieldMobUrchin";
"export FieldMobBomber";
"export FieldMobMystic";
"export FieldMobBlackBear";
"export FieldMobBlackWolf";
"export FieldMobBear";
"export FieldMobScopion";
"export FieldMobShade";
"export FieldMobSpider";
"export FieldMobSkeleton";
"export FieldMobSkeletonLord";
"export FieldMobGargoyle";
"export FieldMobZombie";
"export FieldMobVileZombie";
"export FieldMobGhost";
"export FieldMobLich";
"export FieldMobLichLord";
"export FieldMobMecaGolem";
"export FieldMobNecromancer";
"export FieldMobFireFairy";
"export FieldMobJandor";
"export FieldMobEmberDemon";
"export FieldMobWhiteWizard";
"export FieldMobHecubahWithOrb";
"export FieldMobOgre";
"export FieldMobOgreAxe";
"export FieldMobOgreLord";
"export FieldMobGoon";
"export FieldMobFemale";
"export FieldMobBeast";
"export FieldMobStoneGolem";
"export FieldMobDryad";
"export FieldMobShaman";
"export FieldMobHorrendous";
"export FieldMobDemon";
"export FieldMobMimic";
"export FieldMobBeholder";
"export FieldMobSmallWhiteSpider";
"export FieldMobWhiteWolf";
"export FieldMobAlbinoSpider";
"export FieldMobMecaFlier";
"export FieldMobImp";
"export FieldMobSmallSpider";
"export FieldMobWolf";
"export FieldMobHecubah";
"export FieldMobTalkingSkull";
"export FieldMobFireballWiz";
"export StrVictory";
"export drawStrVictory";
}