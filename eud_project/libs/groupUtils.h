
#include "define.h"
#include "bind.h"

void wallsingleOpen(int x,int y)
{
    WallOpen(Wall( x,y));
}
void wallsingleClose(int x,int y)
{
    WallClose(Wall( x,y));
}
void wallsingleBreak(int x,int y)
{
    WallBreak(Wall( x,y));
}
void wallsingleToggle(int x,int y)
{
    WallToggle(Wall( x,y));
}
void wallCallbackStorage(int *get, int set)
{
    int cb;

    if (get){get[0]=cb; return;}
    cb=set;
}
void wallsingleDoCallback(int x,int y)
{
    int cb,wall=Wall(x,y);

    wallCallbackStorage(&cb,0);
    if (cb!=0)
        Bind(cb,&wall);
}

void wallGroupWorkPrivate(char *pGroup, int action)
{
    if (!pGroup)
        return;

    int actions[]={wallsingleOpen,wallsingleClose,wallsingleBreak,wallsingleToggle,wallsingleDoCallback,};
    int *members=&pGroup[84];
    while (members[0])
    {
        members=members[0];
        Bind(actions[action], &members[0]);
        members=&members[2];
    }
}

void RegistWallGroupCallback(int wallArgFunction)
{
    wallCallbackStorage(0,wallArgFunction);
}

#define WALL_GROUP_ACTION_OPEN 0
#define WALL_GROUP_ACTION_CLOSE 1
#define WALL_GROUP_ACTION_BREAK 2
#define WALL_GROUP_ACTION_TOGGLE 3
#define WALL_GROUP_ACTION_CALLBACK 4

void WallGroupSetting(int groupId, int wallGroupActionType)
{
    int *pGroups = 0x83c890;
    char *cur =pGroups[0];
    int *id;

    while (cur)
    {
        id = &cur[4];
        if (groupId==id[0])
        {
            if (cur[0]!=2)
                cur=NULLPTR;
            break;
        }
        id=&cur[88];
        cur=id[0];
    }
    wallGroupWorkPrivate(cur,wallGroupActionType);
}

