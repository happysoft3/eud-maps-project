
#include "define.h"
#include "stringutil.h"

string MapVersionInfo()
{
    char ver[16];

    StringUtilCopy(0x974ac0, &ver);
    return ReadStringAddressEx(&ver);
}

string GetMapInfoAuthor()
{
    char aut[64];

    StringUtilCopy(0x974ad0, &aut);
    return ReadStringAddressEx(&aut);
}

string GetMapTitle()
{
    char title[64];

    StringUtilCopy(0x974880, &title);
    return ReadStringAddressEx(&title);
}

string GetMapExplanation()
{
    char content[512];

    StringUtilCopy(0x9748c0, &content);
    return ReadStringAddressEx(&content);
}

string GetMapReleaseVersion()
{
    char ver[16];
    
    StringUtilCopy( 0x974ac0, &ver);
    return ReadStringAddressEx(&ver);
}

string GetMapAuthorEmail()
{
    char email[192];

    StringUtilCopy(0x974b10, &email);
    return ReadStringAddressEx(&email);
}

string GetMapSecondaryAuthor()
{
    char partner[64];

    StringUtilCopy( 0x974bd0, &partner);
    return ReadStringAddressEx(&partner);
}

string GetMapPartnerEmail()
{
    char email[192];

    StringUtilCopy(0x974c10, &email);
    return ReadStringAddressEx(&email);
}

string GetMapCopyright()
{
    char auth[128];

    StringUtilCopy(0x974d50, &auth);
    return ReadStringAddressEx(&auth);
}

string GetMapReleaseDate()
{
    char date[32];

    StringUtilCopy(0x974dd0, &date);
    return ReadStringAddressEx(&date);
}

void GetMapRecommendedPlayerMinMax(int *min, int *max)
{
    char *p = 0x974df4;

    min[0] = p[0];
    max[0] = p[1];
}

int GetMapFlagValue()
{
    int *p=0x974df0;

    return p[0];
}

//미니맵의 활성화 상태와 배율값을 얻습니다
void GetMinimapInfo(int *pEnabled, int *pScope)
{
    if (pEnabled)
        pEnabled[0]=GetMemory(0x6e007c);
    if (pScope)
        pScope[0]=GetMemory(0x5ab6f0);
}

//미니맵의 활성화 상태와 배율값을 변경합니다((최소값 500, 최대값 4000, 작을수록 미니맵 범위가 좁아짐)
//디폴트 값- pk배율- 2300, 싱글플레이 배율- 1100), 이 값은 초기화 안됨!
void SetMinimapInfo(int enabled, int scope)
{
    SetMemory(0x6e007c, enabled);
    if (scope<500) return;
    if (scope>4000) return;
    SetMemory(0x5ab6f0, scope);
}
