
#include "memutil.h"

int m_memListClearPreviousList;

/*
struct dmemstruct
{
	void *nextPtr;
	void *allocPtr;
	int order;
};
*/
/*
int GetMemory(int a){}
void SetMemory(int a, int b){}

void MemFree(int ptr){}
int MemAlloc(int size){}
*/

int MemListBaseOffset(int plus)
{
	return 0x59ac00 + (plus<<2);
}

int MemListBaseOffsetCondition()
{
	return 0x20726f6f;
}

void MemListClearPreviousList()
{
	int dmem = GetMemory(MemListBaseOffset(1)), cur;
	int link = GetMemory(GetMemory(0x75ae28) + ((0x30 * (-m_memListClearPreviousList)) + 0x1c));
	
	while (dmem)
	{
		SetMemory(link + 4, dmem);			//cur = dmem
		SetMemory(link, GetMemory(dmem));	//dmem = next
		if (GetMemory(4 + cur))
			MemFree(GetMemory(4 + cur));
		MemFree(cur);
	}
}

//메모리 리스트 초기화 작업입니다
void MemListInitialize()
{
	if (GetMemory(MemListBaseOffset(0)) == MemListBaseOffsetCondition())
	{
		SetMemory(MemListBaseOffset(1), 0);
	}
	if (GetMemory(MemListBaseOffset(0)) ^ GetMemory(0x7520d4))
	{
		SetMemory(MemListBaseOffset(0), GetMemory(0x7520d4));
		if (GetMemory(MemListBaseOffset(1)))
		{
			MemListClearPreviousList();
			SetMemory(MemListBaseOffset(1), 0);
		}
	}
	
}

int MemListGetElementCount()
{
	if (GetMemory(MemListBaseOffset(0)) ^ GetMemory(0x7520d4))
		return 0;
	
	int tail = GetMemory(MemListBaseOffset(1));
	
	if (tail)
	{
		return GetMemory(tail + 8);
	}
	return 0;
}

//할당된 메모리를 할당 메모리 리스트에 의해 관리되도록 합니다
//이렇게 되면 방을 다시 만든 후 이 모듈이 들어간 지도를 로딩했을 때 기존 메모리는 모두 파괴됩니다
int MemListPushBack(int ptr)
{
	int createdNode = MemAlloc(12);
	
	if (createdNode == 0)
		return false;
	SetMemory(createdNode, GetMemory(MemListBaseOffset(1)));	//createdNode->nextPtr = latest
	SetMemory(createdNode + 4, ptr);
	if (GetMemory(MemListBaseOffset(1)) == 0)
		SetMemory(createdNode + 8, 1);
	else
		SetMemory(createdNode + 8, GetMemory(GetMemory(MemListBaseOffset(1)) + 8) + 1);
	SetMemory(MemListBaseOffset(1), createdNode);
	return true;
}

void NOXLibraryEntryPointFunction()
{
	"export MemListBaseOffset";
	"export MemListBaseOffsetCondition";
	"export MemListClearPreviousList";
	"export needinit MemListInitialize";
	"export MemListGetElementCount";
	"export MemListPushBack";
	
	m_memListClearPreviousList = MemListClearPreviousList;
}