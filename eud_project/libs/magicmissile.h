
#include "typecast.h"
#include "unitutil.h"
#include "memutil.h"
#include "stringutil.h"

/*
int GetMemory(int a){}
void SetMemory(int a, int b){}
string ToStr(int a){}
int UnitToPtr(int a){}
int CreateObjectAt(string a, float b, float c){}
int ToInt(float a){}
int MemAlloc(int a){}								//memutil.h
//string ReadStringAddress(int t){}
string StringUtilFindUnitNameById(int thingId){}
*/

//커스텀 마법미사일을 생성합니다
//misName: 미사일 유닛 이름입니다. 미사일 타입이 와야합니다
//owner: 소유자입니다
//speed: 추진속도 값입니다. 0.0 인 경우 기본값 3.3을 사용합니다
//xVect: 소유자 기준 x벡터값 입니다
//yVect: 소유자 기준 y벡터값 입니다
int MagicMissileUtilSummon(string misName, int owner, float speed, float xVect, float yVect)
{
    int mis = CreateObjectAt(misName, GetObjectX(owner) + xVect, GetObjectY(owner) + yVect);
    int ptr = GetMemory(0x750710), ecPtr;
	int target = CreateObjectAt(StringUtilFindUnitNameById(1399), GetObjectX(mis) + xVect, GetObjectY(mis) + yVect);
	int casterPtr = GetMemory(0x750710);

    DeleteObjectTimer(target, 3);
    if (ptr && target)
    {
        SetOwner(owner, mis);
		if (ToInt(speed) == 0)
			speed = 3.3;
        SetMemory(ptr + 0x220, ToInt(speed)); //Speed
        SetMemory(ptr + 0x224, 0); //accel
        ecPtr = MemAlloc(20);
        SetMemory(ecPtr, UnitToPtr(owner));
        SetMemory(ecPtr + 4, casterPtr);
        SetMemory(ptr + 0x2ec, ecPtr);
        SetMemory(ptr + 0x2e8, 5488032);
    }
    return mis;
}

void NOXLibraryEntryPointFunction()
{
	"export MagicMissileUtilSummon";
}