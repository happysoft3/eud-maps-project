
#include "define.h"
#include "array.h"
#include "unitutil.h"
#include "stringutil.h"
#include "callmethod.h"
#include "memutil.h"

#define HASHNODE_KEY 0
#define HASHNODE_DATA 1
#define HASHNODE_NEXT 2
#define HASHNODE_MAX 3

#define HASH_OBJECT_COUNT 0
#define HASH_OBJECT_INSTANCE 1
#define HASH_OBJECT_MAX 2

void HashNewNode(int data, int key, int *pGet)
{
	// int *ptr = UnitToPtr(CreateObjectAt(StringUtilFindUnitNameById(1), 100.0, 100.0));
	int m=0;
	AllocSmartMemEx(HASHNODE_MAX*4, &m);
	int *p=m;

	p[HASHNODE_KEY]=key;
	p[HASHNODE_DATA]=data;
	p[HASHNODE_NEXT]=0;
	if (pGet)
		pGet[0]=p;
}

int HashNodeCompareKey(int *pNode, int key, int *pValue)
{
	int compare =pNode[HASHNODE_KEY]==key;
	
	if (compare && pValue)
		pValue[0]=pNode[HASHNODE_DATA];
	return compare;
}

void HashKeylogic(int *hashInstance, int key, int *realKey)
{
	int t = key% hashInstance[HASH_OBJECT_COUNT];

	if (t<0)
		t=-t;
	realKey[0]=t;
}

void HashPushback(int *hashInstance, int key, int data)
{
	int *node;
	
	HashNewNode(data, key, &node);
	
	int realkey;
	HashKeylogic(hashInstance, key, &realkey);
	int *head = ArrayRefN(hashInstance[HASH_OBJECT_INSTANCE], realkey);
	
	if (head[0])
		node[HASHNODE_NEXT]=head[0];
	head[0]=node;
}

void HashNodeRemove(int *pHead, int *pNode, int *prev)
{
	int pNext=pNode[HASHNODE_NEXT];
	
	if (pHead[0]==pNode)
		pHead[0]= pNext;
	else
		prev[HASHNODE_NEXT]=pNext;
	FreeSmartMemEx(pNode);
}

int HashFind(int *head, int key, int *pDest, int isDelete)
{
	int *cur = head[0];
	int prev = 0;
	
	while (cur)
	{
		if (HashNodeCompareKey(cur, key, pDest))
		{
			if (isDelete)
				HashNodeRemove(head, cur, prev);
			return TRUE;
		}
		prev=cur;
		cur=cur[HASHNODE_NEXT];
	}
	return FALSE;
}

int HashGet(int *hashInstance, int key, int *pDest, int bDelete)
{
	int realkey;
	
	HashKeylogic(hashInstance, key, &realkey);
	
	int pHead = ArrayRefN(hashInstance[HASH_OBJECT_INSTANCE], realkey);
	
	return HashFind(pHead, key, pDest, bDelete);
}

//can override
int HashTableCount()
{
	return 32;
}

void HashCreateInstance(int *pDest)
{
	int hashCount = HashTableCount();
	int pInstance = 0; // MemAlloc(hashCount*4);

	AllocSmartMemEx(hashCount*4, &pInstance);	
	NoxDwordMemset(pInstance, hashCount, 0);
	int hash;

	AllocSmartMemEx(HASH_OBJECT_MAX*4, &hash);
	int *ptr=hash;

	ptr[HASH_OBJECT_COUNT]=hashCount;
	ptr[HASH_OBJECT_INSTANCE]=pInstance;
	pDest[0]=ptr;
}

void HashNodeEraseAll(int *node)
{
	int *del = node;

	while (node)
	{
		del=node;
		node=node[HASHNODE_NEXT];
		FreeSmartMemEx(del);
	}
}

void HashDeleteInstance(int *hashInstance)
{
	if (hashInstance)
	{
		int hashCount = HashTableCount();
		int *table=hashInstance[HASH_OBJECT_INSTANCE];

		while (--hashCount>=0)
			HashNodeEraseAll(table[hashCount]);
		FreeSmartMemEx(table);
		FreeSmartMemEx(hashInstance);
	}
}

#undef HASHNODE_KEY
#undef HASHNODE_DATA
#undef HASHNODE_NEXT
#undef HASHNODE_MAX

#undef HASH_OBJECT_COUNT 
#undef HASH_OBJECT_INSTANCE 
#undef HASH_OBJECT_MAX 


