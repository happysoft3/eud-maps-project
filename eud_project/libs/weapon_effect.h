
#include "memutil.h"
#include "unitutil.h"
#include "callmethod.h"
#include "spellutil.h"

#define FX_ID_SUMMON_CANCEL 0x7f
#define FX_ID_SHIELD 0x80
#define FX_ID_BLUE_SPARKS 0x81
#define FX_ID_YELLOW_SPARKS 0x82
#define FX_ID_PLASMA_SPARK 0x83
#define FX_ID_VIOLET_SPARK 0x84
#define FX_ID_EXPLOSION 0x85
#define FX_ID_LESSER_EXPLOSION 0x86
#define FX_ID_COUNTERSPELL_EXPLOSION 0x87
#define FX_ID_THIN_EXPLOSION 0x88
#define FX_ID_TELEPORT 0x89
#define FX_ID_SMOKE_BLAST 0x8a
#define FX_ID_DAMAGE_POOF 0x8b
#define FX_ID_SPARK_EXPLOSION 0x93
#define FX_ID_RICOCHET 0x96
#define FX_ID_GREEN_EXPLOSION 0x99
#define FX_ID_WHITE_FLASH 0x9a

void SpecialWeaponPropertyExecuteFXCodeGen(int fxId, int soundId, int *pFXCode)
{
	int *code;

	AllocSmartMemEx(96, &code);
	int calls[]= { 0x4e0702, 0x4e0722, 0x4e0731,0 };
	
	OpcodeCopiesAdvance(code, calls, 0x4e06f0, 0x4e073c);
	SetMemory(code + 46, fxId);
	SetMemory(code + 61, soundId);
	
	if (pFXCode)
		pFXCode[0]=code;
}

void SpecialWeaponPropertyExecuteScriptCodeGen(int execFunctionId, int *pScriptExecutor)
{
	int *pCode, length;
	
	if (!pCode)
	{
		/*
		push eax
		lea eax,[esp+0C]
		push edx
		sub esp,0C
		mov edx,[eax]
		test edx,edx
		je 0BDED14A
		mov [esp],00000000 //script id
		mov [esp+08],edx
		mov eax,[eax+04]
		mov [esp+04],eax
		call 0BBA3434 //00507310
		add esp,0C
		pop edx
		pop eax
		ret 
		*/
		int codes[]={
			0x24448D50,0xEC83520C,0x85108B0C,0xC71774D2,0x00002404, 0x54890000,
			0x408B0824,0x24448904,0x62EAE804,0xC483FFDB,0xC3585A0C};
		pCode=codes;
		length=sizeof(codes);
	}
	int *alloc;
	AllocSmartMemEx(length*4, &alloc);
	NoxDwordMemCopy(pCode, alloc, length);
	FixCallOpcode(alloc+0x21, 0x507310);
	
	int *pExec=alloc+18;
	pExec[0]=execFunctionId;
	if (pScriptExecutor)
		pScriptExecutor[0]=alloc;
}

void SpecialWeaponPropertyExecuteCastSpellCodeGen(string spellName, int *pSpellExecutor)
{
	int codes[]={
		0x0C24448B,0x850CEC83,0x8B3074C0, 0x508B3848,0x244C893C,
		0x244C8B04,0x24548910,0x2444C708,0,
    	0x30518B00,0x00244C8D,0x50505152, 0xE80A6A50,
		0x0004CF98,0x8318C483,0x90C30CC4};
	int *alloc, c;
	AllocSmartMemEx(sizeof(codes)*4, &c);
	alloc=c;
	char spellId = SpellUtilGetId(spellName);

	NoxDwordMemCopy(codes, alloc, sizeof(codes));
	FixCallOpcode(alloc + 0x33, 0x4fdd20);
	char *setspellId = alloc+50;
	setspellId[0]=spellId;
	if (pSpellExecutor)
		pSpellExecutor[0]=alloc;
}

void SpecialWeaponPropertyCreate(int *pDest)
{
	int *ptr;
	
	AllocSmartMemEx(144, &ptr);
	NoxByteMemset(ptr, 144, 0);

	ptr[0]=0x5bac5c;
	ptr[1]=0x61;
	ptr[5]=0x4b0;
	ptr[6]=0xb40000;
	ptr[7]=0x1b2ff2;
	ptr[9]=1;
	if (pDest)
		pDest[0]=ptr;
}

void SpecialWeaponPropertySetName(int *pProperty, string propertyName)
{
	char *realStrPtr;

	StringUtilGetScriptStringPtrEx(propertyName, &realStrPtr);
	pProperty[0]=realStrPtr;
}

void SpecialWeaponPropertySetPlusDamageRate(int *pProperty, float plusDamageRate)
{
	pProperty[14]=plusDamageRate;
}

void SpecialWeaponPropertySetId(int *pProperty, int propertyId)
{
	pProperty[1]=propertyId;
}

void SpecialWeaponPropertySetFXCode(int *pProperty, int *pFXCode)
{
	pProperty[13]=pFXCode;
}

void SpecialWeaponPropertySetExecuteCode(int *pProperty, int *pPropertyExecutor)
{
	pProperty[10]=pPropertyExecutor;
}

void SpecialWeaponPropertySetWeapon(int weapon, char slot, int *pProperty)
{
	int *ptr=UnitToPtr(weapon);

	if (!ptr)
		return;
	slot&=3;
	int *pSlot = ptr[173];
	pSlot[slot]=pProperty;

	int counter=32;
	while (--counter>=0)
		ptr[140+counter]=0x200;
}
