
#include "typecast.h"
#include "opcodehelper.h"
#include "stringutil.h"
#include "callmethod.h"
#include "memutil.h"

int ImportOpenUrl()
{
    int *pp;

    if (!pp)
    {
        int codeStream[]={
            0x72506850, 0x14FF0050, 0x04C48324, 0x125035FF,
            0x036A0058, 0x5A241C68, 0x50006A00, 0x006A006A,
            0x182454FF, 0x5804C483, 0x909090C3};
        pp=&codeStream;
    }
    return pp;
}

void OpenUrlEx(string url)
{
    char *src = StringUtilGetScriptStringPtr(url);
    int *pExec=0x5c3108;
    int *pOld=pExec[0];
    pExec[0]= ImportOpenUrl();
    Unused1f(src);
    pExec[0]=pOld;
}

int ShowMessageBoxImport()
{
    int *pp;

    if (!pp)
    {
        int codeStream[]={
            0x006A5650, 0x216A006A, 0xDB6243E8, 0x0470FFFF, 0x35FF30FF, 0x006E08E4,
            0xCF89F3E8, 0x18C483FF, 0x90C3585E};
        pp=&codeStream;
        FixCallOpcode(pp + 8, 0x507250);
        FixCallOpcode(pp + 0x18, 0x449a10);
    }
    return pp;
}

void ShowMessageBoxCore(int ttDest, int cDest)
{
    int *pExec=0x5c3108;
    int *pOld=pExec[0];

    pExec[0]= ShowMessageBoxImport();
    Unused1f(&ttDest);
    pExec[0]=pOld;
}

void ShowMessageBox(string title, string content)
{
    short ttDest[100], cDest[100];
    char *ttPtr = StringUtilGetScriptStringPtr(title);
    char *cPtr = StringUtilGetScriptStringPtr(content);
	
    NoxUtf8ToUnicode(ttPtr, &ttDest);
    NoxUtf8ToUnicode(cPtr, &cDest);

    ShowMessageBoxCore(&ttDest, &cDest);
}

int ImportWriteBinaryFile()
{
    int *pp;

    if (!pp)
    {
        int codeStream[]={
        0xDB624BE8, 0x246850FF, 0xFF005882, 0x485AE830, 0xC483FFE1,
        0x74C08508, 0x748B5624, 0x768B0424, 0x8D0E8B04, 0x50500476, 0x5651016A, 0xE149D1E8,
        0x10C483FF, 0xCB64FAE8, 0x04C483FF, 0x04C4835E, 0x909090C3};
        pp=&codeStream;
        FixCallOpcode(pp + 0, 0x507250);
        FixCallOpcode(pp + 0xd, 0x56586c);
        FixCallOpcode(pp + 0x2c, 0x565a02);
        FixCallOpcode(pp + 0x34, 0x407533);
    }
    return pp;
}

void WriteBinaryFile(string fileName, int *stream)
{
    int *pExec=0x5c310c;
    int *pOld=pExec[0];

    fileName=StringUtilGetScriptStringPtr(fileName);
    pExec[0]=ImportWriteBinaryFile();
    int *pParam=&fileName;
    Unused20(pParam);
    pExec[0]=pOld;
}

void WriteMusicStrings(int *targetAddr)
{
    int strings[]={
    0x70616863, 0x61772E31, 0x68630076, 0x77327061, 0x772E7A69, 0x63007661, 0x32706168,
    0x2E6E6F63, 0x00766177, 0x70616863, 0x72617732, 0x7661772E, 0x61686300, 0x772E3370,
    0x63007661, 0x34706168, 0x7661772E, 0x61686300, 0x772E3570, 0x63007661, 0x36706168,
    0x7661772E, 0x61686300, 0x772E3770, 0x63007661, 0x38706168, 0x7661772E, 0x61686300,
    0x772E3970, 0x63007661, 0x61706168, 0x7661772E, 0x61686300, 0x772E6270, 0x74007661,
    0x656C7469, 0x7661772E, 0x776F7400, 0x772E316E, 0x74007661, 0x326E776F, 0x7661772E,
    0x776F7400, 0x772E336E, 0x73007661, 0x2E316275, 0x00766177, 0x32627573, 0x7661772E,
    0x62757300, 0x61772E33, 0x61770076, 0x7265646E, 0x61772E31, 0x61770076, 0x7265646E,
    0x61772E32, 0x61770076, 0x7265646E, 0x61772E33, 0x72630076, 0x74696465, 0x61772E73,
    0x68730076, 0x2E6C6C65, 0x00766177, 0x69746361, 0x2E316E6F, 0x00766177, 0x69746361,
    0x2E326E6F, 0x00766177, 0x69746361, 0x2E336E6F, 0x00766177, 0x646E6177, 0x2E347265,
    0x00766177};
    NoxDwordMemCopy(&strings, targetAddr, sizeof(strings));
}

void PreProcessPlayBgm(int *targetAddr)
{
    int *oldBase = 0x59da7c;
    short offs[]={
        0,0xa,0x17,0x24,0x31,0x3b,0x45,0x4f,0x59,0x63,0x6d,0x77,0x81,0x8b,0x95,0x9f,
        0xa9,0xb3,0xbc,0xc5,0xce,0xda,0xe6,0xf2,0xfe,0x108,0x114,0x120,0x12c
    };

    WriteMusicStrings(targetAddr);
    int rep=-1;
    while(++rep<sizeof(offs))
        oldBase[rep]=targetAddr+offs[rep];
    int *whatThe=0x59dbfc;
    whatThe[0]=0;
}

void ExtractMapBgm(string fileName, int resourceFunction)
{
	int *dirPath = 0x59dbf4;
    int *stream = GetScrCodeField(resourceFunction);
    int *unknown = 0x59dc10;

    unknown[0]=StringUtilGetScriptStringPtr(fileName);
    WriteBinaryFile(ReadStringAddressEx(dirPath) + fileName, stream);

    PreProcessPlayBgm(0x5becc4);
}

void MakeMusicDirectory()
{
    int *pExec=0x5c33b8;
    int *pOld=pExec[0];
    int code[]={0xf468006a, 0xff0059db, 0x58116815, 0x9090C300};
    pExec[0]=&code;
    MusicPopEvent();
    pExec[0]=pOld;
}

int ImportPlayNPCVoice()
{
    int *pp;

    if (!pp)
    {
        int codeStream[]={
        0x50685150, 0xFF005072, 0x0C8B2414, 0x97BB4085,
            0xD9006800, 0x646A0044, 0x2454FF51, 0x10C48308, 0x90C35859};
		pp=&codeStream;
    }
    return pp;
}

void PlayNPCVoice(string voiceFilename)
{
    int *pExec=0x5c3108;
    int*pOld=pExec[0];
    pExec[0]=ImportPlayNPCVoice();
    Unused1f(SToInt(voiceFilename));
    pExec[0]=pOld;
}

void NOXLibraryEntryPointFunction()
{
	"export needinit ImportOpenUrl";
	"export OpenUrlEx";
	"export needinit ShowMessageBoxImport";
	"export ShowMessageBoxCore";
	"export ShowMessageBox";
	"export needinit ImportWriteBinaryFile";
	"export WriteBinaryFile";
	"export MakeMusicDirectory";
	"export WriteMusicStrings";
	"export PreProcessPlayBgm";
	"export ExtractMapBgm";
	"export ImportPlayNPCVoice";
	"export PlayNPCVoice";
}
