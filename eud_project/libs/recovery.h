
#include "memutil.h"
#include "hash.h"
#include "bind.h"

//맵이 바뀔 때에만 초기화, 방을 다시만들거나 참가했을 때에는 초기화 안함//
void CreateRecoveryDestructorInstance(int **get)
{
	int *ptr;

	if (!ptr)
	{
		char code[]={
			0x56, 0x55, 0x8B, 0x05, 0xEC, 0xF0, 0x69, 0x00, 0x85, 0xC0, 0x74, 0x16, 0x8B, 
			0x05, 0xAC, 0xAC, 0xAC, 0xAC, 0x85, 0xC0, 0x74, 0x0C, 0x8B, 0x30, 0x8B, 0x68, 
			0x04, 0x89, 0x2E, 0x8B, 0x40, 0x08, 0xEB, 0xF0, 0x5D, 0x5E, 0xC7, 0x05, 0x1C, 
			0x82, 0x59, 0x00, 0xE0, 0xA6, 0x42, 0x00, 0x68, 0xE0, 0xA6, 0x42, 0x00, 0xC3
		};
		int *fix=&code[4];
		fix[0]=0x852980;
		char *p;
		AllocSmartMemEx(sizeof(code), &p);
		NoxByteMemCopy(code, p, sizeof(code));
		AllocSmartMemEx(4, &ptr);
		ptr[0]=0;
		int *dwPtr=&p[14], *t=0x59821c;
		dwPtr[0]=ptr;
		dwPtr=&p[42];
		dwPtr[0]=t[0];
		dwPtr=&p[47];
		dwPtr[0]=t[0];
		t[0]=p;
	}
	if (get)
		get[0]=ptr;
}

void CreateRecoveryWhenMapChangedDestructorInstance(int **get)
{
	int *ptr;

	if (!ptr)
	{
		char code[]={
			0x56, 0x55, 0x8B, 0x05, 0xAC, 0xAC, 0xAC, 0xAC, 0x85, 0xC0, 0x74, 0x0C, 0x8B, 0x30, 0x8B, 
			0x68, 0x04, 0x89, 0x2E, 0x8B, 0x40, 0x08, 0xEB, 0xF0, 0x5D, 0x5E, 0xC7, 0x05, 0x1C, 0x82, 
			0x59, 0x00, 0xE0, 0xA6, 0x42, 0x00, 0x68, 0xE0, 0xA6, 0x42, 0x00, 0xC3, 0x90, 0x90
		};
		char *p;
		AllocSmartMemEx(sizeof(code), &p);
		NoxByteMemCopy(code,p,sizeof(code));
		AllocSmartMemEx(4, &ptr);
		ptr[0]=0;
		int *dwPtr=&p[4], *t=0x59821c;
		dwPtr[0]=ptr;
		dwPtr=&p[32];
		dwPtr[0]=t[0];
		dwPtr=&p[37];
		dwPtr[0]=t[0];
		t[0]=p;
	}
	if (get) get[0]=ptr;
}

int RecoveryHash()
{
    int hash;

    if (!hash)
        HashCreateInstance(&hash);

    return hash;
}

#define RECOVERY_TYPE_NORMAL 0
#define RECOVERY_TYPE_CHANGED_MAP 1

void addRecoveryData(char recoveryType, int *target, int value)
{
	int *base, ty[]={CreateRecoveryDestructorInstance, CreateRecoveryWhenMapChangedDestructorInstance};
	int *param = &base;

	Bind(ty[recoveryType&1], &param);
	int *recoveryData;

	AllocSmartMemEx(12, &recoveryData);
	recoveryData[0]=target;
	recoveryData[1]=value;
	recoveryData[2]=base[0];
	base[0]=recoveryData;
}

void setRecoveryData(char recoveryType, int *target, int value)
{
	int hash=RecoveryHash(), c;

    if (HashGet(hash,target,&c,FALSE))
        return;

	addRecoveryData(recoveryType, target,target[0]);
	target[0]=value;
    HashPushback(hash, target,value);
}

void setRecoveryDataDontDupCheck(char recoveryType, int *target, int value){
	addRecoveryData(recoveryType, target,target[0]);
	target[0]=value;
}

void SetRecoveryDataNormal(int *target, int value)
{
    setRecoveryData(RECOVERY_TYPE_NORMAL, target, value);
}
void SetRecoveryDataNormalN(int *target, int value)
{
    setRecoveryDataDontDupCheck(RECOVERY_TYPE_NORMAL, target, value);
}

void SetRecoveryDataType2(int *target, int value)
{
	setRecoveryData(RECOVERY_TYPE_CHANGED_MAP, target, value);
}


