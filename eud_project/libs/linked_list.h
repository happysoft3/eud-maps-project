
#include "define.h"
#include "memutil.h"

#define _LINKNODE_VALUE_ 0
#define _LINKNODE_PREV_ 1
#define _LINKNODE_NEXT_ 2
#define _LINKNODE_MAX_ 3

void LinkedListCreateElem(int value, int *pDestNode)
{
    int *pNode;
    AllocSmartMemEx(_LINKNODE_MAX_ * 4, &pNode);

    pNode[_LINKNODE_VALUE_]=value;
    pNode[_LINKNODE_PREV_]=0;
    pNode[_LINKNODE_NEXT_]=0;
    pDestNode[0]=pNode;
}

#define _LINKCORE_COUNT_ 0
#define _LINKCORE_HEAD_ 1
#define _LINKCORE_TAIL_ 2
#define _LINKCORE_MAX_ 3

//링크드 리스트 객체를 생성합니다
void LinkedListCreateInstance(int *pDestInstance)
{
    int *instance;
    AllocSmartMemEx(_LINKCORE_MAX_ * 4, &instance);

    instance[_LINKCORE_COUNT_]=0;
    int *front, *back;
    LinkedListCreateElem(0, &front);
    LinkedListCreateElem(0, &back);
    front[_LINKNODE_NEXT_]=back;
    back[_LINKNODE_PREV_]=front;
    instance[_LINKCORE_HEAD_]=front;
    instance[_LINKCORE_TAIL_]=back;
    pDestInstance[0]=instance;
}

//새로운 리스트를 뒤에 추가합니다
//pDestNode을 nullptr으로 패스해도 됩니다
void LinkedListPushback(int *pInstance, int value, int *pDestNode)
{
    int *pNode, *pTail = pInstance[_LINKCORE_TAIL_];
    LinkedListCreateElem(value, &pNode);

    int *pPrev=pTail[_LINKNODE_PREV_];
    pPrev[_LINKNODE_NEXT_]=pNode;
    pNode[_LINKNODE_PREV_]=pPrev;
    pNode[_LINKNODE_NEXT_]=pTail;
    pTail[_LINKNODE_PREV_]=pNode;
    ++pInstance[_LINKCORE_COUNT_];
    if (pDestNode)
        pDestNode[0]=pNode;
}

//새로운 리스트를 앞에 추가합니다
//pDestNode을 nullptr으로 패스해도 됩니다
void LinkedListPushFront(int *pInstance, int value, int *pDestNode)
{
    int *pNode,*pHead=pInstance[_LINKCORE_HEAD_];
    LinkedListCreateElem(value, &pNode);
    int*pNext=pHead[_LINKNODE_NEXT_];
    pNext[_LINKNODE_PREV_]=pNode;
    pNode[_LINKNODE_PREV_]=pHead;
    pNode[_LINKNODE_NEXT_]=pNext;
    pHead[_LINKNODE_NEXT_]=pNode;
    ++pInstance[_LINKCORE_COUNT_];
    if (pDestNode)
        pDestNode[0]=pNode;
}

void LinkedListPop(int *pInstance, int *pNode)
{
    int *pNext=pNode[_LINKNODE_NEXT_], *pPrev=pNode[_LINKNODE_PREV_];

    if (!pNext || !pPrev)
        return;

    pPrev[_LINKNODE_NEXT_]=pNext;
    pNext[_LINKNODE_PREV_]=pPrev;
    --pInstance[_LINKCORE_COUNT_];
    FreeSmartMemEx(pNode);
}

//리스트에서, 앞리스트를 제거합니다
void LinkedListPopFront(int *pInstance)
{
    int *pHead=pInstance[_LINKCORE_HEAD_];

    LinkedListPop(pInstance, pHead[_LINKNODE_NEXT_]);
}

//리스트에서, 뒤리스트를 제거합니다
void LinkedListPopBack(int *pInstance)
{
    int *pTail=pInstance[_LINKCORE_TAIL_];

    LinkedListPop(pInstance, pTail[_LINKNODE_PREV_]);
}

int LinkedListFront(int *pInstance, int *pDestNode)
{
    int *pHead=pInstance[_LINKCORE_HEAD_];

    if (pHead[_LINKNODE_NEXT_]==pInstance[_LINKCORE_TAIL_])
    {
        pDestNode[0]=0;
        return FALSE;
    }

    pDestNode[0]=pHead[_LINKNODE_NEXT_];
    return TRUE;
}

int LinkedListBack(int *pInstance, int *pDestNode)
{
    int *pTail=pInstance[_LINKCORE_TAIL_];

    if (pTail[_LINKNODE_PREV_]==pInstance[_LINKCORE_HEAD_])
    {
        pDestNode[0]=0;
        return FALSE;
    }
    pDestNode[0]=pTail[_LINKNODE_PREV_];
    return TRUE;
}

void LinkedListNext(int *pSrcNode, int *pDestNode)
{
    int *pNext=pSrcNode[_LINKNODE_NEXT_];

    pDestNode[0] = (pNext[_LINKNODE_NEXT_]!=0)*pNext;
}

void LinkedListPrev(int *pSrcNode, int*pDestNode)
{
    int*pPrev=pSrcNode[_LINKNODE_PREV_];
    pDestNode[0]=(pPrev[_LINKNODE_PREV_]!=0)*pPrev;
}

int LinkedListGetValue(int *pNode)
{
    return pNode[_LINKNODE_VALUE_];
}

void LinkedListSetValue(int *pNode, int value)
{
    pNode[_LINKNODE_VALUE_]=value;
}

void LinkedListClear(int *pInstance)
{
    int *node=pInstance[_LINKCORE_HEAD_],*erase;

    LinkedListNext(node, &node);
    while (node)
    {
        if (node==pInstance[_LINKCORE_TAIL_])
            break;

        erase=node;
        node=node[_LINKNODE_NEXT_];
        FreeSmartMemEx(erase);
    }
    int *front=pInstance[_LINKCORE_HEAD_];
    int *back=pInstance[_LINKCORE_TAIL_];

    front[_LINKNODE_NEXT_]=back;
    back[_LINKNODE_PREV_]=front;
    pInstance[_LINKCORE_COUNT_]=0;
}

void LinkedListDeleteInstance(int *pInstance)
{
    LinkedListClear(pInstance);
    FreeSmartMemEx(pInstance);
}

int LinkedListCount(int *pInstance)
{
return pInstance[_LINKCORE_COUNT_];
}

void NOXLibraryEntryPointFunction()
{
    "export LinkedListCreateElem";
    "export LinkedListCreateInstance";
    "export LinkedListPushback";
    "export LinkedListPushFront";
    "export LinkedListPop";
    "export LinkedListPopFront";
    "export LinkedListPopBack";
    "export LinkedListFront";
    "export LinkedListBack";
    "export LinkedListNext";
    "export LinkedListPrev";
    "export LinkedListGetValue";
    "export LinkedListSetValue";
    "export LinkedListClear";
    "export LinkedListDeleteInstance";
    "export LinkedListCount";
}

#undef _LINKCORE_COUNT_
#undef _LINKCORE_HEAD_
#undef _LINKCORE_TAIL_
#undef _LINKCORE_MAX_

