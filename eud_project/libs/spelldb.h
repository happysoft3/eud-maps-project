
#include "imageutil.h"
#include "stringutil.h"

int SpelldbGetSpellTable(char spell_id)
{
	int *spelldbBase = 0x663ef0;
	int *spelldbTarget = spelldbBase + (80 * spell_id);
	
	if (spelldbTarget[6] != 0)
		return spelldbTarget;
	
	return 0;
}

//마법 시전 시 필요한 마나량을 얻습니다
short SpelldbGetSpellManaCost(char spell_id)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
	{
		short *pReqMana = spelldbTarget+62;

		return pReqMana[0];
	}
	return 0;
}

//마법 시전 시 필요한 마나량을 수정합니다
void SpelldbSetSpellManaCost(char spell_id, short manacost)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
	{
		short *pReqMana = spelldbTarget+62;
		pReqMana[0]=manacost;
	}
}

//마법 시전에 요구되는 직업을 수정합니다
void SpelldbChangeAvailableClass(char spell_id, char useWiz, char useConj)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
	{
		char *pReqClass = spelldbTarget+19;
		char temp = pReqClass[0]&(~7);
		char flag = 0;
		
		if (useWiz == TRUE)
			flag |= 2;
		if (useConj == TRUE)
			flag |= 4;
		pReqClass[0]=temp|flag;
	}
}

//마술이 허용되었는 지를 확인
int SpelldbIsAllowed(char spell_id)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);

	if (spelldbTarget)
		return spelldbTarget[5];
	return 0;
}

//마술을 허용할 것인지를 설정
void SpelldbSetAllowed(char spell_id, int bAllow)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);

	if (spelldbTarget)
		spelldbTarget[5] = bAllow&1;
}

//마법 시전시 효과음을 변경합니다
//spell_id- 마법 번호 입니다
//soundId- soundDefine.h 을 사용합니다
void SpelldbChangeCastSound(char spell_id, short soundId)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
		spelldbTarget[17]=soundId;
}

//마법 ON 효과음을 변경합니다
void SpelldbChangeSpellOnSound(char spell_id, short soundId)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
		spelldbTarget[18]=soundId;
}

//마법 OFF 효과음을 변경합니다
void SpelldbChangeSpellOffSound(char spell_id, short soundId)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
		spelldbTarget[19] = soundId;
}

//마법 버튼셋 이미지를 바꿉니다
//클라이언트 사이드입니다
void SpelldbChangeButtonSet(char spell_id, int image_id)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
	{
		int *pImage;
		ImageUtilGetPtrFromID(image_id, &pImage);
		spelldbTarget[2] = pImage;
	}
}

//마법 버튼셋 하이라이팅 이미지를 바꿉니다
//클라이언트 사이드입니다
void SpelldbChangeHighlightingButtonSet(char spell_id, int image_id)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
	{
		int *pImage;
		ImageUtilGetPtrFromID(image_id, &pImage);
		spelldbTarget[3] = pImage;
	}
}

//private 함수입니다-- 내부에서만 사용//
char SpellDbCheckRemovable(short *checkWidePtr, int uniqKey)
{
	short length = StringUtilGetWideLength(checkWidePtr);
	int *pCheck = checkWidePtr + (length*2)+2;

	return pCheck[0] == uniqKey;
}

//private 함수입니다-- 내부에서만 사용//
void SpellDbSetStringImpl(char *src, int *destPtr, int uniqKey)
{
	char removable = SpellDbCheckRemovable(destPtr[0], uniqKey);
	short length = (StringUtilGetLength(src)*2)+8;	//word 형 이므로 x2//
	short *newNamePt = MemAlloc(length);	//새 주소할당//

	NoxByteMemset(newNamePt, length, 0);		//새 주소, 초기화
	NoxUtf8ToUnicode(src, newNamePt);			//인코딩//

	short markPos = (StringUtilGetWideLength(newNamePt)*2)+2;
	int *mark = newNamePt+markPos;

	mark[0] = uniqKey;

	short *pOldWString = destPtr[0];
	destPtr[0] = newNamePt;
	if (removable)
	{
		MemFree(pOldWString);
	}
}

//마법 이름을 변경합니다
//클라이언트 사이드입니다
void SpelldbSetSpellName(char spell_id, string spellName)
{
	int *spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
	{
		char *src;

		StringUtilGetScriptStringPtrEx(spellName, &src);
		SpellDbSetStringImpl(src, spelldbTarget, 0xde4160af);
	}
}

//마법 설명을 변경합니다
//클라이언트 사이드입니다
void SpelldbSetSpellDescription(char spell_id, string spellDesc)
{
	int spelldbTarget = SpelldbGetSpellTable(spell_id);
	
	if (spelldbTarget)
	{
		char *src;

		StringUtilGetScriptStringPtrEx(spellDesc, &src);
		SpellDbSetStringImpl(src, spelldbTarget+4, 0xde4160af);
	}
}

void NOXLibraryEntryPointFunction()
{
	"export SpelldbGetSpellTable";
	"export SpelldbGetSpellManaCost";
	"export SpelldbSetSpellManaCost";
	"export SpelldbChangeAvailableClass";
	"export SpelldbIsAllowed";
	"export SpelldbSetAllowed";
	"export SpelldbChangeCastSound";
	"export SpelldbChangeSpellOnSound";
	"export SpelldbChangeSpellOffSound";
	"export SpelldbChangeButtonSet";
	"export SpelldbChangeHighlightingButtonSet";
	"export SpellDbCheckRemovable";
	"export SpellDbSetStringImpl";
	"export SpelldbSetSpellName";
	"export SpelldbSetSpellDescription";
}