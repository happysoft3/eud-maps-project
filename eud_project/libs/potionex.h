
#include "unitutil.h"
#include "stringutil.h"
#include "memutil.h"

/*
int GetMemory(int a){}
void SetMemory(int a,int b){}
int CreateObjectAt(string s, float x, float y){}
string StringUtilFindUnitNameById(int thingId){}
int MemAlloc(int a){}
*/

int PotionExCreateUtil(string potiontype, float xProfile, float yProfile, int healAmount, int addflags)
{
	int cpotion = CreateObjectAt(potiontype, xProfile, yProfile);
	int ptr = GetMemory(0x750710);
	
	if (GetMemory(ptr + 0x2e0))
		return 0;
	
	int allocptr = MemAlloc(4);
	
	SetMemory(ptr + 0x0c, GetMemory(ptr + 0x0c) | addflags);
	SetMemory(ptr + 0x2dc, 0x53ef70);
	SetMemory(ptr + 0x20, 0x28);
	SetMemory(allocptr, healAmount);
	SetMemory(ptr + 0x2e0, allocptr);
	return cpotion;
}

int PotionExCreateYellowPotion(float xProfile, float yProfile, int healAmount)
{
	return PotionExCreateUtil(StringUtilFindUnitNameById(639), xProfile, yProfile, healAmount, 0x10);
}

int PotionExCreateBlackPotion(float xProfile, float yProfile, int healAmount)
{
	return PotionExCreateUtil(StringUtilFindUnitNameById(641), xProfile, yProfile, healAmount, 0x10);
}

int PotionExCreateWhitePotion(float xProfile, float yProfile, int healAmount)
{
	return PotionExCreateUtil(StringUtilFindUnitNameById(640), xProfile, yProfile, healAmount, 0x20);
}

void NOXLibraryEntryPointFunction()
{
	"export PotionExCreateUtil";
	"export PotionExCreateYellowPotion";
	"export PotionExCreateBlackPotion";
	"export PotionExCreateWhitePotion";
}

