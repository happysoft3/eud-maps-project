
#include "define.h"
#include "recovery.h"

#define THINGDB_SERV_THINGID 0
#define THINGDB_SERV_NAMEPTR 1
#define THINGDB_SERV_FIELD08 2
#define THINGDB_SERV_FLAGS 3
#define THINGDB_SERV_FIELD10 4
#define THINGDB_SERV_THINGID2 5
#define THINGDB_SERV_CLASS 6
#define THINGDB_SERV_FIELD1C 7
#define THINGDB_SERV_FIELD20 8
#define THINGDB_SERV_FIELD24 9
#define THINGDB_SERV_FIELD28 10
#define THINGDB_SERV_FIELD2C 11
#define THINGDB_SERV_FIELD30 12
#define THINGDB_SERV_FRICTION_FORCE 13
#define THINGDB_SERV_MASS 14
#define THINGDB_SERV_EXTENT_TYPE 15
#define THINGDB_SERV_EXTENT_CIRCLE 16
#define THINGDB_SERV_CARRY_CAPACITY 17
#define THINGDB_SERV_EXTENT_WIDTH 18
#define THINGDB_SERV_EXTENT_HEIGHT 19
#define THINGDB_SERV_FIELD50 20
#define THINGDB_SERV_FIELD54 21
#define THINGDB_SERV_FIELD58 22
#define THINGDB_SERV_FIELD5C 23
#define THINGDB_SERV_FIELD60 24
#define THINGDB_SERV_FIELD64 25
#define THINGDB_SERV_FIELD68 26
#define THINGDB_SERV_FIELD6C 27
#define THINGDB_SERV_ZSIZE_LOW 28
#define THINGDB_SERV_ZSIZE_HIGH 29
#define THINGDB_SERV_HP_PTR 34
#define THINGDB_SERV_COLLIDE_FUNCTION 35
#define THINGDB_SERV_DAMAGE_FUNCTION 39
#define THINGDB_SERV_DIE_FUNCTION 43
#define THINGDB_SERV_UPDATE_FUNCTION 47
// 00 00 00 00 00 00 34 42 FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
// 00 00 00 A0 87 4E 00 00 00 00 00 00 00 00 00 30 0B 4E 00 20 2E 53 00 00 00 
// 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
// 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 A0 49 4F 00 00 00 00 00 90 D4 56 0A


int getThingDbObject(short thingId)
{
    int *thingDbBase=GetMemory(0x7520d4);
    
    return thingDbBase[thingId];
}

//마찰력 변경- 기본값 0.5
void ChangeThingDbFrictionForce(int thingId, float force)
{
    int *pObject = getThingDbObject(thingId);
    int *data= &force;

    SetRecoveryDataNormal(&pObject[THINGDB_SERV_FRICTION_FORCE], data[0]);
}

void ChangeThingDbMass(int thingId, float fvalue)
{
    int *pObject=getThingDbObject(thingId), *d=&fvalue;

    SetRecoveryDataNormal(&pObject[THINGDB_SERV_MASS], d[0]);
}

//아이템 소지한도 설정
void ChangeThingDbCarryCapacity(int thingId, float fvalue)
{
    int *pObject=getThingDbObject(thingId), *d=&fvalue;

    SetRecoveryDataNormal(&pObject[THINGDB_SERV_CARRY_CAPACITY], d[0]);
}

//충돌크기 변경-원형//
void ChangeThingDbCircle(int thingId, float fsize)
{
    int *pObject=getThingDbObject(thingId), *d=&fsize;

    if (pObject[THINGDB_SERV_EXTENT_TYPE]!=2)
        return;

    SetRecoveryDataNormal(&pObject[THINGDB_SERV_EXTENT_CIRCLE], d[0]);
}

// void computeRectsize(int *pObject)
// {
// #2:1131
// #15:1237
// #20:-1414
// #30:-1414=1:x
// #34:-1273
// #50:-2298
// #60:-4243=1:x
// #64: 0:-4525
// }
//충돌크기 변경-사각//
void ChangeThingDbSize(int thingId, float width, float height)
{
    int *pObject=getThingDbObject(thingId), *d=&width;

    if (pObject[THINGDB_SERV_EXTENT_TYPE]!=3)
        return;

    SetRecoveryDataNormal(&pObject[THINGDB_SERV_EXTENT_WIDTH],d[0]);
    SetRecoveryDataNormal(&pObject[THINGDB_SERV_EXTENT_HEIGHT],d[1]);
    // computeRectsize(pObject);
}
