
#include "array.h"
#include "stringutil.h"
#include "memutil.h"

////guiwindow.h///

int GUIGetWindowId(int *wndPtr)
{
    return wndPtr[0];
}

void GUIGetLastChildWindow(int *wndPtr, int *pDestWnd)
{
    pDestWnd[0] = wndPtr[100];
}

void GUIGetParentWindow(int *wndPtr, int *pDestWnd)
{
    pDestWnd[0] = wndPtr[99];
}

void GUIGetPreviousWindow(int *wndPtr, int *pDestWnd)
{
    pDestWnd[0] = wndPtr[97];
}

void GUIGetNextWindow(int *wndptr, int *pDestWnd)
{
    pDestWnd[0] = wndptr[98];
}

void GUIGetWindowTextColor(int *wndPtr, short *pDestWordColr)
{
    pDestWordColr[0] = wndPtr[26];
}

void GUISetWindowTextColor(int *wndPtr, short wordColr)
{
    short *pColor = ArrayRefN(wndPtr, 26);

    pColor[0] = wordColr;
    pColor[1] = wordColr;
}

void GUISetWindowCaption(int *wndPtr, string name)
{
    if (!wndPtr)
        return;

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(name), wndPtr + 108);
}

void GUIGetWindowFlag(int *wndPtr, int *pDestFlag)
{
    pDestFlag[0] = wndPtr[1];
}

void GUISetWindowFlag(int *wndPtr, int srcFlag)
{
    wndPtr[1] = srcFlag;
}

void GUIGetWindowRect(int *wndPtr, int *pDestXYWH)
{
    int destInc = 0, srcInc = 2;

    pDestXYWH[destInc++] = wndPtr[srcInc++];
    pDestXYWH[destInc++] = wndPtr[srcInc++];
    pDestXYWH[destInc++] = wndPtr[srcInc++];
    pDestXYWH[destInc++] = wndPtr[srcInc++];
}

int GUIGetWindowType(int *wndPtr)
{
    return wndPtr[6];
}

void GUIPrivateScrollListModify(short line, int *pElement, short *wSrc, short length, short color)
{
    if (!length)
        return;

    short *pColor=ArrayRefN(pElement, 129);

    pColor[0]=color;
    pColor[1]=color;
    pElement[0]=line*15;
    pElement[130]=0xe;
    NoxWordMemCopy(wSrc, ArrayRefN(pElement, 1), length);
}

void GUIScrollListWindowDefaultColor(short *pDestColor)
{
    pDestColor[0]=0xe528;
}

void GUISetWindowScrollListboxText(int *wndPtr, string in, short *pColorArray)
{
    if (wndPtr[6]!=330) //scrollListboxType//
        return;
    
    int *dataPtr = wndPtr[8];
    int *pElement = dataPtr[6];
    char *src = StringUtilGetScriptStringPtr(in);
    short dest[1024];
    NoxUtf8ToUnicode(src, &dest);
    short length = StringUtilGetWideLength(&dest), off=0, line=0;
    dest[length++]='\n';

    int count=-1;
    while (++count<length)
    {
        if (dest[count]=='\n')
        {
            short color;

            if (!pColorArray)
                GUIScrollListWindowDefaultColor(&color);
            else
                color = pColorArray[line];
            GUIPrivateScrollListModify(++line, pElement, &dest + (off*2), count-off, color);
            pElement+=524;
            off=count+1;
        }
    }
    dataPtr[10]=line*15;
    short *pLineSlot=ArrayRefN(dataPtr, 11);
    pLineSlot[0]=line;
    pLineSlot[1]=line;
}

void GUIFindChild(int *wndPtr, int childWndId, int *pDestWnd)
{
    int *childWnd;

    GUIGetLastChildWindow(wndPtr, &childWnd);
    while (childWnd)
    {
        if (childWnd[0]==childWndId)
            break;

        GUIGetPreviousWindow(childWnd, &childWnd);
    }
    pDestWnd[0] = childWnd;
}

void NOXLibraryEntryPointFunction()
{
	"export GUIGetWindowId";
	"export GUIGetLastChildWindow";
	"export GUIGetParentWindow";
	"export GUIGetPreviousWindow";
	"export GUIGetNextWindow";
	"export GUIGetWindowTextColor";
	"export GUISetWindowTextColor";
	"export GUISetWindowCaption";
	"export GUIGetWindowFlag";
	"export GUIGetWindowRect";
	"export GUIGetWindowType";
    "export GUIScrollListWindowDefaultColor";
    "export GUIPrivateScrollListModify";
	"export GUISetWindowScrollListboxText";
	"export GUIFindChild";
}
