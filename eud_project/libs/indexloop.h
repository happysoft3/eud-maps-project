
#include "unitstruct.h"
#include "hash.h"
#include "bind.h"
#include "mathlab.h"

int GetIndexLoopHashCore()
{
	int instance;
	
	return &instance;
}

void DemonstrateDetectionIndex(int functionId, int curId, int owner)
{
	Bind(functionId, &functionId + 4);
}

void InvestigateFromIndexLoop(int curId)
{
	int key = GetUnitThingID(curId), action;
	int *p=GetIndexLoopHashCore();
	
	if (HashGet(p[0], key, &action, false))
		DemonstrateDetectionIndex(action, curId, GetOwner(curId));
}

void SustainedIndexLoop()
{
	int *pOff=0x750710;
	int curId, tempId;
	
	while (pOff[0])
	{
		tempId=GetMemory(pOff[0]+0x2c);
		if (curId)
		{
			while (curId < tempId)
				InvestigateFromIndexLoop(++curId);
			break;
		}
		curId= tempId;
		break;
	}
	FrameTimer(1, SustainedIndexLoop);
}

void IndexLoopModuleOnDetectedShuriken(int cur, int owner)
{
	PushObjectTo(cur, UnitRatioX(cur, owner, 40.0), UnitRatioY(cur, owner, 40.0));
}

void IndexLoopModuleOnDetectedBolt(int cur, int owner)
{
	PushObjectTo(cur, UnitRatioX(owner, cur, 50.0), UnitRatioY(owner, cur, 50.0));
}

void IndexLoopModPixieOnCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(SELF);

            if (IsAttackedBy(OTHER, owner))
                Damage(OTHER, SELF, 8, DAMAGE_TYPE_MANA_BOMB);
        }
        break;
    }
}

void IndexLoopModuleOnDetectedPixie(int cur, int owner)
{
	SetUnitCallbackOnCollide(cur, IndexLoopModPixieOnCollide);
}

//you can override
void IntroducedIndexLoopHashCondition(int pInstance)
{
	HashPushback(pInstance, 672, IndexLoopModuleOnDetectedPixie);
	HashPushback(pInstance, 1179, IndexLoopModuleOnDetectedShuriken);
	HashPushback(pInstance, 531, IndexLoopModuleOnDetectedBolt);
}

void StartIndexLoopPrivate()
{
	int *p=GetIndexLoopHashCore();
	HashCreateInstance(p);
	IntroducedIndexLoopHashCondition(p[0]);
	SustainedIndexLoop();
}

//you can override
int IndexLoopRunDelayValue()
{
	return 10;
}

void InitialExecuteIndexLoopPrivate()
{
	FrameTimer(IndexLoopRunDelayValue(), StartIndexLoopPrivate);
}

void NOXLibraryEntryPointFunction()
{
	"export needinit InitialExecuteIndexLoopPrivate";
}

