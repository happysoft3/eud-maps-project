
#include "libs/define.h"
#include "libs/weapon_effect.h"
#include "libs/sound_define.h"
#include "libs/itemproperty.h"
#include "libs/playerinfo.h"
#include "libs/printutil.h"
#include "libs/buff.h"
#include "libs/clientside.h"
int *m_pWeaponProperty1;
int *m_pWeaponPropertyFxCode1;
int *m_pWeaponPropertyExecutor1;

int *m_pWeaponProperty2;
int *m_pWeaponPropertyFxCode2;
int *m_pWeaponPropertyExecutor2;

int *m_pCustomVoice;
int *m_pWizVoice;

void WeaponHitRunScript()
{
    if (!IsPlayerUnit(OTHER))
        return;
    
    int curX, curY;

    if (!GetPlayerMouseXY(OTHER, &curX, &curY))
        return;
    float fx = IntToFloat(curX), fy = IntToFloat(curY);

    CastSpellLocationLocation("SPELL_FIST", fx, fy, fx, fy);
}

void MapInitialize()
{
    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_PLASMA_SPARK, SOUND_ShellSelect, &m_pWeaponPropertyFxCode1);
    SpecialWeaponPropertyExecuteCastSpellCodeGen("SPELL_EXPLOSION", &m_pWeaponPropertyExecutor1);
    SpecialWeaponPropertyCreate(&m_pWeaponProperty1);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponProperty1, m_pWeaponPropertyExecutor1);
    SpecialWeaponPropertySetFXCode(m_pWeaponProperty1, m_pWeaponPropertyFxCode1);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_LESSER_EXPLOSION, SOUND_ShellMouseBoom, &m_pWeaponPropertyFxCode2);
    SpecialWeaponPropertyExecuteScriptCodeGen(WeaponHitRunScript, &m_pWeaponPropertyExecutor2);
    SpecialWeaponPropertyCreate(&m_pWeaponProperty2);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponProperty2, m_pWeaponPropertyExecutor2);
    SpecialWeaponPropertySetFXCode(m_pWeaponProperty2, m_pWeaponPropertyFxCode2);

    SpecialWeaponPropertySetId(m_pWeaponProperty1, 'A');
    SpecialWeaponPropertySetPlusDamageRate(m_pWeaponProperty1, 20.0);

    FrameTimer(10, InitCustomVoiceSet);
}

void IncreaseOpTest()
{
    int sword = CreateObjectAt("GreatSword", GetObjectX(OTHER), GetObjectY(OTHER));

    SetWeaponPropertiesDirect(sword, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_confuse4, ITEM_PROPERTY_fire3);
    SpecialWeaponPropertySetWeapon(sword, 2, m_pWeaponProperty1);
}

void AssignOpTest()
{
    int sword = CreateObjectAt("GreatSword", GetObjectX(OTHER), GetObjectY(OTHER));

    SetWeaponPropertiesDirect(sword, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_confuse4, ITEM_PROPERTY_lightning3);
    SpecialWeaponPropertySetWeapon(sword, 2, m_pWeaponProperty2);
}

int NPCColorStuff(int *pColorIndex)
{
    if (pColorIndex[0] == 3)
    {
        pColorIndex[0] = 0;
        return pColorIndex[0];
    }
    return pColorIndex[0]++;
}

void CreateNewNPC(float xpos, float ypos, int *pDest)
{
    int mon = CreateObjectAt("NPC", xpos, ypos);

    if (pDest)
        pDest[0] = mon;

    int *ptr = UnitToPtr(mon), rep=-1;

    while (++rep<32)
        ptr[140+rep]=0x400;
    int *pec = ptr[187];

    pec[94] = '@';
    pec[331] = 255; //Strength

    int color[] = {Random(0,255),Random(0,255),Random(0,255)}, colorIndex=0;

    pec[519]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[520]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[521]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[522]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8)|(color[NPCColorStuff(&colorIndex)]<<16)|(color[NPCColorStuff(&colorIndex)]<<24);
    pec[523]=color[NPCColorStuff(&colorIndex)]|(color[NPCColorStuff(&colorIndex)]<<8);
}

void MyRuby()
{
    int npc = 0;

    CreateNewNPC(GetObjectX(OTHER), GetObjectY(OTHER), &npc);

    SetUnitMaxHealth(npc, 200);
    ChangeUnitCustomVoice(npc, m_pCustomVoice);
}

void CreateVoiceSetInstance(int *pDest, string voiceName)
{
    if (!pDest)
        return;

    int *pAlloc;

    AllocSmartMemEx(80, &pAlloc);
    pDest[0] = pAlloc;
    NoxDwordMemset(pAlloc, 80, 0);
    int length = StringUtilGetLength(StringUtilGetScriptStringPtr( voiceName) );
    int *pName = MemAlloc(++length);
    int *pNameDest = pName;
    int *pStr = StringUtilGetScriptStringPtr(voiceName);

    while (TRUE)
    {
        pNameDest[0]=(pNameDest[0]&(~0xff))|(pStr[0]&0xff);
        if (!(pStr[0]&0xff))
            break;

        pNameDest+=1;
        pStr+=1;
    }
    pAlloc[0] = pName;
}

#define VOICEOFFSET_ON_TALKABLE 1
#define VOICEOFFSET_ON_TAUNT 2
#define VOICEOFFSET_ON_IDLE 4
#define VOICEOFFSET_ON_ENGAGE 5
#define VOICEOFFSET_ON_ATTACKINIT 6
#define VOICEOFFSET_ON_MELEEHIT 8
#define VOICEOFFSET_ON_MELEEMISS 9
#define VOICEOFFSET_ON_MISSILEINIT 10
#define VOICEOFFSET_ON_SHOOT 11
#define VOICEOFFSET_ON_FLEE 12
#define VOICEOFFSET_ON_RETREAT 13
#define VOICEOFFSET_ON_SPELLINIT 14
#define VOICEOFFSET_ON_DIE 15
#define VOICEOFFSET_ON_HURT 16
#define VOICEOFFSET_ON_RECOGNIZE 17
#define VOICEOFFSET_ON_MOVE 18

void ModificationVoiceData(int *pVoice, int voiceOffsetMacro, int soundId)
{
    if (!pVoice)
        return;

    pVoice[voiceOffsetMacro] = soundId;
}

void InitCustomVoiceSet()
{
    CreateVoiceSetInstance(&m_pCustomVoice, "NPC");
    ModificationVoiceData(m_pCustomVoice, 3, SOUND_ArcheryContestBegins);
    ModificationVoiceData(m_pCustomVoice, 7, SOUND_ArcheryContestBegins);
    ModificationVoiceData(m_pCustomVoice, VOICEOFFSET_ON_HURT, SOUND_HumanMaleHurtHeavy);
    ModificationVoiceData(m_pCustomVoice, VOICEOFFSET_ON_DIE, SOUND_HumanMaleDie);
    ModificationVoiceData(m_pCustomVoice, VOICEOFFSET_ON_RECOGNIZE, SOUND_TauntLaugh);
    ModificationVoiceData(m_pCustomVoice, VOICEOFFSET_ON_ATTACKINIT, SOUND_SpellPhonemeUpRight);

    CreateVoiceSetInstance(&m_pWizVoice, "Wizard");
    ModificationVoiceData(m_pWizVoice, VOICEOFFSET_ON_MOVE, SOUND_FlareWand);
    ModificationVoiceData(m_pWizVoice, VOICEOFFSET_ON_ENGAGE, SOUND_ElevLOTDUp);
    ModificationVoiceData(m_pWizVoice, VOICEOFFSET_ON_SPELLINIT, SOUND_AwardSpell);
    ModificationVoiceData(m_pWizVoice, VOICEOFFSET_ON_FLEE, SOUND_GameOver);
    ModificationVoiceData(m_pWizVoice, VOICEOFFSET_ON_DIE, SOUND_BearDie);

    UniPrintToAll(IntToString(m_pWizVoice));

    UniPrintToAll(IntToString(UnitToPtr(Object("exitwarp"))));
}

void ChangeUnitCustomVoice(int unit, int *pVoice)
{
    int *uptr = UnitToPtr(unit);
    int *uc = uptr[187];

    uc[122] = pVoice;
}

void SummonEmeralds()
{
    int arr[] = {'a', 'b', 'c', 'd'};
    WriteBinaryFile("abc", &arr);
}

void UserUpdateImpl(int *pDestCode)
{
    int *pCodeStream, *pExec;

    if (!pCodeStream)
    {
        int arr[76];
        arr[0] = 0x00011DE9; arr[1] = 0x08EC8300; arr[2] = 0x57565553; arr[3] = 0xCC5634E8; arr[4] = 0x24748BFF; arr[5] = 0x85E88B1C; arr[6] = 0x8BE574F6; arr[7] = 0x0002ECBE; 
        arr[8] = 0x10006800; arr[9] = 0x9F8B0000; arr[10] = 0x00000114; arr[11] = 0xCB9594E8; arr[12] = 0x04C483FF; arr[13] = 0x0A74C085; arr[14] = 0x0224878B; arr[15] = 0xC0850000; 
        arr[16] = 0xDB85BE75; arr[17] = 0x83C70A74; arr[18] = 0x0000125C; arr[19] = 0x00000000; arr[20] = 0x016A016A; arr[21] = 0xE77BE856; arr[22] = 0xC483FFD9; arr[23] = 0xB7B0A10C;
        arr[24] = 0xC0850085; arr[25] = 0x458B0774; arr[26] = 0x74C0853A; arr[27] = 0x56BF0F11; arr[28] = 0x38468D7C; arr[29] = 0x4AE85052; arr[30] = 0x83FFDEEB; arr[31] = 0x4E8B08C4;
        arr[32] = 0x3C568B38; arr[33] = 0x00100068; arr[34] = 0x244C8900; arr[35] = 0x24548914; arr[36] = 0x952FE818; arr[37] = 0xC483FFCB; arr[38] = 0x74C08504; arr[39] = 0x34BF8B17;
        arr[40] = 0x85000001; arr[41] = 0x8D0D74FF; arr[42] = 0x50102444; arr[43] = 0x7013E857; arr[44] = 0x0BEBFFDA; arr[45] = 0x10244C8D; arr[46] = 0xF6E85156; arr[47] = 0x83FFDA69;
        arr[48] = 0x548D08C4; arr[49] = 0x56521024; arr[50] = 0xD95F48E8; arr[51] = 0xE8106AFF; arr[52] = 0xFFCB94F1; arr[53] = 0x850CC483; arr[54] = 0x6A4774C0; arr[55] = 0x6CC3E804;
        arr[56] = 0xC483FFCC; arr[57] = 0x74C08504; arr[58] = 0x08838B39; arr[59] = 0x31000008; arr[60] = 0x34488AC9; arr[61] = 0x79BBE851; arr[62] = 0x408BFFCC; arr[63] = 0x04C4834C;
        arr[64] = 0x1E74C085; arr[65] = 0x01EC888B; arr[66] = 0xC9850000; arr[67] = 0x938B1474; arr[68] = 0x00000808; arr[69] = 0x016A016A; arr[70] = 0xE6E85250; arr[71] = 0x83FFDA22;
        arr[72] = 0x5E5F10C4; arr[73] = 0xC4835B5D; arr[74] = 0x9090C308; arr[75] = 0x00000090; 

        int *pSub = &arr + 5;
        FixCallOpcode(pSub + 7, 0x416640);
        FixCallOpcode(pSub + 0x27, 0x40A5C0);
        FixCallOpcode(pSub + 0x50, 0x4EF7D0);
        FixCallOpcode(pSub+0x71, 0x53FBC0);
        FixCallOpcode(pSub+0x8c, 0x40A5C0);
        FixCallOpcode(pSub+0xa8, 0x4F80C0);
        FixCallOpcode(pSub+0xb5, 0x4F7AB0);
        FixCallOpcode(pSub+0xc3, 0x4E7010);
        FixCallOpcode(pSub+0xca, 0x40A5C0);
        FixCallOpcode(pSub+0xd8, 0x417DA0);
        FixCallOpcode(pSub+0xf0, 0x418AB0);
        FixCallOpcode(pSub+0x115, 0x4F3400);

        int szExec[] = { 0xDB624BE8, 0xE5E850FF, 0x83FFDA6E, 0x90C304C4 };

        FixCallOpcode(&szExec, 0x507250);
        FixCallOpcode(&szExec + 6, pSub);
        pCodeStream=&szExec;
    }
    pDestCode[0] = pCodeStream;
}

int UserUpdate(int user)
{
    if (!IsPlayerUnit(user))
        return FALSE;

    int *ptr = UnitToPtr(user);

    if (!ptr)
        return FALSE;
    
    int *builtins = 0x5c308c;
    int *pOld = builtins[184];
    int *pCode = 0;

    UserUpdateImpl(&pCode);
    if (!pCode)
        return FALSE;

    builtins[184] = pCode;
    Unknownb8(ptr);
    builtins[184] = pOld;

    int *uc = ptr[187];
    int *pInfo = uc[69];

    pInfo[1175] = 1;

    return TRUE;
}

void FixMaxHp()
{
    if (IsPlayerUnit(OTHER))
    {
        if (MaxHealth(OTHER) != 150)
        {
            SetUnitMaxHealth(OTHER, 150);
            UserUpdate(OTHER);
        }
    }
}

void IncreaseMaxHp()
{
    if (IsPlayerUnit(OTHER))
    {
        if (MaxHealth(OTHER) != 300)
        {
            SetUnitMaxHealth(OTHER, 300);
            int res = UserUpdate(OTHER);

            UniPrintToAll(IntToString(res));
        }
    }
}

