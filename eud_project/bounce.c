
#include "bounce_resource.h"
#include "bounce_utils.h"
#include "libs/printutil.h"
#include "libs/waypoint.h"
#include "libs/coopteam.h"
#include "libs/networkRev.h"
#include "libs/fxeffect.h"
#include "libs/buff.h"
#include "libs/sound_define.h"
#include "libs/observer.h"
#include "libs/logging.h"
#include "libs/bind.h"
#include "libs/hash.h"
#include "libs/queueTimer.h"
#include "libs/format.h"
#include "libs/voiceList.h"

#define PLAYER_FLAG_RUSH 2
#define PLAYER_FLAG_RUSH_LEFT 4
#define PLAYER_FLAG_RUSH_RIGHT 8
#define PLAYER_DEATH_FLAG 0x80000000
#define BALL_REVIVE_LOCATION 10
#define PLAYER_CONTROL_ROOM 46
#define PLAYER_INITIAL_POS 47

#define PLAYER_CAM_DISTANCE 260.0

#define SEND_LEFT_SHIFT 1
#define SEND_RIGHT_SHIFT 2

#define WISP_MOVING_DISTANCE 4.0
#define ALLOW_LIMIT_Y 150.0

#define AREA_2_START_POS 76
// #define AREA_2_START_POS 96
#define AREA_3_START_POS 89
#define AREA_5_START_POS 169

#define AREA_1_EXIT_POS 20
#define AREA_2_EXIT_POS 22
#define AREA_3_EXIT_POS 45
#define AREA_4_START_POS 97
#define AREA_4_EXIT_POS 170

//테스트 시작
// #define AREA_1_EXIT_POS 90
// #define AREA_2_EXIT_POS 171
// #define AREA_3_EXIT_POS 172
// #define AREA_4_START_POS 185 //173
//테스트 끝

#define WISP_JUMP_NORMAL 6.3
#define WISP_JUMP_POWERFUL 10.8

#define LEFT_LIMIT 173.0
#define RIGHT_LIMIT 5800.0

int m_unitscanHashInstance;
int m_lastCreatedUnit;
int m_genericHash;

int m_downBridgePart2[3];

int m_currentArea;

int m_firstMovingBlocks[2];
int m_secondMovingBlocks[2];
int m_part2MovingBlocks1[2];

int m_creatureHash;
int *m_clientKeyState;
int m_clientNetId;
int m_player[10];
int m_pFlag[10];
short m_pDeath[10];
int m_usercam[10];
int m_creature[10];
float m_creJump[10];
float m_creFall[10];

void ThisMapInformationPrintMessage(int pUnit)
{
    UniPrint(pUnit, "<<---공튀기기 시즌1___________________________________  제작. 237");
    UniPrint(pUnit, "제작자의 홈페이지- blog.naver.com/ky10613 버그 발생 시 문의하셈욤 ^^");
    UniPrint(pUnit, "즐거운 시간 되십시오");
}

void TeleportCreatureSafety(int plr, float x, float y)
{
    int cre=m_creature[plr], cam=m_usercam[plr];

    if (MaxHealth(cre))
    {
        MoveObject(cre, x,y);
        MoveObject(cam, x, y+PLAYER_CAM_DISTANCE);
    }
}

int getScrIndexWithCreature(int cre, int *destPlr)
{
    int owner=GetOwner(cre);

    if (!IsPlayerUnit(owner))
        return FALSE;
    
    int plr = CheckPlayerWithId(owner);

    if (plr <0)
        return FALSE;

    destPlr[0]=plr;
    return TRUE;
}

void placingStuff()
{
    CreateObject("LargeFlame", 12);
    CreateObject("LargeFlame", 11);
    CreateObject("LargeFlame", 17);
}

void OnEndedUnitScan()
{
    placingStuff();
    // deferredInit();
    // WallGroupOpen(WALLGROUP_startingWalls);
    UniPrintToAll("q047252.c::OnEndedUnitScan");
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}

static int TryJumpHigher(int wisp, float high)
{
    int plr;

    if (!getScrIndexWithCreature(wisp, &plr))
        return FALSE;

    m_creJump[plr]=high;
    return TRUE;
}

void OnBlockPowerBound()
{
    if (GetObjectY(OTHER)-GetObjectY(SELF)>=0.0)
        return;
    if (!TryJumpHigher(GetCaller(), WISP_JUMP_POWERFUL))
        return;

    Effect("COUNTERSPELL_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
    GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(SELF, SOUND_BallBounce);
}

void OnBlockBound()
{
    if (GetObjectY(OTHER)>=GetObjectY(SELF))
        return;
    if (!TryJumpHigher(GetCaller(), WISP_JUMP_NORMAL))
        return;

    Effect("COUNTERSPELL_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
    PlaySoundAround(SELF, SOUND_BallBounce);
}

void serverInputLoop()
{
    int left = KeyboardIOCheckKey(0xcb)<<SEND_LEFT_SHIFT;
    int right = KeyboardIOCheckKey(0xcd)<<SEND_RIGHT_SHIFT;
    int prevState, state = left|right;

    if (prevState!=state)
    {
        prevState=state;
        m_clientKeyState[31]=state;
    }
    FrameTimer(3, serverInputLoop);
}

void ClientKeyHandlerRemix()
{
    int left=KeyboardIOCheckKey(0xcb)<<SEND_LEFT_SHIFT;
    int right=KeyboardIOCheckKey(0xcd)<<SEND_RIGHT_SHIFT;
    int prevState, state = left|right;

    if (prevState!=state)
    {
        prevState=state;

        char testPacket[]={0x3d, 0x00+(m_clientNetId*4),0x10,0x75,0x00, 0, 0,0,0};
        int *p = testPacket + 1;

        p[1] = prevState;
        NetClientSendRaw(31, 0, testPacket, sizeof(testPacket));
    }
}

void ClientClassTimerLoop()
{
    ClientKeyHandlerRemix();
    FrameTimer(1, ClientClassTimerLoop);
}

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    char *p=NETWORK_UTIL_PINDEX_OFF;
    m_clientNetId=p[0];
    FrameTimer(30, ClientClassTimerLoop);
    InitializeResources();
}

#define KEY_UP_SHIFT 1
#define KEY_LEFT_SHIFT 2
#define KEY_RIGHT_SHIFT 4
#define KEY_DOWN_SHIFT 8

static void serverTaskUserInputHandler(int plr, int pUnit, float *px) //, float *py)
{
    int pIndex=GetPlayerIndex(pUnit), cre=m_creature[plr];
    int keyState=m_clientKeyState[pIndex];
    float xVect=0.0, xpos= GetObjectX(cre);

    if (keyState&KEY_LEFT_SHIFT)
    {
        if (xpos>LEFT_LIMIT)
            xVect-=WISP_MOVING_DISTANCE;
    }
    if (keyState&KEY_RIGHT_SHIFT)
    {
        if (xpos<RIGHT_LIMIT)
            xVect+=WISP_MOVING_DISTANCE;
    }
    // if (ToInt(xVect) || ToInt(yVect))    
    //     PushObjectTo(m_creature[plr], xVect, yVect);
    px[0]=xVect;
    //py[0]=yVect;
}

void PlayerClassOnShutdown(int rep)
{
    if (CurrentHealth(m_player[rep]))
        MoveObject(m_player[rep], LocationX(PLAYER_INITIAL_POS), LocationY(PLAYER_INITIAL_POS));
    if (m_creature[rep])
    {
        RemoveCreature(m_creature[rep]);
        m_creature[rep]=0;
    }
    RemoveCamera(m_usercam[rep]);
    m_usercam[rep]=0;
    m_player[rep]=0;
    m_pFlag[rep]=0;
}

void PlayerClassOnDeath(int rep, int user)
{
    // WriteLog("playerclassondeath");
    // char dieMsg[128];
    // int params[]={StringUtilGetScriptStringPtr(PlayerIngameNick(user)), ++m_pDeath[rep]};

    // NoxSprintfString(&dieMsg, "방금 %s님께서 적에게 격추되었습니다 (%d번째 사망)", &params, sizeof(params));
    // UniPrintToAll(ReadStringAddressEx(&dieMsg));
    // WriteLog("playerclassondeath end");
}

static void onCreaturePositionHandler(int plr, float xv)
{
    float yv = 0.0;
    int cre=m_creature[plr];

    if (m_creJump[plr]>0.0)
    {
        m_creJump[plr]-=0.3;
        yv=-m_creJump[plr];
        if (ToInt( m_creFall[plr]) )
            m_creFall[plr]=0;
    }
    else
    {
        m_creFall[plr]+=0.2;
        yv=m_creFall[plr];
    }
    MoveObjectVector(cre, xv, yv);
}

static void readyToRevive(int sub)
{
    int cam = GetUnit1C(sub), plr = GetDirection(sub);

    if (MaxHealth(cam) && CurrentHealth(m_player[plr]))
    {
        PlayerClassStartGame(plr, m_player[plr], FALSE);
    }
    Delete(sub);
}

static void killCreature(int cre, int plr)
{
    int revive=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(cre), GetObjectY(cre));

    SetUnit1C(revive, m_usercam[plr]);
    LookWithAngle(revive, plr);
    SecondTimerWithArg(3, revive, readyToRevive);
    DestroyCreature(cre);
}

static void checkWispDeath(int plr)
{
    int cre=m_creature[plr], cam = m_usercam[plr];
    float limit = GetObjectY(cam)+ALLOW_LIMIT_Y;

    if (limit < GetObjectY(cre))
        killCreature(cre, plr);
}

static void onPlayerRushProc(int plr, int cre, float fInput)
{
    if (ToInt(fInput))
    {
        PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH);
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_LEFT))
            PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH_LEFT);
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_RIGHT))
            PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH_RIGHT);
        return;
    }
    float xvect=0.0, xpos=GetObjectX(cre);

    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_LEFT))
    {
        if (xpos>LEFT_LIMIT)
            xvect-= 5.0;
    }
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_RIGHT))
    {
        if (xpos<RIGHT_LIMIT)
            xvect+= 5.0;
    }
    if (ToInt(xvect))
    {
        MoveObjectVector(cre, xvect, 0.0);
        Effect("YELLOW_SPARKS", GetObjectX(cre), GetObjectY(cre), 0.0,0.0);
    }
}

void PlayerClassOnAlive(int plr, int user)
{
    int cam=m_usercam[plr];

    if (MaxHealth(cam))
    {
        float xvect; //, yvect;
        if (CheckWatchFocus(user))
            PlayerLook(user, cam);
        serverTaskUserInputHandler(plr, user, &xvect); //, &yvect);
        int cre=m_creature[plr];
        if (CurrentHealth(cre))
        {
            if (PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH))
                onPlayerRushProc(plr, cre, xvect);
            else
                onCreaturePositionHandler(plr, xvect);
            MoveObject(m_usercam[plr], GetObjectX(cre), GetObjectY(m_usercam[plr]));
            checkWispDeath(plr);
        }
    }
}

void PlayerClassOnLoop()
{
    int rep = sizeof(m_player);

    while (--rep>=0)
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[rep]))
            {
                if (GetUnitFlags(m_player[rep]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[rep]))
                {
                    PlayerClassOnAlive(rep, m_player[rep]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(rep, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(rep, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(rep, m_player[rep]);
                    }
                    break;
                }                
            }
            if (m_pFlag[rep])
                PlayerClassOnShutdown(rep);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

static void initialServerPatch()
{
    int oldProtect;

    WinApiVirtualProtect(0x51cf84, 1024, 0x40, &oldProtect);
    char *p = 0x51cf84;

    p[23]=0x7a;
    SetMemory(0x51d0c8, 0x513f10);
    WinApiVirtualProtect(0x51cf84, 1024, oldProtect, NULLPTR);
    //46 8B 06 8B 4E 04 89 08 83 C6 09 E9 24 8F 00 00

    WinApiVirtualProtect(0x513f10, 256, 0x40, &oldProtect);

    char servcode[]={0x46, 0x8B, 0x06, 0x8B, 0x4E, 0x04, 0x89, 0x08, 0x83, 0xC6, 0x08, 0xE9, 0x24, 0x8F, 0x00, 0x00};

    NoxByteMemCopy(servcode, 0x513f10, sizeof(servcode));
    WinApiVirtualProtect(0x513f10, 256, oldProtect, NULLPTR);

    SetMemory(0x5c31ec, 0x513f30);
}


static void UnitScanProcessProto(int functionId, int posUnit)
{
    Bind(functionId, &functionId + 4);
}

void UnitScanLoop(int cur)
{
    int rep = 30, action, thingId;

    while (rep--)
    {
        if (++cur >= m_lastCreatedUnit)
        {
            OnEndedUnitScan();
            return;
        }
        thingId=GetUnitThingID(cur);
        if (thingId)
        {
            if (HashGet(m_unitscanHashInstance, thingId, &action, FALSE))
                UnitScanProcessProto(action, cur);
        }
    }
    PushTimerQueue(1, cur, UnitScanLoop);
}

static void startUnitScan(int cur)
{
    UnitScanLoop(cur);
}

static int setWallBox(int box, char *scrName)
{
    SetCallback(box, 9, OnBlockBound);
    if (scrName)
    {
        if (scrName[0]=='0')
        {
            SetCallback(box, 9, OnBlockPowerBound);
            LookWithAngle(box, 128);
        }
    }
    return box;
}

static void createWallBox(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
    int *ptr=UnitToPtr(posUnit);
    int *scrName=ptr[0];

    Delete(posUnit);
    setWallBox( DummyUnitCreateById(OBJ_CARNIVOROUS_PLANT, x,y), scrName );
}

static void onbackmovingBlock2()
{
    if (LocationX(48) < GetObjectX(m_secondMovingBlocks[0]))
    {
        MoveObjectVector(m_secondMovingBlocks[0], -2.0, 0.0);
        MoveObjectVector(m_secondMovingBlocks[1], -2.0, 0.0);
        FrameTimer(1, onbackmovingBlock2);
        return;
    }
    if (m_currentArea==1)
        FrameTimer(1, onmovingBlock2);
}

static void onmovingBlock2()
{
    if (GetObjectX(m_secondMovingBlocks[0]) < LocationX(49))
    {
        MoveObjectVector(m_secondMovingBlocks[0], 2.0, 0.0);
        MoveObjectVector(m_secondMovingBlocks[1], 2.0, 0.0);
        FrameTimer(1, onmovingBlock2);
        return;
    }
    FrameTimer(1, onbackmovingBlock2);
}

static void onbackmovingBlock1()
{
    if (LocationX(13) < GetObjectX(m_firstMovingBlocks[0]))
    {
        MoveObjectVector(m_firstMovingBlocks[0], -2.0, 0.0);
        MoveObjectVector(m_firstMovingBlocks[1], -2.0, 0.0);
        FrameTimer(1, onbackmovingBlock1);
        return;
    }
    if (m_currentArea==1)
        FrameTimer(1, onmovingBlock1);
}

static void onmovingBlock1()
{
    if (GetObjectX(m_firstMovingBlocks[0]) < LocationX(15))
    {
        MoveObjectVector(m_firstMovingBlocks[0], 2.0, 0.0);
        MoveObjectVector(m_firstMovingBlocks[1], 2.0, 0.0);
        FrameTimer(1, onmovingBlock1);
        return;
    }
    FrameTimer(1, onbackmovingBlock1);
}

static void onbackmovingPart2Block1()
{
    if (LocationX(50) < GetObjectX(m_part2MovingBlocks1[0]))
    {
        MoveObjectVector(m_part2MovingBlocks1[0], -2.0, 0.0);
        MoveObjectVector(m_part2MovingBlocks1[1], -2.0, 0.0);
        FrameTimer(1, onbackmovingPart2Block1);
        return;
    }
    if (m_currentArea==2)
        FrameTimer(1, onmovingPart2Block1);
}

static void onmovingPart2Block1()
{
    if (GetObjectX(m_part2MovingBlocks1[0]) < LocationX(51))
    {
        MoveObjectVector(m_part2MovingBlocks1[0], 2.0, 0.0);
        MoveObjectVector(m_part2MovingBlocks1[1], 2.0, 0.0);
        FrameTimer(1, onmovingPart2Block1);
        return;
    }
    FrameTimer(1, onbackmovingPart2Block1);
}

static void movingPart2Backward(int *bls)
{
    int count=8;
    float limit=LocationX(52);
    float baseX=LocationX(53),baseY=LocationY(53);

    while (--count>=0)
    {
        if (GetObjectX(bls[count])>limit)
        {
            MoveObjectVector(bls[count], -2.0, 0.0);
            continue;
        }
        MoveObject(bls[count], baseX,baseY);
    }
    if (m_currentArea==2)
        PushTimerQueue(1, bls, movingPart2Backward);
}

static int macroCreateBox(short location)
{
    return setWallBox(DummyUnitCreateById(OBJ_CARNIVOROUS_PLANT , LocationX(location), LocationY(location)), NULLPTR);
}

static void placingBackwardBlockPart2()
{
    int bls[]={
        macroCreateBox(54),macroCreateBox(55),macroCreateBox(56),macroCreateBox(57),
        macroCreateBox(58),macroCreateBox(59),macroCreateBox(60),macroCreateBox(61),
        };
    PushTimerQueue(1, bls, movingPart2Backward);
}

static void part2BlockMoving(int *bls)
{
    float baseY=LocationY(63);

    if (GetObjectY(bls[0])<=baseY)
    {
        PushTimerQueue(1, bls, part2BlockMoving);
        MoveObjectVector(bls[0], 0.0, 2.0);
        MoveObjectVector(bls[1], 0.0, 2.0);
        MoveObjectVector(bls[2], 0.0, 2.0);
        MoveObjectVector(bls[3], 0.0, -2.0);
        MoveObjectVector(bls[4], 0.0, -2.0);
        MoveObjectVector(bls[5], 0.0, -2.0);
        return;
    }
    float highBaseY=LocationY(64);
    MoveObject(bls[0], GetObjectX(bls[0]), highBaseY);
    MoveObject(bls[1], GetObjectX(bls[1]), highBaseY);
    MoveObject(bls[2], GetObjectX(bls[2]), highBaseY);
    MoveObject(bls[3], GetObjectX(bls[3]), baseY);
    MoveObject(bls[4], GetObjectX(bls[4]), baseY);
    MoveObject(bls[5], GetObjectX(bls[5]), baseY);
    if (m_currentArea==2)
        PushTimerQueue(1, bls, part2BlockMoving);
}

static void startPart2BlockMoving()
{
    int bls[]={
        macroCreateBox(65),
        macroCreateBox(66),
        macroCreateBox(67),
        macroCreateBox(68),
        macroCreateBox(69),
        macroCreateBox(70)
    };
    PushTimerQueue(10, bls, part2BlockMoving);
}

static void part2LastBlockMoving(int *bls)
{
    int count=4;
    float baseY=LocationY(71);

    while(--count>=0)
    {
        if (GetObjectY(bls[count])<baseY)
        {
            MoveObjectVector(bls[count], 0.0, 2.0);
            continue;
        }
        MoveObject(bls[count], GetObjectX(bls[count]), LocationY(72));
    }
    if (m_currentArea==2)
        PushTimerQueue(1, bls, part2LastBlockMoving);
}

static void startPart2LastMoving()
{
    int bls[]={
        macroCreateBox(73),
        macroCreateBox(74),
        macroCreateBox(75),
        macroCreateBox(95)
    };
    PushTimerQueue(10, bls, part2LastBlockMoving);
}

static void exitFromArea()
{
    int plr;

    if (getScrIndexWithCreature(GetCaller(), &plr))
    {
        short location=GetDirection(SELF);

        TeleportCreatureSafety(plr, LocationX(location), LocationY(location));
    }
}

static void placingExit(short location, short destLocation)
{
    int ex=DummyUnitCreateById(OBJ_STONE_GOLEM, LocationX(location), LocationY(location));

    Frozen(CreateObjectAt("Magic", GetObjectX(ex), GetObjectY(ex)), TRUE);
    LookWithAngle(ex, destLocation);
    SetCallback(ex, 9, exitFromArea);
}

static void startPart1()
{
    m_currentArea=1;
    m_firstMovingBlocks[0]=macroCreateBox(13);
    m_firstMovingBlocks[1]=macroCreateBox(14);
    FrameTimer(1, onmovingBlock1);
    m_secondMovingBlocks[0]=macroCreateBox(48);
    m_secondMovingBlocks[1]=macroCreateBox(77);
    FrameTimer(1, onmovingBlock2);
    placingExit(AREA_1_EXIT_POS, AREA_2_START_POS);  //HERE
    // placingExit(90, AREA_2_START_POS);
}

static void startPart2()
{
    m_currentArea=2;
    TeleportLocation(BALL_REVIVE_LOCATION, LocationX(AREA_2_START_POS), LocationY(AREA_2_START_POS));
    //part2 stuff
    m_part2MovingBlocks1[0]=macroCreateBox(50);
    m_part2MovingBlocks1[1]=macroCreateBox(91);
    FrameTimer(10, onmovingPart2Block1);
    FrameTimer(20, placingBackwardBlockPart2);
    startPart2BlockMoving();
    FrameTimer(60, startPart2LastMoving);

    m_downBridgePart2[0]=macroCreateBox(92);
    m_downBridgePart2[1]=macroCreateBox(93);
    m_downBridgePart2[2]=macroCreateBox(94);

    placingExit(AREA_2_EXIT_POS, AREA_3_START_POS);
}

static void part3FirstBackward(int *p)
{
    if (GetObjectX(p[0]) > LocationX(79))
    {
        MoveObjectVector(p[0], -2.0, 0.0);
        MoveObjectVector(p[1], -2.0, 0.0);
        MoveObjectVector(p[2], 2.0, 0.0);
        MoveObjectVector(p[3], 2.0, 0.0);
        PushTimerQueue(1, p, part3FirstBackward);
        return;
    }
    PushTimerQueue(1, p, part3FirstForward);
}

static void part3FirstForward(int *p)
{
    if (GetObjectX(p[0]) < LocationX(78))
    {
        MoveObjectVector(p[0], 2.0, 0.0);
        MoveObjectVector(p[1], 2.0, 0.0);
        MoveObjectVector(p[2], -2.0, 0.0);
        MoveObjectVector(p[3], -2.0, 0.0);
        PushTimerQueue(1, p, part3FirstForward);
        return;
    }
    if (m_currentArea==3)
        PushTimerQueue(1, p, part3FirstBackward);
}

void StartPart3FirstTrap()
{
    int arr[]={macroCreateBox(79),macroCreateBox(80),
        macroCreateBox(81),macroCreateBox(82)};
    
    PushTimerQueue(10, arr, part3FirstForward);
}

static void part3SecondBackward(int *p)
{
    if (GetObjectX(p[0])>LocationX(83))
    {
        MoveObjectVector(p[0], -2.0, 0.0);
        MoveObjectVector(p[1], -2.0, 0.0);
        PushTimerQueue(1, p, part3SecondBackward);
        return;
    }
    if (m_currentArea==3)
        PushTimerQueue(1, p, part3SecondForward);
}

static void part3SecondForward(int *p)
{
    if (GetObjectX(p[0])<LocationX(84))
    {
        MoveObjectVector(p[0], 2.0, 0.0);
        MoveObjectVector(p[1], 2.0, 0.0);
        PushTimerQueue(1, p, part3SecondForward);
        return;
    }
    PushTimerQueue(1, p, part3SecondBackward);
}

void StartPart3SecondBlock()
{
    int arr[]={macroCreateBox(83), macroCreateBox(85)};

    PushTimerQueue(1, arr, part3SecondForward);
}

static void part3ThirdBackward(int *p)
{
    if (GetObjectX(p[0])>LocationX(86))
    {
        MoveObjectVector(p[0], -2.0, 0.0);
        MoveObjectVector(p[1], -2.0, 0.0);
        PushTimerQueue(1, p, part3ThirdBackward);
        return;
    }
    PushTimerQueue(1, p, part3ThirdForward);
}

static void part3ThirdForward(int *p)
{
    if (GetObjectX(p[0])<LocationX(87))
    {
        MoveObjectVector(p[0], 2.0, 0.0);
        MoveObjectVector(p[1], 2.0, 0.0);
        PushTimerQueue(1, p, part3ThirdForward);
        return;
    }
    if (m_currentArea==3)
        PushTimerQueue(1, p, part3ThirdBackward);
}

void StartPart3ThirdBlock()
{
    int arr[]={macroCreateBox(86), macroCreateBox(88)};

    PushTimerQueue(1, arr, part3ThirdForward);
}

void startPart3()
{
    m_currentArea=3;

    TeleportLocation(BALL_REVIVE_LOCATION, LocationX(AREA_3_START_POS), LocationY(AREA_3_START_POS));
    FrameTimer(1, StartPart3FirstTrap);
    FrameTimer(1, StartPart3SecondBlock);
    StartPart3ThirdBlock();

    placingExit(AREA_3_EXIT_POS, AREA_4_START_POS);
}
void startFlamesPart4()
{
    Move(Object("Part4F1"), 98);
    Move(Object("Part4F2"), 99);
    Move(Object("Part4F3"), 100);
    Move(Object("Part4F4"), 101);
    Move(Object("Part4F5"), 102);
    Move(Object("Part4F6"), 103);
    Move(Object("Part4F7"), 110);
}

void startFlamesPart4A()
{
    Move(Object("Part4F1A"), 112);
    Move(Object("Part4F2A"), 113);
    Move(Object("Part4F3A"), 114);
    Move(Object("Part4F4A"), 115);
    Move(Object("Part4F5A"), 116);
    Move(Object("Part4F6A"), 117);
}

void startFlamesPart4B()
{
    Move(Object("Part4BF1"), 124);
    Move(Object("Part4BF2"), 125);
    Move(Object("Part4BF3"), 134);
    Move(Object("Part4BF4"), 135);
}

float GetFlameYpos(int f)
{
    int*p=UnitToPtr(f);

    return ToFloat( p[15] );
}

void revHandlePart4CFlames(int *p)
{
    int dur = GetDirection(p[0]);

    if (dur)
    {
        LookWithAngle(p[0],dur-1);
        int count=16;
        while(--count>=0)
            MoveObjectVector(p[count],0.0,-3.0);
        PushTimerQueue(1,p,revHandlePart4CFlames);
        return;
    }
    LookWithAngle(p[0], 92);
    PushTimerQueue(1, p,handlePart4CFlames);
}

void handlePart4CFlames(int *p)
{
    int dur = GetDirection(p[0]);

    if (dur)
    {
        LookWithAngle(p[0],dur-1);
        int count=16;
        while(--count>=0)
            MoveObjectVector(p[count], 0.0, 3.0);
        PushTimerQueue(1, p, handlePart4CFlames);
        return;
    }
    LookWithAngle(p[0], 92);
    if (m_currentArea==4)
        PushTimerQueue(1, p, revHandlePart4CFlames);
}

void placingPart4CFlames()
{
    int flames[]={
        CreateObject("LargeBlueFlame", 136),
        CreateObject("LargeBlueFlame", 137),
        CreateObject("LargeBlueFlame", 138),
        CreateObject("LargeBlueFlame", 139),
        CreateObject("LargeBlueFlame", 140),
        CreateObject("LargeBlueFlame", 141),
        CreateObject("LargeBlueFlame", 142),
        CreateObject("LargeBlueFlame", 143),
        CreateObject("LargeBlueFlame", 144),
        CreateObject("LargeBlueFlame", 145),
        CreateObject("LargeBlueFlame", 146),
        CreateObject("LargeBlueFlame", 147),
        CreateObject("LargeBlueFlame", 148),
        CreateObject("LargeBlueFlame", 149),
        CreateObject("LargeBlueFlame", 150),
        CreateObject("LargeBlueFlame", 151),
    };
    LookWithAngle(flames[0], 92);
    PushTimerQueue(1,flames,handlePart4CFlames);
}

int part4BridgeSingleBackward(int b)
{
    if (GetObjectY(b)>=LocationY(159))
    {
        MoveObjectVector(b, 0.0, -2.0);
        return TRUE;
    }
    SetUnit1C(b, GetUnit1C(b)^TRUE);
    return FALSE;
}

int part4BridgeSingleForward(int b)
{
    if (GetObjectY(b)<=LocationY(168))
    {
        MoveObjectVector(b, 0.0, 2.0);
        return TRUE;
    }
    SetUnit1C(b, GetUnit1C(b)^TRUE);
    return FALSE;
}

void part4BridgeSingleHandle(int b)
{
    int forward=GetUnit1C(b)==0;
    
    if (forward)
    {
        if (!part4BridgeSingleForward(b))
            part4BridgeSingleBackward(b);
        return;
    }
    if (!part4BridgeSingleBackward(b))
        part4BridgeSingleForward(b);    
}

void handlePart4Bridge(int *p)
{
    int count=16;

    while (--count>=0)
        part4BridgeSingleHandle(p[count]);
    if (m_currentArea==4)
        PushTimerQueue(1, p, handlePart4Bridge);
}

void placingPart4Bridge()
{
    int bridge[]={
        macroCreateBox(152),
        macroCreateBox(153),
        macroCreateBox(154),
        macroCreateBox(155),
        macroCreateBox(156),
        macroCreateBox(157),
        macroCreateBox(158),
        macroCreateBox(159),
        macroCreateBox(160),
        macroCreateBox(161),
        macroCreateBox(162),
        macroCreateBox(163),
        macroCreateBox(164),
        macroCreateBox(165),
        macroCreateBox(166),
        macroCreateBox(167),
    };
    int count =sizeof(bridge);
    while(--count>=0)
        SetUnit1C( bridge[count], (GetObjectX(bridge[count]) > LocationX(159)));
    handlePart4Bridge(bridge);
}

#define PART4_LASTMOVING_SPEED 3.0

void part4LastMovingBackwardLoop(int *p)
{
    if (GetObjectX(p[0])>LocationX(175))
    {
        MoveObjectVector(p[0], -PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[1], -PART4_LASTMOVING_SPEED, 0.0);

        MoveObjectVector(p[2], PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[3], PART4_LASTMOVING_SPEED, 0.0);

        MoveObjectVector(p[4], -PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[5], -PART4_LASTMOVING_SPEED, 0.0);

        MoveObjectVector(p[6], PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[7], PART4_LASTMOVING_SPEED, 0.0);
        PushTimerQueue(1, p, part4LastMovingBackwardLoop);
        return;
    }
    if (m_currentArea!=4)
        return;
    PushTimerQueue(1, p, part4LastMovingForwardLoop);
}

void part4LastMovingForwardLoop(int *p)
{
    if (GetObjectX(p[0])<LocationX(183))
    {
        MoveObjectVector(p[0], PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[1], PART4_LASTMOVING_SPEED, 0.0);

        MoveObjectVector(p[2], -PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[3], -PART4_LASTMOVING_SPEED, 0.0);

        MoveObjectVector(p[4], PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[5], PART4_LASTMOVING_SPEED, 0.0);

        MoveObjectVector(p[6], -PART4_LASTMOVING_SPEED, 0.0);
        MoveObjectVector(p[7], -PART4_LASTMOVING_SPEED, 0.0);
        PushTimerQueue(1, p, part4LastMovingForwardLoop);
        return;
    }
    if (m_currentArea!=4)
        return;
    PushTimerQueue(1, p, part4LastMovingBackwardLoop);
}

void startPart4LastMoving()
{
    int blocks[]={
        macroCreateBox(175),macroCreateBox(176),
        macroCreateBox(177),macroCreateBox(178),
        macroCreateBox(179),macroCreateBox(180),
        macroCreateBox(181),macroCreateBox(182),
    };
    PushTimerQueue(1, blocks, part4LastMovingForwardLoop);
}

static void startPart4()
{
    m_currentArea=4;
    FrameTimer(30, startFlamesPart4);
    FrameTimer(35, startFlamesPart4A);
    FrameTimer(40, startFlamesPart4B);
    FrameTimer(31, placingPart4CFlames);
    FrameTimer(5, placingPart4Bridge);
    PushTimerQueue(10, 0, startPart4LastMoving);

    TeleportLocation(BALL_REVIVE_LOCATION, LocationX(AREA_4_START_POS), LocationY(AREA_4_START_POS));
    placingExit(AREA_4_EXIT_POS, AREA_5_START_POS);
}

static void startPart5()
{
    m_currentArea=5;
    TeleportLocation(BALL_REVIVE_LOCATION, LocationX(AREA_5_START_POS), LocationY(AREA_5_START_POS));
    UniPrintToAll("축하합니다! 모든 구간을 클리어 했습니다!");
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
}

static void initReadables()
{
    RegistSignMessage(Object("mpic1"), "-공튀기기 시즌1 - 20230823빌드 - 제작자. 237- 재미있게 즐겨주세요-");
    RegistSignMessage(Object("mpic2"), "공을 튀기시면 됩니다. 공이 벼랑으로 떨어지면 게임이 리셋됩니다");
    RegistSignMessage(Object("mpic3"), "곳곳에 장애물이 도사리고 있고, 이 게임은 최대 3라운드 까지만 만들었슴니돠");
    RegistSignMessage(Object("mpic4"), "버그제보 환영- 버그 발견 시 여기로 댓글ㄲㄲ blog.naver.com/ky10613 블로그채널");
    RegistSignMessage(Object("mpic5"), "원격 공 제어 운전실 입장하는 곳. 옆에 발판을 밟으셈!");
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("bounce-log.txt");
    m_lastCreatedUnit=CreateObjectAt("SpellBook", 100.0, 100.0);
    m_clientKeyState = 0x751000;
    HashCreateInstance(& m_creatureHash );
    FrameTimer(1, MakeCoopTeam);
    PushTimerQueue(1, Object("firstscan"),startUnitScan);
    initReadables();
    initialServerPatch();
    // initMovingBlocks();
    HashCreateInstance(&m_unitscanHashInstance);
    HashPushback(m_unitscanHashInstance, 2675, createWallBox);
    FrameTimer(10, PlayerClassOnLoop);
    FrameTimer(60, serverInputLoop);
    blockObserverMode();
}

int CheckPlayer()
{
    int rep = sizeof(m_player);

    while(--rep>=0)
    {
        if (IsCaller(m_player[rep]))
            break;
    }
    return rep;
}

int CheckPlayerWithId(int pUnit)
{
    int rep=sizeof(m_player);

    while (--rep>=0)
    {
        if (m_player[rep]^pUnit)
            continue;       
        break;
    }
    return rep;
}

int PlayerClassOnInit(int plr, int pUnit, char *userInit)
{
    m_player[plr]=pUnit;
    m_pFlag[plr]=1;
    m_pDeath[plr]=0;
    m_usercam[plr]=createPlayerCamera(pUnit, LocationX(BALL_REVIVE_LOCATION), LocationY(BALL_REVIVE_LOCATION)+PLAYER_CAM_DISTANCE);

    // if (ValidPlayerCheck(pUnit))
    // {
    //     userInit[0] = TRUE;
    // }
    m_clientKeyState[ GetPlayerIndex(pUnit) ] = 0;
    ChangeGold(pUnit, -GetGold(pUnit));
    return plr;
}

void OnCreatureCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(SELF))
    {
        if (HasClass(OTHER, "DANGEROUS"))
        {
            Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            int plr;

            if (!HashGet(m_creatureHash, GetTrigger(), &plr, FALSE))
                UniPrintToAll("ASSERT-OnCreatureCollide-hash error");
            killCreature(GetTrigger(), plr);
        }
    }
}

// int WillOWispBinTable()
// {
// 	int *pArr;

// 	if (pArr)	return pArr;
// 	int arr[62];

// 	arr[0] = 1819044183; arr[1] = 1936283471; arr[2] = 112; arr[16] = 100000; arr[17] = 50; 
// 		arr[18] = 125; arr[19] = 50; arr[21] = 1056964608; arr[23] = 1; arr[53] = 1128792064; 
// 		arr[54] = 4; arr[60] = 1326; arr[61] = 46913280; 
// 	pArr = &arr;
// 	return pArr;
// }

// void WillOWispSubProcess(int sUnit)
// {
// 	int *ptr = UnitToPtr(sUnit);

// 	if (!ptr) return;

// 	ptr[136] = 1069547520;		ptr[137] = 1069547520;
// 	int *hpTable = ptr[139];
// 	hpTable[0] = 50;	hpTable[1] = 50;
// 	int *uec = ptr[187];
// 	uec[360] = 1;		uec[121] = WillOWispBinTable();
// 	uec[339] = 0;		uec[334] = 0;		uec[336] = 1056964608;
// }

void onBallDie(){
    Effect("SPARK_EXPLOSION",GetObjectX(SELF),GetObjectY(SELF),0.0,0.0);
    Effect("EXPLOSION",GetObjectX(SELF),GetObjectY(SELF),0.0,0.0);
    DeleteObjectTimer(SELF, 1);
}

int createPlayerCreature(int user, float xpos, float ypos)
{
    int cre = CreateObjectById(OBJ_WIZARD, xpos, ypos);
    char dirs[]={0,64,128,192,};

    // WillOWispSubProcess(cre);
    // SetUnitFlags(cre, GetUnitFlags(cre)^UNIT_FLAG_AIRBORNE);
    SetOwner(user, cre);
    SetCallback(cre, 5, onBallDie);
    SetCallback(cre, 9, OnCreatureCollide);
    LookWithAngle(cre,dirs[ Random(0,3) ]);
    SetUnitVoice(cre,MONSTER_VOICE_Bomber);
    // AggressionLevel(cre, 0.0);
    Frozen(cre, TRUE);
    return cre;
}

static int createPlayerCamera(int user, float xpos, float ypos)
{
    int cam = DummyUnitCreateById(OBJ_AIRSHIP_CAPTAIN , xpos, ypos);

    SetOwner(user, cam);
    UnitNoCollide(cam);
    return cam;
}

static void DestroyCreature(int cre)
{
    if (CurrentHealth(cre))
    {
        Frozen(cre, FALSE);
        Damage(cre, 0, 9999, DAMAGE_TYPE_PLASMA);
        HashGet(m_creatureHash, cre, NULLPTR, TRUE);
    }
}

static void RemoveCreature(int cre)
{
    if (MaxHealth(cre))
    {
        HashGet(m_creatureHash, cre, NULLPTR, TRUE);
        Delete(cre);
    }
}

static void RemoveCamera(int cam)
{
    if (MaxHealth(cam))
        Delete(cam);
}

static void syncCameraWithBall(int cam)
{
    MoveObject(cam, LocationX(BALL_REVIVE_LOCATION), LocationY(BALL_REVIVE_LOCATION)+PLAYER_CAM_DISTANCE);
}

static void PlayerClassStartGame(int plr, int user, int initial)
{
    RemoveCreature(m_creature[plr]);

    if (PlayerClassCheckFlag(plr,PLAYER_FLAG_RUSH))
        PlayerClassSetFlag(plr,PLAYER_FLAG_RUSH);
    m_creFall[plr]=0;
    m_creJump[plr]=0;

    m_creature[plr]=createPlayerCreature(user, LocationX(BALL_REVIVE_LOCATION), LocationY(BALL_REVIVE_LOCATION));

    syncCameraWithBall(m_usercam[plr]);
    HashPushback(m_creatureHash, m_creature[plr], plr);
    if (initial)
    {
        Enchant(user, EnchantList(ENCHANT_FREEZE), 0.0);
        MoveObject(user, LocationX(PLAYER_CONTROL_ROOM), LocationY(PLAYER_CONTROL_ROOM));
    }
}

void PlayerClassOnJoin(int plr)
{
    int user = m_player[plr];

    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    PlayerClassStartGame(plr, user, TRUE);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(PLAYER_INITIAL_POS), LocationY(PLAYER_INITIAL_POS));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int plr)
{
    MoveObject(user, LocationX(PLAYER_INITIAL_POS), LocationY(PLAYER_INITIAL_POS));
    Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    PushTimerQueue(3, plr, PlayerClassOnJoin);
    UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
}

void PlayerClassOnEntry(int plrUnit)
{
    WriteLog("PlayerClassOnEntry:start");
    while (TRUE)
    {
        if (!CurrentHealth(plrUnit))
            break;
        int plr = CheckPlayerWithId(plrUnit), rep = sizeof(m_player);
        char initialUser=FALSE;

        while (--rep>=0&&plr<0)
        {
            if (!MaxHealth(m_player[rep]))
            {
                plr = PlayerClassOnInit(rep, plrUnit, &initialUser);
                PushTimerQueue(66, plrUnit, ThisMapInformationPrintMessage);
                break;
            }
        }
        if (plr >= 0)
        {
            // if (initialUser)
            //     OnPlayerDeferredJoin(plrUnit, plr);
            // else
                PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(plrUnit);
        break;
    }
    WriteLog("PlayerClassOnEntry:end");
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (CheckPlayer() >= 0)
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(PLAYER_INITIAL_POS), LocationY(PLAYER_INITIAL_POS));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

int TryStraight(int wisp, int dir)
{
    int plr;

    if (!getScrIndexWithCreature(wisp, &plr))
        return FALSE;

    if (!PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH))
        PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH);
    if (dir==PLAYER_FLAG_RUSH_LEFT)
    {
        if (!PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_LEFT))
            PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH_LEFT);
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_RIGHT))
            PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH_RIGHT);
    }
    else if (dir==PLAYER_FLAG_RUSH_RIGHT)
    {
        if (!PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_RIGHT))
            PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH_RIGHT);
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_RUSH_LEFT))
            PlayerClassSetFlag(plr, PLAYER_FLAG_RUSH_LEFT);
    }
    m_creFall[plr]=0;
    m_creJump[plr]=0;
}

static void wrapEnableObject(int obj)
{
    if (ToInt(GetObjectX(obj)))
        ObjectOn(obj);
}

void StraightRight()
{
    ObjectOff(SELF);
    PushTimerQueue(15, GetTrigger(), wrapEnableObject);
    TryStraight(GetCaller(), PLAYER_FLAG_RUSH_RIGHT);
}

void StraightLeft()
{
    ObjectOff(SELF);
    PushTimerQueue(15, GetTrigger(), wrapEnableObject);
    TryStraight(GetCaller(), PLAYER_FLAG_RUSH_LEFT);
}

static void putDownBridge(int *bls)
{
    if (GetObjectY(bls[0])<=LocationY(62))
    {
        MoveObjectVector(bls[0], 0.0, 2.0);
        MoveObjectVector(bls[1], 0.0, 2.0);
        MoveObjectVector(bls[2], 0.0, 2.0);
        PushTimerQueue(1, bls, putDownBridge);
    }
}

void DownBridge()
{
    ObjectOff(SELF);

    PushTimerQueue(10, m_downBridgePart2, putDownBridge);
}

void StartArea1()
{
    if (IsPlayerUnit(GetOwner(OTHER)))
    {
        ObjectOff(SELF);
        startPart1();
    }
}
void StartArea2()
{
    if (IsPlayerUnit(GetOwner(OTHER)))
    {
        ObjectOff(SELF);
        startPart2();
    }
}

void StartArea3()
{
    if (IsPlayerUnit(GetOwner(OTHER)))
    {
        ObjectOff(SELF);
        startPart3();
    }
}

void StartArea4()
{
    if (IsPlayerUnit(GetOwner(OTHER)))
    {
        ObjectOff(SELF);
        startPart4();
    }
}

void StartArea5()
{
    if (IsPlayerUnit(GetOwner(OTHER)))
    {
        ObjectOff(SELF);
        startPart5();
    }
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    if (pUnit)
        PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
