
#include "lzz99mh_utils.h"
#include "libs/unitstruct.h"
#include "libs/queueTimer.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"
#include "libs/bind.h"

#define SPAWN_RATE_HIGH 0
#define SPAWN_RATE_NORMAL 1
#define SPAWN_RATE_LOW 2
#define SPAWN_RATE_VERY_LOW 3
#define SPAWN_RATE_VERYVERY_LOW 4

#define SPAWN_LIMIT_HIGH 0
#define SPAWN_LIMIT_NORMAL 1
#define SPAWN_LIMIT_LOW 2
#define SPAWN_LIMIT_SINGULAR 3

#define GROUP_thirdPartWalls3 19
#define GROUP_thirdPartWalls4 20

void MonsterGeneratorIdTable(short input, short *dest)
{
    short *pTable;
    short idTable[97];

    if (!pTable)
    {
        pTable=idTable;
        
        idTable[OBJ_URCHIN%97]=OBJ_URCHIN_GENERATOR;
        idTable[OBJ_BAT%97]=OBJ_BAT_GENERATOR;
        idTable[OBJ_GIANT_LEECH%97]=OBJ_GIANT_LEECH_GENERATOR;
        idTable[OBJ_ALBINO_SPIDER%97]=OBJ_ALBINO_SPIDER_GENERATOR;
        idTable[OBJ_WOLF%97]=OBJ_WOLF_GENERATOR;
        idTable[OBJ_ARCHER%97]=OBJ_ARCHER_GENERATOR;
        idTable[OBJ_SWORDSMAN%97]=OBJ_SWORDSMAN_GENERATOR;
        idTable[OBJ_SCORPION%97]=OBJ_SCORPION_GENERATOR;
        idTable[OBJ_SPIDER%97]=OBJ_SPIDER_GENERATOR;
        idTable[OBJ_BLACK_BEAR%97]=OBJ_BEAR_GENERATOR;
        idTable[OBJ_BEAR%97]=OBJ_BEAR_GENERATOR;
        idTable[OBJ_WIZARD%97]=OBJ_WIZARD_GENERATOR;
        idTable[OBJ_EVIL_CHERUB%97]=OBJ_EVIL_CHERUB_GENERATOR;
        idTable[OBJ_SHADE%97]=OBJ_SHADE_GENERATOR;
        idTable[OBJ_OGRE_WARLORD%97]=OBJ_OGRE_WARLORD_GENERATOR;
        idTable[OBJ_GRUNT_AXE%97]=OBJ_GRUNT_AXE_GENERATOR;
        idTable[OBJ_OGRE_BRUTE%97]=OBJ_OGRE_BRUTE_GENERATOR;
        idTable[OBJ_SKELETON%97]=OBJ_SKELETON_GENERATOR;
        idTable[OBJ_HORRENDOUS%97]=OBJ_HORRENDOUS_GENERATOR;
        idTable[OBJ_TROLL%97]=OBJ_TROLL_GENERATOR;
        idTable[OBJ_VILE_ZOMBIE%97]=OBJ_VILE_ZOMBIE_GENERATOR;
        idTable[OBJ_FLYING_GOLEM%97]=OBJ_FLYING_GOLEM_GENERATOR;
        idTable[OBJ_SKELETON_LORD%97]=OBJ_SKELETON_LORD_GENERATOR;
        idTable[OBJ_LICH%97]=OBJ_LICH_GENERATOR;
        idTable[OBJ_BOMBER_BLUE%97]=OBJ_BOMBER_GENERATOR;
        idTable[OBJ_DEMON%97]=OBJ_DEMON_GENERATOR;
        idTable[OBJ_MECHANICAL_GOLEM%97]=OBJ_MECHANICAL_GOLEM_GENERATOR;
        idTable[OBJ_HECUBAH_WITH_ORB%97]=OBJ_WIZARD_GREEN_GENERATOR;
        idTable[OBJ_BEHOLDER%97]=OBJ_BEHOLDER_GENERATOR;
        idTable[OBJ_EMBER_DEMON%97]=OBJ_EMBER_DEMON_GENERATOR;
    }
    dest[0]=idTable[input%97];
}

void ProduceMonsterGenerator(float xpos, float ypos, int unitSlot1, int unitSlot2, int unitSlot3, int *pDestGen)
{
    int gen = CreateObjectAt("MonsterGenerator", xpos, ypos);
    int *ptr = UnitToPtr(gen);

    if (pDestGen)
        pDestGen[0] = gen;
    int *uec = ptr[187];

    int *slotPtr;
    AllocateXtraObjectSpec(unitSlot1, &slotPtr);
    if (slotPtr[4]&UNIT_FLAG_RESPAWN)
        slotPtr[4]^=UNIT_FLAG_RESPAWN;
    uec[0] = slotPtr;
    AllocateXtraObjectSpec(unitSlot2, &slotPtr);
    uec[4] = slotPtr;
    AllocateXtraObjectSpec(unitSlot3, &slotPtr);
    uec[8] = slotPtr;
    uec[23] = 1;

    short genmodel;

    MonsterGeneratorIdTable(unitSlot1, &genmodel);
    if (genmodel)
        ptr[1]=genmodel;
}

void ModificationMonsterGeneratorSummonInfo(int mobgen, int delayLv, int capaLv)
{
    int *ptr = UnitToPtr(mobgen);
    int *uec = ptr[187];

    delayLv &= 7;
    capaLv &= 7;

    uec[20] = delayLv|(delayLv<<8)|(delayLv<<16)|(capaLv<<24);
    uec[21] = capaLv|(capaLv<<8);
}

void SetMonsterGeneratorCallbackOnProduce(int mobgen, int functionId)
{
    int *ptr=UnitToPtr(mobgen);
    int *uec=ptr[187];

    uec[17] = functionId;
}

void ProduceTestGenerator(int locationId)
{
    int mobgen;

    ProduceMonsterGenerator(LocationX(locationId), LocationY(locationId), 1346, 1346, 1346, &mobgen);
    ModificationMonsterGeneratorSummonInfo(mobgen, 3, 3);
    SetMonsterGeneratorCallbackOnProduce(mobgen, GenOnProduce);
}

void OnGeneratorMonsterDeath(int mob) //virtual
{
    DeleteObjectTimer(mob, 150);
}

void GetUnitOnDeath()
{
    int *spawnClass = GetMemory( 0x81aebc );
    
    --spawnClass[35];

    //ec+0x890 = obelisk ptr
    //ec+0x894 = dwordptr->ptr that it's me
    int *ptr = UnitToPtr(SELF);
    int *ec = ptr[187];
    int *mobgenPtr = ec[548];

    if (mobgenPtr)
    {
        int *mobgenEc = mobgenPtr[187];
        int *curCount = mobgenEc + 86;

        if (curCount[0] & 0xff)
            --curCount[0];
    }
    OnGeneratorMonsterDeath(GetTrigger());

    // UniPrintToAll(IntToString(ec[548]));
    // UniPrintToAll(IntToString(ec[549]));
}

void GenProduceProcPkModeOnly()
{
    int *pSpawnClassOff = 0x81aebc;
    int *spawnClass = pSpawnClassOff[0];
    int *produceSlot = spawnClass[28];

    produceSlot[2] = produceSlot-28;
    produceSlot[4] = 0xacacacac;
    produceSlot[5] = 0xacacacac;
    produceSlot[6] = 0xacacacac;

    spawnClass[24] = produceSlot;
    spawnClass[28] = 0;
}

void OnGeneratorSummoned(int gen, int mob) //virtual
{
    Effect("SENTRY_RAY", GetObjectX(gen),GetObjectY(gen),GetObjectX(mob),GetObjectY(mob));
}

void GenOnProduce()
{
    SetCallback(OTHER, 5, GetUnitOnDeath);
    GenProduceProcPkModeOnly();
    OnGeneratorSummoned(GetTrigger(), GetCaller());
}

void OnGeneratorDestroyed(int gen)
{
    GreenExplosion(GetObjectX(gen), GetObjectY(gen));
    PlaySoundAround(gen, SOUND_MonsterGeneratorDie);
}

void GenOnDestroyed()
{
    OnGeneratorDestroyed(GetTrigger());
    Delete(SELF);
}

int CreateMonsterGenerator(short locationId, short mobId, int delayLv, int capaLv)
{
    int mobgen;

    ProduceMonsterGenerator(LocationX(locationId), LocationY(locationId), mobId, mobId, mobId, &mobgen);
    ModificationMonsterGeneratorSummonInfo(mobgen, delayLv, capaLv);
    SetMonsterGeneratorCallbackOnProduce(mobgen, GenOnProduce);
    SetUnitCallbackOnDeath(mobgen, GenOnDestroyed);
    SetUnitMaxHealth(mobgen, 475);
    return mobgen;
}

void InitGenSetting()
{
    int *pSpawnClassOff = 0x81aebc;
    int *spawnClass = pSpawnClassOff[0];

    spawnClass[24] = spawnClass[29] + 0xa64;
    spawnClass[25] = spawnClass[29];
}

void PutGeneratorArea1()
{
    CreateMonsterGenerator(11, OBJ_URCHIN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(12, OBJ_BAT, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(13, OBJ_URCHIN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);

    CreateMonsterGenerator(14, OBJ_GIANT_LEECH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(15, OBJ_ALBINO_SPIDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(16, OBJ_WOLF, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);

    CreateMonsterGenerator(17, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(18, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(19, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);

    CreateMonsterGenerator(20, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(21, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);

    CreateMonsterGenerator(22, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea1P1()
{
    CreateMonsterGenerator(39, OBJ_SCORPION, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(38, OBJ_GIANT_LEECH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(40, OBJ_SPIDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(36, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(37, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea1P2()
{
    CreateMonsterGenerator(32, OBJ_SCORPION, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(33, OBJ_URCHIN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(34, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(26, OBJ_BLACK_BEAR, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(27, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(28, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void SurpriseGenArea1P3(int gen)
{
    Effect("SMOKE_BLAST", GetObjectX(gen),GetObjectY(gen),0.0,0.0);
    UniChatMessage(gen, "짜자자잔~~!", 120);
}

void PutGeneratorArea1P3()
{
    SurpriseGenArea1P3(CreateMonsterGenerator(24, OBJ_ARCHER, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW));
    SurpriseGenArea1P3(CreateMonsterGenerator(29, OBJ_ARCHER, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW));
    SurpriseGenArea1P3(CreateMonsterGenerator(30, OBJ_ARCHER, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW));
    SurpriseGenArea1P3(CreateMonsterGenerator(31, OBJ_ARCHER, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW));
}

void PutGeneratorArea1P4()
{
    CreateMonsterGenerator(107, OBJ_BLACK_BEAR, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(108, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(109, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea1P5()
{
    CreateMonsterGenerator(42, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(41, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(43, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(44, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(45, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(46, OBJ_SPIDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(47, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(49, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(48, OBJ_BEAR, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(50, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(51, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(53, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
}

void PutGeneratorArea1P6()
{
    CreateMonsterGenerator(63, OBJ_GRUNT_AXE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(64, OBJ_GRUNT_AXE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(59, OBJ_SKELETON, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(60, OBJ_SKELETON, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(67, OBJ_SKELETON, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(70, OBJ_SKELETON, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(62, OBJ_OGRE_WARLORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(65, OBJ_OGRE_WARLORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(68, OBJ_OGRE_BRUTE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(69, OBJ_OGRE_BRUTE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(66, OBJ_OGRE_WARLORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(76, OBJ_GRUNT_AXE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(74, OBJ_WIZARD, SPAWN_RATE_LOW, SPAWN_LIMIT_SINGULAR);
}

void PutGeneratorArea1P6_1()
{
    SurpriseGenArea1P3(CreateMonsterGenerator(71, OBJ_GRUNT_AXE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW) );
    SurpriseGenArea1P3(CreateMonsterGenerator(72, OBJ_GRUNT_AXE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW) );
    SurpriseGenArea1P3(CreateMonsterGenerator(73, OBJ_OGRE_BRUTE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW) );
}

void PutGeneratorArea1P7()
{
    CreateMonsterGenerator(79, OBJ_FLYING_GOLEM, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(80, OBJ_FLYING_GOLEM, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(85, OBJ_SHADE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(341, OBJ_OGRE_WARLORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea1P8()
{
    CreateMonsterGenerator(82, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(83, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(84, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(78, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
}

void PutGeneratorArea1P9()
{
    CreateMonsterGenerator(126, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(127, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(128, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(129, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(125, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea2P1()
{
    CreateMonsterGenerator(122, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(123, OBJ_SKELETON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(124, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(119, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(120, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(121, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(130, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(131, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(132, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(133, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(134, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea2P2()
{
    CreateMonsterGenerator(139, OBJ_TROLL, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(140, OBJ_TROLL, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(141, OBJ_TROLL, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(142, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(143, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(144, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(145, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(146, OBJ_SKELETON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(147, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(148, OBJ_TROLL, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(149, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea2P3()
{
    CreateMonsterGenerator(154, OBJ_SKELETON_LORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(150, OBJ_EVIL_CHERUB, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(151, OBJ_EVIL_CHERUB, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(152, OBJ_EVIL_CHERUB, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(153, OBJ_EVIL_CHERUB, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(205, OBJ_VILE_ZOMBIE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(206, OBJ_WIZARD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea2P4()
{
    CreateMonsterGenerator(155, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(156, OBJ_TROLL, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(157, OBJ_VILE_ZOMBIE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(158, OBJ_VILE_ZOMBIE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(159, OBJ_SKELETON_LORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(160, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(161, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(162, OBJ_EVIL_CHERUB, SPAWN_RATE_NORMAL, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(163, OBJ_EVIL_CHERUB, SPAWN_RATE_NORMAL, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(164, OBJ_EVIL_CHERUB, SPAWN_RATE_NORMAL, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(165, OBJ_EVIL_CHERUB, SPAWN_RATE_NORMAL, SPAWN_LIMIT_NORMAL);
}

void PutGeneratorArea2P5()
{
    CreateMonsterGenerator(166, OBJ_VILE_ZOMBIE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(167, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(168, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(169, OBJ_SHADE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(170, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGeneratorArea2P6()
{
    CreateMonsterGenerator(173, OBJ_SHADE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(180, OBJ_SHADE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(186, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(187, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGenArea1Surprise1()
{
    CreateMonsterGenerator(196, OBJ_SPIDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(197, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGenArea1Surprise2()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(202, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(203, OBJ_EVIL_CHERUB, SPAWN_RATE_NORMAL, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(204, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGenArea3Part1()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(207, OBJ_BOMBER_BLUE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(208, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(209, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(210, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(211, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(212, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGenArea3Point1()
{
    CreateMonsterGenerator(251, OBJ_BOMBER_BLUE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(252, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(253, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(254, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void generatorRaid1(int *p)
{
    int i = 0;
    p[i++]=CreateMonsterGenerator(87, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(88, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(89, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(90, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(91, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(92, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void generatorRaid2(int *p)
{
    int i = 0;
    p[i++]=CreateMonsterGenerator(87, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(88, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(89, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(90, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(91, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(92, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void generatorRaid3(int *p)
{
    int i = 0;
    p[i++]=CreateMonsterGenerator(87, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(88, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(89, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(90, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(91, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[i++]=CreateMonsterGenerator(92, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void invokeGenRaid(int f, int *p)
{
    Bind(f, &f + 4);
}

void raidCheckLoop(int *p)
{
    int count = 6;
    int sum=0;

    while (--count>=0)
        sum+=CurrentHealth(p[count]);

    if (sum)
    {
        PushTimerQueue(150, p, raidCheckLoop);
        return;
    }
    area3Part3StartRaid();
}

void raidDoFX(int *p)
{
    int count=6;

    while (--count>=0)
    {
        Effect("YELLOW_SPARKS", GetObjectX(p[count]), GetObjectY(p[count]), 0.0, 0.0);
    }
}

void area3Part3StartRaid()
{
    int count;
    int fn[]={generatorRaid1, generatorRaid2, generatorRaid3, };
    int gens[6];

    if (count < 3)
    {
        invokeGenRaid(fn[count++], gens);
        raidCheckLoop(gens);
        raidDoFX(gens);
        return;
    }
    UnlockDoor(Object("raidGate1"));
    UnlockDoor(Object("raidGate2"));
    UnlockDoor(Object("raidGate3"));
    UnlockDoor(Object("raidGate4"));
    PutGenArea3Point1();
    WallGroupOpen(GROUP_thirdPartWalls3);
}

void area3Part3Raid(int gen)
{
    if (CurrentHealth(gen))
    {
        PushTimerQueue(150, gen, area3Part3Raid);
        return;
    }
    area3Part3StartRaid();
}

void PutGenArea3Part2()
{
    CreateMonsterGenerator(219, OBJ_BOMBER_BLUE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(220, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(221, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    area3Part3Raid(CreateMonsterGenerator(233, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW));
}

#define GROUP_undergroundWalls1 14
#define GROUP_undergroundWalls2 15
#define GROUP_undergroundWalls3 16
#define GROUP_undergroundWalls4 17
#define GROUP_undergroundWalls5 18

void PutGenAreaP1Part1()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_undergroundWalls1);
    CreateMonsterGenerator(234, OBJ_GRUNT_AXE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(235, OBJ_OGRE_BRUTE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(236, OBJ_GRUNT_AXE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGenAreaP1Part2()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_undergroundWalls2);
    CreateMonsterGenerator(237, OBJ_SKELETON_LORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(238, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(239, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(240, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGenAreaP1Part3()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_undergroundWalls3);
    CreateMonsterGenerator(241, OBJ_SKELETON_LORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(242, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(243, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(244, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(245, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(246, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(247, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PutGenAreaP1Part4()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_undergroundWalls4);
    CreateMonsterGenerator(248, OBJ_LICH, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(249, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(250, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void openUnderWalls5()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_undergroundWalls5);
}

void openThirdPartWalls4()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(255, OBJ_OGRE_WARLORD, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(256, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(257, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(258, OBJ_GRUNT_AXE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(259, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(260, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(261, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(262, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    WallGroupOpen(GROUP_thirdPartWalls4);
}

void ThirdPartGen()
{
    CreateMonsterGenerator(263, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(264, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(265, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(266, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(267, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(268, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(269, OBJ_SCORPION, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(270, OBJ_OGRE_BRUTE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void doThirdPartGen2()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(276, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(277, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(278, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(279, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(280, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void doThirdPartGen3()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(271, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(272, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(273, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(274, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(275, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void doUndergroundPartgen()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(281, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(282, OBJ_BLACK_BEAR, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(283, OBJ_OGRE_BRUTE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(284, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(285, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(286, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(287, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void FinalPartGen1()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(290, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(291, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(292, OBJ_BEHOLDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(293, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(294, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(295, OBJ_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(296, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(297, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(298, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void FinalPartGen2()
{
    CreateMonsterGenerator(299, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(300, OBJ_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(301, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(302, OBJ_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(303, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(304, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(305, OBJ_EMBER_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(306, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(309, OBJ_BEHOLDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(310, OBJ_BEHOLDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PlaceFinalGenComplex()
{
    CreateMonsterGenerator(314, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(315, OBJ_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(316, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(317, OBJ_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(318, OBJ_BEHOLDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(319, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(320, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(321, OBJ_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(322, OBJ_BEHOLDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PlaceFinalEx1()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(362, OBJ_DEMON, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(363, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(369, OBJ_BEHOLDER, SPAWN_RATE_VERYVERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(370, OBJ_EVIL_CHERUB, SPAWN_RATE_HIGH, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(367, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(368, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

void PlaceFinalEx2()
{
    ObjectOff(SELF);
    CreateMonsterGenerator(371, OBJ_EVIL_CHERUB, SPAWN_RATE_HIGH, SPAWN_LIMIT_NORMAL);
    CreateMonsterGenerator(372, OBJ_BEHOLDER, SPAWN_RATE_VERYVERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(373, OBJ_SHADE, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(374, OBJ_EMBER_DEMON, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(375, OBJ_OGRE_WARLORD, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(376, OBJ_EMBER_DEMON, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(377, OBJ_BEHOLDER, SPAWN_RATE_VERYVERY_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(378, OBJ_OGRE_WARLORD, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(379, OBJ_SKELETON_LORD, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(386, OBJ_SKELETON_LORD, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(387, OBJ_EVIL_CHERUB, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(388, OBJ_BOMBER_BLUE, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(389, OBJ_EMBER_DEMON, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
}

#define GROUP_finalExWalls 25
void placeFinalEx3()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_finalExWalls);
    CreateMonsterGenerator(380, OBJ_SHADE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(381, OBJ_EVIL_CHERUB, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(382, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(383, OBJ_BOMBER_BLUE, SPAWN_RATE_LOW, SPAWN_LIMIT_LOW);
    CreateMonsterGenerator(384, OBJ_LICH, SPAWN_RATE_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(385, OBJ_GRUNT_AXE, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
}

void placeFinalEx4()
{
    ObjectOff(SELF);
    WallOpen(Wall(146,242));
    WallOpen(Wall(145,243));
    WallOpen(Wall(144,244));
    CreateMonsterGenerator(390, OBJ_DEMON, SPAWN_RATE_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(391, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_LOW, SPAWN_LIMIT_SINGULAR);
    CreateMonsterGenerator(392, OBJ_EMBER_DEMON, SPAWN_RATE_HIGH, SPAWN_LIMIT_LOW);
}

void checkFinalWave(int *p)
{
    int r = 7;
    int sum=0;

    while (--r>=0)
        sum+=CurrentHealth(p[r]);

    if (sum)
    {
        PushTimerQueue(150, p, checkFinalWave);
        return;
    }
    PushTimerQueue(60, 0, StartFinalWave);
}

void finalWave1(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_ARCHER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_SWORDSMAN, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void finalWave2(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_SPIDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_SCORPION, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_SCORPION, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_SCORPION, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_SHADE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void finalWave3(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_VILE_ZOMBIE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_SKELETON_LORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_EVIL_CHERUB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void finalWave4(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_WIZARD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void finalWave5(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_OGRE_BRUTE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_GRUNT_AXE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_GRUNT_AXE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_OGRE_BRUTE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_OGRE_WARLORD, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void finalWave6(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_BEHOLDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_BEHOLDER, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_HORRENDOUS, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_TROLL, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_TROLL, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void finalWave7(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_LICH, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_BOMBER_BLUE, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}
void finalWave8(int *p)
{
    int c=0;

    p[c++]= CreateMonsterGenerator(323, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(324, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(325, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(326, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(342, OBJ_HECUBAH_WITH_ORB, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(343, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
    p[c++]= CreateMonsterGenerator(344, OBJ_MECHANICAL_GOLEM, SPAWN_RATE_VERY_LOW, SPAWN_LIMIT_LOW);
}

int FishBigBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1752394054; arr[1] = 6777154; arr[17] = 2600; arr[19] = 80; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1073741824; arr[27] = 2; arr[28] = 1116471296; arr[29] = 65; 
		arr[31] = 10; arr[32] = 3; arr[33] = 6; arr[59] = 5544288; arr[60] = 1329; 
		arr[61] = 46905600; 
	pArr = arr;
	return pArr;
}

void FishBigSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 2600;	hpTable[1] = 2600;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = FishBigBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onFinalBossDeath()
{
    WallOpen(Wall( 220,164 ));
    WallOpen(Wall( 221,165 ));
    WallOpen(Wall( 222,166 ));
    WallOpen(Wall( 223,167 ));
    WallOpen(Wall( 224,168 ));
    UniPrintToAll("!--최종 보스가 죽었습니다--! 승리!!");
}

void glueImageLoop(int image)
{
    int target=GetOwner(image);

    if (CurrentHealth(target))
    {
        PushTimerQueue(1,image,glueImageLoop);
        MoveObject(image,GetObjectX(target),GetObjectY(target));
        return;
    }
    Delete(image);
}

void deferredBossMoving(int boss)
{
    if (CurrentHealth(boss))
    {
        Walk(boss,LocationX(342),LocationY(342));
        AggressionLevel(boss, 1.0);
        UniChatMessage(boss,"나의 이 섹시함에 죽을 각오는 되어 있겠지?", 150);
    }
}

void createFinalBoss(int locationId)
{
    int boss=CreateObjectById(OBJ_FISH_BIG,LocationX(locationId),LocationY(locationId));
    int img=boss+1;

    SetOwner(boss, DummyUnitCreateById(OBJ_WASP, LocationX(locationId),LocationY(locationId)));
    UnitNoCollide(img);
    PushTimerQueue(1,img,glueImageLoop);
    FishBigSubProcess(boss);
    SetUnitScanRange(boss, 400.0);
    SetCallback(boss,5,onFinalBossDeath);
    PushTimerQueue(12,boss,deferredBossMoving);
}

#define GROUP_emergencyExitWalls 23

void endFinalWave()
{
    WallGroupOpen(GROUP_emergencyExitWalls);
    CreateObjectById(OBJ_GAUNTLET_EXIT_A, LocationX(328), LocationY(328));
    CreateObjectById(OBJ_GAUNTLET_EXIT_A, LocationX(329), LocationY(329));
    CreateObjectById(OBJ_GAUNTLET_EXIT_A, LocationX(330), LocationY(330));
    CreateObjectById(OBJ_GAUNTLET_EXIT_B, LocationX(331), LocationY(331));
    CreateObjectById(OBJ_GAUNTLET_EXIT_B, LocationX(332), LocationY(332));
    CreateObjectById(OBJ_GAUNTLET_EXIT_B, LocationX(327), LocationY(327));
    createFinalBoss(393);
}

void StartFinalWave()
{
    int arr[7];
    int count;

    if (count < 8)
    {
        int fns[]={finalWave1, finalWave2, finalWave3, finalWave4, finalWave5, finalWave6, finalWave7, finalWave8, finalWave8, };

        invokeGenRaid(fns[count++], arr);
        PushTimerQueue(1, arr, checkFinalWave);
        return;
    }
    endFinalWave();
}
