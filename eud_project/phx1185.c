
#include "phx1185_gvar.h"
#include "phx1185_player.h"
#include "phx1185_input.h"
#include "phx1185_mons.h"
#include "phx1185_weapon.h"
#include "phx1185_resource.h"
#include "phx1185_map.h"
#include "libs/coopteam.h"
#include "libs/logging.h"
#include "libs/displayInfo.h"

int m_clientNetId;

#define FALSE 0

#define UPGRADE_GUARANTEE_LEVEL 30
int m_upgradeTable[UPGRADE_GUARANTEE_LEVEL];

void initUpgradeTable()
{
    int up[]={
        96, 96,95,94,93, 92, 90, 87,
        83, 80, 78, 74, 71,
        69, 66, 61,57,53,
        49,44,36,30,24,
        18,15,13,10,7,
        1,1,1,1,1,
    };
    NoxDwordMemCopy(up,m_upgradeTable,sizeof(m_upgradeTable));
}

int m_abandondedMaster;

static int GetAbandondedItemOwner() //virtual
{
    return m_abandondedMaster;
}

int m_masterUnit;

static int GetMasterUnit() //virtual
{
    return m_masterUnit;
}

int m_bossMonster;

static int GetBossMonster() //virtual
{
    return m_bossMonster;
}

int m_avataHash;

static int GetAvataHash() //virtual
{
    return m_avataHash;
}

char m_jumpTime[MAX_PLAYER_COUNT];

static int GetAvataJumpTime(int pIndex) //virtual
{
    return m_jumpTime[pIndex];
}

static void SetAvataJumpTime(int pIndex, int duration) //virtual
{
    m_jumpTime[pIndex]=duration;
}

char m_altitude[MAX_PLAYER_COUNT];

static int GetAvataAltitude(int pIndex) //virtual
{ return m_altitude[pIndex]; }

static void SetAvataAltitude(int pIndex, int set) //virtual
{
    int limit=USER_JUMP_DURATION*2;
    if (set>limit)
        set = limit;
    m_altitude[pIndex]=set;
}

char m_attackDelay[MAX_PLAYER_COUNT];

static int GetAttackDelay(int pIndex) //virtual
{
    return m_attackDelay[pIndex];
}

static void SetAttackDelay(int pIndex, int delay) //virtual
{
    m_attackDelay[pIndex]=delay;
}

int m_player[MAX_PLAYER_COUNT];
int m_pFlags[MAX_PLAYER_COUNT];
int m_avata[MAX_PLAYER_COUNT];
int m_avataSub[MAX_PLAYER_COUNT];


static int GetAvataSub(int pIndex) //virtual
{
    return m_avataSub[pIndex];
}

static void SetAvataSub(int pIndex, int sub) //virtual
{
    m_avataSub[pIndex]=sub;
}

static int GetPlayerAvata(int pIndex) //virtual
{
    return m_avata[pIndex];
}

static void SetPlayerAvata(int pIndex, int avata) //virtual
{
    m_avata[pIndex]=avata;
}

static int GetPlayer(int pIndex) //virtual
{
    return m_player[pIndex];
}

static void SetPlayer(int pIndex, int user) //virtual
{
    m_player[pIndex]=user;
}

static int GetPlayerFlags(int pIndex) //virtual
{
    return m_pFlags[pIndex];
}

static void SetPlayerFlags(int pIndex, int flags) //virtual
{
    m_pFlags[pIndex]=flags;
}

int m_userDamage[MAX_PLAYER_COUNT];

static void SetPlayerDamage(int pIndex, int amount) //virtual
{
    m_userDamage[pIndex]=amount;
}

static int GetPlayerDamage(int pIndex) //virtual
{
    return m_userDamage[pIndex];
}

#define MAX_CRITICAL_RATE 100

char m_criticalRate[MAX_PLAYER_COUNT];

static void SetCriticalRate(int pIndex, int rate, int inc)
{
    if (!inc)
    {
        m_criticalRate[pIndex]=rate;
        return;
    }
    m_criticalRate[pIndex]+=inc;
}

int m_userHandler[MAX_PLAYER_COUNT];

static int GetUserHandler(int pIndex) //virtual
{ return m_userHandler[pIndex]; }

static void SetUserHandler(int pIndex, int handler) //virtual
{
    m_userHandler[pIndex]=handler;
}

int *m_pClientKeyState;

static int GetClientKeyState(int pIndex) //virtual
{
    return m_pClientKeyState[pIndex];
}

static void SetClientKeyState(int pIndex, int value) //virtual
{
    m_pClientKeyState[pIndex]=value;
}

short *m_pMonsterCredits;
short *m_pMonsterHitPoints;
short *m_pMonsterArmors;
short *m_pMonsterDeathFn;

static int GetMonsterDeathFunction(short thingId) //virtual
{
    return m_pMonsterDeathFn[thingId%97];
}

static void SetMonsterInfoPtr(short *pHp, short *pCredit, short *pAmmo, short *pDeathFn) //virtual
{
    m_pMonsterHitPoints=pHp;
    m_pMonsterCredits=pCredit;
    m_pMonsterArmors=pAmmo;
    m_pMonsterDeathFn=pDeathFn;
}

static int GetMonsterArmor(int n) //virtual
{
    return m_pMonsterArmors[n];
}

static int GetMonsterInitHP(int n) //virtual
{
    return m_pMonsterHitPoints[n];
}

static int GetMonsterCredit(int n) //virtual
{
    return m_pMonsterCredits[n];
}

string m_popupMessage[32];

static string GetPopupMessage(int index) //virtual
{
    return m_popupMessage[index];
}

int m_dialogCtx;

static void SetDialogCtx(int ctx) //virtual
{
    m_dialogCtx=ctx;
}

static int GetDialogCtx() //virtual
{
    return m_dialogCtx;
}

int m_sellWeaponHash;

static int GetSellWeaponHash() //virtual
{
    return m_sellWeaponHash;
}

int m_useItemSlot[MAX_PLAYER_COUNT];
int m_useItemSlotFps[MAX_PLAYER_COUNT];
int m_userEquipment[MAX_PLAYER_COUNT];
int m_userEquipmentCopy[MAX_PLAYER_COUNT];

static int GetPlayerEquipment(int pIndex) //virtual
{
    return m_userEquipment[pIndex];
}

static int GetPlayerEquipmentCopy(int pIndex) //virtual
{
    return m_userEquipmentCopy[pIndex];
}

void OnItemEquip(int pIndex, int item)
{
    if (m_userEquipment[pIndex]==item)
        return;

    if (m_userEquipmentCopy[pIndex])
        Delete(m_userEquipmentCopy[pIndex]);

    int cre=m_avata[pIndex];

    if (!MaxHealth(cre))
        return;
    int copy=CreateObjectById(GetUnitThingID(item),GetObjectX(cre),GetObjectY(cre));

    UnitNoCollide(copy);
    m_userEquipmentCopy[pIndex]=copy;
    OnPlayerWeaponChanged(pIndex);
}

static void TryItemUse(int pIndex, int item)
{
    int fps=GetMemory(0x84ea04);

    if (m_useItemSlot[pIndex]==item)
    {
        if (ABS( fps - m_useItemSlotFps[pIndex]) < 20 )
        {
            //착용
            OnItemEquip(pIndex, item);
            m_userEquipment[pIndex]=item;
            m_useItemSlot[pIndex]=0;
        }
    }
    m_useItemSlotFps[pIndex]=fps;
    m_useItemSlot[pIndex]=item;
}

int m_weaponEffectHash;

static int GetWeaponEffectHash() //virtual
{
    return m_weaponEffectHash;
}

int m_upgradeRate[MAX_PLAYER_COUNT];

static void PlayerClassDoInitialize(int pIndex, int user) //virtual
{
    m_useItemSlot[pIndex]=0;
    m_useItemSlotFps[pIndex]=0;
    m_userEquipment[pIndex]=0;
    m_criticalRate[pIndex]=0;
    m_userDamage[pIndex]=1;
    m_upgradeRate[pIndex]=0;
    m_jumpTime[pIndex]=0;
    m_altitude[pIndex]=0;
}

static void PlayerClassDoDeinit(int pIndex) //virtual
{
    Delete( m_userEquipmentCopy[pIndex] );
    m_userEquipmentCopy[pIndex]=0;
    m_userEquipment[pIndex]=0;
}

int m_pImgVector;
#define IMAGE_VECTOR_SIZE 1024

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

void commonServerClientProcedure() //virtual
{
    int index=0;
    m_pImgVector=CreateImageVector(IMAGE_VECTOR_SIZE);

    CommonInitializeImage(m_pImgVector);
    DoImageDataExchange(m_pImgVector);
    InitPopupMessage(m_popupMessage);
    AddShowInfoData(30, 280, 0x751090, 0xEFE0);
    FrameTimerWithArg(10, 0x751090,ClientDisplayUserGoldAmount);
}

static void NetworkUtilClientMain()
{
    char *p=NETWORK_UTIL_PINDEX_OFF;
    m_clientNetId=p[0];
    commonServerClientProcedure();
    FrameTimerWithArg(30, m_clientNetId, ClientClassTimerLoop);
    FrameTimer(31, ClientProcLoop);
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("phx1185-log.txt");
    InitialServerPatchMapleOnly();
    m_pClientKeyState=0x751000;
    m_masterUnit=DummyUnitCreateById(OBJ_HECUBAH, LocationX(25),LocationY(25));
    m_abandondedMaster=DummyUnitCreateById(OBJ_HECUBAH, LocationX(77),LocationY(77));
    PushTimerQueue(60, m_pClientKeyState, ServerInputLoop);
    HashCreateInstance(&m_avataHash);
    MakeCoopTeam();
    PushTimerQueue(10, 0, InitialPlaceMonsters);
    PushTimerQueue(1, 0, StartDrawMap);
    PushTimerQueue(10, 0, InitMapWeapons);
    HashCreateInstance(&m_weaponEffectHash);
    HashCreateInstance(&m_sellWeaponHash);
    InitWeaponHash(m_weaponEffectHash, m_sellWeaponHash);
    initUpgradeTable();
    InitializePlayerManager();
    CreateMeso(LocationX(119),LocationY(119), 100);
    blockObserverMode();

    // PushTimerQueue(30, 0, imageAlignmentTest);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
}

static void GoUserAttack(int pIndex, int victim, int dam) //virtual
{
    int user = m_player[pIndex];

    if (CurrentHealth(user))
    {
        int criticalRate = m_criticalRate[pIndex];

        ComputeCritical(pIndex, &criticalRate);
        if (criticalRate)
        {
            // if (criticalRate>=MAX_CRITICAL_RATE)
            //     criticalRate=MAX_CRITICAL_RATE-1;

            // if (Random(criticalRate, MAX_CRITICAL_RATE)==MAX_CRITICAL_RATE)
            if (GetPercentRate(criticalRate, MAX_CRITICAL_RATE))
                dam+=(dam>>1);
        }
    }
    HitMonsterByUser(victim, user, dam);
}

static void TryUpdatePlayerGold(int user) //virtual
{
    UpdatePlayerGold(user);
}

static void FetchWeaponEffect(int weapon, int effectHash) //virtual
{
    WeaponUtilLoadEffect(weapon, effectHash);
    HashDeleteInstance(effectHash);
}

static void OnItemDropped(int pIndex, int item) //virtual
{
    if (m_userEquipment[pIndex]==item) //버린게 장착중인 무기라면,
    {
        m_userEquipment[pIndex]=0;
        Delete(m_userEquipmentCopy[pIndex]);
        m_userEquipmentCopy[pIndex]=0;
    }
}

#define MAX_UPGRADE_RATE 100
#define UPGRADE_CUT_LINE 105

static int TryItemUpgrade(int pIndex, int item) //virtual
{
    //아이템 강화... 확률 어떻게 처리?
    //return false 반환 시 장비 파괴됨!//
    int powerLv=GetUnit1C(item);
    int rate = m_upgradeRate[pIndex];

    if (powerLv < UPGRADE_GUARANTEE_LEVEL)
        rate += m_upgradeTable[powerLv];

    if (rate>=MAX_UPGRADE_RATE)
        rate=MAX_UPGRADE_RATE-1;

    if (Random(rate, UPGRADE_CUT_LINE)>=MAX_UPGRADE_RATE)
    {
        //성공
        SetUnit1C(item, powerLv+1);
        return TRUE;
    }
    if (powerLv)
        SetUnit1C(item, powerLv-1);
    return FALSE;
}

static int TryIncreaseUpgradeRate(int user) //virtual
{
    int pIndex=GetPlayerIndex(user);

    if (pIndex<0)
        return 0;

    int up=m_upgradeRate[pIndex]+1;

    if (up<MAX_UPGRADE_RATE)
    {
        m_upgradeRate[pIndex] = up;
        return up;
    }
    return 0;
}

int m_bossLevel;

static int TrySummonBossMonster(int counterUnit) //virtual
{
    if (MaxHealth(m_bossMonster))
        return FALSE;

    if (!SummonBossMonster(&m_bossMonster, counterUnit, &m_bossLevel))
    {
        UniChatMessage(counterUnit, "모든 보스를 처치하셔서, 더 이상 보스가 없습니다", 120);
        return FALSE;
    }
    return TRUE;
}

void onMesoPickup()
{
    int amount=GetUnit1C(SELF);
    char message[128];

    if (IsPlayerUnit(OTHER))
    {
        NoxSprintfString(message, "메소 %d 을 획득했습니다", &amount, 1);
        UniPrint(OTHER, ReadStringAddressEx(message));
        ChangeGold(OTHER, amount);
        UpdatePlayerGold(OTHER);
    }
    Delete(SELF);
}

static void CreateMeso(float x, float y, int amount) //virtual
{
    int g=CreateObjectById(OBJ_GOLD,x,y);
    int flags=GetUnitFlags(g);

    SetUnit1C(g,amount);

    if (flags&UNIT_FLAG_NO_COLLIDE)
        flags^=UNIT_FLAG_NO_COLLIDE;
    if (!(flags&UNIT_FLAG_NO_PUSH_CHARACTERS))
        flags^=UNIT_FLAG_NO_PUSH_CHARACTERS;
    SetUnitFlags(g, flags);
    Frozen(g, TRUE);
    SetOwner(GetAbandondedItemOwner(),g);
    SetUnitCallbackOnPickup(g,onMesoPickup);
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);
    
    if (pUnit)
        ShowQuestIntroOne(pUnit, 237, "WarriorChapterBegin6", "GeneralPrint:MapNameSanturary");

    if (pInfo==0x653a7c)
    {
        commonServerClientProcedure();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
