
#include "xtest511_gvar.h"
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/queueTimer.h"
#include "libs/playerinfo.h"

int HasGateKey(int unit)
{
    int inv = GetLastItem(unit);

    while (IsObjectOn(inv))
    {
        if (GetUnitThingID(inv) ^ 2182)
            inv = GetPreviousItem(inv);
        else
            return inv;
    }
    return 0;
}

int StartLocationWithPlayer()
{
    int start;

    if (!start)
        start = Object("PlayerStartLocation");
    return start;
}

int GetMaster()
{
    int unit;
    
    if (!unit)
    {
        unit = CreateObjectById(OBJ_HECUBAH, 5500.0, 100.0);
        Frozen(unit, 1);
    }
    return unit;
}
int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}
void RhombusUnitTeleport(int unit, float x_low, float x_high, float y_low, float y_high)
{
    float xf = RandomFloat(y_low, y_high), yf = RandomFloat(0.0, x_high - x_low);

    MoveObject(unit, x_high - y_high + xf - yf, xf + yf);
}

int UnitEquipedWeapon(int unit)
{
    int ptr = UnitToPtr(unit), pic;
    
    if (ptr)
    {
        pic = GetMemory(GetMemory(ptr + 0x2ec) + 0x810);
        if (pic)
            return GetMemory(pic + 0x2c);
    }
    return 0;
}

int GetPrevNode(int cur)
{
    return GetOwner(cur);
}

int GetNextNode(int cur)
{
    return ToInt(GetObjectZ(cur));
}

void SetPrevNode(int cur, int tg)
{
    SetOwner(tg, cur);
}

void SetNextNode(int cur, int tg)
{
    Raise(cur, ToFloat(tg));
}

int NewList(int data)
{
    int cur = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(data), GetObjectY(data)),gnode;
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(cur), GetObjectY(cur)), ToFloat(data));

    GlobalHeadNode(0,&gnode);
    SetPrevNode(cur, GetPrevNode(gnode + 1));
	SetNextNode(cur, gnode + 1);
	SetNextNode(GetPrevNode(gnode + 1), cur);
	SetPrevNode(gnode + 1, cur);

    return cur;
}

int RemoveList(int cur)
{
    int next = GetNextNode(cur);
    SetNextNode(GetPrevNode(cur), GetNextNode(cur));
    SetPrevNode(GetNextNode(cur), GetPrevNode(cur));
    Delete(cur);
    Delete(cur + 1);
    return next;
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}

int GetUnitParent(int sUnit)
{
    int cur = sUnit, res;

    while (1)
    {
        res = cur;
        cur = GetOwner(cur);
        if (!cur) break;
    }
    return res;
}

void InitMapSigns()
{
    RegistSignMessage(Object("UniSign1"), "베이스 캠프 마켓: 없는 것 빼고는 여기에 다 있다");
    RegistSignMessage(Object("UniSign2"), "슬리피우드 던전 입구: 무기와 갑옷 빡세게 챙길 것");
    RegistSignMessage(Object("UniSign3"), "아테네 신전 후문 입구: 원래는 여기가 정문이었음");
    RegistSignMessage(Object("UniSign4"), "세상에서 가장 강한 몬스터가 이 방안에 서식하고 있음");
    RegistSignMessage(Object("UniSign5"), "비밀 휴게실: 호바스를 클릭하면 힐링 버프를 시전해 줍니다");
    RegistSignMessage(Object("UniSign6"), "마법 무기마켓: 각 무기에 대한 설명을 보려면 클릭, 구입하려면 더블클릭하세요");
    RegistSignMessage(Object("UniSign7"), "전사 신규능력을 구입할 수 있습니다");
    RegistSignMessage(Object("UniSign8"), "이곳은 아테네 신전 입니다. 강력했던 고대 괴물들이 이곳에서 여전히 살아 숨쉬고 있습니다");
    RegistSignMessage(Object("UniSign9"), "-신전2층- 특수한 아이템을 판매하고 있으니 지갑 빵빵한 사람이라면 언제든 환영!");
    RegistSignMessage(Object("UniSign10"), "가운데 NPC 는 3천 골드만으로 모든 아이템을 수리해줍니다");
    RegistSignMessage(Object("UniSign11"), "-고대괴물- 처녀귀신: 그녀의 손 끝에 한번만 스쳐도 당신은 죽음이다");
    RegistSignMessage(Object("UniSign12"), "-고대괴물- 키러리언: 이동속도가 매우 빠르며 근접한 거리에서 화염을 내뿜는다");
    RegistSignMessage(Object("UniSign13"), "타락한 여신: 빠른 속도로 무수히 많은 개구리를 토해낸다");
    RegistSignMessage(Object("UniSign14"), "주인없는 별장: 이제 이곳은 먼지만 가득하다...");
    RegistSignMessage(Object("UniSign15"), "스톤골렘: 이 녀석을 깨우는 순간 공포게임 된다");
    RegistSignMessage(Object("UniSign16"), "공포의 얼굴: 썩쏘에 역겨움을 제대로 느껴봐라");
    RegistSignMessage(Object("UniSign17"), "불의 영혼: 용광로에 빠져죽은 자가 불의 괴물로 부활했다는 소문이 있다");
}

void StrWarSkill()
{
	int arr[26], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = LocationX(45), pos_y = LocationY(45);

	arr[0] = 1075905278; arr[1] = 133713856; arr[2] = 67649679; arr[3] = 18874881; arr[4] = 956449312; arr[5] = 2080900128; arr[6] = 151552009; arr[7] = 1343258900; 
	arr[8] = 570434048; arr[9] = 1107595336; arr[10] = 8881216; arr[11] = 337714704; arr[12] = 6344232; arr[13] = 1073750020; arr[14] = 565376; arr[15] = 603922400; 
	arr[16] = 608305184; arr[17] = 4194304; arr[18] = 1081601; arr[19] = 2113929506; arr[20] = 1140589571; arr[21] = 2143618048; arr[22] = 269484049; arr[23] = 268566032; 
	arr[24] = 1077919744; arr[25] = 32640; 
	for (i = 0 ; i < 26 ; i ++)
		count = DrawStrWarSkill(arr[i], name, count);
	TeleportLocation(45, pos_x, pos_y);
}

int DrawStrWarSkill(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 806 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 45);
		if (count % 72 == 71)
            TeleportLocationVector(45, -140.0, 144.0);
		else
            TeleportLocationVector(45, 2.0, -2.0);
		count ++;
	}
	return count;
}

void StrSpecialWeapon()
{
	int arr[32], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(46), pos_y = GetWaypointY(46);

	arr[0] = 271310912; arr[1] = 1338505220; arr[2] = 9375728; arr[3] = 478232578; arr[4] = 37756992; arr[5] = 134513728; arr[6] = 8784464; arr[7] = 1179906; 
	arr[8] = 1084236834; arr[9] = 134697042; arr[10] = 134254600; arr[11] = 2047377697; arr[12] = 1069556802; arr[13] = 2017461376; arr[14] = 173114120; arr[15] = 257; 
	arr[16] = 65570; arr[17] = 2018672640; arr[18] = 134217743; arr[19] = 16924657; arr[20] = 537039872; arr[21] = 69271488; arr[22] = 528400; arr[23] = 33428640; 
	arr[24] = 1075871808; arr[25] = 1341947776; arr[26] = 4271; arr[27] = 33620738; arr[28] = 516; arr[29] = 134250373; arr[30] = 1880098816; arr[31] = 31; 
	
	for (i = 0 ; i < 32 ; i ++)
		count = DrawStrSpecialWeapon(arr[i], name, count);
	TeleportLocation(46, pos_x, pos_y);
}

int DrawStrSpecialWeapon(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 992 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 46);
		if (count % 88 == 87)
            TeleportLocationVector(46, -172.0, 176.0);
		else
            TeleportLocationVector(46, 2.0, -2.0);
		count ++;
	}
	return count;
}

void StrMissionFail()
{
	int arr[17], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(47), pos_y = GetWaypointY(47);

	arr[0] = 272613948; arr[1] = 75782084; arr[2] = 537411713; arr[3] = 1090654737; arr[4] = 606245922; arr[5] = 675873800; arr[6] = 267423498; arr[7] = 1352002; 
	arr[8] = 1073872898; arr[9] = 114; arr[10] = 1889865712; arr[11] = 268697375; arr[12] = 1075856020; arr[13] = 86540160; arr[14] = 16842881; arr[15] = 33428976; 
	arr[16] = 671349764; 
	for (i = 0 ; i < 17 ; i ++)
		count = DrawStrMissionFail(arr[i], name, count);
	MoveWaypoint(47, pos_x, pos_y);
}

int DrawStrMissionFail(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 47);
		if (count % 48 == 47)
			TeleportLocationVector(47, -92.0, 96.0);
		else
			TeleportLocationVector(47, 2.0, -2.0);
		count ++;
	}
	return count;
}

void StrMissionClear()
{
	int arr[23], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(48), pos_y = GetWaypointY(48);

	arr[0] = 1008796286; arr[1] = 1338015748; arr[2] = 142870536; arr[3] = 537002002; arr[4] = 571482146; arr[5] = 3539016; arr[6] = 144933001; arr[7] = 50725153; 
	arr[8] = 612564516; arr[9] = 1073742968; arr[10] = 554172575; arr[11] = 1072697345; arr[12] = 33554946; arr[13] = 16388; arr[14] = 8259592; arr[15] = 2080440336; 
	arr[16] = 1107566625; arr[17] = 134740992; arr[18] = 135299208; arr[19] = 537919490; arr[20] = 528678432; arr[21] = 4196344; arr[22] = 524414; 
	for (i = 0 ; i < 23 ; i ++)
		count = DrawStrMissionClear(arr[i], name, count);
	MoveWaypoint(48, pos_x, pos_y);
}

int DrawStrMissionClear(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 713 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 48);
		if (count % 64 == 63)
			TeleportLocationVector(48, -124.0, 128.0);
		else
			TeleportLocationVector(48, 2.0, -2.0);
		count ++;
	}
	return count;
}

void StrDungeonBoss()
{
	int arr[18], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(50), pos_y = GetWaypointY(50);

	arr[0] = 1076879998; arr[1] = 1077968960; arr[2] = 135268416; arr[3] = 1209012256; arr[4] = 67369987; arr[5] = 4523908; arr[6] = 8552577; arr[7] = 270537281; 
	arr[8] = 69214480; arr[9] = 1099429893; arr[10] = 8424441; arr[11] = 32; arr[12] = 67117058; arr[13] = 134250496; arr[14] = 65536; arr[15] = 131104; 
	arr[16] = 2088757246; arr[17] = 32647; 
	for (i = 0 ; i < 18 ; i ++)
		count = DrawStrDungeonBoss(arr[i], name, count);
	MoveWaypoint(50, pos_x, pos_y);
}

int DrawStrDungeonBoss(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 558 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 50);
		if (count % 52 == 51)
			TeleportLocationVector(50, -104.0, -100.0);
		else
			TeleportLocationVector(50, 2.0, 2.0);
		count ++;
	}
	return count;
}
void DisableObject(int obj)
{
    ObjectOff(obj);
}
void UpdateRepairItem(int plrIndex, int item)
{
    int link, temp = GetMemory(0x5c3108), *ptr = UnitToPtr(item);
    int arr[9];

    if (!link)
    {
        arr[0] = 0x50685056; arr[1] = 0xFF005072; arr[2] = 0x708B2414; arr[3] = 0x04C48304; arr[4] = 0x4D87A068; arr[5] = 0x30FF5600; arr[6] = 0x082454FF;
        arr[7] = 0x580CC483; arr[8] = 0x9090C35E;
        link = arr;
    }
    if (ptr != NULLPTR)
    {
        item = ptr;
        SetMemory(0x5c3108, link);
        Unused1f(&plrIndex);
        SetMemory(0x5c3108, temp);
    }
}

int RepairAll(int unit)
{
    int inv = GetLastItem(unit), count = 0, plrIndex = GetPlayerIndex(unit);

    if (plrIndex < 0)
        return 0;
    while (IsObjectOn(inv))
    {
        if (MaxHealth(inv) ^ CurrentHealth(inv))
        {
            RestoreHealth(inv, MaxHealth(inv) - CurrentHealth(inv));
            UpdateRepairItem(plrIndex, inv);
            count += 1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}
int AddNewNode(int headNode, int data)
{
    int node = CreateObjectAt("ImaginaryCaster", GetObjectX(data), GetObjectY(data));

    SetUnit1C(node, data);
    SetNextNode(node, GetNextNode(headNode));
    SetPrevNode(node, headNode);
    SetPrevNode(GetNextNode(headNode), node);
    SetNextNode(headNode, node);
    LookWithAngle(headNode, GetDirection(headNode) + 1);

    return node;
}
void CustomMeleeAttackCode(char *bin, int fn)
{
    char code[]={
        0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
        0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
        0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
        0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
    };
    
    int *pStrike = &bin[0xEC];

    pStrike[0] = code;
    int *hack = &bin[0x94];

    hack[15]=fn;
}
