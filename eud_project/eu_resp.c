
#include "noxscript\builtins.h"
#include "libs\typecast.h"
#include "libs\unitstruct.h"
#include "libs\unitutil.h"
#include "libs\printutil.h"
#include "libs\mathlab.h"
#include "libs\spellutil.h"

#include "libs\coopteam.h"
#include "libs\fxeffect.h"
#include "libs\waypoint.h"
#include "libs\cmdline.h"

#define PLAYER_DEATH_FLAG 0x80000000
#define NPC_TABLE_COUNT 56

int player[20];
int Gvar_2[10];


void MapExit()
{
    RemoveCoopTeamMode();
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & PLAYER_DEATH_FLAG;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] ^= PLAYER_DEATH_FLAG;
}

void initMap()
{
    int npc = CreateObject("AirshipCaptain", 76);
    Enchant(npc, "ENCHANT_FREEZE", 0.0);
    LookWithAngle(npc, 32);
    Frozen(npc, 1);
    SetCallback(npc, 9, teleportSelectRoom);

    FrameTimer(30, initSelectBots);
    FrameTimerWithArg(50, npc, sayCaptain);
}

void sayCaptain(int npc)
{
    UniChatMessage(npc, "대결 상대를 고르려면 나에게 오라...!", 150);
    PlaySoundAround(npc, 551);
}

void Nop(int n)
{ }

int chooseTarget(int pick)
{
    int rnd = Random(pick, pick + 7), i;

    for (i = 0 ; i < 8 ; i += 1)
    {
        int npc = PickFromNpcTable(rnd);

        if (!npc)
            return -1;
        if (CurrentHealth(npc))
            return npc;
        rnd += 1;
        if (rnd > pick + 7)
            rnd = pick;
    }
    return 0;
}

void releaseEnemy(int npc)
{
    int classtype = GetDirection(GetLastItem(npc));

    ObjectOn(npc);
    setUnitHealth(npc, 20000);
    SetCallback(npc, 7, setDeaths);
    if (classtype >= 0 && classtype <= 2)
        SetCallback(npc, 3, wizardSkill);
    else if (classtype == 3 || classtype == 4)
    {
        SetCallback(npc, 3, warriorSkills);
        SetCallback(npc, 9, warCollisionEvent);
    }
    else if (classtype == 5 || classtype == 6)
        SetCallback(npc, 3, conjurerSkill);
    Enchant(npc, "ENCHANT_INVULNERABLE", 3.0);
    respawnLocation(npc);
}

void selectEnemy(int pick)
{
    int queue[7], order;

    if (pick >= 0)
    {
        queue[order++] = chooseTarget(pick);
    }
    else
    {
        int i;

        for (i = 0 ; i < 7 ; i += 1)
        {
            if (CurrentHealth(queue[i]))
                releaseEnemy(queue[i]);
        }
    }
}

void touchSelect()
{
    int select = GetDirection(GetLastItem(self));

    UniPrintToAll(botType(select) + "이 선택되었습니다.");
    Effect("CYAN_SPARKS", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
    PlaySoundAround(self, 907);
    PlaySoundAround(self, 132);
    Delete(self);
    selectEnemy(select * 8);
    maxLessons(8);
}

void initSelectBots()
{
    int npc[7], i;

    for (i = 0 ; i < 7 ; Nop(++i))
    {
        npc[i] = Object("XBOT" + IntToString(i));
        LookWithAngle(GetLastItem(npc[i]), i);
        SetCallback(npc[i], 9, touchSelect);
        ObjectOff(npc[i]);
    }
}

void wizardSkill() {
    int var_0;
    float var_1;

    if (CurrentHealth(self) > 0 && !HasEnchant(self, "ENCHANT_ANTI_MAGIC")) {
        if (CurrentHealth(other) > 0 && !HasEnchant(self, "ENCHANT_DETECTING") && IsAttackedBy(other, self)) {
            var_0 = Random(0, 5);
            if (var_0 == 0) {
                thunderStatus(GetTrigger());
                thunderStatus(GetCaller());
                CastSpellObjectObject("SPELL_SHIELD", self, self);
                PauseObject(self, 50);
                var_0 = 60;
                var_1 = 2.0;
            }
            else if (var_0 == 1) {
                CastSpellObjectLocation("SPELL_FIREBALL", self, GetObjectX(other), GetObjectY(other));
                FrameTimerWithArg(1, GetTrigger(), bigBurn);
                FrameTimerWithArg(1, GetCaller(), bigBurn);
                var_0 = 60;
                var_1 = 2.0;
            }
            else if (var_0 == 2) {
                CastSpellObjectLocation("SPELL_MAGIC_MISSILE", self, GetObjectX(other), GetObjectY(other));
                CastSpellObjectLocation("SPELL_SLOW", self, GetObjectX(other), GetObjectY(other));
                var_0 = 60;
                var_1 = 2.0;
                FrameTimerWithArg(50, GetTrigger(), respawnLocation);
            }
            else if (var_0 == 3) {
                CastSpellObjectLocation("SPELL_FIREBALL", self, GetObjectX(other), GetObjectY(other));
                CastSpellObjectLocation("SPELL_CONFUSE", self, GetObjectX(other), GetObjectY(other));
                var_0 = 45;
                var_1 = 1.5;
                FrameTimerWithArg(30, GetTrigger(), respawnLocation);
            }
            else if (var_0 == 4) {
                CastSpellObjectLocation("SPELL_DEATH_RAY", self, GetObjectX(other) + RandomFloat(-12.0, 12.0), GetObjectY(other) + RandomFloat(-12.0, 12.0));
                CastSpellObjectObject("SPELL_SHIELD", self, self);
                var_0 = 30;
                var_1 = 1.0;
                FrameTimerWithArg(30, GetTrigger(), respawnLocation);
            }
            else if (var_0 == 5) {
                FrameTimerWithArg(1, GetTrigger(), bigBurn);
                FrameTimerWithArg(1, GetCaller(), bigBurn);
                shockMissile(self, other);
                CastSpellObjectObject("SPELL_SHOCK", self, self);
                var_0 = 45;
                var_1 = 1.5;
            }
            cureHealth(self);
            Enchant(self, "ENCHANT_DETECTING", var_1);
            FrameTimerWithArg(var_0, GetTrigger(), delayResetVision);
        }
    }
}

void thunderStatus(int arg_0)
{
    int var_0;

    if (!var_0)
        var_0 = arg_0;

    else {
        if (CurrentHealth(var_0) > 0 && CurrentHealth(arg_0) > 0) {
            Effect("LIGHTNING", GetObjectX(arg_0), GetObjectY(arg_0), GetObjectX(arg_0), GetObjectY(arg_0) - 200.0);
            MoveWaypoint(53, GetObjectX(arg_0), GetObjectY(arg_0));
            FrameTimerWithArg(7, var_0, thunderExplosion);
            FrameTimerWithArg(7, CreateObject("PlayerWaypoint", 53), thunderExplosion);
        }
        var_0 = 0;
    }
}

void thunderExplosion(int arg_0) {
    int var_0;
    int var_1;

    if (!var_0) {
        var_0 = arg_0;
    }
    else {
        if (CurrentHealth(var_0) > 0) {
            MoveWaypoint(53, GetObjectX(arg_0), GetObjectY(arg_0));
            Effect("RICOCHET", GetWaypointX(53), GetWaypointY(53), 0.0, 0.0);
            Effect("JIGGLE", GetWaypointX(53), GetWaypointY(53), 30.0, 0.0);
            AudioEvent("EarthquakeCast", 53);
            AudioEvent("HammerMissing", 53);
            var_1 = CreateObject("CarnivorousPlant", 53);
            SetOwner(var_0, var_1);
            Frozen(var_1, 1);
            DeleteObjectTimer(var_1, 1);
            SetCallback(var_1, 9, thunderTouched);
        }
        Delete(arg_0);
        var_0 = 0;
    }
}

void thunderTouched()
{
    if (CurrentHealth(other) && IsAttackedBy(other, self))
    {
        Damage(other, self, 75, 9);
        Effect("BLUE_SPARKS", GetObjectX(other), GetObjectY(other), 0.0, 0.0);
    }
}

void shockMissile(int arg_0, int arg_1)
{
    int var_0;

    if (CurrentHealth(arg_0) > 0) {
        MoveWaypoint(47, GetObjectX(arg_0) + UnitAngleCos(arg_0, 30.0), GetObjectY(arg_0) + UnitAngleSin(arg_0, 30.0));
        var_0 = CreateObject("ghost", 47);
        SetOwner(arg_0, var_0);
        Enchant(var_0, "ENCHANT_SHOCK", 0.0);
        Enchant(var_0, "ENCHANT_HASTED", 0.0);
        Enchant(var_0, "ENCHANT_RUN", 0.0);
        ObjectOff(var_0);
        DeleteObjectTimer(var_0, 10);
        PushObject(var_0, -200.0, GetObjectX(arg_1), GetObjectY(arg_1));
    }
}

void conjurerSkill()
{
    if (CurrentHealth(self) && !HasEnchant(self, "ENCHANT_ANTI_MAGIC"))
    {
        if (CurrentHealth(other) && !HasEnchant(self, "ENCHANT_CROWN") && IsAttackedBy(other, self))
        {
            int rndpick = Random(0, 5), delay;
            float durate;

            if (rndpick == 0)
            {
                CastSpellObjectObject("SPELL_METEOR", self, other);
                CastSpellObjectObject("SPELL_PIXIE_SWARM", self, self);
                delay = 60;
                durate = 2.0;
            }
            else if (rndpick == 1) {
                CastSpellObjectObject("SPELL_FIST", self, other);
                FrameTimerWithArg(20, GetTrigger(), bufferShotcrossbow);
                FrameTimerWithArg(20, GetCaller(), bufferShotcrossbow);
                delay = 45;
                durate = 1.5;
            }
            else if (rndpick == 2) {
                CastSpellObjectObject("SPELL_TOXIC_CLOUD", self, other);
                CastSpellObjectObject("SPELL_METEOR", self, other);
                delay = 45;
                durate = 1.5;
                FrameTimerWithArg(50, GetTrigger(), respawnLocation);
            }
            else if (rndpick == 3) {
                CastSpellObjectObject("SPELL_FIST", self, other);
                CastSpellObjectObject("SPELL_SLOW", self, other);
                CastSpellObjectObject("SPELL_PIXIE_SWARM", self, self);
                delay = 45;
                durate = 1.5;
                FrameTimerWithArg(30, GetTrigger(), respawnLocation);
            }
            else if (rndpick == 4) {
                CastSpellObjectLocation("SPELL_METEOR", self, GetObjectX(other), GetObjectY(other));
                CastSpellObjectObject("SPELL_PIXIE_SWARM", self, self);
                FrameTimerWithArg(20, GetTrigger(), bufferShotcrossbow);
                FrameTimerWithArg(20, GetCaller(), bufferShotcrossbow);
                delay = 30;
                durate = 1.0;
                FrameTimerWithArg(30, GetTrigger(), respawnLocation);
            }
            else if (rndpick == 5) {
                shotCrossbow(self, other);
                if (!Random(0, 2))
                    FrameTimerWithArg(30, GetTrigger(), summonUnit);
                else {
                    FrameTimerWithArg(30, GetTrigger(), bigBurn);
                    FrameTimerWithArg(30, GetCaller(), bigBurn);
                }
                FrameTimerWithArg(60, GetTrigger(), respawnLocation);
                delay = 60;
                durate = 2.0;
            }
            cureHealth(self);
            Enchant(self, "ENCHANT_CROWN", durate);
            FrameTimerWithArg(delay, GetTrigger(), delayResetVision);
        }
    }
}

void bigBurn(int anyunit)
{
    int caster;

    if (!caster)
        caster = anyunit;
    else
    {
        if (CurrentHealth(caster) && CurrentHealth(anyunit))
        {
            int flame = CreateObjectAt("flame", GetObjectX(anyunit) + RandomFloat(-12.0, 12.0), GetObjectY(anyunit) + RandomFloat(-12.0, 12.0));

            DeleteObjectTimer(flame, 300);
            SetOwner(caster, flame);
            PlaySoundAround(caster, 8);
            Effect("EXPLOSION", GetObjectX(flame), GetObjectY(flame), 0.0, 0.0);
        }
        caster = 0;
    }
}

void summonUnit(int caster)
{
    if (CurrentHealth(caster))
    {
        string summonspell[] = {"SPELL_SUMMON_GHOST", "SPELL_SUMMON_EMBER_DEMON", "SPELL_SUMMON_EVIL_CHERUB", "SPELL_SUMMON_MECHANICAL_GOLEM"};

        CastSpellObjectObject(summonspell[Random(0, 3)], caster, caster);
    }
}

void bufferShotcrossbow(int arg_0) {
    int var_0;

    if (!var_0)
        var_0 = arg_0;
    else {
        shotCrossbow(var_0, arg_0);
        var_0 = 0;
    }
}

void shotCrossbow(int arg_0, int arg_1) {
    int var_0;

    if (CurrentHealth(arg_0) > 0 && CurrentHealth(arg_1) > 0) {
        MoveWaypoint(47, GetObjectX(arg_0) + UnitAngleCos(arg_0, 25.0), GetObjectY(arg_0) + UnitAngleSin(arg_0, 25.0));
        var_0 = CreateObject("archerBolt", 47);
        SetOwner(arg_0, var_0);
        LookAtObject(var_0, arg_1);
        LookAtObject(arg_0, arg_1);
        PushObject(var_0, -80.0, GetObjectX(arg_1), GetObjectY(arg_1));
        AudioEvent("CrossBowShoot", 47);
        PauseObject(arg_0, 24);
    }
}

void delayResetVision(int arg_0) {
    Enchant(arg_0, "ENCHANT_BLINDED", 0.1);
}

void warriorSkills() {
    if (CurrentHealth(self) > 0) {
        if (CurrentHealth(other) > 0 && !HasEnchant(self, "ENCHANT_CROWN")) {
            if (!HasEnchant(self, "ENCHANT_DETECTING")) {
                Enchant(self, "ENCHANT_DETECTING", 10.0);
                LookAtObject(self, other);
                FrameTimerWithArg(1, GetTrigger(), tripleSurikens);
                FrameTimerWithArg(5, GetTrigger(), tripleSurikens);
                FrameTimerWithArg(9, GetTrigger(), tripleSurikens);
            }
            else if (!HasEnchant(self, "ENCHANT_ETHEREAL")) {
                MoveWaypoint(49, GetObjectX(self), GetObjectY(self));
                AudioEvent("BerserkerChargeInvoke", 49);
                Enchant(self, "ENCHANT_ETHEREAL", 15.0);
                Enchant(self, "ENCHANT_VILLAIN", 4.0);
                Enchant(self, "ENCHANT_FREEZE", 4.0);
                LookAtObject(self, other);
                ObjectOff(self);
                berserkerCharge(GetTrigger());
            }
            cureHealth(self);
            Enchant(self, "ENCHANT_CROWN", 3.0);
            FrameTimerWithArg(90, GetTrigger(), delayResetVision);
        }
    }
}

void tripleSurikens(int arg_0) {
    float var_0;
    float var_1;
    float var_2;
    float var_3 = 0.1;
    int var_4[9];
    int i = 0;
    
    if (CurrentHealth(arg_0) > 0) {
        var_1 = UnitAngleCos(arg_0, -7.0);
        var_2 = UnitAngleSin(arg_0, -7.0);
        MoveWaypoint(48, GetObjectX(arg_0) - var_1, GetObjectY(arg_0) - var_2);
        var_4[8] = CreateObject("OgreShuriken", 48);
        while(var_3 < 0.4) {
            MoveWaypoint(48, (var_3 * var_2) + GetObjectX(arg_0) - var_1, (-var_3 * var_1) + GetObjectY(arg_0) - var_2);
            var_4[i * 2] = CreateObject("OgreShuriken", 48);
            MoveWaypoint(48, (-var_3 * var_2) + GetObjectX(arg_0) - var_1, (var_3 * var_1) + GetObjectY(arg_0) - var_2);
            var_4[i * 2 + 1] = CreateObject("OgreShuriken", 48);
            i += 1;
            var_3 += 0.1;
        }
        for (i = 0; i < 9 ; i += 1) {
            SetOwner(arg_0, var_4[i]);
            LookAtObject(var_4[i], arg_0);
            if (GetDirection(var_4[i]) + 128 > 255)
                LookWithAngle(var_4[i], GetDirection(var_4[i]) - 127);
            else
                LookWithAngle(var_4[i], GetDirection(var_4[i]) + 128);
            PushObject(var_4[i], 27.0, GetObjectX(arg_0), GetObjectY(arg_0));
        }
    }
}

void berserkerCharge(int arg_0) {
    int var_0;

    if (CurrentHealth(arg_0) > 0 && HasEnchant(arg_0, "ENCHANT_VILLAIN")) {
        MoveWaypoint(49, GetObjectX(arg_0) + UnitAngleCos(arg_0 , 21.0), GetObjectY(arg_0) + UnitAngleSin(arg_0, 21.0));
        var_0 = CreateObject("redPotion", 49);
        if (IsVisibleTo(arg_0, var_0)) {
            Delete(var_0);
            MoveObject(arg_0, GetWaypointX(49), GetWaypointY(49));
        }
        else {
            Enchant(arg_0, "ENCHANT_HELD", 1.0);
            Delete(var_0);
            EnchantOff(arg_0, "ENCHANT_VILLAIN");
            EnchantOff(arg_0, "ENCHANT_FREEZE");
            ObjectOn(arg_0);
        }
        FrameTimerWithArg(1, arg_0, berserkerCharge);
    }
}

void warCollisionEvent() {
    if (CurrentHealth(self) > 0 && HasEnchant(self, "ENCHANT_VILLAIN") && CurrentHealth(other) > 0) {
        if (CurrentHealth(other) > 0 && IsAttackedBy(other, self))
            Damage(other, self, 150, 2);
        EnchantOff(self, "ENCHANT_VILLAIN");
        EnchantOff(self, "ENCHANT_FREEZE");
        ObjectOn(self);
    }
}

void cureHealth(int arg_0) {
    int var_0 = 0;

    if (CurrentHealth(arg_0) < 20000 - 3 && !HasEnchant(arg_0, "ENCHANT_INVULNERABLE")) {
        Effect("GREATER_HEAL", GetObjectX(arg_0), GetObjectY(arg_0), GetObjectX(arg_0), GetObjectY(arg_0) - 120.0);
        MoveWaypoint(52, GetObjectX(arg_0), GetObjectY(arg_0));
        AudioEvent("RubyDrop", 52);
        EnchantOff(arg_0, "ENCHANT_INVULNERABLE");
        if (HasEnchant(arg_0, "ENCHANT_SHIELD")) {
            //PrintToAll("회복 전 체력: " + IntToString(CurrentHealth(arg_0)));
            EnchantOff(arg_0, "ENCHANT_SHIELD");
            var_0 = 1;
        }
        setUnitHealth(arg_0, CurrentHealth(arg_0) + 3);
        if (var_0 == 1) {
            Enchant(arg_0, "ENCHANT_SHIELD", 0.0);
            //PrintToAll("회복 후 체력: " + IntToString(CurrentHealth(arg_0)));
        }
    }
}

void goField() {
    UniPrintToAll("선택이 완료되었습니다, 모든 플레이어가 전장으로 이동됩니다.");
    EnchantOff(other, "ENCHANT_ANTI_MAGIC");
    EnchantOff(other, "ENCHANT_ANCHORED");
    selectEnemy(-1);
    ObjectOn(Object("mainSwitch"));
    CastSpellObjectObject("SPELL_RESTORE_MANA", other, other);
    MoveObject(other, GetWaypointX(77), GetWaypointY(77));
}

void teleportAllPlayers(int wp) {
    int i;

    for (i = 0 ; i < 10 ; i += 1) {
        if (CurrentHealth(player[i]) > 0)
            MoveObject(player[i], GetWaypointX(wp), GetWaypointY(wp));
    }
}

string botType(int order)
{
    string botname[] =
    {
        "마법사(A타입)", "마법사(B타입)", "마법사(C타입)",
        "전사(A타입)", "전사(B타입)", "소환술사(A타입)", "소환술사(B타입)"
    };

    return botname[order];
}

void teleportSelectRoom()
{
    if (!IsPlayerUnit(other))
        return;

    if (CurrentHealth(other))
    {
        MoveObject(other, GetWaypointX(50), GetWaypointY(50));
        Enchant(other, "ENCHANT_FREEZE", 2.0);
        Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
        Enchant(other, "ENCHANT_ANCHORED", 0.0);
        UniChatMessage(other, "대결상대를 선택하세요.", 150);
        AudioEvent("DrainManaCast", 50);
        Effect("CYAN_SPARKS", GetWaypointX(50), GetWaypointY(50), 0.0, 0.0);
        Delete(self);
    }
}

void respawnLocation(int targetunit)
{
    int destlocation = Waypoint("rp" + IntToString(Random(0, 27)));

    if (CurrentHealth(targetunit))
    {
        Effect("TELEPORT", GetObjectX(targetunit), GetObjectY(targetunit), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(targetunit), GetObjectY(targetunit), 0.0, 0.0);
        MoveObject(targetunit, GetWaypointX(destlocation), GetWaypointY(destlocation));
        AudioEvent("BlindOff", destlocation);
        AudioEvent("ElectricalArc1", destlocation);
        Effect("TELEPORT", GetWaypointX(destlocation), GetWaypointY(destlocation), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetWaypointX(destlocation), GetWaypointY(destlocation), 0.0, 0.0);
    }
}

int NpcTable()
{
    int npctable[NPC_TABLE_COUNT];

    return &npctable;
}

void InitializeNpcTable()
{
    int *ntable = NpcTable(), i;

    for (i = 0 ; i < NPC_TABLE_COUNT ; Nop(++i))
    {
        ntable[i] = Object("XNPC" + IntToString(i));

        ObjectOff(ntable[i]);
        Enchant(GetLastItem(ntable[i]), "ENCHANT_INVULNERABLE", 0.0);
        LookWithAngle(GetLastItem(ntable[i]), i / 8);
    }
}

int PickFromNpcTable(int pick)
{
    if (pick >= NPC_TABLE_COUNT)
    {
        UniPrintToAll("errorprint::buffer overrun::void PickFromNpcTable(int pick)::in file eu_resp.c");
        return 0;
    }
    int *npc = NpcTable();

    return npc[pick];
}

int limitHp(int order)
{
    int hptable[] = {
        20000 - 100, 20000 - 100, 20000 - 100,
        20000 - 200, 20000 - 200, 20000 - 125, 20000 - 125
    };

    return hptable[order];
}

int maxLessons(int mode)
{
    int games;

    if (mode > 0)
        games += mode;
    return games;
}

void setDeaths()
{
    int classtype = GetDirection(GetLastItem(self));
    int deathcount;

    if (CurrentHealth(self) <= limitHp(classtype))
    {
        int deadcin = CreateObjectAt("swordsman", GetObjectX(self), GetObjectY(self));

        LookWithAngle(deadcin, GetDirection(self));
        Damage(self, other, 100, 14);
        Damage(deadcin, 0, 999, 14);
        Delete(GetLastItem(self));

        if (CurrentHealth(self) < 19000)
            UniPrintToAll("경고!_ 방금 사망한 유닛의 체력이 19000 미만으로 하락했습니다.");
        Delete(self);
        int nexttarget = chooseTarget(classtype * 8);
        if (!nexttarget)
            UniPrintToAll(botType(classtype) + " 세력이 모두 격추되었습니다.");
        else
            FrameTimerWithArg(300, nexttarget, releaseEnemy);
        int plr = checkOwner(other);
        if (plr != -1)
            ChangeScore(player[plr], 1);
        if ((++deathcount) == maxLessons(0))
            victoryMission(self);
    }
}

int checkOwner(int target)
{
    int i;

    for (i = 0 ; i < 10 ; Nop(++i))
    {
        if (IsOwnedBy(target, player[i]))
            return i;
    }
    return -1;
}

void victoryMission(int posunit)
{
    int i;

    UniPrintToAll("승리_!!- 전장에 모든 적들을 격추시켰습니다.");
    for (i = 0 ; i < 10 ; Nop(++i))
    {
        int pUnit = player[i];

        if (CurrentHealth(pUnit))
        {
            Effect("WHITE_FLASH", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
            PlaySoundAround(pUnit, 306);
            PlaySoundAround(pUnit, 777);
        }
    }
    TeleportLocation(73, GetObjectX(posunit), GetObjectY(posunit));
    MissionSuccess();
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    return plr;
}

void PlayerFailToJoin()
{
    UniPrintToAll("현재 맵에 플레이어를 수용가능한 수치가 초과되었기 때문에 더 이상 입장하실 수 없습니다.");
    MoveObject(other, LocationX(76), LocationY(76));
}

void PlayerClassOnJoin(int plr)
{
    int pUnit = player[plr];

    respawnLocation(pUnit);
}

void getPlayer()
{
    int i;

    while (true)
    {
        if (CurrentHealth(other))
        {
            int plr = CheckPlayer();
            for (i = 9 ; i >= 0 && plr < 0 ; Nop(i --))
            {
                if (!MaxHealth(player[i]))
                {
                    plr = PlayerClassOnInit(i, GetCaller());
                    break;
                }
            }
            if (plr + 1)
            {
                PlayerClassOnJoin(plr);
                break;
            }
            PlayerFailToJoin();
        }
        break;
    }
}

void PlayerClassOnDeath(int plr)
{ }

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (HasEnchant(pUnit, "ENCHANT_SNEAK"))
    {
        EnchantOff(pUnit, "ENCHANT_SNEAK");
        RemoveTreadLightly(pUnit);
        putTrapBottom(plr);
    }
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (true)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void AutoTargetDeathray(int k)
{
    float pos_x = UnitAngleCos(player[k], 20.0);
    float pos_y = UnitAngleSin(player[k], 20.0);

    int unit = CreateObjectAt("Bat", GetObjectX(player[k]) + pos_x, GetObjectY(player[k]) + pos_y);
    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    LookWithAngle(unit + 1, k);
    DeleteObjectTimer(unit, 1);
    SetOwner(player[k], unit);
    SetUnitScanRange(unit, 450.0);
    LookWithAngle(unit, GetDirection(player[k]));
	AggressionLevel(unit, 1.0);

	SetCallback(unit, 3, ShotDeathray);
}

void ShotDeathray()
{
    int ptr = GetTrigger() + 1;
    int plr = GetDirection(ptr);

    CastSpellObjectObject("SPELL_DEATH_RAY", self, other);
    CastSpellObjectObject("SPELL_DEATH_RAY", player[plr], other);
    Delete(ptr);
}

void putTrapBottom(int plr)
{
    int pUnit = player[plr];

    SetOwner(pUnit, PlayerPutTrap(plr, pUnit));
    Effect("SMOKE_BLAST", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    Effect("JIGGLE", GetObjectX(pUnit), GetObjectY(pUnit), 10.0, 0.0);
    PlaySoundAround(pUnit, 39);
}

int PlayerPutTrap(int plr, int pUnit)
{
    int traps[10];

    if (IsObjectOn(traps[plr]))
        Delete(traps[plr]);
    traps[plr] = CreateObjectAt("BearTrap", GetObjectX(pUnit), GetObjectY(pUnit));
    return traps[plr];
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

void setUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void MissionSuccess()
{
	string name = "ManaBombOrb";
	int i = 0, matrix[] = {
        272613948, 285245316, 270549570, 285229060, 1478509122, 285230087, 
        673202754, 285221892, 1145029180, 285213700,
	    33554944, 285278149, 0, 285212672, 2021647356, 16131, 67371524,
        285229188, 67371524, 293355652, 2013529084, 16131 };

	while(i < 22)
		drawMissionSuccess(matrix[i++], name);
}

void drawMissionSuccess(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = LocationX(73);
		pos_y = LocationY(73);
	}
	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg_0)
			Nop(CreateObject(name, 73));
		if (count % 62 == 61)
            TeleportLocationVector(73, -122.0, 2.0);
		else
            TeleportLocationVector(73, 2.0, 0.0);
		Nop(count ++);
	}
	if (count >= 682)
	{
		count = 0;
		TeleportLocation(73, pos_x, pos_y);
	}
}

void errorProcess()
{
    UniPrintToAll("경고_!!- 0, 0 위치에 뭔가 감지됨.");
}

void MapInitialize()
{
    MusicEvent();
    initMap();
    InitializeNpcTable();

    //loop
    FrameTimer(10, PlayerClassOnLoop);

    //delay_run
    FrameTimer(1, MakeCoopTeam);

    RegistSignMessage(Object("SelCharSign"), "상대할 캐릭터를 선택해주세요! 선택하려는 NPC 와 접촉하시면 됩니다");
    RegistSignMessage(Object("mapinfo1"), "@이스테이트 컴까기@   제작. 녹스게임리마스터");

    string mapinfo = "이스테이트 컴까기입니다! 다른 컴까기에 비해서 어렵지 않아요~ 무엇보다 라이프 제한이 없어요";

    RegistSignMessage(Object("mapinfo2"), mapinfo);
    RegistSignMessage(Object("mapinfo3"), mapinfo);

    RegistSignMessage(Object("mapinfo4"), "시작하려면 이 망할 선장놈에게 말을 걸어라-!");
    RegistSignMessage(Object("mapinfo5"), mapinfo);
    RegistSignMessage(Object("mapinfo6"), "안녕하세요~ 이스테이트 컴까기 입니다. 잘 부탁드려요!");

    CmdLine("set lessons 0", false);
}

