
#include "fov_utils.h"
#include "libs/weapon_effect.h"
#include "libs/itemproperty.h"
#include "libs/buff.h"
#include "libs/sound_define.h"
#include "libs/hash.h"


int getUserWeaponFxHash(){
    int hash;
    if (!hash)HashCreateInstance(&hash);
    return hash;
}

void DeferredPickupUserWeapon(int weapon)
{
    if (IsObjectOn(weapon))
    {
        RegistItemPickupCallback(weapon, OnPickupUserWeapon);
        return;
    }
    int del;

    HashGet(getUserWeaponFxHash(), weapon, &del, TRUE);
}

void OnPickupUserWeapon()
{
    int *pMem;

    FrameTimerWithArg(1, GetTrigger(), DeferredPickupUserWeapon);
    if (HashGet(getUserWeaponFxHash(), GetTrigger(), &pMem, FALSE))
    {
        Delete(pMem[0]);
        Effect("CYAN_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void DeferredDiscardUserWeapon(int weapon)
{
    if (!MaxHealth(weapon))
        return;

    int *pMem;
    float xy[]={GetObjectX(weapon), GetObjectY(weapon)};
    if (HashGet(getUserWeaponFxHash(), weapon, &pMem, FALSE))
    {
        pMem[0]=CreateObjectById(OBJ_BLUE_SUMMONS, xy[0], xy[1]);
        PlaySoundAround(weapon, SOUND_BurnCast);
        Effect("SPARK_EXPLOSION", xy[0], xy[1], 0.0, 0.0);
    }
    // else
    // {
    //     WriteLog("fail hash error - deferredDiscardUserWeapon");
    // }
    
}

void OnDiscardUserWeapon()
{
    PushTimerQueue(1, GetTrigger(), DeferredDiscardUserWeapon);
}

void CreateUserWeaponFX(int weapon)
{
    int *pMem;

    AllocSmartMemEx(4, &pMem);
    pMem[0]=CreateObjectById(OBJ_BLUE_SUMMONS, GetObjectX(weapon), GetObjectY(weapon));
    HashPushback(getUserWeaponFxHash(), weapon, pMem);
    RegistItemPickupCallback(weapon, OnPickupUserWeapon);
    SetUnitCallbackOnDiscardBypass(weapon, OnDiscardUserWeapon);
}





////////////////////////////////////////////////


#define SPECIAL_WEAPON_PROPERTY 0
#define SPECIAL_WEAPON_FXCODE 1
#define SPECIAL_WEAPON_EXECCODE 2
#define SPECIAL_WEAPON_MAX 3

int CreateMagicArrow(float xProfile, float yProfile, int owner)
{
    int unit = CreateObjectById(OBJ_ARCHER_ARROW, xProfile, yProfile);
    
    SetOwner(owner, unit);
    LookAtObject(unit, owner);
    LookWithAngle(unit, GetDirection(unit) + 128);
    PushObject(unit, 20.0, GetObjectX(owner), GetObjectY(owner));
    SetUnitEnchantCopy(unit, GetLShift(ENCHANT_HASTED)|GetLShift(ENCHANT_REFLECTIVE_SHIELD));
    return unit;
}

void ShotTripleArrow()
{
    int owner=OTHER;
    float dx = UnitAngleCos(owner, 9.0), dy = UnitAngleSin(owner, 9.0);
    int mis = CreateMagicArrow(GetObjectX(owner) + dx, GetObjectY(owner) + dy, owner);
    CreateMagicArrow(GetObjectX(mis) - (dy * 0.3), GetObjectY(mis) + (dx * 0.3), owner);
    CreateMagicArrow(GetObjectX(mis) + (dy * 0.3), GetObjectY(mis) - (dx * 0.3), owner);
    PlaySoundAround(owner, SOUND_CrossBowShoot);
}

int CreateTripleArrowAxe(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
        SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
        SpecialWeaponPropertyExecuteScriptCodeGen(ShotTripleArrow, &property[SPECIAL_WEAPON_EXECCODE]);
        SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_BATTLE_AXE,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_stun1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}


////////////////////////////////////////////////


void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            int hash=GetUnit1C(SELF);

            if (hash)
            {
                if (HashGet(hash,GetCaller(),NULLPTR,FALSE))
                    return;
                HashPushback(hash,GetCaller(),TRUE);
                Damage(OTHER, owner, 200, DAMAGE_TYPE_ELECTRIC);
                Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            }
        }
    }
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 17.0), yvect = UnitAngleSin(OTHER, 17.0);
    int arr[30], u=0,hash;

    arr[u] = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(OTHER)+xvect,GetObjectY(OTHER)+yvect);
    HashCreateInstance(&hash);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateById(OBJ_DEMON, GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetUnitFlags(arr[u],GetUnitFlags(arr[u])^UNIT_FLAG_NO_PUSH_CHARACTERS);
        SetUnit1C(arr[u],hash);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    DrawYellowLightningFx(GetObjectX(arr[0]), GetObjectY(arr[0]), GetObjectX(arr[u-1]), GetObjectY(arr[u-1]), 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}
int CreateThunderSword(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &property[SPECIAL_WEAPON_EXECCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_GREAT_SWORD,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_stun1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}

////////////////////////////////////////////////

void onFONSplash(){
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 600, DAMAGE_TYPE_PLASMA);
        }
    }
}

void onFONCollide(){
    if (!GetTrigger()) return;

    while (TRUE)
    {
        if (!GetCaller())
            break;

        if (CurrentHealth(OTHER))
        {
            if (IsAttackedBy(OTHER,SELF)){
                break;
            }
        }
        return;
    }
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    int owner=GetOwner(SELF);

    Delete(SELF);
    if (owner)
    {
        DeleteObjectTimer( CreateObjectById(OBJ_FORCE_OF_NATURE_CHARGE,x,y), 30);
        SplashDamageAtEx(owner, x,y,90.0,onFONSplash);
    }
}

void ShotFON()
{
    int fon=CreateObjectById(OBJ_DEATH_BALL,GetObjectX(OTHER)+UnitAngleCos(OTHER,9.0),GetObjectY(OTHER)+UnitAngleSin(OTHER,9.0));
    int ptr=UnitToPtr(fon);

    SetOwner(OTHER,fon);
    PlaySoundAround(OTHER,SOUND_ForceOfNatureRelease);
    PushObject(fon,22.0,GetObjectX(OTHER),GetObjectY(OTHER));
    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(fon,onFONCollide);
}

int CreateFONAxe(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(ShotFON, &property[SPECIAL_WEAPON_EXECCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_BATTLE_AXE,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_venom4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}

