
#include "pam00753_mon.h"
#include "libs/coopteam.h"
#include "libs/game_flags.h"
#include "libs/playerupdate.h"
#include "libs/wallutil.h"
#include "libs/waypoint.h"
#include "libs/mathlab.h"

void removeFrontWalls(short startAt, short directAt)
{
	float point1[]={LocationX(startAt),LocationY(startAt)};
	float point2[]={LocationX(directAt),LocationY(directAt)};
	float xyVect[]={0,0,
	};	
	ComputePointRatio(point1,point2,xyVect,23.0);
	
	int wall= WallUtilGetWallAtObjectPosition(startAt);
	while (wall)
	{
		WallOpen(wall);
		TeleportLocationVector(startAt,xyVect[0],xyVect[1]);
		wall=WallUtilGetWallAtObjectPosition(startAt);
	}
}

void StartArea1()
{
	ObjectOff(SELF);
	removeFrontWalls(13,14);
	AppearMonsterAtMarker(0);
}

void InitializeSubPart()
{
    MakeCoopTeam();
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
    	SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}

void DeInitializeSubPart()
{
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}
