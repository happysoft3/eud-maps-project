
int GetItemMaster() //virtual
{ }

#define MAX_PLAYER_COUNT 32

int GetPlayer(int pIndex) //virtual
{ }

void SetPlayer(int pIndex, int user) //virtual
{ }

int GetCreature(int pIndex) //virtual
{ }

void SetCreature(int pIndex, int cre) //virtual
{ }

int GetPetLevel(int pIndex) //virtual
{ }

void SetPetLevel(int pIndex, int lv) //virtual
{ }

int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

#define PLAYER_FLAG_FIRST_SKILL 2
#define PLAYER_FLAG_SECOND_SKILL 4
#define PLAYER_FLAG_LAST_SKILL 8
#define PLAYER_FLAG_BONUS_SKILL 16

int PlayerClassCheckFlag(int pIndex, int flags)
{
    return GetPlayerFlags(pIndex) & flags;
}

void PlayerClassSetFlag(int pIndex, int flags)
{
    SetPlayerFlags(pIndex, GetPlayerFlags(pIndex)^flags);
}

int GetCreatureStatExp(int pIndex) //virtual
{ }

void SetCreatureStatExp(int pIndex, int exp) //virtual
{ }

int GetRespawnSpot(int pIndex) //virtual
{ }

void SetRespawnSpot(int pIndex, int spot) //virtual
{ }

int IsPickupableItem(int item) //virtual
{ }

int GetClientKeyState(int pIndex) //virtual
{ }

void SetClientKeyState(int pIndex, int value) //virtual
{ }

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10

int GetLevelBuffer() //virtual
{ }

int GetDropCode() //virtual
{ }

void SetDropCode(int p) //virtual
{ }

void OnCreatureKill(int pIndex, int credit) //virtual
{ }

void OnFieldMobDeath(int mob) //virtual
{ }
