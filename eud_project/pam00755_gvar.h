
#include "libs/define.h"
#include "libs/typecast.h"

#define MAX_PLAYER_COUNT 32

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_HARPOON 32
#define PLAYER_FLAG_THREADLIGHTLY 64

#define ABILITY_ID_BERSERKER_CHARGE 	1
#define ABILITY_ID_WARCRY 			2
#define ABILITY_ID_HARPOON 			3
#define ABILITY_ID_TREAD_LIGHTLY		4
#define ABILITY_ID_EYE_OF_WOLF		5

int GetUserRespawnMark(int pIndex) //virtual
{ }
void SetUserRespawnMark(int pIndex, int mark) //virtual
{ }

void ResetUserRespawnMarkPos(int pIndex)
{
    int mark=GetUserRespawnMark(pIndex);
    int initPos = mark+1;

    if (!ToInt(GetObjectX(initPos)))
    {
        // WriteLog("ResetUserRespawnMarkPos::error");
        return;
    }
    MoveObject(mark, GetObjectX(initPos), GetObjectY(initPos));
}

int FirewayControlPtr() //virtual
{ }

int FonTrapControlPtr() //virtual
{ }

int SentryWallsControlPtr() //virtual
{ }

void MonsterElevator(int n, int setTo, int *get) //virtual
{ }

int GetWarBoss() //virtual
{ }

void SetWarBoss(int boss) //virtual
{ }
