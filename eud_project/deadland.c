
#include "libs\define.h"
#include "libs\printutil.h"
#include "libs\waypoint.h"
#include "libs\buff.h"
#include "libs\bind.h"
#include "libs\sound_define.h"
#include "libs\fxeffect.h"
#include "libs\unitstruct.h"
#include "libs\coopteam.h"
#include "libs\array.h"
#include "libs\wallutil.h"
#include "libs\username.h"
#include "libs\network.h"
#include "libs\drawtext.h"

#define RAII_HEADER_LENGTH 28

#define PLAYER_DEATH_FLAG 2

#define LAST_LOCATION 74

int m_rayStatue[2];
int *m_currentTrap;
int *m_multipleTrap;
int m_touchMob[52];
int m_fonTrap;

int m_electricTrapEnable;
int m_frontElectricTrapFlag;
int m_backElectricTrapFlag;
int m_boundLocations[9];
int m_mirrorLocations[9];
int m_area42Condvar1;
int m_area42Condvar2;
int m_area42Condvar3;

int m_player[10];
int m_pFlags[10];
int m_rubydoor;
int m_rubykey;

int *m_preventRock;

void TeleportPlayerWithFx(int pUnit, int destLocationId)
{
    Effect("COUNTERSPELL_EXPLOSION", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    PlaySoundAround(pUnit, SOUND_BlindOff);
    MoveObject(pUnit, LocationX(destLocationId), LocationY(destLocationId));
    Effect("COUNTERSPELL_EXPLOSION", LocationX(destLocationId), LocationY(destLocationId), 0.0, 0.0);
    Effect("VIOLET_SPARKS", LocationX(destLocationId), LocationY(destLocationId), 0.0, 0.0);
    PlaySoundAround(pUnit, SOUND_HecubahDieFrame98);
}

void InitBoundLocation()
{
    int mirror[] = {236,233,230,237,234,231,238,235,232};
    int bound[]={227,224,221,228,225,222,229,226,223};

    int rep=-1;
    while (++rep<sizeof(m_boundLocations))
    {
        m_mirrorLocations[rep]=mirror[rep];
        m_boundLocations[rep]=bound[rep];
    }
}

void HitBound(int excludeLocationIndex)
{
    int rep=-1;

    while (++rep<sizeof(m_boundLocations))
    {
        if (excludeLocationIndex==rep)
            continue;
        PutBombExplosion(m_boundLocations[rep]);
    }
}

void HitMirror(int excludeLocationIndex)
{
    int rep=-1;

    while (++rep<sizeof(m_mirrorLocations))
    {
        if (excludeLocationIndex==rep)
            continue;
        PutBombExplosion(m_mirrorLocations[rep]);
    }
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);

    return unit;
}

void BreakBoundTriggered()
{
    if (!GetTrigger())
        return;

    if (IsPlayerUnit(OTHER))
    {
        Delete(SELF);
        WallOpen(Wall(129, 113));
        WallOpen(Wall(134, 112));
        UniPrintToAll("바운드 트랩이 완전히 종료되었습니다. 다음 구간으로 가세요");
    }
}

void BoundReset(int endunit)
{
    if (MaxHealth(endunit))
    {
        UniPrintToAll("시간 경과로 인하여 바운드 트랩이 리셋됩니다...");
        Delete(endunit);
        SecondTimer(1, CreateStartBoundUnit);
    }
}

void EndBound()
{
    int locationId = 225;
    int endunit = DummyUnitCreateAt("WizardWhite", LocationX(225), LocationY(225));

    SetCallback(endunit, 9, BreakBoundTriggered);

    UniChatMessage(endunit, "바운드 트랩이 모두 끝났어요!\n나를 만지면 트랩을 완전히 끝낼 수 있어요!!", 120);
    PlaySoundAround(endunit, SOUND_FlagRespawn);

    SecondTimerWithArg(6, endunit, BoundReset);
}

void BoundInProgress(int *pCount)
{
    if (!pCount[0])
    {
        EndBound();
        return;
    }
    --pCount[0];
    int exclude = Random(0, sizeof(m_boundLocations) - 1);

    HitMirror(exclude);
    FrameTimerWithArg(35, exclude, HitBound);
    FrameTimerWithArg(60, pCount, BoundInProgress);
}

void BoundTriggered()
{
    if (!GetTrigger())
        return;

    if (IsPlayerUnit(OTHER))
    {
        Delete(SELF);
        int count = 10;
        WallClose(Wall(129, 113));
        SecondTimerWithArg(2, &count, BoundInProgress);
        UniPrintToAll("바운드를 시작합니다...!!");
    }
}

void CreateStartBoundUnit()
{
    int locationId = 225;
    int unit = DummyUnitCreateAt("Necromancer", LocationX(locationId), LocationY(locationId));

    SetCallback(unit, 9, BoundTriggered);
    WallOpen(Wall(129, 113));
}

void InitPlayerPreventRock()
{
    int rocks[10];
    int count=-1, location = 577;

    while(++count<sizeof(rocks))
    {
        rocks[count]=CreateObjectAt("Rock6", LocationX(location), LocationY(location));

        // SetUnitFlags(rocks[count], GetUnitFlags(rocks[count]) ^ UNIT_FLAG_NO_PUSH_CHARACTERS);
    }
    m_preventRock=&rocks;
}

void TeleportLastTransmissionPos(float xpos, float ypos)
{
    TeleportLocation(LAST_LOCATION, xpos, ypos);
}

void StartTrapProto(int trapFunctionId, int startDelay, int *pDest)
{
    Bind(trapFunctionId, &trapFunctionId+4);
}

void TrapExecutor(int trapFunctionId, int startDelay, int *pOnce)
{
    ObjectOff(SELF);

    if (pOnce[0])
        return;
    
    pOnce[0]=TRUE;
    ClearCurrentTrap();
    if (trapFunctionId)
        StartTrapProto(trapFunctionId, startDelay, &m_currentTrap);
}

static int MapWaypointTable(int idx)
{
    int table[1000];

    return table[idx - 1];
}

void OnBombCollide()
{
    if (IsPlayerUnit(OTHER))
    {
        if (UnitCheckEnchant(OTHER, GetLShift(ENCHANT_INVULNERABLE)))
            EnchantOff(OTHER, EnchantList(ENCHANT_INVULNERABLE));
        Damage(OTHER, 0, 255, 14);
    }
}

void PutBombExplosion(int locationId)
{
    int bomb = CreateObjectAt("DemonGenerator", LocationX(locationId), LocationY(locationId));

    ObjectOff(bomb);
    Frozen(bomb, TRUE);
    SetUnitCallbackOnCollide(bomb, OnBombCollide);
    SetUnitFlags(bomb, GetUnitFlags(bomb) ^ UNIT_FLAG_NO_PUSH_CHARACTERS);
    DeleteObjectTimer(bomb, 1);
    Effect("EXPLOSION", GetObjectX(bomb), GetObjectY(bomb), 0.0, 0.0);
    PlaySoundAround(bomb, SOUND_BomberDie);
}

void AllocRAIIMemory(int size, int *pDest, int *pun)
{
    int un = CreateObjectAt(StringUtilFindUnitNameById(1), 100.0, 100.0);
    int *ptr = UnitToPtr(un);
    int *alloc = MemAlloc(size);

    ptr[191] = alloc;
    pDest[0] = alloc;
    if (pun)
        pun[0] = un;
}

void MakeHeader(int count, int delay, int *pDest)
{
    int *mem, c;

    AllocRAIIMemory(RAII_HEADER_LENGTH + (count*4), &mem, &c);
    mem[0] = count;
    mem[1] = delay;
    mem[2] = 0;
    mem[3] = c;
    mem[4] = 0; //condition_ptr
    mem[5] = 0; //condition_value
    mem[6] = 0; //executor
    pDest[0] = mem;
}

void NewSingleNode(int locationId, int delay, int *pDest)
{
    int *ptr;

    MakeHeader(1, delay, &ptr);
    if (pDest)
        pDest[0] = ptr;
    ptr+=RAII_HEADER_LENGTH;
    ptr[0]=locationId;
}

void NewMultipleNode(int *locations, int count, int delay, int *pDest)
{
    int *ptr;

    MakeHeader(count, delay, &ptr);
    if (pDest)
        pDest[0] = ptr;
    ptr+=RAII_HEADER_LENGTH;
    while (count--)
    {
        ptr[0] = locations[0];
        ptr+=4;
        locations+=4;
    }
}

void AppendNextNode(int *curNode, int *nextNode)
{
    curNode[2] = nextNode;
}

void SetCooldown(int *cur, int cooldown)
{
    cur[1] = cooldown;
}

void SetNodeCondition(int *cur, int *pCondVar, int varValue)
{
    cur[4] = pCondVar;
    cur[5] = varValue;
}

void SetExecutorPass(int *cur, int execFunctionId)
{
    cur[6] = execFunctionId;
}

void ExcuteOneOfField(int *curNode)
{
    int count = curNode[0];
    int *locations = ArrayRefAdd(curNode, RAII_HEADER_LENGTH);

    while (count--)
        PutBombExplosion(locations[count]);
}

void RemoveAllNode(int *front)
{
    int *cur = front, *del;

    while (cur)
    {
        del = cur;
        cur = cur[2];
        Delete(del[3]);
        if (cur==front)
            break;
    }
}

void ClearCurrentTrap()
{
    if (m_currentTrap)
    {
        RemoveAllNode(m_currentTrap);
        m_currentTrap=NULLPTR;
    }
    if (m_multipleTrap)
    {
        RemoveAllNode(m_multipleTrap);
        m_multipleTrap=NULLPTR;
    }
}

void ExecutorPassProto(int functionId)
{
    Bind(functionId, &functionId+4);
}

void ListTraversal(int *startNode)
{
    int *trav=startNode, delay, *condvar;

    while (trav)
    {
        if (!IsObjectOn(trav[3]))
            return;

        if (trav[4])
        {
            condvar = trav[4];
            if (condvar[0] != trav[5])
            {
                trav=trav[2];
                continue;
            }
        }
        if (trav[6])
        {
            ExecutorPassProto(trav[6]);
            trav=trav[2];
            continue;
        }

        delay = trav[1];

        ExcuteOneOfField(trav);
        if (!delay)
            ++delay;
        Nop(FrameTimerWithArg(trav[1], trav[2], ListTraversal));
        break;
    }
}

void InitTrapA11(int startDelay, int *pDest)
{
    int arr[] = {3,4,5,6,7,8,9,10,11,12,13,14,15,16,854};
    int *front, *back, *cur;
    int delay = 5;

    NewSingleNode(arr[0], delay, &front);
    back=front;

    int count=0;
    while (++count<sizeof(arr))
    {
        NewSingleNode(arr[count], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    SetCooldown(back, 30);
    AppendNextNode(back, front);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap01On()
{
    int once;

    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(InitTrapA11, 3, &once);
}

void InitTrapA12(int startDelay, int *pDest)
{
    int odd[] = {18,19,20,21,22,23,24,25};
    int even[] = {26,27,28,29,30,31,32};
    int *front, *back;

    NewMultipleNode(&odd, sizeof(odd), 32, &front);
    NewMultipleNode(&even, sizeof(even), 32, &back);
    AppendNextNode(front, back);
    AppendNextNode(back, front);

    FrameTimerWithArg(startDelay, front, ListTraversal);
    pDest[0] = front;
}

void BomberTrap02On()
{
    int once;

    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));

    TrapExecutor(InitTrapA12, 5, &once);
}

void InitTrapA13(int startDelay, int *pDest)
{
    int pat1[]={33, 34, 36, 37, 38, 39, 41, 42, 55,60};
    int pat2[]={43, 44, 46, 47, 48,49,51,52,45,50};
    int pat3[]={53,54,56,57,58,59,61,62,35,40};

    int *front, *back, *cur;

    NewMultipleNode(&pat1, sizeof(pat1), 9, &front);
    back=front;
    NewMultipleNode(&pat2, sizeof(pat2), 9, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), 9, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), 9, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat2, sizeof(pat2), 9, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat1, sizeof(pat1), 9, &cur);
    AppendNextNode(back, cur);
    back=cur;
    SetCooldown(back, 22);
    AppendNextNode(back, front);
    FrameTimerWithArg(startDelay, front, ListTraversal);
    pDest[0] = front;
}

void BomberTrap03On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(InitTrapA13, 3, &once);
}

void InitTrapA14(int startDelay)
{
    int part[] = {63,64,65,66,67,68,69,70};
    int *front, *back, *cur;

    NewMultipleNode(&part, sizeof(part), 16, &front);   //18
    back=front;
    NewSingleNode(72, 16, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewSingleNode(73, 16, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewSingleNode(71, 18, &cur);
    AppendNextNode(back, cur);
    back=cur;
    AppendNextNode(back, front);

    FrameTimerWithArg(startDelay, front, ListTraversal);
    m_currentTrap = front;
}

void BomberTrap04On()
{
    int once;

    ObjectOff(SELF);
    if (once)
        return;

    once = TRUE;
    TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    ClearCurrentTrap();
    InitTrapA14(3);
}

void InitTrapA15(int startDelay)
{
    int delay=5;
    int *front, *back, *cur;

    int pr1[] = {75,92};
    NewMultipleNode(&pr1, sizeof(pr1), delay, &front);
    back=front;

    NewSingleNode(76, delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pr2[] = {77,78};
    NewMultipleNode(&pr2, sizeof(pr2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pr3[] = {79,80};
    NewMultipleNode(&pr3, sizeof(pr3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pr4[] = {81,82};
    NewMultipleNode(&pr4, sizeof(pr4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pr5[] = {83,84};
    NewMultipleNode(&pr5, sizeof(pr5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pr6[] = {86,85};
    NewMultipleNode(&pr6, sizeof(pr6), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pr7[] = {88,87};
    NewMultipleNode(&pr7, sizeof(pr7), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pr8[] = {90,89};
    NewMultipleNode(&pr8, sizeof(pr8), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewSingleNode(91, delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    AppendNextNode(back, front);
    FrameTimerWithArg(startDelay, front, ListTraversal);
    m_currentTrap = front;
}

void BomberTrap05On()
{
    int once;

    ObjectOff(SELF);
    if (once)
        return;

    once = TRUE;
    TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    ClearCurrentTrap();
    InitTrapA15(3);
}

void InitTrapA16(int startDelay, int *pDest)
{
    int fastdelay=2;
    int lazydelay=8;
    int *front, *back, *cur;
    int arr[] = {93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110};

    NewSingleNode(arr[0], fastdelay, &front);
    back=front;
    int count=0;
    while(++count<sizeof(arr))
    {
        NewSingleNode(arr[count], fastdelay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    NewSingleNode(111, lazydelay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewSingleNode(112, lazydelay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    AppendNextNode(back, front);

    FrameTimerWithArg(startDelay, front, ListTraversal);
    pDest[0] = front;
}

void BomberTrap06On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(InitTrapA16, 3, &once);
}

void InitTrapA17(int startDelay, int *pDest)
{
    int delay=4;
    int *front, *back, *cur;

    NewSingleNode(113, delay, &front);
    back=front;
    NewSingleNode(114,delay,&cur);
    AppendNextNode(back, cur);
    back=cur;
    NewSingleNode(115,delay,&cur);
    AppendNextNode(back, cur);
    back=cur;
    int pat1[]={116,113};
    NewMultipleNode(&pat1, sizeof(pat1), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat1[0]; ++pat1[1];   //117
    NewMultipleNode(&pat1, sizeof(pat1), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat1[0]; ++pat1[1];       //118
    NewMultipleNode(&pat1, sizeof(pat1), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    int pat2[]={119,116, 113};
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    int pat3[]={120,114};
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat3[0]; ++pat3[1];   //121, 115
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat3[0]; ++pat3[1];   //122, 116
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat3[0]; pat3[1]=120; //123,120
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat3[0]; ++pat3[1]; //124,121
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat3[0]; ++pat3[1]; //125,122
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    ++pat3[0]; ++pat3[1]; //126,123
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    NewSingleNode(127, delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    NewSingleNode(128, delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    NewSingleNode(129, delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    InitTrapA17Back(front, back, cur, delay);
    FrameTimerWithArg(startDelay, front, ListTraversal);
    pDest[0] = front;
}

void InitTrapA17Back(int *front, int *back, int *cur, int delay)
{
    int arr[]={128,127,123,122,121,120,116,115,114};

    int count=0;
    while (count<sizeof(arr))
    {
        NewSingleNode(arr[count++], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
}

void BomberTrap07On()
{
    int once;
    TrapExecutor(InitTrapA17, 2, &once);
}

void BomberTrap08On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(0, 0, &once);
}

void shotDeathstatue()
{
    int target = m_rayStatue[1];

    if (DistanceUnitToUnit(OTHER, m_rayStatue[0]) < DistanceUnitToUnit(OTHER, m_rayStatue[1]))
        target = m_rayStatue[0];
    CastSpellObjectLocation("SPELL_DEATH_RAY", target, GetObjectX(OTHER), GetObjectY(OTHER));    
}

void InitTrapA18(int startDelay, int delay, int *pDest)
{
    int *front, *back, *cur;

    int step1[]={130,131,132,133,134,135,136,137};

    int count=0;
    NewSingleNode(step1[0], delay, &front);
    back=front;
    while(count++<sizeof(step1))
    {
        NewSingleNode(step1[count], delay, &cur);
        AppendNextNode(back,cur);
        back=cur;
    }
    InitTrapA18Multi(front, back, cur, delay);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void InitTrapA18Multi(int *front, int *back, int *cur, int delay)
{
    int step1[]={139, 138};
    NewMultipleNode(&step1, sizeof(step1), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    step1[0]=141; step1[1]=140;
    NewMultipleNode(&step1, sizeof(step1), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int step2[]={142,143,144,145,146,147,148,149};
    int count;
    while (count<sizeof(step2))
    {
        NewSingleNode(step2[count++], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
}

void InitTrapA18Re(int startDelay, int *pDest)
{
    int *front, *back, *cur;
    int step1[]={130,135,142,147};
    int step2[]={131,136,143,148};
    int step3[]={132,137,144,149};
    int step4[]={133,138,139,145};
    int step5[]={134,140,141,146};
    int delay=15;

    NewMultipleNode(&step1, sizeof(step1), delay, &front);
    NewMultipleNode(&step2, sizeof(step2), delay, &cur);
    AppendNextNode(front, cur);
    back=cur;
    NewMultipleNode(&step3, sizeof(step3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&step4, sizeof(step4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&step5, sizeof(step5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    pDest[0] = front;
    AppendNextNode(back, front);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap09On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(InitTrapA18Re, 3, &once);
}

void DisableLOTDMagicWalls01()
{
    WallToggle(Wall(74,98));
    WallToggle(Wall(78,102));
    int wx=73, wy=99;
    int rep=-1;

    while ((++rep)<5)
        WallToggle(Wall(wx++, wy++));
}

void BomberTrap10On()
{
    int once;
        
    ObjectOff(SELF);
    if (once)
        return;

    TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    once = TRUE;

    ClearCurrentTrap();
    InitTrapA19(3, &m_currentTrap);
    InitTrapA19Rev(3, &m_multipleTrap);
}

void InitTrapA19(int startDelay, int *pDest)
{
    int delay=9;
    int pat[]={150,151,152,159,160,161};
    int pat2[]={156,157,158,153,154,155};
    int *front, *back, *cur;

    NewSingleNode(pat[0], delay, &front);
    back=front;
    int count=0;
    while((++count)<sizeof(pat))
    {
        NewSingleNode(pat[count], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void InitTrapA19Rev(int startDelay, int *pDest)
{
    int delay=9;
    int pat[]={156,157,158,153,154,155};
    int *front, *back, *cur;

    NewSingleNode(pat[0], delay, &front);
    back=front;
    int count=0;
    while((++count)<sizeof(pat))
    {
        NewSingleNode(pat[count], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap11On()
{
    int once;
    TrapExecutor(StartTrapA1A, 5, &once);
}

void StartTrapA1A(int startDelay, int *pDest)
{
    int delay = 3;
    int pat[] = {162, 176, 163, 163, 164, 187, 165, 186, 167, 184,
    168, 183, 169, 182, 170, 181, 171, 180, 172, 179, 173, 178,
    174, 177, 175, 175};
    int *front, *back, *cur;

    NewMultipleNode(ArrayRefN(&pat, 0), 2, delay, &front);
    back=front;
    int count=0, max=sizeof(pat)/2;
    while ((++count) < max)
    {
        NewMultipleNode(ArrayRefN(&pat, count*2), 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    InitTrapA1A2Rev(front, back, cur, delay);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void InitTrapA1A2Rev(int *front, int *back, int *cur, int delay)
{
    int pat[]={162,176,163,163,164,187,165,186,166,185,167,184,168,
    183,169,182,170,181,171,180,173,178,174,177,175,175
    };
    int count = 0, max=sizeof(pat)/2;
    while (count++<max)
    {
        NewMultipleNode(ArrayRefN(&pat, count*2), 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
}

void BomberTrap12On()
{
    int once;
    TrapExecutor(StartTrapA1B, 5, &once);
}

void StartTrapA1B(int startDelay, int *pDest)
{
    int delay=10;
    int *front, *back, *cur;
    
    int pat1[] = {188, 191,194,197,200,203};
    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;

    int pat2[] ={189,192,195,198,201};
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int pat3[] ={190,193,196,199,202};
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;

    AppendNextNode(back,front);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void InitTrapA1C2(int *front, int *back, int *cur, int delay)
{
    int pat[] = {210, 220, 217};

    NewMultipleNode(&pat, sizeof(pat), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    AppendNextNode(back,front);
}

void StartTrapA1C(int startDelay, int *pDest)
{
    int delay=5;
    int *front, *back, *cur;

    int pat[]={211, 218, 212, 219, 204, 205, 206, 213, 207, 214, 208, 215, 209, 216};

    NewMultipleNode(ArrayRefN(&pat, 0), 2, delay, &front);
    back=front;
    int count=0, max=sizeof(pat)/2;

    while (count++<max)
    {
        NewMultipleNode(ArrayRefN(&pat, count*2), 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    InitTrapA1C2(front, back, cur, delay);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap13On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapA1C, 3, &once);
}

void SetSavePoint()
{
    ObjectOff(SELF);
    TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    PlaySoundAround(OTHER, SOUND_SoulGateTouch);
    Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
}

void BomberTrap14On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(0, 3, &once);
}

void SquareJump1()
{
    TeleportPlayerWithFx(OTHER, 826);
}

void HecubahOnTouched()
{
    if (IsPlayerUnit(OTHER))
    {
        if (UnitCheckEnchant(OTHER, GetLShift(ENCHANT_INVULNERABLE)))
            EnchantOff(OTHER, EnchantList(ENCHANT_INVULNERABLE));
        Damage(OTHER, 0, 100, DAMAGE_TYPE_ELECTRIC);
    }
}

void SpawnHecubahOneOf(int location, int *pDest)
{
    int hec=CreateObjectAt("Hecubah", LocationX(location), LocationY(location));

    Enchant(hec, EnchantList(ENCHANT_CONFUSED), 0.0);
    Enchant(hec, EnchantList(ENCHANT_INVULNERABLE), 0.0);
    SetCallback(hec, 9, HecubahOnTouched);
    if (pDest)
        pDest[0] = hec;
}

void SpawnDeathHecubahAll()
{
    int count;

    if (count < sizeof(m_touchMob))
    {
        SpawnHecubahOneOf(780, ArrayRefN(&m_touchMob, count));
        FrameTimer(1, SpawnDeathHecubahAll);
        ++count;
    }
}

void RemoveAllTouchMob()
{
    int rep=-1;

    while ((++rep) < sizeof(m_touchMob))
    {
        if (CurrentHealth(m_touchMob[rep]))
        {
            EnchantOff(m_touchMob[rep], EnchantList(ENCHANT_INVULNERABLE));
            Damage(m_touchMob[rep], 0, MaxHealth(m_touchMob[rep] + 1), 14);
            DeleteObjectTimer(m_touchMob[rep], 11);
        }
    }
}

void ActivationDeathTouchRoom()
{
    ObjectOff(SELF);

    int once;

    if (once)
        return;

    once=TRUE;
    FrameTimer(1, SpawnDeathHecubahAll);
}

void DestroyTouchMobRoom()
{
    ObjectOff(SELF);
    int once;
    if (once)
        return;

    once=TRUE;
    RemoveAllTouchMob();
}

void ClientsideProcess()
{
    int drawtextFunction = DrawTextBuildTextDrawFunction();

    DrawTextSetupBottmText(2558, drawtextFunction, 0x7f6, "승리하셨어요..!!");
}

static void NetworkUtilClientMain()
{
    ClientsideProcess();
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
}

void PatrolRot(int rot)
{
    if (!IsObjectOn(rot))
        return;
    int tof;

    if (!tof)
        Move(rot, 759);
    else
    {
        Move(rot, 764);
    }
    tof ^= TRUE;
    SecondTimerWithArg(5, rot, PatrolRot);
}

void RubyDoorChecker()
{
    if (!IsLocked(m_rubydoor))
        return;

    if (!IsObjectOn(m_rubykey))
    {
        m_rubykey = CreateObjectAt("RubyKey", LocationX(853), LocationY(853));
        UnitNoCollide(m_rubykey);
    }
    SecondTimer(4, RubyDoorChecker);
}

void WakeDeco(int location)
{
    int wake=CreateObjectAt("TeleportWake", LocationX(location), LocationY(location));

    Frozen(wake, TRUE);
    UnitNoCollide(wake);
}

void OnPickedFlag()
{
    UniPrint(OTHER, "시작하려면 더 가까이 오시기 바랍니다 by. 제작자");
}

void PutWinnerFlag(float xpos, float ypos, int *pDest)
{
    int flag = CreateObjectAt("GreenFlag", xpos, ypos);

    UnitNoCollide(flag);
    SetUnitCallbackOnPickup(flag, OnPickedFlag);
    if (pDest)
        pDest[0]=flag;
}

void YoureWinner()
{
    int once;

    if (once)
        return;
    once=TRUE;
    PlaySoundAround(OTHER, SOUND_BigGong);
    PlaySoundAround(OTHER, SOUND_FlagCapture);

    UniPrintToAll("!당신은 승리하였습니다! 저 앞에 보물은 모두 당신의 소유입니다!");

    DrawImageAtEx(LocationX(300), LocationY(300),2558);
    DrawImageAtEx(LocationX(410), LocationY(410),2558);
}

void MapInitialize()
{
    MusicEvent();
    m_rayStatue[0]=Object("DeathStatue");
    m_rayStatue[1]=Object("DeathStatue2");
    FrameTimerWithArg(30, Object("RotSpike"), PatrolRot);

    FrameTimer(1, CreateStartBoundUnit);
    InitBoundLocation();
    InitPlayerPreventRock();
    FrameTimer(10, PlayerClassOnLoop);

    m_rubydoor = Object("area5rubydoor");

    SecondTimer(4, RubyDoorChecker);
    RegistSignMessage(Object("msg1"), "이 비콘은, 사용자가 가장 최근에 완료한 함정으로 이동할 수 있는 포탈입니다");
    RegistSignMessage(Object("msg2"), "당신은 모든 시련을 통과한 최후의 인류이다! 당신은 오늘의 우승자이다!!");
    RegistSignMessage(Object("msg3"), "저 앞에 보이는 헤쿠바 옷깃에 스치기 만 하여도 당신은 쓰러진다");
    WakeDeco(301);
    PutWinnerFlag(LocationX(299), LocationY(299), NULLPTR);
    FrameTimer(3, MakeCoopTeam);
}

void goLastTransmission()
{
    Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    MoveObject(OTHER, LocationX(LAST_LOCATION), LocationY(LAST_LOCATION));
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    UnitSetEnchantTime(OTHER, ENCHANT_FREEZE, 40);
    PlaySoundAround(OTHER, SOUND_BlindOff);
    UniPrint(OTHER, "최근 저장된 위치로 이동했습니다");
}

void SquareJump2()
{
    TeleportPlayerWithFx(OTHER, 832);
}

void SquareJump3()
{
    TeleportPlayerWithFx(OTHER, 833);
}

void PlateBomber()
{
    Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 20.0, 0.0);
    Effect("CYAN_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    EnchantOff(OTHER, EnchantList(ENCHANT_INVULNERABLE));
    Damage(OTHER, 0, 255, DAMAGE_TYPE_MANA_BOMB);
    PlaySoundAround(OTHER, SOUND_FistHit);
}

void DeathDimend()
{
    PlateBomber();
}

void StartTrapA20(int startDelay, int *pDest)
{
    int delay=5;
    int *front, *back, *cur;

    int pat1[]={
        239,250,260,
        240,249,254,
        241,248,258,
        242,247,257,
        243,246,255,
        244,245,259
    };
    int pat2[]={
        251,258,
        249,257,
        243,256,
        253,244,
        254,250,
        255,252
    };
    int repeat = sizeof(pat2)/2;

    NewMultipleNode(ArrayRefN(&pat1, 0), 3, delay, &front);
    back=front;
    int index = 0;

    while (++index<repeat)
    {
        NewMultipleNode(ArrayRefN(&pat1, index*3), 3, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    index=-1;
    while (++index<repeat)
    {
        NewMultipleNode(ArrayRefN(&pat2, index*2), 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    SetCooldown(back, 8);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap15On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapA20, 3, &once);
}

void ActivateLaserTrap(int *pLaser)
{
    //4second
    int tof;

    if (!tof)
    {
        Move(pLaser[0], 262);
        Move(pLaser[1], 264);
    }
    else
    {
        Move(pLaser[0], 261);
        Move(pLaser[1], 263);
    }    
    tof^=TRUE;
    SecondTimerWithArg(4, pLaser, ActivateLaserTrap);
}

void BomberTrap16On()
{
    ObjectOff(SELF);
    int once;
    if (once)
        return;

    once=TRUE;
    ClearCurrentTrap();
    int laser[]={Object("RaizerSentry1"), Object("RaizerSentry2")};

    FrameTimerWithArg(10, &laser, ActivateLaserTrap);
}

void StartTrapA21(int startDelay, int *pDest)
{
    int delay=8;
    int *front, *back, *cur;

    int pat[]={
        265,266,
        267,268,
        269,270,
        277,273,
        275,279,
        278,276,
        274,272,
        273,271,
        270,269,
        268,267,
        266,265
    };
    int repeat=sizeof(pat)/2;
    int index=0;

    NewMultipleNode(ArrayRefN(&pat, index*2), 2, delay, &front);
    back=front;
    
    while (++index < repeat)
    {
        NewMultipleNode(ArrayRefN(&pat, index*2), 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    pDest[0] = front;
    AppendNextNode(back, front);
    FrameTimerWithArg(startDelay, front,ListTraversal);
}

void StartTrapA22(int startDelay, int *pDest)
{
    int delay = 8;
    int *front, *back, *cur;

    int pat[]={
        280,17,
        281,286,
        282,287,
        283,288,
        284,289
    };
    int repeat=sizeof(pat)/2;
    int index=0;

    NewMultipleNode(ArrayRefN(&pat, index*2), 2, delay, &front);
    back=front;

    while (++index<repeat)
    {
        NewMultipleNode(ArrayRefN(&pat, index*2),2,delay,&cur);
        AppendNextNode(back,cur);
        back=cur;
    }
    pDest[0]=front;
    AppendNextNode(back, front);
    SetCooldown(back, 13);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapA23(int startDelay, int *pDest)
{
    int delay = 9;
    int *front, *back, *cur;

    int pat1[] = {290,291,292};
    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;
    int pat2[]={298,297,296,295};
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    int pat3[] ={292,293,294,295};
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewSingleNode(298, delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    int pat4[]={291,296,
        293,294,
        291,296};
    NewMultipleNode(ArrayRefN(&pat4, 0), 2, delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(ArrayRefN(&pat4, 2), 2, delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(ArrayRefN(&pat4, 4), 2, delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewSingleNode(298, delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    int pat5[]={290,297};
    NewMultipleNode(&pat5, sizeof(pat5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    int pat6[]={295,292};
    NewMultipleNode(&pat6, sizeof(pat6), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    SetCooldown(back, 17);
    pDest[0]=front;
    AppendNextNode(back, front);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void MacroAppendSetMultiple(int *pArr, int length, int delay, int *pCur, int *pBack)
{
    NewMultipleNode(pArr, length, delay, pCur);
    AppendNextNode(pBack[0], pCur[0]);
    pBack[0]=pCur[0];
}

void MacroAppendSetSingle(int location, int delay, int *pCur,int *pBack)
{
    NewSingleNode(location, delay, pCur);
    AppendNextNode(pBack[0], pCur[0]);
    pBack[0]=pCur[0];
}

void StartTrapArea30(int startDelay, int *pDest)
{
    int delay=9;
    int *front, *back, *cur;
    int pat[]={
        310,285,
        309,302,
        308,303,
        307,304,
        306,305,
        305,306,
        304,307,
        303,308,
        302,309,
        285,310
    };
    int repeat=sizeof(pat)/2;
    int index=0;

    NewMultipleNode(ArrayRefN(&pat, 0), 2, delay, &front);
    back=front;
    
    while (++index<repeat)
        MacroAppendSetMultiple(ArrayRefN(&pat, index*2), 2, delay, &cur, &back);
    pDest[0]=front;
    AppendNextNode(back, front);
    SetCooldown(back, 12);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap17On()
{
    int once;
    TrapExecutor(StartTrapA21, 5, &once);
}

void BomberTrap18On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapA22, 5, &once);
}

void CommonStartLocationHandler(int invokableId, int *pCheck)
{
    ObjectOff(SELF);
    if (pCheck[0])
        return;

    pCheck[0]=TRUE;
    ClearCurrentTrap();
    //Todo.
}

void BomberTrap19On()
{
    int once;
    TrapExecutor(StartTrapA23, 3, &once);
}

void BomberTrap20On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapArea30, 3, &once);
}

void SquareJump4()
{
    TeleportPlayerWithFx(OTHER, 836);
}

void ToggleFonTrap(int unit)
{
    if (IsObjectOn(unit))
    {
        CastSpellObjectObject("SPELL_FORCE_OF_NATURE", unit, unit+1);
        SecondTimerWithArg(4, unit, ToggleFonTrap);
    }
}

void StartFonTrap()
{
    int unit;

    if (unit)
        return;

    unit = CreateObjectAt("InvisibleLightBlueLow", LocationX(312), LocationY(312));
    CreateObjectAtEx("InvisibleLightBlueLow", GetObjectX(unit) - 46.0, GetObjectY(unit) + 46.0, NULLPTR);
    FrameTimerWithArg(3, unit, ToggleFonTrap);

    m_fonTrap=unit;
}

void ToggleLever()
{
    ObjectOff(SELF);

    WallOpen(Wall(81, 181));
}

void BomberTrap21On()
{
    ObjectOff(SELF);
    int once;
    if (once)
        return;
    once=TRUE;
    ClearCurrentTrap();
    StartFonTrap();
}

void StartTrapArea31(int startDelay, int *pDest)
{
    int delay=3;
    int *front, *back, *cur;
    int pat[]={
        334,340,
        333,341,
        332,342,
        331,343,
        330,344,
        329,345,
        328,334,
        327,335,
        326,336,
        325,337,
        324,338,
        323,339
    };

    NewMultipleNode(ArrayRefN(&pat, 0), 2, delay, &front);
    back=front;
    int rep=1, max=sizeof(pat)/2;

    for ( TRUE; rep<max ; Nop(++rep))
    {
        NewMultipleNode(ArrayRefN(&pat, rep*2), 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    SetCooldown(back, 7);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap22On()
{
    int once;
    TrapExecutor(StartTrapArea31, 3, &once);
}

void StartTrapArea32(int startDelay, int *pDest)
{
    int delay=4;
    int *front, *back, *cur;
    int pat[]={
        357,358,359,360,361,362,363,364,365,366,367,368,
        347,348,349,350,351,352,353,354,355,356
        };

    NewSingleNode(pat[0], delay, &front);
    back=front;

    int rep=0, max=sizeof(pat);
    while (++rep<max)
    {
        NewSingleNode(pat[rep], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    pDest[0] = front;
    AppendNextNode(back, front);
    SetCooldown(back, 7);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap23On()
{
    int once;
    TrapExecutor(StartTrapArea32, 3, &once);
}

void StartTrapArea33(int startDelay, int *pDest)
{
    int delay = 7;
    int *front, *back, *cur;
    int pat[] = {311,313,314,315,316,317,318,319,320,321};

    NewSingleNode(pat[0], delay, &front);
    back=front;
    int rep=0, max=sizeof(pat);
    while (++rep<max)
    {
        NewSingleNode(pat[rep], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    pDest[0] = front;
    AppendNextNode(back, front);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap24On()
{
    int once;
    TrapExecutor(StartTrapArea33, 3, &once);
}

void StartTrapArea34(int startDelay, int *pDest)
{
    int delay = 3;
    int *front, *back, *cur;
    int pat[]={322,346,369,370,371,372,373,374,375,376,377,370,371,372,373,374,378,379,380,382};

    NewSingleNode(pat[0], delay, &front);
    back=front;
    int rep=0, max=sizeof(pat);
    while (++rep<max)
    {
        NewSingleNode(pat[rep], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    pDest[0] = front;
    AppendNextNode(back, front);
    SetCooldown(back, 4);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap25On()
{
    int once;
    TrapExecutor(StartTrapArea34, 3, &once);
}

void BomberTrap26On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapArea35, 30, &once);
}

void StartTrapArea35(int startDelay, int *pDest)
{
    int delay = 9;
    int *front, *back, *cur;
    int pat1[]={394,395,396,397,398,401,404,405,406};
    int pat2[]={407,408,411,412,413,414,416,417,418};
    int pat3[]={419,421,422,423,424,427,430,431,432};
    int pat4[]={437,438,439,440,442,443,448,446,447};

    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;

    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    pDest[0] = front;
    AppendNextNode(back, front);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void WallsClear()
{
    int count;

    ObjectOff(SELF);
    if (++count==2)
    {
        int wallcount=6;
        int rep=-1;

        while ((++rep)<wallcount)
        {
            WallUtilOpenWallAtObjectPosition(668);
            TeleportLocationVector(668, 23.0, 23.0);
        }
        UniPrintToAll("비밀의 벽이 열립니다~");
    }
}

void BomberTrap27On()
{
    ObjectOff(SELF);

    int once;
    if (once)
        return;

        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));

    once=TRUE;
    ClearCurrentTrap();
    StartElectricTrap();
}

void StartElectricTrap()
{
    m_electricTrapEnable=TRUE;

    FrameTimer(1, FrontElectricTrapLoop);
    FrameTimer(1, FrontElectricFx);
    FrameTimer(1, BackElectricTrapLoop);
    FrameTimer(1, BackElectricFx);
}

void FrontElectricTrapLoop()
{
    if (!m_electricTrapEnable)
        return;

    m_frontElectricTrapFlag^=TRUE;

    SecondTimer(5, FrontElectricTrapLoop);
}

void ElectricFxCommon(int location1, int location2)
{
    Effect("LIGHTNING", LocationX(location1), LocationY(location1), LocationX(location2), LocationY(location2));
}

void FrontLightningTrap1()
{
    if (!m_electricTrapEnable)
    {
        ObjectOff(SELF);
        return;
    }
    if (m_frontElectricTrapFlag)
        Damage(OTHER, 0, 30, DAMAGE_TYPE_ELECTRIC);
}

void FrontLightningTrap2()
{
    if (!m_electricTrapEnable)
    {
        ObjectOff(SELF);
        return;
    }
    if (!m_frontElectricTrapFlag)
        Damage(OTHER, 0, 30, DAMAGE_TYPE_ELECTRIC);
}

void FrontElectricFx()
{
    if (!m_electricTrapEnable)
        return;

    if (m_frontElectricTrapFlag)
        ElectricFxCommon(381, 384);
    else
    {
        ElectricFxCommon(386, 387);
        
    }
    FrameTimer(2, FrontElectricFx);
}

void BackElectricTrapLoop()
{
    if (!m_electricTrapEnable)
        return;

    m_backElectricTrapFlag=(m_backElectricTrapFlag+1)%3;
    SecondTimer(2, BackElectricTrapLoop);
}

void BackElectricFx()
{
    if (!m_electricTrapEnable)
        return;

    if (m_backElectricTrapFlag==0)
        ElectricFxCommon(399, 392);
    else if (m_backElectricTrapFlag==1)
        ElectricFxCommon(400, 402);
    else if (m_backElectricTrapFlag==2)
        ElectricFxCommon(403, 409);

    FrameTimer(2, BackElectricFx);
}

void BackLightningTrap1()
{
    if (!m_electricTrapEnable)
    {
        ObjectOff(SELF);
        return;
    }
    if (m_backElectricTrapFlag==0)
        Damage(OTHER, 0, 30, DAMAGE_TYPE_ELECTRIC);
}

void BackLightningTrap2()
{
    if (!m_electricTrapEnable)
    {
        ObjectOff(SELF);
        return;
    }
    if (m_backElectricTrapFlag==1)
        Damage(OTHER, 0, 30, DAMAGE_TYPE_ELECTRIC);
}

void BackLightningTrap3()
{
    if (!m_electricTrapEnable)
    {
        ObjectOff(SELF);
        return;
    }
    if (m_backElectricTrapFlag==2)
        Damage(OTHER, 0, 30, DAMAGE_TYPE_ELECTRIC);
}

void DisableSentry()
{
    ObjectOff(SELF);

    ObjectOff(Object("SouthSentryLock"));
    UniPrintToAll("덫이 해제 되었어요");
}

void StartTrapArea36(int startDelay, int *pDest)
{
    int delay = 4;
    int *front, *back, *cur;
    int pat[]={429,433,435,436,441,444,449,450,451,453,457,459,462,463,464,465,466,467,468,469};

    NewSingleNode(pat[0], delay, &front);
    back=front;

    int rep=0;

    while (++rep<sizeof(pat))
    {
        NewSingleNode(pat[rep], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    pDest[0]=front;
    AppendNextNode(back, front);
    SetCooldown(back, 6);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap28On()
{
    int once;

    if (!once)
        m_electricTrapEnable=FALSE;

    TrapExecutor(StartTrapArea36, 3, &once);
}

void BomberTrap29On()
{
    int once;

    TrapExecutor(StartTrapArea37, 3, &once);
}

void StartTrapArea41(int startDelay, int *pDest)
{
    int delay=3; //7
    int *front, *back, *cur;
    int left[]={
        787,502,503,504,505,509,510,511,513,514,515,521,526,527,528,529,530,531,532,533,534};
    int right[]={
        789,535,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555};
    int combine[2];

    combine[0]=left[0];
    combine[1]=right[0];
    NewMultipleNode(&combine, 2, delay, &front);
    back=front;
    int rep=0;
    while (++rep<sizeof(left))
    {
        combine[0]=left[rep];
        combine[1]=right[rep];
        NewMultipleNode(&combine, 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    pDest[0]=front;
    SetCooldown(back, 5);
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea40(int startDelay, int *pDest)
{
    int delay=14;
    int *front, *back, *cur;

    int left[]={415,420,425,426,428,445,452,454,455,456};
    int right[]={458,460,461,488,494,495,497,496,498,500};

    NewMultipleNode(&left, sizeof(left), delay, &front);
    NewMultipleNode(&right, sizeof(right), delay, &back);
    AppendNextNode(front, back);
    AppendNextNode(back, front);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea37(int startDelay, int *pDest)
{
    int delay=13; //18
    int *front, *back, *cur;

    int pat1[]={470,471,472,473,474,475,476,477};
    int pat2[]={479,478,473,480,481};
    int pat3[]={485,486,474,487,489};
    int pat4[] ={489,491,492,493,481};
    int pat5[]={485,484,483,482,479};

    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;

    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat5, sizeof(pat5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    SetCooldown(back, 17);
    AppendNextNode(back, front);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap30On()
{
    int once;

    TrapExecutor(StartTrapArea40, 40, &once);
}

void SquareJump5()
{
    TeleportPlayerWithFx(OTHER, 840);
}

void EndCycleArea42Condition()
{
    int varTable[]={&m_area42Condvar1, &m_area42Condvar2, &m_area42Condvar3}, count, rep=-1, *pic;

    while ((++rep)<sizeof(varTable))
    {
        pic=varTable[rep];
        pic[0]=count!=rep;
    }
    count = (count + 1) % sizeof(varTable);
}

void StartTrapArea42(int startDelay, int *pDest)
{
    int delay=2;
    int *front, *back, *cur;
    int prevPat[]={
        792,794,795,796,800,
        801,802,805,816,
        807,809,810,
        813,815,818};
    int varptr[]={  0,  0,  0,  0,  &m_area42Condvar1,
        0,  0,  0,  &m_area42Condvar2,
        0,  0,  &m_area42Condvar3,
        0,  0,  0};

    NewSingleNode(prevPat[0], delay, &front);
    back=front;

    int rep=0;

    while (++rep<sizeof(prevPat))
    {
        NewSingleNode(prevPat[rep], delay*(prevPat[rep]!=816), &cur);
        AppendNextNode(back, cur);
        if (varptr[rep])
            SetNodeCondition(cur, varptr[rep], 1);
        back=cur;
    }
    // AppendNextNode(back, front);
    SetCooldown(back, 25);

    NewSingleNode(prevPat[0], 0, &cur);
    AppendNextNode(back, cur);
    SetExecutorPass(cur, EndCycleArea42Condition);
    AppendNextNode(cur, front);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
    EndCycleArea42Condition();
}

void BomberTrap31On()
{
    int once;

    TrapExecutor(StartTrapArea41, 3, &once);
}

void BomberTrap32On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapArea42, 3, &once);
}

void StartTrapArea43(int startDelay, int *pDest)
{
    int delay=7;
    int *front, *back, *cur;
    int pat[]={
        434,499,506,
        512,518,520,
        557,558,561,
        563,565,566,
        574,573,571,
        566,570,571,
        563,562,561,
        520,524,557,
        512,508,506
    };

    int index=0;
    NewMultipleNode(ArrayRefN(&pat, index*3), 3, delay, &front);
    back=front;

    while (++index<sizeof(pat)/3)
    {
        NewMultipleNode(ArrayRefN(&pat, index*3), 3, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back,front);
    SetCooldown(back, 16);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap32PointOn()
{
    int once;

    TrapExecutor(StartTrapArea43, 1, &once);
}

void BomberTrap33On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapArea44, 3, &once);
}

void StartTrapArea44(int startDelay, int *pDest)
{
    int delay=7;
    int *front, *back, *cur;
    int pat1[]={490,568,603,591,604,607};
    int pat2[]={501,572,600,590};
    int pat3[]={516,575,599,589};
    int pat4[]={559,576,596,588,605,608};
    int pat5[]={560,580,594,587};
    int pat6[]={567,581,593,584};

    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat5, sizeof(pat5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat6, sizeof(pat6), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    AppendNextNode(back, front);
    SetCooldown(back, 13);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea45(int startDelay, int *pDest)
{
    int delay=4;
    int *front, *back, *cur;
    int pat1[]={ 609,611,613,615,616,621,622};
    int pat2[]={611,627,637,647,659,667};
    int pat3[]={627,629,630,633,635,636};
    int pat4[]={613,629,638,648,660,671};
    int pat5[]={637,638,641,643,645,646};
    int pat6[]={615,630,641,650,661,672};
    int pat7[]={647,648,650,656,657,658};
    int pat8[]={616,633,643,656,662,675};
    int pat9[]={659,660,661,662,664,666};
    int pata[]={621,635,645,657,664,679};
    int patb[]={667,671,672,675,679,681,682};
    int patc[]={622,636,646,658,666,681};

    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat5, sizeof(pat5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat6, sizeof(pat6), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat7, sizeof(pat7), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat8, sizeof(pat8), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat9, sizeof(pat9), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pata, sizeof(pata), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&patb, sizeof(patb), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&patc, sizeof(patc), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    AppendNextNode(back, front);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap34On()
{
    int once;

    TrapExecutor(StartTrapArea45, 5, &once);
}

void SquareJump6()
{
    TeleportPlayerWithFx(OTHER, 841);
}

void BomberTrap35On()
{
    int once;

    TrapExecutor(0, 1, &once);
}

void StartTrapArea50(int startDelay, int *pDest)
{
    int delay = 3;
    int *front, *cur, *back;
    int pat[]={
        614,606,612,601,
        620,617,619,614,
        632,625,626,620,
        653,642,649,632,
        663,654,655,653
    };

    NewSingleNode(pat[0], delay, &front);
    back=front;

    int count=0;
    while (++count<sizeof(pat))
    {
        NewSingleNode(pat[count], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    SetCooldown(back, 5);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void BomberTrap36On()
{
    int once;

    TrapExecutor(StartTrapArea50, 1, &once);
}

void BomberTrap37On()
{
    int once;

    TrapExecutor(StartTrapArea51, 1, &once);
}

void BomberTrap38On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapArea52, 15, &once);
}

void BomberTrap39On()
{
    int once;

    if (!once)
    {
        Move(Object("area5block"), 714);
    }
    TrapExecutor(0, 0, &once);
}

void BomberTrap40On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapArea53, 1, &once);
}

void BomberTrap42On()
{
    int once;

    TrapExecutor(StartTrapArea54, 1, &once);
}

void BomberTrap43On()
{
    int once;
    if (!once)
        TeleportLastTransmissionPos(GetObjectX(SELF), GetObjectY(SELF));
    TrapExecutor(StartTrapArea55, 1, &once);
}

void BomberTrap45On()
{
    int once;

    TrapExecutor(StartTrapArea56, 12, &once);
}

void SquareJump8()
{
    TeleportPlayerWithFx(OTHER, 844);
}

void SquareJump9()
{
    TeleportPlayerWithFx(OTHER, 846);
}

void SquareJump10()
{
    TeleportPlayerWithFx(OTHER, 848);
}

void SquareJump11()
{
    TeleportPlayerWithFx(OTHER, 849);
}

void SquareJump12()
{
    TeleportPlayerWithFx(OTHER, 850);
}

void SquareJump7()
{
    TeleportPlayerWithFx(OTHER, 843);
}

void StartTrapArea51(int startDelay, int *pDest)
{
    int delay=4;
    int *front, *cur, *back;
    int pat[]={
        669,670,673,674,676,677,685,683,686,687,688,689,691,692,693,694,695,696,697,698,699,700,701,784
    };

    NewSingleNode(pat[0], delay, &front);
    back=front;

    int count=0;
    while (++count<sizeof(pat))
    {
        NewSingleNode(pat[count], delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea52(int startDelay, int *pDest)
{
    int delay[] = {10, 5};
    int *front, *cur, *back;
    int pat[]={631,634,639,644,665,678,680,684,690,702,704,705,709,710,799};

    NewSingleNode(pat[0], delay[0], &front);
    back=front;

    int count=0;
    while (++count<sizeof(pat))
    {
        NewSingleNode(pat[count], delay[count & 1], &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea53(int startDelay, int *pDest)
{
    int delay=7;
    int longdelay=24;
    int *front, *cur, *back;
    int pat1[]={ 628,652,706,707,708};
    int pat2[]={712,716,717,718};
    int pat3[]={721,723,724,725};
    int pat4[]={726,727,728,730};
    int pat5[]={735,736,738,739};
    int pat6[]={740,741,742,746,747,748};

    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat5, sizeof(pat5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat6, sizeof(pat6), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewSingleNode(749, longdelay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    NewMultipleNode(&pat6, sizeof(pat6), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat5, sizeof(pat5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat1, sizeof(pat1), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    int subpat[] = {750, 751};
    NewMultipleNode(&subpat, sizeof(subpat), longdelay, &cur);
    AppendNextNode(back, cur);
    back=cur;

    AppendNextNode(back, front);
    pDest[0] = front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea54(int startDelay, int *pDest)
{
    int delay = 7;
    int *front, *cur, *back;
    int pat1[] = {811,812,814,824,817,819,822};
    int pat2[] = {865,874,875,830,824,831,878,879,880};
    int pat3[] = {828,829,830,824,831,834,835};
    int pat4[] = {882,881,883,830,824,831,886,888,893};
    int pat5[] = {837,838,839,824,842,845,847};
    int pat6[] = {895,896,898,861,824,857,899,905,908};
    int pat7[] = {864,863,861,824,857,856,852};
    int pat8[] = {930,928,911,861,824,857,925,924,922};

    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;

    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat5, sizeof(pat5), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat6, sizeof(pat6), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat7, sizeof(pat7), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    NewMultipleNode(&pat8, sizeof(pat8), delay, &cur);
    AppendNextNode(back, cur);
    back=cur;
    AppendNextNode(back, front);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea55(int startDelay, int *pDest)
{
    int delay =7;
    int *front,*cur,*back;
    int pat[]={793,783,
        756,785,
        758,765,
        760,763,
        788,762,
        786,790,
        774,791,
        777,755,
        779,754
    };
    NewMultipleNode(ArrayRefN(&pat, 0), 2, delay, &front);
    back=front;
    int count=0, max=sizeof(pat)/2;
    while (++count<max)
    {
        NewMultipleNode(ArrayRefN(&pat, count*2), 2, delay, &cur);
        AppendNextNode(back, cur);
        back=cur;
    }
    AppendNextNode(back, front);
    SetCooldown(back, 15);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void StartTrapArea56(int startDelay, int *pDest)
{
    int delay = 19;
    int *front, *cur, *back;
    int pat1[]={610,771,624, 737,719, 722,743, 761,767, 797,781, 804,806};
    int pat2[]={624,651,719,729,745,743,767,770,778,781,806,808};
    int pat3[]={651,711,732,729,753,745,775,770,798,778,757,803,808};
    int pat4[]={610,771,711,732,737,722,753,775,761,797,798,804,803,757};

    NewMultipleNode(&pat1, sizeof(pat1), delay, &front);
    back=front;
    NewMultipleNode(&pat2, sizeof(pat2), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    NewMultipleNode(&pat3, sizeof(pat3), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    NewMultipleNode(&pat4, sizeof(pat4), delay, &cur);
    AppendNextNode(back,cur);
    back=cur;
    AppendNextNode(back, front);
    pDest[0]=front;
    FrameTimerWithArg(startDelay, front, ListTraversal);
}

void DrawImageAtEx(float x, float y, int thingId)
{
    int *ptr = UnitToPtr(CreateObjectAt("AirshipBasketShadow", x, y));

    ptr[1] = thingId;
}

int CheckPlayerWithId(int pUnit)
{
    int rep = -1;

    while ((++rep) < sizeof(m_player))
    {
        if (m_player[rep] ^ pUnit)
            continue;
        
        return rep;
    }
    return -1;
}

int CheckPlayer()
{
    int rep = -1;

    while ((++rep) < sizeof(m_player))
    {
        if (IsCaller(m_player[rep]))
            return rep;
    }
    return -1;
}

int PlayerClassCheckFlag(int plr, int flag)
{
    return m_pFlags[plr] & flag;
}

void PlayerClassSetFlag(int plr, int flag)
{
    m_pFlags[plr] ^= flag;
}

void PlayerClassOnJoin(int plr, int pUnit)
{
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    
    Enchant(pUnit, EnchantList(ENCHANT_ANTI_MAGIC), 0.0);
    Enchant(pUnit, EnchantList(ENCHANT_ANCHORED), 0.0);
    MoveObject(pUnit, LocationX(731), LocationY(731));
    UniPrint(pUnit, "폭탄피하기, 제작자: noxgameremaster, 릴리즈 일자. 10 december-2021");
}

void PlayerClassOnFailToJoin(int pUnit)
{
    MoveObject(pUnit, LocationX(733), LocationY(733));
    UniPrint(pUnit, "합류 실패! 다시 시도해보세요");
}

int PlayerClassOnInit(int plr, int pUnit)
{
    m_player[plr] = pUnit;
    m_pFlags[plr] = 1;

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
        {
            ClientsideProcess();
        }
        // FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);
    }

    // DiePlayerHandlerEntry(pUnit);
    // SelfDamageClassEntry(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));

    return plr;
}

void PlayerClassOnEntry(int pUnit)
{
    while (TRUE)
    {
        if (CurrentHealth(pUnit))
        {
            int plr = CheckPlayerWithId(pUnit);
            int i;

            for (i = sizeof(m_player)-1 ; i >= 0 && plr < 0 ; Nop(--i))
            {
                if (!MaxHealth(m_player[i]))
                {
                    plr = PlayerClassOnInit(i, pUnit);
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerClassOnJoin(plr, pUnit);
                break;
            }
            PlayerClassOnFailToJoin(pUnit);
        }
        break;
    }
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        if (CheckPlayer() >= 0)
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
        {
            MoveObject(OTHER, LocationX(733), LocationY(733));
        }
        
    }
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (ToInt(DistanceUnitToUnit(pUnit, m_preventRock[plr])))
        MoveObject(m_preventRock[plr], GetObjectX(pUnit), GetObjectY(pUnit));
}

void PlayerClassOnDeath(int plr, int pUnit)
{
    UniPrintToAll(PlayerIngameNick(pUnit) + " 님께서 격추 당하셨습니다");
}

void PlayerClassOnExit(int plr)
{
    m_player[plr] = 0;
    m_pFlags[plr] = 0;
}

void PlayerClassOnLoop()
{
    int u;

    for (u = 9 ; u >= 0 ; Nop(--u))
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[u]))
            {
                if (GetUnitFlags(m_player[u]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[u]))
                {
                    PlayerClassOnAlive(u, m_player[u]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(u, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(u, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(u, m_player[u]);
                    }
                    break;
                }                
            }
            if (m_pFlags[u])
                PlayerClassOnExit(u);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void OnHitEntrypad()
{
    PlayerClassOnEntry(GetCaller());
}
