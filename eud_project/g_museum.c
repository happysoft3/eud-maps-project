
#include "libs\define.h"
#include "libs\printutil.h"
#include "libs\unitstruct.h"
#include "libs\waypoint.h"
#include "libs\buff.h"

int LastUnitID = 2857;
int SewerRotTrap[10], MAGIC_GEN, EX_STATUE, WALL_X, WALL_Y[2], STOP_COUNT;
int ST_ROW[6], ARWTRP_PTR;
int NewSentry, NewPusherTrap[2];
float Movement[2], CrashMove[2];



int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196;
		arr[17] = 20; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; arr[29] = 40; 
		arr[30] = 1092616192; arr[32] = 9; arr[33] = 17;
		arr[57] = 5548288; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, LastUnitID);
        }
    }
    LastUnitID ++;

    return unitMover;
}

void InitPickets()
{
    RegistSignMessage(Object("GasNotify1"), "출입 제한구역: 유독가스 생체 실험실");
    RegistSignMessage(Object("GasNotify2"), "출입 제한구역: 유독가스 생체 실험실");
    RegistSignMessage(Object("marketSign"), "길드 박물관 입구앞 스토리웨이 편의점: 다른 곳보다 비쌀 수 있으니 주의");
}

void MapInitialize()
{
    MusicEvent();
    Movement[0] = 1.0;
    Movement[1] = -1.0;
    CrashMove[0] = 3.0;
    CrashMove[1] = -3.0;
    WALL_X = 182;
    WALL_Y[0] = 74;
    WALL_Y[1] = 86;
    EX_STATUE = Object("BreakStatue");
    Enchant(Object("LastInviso1"), "ENCHANT_INVISIBLE", 0.0);
    Enchant(Object("LastInviso2"), "ENCHANT_INVISIBLE", 0.0);
    DefLockedDoor();
    DefStreetRow();
    CounterWalls();
    TurnRow();
    Pusher(0);
    FlameTrap();
    PoisonTrap();
    RemoveTimeTable(0);
    FrameTimer(1, DelayRun);
    FrameTimerWithArg(10, 0, PassageWalls);
    FrameTimer(3, InitPickets);
}

void DelayRun()
{
    InitPackRots();
    NewPartRowTrapSetup();
    WayPusher();
    CrasherRow();
    LastPusher();
    MecaRow();
    DefMapAllArrowTraps();
    NewSentry = NewSentryTrapSetup();
}

void BreakRockGensWall()
{
    ObjectOff(self);
    Effect("JIGGLE", GetObjectX(self), GetObjectY(self), 40.0, 0.0);
    WallBreak(Wall(29, 65));
    WallBreak(Wall(30, 66));
    WallBreak(Wall(31, 67));
    WallBreak(Wall(30, 68));
    WallBreak(Wall(29, 69));
    WallBreak(Wall(28, 68));
    WallBreak(Wall(27, 67));
    WallBreak(Wall(28, 66));

    WallBreak(Wall(35, 71));
    WallBreak(Wall(36, 72));
    WallBreak(Wall(37, 73));
    WallBreak(Wall(36, 74));
    WallBreak(Wall(35, 75));
    WallBreak(Wall(34, 74));
    WallBreak(Wall(33, 73));
    WallBreak(Wall(34, 72));

    WallBreak(Wall(29, 77));
    WallBreak(Wall(30, 78));
    WallBreak(Wall(31, 79));
    WallBreak(Wall(30, 80));
    WallBreak(Wall(29, 81));
    WallBreak(Wall(28, 80));
    WallBreak(Wall(27, 79));
    WallBreak(Wall(28, 78));

    WallBreak(Wall(23, 71));
    WallBreak(Wall(24, 72));
    WallBreak(Wall(25, 73));
    WallBreak(Wall(24, 74));
    WallBreak(Wall(23, 75));
    WallBreak(Wall(22, 74));
    WallBreak(Wall(21, 73));
    WallBreak(Wall(22, 72));
}

void BreakGensWalls()
{
    int k;
    ObjectOff(self);
    if (!k)
    {
        for (k = 0 ; k < 8 ; k ++)
            WallBreak(Wall(155 + k, 119 + k));
        Effect("JIGGLE", GetObjectX(self), GetObjectY(self), 35.0, 0.0);
        FrameTimerWithArg(25, 0, ControlPackLoop);
    }
}

void Remove2FPointWayWalls()
{
    int i;

    ObjectOff(self);
    for (i = 0 ; i < 4 ; i ++)
        WallOpen(Wall(166 + i, 42 - i));
}

int NewSentryTrapSetup()
{
    int unit = CreateObject("InvisibleLightBlueLow", 1);

    Raise(CreateObject("InvisibleLightBlueLow", 1), Object("NewPartSentry2"));
    Raise(CreateObject("InvisibleLightBlueLow", 1), Object("NewPartSentry3"));
    Raise(unit, Object("NewPartSentry1"));

    return unit;
}

void NewSentryTrapSetStatus(int ptr)
{
    int stat = GetDirection(ptr + 1);

    if (stat)
    {
        ObjectOn(ToInt(GetObjectZ(ptr)));
        ObjectOn(ToInt(GetObjectZ(ptr + 1)));
        ObjectOn(ToInt(GetObjectZ(ptr + 2)));
        LookWithAngle(ptr + 1, 0);
    }
    else
    {
        ObjectOff(ToInt(GetObjectZ(ptr)));
        ObjectOff(ToInt(GetObjectZ(ptr + 1)));
        ObjectOff(ToInt(GetObjectZ(ptr + 2)));
    }
}

void NewSentryTrapReset(int ptr)
{
    LookWithAngle(ptr, 0);
}

void ShotNewPartLaiser()
{
    int ptr = NewSentry;

    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        LookWithAngle(ptr + 1, 1);
        FrameTimerWithArg(30, ptr, NewSentryTrapSetStatus);
        FrameTimerWithArg(90, ptr, NewSentryTrapSetStatus);
        FrameTimerWithArg(100, ptr, NewSentryTrapReset);
        UniPrint(other, "트랩을 건드리셨습니다");
    }
}

void NewTrapHomeSlide(int ptr)
{
    int count = GetDirection(ptr);

    if (IsObjectOn(ptr))
    {
        if (count < 23)
        {
            MoveObject(ptr, GetObjectX(ptr) + 4.0, GetObjectY(ptr) - 4.0);
            MoveObject(ptr + 1, GetObjectX(ptr + 1) + 4.0, GetObjectY(ptr + 1) - 4.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) + 4.0, GetObjectY(ptr + 2) - 4.0);
            MoveObject(ptr + 3, GetObjectX(ptr + 3) + 4.0, GetObjectY(ptr + 3) - 4.0);
            LookWithAngle(ptr, count + 1);
            FrameTimerWithArg(1, ptr, NewTrapHomeSlide);
        }
        else
        {
            LookWithAngle(ptr, 0);
            LookWithAngle(ptr - 1, 0);
        }
    }
}

void NewTrapFrontSlide(int ptr)
{
    int count = GetDirection(ptr);

    if (IsObjectOn(ptr))
    {
        if (count < 23)
        {
            MoveObject(ptr, GetObjectX(ptr) - 4.0, GetObjectY(ptr) + 4.0);
            MoveObject(ptr + 1, GetObjectX(ptr + 1) - 4.0, GetObjectY(ptr + 1) + 4.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) - 4.0, GetObjectY(ptr + 2) + 4.0);
            MoveObject(ptr + 3, GetObjectX(ptr + 3) - 4.0, GetObjectY(ptr + 3) + 4.0);
            LookWithAngle(ptr, count + 1);
            FrameTimerWithArg(1, ptr, NewTrapFrontSlide);
        }
        else
        {
            TeleportLocation(1, GetObjectX(ptr), GetObjectY(ptr));
            AudioEvent("MechGolemHitting", 1);
            AudioEvent("HammerMissing", 1);
            Effect("JIGGLE", LocationX(1), LocationY(1), 24.0, 0.0);
            LookWithAngle(ptr, 0);
            FrameTimerWithArg(22, ptr, NewTrapHomeSlide);
        }
    }
}

int NewPartRowTrap(int location)
{
    int unit = CreateObject("InvisibleLightBlueHigh", location);

    Frozen(CreateObject("SpikeBlock", location), 1);
    Frozen(CreateObject("SpikeBlock", location + 1), 1);

    TeleportLocationVector(location, 46.0, -46.0);
    TeleportLocationVector(location + 1, 46.0, -46.0);
    Frozen(CreateObject("SpikeBlock", location), 1);
    Frozen(CreateObject("SpikeBlock", location + 1), 1);

    return unit;
}

void NewPartRowTrapSetup()
{
    NewPusherTrap[0] = NewPartRowTrap(344);
    NewPusherTrap[1] = NewPartRowTrap(346);
}

void StartNewPusher1()
{
    int ptr = NewPusherTrap[0];

    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(1, ptr + 1, NewTrapFrontSlide);
    }
}

void StartNewPusher2()
{
    int ptr = NewPusherTrap[1];

    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(1, ptr + 1, NewTrapFrontSlide);
    }
}

void GenLichSummonHandler()
{
    if (GetUnitThingID(other) == 1342)
    {
        UnitZeroFleeRange(other);
        SetUnitStatus(other, GetUnitStatus(other) ^ 0x20);
        UnitLinkBinScript(other, LichLordBinTable());
        RetreatLevel(other, 0.0);
        ResumeLevel(other, 1.0);
        AggressionLevel(other, 1.0);
    }
}

void GenHorrendousSummonHandler()
{
    if (GetUnitThingID(other) == 1386)
    {
        SetUnitMaxHealth(other, CurrentHealth(other) / 4);
        SetUnitSpeed(other, 1.3);
        RetreatLevel(other, 0.0);
        ResumeLevel(other, 1.0);
        AggressionLevel(other, 1.0);
    }
}

void DefLockedDoor()
{
    LockDoor(Object("TimerLock1"));
    LockDoor(Object("TimerLock2"));
}

void InitPackRots()
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    { //10.0
        SewerRotTrap[i] = Object("PackRot" + IntToString(i + 1));
        LookWithAngle(SewerRotTrap[i], 124 + i);
    }
}

void ControlPackLoop(int arg)
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        Move(SewerRotTrap[i], GetDirection(SewerRotTrap[i]) + (arg * 10) + 200);
    }
    FrameTimerWithArg(60 + (35 * arg), (arg + 1) % 2, ControlPackLoop);
}

void StartSubRows()
{
    int tog;

    if (!tog)
    {
        tog = 1;
        UniPrint(other, "동력장치가 움직입니다");
        AudioEvent("Gear3", 86);
        AudioEvent("CreatureCageAppears", 86);
        Move(Object("MovingSub1"), 3);
        Move(Object("MovingSub2"), 4);
        Move(Object("MovingSub3"), 5);
    }
    else
    {
        ObjectToggle(Object("SubMoivng1"));
        ObjectToggle(Object("SubMoivng2"));
        ObjectToggle(Object("SubMoivng3"));
    }
}

int TurnRow()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("RotatingSpikes", 9);
        TeleportLocationVector(9, 23.0, 23.0);
        CreateObject("RotatingSpikes", 9);
        TeleportLocationVector(9, 23.0, 23.0);
        CreateObject("RotatingSpikes", 9);
        TeleportLocationVector(9, 23.0, 23.0);
        CreateObject("RotatingSpikes", 9);
        TeleportLocationVector(9, 23.0, 23.0);
        CreateObject("RotatingSpikes", 9);
    }
    return ptr;
}

void ActivateLeftTurnRows()
{
    int ptr = TurnRow();
    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(10, ptr, ControlLeftRows);
    }
}

void ControlLeftRows(int ptr)
{
    int k;
    if (GetObjectY(ptr) > 3668.0)
    {
        if (GetObjectY(ptr) > 3852.0)
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + k, GetObjectX(ptr + k) - 5.0, GetObjectY(ptr + k) - 5.0);
        }
        else
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + k, GetObjectX(ptr + k) + 4.0, GetObjectY(ptr + k) - 4.0);
        }
        FrameTimerWithArg(1, ptr, ControlLeftRows);
    }
    else
    {
        FrameTimerWithArg(10, ptr, BackLeftRows);
    }
}

void BackLeftRows(int ptr)
{
    int k;

    if (GetObjectY(ptr) < 3967.0)
    {
        if (GetObjectY(ptr) < 3852.0)
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + k, GetObjectX(ptr + k) - 4.0, GetObjectY(ptr + k) + 4.0);
        }
        else
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + k, GetObjectX(ptr + k) + 5.0, GetObjectY(ptr + k) + 5.0);
        }
        FrameTimerWithArg(1, ptr, BackLeftRows);
    }
    else
        LookWithAngle(ptr, 0);
}

void ToggleWalls()
{
    int k, tog;

    for (k = 11 ; k >= 0 ; k --)
        WallToggle(Wall(153 + k, 93 + k));
    ObjectToggle(Object("Brightness1"));
    ObjectToggle(Object("Brightness2"));
    ObjectToggle(Object("Brightness3"));
    ObjectToggle(Object("DarknessLight1"));
    ObjectToggle(Object("DarknessLight2"));
    ObjectToggle(Object("DarknessLight3"));
    if (!tog)
    {
        tog = Object("LastInviso1");
        ObjectOn(tog);
        EnchantOff(tog, "ENCHANT_INVISIBLE");
        EnchantOff(Object("LastInviso2"), "ENCHANT_INVISIBLE");
        ObjectOn(Object("LastInviso2"));
    }
}

void GuardGenInviso()
{
    if (CurrentHealth(other) > 10)
        SetUnitHealth(other, MaxHealth(other) + 100);
    Enchant(other, "ENCHANT_INVISIBLE", 0.0);
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObject("RottenMeat", 9));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void StartEastMovingGen()
{
    ObjectOff(self);
    if (CurrentHealth(MAGIC_GEN))
    {
        GeneratorPusherToEast(MAGIC_GEN);
    }
}

void GeneratorPusherToEast(int unit)
{
    if (CurrentHealth(unit))
    {
        if (GetObjectX(unit) < 3691.0)
        {
            MoveObject(unit, GetObjectX(unit) + 4.0, GetObjectY(unit) - 4.0);
            FrameTimerWithArg(1, unit, GeneratorPusherToEast);
        }
        else
        {
            Frozen(unit, 0);
            ObjectOn(unit);
        }
    }
}

void GetGeneratorPosition2()
{
    MAGIC_GEN = GetCaller();
    Frozen(other, 1);
    ObjectOff(other);
    ObjectOff(self);
}

void GetGeneratorPosition()
{
    ObjectOff(self);
    if (GetObjectY(self) > LocationY(55))
    {
        if (Random(0, 1))
            MoveObject(other, 540.0, 1690.0);
    }
    else
    {
        if (Random(0, 1))
            MoveObject(other, 816.0, 1690.0);
    }
}

void StartBreakStatueEvent()
{
    ObjectOff(self);
    StatuePusher(EX_STATUE);
}

void StatuePusher(int st)
{
    int count;
    if (GetObjectY(st) < 1713.0)
    {
        if (GetObjectY(st) < 1506.0)
            MoveObject(st, GetObjectX(st) - 4.0, GetObjectY(st) + 4.0);
        else
            MoveObject(st, GetObjectX(st) + 4.0, GetObjectY(st) + 4.0);
        FrameTimerWithArg(1, st, StatuePusher);
    }
    else if (count < 90)
    {
        count ++;
        TeleportLocation(10, GetObjectX(st), GetObjectY(st));
        AudioEvent("EarthRumbleMajor", 10);
        Effect("JIGGLE", GetObjectX(st), GetObjectY(st), 50.0, 0.0);
        Effect("DAMAGE_POOF", GetObjectX(st), GetObjectY(st), 0.0, 0.0);
        FrameTimerWithArg(2, st, StatuePusher);
    }
    else
    {
        TeleportLocation(10, GetObjectX(st), GetObjectY(st));
        DeleteObjectTimer(CreateObject("Explosion", 10), 16);
        AudioEvent("PowderBarrelExplode", 10);
        NoWallSound(1);
        WallOpen(Wall(161, 73));
        WallOpen(Wall(160, 74));
        WallOpen(Wall(159, 75));
        WallOpen(Wall(158, 76));
        WallBreak(Wall(163, 73));
        WallBreak(Wall(162, 74));
        WallBreak(Wall(161, 75));
        WallBreak(Wall(160, 76));
        WallBreak(Wall(159, 77));
        NoWallSound(0);
    }
}

void StartMagicWallCount()
{
    int start;
    ObjectOff(self);
    if (!start)
    {
        start = 1;
        AudioEvent("BearTrapTriggered", 11);
        FrameTimer(20, CloseStepByStep);
    }
}

void CloseStepByStep()
{
    int count, fast;

    if (!STOP_COUNT)
    {
        if (count < 7)
        {
            if (fast < 5)
                fast ++;
            else
            {
                WallClose(Wall(WALL_X, WALL_Y[0]));
                if (count < 6)
                    WallClose(Wall(WALL_X, WALL_Y[1]));
                WALL_X --;
                WALL_Y[0] ++;
                WALL_Y[1] --;
                fast = 0;
                count ++;
            }
            AudioEvent("Gear1", 11);
        }
        FrameTimer(20, CloseStepByStep);
    }
}

void StopWallsCountDown()
{
    ObjectOff(self);
    STOP_COUNT = 1;
    CounterWalls();
    UnlockDoor(Object("TimerLock1"));
    UnlockDoor(Object("TimerLock2"));
    WallOpen(Wall(164, 56));
    WallOpen(Wall(163, 57));
    WallOpen(Wall(162, 58));
    WallOpen(Wall(161, 59));
}

void CounterWalls()
{
    WallOpen(Wall(182, 74));
    WallOpen(Wall(181, 75));
    WallOpen(Wall(180, 76));
    WallOpen(Wall(179, 77));
    WallOpen(Wall(178, 78));
    WallOpen(Wall(177, 79));
    WallOpen(Wall(176, 80));
    WallOpen(Wall(177, 81));
    WallOpen(Wall(178, 82));
    WallOpen(Wall(179, 83));
    WallOpen(Wall(180, 84));
    WallOpen(Wall(181, 85));
    WallOpen(Wall(182, 86));
}

void OpenSecretBookcase()
{
    int ptr;
    ObjectOff(self);

    ptr = CreateObject("InvisibleLightBlueHigh", 119);
    CreateObject("InvisibleLightBlueHigh", 120);
    Raise(ptr, ToFloat(Object("LeftBook")));
    Raise(ptr + 1, ToFloat(Object("RightBook")));
    FrameTimerWithArg(1, ptr, LeftMoving);
    FrameTimerWithArg(1, ptr + 1, RightMoving);
    FrameTimer(15, DelayRemoveSecretLibraryWalls);
}

void DelayRemoveSecretLibraryWalls()
{
    WallOpen(Wall(144, 50));
}

void LeftMoving(int ptr)
{
    int unit = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (count < 38)
    {
        MoveObject(unit, GetObjectX(unit) - 1.0, GetObjectY(unit) + 1.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, LeftMoving);
    }
}

void RightMoving(int ptr)
{
    int unit = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (count < 38)
    {
        MoveObject(unit, GetObjectX(unit) + 1.0, GetObjectY(unit) - 1.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, RightMoving);
    }
}

void DefStreetRow()
{
    int k;

    for (k = 4 ; k >= 0 ; k --)
    {
        ST_ROW[k] = Object("StreetRow" + IntToString(k + 1));
        Frozen(ST_ROW[k], 1);
    }
    FrameTimer(10, StreetRowLoop);
}

void StreetRowLoop()
{
    if (CurrentHealth(ST_ROW[5]))
        FrameTimer(60, StreetRowLoop);
    else
    {
        Move(ST_ROW[0], 12);
        Move(ST_ROW[1], 36);
        Move(ST_ROW[2], 37);
        Move(ST_ROW[3], 38);
        Move(ST_ROW[4], 39);
        FrameTimer(180, StreetRowLoop);
    }
}

void GetGeneratorPosition3()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        ObjectOff(self);
        ST_ROW[5] = GetCaller();
        if (Random(0, 1))
            MoveObject(other, 3644.0, 735.0);
    }
}

int MagicGen(int id)
{
    int temp;

    if (id) temp = id;
    return temp;
}

void GetGeneratorPosition4()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        ObjectOff(self);
        MagicGen(GetCaller());
    }
}

void MovingChessGen()
{
    ObjectOff(self);
    FrameTimerWithArg(30, MagicGen(0), MovingLoop);
}

void MovingLoop(int unit)
{
    if (CurrentHealth(unit))
    {
        if (GetObjectX(unit) < 4234.0)
        {
            if (GetObjectX(unit) < 3990.0)
                MoveObject(unit, GetObjectX(unit) + 2.0, GetObjectY(unit) + 2.0);
            else
                MoveObject(unit, GetObjectX(unit) + 4.0, GetObjectY(unit) - 4.0);
            FrameTimerWithArg(1, unit, MovingLoop);
        }
        else
            MovingBackLoop(unit);
    }
}

void MovingBackLoop(int unit)
{
    if (CurrentHealth(unit))
    {
        if (GetObjectX(unit) > 3898.0)
        {
            if (GetObjectX(unit) > 3990.0)
                MoveObject(unit, GetObjectX(unit) - 4.0, GetObjectY(unit) + 4.0);
            else
                MoveObject(unit, GetObjectX(unit) - 2.0, GetObjectY(unit) - 2.0);
            FrameTimerWithArg(1, unit, MovingBackLoop);
        }
        else
            MovingLoop(unit);
    }
}

void OpenLibrarySecret()
{
    int ptr;

    ObjectOff(self);
    ptr = CreateObject("InvisibleLightBlueHigh", 139);
    CreateObject("InvisibleLightBlueHigh", 139);
    CreateObject("InvisibleLightBlueHigh", 139);
    CreateObject("InvisibleLightBlueHigh", 139); //+3
    CreateObject("InvisibleLightBlueHigh", 139);
    CreateObject("InvisibleLightBlueHigh", 139);
    Raise(ptr, ToFloat(Object("SecretLibrary1")));
    Raise(ptr + 1, 1.0);
    Raise(ptr + 2, 1.0);
    LookWithAngle(ptr + 1, 28);
    Raise(ptr + 3, ToFloat(Object("SecretLibrary2")));
    Raise(ptr + 4, -1.0);
    Raise(ptr + 5, -1.0);
    LookWithAngle(ptr + 4, 28);
    FrameTimerWithArg(1, ptr, MovingToDirection);
    FrameTimerWithArg(1, ptr + 3, MovingToDirection);
    WallOpen(Wall(48, 156));
    WallOpen(Wall(49, 157));
}

void MovingToDirection(int ptr)
{
    int unit = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr), max = GetDirection(ptr + 1);

    if (count < max)
    {
        MoveObject(unit, GetObjectX(unit) - GetObjectZ(ptr + 1), GetObjectY(unit) - GetObjectZ(ptr + 2));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, MovingToDirection);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

int Pusher(int num)
{
    int arr[2];

    if (!arr[0])
    {
        arr[0] = Object("FirstBlock");
        arr[1] = Object("SecondBlock");
    }
    return arr[num];
}

void StartPusherRow()
{
    if (!GetDirection(Pusher(0)))
    {
        Move(Pusher(0), Waypoint("FirstGo"));
        LookWithAngle(Pusher(0), 1);
        FrameTimer(1, ControlPusherRow);
    }
}

void ControlPusherRow()
{
    if (GetObjectY(Pusher(0)) > 2657.0)
    {
        Move(Pusher(1), Waypoint("SecondGo"));
        FrameTimer(1, WaitForBack);
    }
    else
        FrameTimer(1, ControlPusherRow);
}

void WaitForBack()
{
    if (GetObjectY(Pusher(0)) > 2835.0)
        FrameTimer(60, BackControl);
    else
        FrameTimer(1, WaitForBack);
}

void BackControl()
{
    int k;

    if (!k)
    {
        Move(Pusher(0), Waypoint("FirstBack"));
        FrameTimer(25, BackControl);
    }
    else
    {
        Move(Pusher(1), Waypoint("SecondBack"));
        FrameTimerWithArg(1, Pusher(0), ControllerReset);
    }
    k = (k + 1) % 2;
}

void ControllerReset(int unit)
{
    int k;
    if (GetObjectX(unit) <= 2575.0)
    {
        if (!k) k = FrameTimerWithArg(150, unit, ControllerReset);
        else
        {
            LookWithAngle(unit, 0);
            k = 0;
        }
    }
    else
        FrameTimerWithArg(1, unit, ControllerReset);
}

int DarknessGen(int id)
{
    int arr[2], cur;

    if (id > 0xff)
    {
        arr[cur] = id;
        cur ++;
        return 0;
    }
    return arr[id];
}

void GetGeneratorPosition5()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        Enchant(other, "ENCHANT_INVISIBLE", 0.0);
        ObjectOff(other);
        ObjectOff(self);
        DarknessGen(GetCaller());
        if (Random(0, 1))
            MoveObject(other, 908.0, 2081.0);
    }
}

void GetGeneratorPosition6()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        Enchant(other, "ENCHANT_INVISIBLE", 0.0);
        ObjectOff(other);
        ObjectOff(self);
        DarknessGen(GetCaller());
        if (Random(0, 1))
            MoveObject(other, 1253.0, 2426.0);
    }
}

void HurtDarknessGen()
{
    if (!IsObjectOn(self))
        ObjectOn(self);
}

void TurnOnLight()
{
    ObjectOff(self);
    if (!IsObjectOn(DarknessGen(0)))
    {
        EnchantOff(DarknessGen(0), "ENCHANT_INVISIBLE");
        ObjectOn(DarknessGen(0));
    }
    if (!IsObjectOn(DarknessGen(1)))
    {
        EnchantOff(DarknessGen(1), "ENCHANT_INVISIBLE");
        ObjectOn(DarknessGen(1));
    }
    ObjectOff(Object("DarknessPoint1"));
    ObjectOff(Object("DarknessPoint2"));
    ObjectOff(Object("DarknessPoint3"));
    ObjectOff(Object("DarknessPoint4"));
    ObjectOff(Object("DarknessPoint5"));
    ObjectOff(Object("DarknessPoint6"));
    ObjectOn(Object("SouthTorch"));
    ObjectOn(Object("NorthTorch"));
    ObjectOn(Object("EastTorch"));
    ObjectOn(Object("LightPoint3"));
    ObjectOn(Object("LightPoint2"));
    ObjectOn(Object("LightPoint1"));
}

void EnableSemiFireTrap()
{
    ObjectOn(Object("SemiFireTrap"));
}

void DisableSemiFireTrap()
{
    ObjectOff(Object("SemiFireTrap"));
}

void HurtLightning()
{
    int st;
    if (CurrentHealth(other))
    {
        TeleportLocation(22, GetObjectX(other), GetObjectY(other));
        AudioEvent("LightningBolt", 22);
        AudioEvent("PlasmaSustain", 22);
        Effect("RICOCHET", LocationX(22), LocationY(22), 0.0, 0.0);
        st = Object("Lightning" + IntToString(Random(1, 4)));
        Damage(other, st, 15, 9);
        Effect("LIGHTNING", GetObjectX(st), GetObjectY(st), GetObjectX(other), GetObjectY(other));
    }
}

int FlameTrap()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueLow", 24);
        CreateObject("InvisibleLightBlueLow", 24);
        CreateObject("InvisibleLightBlueLow", 25);
        CreateObject("InvisibleLightBlueLow", 26);
        CreateObject("InvisibleLightBlueLow", 27);
        CreateObject("InvisibleLightBlueLow", 24);
        CreateObject("InvisibleLightBlueLow", 25);
        CreateObject("InvisibleLightBlueLow", 26);
        CreateObject("InvisibleLightBlueLow", 27);
        LookWithAngle(ptr + 1, 24);
        LookWithAngle(ptr + 2, 25);
        LookWithAngle(ptr + 3, 26);
        LookWithAngle(ptr + 4, 27);
    }
    return ptr;
}

void ActivateFlameWay()
{
    int ptr = FlameTrap();
    if (!GetDirection(ptr))
    {
        RiseFlameAt(ptr + 1);
        RiseFlameAt(ptr + 2);
        RiseFlameAt(ptr + 3);
        RiseFlameAt(ptr + 4);
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(18 * 4, ptr, ResetFlameTrap);
    }
}

void ResetFlameTrap(int ptr)
{
    LookWithAngle(ptr, 0);
}

void RiseFlameAt(int ptr)
{
    int wp = GetDirection(ptr), k, unit, count = GetDirection(ptr + 4);

    if (count < 3)
    {
        TeleportLocation(wp, GetObjectX(ptr), GetObjectY(ptr));
        unit = CreateObject("InvisibleLightBlueLow", wp);
        for (k = 4 ; k >= 0 ; k --)
        {
            CreateObject(FlameType(count), wp);
            TeleportLocationVector(wp, -23.0, -23.0);
        }
        AudioEvent("FireballCast", wp);
        LookWithAngle(ptr + 4, count + 1);
        FrameTimerWithArg(RemoveTimeTable(count), unit, RemoveFlames);
        FrameTimerWithArg(9, ptr, RiseFlameAt);
    }
    else
    {
        LookWithAngle(ptr + 4, 0);
    }
}

void RemoveFlames(int ptr)
{
    int k;
    for (k = 5 ; k >= 0 ; k --)
        Delete(ptr + k);
}

int RemoveTimeTable(int num)
{
    int time[3];

    if (!time[0])
    {
        time[0] = 54; time[1] = 36; time[2] = 18;
    }
    return time[num];
}

string FlameType(int num)
{
    string table[] = {
        "SmallFlame", "MediumFlame", "Flame"
    };
    return table[num];
}

void PassageWalls(int stat)
{
    int k;
    if (!stat)  //open
    {
        for (k = 5 ; k >= 0 ; k --)
        {
            WallOpen(Wall(103 + k, 153 + k));
            WallOpen(Wall(95 + k, 161 + k));
        }
    }
    else        //close
    {
        for (k = 5 ; k >= 0 ; k --)
        {
            WallClose(Wall(103 + k, 153 + k));
            WallClose(Wall(95 + k, 161 + k));
        }
    }
}

int PoisonTrap()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 28);
        CreateObject("InvisibleLightBlueHigh", 181);
    }
    return ptr;
}

void RemoveWallsByHand()
{
    int ptr = PoisonTrap(), time = ToInt(GetObjectZ(ptr));
    if (IsObjectOn(time))
    {
        UniPrint(other, "가스실 함정이 해제되었습니다");
        Delete(time);
        PassageWalls(0);
        LookWithAngle(ptr, 0);
    }
}

void CloseWayGate()
{
    int ptr = PoisonTrap(), time;

    if (!GetDirection(ptr))
    {
        PassageWalls(1);
        time = CreateObject("InvisibleLightBlueHigh", 181);
        Raise(ptr, ToFloat(time));
        Raise(time, ToFloat(ptr));
        FrameTimerWithArg(30, time, Countdown);
        LookWithAngle(ptr, 1);
    }
}

void Countdown(int time)
{
    int count = GetDirection(time);

    if (IsObjectOn(time))
    {
        if (count < 15)
        {
            AudioEvent("GenerateTick", 28);
            LookWithAngle(time, count + 1);
            FrameTimerWithArg(30, time, Countdown);
        }
        else
        {
            SpreadGasOnRoom();
            FrameTimerWithArg(60, ToInt(GetObjectZ(time)), DisableGasTrap);
            Delete(time);
        }
    }
}

void SpreadGasOnRoom()
{
    string name = "Polyp";
    int k;

    for (k = 268 ; k <= 288 ; k ++)
        Damage(CreateObject(name, k), 0, 100, 0);
    Damage(CreateObject(name, 300), 0, 100, 0);
    Damage(CreateObject(name, 301), 0, 100, 0);
    Damage(CreateObject(name, 302), 0, 100, 0);
}

void DisableGasTrap(int ptr)
{
    PassageWalls(0);
    LookWithAngle(ptr, 0);
}

int WayPusher()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("IronBlock", 304);
        CreateObject("IronBlock", 303);
        CreateObject("StoneBlock", 305);
        CreateMoverFix(ptr, 0, 30.0);
        CreateMoverFix(ptr + 1, 0, 30.0);
    }
    return ptr;
}

void GoWayBlocks()
{
    ObjectOff(self);
    AudioEvent("SpikeBlockMove", 304);
    FrameTimerWithArg(10, WayPusher(), ControlTwoWays);
}

void ControlTwoWays(int ptr)
{
    if (GetObjectY(ptr + 1) < 4246.0)
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 4.0, GetObjectY(ptr + 1) + 4.0);
    if (GetObjectY(ptr) < 4312.0)
    {
        if (GetObjectY(ptr) < 4264.0)
            MoveObject(ptr, GetObjectX(ptr) + 4.0, GetObjectY(ptr) + 4.0);
        else
            MoveObject(ptr, GetObjectX(ptr) - 4.0, GetObjectY(ptr) + 4.0);
        FrameTimerWithArg(1, ptr, ControlTwoWays);
    }
    else
    {
        Move(ptr, 307);
        Move(ptr + 1, 306);
    }
}

void OpenSecretElevatorWalls()
{
    int k;
    ObjectOff(self);
    for (k = 3 ; k >= 0 ; k --)
        WallOpen(Wall(105 - k, 199 + k));
}

void ActivateMonoBlockDrift()
{
    int ptr = WayPusher() + 2;

    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(6, ptr, ControlMonoBlock);
    }
}

void ControlMonoBlock(int ptr)
{
    float pos_x = GetObjectX(ptr);
    if (pos_x < 2128.0)
    {
        if (pos_x < 2058.0)
        {
            if (pos_x < 1864.0)
                MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) - 2.0);
            else
                MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) + 2.0);
        }
        else
            MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) - 2.0);
        FrameTimerWithArg(1, ptr, ControlMonoBlock);
    }
    else
        FrameTimerWithArg(60, ptr, MonoBlockComebackHome);
}

void MonoBlockComebackHome(int ptr)
{
    float pos_x = GetObjectX(ptr);

    if (pos_x > 1828.0)
    {
        if (pos_x > 1864.0)
        {
            if (pos_x > 2058.0)
                MoveObject(ptr, GetObjectX(ptr) - 2.0, GetObjectY(ptr) + 2.0);
            else
                MoveObject(ptr, GetObjectX(ptr) - 2.0, GetObjectY(ptr) - 2.0);
        }
        else
            MoveObject(ptr, GetObjectX(ptr) - 2.0, GetObjectY(ptr) + 2.0);
        FrameTimerWithArg(1, ptr, MonoBlockComebackHome);
    }
    else
        LookWithAngle(ptr, 0);
}

void SurpriseGenWalls()
{
    int k;
    ObjectOff(self);
    if (!k)
    {
        Effect("JIGGLE", GetObjectX(self), GetObjectY(self), 42.0, 0.0);
        NoWallSound(1);
        for (k = 0 ; k < 8 ; k ++)
        {
            WallOpen(Wall(69 - k, 207 + k));
            WallOpen(Wall(74 - k, 212 + k));
            WallBreak(Wall(68 - k, 206 + k));
            WallBreak(Wall(75 - k, 213 + k));
        }
        NoWallSound(0);
    }
}

int BounceRow()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("SpikeBlock", 308);
        TeleportLocationVector(308, 46.0, 46.0);
        CreateObject("SpikeBlock", 308);
        TeleportLocationVector(308, 46.0, 46.0);
        CreateObject("SpikeBlock", 308);
        CreateObject("SpikeBlock", 309);
        TeleportLocationVector(309, 46.0, 46.0);
        CreateObject("SpikeBlock", 309);
        TeleportLocationVector(309, 46.0, 46.0);
        CreateObject("SpikeBlock", 309);
    }
    return ptr;
}

void ActivateBounceBlocks()
{
    int ptr = BounceRow();
    
    ObjectOff(self);
    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(30, ptr, ControlBouncy);
    }
}

void ControlBouncy(int ptr)
{
    int count = GetDirection(ptr + 2), mod = GetDirection(ptr + 1);

    if (count < 57)
    {
        MoveObject(ptr, GetObjectX(ptr) - Movement[mod], GetObjectY(ptr) + Movement[mod]);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - Movement[mod], GetObjectY(ptr + 1) + Movement[mod]);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) - Movement[mod], GetObjectY(ptr + 2) + Movement[mod]);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) + Movement[mod], GetObjectY(ptr + 3) - Movement[mod]);
        MoveObject(ptr + 4, GetObjectX(ptr + 4) + Movement[mod], GetObjectY(ptr + 4) - Movement[mod]);
        MoveObject(ptr + 5, GetObjectX(ptr + 5) + Movement[mod], GetObjectY(ptr + 5) - Movement[mod]);
        LookWithAngle(ptr + 2, count + 1);
        FrameTimerWithArg(1, ptr, ControlBouncy);
    }
    else
    {
        if (!mod)
        {
            AudioEvent("HammerMissing", 29);
            Effect("JIGGLE", LocationX(29), LocationY(29), 50.0, 0.0);
            FrameTimerWithArg(50, ptr, ControlBouncy);
        }
        else
            FrameTimerWithArg(1, ptr, ControlBouncy);
        mod = (mod + 1) % 2;
        LookWithAngle(ptr + 1, mod);
        LookWithAngle(ptr + 2, 0);
    }
}

void Part2ElevatorOn()
{
    int gear = Object("AssistGear");
    ObjectOff(self);
    UniPrint(other, "엘리베이터가 작동합니다");
    ObjectOn(Object("Part2StopElev"));
    ObjectOn(gear);
    TeleportLocation(22, GetObjectX(gear), GetObjectY(gear));
    DeleteObjectTimer(CreateObject("BigSmoke", 22), 10);
    AudioEvent("CreatureCageAppears", 207);
    AudioEvent("MechGolemDie", 207);
}

void StartSpinGenerators()
{
    int k;
    ObjectOff(self);
    if (!k)
    {
        Move(CastleGen(0, 0), 311);
        Move(CastleGen(0, 1), 315);
        k = 1;
    }
}

int CastleGen(int id, int idx)
{
    int gen[2];

    if (id)
    {
        gen[idx] = id;
        return 0;
    }
    return gen[idx];
}

void GetGeneratorPositionA()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        ObjectOff(self);
        if (GetObjectX(self) < 4396.0)
            CastleGen(GetCaller(), 0);
        else
            CastleGen(GetCaller(), 1);
        CreateMoverFix(other, 0, 21.0);
    }
}

void PushToNorth()
{
    if (!UnitCheckEnchant(other, GetLShift(10)) && IsObjectOn(other))
    {
        PushObjectTo(other, -30.0, -30.0);
        Effect("DAMAGE_POOF", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
    }
}

void FFHitSouth()
{
    int unit;
    if (CurrentHealth(other) && !UnitCheckEnchant(other, GetLShift(7)) && IsObjectOn(other))
    {
        Enchant(other, EnchantList(10), 0.15);
        Enchant(other, EnchantList(7), 0.1);
        TeleportLocation(32, GetObjectX(other) - 10.0, GetObjectY(other) - 10.0);
        unit = CreateObject("Maiden", 32);
        ObjectOff(unit);
        Frozen(unit, 1);
        DeleteObjectTimer(unit, 1);
    }
}

void FFHitWest()
{
    int unit;
    if (CurrentHealth(other) && !UnitCheckEnchant(other, GetLShift(7)) && IsObjectOn(other))
    {
        Enchant(other, EnchantList(10), 0.15);
        Enchant(other, EnchantList(7), 0.1);
        TeleportLocation(32, GetObjectX(other) + 10.0, GetObjectY(other) - 10.0);
        unit = CreateObject("Maiden", 32);
        ObjectOff(unit);
        Frozen(unit, 1);
        DeleteObjectTimer(unit, 1);
    }
}

void FFHitEast()
{
    int unit;
    if (CurrentHealth(other) && !UnitCheckEnchant(other, GetLShift(7)) && IsObjectOn(other))
    {
        Enchant(other, EnchantList(10), 0.15);
        Enchant(other, EnchantList(7), 0.1);
        TeleportLocation(32, GetObjectX(other) - 10.0, GetObjectY(other) + 10.0);
        unit = CreateObject("Maiden", 32);
        ObjectOff(unit);
        Frozen(unit, 1);
        DeleteObjectTimer(unit, 1);
    }
}

int CrasherRow()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 31) + 1;
        Delete(ptr - 1);
        for (k = 7 ; k >= 0 ; k --)
        {
            Frozen(CreateObject("SpikeBlock", 318), 1);
            Frozen(CreateObject("SpikeBlock", 319), 1);
            TeleportLocationVector(318, -46.0, 46.0);
            TeleportLocationVector(319, -46.0, 46.0);
        }
    }
    return ptr;
}

void ActivateFirstCrashRow()
{
    int ptr = CrasherRow();

    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(1, ptr, CrasherSection1);
    }
}

void CrasherSection1(int ptr)
{
    int count = GetDirection(ptr + 1), mod = GetDirection(ptr + 2);

    if (count < 23) //movement: 3
    {
        MoveObject(ptr, GetObjectX(ptr) + CrashMove[mod], GetObjectY(ptr) + CrashMove[mod]);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - CrashMove[mod], GetObjectY(ptr + 1) - CrashMove[mod]);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + CrashMove[mod], GetObjectY(ptr + 2) + CrashMove[mod]);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) - CrashMove[mod], GetObjectY(ptr + 3) - CrashMove[mod]);
        LookWithAngle(ptr + 1, count + 1);
        FrameTimerWithArg(1, ptr, CrasherSection1);
    }
    else
    {
        if (!mod)
        {
            AudioEvent("HammerMissing", 30);
            Effect("DAMAGE_POOF", LocationX(30), LocationY(30), 50.0, 0.0);
            Effect("JIGGLE", GetObjectX(ptr), GetObjectY(ptr), 50.0, 0.0);
            FrameTimerWithArg(30, ptr, CrasherSection1);
        }
        else
            LookWithAngle(ptr, 0);
        mod = (mod + 1) % 2;
        LookWithAngle(ptr + 2, mod);
        LookWithAngle(ptr + 1, 0);
    }
}

void ActivateSecondCrashRow()
{
    int ptr = CrasherRow() + 4;
    
    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(1, ptr, GenericCrasher);
    }
}

void ActivateThirdCrashRow()
{
    int ptr = CrasherRow() + 10;

    if (!GetDirection(ptr))
    {
        LookWithAngle(ptr, 1);
        FrameTimerWithArg(1, ptr, GenericCrasher);
    }
}

void GenericCrasher(int ptr)
{
    int count = GetDirection(ptr + 1), mod = GetDirection(ptr + 2);

    if (count < 23)
    {
        MoveObject(ptr, GetObjectX(ptr) + CrashMove[mod], GetObjectY(ptr) + CrashMove[mod]);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - CrashMove[mod], GetObjectY(ptr + 1) - CrashMove[mod]);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + CrashMove[mod], GetObjectY(ptr + 2) + CrashMove[mod]);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) - CrashMove[mod], GetObjectY(ptr + 3) - CrashMove[mod]);
        MoveObject(ptr + 4, GetObjectX(ptr + 4) + CrashMove[mod], GetObjectY(ptr + 4) + CrashMove[mod]);
        MoveObject(ptr + 5, GetObjectX(ptr + 5) - CrashMove[mod], GetObjectY(ptr + 5) - CrashMove[mod]);
        LookWithAngle(ptr + 1, count + 1);
        FrameTimerWithArg(1, ptr, GenericCrasher);
    }
    else
    {
        if (!mod)
        {
            TeleportLocation(31, GetObjectX(ptr + 2), GetObjectY(ptr + 2));
            AudioEvent("HammerMissing", 31);
            Effect("DAMAGE_POOF", LocationX(31), LocationY(31), 50.0, 0.0);
            Effect("JIGGLE", LocationX(31), LocationY(31), 50.0, 0.0);
            FrameTimerWithArg(30, ptr, GenericCrasher);
        }
        else
            LookWithAngle(ptr, 0);
        mod = (mod + 1) % 2;
        LookWithAngle(ptr + 2, mod);
        LookWithAngle(ptr + 1, 0);
    }
}

int LastPusher()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 116);
        CreateObject("InvisibleLightBlueHigh", 289);
        CreateObject("InvisibleLightBlueHigh", 290);
        Raise(ptr, ToFloat(Object("LastPusher1")));
        Raise(ptr + 1, ToFloat(Object("LastPusher2")));
        Raise(ptr + 2, ToFloat(Object("LastPusher3")));
    }
    return ptr;
}

void ActivateLastPusherRow()
{
    int ptr = LastPusher();

    if (!GetDirection(ptr))
    {
        Move(ToInt(GetObjectZ(ptr)), 116);
        Move(ToInt(GetObjectZ(ptr + 1)), 289);
        Move(ToInt(GetObjectZ(ptr + 2)), 290);
        LookWithAngle(ptr, 1);
    }
}

void BackLastPusher()
{
    int ptr = LastPusher(), count;

    if (IsCaller(ToInt(GetObjectZ(ptr))))
    {
        count ++;
        if (count == 2)
        {
            count = 0;
            LookWithAngle(ptr, 0);
            ObjectOff(self);
        }
        else
        {
            ObjectOn(Object("ResetPusherTrg"));
            FrameTimerWithArg(60, ptr, BackToTheHome);
        }
    }
}

void BackToTheHome(int ptr)
{
    Move(ToInt(GetObjectZ(ptr)), 291);
    Move(ToInt(GetObjectZ(ptr + 1)), 292);
    Move(ToInt(GetObjectZ(ptr + 2)), 293);
}

int MecaRow()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 320) + 1;
        Delete(ptr - 1);
        for (k = 2 ; k >= 0 ; k --)
        {
            Frozen(CreateObject("IronBlock", 320), 1);
            Frozen(CreateObject("IronBlock", 321), 1);
            Frozen(CreateObject("IronBlock", 322), 1);
            Frozen(CreateObject("IronBlock", 323), 1);
            TeleportLocationVector(320, 46.0, 46.0);
            TeleportLocationVector(323, 46.0, 46.0);
            TeleportLocationVector(321, -46.0, -46.0);
            TeleportLocationVector(322, -46.0, -46.0);
        }
    }
    return ptr;
}

void RollMechanicalBlocks()
{
    int ptr = MecaRow();

    UniPrint(other, "동력장치가 움직입니다");
    ObjectOff(self);
    TeleportLocation(22, GetObjectX(other), GetObjectY(other));
    AudioEvent("BoulderMove", 22);
    FrameTimerWithArg(1, ptr, MecaBlocksWalkToLeft);
    FrameTimerWithArg(1, ptr + 1, MecaBlocksWalkToRight);
    FrameTimerWithArg(1, ptr + 2, MecaBlocksWalkToRight);
    FrameTimerWithArg(1, ptr + 3, MecaBlocksWalkToLeft);
}

void MecaBlocksWalkToLeft(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 130)
    {
        MoveObject(ptr, GetObjectX(ptr) - 1.0, GetObjectY(ptr) - 1.0);
        MoveObject(ptr + 4, GetObjectX(ptr + 4) - 1.0, GetObjectY(ptr + 4) - 1.0);
        MoveObject(ptr + 8, GetObjectX(ptr + 8) - 1.0, GetObjectY(ptr + 8) - 1.0);
        Effect("JIGGLE", GetObjectX(ptr + 4), GetObjectY(ptr + 4), 38.0, 0.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, MecaBlocksWalkToLeft);
    }
}

void MecaBlocksWalkToRight(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 130)
    {
        MoveObject(ptr, GetObjectX(ptr) + 1.0, GetObjectY(ptr) + 1.0);
        MoveObject(ptr + 4, GetObjectX(ptr + 4) + 1.0, GetObjectY(ptr + 4) + 1.0);
        MoveObject(ptr + 8, GetObjectX(ptr + 8) + 1.0, GetObjectY(ptr + 8) + 1.0);
        Effect("JIGGLE", GetObjectX(ptr + 4), GetObjectY(ptr + 4), 38.0, 0.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, MecaBlocksWalkToRight);
    }
}

int LastGen(int id, int idx)
{
    int arr[2];

    if (id)
    {
        arr[idx] = id;
        return 0;
    }
    return arr[idx];
}

void GetGeneratorPositionB()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        if (GetObjectX(self) < 5552.0)
            LastGen(GetCaller(), 0);
        else
            LastGen(GetCaller(), 1);
        ObjectOff(self);
        ObjectOff(other);
    }
}

void OpenLastSurpriseGenWalls()
{
    ObjectOn(LastGen(0, 0));
    ObjectOn(LastGen(0, 1));
    WallOpen(Wall(238, 48));
    WallOpen(Wall(243, 53));
    FrameTimerWithArg(1, LastGen(0, 0), RespectHideLeftGen);
    FrameTimerWithArg(1, LastGen(0, 1), RespectHideRightGen);
    ObjectOff(self);
}

void RespectHideLeftGen(int gen)
{
    if (CurrentHealth(gen))
    {
        if (GetObjectX(gen) < 5554.0)
        {
            if (GetObjectX(gen) < 5508.0)
                MoveObject(gen, GetObjectX(gen) + 2.0, GetObjectY(gen) + 2.0);
            else
                MoveObject(gen, GetObjectX(gen) + 1.0, GetObjectY(gen) - 1.0);
            FrameTimerWithArg(1, gen, RespectHideLeftGen);
        }
    }
}

void RespectHideRightGen(int gen)
{
    if (CurrentHealth(gen))
    {
        if (GetObjectY(gen) > 1092.0)
        {
            if (GetObjectY(gen) > 1207.0)
                MoveObject(gen, GetObjectX(gen) - 2.0, GetObjectY(gen) - 2.0);
            else
                MoveObject(gen, GetObjectX(gen) + 2.0, GetObjectY(gen) - 2.0);
            FrameTimerWithArg(1, gen, RespectHideRightGen);
        }
    }
}

void DefMapAllArrowTraps()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 33), k;

    ARWTRP_PTR = ptr;
    for (k = 48 ; k >= 0 ; k --)
    {
        CreateObject("InvisibleLightBlueHigh", 33);
        TeleportLocationVector(33, -7.0, 7.0);
    }
    FrameTimerWithArg(1, ptr, LinkUnitIdToPtr);
}

void LinkUnitIdToPtr(int ptr)
{
    Raise(ptr, ToFloat(Object("WestLine1Arrow1")));
    Raise(ptr + 1, ToFloat(Object("WestLine1Arrow2")));
    Raise(ptr + 2, ToFloat(Object("WestLine2Arrow1")));
    Raise(ptr + 3, ToFloat(Object("WestLine2Arrow2")));
    Raise(ptr + 4, ToFloat(Object("WestLine3Arrow1")));
    Raise(ptr + 5, ToFloat(Object("WestLine3Arrow2")));
    Raise(ptr + 6, ToFloat(Object("FloorTrap11")));
    Raise(ptr + 7, ToFloat(Object("FloorTrap12")));
    Raise(ptr + 8, ToFloat(Object("FloorTrap13")));
    Raise(ptr + 9, ToFloat(Object("FloorTrap21")));
    Raise(ptr + 10, ToFloat(Object("FloorTrap22")));
    Raise(ptr + 11, ToFloat(Object("FloorTrap23")));
    Raise(ptr + 12, ToFloat(Object("FloorTrap31")));
    Raise(ptr + 13, ToFloat(Object("FloorTrap32")));
    Raise(ptr + 14, ToFloat(Object("FloorTrap33")));
    Raise(ptr + 15, ToFloat(Object("FloorTrap41")));
    Raise(ptr + 16, ToFloat(Object("FloorTrap42")));
    Raise(ptr + 17, ToFloat(Object("FloorTrap43")));
    Raise(ptr + 18, ToFloat(Object("LibraryTrap11")));
    Raise(ptr + 19, ToFloat(Object("LibraryTrap12")));
    Raise(ptr + 20, ToFloat(Object("LibraryTrap13")));
    Raise(ptr + 21, ToFloat(Object("LibraryTrap21")));
    Raise(ptr + 22, ToFloat(Object("LibraryTrap22")));
    Raise(ptr + 23, ToFloat(Object("LibraryTrap23")));
    Raise(ptr + 24, ToFloat(Object("LibraryTrap24")));
    Raise(ptr + 25, ToFloat(Object("LibraryTrap25")));
    Raise(ptr + 26, ToFloat(Object("LibraryTrap26")));
    Raise(ptr + 27, ToFloat(Object("LibraryTrap27")));
    Raise(ptr + 28, ToFloat(Object("LibraryTrap31")));
    Raise(ptr + 29, ToFloat(Object("LibraryTrap32")));
    Raise(ptr + 30, ToFloat(Object("LibraryTrap33")));
    Raise(ptr + 31, ToFloat(Object("LibraryTrap41")));
    Raise(ptr + 32, ToFloat(Object("LibraryTrap42")));
    Raise(ptr + 33, ToFloat(Object("LibraryTrap43")));
    Raise(ptr + 34, ToFloat(Object("LibraryTrap51")));
    Raise(ptr + 35, ToFloat(Object("LibraryTrap52")));
    Raise(ptr + 36, ToFloat(Object("LibraryTrap53")));
    Raise(ptr + 37, ToFloat(Object("SouthShot11")));
    Raise(ptr + 38, ToFloat(Object("SouthShot12")));
    Raise(ptr + 39, ToFloat(Object("SouthShot13")));
    Raise(ptr + 40, ToFloat(Object("SouthShot21")));
    Raise(ptr + 41, ToFloat(Object("SouthShot22")));
    Raise(ptr + 42, ToFloat(Object("SouthShot23")));
    Raise(ptr + 43, ToFloat(Object("SouthShot31")));
    Raise(ptr + 44, ToFloat(Object("SouthShot32")));
    Raise(ptr + 45, ToFloat(Object("SouthShot33")));
}

void EnableWest1Rows()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 0)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 1)));
}

void DisableWest1Rows()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 0)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 1)));
}

void EnableWest2Rows()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 2)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 3)));
}

void DisableWest2Rows()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 2)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 3)));
}

void EnableWest3Rows()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 4)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 5)));
}

void DisableWest3Rows()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 4)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 5)));
}

void EnableFloor1Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 6)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 7)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 8)));
}

void DisableFloor1Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 6)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 7)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 8)));
}

void EnableFloor2Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 9)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 10)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 11)));
}

void DisableFloor2Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 9)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 10)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 11)));
}

void EnableFloor3Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 12)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 13)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 14)));
}

void DisableFloor3Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 12)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 13)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 14)));
}

void EnableFloor4Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 15)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 16)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 17)));
}

void DisableFloor4Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 15)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 16)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 17)));
}

void EnableLibrary1Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 18)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 19)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 20)));
}

void DisableLibrary1Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 18)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 19)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 20)));
}

void EnableLibrary2Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 20)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 21)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 22)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 23)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 24)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 25)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 26)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 27)));
}

void DisableLibrary2Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 20)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 21)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 22)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 23)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 24)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 25)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 26)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 27)));
}

void EnableLibrary3Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 28)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 29)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 30)));
}

void DisableLibrary3Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 28)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 29)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 30)));
}

void EnableLibrary4Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 31)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 32)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 33)));
}

void DisableLibrary4Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 31)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 32)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 33)));
}

void EnableLibrary5Row()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 34)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 35)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 36)));
}

void DisableLibrary5Row()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 34)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 35)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 36)));
}

void EnableSouthShot1()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 37)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 38)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 39)));
}

void DisableSouthShot1()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 37)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 38)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 39)));
}

void EnableSouthShot2()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 40)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 41)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 42)));
}

void DisableSouthShot2()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 40)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 41)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 42)));
}

void EnableSouthShot3()
{
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 43)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 44)));
    ObjectOn(ToInt(GetObjectZ(ARWTRP_PTR + 45)));
}

void DisableSouthShot3()
{
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 43)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 44)));
    ObjectOff(ToInt(GetObjectZ(ARWTRP_PTR + 45)));
}

void GoToTest()
{
    MoveObject(other, LocationX(35), LocationY(35));
}

void RemoveExitSecretWalls()
{
    ObjectOff(self);
    UniPrint(other, "비밀의 벽이 열립니다");
    WallOpen(Wall(186, 10));
    WallOpen(Wall(187, 11));
}

void RemoveNorthSecretSingleWall()
{
    ObjectOff(self);
    WallOpen(Wall(238, 108));
}
