
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/sound_define.h"
#include "libs/fxeffect.h"
#include "libs/buff.h"
#include "libs/spellutil.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/coopteam.h"
#include "libs/username.h"
#include "libs/mathlab.h"
#include "libs/playerupdate.h"
#include "libs/anti_charm.h"

int player[20], m_LastUnitID = 569;
int m_KillScore; //current_kills
int m_Shutdown; //승패 유무 1일때 게임종료.
int m_Victim[200]; //몬스터 유닛의 배열입니다.

string *m_unitRealNameTable;
string *m_unitDescTable;
int *m_mobHPTable;
int m_unitDescTableLength;

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[17] = 100; arr[18] = 30; arr[19] = 50; 
		arr[20] = 1045220557; arr[21] = 1061158912; arr[23] = 32; arr[24] = 1067869798; 
		arr[26] = 4; 		
		arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; 
		arr[40] = 116; arr[53] = 1073741824;
		arr[55] = 12; arr[56] = 20; 
		link = &arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743;		
		arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 15; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 1; 
		arr[35] = 2; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = &arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520;
		arr[37] = 1701996870; arr[38] = 1819042146;		
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;		
		arr[57] = 5548112; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;		
		arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 		
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = &arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link = &arr;
	}
	return link;
}

void CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        arr[5] = MonsterGoonProcess; arr[72] = MonsterStrongWhiteWizProcess; arr[30] = MonsterWeirdlingBeastProcess; arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process;
        arr[12] = MonsterFireSpriteProcess; arr[73] = MonsterWizardRedProcess; arr[29] = MonsterAirshipCaptainProcess;
    }
    if (thingID)
    {
        if (arr[key])
            CallFunctionWithArg(arr[key], unit);
    }
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        UnitZeroFleeRange(unit);
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetUnitMaxHealth(unit, 325);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        UnitZeroFleeRange(unit);
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
    }
}


int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, m_LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, m_LastUnitID);
        }
    }
    m_LastUnitID ++;

    return unitMover;
}

void ImportBinTable()
{
    BlackWidowBinTable();
    AirshipCaptainBinTable();
    FireSpriteBinTable();
    GoonBinTable();
    MaidenBinTable();
    StrongWizardWhiteBinTable();
    WeirdlingBeastBinTable();
    WizardRedBinTable();
}

void MapInitialize()
{
    MusicEvent();
    ImportBinTable();
    InitMonsterInfo();
    CheckMonsterThing(getMaster());
    MoveWaypoint(40, GetWaypointX(20), GetWaypointY(20));
    PlacedPotions();
    FrameTimerWithArg(1, Object("StartLocation"), StartLocationOn);
    FrameTimer(1, InitMapSign);
    FrameTimerWithArg(100, GetHost(), SelectRoomToHostPlayer);
    FrameTimer(2, MakeCoopTeam);
}

int PlayerClassDeathFlagCheck(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassDeathFlagSet(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

void PlayerClassOnInit(int plr, int pUnit)
{
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    player[plr] = pUnit;
    player[plr + 10] = 1;
}

void getPlayers()
{
    int i, plr;

    if (CurrentHealth(OTHER))
    {
        plr = CheckPlayer();
        for (i = 9 ; i >= 0 && plr < 0 ; i --)
        {
            if (!MaxHealth(player[i]))
            {
                PlayerClassOnInit(i, GetCaller());
                plr = i;
                break;
            }
        }
        if (plr >= 0)
            PlayerClassOnJoin(plr);
        else
            PlayerClassFailedJoin();
    }
}

void PlayerClassOnJoin(int plr)
{
    int ptr;

    if (PlayerClassDeathFlagCheck(plr))
        PlayerClassDeathFlagSet(plr);
    Enchant(OTHER, EnchantList(14), 0.0);
    MoveObject(OTHER, LocationX(5), LocationY(5));
    DeleteObjectTimer(CreateObjectAt("BlueRain", GetObjectX(other), GetObjectY(other)), 10);
    PlaySoundAround(OTHER, 1008);
}

void PlayerClassFailedJoin()
{
    MoveObject(OTHER, 3263.0, 1437.0);
    UnitSetEnchantTime(OTHER, 25, 0);
    UnitSetEnchantTime(OTHER, 29, 0);
    UniPrint(OTHER, "이 맵이 수용할 수 있는 플레이어 허용치를 초과했기 때문에 더 이상 입장을 할 수 없습니다");
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

void PlayerClassUseSkillCheck(int plr)
{
    int pUnit = player[plr];

    if (UnitCheckEnchant(pUnit, GetLShift(31)))
    {
        EnchantOff(pUnit, EnchantList(31));
        RemoveTreadLightly(pUnit);
        warriorSkillR(plr);
    }
}

void PlayerClassOnFree(int plr)
{
    // CancelPlayerDialog(player[plr]);
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PreservePlayersLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; Nop(i --))
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassUseSkillCheck(i);
                    break;
                }
                else
                {
                    if (PlayerClassDeathFlagCheck(i)) break;
                    else
                    {
                        PlayerClassDeathFlagSet(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, PreservePlayersLoop);
}

int DummyUnitCreate(string name, float locX, float locY)
{
    int unit = CreateObjectAt(name, locX, locY);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    ObjectOff(unit);
    Frozen(unit, 1);
    return unit;
}

void DemonFireWalking(int sUnit)
{
    int durate = GetDirection(sUnit), owner = GetOwner(sUnit);

    while (1)
    {
        if (CurrentHealth(owner) && durate)
        {
            if (IsVisibleTo(sUnit, sUnit + 2))
            {
                MoveObject(sUnit + 2, GetObjectX(sUnit + 2) + GetObjectZ(sUnit), GetObjectY(sUnit + 2) + GetObjectZ(sUnit + 1));
                LookWithAngle(sUnit, durate - 1);
                FrameTimerWithArg(1, sUnit, DemonFireWalking);
                PlaySoundAround(sUnit + 2, 375);
                DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(sUnit + 2), GetObjectY(sUnit + 2)), 6);
                break;
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        Delete(sUnit + 2);
        break;
    }
}

void DemonFireWalkTouch()
{
    int owner = GetOwner(GetTrigger() - 2);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        if (UnitCheckEnchant(OTHER, GetLShift(25)))
            return;
        Damage(OTHER, owner, 135, 14);
        UnitSetEnchantTime(OTHER, 25, 36);
    }
}

void FONClass(int curId)
{
    int owner = GetOwner(curId), unit;
    float xVect, yVect;

    if (CurrentHealth(owner))
    {
        xVect = UnitRatioX(curId, owner, 17.0);
        yVect = UnitRatioY(curId, owner, 17.0);
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner));
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), yVect);
        Raise(unit, xVect);
        SetOwner(owner, DummyUnitCreate("Demon", GetObjectX(owner) + xVect, GetObjectY(owner) + yVect) - 2);
        LookWithAngle(unit + 2, GetDirection(owner));
        LookWithAngle(unit, 30);
        SetCallback(unit + 2, 9, DemonFireWalkTouch);
        FrameTimerWithArg(1, unit, DemonFireWalking);
    }
    Delete(curId);
}

int TripleArrowClassCreate(int sOwner, float sX, float sY)
{
    int mis = CreateObjectAt("ArcherArrow", sX, sY);

    SetOwner(sOwner, mis);
    LookAtObject(mis, sOwner);
    LookWithAngle(mis, GetDirection(mis) + 128);
    UnitSetEnchantTime(mis, 25, 0);
    return mis;
}

void CastAbilityTripleArrow(int sOwner, float sForce)
{
    float xVect = UnitAngleCos(sOwner, - 7.0), yVect = UnitAngleSin(sOwner, -7.0);
    float gap = 0.1, xProfile = GetObjectX(sOwner), yProfile = GetObjectY(sOwner);
    
    if (CurrentHealth(sOwner))
    {
        xVect = UnitAngleCos(sOwner, -7.0);
        yVect = UnitAngleSin(sOwner, -7.0);
        PlaySoundAround(sOwner, 592);
        PushObject(
            TripleArrowClassCreate(sOwner, xProfile - xVect, yProfile - yVect), sForce, xProfile, yProfile);
        while (gap < 0.4)
        {
            PushObject(
                TripleArrowClassCreate(sOwner, (gap * yVect) + GetObjectX(sOwner) - xVect, (-gap * xVect) + GetObjectY(sOwner) - yVect), sForce, xProfile, yProfile);
            PushObject(
                TripleArrowClassCreate(sOwner, (-gap * yVect) + GetObjectX(sOwner) - xVect, (gap * xVect) + GetObjectY(sOwner) - yVect), sForce, xProfile, yProfile);
            gap += 0.1;
        }
        UniChatMessage(sOwner, "트리플 에로우 샷!", 150);
    }
}

void RemoveCoreUnits(int ptr)
{
    Delete(ptr);
    Delete(ptr + 1);
}

void BerserkerNoDelayCore(int plr)
{
    int arr[10];

    if (!MaxHealth(arr[plr]))
    {
        arr[plr] = CreateObjectAt("Bear2", LocationX(9 + plr), LocationY(9 + plr));
        CheckMonsterThing(CreateObjectAt("Rat", GetObjectX(arr[plr]), GetObjectY(arr[plr]) + 23.0) - 1);
        SetOwner(player[plr], arr[plr]);
        LookAtObject(arr[plr], arr[plr] + 1);
        HitLocation(arr[plr], GetObjectX(arr[plr] + 1), GetObjectY(arr[plr] + 1));
        FrameTimerWithArg(3, arr[plr], RemoveCoreUnits);
    }
}

void warriorSkillR(int plr)
{
    int trap[10], pUnit = player[plr];

    if (IsObjectOn(trap[plr]))
        MoveObject(trap[plr], GetObjectX(pUnit), GetObjectY(pUnit));
    else
        trap[plr] = CreateObjectAt("PoisonGasTrap", GetObjectX(pUnit), GetObjectY(pUnit));
    SetOwner(pUnit, trap[plr]);
    PlaySoundAround(pUnit, 39);
    BerserkerNoDelayCore(plr);
}

void DetectedSpecificIndex(int curId)
{
    int owner = GetOwner(curId), thingID = GetUnitThingID(curId);

    if (thingID == 526)
    {
        CastAbilityTripleArrow(owner, 22.0);
        Delete(curId);
    }
    else if (thingID == 706)
        FONClass(curId);
}

void LoopSearchIndex()
{
    int curId, tempId;
    int *ptr = 0x750710;

    ptr = ptr[0];
    while (ptr != NULLPTR)
    {
        tempId = ptr[11];
        if (curId)
        {
            while (curId < tempId)
                DetectedSpecificIndex(++curId);
            break;
        }
        curId = tempId;
        break;
    }
    FrameTimer(1, LoopSearchIndex);
}

void ItemUseClassTeleportAmulet()
{
    FrameTimerWithArg(150, GetTrigger() + 1, AmuletClassRespawn);
    Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    MoveObject(OTHER, GetWaypointX(5), GetWaypointY(5));
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    PlaySoundAround(OTHER, 594);
    Delete(SELF);
    UniPrint(OTHER, "시작 위치로 공간이동 되었습니다");
}

int AmuletClassCreate(int sUnit)
{
    int amlet = CreateObjectAt("AmuletofManipulation", GetObjectX(sUnit), GetObjectY(sUnit));

    CreateObjectAt("ImaginaryCaster", GetObjectX(amlet), GetObjectY(amlet));
    SetUnitCallbackOnUseItem(amlet, ItemUseClassTeleportAmulet);
    return amlet;
}

void AmuletClassRespawn(int sUnit)
{
    if (IsObjectOn(sUnit))
    {
        AmuletClassCreate(sUnit);
        Delete(sUnit);
    }
}

void AmuletClassPlacing(int sUnit)
{
    if (IsObjectOn(sUnit))
    {
        AmuletClassCreate(sUnit);
        if (!GetDirection(sUnit))
            FrameTimerWithArg(1, sUnit + 1, AmuletClassPlacing);
        Delete(sUnit);
    }
}

void AmuletClassInit()
{
    int unit = CreateObject("InvisibleLightBlueLow", 23);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueLow", 24) - 1, AmuletClassPlacing);
    CreateObject("InvisibleLightBlueLow", 25);
    CreateObject("InvisibleLightBlueLow", 26);
    CreateObject("InvisibleLightBlueLow", 28);
    CreateObject("InvisibleLightBlueLow", 29);
    CreateObject("InvisibleLightBlueLow", 30);
    CreateObject("InvisibleLightBlueLow", 31);
    CreateObject("InvisibleLightBlueLow", 32);
    CreateObject("InvisibleLightBlueLow", 33);
    CreateObject("InvisibleLightBlueLow", 34);
    CreateObject("InvisibleLightBlueLow", 35);
    CreateObject("InvisibleLightBlueLow", 36);
    CreateObject("InvisibleLightBlueLow", 37);
    CreateObject("InvisibleLightBlueLow", 38);
    CreateObject("InvisibleLightBlueLow", 39);
    CreateObject("InvisibleLightBlueLow", 41);
    CreateObject("InvisibleLightBlueLow", 42);
    CreateObject("InvisibleLightBlueLow", 43);
    CreateObject("InvisibleLightBlueLow", 44);
    CreateObject("InvisibleLightBlueLow", 45);
    CreateObject("InvisibleLightBlueLow", 46);
    CreateObject("InvisibleLightBlueLow", 47);
    CreateObject("InvisibleLightBlueLow", 48);
    CreateObject("InvisibleLightBlueLow", 49);
    CreateObject("InvisibleLightBlueLow", 50);
    CreateObject("InvisibleLightBlueLow", 51);
    CreateObject("InvisibleLightBlueLow", 52);
    CreateObject("InvisibleLightBlueLow", 53);
    CreateObject("InvisibleLightBlueLow", 54);
    CreateObject("InvisibleLightBlueLow", 55);
    CreateObject("InvisibleLightBlueLow", 56);
    LookWithAngle(CreateObject("InvisibleLightBlueLow", 57), 1);
}

void UnitDropItem(int unit)
{
    if (!Random(0, 2))
    {
        MoveWaypoint(2, GetObjectX(unit), GetObjectY(unit));
        DeleteObjectTimer(CreatePotion(2, Random(0, 13)), 900);
    }
}

void SetDeaths()
{   
    UnitDropItem(SELF);
    FrameTimerWithArg(900, ToInt(GetObjectZ(GetTrigger() + 1)), SpawnMonsters);
    Delete(GetTrigger() + 1);
    if (++m_KillScore >= setLessons(0))
        victoryEvent();
}

void victoryEvent() {
    UniPrintToAll("승리__!! 제한시간 내로 목표킬수에 달성했습니다. 축하드려요~");
    m_Shutdown = TRUE;
    RemoveAllMonsters();
    teleportAllPlayers(90);
    PlayWav(593);
    PlayWav(589);
    Effect("WHITE_FLASH", LocationX(90), LocationY(90), 0.0, 0.0);

    FrameTimerWithArg(30, 90, DrawVictoryText);
}

void DrawVictoryText(int location)
{
    MoveWaypoint(1, LocationX(location), LocationY(location));
    StrYourWinner();
}

void DisplayLadderBoard()
{
    int time;

    if (IsCaller(GetTrigger() + 1))
    {
        if (GetDirection(SELF))
            LookWithAngle(SELF, GetDirection(SELF) - 1);
        else
        {
            LookWithAngle(SELF, 30);
            time = ToInt(GetObjectZ(GetTrigger() + 2));
            if (time)
            {
                UniChatMessage(SELF, "남은시간 " + IntToString(time) + "초\n킬 스코어: " + IntToString(m_KillScore) + " / " + IntToString(setLessons(0)), 60);
                Raise(GetTrigger() + 2, time - 1);
            }
            else
            {
                Delete(SELF);
                Delete(GetTrigger() + 1);
                timeOver();
            }
        }
    }
}

void StartCountdown(int time)
{
    int counter = getMaster();

    Raise(counter + 2, time);
    LookWithAngle(counter, 0);
    SetCallback(counter, 9, DisplayLadderBoard);
}

void timeOver()
{
    m_Shutdown = TRUE;
    UniPrintToAll("패배__!! 제한시간이 초과 되었습니다...");
    ObjectOff(Object("StartLocation"));
    RemoveAllMonsters();
    teleportAllPlayers(3);
    PlayWav(589);
    MoveWaypoint(1, LocationX(3), LocationY(3));
    StrFailMission();
}

void RemoveAllMonsters()
{
    int rep = -1;

    while (++rep<sizeof(m_Victim))
    {
        if (CurrentHealth(m_Victim[rep]))
        {
            Delete(m_Victim[rep] + 1);
            Delete(m_Victim[rep]);
        }
    }
}

int getMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 27);
        CreateObject("BlackPowder", 27);
        CreateObject("ImaginaryCaster", 27);
        Frozen(unit, 1);
    }
    return unit;
}

int setLessons(int arg0)
{
    int dump;

    if (arg0 > 0)
        dump = arg0;
    return dump;
}

void SelectRoomToHostPlayer(int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        UnitSetEnchantTime(pUnit, 25, 60);
        MoveObject(pUnit, GetWaypointX(22), GetWaypointY(22));
        UniPrint(pUnit, "목표 킬 수치를 선택해주세요");
        UniPrintToAll(PlayerIngameNick(pUnit) + "님께서 목표킬수를 정하고 있습니다");
    }
}

void KillPointChooseComplete(int pUnit)
{
    int killPoint = setLessons(0);

    MoveObject(pUnit, GetWaypointX(5), GetWaypointY(5));
    UniPrint(pUnit, IntToString(killPoint) + "킬을 선택하셨습니다");
}

void getLessons400()
{
    setLessons(400);
    KillPointChooseComplete(OTHER);
    FrameTimerWithArg(100, 300, noticeKills);
}

void getLessons550()
{
    setLessons(550);
    KillPointChooseComplete(OTHER);
    FrameTimerWithArg(100, 360, noticeKills);
}

void getLessons700()
{
    setLessons(700);
    KillPointChooseComplete(OTHER);
    FrameTimerWithArg(100, 420, noticeKills);
}

void getLessons900()
{
    setLessons(900);
    KillPointChooseComplete(OTHER);
    FrameTimerWithArg(100, 450, noticeKills);
}

void fieldOut()
{
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    MoveObject(OTHER, LocationX(91), LocationY(91));
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    PlaySoundAround(OTHER, SOUND_BlindOff);
}

void teleportAllPlayers(int wp)
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (CurrentHealth(player[i]))
            MoveObject(player[i], LocationX(wp), LocationY(wp));
    }
}

void noticeKills(int time)
{
    UniPrintToAll("목표킬수가 " + IntToString(setLessons(0)) + "킬(제한시간 " + IntToString(time) + "초) 으로 정해졌습니다");
    UniPrintToAll("잠시 후 게임시작과 동시에 전장에 몬스터가 출현됩니다");
    UniPrintToAll("게임 룰: 정해진 시간 내에 목표킬수에 달성하지 못하면 패배처리가 됩니다");
    PlayWav(38);
    FrameTimer(10, LoopSearchIndex);
    FrameTimerWithArg(200, time, StartCountdown);
    FrameTimer(200, initSpawnMonsters);
    FrameTimer(400, NoticeGameInfo);
}

void NoticeGameInfo()
{
    UniPrintToAll("제한시간내로 괴수들을 잡아 목표킬수를 채우세요...!");
    UniPrintToAll("전사 스킬설명: 작살-트리플 에로우 샷, 조심스럽게 걷기-버저커 차지의 딜레이를 없앱니다.");
    UniPrintToAll("자세한 사항은 콘솔 키(F1)를 눌러서 확인하세요.");
    PlayWav(913);
}

void initSpawnMonsters()
{
    int i;

    if (i < 200) {
        SpawnMonsters(i ++);
        FrameTimer(1, initSpawnMonsters);
    }
}

void CollisionEvent()
{
    if (HasClass(OTHER, "MISSILE"))
    {
        if (HasSubclass(OTHER, "CHAKRAM"))
            MoveObject(OTHER, GetObjectX(SELF), GetObjectY(SELF));
    }
}

void SpawnMonsters(int unitIndex)
{
    if (!m_Shutdown)
    {
        if (!CurrentHealth(m_Victim[unitIndex]))
        {
            Delete(m_Victim[unitIndex]);
            int pic = Random(0, 21);
            int unit = CreateObject(m_unitRealNameTable[pic], (unitIndex % 10) + 101);
            AntiCharmSetMonster(unit);
            InsertSpecialValue(pic, unitIndex, unit);
            CheckMonsterThing(unit);
            SetUnitMaxHealth(unit, m_mobHPTable[pic]);
            SetCallback(unit, 3, LookEnemy);
            SetCallback(unit, 5, SetDeaths);
            SetCallback(unit, 7, CollisionEvent);
            SetOwner(getMaster(), unit);
            SpecialUnitAbility(unit, pic);
            m_Victim[unitIndex] = unit;
        }
        else
            UniPrintToAll("인덱스 충돌!_ 인덱스 " + IntToString(unitIndex) + " 에서 에러가 발생했습니다.");
    }
}

void InsertSpecialValue(int pic, int unitIndex, int targetUnit)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(targetUnit), GetObjectY(targetUnit));

    Raise(unit, unitIndex);
    LookWithAngle(unit, pic);
}

void LookEnemy()
{
    if (CurrentHealth(OTHER))
        UniChatMessage(SELF, m_unitDescTable[(GetDirection(GetTrigger() + 1))], 150);
}

void SpecialUnitAbility(int unit, int num)
{
    if (GetUnitStatus(unit) & 0x20)
        Enchant(unit, "ENCHANT_ANCHORED", 0.0);
}

void PlacedPotions()
{
    int idx, i;

    if (idx < 11)
    {
        TeleportLocation(4, LocationX(21), LocationY(21));
        for (i = 0 ; i < 14 ; i ++)
        {
            CreatePotion(4, idx);
            CreatePotion(4, idx);
            CreatePotion(4, idx);
            TeleportLocationVector(4, -23.0, 23.0);
        }
        TeleportLocationVector(21, 23.0, 23.0);
        idx ++;
        FrameTimer(1, PlacedPotions);
    }
}

int CreatePotion(int location, int idx)
{
    return CheckPotionThingID(CreateObject(potionList(idx), location));
}

void InitMonsterInfo()
{
    string name[] = {"Swordsman", "Archer", "Skeleton", "SkeletonLord", "WillOWisp", "WizardGreen", "BlackWolf", "OgreBrute",
        "GruntAxe", "Scorpion", "Shade", "AlbinoSpider", "BlackWidow", "Troll", "Bear", "OgreWarlord", "MeleeDemon",
        "FireSprite", "FlyingGolem", "EvilCherub", "GiantLeech", "Bat"};
    string desc[] = { "만취자", "아베신조", "허세남", "고교 패거리", "위스프",
        "최순실(비선실세)", "거리에 똥싸는 개", "불륜남", "불륜녀", "음식가지고 장난치는 사람",
        "묻지마 살인범", "성폭행범", "(히든)대왕거미", "일베충", "폭주족",
        "난폭운전자", "방화범", "불의 정령", "무선조종드론", "까마귀",
        "자이언트 거머리", "닭둘기"};
    int hpTable[] = {306, 128, 250, 290, 250,
        225, 225, 295, 225, 240,
        180, 180, 200, 285, 325,
        350, 225, 96, 96, 128,
        180, 96};
    m_unitRealNameTable=&name;
    m_unitDescTableLength=sizeof(name);
    m_unitDescTable=&desc;
    m_mobHPTable=&hpTable;
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

void PlayWav(int soundId)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]))
            PlaySoundAround(player[i], soundId);
    }
}

int reventCharm() {
    int var_0;

    if (!var_0)
        var_0 = CreateObject("InvisibleLightBlueHigh", 20);
    return var_0;
}

int CreateYellowPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 639); //YellowPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateBlackPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 641); //BlackPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateWhitePotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 640); //WhitePotion
    SetMemory(ptr + 12, GetMemory(ptr + 12) ^ 0x20);
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = CreateYellowPotion(125, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 640)
        x = CreateWhitePotion(100, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 641)
        x = CreateBlackPotion(85, GetObjectX(unit), GetObjectY(unit));
    if (x ^ unit) Delete(unit);
    return x;
}

string potionList(int numbering)
{
    string potionTable[] = {"RedPotion", "BluePotion", "CurePoisonPotion", "VampirismPotion", "ShieldPotion", "HastePotion",
        "WhitePotion", "YellowPotion", "BlackPotion", "RedPotion", "RedPotion", "PoisonProtectPotion", "FireProtectPotion",
        "ShockProtectPotion"};

    return potionTable[numbering];
}

void MakePermanentItem()
{
    int inv = GetLastItem(OTHER), res = 0;

    while (inv)
    {
        if (UnitCheckEnchant(inv, GetLShift(23)))
            1;
        else
        {
            UnitSetEnchantTime(inv, 23, 0);
            Nop( res ++ );
        }
        inv = GetPreviousItem(inv);
    }
    if (res)
    {
        UnitSetEnchantTime(OTHER, 25, 30);
        UniPrint(OTHER, "[!!] " + IntToString(res) + " 개의 아이템을 무적화 했습니다");
    }
}

int CheckOwner(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsOwnedBy(unit, player[i]))
            return i;
    }
    return -1;
}

void StrYourWinner()
{
	int arr[26], i, count = 0;
	string name = "HealOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 1008796286; arr[1] = 1338015748; arr[2] = 270541312; arr[3] = 134236228; arr[4] = 35653888; arr[5] = 75776516; arr[6] = 9438912; arr[7] = 1146208324; 
	arr[8] = 25362576; arr[9] = 100417554; arr[10] = 585874; arr[11] = 67190528; arr[12] = 659584; arr[13] = 269745921; arr[14] = 8388798; arr[15] = 4097; 
	arr[16] = 2114454018; arr[17] = 16781312; arr[18] = 2105848; arr[19] = 1879183392; arr[20] = 35668031; arr[21] = 553783300; arr[22] = 67239936; arr[23] = 1090543556; 
	arr[24] = 537132063; arr[25] = 67124992; 
	for (i = 0 ; i < 26 ; i ++)
		count = DrawStrYourWinner(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrYourWinner(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 806 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 74 == 73)
			MoveWaypoint(1, GetWaypointX(1) - 146.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void StrFailMission()
{
	int arr[22], i, count = 0;
	string name = "HealOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 270598782; arr[1] = 285235140; arr[2] = 272171586; arr[3] = 285233156; arr[4] = 673251906; arr[5] = 285233796; arr[6] = 1210221122; arr[7] = 285233796; 
	arr[8] = 71074370; arr[9] = 285233797; arr[10] = 36192834; arr[11] = 285241984; arr[12] = 2082480706; arr[13] = 285233799; arr[14] = 2097730; arr[15] = 21124; 
	arr[16] = 2080391746; arr[17] = 285233799; arr[18] = 67125886; arr[19] = 293361600; arr[20] = 2084553216; arr[21] = 20487; 
	for (i = 0 ; i < 22 ; i ++)
		count = DrawStrFailMission(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrFailMission(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 62 == 61)
			MoveWaypoint(1, GetWaypointX(1) - 122.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void InitMapSign()
{
    RegistSignMessage(Object("SignKill400"), "목표킬수 400 킬로 설정합니다");
    RegistSignMessage(Object("SignKill500"), "목표킬수 550 킬로 설정합니다");
    RegistSignMessage(Object("SignKill600"), "목표킬수 700 킬로 설정합니다");
    RegistSignMessage(Object("TestPicket1"), "인벤토리 내 모든 아이템을 파괴불능 상태로 설정해주는 비콘입니다. 3개 모두 같은 역할을 합니다");
    RegistSignMessage(Object("TestPicket2"), "우리에겐 시간이 없어요! 우측 상단에 표시된 시간내에 목표킬수에 달성해야 합니다");
}

void MapExit()
{
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}

void UserMapSetting()
{
    SetMemory(0x5d5330, 0x2000);
    SetMemory(0x5d5394, 1);
}

void StartLocationOn(int startLocationName)
{
    if (ToInt(GetObjectX(startLocationName)))
    {
        ObjectOn(startLocationName);
        FrameTimer(10, PreservePlayersLoop);
        UserMapSetting();
        FrameTimer(1, AmuletClassInit);
    }
}

