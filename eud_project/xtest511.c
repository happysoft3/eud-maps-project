
#include "xtest511_main.h"
#include "libs/spellutil.h"
#include "libs/monsteraction.h"

int BossCnt = 20, GOver;
int player[20], Dungeon[6];

static void NetworkUtilClientMain()
{
    InitializeResources();
}

void HealingBuff(int sUnit)
{
    int owner = GetOwner(sUnit), durate = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(owner))
        {
            if (durate)
            {
                RestoreHealth(owner, 1);
                LookWithAngle(sUnit, durate - 1);
                MoveObject(sUnit, GetObjectX(owner), GetObjectY(owner));
                FrameTimerWithArg(1, sUnit, HealingBuff);
                break;
            }
            GreenSparkAt(GetObjectX(owner), GetObjectY(owner));
            UniPrint(owner, "힐링 포션의 효력이 사라졌습니다");
        }
        Delete(sUnit);
        break;
    }
}

void ItemUseClassTeleportAmulet()
{
    if (CurrentHealth(OTHER))
    {
        Delete(SELF);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER, LocationX(12), LocationY(12));
        PlaySoundAround(OTHER, 592);
        Effect("COUNTERSPELL_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        UniPrint(OTHER, "시작 위치로 공간이동했습니다");
    }
}

void ItemUseFastHealingPotion()
{
    int unit;

    if (CurrentHealth(OTHER))
    {
        Delete(SELF);
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(OTHER, unit);
        UnitNoCollide(unit);
        LookWithAngle(unit, 250);
        UnitSetEnchantTime(unit, 8, 0);
        UnitSetEnchantTime(unit, 26, 0);
        UnitSetEnchantTime(unit, 21, 0);
        FrameTimerWithArg(3, unit, HealingBuff);
        UniPrint(OTHER, "힐링 포션을 사용했습니다. 잠시동안 체력이 빠르게 회복될 것입니다");
    }
}

void ItemUseShockEnchant()
{
    if (CurrentHealth(OTHER))
    {
        Delete(SELF);
        UnitSetEnchantTime(OTHER, 22, 30 * 70);
    }
}

void ItemUseOneCoin()
{
    Delete(SELF);
    int c;
    GetXTraCoin(0,&c);
    if (c)
    {
        GetXTraCoin(++c,0);
        char buff[128];

        NoxSprintfString(buff, "코인 1개가 추가되어 모두 %d 개를 보유하고 있습니다", &c, 1);
        UniPrint(OTHER, ReadStringAddressEx(buff));
    }
}

////////////Impletmented ChainingList//////



void TeleportAmulet(int sUnit)
{
    SetUnitCallbackOnUseItem(sUnit, ItemUseClassTeleportAmulet);
}

void HealingPotion(int sUnit)
{
    SetUnitCallbackOnUseItem(sUnit, ItemUseFastHealingPotion);
}

void ElectricAmulet(int sUnit)
{
    SetUnitCallbackOnUseItem(sUnit, ItemUseShockEnchant);
}

void AddOneCoin(int sUnit)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        SetMemory(ptr + 0x2c4, 0x4f31e0);
        SetUnitCallbackOnUseItem(sUnit, ItemUseOneCoin);
    }
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639) //here
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    else if (thingID == 1184)
        TeleportAmulet(unit);
    else if (thingID == 239)
        ElectricAmulet(unit);
    else if (thingID == 1185)
        HealingPotion(unit);
    else if (thingID == 2689)
        AddOneCoin(unit);

    if (x ^ unit) Delete(unit);
    PotionPickupRegist(x);
    return x;
}

void KeepOutMonsterHere()
{
    if (IsOwnedBy(OTHER, GetMaster()))
    {
        int safeZone=GetSafeZone();
        PushObjectTo(OTHER, UnitRatioX(OTHER, safeZone, 100.0), UnitRatioY(OTHER, safeZone, 100.0));
    }
}






// void CheckSpecialItem(int weapon)
// {
//     int thingId = GetUnitThingID(weapon);

//     if (thingId >= 222 && thingId <= 225)
//     {
//         DisableOblivionItemPickupEvent(weapon);
//         SetItemPropertyAllowAllDrop(weapon);
//     }
//     else if (thingId == 1178 || thingId == 1168)
//         SetConsumablesWeaponCapacity(weapon, 255, 255);
// }

// void NormalMonsterItemRelease(int unit)
// {
//     MoveWaypoint(1, GetObjectX(unit), GetObjectY(unit));
//     CallFunctionWithArgInt(FieldItemFunctionTable(Random(0, 4)), 1);
// }

// void DungeonMonsterItemRelease(int unit)
// {
//     MoveWaypoint(1, GetObjectX(unit), GetObjectY(unit));
//     CallFunctionWithArgInt(DungeonItemFunctionTable(Random(0, 7)), 1);
// }



static void BuyNewSkill()
{
    int plr, pay = ToInt(GetObjectZ(GetTrigger() + 1)), byte = GetDirection(GetTrigger() + 1);

    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= pay)
        {
            plr = CheckPlayer();
            if (plr + 1)
            {
                if (player[plr + 10] & byte)
                {
                    UniPrint(OTHER, "오류_ 이미 습득한 상태 입니다!");
                }
                else
                {
                    player[plr + 10] = player[plr + 10] ^ byte;
                    ChangeGold(OTHER, -pay);
                    AbilityAwardEvent(GetCaller());
                    UniPrint(OTHER, "성공_ 마법 습득완료");
                }
            }
        }
    }
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.3);
        UniPrint(OTHER, SkillDescript(GetDirection(SELF)));
        char buff[128];

        NoxSprintfString(buff, "이 작업은 %d골드가 필요해요. 계속하려면 더블클릭 하세요", &pay, 1);
        UniPrint(OTHER, ReadStringAddressEx(buff));
    }
}

void MapExit()
{
    MusicEvent();
    OnShutdownMap();
}

void TeleportAllPlayer(int wp)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]))
            MoveObject(player[i], LocationX(wp), LocationY(wp));
    }
}

void ShutdownThisGame(int wp)
{
    ObjectOff(Object("MainProcBeacon"));
    TeleportAllPlayer(wp);
    MoveObject(StartLocationWithPlayer(), LocationX(wp), LocationY(wp));
}

void PlayerDeath()
{
    int c;
    GetXTraCoin(0, &c);
    if (c)
    {
        c-=1;
        char buff[256];
        NoxSprintfString(buff, "누군가 죽었습니다, 라이프가 하나 차감되며 더 이상 라이프가 없을 시 패배처리가 됩니다, 남은 라이프: %d", &c, 1);
        UniPrintToAll(ReadStringAddressEx(buff));
    }
    else
    {
        DefeatMission();
    }
}

void DefeatMission()
{
    if (!GOver)
    {
        GOver = 1;
        ShutdownThisGame(20);
        AudioEvent("StaffOblivionAchieve1", 20);
        UniPrintToAll("미션실패! 라이프가 모두 소모되었습니다!!");

        FrameTimer(30, StrMissionFail);
    }
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

int PlayerClassOnInit(int plrIdx, int plrUnit)
{
    int pResult = plrIdx;

    player[plrIdx] = plrUnit;
    player[plrIdx + 10] = 1;
    ChangeGold(plrUnit, -GetGold(plrUnit));
    SelfDamageClassEntry(plrUnit);
    DiePlayerHandlerEntry(plrUnit);
    EmptyAll(plrUnit);
    if (ValidPlayerCheck(plrUnit))
    {
        pResult |= (1 << 8);
    }
    UniPrintToAll(PlayerIngameNick(plrUnit) + " 님께서 게임에 참가하셨습니다");
    return pResult;
}

void PlayerEntryPoint()
{
    int plr, i;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            if (MaxHealth(OTHER) == 150)
            {
                plr = CheckPlayer();
                for (i = 9 ; i >= 0 && plr < 0 ; i --)
                {
                    if (!MaxHealth(player[i]))
                    {
                        plr = PlayerClassOnInit(i, GetCaller());
                        break;
                    }
                }
                if (plr + 1)
                {
                    if (plr >> 8)
                        PlayerClassFirstJoin(OTHER, plr & 0xff);
                    else
                        PlayerJoin(plr);
                    break;
                }
            }
        }
        PlayerCantJoin();
        break;
    }
}

void EmptyAll(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void TeleportPlayer(int pArg)
{
    int location = pArg & 0x3ff, plr = (pArg >> 10) & 0x3ff;
    int pUnit = player[plr];

    if (CurrentHealth(pUnit))
        MoveObject(pUnit, LocationX(location), LocationY(location));
}

void PlayerClassFirstJoin(int pUnit, int plr)
{
    Enchant(pUnit, "ENCHANT_BLINDED", 1.0);
    MoveObject(pUnit, LocationX(91), LocationY(91));
    FrameTimerWithArg(25, (plr << 10) | 89, TeleportPlayer);
    UniPrint(pUnit, "지도에 입장을 시도하고 있습니다... 잠시만 기다려 주세요");
}

void PlayerJoin(int plr)
{
    if (PlayerClassCheckDeathFlag(plr))
    {
        // EquipFix(player[plr]);  //@brief. 28th march 2021 23:04 추가
        PlayerClassSetDeathFlag(plr);
    }
    MoveObject(player[plr], LocationX(12), LocationY(12));
    Effect("TELEPORT", LocationX(12), LocationY(12), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("BlueRain", 12), 10);
    AudioEvent("BlindOff", 12);
}

void PlayerCantJoin()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    MoveObject(OTHER, LocationX(11), LocationY(11));
    UniPrintToAll("맵 입장에 실패했습니다");
}

void PlayerClassOnFree(int plr)
{
    int *ptr = UnitToPtr(player[plr]);

    if (ptr != NULLPTR)
        CancelPlayerDialogWithPTR(ptr);
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

static void PlayerPreserveHandler()
{
    int i;

    for (i = 9 ; i >= 0 ; i-=1)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    CheckUseSkill(i);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i)) break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, PlayerPreserveHandler);
}

void BerserkerNoDelayCore(int plr)
{
    int arr[10];

    if (!MaxHealth(arr[plr]))
    {
        arr[plr] = CreateObject("Bear2", 22 + plr);
        UnitLinkBinScript(CreateObjectAt("Rat", GetObjectX(arr[plr]), GetObjectY(arr[plr]) + 20.0) - 1, Bear2BinTable());
        SetOwner(player[plr], arr[plr]);
        LookAtObject(arr[plr], arr[plr] + 1);
        HitLocation(arr[plr], GetObjectX(arr[plr]), GetObjectY(arr[plr]));
        FrameTimerWithArg(3, arr[plr], RemoveCoreUnits);
    }
}

void RemoveCoreUnits(int ptr)
{
    Delete(ptr);
    Delete(ptr + 1);
}

void CheckUseSkill(int plr)
{
    if (HasEnchant(player[plr], "ENCHANT_SNEAK"))
    {
        EnchantOff(player[plr], "ENCHANT_SNEAK");
        RemoveTreadLightly(player[plr]);
        if (player[plr + 10] & 0x2)
            PlayerWindBoost(player[plr]);
        else if (player[plr + 10] & 8)
        {
            BerserkerNoDelayCore(plr);
            GreenSparkAt(GetObjectX(player[plr]), GetObjectY(player[plr]));
        }
    }
    if (player[plr + 10] & 0x04)
    {
        if (CheckPlayerInput(player[plr]) == 47)
        {
            if (!HasEnchant(player[plr], "ENCHANT_LIGHT"))
                ThunderBolt(player[plr]);
        }
    }
    if (player[plr + 10] & 0x10)
    {
        AdvancedStat(player[plr]);
    }
}

void PlayerFastJoin()
{
    int plr = CheckPlayer();

    if (CurrentHealth(OTHER))
    {
        if (plr < 0)
            MoveObject(OTHER, LocationX(90), LocationY(90));
        else
        {
            MoveObject(OTHER, LocationX(89), LocationY(89));
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
    }
}

void AdvancedStat(int unit)
{
    if (!GOver)
    {
        if (!HasEnchant(unit, "ENCHANT_BURNING"))
        {
            Enchant(unit, "ENCHANT_BURNING", 0.1);
            RestoreHealth(unit, 1);
            Effect("GREATER_HEAL", GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit) - 180.0);
        }
        if (GetPlayerAction(unit) == 5)
            PushObjectTo(unit, UnitAngleCos(unit, 1.3), UnitAngleSin(unit, 1.3));
    }
}

void PlayerWindBoost(int unit)
{
    Effect("RICOCHET", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    PushObjectTo(unit, UnitAngleCos(unit, 60.0), UnitAngleSin(unit, 60.0));
}

void ThunderBolt(int unit)
{
    float vectX = UnitAngleCos(unit, 38.0), vectY = UnitAngleSin(unit, 38.0);
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit) + vectX, GetObjectY(unit) + vectY);

    Enchant(unit, "ENCHANT_LIGHT", 8.0);
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), vectY);
    Raise(ptr, vectX);
    SetOwner(unit, ptr);
    FrameTimerWithArg(3, ptr, ThunderBoltCritical);
}

void ThunderBoltCritical(int ptr)
{
    float vectX = GetObjectZ(ptr), vectY = GetObjectZ(ptr + 1);
    int owner = GetOwner(ptr), i, unit;

    if (CurrentHealth(owner))
    {
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)) + 1;
        Raise(unit - 1, ToFloat(owner));
        for (i = 0 ; i < 13 ; i ++)
        {
            Frozen(CreateObjectAt("ShopkeeperConjurerRealm", GetObjectX(ptr), GetObjectY(ptr)), 1);
            SetOwner(unit - 1, unit + i);
            DeleteObjectTimer(unit + i, 1);
            SetUnitFlags(unit + i, GetUnitFlags(unit + i) ^ 0x2000);
            SetCallback(unit + i, 9, ThunderBoltCollideHandler);
            MoveObject(ptr, GetObjectX(ptr) + vectX, GetObjectY(ptr) + vectY);
            if (!IsVisibleTo(ptr, ptr + 1))
                break;
        }
        
        DrawYellowLightningFx(GetObjectX(ptr + 1), GetObjectY(ptr + 1), GetObjectX(ptr), GetObjectY(ptr), 24);
        DeleteObjectTimer(unit - 1, 3);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void ThunderBoltCollideHandler()
{
    int owner = ToInt(GetObjectZ(GetOwner(SELF)));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 200, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.09);
    }
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

void CheckResetSight(int unit, int delay)
{
    if (!UnitCheckEnchant(unit, GetLShift(6)))
    {
        Enchant(unit, EnchantList(6), 0.0);
        FrameTimerWithArg(delay, unit, ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, EnchantList(6));
    Enchant(unit, EnchantList(2), 0.06);
    AggressionLevel(unit, 1.0);
}


void MapInitialize()
{
    MusicEvent();
    OnInitializeMap();
}

int SummonPowerGhost(int ptr)
{
    int unit = CreateObjectById(OBJ_GHOST, GetObjectX(ptr), GetObjectY(ptr));

    SetUnitScanRange(unit, 450.0);
    GhostSubProcess(unit);
    return unit;
}

int SummonPowerPurpleGirl(int ptr)
{
    int unit = CreateObjectById(OBJ_WOUNDED_CONJURER, GetObjectX(ptr), GetObjectY(ptr));

    SetUnitScanRange(unit, 400.0);
    BullDakSubProcess(unit);
    // FrameTimerWithArg(1, unit + 1, BullDakImageLoop);
    return unit;
}

int SummonPowerMonster3(int ptr)
{
    int unit = CreateObjectAt("Demon", GetObjectX(ptr), GetObjectY(ptr));

    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    SetCallback(unit, 3, DemonSightEvent);
    SetCallback(unit, 13, DemonResetSight);
    SetUnitScanRange(unit, 400.0);
    SetUnitMaxHealth(unit, 500);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020); //ALWAYS_RUN

    return unit;
}

int SummonGWizard(int ptr)
{
    int unit = CreateObjectAt("WizardGreen", GetObjectX(ptr), GetObjectY(ptr));

    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    SetCallback(unit, 3, GWizSightEvent);
	SetCallback(unit, 13, GWizResetSight);
    SetUnitScanRange(unit, 400.0);
    SetUnitMaxHealth(unit, 420);
    UnitZeroFleeRange(unit);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020); //ALWAYS_RUN

    return unit;
}

int SummonPowerMonster5(int ptr)
{
    int unit = CreateObjectAt("FireSprite", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, FireSpriteBinTable());
    SetUnitScanRange(unit, 400.0);
    SetUnitMaxHealth(unit, 420);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000); //ALWAYS_RUN
    SetUnitSpeed(unit, ToFloat(1076426178));

    return unit;
}

int SummonPowerMecaGolem(int ptr)
{
    int unit = CreateObjectAt("StoneGolem", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitScanRange(unit, 450.0);
    StoneGolemSubProcess(unit);
    return unit;
}

int SummonHecubah(int target)
{
    int unit = CreateObjectAt("Hecubah", GetObjectX(target), GetObjectY(target));
    
    HecubahSubProcess(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)) - 1);
    SetUnitScanRange(unit, 500.0);
    SetUnitMaxHealth(unit, 1350);
    SetCallback(unit, 3, HecubahSight);
    SetCallback(unit, 13, HecubahLostEnemy);

    return unit;
}

int SummonBomber(int target)
{
    string name[] = {"Bomber", "BomberBlue", "BomberGreen", "BomberYellow"};
    int unit = CreateObjectAt(name[Random(0, 3)], GetObjectX(target), GetObjectY(target));
    int ptr = GetMemory(0x750710);

    UnitLinkBinScript(unit, BomberGreenBinTable());
    SetUnitVoice(unit, 57);
    SetUnitMaxHealth(unit, 600);
    SetMemory(ptr + 0x2b8, 0x4e83b0);
    SetUnitScanRange(unit, 330.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    AggressionLevel(unit, 1.0);

    return unit;
}

void HecubahImpactDamage(int sUnit)
{
    int owner = GetOwner(sUnit), durate = GetDirection(sUnit), target = ToInt(GetObjectZ(sUnit));

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(target))
        {
            if (WallUtilGetWallAtObjectPosition(target))
            {
                Damage(target, 0, 120, 11);
                PlaySoundAround(target, 42);
                Effect("SPARK_EXPLOSION", GetObjectX(target), GetObjectY(target), 0.0, 0.0);
            }
            else if (durate)
            {
                FrameTimerWithArg(1, sUnit, HecubahImpactDamage);
                LookWithAngle(sUnit, durate - 1);
                MoveObject(sUnit + 1, GetObjectX(target) + UnitAngleCos(sUnit + 1, 8.0), GetObjectY(target) + UnitAngleSin(sUnit + 1, 8.0));
                if (!ToInt(GetObjectZ(sUnit + 1)))
                {
                    Raise(sUnit + 1, 6.0);
                    Damage(target, 0, 20, 11);
                }
                PlaySoundAround(target, 171);
                DeleteObjectTimer(CreateObjectAt("OldSmoke", GetObjectX(sUnit + 1), GetObjectY(sUnit + 1)), 12);
                break;
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        break;
    }
}

void HecubahMeleeAttack(int caster, int target)
{
    int unit = CreateObjectAt("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster));

    Frozen(CreateObjectAt("Maiden", GetObjectX(target) + UnitRatioX(caster, target, 3.0), GetObjectY(target) + UnitRatioY(caster, target, 3.0)), 1);
    LookAtObject(unit + 1, caster);
    Raise(unit, target);
    SetOwner(caster, unit);
    LookWithAngle(unit, 128);
    Enchant(target, EnchantList(25), 3.0);
    FrameTimerWithArg(1, unit, HecubahImpactDamage);
}

void BlueCrystalCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 200, 11);
            Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int ThrowBlueCrystal(int sUnit, int sTarget)
{
    int mis = CreateObjectAt("Gameball", GetObjectX(sUnit) + UnitRatioX(sTarget, sUnit, 19.0), GetObjectY(sUnit) + UnitRatioY(sTarget, sUnit, 19.0));

    SetUnitCallbackOnCollide(mis, BlueCrystalCollide);
    SetOwner(sUnit, mis);
    DeleteObjectTimer(mis, 240);

    return mis;
}

void DelayShootToTarget(int sUnit)
{
    int durate = GetDirection(sUnit);
    int mis = sUnit + 1, target = ToInt(GetObjectZ(sUnit));
    
    while (1)
    {
        if (IsObjectOn(mis) && CurrentHealth(target))
        {
            if (durate)
            {
                PushObjectTo(mis, UnitRatioX(target, mis, 0.6), UnitRatioY(target, mis, 0.6));
                LookWithAngle(sUnit, durate - 1);
                FrameTimerWithArg(1, sUnit, DelayShootToTarget);
                break;
            }
        }
        Delete(sUnit);
        break;
    }
}

void HecubahSight()
{
    int delay = 30, unit;

    if (!UnitCheckEnchant(SELF, GetLShift(30)))
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < 200.0)
        {
            LookAtObject(SELF, OTHER);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            HecubahMeleeAttack(GetTrigger(), GetCaller());
        }
        else
        {
            unit = CreateObjectAt("ImaginaryCaster", GetObjectX(SELF), GetObjectY(SELF));
            Raise(unit, GetCaller());
            LookWithAngle(unit, 250);
            FrameTimerWithArg(1, unit, DelayShootToTarget);
            CreatureIdle(SELF);
            MonsterForceCastSpell(SELF, 0, GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 30.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 30.0));
            PushObject(ThrowBlueCrystal(SELF, OTHER), -1.5, GetObjectX(OTHER), GetObjectY(OTHER));
            delay = 20;
        }
        Enchant(SELF, EnchantList(30), 0.5);
    }
    if (GetCaller() ^ GetUnit1C(GetTrigger() + 1))
    {
        SetUnit1C(GetTrigger() + 1, GetCaller());
        AggressionLevel(SELF, 1.0);
    }
    CheckResetSight(GetTrigger(), delay);
}

void HecubahLostEnemy()
{
    int enemy = GetUnit1C(GetTrigger() + 1);

    if (CurrentHealth(enemy))
        Attack(SELF, enemy);
}

void WhenRunout()
{
    EnchantOff(SELF, EnchantList(2));
}

void onGhostAttack()
{
    Damage(OTHER, SELF, 15, DAMAGE_TYPE_AIRBORNE_ELECTRIC);
    DeleteObjectTimer(CreateObjectById(OBJ_RELEASED_SOUL, GetObjectX(OTHER), GetObjectY(OTHER)), 9);
    PlaySoundAround(OTHER,SOUND_DeathRayKill);
}

int GhostBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936681031; arr[1] = 116; arr[16] = 20000; arr[17] = 180; arr[18] = 10; 
		arr[19] = 60; arr[21] = 1065353216; arr[23] = 1; arr[24] = 1066108191; arr[27] = 1; 
		arr[28] = 1092616192; arr[29] = 10; arr[31] = 4; arr[59] = 5542784; arr[60] = 1325; 
		arr[61] = 46900224; 
        CustomMeleeAttackCode(arr, onGhostAttack);
	pArr = arr;
	return pArr;
}

void GhostSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 180;	hpTable[1] = 180;
	int *uec = ptr[187];
	uec[360] = 1;		uec[121] = GhostBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int StoneGolemBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852798035; arr[1] = 1819232101; arr[2] = 28005; arr[16] = 120000; arr[17] = 300; 
		arr[18] = 300; arr[19] = 300; arr[21] = 1065353216; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[28] = 1120403456; arr[29] = 100; arr[31] = 2; arr[32] = 3; 
		arr[33] = 3; arr[58] = 5545888; arr[59] = 5543904; arr[60] = 1324; arr[61] = 46901248; 
	pArr = arr;
	return pArr;
}

void StoneGolemSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1091567616;		ptr[137] = 1091567616;
	int *hpTable = ptr[139];
	hpTable[0] = 800;	hpTable[1] = 800;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = StoneGolemBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void DemonSightEvent()
{
    if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < 98.0)
	{
		DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
		Damage(OTHER, SELF, 30, 14);
	}
	if (GetCaller() ^ ToInt(GetObjectZ(GetTrigger() + 1)))
	{
		CreatureFollow(SELF, OTHER);
		Raise(GetTrigger() + 1, ToFloat(GetCaller()));
	}
	MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void DemonResetSight()
{
	int target = ToInt(GetObjectZ(GetTrigger() + 1));

	EnchantOff(SELF, "ENCHANT_BLINDED");
	if (!IsVisibleTo(SELF, target) || !CurrentHealth(target))
	{
		Raise(GetTrigger() + 1, ToFloat(0));
		CreatureIdle(SELF);
	}
}

void GWizSightEvent()
{
	LookAtObject(SELF, OTHER);
    if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    {
	    SummonFrog(GetTrigger(), GetCaller());
        Enchant(SELF, EnchantList(ENCHANT_PROTECT_FROM_MAGIC), 0.09);
    }
	if (GetCaller() ^ ToInt(GetObjectZ(GetTrigger() + 1)))
	{
		CreatureFollow(SELF, OTHER);
		Raise(GetTrigger() + 1, ToFloat(GetCaller()));
	}
	MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void GWizResetSight()
{
	int target = ToInt(GetObjectZ(GetTrigger() + 1));

	EnchantOff(SELF, "ENCHANT_BLINDED");
	if (!IsVisibleTo(SELF, target) || !CurrentHealth(target))
	{
		Raise(GetTrigger() + 1, ToFloat(0));
		CreatureIdle(SELF);
	}
}

void SummonFrog(int owner, int target)
{
	int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));

	UnitNoCollide(CreateObjectAt("GreenFrog", GetObjectX(unit), GetObjectY(unit)));
	ObjectOff(unit + 1);
	SetOwner(owner, unit);
	Raise(unit, ToFloat(target));
	FrameTimerWithArg(1, unit, FrogFlyingHandler);
}

void FrogFlyingHandler(int ptr)
{
	int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

	if (CurrentHealth(owner) && CurrentHealth(target) && count < 50)
	{
		if (IsVisibleTo(ptr + 1, target))
		{
			if (Distance(GetObjectX(ptr + 1), GetObjectY(ptr + 1), GetObjectX(target), GetObjectY(target)) > 29.0)
			{
				MoveObject(ptr + 1, GetObjectX(ptr + 1) + UnitRatioX(target, ptr + 1, 11.0), GetObjectY(ptr + 1) + UnitRatioY(target, ptr + 1, 11.0));
				LookAtObject(ptr + 1, target);
				Walk(ptr + 1, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
				LookWithAngle(ptr, count + 1);
			}
			else
			{
				DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(target), GetObjectY(target)), 9);
				Damage(target, owner, 10, 14);
				ObjectOn(ptr + 1);
				Damage(ptr + 1, 0, MaxHealth(ptr + 1) + 1, -1);
				LookWithAngle(ptr, 100);
			}
		}
		else
			LookWithAngle(ptr, 100);
		FrameTimerWithArg(1, ptr, FrogFlyingHandler);
	}
	else
	{
		Delete(ptr);
		Delete(ptr + 1);
	}
}

void AbsoluteTargetStrike(int owner, int target, float threshold, int func)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target), GetObjectY(target));
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner)), threshold);

    SetOwner(owner, unit);
    Raise(unit, ToFloat(target));
    FrameTimerWithArg(1, unit, func);
}

void FireSpritEnemyFind()
{
    AbsoluteTargetStrike(GetTrigger(), GetCaller(), DistanceUnitToUnit(SELF, OTHER) / 42.0, SpitImpShot);
    UnitSetEnchantTime(SELF, 2, 60);
}

void WizRunAway()
{
	if (HasEnchant(SELF, "ENCHANT_ANTI_MAGIC"))
	{
		EnchantOff(SELF, "ENCHANT_ANTI_MAGIC");
	}
}

int ThrowSmallFist(int owner, int target)
{
    int fistTrap = CreateObjectAt("ImaginaryCaster", GetObjectX(target), GetObjectY(target)) + 1;

    Delete(fistTrap - 1);
    CastSpellLocationObject("SPELL_FIST", GetObjectX(owner), GetObjectY(owner), target);
    SetOwner(owner, fistTrap);
    return fistTrap;
}

void SingleMissileShooter(int owner, int target)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));

    SetOwner(owner, unit);
    ShotSingleMagicMissile(unit, target);
    DeleteObjectTimer(unit, 60);
}

void ShotSingleMagicMissile(int unit, int target)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));

    CastSpellObjectLocation("SPELL_MAGIC_MISSILE", unit, GetObjectX(target), GetObjectY(target));
    Delete(ptr);
    Delete(ptr + 2);
    Delete(ptr + 3);
    Delete(ptr + 4);
}

void OpenBossGate()
{
    int key, count;

    count ++;
    ObjectOff(SELF);
    if (count ^ 6)
    {
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        key = CreateObjectAt("RedOrbKeyOfTheLich", 200.0, 200.0);
        Enchant(key, "ENCHANT_FREEZE", 0.0);
        MoveObject(key, GetObjectX(OTHER), GetObjectY(OTHER));
        Raise(key, 225.0);
        AudioEvent("KeyDrop", 1);
    }
    else
    {
        UnlockDoor(Object("BossGate1"));
        UnlockDoor(Object("BossGate11"));
        FrameTimerWithArg(30, BossCnt, StartBossMonsterSummons);
        UniPrint(OTHER, "보스 방 게이트를 열었다");
        char buff[128], *userName = StringUtilGetScriptStringPtr(PlayerIngameNick(OTHER));

        NoxSprintfString(buff, "방금%s님이 보스방 출입문을 열었습니다", &userName, 1);
        UniPrintToAll(ReadStringAddressEx(buff));
    }
}

void WallsLineOpen(int x, int y, int count, int dir)
{
    int i;

    for (i = 0 ; i < count ; i ++)
        WallOpen(Wall(x + i, y + (i*dir)));
}

int DungeonMobDeathFunc()
{
    StopScript(DungeonMonster1Death);
}

void DungeonMonster1Death()
{
    int ptr = Dungeon[0] + 1;
    int count = GetDirection(ptr);

    LookWithAngle(ptr, count - 1);
    DungeonUnitDeathHandler();
    if (!GetDirection(ptr))
    {
        WallsLineOpen(49, 129, 7, -1);
        UniPrintToAll("던전1 의 비밀벽이 열립니다");
    }
}

void DungeonMonster2Death()
{
    int ptr = Dungeon[1] + 1;
    int count = GetDirection(ptr);

    LookWithAngle(ptr, count - 1);
    DungeonUnitDeathHandler();
    if (!GetDirection(ptr))
    {
        WallsLineOpen(62, 116, 7, -1);
        UniPrintToAll("던전2 의 비밀벽이 열립니다");
    }
}

void DungeonMonster3Death()
{
    int ptr = Dungeon[2] + 1;
    int count = GetDirection(ptr);

    LookWithAngle(ptr, count - 1);
    Delete(GetTrigger() + 1);
    DungeonUnitDeathHandler();
    if (!GetDirection(ptr))
    {
        WallsLineOpen(77, 101, 7, -1);
        UniPrintToAll("던전3 의 비밀벽이 열립니다");
    }
}

void DungeonMonster4Death()
{
    int ptr = Dungeon[3] + 1;
    int count = GetDirection(ptr);

    LookWithAngle(ptr, count - 1);
    Delete(GetTrigger() + 1);
    DungeonUnitDeathHandler();
    if (!GetDirection(ptr))
    {
        WallsLineOpen(142, 98, 6, 1);
        UniPrintToAll("던전4 의 비밀벽이 열립니다");
    }
}

void DungeonMonster5Death()
{
    int ptr = Dungeon[4] + 1;
    int count = GetDirection(ptr);

    LookWithAngle(ptr, count - 1);
    DungeonUnitDeathHandler();
    if (!GetDirection(ptr))
    {
        WallsLineOpen(151, 107, 6, 1);
        UniPrintToAll("던전5 의 비밀벽이 열립니다");
    }
}

void DungeonMonster6Death()
{
    int ptr = Dungeon[5] + 1;
    int count = GetDirection(ptr);

    LookWithAngle(ptr, count - 1);
    DungeonUnitDeathHandler();
    if (!GetDirection(ptr))
    {
        WallsLineOpen(169, 125, 6, 1);
        UniPrintToAll("던전6 의 비밀벽이 열립니다");
    }
}

void StartDungeon(int idx)
{
    int dungeons[] = {SpawnDungeon1Monsters, SpawnDungeon2Monsters, SpawnDungeon3Monsters, SpawnDungeon4Monsters, SpawnDungeon5Monsters, SpawnDungeon6Monsters};
    char buff[92];

    Dungeon[idx] = CallFunctionWithArgInt(dungeons[idx], 80);
    int a = idx+1;
    NoxSprintfString(buff, "DunGate%d", &a, 1);
    UnlockDoor(Object(ReadStringAddressEx(buff)));
    NoxSprintfString(buff, "DunGate%d1", &a, 1);
    UnlockDoor(Object(ReadStringAddressEx(buff)));
    NoxSprintfString(buff, "%d번째 던전 출입문이 열렸습니다", &a, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

int SpawnDungeon1Monsters(int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 13);
    CreateObject("InvisibleLightBlueHigh", 13);
    LookWithAngle(unit, count);
    LookWithAngle(unit + 1, count);
    Raise(unit, ToFloat(0));
    FrameTimerWithArg(1, unit, SpawnDungeonMonster);
    return unit;
}

int SpawnDungeon2Monsters(int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 14);
    CreateObject("InvisibleLightBlueHigh", 14);
    LookWithAngle(unit, count);
    LookWithAngle(unit + 1, count);
    Raise(unit, ToFloat(1));
    FrameTimerWithArg(1, unit, SpawnDungeonMonster);
    return unit;
}

int SpawnDungeon3Monsters(int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 15);
    CreateObject("InvisibleLightBlueHigh", 15);
    LookWithAngle(unit, count);
    LookWithAngle(unit + 1, count);
    Raise(unit, ToFloat(2));
    FrameTimerWithArg(1, unit, SpawnDungeonMonster);
    return unit;
}

int SpawnDungeon4Monsters(int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 16);
    CreateObject("InvisibleLightBlueHigh", 16);
    LookWithAngle(unit, count);
    LookWithAngle(unit + 1, count);
    Raise(unit, ToFloat(3));
    FrameTimerWithArg(1, unit, SpawnDungeonMonster);
    return unit;
}

int SpawnDungeon5Monsters(int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 17);
    CreateObject("InvisibleLightBlueHigh", 17);
    LookWithAngle(unit, count);
    LookWithAngle(unit + 1, count);
    Raise(unit, ToFloat(4));
    FrameTimerWithArg(1, unit, SpawnDungeonMonster);
    return unit;
}

int SpawnDungeon6Monsters(int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 18);
    CreateObject("InvisibleLightBlueHigh", 18);
    LookWithAngle(unit, count);
    LookWithAngle(unit + 1, count);
    Raise(unit, ToFloat(5));
    FrameTimerWithArg(1, unit, SpawnDungeonMonster);
    return unit;
}

int DungeonPowerMobFunctions(int type)
{
    int powerMobs[] = {SummonPowerGhost, SummonPowerPurpleGirl, SummonPowerMonster3, 
        SummonGWizard, SummonPowerMonster5, SummonPowerMecaGolem, SummonHecubah, SummonBomber};

    return powerMobs[type];
}

void SpawnDungeonMonster(int ptr)
{
    int count = GetDirection(ptr), type = ToInt(GetObjectZ(ptr)), unit;

    if (count)
    {
        unit = CallFunctionWithArgInt(DungeonPowerMobFunctions(type), ptr);
        SetCallback(unit, 5, DungeonMobDeathFunc() + type);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, SpawnDungeonMonster);
    }
}

void EnableObject(int disUnit)
{
    if (ToInt(GetObjectX(disUnit)))
    {
        if (IsObjectOn(disUnit))
            return;
        else
            ObjectOn(disUnit);
    }
}

void OpenDungenGate()
{
    int key = HasGateKey(OTHER), dun;

    ObjectOff(SELF);
    if (IsObjectOn(key))
    {
        dun = CheckDungeon(OTHER);
        if (dun + 1)
        {
            StartDungeon(dun);
            Delete(key);
        }
    }
    else
    {
        FrameTimerWithArg(70, GetTrigger(), EnableObject);
        UniPrint(OTHER, "[!] 이 던전 게이트를 열려면 리치의 붉은색 열쇠가 필요합니다");
    }
}

void BossMonsterDeath()
{
    int count;

    if ((++count) == BossCnt)
    {
        VictoryEvent();
    }
    DungeonUnitDeathHandler();
}

void StartBossMonsterSummons(int count)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 19);

    LookWithAngle(unit, count);
    FrameTimerWithArg(3, unit, SummonBossMonster);
}

void SummonBossMonster(int sUnit)
{
    int count = GetDirection(sUnit);

    if (count)
    {
        SetCallback(SummonHecubah(sUnit), 5, BossMonsterDeath);
        LookWithAngle(sUnit, count - 1);
        FrameTimerWithArg(1, sUnit, SummonBossMonster);
    }
}

void VictoryEvent()
{
    if (!GOver)
    {
        GOver = 1;
        TeleportAllPlayer(48);
        FrameTimer(30, StrMissionClear);
        UniPrintToAll("승리하셨습니다! 모든 보스 몬스터를 물리쳤습니다!");
    }
}

void ImpShotCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, 0, 40, 1);
            break;
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int ImpMissile(int sOwner, int sTarget, float gap)
{
    int mis = CreateObjectAt("RoundChakramInMotion", GetObjectX(sOwner) + UnitRatioX(sTarget, sOwner, gap), GetObjectY(sOwner) + UnitRatioY(sTarget, sOwner, gap));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, ImpShotCollide);
    SetOwner(sOwner, mis);
    return mis;
}

void SpitImpShot(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), mis;
    float dt = Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(target), GetObjectY(target));
    float vectX = UnitRatioX(target, ptr, dt), vectY = UnitRatioY(target, ptr, dt);

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        mis = ImpMissile(owner, target, 14.0);
        SetOwner(owner, mis);
        UnitSetEnchantTime(mis, 8, 90);
        MoveObject(ptr, GetObjectX(target) + (vectX * GetObjectZ(ptr + 1)), GetObjectY(target) + (vectY * GetObjectZ(ptr + 1)));
        if (IsVisibleTo(ptr, owner))
            PushObject(mis, -35.0, GetObjectX(ptr), GetObjectY(ptr));
        else
            PushObject(mis, -35.0, GetObjectX(target), GetObjectY(target));
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void RhombusPut(int wp, float x_low, float x_high, float y_low, float y_high)
{
    float xf = RandomFloat(y_low, y_high), yf = RandomFloat(0.0, x_high - x_low);

    MoveWaypoint(wp, x_high - y_high + xf - yf, xf + yf);
}

void SplashHandler(int owner, int func, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 1, k;

    SetOwner(owner, ptr - 1);
    Raise(ptr - 1, ToFloat(func));
    for (k = 0 ; k < 8 ; k += 1)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 32);
        SetOwner(ptr - 1, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(ptr - 1, 2);
}

void UnitVisibleSplash()
{
    int parent;

    if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    {
        parent = GetOwner(SELF);
        if (CurrentHealth(GetOwner(parent)))
        {
            if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
            {
                Enchant(OTHER, "ENCHANT_VILLAIN", 0.1);
                CallFunction(ToInt(GetObjectZ(parent)));
            }
        }
    }
}


int GetUnitDeathFunc(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        return GetMemory(GetMemory(ptr + 0x2ec) + 0x4f4);
    }
    return 0;
}

void MovingBackEntranceRow(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 92)
    {
        MoveObject(ptr, GetObjectX(ptr) + 2.0, GetObjectY(ptr) - 2.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) + 2.0, GetObjectY(ptr + 1) - 2.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + 2.0, GetObjectY(ptr + 2) - 2.0);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) - 2.0, GetObjectY(ptr + 3) + 2.0);
        MoveObject(ptr + 4, GetObjectX(ptr + 4) - 2.0, GetObjectY(ptr + 4) + 2.0);
        MoveObject(ptr + 5, GetObjectX(ptr + 5) - 2.0, GetObjectY(ptr + 5) + 2.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, MovingBackEntranceRow);
    }
    else
    {
        WallOpen(Wall(145, 185));
        WallOpen(Wall(144, 186));
        WallOpen(Wall(137, 193));
        WallOpen(Wall(136, 194));
    }
}

void OpenCastleBackEntrance()
{
    ObjectOff(SELF);
    MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("CreatureCageAppears", 1);
    AudioEvent("BoulderMove", 1);
    AudioEvent("ChainPull", 1);
    ObjectOn(Object("CastleGear1"));
    ObjectOn(Object("CastleGear2"));
    ObjectOn(Object("CastleGear3"));
    WallOpen(Wall(130, 202));
    WallOpen(Wall(132, 204));
    WallOpen(Wall(153, 179));
    FrameTimerWithArg(12, BackRow(), MovingBackEntranceRow);
    Enchant(CreateObjectAt("RedOrbKeyOfTheLich", GetObjectX(OTHER), GetObjectY(OTHER)), "ENCHANT_FREEZE", 0.0);
    UniPrint(OTHER, "후문 출입구가 열립니다");
}

int HorrendousBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 400; arr[18] = 400; 
		arr[19] = 95; arr[23] = 32768; arr[24] = 1065437102; arr[25] = 1; arr[26] = 9; 
		arr[27] = 5; arr[28] = 1137180672; arr[31] = 17; arr[54] = 4; arr[59] = 5542784; 
		arr[60] = 1386; arr[61] = 46907648; 
        CustomMeleeAttackCode(arr,HorrendousMissileAttack);
        link = arr;
	}
	return link;
}

void HorrendousSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077306982);
		SetMemory(ptr + 0x224, 1077306982);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 400);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HorrendousBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 0);
	}
}

void HorrendousHarpoonCollide()
{
    int owner = GetOwner(SELF);

    while (IsObjectOn(SELF))
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 12, 1);
            Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int HorrendousHarpoon(int sUnit, int sTarget)
{
    int mis = CreateObjectAt("HarpoonBolt", GetObjectX(sUnit) + UnitRatioX(sTarget, sUnit, 16.0), GetObjectY(sUnit) + UnitRatioY(sTarget, sUnit, 16.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, HorrendousHarpoonCollide);
    SetOwner(sUnit, mis);
    LookAtObject(mis, sTarget);
    return mis;
}

void GuardianHitShuriken(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), mis;
    float dt = Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(target), GetObjectY(target));
    float vectX = UnitRatioX(target, ptr, dt), vectY = UnitRatioY(target, ptr, dt);
    float thresHold;

    if (CurrentHealth(owner) && CurrentHealth(target) && IsObjectOn(owner))
    {
        mis = HorrendousHarpoon(owner, target);
        thresHold = DistanceUnitToUnit(mis, target) / GetObjectZ(ptr + 1);
        MoveObject(ptr, GetObjectX(target) + UnitRatioX(target, ptr, dt * thresHold), GetObjectY(target) + UnitRatioY(target, ptr, dt * thresHold));
        if (IsVisibleTo(ptr, owner))
            PushObject(mis, -42.0, GetObjectX(ptr), GetObjectY(ptr));
        else
            PushObject(mis, -42.0, GetObjectX(target), GetObjectY(target));
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void HorrendousMissileAttack()
{
    if (!GetCaller()) return;

    AbsoluteTargetStrike(GetTrigger(), GetCaller(), 72.0, GuardianHitShuriken);
}

int SummonHorrendous(float xProfile, float yProfile)
{
    int unit = CreateObjectAt("Horrendous", xProfile, yProfile);

    HorrendousSubProcess(unit);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    SetCallback(unit, 7, FieldMonsterHurt);
    return unit;
}

void RespectThreeHorrendous(int sUnit)
{
    int enemy = ToInt(GetObjectZ(sUnit)), cre;

    if (IsObjectOn(sUnit))
    {
        if (!GetDirection(sUnit))
        {
            cre = SummonHorrendous(GetObjectX(sUnit), GetObjectY(sUnit));
            LookAtObject(cre, enemy);
            Raise(sUnit + 1, enemy);
            FrameTimerWithArg(1, sUnit + 1, RespectThreeHorrendous);
            DeleteObjectTimer(CreateObjectAt("OldSmoke", GetObjectX(cre), GetObjectY(cre)), 15);
            UniChatMessage(cre, "너의 그 자존심! 내가 박살내주지", 150);
        }
        Delete(sUnit);
    }
}

void RespectSixHorrendous()
{
    ObjectOff(SELF);

    FrameTimerWithArg(5, CreateObject("InvisibleLightBlueLow", 69), RespectThreeHorrendous);
    Raise(CreateObject("InvisibleLightBlueLow", 70) - 1, GetCaller());
    CreateObject("InvisibleLightBlueLow", 71);
    LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF)), 1);
    FrameTimerWithArg(5, CreateObject("InvisibleLightBlueLow", 72), RespectThreeHorrendous);
    Raise(CreateObject("InvisibleLightBlueLow", 73) - 1, GetCaller());
    CreateObject("InvisibleLightBlueLow", 74);
    LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF)), 1);
}











void RemoveEntranceFence(int sUnit)
{
    int i, count = GetDirection(sUnit);

    for (i = 0 ; i < count ; i ++)
        WallUtilOpenWallAtObjectPosition(sUnit + i);
}

void RemoveDungeonEntranceWalls()
{
    int unit = CreateObject("ImaginaryCaster", 88), i;

    ObjectOff(SELF);
    FrameTimerWithArg(3, unit, RemoveEntranceFence);
    LookWithAngle(unit, 6);
    for (i = 0 ; i < 5 ; i ++)
        CreateObjectAt("ImaginaryCaster", GetObjectX(unit + i) - 23.0, GetObjectY(unit + i) + 23.0);
    
    UniPrint(OTHER, "던전 입구 앞 철조망을 걷어내었습니다. 행운을 빕니다 후보생!");
}



// void MonsterStrikeCallback()
// {
//     int thingId = GetUnitThingID(SELF);

//     if (CurrentHealth(OTHER))
//     {
//         // if (thingId == 1386)
//         //     HorrendousMissileAttack();
//         // else if (thingId == 1360)
//         //     ZombieAttackReportComplete();
//         else if (thingId == 2303)
//             BullDakStrike();
//     }
// }

int BullDakBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50; arr[17] = 430; arr[19] = 200; arr[21] = 1065353216; 
		arr[23] = 32769; arr[24] = 1065353216; arr[28] = 1117782016; arr[29] = 10; arr[31] = 4; 
		arr[32] = 18; arr[33] = 26; arr[34] = 5; arr[35] = 5; arr[36] = 50; 
		arr[58] = 5545472; arr[59] = 5542784; 
        CustomMeleeAttackCode(arr,BullDakStrike);
		link = arr;
	}
	return link;
}

void BullDakSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1086324736);
		SetMemory(ptr + 0x224, 1086324736);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32769);
		SetMemory(GetMemory(ptr + 0x22c), 430);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 430);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BullDakBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void BullDakStrike()
{
    if (!GetCaller()) return;

    Damage(OTHER, SELF, 160, 8);
    DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 18);
}

void BullDakImageLoop(int imgUnit)
{
    int parent = imgUnit - 1;
    if (CurrentHealth(parent))
    {
        if (ToInt(DistanceUnitToUnit(imgUnit, parent)))
            MoveObject(imgUnit, GetObjectX(parent), GetObjectY(parent));
        FrameTimerWithArg(2, imgUnit, BullDakImageLoop);
    }
    else
        Delete(imgUnit);
}

void UnderGroundElevOn()
{
    ObjectOff(SELF);
    ObjectOn(Object("UndergroundElev"));
    UniPrint(OTHER, "엘리베이터가 작동을 시작했습니다!");
}

void SecretWallOpen()
{
    ObjectOff(SELF);
    WallOpen(Wall(118, 204));
    WallOpen(Wall(119, 203));
    WallOpen(Wall(120, 202));

    WallOpen(Wall(122, 200));
    WallOpen(Wall(123, 199));
    WallOpen(Wall(124, 198));

    WallOpen(Wall(126, 198));
    WallOpen(Wall(127, 199));
    WallOpen(Wall(128, 200));
}

#define HASH_OWNER 2
#define HASH_LIST 3

void onSensorDetected()
{
    int hash=GetUnit1C(SELF), owner = GetOwner(SELF);

    if (!HashGet(hash, GetCaller(), NULLPTR, FALSE))
    {
        HashPushback(hash, GetCaller(), TRUE);
        int list=0;

        HashGet(hash, HASH_LIST, &list, FALSE);
        if (list)
            LinkedListPushback(list, GetCaller(), 0);
    }
}

int findoutNearlyEnemy(int list, int baseUnit)
{
    int node=0, unit, ret = 0;
    float tempDist=9999.0;

    if (LinkedListFront(list, &node))
    {
        while (node)
        {
            unit = LinkedListGetValue(node);

            if (CurrentHealth(unit))
            {
                if (DistanceUnitToUnit(baseUnit, unit) < tempDist)
                {
                    tempDist=DistanceUnitToUnit(baseUnit, unit);
                    ret = unit;
                }
            }
            LinkedListNext(node, &node);
        }
    }
    return ret;
}

void onThrowingPotionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 150, DAMAGE_TYPE_EXPLOSION);
			PushObjectTo(OTHER,UnitRatioX(SELF,OTHER,21.0),UnitRatioY(SELF,OTHER,21.0));
        }
    }
}

void endThrowingPotion(int potionfx)
{
    float xpos=GetObjectX(potionfx), ypos= GetObjectY(potionfx);
    int owner = GetOwner(potionfx);
    int pot = CreateObjectAt("BreakingPotion", xpos, ypos);
    
    SplashDamageAtEx(owner, xpos, ypos, 80.0, onThrowingPotionSplash);
    PlaySoundAround(pot, SOUND_PotionBreak);
    DeleteObjectTimer(CreateObjectAt("ThinFireBoom", xpos, ypos), 9);
}

void throwingPotionLoop(int subunit)
{
    while (IsObjectOn(subunit))
    {
        int owner = GetOwner(subunit + 2), durate = GetDirection(subunit);

        if (CurrentHealth(owner))
        {
            if (durate)
            {
                FrameTimerWithArg(1, subunit, throwingPotionLoop);
                MoveObjectVector(subunit + 2, GetObjectZ(subunit), GetObjectZ(subunit + 1));
                
                int angle = GetDirection(subunit + 2);
                Raise(subunit + 2, MathSine(angle * 15, 240.0));
                LookWithAngle(subunit + 2, ++angle);
                LookWithAngle(subunit, --durate);
                break;
            }
            else
            {
                endThrowingPotion(subunit + 2);
            }
            
        }
        Delete(subunit++);
        Delete(subunit++);
        Delete(subunit++);
        break;
    }
}

void throwingStuffImpl(int caster, int target)
{
    float cdist = DistanceUnitToUnit(caster, target) / 12.0;
    float xvect = UnitRatioX(target, caster, cdist), yvect = UnitRatioY(target, caster, cdist);
    int subunit = CreateObjectAt("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster));

    Raise(CreateObjectAt("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster)), yvect);
    UnitNoCollide(CreateObjectAt("BottleCandleUnlit", GetObjectX(subunit), GetObjectY(subunit)));
    Raise(subunit, xvect);
    SetOwner(caster, subunit + 2);
    LookWithAngle(subunit, 12);
    FrameTimerWithArg(1, subunit, throwingPotionLoop);
}

void afterHashDetected(int hash)
{
    int owner, list, targ=0;
    // char logbuff[64];

    HashGet(hash, HASH_OWNER, &owner, TRUE);
    HashGet(hash, HASH_LIST, &list, TRUE);

    // NoxSprintfString(&logbuff, "2afterhashdetected-unit-%d", &owner, 1);
    // WriteLog(ReadStringAddressEx(&logbuff));

    if (CurrentHealth(owner))
    {
        targ= findoutNearlyEnemy(list, owner);

        if (targ)
            throwingStuffImpl(owner, targ);
    }
    LinkedListDeleteInstance(list);
    HashDeleteInstance(hash);

}

void OnTrackedFirespriteMissile(int cur, int owner)
{
    int sensor = CreateObjectAt("WeirdlingBeast", GetObjectX(owner), GetObjectY(owner));
    int hash,list;

    HashCreateInstance(&hash);
    LinkedListCreateInstance(&list);
    HashPushback(hash, HASH_OWNER, owner);
    HashPushback(hash, HASH_LIST, list);
    SetUnit1C(sensor, hash);
    LookAtObject(sensor, cur);
    UnitNoCollide(sensor);
    UnitSetEnchantTime(sensor, ENCHANT_INVULNERABLE, 0);
    DeleteObjectTimer(sensor, 1);
    FrameTimerWithArg(2, hash, afterHashDetected);
    SetUnitScanRange(sensor, 600.0);
    SetOwner(owner, sensor);
    SetCallback(sensor, 3, onSensorDetected);
    Delete(cur);
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 697, OnTrackedFirespriteMissile);    //PitifulFireball
}

static void OnPlayerEntryMap(int pInfo)
{
    OnPlayerEntryMapProc(pInfo);
}
