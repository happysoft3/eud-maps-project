

#include "libs/define.h"
#include "libs/itemproperty.h"
#include "libs/unitstruct.h"
#include "libs/weaponcapacity.h"
#include "libs/potionex.h"
#include "libs/hash.h"
#include "libs/buff.h"
#include "libs/potionpickup.h"
#include "libs/printutil.h"
#include "libs/sound_define.h"
#include "libs/waypoint.h"
#include "libs/queueTimer.h"
#include "libs/fxeffect.h"
#include "libs/bind.h"
#include "libs/objectIDdefines.h"

#define PLAYER_SAFE_LOCATION 12

int yellowPotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateYellowPotion(GetObjectX(del), GetObjectY(del), 125);
    Delete(del);
    return pot;
}
int blackPotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateBlackPotion(GetObjectX(del), GetObjectY(del), 85);
    Delete(del);
    return pot;
}
int whitePotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateWhitePotion(GetObjectX(del), GetObjectY(del), 100);
    Delete(del);
    return pot;
}
void onGodPotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_INVULNERABLE), 25.0);
}
int godPotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onGodPotionUse);
    return pot;
}

void onHastePotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_HASTED), 0.0);
}

int hastePotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onHastePotionUse);
    return pot;
}

void onVampPotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_VAMPIRISM), 0.0);
}

int vampPotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onVampPotionUse);
    return pot;
}
void onTeleportAmuletUse()
{
    int pIndex =GetPlayerIndex(OTHER);

    if (pIndex<0)
        return;

    Delete(SELF);
    Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
    MoveObject(OTHER, LocationX(PLAYER_SAFE_LOCATION),LocationY(PLAYER_SAFE_LOCATION));
    Effect("TELEPORT",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
    PlaySoundAround(OTHER,SOUND_BlindOff);
    UniPrint(OTHER, "공간이동 목걸이를 사용하여, 안전한 장소로 이동을 했습니다");
}

int teleportAmulet(int pot)
{
    SetUnitCallbackOnUseItem(pot,onTeleportAmuletUse);
    SetItemPropertyAllowAllDrop(pot);
    return pot;
}

void onHealingBuff(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int owner= GetOwner(sub);
        if (CurrentHealth(owner))
        {
            int dur=GetDirection(sub);
            if (dur)
            {
                if (ToInt(DistanceUnitToUnit(sub, owner)))
                    MoveObject(sub, GetObjectX(owner), GetObjectY(owner));
                LookWithAngle(sub, dur-1);
                RestoreHealth(owner, 4);
                PushTimerQueue(3, sub, onHealingBuff);
                return;
            }
        }
        Delete(sub);
    }
}

void startHealingBuff(int owner)
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(owner), GetObjectY(owner));

    LookWithAngle(sub, 80);
    SetOwner(owner, sub);
    SetUnitEnchantCopy(sub, GetLShift(ENCHANT_RUN));
    PushTimerQueue(1, sub, onHealingBuff);
}

void onHealingPotionUse()
{
    Delete(SELF);
    startHealingBuff(GetCaller());
}

int healingAmulet(int pot)
{
    SetUnitCallbackOnUseItem(pot,onHealingPotionUse);
    return pot;
}

void onFearUse()
{
    Delete(SELF);
    RestoreHealth(OTHER, 999);
    Enchant(OTHER, "ENCHANT_SHOCK", 120.0);
}

int potionFear(int pot)
{
    SetUnitCallbackOnUseItem(pot,onFearUse);
    return pot;
}

void onAmuletofManipulationUse()
{
    Delete(SELF);
    CastSpellObjectObject("SPELL_METEOR_SHOWER",OTHER,OTHER);
    UniPrint(OTHER,"자신 주변에 운석 소나기를 내립니다");
}

int potionAmuletOfManipulation(int pot)
{
    SetUnitCallbackOnUseItem(pot,onAmuletofManipulationUse);
    return pot;
}

void queryPotionHash(int setHash, int *getHash)
{
    int hash;

    if (setHash)
        hash=setHash;
    if (getHash)
        getHash[0]=hash;
}

void FillElemPotionHash(int *pHash)
{
    HashPushback(pHash, OBJ_YELLOW_POTION, yellowPotionHash);
    HashPushback(pHash, OBJ_WHITE_POTION, whitePotionHash);
    HashPushback(pHash, OBJ_BLACK_POTION, blackPotionHash);
    HashPushback(pHash, OBJ_INVULNERABILITY_POTION, godPotionHash);
    HashPushback(pHash, OBJ_HASTE_POTION, hastePotionHash);
    HashPushback(pHash, OBJ_VAMPIRISM_POTION, vampPotionHash);
    HashPushback(pHash, OBJ_AMULET_OF_TELEPORTATION, teleportAmulet);
    HashPushback(pHash, OBJ_AMULETOF_NATURE, healingAmulet);
    HashPushback(pHash, OBJ_FEAR, potionFear);
    HashPushback(pHash, OBJ_AMULETOF_MANIPULATION, potionAmuletOfManipulation);
}

int ItemDataWeaponNameTable() { int *p, count; return &p; }
int ItemDataArmorNameTable() { int *p, count; return &p; }
int ItemDataPotionNameTable() { int *p, count; return &p; }
int ItemDataStaffNameTable() { int *p, count; return &p; }
int ItemDataCreateItemTable() { int *p, count; return &p; }

int invokeNameTable(int fn)
{
    return Bind(fn, &fn + 4);
}

void QueryNameTable(int tableFn, int *setTo, int **get, int setCount, int *getCount)
{
    int *p = invokeNameTable(tableFn);

    if (setTo)
        p[0]=setTo;
    if (get)
        get[0]=p[0];
    if (setCount)
        p[1]=setCount;
    if (getCount)
        getCount[0]=p[1];
}

void InitItemNames()
{
    int weapons[]={OBJ_WAR_HAMMER, OBJ_GREAT_SWORD, OBJ_SWORD, OBJ_LONGSWORD, OBJ_STAFF_WOODEN,
    OBJ_OGRE_AXE, OBJ_BATTLE_AXE, OBJ_FAN_CHAKRAM, OBJ_ROUND_CHAKRAM,OBJ_MORNING_STAR,
    OBJ_OBLIVION_HALBERD,OBJ_OBLIVION_HEART,OBJ_OBLIVION_WIERDLING,
    OBJ_CROSS_BOW,OBJ_BOW,OBJ_QUIVER,};
    int armors[]={OBJ_ORNATE_HELM, OBJ_BREASTPLATE, OBJ_PLATE_ARMS, OBJ_PLATE_BOOTS,
    OBJ_PLATE_LEGGINGS, OBJ_CHAIN_COIF, OBJ_CHAIN_LEGGINGS, OBJ_CHAIN_TUNIC, OBJ_STEEL_SHIELD, OBJ_WOODEN_SHIELD,
    OBJ_LEATHER_ARMBANDS, OBJ_LEATHER_ARMOR, OBJ_LEATHER_ARMORED_BOOTS, OBJ_LEATHER_BOOTS, OBJ_LEATHER_HELM, OBJ_LEATHER_LEGGINGS,
    OBJ_MEDIEVAL_CLOAK, OBJ_MEDIEVAL_PANTS, OBJ_MEDIEVAL_SHIRT, OBJ_STREET_SHIRT,OBJ_STREET_SNEAKERS,OBJ_STREET_PANTS,};
    int pots[]={
        OBJ_RED_POTION,OBJ_RED_POTION,OBJ_CURE_POISON_POTION,OBJ_VAMPIRISM_POTION,OBJ_RED_POTION,
        OBJ_SHIELD_POTION,OBJ_INVISIBILITY_POTION,OBJ_INVULNERABILITY_POTION,OBJ_CURE_POISON_POTION,
        OBJ_HASTE_POTION,OBJ_FIRE_PROTECT_POTION,OBJ_SHOCK_PROTECT_POTION,OBJ_POISON_PROTECT_POTION,OBJ_YELLOW_POTION,
        OBJ_BLACK_POTION,OBJ_CURE_POISON_POTION,OBJ_AMULET_OF_TELEPORTATION,OBJ_AMULETOF_NATURE,OBJ_FEAR,OBJ_AMULETOF_MANIPULATION,
    };
    int wands[]={OBJ_DEATH_RAY_WAND, OBJ_LESSER_FIREBALL_WAND, OBJ_FIRE_STORM_WAND, OBJ_FORCE_WAND,
    OBJ_INFINITE_PAIN_WAND,OBJ_SULPHOROUS_FLARE_WAND,OBJ_SULPHOROUS_SHOWER_WAND, };

    QueryNameTable(ItemDataWeaponNameTable, weapons, NULLPTR, sizeof(weapons), NULLPTR);
    QueryNameTable(ItemDataArmorNameTable, armors, NULLPTR, sizeof(armors), NULLPTR);
    QueryNameTable(ItemDataPotionNameTable, pots, NULLPTR, sizeof(pots), NULLPTR);
    QueryNameTable(ItemDataStaffNameTable, wands, NULLPTR, sizeof(wands), NULLPTR);
    int items[]={ CreateHotPotion, CreateHotPotion2, CreateCoinBetter, CreateGerm,
        CreateMagicalStaff, CreateRandomArmor, CreateRandomPotion, CreateRandomWeapon};
    QueryNameTable(ItemDataCreateItemTable, items, NULLPTR, sizeof(items), NULLPTR);
}

void CreateRandomWeapon(float xpos, float ypos, int *pDest)
{
    int *p;
    int count;
    QueryNameTable(ItemDataWeaponNameTable, NULLPTR, &p, 0, &count);
    int weapon = CreateObjectById(p[Random(0, count-1)], xpos, ypos);

    if (pDest)
        pDest[0] = weapon;

    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    
    int thingId = GetUnitThingID(weapon);

    if (thingId>=OBJ_OBLIVION_HALBERD&&thingId<=OBJ_OBLIVION_ORB)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId==OBJ_FAN_CHAKRAM||OBJ_QUIVER==thingId)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
}

void CreateRandomArmor(float xpos, float ypos, int *pDest)
{
    int *p;
    int count;
    QueryNameTable(ItemDataArmorNameTable, NULLPTR, &p, 0, &count);
    int armor = CreateObjectById(p[Random(0,count-1)], xpos, ypos);

    if (pDest)
        pDest[0] = armor;

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
}

void CreateCoin(float xpos, float ypos, int *pDest)
{
    int coin = CreateObjectAt("QuestGoldChest", xpos, ypos);

    if (pDest)
        pDest[0] = coin;
    UnitStructSetGoldAmount(coin, Random(500, 2000));
}

void CreateCoinBetter(float xpos, float ypos, int *pDest)
{
    int coin = CreateObjectAt("QuestGoldChest", xpos, ypos);

    if (pDest)
        pDest[0] = coin;
    UnitStructSetGoldAmount(coin, Random(1500, 5000));
}

void CreateGerm(float xpos, float ypos, int *pDest)
{
    string germs[] = {"Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Emerald", "Diamond"};
    int pic = CreateObjectAt(germs[Random(0, sizeof(germs) - 1)], xpos, ypos);

    if (pDest)
        pDest[0] = pic;
}

int potionHashProto(int functionId, int pot)
{
    return Bind(functionId, &functionId+4);
}

void CreateRandomPotion(float xpos, float ypos, int *pDest)
{
    int *p;
    int count;
    QueryNameTable(ItemDataPotionNameTable, NULLPTR, &p, 0, &count);
    int potion = CreateObjectById(p[Random(0,count-1)], xpos, ypos);

    // CheckPotionThingID(potion);
    int fn, hash;

    queryPotionHash(0, &hash);
    if (HashGet(hash, GetUnitThingID(potion), &fn, FALSE))
        potion= potionHashProto(fn, potion);
    PotionPickupRegist(potion);
    if (pDest)
        pDest[0] = potion;
    if (MaxHealth(potion))
        SetUnitMaxHealth(potion, 0);
}

void CreateHotPotion(float xpos, float ypos, int *pDest)
{
    int potion = CreateObjectAt("RedPotion", xpos, ypos);

    PotionPickupRegist(potion);
    if (pDest)
        pDest[0] = potion;
}

void CreateHotPotion2(float x,float y, int*p)
{
    string n[]={"RedPotion","CurePoisonPotion"};
    int pot=CreateObjectAt(n[Random(0,1)], x,y);

    PotionPickupRegist(pot);
    if (p)
        p[0]=pot;
}

void CreateMagicalStaff(float xpos, float ypos, int *pDest)
{
    int *p;
    int count;
    QueryNameTable(ItemDataStaffNameTable, NULLPTR, &p, 0, &count);
    int s= CreateObjectById(p[Random(0,count-1)], xpos, ypos);
    if(pDest)
     pDest[0]=s;
}

void CreateRandomItemProto(int functionId, float xpos, float ypos, int *ptr)
{
    Bind(functionId, &functionId+4);
}

void postItemCommon(int getitem)
{
    if (getitem)
    {
        if (GetUnitFlags(getitem) & UNIT_FLAG_NO_COLLIDE)
            UnitNoCollide(getitem);
        int durability=MaxHealth(getitem);

        if (durability)
        {
            durability*=4;
            if (durability<5000)
                SetUnitMaxHealth(getitem, durability);
        }
    }
}

void CreateRandomItemCommon(int posUnit)
{
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    Delete(posUnit);

    int getitem = 0, *p, count;

    QueryNameTable(ItemDataCreateItemTable, NULLPTR, &p, 0, &count);
    CreateRandomItemProto(p[ Random(0, count-1)], xpos, ypos, &getitem);
    postItemCommon(getitem);
}

void InitializeRewardData()
{
    InitItemNames();
    int hash;
    HashCreateInstance(&hash);
    queryPotionHash(hash, NULLPTR);
    FillElemPotionHash(hash);
}

// void NOXLibraryEntryPointFunction()
// {
//     "export needinit InitItemNames";
// }

