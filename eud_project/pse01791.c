
#include "libs\define.h"
#include "libs\printutil.h"
#include "libs\waypoint.h"
#include "libs\unitstruct.h"
#include "libs\username.h"
#include "libs\coopteam.h"
#include "libs\observer.h"
#include "libs\reaction.h"

#include "libs\mathlab.h"

#define PLAYER_FLAG_DEATH 0x80000000

#define LADDER_BOARD_UPDATE_TIMERATE 30

int g_player[20];
int g_creunit[10];
int g_crelevel[10];
int g_ladderUnit;

int DummyUnitCreateAt(string unitname, float xpos, float ypos)
{
    int dum = CreateObjectAt(unitname, xpos, ypos);

    if (!dum)
        return 0;

    Damage(dum, 0, MaxHealth(dum) + 1, -1);
    ObjectOff(dum);
    Frozen(dum, TRUE);
    return dum;
}

void FlagUnitPickupEventNotWorking()
{
    UniPrint(OTHER, "어이 김씨, 뭐하고 있어! 꾸물거리지 말고 어서 일해~");
}

int CheckPlayer()
{
    int u;

    for (u = 0 ; u < 10 ; u += 1)
    {
        if (IsCaller(g_player[u]))
            return u;
    }
    return -1;
}

int WorkerInfoBeginner(int locationId)
{
    int worker = CreateObjectAt("Wolf", LocationX(locationId), LocationY(locationId));

    UnitLinkBinScript(worker, 0);
    return worker;
}

string WorkerClassDescription(int workerId)
{
    string desc[] =
    {
        "저는 신입이지롱~", "입으로 물어뜯기", "칼로 베기", "도끼로 찍기", "턱으로 내려치기",
            "불로 태우기", "전기로 지지기", "돌주먹", "메테오", "마법 부리기"
    };

    return desc[workerId];
}

string WorkerClassName(int workerId)
{
    string workers[] = {"GreenFrog", "Wolf", "Swordsman", "GruntAxe", "Mimic",
        "EmberDemon", "WillOWisp", "StoneGolem", "UrchinShaman", "Wizard"};

    return workers[workerId];
}

int WorkerClassHealth(int workerId)
{
    int hptable[] = {10, 50, 75, 110, 160,
        200, 220, 240, 275, 280};

    return hptable[workerId];
}

float WorkerClassStepPoint(int workerId)
{
    float steps[] = {2.6, 3.0, 3.0, 3.0, 3.7,
        4.0, 4.0, 4.0, 5.0, 5.0};

    return steps[workerId];
}

int WorkerClassSetParam(int creType, int owner, int locationId)
{
    return &creType;
}

int WorkerClassCreate(int *params)
{
    int locationId = params[2], creType = params[0];
    int owner = params[1];

    if (!CurrentHealth(owner))
        return 0;

    int cre = CreateObjectAt(WorkerClassName(creType), LocationX(locationId), LocationY(locationId));

    SetOwner(owner, cre);
    SetUnitMaxHealth(cre, WorkerClassHealth(creType));
    UnitZeroFleeRange(cre);
    AggressionLevel(cre, 0.0);
    RetreatLevel(cre, 0.0);
    ResumeLevel(cre, 0.0);
    return cre;
}

void PlayerClassRemoveWorker(int plr)
{
    if (MaxHealth(g_creunit[plr]))
        Delete(g_creunit[plr]);
    g_crelevel[plr] = 0;
}

int PlayerClassRespawnCreature(int plr)
{
    int pUnit = g_player[plr];

    if (CurrentHealth(g_creunit[plr]))
        return g_creunit[plr];

    int cre = WorkerClassCreate(WorkerClassSetParam(g_crelevel[plr], pUnit, 13));

    g_creunit[plr] = cre;
    return cre;
}

int PlayerClassGetMouseCursor(int plr)
{
    int cursor[10];

    if (!IsObjectOn(cursor[plr]))
    {
        cursor[plr] = CreateObjectAt("Moonglow", GetObjectX(g_player[plr]), GetObjectY(g_player[plr]));

        SetOwner(g_player[plr], cursor[plr]);
    }
    return cursor[plr];
}

int PlayerClassDeathFlagCheck(int plr)
{
    return g_player[plr + 10] & PLAYER_FLAG_DEATH;
}

void PlayerClassDeathFlagSet(int plr)
{
    g_player[plr + 10] ^= PLAYER_FLAG_DEATH;
}

void PlayerClassOnFailed(int pUnit)
{
    MoveObject(pUnit, LocationX(12), LocationY(12));
    UniPrint(pUnit, "맵 입장에 실패하였습니다");
}

void PlayerClassOnJoin(int plr, int pUnit)
{
    PlayerClassRespawnCreature(plr);
    Enchant(pUnit, "ENCHANT_INVULNERABLE", 0.0);
    MoveObject(pUnit, LocationX(11), LocationY(11));
}

int PlayerClassOnInit(int plr, int pUnit)
{
    g_player[plr] = pUnit;
    g_player[plr + 10] = 1;

    g_crelevel[plr] = 0;

    ObserverPlayerCameraLock(pUnit);

    return plr;
}

void PlayerClassEntry()
{
    while (true)
    {
        if (CurrentHealth(other))
        {
            int plr = CheckPlayer(), u;

            for (u = 0 ; u < 10 ; Nop(++u))
            {
                if (!MaxHealth(g_player[u]))
                {
                    plr = PlayerClassOnInit(u, GetCaller());
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerClassOnJoin(plr, GetCaller());
                break;
            }
        }
        PlayerClassOnFailed(other);
        break;
    }
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(g_player[plr]) + " 님께서 적에게 격추되었습니다");
}

void PlayerWorkerMoving(int plr)
{
    int cursor = PlayerClassGetMouseCursor(plr);
    int cre = g_creunit[plr];

    if (DistanceUnitToUnit(cursor, cre) > 10.0)
    {
        LookAtObject(cre, cursor);
        Walk(cre, GetObjectX(cursor), GetObjectY(cursor));

        float stepp = WorkerClassStepPoint(g_crelevel[plr]);

        MoveObjectVector(cre, UnitRatioX(cursor, cre, stepp), UnitRatioY(cursor, cre, stepp));
    }
}

void PlayerWorkerStop(int plr)
{
    int cre = g_creunit[plr];

    CreatureIdle(cre);
}

void PlayerWorkerStrike(int plr)
{
    int pUnit = g_player[plr];

    UniPrint(pUnit, "공격명령 입니다");
}

void PlayerClassOnCreatCtrl(int plr, int pUnit)
{
    int input = CheckPlayerInput(pUnit);

    //Todo. 이동, 공격 등을 여기에서 정의합니다
    if (input == 2) //Move
    {
        PlayerWorkerMoving(plr);
    }
    else    //Stop
    {
        PlayerWorkerStop(plr);
        if (input == 6)
        {
            PlayerWorkerStrike(plr);
        }
    }
    
}

int PlayerClassPickUnitCreate(int cursor, int plr)
{
    int owner = g_player[plr];
    int picker = DummyUnitCreateAt("Necromancer", GetObjectX(cursor), GetObjectY(cursor));

    SetUnitMass(picker, 9999.0);
    SetUnitFlags(picker, GetUnitFlags(picker) ^ 0x2000);
    SetOwner(owner, picker);
    DeleteObjectTimer(picker, 1);
    return picker;
}

void PlayerClassOnCreatClick(int plr, int creat)
{
    int cursor = PlayerClassGetMouseCursor(plr);

    if (IsVisibleTo(cursor, creat) || IsVisibleTo(creat, cursor))
    {
        if (DistanceUnitToUnit(creat, cursor) < 100.0)
        {
            int pick = PlayerClassPickUnitCreate(cursor, plr);
            DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(cursor), GetObjectY(cursor)), 9);
        }
    }
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    int cre = g_creunit[plr];

    if (MaxHealth(cre))
    {
        if (CheckWatchFocus(pUnit))
        {
            PlayerLook(pUnit, cre);
            PlayerClassOnCreatClick(plr, cre);
        }
        if (CurrentHealth(cre))
            PlayerClassOnCreatCtrl(plr, pUnit);
    }
}

void PlayerClassOnExit(int plr)
{
    PlayerClassRemoveWorker(plr);

    g_player[plr] = 0;
    g_player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int u;

    for (u = 0 ; u < 10 ; Nop(++u))
    {
        while (true)
        {
            if (MaxHealth(g_player[u]))
            {
                if (GetUnitFlags(g_player[u]) & 0x40)
                    1;
                else if (CurrentHealth(g_player[u]))
                {
                    PlayerClassOnAlive(u, g_player[u]);
                    break;
                }
                else
                {
                    if (!PlayerClassDeathFlagCheck(u))
                    {
                        PlayerClassDeathFlagSet(u);
                        PlayerClassOnDeath(u);
                    }
                    break;
                }
                
            }
            if (g_player[u + 10])
                PlayerClassOnExit(u);
            break;
        }
    }

    FrameTimer(1, PlayerClassOnLoop);
}

void LadderBoardWork()
{
    if (!IsCaller(GetTrigger() + 1))
        return;
    
    if (GetDirection(self) < LADDER_BOARD_UPDATE_TIMERATE)
    {
        LookWithAngle(self, GetDirection(self) + 1);
        return;
    }
    string content = "유저 스코어 현황\n";
    LookWithAngle(self, 0);
}

void InitializeLadder()
{
    int master = CreateObjectAt("Hecubah", 5500.0, 100.0);

    Frozen(CreateObjectAt("BlackPowder", GetObjectX(master), GetObjectY(master)) - 1, true);
    SetCallback(master, 9, LadderBoardWork);
    g_ladderUnit = master;
}

void InitializePlaceMapDecorations()
{
    int flagunit = CreateObjectAt("GreenFlag", LocationX(14), LocationY(14));

    SetUnitCallbackOnPickup(flagunit, FlagUnitPickupEventNotWorking);
    UnitNoCollide(flagunit);
}

int PlaceShopItemSingle(int locationId, string unitname)
{
    int item = CreateObjectAt(unitname, LocationX(locationId), LocationY(locationId));

    if (item == 0)
        return 0;

    ObjectOff(item);
    Frozen(item, true);
    return item;
}

void PlaceShopItems()
{
    int u, center = CreateObjectAt("ImaginaryCaster", LocationX(25), LocationY(25));

    for (u = 0 ; u < 9 ; Nop(++u))
        LookAtObject(PlaceShopItemSingle(15 + u, WorkerClassName(u + 1)), center);
}

void MapInitialize()
{
    MusicEvent();
    FrameTimer(10, PlayerClassOnLoop);

    FrameTimer(1, MakeCoopTeam);

    InitializePlaceMapDecorations();
    FrameTimer(2, PlaceShopItems);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
}


