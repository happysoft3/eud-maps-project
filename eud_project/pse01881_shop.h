
#include "pse01881_utils.h"
#include "libs/waypoint.h"
#include "libs/cweaponproperty.h"
#include "libs/itemproperty.h"

void XBowShot()     //@brief. WeaponProperty -3-
{
    int xbow = CreateObjectAt("ArcherArrow", GetObjectX(OTHER) + UnitAngleCos(OTHER, 8.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 8.0));

    SetOwner(OTHER, xbow);
    LookWithAngle(xbow, GetDirection(OTHER));
    Enchant(xbow, "ENCHANT_SHOCK", 0.0);
    PushObject(xbow, 34.0, GetObjectX(OTHER), GetObjectY(OTHER));
}

int PlaceArrowSword(int location)
{
    int arrow = CreateObjectAt("Sword", LocationX(location), LocationY(location));

    SetWeaponProperties(arrow, 5, 5, 8, 3);
    RegistWeaponCPropertyExecScript(arrow, 2, XBowShot);
    return arrow;
}

int MagicWeaponClassArrowSword()
{
    string name = "에로우 서드";
    int pay = 20000;
    object createFunction = PlaceArrowSword;

    return SToInt(&name);
}


int SingleShuriken(int owner, float posX, float posY)
{
    int mis = CreateObjectAt("OgreShuriken", posX, posY);
    int ptr = GetMemory(0x750710);

    SetMemory(GetMemory(ptr + 0x2bc) + 4, 66);
    PushObject(mis, 34.0, GetObjectX(owner), GetObjectY(owner));
    SetOwner(owner, mis);
    return mis;
}

void TripleShurikens(int dirUnit)
{
    int owner = GetOwner(dirUnit);

    if (CurrentHealth(owner) && IsObjectOn(dirUnit))
    {
        int i;

        for (i = 0 ; i < 13 ; i += 1)
        {
            SingleShuriken(owner, GetObjectX(dirUnit) + UnitAngleCos(dirUnit, 18.0), GetObjectY(dirUnit) + UnitAngleSin(dirUnit, 18.0));
            LookWithAngle(dirUnit, GetDirection(dirUnit) + 5);
        }
    }
    Delete(dirUnit);
}

void CastTripleShurikens()
{
    int dirUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(other), GetObjectY(other));

    SetOwner(other, dirUnit);
    LookWithAngle(dirUnit, GetDirection(other) - 30);
    FrameTimerWithArg(1, dirUnit, TripleShurikens);
}

int PlaceTripleShurikenSword(int location)
{
    int triple = CreateObjectAt("GreatSword", LocationX(location), LocationY(location));

    SetWeaponProperties(triple, 5, 5, 8, 3);
    RegistWeaponCPropertyExecScript(triple, 2, CastTripleShurikens);
    Enchant(triple, EnchantList(4), 0.0);
    return triple;
}

int MagicWeaponClassTripleHammer()
{
    string name = "트리플 슈리켄 해머";
    int pay = 25500;
    object createFunction = PlaceTripleShurikenSword;

    return SToInt(&name);
}


void EnergyparTouched()
{
    int owner = GetOwner(self);

    if (CurrentHealth(other) && IsAttackedBy(other, owner))
        Damage(other, owner, 100, 14);
}

void EnergyparFx(int helper)
{
    int owner = GetOwner(helper);

    Effect("EXPLOSION", GetObjectX(helper), GetObjectY(helper), 0.0, 0.0);

    int dmHelper = DummyUnitCreateById(OBJ_DEMON, GetObjectX(helper), GetObjectY(helper));
    SetUnitFlags(dmHelper, GetUnitFlags(dmHelper) ^ 0x2000);    //NO_PUSH_CHARACTERS
    SetOwner(owner, dmHelper);
    SetCallback(dmHelper, 9, EnergyparTouched);
    DeleteObjectTimer(dmHelper, 1);
}

void EnergyparGoForward(int helper)
{
    while (IsObjectOn(helper))
    {
        int owner = GetOwner(helper);

        if (CurrentHealth(owner))
        {
            int durate = GetDirection(helper);

            if (durate && IsVisibleTo(helper, helper + 1))
            {
                FrameTimerWithArg(1, helper, EnergyparGoForward);
                MoveObjectVector(helper, GetObjectZ(helper), GetObjectZ(helper + 1));
                MoveObjectVector(helper + 1, GetObjectZ(helper), GetObjectZ(helper + 1));
                LookWithAngle(helper, --durate);
                EnergyparFx(helper);
                break;
            }
        }
        Delete(helper);
        Delete(++helper);
        break;
    }
}

void EnergyparShot()    //@brief. WeaponProperty -2-
{
    float vectX = UnitAngleCos(other, 19.0), vectY = UnitAngleSin(other, 19.0);
    int helper = CreateObjectAt("ImaginaryCaster", GetObjectX(other) + vectX, GetObjectY(other) + vectY);

    Raise(helper, vectX);
    Raise(CreateObjectAt("ImaginaryCaster", GetObjectX(other) - vectX, GetObjectY(other) - vectY), vectY);
    LookWithAngle(helper, 44);
    SetOwner(other, helper);
    FrameTimerWithArg(1, helper, EnergyparGoForward);
}

int PlaceEnergyparAxe(int location)
{
    int par = CreateObjectAt("OgreAxe", LocationX(location), LocationY(location));

    SetWeaponProperties(par, 5, 5, 8, 3);
    RegistWeaponCPropertyExecScript(par, 2, EnergyparShot);
    return par;
}

int MagicWeaponClassEnergyparSword()
{
    string name = "에너지파 엑스칼리버";
    int pay = 28350;
    object createFunction = PlaceEnergyparAxe;

    return SToInt(&name);
}

void onAngelCrystalSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 140, DAMAGE_TYPE_PLASMA);
        }
    }
}

void AngelCrystalCollide()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (GetTrigger())
    {
        if (CurrentHealth(other) && IsAttackedBy(other, owner))
        {
            WispDestroyFX(GetObjectX(self), GetObjectY(self));
            SplashDamageAtEx(owner, GetObjectX(self), GetObjectY(self), 50.0,onAngelCrystalSplash);
            Delete(self);
        }
    }
}

int AngelCrystal(int owner, float vectSize)
{
    int crystal = CreateObjectAt("GameBall", GetObjectX(owner) + UnitAngleCos(owner, vectSize), GetObjectY(owner) + UnitAngleSin(owner, vectSize));

    SetOwner(owner, CreateObjectAtUnit("InvisibleLightBlueLow", owner));
    SetUnitCallbackOnCollide(crystal, AngelCrystalCollide);
    DeleteObjectTimer(crystal, 90);
    DeleteObjectTimer(crystal + 1, 100);
    return crystal;
}

void ShotAngelCrystal() //@brief. WeaponProperty -1-
{
    PushObject(AngelCrystal(other, 19.0), 24.0, GetObjectX(other), GetObjectY(other));
    PlaySoundAround(other, 204);
    Effect("CYAN_SPARKS", GetObjectX(other), GetObjectY(other), 0.0, 0.0);
}

int PlaceAngelCrystalSword(int location)
{
    int angel = CreateObjectAt("GreatSword", LocationX(location), LocationY(location));

    SetWeaponProperties(angel, 5, 5, 8, 3);
    RegistWeaponCPropertyExecScript(angel, 2, ShotAngelCrystal);
    return angel;
}

int MagicWeaponClassCrystalSword()
{
    string name = "지랄발광 수정구 서드";
    int pay = 26533;
    object createFunction = PlaceAngelCrystalSword;

    return SToInt(&name);
}


void CastFireballShotSword()
{
    int mis = CreateObjectAt("TitanFireball", GetObjectX(OTHER) + UnitAngleCos(OTHER, 17.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 17.0));

    SetOwner(OTHER, mis);
    LookWithAngle(mis, GetDirection(other));
    PushObject(mis, 33.0, GetObjectX(OTHER), GetObjectY(OTHER));
}

int PlaceSuperPowerSword(int location)
{
    int super = CreateObjectAt("GreatSword", LocationX(location), LocationY(location));

    RegistWeaponCPropertyExecScript(super, 2, CastFireballShotSword);
    Enchant(super, EnchantList(4), 0.0);
    return super;
}

int MagicWeaponClassPowerfulSword()
{
    string name = "화이어볼 서드";
    int pay = 24000;
    object createFunction = PlaceSuperPowerSword;

    return SToInt(&name);
}

void InitWeaponContainer()
{
    WeaponContainerPushback(MagicWeaponClassArrowSword);
    WeaponContainerPushback(MagicWeaponClassCrystalSword);
    WeaponContainerPushback(MagicWeaponClassEnergyparSword);
    WeaponContainerPushback(MagicWeaponClassPowerfulSword);
    WeaponContainerPushback(MagicWeaponClassTripleHammer);
}


