
#include "q047251_gvar.h"
#include "q047251_utils.h"
#include "q047251_client.h"
#include "libs\printutil.h"
#include "libs\coopteam.h"
#include "libs\mathlab.h"
#include "libs\fxeffect.h"
#include "libs\waypoint.h"
#include "libs\itemproperty.h"
#include "libs\reaction.h"
#include "libs\networkRev.h"
#include "libs/sound_define.h"
#include "libs/voiceList.h"

int HapBugMUnit;
int *m_pImgVector;
int FrogCenter;
int player[30], RespPoint, MobCount;
int BLeft, BRight, Tvar;

int m_firstNumber;
static int FirstNumber(){return m_firstNumber;}//virtual

int m_secondNumber;
static int SecondNumber(){return m_secondNumber;}//virtual

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & 0x80000000;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] ^= 0x80000000;
}

static int ImportUseItemFuncOffset()
{ return 0xbc; }

void ShowMapInfo()
{
    int unit = CreateObject("Wizard", 128);
    int ptr = GetLastUnitPtr();

    ObjectOff(unit);
    Frozen(unit, 1);
    SetDialog(unit, "NORMAL", ChakramBugPotal, Nothing);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e0, 68364);
    UniPrintToAll("버그 탈출 넘버원                                                        제작. 237");
    UniPrintToAll("녹스 내 버그를 최대한 이용하여 이곳을 탈출해보자!                                  ");
    InitTeleportMarker();
    MoveWaypoint(1, GetWaypointX(129), GetWaypointY(129));
    InitializeDungeonDrawText();
}

void RemoveMagicWalls()
{
    WallOpen(Wall(199, 25));
    WallOpen(Wall(198, 26));
    ObjectOff(SELF);
}

void BlockSet()
{
    BLeft = Object("LeftRow");
    BRight = Object("RightRow");
    FrameTimerWithArg(30, 0, FireWayBlockSet);
}

void FireWayBlockSet(int mode)
{
    if (mode)
    {
        Move(BLeft, 95);
        Move(BRight, 96);
    }
    else
    {
        Move(BLeft, 93);
        Move(BRight, 94);
    }
    AudioEvent("SpikeBlockMove", 93 + (mode * 2));
    AudioEvent("SpikeBlockMove", 94 + (mode * 2));
}

void InitRotMeatPut()
{
    RotMeatCreate(CreateObject("InvisibleLightBlueHigh", 45));
    RotMeatCreate(CreateObject("InvisibleLightBlueHigh", 46));
    RotMeatCreate(CreateObject("InvisibleLightBlueHigh", 47));
    RotMeatCreate(CreateObject("InvisibleLightBlueHigh", 145));
    RotMeatCreate(CreateObject("InvisibleLightBlueHigh", 146));
    RotMeatCreate(CreateObject("InvisibleLightBlueHigh", 147));
}

void RotMeatCreate(int unit)
{
    int meat = CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit));
    int ptr = GetLastUnitPtr();

    SetMemory(ptr + 4, 239);
    Effect("RICOCHET", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Enchant(meat, "ENCHANT_RUN", 0.0);
    
    RegistItemPickupCallback(meat, HaveRotMeat);
    Delete(unit);
}

void DontHaveMeatHere()
{
    int inv = GetLastItem(OTHER);

    if (!HasEnchant(OTHER, "ENCHANT_DETECTING"))
    {
        Enchant(OTHER, "ENCHANT_DETECTING", 3.0);
        while (IsObjectOn(inv))
        {
            Delete(inv);
            inv = GetPreviousItem(inv);
        }
    }
}

void HaveRotMeat()
{
    FrameTimerWithArg(200, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER)), RotMeatCreate);
}

void GoalAroundLoc(int ptr)
{
    int k;

    for (k = 0 ; k < 36 ; k += 1)
        Enchant(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr) + MathSine(k * 10 + 90, 90.0), GetObjectY(ptr) + MathSine(k * 10, 90.0)), "ENCHANT_SHIELD", 0.0);
}

void InitBerserkerChargeTestRoom()
{
    GoalAroundLoc(CreateObject("InvisibleLightBlueHigh", 43));
}

void EnterBerserkerBugRoom()
{
    if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
    {
        if (!HasEnchant(OTHER, "ENCHANT_CROWN"))
        {
            int subUnit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));
            SetOwner(OTHER, subUnit);
            Enchant(OTHER, "ENCHANT_CROWN", 0.0);
            FrameTimerWithArg(1, subUnit, LoopBerserkerBugPlayer);
        }
    }
}

void LoopBerserkerBugPlayer(int ptr)
{
    int unit = GetOwner(ptr);

    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit, "ENCHANT_CROWN"))
        {
            if (GetObjectX(unit) + GetObjectY(unit) > 3472.0)
            {
                if (GetPlayerAction(unit) ^ 0x1)
                {
                    if (Distance(GetObjectX(unit), GetObjectY(unit), LocationX(43), LocationY(43)) < 90.0)
                    {
                        EscapeBerserkerRoom(unit);
                    }
                    else
                    {
                        GreenSparkAt(GetObjectX(unit), GetObjectY(unit));
                        Damage(unit, 0, 999, 14);
                        PlaySoundAround(unit, SOUND_ForceOfNatureRelease);
                    }
                }
            }
            FrameTimerWithArg(1, ptr, LoopBerserkerBugPlayer);
        }
        else
            unit = 0;
    }
    else
    {
        Delete(ptr);
    }
}

void EscapeBerserkerRoom(int unit)
{
    MoveObject(unit, LocationX(44), LocationY(44));
    ShockFx(unit);
    EnchantOff(unit, "ENCHANT_CROWN");
}

void PlaceThreeGirls()
{
    int ptr = ColorMaiden(255, 0, 0, 36);
    Frozen(ColorMaiden(0, 255, 0, 37), 1);
    Frozen(ColorMaiden(0, 0, 255, 38), 1);
    Frozen(ptr, 1);
    LookWithAngle(ptr, 0);
    LookWithAngle(ptr + 1, 1);
    LookWithAngle(ptr + 2, 2);
    PlantDead(39);
    PlantDead(40);
    PlantDead(41);
    PlantDead(42);
    SetCallback(ptr, 9, TouchedGirl);
    SetCallback(ptr + 1, 9, TouchedGirl);
    SetCallback(ptr + 2, 9, TouchedGirl);
}

void OrbRedEvent()
{
    if (HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_FIRE") && GetDirection(OTHER))
    {
        OpenPassGate();
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Delete(OTHER);
    }
}

void OrbGreenEvent()
{
    if (HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_POISON") && GetDirection(OTHER))
    {
        OpenPassGate();
        GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, SOUND_AwardSpell);
        Delete(OTHER);
    }
}

void OrbBlueEvent()
{
    if (HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_ELECTRICITY") && GetDirection(OTHER))
    {
        OpenPassGate();
        Effect("CYAN_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Delete(OTHER);
    }
}

void TouchedGirl()
{
    int orbEvent[] = {OrbRedEvent, OrbGreenEvent, OrbBlueEvent};

    CallFunction(orbEvent[GetDirection(SELF) % 3]);
}

void OpenPassGate()
{
    if (--MobCount == 0)
    {
        WallOpen(Wall(110, 84));
        WallOpen(Wall(111, 85));
        WallOpen(Wall(112, 86));
        UniPrintToAll("벽이 열렸습니다");
    }
}

void PlantDead(int wp)
{
    int unit = CreateObject("CarnivorousPlant", wp);

    SetCallback(unit, 9, WhenTouchedYouDie);
    Damage(unit, 0, MaxHealth(unit) + 10, -1);
}

void WhenTouchedYouDie()
{
    if (CurrentHealth(OTHER))
    {
        Effect("DAMAGE_POOF", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, 0, 200, 14);
    }
}

void TwoByTwoWallOpen(int x, int y)
{
    WallOpen(Wall(x, y));
    WallOpen(Wall(x + 1, y - 1));
    WallOpen(Wall(x, y + 2));
    WallOpen(Wall(x + 1, y + 3));
    WallOpen(Wall(x + 3, y + 3));
    WallOpen(Wall(x + 4, y + 2));
    WallOpen(Wall(x + 3, y - 1));
    WallOpen(Wall(x + 4, y));
}

void IxOpenWalls()
{
    ObjectOff(SELF);
    TwoByTwoWallOpen(106, 74);
    TwoByTwoWallOpen(112, 80);
    TwoByTwoWallOpen(118, 86);
    TwoByTwoWallOpen(124, 80);
    FrameTimerWithArg(60, 8, EastWallOpen);
}

void EastWallOpen(int n)
{
    int k;

    for (k = 0 ; k < n ; k ++)
        WallOpen(Wall(120 + k, 68 + k));
}

void PutHiddenMonsters()
{
    FireWisp(32);
    FireWisp(32);
    FireWisp(32);
    FireWisp(32);
    FireWisp(32);
    FireWisp(32);
    FireWisp(32);
    PurpleMaiden(33);
    PurpleMaiden(33);
    PurpleMaiden(33);
    PurpleMaiden(33);
    SmallGreenMob(34);
    SmallGreenMob(34);
    SmallGreenMob(34);
    SmallGreenMob(34);
    SmallGreenMob(34);
    StumpSummon(35);
    StumpSummon(35);
    StumpSummon(35);
}

int PurpleMaiden(int wp)
{
    int unit = ColorMaiden(250, 36, 225, wp);

    SetUnitMaxHealth(unit, 225);
    SetCallback(unit, 5, HiddenMobDead);
    MobCount ++;
    return unit;
}

int FireWisp(int wp)
{
    int unit = CreateObject("FireSprite", wp);

    UnitLinkBinScript(unit, FireSpriteBinTable());
    SetUnitMaxHealth(unit, 128);
    SetCallback(unit, 5, HiddenMobDead);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    MobCount ++;
    return unit;
}

int SmallGreenMob(int wp)
{
    int unit = CreateObject("Goon", wp);

    UnitLinkBinScript(unit, GoonBinTable());
    SetUnitMaxHealth(unit, 225);
    SetUnitVoice(unit, 63);
    SetCallback(unit, 5, HiddenMobDead);
    MobCount ++;
    return unit;
}

int StumpSummon(int wp)
{
    int unit = CreateObject("BlackWidow", wp);

    UnitLinkBinScript(unit, BlackWidowBinTable());
    SetUnitMaxHealth(unit, 275);
    SetUnitVoice(unit, 19);
    SetCallback(unit, 5, HiddenMobDead);
    MobCount ++;

    return unit;
}

void HiddenMobDead()
{
    int orbCreator[] = {DropRedOrb, DropBlueOrb, DropGreenOrb};
    int orb = CallFunctionWithArgInt(orbCreator[Random(0, 2)], SELF);

    DeleteObjectTimer(SELF, 90);
}

int DropRedOrb(int posUnit)
{
    int orb = CreateObjectAt("RedOrb", GetObjectX(posUnit), GetObjectY(posUnit));

    LookWithAngle(orb, 1);
    Enchant(orb, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
    return orb;
}

int DropBlueOrb(int posUnit)
{
    int orb = CreateObjectAt("BlueOrb", GetObjectX(posUnit), GetObjectY(posUnit));

    LookWithAngle(orb, 1);
    Enchant(orb, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
    return orb;
}

int DropGreenOrb(int posUnit)
{
    int orb = CreateObjectAt("GreenOrb", GetObjectX(posUnit), GetObjectY(posUnit));

    LookWithAngle(orb, 1);
    Enchant(orb, "ENCHANT_PROTECT_FROM_POISON", 0.0);
    return orb;
}

void RedMonster()
{
    int unit = CreateObject("Demon", 28);

    AggressionLevel(unit, 0.0);
    Enchant(unit, "ENCHANT_BLINDED", 0.0);
    FrameTimerWithArg(1, unit, IsAntiBerp);
}

void IsAntiBerp(int unit)
{
    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit, "ENCHANT_ANTI_MAGIC"))
        {
            GreenSparkAt(LocationX( 29), LocationY(29) );
            SightBlinkOn(29);
            EnchantOff(unit, "ENCHANT_ANTI_MAGIC");
        }
        FrameTimerWithArg(1, unit, IsAntiBerp);
    }
}

void SightBlinkOn(int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 10);
    LookWithAngle(unit, 160);
    SetCallback(unit, 3, InSightThenMove);
    DeleteObjectTimer(unit, 1);
}

void InSightThenMove()
{
    if (IsPlayerUnit(OTHER))
    {
        if (!HasEnchant(OTHER, "ENCHANT_ANCHORED"))
        {
            Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
            FrameTimerWithArg(35, GetCaller(), DelayTeleport);
        }
    }
}

void DelayTeleport(int unit)
{
    if (CurrentHealth(unit) && HasEnchant(unit, "ENCHANT_ANCHORED"))
    {
        EnchantOff(unit, "ENCHANT_ANCHORED");
        MoveObject(unit, GetWaypointX(30), GetWaypointY(30));
    }
}

void Upthrow()
{
    int k;

    for (k = 0 ; k < 11 ; k ++)
        SentryRightMoving(Object("DeathSen" + IntToString(k + 1)));
}

void SentryRightMoving(int unit)
{
    if (IsObjectOn(unit))
    {
        if (GetObjectX(unit) >= 2540.0)
            MoveObject(unit, GetWaypointX(27), GetWaypointY(27));
        else
            MoveObject(unit, GetObjectX(unit) + 5.0, GetObjectY(unit) - 5.0);
        FrameTimerWithArg(1, unit, SentryRightMoving);
    }
}

// int DrawImageAt(float x, float y, int thingId)
// {
//     int unit = CreateObjectAt("AirshipBasketShadow", x, y);
//     int ptr = GetMemory(0x750710);

//     SetMemory(ptr + 0x04, thingId);
//     return unit;
// }

void InitializeDungeonDrawText()
{
    DrawTextSprite(LocationX(179), LocationY(179),11);//test



    DrawTextSprite(LocationX(129), LocationY(129),0);
    DrawTextSprite(LocationX(31), LocationY(31), 1);
    DrawTextSprite(LocationX(112), LocationY(112), 2);
    DrawTextSprite(LocationX(148), LocationY(148), 3);
    DrawTextSprite(LocationX(149), LocationY(149), 4);
    DrawTextSprite(LocationX(150), LocationY(150), 5);
    DrawTextSprite(LocationX(154), LocationY(154), 4);
    DrawTextSprite(LocationX(169), LocationY(169), 7);
    DrawTextSprite(LocationX(170), LocationY(170), 8);
    DrawTextSprite(LocationX(171), LocationY(171), 9);
    DrawTextSprite(LocationX(172), LocationY(172), 10);

    int u;

    for (u = 0 ; u < 10 ; u += 1)
        DrawTextSprite(LocationX(77 + u) - 150.0, LocationY(77 + u) - 150.0, 6);
}

static void NetworkUtilClientMain()
{
    PlayerClassCommonWhenEntry();
}

void InitPlayerRespawnPoint()
{
    int k;

    RespPoint = CreateObject("InvisibleLightBlueLow", 5);
    for (k = 9 ; k ; k --)
        CreateObject("InvisibleLightBlueLow", 5);
    PutDec2();
}

void PutDec2()
{
    int ptr = GodBomber(10);
    GodBomber(21);
    LookWithAngle(TeleportWiz(22), 192);
    LookWithAngle(TeleportWiz(23), 92);
    BerpOffDecor(8);
    RedMonster();
}

int TeleportWiz(int wp)
{
    int unit = CreateObject("Wizard", wp);

    Frozen(unit, 1);
    SetDialog(unit, "NORMAL", TeleportToTarget, Nothing);

    return unit;
}

int GodBomber(int wp)
{
    string bombername[] = {"Bomber", "BomberBlue", "BomberGreen", "BomberYellow"};
    int unit = CreateObject(bombername[Random(0, 3)], wp);

    UnitLinkBinScript(unit, MaidenBinTable());
    BomberSetMonsterCollide(unit);
    CreateObject("InvisibleLightBlueHigh", wp);
    Frozen(unit, 1);
    LookWithAngle(unit, 32);
    SetDialog(unit, "NORMAL", PlayerGetBomber, Nothing);

    return unit;
}

void PlayerGetBomber()
{
    int unit, idx;

    if (!CurrentHealth(ToInt(GetObjectZ(GetTrigger() + 1))))
    {
        unit = CreateObject("LargeBarrel2", 11 + idx);
        DeleteObjectTimer(unit, 300);
        SetOwner(OTHER, unit);
        Raise(GetTrigger() + 1, ToFloat(unit));
        FrameTimerWithArg(1, unit, DelayPickUnit);
        idx = (idx + 1) % 10;
        UniChatMessage(OTHER, "인벤토리에 추가되었습니다", 120);
    }
    else
        UniPrint(OTHER, "인벤토리에 이미 상자가 있습니다");
}

void SightEnemy()
{
    HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
}

void DelayPickUnit(int unit)
{
    if (CurrentHealth(GetOwner(unit)))
        Pickup(GetOwner(unit), unit);
    else
        DeleteObjectTimer(unit, 1);
}

void TeleportToTarget()
{
    CastSpellObjectObject("SPELL_TELEPORT_TO_MARK_1", OTHER, OTHER);
}

void BerpOffDecor(int wp)
{
    int ptr = CreateObject("Maiden", wp);
    Frozen(CreateObject("RewardMarkerPlus", wp), 1);
    Frozen(ptr, 1);
    SetDialog(ptr, "NORMAL", BerpOff, Nothing);
}

void RunPotionPick()
{
    Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
}

void RunPotionUse()
{
    int count;

    if (++count == 10)
    {
        count = 0;
        FrameTimer(30, RunPotionPlace);
    }
    Delete(SELF);
    Enchant(OTHER, "ENCHANT_RUN", 0.1);
}

void RunPotionPlace()
{
    int k, baseUnit = CreateObject("InvisibleLightBlueHigh", 9) + 1;

    Delete(baseUnit - 1);
    TeleportLocation(5, GetWaypointX(9), GetWaypointY(9));
    for (k = 0 ; k < 10 ; k ++)
    {
        CreateObject("AmuletofManipulation", 5);
        RegistItemPickupCallback(baseUnit + k, RunPotionPick);
        SetUnitCallbackOnUseItem(baseUnit + k, RunPotionUse);
        TeleportLocationVector(5, 32.0, 32.0);
    }
}

void Nothing()
{
    return;
}

void BerpOff()
{
    if (HasEnchant(OTHER, "ENCHANT_RUN"))
    {
        EnchantOff(OTHER, "ENCHANT_RUN");
        GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
        UniPrint(OTHER, "모든 버프가 해제됩니다");
    }
}

void InitialDisposSewerDeco(int count)
{
    if (count > 0)
    {
        CreateObjectAt("Rock3", LocationX(174), LocationY(174));
        FrameTimerWithArg(6, count - 1, InitialDisposSewerDeco);
    }
}

void initialPutNumber(){
    m_firstNumber=createNumber(LocationX(176),LocationY(176));
    m_secondNumber=createNumber(LocationX(177),LocationY(177));
}

void initializeReadables(){
    RegistSignMessage(Object("MapSign1"), "버그탈출 넘버원! 녹스의 각종 버그를 이용하여 함정을 벗어나시오");
    RegistSignMessage(Object("MapSign2"), "좌측 벽에 비밀벽있음요");
    RegistSignMessage(Object("MapSign3"), "구슬 색깔과 옷 색깔이 맞는 여성분께 구슬을 드리세요");
    RegistSignMessage(Object("MapSign4"), "텔레포트 입니다~~ 얼릉 타세요!!");
    RegistSignMessage(Object("MapSign5"), "이곳은 맵 개발자가 이용하는 휴게실입니다. 관계자 외 출입을 금합니다");
    RegistSignMessage(Object("read1"), "게임을 시작하려면은 오른쪽 위로 가시요");
}

void MapInitialize()
{
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(2, InitBerserkerChargeTestRoom);
    FrameTimerWithArg(1, 8, InitialDisposSewerDeco);
    
    InitPlayerRespawnPoint();
    
    BlockSet();
    FrogCenter = CreateObject("InvisibleLightBlueLow", 130);
    Enchant(FrogCenter, "ENCHANT_RUN", 0.0);
    FrameTimer(1, RunPotionPlace);
    FrameTimer(1, Upthrow);
    FrameTimer(1, PutHiddenMonsters);
    FrameTimer(2, PlaceThreeGirls);
    FrameTimer(3, InitRotMeatPut);
    FrameTimer(3, InitDeathRayTrap); //error
    FrameTimer(4, InitPlaceMapHint);
    FrameTimer(6, PutBall); //
    FrameTimerWithArg(44, 114, GeneratorPull);
    FrameTimer(60, LoopFistTraps);
    FrameTimer(30, PlayerClassOnLoop);
    SecondTimer(7, ShowMapInfo);

    initializeReadables();
    initGame();
    blockObserverMode();
    initialPutNumber();
    int ptr;

    MapBgmTable(0);
    ptr = CreateObject("InvisibleLightBlueHigh", 1);
    Raise(ptr, ToFloat(9999));
    FrameTimerWithArg(30, ptr, MapMusicLoop);
    FrameTimer(3, MapDecorations);
    FrameTimer(30, SpawnBerserkerZoneUnit);
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;
    MoveObject(RespPoint + plr, LocationX(4), LocationY(4));
    return plr;
}

void PlayerEntry()
{
    int plr, k;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            if (MaxHealth(OTHER) == 150)
            {
                plr = CheckPlayer();
                for (k = 9 ; k >= 0 && plr < 0 ; k --)
                {
                    if (!MaxHealth(player[k]))
                    {
                        plr = PlayerClassOnInit(k, GetCaller());
                        break;
                    }
                }
                if (plr >= 0)
                {
                    JoinPlayer(plr);
                    break;
                }
            }
        }
        CantJoinTheMap();
        break;
    }
}

void JoinPlayer(int plr)
{
    if (PlayerClassCheckDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);

    EnchantOff(player[plr], "ENCHANT_INVULNERABLE");
    EnchantOff(player[plr], "ENCHANT_AFRAID");
    MoveObject(player[plr], GetObjectX(RespPoint + plr), GetObjectY(RespPoint + plr));
    ShockFx(player[plr]);
    PlaySoundAround(player[plr], 23);
}

void FastJoin(){
    if (!CurrentHealth(OTHER))
        return;

    int r=10;

    while (--r>=0){
        if (IsCaller(player[r]))
        {
            PlayerEntry();
            return;
        }
    }
    MoveObject(OTHER,LocationX(178),LocationY(178));
}

void CantJoinTheMap()
{
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    MoveObject(OTHER, GetWaypointX(3), GetWaypointY(3));
    UniPrint(OTHER, "맵에 입장할 수 없습니다");
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (HasEnchant(player[plr], "ENCHANT_AFRAID"))
    {
        BerserkerRemoveDelay(plr);
    }
    PlayerMaxHealthSetTo(plr, 900);
}

void PlayerClassOnDeath(int plr)
{ }

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (TRUE)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void RemoveCoreUnits(int ptr)
{
    Delete(ptr);
    Delete(++ptr);
}

void BerserkerNoDelayCore(int plr)
{
    int arr[10];

    if (!MaxHealth(arr[plr]))
    {
        MoveWaypoint(5, GetWaypointX(plr + 117), GetWaypointY(plr + 117) + 23.0);
        arr[plr] = CreateObject("Bear2", 117 + plr);
        UnitLinkBinScript(CreateObject("Rat", 5) - 1, MaidenBinTable());
        SetOwner(player[plr], arr[plr]);
        LookAtObject(arr[plr], arr[plr] + 1);
        HitLocation(arr[plr], GetObjectX(arr[plr] + 1), GetObjectY(arr[plr] + 1));
        FrameTimerWithArg(3, arr[plr], RemoveCoreUnits);
    }
}

void BerserkerRemoveDelay(int plr)
{
    if (HasEnchant(player[plr], "ENCHANT_SNEAK"))
    {
        MoveWaypoint(5, GetObjectX(player[plr]), GetObjectY(player[plr]));
        UniPrint(player[plr], "버저커 차지 쿨다운을 없앴습니다");
        AudioEvent("GlyphCast", 5);
        EnchantOff(player[plr], "ENCHANT_SNEAK");
        BerserkerNoDelayCore(plr);
    }
}

void PlayerMaxHealthSetTo(int plr, int max)
{
    if (CurrentHealth(player[plr]) > max)
    {
        EnchantOff(player[plr], "ENCHANT_INVULNERABLE");
        Damage(player[plr], 0, CurrentHealth(player[plr]) - max, -1);
    }
}

void ShockFx(int unit)
{
    int ptr = CreateObject("InvisibleLightBlueLow", 6);

    Enchant(ptr, "ENCHANT_SHOCK", 0.0);
    MoveObject(ptr, GetObjectX(unit), GetObjectY(unit));
    DeleteObjectTimer(ptr, 20);
}

int CheckPlayer()
{
    int k;
    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void LoopFistTraps()
{
    int k;

    for (k = 0 ; k < 23 ; k ++)
        FallStoneFist(k + 48);
    FrameTimer(20, LoopFistTraps);
}

void FallStoneFist(int wp)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", wp);

    CastSpellLocationLocation("SPELL_FIST", GetObjectX(ptr), GetObjectY(ptr) - 2.0, GetObjectX(ptr), GetObjectY(ptr));
    SetOwner(ptr, ptr + 1);
    Enchant(ptr + 1, "ENCHANT_FREEZE", 0.0);
    DeleteObjectTimer(ptr, 3);
}

int DrawMagicIconAt(float xProfile, float yProfile)
{
    int unit = CreateObjectAt("AirshipBasketShadow", xProfile, yProfile);
    int *ptr = UnitToPtr(unit);
    
    ptr[1] = 1416;
    
    return unit;
}

void HitPad()
{
    if (CurrentHealth(OTHER))
    {
        int mem = ToInt(GetObjectZ(GetTrigger() + 1));

        if (GetCaller() ^ mem)
        {
            int owner = GetOwner(GetTrigger() + 1);

            LookWithAngle(SELF, 30);
            GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
            Effect("DEATH_RAY", GetObjectX(owner + 1), GetObjectY(owner + 1), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, 0, 100, 16);
            Raise(GetTrigger() + 1, ToFloat(GetCaller()));
        }
        else
        {
            LookWithAngle(SELF, GetDirection(SELF) - 1);
            if (!GetDirection(SELF))
                Raise(GetTrigger() + 1, ToFloat(0));
        }
    }
}

void RayPad(int owner)
{
    int wp = GetDirection(owner);
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 10);
    SetOwner(owner, CreateObject("InvisibleLightBlueLow", wp));
    SetCallback(unit, 9, HitPad);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
}

void PadXRow(int owner)
{
    int k;

    TeleportLocation(72, LocationX(71), LocationY(71));
    for (k = 0 ; k < 8 ; k += 1)
    {
        RayPad(owner);
        TeleportLocationVector(72, 23.0, 23.0);
    }
    TeleportLocationVector(71, -23.0, 23.0);
}

void GhostHittingBugChecker(int wp)
{
    int unit = CreateObject("StoneGolem", wp);

    SetUnitMaxHealth(unit, 2000);
    CreateObject("InvisibleLightBlueHigh", wp);
    AggressionLevel(unit, 0.0);
    Enchant(unit, "ENCHANT_BLINDED", 0.0);
    SetCallback(unit, 7, HitCheckFunction);
    SetCallback(unit, 9, GolemCollideFunc);
}

void InitDeathRayTrap()
{
    int subUnit = CreateObject("InvisibleLightBlueHigh", 73), k;

    LookWithAngle(DrawMagicIconAt(GetObjectX(subUnit), GetObjectY(subUnit)) - 1, 72);

    for (k = 0 ; k < 19 ; k += 1)
        FrameTimerWithArg(k + 1, subUnit, PadXRow);
    
    PadXRow(subUnit);
    
    Enchant(subUnit, "ENCHANT_SHIELD", 0.0);
    GhostHittingBugChecker(74);
}

void HitCheckFunction()
{
    if (IsMonsterUnit(OTHER))
    {
        if (!HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_ELECTRICITY"))
        {
            if (!HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_POISON"))
            {
                if (!HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_FIRE"))
                    Enchant(OTHER, "ENCHANT_PROTECT_FROM_FIRE", 1.0);
                else
                    Enchant(OTHER, "ENCHANT_PROTECT_FROM_POISON", 1.0);
            }
            else
                Enchant(OTHER, "ENCHANT_PROTECT_FROM_ELECTRICITY", 1.0);
        }
        else
        {
            int plr = CheckOwnerPlayer(OTHER);
            if (plr + 1)
            {
                if (CurrentHealth(player[plr]))
                    MoveObject(player[plr], GetWaypointX(76), GetWaypointY(76));
            }
        }
    }
    BackInitPosition(GetTrigger());
}

void GolemCollideFunc()
{
    if (CurrentHealth(OTHER))
        BackInitPosition(GetTrigger());
}

void BackInitPosition(int unit)
{
    if (DistanceUnitToUnit(unit, unit + 1) > 50.0)
        MoveObject(unit, GetObjectX(unit + 1), GetObjectY(unit + 1));
}

void SummonGhost()
{
    if (CurrentHealth(OTHER))
        CastSpellObjectObject("SPELL_SUMMON_GHOST", OTHER, OTHER);
}

int CheckOwnerPlayer(int unit)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
        {
            if (IsOwnedBy(unit, player[k]))
                return k;
        }
    }
    return -1;
}

void ChakramBugPotal()
{
    int plr = CheckPlayer();
    int inv[20];

    if (plr + 1)
    {
        if (CurrentHealth(OTHER))
        {
            TeleportLocation(5, LocationX(77 + plr) - 53.0, LocationY(77 + plr) - 53.0);
            if (!IsObjectOn(inv[plr]))
            {
                inv[plr] = CreateObject("SteelShield", 5);
                Frozen(inv[plr], 1);
                SetArmorProperties(inv[plr], 5, 3, 16, 19);
            }
            else
                MoveObject(inv[plr], LocationX(5), LocationY(5));
            if (!IsObjectOn(inv[plr + 10]))
            {
                inv[plr + 10] = CreateObject("RoundChakram", 5);
                SetWeaponProperties(inv[plr + 10], 5, 3, 24, 7);
            }
            else
                MoveObject(inv[plr + 10], LocationX(5), LocationY(5));
            SpawnChakramTestGolem(plr);
            MoveObject(OTHER, LocationX(77 + plr), LocationY(77 + plr));
        }
    }
}

void SpawnChakramTestGolem(int plr)
{
    int unit[10];

    if (CurrentHealth(unit[plr]))
    {
        RestoreHealth(unit[plr], MaxHealth(unit[plr]) - CurrentHealth(unit[plr]));
        MoveObject(unit[plr], GetWaypointX(77 + plr) - 292.0, GetWaypointY(77 + plr) - 292.0);
    }
    else
    {
        unit[plr] = CreateObjectAt("MechanicalGolem", GetWaypointX(77 + plr) - 292.0, GetWaypointY(77 + plr) - 292.0);
        SetUnitMaxHealth(unit[plr], 600);
        LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit[plr]), GetObjectY(unit[plr])), plr);
        SetCallback(unit[plr], 7, MecaHurt);
        SetCallback(unit[plr], 5, MecaDead);
    }
    LookWithAngle(unit[plr], 32);
    Raise(unit[plr] + 1, ToFloat(0));
    Walk(unit[plr], LocationX(77 + plr), LocationY(77 + plr));
    AggressionLevel(unit[plr], 1.0);
}

void MecaHurt()
{
    int count = ToInt(GetObjectZ(GetTrigger() + 1)), plr = GetDirection(GetTrigger() + 1);

    if (!count)
    {
        if (CurrentHealth(player[plr]))
        {
            FrameTimerWithArg(1, GetTrigger(), ChakramKillToMecaTimer);
            Raise(GetTrigger() + 1, ToFloat(30 * 5));
            UniPrint(player[plr], "당신의 임무: 체력 600의 기계골렘을 5 초 안에 잡아야 합니다");
        }
    }
}

void MecaDead()
{
    int plr = GetDirection(GetTrigger() + 1), count = ToInt(GetObjectZ(GetTrigger() + 1));

    if (CurrentHealth(player[plr]))
    {
        if (count)
        {
            UniPrint(player[plr], "성공하였습니다");
            RemoveChakram(player[plr]);
            MoveObject(player[plr], GetWaypointX(87), GetWaypointY(87));
            Delete(GetTrigger() + 1);
        }
    }

    Delete(GetTrigger() + 1);
}

void RemoveMecaGolem(int unit)
{
    Delete(unit);
    Delete(unit + 1);
}

void ChakramKillToMecaTimer(int ptr)
{
    int count = ToInt(GetObjectZ(ptr + 1)), plr = GetDirection(ptr + 1);

    if (CurrentHealth(player[plr]) && CurrentHealth(ptr))
    {
        if (count)
        {
            Raise(ptr + 1, ToFloat(count - 1));
            FrameTimerWithArg(1, ptr, ChakramKillToMecaTimer);
        }
        else
        {
            UniPrint(player[plr], "5 초 경과! 실패하였습니다");
            RemoveMecaGolem(ptr);
            Effect("SENTRY_RAY", GetObjectX(ptr), GetObjectY(ptr), GetObjectX(player[plr]), GetObjectY(player[plr]));
            MoveObject(player[plr], GetWaypointX(128), GetWaypointY(128));
            AudioEvent("NoCanDo", 128);
        }
    }
}

void WhenDeadLavaFlow()
{
    if (CurrentHealth(OTHER) && HasClass(OTHER, "PLAYER"))
    {
        GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, SOUND_DeathOn);
        if (HasEnchant(OTHER, "ENCHANT_INVULNERABLE"))
            EnchantOff(OTHER, "ENCHANT_INVULNERABLE");
        Damage(OTHER, 0, MaxHealth(OTHER) + 1, 14);
    }
}

void RemoveChakram(int unit)
{
    int inv = GetLastItem(unit),cur;

    while (IsObjectOn(inv))
    {
        cur=inv;
        inv = GetPreviousItem(inv);
        if (GetUnitThingID(cur)  == OBJ_ROUND_CHAKRAM)
            Delete(cur);
    }
}

void RhombusPut(int wp, float x_low, float x_high, float y_low, float y_high)
{
    float x = RandomFloat(y_low, y_high), y = RandomFloat(0.0, x_high - x_low);

    MoveWaypoint(wp, x_high - y_high + x - y, x + y);
}

int LavaUnitManager()
{
    int unit;

    if (!unit)
        unit = CreateObject("InvisibleLightBlueHigh", 88);
    return unit;
}

void LavaUnitHurt()
{
    if (CurrentHealth(SELF) ^ MaxHealth(SELF))
    {
        RestoreHealth(SELF, MaxHealth(SELF));
        if (!HasEnchant(SELF, "ENCHANT_INVULNERABLE"))
            Enchant(SELF, "ENCHANT_INVULNERABLE", 0.0);
    }
}

void LavaUnitCollideDummy()
{
    return;
}

void LavaUnitCollide()
{
    if (CurrentHealth(OTHER) && CurrentHealth(SELF))
    {
        if (HasClass(OTHER, "PLAYER"))
        {
            if (GetPlayerAction(OTHER) ^ 13)
                Damage(OTHER, SELF, 255, 14);
            else
            {
                UnitNoCollide(SELF);
                MoveObject(OTHER, GetObjectX(OTHER) + UnitRatioX(SELF, OTHER, 7.0), GetObjectY(OTHER) + UnitRatioY(SELF, OTHER, 7.0));
                EnchantOff(SELF, "ENCHANT_INVULNERABLE");
                Damage(SELF, OTHER, CurrentHealth(SELF) + 1, 14);
                Effect("CYAN_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            }
        }
    }
}

void LavaUnitCommonProperties(int unit)
{
    SetCallback(unit, 5, SquareZoneUnitDeath);
    SetCallback(unit, 7, LavaUnitHurt);
    SetCallback(unit, 9, LavaUnitCollide);
    Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
    //SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x2000);
}

void SpawnSquareZoneMob()
{
    int c;

    if (!c)
    {
        c = LavaUnitManager();
    }
    if (GetDirection(c) < 8)
    {
        RhombusPut(1, 3301.0, 3528.0, 1208.0, 1437.0);
        LookWithAngle(c, GetDirection(c) + 1);
        LavaUnitCommonProperties(SpawnSpider(1));
        SetOwner(c, CreateObject("InvisibleLightBlueHigh", 1));
        DrawLavaUnitKills(GetDirection(c));
        FrameTimer(3, SpawnSquareZoneMob);
    }
}

void SquareZoneUnitDeath()
{
    int c = GetOwner(GetTrigger() + 1);

    if (IsObjectOn(c))
    {
        LookWithAngle(c, GetDirection(c) - 1);
        if (!GetDirection(c))
        {
            FireWayBlockSet(1);
            Raise(c, ToFloat(10));
            SecondTimerWithArg(1, c, RespawnSquareZoneTime);
            UniPrintToAll("지금 다음 구간으로 가는 포탈이 열렸습니다, 잠시 후에 다시 닫히므로 서둘러 이동해 주시기 바랍니다");
        }
        DrawLavaUnitKills(GetDirection(c));
    }
    SetCallback(SELF, 9, LavaUnitCollideDummy);
    DeleteObjectTimer(SELF, 90);
    Delete(GetTrigger() + 1);
}

void RespawnSquareZoneTime(int c)
{
    int time = ToInt(GetObjectZ(c));

    if (IsObjectOn(c))
    {
        if (time)
        {
            Raise(c, time - 1);
            SecondTimerWithArg(1, c, RespawnSquareZoneTime);
        }
        else
        {
            FireWayBlockSet(0);
            SpawnSquareZoneMob();
            UniPrintToAll("다시 닫힙니다");
        }
    }
}

int SpawnSpider(int wp)
{
    int unit = CreateObject("SmallAlbinoSpider", wp);

    SetUnitMaxHealth(unit, 500);
    SetUnitVoice(unit, MONSTER_VOICE_WillOWisp);
    return unit;
}

void PolygonHandlerLavaUnitLimitArea()
{
    if (CurrentHealth(OTHER) && IsMonsterUnit(OTHER))
    {
        Effect("LIGHTNING", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(LavaUnitManager()), GetObjectY(LavaUnitManager()));
        MoveObject(OTHER, GetObjectX(LavaUnitManager()), GetObjectY(LavaUnitManager()));
    }
}

void CheckResetSight(int unit, int delay)
{
    if (!HasEnchant(unit, "ENCHANT_DETECTING"))
    {
        Enchant(unit, "ENCHANT_DETECTING", 0.0);
        FrameTimerWithArg(delay, unit, ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.06);
    AggressionLevel(unit, 1.0);
}

void DuringTheMoving(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), dest;

    while (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                if (Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(owner), GetObjectY(owner)) < 31.0)
                {
                    LookWithAngle(ptr, count - 1);
                    FrameTimerWithArg(1, ptr, DuringTheMoving);
                    break;
                }
            }
            else
            {
                dest = ToInt(GetObjectZ(ptr));
                MoveWaypoint(1, GetObjectX(dest), GetObjectY(dest));
                Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                MoveObject(owner, GetWaypointX(1), GetWaypointY(1));
                Effect("TELEPORT", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
                AudioEvent("BlindOff", 1);
            }
            EnchantOff(owner, "ENCHANT_ETHEREAL");
        }
        Delete(ptr);
        Delete(++ptr);
        break;
    }
}

void StartTeleporting()
{
    int unit;

    if (CurrentHealth(OTHER))
    {
        if (HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
        {
            return;
        }
        else
        {
            MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
            unit = CreateObject("InvisibleLightBlueLow", 1);
            SetOwner(OTHER, CreateObject("BlueRain", 1) - 1);
            Raise(unit, GetTrigger() + 1);
            LookWithAngle(unit, 90);
            FrameTimerWithArg(1, unit, DuringTheMoving);
            Enchant(OTHER, "ENCHANT_ETHEREAL", 5.0);
            UniPrint(OTHER, "공간이동을 시작합니다, 중지하려면 이 구역에서 벗어나세요");
        }
    }
}

int LavaTeleport(int wp, int dest)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(CreateObject("InvisibleLightBlueLow", dest) - 1, 10);
    UnitNoCollide(CreateObject("TeleportWake", wp));
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, StartTeleporting);
    Frozen(unit + 2, 1);
    return unit;
}

void InitTeleportMarker()
{
    int ptr = LavaTeleport(89, 91);
    LavaTeleport(90, 92);
    LavaUnitManager();
    FrameTimer(1, ClearWallMaze);
}

int HintForCurPart(int wp, int lv)
{
    int unit = CreateObject("Maiden", wp);

    Frozen(CreateObject("SpinningCrown", wp), 1);
    Frozen(unit, 1);
    LookWithAngle(unit, lv);
    SetDialog(unit, "NORMAL", ShowHintDetails, Nothing);

    return unit;
}

void ShowHintDetails()
{
    int lv = GetDirection(SELF), plr = CheckPlayer();

    if (plr)
    {
        if (CurrentHealth(player[plr]))
        {
            MoveWaypoint(5, GetObjectX(SELF), GetObjectY(SELF));
            MoveObject(RespPoint + plr, GetObjectX(SELF), GetObjectY(SELF));
            Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            AudioEvent("SoulGateTouch", 5);
            UniChatMessage(SELF, HintName(lv), 150);
        }
    }
}

string HintName(int num)
{
    string name[] = {
        "런 버그: 점프 후 즉시 독저항 포션을 사용하세요\n인벤토리를 닫거나 다른 모션을 취하면 풀립니다",
        "충돌크기 버그: 오벨사이에 낀 후 울타리 너머로 달리면 관통할 수 있어요",
        "함성 딜레이버그: 감시광선을 피해 통로를 쭉 가다가 우측에 데몬이 보이면\n함성버그를 쓰고 통로끝으로 계속 달리세요",
        "숨겨진 몬스터 체험 박물관: 몬스터가 드롭하는 3개 색깔의 구슬을 모아서\n각 색상에 맞는 여자에게 버리세요",
        "텔레포트 버그: 텔레포트로 공간이동 직전 점프하세요",
        "버저커차지 정지버그: 버저커차지를 쓰다가 원형범위 안에서 멈추면 되요",
        "썩은고기 버그: 체력 10 이하에서 썩은고기를 먹으면 자신의 체력이 뻥튀기 됩니다\n여기선 '두려움'이라는 오브젝트가 썩은고기입니다",
        "고스트 연타버그: 고스트가 골렘을 치게한 후 지속적으로 오더를 내리면 됩니다",
        "골렘과의 1대 1 사투...: 채크럼 하나 만으로 적을 5 초만에 격추 시켜야 한다",
        "작살과 버저커차지 조합: 용암은 닿기만 해도 즉사합니다(점프불가)\n작살과 버저커차지 만으로 거미를 모두 잡으면 블럭이 열립니다",
        "비밀벽 피격소리 버그: 해머로 땅을 내리쳤을 때 철망소리가 들린다면\n그 앞엔 보이지 않는 벽이 있다는 거에요\n보이지 않는 철망엔 고압전류가 흐르고 있어요",
        "감시광선 뚫기 버그:\n특정 위치에서 버저커차지를 시전하면 감시광선도 피할 수 있다\n(단, 감시광선이 화면에 보이면 안됩니다)",
        "작살 철망 관통버그: 개구리 위치로 철망을 향해 달리면서 작살을 쓰세요\n루비 5개를 모으면 다음포탈로 이동할 수 있어요",
        "작살 벽 관통버그: 오우거는 피 1입니다\n오우거 위치를 기억하셔서 오우거 방향으로 벽을 달리며 작살을 쓰세요",
        "이동속도 버그:\n옷을 입고 이 방을 나가보세요"};

    return name[num];
}

void InitPlaceMapHint()
{
    int k, area[] = {97,98,99,100,101,102,103,104,105,106,107,108,109,110,173,111};
    for (k = 0 ; k < sizeof(area) ; k ++)
        HintForCurPart(area[k], k);
}

int SpawnBall(int wp)
{
    int unit = CreateObject("Maiden", wp);

    CreateObject("InvisibleLightBlueHigh", wp);
    UnitNoCollide(CreateObject("GameBall", wp));
    CreateObject("InvisibleLightBlueHigh", wp); //+3
    CreateObject("InvisibleLightBlueHigh", wp); //+4
    CreateObject("InvisibleLightBlueHigh", wp); //+5
    CreateObject("InvisibleLightBlueHigh", wp); //+6
    LookWithAngle(unit, 0);
    LookWithAngle(unit + 1, 4);
    ObjectOff(unit);
    Damage(unit, 0, CurrentHealth(unit) + 1, -1);
    SetCallback(unit, 9, TouchedBall);
    SetDialog(unit, "NORMAL", ClickBall, ClickBall);
    FrameTimerWithArg(1, unit, LoopCheckingBall);
    return unit;
}

void LoopCheckingBall(int ball)
{
    if (MaxHealth(ball))
    {
        PushObjectTo(ball, UnitAngleCos(ball, GetObjectZ(ball + 1)), UnitAngleSin(ball, GetObjectZ(ball + 1)));
        MoveObject(ball + 2, GetObjectX(ball), GetObjectY(ball));
        FrameTimerWithArg(1, ball, LoopCheckingBall);
    }
}

void BallRemove(int unit)
{
    int k;

    if (MaxHealth(unit))
    {
        for (k = 0 ; k < 7 ; k ++)
            Delete(unit + k);
    }
}

int WallAngleTable(int num)
{
    int angle[4] = {45, 135, 315, 225};

    return angle[num];
}

void TouchedBall()
{
    int wall_t, reflect, c_wall = GetDirection(GetTrigger() + 1);

    if (!GetCaller())
    {
        wall_t = GetWallDirection(GetTrigger());
        if (wall_t != c_wall && wall_t >= 0)
        {
            reflect = (2 * WallAngleTable(wall_t)) - ((DirToAngle(GetDirection(SELF)) + 180) % 360);
            if (reflect < 0) reflect += 360;
            LookWithAngle(SELF, AngleToDir(reflect));
            LookWithAngle(GetTrigger() + 1, wall_t);
            MoveWaypoint(16, GetObjectX(SELF), GetObjectY(SELF));
            AudioEvent("LightningWand", 16);
            Effect("VIOLET_SPARKS", GetWaypointX(16), GetWaypointY(16), 0.0, 0.0);
        }
    }
}

int DirToAngle(int num)
{
    return num * 45 / 32;
}

int AngleToDir(int num)
{
    return num * 32 / 45;
}

int GetWallDirection(int unit)
{
    int res = -1, k;
    float pos_x, pos_y;

    for (k = 0 ; k < 4 ; k ++)
    {
        if (k & 1) pos_x = 20.0;
        else pos_x = -20.0;
        if (k & 2) pos_y = 20.0;
        else pos_y = -20.0;
        MoveObject(unit + k + 3, GetObjectX(unit) + pos_x, GetObjectY(unit) + pos_y);
        if (!IsVisibleTo(unit + k + 3, unit))
            res = k;
        if (res >= 0) break;
    }
    return res;
}

void PutBall()
{
    int unit;
    
    if (MaxHealth(unit))
    {
        UniPrint(OTHER, "공을 던져서 안으로 골인시키기");
        BallRemove(unit);
    }
    else
        unit = SpawnBall(113);
}

void ClickBall()
{
    LookWithAngle(SELF, GetDirection(OTHER));
    LookWithAngle(GetTrigger() + 1, 4);
    Raise(GetTrigger() + 1, 5.0);
}

void GoalBall()
{
    int k;

    if (MaxHealth(OTHER))
    {
        if (!Tvar)
        {
            Tvar = 1;
            for (k = 0 ; k < 8 ; k ++)
                WallOpen(Wall(174 + k, 26 + k));
            UniPrintToAll("골인! 벽이 열립니다");
        }
        ObjectOff(SELF);
    }
}

void GeneratorPull(int wp)
{
    string name = "StoneGolemGenerator";
    int unit = CreateObject(name, wp);

    ObjectOff(CreateObject(name, wp));
    ObjectOff(CreateObject(name, wp + 1));
    ObjectOff(CreateObject(name, wp + 1));
    ObjectOff(unit);
    Frozen(unit, 1);
    Frozen(unit + 1, 1);
    Frozen(unit + 2, 1);
    Frozen(unit + 3, 1);

    DeleteObjectTimer(unit, 6);
    DeleteObjectTimer(unit + 1, 6);
    DeleteObjectTimer(unit + 2, 6);
    DeleteObjectTimer(unit + 3, 6);
    FrameTimerWithArg(44, wp, GeneratorPull);
}

void PlayerHealthFix()
{
    if (CurrentHealth(OTHER) && HasClass(OTHER, "PLAYER"))
    {
        if (CurrentHealth(OTHER) > 150)
        {
            MoveWaypoint(5, GetObjectX(OTHER), GetObjectY(OTHER));
            AudioEvent("SummonAbort", 5);
            UniPrint(OTHER, "체력 복구!");
            Damage(OTHER, 0, CurrentHealth(OTHER) - 150, -1);
        }
    }
}

void GayRoomInit()
{
    CreateObject("VortexSource", 127);
    GayRoomLight();
    SpawnGay(116);
}

int GayRoomLight()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("InvisibleLightBlueHigh", 116);
        Enchant(CreateObject("InvisibleLightBlueHigh", 116), "ENCHANT_RUN", 0.0);
    }
    return unit;
}

int GetLastUnitPtr() { return GetMemory(0x750710); }

int SpawnGay(int wp)
{
    int unit = CreateObject("StoneGolem", wp);
    int ptr = GetLastUnitPtr();

    CreateObject("InvisibleLightBlueHigh", wp);
    SetOwner(GayRoomLight(), unit);
    SetUnitMaxHealth(unit, 1500);
    SetMemory(ptr + 4, 1391);
    SetUnitVoice(unit, 26);
    SetCallback(unit, 7, GayHurt);
    SetCallback(unit, 3, GaySightEvent);

    return unit;
}

void GaySightEvent()
{
    if (!IsVisibleTo(GayRoomLight(), SELF))
    {
        MoveWaypoint(5, GetObjectX(SELF), GetObjectY(SELF));
        DeleteObjectTimer(CreateObject("GreenPuff", 5), 20);
        AudioEvent("CharmFailure", 5);
        AudioEvent("Maiden1Die", 5);
        Effect("LIGHTNING", GetObjectX(GayRoomLight()), GetObjectY(GayRoomLight()), GetObjectX(SELF), GetObjectY(SELF));
        MoveObject(SELF, GetObjectX(GayRoomLight()), GetObjectY(GayRoomLight()));
        UniChatMessage(SELF, "이리와, 나는 여길 벗어날 수 없다구~~", 120);
    }
    if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
    {
        Enchant(SELF, "ENCHANT_DETECTING", 0.0);
        FrameTimerWithArg(60, GetTrigger(), ResetUnitSight);
    }
}

void GayHurt()
{
    int ptr;

    MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
    if (MaxHealth(SELF) - CurrentHealth(SELF) >= 150)
    {
        if (GetCaller() ^ ToInt(GetObjectZ(GetTrigger() + 1)))
        {
            LookWithAngle(GetTrigger() + 1, 0);
            Raise(GetTrigger() + 1, ToFloat(GetCaller()));
            UniChatMessage(OTHER, "1 연타!", 120);
        }
        else
        {
            LookWithAngle(GetTrigger() + 1, GetDirection(GetTrigger() + 1) + 1);
            UniChatMessage(OTHER, IntToString(GetDirection(GetTrigger() + 1)) + " 연타!!", 120);
            if (GetDirection(GetTrigger() + 1) > 4)
            {
                EnchantOff(OTHER, "ENCHANT_AFRAID");
                MoveObject(OTHER, LocationX(127), LocationY(127));
                UniChatMessage(OTHER, IntToString(GetDirection(GetTrigger() + 1)) + " 연타 성공!!", 120);
            }
        }
        MoveWaypoint(5, GetObjectX(SELF), GetObjectY(SELF));
        if (IsObjectOn(ptr))
            Delete(ptr);
        ptr = CreateObject("InvisibleLightBlueHigh", 5);
        LookWithAngle(ptr, 40);
        SetOwner(SELF, ptr);
        Effect("SPARK_EXPLOSION", LocationX(5), LocationY(5), 0.0, 0.0);
        AudioEvent("BurnCast", 5);
        FrameTimerWithArg(1, ptr, ResetVaildHitTime);
    }
    RestoreHealth(SELF, MaxHealth(SELF) - CurrentHealth(SELF));
}

void ResetVaildHitTime(int ptr)
{
    if (IsObjectOn(ptr))
    {
        if (GetDirection(ptr))
            LookWithAngle(ptr, GetDirection(ptr) - 1);
        else
        {
            Raise(GetOwner(ptr) + 1, ToFloat(0));
            Delete(ptr);
            UniChatMessage(GetOwner(ptr), "콤보 유효시간 끝남", 120);
        }
        FrameTimerWithArg(3, ptr, ResetVaildHitTime);
    }
}

void GayRoomWallOpen()
{
    int k;

    ObjectOff(SELF);
    for (k = 0 ; k < 3 ; k ++)
    {
        WallOpen(Wall(190 + k, 28 - k));
        WallOpen(Wall(193 + k, 31 - k));
    }
    GayRoomInit();
    UniPrint(OTHER, "\"게이\" 룸 벽이 열린다...");
}

void MapExit()
{
    RemoveCoopTeamMode();
    MusicEvent();
}

void MapDecorations()
{
    HapBugMUnit = CreateObject("InvisibleLightBlueHigh", 133);

    Frozen(CreateObject("DunMirScaleTorch2", 133), 1);
    FrameTimerWithArg(30, HapBugMUnit, HarpoonBugInit);
    LavaTeleport(136, 135);
    FrameTimer(200, FlagDecorations);
}

void MapMusicLoop(int ptr)
{
    int count, misc;

    if (IsObjectOn(ptr))
    {
        count = ToInt(GetObjectZ(ptr));
        misc = MapBgmTable(GetDirection(ptr));
        if (count < (misc & 0xffff))
            Raise(ptr, ToFloat(count + 1));
        else
        {
            misc = Random(0, 7);
            LookWithAngle(ptr, misc);
            Raise(ptr, ToFloat(0));
            Music(MapBgmTable(misc) >> 16, 100);
        }
        SecondTimerWithArg(1, ptr, MapMusicLoop);
    }
    else
        MusicEvent();
}

int MapBgmTable(int idx)
{
    int arr[8];

    if (!arr[0])
    {
        arr[0] = 236 | (16 << 16); //town2
        arr[1] = 244 | (21 << 16); //wander1
        arr[2] = 274 | (22 << 16); //wander2
        arr[3] = 181 | (23 << 16); //wander3
        arr[4] = 54 | (29 << 16); //wander4
        arr[5] = 99 | (24 << 16); //credit
        arr[6] = 32 | (4 << 16);
        arr[7] = 112 | (25 << 16);
        return 0;
    }
    return arr[idx];
}

void SpawnBerserkerZoneUnit()
{
    int unit = CreateObject("WeirdlingBeast", 87);

    SetUnitMaxHealth(unit, 100);
    Damage(unit, 0, MaxHealth(unit) + 1, 14);
    SetCallback(unit, 9, CollideEventWithSpawnMobs);
}

void CollideEventWithSpawnMobs()
{
    if (IsPlayerUnit(OTHER))
    {
        Delete(SELF);
        FrameTimer(30, SpawnSquareZoneMob);
    }
}

void ClearWallMaze()
{
    WallGroupOpen(1);
}

void GiveHammer()
{
    int arr[10], i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (!IsObjectOn(arr[i]))
        {
            arr[i] = CreateObjectAt("WarHammer", GetObjectX(OTHER), GetObjectY(OTHER));
            Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            break;
        }
    }
}

void FrogKillHandler(int bUnit, int victim)
{
    float vectX = UnitRatioX(victim, bUnit, 180.0), vectY = UnitRatioY(victim, bUnit, 180.0);
    int unit;

    MoveWaypoint(1, GetObjectX(victim) + vectX, GetObjectY(victim) + vectY);
    unit = CreateObject("Ruby", 1);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    DeleteObjectTimer(unit, 300);
    Effect("GREATER_HEAL", GetWaypointX(1), GetWaypointY(1), GetObjectX(victim), GetObjectY(victim));
    Effect("DRAIN_MANA", GetWaypointX(1), GetWaypointY(1), GetObjectX(victim), GetObjectY(victim));
    Effect("CHARM", GetWaypointX(1), GetWaypointY(1), GetObjectX(victim), GetObjectY(victim));
}

void FrogDeathHandler()
{
    FrogKillHandler(ToInt(GetObjectZ(GetTrigger() + 1)), SELF);
    FrameTimerWithArg(64, GetObjectZ(GetTrigger() + 1), PlacingFrogs);
    Effect("SMOKE_BLAST", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    DeleteObjectTimer(SELF, 60);
    Delete(GetTrigger() + 1);
}

void PlacingFrogs(int bUnit)
{
    float fSize = RandomFloat(30.0, 120.0);
    int angle = Random(0, 180) * 2;
    int unit = CreateObjectAt("GreenFrog", GetObjectX(bUnit) + MathSine(angle + 90, fSize), GetObjectY(bUnit) + MathSine(angle, fSize));

    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), bUnit);
    SetUnitMaxHealth(unit, 1);
    LookAtObject(unit, bUnit);
    LookWithAngle(unit, GetDirection(unit) + 128);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    SetCallback(unit, 5, FrogDeathHandler);
}

void PlacingFrogsAll(int bUnit, int amount)
{
    int i;

    for (i = amount ; i ; i --)
        PlacingFrogs(bUnit);
}

int CheckHasRubyCount(int unit)
{
    int cur = GetLastItem(unit), res = 0;

    while (cur)
    {
        if (GetUnitThingID(cur) == 2797) //Ruby
            res ++;
        cur = GetPreviousItem(cur);
    }
    return res;
}

void RemoveAllHasRuby(int unit)
{
    int cur = GetLastItem(unit), del;

    while (cur)
    {
        if (GetUnitThingID(cur) == 2797) del = cur; //Ruby
        else del = 0;
        cur = GetPreviousItem(cur);
        if (del) Delete(del);
    }
}

void FrogZoneExit()
{
    if (CheckHasRubyCount(OTHER) >= 5)
    {
        RemoveAllHasRuby(OTHER);
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER, 1345.0, 2035.0);
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
    else
        UniPrint(OTHER, "루비 개수가 부족해... 조금 더 모아와! 포탈을 타려면 적어도 5개가 필요하단 말이지");
}

void FrogZoneInit()
{
    ObjectOff(SELF);
    PlacingFrogsAll(FrogCenter, 12);
}

void HarpoonBugNextWallsHandler(int stat)
{
    int i;

    if (stat)
    {
        for (i = 0 ; i < 4 ; i ++)
            WallClose(Wall(230 - i, 64 + i));
    }
    else
    {
        for (i = 0 ; i < 4 ; i ++)
            WallOpen(Wall(230 - i, 64 + i));
        SecondTimerWithArg(3, 1, HarpoonBugNextWallsHandler);
        UniPrintToAll("이곳을 나가는 출구가 열렸습니다. 잠시 후 다시 닫히게 되므로 서두르세요");
    }
}

void HarpoonBugUnitDeath()
{
    int mUnit = GetOwner(GetTrigger() + 1), count;

    if (IsObjectOn(mUnit))
    {
        count = GetDirection(mUnit) - 1;
        LookWithAngle(mUnit, count);
        if (!count)
        {
            FrameTimerWithArg(30, 0, HarpoonBugNextWallsHandler);
            SecondTimerWithArg(5, mUnit, HarpoonBugInit);
        }
    }
    DeleteObjectTimer(SELF, 30);
    Delete(GetTrigger() + 1);
    UniChatMessage(SELF, "으앙ㅠㅠ 주금..", 90);
}

void HarpoonBugUnitCollide()
{
    if (CurrentHealth(SELF))
    {
        if (IsMissileUnit(OTHER))
        {
            Damage(SELF, 0, CurrentHealth(SELF) + 1, -1);
            Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
    }
}

int HarpoonBugUnit(int wp, int mUnit)
{
    int unit = CreateObject("OgreWarlord", wp);

    Enchant(CreateObject("InvisibleLightBlueHigh", wp) - 1, "ENCHANT_FREEZE", 0.0);
    SetOwner(mUnit, unit + 1);
    LookWithAngle(mUnit, GetDirection(mUnit) + 1);
    AggressionLevel(unit, 0.0);
    SetCallback(unit, 9, HarpoonBugUnitCollide);
    SetCallback(unit, 5, HarpoonBugUnitDeath);
    LookWithAngle(unit, 32);

    return unit;
}

void HarpoonBugInit(int mUnit)
{
    HarpoonBugUnit(131, mUnit);
    HarpoonBugUnit(132, mUnit);
    HarpoonBugUnit(134, mUnit);
}

void FlagPickupNotWork()
{
    string ment[] =
    {
        "여기까지 오다니..!",
        "나는 당신의 승리를 정말로 축하해요!",
        "드디어 날 구하러 오셨군요 용감한 방랑자님",
        "MISSING.c_str: NULL",
        "이곳은 당신을 위해 마련한 명예의 전당입니다",
        "성 알리스토의 교회에 온것을 환영합니다. 이곳은 휴식과 명상을 위한 안전한 피신처가 되어줄 것이네!",
        "당신이 있는한 오늘도 녹스월드는 안전합니다",
        "그리워하면~ 언젠가 만나게되는~ 어느 영화와 같은 일들이 이뤄져 가기를~~~",
        "힘겨워한 날에 너를 지킬수 없었던~ 아름다운 시절속에 머문 그대이기에~"
    };
    UniPrint(OTHER, ment[Random(0, 8)]);
}

void PlacingColorFlag(int wp, int color)
{
    string flagname[] = {"AzureFlag", "BlueFlag", "BlackFlag", "WhiteFlag", "YellowFlag", "GreenFlag",
        "RedFlag", "VioletFlag", "Flag"};
    int flagunit = CreateObjectAt(flagname[color], LocationX(wp), LocationY(wp));

    UnitNoCollide(flagunit);
    SetUnitCallbackOnPickup(flagunit, FlagPickupNotWork);
}

void FlagDecorations()
{
    int i;

    for (i = 7 ; i >= 0 ; i --)
        PlacingColorFlag(137 + i, i);
}

int SummonLichLord(float xProfile, float yProfile)
{
    int lich = CreateObjectAt("LichLord", xProfile, yProfile);

    LichLordSubProcess(lich);
    return lich;
}

int SummonRedWizard(float xProfile, float yProfile)
{
    int wiz = CreateObjectAt("WizardRed", xProfile, yProfile);

    WizardRedSubProcess(wiz);
    return wiz;
}

void InitHiddenPlace()
{
    ObjectOff(SELF);
    int sword = CreateObjectAt("GreatSword", LocationX(151), LocationY(151));

    SetWeaponProperties(sword, 3, 3, Random(0, 36), Random(0, 36));
    int potion = CreateObjectAt("RedPotion", LocationX(152), LocationY(152));
    int sword2 = CreateObjectAt("GreatSword", LocationX(153), LocationY(153));

    SetWeaponProperties(sword2, 3, 3, Random(0, 36), Random(0, 36));

    int u;
    for (u = 0 ; u < 5 ; u += 1)
        CreateObjectAt("RedPotion", LocationX(u + 161), LocationY(u + 161));
}

int SpawnMons(int locationId, int target)
{
    int mob;

    if (Random(0, 1))
        mob = SummonLichLord(LocationX(locationId), LocationY(locationId));
    else
    {
        mob = SummonRedWizard(LocationX(locationId), LocationY(locationId));
    }
    SetUnitScanRange(mob, 300.0);
    Effect("SMOKE_BLAST", GetObjectX(mob), GetObjectY(mob), 0.0, 0.0);
    if (IsVisibleTo(target, target))
    {
        LookAtObject(mob, target);
        UniChatMessage(mob, "네이놈 관리자도 아닌놈이 들어오다니?!\n그럼 죽어야지 뭐 우짜겠써", 120);
    }
    return mob;
    
}

void SurpriseMonsters()
{
    int execOnce;

    ObjectOff(SELF);
    if (execOnce == FALSE)
    {
        execOnce = TRUE;
        int u;

        for (u = 0 ; u < 6 ; u ++)
            SpawnMons(u + 155, OTHER);
    }
}

void SurpiseSecret2()
{
    int execOnce;

    ObjectOff(SELF);
    if (execOnce == FALSE)
    {
        execOnce = TRUE;
        int u;
        for (u = 0 ; u < 10 ; u += 1)
        {
            CreateObjectAt("RedPotion", LocationX(168), LocationY(168));
            TeleportLocationVector(168, 23.0, -23.0);
        }
        SummonRedWizard(LocationX(166), LocationY(166));
        SummonRedWizard(LocationX(166), LocationY(166));
        SummonLichLord(LocationX(167), LocationY(167));
        SummonLichLord(LocationX(167), LocationY(167));
    }
}

void applySpeed()
{
    Enchant(OTHER, "ENCHANT_HASTED", 6.0);
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);
    
    // if (pUnit)
    //     ShowQuestIntroOne(pUnit, 237, "NoxWorldMap", "Noxworld.wnd:NotReady");

    if (pInfo==0x653a7c)
    {
        PlayerClassCommonWhenEntry();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

