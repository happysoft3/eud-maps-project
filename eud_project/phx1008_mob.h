
#include "phx1008_utils.h"
#include "libs/queueTimer.h"
#include "libs/voiceList.h"

int SwordsmanBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919907667; arr[1] = 1634562916; arr[2] = 110; arr[17] = 250; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1106247680; arr[29] = 18; arr[59] = 5542784; arr[60] = 1346; arr[61] = 46900480; 
	pArr = arr;
	return pArr;
}

void SwordsmanSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 250;	hpTable[1] = 250;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = SwordsmanBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onTrollSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 20, DAMAGE_TYPE_CRUSH);
        }
    }
}

void FallenStone(int stone)
{
    while (IsObjectOn(stone))
    {
        if (GetObjectZ(stone) > 0.0)
        {
            PushTimerQueue(1, stone, FallenStone);
            Raise(stone, GetObjectZ(stone) - 10.0);
            break;
        }
        DeleteObjectTimer(CreateObjectAt("GreenSmoke", GetObjectX(stone), GetObjectY(stone)), 12);
        SplashDamageAtEx(GetOwner(stone),GetObjectX(stone),GetObjectY(stone),48.0,onTrollSplash);
        Delete(stone);
        break;
    }
}

void TrollOnStrike()
{
    if (CurrentHealth(OTHER))
    {
        int dropstone = CreateObjectAt("CaveBoulders", GetObjectX(OTHER), GetObjectY(OTHER));

        UnitNoCollide(dropstone);
        Frozen(dropstone, TRUE);
        Raise(dropstone, 220.0);
        SetOwner(SELF,dropstone);
        FrameTimerWithArg(1, dropstone, FallenStone);
    }
    Effect("JIGGLE", GetObjectX(SELF), GetObjectY(SELF), 7.0, 0.0);
}

int TrollBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1819243092; arr[1] = 108; arr[17] = 325; arr[19] = 65; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 7; arr[27] = 4; 
		arr[28] = 1103626240; arr[29] = 40; arr[30] = 1109393408; arr[31] = 11; arr[32] = 20; 
		arr[33] = 30; arr[58] = 5546608; arr[59] = 5542784; arr[60] = 2273; arr[61] = 46912256; 
	pArr = arr;
    CustomMeleeAttackCode(arr, TrollOnStrike);
	return pArr;
}

void TrollSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073322393;		ptr[137] = 1073322393;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = TrollBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int UrchinBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347797; arr[1] = 28265; arr[17] = 135; arr[19] = 75; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[37] = 1869768788; arr[38] = 1735289207; 
		arr[39] = 1852798035; arr[40] = 101; arr[53] = 1128792064; arr[54] = 4; arr[55] = 9; 
		arr[56] = 17; arr[60] = 1339; arr[61] = 46904832; 
	pArr = arr;
	return pArr;
}

void UrchinSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074790400;		ptr[137] = 1074790400;
	int *hpTable = ptr[139];
	hpTable[0] = 135;	hpTable[1] = 135;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = UrchinBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonMobUrchin(int posUnit)
{
    int mob = CreateObjectAt("Urchin", GetObjectX(posUnit), GetObjectY(posUnit));

    UrchinSubProcess(mob);
    SetUnitVoice(mob,MONSTER_VOICE_Necromancer);
    return mob;
}

int WaspBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1886609751; arr[17] = 64; arr[19] = 90; arr[21] = 1065353216; arr[23] = 32769; 
		arr[24] = 1065353216; arr[27] = 5; arr[28] = 1101004800; arr[29] = 10; arr[31] = 3; 
		arr[34] = 30; arr[35] = 10; arr[36] = 50; arr[59] = 5544320; arr[60] = 1331; 
		arr[61] = 46900736; 
	pArr = arr;
	return pArr;
}

void WaspSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 64;	hpTable[1] = 64;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = WaspBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonMobBee(int posUnit)
{
    int bee = CreateObjectAt("Wasp", GetObjectX(posUnit), GetObjectY(posUnit));

    WaspSubProcess(bee);
    return bee;
}

void onBillyOniSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 50, DAMAGE_TYPE_CRUSH);
        }
    }
}

void OnBillyOniAttack()
{
    if (CurrentHealth(OTHER))
    {
        int fx = CreateObjectById(OBJ_BIG_SMOKE, GetObjectX(OTHER), GetObjectY(OTHER));

        DeleteObjectTimer(fx,9);
        SplashDamageAtEx(SELF,GetObjectX(OTHER),GetObjectY(OTHER),60.0,onBillyOniSplash);
        Effect("JIGGLE", GetObjectX(SELF), GetObjectY(SELF), 14.0, 0.0);
    }
}

int BearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[17] = 360; arr[19] = 50; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1069547520; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1101004800; 
		arr[29] = 91; arr[30] = 1114636288; arr[31] = 2; arr[32] = 13; arr[33] = 21; 
		arr[59] = 5542784; arr[60] = 1365; arr[61] = 46903040; 
	pArr = arr;
    CustomMeleeAttackCode(arr, OnBillyOniAttack);
	return pArr;
}

void BearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = 360;	hpTable[1] = 360;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = BearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonMobBear(int posUnit)
{
    int mob = CreateObjectById(OBJ_BEAR, GetObjectX(posUnit), GetObjectY(posUnit));

    BearSubProcess(mob);
    SetUnitVoice(mob, MONSTER_VOICE_StoneGolem);
    return mob;
}

int SummonMobWolf(int posUnit)
{
    int mob = CreateObjectById(OBJ_WOLF, GetObjectX(posUnit), GetObjectY(posUnit));

    SetUnitMaxHealth(mob, 225);
    RetreatLevel(mob,0.0);
    SetUnitVoice(mob,MONSTER_VOICE__MaleNPC2);
    return mob;
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 210; arr[19] = 90; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; 
		arr[26] = 4; arr[28] = 1112014848; arr[29] = 40; arr[30] = 1106247680; arr[31] = 2; 
		arr[32] = 13; arr[33] = 21; arr[58] = 5546320; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 210;	hpTable[1] = 210;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonMobBunnyGirl(int posUnit)
{
    int mob = CreateObjectById(OBJ_WOUNDED_APPRENTICE, GetObjectX(posUnit), GetObjectY(posUnit));

    WoundedApprenticeSubProcess(mob);
    SetUnitVoice(mob,MONSTER_VOICE__Maiden1);
    return mob;
}

int GreenFrogBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[17] = 1; arr[18] = 1; 
		arr[19] = 72; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1112014848; arr[29] = 30; arr[32] = 18; arr[33] = 22; arr[59] = 5542784; 
		arr[60] = 1313; arr[61] = 46905856; 
	pArr = arr;
	return pArr;
}

void GreenFrogSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074412912;		ptr[137] = 1074412912;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GreenFrogBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonMobGreenFrog(int posUnit)
{
    int mob = CreateObjectAt("GreenFrog", GetObjectX(posUnit), GetObjectY(posUnit));

    GreenFrogSubProcess(mob);
    return mob;
}

int SummonMobSwordsman(int posUnit)
{
    int mob = CreateObjectAt("Swordsman", GetObjectX(posUnit), GetObjectY(posUnit));

    SwordsmanSubProcess(mob);
    SetUnitVoice(mob,MONSTER_VOICE__Maiden2);
    return mob;
}

int SummonMobTroll(int posUnit)
{
    int mob = CreateObjectAt("Troll", GetObjectX(posUnit), GetObjectY(posUnit));

    TrollSubProcess(mob);
    return mob;
}

int SummonMobOgrelord(int posUnit)
{
    int ogre=CreateObjectAt("OgreWarlord", GetObjectX(posUnit), GetObjectY(posUnit));

    SetUnitMaxHealth(ogre, 325);
    return ogre;
}

int SummonMobArcher(int posUnit)
{
    int mob = CreateObjectAt("Archer", GetObjectX(posUnit), GetObjectY(posUnit));

    SetUnitMaxHealth(mob, 135);
    SetUnitVoice(mob,MONSTER_VOICE__Maiden1);
    return mob;
}

int WoundedApprenticeBinTable2()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 180; arr[19] = 100; arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1065353216; 
		arr[26] = 4; arr[28] = 1114636288; arr[29] = 10; arr[31] = 3; arr[32] = 8; 
		arr[33] = 14; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void WoundedApprenticeSubProcess2(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 180;	hpTable[1] = 180;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = WoundedApprenticeBinTable2();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonMobBrownFox(int posUnit)
{
    int mob = CreateObjectById(OBJ_WOUNDED_CONJURER, GetObjectX(posUnit), GetObjectY(posUnit));

    WoundedApprenticeSubProcess2(mob);
    SetUnitVoice(mob,MONSTER_VOICE__Maiden1);
    return mob;
}

void InitMonsterTable(int *destFunction, int *destLength)
{
    int *pTable, length;

    if (!pTable)
    {
        int mobTable[] = {0, SummonMobSwordsman, SummonMobUrchin, SummonMobArcher, 
        SummonMobBear, SummonMobBunnyGirl, SummonMobOgrelord, SummonMobTroll, 
        SummonMobBee, SummonMobWolf,SummonMobBrownFox,};

        length = sizeof(mobTable);
        pTable = mobTable;
    }
    destFunction[0] = pTable;
    destLength[0] = length;
}

