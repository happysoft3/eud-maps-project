
#include "libs/define.h"

#define MAX_PLAYER_COUNT 32

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_HARPOON 32
#define PLAYER_FLAG_THREADLIGHTLY 64
#define PLAYER_FLAG_ADVANCED_HARPOON 128
#define PLAYER_FLAG_ALL_ENCHANT 256

#define ABILITY_ID_BERSERKER_CHARGE 	1
#define ABILITY_ID_WARCRY 			2
#define ABILITY_ID_HARPOON 			3
#define ABILITY_ID_TREAD_LIGHTLY		4
#define ABILITY_ID_EYE_OF_WOLF		5

void QueryMainHall(int *get, int set){}//virtual
void QueryPlayerLastPos(int *get, int n, int set){}//virtual
void QueryDungeonPortal(int *get, int n, int set){}//virtual

