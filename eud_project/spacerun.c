//@ignore_object_type

#include "libs/define.h"
#include "libs/format.h"
#include "libs/printutil.h"
#include "libs/playerinfo.h"
#include "libs/waypoint.h"
#include "libs/unitstruct.h"
#include "libs/itemproperty.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/bind.h"
#include "libs/buff.h"
#include "libs/indexloop.h"
#include "libs/playerupdate.h"
#include "libs/reaction.h"
#include "libs/weapon_effect.h"

#define PLAYER_DEATH_FLAG 0x80000000

int CUR_INDEX = 0;
int RND_BOX[7];
int LESSONS = 0;
int CENTER = 0;
int P_SCORE[10];
float P_SPEED[10];
int m_player[10];
int m_pFlag[10];
short m_pDeath[10];
string *m_summonUnitNameArray;
int m_summonUnitNameArrayLength;
string *m_weaponNameArray;
int m_weaponNameArrayLength;
string *m_statueNameArray;
    int m_statueNameArrayLength;

int *m_pWeaponUserSpecialProperty1;
int *m_pWeaponUserSpecialPropertyExecutor1;
int *m_pWeaponUserSpecialPropertyFxCode1;

int *m_pWeaponUserSpecialProperty2;
int *m_pWeaponUserSpecialPropertyExecutor2;
int *m_pWeaponUserSpecialPropertyFxCode2;

int *m_pWeaponUserSpecialProperty3;
int *m_pWeaponUserSpecialPropertyExecutor3;
int *m_pWeaponUserSpecialPropertyFxCode3;

int *m_pWeaponUserSpecialProperty4;
int *m_pWeaponUserSpecialPropertyExecutor4;
int *m_pWeaponUserSpecialPropertyFxCode4;

int *m_pWeaponUserSpecialProperty5;
int *m_pWeaponUserSpecialPropertyExecutor5;
int *m_pWeaponUserSpecialPropertyFxCode5;

    void UnitVisibleSplashA()
{
    int parent = GetOwner(SELF);
    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (CurrentHealth(GetOwner(parent)))
    {
        if (GetUnit1C(OTHER) ^ spIdx)
        {
            if (DistanceUnitToUnit(SELF, OTHER) <= GetObjectX(parent))
            {
                Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
                SetUnit1C(OTHER, spIdx);
            }
        }
    }
}

void SplashDamageAt(int owner, int dam, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, SplashIdx;

    SplashIdx ++;
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(dam));

    int k = -1;
    while (++k < 4)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 64);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplashA);
    }
    DeleteObjectTimer(ptr - 1, 2);
    DeleteObjectTimer(ptr - 2, 2);
}

static void earthquakeHammer()
{
    float xpos = GetObjectX(OTHER), ypos = GetObjectY(OTHER);

    SplashDamageAt(OTHER, 100, xpos, ypos, 165.0);
    GreenExplosion(xpos, ypos);
    PlaySoundAround(OTHER, SOUND_WillOWispDie);
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	FrameTimerWithArg(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 135, DAMAGE_TYPE_ELECTRIC);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.1);
            Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
    }
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 19.0), yvect = UnitAngleSin(OTHER, 19.0);
    int arr[30], u=0;

    arr[u] = DummyUnitCreateAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateAt("Archer", GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    DrawYellowLightningFx(GetObjectX(arr[0]), GetObjectY(arr[0]), GetObjectX(arr[u-1]), GetObjectY(arr[u-1]), 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}

void OnIcecrystalCollide()
{
    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(GetTrigger()+1);

            if (!IsAttackedBy(OTHER, owner))
                break;

            Damage(OTHER, owner, 120, DAMAGE_TYPE_ELECTRIC);
        }
        else if (GetCaller())
            break;

        wispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(GetTrigger() + 1);
        Delete(SELF);
        break;
    }
}

int CreateIceCrystal(int owner, float gap, int lifetime)
{
    int cry=CreateObjectAt("GameBall", GetObjectX(owner) + UnitAngleCos(owner, gap), GetObjectY(owner) + UnitAngleSin(owner, gap));

    SetOwner(owner, CreateObjectAt("ImaginaryCaster", GetObjectX(cry), GetObjectY(cry)));
    SetUnitCallbackOnCollide(cry, OnIcecrystalCollide);
    DeleteObjectTimer(cry, lifetime);
    DeleteObjectTimer(cry+1, lifetime + 15);
    return cry;
}

void CUserWeaponShootIceCrystal()
{
    PushObject(CreateIceCrystal(OTHER, 19.0, 75), 22.0, GetObjectX(OTHER), GetObjectY(OTHER));
    wispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(OTHER, SOUND_LightningWand);
    PlaySoundAround(OTHER, SOUND_ElevLOTDDown);
}

static void wispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

static void dropRainSingle(float xpos, float ypos)
{
    int angle = Random(0, 359);
        float    rgap=RandomFloat(10.0, 200.0);
        int    arw= CreateObjectAt("CherubArrow", xpos+MathSine(angle+90, rgap),ypos+MathSine(angle,rgap));
            LookWithAngle(arw, 64);
            UnitNoCollide(arw);
            Raise(arw, 250.0);
            DeleteObjectTimer(arw, 20);
            PlaySoundAround(arw, SOUND_CrossBowShoot);
}

static void arrowRainLoop(int sub)
{
    float xpos=GetObjectX(sub),ypos=GetObjectY(sub);
    int dur;

    while (ToInt(xpos))
    {
        dur=GetDirection(sub);
        if (dur)
        {
            dropRainSingle(xpos, ypos);
            if (dur&1)
                SplashDamageAt(GetOwner(sub), 15, xpos, ypos, 200.0);
            FrameTimerWithArg(1, sub, arrowRainLoop);
            LookWithAngle(sub, dur-1);
            break;
        }
        wispDestroyFX(xpos, ypos);
        Delete(sub);
        break;
    }
}

static void startArrowRain()
{
    int ix,iy;

    if(! GetPlayerMouseXY(OTHER, &ix,&iy))
        return;
    
    float x=IntToFloat(ix), y=IntToFloat(iy);

    if (!checkCoorValidate(x,y))
        return;

    int sub=CreateObjectAt("PlayerWaypoint", x,y);

    FrameTimerWithArg(1, sub, arrowRainLoop);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, 60);
    PlaySoundAround(sub, SOUND_MetallicBong);
}

int FallingMeteor(float sX, float sY, int sDamage, float sSpeed)
{
    int mUnit = CreateObjectAt("Meteor", sX, sY);
    int *ptr = UnitToPtr(mUnit);

    if (ptr)
    {
        int *pEC=ptr[187];

        pEC[0]=sDamage;
        ptr[5]|=0x20;
        Raise(mUnit, 255.0);
        ptr[27]=sSpeed;
    }
    return mUnit;
}

static int CheckCoorValidate(float x, float y)
{
    if (x<100.0)
        return FALSE;
    if (y<100.0)
        return FALSE;
    if (x>5750.0)
        return FALSE;
    if (y>5750.0)
        return FALSE;
    return TRUE;
}

void FallenMeteorSwordTriggered()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int ixpos, iypos;

    if (!GetPlayerMouseXY(OTHER, &ixpos, &iypos))
        return;
    float xpos = IntToFloat(ixpos), ypos = IntToFloat(iypos);

    if (!CheckCoorValidate(xpos, ypos))
        return;

    int test=CreateObjectAt("ImaginaryCaster", xpos, ypos);

    if (IsVisibleTo(OTHER, test))
    {
        SetOwner(OTHER, FallingMeteor(xpos, ypos, 220, -8.0));
        PlaySoundAround(OTHER, SOUND_MeteorCast);
    }
    else
    {
        UniPrint(OTHER, "마우스 커서 지역은 볼 수 없습니다- 마법 캐스팅 실패입니다");
    }    
    Delete(test);
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 
		link=&arr;
	}
	return link;
}

static void initNameArray()
{
    string weaponNameArray[]={ "WarHammer", "GreatSword", "StaffWooden", "FanChakram", "RoundChakram",
        "WarHammer", "BattleAxe"};

    m_weaponNameArray=&weaponNameArray;
    m_weaponNameArrayLength=sizeof(weaponNameArray);

    string mobNameArray[]= {"MechanicalGoleM", "HorrendouS", "FireSpritE", "StoneGoleM", "CarnivorousPlanT",
    "OgreBrutE", "SkeletonLorD", "EmberDemoN", "SwordsmaN", "FlyingGoleM"};
    m_summonUnitNameArray=&mobNameArray;
    m_summonUnitNameArrayLength=sizeof(mobNameArray);

    string statueNameArray[] = {"MovableStatue2B", "MovableStatue2A", "MovableStatue2H", "MovableStatue2G", "MovableStatue2F", "MovableStatue2E",
     "MovableStatue2D", "MovableStatue2C",
    "MovableStatuE2B"};
    m_statueNameArray=&statueNameArray;
    m_statueNameArrayLength=sizeof(statueNameArray);
}

void DelayRun(int angle)
{
    PlayerPusher(0);
    PlayerGrp();
    TeleportHostPlayerForSelectLessons(GetHost());
    GetMaster();
    MoveWaypoint(10, GetWaypointX(1) + MathSine(angle + 90 + 5, 1650.0), GetWaypointY(1) + MathSine(angle + 5, 1650.0));
    MoveWaypoint(11, GetWaypointX(1) + MathSine(angle + 90 - 6, 1650.0), GetWaypointY(1) + MathSine(angle - 6, 1650.0));
    StrGoArrowImage();
    FrameTimer(1, StrFinish);
    FrameTimer(2, PutItemRespawn);
}

void MapExit()
{
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
}

static void initMapReadable()
{
    RegistSignMessage(Object("mpic1"), "목표점수를 선택하여 주십시오");
}

static void initSpecialWeaponProperties()
{
    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty1);
    SpecialWeaponPropertyExecuteScriptCodeGen(earthquakeHammer, &m_pWeaponUserSpecialPropertyExecutor1);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyFxCode1);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty1, m_pWeaponUserSpecialPropertyExecutor1);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty2);
    SpecialWeaponPropertyExecuteScriptCodeGen(startArrowRain, &m_pWeaponUserSpecialPropertyExecutor2);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyFxCode2);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty2, m_pWeaponUserSpecialPropertyExecutor2);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_YELLOW_SPARKS, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty3);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &m_pWeaponUserSpecialPropertyExecutor3);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyFxCode3);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty3, m_pWeaponUserSpecialPropertyExecutor3);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_PLASMA_SPARK, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty4);
    SpecialWeaponPropertyExecuteScriptCodeGen(CUserWeaponShootIceCrystal, &m_pWeaponUserSpecialPropertyExecutor4);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyFxCode4);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty4, m_pWeaponUserSpecialPropertyExecutor4);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty5);
    SpecialWeaponPropertyExecuteScriptCodeGen(FallenMeteorSwordTriggered, &m_pWeaponUserSpecialPropertyExecutor5);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyExecutor5);
}

void MapInitialize()
{
    int angle = 210;

    MusicEvent();
    initNameArray();
    initMapReadable();
    initSpecialWeaponProperties();
    FlagGrp(-1);
    SummonUnitHealth(-1);
    //init_exec
    CENTER = CreateObject("InvisibleLightBlueHigh", 1);
    MoveWaypoint(4, GetWaypointX(1) + MathSine(angle + 90, 1650.0), GetWaypointY(1) + MathSine(angle, 1650.0));
    MoveWaypoint(8, GetWaypointX(1) + MathSine(angle - 6 + 90, 1500.0), GetWaypointY(1) + MathSine(angle - 6, 1500.0));
    MoveWaypoint(9, GetWaypointX(1) + MathSine(angle - 6 + 90, 1800.0), GetWaypointY(1) + MathSine(angle - 6, 1800.0));

    //delay_run
    FrameTimerWithArg(6, angle, DelayRun);
    FrameTimer(5, LoopDrawFinishLine);
    FrameTimerWithArg(1, 1500.0, PutInnerRing);
    FrameTimerWithArg(1, 1800.0, PutInnerRing);
    FrameTimerWithArg(2, angle, DrawStartLine);
    FrameTimerWithArg(3, angle - 10, DrawStartLine);
    FrameTimerWithArg(3, angle - 3, DrawObstacles);
    FrameTimerWithArg(4, angle - 6, DrawGoalLocation);

    //loop_run
    FrameTimer(1, PlayerClassOnLoop);
}

void RemoveAllRndBox(int key)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsObjectOn(RND_BOX[key] + k))
            Delete(RND_BOX[key] + k);
    }
    if (key)
        FrameTimerWithArg(1, key - 1, RemoveAllRndBox);
}

void PutItemRespawn()
{
    if (CUR_INDEX)
    {
        CUR_INDEX = 0;
        RemoveAllRndBox(6);
    }
    FrameTimerWithArg(20, 270, SpawnRandomBox);
    FrameTimerWithArg(21, 315, SpawnRandomBox);
    FrameTimerWithArg(22, 0, SpawnRandomBox);
    FrameTimerWithArg(23, 45, SpawnRandomBox);
    FrameTimerWithArg(24, 90, SpawnRandomBox);
    FrameTimerWithArg(25, 135, SpawnRandomBox);
    FrameTimerWithArg(26, 180, SpawnRandomBox);
}

int PlayerPusher(int n)
{
    int ptr;
    int k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 1) + 1;
        Delete(ptr - 1);
        for (k = 0 ; k < 10 ; k ++)
        {
            Frozen(CreateObject("Rock5", 21), 1);
            TeleportLocationVector(21, 20.0, 20.0);
        }
    }
    return ptr + n;
}

int PlayerGrp()
{
    int ptr;
    int k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 1) + 1;
        Delete(ptr - 1);
        for (k = 0 ; k < 10 ; k ++)
        {
            UnitNoCollide(CreateObject("AirshipBasket", 19));
            TeleportLocationVector(19, 20.0, 20.0);
        }
    }
    return ptr;
}

void TeleportHostPlayerForSelectLessons(int host)
{
    int ptr = CreateObject("Maiden", 13);
    CreateObject("Maiden", 14);
    CreateObject("Maiden", 15);
    int mg = CreateObject("Magic", 13);
    Frozen(mg, TRUE);
    SetUnitSubclass(mg, GetUnitSubclass(mg) ^ 1);
    mg = CreateObject("Magic", 14);
    Frozen(mg, TRUE);
    SetUnitSubclass(mg, GetUnitSubclass(mg) ^ 1);
    mg = CreateObject("Magic", 15);
    Frozen(mg, 1);
    SetUnitSubclass(mg, GetUnitSubclass(mg) ^ 1);
    LookWithAngle(ptr, 3);
    LookWithAngle(ptr + 1, 6);
    LookWithAngle(ptr + 2, 10);
    Frozen(ptr, 1);
    Frozen(ptr + 1, 1);
    Frozen(ptr + 2, 1);
    MoveObject(host, GetWaypointX(16), GetWaypointY(16));
    UniChatMessage(host, "목표점수를 선택하세요.", 150);
    AudioEvent("SecretFound", 16);
    FrameTimerWithArg(30, ptr, SaySelfScore);
}

void SelectLessons()
{
    int goal = GetDirection(SELF);

    ObjectOn(Object("StartSwitch"));
    MoveObject(OTHER, LocationX(17), LocationY(17));
    LESSONS = goal;
    UniPrint(OTHER, "목표치를 선택했습니다, 게임을 시작합니다.");
char buff[128];

    NoxSprintfString(&buff, "목표점수가 %d으로 선택되었습니다", &goal, 1);
    UniPrintToAll(ReadStringAddressEx(&buff));
}

void SaySelfScore(int ptr)
{
    char buff[64];
    string fmt="> %d 점 선택";
    int lesson=GetDirection(ptr);

    NoxSprintfString(&buff, fmt, &lesson, 1);
    UniChatMessage(ptr, ReadStringAddressEx(&buff), 150);

    lesson=GetDirection(ptr+1);

    NoxSprintfString(&buff, fmt, &lesson, 1);
    UniChatMessage(ptr+1, ReadStringAddressEx(&buff), 150);
    lesson=GetDirection(ptr+2);

    NoxSprintfString(&buff, fmt, &lesson, 1);
    UniChatMessage(ptr+2, ReadStringAddressEx(&buff), 150);
    SetDialog(ptr, "NORMAL", SelectLessons, SelectLessons);
    SetDialog(ptr + 1, "NORMAL", SelectLessons, SelectLessons);
    SetDialog(ptr + 2, "NORMAL", SelectLessons, SelectLessons);
}

void LoopDrawFinishLine()
{
    Effect("DRAIN_MANA", LocationX(8), LocationY(8), LocationX(9), LocationY(9));
    Effect("DRAIN_MANA", LocationX(9), LocationY(9), LocationX(8), LocationY(8));
    FrameTimer(1, LoopDrawFinishLine);
}

void SpawnRandomBox(int angle)
{
    float rad = 1500.0;
    int ptr = CreateObject("InvisibleLightBlueHigh", 1);

    Delete(ptr);
    RND_BOX[CUR_INDEX] = ptr + 1;
    while (rad < 1800.0)
    {
        int item=CreateObjectAt("BookOfOblivion", LocationX(1) + MathSine(angle + 90, rad), LocationY(1) + MathSine(angle, rad));
        Frozen(item, TRUE);
        RegistItemPickupCallback(item, RandomBoxEvent);
        rad += 30.0;
    }
    CUR_INDEX ++;
}

void DrawStartLine(int angle)
{
    float rad = 1500.0;

    while (rad < 1800.0)
    {
        TeleportLocation(3, LocationX(1) + MathSine(angle + 90, rad), LocationY(1) + MathSine(angle, rad));
        Frozen(CreateObject("SpiderSpit", 3), 1);
        rad += 5.0;
    }
}

void DrawObstacles(int angle)
{
    float rad = 1500.0;

    while (rad < 1800.0)
    {
        TeleportLocation(3, LocationX(1) + MathSine(angle + 90, rad), LocationY(1) + MathSine(angle, rad));
        CreateObject("ExtentStoneBoxLarge", 3);
        CreateObject("MovableSign2", 3);
        rad += 7.0;
    }
}

void Winner(int pUnit)
{
    if (!CurrentHealth(pUnit))
    {
        UniPrintToAll("에러-! 승리자가 게임에 존재하지 않습니다, 무승부 처리가 됩니다.");
        return;
    }
    int k;

    TeleportLocation(3, LocationX(4), LocationY(4));
    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(pUnit))
        {
            MoveObject(pUnit, LocationX(3), LocationX(3));
            TeleportLocationVector(3, 30.0, -30.0);
            if (m_player[k] == pUnit)
            {
                EnchantOff(pUnit, "ENCHANT_FREEZE");
                Enchant(pUnit, "ENCHANT_CROWN", 0.0);
                UniChatMessage(pUnit, "와 신난다^^ 이겼다-!", 180);
                PlaySoundAround(pUnit, SOUND_CrownChange);
            }
            pUnit = 0;
        }
    }
    char buff[128], *userName=StringUtilGetScriptStringPtr(PlayerIngameNick(pUnit));

    NoxSprintfString(&buff, "%s 님께서 먼저 목표점수에 달성하셨기 때문에,", &userName, 1);
    UniPrintToAll(ReadStringAddressEx(&buff));
    NoxSprintfString(&buff, "이번 게임의 승리자는 %s 님 입니다", &userName, 1);
    UniPrintToAll(ReadStringAddressEx(&buff));
    UniPrintToAll("승리를 축하드립니다-!");
}

void VictoryEvent(int plr, int pUnit)
{
    int k;

    UniPrintToAll("방금 게임의 승리자가 결정되었습니다.");
    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(pUnit))
        {
            Enchant(pUnit, "ENCHANT_INVULNERABLE", 0.0);
            Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
            PlaySoundAround(pUnit, SOUND_StaffOblivionAchieve1);
        }
    }
    FrameTimerWithArg(50, pUnit, Winner);
}

void GoalEvent()
{
    if (!IsPlayerUnit(OTHER))
        return;

    if (!UnitCheckEnchant(OTHER, GetLShift(ENCHANT_VILLAIN)))
    {
        Enchant(OTHER, "ENCHANT_VILLAIN", 1.0);
        int plr = CheckPlayer();
        if (plr >= 0)
        {
            int pUnit = m_player[plr];

            P_SCORE[plr] ++;
            if (P_SCORE[plr] < LESSONS)
            {
                int args[]={StringUtilGetScriptStringPtr(PlayerIngameNick(pUnit)), P_SCORE[plr]};
                char buff[128];

                NoxSprintfString(&buff, "[!!] %s 님께서 코스를 %d번 완주했습니다", &args, sizeof(args));
                UniPrintToAll(ReadStringAddressEx(&buff));
            }
            else
                VictoryEvent(plr, pUnit);
        }
        UniChatMessage(OTHER, "오우예~ 도착했다-!\n*^.^*", 120);
        Effect("WHITE_FLASH", LocationX(4), LocationY(4), 0.0, 0.0);
        MoveObject(OTHER, LocationX(4), LocationY(4));
        AudioEvent("FlagCapture", 4);
    }
}

void DrawGoalLocation(int angle)
{
    float rad = 1500.0;
    int ptr = CreateObject("InvisibleLightBlueHigh", 3) + 1;
    string name = "WeirdlingBeast";

    while (rad < 1800.0)
    {
        SetUnitMaxHealth( CreateObjectAt(name, LocationX(1) + MathSine(angle + 90, rad), LocationY(1) + MathSine(angle, rad)), 10);
        Damage(ptr, 0, CurrentHealth(ptr) + 1, -1);
        SetCallback(ptr, 9, GoalEvent);
        rad += 23.0;
        ptr +=1;
    }
}

void TouchedStone()
{
    int owner = GetOwner(SELF);

    if (!CurrentHealth(owner))
        return;
    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 75, 14);
            Enchant(OTHER, "ENCHANT_CHARMING", 1.0);
        }
    }
}

void ShootPreserve(int ptr)
{
    int owner = GetOwner(ptr);
    int step = GetDirection(ptr + 2);

    if (CurrentHealth(owner) && step < 30)
    {
        MoveObject(ptr, GetObjectX(ptr) + UnitAngleCos(ptr + 1, 20.0), GetObjectY(ptr) + UnitAngleSin(ptr + 1, 20.0));
        MoveObject(ptr + 1, GetObjectX(ptr), GetObjectY(ptr));
        LookWithAngle(ptr + 2, step + 1);
        FrameTimerWithArg(1, ptr, ShootPreserve);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void HarpoonEvent(int cur, int owner)
{
    Delete(cur);
    if (CurrentHealth(owner))
    {
        float x=GetObjectX(owner) + UnitAngleCos(owner, 30.0), y= GetObjectY(owner) + UnitAngleSin(owner, 30.0);
        
        DeleteObjectTimer(CreateObjectAt("Smoke", x,y), 6);
        int unit = CreateObjectAt("Maiden", x,y);
        CreateObjectAt("Rock3", x,y);
        CreateObjectAt("InvisibleLightBlueHigh", x,y);
        PlaySoundAround(unit, SOUND_ChakramThrow);
        PlaySoundAround(unit, SOUND_GolemHurt);
        Damage(unit, 0, MaxHealth(unit) + 1, -1);
        ObjectOff(unit);
        Frozen(unit, 1);
        Frozen(unit + 1, 1);
        SetOwner(owner, unit);
        LookWithAngle(unit + 1, GetDirection(owner));
        SetCallback(unit, 9, TouchedStone);
        FrameTimerWithArg(1, unit, ShootPreserve);
    }
}

void ChakramCopyProperies(int src, int dest)
{
    int *srcPtr = UnitToPtr(src), *destPtr = UnitToPtr(dest);
    
    int *srcProperty = srcPtr[173], *destProperty = destPtr[173];

    int rep=-1;

    while (++rep<4)
        destProperty[rep] = srcProperty[rep];

    int rep2=-1;

    while (++rep2<32)
        destPtr[140+rep2] = srcPtr[140+rep2];
}

static void CreateChakram(float xpos, float ypos, int *pDest, int owner)
{
    int mis = CreateObjectAt("RoundChakramInMotion", xpos, ypos);
    int *ptr=UnitToPtr(mis);

    if (pDest)
        pDest[0] = mis;

    ptr[174] = 5158032;
    ptr[186] = 5483536;
    SetOwner(owner, mis);

    int *collideDataMb = MemAlloc(8);
    collideDataMb[0]=0;
    collideDataMb[1] = UnitToPtr(owner);
    ptr[175] = collideDataMb;
}

void OnChakramTracking(int owner, int cur)
{
    int mis;

    CreateChakram(GetObjectX(owner) + UnitAngleCos(owner, 11.0), GetObjectY(owner) + UnitAngleSin(owner, 11.0), &mis, owner);
    ChakramCopyProperies(cur, mis);

    PushObject(mis, 30.0, GetObjectX(owner), GetObjectY(owner));
}

void AfterChakramTracking(int *pMem)
{
    if (!pMem)
        return;

    int inv = pMem[0], owner = pMem[1];

    if (CurrentHealth(owner) && IsObjectOn(inv))
    {
        Pickup(owner, inv);
    }
    FreeSmartMemEx(pMem);
}

static void HookChakrm(int cur, int owner)
{
    // UnitSetEnchantTime(cur, ENCHANT_RUN, 0);
    // Enchant(cur, EnchantList(ENCHANT_ETHEREAL),0.0);

    int *ptr=UnitToPtr(cur);

    if (ptr[186] == 5496000)
    {
        if (!CurrentHealth(owner))
            return;

        int *pMem;

        AllocSmartMemEx(8, &pMem);
        pMem[0] = GetLastItem(cur);
        pMem[1] = owner;
        OnChakramTracking(owner, cur); //이게 여기로 옮겨지고 cur 인자를 더 전달합니다
        Delete(cur);
        FrameTimerWithArg(2, pMem, AfterChakramTracking);
    }
}

void ShurikenEvent(int cur, int owner)
{
    if (CurrentHealth(owner))
    {
        int unit = 
            CreateObjectAt("ArcherArrow", 
                GetObjectX(owner) + UnitAngleCos(owner, 17.0), GetObjectY(owner) + UnitAngleSin(owner, 17.0));
        Enchant(unit, "ENCHANT_SHOCK", 0.0);
        SetOwner(owner, unit);
        LookWithAngle(unit, GetDirection(owner));
        PushObject(unit, 50.0, GetObjectX(owner), GetObjectY(owner));
        DeleteObjectTimer(unit, 11);
        Delete(cur);
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
	HashPushback(pInstance, 526, HarpoonEvent);
	HashPushback(pInstance, 1179, ShurikenEvent);
	HashPushback(pInstance, 1177, HookChakrm);
}

static void delaySetDirection(int plr)
{
    if (CurrentHealth(m_player[plr]))
        LookWithAngle(FlagGrp(plr), 1);
}

int PlayerClassOnInit(int plr, int pUnit, char *userInit)
{
    m_player[plr]=pUnit;
    m_pFlag[plr]=1;
    m_pDeath[plr]=0;
    P_SCORE[plr] = 0;
    P_SPEED[plr] = 15.0;

    // if (ValidPlayerCheck(pUnit))
    // {
    //     if (GetHost() ^ pUnit)
    //         NetworkUtilClientEntry(pUnit);
    //     else
    //         ClientsideProcess();
    //     // FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);
    //     userInit[0] = TRUE;
    // }
    userInit[0]=FALSE; //unused

    FrameTimerWithArg(1, plr, delaySetDirection);
    Enchant(pUnit, "ENCHANT_BLINDED", 0.8);
    SelfDamageClassEntry(pUnit);
    // DiePlayerHandlerEntry(pUnit);

    ChangeGold(pUnit, -GetGold(pUnit));
    return plr;
}

static void playerClassOnJoin(int plr)
{
    int wp = 4;
    EmptyInventory(OTHER);
    Enchant(OTHER, "ENCHANT_HELD", 0.0);
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);

    if (GetDirection(FlagGrp(plr)))
    {
        MoveWaypoint(18, GetObjectX(FlagGrp(plr)), GetObjectY(FlagGrp(plr)));
        wp = 18;
    }
    MoveObject(OTHER, GetWaypointX(wp), GetWaypointY(wp));
    AudioEvent("ManaBombCast", wp);
    Effect("WHITE_FLASH", GetWaypointX(wp), GetWaypointY(wp), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("BlueRain", wp), 20);
    UniChatMessage(m_player[plr], "레이스 입장! J 키를 누르면 이동, L 키 정지.", 120);

    char buff[128];

    NoxSprintfString(&buff,"게임목표- 레이스를 따라 총%d바퀴를 먼저돌면 승리-!", &LESSONS, 1);
    UniPrint(OTHER, ReadStringAddressEx(&buff));
    UniPrint(OTHER,"게임 팁- J 키를 누르면 이동하고, 움직이는 도중 L 키를 눌러 정지할 수도 있습니다.");
}

static void playerClassOnEntryFailure(int pUnit)
{
    Enchant(pUnit, "ENCHANT_BLINDED", 0.0);
    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    UniPrint(pUnit,"맵에 입장할 수 없습니다, 잠시 후 다시 시도하세요");
    MoveObject(pUnit, LocationX(5), LocationY(5));
}

int CheckPlayerWithId(int pUnit)
{
    int rep=sizeof(m_player);

    while (--rep>=0)
    {
        if (m_player[rep]^pUnit)
            continue;       
        break;
    }
    return rep;
}

void OnPlayerDeferredJoin(int user, int plr)
{
    MoveObject(user, LocationX(474), LocationY(474));
    Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    FrameTimerWithArg(3, plr, playerClassOnJoin);
    UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
}

void PlayerClassOnEntry(int plrUnit)
{
    while (TRUE)
    {
        if (!CurrentHealth(plrUnit))
            break;
        int plr = CheckPlayerWithId(plrUnit), rep = sizeof(m_player);
        char initialUser=FALSE;

        while (--rep>=0&&plr<0)
        {
            if (!MaxHealth(m_player[rep]))
            {
                plr = PlayerClassOnInit(rep, plrUnit, &initialUser);
                // FrameTimerWithArg(66, plrUnit, ThisMapInformationPrintMessage);
                break;
            }
        }
        if (plr >= 0)
        {
            if (initialUser)
                OnPlayerDeferredJoin(plrUnit, plr);
            else
                playerClassOnJoin(plr);
            break;
        }
        playerClassOnEntryFailure(plrUnit);
        break;
    }
}

void RegistPlayer()
{
    PlayerClassOnEntry(GetCaller());
}

void PlayerClassOnAlive(int plr, int user)
{
    float x=GetObjectX(user),y=GetObjectY(user);
    MoveObject(FlagGrp(plr), x,y);
    MoveObject(PlayerGrp() + plr, x,y);

    int act = CheckPlayerInput(user);

    if (!UnitCheckEnchant(user, GetLShift(ENCHANT_FREEZE)))
    {
        if (CheckPlayerInput(user)==2)
            MoveObject(PlayerPusher(plr), x - UnitAngleCos(user, P_SPEED[plr]), y - UnitAngleSin(user, P_SPEED[plr]));
    }
    
    float dis = Distance(GetObjectX(user), GetObjectY(user), LocationX(1), LocationY(1));

    if (dis < 1500.0 || dis > 1800.0)
        OverRangePlayer(plr, user);
}

void PlayerClassOnShutdown(int plr)
{
    m_player[plr]=0;
    m_pFlag[plr]=0;
    LookWithAngle(FlagGrp(plr), 0);
    MoveObject(PlayerPusher(plr), LocationX(21), LocationY(21));
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}

void PlayerClassOnDeath(int rep, int user)
{
    char dieMsg[128];
    int params[]={StringUtilGetScriptStringPtr(PlayerIngameNick(user)), ++m_pDeath[rep]};

    NoxSprintfString(&dieMsg, "방금 %s님께서 적에게 격추되었습니다 (%d번째 사망)", &params, sizeof(params));
    UniPrintToAll(ReadStringAddressEx(&dieMsg));
}

void PlayerClassOnLoop()
{
    int rep = sizeof(m_player);

    while (--rep>=0)
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[rep]))
            {
                if (GetUnitFlags(m_player[rep]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[rep]))
                {
                    PlayerClassOnAlive(rep, m_player[rep]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(rep, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(rep, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(rep, m_player[rep]);
                    }
                    break;
                }                
            }
            if (m_pFlag[rep])
                PlayerClassOnShutdown(rep);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

// int CheckOwner(int unit)
// {
//     int k;

//     for (k = 9 ; k >= 0 ; k --)
//     {
//         if (IsOwnedBy(unit, player[k]))
//             return k;
//     }
//     return -1;
// }

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(m_player[k]))
            return k;
    }
    return -1;
}

void EmptyInventory(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void PlayerPullAt(int ptr, int owner)
{
    if (CurrentHealth(owner))
    {
        if (ToInt(DistanceUnitToUnit(owner,ptr - 1)))
            MoveObject(ptr + 1, GetObjectX(owner) + UnitRatioX(owner, ptr - 1, 20.0), 
                GetObjectY(owner) + UnitRatioY(owner, ptr - 1, 20.0));
    }
}

void GreenChainFx(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        int dur = GetDirection(ptr);

        if (dur < 50)
        {
            Effect("CHARM", GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr + 1), GetObjectY(ptr + 1));
            LookWithAngle(ptr, dur + 1);
            PlayerPullAt(ptr + 1, owner);
            FrameTimerWithArg(1, ptr, GreenChainFx);
            return;
        }
    }   
    Delete(ptr++);
    Delete(ptr++);
    Delete(ptr++);
}

void OverRangePlayer(int plr, int user)
{
    if (!UnitCheckEnchant(user, GetLShift(ENCHANT_FREEZE)))
    {
        Damage(user, 0, 30, 14);
        Enchant(user, "ENCHANT_FREEZE", 2.0);
        int ptr = 
            CreateObjectAt("InvisibleLightBlueHigh", 
                GetWaypointX(1) - UnitRatioX(CENTER, user, 1650.0), GetWaypointY(1) - UnitRatioY(CENTER, user, 1650.0));
        CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(user), GetObjectY(user));
        Frozen( CreateObject("Maiden", 1) , TRUE);
        LookWithAngle(ptr + 1, plr);
        PlaySoundAround(ptr+1, SOUND_TelekinesisCast);
        SetOwner(user, ptr);
        GreenChainFx(ptr);
    }
}

void PutInnerRing(float size)
{
    int k=180;
    string n="MediumBlueFlame";

    while (--k)
    {
        CreateObjectAt(n, GetWaypointX(1) + MathSine(k * 2 + 90, size), GetWaypointY(1) + MathSine(k * 2, size));
    }
}

static void randomBoxFxProto(int functionId, int plr, int posUnit)
{
    Bind(functionId, &functionId + 4);
}

void RandomBoxEvent()
{
    int plr = CheckPlayer();
    int count;

    Delete(SELF);

    if (++count > 40)
    {
        count = 0;
        UniPrintToAll("아이템을 재 생성합니다.");
        FrameTimer(30, PutItemRespawn);
    }
    if (plr >= 0)
    {
        int magical[]={
            BoxEventSlowdown,
BoxEventHaste,
BoxEventDeathray,
BoxEventFirewalk,
BoxEventFlying,
BoxEventSummons,
BoxEventSwap,
BoxEventSuicide,
BoxEventCreateObelisk,
BoxEventCrushStep,
BoxEventPicRandomWeapon,
BoxEventFireballTrap,
BoxEventPoisonGas
        };
        randomBoxFxProto(magical[Random(0, sizeof(magical)-1)], plr, m_player[plr]);
    }
}

static void DisableSlowDown(int plr)
{
    if (!UnitCheckEnchant(m_player[plr], GetLShift(ENCHANT_SLOWED)) && CurrentHealth(m_player[plr]))
        P_SPEED[plr] -= 3.0;
}

void BoxEventSlowdown(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_SLOWED)) )
    {
        UniChatMessage(pUnit, "슬로우에 걸렸습니다, 5 초간 이동속도가 감소합니다.", 120);
        Enchant(pUnit, "ENCHANT_SLOWED", 5.0);
        P_SPEED[plr] += 3.0;
        FrameTimerWithArg(151, plr, DisableSlowDown);
    }
    }
}

void DisableBooster(int plr)
{
    if (!CurrentHealth(m_player[plr]))
        return;

    if (!UnitCheckEnchant(m_player[plr], GetLShift(ENCHANT_HASTED)))
    {
        P_SPEED[plr] += 3.0;
    }
}

void BoxEventHaste(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_HASTED)))
        {
            UniChatMessage(pUnit, "부스터 발동, 5 초간 이동속도가 증가합니다.", 120);
            Enchant(pUnit, "ENCHANT_HASTED", 5.0);
            P_SPEED[plr] -= 3.0;
            FrameTimerWithArg(151, plr, DisableBooster);
        }
    }
}

void BoxEventDeathray(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        UniChatMessage(pUnit, "주변 적에게 데스레이를 시전합니다.", 120);
        int ptr = CreateObject("InvisibleLightBlueHigh", 1) + 1, k;
        Delete(ptr - 1);
        for (k = 0 ; k < 8 ; k ++)
        {
            float x=GetObjectX(pUnit) + MathSine(k * 40 + 90, 70.0),y= GetObjectY(pUnit) + MathSine(k * 40, 70.0);
            Effect("VIOLET_SPARKS", x,y, 0.0, 0.0);
            UnitLinkBinScript(CreateObjectAt("WeirdlingBeast", x,y), WeirdlingBeastBinTable());
            UnitZeroFleeRange(ptr + k);
            SetOwner(pUnit, ptr + k);
            SetUnitScanRange(ptr +k, 450.0);
            LookAtObject(ptr + k, pUnit);
            LookWithAngle(ptr + k, GetDirection(ptr + k) + 128);
            SetCallback(ptr + k, 3, ShotDeathRay);
            DeleteObjectTimer(ptr + k, 1);
        }
    }
}

static void PreserveFirewalk(int ptr)
{
    int owner = GetOwner(ptr);
    int dur = GetDirection(ptr);

    if (CurrentHealth(owner))
    {
        if (dur < 180)
        {
            if (DistanceUnitToUnit(owner,ptr) > 30.0)
            {
                int flame = CreateObjectAt("Flame", GetObjectX(ptr), GetObjectY(ptr));
                SetOwner(owner, flame);
                DeleteObjectTimer(flame, 30 * 7);
                MoveObject(ptr, GetObjectX(owner), GetObjectY(owner));
            }
            LookWithAngle(ptr, dur + 1);
            FrameTimerWithArg(1, ptr, PreserveFirewalk);
            return;
        }
    }
    Delete(ptr);
}

void BoxEventFirewalk(int plr, int pUnit)
{
    if (!CurrentHealth(pUnit))
        return;

    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_PROTECT_FROM_FIRE) ))
    {
        int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(pUnit), GetObjectY(pUnit));

        SetOwner(pUnit, unit);
        UniChatMessage(pUnit, "6 초간 지나온 자리에 불꽃을 생성합니다.", 120);
        PlaySoundAround(unit, SOUND_FirewalkOn);
        Raise(unit, ToFloat(plr));
        Enchant(pUnit, "ENCHANT_PROTECT_FROM_FIRE", 8.0);
        FrameTimerWithArg(1, unit, PreserveFirewalk);
    }
}

void LoopMissileDetection(int ptr)
{
    int dur = GetDirection(ptr + 1), owner=GetOwner(ptr);
    float xpos=GetObjectX(ptr),ypos=GetObjectY(ptr);

    if (CurrentHealth(owner))
    {
        if (dur < 100)
        {
            MoveObject(ptr, GetObjectX(ptr) + UnitAngleCos(ptr, 12.0), GetObjectY(ptr) + UnitAngleSin(ptr, 12.0));
            if (checkCoorValidate(xpos, ypos))
            {
                Effect("CYAN_SPARKS", xpos,ypos, 0.0, 0.0);
                int unit = CreateObjectAt("Bat", xpos, ypos);
                CreateObjectAt("InvisibleLightBlueHigh", xpos, ypos);
                SetOwner(owner, unit);
                LookWithAngle(unit, GetDirection(ptr));
                SetCallback(unit, 3, DetectEnemy);
                SetCallback(unit, 9, TouchedDetectMissile);
                Raise(unit + 1, ToFloat(ptr));
                DeleteObjectTimer(unit, 1);
                DeleteObjectTimer(unit + 1, 1);
                LookWithAngle(ptr + 1, dur + 1);
            }
            else
                LookWithAngle(ptr + 1, 100);
            FrameTimerWithArg(1, ptr, LoopMissileDetection);
            return;
        }
        EnchantOff(owner, "ENCHANT_AFRAID");
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void BoxEventFlying(int plr, int pUnit)
{
    if (!CurrentHealth(pUnit))
        return;

    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_AFRAID)))
    {
        UniChatMessage(pUnit, "적을 추적하는 유도탄 하나를 발사합니다.", 120);
        Enchant(pUnit, "ENCHANT_AFRAID", 0.0);
        float xpos= GetObjectX(pUnit) + UnitAngleCos(pUnit, 23.0),ypos= GetObjectY(pUnit) + UnitAngleSin(pUnit, 23.0);
        int unit = CreateObjectAt("InvisibleLightBlueHigh", xpos,ypos);
        CreateObjectAt("InvisibleLightBlueHigh", xpos,ypos);
        PlaySoundAround(unit, SOUND_MagicMissileCast);
        LookWithAngle(unit, GetDirection(pUnit));
        Raise(unit, ToFloat(plr));
        SetOwner(pUnit, unit);
        FrameTimerWithArg(1, unit, LoopMissileDetection);
    }
}

void DelaySummon(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        float x=GetObjectX(ptr),y= GetObjectY(ptr);
        int unit = SpawnSummonUnit(x,y);

        SpawnSummonUnit(x,y);
        SetOwner(owner, unit);
        SetOwner(owner, unit + 1);
        FrameTimerWithArg(1, owner, DelayGuardCreature);
        FrameTimerWithArg(1, unit, DelayGuardCreature);
        FrameTimerWithArg(1, owner, DelayGuardCreature);
        FrameTimerWithArg(1, unit + 1, DelayGuardCreature);
    }
    Delete(ptr);
}

void BoxEventSummons(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        UniChatMessage(pUnit, "랜덤유닛 두개를 소환합니다.", 120);
        float xpos=GetObjectX(pUnit) + UnitAngleCos(pUnit, 30.0),ypos= GetObjectY(pUnit) + UnitAngleSin(pUnit, 30.0);
        int unit = CreateObjectAt("InvisibleLightBlueHigh", xpos,ypos);
        DeleteObjectTimer(CreateObjectAt("BigSmoke", xpos,ypos), 10);
        LookWithAngle(unit, plr);
        SummonEffect(xpos,ypos);
        SetOwner(pUnit, unit);
        FrameTimerWithArg(90, unit, DelaySummon);
    }
}

void BoxEventSwap(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        UniChatMessage(pUnit, "위치바꾸기를 시전합니다, 주변에 적이 보인다면 적과 위치를 교환할 것입니다.", 120);
        float xpos=GetObjectX(pUnit),ypos= GetObjectY(pUnit);

        PlaySoundAround(pUnit,SOUND_FumbleEffect);
        Effect("RICOCHET", xpos,ypos, 0.0, 0.0);
        Effect("SMOKE_BLAST", xpos,ypos, 0.0, 0.0);
        int k;
        for (k = 9 ; k >= 0 ; k --)
        {
            if (k == plr) continue;
            if (IsVisibleTo(pUnit, m_player[k]))
            {
                Effect("LIGHTNING", GetObjectX(pUnit), GetObjectY(pUnit), GetObjectX(m_player[k]), GetObjectY(m_player[k]));
                MoveObject(pUnit, GetObjectX(m_player[k]), GetObjectY(m_player[k]));
                MoveObject(m_player[k], xpos,ypos);
                break;
            }
        }
    }
}

void ExplosionManaBomb(int ptr)
{
    int plr = GetDirection(ptr), pUnit = m_player[plr];

    if (CurrentHealth(pUnit))
    {
        int k;

        for (k = 9 ; k >= 0 ; k --)
        {
            if (k == plr) continue;
            if (CurrentHealth(m_player[k]))
            {
                if (IsVisibleTo(ptr, m_player[k]))
                {
                    EnchantOff(m_player[k], "ENCHANT_CHARMING");
                    Damage(m_player[k], pUnit, 150, 14);
                }
            }
        }
        Effect("WHITE_FLASH", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
        PlaySoundAround(ptr, SOUND_ManaBombEffect);
        UniChatMessage(pUnit, "나 빼고 ㅋㅋㅋㅋㅋㅋ", 120);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void BoxEventSuicide(int plr, int pUnit)
{    
    if (CurrentHealth(pUnit))
    {
        UniChatMessage(pUnit, "다 같이 죽읍시다!", 120);
        Effect("WHITE_FLASH", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(pUnit), GetObjectY(pUnit));
        CreateObjectAt("ManaBombCharge", GetObjectX(ptr), GetObjectY(ptr));
        LookWithAngle(ptr, plr);
        PlaySoundAround(ptr, SOUND_ManaBombCast);
        FrameTimerWithArg(57, ptr, ExplosionManaBomb);
        int k;
        for (k = 9 ; k >= 0 ; k --)
        {
            if (plr == k) continue;
            if (CurrentHealth(m_player[k]) && IsVisibleTo(ptr, m_player[k]) && IsAttackedBy(m_player[k], pUnit))
            {
                Effect("VIOLET_SPARKS", GetObjectX(m_player[k]), GetObjectY(m_player[k]), 0.0, 0.0);
                Enchant(m_player[k], "ENCHANT_CHARMING", 5.0);
            }
        }
    }
}

void SpawnObelisk(int ptr)
{
    float pos_x = UnitRatioX(CENTER, ptr, 46.0);
    float pos_y = UnitRatioY(CENTER, ptr, 46.0);
    int pos = 300;
    string name = "WizardGenerator";

    TeleportLocation(22, GetObjectX(ptr), GetObjectY(ptr));
    while (pos > 0)
    {
        int gen=CreateObject(name, 22);
        ObjectOff(gen);
        SetUnitMaxHealth(gen, 400);
        TeleportLocationVector(22,-pos_x,-pos_y);
        pos -= 46;
    }
    Delete(ptr);
}

void BoxEventCreateObelisk(int plr, int pUnit)
{
    float pos_x;
    float pos_y;

    if (CurrentHealth(pUnit))
    {
        UniChatMessage(pUnit, "전방에 오벨리스크 장벽이 생성됩니다.", 120);
        pos_x = UnitRatioX(CENTER, pUnit, 100.0);
        pos_y = UnitRatioY(CENTER, pUnit, 100.0);
        FrameTimerWithArg(30, 
            CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(CENTER) - (pos_x * 15.0) + pos_y, GetObjectY(CENTER) - (pos_y * 15.0) - pos_x), 
            SpawnObelisk);
    }
}

void EarthQuakeStep(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        int dur = GetDirection(ptr);
        if (dur)
        {
            FrameTimerWithArg(10, ptr, EarthQuakeStep);
            LookWithAngle(ptr,dur-1);
            if (DistanceUnitToUnit(owner, ptr) > 23.0)
            {
                Effect("DAMAGE_POOF", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                CastSpellObjectObject("SPELL_EARTHQUAKE", owner, owner);
                MoveObject(ptr, GetObjectX(owner), GetObjectY(owner));
            }
            return;
        }
    }
    Delete(ptr);
}

void BoxEventCrushStep(int plr, int pUnit)
{
    if (!CurrentHealth(pUnit))
        return;
    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_ANTI_MAGIC)))
    {
        Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 9.0);
        UniChatMessage(pUnit, "광란의 발걸음! 이동할 때마다 지진스킬이 발동됩니다.", 120);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(pUnit), GetObjectY(pUnit));
        LookWithAngle(ptr, 29);
        SetOwner(pUnit, ptr);
        EarthQuakeStep(ptr);
    }
}

static void specialProperty7(int weapon, int *first, int *second, int *special)
{
    first[0]=ITEM_PROPERTY_impact4;
    special[0]= m_pWeaponUserSpecialProperty4;
}

static void specialProperty0(int weapon, int *first, int *second, int *special)
{
    first[0]=ITEM_PROPERTY_lightning4;
    special[0]=m_pWeaponUserSpecialProperty1;
}

static void specialProperty1(int weapon, int *first, int *second,int *special)
{
    first[0]=ITEM_PROPERTY_lightning4;
        special[0] =m_pWeaponUserSpecialProperty5;
}

static void specialProperty2(int weapon, int *first, int *second,int *special)
{
    first[0]=ITEM_PROPERTY_fire4;
        FrameTimerWithArg(1, weapon, LoopStaffStat);
}

static void specialProperty5(int weapon, int *first, int *second,int *special)
{
    first[0]=ITEM_PROPERTY_fire4;
    special[0]= m_pWeaponUserSpecialProperty2;
}

static void specialProperty6(int weapon, int *first, int *second,int *special)
{
    first[0]=ITEM_PROPERTY_lightning4;
    special[0]= m_pWeaponUserSpecialProperty3;
}

static void selectSpecialPropertyProto(int functionId, int weapon, int *first, int *second,int *special)
{
    Bind(functionId, &functionId+4);
}

static void specialWeapon(int weapon, int pic)
{
    int features[]={specialProperty0, specialProperty1, specialProperty2, 0,0,
     specialProperty5, specialProperty6, specialProperty7 };
    int firstProperty=0, secondProperty=0, specialProperty =0 ;

    if (pic < sizeof(features))
    {
        if (features[pic])
            selectSpecialPropertyProto(features[pic], weapon, &firstProperty, &secondProperty, &specialProperty);
    }
    SetWeaponPropertiesDirect(weapon,
        ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, firstProperty, secondProperty);
    if (specialWeapon)
        SpecialWeaponPropertySetWeapon(weapon, 3, specialProperty);
}

void GiveWeaponToPlayer(int weapon)
{
    int owner = GetOwner(weapon);

    if (CurrentHealth(owner))
        Pickup(owner, weapon);
}

void BoxEventPicRandomWeapon(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        UniChatMessage(pUnit, "랜덤한 아이템이 지급됩니다.", 120);
        int rnd = Random(0, m_weaponNameArrayLength-1);
        int weapon = CreateObjectAt(m_weaponNameArray[rnd], GetObjectX(pUnit), GetObjectY(pUnit));
        specialWeapon(weapon, rnd);
        SetOwner(pUnit, weapon);
        DeleteObjectTimer(weapon, 300);
        FrameTimerWithArg(1, weapon, GiveWeaponToPlayer);
    }
}

void DelayEnableSkullTraps(int ptr)
{
    LookAtObject(ptr + 2, ptr);
    LookAtObject(ptr + 3, ptr);
    LookAtObject(ptr + 4, ptr);
    ObjectOn(ptr + 2);
    ObjectOn(ptr + 3);
    ObjectOn(ptr + 4);
    Delete(ptr);
    Delete(ptr + 1);
    int st = CreateObjectAt(m_statueNameArray[GetDirection(ptr + 2) / 32], GetObjectX(ptr + 2), GetObjectY(ptr + 2));

    UnitNoCollide(st);
}

void BoxEventFireballTrap(int plr, int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        float x=GetObjectX(pUnit), y=GetObjectY(pUnit);
        UniChatMessage(pUnit, "전방에 화이어볼 함정이 생성됩니다.", 120);
        TeleportLocation(3, x,y);
        int ptr = CreateObject("InvisibleLightBlueHigh", 3);
        CreateObject("InvisibleLightBlueHigh", 3);
        TeleportLocation(3, x + UnitAngleCos(pUnit, 120.0), y + UnitAngleSin(pUnit, 120.0));
        ObjectOff(CreateObject("Skull3", 3)); //ptr+2
        LookWithAngle(ptr, GetDirection(pUnit) + 5);
        LookWithAngle(ptr + 1, GetDirection(pUnit) - 5);
        TeleportLocation(3, x + UnitAngleCos(ptr, 120.0), y + UnitAngleSin(ptr, 120.0));
        ObjectOff(CreateObject("Skull3", 3));
        TeleportLocation(3, x + UnitAngleCos(ptr + 1, 120.0), y + UnitAngleSin(ptr + 1, 120.0));
        ObjectOff(CreateObject("Skull3", 3));
        SetOwner(pUnit, ptr + 2);
        SetOwner(pUnit, ptr + 3);
        SetOwner(pUnit, ptr + 4);
        FrameTimerWithArg(1, ptr, DelayEnableSkullTraps);
    }
}

void BoxEventPoisonGas(int plr, int pUnit)
{
    int k;
    float *area=MemAlloc(8);
    if (CurrentHealth(pUnit))
    {
        UniChatMessage(pUnit, "방구낀놈 누구야?!", 120);
        for (k = 19 ; k >= 0 ; k --)
        {
            ComputeAreaRhombus(area, GetObjectX(pUnit), GetObjectX(pUnit) + 200.0, GetObjectY(pUnit) - 200.0, GetObjectY(pUnit));
            ToxicCloudCreate(area, 90);
        }
    }
    int *p=area;
    MemFree(p );
}

void ShotDeathRay()
{
    if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    {
        Enchant(OTHER, "ENCHANT_VILLAIN", 0.3);
        CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
    }
}

static int checkCoorValidate(float x, float y)
{
    if (x<100.0)
        return FALSE;
    if (y<100.0)
        return FALSE;
    if (x>5750.0)
        return FALSE;
    if (y>5750.0)
        return FALSE;
    return TRUE;
}

void DetectEnemy()
{
    int ptr = ToInt(GetObjectZ(GetTrigger() + 1));

    LookAtObject(ptr, OTHER);
    Delete(SELF);
    Delete(GetTrigger() + 1);
}

void TouchedDetectMissile()
{
    if (CurrentHealth(OTHER) && IsAttackedBy(SELF, OTHER) && HasClass(OTHER, "PLAYER"))
    {
        Effect("EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, SELF, 100, 9);
        CastSpellObjectObject("SPELL_BURN", SELF, OTHER);
        Delete(SELF);
    }
}

void ToxicCloudCreate(float *area, int duration)
{
    CreateObjectAt("ToxicCloud", area[0], area[1]);
    SetMemory(GetMemory(GetMemory(0x750710) + 0x2ec), duration);
}

// int CheckLimitLineForWaypoint(int wp)
// {
//     float pos_x = GetWaypointX(wp), pos_y = GetWaypointY(wp);

//     if (pos_x > 100.0 && pos_y > 100.0 && pos_x < 5532.0 && pos_y < 5532.0)
//         return 1;
//     return 0;
// }

static void Dispell(int unit)
{
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Delete(unit);
}

void SummonEffect(float xpos, float ypos)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", xpos, ypos);

    CastSpellObjectObject("SPELL_SUMMON_BEHOLDER", unit, unit);
    FrameTimerWithArg(90, unit, Dispell);
}

void DelayGuardCreature(int ptr)
{
    int parent;

    if (!parent) parent = ptr;
    else
    {
        CreatureFollow(ptr, parent);
        AggressionLevel(ptr, 1.0);
        parent = 0;
    }
}

void LoopSwordStat(int weapon)
{
    if (IsObjectOn(weapon))
    {
        if (MaxHealth(weapon) - CurrentHealth(weapon))
        {
            int owner = GetOwner(weapon);
            if (owner)
            {
                if (CurrentHealth(owner) && !HasEnchant(owner, "ENCHANT_PROTECT_FROM_MAGIC"))
                {
                    float xpos = GetObjectX(owner) + UnitAngleCos(owner, 45.0), ypos = GetObjectY(owner) + UnitAngleSin(owner, 45.0);
                    int unit = CreateObjectAt("Maiden", xpos, ypos);

                    Enchant(owner, "ENCHANT_PROTECT_FROM_MAGIC", 0.7);
                    PlaySoundAround(unit, SOUND_SentryRayHit);
                    Effect("RICOCHET", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                    CreateObjectAt("InvisibleLightBlueHigh", xpos, ypos);
                    Damage(unit, 0, MaxHealth(unit) + 1, 14);
                    Frozen(unit, 1);
                    Raise(unit + 1, ToFloat(owner));
                    SetCallback(unit, 9, StrikeSword);
                    DeleteObjectTimer(unit, 1);
                    DeleteObjectTimer(unit + 1, 1);
                }
            }
            RestoreHealth(weapon, 2000);
        }
        FrameTimerWithArg(1, weapon, LoopSwordStat);
    }
}

void LoopStaffStat(int staff)
{
    int owner;

    if (IsObjectOn(staff))
    {
        if (MaxHealth(staff) - CurrentHealth(staff))
        {
            owner = GetOwner(staff);
            if (owner)
            {
                if (CurrentHealth(owner) && !HasEnchant(owner, "ENCHANT_PROTECT_FROM_MAGIC"))
                {
                    float xpos = GetObjectX(owner) + UnitAngleCos(owner, 35.0), ypos = GetObjectY(owner) + UnitAngleSin(owner, 35.0);
                    int unit = CreateObjectAt("Maiden", xpos, ypos);

                    Enchant(owner, "ENCHANT_PROTECT_FROM_MAGIC", 0.3);
                    PlaySoundAround(unit, SOUND_AxeMissing);
                    Effect("DAMAGE_POOF", xpos, ypos, 0.0, 0.0);
                    CreateObjectAt("InvisibleLightBlueHigh", xpos, ypos);
                    Damage(unit, 0, MaxHealth(unit) + 1, 14);
                    Frozen(unit, 1);
                    Raise(unit + 1, ToFloat(owner));
                    SetCallback(unit, 9, StrikeStaff);
                    DeleteObjectTimer(unit, 1);
                    DeleteObjectTimer(unit + 1, 1);
                }
            }
            RestoreHealth(staff, 2000);
        }
        FrameTimerWithArg(1, staff, LoopStaffStat);
    }
}

void StrikeSword()
{
    int owner = ToInt(GetObjectZ(GetTrigger() + 1));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Effect("BLUE_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Enchant(OTHER, "ENCHANT_CHARMING", 1.0);
        Damage(OTHER, owner, 90, 14);
    }
}

void StrikeStaff()
{
    int owner = ToInt(GetObjectZ(GetTrigger() + 1));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Effect("DAMAGE_POOF", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, owner, 25, 14);
    }
}

int SummonUnitHealth(int num)
{
    int arr[10];

    if (num < 0)
    {
        arr[0] = 400; arr[1] = 200; arr[2] = 60; arr[3] = 400; arr[4] = 200;
        arr[5] = 150; arr[6] = 150; arr[7] = 90; arr[8] = 180; arr[9] = 60;
        return 0;
    }
    return arr[num];
}

static void SetSpecialFlags(int unit, int rnd)
{
    SetUnitMaxHealth(unit, SummonUnitHealth(rnd));
    if (rnd == 1 || rnd == 2)
        Enchant(unit, "ENCHANT_SLOWED", 0.0);
    else if (rnd == 4)
        SetUnitSpeed(unit, 1.9);
}

int SpawnSummonUnit(float x, float y)
{
    int rnd = Random(0, 9);
    int unit = CreateObjectAt(m_summonUnitNameArray[rnd], x,y);

    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    SetSpecialFlags(unit, rnd);
    return unit;
}

static void drawStrGoArrowImage(int arg_0, string name)
{
	int i,count;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = LocationX(10);
		pos_y = LocationY(10);
	}
	for (i = 1 ; i > 0 && count < 434 ; i <<= 1)
	{
		if (i & arg_0)
			Nop( CreateObject(name, 10) );
		if (count % 44 == 43)
            TeleportLocationVector(10, -86.0, 2.0);
		else
            TeleportLocationVector(10, 2.0, 0.0);
		count +=1;
	}
	if (count >= 434)
	{
		count = 0;
		TeleportLocation(10, pos_x, pos_y);
	}
}

void StrGoArrowImage()
{
	int i=0, arr[14]={
	 33583224,  286294016, 166723712, 1050656, 17039968, 508166148, 536903745, 268968978, 75645952, 1077937154,
	 16672,  71704584, 939556896, 134607073 }; 
	while(i < 14)
		drawStrGoArrowImage(arr[i++], "ManaBombOrb");
}

static void drawStrFinish(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = LocationX(11);
		pos_y = LocationY(11);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			Nop( CreateObject(name, 11) );
		if (count % 38 == 37)
            TeleportLocationVector(11, -74.0, 2.0);
		else
            TeleportLocationVector(11, 2.0, 0.0);
		count +=1;
	}
	if (count >= 403)
	{
		count = 0;
		TeleportLocation(11, pos_x, pos_y);
	}
}

void StrFinish()
{
	int i=0,arr[13]={
	 252844414, 1225556257, 172003472, 541608009, 1679828106, 1885489447, 1092915239, 340267537, 604571905, 75793556,
	 1111641124, 505811460, 66 };
	while(i < 13)
		drawStrFinish(arr[i++], "ManaBombOrb");
}

int FlagGrp(int num)
{
    int arr[10];
    int k;

    if (num < 0)
    {
        arr[0] = Object("flagBase");
        for (k = 1 ; k < 10 ; k ++)
            arr[k] = arr[0] + (k * 2);
        return 0;
    }
    return arr[num];
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 12);
        CreateObject("BlackPowder", 12);
        Frozen(unit, 1);
        SetCallback(unit, 9, DisplayScoreBoard);
    }
    return unit;
}

void DisplayScoreBoard()
{
    char buff[1024], localBuff[128];
    int k;

    NoxSprintfString(&buff, "플레이어 점수 현황판\n목표점수: %d\n", &LESSONS, 1);
    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(m_player[k]))
        {
            int args[]={ StringUtilGetScriptStringPtr(PlayerIngameNick(m_player[k])), P_SCORE[k] };
            
            NoxSprintfString(&localBuff, "%s:\t%d\n", &args, sizeof(args));
            StringUtilAppend(&buff, &localBuff);
        }
    }
    UniChatMessage(SELF, ReadStringAddressEx(&buff), 60);
}
