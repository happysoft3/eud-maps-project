
#include "subway_initscan.h"
#include "boss_mon.h"
#include "boss_player.h"
#include "boss_shop.h"
#include "boss_resource.h"
#include "libs/coopteam.h"
#include "libs/indexloop.h"
#include "libs/game_flags.h"

void teleportAllPlayer(short loc){
    float x=LocationX(loc),y=LocationY(loc);
    int r=32;
    while (--r>=0){
        if (CurrentHealth( GetPlayerUnit(r)) )
            MoveObject(GetPlayerUnit(r),x,y);
    }
}

void onYoureWinnerPickup(){
    PlaySoundAround(OTHER,SOUND_JournalEntryAdd);
    UniPrint(OTHER, "당신은 '승리자'이다-!!");
}
void onYouAreLoserPickup(){
    PlaySoundAround(OTHER,SOUND_NoCanDo);
    UniPrint(OTHER, "당신은 '패배자'입니다");
}

int createYouAreLoser(float x,float y){
    int orb=CreateObjectById(OBJ_LANTERN_2, x,y);

    SetUnitCallbackOnPickup(orb, onYouAreLoserPickup);
	return orb;
}

int createYoureWinner(float x, float y){
	int orb=CreateObjectById(OBJ_RED_ORB, x,y);

    SetUnitCallbackOnPickup(orb, onYoureWinnerPickup);
	return orb;
}

void showGameResult(int win){
    int s;
    if (win){
        UniPrintToAll("축하합니다! - 승리하셨습니다. 모든 임무 완료!");
        teleportAllPlayer(25);
        s=CreateObjectById(OBJ_LEVEL_UP, LocationX(25), LocationY(25));
        DeleteObjectTimer( s, 180 );
        PlaySoundAround(s,SOUND_BigGong);
        PlaySoundAround(s,SOUND_StaffOblivionAchieve1);
        return;
    }
    teleportAllPlayer(26);
    createYouAreLoser(LocationX(11), LocationY(11));
    WallGroupSetting(WALLGROUP_startRoomWalls, WALL_GROUP_ACTION_CLOSE);
    UniPrintToAll("미션 실패! 웨이브는 모두 클리어 하였으나 일부 미션이 누락되었습니다. 그래서 패배");
}

int computeGameResult(float x,float y){
    int s;
    QueryGardenClear(&s,0);
    if (s){
        QureyBossClear(&s,0);
        if (s)
        {
            SecondTimerWithArg(8, s, showGameResult);
            QueryResultGameFn(0, createYoureWinner);
            return createYoureWinner(x,y);
        }
    }
    SecondTimerWithArg(8, s, showGameResult);
    QueryResultGameFn(0, createYouAreLoser);
    return createYouAreLoser(x,y);
}

void EndScan(int *pParams)
{
    int count =pParams[UNITSCAN_PARAM_COUNT];
    char msg[128];

    NoxSprintfString(msg, "%d개의 유닛을 스캔했습니다", &count, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
    StartMonSummon();
    InitializeShopSystem();
    WallGroupSetting(WALLGROUP_startRoomWalls, WALL_GROUP_ACTION_OPEN);
}

void createDefaultItems(int pos){
    float x=GetObjectX(pos),y=GetObjectY(pos);
    int rnd=Random(0,3);

    if (rnd==0)
        SetWeaponPropertiesDirect( CreateObjectById(OBJ_LONGSWORD, x,y), 0, ITEM_PROPERTY_Matrial3, ITEM_PROPERTY_fire2, 0);
    else if (rnd==1)
        SetArmorPropertiesDirect( CreateObjectById(OBJ_MEDIEVAL_CLOAK, x,y), ITEM_PROPERTY_armorQuality4, 0, ITEM_PROPERTY_Regeneration2, 0);
    else
        CreateHotPotion2(x,y,0);
    Delete(pos);
}

void startInitscan(){
    int initScanHash, lastUnit=GetMaster();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, PlaceMonsterMarker);
    HashPushback(initScanHash, OBJ_REWARD_MARKER_PLUS, createDefaultItems);
    StartUnitScanEx(Object("firstscan"), lastUnit, initScanHash, EndScan);
}

void initializeReadables(){
    RegistSignMessage(Object("read1"), "전사 기술배우기 및 비활성화 되어진 기술을 활성화를 하기 - 기술마다 가격이 다를 수 있으므로, 잘 확인을 하여 보세요");
    RegistSignMessage(Object("read2"), "!상점지역입니다! - 안전지대 이기도 하며, 여러분이 지금까지 모은 돈은 여기에서 사용을 할 수 있습니다");
    RegistSignMessage(Object("read3"), "상점으로 내려가는 곳이 이 안에 있습니다");
    RegistSignMessage(Object("read4"), "전방에 보이는 저 리프트는 상점 지역으로 이동할 수도 있습니다");
    RegistSignMessage(Object("read5"), "--------------------비밀의 정원-------제작. 237 -----------------------------------------------------");
    RegistSignMessage(Object("read6"), "기본 제공 아이템은 이 방에 다 있습니다. 시작전에 꼭 들르세요");
    RegistSignMessage(Object("read7"), "WARNING! 이곳 아래에는 보스 괴물이 있습니다. 내려가실 때에는 충분히 무장을 하세요");
    RegistSignMessage(Object("read8"), "-승리조건- [I].7곳의 비밀의 정원을 모두 열것, [II].모든 보스를 처치하시오, [III].모든 웨이브를 클리어 하시오");
    RegistSignMessage(Object("read9"), "-비밀의정원 관리자- 모두 7곳의 비밀정원이 있습니다. 모두 열어야 승리할 수 있습니다!");
    RegistSignMessage(Object("read10"), "이곳은 보스구역입니다! 모든 보스를 잡아야 승리할 수 있습니다. 조심하세요");
    RegistSignMessage(Object("read11"), "이 비밀스러운 정원은 다른 정원 보다도 후하게 아이템을 배치해 놓았음");
    RegistSignMessage(Object("read12"), "첫번째 비밀스러운 정원. 앞으로 열릴 비밀의 정원은 이곳보다는 나을거에요");
}

void OnInitializeMap(){
    MusicEvent();
    startInitscan();
    blockObserverMode();
    InitializeMonsterSettings();
    QueryResultGameFn(0, computeGameResult);
    InitializePlayerSystem();
    InitializeShopSystem();
    FrameTimer(1, MakeCoopTeam);
    initializeReadables();
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}
void OnShutdownMap(){
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}
void ExecClientPart()
{
    InitializeResources();
    InitializePopupMessages();
    NetworkUtilClientTimerEnablerExec(ClientProcLoop);
}

void OnPlayerEnteredMap(int pInfo){
    int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
    {
        pUnit = GetMemory(ptr+0x2c);
        int pIndex = GetMemory(pInfo+0x810);
        PlayerClassOnInit   (pIndex, pUnit);
        ShowQuestIntroOne(pUnit, 237, "CopyrightScreen", "Wolchat.c:PleaseWait");
    }
    
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

void onImpMisCollide(){
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER)){
        if (IsAttackedBy(OTHER,SELF))
        {
            Damage(OTHER,SELF,10,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
            Delete(SELF);
        }
    }
}

void onPandaMisCollide(){
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER)){
        if (IsAttackedBy(OTHER,SELF))
        {
            DeleteObjectTimer(CreateObjectById(OBJ_SMOKE, GetObjectX(OTHER), GetObjectY(OTHER)), 9);
            Damage(OTHER,SELF,18,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
            Delete(SELF);
        }
    }
}

void onImpShotDetected(int cur, int owner){
    int id=GetUnitThingID(owner);

    if (id==OBJ_IMP){SetUnitCallbackOnCollide(cur,onImpMisCollide);}
    else if (id==OBJ_BAT){
        SetUnitEnchantCopy(cur,GetLShift(ENCHANT_INFRAVISION)|GetLShift(ENCHANT_SHIELD));
        SetUnitCallbackOnCollide(cur, onPandaMisCollide);
    }
}

void onSmallEnergyBoltCollide(){
    if (!GetCaller()){
        Delete(SELF);
        return;
    }
    if (!GetTrigger()) return;
    if (CurrentHealth(OTHER)){
        if (IsAttackedBy(OTHER,GetMaster()))
        {
            Damage(OTHER,SELF,18,DAMAGE_TYPE_IMPALE);
            DeleteObjectTimer( CreateObjectById(OBJ_OLD_PUFF, GetObjectX(OTHER),GetObjectY(OTHER)), 9);
            Delete(SELF);
        }
    }
}

void deferredSEnergyBolt(int cur){
    if (ToInt(GetObjectX(cur))){
        int owner = GetOwner(cur);

        if (CurrentHealth(owner))
            PushObjectTo(cur,UnitRatioX(cur,owner,22.0),UnitRatioY(cur,owner,22.0));
    }
}

void onSmallEnergyBoltDetected(int cur, int owner){
    PushTimerQueue(3, cur, deferredSEnergyBolt);
    SetUnitEnchantCopy(cur,GetLShift(ENCHANT_REFLECTIVE_SHIELD));
    SetUnitCallbackOnCollide(cur,onSmallEnergyBoltCollide);
}

void onOgreShurikenCollide(){
    if (!GetTrigger()) return;
    if (!GetCaller()){
        Delete(SELF);
        return;
    }
    if (CurrentHealth(OTHER)){
        if (IsAttackedBy(OTHER,GetOwner(SELF)))
        {
            Damage(OTHER,SELF,20,DAMAGE_TYPE_FLAME);
            Damage(OTHER,SELF,50,DAMAGE_TYPE_BLADE);
        }
    }
    Effect("SPARK_EXPLOSION", GetObjectX(SELF),GetObjectY(SELF),0.0,0.0);
    Delete(SELF);
}

void onOgreShrikenDetected(int cur, int owner){
    SetUnitCallbackOnCollide(cur,onOgreShurikenCollide);
}

void InitializeIndexLoop(int pInstance){
    HashPushback(pInstance, OBJ_IMP_SHOT, onImpShotDetected);
    HashPushback(pInstance, OBJ_SMALL_ENERGY_BOLT, onSmallEnergyBoltDetected);
    HashPushback(pInstance, OBJ_OGRE_SHURIKEN, onOgreShrikenDetected);
}

int GetGenericHash(){
    int hash;
    if (!hash)
        HashCreateInstance(&hash);
    return hash;
} //virtual

