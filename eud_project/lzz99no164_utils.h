
#include "lzz99no164_gvar.h"
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"
#include "libs/recovery.h"
#include "libs/winapi.h"

int GetMasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectById(OBJ_HECUBAH, 5500.0, 100.0);
        Frozen(unit, TRUE);
    }
    return unit;
}

void SetUnitMaxHealthEx(int unit, int amount){
    int ptr = UnitToPtr(unit);
    if (ptr)
    {
        int *pHealth=GetMemory(ptr+0x22c);
        if (pHealth==0){
            pHealth=MemAlloc(12);
            SetMemory(ptr+0x22c,pHealth);
        }
        pHealth[0]= amount;
        pHealth[1]= amount;
    }
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}
int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int GetPlayerUnit(int pIndex){
    int *pInfo=0x62f9e0 + (0x12dc*pIndex);

    if (!pInfo[0])
        return 0;

    int *ptr=pInfo[0];
    return ptr[11];
}
