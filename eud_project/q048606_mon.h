
#include "q048606_utils.h"
#include "q048606_reward.h"
#include "libs/voiceList.h"

#define ITEM_QUEUE_MAX_COUNT 200

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4;		
		arr[53] = 1128792064; arr[54] = 4; 
		link =arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 
		link =arr;
	}
	return link;
}

int Bear2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50;
		arr[16] = 20000; arr[17] = 90; arr[18] = 100; arr[19] = 40; 
		arr[21] = 1065353216; arr[23] = 65545; arr[24] = 1067450368; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30;
		arr[58] = 5547856; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int BomberGreenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339074; arr[1] = 1917284965; arr[2] = 7234917;
		arr[17] = 20; arr[19] = 80; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[31] = 4; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
        arr[53] = 1128792064;
		arr[55] = 11; arr[56] = 17; arr[57] = 5548112; arr[58] = 5545344; arr[59] = 5543344; 
		link=arr;
	}
	return link;
}

int AirshipCaptainBinTable2()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 425; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1113325568; arr[29] = 25; arr[31] = 4; arr[32] = 5; arr[33] = 11; 
		arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328; 
	pArr = arr;
	return pArr;
}

void AirshipCaptainSubProcess2(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 1825;	hpTable[1] = 1825;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = AirshipCaptainBinTable2();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 48; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		
		link=arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 90; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064; 
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link=arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
        arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link=arr;
	}
	return link;
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 355; arr[19] = 98; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 45; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; arr[35] = 3; 
		arr[36] = 20; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077684469;		ptr[137] = 1077684469;
	int *hpTable = ptr[139];
	hpTable[0] = 355;	hpTable[1] = 355;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int UrchinShamanBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751347797; arr[1] = 1750298217; arr[2] = 1851878753;
		arr[17] = 20; arr[19] = 50; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[26] = 4; 		
		arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; 
		arr[40] = 116; 
		arr[53] = 1128792064;
		arr[55] = 5; arr[56] = 11;
		link=arr;
	}
	return link;
}

void PurpleGirlSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 1.5);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        CastSpellObjectObject("SPELL_MAGIC_MISSILE", SELF, OTHER);
        Delete(ptr);
        Delete(ptr + 2);
        Delete(ptr + 3);
        Delete(ptr + 4);
    }
    CheckResetSight(GetTrigger(), 26);
}

void GoonShootWeb()
{
    if (!HasEnchant(SELF, "ENCHANT_SNEAK"))
    {
        int ptr = CreateObjectAt("SpiderSpit", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 20.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 20.0));

        SetOwner(SELF, CreateObjectAt("ThrowingStone", GetObjectX(ptr), GetObjectY(ptr)));
        SetOwner(SELF, ptr);
        PlaySoundAround(ptr, SOUND_EggBreak);
        PlaySoundAround(ptr, SOUND_MeatDrop);
        PushObjectTo(ptr, -UnitRatioX(SELF, OTHER, 50.0), -UnitRatioY(SELF, OTHER, 50.0));
        PushObjectTo(ptr + 1, -UnitRatioX(SELF, OTHER, 50.0), -UnitRatioY(SELF, OTHER, 50.0));
        Enchant(SELF, "ENCHANT_SNEAK", 2.5);
    }
    CheckResetSight(GetTrigger(), 38);
}

void SkeletonLordThunderHit()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 4.0);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(SELF, ptr);
        DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", GetObjectX(ptr), GetObjectY(ptr)), 30);
        PlaySoundAround(ptr, SOUND_LightningCast);
        Effect("LIGHTNING", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectY(OTHER), GetObjectY(OTHER) - 200.0);
        PushTimerQueue(39, ptr, DelayThunderRisk);
    }
    CheckResetSight(GetTrigger(), 50);
}

void DelayThunderRisk(int ptr)
{
    float xpos=GetObjectX(ptr),ypos=GetObjectY(ptr);

    if (IsObjectOn(ptr))
    {
        Effect("BLUE_SPARKS", xpos,ypos, 0.0, 0.0);
        int pic = CreateObjectAt("ShopkeeperLandOfTheDead", xpos,ypos);
        SetOwner(GetOwner(ptr), pic);
        SetCallback(pic, 9, ThunderTouchEvent);
        DeleteObjectTimer(pic, 1);
    }
}

void ThunderTouchEvent()
{
    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, GetOwner(SELF)))
    {
        PlaySoundAround(OTHER, SOUND_LightningBolt);
        Effect("RICOCHET", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, GetOwner(SELF), 35, 9);
    }
}

void ZombieShadowHit()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 1.5);
        MoveObject(SELF, GetObjectX(OTHER) - UnitAngleCos(OTHER, 28.0), GetObjectY(OTHER) - UnitAngleSin(OTHER, 28.0));
        LookAtObject(SELF, OTHER);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
    }
    CheckResetSight(GetTrigger(), 30);
}

void ZombieRespawnWhenDead()
{
    int dir = GetDirection(SELF), unit;
    float xpos=GetObjectX(SELF),ypos=GetObjectY(SELF);
    UnitCommonDeadEvent();
    Delete(SELF);
    unit = SpawnInfiniteZombie(xpos,ypos);
    LookWithAngle(unit, dir);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
}

int SpawnInfiniteZombie(float xpos, float ypos)
{
    int unit = CreateObjectAt("Zombie", xpos,ypos);

    SetUnitMaxHealth(unit, 128);
    SetCallback(unit, 5, ZombieDeadEvent);
    return unit;
}

void ZombieDeadEvent()
{
    PushTimerQueue(23, GetTrigger(), ReviveZombie);
}

void ReviveZombie(int unit)
{
    if (MaxHealth(unit))
    {
        float xpos=GetObjectX(SELF),ypos=GetObjectY(SELF);
        
        int ptr = CreateObjectAt("Magic", xpos,ypos);
        Frozen(ptr, TRUE);
        DeleteObjectTimer(ptr, 6);
        GreenSparkAt(xpos,ypos);
        PlaySoundAround(ptr, SOUND_VampirismOn);
        RaiseZombie(unit);
    }
}

void SpiderDeadEvent()
{
    float x=GetObjectX(SELF),y=GetObjectY(SELF);

    UnitCommonDeadEvent();
    PlaySoundAround(SELF, SOUND_BeholderDie);
    PlaySoundAround(SELF, SOUND_PoisonTrapTriggered);
    DeleteObjectTimer(CreateObjectAt("WaterBarrelBreaking", x,y), 12);
    DeleteObjectTimer(CreateObjectAt("BigSmoke", x,y), 9);
    CreateObjectAt("ArachnaphobiaFocus", x,y);
}

void LichDeadEvent()
{
    UnitCommonDeadEvent();
    DeleteObjectTimer(SELF, 20);
}

void LichSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        int unit = CreateObjectAt("Maiden", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 18.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 18.0));
        SetOwner(SELF, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit),GetObjectY(unit)));
        Raise(unit + 1, UnitRatioX(SELF, OTHER, 25.0));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit),GetObjectY(unit)), UnitRatioY(SELF, OTHER, 25.0));
        ObjectOff(unit);
        Damage(unit, 0, MaxHealth(unit) + 1, -1);
        Frozen(unit, 1);
        SetCallback(unit, 9, CollideLichBullet);
        Enchant(SELF, "ENCHANT_BURNING", 2.5);
        LookAtObject(SELF, OTHER);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        PushTimerQueue(1, unit, ImpulseLichMissile);
    }
    CheckResetSight(GetTrigger(), 50);
}

void ImpulseLichMissile(int ptr)
{
    int owner = GetOwner(ptr + 1), count = GetDirection(ptr + 1);

    if (MaxHealth(ptr))
    {
        if (CurrentHealth(owner) && count < 30 && IsVisibleTo(ptr + 1, ptr))
        {
            MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr + 1), GetObjectY(ptr) - GetObjectZ(ptr + 2));
            MoveObject(ptr + 2, GetObjectX(ptr), GetObjectY(ptr));
            DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(ptr), GetObjectY(ptr)), 6);
            LookWithAngle(ptr + 1, count + 1);
        }
        else
        {
            Frozen(ptr, 0);
            Delete(ptr);
            LookWithAngle(ptr + 1, 200);
        }
        PushTimerQueue(1, ptr, ImpulseLichMissile);
    }
    else
    {
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void CollideLichBullet()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Effect("COUNTERSPELL_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, owner, 20, 5);
        Delete(SELF);
    }
}

void CherubSightEvent()
{
    int ptr;

    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 16.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 16.0));
        LookWithAngle(ptr, GetDirection(SELF));
        SetOwner(SELF, ptr);
        Enchant(SELF, "ENCHANT_BURNING", 2.0);
        PushTimerQueue(3, ptr, TripleMissile);
    }
    CheckResetSight(GetTrigger(), 35);
}

void TripleMissile(int arg)
{
    int owner = GetOwner(arg), k, ptr = CreateObject("InvisibleLightBlueHigh", 10) + 1, dir;

    Delete(ptr - 1);
    if (CurrentHealth(owner) && IsObjectOn(arg) && CurrentHealth(GetOwner(arg)))
    {
        dir = GetDirection(arg) - 7;
        for (k = 0 ; k < 15 ; k ++)
        {
            SetOwner(GetOwner(arg), CreateObjectAt("CherubArrow", GetObjectX(arg) + UnitAngleCos(arg, 16.0), GetObjectY(arg) + UnitAngleSin(arg, 16.0)));
            LookWithAngle(arg, dir + k);
            LookWithAngle(ptr + k, GetDirection(arg));
            PushObjectTo(ptr + k, UnitRatioX(arg, ptr + k, 18.0), UnitRatioY(arg, ptr + k, 18.0));
        }
    }
    Delete(arg);
}

void OgreLordSightEvent()
{
    int ptr;

    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        LookAtObject(SELF, OTHER);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        UniChatMessage(SELF, "죽어라, 하찮은 인간!", 90);
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 20.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 20.0));
        PlaySoundAround(SELF, SOUND_TowerShoot);
        SetOwner(SELF, ptr);
        Raise(ptr, UnitRatioX(OTHER, SELF, 20.0));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr),GetObjectY(ptr)), UnitRatioY(OTHER, SELF, 20.0));
        Enchant(SELF, "ENCHANT_ETHEREAL", 3.0);
        PushTimerQueue(1, ptr, OgreBulletLoop);
    }
    CheckResetSight(GetTrigger(), 40);
}

void OgreBulletLoop(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner) && count < 30)
        {
            unit = CreateObjectAt("ShopkeeperConjurerRealm", GetObjectX(ptr), GetObjectY(ptr));
            DeleteObjectTimer(CreateObjectAt("PiledBarrels3Breaking", GetObjectX(ptr), GetObjectY(ptr)), 9);
            UnitNoCollide(unit + 1);
            SetCallback(unit, 9, OgreBulletTouchEvent);
            Frozen(unit, 1);
            DeleteObjectTimer(unit, 1);
            MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr), GetObjectY(ptr) + GetObjectZ(ptr + 1));
        }
        else
        {
            Delete(ptr);
            Delete(ptr + 1);
        }
    }
}

void OgreBulletTouchEvent()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 50, 2);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
    }
}

void SpecialBearSightEvent()
{
    int ptr;
    float r;

    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        LookAtObject(SELF, OTHER);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        r = Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) / 36.0;
        AudioEvent("EmberDemonHitting", 10);
        AudioEvent("SwordMissing", 10);
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        Raise(ptr, UnitRatioX(OTHER, SELF, r));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), UnitRatioY(OTHER, SELF, r));
        UnitNoCollide(CreateObjectAt("BoulderCave", GetObjectX(ptr), GetObjectY(ptr)));
        PlaySoundAround(SELF, SOUND_EmberDemonHitting);
        PlaySoundAround(SELF, SOUND_SwordMissing);
        SetOwner(SELF, ptr);
        Enchant(SELF, "ENCHANT_ETHEREAL", 2.0);
        PushTimerQueue(1, ptr, SpecialBearBulletLoop);
    }
    CheckResetSight(GetTrigger(), 30);
}

void SpecialBearBulletLoop(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), pic;

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner) && count < 36)
        {
            MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr), GetObjectY(ptr) + GetObjectZ(ptr + 1));
            MoveObject(ptr + 2, GetObjectX(ptr), GetObjectY(ptr));
            if (count < 18)
                Raise(ptr + 2, GetObjectZ(ptr + 2) + 11.0);
            else
                Raise(ptr + 2, GetObjectZ(ptr + 2) - 11.0);
            LookWithAngle(ptr, count + 1);
            PushTimerQueue(1, ptr, SpecialBearBulletLoop);
        }
        else
        {
            float x=GetObjectX(ptr),y=GetObjectY(ptr);
            AudioEvent("BerserkerCrash", 10);
            PlaySoundAround(ptr,SOUND_BerserkerCrash);
            Effect("JIGGLE", x,y, 75.0, 0.0);
            Effect("DAMAGE_POOF", x,y, 100.0, 0.0);
            Delete(ptr);
            Delete(ptr + 1);
            Delete(ptr + 2);
            DeleteObjectTimer(CreateObjectAt("BigSmoke", x,y), 9);
            SplashDamage(owner, 50, 200.0, x,y);
        }
    }
}

void GrnBomberSightEvent()
{
    int ptr;

    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 0.8);
        ptr = CreateObjectAt("DeathBallFragment", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 18.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 18.0));
        PlaySoundAround(ptr, SOUND_PixieHit);
        SetOwner(SELF, ptr);
        PushObjectTo(ptr, UnitRatioX(OTHER, SELF, 55.0), UnitRatioY(OTHER, SELF, 55.0));
        DeleteObjectTimer(ptr, 12);
    }
    CheckResetSight(GetTrigger(), 15);
}

void ShamanSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 2.0);
        int mis = CreateObjectAt("DeathBallFragment", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 19.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 19.0));
        SetOwner(SELF, mis);
        PushObjectTo(mis, UnitRatioX(OTHER, SELF, 42.0), UnitRatioY(OTHER, SELF, 42.0));
        DeleteObjectTimer(mis, 27);
    }
    CheckResetSight(GetTrigger(), 29);
}

void CheckResetSight(int unit, int delay)
{
    if (!HasEnchant(unit, "ENCHANT_DETECTING"))
    {
        Enchant(unit, "ENCHANT_DETECTING", 0.0);
        PushTimerQueue(delay, unit, ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.06);
    AggressionLevel(unit, 1.0);
}

void UnitHurtEvent()
{
    int idx;

    if (GetUnitClass(OTHER)&UNIT_CLASS_WEAPON)
    {
        if (IsPlayerUnit(GetOwner(OTHER)))
        {
            idx = WUpgradeTable(GetMemory(0x979720));
            if (idx && GetDirection(OTHER))
            {
                Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
                Damage(SELF, OTHER, idx * GetDirection(OTHER), 14);
            }
        }
    }
}

void UnitCommonProperties(int unit)
{
    SetCallback(unit, 7, UnitHurtEvent);
    SetOwner(GetMasterUnit(), unit);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    AggressionLevel(unit, 1.0);
}

int getItemQueue()
{
    int queue;

    if (!queue)
        LinkedListCreateInstance(&queue);
    return queue;
}

int getItemQueueHash()
{
    int hash;
    if (!hash)
        HashCreateInstance(&hash);
    return hash;
}

void removeItemOnQueue(int item)
{
    int node=0;
    HashGet(getItemQueueHash(),item, &node, TRUE);
    if (node)
        LinkedListPop(getItemQueue(),node);
}

void onQueueItemPickup(){
    WispDestroyFX(GetObjectX(OTHER),GetObjectY(OTHER));
    removeItemOnQueue(GetTrigger());
}

void pushItemQueue(int item)
{
    int queue=getItemQueue();
    LinkedListPushback(queue, item, NULLPTR);
    int node=0;
    if (LinkedListCount(queue)>ITEM_QUEUE_MAX_COUNT)
    {
        if (LinkedListFront(queue,&node))
        {
            int prev=LinkedListGetValue(node);            
            removeItemOnQueue(prev);

            if (ToInt(GetObjectX(prev))){
                DeleteObjectTimer(prev,1);
            }
        }
    }
    LinkedListBack(queue, &node);
    HashPushback(getItemQueueHash(),item,node);
    RegistItemPickupCallback(item, onQueueItemPickup);
}

void UnitCommonDeadEvent()
{
    float x=GetObjectX(SELF),y= GetObjectY(SELF);
    int item = CreateRandomItemCommonRet(CreateObjectById(OBJ_AIRSHIP_BASKET_SHADOW,x,y));

    if (item)
        pushItemQueue(item);
    CheckCurrentKills();
    DeleteObjectTimer(SELF, 3);
}

int RatBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 7627090; arr[17] = 85; arr[18] = 1; arr[19] = 40; arr[21] = 1065353216; 
		arr[24] = 1073741824; arr[26] = 4; arr[28] = 1112014848; arr[29] = 6; arr[31] = 8; 
		arr[32] = 6; arr[33] = 18; arr[59] = 5542784; arr[60] = 1359; arr[61] = 46906112; 
	pArr = arr;
	return pArr;
}

void RatSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1067030937;		ptr[137] = 1067030937;
	int *hpTable = ptr[139];
	hpTable[0] = 85;	hpTable[1] = 85;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = RatBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonMonsterRat(int wp)
{
    int unit = CreateObject("Rat", wp);

    SetCallback(unit, 5, UnitCommonDeadEvent);
    SetUnitMaxHealth(unit, 85);
    RatSubProcess(unit);
    SetUnitSpeed(unit, 1.2);
    SetUnitVoice(unit, 22);
    UnitCommonProperties(unit);
    return unit;
}

int SummonUrchin(int wp)
{
    int unit = CreateObject("Urchin", wp);

    SetUnitMaxHealth(unit, 70);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    UnitZeroFleeRange(unit);
    SetUnitVoice(unit,MONSTER_VOICE__Maiden1);
    return unit;
}

int SummonImp(int wp)
{
    int unit = CreateObject("Imp", wp);

    SetUnitMaxHealth(unit, 60);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    Enchant(unit, "ENCHANT_SLOWED", 0.0);

    return unit;
}

int GreenFrogBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[24] = 1065353216; arr[28] = 1101004800; 
		arr[29] = 20; arr[31] = 10; arr[32] = 6; arr[33] = 11; arr[59] = 5544320;
		link = &arr;
	}
	return link;
}

void FrogMissileAttackLoop(int ptr)
{
    int owner = GetOwner(ptr + 1), count = GetDirection(ptr + 1);

    if (MaxHealth(ptr))
    {
        if (CurrentHealth(owner) && count < 20 && IsVisibleTo(owner, ptr))
        {
            MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr + 1), GetObjectY(ptr) - GetObjectZ(ptr + 2));
            LookWithAngle(ptr + 1, count + 1);
        }
        else
        {
            LookWithAngle(ptr + 1, 200);
            Delete(ptr);
        }
        PushTimerQueue(1, ptr, FrogMissileAttackLoop);
    }
    else
    {
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void CollideFishBig()
{
    int owner = GetTrigger() + 1;

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, GetOwner(SELF), 4, 11);
        Delete(SELF);
    }
}

void FrogSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        LookAtObject(SELF, OTHER);
        LookAtObject(GetTrigger() + 1, OTHER);
        Walk(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
        Enchant(SELF, "ENCHANT_BURNING", 0.9);
        int ptr = CreateObjectAt("FishBig", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 16.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 16.0));
        SetUnitFlags(ptr, GetUnitFlags(ptr) ^ (UNIT_FLAG_NO_PUSH_CHARACTERS));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr),GetObjectY(ptr)), UnitRatioX(SELF, OTHER, 22.0));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr),GetObjectY(ptr)), UnitRatioY(SELF, OTHER, 22.0));
        ObjectOff(ptr);
        LookAtObject(ptr, OTHER);
        Frozen(ptr, 1);
        SetCallback(ptr, 9, CollideFishBig);
        SetOwner(GetOwner(SELF), ptr);
        SetOwner(SELF, ptr + 1);
        PushTimerQueue(1, ptr, FrogMissileAttackLoop);
    }
    CheckResetSight(GetTrigger(), 25);
}

int SummonFrog(int wp)
{
    int unit = CreateObject("GreenFrog", wp);

    SetCallback(unit, 3, FrogSightEvent);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitLinkBinScript(unit, GreenFrogBinTable());
    SetUnitSpeed(unit, 1.3);
    SetUnitMaxHealth(unit, 105);
    SetUnitVoice(unit, 49);
    UnitCommonProperties(unit);
    return unit;
}

int SummonMiniSpider(int wp)
{
    int unit = CreateObject("SmallAlbinoSpider", wp);

    SetUnitMaxHealth(unit, 98);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    Enchant(unit, "ENCHANT_SLOWED", 0.0);

    return unit;
}

int SummonWildWolf(int wp)
{
    int unit = CreateObject("Wolf", wp);

    SetUnitMaxHealth(unit, 130);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);

    return unit;
}

int SummonTheif(int wp)
{
    int unit = CreateObject("Swordsman", wp);

    SetUnitMaxHealth(unit, 175);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);

    return unit;
}

int SummonTheifWithBow(int wp)
{
    int unit = CreateObject("Archer", wp);

    SetUnitMaxHealth(unit, 125);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);

    return unit;
}

int SummonGiantWorms(int wp)
{
    int unit = CreateObject("GiantLeech", wp);

    SetUnitMaxHealth(unit, 200);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);

    return unit;
}

void FlierBulletLoop(int ptr)
{
    int lt = GetDirection(ptr), enemy = ToInt(GetObjectZ(ptr)), pic;

    if (IsObjectOn(ptr))
    {
        if (lt < 17 && CurrentHealth(enemy) && IsVisibleTo(ptr, enemy))
        {
            if (Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(enemy), GetObjectY(enemy)) > 40.0)
            {
                MoveObject(ptr, GetObjectX(ptr) - UnitRatioX(ptr, enemy, 23.0), GetObjectY(ptr) - UnitRatioY(ptr, enemy, 23.0));
                pic = CreateObjectAt("HarpoonBolt", GetObjectX(ptr), GetObjectY(ptr));
                Frozen(pic, 1);
                LookAtObject(pic, enemy);
                DeleteObjectTimer(pic, 3);
                LookWithAngle(ptr, lt + 1);
            }
            else
            {
                DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(enemy), GetObjectY(enemy)), 9);
                Damage(enemy, GetOwner(ptr), 8, 0);
                PlaySoundAround(enemy, SOUND_PowderBarrelExplode);
                PlaySoundAround(enemy, SOUND_BerserkerCrash);
                LookWithAngle(ptr, 200);
            }
        }
        else
            Delete(ptr);
        PushTimerQueue(1, ptr, FlierBulletLoop);
    }
}

void FlierSightEvent()
{
    int ptr;

    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 2.0);
        LookAtObject(SELF, OTHER);
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0));
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        PushTimerQueue(1, ptr, FlierBulletLoop);
    }
    CheckResetSight(GetTrigger(), 30);
}

int SummonFlier(int wp)
{
    int unit = CreateObject("FlyingGolem", wp);

    SetUnitMaxHealth(unit, 140);
    SetCallback(unit, 3, FlierSightEvent);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    SetUnitScanRange(unit,450.0);
    UnitCommonProperties(unit);

    return unit;
}

int SummonWebSpider(int wp)
{
    int unit = CreateObject("SpittingSpider", wp);

    SetUnitMaxHealth(unit, 170);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);

    return unit;
}

int SummonShade(int wp)
{
    int unit = CreateObject("Shade", wp);

    SetUnitMaxHealth(unit, 250);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    
    return unit;
}

void ScorpionHit()
{
    UnitHurtEvent();
    if (CurrentHealth(SELF) < (MaxHealth(SELF) / 2))
    {
        if (!HasEnchant(SELF, "ENCHANT_SNEAK"))
        {
            Enchant(SELF, "ENCHANT_SNEAK", 10.0);
            CastSpellObjectLocation("SPELL_TOXIC_CLOUD", SELF, GetObjectX(SELF), GetObjectY(SELF));
        }
    }
}

int SummonScorpion(int wp)
{
    int unit = CreateObject("Scorpion", wp);

    SetUnitMaxHealth(unit, 295);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    SetCallback(unit, 7, ScorpionHit);
    return unit;
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 245; arr[19] = 99; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; 
		arr[26] = 4; arr[28] = 1112014848; arr[29] = 40; arr[30] = 1106247680; arr[31] = 2; 
		arr[32] = 12; arr[33] = 20; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077810299;		ptr[137] = 1077810299;
	int *hpTable = ptr[139];
	hpTable[0] = 245;	hpTable[1] = 245;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonBlueDog(int wp)
{
    int unit = CreateObjectById(OBJ_WOUNDED_APPRENTICE, LocationX(wp),LocationY(wp));
    
    WoundedApprenticeSubProcess(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    return unit;
}

int WhiteWolfBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1953065047; arr[1] = 1819236197; arr[2] = 102; arr[17] = 260; arr[18] = 50; 
		arr[19] = 110; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; 
		arr[26] = 5; arr[27] = 5; arr[28] = 1110704128; arr[29] = 40; arr[31] = 10; 
		arr[32] = 6; arr[33] = 11; arr[59] = 5542784; arr[60] = 1367; arr[61] = 46902016; 
	pArr = arr;
	return pArr;
}

void WhiteWolfSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1079194419;		ptr[137] = 1079194419;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WhiteWolfBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int SummonWhiteWolf(int wp)
{
    int unit = CreateObjectById(OBJ_WHITE_WOLF, LocationX(wp),LocationY(wp));

    WhiteWolfSubProcess(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    SetUnitVoice(unit,MONSTER_VOICE__Maiden2);
    return unit;
}

void TrollBulletLoop(int ptr)
{
    int enemy = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(enemy) && count < 30)
        {
            if (DistanceUnitToUnit(enemy,ptr) > 46.0)
            {
                MoveObject(ptr, GetObjectX(ptr) - UnitRatioX(ptr, enemy, 23.0), GetObjectY(ptr) - UnitRatioY(ptr, enemy, 23.0));
                MoveObject(ptr + 1, GetObjectX(ptr), GetObjectY(ptr));
                LookWithAngle(ptr, count + 1);
            }
            else
            {
                DeleteObjectTimer(CreateObjectAt("Smoke", GetObjectX(ptr), GetObjectY(ptr)), 9);
                Damage(enemy, GetOwner(ptr), 15, 11);
                PlaySoundAround(ptr,SOUND_HitStoneBreakable);
                PlaySoundAround(ptr,SOUND_AxeMissing);
                LookWithAngle(ptr, 200);
            }
        }
        else
        {
            Delete(ptr);
            Delete(ptr + 1);
        }
        PushTimerQueue(1, ptr, TrollBulletLoop);
    }
}

void TrollSightEvent()
{
    int ptr;

    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        Enchant(SELF, "ENCHANT_BURNING", 5.0);
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 20.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 20.0));
        Raise(CreateObjectAt("CaveRocksHuge", GetObjectX(ptr), GetObjectY(ptr)), ToFloat(GetCaller()));
        SetOwner(SELF, ptr);
        LookAtObject(SELF, OTHER);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        PushTimerQueue(1, ptr, TrollBulletLoop);
    }
    CheckResetSight(GetTrigger(), 35);
}

int SummonTroll(int wp)
{
    int unit = CreateObject("Troll", wp);
    
    SetUnitMaxHealth(unit, 305);
    SetCallback(unit, 3, TrollSightEvent);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int WoundedWarriorBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1466197348; arr[2] = 1769108065; arr[3] = 29295; arr[17] = 275; 
		arr[19] = 105; arr[21] = 1065353216; arr[23] = 65537; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1120403456; arr[29] = 20; arr[31] = 9; arr[32] = 16; arr[33] = 26; 
		arr[59] = 5542784; arr[60] = 2272; arr[61] = 46912512; 
	pArr = arr;
	return pArr;
}

void WoundedWarriorSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1078565273;		ptr[137] = 1078565273;
	int *hpTable = ptr[139];
	hpTable[0] = 275;	hpTable[1] = 275;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = WoundedWarriorBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onScamWispDeath(){
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    UnitCommonDeadEvent();
    WispDestroyFX(x,y);
    Effect("SMOKE_BLAST",x,y,0.0,0.0);
    Effect("RICOCHET",x,y,0.0,0.0);
}

int SummonWisp(int wp)
{
    int unit = CreateObjectById(OBJ_WOUNDED_WARRIOR,LocationX(wp),LocationY(wp));

    WoundedWarriorSubProcess(unit);
    SetCallback(unit, 5, onScamWispDeath);
    UnitCommonProperties(unit);
    SetUnitVoice(unit,MONSTER_VOICE_WillOWisp);
    return unit;
}

int GruntAxeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853190727; arr[1] = 1702379892; arr[17] = 260; arr[19] = 99; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1108082688; arr[29] = 38; arr[30] = 1097859072; arr[32] = 15; arr[33] = 25; 
		arr[59] = 5542432; arr[60] = 1336; 
	pArr = arr;
	return pArr;
}

void GruntAxeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077810299;		ptr[137] = 1077810299;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GruntAxeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int SummonGruntAxe(int wp)
{
    int unit = CreateObject("GruntAxe", wp);

    GruntAxeSubProcess(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int OgreBruteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996367; arr[1] = 1953854018; arr[2] = 101; arr[17] = 300; arr[19] = 108; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 2; arr[26] = 10; 
		arr[27] = 7; arr[28] = 1112014848; arr[29] = 45; arr[30] = 1101004800; arr[31] = 11; 
		arr[32] = 10; arr[33] = 20; arr[59] = 5542784; arr[60] = 1390; arr[61] = 46913792; 
	pArr = arr;
	return pArr;
}

void OgreBruteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1078942761;		ptr[137] = 1078942761;
	int *hpTable = ptr[139];
	hpTable[0] = 300;	hpTable[1] = 300;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = OgreBruteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int SummonOgreBrute(int wp)
{
    int unit = CreateObject("OgreBrute", wp);
    
    OgreBruteSubProcess(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int WizardGreenBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 1917281394; arr[2] = 7234917; arr[17] = 320; arr[19] = 110; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1118437376; 
		arr[29] = 25; arr[31] = 4; arr[32] = 18; arr[33] = 28; arr[53] = 1128792064; 
		arr[54] = 4; arr[59] = 5542784; arr[60] = 1335; arr[61] = 46914816; 
	pArr = arr;
	return pArr;
}

void WizardGreenSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1079194419;		ptr[137] = 1079194419;
	int *hpTable = ptr[139];
	hpTable[0] = 320;	hpTable[1] = 320;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WizardGreenBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonDryad(int wp)
{
    int unit = CreateObject("WizardGreen", wp);

    WizardGreenSubProcess(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int BlackBearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1667329090; arr[1] = 1634026091; arr[2] = 114; arr[17] = 350; arr[19] = 105; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1108869120; arr[29] = 64; arr[30] = 1114636288; arr[31] = 2; 
		arr[32] = 25; arr[33] = 30; arr[59] = 5542784; arr[60] = 1366; arr[61] = 46902784; 
	pArr = arr;
	return pArr;
}

void BlackBearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1078565273;		ptr[137] = 1078565273;
	int *hpTable = ptr[139];
	hpTable[0] = 350;	hpTable[1] = 350;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = BlackBearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int SummonDarkBear(int wp)
{
    int unit = CreateObject("BlackBear", wp);
    
    BlackBearSubProcess(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    return unit;
}

int SummonGoon(int wp)
{
    int unit = CreateObject("Goon", wp);

    GoonSubProcess(unit);
    SetUnitVoice(unit, MONSTER_VOICE__Guard2);
    SetCallback(unit, 3, GoonShootWeb);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonShaman(int wp)
{
    int unit = CreateObject("UrchinShaman", wp);

    SetUnitMaxHealth(unit, 198);

    UnitLinkBinScript(unit, UrchinShamanBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    UnitZeroFleeRange(unit);

    return unit;
}

int SummonSkeleton(int wp)
{
    int unit = CreateObject("Skeleton", wp);

    SetUnitMaxHealth(unit, 295);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonSkeletonLord(int wp)
{
    int unit = CreateObject("SkeletonLord", wp);
    
    SetUnitMaxHealth(unit, 305);
    SetCallback(unit, 3, SkeletonLordThunderHit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonZombie(int wp)
{
    int unit = CreateObject("VileZombie", wp);
    
    SetUnitMaxHealth(unit, 290);
    SetCallback(unit, 3, ZombieShadowHit);
    SetCallback(unit, 5, ZombieRespawnWhenDead);
    UnitCommonProperties(unit);
    return unit;
}

int SummonDarkSpider(int wp)
{
    int unit = CreateObject("BlackWidow", wp);
    
    SetUnitVoice(unit, 19);
    UnitLinkBinScript(unit, BlackWidowBinTable());
    SetUnitMaxHealth(unit, 325);
    SetCallback(unit, 5, SpiderDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonGargoyle(int wp)
{
    int unit = CreateObject("EvilCherub", wp);
    
    SetUnitMaxHealth(unit, 128);
    SetCallback(unit, 3, CherubSightEvent);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonFireFairy(int wp)
{
    int unit = CreateObject("FireSprite", wp);
    
    SetUnitMaxHealth(unit, 192);
    UnitLinkBinScript(unit, FireSpriteBinTable());
    SetCallback(unit, 5, UnitCommonDeadEvent);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    UnitCommonProperties(unit);
    return unit;
}

int SummonEmberDemon(int wp)
{
    int unit = CreateObject("EmberDemon", wp);
    
    SetUnitMaxHealth(unit, 225);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonMystic(int wp)
{
    int unit = CreateObject("Wizard", wp);
    
    SetUnitMaxHealth(unit, 275);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SummonFeminist(int wp)
{
    int unit = ColorMaiden(255, 0, 32, wp);
    
    UnitLinkBinScript(unit, MaidenBinTable());
    SetUnitMaxHealth(unit, 380);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonOgreWarlord(int wp)
{
    int unit = CreateObject("OgreWarlord", wp);
    
    SetUnitMaxHealth(unit, 385);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    SetCallback(unit, 3, OgreLordSightEvent);
    UnitCommonProperties(unit);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    SetUnitScanRange(unit,600.0);
    return unit;
}

int SummonSpecialBear(int wp)
{
    int unit = CreateObject("Bear", wp);
    
    SetUnitMaxHealth(unit, 410);
    SetCallback(unit, 3, SpecialBearSightEvent);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    SetUnitScanRange(unit,600.0);
    return unit;
}

int SummonDeadFlower(int wp)
{
    int unit = CreateObject("CarnivorousPlant", wp);

    SetUnitMaxHealth(unit, 480);
    SetUnitSpeed(unit, 1.4);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonJandor(int wp)
{
    int unit = CreateObject("AirshipCaptain", wp);

    UnitLinkBinScript(unit, AirshipCaptainBinTable());
    SetUnitMaxHealth(unit, 455);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);

    return unit;
}

int SummonBeholder(int wp)
{
    int unit = CreateObject("Beholder", wp);

    SetUnitMaxHealth(unit, 420);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SummonGreenBomber(int wp)
{
    int unit = CreateObject("BomberGreen", wp);
    int ptr = GetMemory(0x750710);

    SetUnitVoice(unit, 56);
    SetUnitMaxHealth(unit, 210);
    SetMemory(ptr + 0x2b8, 0x4e83b0);
    UnitLinkBinScript(unit, BomberGreenBinTable());
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);

    return unit;
}

int WoundedConjurerBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1130653028; arr[2] = 1969909359; arr[3] = 7497074; arr[17] = 490; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1113325568; arr[29] = 48; arr[31] = 4; arr[32] = 14; arr[33] = 24; 
		arr[59] = 5542784; arr[60] = 2271; arr[61] = 46912768; 
	pArr = arr;
	return pArr;
}

void WoundedConjurerSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 490;	hpTable[1] = 490;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedConjurerBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SummonBigRedHead(int wp)
{
    int unit = CreateObjectById(OBJ_WOUNDED_CONJURER, LocationX(wp),LocationY(wp));

    WoundedConjurerSubProcess(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    SetUnitVoice(unit,MONSTER_VOICE_BomberYellow);
    return unit;
}

int SummonDemon(int wp)
{
    int unit = CreateObject("Demon", wp);

    SetUnitMaxHealth(unit, 400);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonMimic(int wp)
{
    int unit = CreateObject("Mimic", wp);

    SetUnitMaxHealth(unit, 600);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonStoneGolem(int wp)
{
    int unit = CreateObject("StoneGolem", wp);

    SetUnitMaxHealth(unit, 800);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonGirl2(int wp)
{
    int unit = ColorMaiden(0, 128, 225, wp);

    SetUnitMaxHealth(unit, 550);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    SetCallback(unit, 3, PurpleGirlSightEvent);
    return unit;
}

int SummonLich(int wp)
{
    int unit = CreateObject("OgreBrute", wp);

    SetUnitMaxHealth(unit, 580);
    SetCallback(unit, 5, LichDeadEvent);
    UnitCommonProperties(unit);
    SetCallback(unit, 3, LichSightEvent);
    SetUnitScanRange(unit, 600.0);
    return unit;
}

int SummonHorrendous(int wp)
{
    int unit = CreateObject("Horrendous", wp);

    SetUnitSpeed(unit, 1.2);
    SetUnitMaxHealth(unit, 600);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}

int SummonBeast(int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 480);
    UnitLinkBinScript(unit, WeirdlingBeastBinTable());
    UnitZeroFleeRange(unit);
    SetCallback(unit, 5, UnitCommonDeadEvent);
    UnitCommonProperties(unit);
    return unit;
}



