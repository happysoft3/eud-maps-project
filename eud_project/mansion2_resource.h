
#include "fieldguide.h"
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/clientside.h"
#include "libs/objectIDdefines.h"

#define SENTRY_COLOR_BLUE 0x037F

void MapBgmData()
{ }

void GRPDump_SeleneOutput(){}
void initializeSeleneImageFrame(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_NECROMANCER;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SeleneOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS, 2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS, 2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS, 2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS, 2);
    AnimFrameAssign4Directions(0,  131415, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy4Directions(131415,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy4Directions(131415,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy4Directions(131415,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

#define OBUNGAH_IMAGE_START_AT 133943
void GRPDump_ObungahOutput(){}
void initializeObungaOutputImageFrame(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_ObungahOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameAssign4Directions(0,  OBUNGAH_IMAGE_START_AT, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy4Directions(OBUNGAH_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy4Directions(OBUNGAH_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy4Directions(OBUNGAH_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
    AppendImageFrame(imgVector, frames[1],14527);
}
#define YOSHINE_IMAGE_START_AT 122915
void GRPDump_YoshineOutput(){}
void initializeYOSHINEImageFrame(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_YoshineOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameAssign4Directions(0,  YOSHINE_IMAGE_START_AT, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy4Directions(YOSHINE_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy4Directions(YOSHINE_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy4Directions(YOSHINE_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_globglogabgalabOutput(){}

#define GLOBGLOGABGALAB_IMAGE_START_ID 129717
void initializeImage_globglogabgalab(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int xyInc[]={0,-3};
    ChangeSpriteImageSize(thingId,128,128);
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_globglogabgalabOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4, 3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4, 3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    AnimFrameAssign8Directions(0,GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(10,GLOBGLOGABGALAB_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(GLOBGLOGABGALAB_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void ImageDataArchonPic(){}
void ImageDataDarkArchonPic(){}
void ImageDataYouwin(){}

void ClientsideProcess()
{
    int imgVector = CreateImageVector(256);

    // if (LoadImageResource(GetScrCodeField(MapImageResourcePool)+4))
    // {
        // int num = 0;
        // int index = 0;

        // AddImageFromResource(imgVector, GetGrpStream(num++), 112660, index++); //MovableCrypt1
        // AddImageFromResource(imgVector, GetGrpStream(num++), 112662, index++); //MovableCrypt2
        // AddImageFromResource(imgVector, GetGrpStream(num++), 15026, index++);
        // AddImageFromResource(imgVector, GetGrpStream(num++), 14997, index++);
        // AddImageFromResource(imgVector, GetGrpStream(num++), 17828, index++);
        // AddImageFromResource(imgVector, GetGrpStream(num++), 132315, index++);
        // AddImageFromResource(imgVector, GetScrCodeField(ImageYoshie)+4, 132316, index++);
        // AddImageFromResource(imgVector, GetScrCodeField(ImageYouwin)+4, 132317, index++);
        // AddImageFromResource(imgVector, GetScrCodeField(ImageObungaPic)+4, 14527, index++);
    // }
    AppendImageFrame(imgVector,GetScrCodeField(ImageDataYouwin)+4,132317);
    AppendImageFrame(imgVector,GetScrCodeField(ImageDataArchonPic)+4,15026);
    AppendImageFrame(imgVector,GetScrCodeField(ImageDataDarkArchonPic)+4,14997);
    initializeSeleneImageFrame(imgVector);
    initializeObungaOutputImageFrame(imgVector);
    initializeYOSHINEImageFrame(imgVector);
    initializeImage_globglogabgalab(imgVector);
    initializeImageHealthBar(imgVector);
    // int drawTextF = DrawTextBuildTextDrawFunction();

    // DrawTextSetupBottmText(2560, drawTextF, 0x7f3, "승리! 고분가를 모두격추했어요");
    ExtractMapBgm("..\\gobunga.mp3", MapBgmData);
    short *pSentryColor=0x7170D8;

    pSentryColor[0]=SENTRY_COLOR_BLUE;
    pSentryColor[1]=SENTRY_COLOR_BLUE;

    ReserveResetFieldGuideData();
    //here
    short *guideName;

    AllocSmartMemEx(32, &guideName);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("고분가"), guideName);
    ChangeGuideName(FIELD_GUIDE_BEHOLDER, guideName);

    short *guideDesc;

    AllocSmartMemEx(256, &guideDesc);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("넥스트봇에 등장하는 오붕가 캐릭터. 무섭게 생긴 얼굴 png 괴물이 끈질기도록 당신을 쫓아온다"), guideDesc);
    ChangeGuideDescription(FIELD_GUIDE_BEHOLDER, guideDesc);
    DoImageDataExchange(imgVector);
}


