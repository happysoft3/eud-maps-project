
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/memutil.h"

#define PASS 1

int CheckBombDistance(int a,int b)
{
    return DistanceUnitToUnit(a,b)<13.0;
}

int IsBombUnit(int unit)
{
    int thingId=GetUnitThingID(unit);

    if (thingId==OBJ_CARNIVOROUS_PLANT)
        return TRUE;

    return FALSE;
}

int IsItemUnit(int unit)
{
    int thingId=GetUnitThingID(unit);

    if (thingId==OBJ_MAIDEN)
        return TRUE;
    return FALSE;
}

void RemoveItemUnit(int unit)
{
    Delete(unit--);
    Delete(unit--);
}

void computeGridSnapCoor(float inputX, float inputY, float *destXY)
{
    int ix=FloatToInt(inputX),iy=FloatToInt(inputY);
    int xn=(ix-57)/46,yn=(iy-11)/46;

    float point[]={
        (IntToFloat(xn)*46.0)+80.0,
        (IntToFloat(yn)*46.0)+34.0,
    };

    while (TRUE)
    {
        if ((xn&1)&&(!(yn&1))) PASS;
        else if ((!(xn&1)) && (yn&1)) PASS;
        else break;

        if (inputY<point[1]) point[1]-=46.0;
        else point[1]+=46.0;
        break;
    }
    destXY[0]= point[0];
    destXY[1]= point[1];
}

int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}
