
#include "libs/define.h"

#define MAX_PLAYER_COUNT 32
#define PLAYER_SAFE_LOCATION 12

int GetPlayer(int pIndex){}//virtual
void SetPlayer(int pIndex, int user){}//virtual
int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

void QueryFloorLevel(int *get, int set){}//virtual

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_HARPOON 32
#define PLAYER_FLAG_THREADLIGHTLY 64

#define MAPEDITOR_LAST_UNIT_EXTENT 1000

int GetMobCount(){}//virtual
void SetMobCount(int count){}//virtual
