
#include "dim_resource.h"
#include "dim_utils.h"
#include "libs/unitstruct.h"
#include "libs/stringutil.h"
#include "libs/shop.h"
#include "libs/itemproperty.h"
#include "libs/mathlab.h"
#include "libs/buff.h"
#include "libs/weapon_effect.h"
#include "libs/objectIDdefines.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/waypoint.h"

#define SPECIAL_WEAPON_PROPERTY 0
#define SPECIAL_WEAPON_FXCODE 1
#define SPECIAL_WEAPON_EXECCODE 2
#define SPECIAL_WEAPON_MAX 3

#define THING_ID_REDPOTION2 637

void DrawRingPotion(int locationId)
{
    int rep=120;

    while (--rep)
        CreateObjectAtEx("RedPotion", LocationX(locationId) + MathSine(rep*3 + 90, 60.0), LocationY(locationId) + MathSine(rep*3, 60.0), NULLPTR);
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

int WillOWispBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1819044183; arr[1] = 1936283471; arr[2] = 112; arr[17] = 180; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 34817; arr[24] = 1065353216; arr[28] = 1117782016; arr[29] = 3; 
		arr[31] = 9; arr[32] = 9; arr[33] = 17; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1326; arr[61] = 46913280; 
	pArr = arr;
	return pArr;
}

void WillOWispSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 180;	hpTable[1] = 180;
	int *uec = ptr[187];
	uec[360] = 34817;		uec[121] = WillOWispBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 60; 
		arr[19] = 70; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1108082688; arr[29] = 3; arr[31] = 3; arr[32] = 6; arr[33] = 12; 
		arr[59] = 5542784; arr[60] = 1388; arr[61] = 46915072; 
	pArr = arr;
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 60;	hpTable[1] = 60;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int BomberBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1651339074; arr[1] = 29285; arr[17] = 105; arr[18] = 1; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1065353216; arr[26] = 4; arr[27] = 1; 
		arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; arr[40] = 116; arr[53] = 1128792064; 
		arr[55] = 9; arr[56] = 17; arr[59] = 5542784; arr[60] = 1348; arr[61] = 46899968; 
	pArr = arr;
	return pArr;
}

void BomberSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 105;	hpTable[1] = 105;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = BomberBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void BomberSetMonsterCollide(int bombUnit)
{
    int *ptr = UnitToPtr(bombUnit);

    ptr[174]=0x4e83b0;
}

int ShopkeeperBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1886349395; arr[1] = 1885693291; arr[2] = 29285; arr[17] = 160; arr[19] = 88; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 6; 
		arr[27] = 1; arr[28] = 1114636288; arr[29] = 10; arr[31] = 4; arr[32] = 9; 
		arr[33] = 17; arr[57] = 5548288; arr[58] = 5546320; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void ShopkeeperSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076426178;		ptr[137] = 1076426178;
	int *hpTable = ptr[139];
	hpTable[0] = 160;	hpTable[1] = 160;
	int *uec = ptr[187];
	uec[360] = 65540;		uec[121] = ShopkeeperBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int FireSpriteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 64; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 20; arr[56] = 28; 
		arr[58] = 5545472; 
	pArr = arr;
	return pArr;
}

void FireSpriteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 64;	hpTable[1] = 64;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = FireSpriteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int BatBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 7627074; arr[17] = 85; arr[19] = 90; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1106247680; 
		arr[29] = 4; arr[31] = 16; arr[59] = 5542784; arr[60] = 1357; arr[61] = 46906368; 
	pArr = arr;
	return pArr;
}

void BatSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = 85;	hpTable[1] = 85;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = BatBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SkeletonLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818585939; arr[1] = 1852798053; arr[2] = 1685221196; arr[17] = 325; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1065353216; arr[26] = 4; arr[27] = 4; 
		arr[28] = 1112014848; arr[29] = 34; arr[30] = 1092616192; arr[32] = 10; arr[33] = 15; 
		arr[58] = 5547856; arr[59] = 5542784; arr[60] = 1315; arr[61] = 46910464; 
	pArr = arr;
	return pArr;
}

void SkeletonLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = SkeletonLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SmallAlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1651261804; arr[2] = 1399811689; arr[3] = 1701079408; arr[4] = 114; 
		arr[17] = 92; arr[18] = 1; arr[19] = 60; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; arr[28] = 1103626240; arr[29] = 6; 
		arr[31] = 8; arr[59] = 5542784; arr[60] = 1356; arr[61] = 46906624; 
	pArr = arr;
	return pArr;
}

void SmallAlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 92;	hpTable[1] = 92;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = SmallAlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int AlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1768057921; arr[1] = 1884516206; arr[2] = 1919247465; arr[17] = 260; arr[19] = 120; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1097859072; arr[29] = 55; arr[30] = 1084227584; arr[31] = 8; arr[32] = 15; 
		arr[33] = 25; arr[34] = 1; arr[35] = 10; arr[36] = 100; arr[59] = 5544896; 
		arr[60] = 1355; arr[61] = 46908928; 
	pArr = arr;
	return pArr;
}

void AlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 260;	hpTable[1] = 260;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = AlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684631635; arr[1] = 29285; arr[17] = 325; arr[19] = 150; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; arr[28] = 1113325568; 
		arr[29] = 100; arr[31] = 8; arr[32] = 15; arr[33] = 25; arr[34] = 100; 
		arr[35] = 100; arr[36] = 300; arr[59] = 5544896; arr[60] = 1353; arr[61] = 46914304; 
	pArr = arr;
	return pArr;
}

void SpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1083179008;		ptr[137] = 1083179008;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = SpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 130; arr[19] = 80; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 30; arr[35] = 5; 
		arr[36] = 100; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 130;	hpTable[1] = 130;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int ShadeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684105299; arr[1] = 101; arr[17] = 285; arr[19] = 220; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; arr[28] = 1106247680; 
		arr[29] = 55; arr[31] = 4; arr[32] = 1; arr[33] = 3; arr[59] = 5542784; 
		arr[60] = 1362; arr[61] = 46905088; 
	pArr = arr;
	return pArr;
}

void ShadeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1087583027;		ptr[137] = 1087583027;
	int *hpTable = ptr[139];
	hpTable[0] = 285;	hpTable[1] = 285;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = ShadeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int MimicBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1768778061; arr[1] = 99; arr[17] = 750; arr[19] = 150; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1106247680; arr[29] = 175; arr[31] = 10; arr[59] = 5542784; arr[60] = 1372; 
		arr[61] = 46901504; 
	pArr = arr;
	return pArr;
}

void MimicSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1083179008;		ptr[137] = 1083179008;
	int *hpTable = ptr[139];
	hpTable[0] = 750;	hpTable[1] = 750;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = MimicBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SwordsmanBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919907667; arr[1] = 1634562916; arr[2] = 110; arr[17] = 130; arr[18] = 100; 
		arr[19] = 155; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 3; arr[28] = 1106247680; arr[29] = 15; arr[57] = 5547984; arr[59] = 5542784; 
		arr[60] = 1346; arr[61] = 46900480; 
	pArr = arr;
	return pArr;
}

void SwordsmanSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1083493581;		ptr[137] = 1083493581;
	int *hpTable = ptr[139];
	hpTable[0] = 130;	hpTable[1] = 130;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = SwordsmanBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int ArcherBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751347777; arr[1] = 29285; arr[17] = 250; arr[19] = 175; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[37] = 1668441421; arr[38] = 1751347777; 
		arr[39] = 1916891749; arr[40] = 7827314; arr[53] = 1128792064; arr[54] = 4; arr[57] = 5548176; 
		arr[60] = 1345; arr[61] = 46904320; 
	pArr = arr;
	return pArr;
}

void ArcherSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1084751872;		ptr[137] = 1084751872;
	int *hpTable = ptr[139];
	hpTable[0] = 250;	hpTable[1] = 250;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = ArcherBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int HorrendousBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 500; arr[19] = 150; 
		arr[20] = 1035489772; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; 
		arr[26] = 9; arr[27] = 5; arr[28] = 1109393408; arr[29] = 134; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1386; arr[61] = 46907648; 
	pArr = arr;
	return pArr;
}

void HorrendousSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1083179008;		ptr[137] = 1083179008;
	int *hpTable = ptr[139];
	hpTable[0] = 500;	hpTable[1] = 500;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = HorrendousBinTable();
	uec[339] = 0;		uec[334] = 1035489772;		uec[336] = 1065353216;
}

int GhostBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936681031; arr[1] = 116; arr[17] = 92; arr[19] = 70; arr[21] = 1065353216; 
		arr[23] = 32769; arr[24] = 1065353216; arr[27] = 1; arr[28] = 1092616192; arr[29] = 10; 
		arr[31] = 4; arr[59] = 5542784; arr[60] = 1325; arr[61] = 46900224; 
	pArr = arr;
	return pArr;
}

void GhostSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 92;	hpTable[1] = 92;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = GhostBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int VileZombieBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701603670; arr[1] = 1651339098; arr[2] = 25961; arr[17] = 360; arr[19] = 130; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; 
		arr[27] = 1; arr[28] = 1113325568; arr[29] = 80; arr[31] = 10; arr[34] = 30; 
		arr[35] = 5; arr[36] = 30; arr[59] = 5543680; arr[60] = 1361; arr[61] = 46895184; 
	pArr = arr;
	return pArr;
}

void VileZombieSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1081711001;		ptr[137] = 1081711001;
	int *hpTable = ptr[139];
	hpTable[0] = 360;	hpTable[1] = 360;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = VileZombieBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int LichLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 530; arr[19] = 86; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1113325568; arr[29] = 100; arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; 
		arr[57] = 5548288; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void LichLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076174520;		ptr[137] = 1076174520;
	int *hpTable = ptr[139];
	hpTable[0] = 530;	hpTable[1] = 530;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int Bear2BinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[1] = 50; arr[17] = 175; arr[19] = 88; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[27] = 1; arr[28] = 1106247680; arr[29] = 20; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30; arr[58] = 5547856; 
		arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void Bear2SubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076426178;		ptr[137] = 1076426178;
	int *hpTable = ptr[139];
	hpTable[0] = 175;	hpTable[1] = 175;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = Bear2BinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int WizardGreenBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 1917281394; arr[2] = 7234917; arr[17] = 307; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1133903872; 
		arr[29] = 34; arr[30] = 1092616192; arr[31] = 11; arr[53] = 1128792064; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1335; arr[61] = 46914816; 
	pArr = arr;
	return pArr;
}

void WizardGreenSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 307;	hpTable[1] = 307;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WizardGreenBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int HecubahBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 650; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 100; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; 
		arr[57] = 5548288; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void HecubahSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 650;	hpTable[1] = 650;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = HecubahBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int GreenFrogBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[17] = 1; arr[18] = 1; 
		arr[19] = 72; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1112014848; arr[29] = 5; arr[32] = 18; arr[33] = 22; arr[59] = 5542784; 
		arr[60] = 1313; arr[61] = 46905856; 
	pArr = arr;
	return pArr;
}

void GreenFrogSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074412912;		ptr[137] = 1074412912;
	int *hpTable = ptr[139];
	hpTable[0] = 95;	hpTable[1] = 95;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GreenFrogBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int WizardBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 25714; arr[17] = 225; arr[19] = 85; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1128792064; arr[31] = 4; 
		arr[53] = 1128792064; arr[54] = 4; arr[60] = 1327; arr[61] = 46910208; 
	pArr = arr;
	return pArr;
}

void WizardSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WizardBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int CarnivorousPlantBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 666; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[27] = 5; 
		arr[28] = 1109393408; arr[29] = 100; arr[31] = 8; arr[59] = 5542784; arr[60] = 1371; 
		arr[61] = 46901760; 
	pArr = arr;
	return pArr;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 666;	hpTable[1] = 666;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = CarnivorousPlantBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

#define THING_ID_WARHAMMER 1175
#define THING_ID_GREATSWORD 1170
#define THING_ID_CHAKRAM 1176
#define THING_ID_ShopkeeperLandOfTheDead 1378

void PlaceWeaponShop(float xpos, float ypos, int *pDest)
{
    int shop = CreateObjectAt(StringUtilFindUnitNameById(THING_ID_ShopkeeperLandOfTheDead), xpos, ypos);

    if (pDest)
        pDest[0] = shop;

    Frozen(shop, TRUE);
    ShopUtilAppendItemWithProperties(shop, THING_ID_WARHAMMER, 10, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_fire4, ITEM_PROPERTY_vampirism3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_WARHAMMER, 10, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_impact4, ITEM_PROPERTY_lightning3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_GREATSWORD, 10, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_confuse3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_GREATSWORD, 10, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_fire4, ITEM_PROPERTY_stun3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_CHAKRAM, 10, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_fire4, ITEM_PROPERTY_vampirism3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_CHAKRAM, 10, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_venom3);

    ShopUtilSetTradePrice(shop, 3.0, 0.0);
}

#define THING_ID_ShopkeeperPurple 1380
#define THING_ID_REDPOTION 636
#define THING_ID_CUREPOISONPOTION 631
#define THING_ID_MUSHROOM 647

void PlacePotionShop(float xpos, float ypos, int *pDest)
{
    int pshop = CreateObjectAt(StringUtilFindUnitNameById(THING_ID_ShopkeeperPurple), xpos, ypos);

    if (pDest)
        pDest[0] = pshop;

    Frozen(pshop, TRUE);
    ShopUtilAppendItem(pshop, THING_ID_REDPOTION, 32);
    ShopUtilAppendItem(pshop, THING_ID_CUREPOISONPOTION, 32);
    ShopUtilAppendItem(pshop, THING_ID_REDPOTION2, 32);
    ShopUtilAppendItem(pshop, THING_ID_MUSHROOM, 32);
    ShopUtilAppendItem(pshop, 646, 32);

    int magicPotionId = 2679;

    while (magicPotionId <= 2687)
        ShopUtilAppendItem(pshop, magicPotionId++, 32);

    ShopUtilSetTradePrice(pshop, 3.0, 0.0);
}

#define THING_ID_ORNATEHELM 1157
#define THING_ID_BREASTPLATE 1152
#define THING_ID_PLATE_ARMS 1144
#define THING_ID_PLATE_LEGGINGS 1143
#define THING_ID_PLATE_BOOTS 1142
#define THING_ID_CLOAK 1137
#define THING_ID_MEDIEVAL_SHIRT 1135
#define THING_ID_MEDIEVAL_PANTS 1136

#define THING_ID_ShopkeeperYellow 1381

void PlaceArmorShop(float xpos, float ypos, int *pDest)
{
    int shop = CreateObjectAt(StringUtilFindUnitNameById(THING_ID_ShopkeeperYellow), xpos, ypos);

    if (pDest)
        pDest[0] = shop;

    Frozen(shop, TRUE);
    ShopUtilAppendItemWithProperties(shop, THING_ID_ORNATEHELM, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_BREASTPLATE, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_PLATE_ARMS, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_PLATE_LEGGINGS, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_PLATE_BOOTS, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_CLOAK, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_MEDIEVAL_SHIRT, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(shop, THING_ID_MEDIEVAL_PANTS, 8, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);

    ShopUtilSetTradePrice(shop, 2.2, 0.0);
}

int CheckValidationXYCoor(float coor)
{
    return (coor >= 100.0 && coor <= 5500.0);
}

#define XYLOW 0
#define XYHIGH 1
int RhombusGetXYCoor(float *xLowHigh, float *yLowHigh, float *destXY)
{
    float xf = RandomFloat(yLowHigh[XYLOW], yLowHigh[XYHIGH]), yf = RandomFloat(0.0, xLowHigh[XYHIGH] - xLowHigh[XYLOW]);

    destXY[0] = xLowHigh[XYHIGH]-yLowHigh[XYHIGH]+xf - yf;
    destXY[1] = xf+yf;
    if (!CheckValidationXYCoor(destXY[0]))
        return FALSE;
    if (!CheckValidationXYCoor(destXY[1]))
        return FALSE;

    return TRUE;
}

int AirshipCaptainBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 2000; 
		arr[19] = 150; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1114636288; arr[29] = 1; arr[32] = 15; arr[33] = 21; arr[37] = 1818324307; 
		arr[38] = 1701725548; arr[39] = 1115252594; arr[40] = 7629935; arr[53] = 1133903872; arr[55] = 15; 
		arr[56] = 21; arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328; 
	pArr = arr;
	return pArr;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1083179008;		ptr[137] = 1083179008;
	int *hpTable = ptr[139];
	hpTable[0] = 2000;	hpTable[1] = 2000;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = AirshipCaptainBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

#define THING_ID_ARCHER_ARROW 527
#define THING_ID_OgreShuriken 1180
void FlyingArrowFx(float xpos, float ypos, int dir)
{
    int fxarrw = CreateObjectById(OBJ_OGRE_SHURIKEN , xpos, ypos);

    // LookWithAngle(fxarrw, dir);
    Frozen(fxarrw, TRUE);
    DeleteObjectTimer(fxarrw, 3);
}

#define ARRW_INFO_UNIT 0
#define ARRW_INFO_COUNT 1
#define ARRW_INFO_XVECTOR 2
#define ARRW_INFO_YVECTOR 3
#define ARRW_INFO_DIR 4
#define ARRW_INFO_MEMUNIT 5
#define ARRW_INFO_ALLOC_SIZE 6

void FlyingArrowProgress(int *arrowInfo)
{
    int unit = arrowInfo[ARRW_INFO_UNIT];

    while (TRUE)
    {
        int owner = GetOwner(unit);

        if (MaxHealth(unit))
        {
            if ( IsVisibleTo(owner, unit) && IsVisibleTo(unit, owner))
            {
                if (arrowInfo[ARRW_INFO_COUNT]--)
                {
                    FrameTimerWithArg(1, arrowInfo, FlyingArrowProgress);
                    MoveObjectVector(unit, ToFloat(arrowInfo[ARRW_INFO_XVECTOR]), ToFloat(arrowInfo[ARRW_INFO_YVECTOR]));
                    FlyingArrowFx(GetObjectX(unit), GetObjectY(unit), arrowInfo[ARRW_INFO_DIR]);
                    break;
                }
            }
            Delete(unit);
        }
        FreeSmartMemEx(arrowInfo);
        break;
    }
}

void ArrowMakeInfo(int *pDestPtr, int unit, int dir, int count, float xVect, float yVect)
{
    int memunit, *alloc;

    AllocSmartMemEx(4*ARRW_INFO_ALLOC_SIZE, &alloc);
    alloc[ARRW_INFO_UNIT] = unit;
    alloc[ARRW_INFO_COUNT] = count;
    alloc[ARRW_INFO_XVECTOR] = ToInt(xVect);
    alloc[ARRW_INFO_YVECTOR] = ToInt(yVect);
    alloc[ARRW_INFO_MEMUNIT] = memunit;
    alloc[ARRW_INFO_DIR] = dir;

    pDestPtr[0] = alloc;
}

void ArrowObjectCollide()
{
    if (!GetTrigger())
        return;

    if (!CurrentHealth(OTHER))
        return;

    if (IsAttackedBy(OTHER, GetOwner(SELF)))
    {
        Damage(OTHER, GetOwner(SELF), 200, DAMAGE_TYPE_IMPACT);
        Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
    }
}

#define THING_ID_BOMBER 1348
#define THING_ID_ImaginaryCaster 1399
void ArrowSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 9.0), yvect = UnitAngleSin(OTHER, 9.0);
    int posUnit = CreateObjectAt(StringUtilFindUnitNameById(THING_ID_ImaginaryCaster), GetObjectX(OTHER) + xvect -(yvect*2.0), GetObjectY(OTHER) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(OTHER);

    while (++rep<5)
    {
        single= CreateObjectById(OBJ_ARCHER_ARROW, GetObjectX(posUnit), GetObjectY(posUnit));
        SetOwner(OTHER, single);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        LookWithAngle(single, dir);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
}


void PlacingArrowSword(float xpos, float ypos, int *pDest)
{
	int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(ArrowSwordTriggered, &property[SPECIAL_WEAPON_EXECCODE]);
    // SpecialWeaponPropertySetFXCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_SWORD,xpos,ypos);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_stun4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    // CreateUserWeaponFX(sd);
    if (pDest)
        pDest[0] = sd;
}

void EnergyParCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER))
        {
            if (IsAttackedBy(GetOwner(SELF), OTHER))
            {
                Damage(OTHER, SELF, 200, DAMAGE_TYPE_PLASMA);
                Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.3);
            }
        }
        break;
    }
}

void EnergyParProcess(int sub)
{
    while (IsObjectOn(sub))
    {
        if (IsVisibleTo(sub, sub+1))
        {
            int owner = GetOwner(sub), count = GetDirection(sub);

            if (CurrentHealth(owner) && count)
            {
                FrameTimerWithArg(1, sub, EnergyParProcess);
                LookWithAngle(sub, --count);

                int damHelper = DummyUnitCreateAt("Demon", GetObjectX(sub), GetObjectY(sub));

                SetUnitFlags(damHelper, GetUnitFlags(damHelper) ^ UNIT_FLAG_NO_PUSH_CHARACTERS);
                SetOwner(owner, damHelper);
                DeleteObjectTimer(damHelper, 1);
                SetCallback(damHelper, 9, EnergyParCollide);
                MoveObjectVector(sub, GetObjectZ(sub), GetObjectZ(sub+1));
                MoveObjectVector(sub+1, GetObjectZ(sub), GetObjectZ(sub+1));
                Effect("SENTRY_RAY", GetObjectX(sub), GetObjectY(sub), GetObjectX(sub+1), GetObjectY(sub+1));
                break;
            }
        }
        Delete(sub++);
        Delete(sub++);
        break;
    }
}

void EnergyParSword()
{
    float xVect = UnitAngleCos(OTHER, 19.0), yVect = UnitAngleSin(OTHER, 19.0);
    int sub = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER) + xVect, GetObjectY(OTHER) + yVect);

    CreateObjectAtEx("InvisibleLightBlueLow", GetObjectX(sub) - xVect, GetObjectY(sub) - yVect, NULLPTR);
    FrameTimerWithArg(1, sub, EnergyParProcess);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, 32);
    Raise(sub, xVect);
    Raise(sub+1, yVect);
}

void PlacingEnergyParSword(float xpos, float ypos, int *pDest)
{
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(EnergyParSword, &property[SPECIAL_WEAPON_EXECCODE]);
    // SpecialWeaponPropertySetFXCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_GREAT_SWORD,xpos,ypos);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);

    if (pDest)
        pDest[0] = sd;
}

void ManaBombCancelFx(int sUnit)
{
    int caster = CreateObjectAt("ImaginaryCaster", GetObjectX(sUnit), GetObjectY(sUnit));

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}

void EarthQuakeTriggered()
{
    SplashDamageAt(OTHER, 350, GetObjectX(OTHER) + UnitAngleCos(OTHER, 11.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 11.0), 200.0);

    ManaBombCancelFx(OTHER);
    Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    PlaySoundAround(OTHER, SOUND_ElectricalArc1);
}

void PlacingEarthQuakeHammer(float xpos, float ypos, int *pDest)
{
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(EarthQuakeTriggered, &property[SPECIAL_WEAPON_EXECCODE]);
    // SpecialWeaponPropertySetFXCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_WAR_HAMMER,xpos,ypos);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_confuse1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);

    if (pDest)
        pDest[0] = sd;
}

void ExecWhenPlayerEnteredMap(int pInfo)
{
	int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
	{
        pUnit = GetMemory(ptr+0x2c);
        ShowQuestIntroOne(pUnit, 237, "NoxWorldMap", "ccs:HUNT");
	}
    if (pInfo==0x653a7c)
    {
        InitializeCommonImage();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}


