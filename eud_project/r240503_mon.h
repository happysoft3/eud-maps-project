
#include "r240503_gvar.h"
#include "r240503_utils.h"
#include "r240503_reward.h"
#include "libs/bind.h"
#include "libs/printutil.h"
#include "libs/voiceList.h"

#define GREENGIANT_VECT_X 0
#define GREENGIANT_VECT_Y 1
#define GREENGIANT_OWNER 2
#define GREENGIANT_COUNT 3
#define GREENGIANT_SUB 4
#define GREENGIANT_DAMAGE_HASH 5
#define GREENGIANT_MAX 6

void flyingAcid(int *d){
    int sub=d[GREENGIANT_SUB];

    if (ToInt(GetObjectX(sub))){
        if (--d[GREENGIANT_COUNT]>=0){
            int owner=d[GREENGIANT_OWNER];
            float *pVect=&d[GREENGIANT_VECT_X];
            if (CurrentHealth(owner)&&IsVisibleTo(sub,owner)){
                PushTimerQueue(1,d,flyingAcid);
                MoveObjectVector(sub,pVect[0],pVect[1]);
                CreateObjectById(OBJ_LARGE_BARREL_BREAKING_1,GetObjectX(sub),GetObjectY(sub));
                return;
            }
        }
        HashGet(GenericHash(),sub,NULLPTR,TRUE);
        Delete(sub);
    }
    HashDeleteInstance(d[GREENGIANT_DAMAGE_HASH]);
    FreeSmartMemEx(d);
}

void onGreenGiantMissileCollide(){
    if (!GetTrigger())return;
    if (CurrentHealth(OTHER)){
        int *d;
        if( HashGet(GenericHash(),GetTrigger(),&d,FALSE))
        {
            int owner=d[GREENGIANT_OWNER];
            if (IsAttackedBy(OTHER,owner)){
                if (HashGet(d[GREENGIANT_DAMAGE_HASH],GetCaller(),NULLPTR,FALSE))
                    return;

                Damage(OTHER,owner,100,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
                HashPushback(d[GREENGIANT_DAMAGE_HASH],GetCaller(),TRUE);
            }
        }
    }
}

void startGreenGiantShot(int caster, float *pVect){
    int *d;
    AllocSmartMemEx(GREENGIANT_MAX*4,&d);
    PushTimerQueue(1,d,flyingAcid);
    d[GREENGIANT_OWNER]=GetTrigger();
    d[GREENGIANT_COUNT]=32;
    int unit =DummyUnitCreateById(OBJ_AIRSHIP_CAPTAIN,GetObjectX(caster)+pVect[0],GetObjectY(caster)+pVect[1]);
    SetCallback(unit,9,onGreenGiantMissileCollide);
    HashPushback(GenericHash(),unit,d);
    d[GREENGIANT_SUB]=unit;
    d[GREENGIANT_VECT_X]=pVect[0];
    d[GREENGIANT_VECT_Y]=pVect[1];
    HashCreateInstance(&d[GREENGIANT_DAMAGE_HASH]);
}

void onGreenGiantMeleeAttack(){
    float vect[2];
    if (GetCaller())
    {
        vect[0]=UnitRatioX(OTHER,SELF,13.0);
        vect[1]=UnitRatioY(OTHER,SELF,13.0);
    }else{
        vect[0]=UnitAngleCos(SELF,13.0);
        vect[1]=UnitAngleSin(SELF,13.0);
    }
    startGreenGiantShot(GetTrigger(),vect);
}

int BlackBearBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1667329090; arr[1] = 1634026091; arr[2] = 114; arr[17] = 500; arr[19] = 92; 
		arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1069547520; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1114636288; arr[29] = 88; arr[30] = 1128792064; arr[31] = 11; 
		arr[59] = 5542784; arr[60] = 1366; arr[61] = 46902784; 
	pArr = arr;
    CustomMeleeAttackCode(arr, onGreenGiantMeleeAttack);
	return pArr;
}

void BlackBearSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076929495;		ptr[137] = 1076929495;
	int *hpTable = ptr[139];
	hpTable[0] = 3250;	hpTable[1] = 3250;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = BlackBearBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpawnFinalBoss(float x, float y){
	int boss=CreateObjectById(OBJ_BLACK_BEAR,x,y);
	BlackBearSubProcess(boss);
	AttachHealthbar(boss);
	return boss;
}

void monSpawnPoints(int *sel)
{
    char *pp;
    int max;

    if (pp!=0)
    {
        if (sel)
            sel[0] = pp[Random(0,max-1)];
        return;
    }
    char points[]={22,23,24,55,56,60,67,66,76,94,57,61,25,30,85,43,41,83,};
    max=sizeof(points);
    pp=points;
}

#define MONSTER_MELEE_DAMAGE 29
#define LV1_HP_AMOUNT 100
#define LV2_HP_AMOUNT 225
#define LV3_HP_AMOUNT 430
#define LV4_HP_AMOUNT 500
#define LV5_HP_AMOUNT 550
#define LV6_HP_AMOUNT 610

int SmallSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1768969068; arr[2] = 7497060; arr[17] = 60; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; 
		arr[28] = 1097859072; arr[MONSTER_MELEE_DAMAGE] = 10; arr[31] = 8; arr[59] = 5542784; arr[60] = 1354; 
		arr[61] = 46906880; 
	pArr = arr;
	return pArr;
}

void SmallSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = LV1_HP_AMOUNT;	hpTable[1] = LV1_HP_AMOUNT;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SmallSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpawnSmallSpider(float x, float y)
{
    int mon=CreateObjectById(OBJ_SMALL_SPIDER,x,y);

    SmallSpiderSubProcess(mon);
    return mon;
}

int SmallAlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1651261804; arr[2] = 1399811689; arr[3] = 1701079408; arr[4] = 114; 
		arr[17] = 85; arr[19] = 90; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; 
		arr[26] = 4; arr[27] = 5; arr[28] = 1107820544; arr[MONSTER_MELEE_DAMAGE] = 18; arr[31] = 8; 
		arr[32] = 6; arr[33] = 12; arr[59] = 5542784; arr[60] = 1356; arr[61] = 46906624; 
	pArr = arr;
	return pArr;
}

void SmallAlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = LV2_HP_AMOUNT;	hpTable[1] = LV2_HP_AMOUNT;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SmallAlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int SpawnSmallWhiteSpider(float x,float y)
{
    int mon=CreateObjectById(OBJ_SMALL_ALBINO_SPIDER,x,y);
    SmallAlbinoSpiderSubProcess(mon);
    return mon;
}

int AlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1768057921; arr[1] = 1884516206; arr[2] = 1919247465; arr[17] = 100; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1106247680; arr[MONSTER_MELEE_DAMAGE] = 36; arr[31] = 10; arr[32] = 12; arr[33] = 20; 
		arr[34] = 20; arr[35] = 1; arr[36] = 3; arr[59] = 5544896; arr[60] = 1355; 
		arr[61] = 46908928; 
	pArr = arr;
	return pArr;
}

void AlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = LV3_HP_AMOUNT;	hpTable[1] = LV3_HP_AMOUNT;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = AlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int SpawnAlbinoSpider(float x,float y){
    int s=CreateObjectById(OBJ_ALBINO_SPIDER,x,y);
    AlbinoSpiderSubProcess(s); 
    return s;
}

int SpittingSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1953067091; arr[1] = 1735289204; arr[2] = 1684631635; arr[3] = 29285; arr[17] = 135; 
		arr[19] = 85; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 4; arr[28] = 1106247680; arr[MONSTER_MELEE_DAMAGE] = 44; arr[31] = 10; arr[32] = 13; 
		arr[33] = 21; arr[34] = 20; arr[35] = 1; arr[36] = 3; arr[59] = 5545120; 
		arr[60] = 1352; arr[61] = 46908672; 
	pArr = arr;
	return pArr;
}

void SpittingSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = LV4_HP_AMOUNT;	hpTable[1] = LV4_HP_AMOUNT;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SpittingSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int SpawnSpittingSpider(float x,float y){
    int s=CreateObjectById(OBJ_SPITTING_SPIDER,x,y);
    SpittingSpiderSubProcess(s);
    SetMonsterPoisonImmune(s);
    return s;
}
int SpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684631635; arr[1] = 29285; arr[17] = 160; arr[19] = 90; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; arr[28] = 1110704128; 
		arr[MONSTER_MELEE_DAMAGE] = 60; arr[31] = 8; arr[32] = 11; arr[33] = 19; arr[34] = 50; 
		arr[35] = 2; arr[36] = 3; arr[59] = 5544896; arr[60] = 1353; arr[61] = 46914304; 
	pArr = arr;
	return pArr;
}

void SpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = LV5_HP_AMOUNT;	hpTable[1] = LV5_HP_AMOUNT;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int SpawnSpiderBlack(float x,float y){
    int s=CreateObjectById(OBJ_BLACK_WIDOW,x,y);
    SpiderSubProcess(s);
    SetMonsterPoisonImmune(s);
    return s;
}

int WolfBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1718382423; arr[16] = 18000; arr[17] = 100; arr[19] = 90; arr[21] = 1056964608; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1106247680; arr[MONSTER_MELEE_DAMAGE] = 77; arr[31] = 8; arr[32] = 8; arr[33] = 17; 
		arr[59] = 5542784; arr[60] = 1369; arr[61] = 46902528; 
	pArr = arr;
	return pArr;
}

void WolfSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076677837;		ptr[137] = 1076677837;
	int *hpTable = ptr[139];
	hpTable[0] = LV6_HP_AMOUNT;	hpTable[1] = LV6_HP_AMOUNT;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WolfBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1056964608;
}

int SpawnNormalWolf(float x,float y)
{
    int wolf=CreateObjectById(OBJ_WOLF,x,y);
    WolfSubProcess(wolf);
    return wolf;
}

int getNextLevelAmount(int lv){
    int arr[]={225,180,125,98,87,81,81,81,81,81,81,999999,999999,};

    return arr[lv];
}

void queryEndSpawnFn(int *get,int set){
	int fn;
	if (get){
		get[0]=fn;
		return;
	}
	fn=set;
}

void endSpawnMonsterPermanently(int *d){
    if (d!=0)
        FreeSmartMemEx(d);
	int onEndSpawn;
	queryEndSpawnFn(&onEndSpawn,0);

	if (onEndSpawn)
		Bind(onEndSpawn,NULLPTR);
    UniPrintToAll("-endspawnloop-");
}

int SpawnEnd(float x, float y)
{
    int endMarker=CreateObjectById(OBJ_FEAR,x,y);

    UnitNoCollide(endMarker);
    SetSpawnPermanentlyFunction(endSpawnMonsterPermanently);
    return 0;
}

void deferredJunkYardDog(int mon)
{
    Wander(mon);
}

void onMonsterDeath()
{
    float x=GetObjectX(SELF),y=GetObjectY(SELF);

    DeleteObjectTimer(SELF,3);
    if (Random(0,3))
    {
        int pos=CreateObjectById(OBJ_MOVER,x,y);
        CreateRandomItemCommon(pos);
    }
    int fx=CreateObjectById(OBJ_EXPLOSION,x,y);
    DeleteObjectTimer(fx,12);
    PlaySoundAround(fx, SOUND_BurnCast);
    SetMonsterCount(GetMonsterCount()-1);
}

void MonsterCommonHitEvent()
{
    if (GetCaller())
        return;

    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 2, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectById(OBJ_GREEN_PUFF, GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void monCommonSettings(int mon)
{
    RetreatLevel(mon,0.0);
    AggressionLevel(mon,1.0);
    PushTimerQueue(1,mon,deferredJunkYardDog);
    SetCallback(mon,5,onMonsterDeath);
    SetCallback(mon,7,MonsterCommonHitEvent);
}

#define SPAWN_MOB_NEXT_AMOUNT 0
#define SPAWN_MOB_CUR_LEVEL 1
#define SPAWN_MOB_SPAWN_MOB_FNTABLE 2
#define SPAWN_MOB_MAX 3

void spawnMonster(int fn)
{
    int sel;

    monSpawnPoints(&sel);
    int xy[]={LocationX(sel),LocationY(sel)};
    int mon = Bind(fn, xy);

    if (!mon)
        return;

    monCommonSettings(mon);
    SetMonsterCount(GetMonsterCount()+1);
    SetMonsterTotalCount(GetMonsterTotalCount()+1);
}

void spawnMonsterPermanently(int *d){
    int fn=GetSpawnPermanentlyFunction();

    if (!fn) return;
    int *pNext=&d[SPAWN_MOB_NEXT_AMOUNT];
    
    if (GetMonsterTotalCount()>=pNext[0])
        pNext[0]+=getNextLevelAmount(++d[SPAWN_MOB_CUR_LEVEL]);
    int *pSpawnfn=d[SPAWN_MOB_SPAWN_MOB_FNTABLE];
    spawnMonster(pSpawnfn[d[SPAWN_MOB_CUR_LEVEL]]);
    PushTimerQueue(Random(20,70),d,fn);
}

void deferredExecInitialSpawn(int *d)
{
    int *pSpawnfn=d[SPAWN_MOB_SPAWN_MOB_FNTABLE],curLv=d[SPAWN_MOB_CUR_LEVEL];
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
    spawnMonster(pSpawnfn[curLv]);
}

void InitMonsters(int endSpawnFn)
{
    monSpawnPoints(0);
    SetSpawnPermanentlyFunction(spawnMonsterPermanently);
    int *d;
    AllocSmartMemEx(SPAWN_MOB_MAX*4,&d);
    d[SPAWN_MOB_CUR_LEVEL]=0;
    d[SPAWN_MOB_NEXT_AMOUNT]=getNextLevelAmount(d[SPAWN_MOB_CUR_LEVEL]);
    int spawnFn[]={SpawnSmallSpider,SpawnSmallWhiteSpider,SpawnAlbinoSpider,SpawnSpittingSpider,SpawnSpiderBlack,SpawnNormalWolf,SpawnEnd,};
    d[SPAWN_MOB_SPAWN_MOB_FNTABLE]=spawnFn;
    PushTimerQueue(150,d,deferredExecInitialSpawn);
    PushTimerQueue(300,d,spawnMonsterPermanently);
	queryEndSpawnFn(NULLPTR, endSpawnFn);
}

void haltSpawnMonsterLoop(int *d){
    if (d!=0)
        FreeSmartMemEx(d);
    UniPrintToAll("-loopHalted-");
}
void StopSpawnMonsterLoop(){
    SetSpawnPermanentlyFunction(haltSpawnMonsterLoop);
}

void onFlySwatterAttack(){
    if (!GetCaller())
        return;

    Effect("GREATER_HEAL",GetObjectX(SELF),GetObjectY(SELF),GetObjectX(OTHER),GetObjectY(OTHER));
    RestoreHealth(SELF, 25);
    Damage(OTHER,SELF, 85,DAMAGE_TYPE_PLASMA);
}

int WoundedApprenticeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 900; arr[19] = 80; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; 
		arr[26] = 4; arr[28] = 1112014848; arr[29] = 100; arr[30] = 1112014848; arr[31] = 4; 
		arr[32] = 12; arr[33] = 20; arr[57] = 5546320; arr[59] = 5542784; 
	pArr = arr;
    CustomMeleeAttackCode(arr, onFlySwatterAttack);
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 1300;	hpTable[1] = 1300;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedApprenticeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onBearGryllsAttack(){
    if (GetCaller()){
    Damage(OTHER,SELF,118,DAMAGE_TYPE_CLAW);
    float xy[]={GetObjectX(OTHER),GetObjectY(OTHER)};
    Effect("SMOKE_BLAST",xy[0],xy[1],0.0,0.0);
    CreateObjectById(OBJ_COFFIN_BREAKING_1,xy[0],xy[1]);
    }
}

int WoundedConjurerBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1130653028; arr[2] = 1969909359; arr[3] = 7497074; arr[17] = 1500; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1118437376; arr[31] = 2; arr[32] = 9; arr[33] = 9; arr[59] = 5542784; 
		arr[60] = 2271; arr[61] = 46912768; 
    CustomMeleeAttackCode(arr, onBearGryllsAttack);
	pArr = arr;
	return pArr;
}

void WoundedConjurerSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 3500;	hpTable[1] = 3500;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedConjurerBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void onMedSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
            Damage(OTHER, SELF, 135, DAMAGE_TYPE_PLASMA);
    }
}

void onMedAttack(){
    if (!GetCaller())
        return;
    float x=GetObjectX(OTHER),y=GetObjectY(OTHER);
    DeleteObjectTimer( CreateObjectById(OBJ_METEOR_EXPLODE, x,y), 12);
    SplashDamageAtEx(SELF,x,y,85.0, onMedSplash);
}

int WoundedWarriorBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1466197348; arr[2] = 1769108065; arr[3] = 29295; arr[17] = 1500; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1118437376; arr[29] = 30; arr[32] = 15; arr[33] = 15; arr[57] = 5545344; 
		arr[59] = 5542784; arr[60] = 2272; arr[61] = 46912512; 
    CustomMeleeAttackCode(arr, onMedAttack);
	pArr = arr;
	return pArr;
}

void WoundedWarriorSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 3900;	hpTable[1] = 3900;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedWarriorBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int EvilCherubBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818850885; arr[1] = 1919248451; arr[2] = 25205; arr[17] = 1080; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[37] = 1751347777; 
		arr[38] = 1866625637; arr[39] = 29804; arr[53] = 1132068864; arr[54] = 4; arr[55] = 12; 
		arr[56] = 18; arr[60] = 1317; arr[61] = 46913536; 
	pArr = arr;
	return pArr;
}

void EvilCherubSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 1680;	hpTable[1] = 1680;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = EvilCherubBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


void removeBeholderTrap(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        Delete(sub);
        Delete(sub+1);
    }
}

void startDrainHighVoltage()
{
    float vect[2];
    if (GetCaller())
    {
        vect[0]=UnitRatioX(OTHER,SELF,13.0);
        vect[1]=UnitRatioY(OTHER,SELF,13.0);
    }else{
        vect[0]=UnitAngleCos(SELF,13.0);
        vect[1]=UnitAngleSin(SELF,13.0);
    }
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);
    int t= CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x+vect[0],y+vect[1]);

    PushTimerQueue(48, sub, removeBeholderTrap);
    SetOwner(SELF,sub);
    Damage(OTHER,SELF,10,DAMAGE_TYPE_AIRBORNE_ELECTRIC);
    CastSpellObjectObject("SPELL_CHAIN_LIGHTNING",sub,t);
}

int HecubahBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 2500; arr[19] = 120; arr[21] = 1065353216; 
		arr[23] = 67584; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1106247680; arr[29] = 8; arr[53] = 1128792064; arr[54] = 4; arr[59] = 5542784; 
		arr[60] = 1383; arr[61] = 46908160; 
    CustomMeleeAttackCode(arr, startDrainHighVoltage);
	pArr = arr;
	return pArr;
}

void HecubahSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 3100;	hpTable[1] = 3100;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = HecubahBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


void onBodyGuardLoopInterval(int *d){
    int cre=d[0];

    if (CurrentHealth(cre))
    {
        int owner=GetOwner(cre);
        if (MaxHealth(owner)){
            PushTimerQueue(13,d,onBodyGuardLoopInterval);
            if (!IsVisibleOr(cre,owner))
            {
                MoveObject(cre,GetObjectX(owner)+UnitAngleCos(owner, 13.0),GetObjectY(owner)+UnitAngleSin(owner,13.0));
                AggressionLevel(cre,1.0);
                CreatureFollow(cre,owner);
            }
            return;
        }
        Delete(cre);
    }
    d[0]=0;
}

int CreateFlySwatter(float x, float y){
    int s=CreateObjectById(OBJ_WOUNDED_APPRENTICE,x,y);
    WoundedApprenticeSubProcess(s);
    return s;
}

int CreateBearGrylls(float x,float y){
    int s=CreateObjectById(OBJ_WOUNDED_CONJURER,x,y);
    WoundedConjurerSubProcess(s);
    return s;
}

int CreatePesticide(float x,float y){
    int s=CreateObjectById(OBJ_WOUNDED_WARRIOR,x,y);
    WoundedWarriorSubProcess(s);
    return s;
}

int CreateStelsShip(float x,float y){
    int s=CreateObjectById(OBJ_EVIL_CHERUB,x,y);
    EvilCherubSubProcess(s);
    return s;
}
int CreateBlueGiant(float x,float y){
    int s=CreateObjectById(OBJ_HECUBAH,x,y);
    HecubahSubProcess(s);
    SetUnitVoice(s, MONSTER_VOICE_Demon);
    return s;
}
void onBodyGuardDeath(){
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    Delete(SELF);
    DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION,x,y), 12);
}
void onBodyGuardHurt(){
    Enchant(SELF,EnchantList(ENCHANT_INVULNERABLE),3.0);
    RestoreHealth(SELF, 40);
}

void SummonBodyGuard(int owner, int *credata, int spawnFn){
    float xy[]={GetObjectX(owner),GetObjectY(owner)};
    int *arg=xy;
    int s= Bind(spawnFn,arg);

    credata[0]=s;
    SetOwner(owner,s);
    AttachHealthbar(s);
    SetUnitFlags(s,GetUnitFlags(s)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(s,5,onBodyGuardDeath);
    SetCallback(s,7,onBodyGuardHurt);
    PushTimerQueue(1,credata,onBodyGuardLoopInterval);
}
