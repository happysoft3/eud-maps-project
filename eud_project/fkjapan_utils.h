
#include "fkjapan_gvar.h"
#include "libs/bind.h"
#include "libs/absolutelypickup.h"
#include "libs/queueTimer.h"
#include "libs/spellutil.h"
#include "libs/wallutil.h"
#include "libs/objectIDdefines.h"
#include "libs/buff.h"

int LastParentUnit(int unit)
{
    int pic = GetOwner(unit);

    while (pic)
    {
        unit = pic;
        pic = GetOwner(pic);
    }
    return unit;
}

void DelayGiveItemToOwner(int sItem)
{
    int owner = GetOwner(sItem);

    if (CurrentHealth(owner))
        Pickup(owner, sItem);
    else
        ClearOwner(sItem);
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 240; arr[19] = 88; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 11; arr[56] = 17; 
		arr[58] = 5546320; 
        link=arr;
	}
	return link;
}

void FireSpriteSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076426178);
		SetMemory(ptr + 0x224, 1076426178);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 240);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 240);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int Bear2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50; arr[2] = 0; arr[3] = 0; arr[4] = 0; 
		arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
		arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
		arr[15] = 0; arr[16] = 20000; arr[17] = 90; arr[18] = 100; arr[19] = 40; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 65545; arr[24] = 1067450368; 
		arr[25] = 0; arr[26] = 0; arr[27] = 1; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30;
		arr[55] = 0; arr[56] = 0; arr[57] = 0; arr[58] = 5547856; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr1 = GetMemory(0x750710), k, num;

    SetMemory(ptr1 + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    Delete(unit + 1);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));

    return unit;
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[16] = 80000; arr[17] = 300; 
		arr[18] = 100; arr[19] = 50; arr[21] = 1065353216; arr[24] = 1067869798; arr[26] = 4; 
		arr[27] = 4; arr[53] = 1128792064; arr[54] = 4;
		link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[2] = 0; arr[3] = 0; arr[4] = 0; 
		arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
		arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
		arr[15] = 0; arr[16] = 0; arr[17] = 30; arr[18] = 92; arr[19] = 80; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1065688760; 
		arr[25] = 0; arr[26] = 0; arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[55] = 0; arr[56] = 0; arr[57] = 0; arr[58] = 5546320; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1852796743; arr[1] = 0; arr[2] = 0; arr[3] = 0; arr[4] = 0; 
		arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
		arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
		arr[15] = 0; arr[16] = 0; arr[17] = 85; arr[18] = 0; arr[19] = 15; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1066192077; 
		arr[25] = 0; arr[26] = 4; arr[27] = 0; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20;
		arr[55] = 0; arr[56] = 0; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680;
        link = arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[19] = 1; arr[24] = 1065772646; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984; 
		link = arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[4] = 0; 
		arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
		arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
		arr[15] = 0; arr[16] = 0; arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1068708659; 
		arr[25] = 0; arr[26] = 4; arr[27] = 0; arr[28] = 1082130432; arr[29] = 20; 
		arr[30] = 0; arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[55] = 0; arr[56] = 0; arr[57] = 5548112; arr[58] = 0; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[3] = 0; arr[4] = 0; 
		arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
		arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
		arr[15] = 0; arr[16] = 0; arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 4; arr[24] = 1069547520; 
		arr[25] = 0; arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[30] = 0; arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[40] = 0; arr[41] = 0; arr[42] = 0; arr[43] = 0; arr[44] = 0; 
		arr[45] = 0; arr[46] = 0; arr[47] = 0; arr[48] = 0; arr[49] = 0; 
		arr[50] = 0; arr[51] = 0; arr[52] = 0; arr[53] = 1128792064; arr[54] = 0; 
		arr[55] = 20; arr[56] = 28; arr[57] = 0; arr[58] = 0; arr[59] = 5544896; 
		arr[60] = 0; arr[61] = 45071360;
        link = arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 400; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 2049; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1101004800; arr[29] = 40; arr[32] = 22; arr[33] = 30; arr[34] = 2; 
		arr[35] = 2; arr[36] = 20; arr[58] = 5545888; arr[59] = 5543680; arr[60] = 1387; 
		arr[61] = 46915328; 
		link = arr;
	}
	return link;
}

int WoundedApprenticeBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
		arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
		arr[15] = 0; arr[16] = 0; arr[17] = 50; arr[18] = 10; arr[19] = 100; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 0; arr[24] = 1065353216; 
		arr[25] = 0; arr[26] = 0; arr[27] = 0; arr[28] = 1112014848; arr[29] = 40; 
		arr[30] = 1106247680; arr[31] = 2; arr[32] = 12; arr[33] = 20; arr[34] = 0; 
		arr[35] = 0; arr[36] = 0; arr[37] = 0; arr[38] = 0; arr[39] = 0; 
		arr[40] = 0; arr[41] = 0; arr[42] = 0; arr[43] = 0; arr[44] = 0; 
		arr[45] = 0; arr[46] = 0; arr[47] = 0; arr[48] = 0; arr[49] = 0; 
		arr[50] = 0; arr[51] = 0; arr[52] = 0; arr[53] = 0; arr[54] = 0; 
		arr[55] = 0; arr[56] = 0; arr[57] = 5548112; arr[58] = 0; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int MonsterProcessFunction()
{
    StopScript(DefaultMonsterProcess);
}

void DefaultMonsterProcess(int unit)
{
    return;
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

void MonsterWoundedApprentice(int unit)
{
    //TODO: Index. 9, ThingName= WoundedApprentice
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedApprenticeBinTable());
        SetUnitMaxHealth(unit, 230);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}


void CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        arr[5] = MonsterGoonProcess; arr[72] = MonsterStrongWhiteWizProcess; arr[30] = MonsterWeirdlingBeastProcess; 
        arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process; arr[12] = MonsterFireSpriteProcess;
        arr[73] = MonsterWizardRedProcess; arr[29] = MonsterAirshipCaptainProcess; arr[39] = MonsterWoundedApprentice;
    }
    if (thingID)
        Bind(arr[key], &unit);
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}


int TalkingSkullBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 200; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; 
		arr[29] = 90; arr[31] = 3; arr[32] = 3; arr[33] = 7; arr[59] = 5542432; 
		arr[60] = 2303; arr[61] = 46917632; 
		link=arr;
	}
	return link;
}

void TalkingSkullSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 325);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 325);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, TalkingSkullBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int TalkingSkull2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 325; arr[19] = 110; 
		arr[21] = 1060320051; arr[24] = 1065353216; arr[26] = 4; arr[28] = 1101004800; arr[29] = 160; 
		arr[30] = 1106247680; arr[32] = 7; arr[33] = 14; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 12; arr[56] = 20; 
		arr[59] = 5544320; arr[60] = 2303; arr[61] = 46917632; 
		link=arr;
	}
	return link;
}

void TalkingSkull2SubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1075167887);
		SetMemory(ptr + 0x224, 1075167887);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 360);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 360);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, TalkingSkull2BinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}
void AirshipCaptainSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076677837);
		SetMemory(ptr + 0x224, 1076677837);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2049);
		SetMemory(GetMemory(ptr + 0x22c), 400);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WizardBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 25714; arr[17] = 425; arr[19] = 50; arr[23] = 34816; 
		arr[24] = 1067869798; arr[26] = 4; arr[28] = 1103626240; arr[29] = 50; arr[32] = 15; 
		arr[33] = 31; arr[53] = 1128792064; arr[54] = 4; arr[59] = 5542432; arr[60] = 1327; 
		arr[61] = 46910208; 
		link=arr;
	}
	return link;
}

void WizardSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1069547520);
		SetMemory(ptr + 0x224, 1069547520);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 425);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 425);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 0);
	}
}

int HecubahOrbBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 1332240738; arr[2] = 25202; arr[17] = 600; arr[19] = 110; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; 
		arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; arr[40] = 1852140903; arr[41] = 116; 
		arr[53] = 1133903872; arr[55] = 26; arr[56] = 36; arr[57] = 5547984; arr[58] = 5545616; 
		arr[60] = 1384; arr[61] = 46914560; 
		link=arr;
	}
	return link;
}

void HecubahOrbSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1079194419);
		SetMemory(ptr + 0x224, 1079194419);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 600);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 600);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahOrbBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}
int GreenFrogBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[17] = 500; arr[19] = 64; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[28] = 1106247680; 
		arr[29] = 85; arr[30] = 1112014848; arr[31] = 2; arr[32] = 7; arr[33] = 15; 
		arr[59] = 5543904; arr[60] = 1313; arr[61] = 46905856;
		link = arr;
	}
	return link;
}

void GreenFrogSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1073070735);
		SetMemory(ptr + 0x224, 1073070735);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 1000);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 1000);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GreenFrogBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}
int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 20; arr[19] = 100; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; 
		arr[29] = 180; arr[30] = 1092616192; arr[31] = 11; arr[32] = 9; arr[33] = 17; arr[57] = 5548288; 
		arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

void LichLordSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 1400);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 1400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichLordBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void StrBossDeath()
{
	int arr[22], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 268698116; arr[1] = 70799300; arr[2] = 134349058; arr[3] = 33563682; arr[4] = 67174655; arr[5] = 1094685201; arr[6] = 1107378240; arr[7] = 595933448; 
	arr[8] = 553717792; arr[9] = 1883522300; arr[10] = 1900217887; arr[11] = 2236928; arr[12] = 144703489; arr[13] = 1074847744; arr[14] = 76021760; arr[15] = 536871038; 
	arr[16] = 1111752704; arr[17] = 2139617344; arr[18] = 557924339; arr[19] = 132128; arr[20] = 1619001344; arr[21] = 130575; 
	for (i = 0 ; i < 22 ; i ++)
		count = DrawStrBossDeath(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrBossDeath(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 61 == 60)
			MoveWaypoint(1, GetWaypointX(1) - 120.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void HookArrowTraps(int trapUnit)
{
    int ptr = UnitToPtr(trapUnit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x0c, 693);
}

void PixieCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 125, 1);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.3);
            Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            break;
        }
        else if (!GetCaller())
			WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int MissilePixie(float sX, float sY, int sOwner)
{
    int mis = CreateObjectAt("Pixie", sX, sY);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetMemory(ptr + 0x2b8, ImportUnitCollideFunc());
    SetMemory(ptr + 0x2fc, PixieCollide);
    SetOwner(sOwner, mis);
    return mis;
}

int FallingMeteor(float sX, float sY, int sDamage, float sSpeed)
{
    int mUnit = CreateObjectAt("Meteor", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec), sDamage);
        SetMemory(ptr + 0x14, GetMemory(ptr + 0x14) | 0x20);
        Raise(mUnit, 255.0);
        SetMemory(ptr + 0x6c, ToInt(sSpeed));
    }
    return mUnit;
}
void SendPacketAll(char *packet, int packetLength)
{
    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x90, 0x53, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x1C, 0x31, 0xC0, 0xC3,
    };
    int args[]={
        packet,0xff,packetLength,
    }; //0xff or 0x9f
    invokeRawCode(code, args);
}

int GetPlayerUnit(int pIndex){
    int *pInfo=0x62f9e0 + (0x12dc*pIndex);

    if (!pInfo[0])
        return 0;

    int *ptr=pInfo[0];
    return ptr[11];
}

void SendPacketSpecifyOne(int user, char *packet, int packetLength)
{
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0)
        return;
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
    };
    int args[]={
        packet,pIndex,packetLength,
    };
    invokeRawCode(code, args);
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}
int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}
int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int DrawImageAt(float x, float y, int thingId)
{
    int unit = CreateObjectAt("AirshipBasketShadow", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, thingId);
    return unit;
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

int ImportWeaponHitFunc()
{
    int arr[9], link;

    if (!link)
    {
        arr[0] = 0x50731068; arr[1] = 0x8B565000; arr[2] = 0x8B102474; arr[3] = 0x0002FC86;
        arr[4] = 0x006A5600; arr[5] = 0x2454FF50; arr[6] = 0x0CC48314; arr[7] = 0xC483585E;
        arr[8] = 0x9090C304;
        link=arr;
    }
    return link;
}
int GetMasterUnit(){
    int master;

    if (!master){
        master=CreateObjectById(OBJ_HECUBAH,100.0,100.0);
        Frozen(master,TRUE);
    }
    return master;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}
void DrawText(float x, float y, char n){
	int u=DummyUnitCreateById(OBJ_FISH_BIG,x,y);

	// controlHealthbarMotion(u,n);
	LookWithAngle(u, n*32);
}

int GetWeaponEnchantLevel(int unit, int propertyPtr)
{
	int ptr = UnitToPtr(unit), i, lv = 0;

	if (ptr)
	{
		if (GetMemory(ptr + 8) & 0x1000000)
		{
			for (i = 0 ; i < 4 ; i ++)
			{
				if (GetMemory(GetMemory(ptr + 0x2b4) + 8) == GetMemory(propertyPtr + (i * 24)))
				{
					lv += (i + 1);
					break;
				}
			}
			for (i = 0 ; i < 3 ; i ++)
			{
				if (GetMemory(GetMemory(ptr + 0x2b4) + 12) == GetMemory(propertyPtr + (i * 24)))
				{
					lv += (i + 1);
					break;
				}
			}
		}
	}
	return lv;
}
int InvincibleInventoryItem(int unit)
{
    int inv = GetLastItem(unit), count = 0;

    while (inv)
    {
        if (UnitCheckEnchant(inv, GetLShift(11)))
            1;
        else
        {
            count ++;
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void RemoveEquipments(int unit)
{
	int inven = unit + 2;
	
	while (HasClass(inven, "WEAPON") || HasClass(inven, "ARMOR"))
    {
		Delete(inven);
		inven += 2;
	}
}
void ManaBombCancelFx(int sUnit)
{
    int caster = CreateObjectById(OBJ_IMAGINARY_CASTER, GetObjectX(sUnit), GetObjectY(sUnit));

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}
