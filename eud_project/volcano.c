
#include "volcano_resource.h"
#include "libs\printutil.h"
#include "libs/format.h"
#include "libs\mathlab.h"
#include "libs\coopteam.h"
#include "libs\fxeffect.h"
#include "libs\observer.h"
#include "libs\reaction.h"
#include "libs\network.h"
#include "libs\waypoint.h"
#include "libs\network.h"
#include "libs/indexloop.h"
#include "libs/linked_list.h"
#include "libs/objectIDdefines.h"
#include "libs/queueTimer.h"
#include "libs/game_flags.h"
#include "libs/sound_define.h"

int m_pImgVector;
int m_towerList;
int player[20];
int g_towershotDelay = 90;
int g_GGOver = 0;
int g_centerUnit;
int g_remainTime = 300;
int g_mainPoint;

int g_plrGrp[10];

void InitPlayerGrp()
{
    int u;

    for (u = 0 ; u < 10 ; ++u)
    {
        g_plrGrp[u] = CreateObjectAt("MovableCrypt1", LocationX(51), LocationY(51));
        UnitNoCollide(g_plrGrp[u]);
        SetUnitFlags(g_plrGrp[u], GetUnitFlags(g_plrGrp[u]) ^ 0x4000);
        TeleportLocationVector(51, 10.0, 0.0);
    }
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
        arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4;
		arr[53] = 1128792064; arr[54] = 4; 
		link = arr;
	}
	return link;
}

void startupMent(int order)
{
    string guidement[5] = {
        "패닉 저장소 blog.daum.net/ky10613 에서 언제든지 최신 맵들을 다운받으실 수 있습니다.",
        "게임방법- 5 분동안 탄막을 피하면서 탄막을 발사하는 가드타워를 모두 파괴하세요 ...!", 
        "탄막피하기- 지금부터 게임을 시작하겠습니다 ...!", 
        "이제부터 더 이상 플레이어가 맵에 진입할 수 없습니다.",
        "가드 타워가 공격을 시작합니다...!"};
    UniPrintToAll(guidement[order]);
    if (order < 3)
        FrameTimerWithArg(140, ++order, startupMent);
    else if (order == 3)
    {
        // ObjectOff(mainSw());
        g_mainPoint = 1;
        FrameTimerWithArg(75, ++order, startupMent);
    }
    else
        startGame();
}

#define IMAGE_VECTOR_SIZE 32
void PlayerClassCommonWhenEntry()
{
    m_pImgVector=CreateImageVector(IMAGE_VECTOR_SIZE);

    InitializeImageHandlerProcedure(m_pImgVector);
    InitializeCommonResource(m_pImgVector);
}

static void NetworkUtilClientMain()
{
    PlayerClassCommonWhenEntry();
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
}

void initTemp()
{
    grpFormat(-1);
    guardTower(-1);
    //guardFlag(-1);
    FrameTimerWithArg(1, 1, toggleTowerGodMode);
    // ObjectOn(mainSw());

    //delay_run
    FrameTimerWithArg(150, 0, startupMent);

    //loop_run
    FrameTimer(1, LoopPreservePlayer);
}

void FlagPickupNothing()
{
    UniPrint(OTHER, "뭐하는거야... 어서 빨리 들어가라구!!");
}

void onFoilPickup()
{
    PlaySoundAround(OTHER, SOUND_JournalEntryAdd);
    UniPrint(OTHER, "!!슈팅 탄막타워 부수기!! 맵을 도전하고 있습니다");
}

void putFoilSingle(short location)
{
    int f=CreateObjectById(OBJ_FOIL_CANDLE_LIT, LocationX(location), LocationY(location));

    Frozen(f, TRUE);
    UnitNoCollide(f);
    SetUnitCallbackOnPickup(f, onFoilPickup);
}

void PutFoils()
{
    putFoilSingle(59);
    putFoilSingle(60);
    putFoilSingle(61);
    putFoilSingle(62);
}

void MapInitialize()
{
    MusicEvent();
    InitPlayerGrp();
    FrameTimer(1, MakeCoopTeam);
    setLighting(33, 45);
    FrameTimer(3, initTemp);
    g_centerUnit = CreateObjectAt("InvisibleLightBlueHigh", LocationX(11), LocationY(11));

    RegistSignMessage(Object("mapPick1"), "관전자 모드로 전환하려면 이 비콘을 밟아주세요");
    RegistSignMessage(Object("mapPick2"), "게임에 참가 하시려면 왼쪽으로 달려가세요");
    RegistSignMessage(Object("mapPick3"), "게임방법: 맵 양끝 8방향에 슈팅타워가 놓여져 있습니다. 이걸 다 부수세여");
    RegistSignMessage(Object("mapPick4"), "게임방법: 스킬은 늑데의 눈[트리플 에로우]와 작살[화이어볼] 2개 있어요");
    RegistSignMessage(Object("mapPick5"), "라이프는 그딴 거 없습니다. 죽으면 관전할 수 있고 전멸하면 끝이에요 끝!");

    int flag = CreateObjectAt("AzureFlag", LocationX(50), LocationY(50));

    UnitNoCollide(flag);
    SetUnitCallbackOnPickup(flag, FlagPickupNothing);

    int flagob = CreateObjectAt("AzureFlag", LocationX(56), LocationY(56));

    UnitNoCollide(flagob);
    SetUnitCallbackOnPickup(flagob, FlagPickupNothing);

    CandleGroupCreate();
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_ENABLE_CAMPER_ALARM))
        SetGameFlags(GAME_FLAG_ENABLE_CAMPER_ALARM);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);

    PutFoils();
}

int mainSw() {
    int mainsw;

    if (!mainsw) mainsw = Object("mainSwitch");
    return mainsw;
}

void killUnitAll() {
    int i;

    for(i = 0 ; i < 10 ; i += 1) {
        if (CurrentHealth(player[i]) > 0) {
            EnchantOff(player[i], "ENCHANT_INVULNERABLE");
            Damage(player[i], 0, 255, 14);
        }
    }
}

void mixArray(int *p, int length)
{
    int r, max=length-1, t;
    while (--length>=0)
    {
        r=Random(0, max);
        if (r==length)
            continue;
        t=p[r];
        p[r]=p[length];
        p[length]=t;
    }
}

#define GUARD_TOWER_MAX 8

void initializeGuardTower()
{
    int order[GUARD_TOWER_MAX]={0,1,2,3,4,5,6,7};

    mixArray(order, GUARD_TOWER_MAX);
    mixArray(order, GUARD_TOWER_MAX);
    mixArray(order, GUARD_TOWER_MAX);
    LinkedListCreateInstance(&m_towerList);
    int count = GUARD_TOWER_MAX;

    while (--count>=0)
        LinkedListPushback(m_towerList, order[count], NULLPTR);
}

void startGame()
{
    getCircle(CreateObject("InvisibleLightBlueHigh", 11), 200.0);
    toggleTowerGodMode(0);
    flashEffectWithPlayers();
    initializeGuardTower();
    PushTimerQueue(100, 0, onGuardTowerShooting);
    // timerUnit();
}

void setLighting(int min, int max) {
    int i;

    for(i = min ; i <= max ; i += 1)
        Enchant(CreateObject("InvisibleLightBlueHigh", i), "ENCHANT_LIGHT", 0.0);
}

int GetNearPlayer(int arg_0)
{
    float cdist, tempdist = 9999.0;
    int i, res = -1;

    for (i = 0 ; i < 10 ; i += 1)
    {
        if (CurrentHealth(player[i]))
        {
            if (CheckPlayerDeath(i)) continue;
            cdist = DistanceUnitToUnit(arg_0, player[i]);

            if (cdist < tempdist)
            {
                tempdist = cdist;
                res = i;
            }
        }
    }
    return res;
}

// void rearrangeTowerList()
// {
//     int node = 0, n = 0, next = 0;

//     LinkedListFront(m_towerList, &node);
//     while (node)
//     {
//         n= LinkedListGetValue(node);
//         LinkedListNext(node, &next);
//         if (!CurrentHealth(guardTower(n)))
//         {
//             LinkedListPop(m_towerList, node);
//         }
//         node=next;
//     }
// }

#define GUARDTOWER_ALT_DELAY 3

void onGuardTowerShooting(int node)
{
    int next;

    if (!node)
    {
        if (!LinkedListFront(m_towerList, &node))
        {
            UniPrintToAll("모든 가드타워가 파괴되었습니다__!!");
            return;
        }
    }
    int n = LinkedListGetValue(node);
    int delay = g_towershotDelay;

    LinkedListNext(node, &next);
    if (CurrentHealth(guardTower(n)))
        shotGuardTower(n);
    else
    {
        LinkedListPop(m_towerList, node);
        delay=GUARDTOWER_ALT_DELAY;
    }
        // rearrangeTowerList();
        // delay = GUARDTOWER_ALT_DELAY;
    PushTimerQueue(delay, next, onGuardTowerShooting);
}

void releaseTower()
{
    int deaths;
    char message[192];

    ObjectOff(SELF);
    g_towershotDelay += 3;
    // rearrangeTowerList();
    ++deaths;
    NoxSprintfString(message, "방금 가드타워 하나가 파괴 되었습니다 .\n현재 파괴회수: %d 킬, 목표치: 8킬", &deaths, 1);
    UniPrintToAll(ReadStringAddressEx(message));
    if (deaths == 8)
        victoryEvent();
}

void onVictoryPickup()
{
    PlaySoundAround(OTHER, SOUND_JournalEntryAdd);
    UniPrint(OTHER, "***당신의 승리입니다-!");
}

void victoryEvent()
{
    g_GGOver = 1;
    teleportAllPlayer(11);

    UniPrintToAll("승리!! - 가드타워를 전부 파괴하셨습니다 .");
    AudioEvent("FlagCapture", 11);
    Effect("WHITE_FLASH", GetWaypointX(11), GetWaypointY(11), 0.0, 0.0);
    int unit1 = CreateObjectAt("BridgeGuardsBoots", LocationX(11), LocationY(11));

    SetUnitCallbackOnPickup(unit1, onVictoryPickup);

    int u, unit[4];

    for (u = 0 ; u < 4 ; u ++)
    {
        unit[u]= CreateObjectAt("BottleCandle", LocationX(52 + u), LocationY(52 + u));
        SetUnitCallbackOnPickup(unit[u], onVictoryPickup);
    }
}

void teleportAllPlayer(int wp)
{
    int i;

    for (i = 0; i < 10 ; i += 1)
    {
        if (CurrentHealth(player[i]))
        {
            MoveObject(player[i], GetWaypointX(wp), GetWaypointY(wp));
            Enchant(player[i], "ENCHANT_FREEZE", 15.0);
        }
    }
}

int checkAllGens()
{
    int i;
    int res = 0;

    for (i = 0 ; i < 8 ; i += 1)
        res += CurrentHealth(guardTower(i));
    if (!res) return -1;
    else return 0;
}

#define SHOOTING_DISTANCE 69.0

void shotGuardTower(int towerId)
{
    int tower = guardTower(towerId);
    int targetIndex = GetNearPlayer(tower);

    if (targetIndex + 1)
    {
        float dist = Distance(GetObjectX(tower), GetObjectY(tower), GetObjectX(player[targetIndex]), GetObjectY(player[targetIndex]));
        float xvect = (GetObjectX(tower) - GetObjectX(player[targetIndex])) * SHOOTING_DISTANCE / dist;
        float yvect = (GetObjectY(tower) - GetObjectY(player[targetIndex])) * SHOOTING_DISTANCE / dist;
        int subUnit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(tower) - xvect, GetObjectY(tower) - yvect);
        int demon = CreateObjectAt("AirshipBasket", GetObjectX(subUnit), GetObjectY(subUnit));

        // SetUnitMaxHealth(demon, towerId + 10);
        LookAtObject(demon, player[targetIndex]);
        SetUnitCallbackOnCollide(demon, collisonEvent);
        // SetCallback(demon, 9, collisonEvent);
        trailUnit(subUnit);
        UniChatMessage(tower, "준비하시고~ 쏘세요!", 48);
    }
}

void trailUnit(int flier)
{
    while (IsObjectOn(flier))
    {
        if (checkUnitLimitLine(flier))
        {
            float xVect = UnitAngleCos(flier + 1, 30.0), yVect = UnitAngleSin(flier + 1, 30.0);

            MoveObjectVector(flier, xVect, yVect);
            MoveObject(flier + 1, GetObjectX(flier), GetObjectY(flier));
            Effect("Explosion", GetObjectX(flier), GetObjectY(flier), 0.0, 0.0);
            PlaySoundAround(flier, 285);
            FrameTimerWithArg(1, flier, trailUnit);
            break;
        }
        Delete(flier);
        Delete(++flier);
        break;
    }
}

void collisonEvent()
{
    if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER) && !HasEnchant(OTHER, "ENCHANT_FREEZE"))
    {
        Damage(OTHER, SELF, 150, 9);
        UniChatMessage(OTHER, "격추 당했습니다...!", 120);
    }
}

void toggleTowerGodMode(int arg_0) {
    int i;
    
    for(i = 0 ; i < 8 ; i += 1) {
        if (arg_0 == 0) EnchantOff(guardTower(i), "ENCHANT_INVULNERABLE");
        else Enchant(guardTower(i), "ENCHANT_INVULNERABLE", 0.0);
    }
}

void flashEffectWithPlayers() {
    int i;

    for(i = 0 ; i < 10 ; i += 1) {
        if (CurrentHealth(player[i]) > 0) {
            MoveWaypoint(5, GetObjectX(player[i]), GetObjectY(player[i]));
            Effect("WHITE_FLASH", GetWaypointX(5), GetWaypointY(5), 0.0 , 0.0);
            AudioEvent("HecubahTaunt", 5);
        }
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, OBJ_HARPOON_BOLT, HarpoonEvent);
    HashPushback(pInstance, OBJ_FAN_CHAKRAM_IN_MOTION, MissileAttack);
}

void FireStoneFlying(int subUnit)
{
    while (IsObjectOn(subUnit))
    {
        int durate = GetDirection(subUnit);

        if (durate && IsVisibleTo(subUnit, subUnit + 1) && IsObjectOn(subUnit + 2))
        {
            int owner = GetOwner(subUnit);

            if (CurrentHealth(owner))
            {
                FrameTimerWithArg(1, subUnit, FireStoneFlying);
                LookWithAngle(subUnit, --durate);
                MoveObjectVector(subUnit, GetObjectZ(subUnit), GetObjectZ(subUnit + 1));
                MoveObjectVector(subUnit + 1, GetObjectZ(subUnit), GetObjectZ(subUnit + 1));
                MoveObject(subUnit + 2, GetObjectX(subUnit), GetObjectY(subUnit));
                DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(subUnit), GetObjectY(subUnit)), 9);
                break;
            }
        }
        Delete(subUnit ++);
        Delete(subUnit ++);
        Delete(subUnit ++);
        break;
    }
}

void FireStoneCollide()
{
    int owner = GetOwner(SELF);

    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 120, 9);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.3);
        }
        else if (!GetCaller())
            break;
        else
            break;
        Delete(SELF);
        break;
    }
}

void HarpoonEvent(int cur, int owner)
{
    if (CurrentHealth (owner))
    {
        float vectsize = 18.0;
        float xvect = UnitAngleCos(owner, vectsize), yvect = UnitAngleSin(owner, vectsize);
        int mis = CreateObjectAt ("ImaginaryCaster", GetObjectX(cur) + xvect, GetObjectY(cur) + yvect);

        Raise (mis, xvect);
        Raise (CreateObjectAt ("ImaginaryCaster", GetObjectX(cur) - xvect, GetObjectY(cur) - yvect), yvect);
        Frozen (CreateObjectAt ("TraderArmorRack1", GetObjectX(mis), GetObjectY(mis)), TRUE);
        SetOwner (owner, mis);
        SetOwner (owner, mis + 2);
        LookWithAngle (mis, 64);
        SetUnitCallbackOnCollide(mis + 2, FireStoneCollide);
        FrameTimerWithArg (1, mis, FireStoneFlying);
        UniChatMessage(mis + 2, "마동석: 마. 니 자신있나?", 108);
    }
    Delete(cur);
}

void MissileAttack(int cur, int owner)
{    
    if (CurrentHealth(owner))
    {
        int mis = CreateObjectAt("WeakFireball", GetObjectX(cur), GetObjectY(cur));
        SetOwner(owner, CreateObjectAt("OgreShuriken", GetObjectX(mis), GetObjectY(mis)));
        SetOwner(owner, mis);
        PushObjectTo(mis, UnitRatioX(cur, owner, 50.0), UnitRatioY(cur, owner, 50.0));
        PushObjectTo(mis + 1, UnitRatioX(cur, owner, 50.0), UnitRatioY(cur, owner, 50.0));
        MoveWaypoint(1, GetObjectX(cur), GetObjectY(cur));
        AudioEvent("PixieHit", 1);
    }
    Delete(cur);
}

int CreateMagicArrow(float xProfile, float yProfile, int owner)
{
    int unit = CreateObjectAt("ArcherArrow", xProfile, yProfile);
    Enchant(unit, "ENCHANT_HASTED", 0.0);
    Enchant(unit, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
    SetOwner(owner, unit);
    LookAtObject(unit, owner);
    LookWithAngle(unit, GetDirection(unit) + 128);
    PushObject(unit, 20.0, GetObjectX(owner), GetObjectY(owner));

    return unit;
}

void MagicArrow(int owner)
{
    if (CurrentHealth(owner))
    {
        float dx = UnitAngleCos(owner, 20.0), dy = UnitAngleSin(owner, 20.0);
        int mis = CreateMagicArrow(GetObjectX(owner) + dx, GetObjectY(owner) + dy, owner);
        CreateMagicArrow(GetObjectX(mis) - (dy * 0.3), GetObjectY(mis) + (dx * 0.3), owner);
        CreateMagicArrow(GetObjectX(mis) + (dy * 0.3), GetObjectY(mis) - (dx * 0.3), owner);
        
        DeleteObjectTimer(CreateObjectAt("ManaBombCharge", GetObjectX(mis), GetObjectY(mis)), 20);
        PlaySoundAround(owner, 226);
    }
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;
    ObserverPlayerCameraLock(pUnit);
    if (ValidPlayerCheck(pUnit))
    {
        if (pUnit ^ GetHost())
            NetworkUtilClientEntry(pUnit);
        else
            PlayerClassCommonWhenEntry();
        PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    }

    return plr;
}

void getPlayers()
{
    if (g_mainPoint)
    {
        MoveObject(OTHER, LocationX(47), LocationY(47));
        return;
    }

    int plr, i;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            if (MaxHealth(OTHER) == 150)
            {
                plr = CheckPlayer();
                for (i = 9 ; i >= 0 && plr < 0 ; i -= 1)
                {
                    if (!MaxHealth(player[i]))
                    {
                        plr = PlayerClassOnInit(i, GetCaller());
                        break;
                    }
                }
                if (plr + 1)
                {
                    entryMid(player[plr], plr);
                    break;
                }
            }
        }
        ForbidEntry(OTHER);
        break;
    }
}

int PlayerRocks(int num)
{
    int ptr, i;

    if (num < 0)
    {
        ptr = CreateObject("InvisibleLightBlueLow", 1) + 1;
        for (i = 0 ; i < 10 ; i ++)
        {
            CreateObject("Rock5", 22 + i);
            SetUnitFlags(ptr + i, GetUnitFlags(ptr + i) ^ 0x2000);
        }
    }
    return ptr + num;
}

int CheckPlayerDeath(int plr)
{
    return player[plr + 10] & 0x02;
}

void SetPlayerDeathFlag(int plr)
{
    player[plr + 10] ^= 0x02;
}

int PlayerClassDeathFlag(int plr)
{
    return player[plr + 10] & 0x80000000;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] ^= 0x80000000;
}

void PlayerOnDeath(int plr)
{
    int unit = CreateObjectAt("Wizard", GetObjectX(player[plr]), GetObjectY(player[plr]));

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x40);
    DeleteObjectTimer(unit, 30);
    resetPlacedGrp(plr);
}

void walkingAnimate(int plr, int pUnit)
{
    float dist = DistanceUnitToUnit(pUnit, grpFormat(plr));

    if (dist <= 1.0)
    {
        if (HasEnchant(grpFormat(plr), "ENCHANT_VILLAIN"))
        {
            PauseObject(grpFormat(plr), 1);
            EnchantOff(grpFormat(plr), "ENCHANT_VILLAIN");
        }
    }
    else
    {
        Frozen(grpFormat(plr), 0);
        Enchant(grpFormat(plr), "ENCHANT_VILLAIN", 0.0);
        if (!HasEnchant(grpFormat(plr), "ENCHANT_DETECTING"))
        {
            Enchant(grpFormat(plr), "ENCHANT_DETECTING", 0.35);
            PlaySoundAround(pUnit, 518);
        }
        Frozen(grpFormat(plr), 1);
        LookWithAngle(grpFormat(plr), GetDirection(pUnit));
        Walk(grpFormat(plr), GetObjectX(grpFormat(plr)), GetObjectY(grpFormat(plr)));
        MoveObject(grpFormat(plr), GetObjectX(pUnit), GetObjectY(pUnit) + 5.0);
    }
}

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
    resetPlacedGrp(plr);
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    MoveObject(PlayerRocks(plr), GetObjectX(pUnit), GetObjectY(pUnit));
    // walkingAnimate(plr, pUnit);
    if (HasEnchant(pUnit, "ENCHANT_INFRAVISION"))
    {
        EnchantOff(pUnit, "ENCHANT_INFRAVISION");
        FrameTimerWithArg(1, pUnit, MagicArrow);
    }
    if (!CheckPlayerDeath(plr))
    {
        if (!HasEnchant(pUnit, "ENCHANT_INVISIBLE"))
            Enchant(pUnit, "ENCHANT_INVISIBLE", 0.0);
        if (ToInt(Distance(GetObjectX(pUnit) - 30.0, GetObjectY(pUnit) -20.0, GetObjectX(g_plrGrp[plr]), GetObjectY(g_plrGrp[plr]))))
            MoveObject(g_plrGrp[plr], GetObjectX(pUnit) - 30.0, GetObjectY(pUnit) - 20.0);
    }
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님 사망!");
    MoveObject(g_plrGrp[plr], LocationX(51), LocationY(51));
}

void LoopPreservePlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    player[i] = 0;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                        if (!CheckPlayerDeath(i))
                        {
                            SetPlayerDeathFlag(i);
                            PlayerOnDeath(i);
                        }
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayer);
}

int UnitLastItem(int unit)
{
    return GetLastItem(unit);
}

void DropLastItem(int unit)
{
    Drop(unit, GetLastItem(unit));
}

void DropAll(int unit)
{
    while (UnitLastItem(unit))
        DropLastItem(unit);
}

void ForbidEntry(int unit)
{
    MoveObject(unit, 1286.0, 4095.0);
    Enchant(unit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
}

void BufferTeleport(int pUnit)
{
    if (!CurrentHealth(pUnit))
        return;

    MoveObject(pUnit, LocationX(11), LocationY(11));

    DeleteObjectTimer(CreateObject("BlueRain", 11), 15);
    Effect("TELEPORT", LocationX(11), LocationY(11), 0.0, 0.0);
    AudioEvent("BlindOff", 11);
}

void entryMid(int unit, int plr)
{
    DropAll(unit);
    if (PlayerClassDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    if (CheckPlayerDeath(plr))
        SetPlayerDeathFlag(plr);
    MoveObject(unit, GetWaypointX(48), GetWaypointY(48));
    FrameTimerWithArg(3, unit, BufferTeleport);
    
    // Enchant(unit, "ENCHANT_SLOWED", 0.0);
    // Enchant(unit, "ENCHANT_HASTED", 0.0);
    Enchant(unit, "ENCHANT_AFRAID", 0.0);
}

void GrpProcess(int unit)
{
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
    }
}

int grpFormat(int arg_0)
{
    int var_0[10];
    int i;

    if (arg_0 == -1) {
        var_0[0] = Object("grpFormatBase");
        GrpProcess(var_0[0]);
        for(i = 1 ; i < 10 ; i += 1)
        {
            var_0[i] = var_0[0] + (i * 2);
            GrpProcess(var_0[i]);
        }
        return var_0[0];
    }
    return var_0[arg_0];
}

void resetPlacedGrp(int arg_0)
{
    MoveObject(grpFormat(arg_0), GetWaypointX(3) + IntToFloat(arg_0 * 23), GetWaypointY(3));
}

int guardTower(int arg_0)
{
    int var_0[8];
    int i;
    
    if (arg_0 == -1)
    {
        for(i = 0 ; i < 8 ; i += 1)
        {
            var_0[i] = CreateObject("NecromancerGenerator", 14 + i);
            ObjectOff(var_0[i]);
            SetUnitMaxHealth(var_0[i], 600);
        }
        return var_0[0];
    }
    return var_0[arg_0];
}

int guardFlag(int arg_0) {
    int var_0[8];
    int i;

    if (arg_0 == -1) {
        for(i = 0; i < 8 ; i += 1) {
            var_0[i] = CreateObject("flag", 14 + i);
            Frozen(var_0[i], 1);
        }
        return var_0[0];
    }
    return var_0[arg_0];
}

void getCircle(int arg_0, float arg_1) {
    int var_0 = 0;
    int var_1;
    float var_2;
    float var_3;
    float var_4;
    float var_5;
    float var_6;
 
    MoveWaypoint(1, GetObjectX(arg_0), GetObjectY(arg_0));
    var_1 = CreateObject("Brick3", 1);
    Frozen(var_1, 1);
    var_2 = GetObjectX(var_1);
    var_3 = GetObjectY(var_1) + 100.0;
 
    while(var_0 < 63) {
        var_4 = Distance(GetObjectX(arg_0), GetObjectY(arg_0), var_2, var_3);
        var_5 = (GetObjectX(arg_0) - var_2) * arg_1 / var_4; //dx
        var_6 = (GetObjectY(arg_0) - var_3) * arg_1 / var_4; //dy
        MoveWaypoint(1, GetObjectX(arg_0) - var_5, GetObjectY(arg_0) - var_6);
        CreateObject("FanChakram", 1);
        CreateObject("FanChakram", 1);
        CreateObject("FanChakram", 1);
        var_2 = GetObjectX(arg_0) - (0.1 * var_6) - var_5; //tp_rx
        var_3 = GetObjectY(arg_0) + (0.1 * var_5) - var_6; //tp_ry
        var_0 += 1;
    }
    Delete(var_1);
}

int checkUnitLimitLine(int unit)
{
    float xProfile = GetObjectX(unit), yProfile = GetObjectY(unit);

    if (xProfile > 100.0 && yProfile > 100.0 && xProfile < 5532.0 && yProfile < 5532.0)
        return 1;
    else
        return 0;
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

int DummyUnitCreateAt(string name, float xProfile, float yProfile)
{
    int dummy = CreateObjectAt(name, xProfile, yProfile);

    ObjectOff(dummy);
    Damage(dummy, 0, MaxHealth(dummy) + 1, -1);
    Frozen(dummy, 1);

    return dummy;
}

int ObserverGetTarget()
{
    int u;

    for (u = 0 ; u < 10 ; ++u)
    {
        if (CurrentHealth(player[u]))
        {
            if (CheckPlayerDeath(u))
                continue;
            return player[u];
        }
    }
    return g_centerUnit;
}

int ObserverGetNextTarget(int prevTarget)
{
    int u;

    for (u = 0 ; u < 10 ; ++ u)
    {
        if (CurrentHealth(player[u]))
        {
            if (CheckPlayerDeath(u))
                continue;
            if (prevTarget ^ player[u])
                return player[u];
        }
    }
    return g_centerUnit;
}

int PlayerObserverKeyscan(int pUnit, int *plrcre)
{
    int keyscan = CheckPlayerInput(pUnit);

    if (keyscan == 2)
        return 0;   //shutdown
    else if (keyscan == 6)
        SetUnit1C(plrcre[0], ObserverGetNextTarget(GetUnit1C(plrcre[0])));
    return 1;
}

void PlayerObserverShutdown(int pUnit)
{
    MoveObject(pUnit, LocationX(47), LocationY(47));
    UniPrint(pUnit, "관전모드에서 나오셨습니다");
    PlaySoundAround(pUnit, 1003);
}

void PlayerObserverOnLoop(int *plrcre)
{
    while (MaxHealth(plrcre[0]))
    {
        int owner = GetOwner(plrcre[0]);
        int target = GetUnit1C(plrcre[0]);

        if (CurrentHealth(target))
        {
            if (ToInt(DistanceUnitToUnit(target, plrcre[0])))
                MoveObject(plrcre[0], GetObjectX(target), GetObjectY(target));
        }

        if (MaxHealth(owner))
        {
            if (CheckWatchFocus(owner))
                PlayerLook(owner, plrcre[0]);
            if (PlayerObserverKeyscan(owner, plrcre))
            {
                FrameTimerWithArg(1, plrcre, PlayerObserverOnLoop);
                break;
            }
            else
            {
                PlayerObserverShutdown(owner);
            }
            
        }
        Delete(plrcre[0]);
        plrcre[0] = 0;
        break;
    }
}

int CreatePlayerObserverCamera(int owner)
{
    int target = ObserverGetTarget();
    int cam = DummyUnitCreateAt("Bomber", GetObjectX(target), GetObjectY(target));

    UnitNoCollide(cam);
    SetOwner(owner, cam);
    SetUnit1C(cam, target);

    return cam;
}

void StartObserver()
{
    int creats[32];
    
    if (!IsPlayerUnit(OTHER))
        return;
    int pIndex = GetPlayerIndex(OTHER);

    if (pIndex < 0)
        return;

    if (creats[pIndex])
    {
        UniPrint(OTHER, "이미 관전모드 실행중!");
        return;
    }
    creats[pIndex] = CreatePlayerObserverCamera(OTHER);

    MoveObject(OTHER, LocationX(48), LocationY(48));
    Enchant(OTHER, "ENCHANT_INVULNERABLE", 0.0);
    FrameTimerWithArg(1, &creats + (pIndex * 4), PlayerObserverOnLoop);

    UniPrint(OTHER, "관전모드로 전환되었습니다. 관전모드에서 나오려면 우클릭 하세요");
}

void PlayerFastJoin()
{
    int plr;

    if (CurrentHealth(OTHER))
    {
        plr = CheckPlayer();
        if (plr + 1)
            MoveObject(OTHER, LocationX(50), LocationY(50));
        else
            MoveObject(OTHER, LocationX(49), LocationY(49));
    }
}

void CandlePickupNotWorking()
{ }

int CreateCandleSingle(int locationId)
{
    int candle = CreateObjectAt("BottleCandle", LocationX(locationId), LocationY(locationId));

    SetUnitCallbackOnPickup(candle, CandlePickupNotWorking);
    TeleportLocationVector(locationId, 23.0, 23.0);
    Frozen(candle, TRUE);
    return candle;
}

void CandleGroupCreate()
{
    int u;

    for (u = 0 ; u < 17 ; u += 1)
    {
        CreateCandleSingle(57);
        CreateCandleSingle(58);
    }
}


