
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"

void imageAmulet(){}
void imagePortraitWizard(){}
void imagePortraitDropship(){}
void imagePortraitSCV(){}
void imagePortraitRitz(){}
void imagePortraitMedic(){}

void GRPDump_cent1_output(){}
void imageYouwin(){}

void ImageSpellset()
{ }
void ImageSpellsetLeftbase()
{ }
void ImageSpellsetRightbase()
{ }
void GRPDumpDragonRedOutput(){}

#define RED_DRAGON_IMAGE_START_ID 128403
void initializeImageRedDragon(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH_WITH_ORB;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpDragonRedOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,4,3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_WALK,4,3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_RUN,4,3,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3,USE_DEFAULT_SETTINGS,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, RED_DRAGON_IMAGE_START_ID,   IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, RED_DRAGON_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign4Directions(6, RED_DRAGON_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameCopy4Directions(     RED_DRAGON_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameCopy4Directions(RED_DRAGON_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
}

void GRPDump_ToothOniOutput(){}

#define TOOTH_ONI_BASE_IMAGE_ID 133967
void initializeImageFrameToothOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_WARRIOR;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_ToothOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, TOOTH_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, TOOTH_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, TOOTH_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void initializeCentAnimation(int imgVec)
{
    int *frames, *sizes, thingId=OBJ_TREASURE_CHEST;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_cent1_output)+4, thingId, xyInc, &frames, &sizes);
    AppendImageFrame(imgVec,frames[0], 112596);
    AppendImageFrame(imgVec,frames[0], 112597);
    AppendImageFrame(imgVec,frames[1], 112598);
    AppendImageFrame(imgVec,frames[2], 112599);
    AppendImageFrame(imgVec,frames[3], 112600);
    AppendImageFrame(imgVec,frames[4], 112601);
    AppendImageFrame(imgVec,frames[5], 112602);
    AppendImageFrame(imgVec,frames[0], 112603);
}

void GRPDumpRoachOniOutput(){}

#define ROACH_ONI_START_IMAGE_AT 133943

void initializeImageFrameRoachOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRoachOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign4Directions(0, ROACH_ONI_START_IMAGE_AT, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, ROACH_ONI_START_IMAGE_AT+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_AT  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, ROACH_ONI_START_IMAGE_AT+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_AT+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_AT   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_AT+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
}

void GRPDump_TalesweaverWallensteinOutput(){}

#define TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT 127714

void initializeImageFrameTalesweavergirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH;
    int xyInc[]={0,-13};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_TalesweaverWallensteinOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,7,2,2);
    AnimFrameAssign8Directions(0,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(30, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameAssign8Directions(35, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameAssign8Directions(40, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameAssign8Directions(75, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssign8Directions(70, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssign8Directions(65, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign8Directions(60, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+96, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameAssign8Directions(55, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameAssign8Directions(50, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameAssign8Directions(45, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
}

void GRPDumpBillyFaceOutput(){}

#define BILLYFACE_IMAGE_START_ID 133955
void initializeImageBillyFace(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int xyInc[]={0,-15};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpBillyFaceOutput)+4, thingId, xyInc, &frames, &sizes);

    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,2);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_WALK,3);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_WALK,2);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_RUN,3);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_RUN,2);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,2);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    AnimFrameAssign4Directions(0, BILLYFACE_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);    
    AnimFrameAssign4Directions(3, BILLYFACE_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BILLYFACE_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign4Directions(6, BILLYFACE_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BILLYFACE_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BILLYFACE_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions(     BILLYFACE_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BILLYFACE_IMAGE_START_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     BILLYFACE_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameCopy4Directions(     BILLYFACE_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
}

void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
}

void GRPDump_RedmistOutput(){}
#define REDMIST_OUTPUT_BASE_IMAGE_ID 120669
void initializeImageFrameRedmistOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_SPIDER;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_RedmistOutput)+4, thingId, xyinc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1, USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_BunnyGirlBlackOutput(){}
#define BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID 130005
void initializeImageFrameBunnyGirlBlackOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int xyInc[]={0,-11};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BunnyGirlBlackOutput)+4, thingId, xyInc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BUNNY_GIRL_BLACK_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_GobdungMediumOutput(){}

#define MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT 115269

void initializeImageFrameMediumGopdung(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_FISH_BIG;
    int xyInc[]={0,-2};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GobdungMediumOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
}

void CommonServerClientProcedure()
{
    int imgVec=CreateImageVector(2048);

    initializeImageFrameTalesweavergirl(imgVec);
    initializeCentAnimation(imgVec);
    initializeImageRedDragon(imgVec);
    initializeImageFrameToothOni(imgVec);
    initializeImageFrameRoachOni(imgVec);
    initializeImageBillyFace(imgVec);
    initializeImageFrameSoldier(imgVec);
    initializeImageFrameRedmistOutput(imgVec);
    initializeImageFrameBunnyGirlBlackOutput(imgVec);
    initializeImageFrameMediumGopdung(imgVec);
    AppendImageFrame(imgVec, GetScrCodeField(imageAmulet) + 4, 112960);
    AppendImageFrame(imgVec, GetScrCodeField(imagePortraitSCV) + 4, 14798);
    AppendImageFrame(imgVec, GetScrCodeField(imagePortraitDropship) + 4, 15020);
    AppendImageFrame(imgVec, GetScrCodeField(imagePortraitWizard) + 4, 14974);
    AppendImageFrame(imgVec, GetScrCodeField(imageYouwin) + 4, 112997);
    AppendImageFrame(imgVec, GetScrCodeField(imagePortraitRitz) + 4, 14822);
    AppendImageFrame(imgVec, GetScrCodeField(imagePortraitMedic) + 4, 14958);
    AppendImageFrame(imgVec, GetScrCodeField(ImageSpellset)+4, 14415);
    AppendImageFrame(imgVec, GetScrCodeField(ImageSpellsetLeftbase)+4, 14446);
    AppendImageFrame(imgVec, GetScrCodeField(ImageSpellsetRightbase)+4, 14447);
    DoImageDataExchange(imgVec);
}
