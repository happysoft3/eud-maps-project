
#include "lsdcomp_gvar.h"
#include "lsdcomp_utils.h"
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/hash.h"
#include "libs/objectIDdefines.h"
#include "libs/buff.h"
#include "libs/fxeffect.h"

void AddPetExpUsingAnItem(int pIndex, int amount) //virtual
{ }

void nullUseFunction()
{
    UniPrint(OTHER, "널 펑션...");
}

int createDropItemCode(int cb)
{
    char code[]={
        0x53, 0x56, 0x8B, 0x5C, 0x24, 0x0C, 0x8B, 0x74, 0x24, 0x10, 0x56, 
        0x53, 0x68, 0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 
        0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x31, 0xC0, 0x5E, 0x5B, 0xC3
    };
    int c;
    AllocSmartMemEx(sizeof(code), &c);
    char *pcode=c;
    NoxByteMemCopy(code,pcode,sizeof(code));
    int*p=&pcode[13];
    p[0]=cb;
    return c;
}

void settingCommonItem(int item)
{
    SetOwner(GetItemMaster(), item);
    int flags=GetUnitFlags(item);

    if (flags&UNIT_FLAG_NO_COLLIDE)
        flags^=UNIT_FLAG_NO_COLLIDE;
    if (!(flags&UNIT_FLAG_NO_PUSH_CHARACTERS))
        flags^=UNIT_FLAG_NO_PUSH_CHARACTERS;
    SetUnitFlags(item, flags);
    // Frozen(item, TRUE);
    // SetUnitCallbackOnDiscard(item, onCommonItemDrop); //콜백 필드에서 +1 오류가 나서 지움
    char *ptr=UnitToPtr(item);

    SetMemory(&ptr[0x2c8], GetDropCode());
}

void copyCommonItem(int target, int holder)
{
    int item=CreateObjectById(GetUnitThingID(target), GetObjectX(holder),GetObjectY(holder));
    int *ptr=UnitToPtr(item);
    int *targetPtr = UnitToPtr(target);

    settingCommonItem(item);
    SetUnitCallbackOnUseItem(item, nullUseFunction);
    SetMemory(ptr+ImportUseItemFuncOffset(), GetMemory(targetPtr+ImportUseItemFuncOffset()));
}


void onCommonItemDrop()
{
    if (IsPlayerUnit(OTHER))
    {
        int pIndex = GetPlayerIndex(OTHER);
        int cre = GetCreature(pIndex);

        if(CurrentHealth(cre))
        {
            copyCommonItem(SELF, cre);
        }
    }
    Delete(SELF);
}

int commonItemCreate(short thingId, int effectFn, float x, float y)
{
    int item = CreateObjectById(thingId, x,y);

    settingCommonItem(item);
    if (effectFn)
        SetUnitCallbackOnUseItem(item, effectFn);
    return item;
}

void UseHealthPotion()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        RestoreHealth(cre, 30);
        UniPrint(OTHER, "체력회복약을 사용하여 여신의 체력을 회복하였습니다");
    }
    Delete(SELF);
}

void onCiderUse()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        RestoreHealth(cre, 60);
        UniPrint(OTHER, "사과술을 사용하여 여신의 체력을 회복하였습니다");
    }
    Delete(SELF);
}

void onUseShockPotion()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        RestoreHealth(cre, 50);
        Enchant(cre, EnchantList(ENCHANT_SHOCK), 0.0);
        UniPrint(OTHER, "쇼크물약을 사용했습니다");
    }
    Delete(SELF);
}

void onExpBookUse()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        AddPetExpUsingAnItem(pIndex, 15);
        Effect("YELLOW_SPARKS", GetObjectX(cre), GetObjectY(cre), 0.0, 0.0);
        UniPrint(OTHER, "경험치 책을 사용했습니다 (경험치+15)");
    }
    Delete(SELF);
}

void onPixieCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (!CurrentHealth(owner))
        {
            Delete(SELF);
            return;
        }
        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 3, DAMAGE_TYPE_PLASMA);
        }
    }
}

int summonSuperPixie(int owner)
{
    int pix=CreateObjectById(OBJ_PIXIE, GetObjectX(owner), GetObjectY(owner));

    SetOwner(owner, pix);
    SetUnitSubclass(pix, GetUnitSubclass(pix) ^ 1);
    SetUnitCallbackOnCollide(pix, onPixieCollide);
    return pix;
}

void onAmuletofCombatUse()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        summonSuperPixie(cre);
        UniPrint(OTHER, "픽시 하나를 소환했습니다 (죽게되면 사라집니다)");
    }
    Delete(SELF);
}

void onShieldPotionUse()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        Enchant(cre, EnchantList(ENCHANT_REFLECTIVE_SHIELD), 120.0);
        UniPrint(OTHER, "잠시동안 마법 방패가 지속됩니다");
    }
    Delete(SELF);
}

void onCandleSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 100, DAMAGE_TYPE_PLASMA);
        }
    }
}

void onCandleUse()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        float x= GetObjectX(cre), y=GetObjectY(cre);
        SplashDamageAt(OTHER,x,y, 100.0, onCandleSplash);
        GreenExplosion(x,y);
        UniPrint(OTHER, "주변에 데미지 100 을 줍니다");
    }
    Delete(SELF);
}

void onCurePotionUse()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        RestoreHealth(cre, 15);
        CastSpellObjectObject("SPELL_CURE_POISON", cre, cre);
        UniPrint(OTHER, "체력을 약간 회복하면서 중독상태를 치료해 줍니다");
    }
    Delete(SELF);
}

void onHealingPotionPreserve(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int owner = GetUnit1C(sub);

        if (CurrentHealth(owner))
        {
            int dur=GetDirection(sub);
            if (dur)
            {
                PushTimerQueue(3, sub, onHealingPotionPreserve);
                MoveObject(sub, GetObjectX(owner), GetObjectY(owner));
                LookWithAngle(sub, dur-1);
                RestoreHealth(owner, 2);
                return;
            }
        }
        Delete(sub);
    }
}

void onFoilCandleUse()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int pIndex = GetPlayerIndex(OTHER);
    int cre = GetCreature(pIndex);

    if (CurrentHealth(cre))
    {
        int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(cre),GetObjectY(cre));

        PushTimerQueue(1, sub, onHealingPotionPreserve);
        LookWithAngle(sub, 60);
        SetUnit1C(sub, cre);
        Enchant(sub, EnchantList(ENCHANT_RUN), 0.0);
        UniPrint(OTHER, "잠시동안 체력회복 속도가 증가합니다");
    }
    Delete(SELF);
}

void PlayerTeleportToSafeZone(int user)
{
    int pIndex = GetPlayerIndex(user);

    if (pIndex<0)
        return;
        
    int cre = GetCreature(pIndex);
    int resp = GetRespawnSpot(pIndex);

    if (CurrentHealth(cre))
    {
        if (DistanceUnitToUnit(cre, resp) > 200.0)
        {
            Effect("TELEPORT", GetObjectX(cre), GetObjectY(cre), 0.0, 0.0);
            Effect("SMOKE_BLAST", GetObjectX(cre), GetObjectY(cre), 0.0, 0.0);
            MoveObject(cre, GetObjectX(resp), GetObjectY(resp));
            Effect("TELEPORT", GetObjectX(cre), GetObjectY(cre), 0.0, 0.0);
            Effect("BLUE_SPARKS", GetObjectX(cre), GetObjectY(cre), 0.0, 0.0);
            UniPrint(user, "세이프 존으로 공간이동 하였습니다");
            return;
        }
        UniPrint(user, "세이프 존 주변에서는 공간이동을 할 수 없습니다");
    }
}

void onBlinkBookUse()
{
    PlayerTeleportToSafeZone(OTHER);
}

int commonItemHash()
{
    int hash;

    if (!hash)
    {
        HashCreateInstance(&hash);
    }
    return hash;
}

void InitializeCommonItemHash()
{
    int hash = commonItemHash();

    HashPushback(hash, OBJ_BRACELETOF_HEALTH, UseHealthPotion);
    HashPushback(hash, OBJ_SPELL_BOOK, onBlinkBookUse);
    HashPushback(hash, OBJ_FEAR, onUseShockPotion);
    HashPushback(hash, OBJ_ABILITY_BOOK, onExpBookUse);
    HashPushback(hash, OBJ_CIDER, onCiderUse);
    HashPushback(hash, OBJ_AMULETOF_COMBAT, onAmuletofCombatUse);
    HashPushback(hash, OBJ_AMULETOF_MANIPULATION, onShieldPotionUse);
    HashPushback(hash, OBJ_FOIL_CANDLE_LIT, onFoilCandleUse);
    HashPushback(hash, OBJ_CANDLE_1, onCandleUse);
    HashPushback(hash, OBJ_CURE_POISON_POTION, onCurePotionUse);

    SetDropCode( createDropItemCode(onCommonItemDrop) );
}

int CreateEffectiveItem(short itemId, float x, float y)
{
    int fn;

    if (!HashGet(commonItemHash(), itemId, &fn, FALSE))
        fn = nullUseFunction;

    return commonItemCreate(itemId, fn, x,y);
}

int CreateEffectiveItemRandomly(float x, float y)
{
    short itemNames[]={
        OBJ_BRACELETOF_HEALTH, OBJ_FEAR,
        OBJ_ABILITY_BOOK,OBJ_CIDER,
        OBJ_AMULETOF_COMBAT,OBJ_AMULETOF_MANIPULATION,
        OBJ_FOIL_CANDLE_LIT,OBJ_CANDLE_1,OBJ_CURE_POISON_POTION,
    };
    return CreateEffectiveItem(itemNames[Random(0, sizeof(itemNames)-1)], x,y);
}

void CreateEffectiveItemUnitPos(int pos)
{
    float x=GetObjectX(pos),y=GetObjectY(pos);

    Delete(pos);
    CreateEffectiveItemRandomly(x,y);
}

