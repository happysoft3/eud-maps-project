
#include "libs/define.h"
#include "libs/logging.h"

#define GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM 1

#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_1 2
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_2 3
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_3 4
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_4 5
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_5 6
#define GUI_DIALOG_MESSAGE_SPECIAL_WEAPON_6 7

#define GUI_DIALOG_MESSAGE_SUMMON_CARROT 8
#define GUI_DIALOG_MESSAGE_POTION_SHOP 9
#define GUI_DIALOG_MESSAGE_HEALING_AMULET_SHOP 10
#define GUI_DIALOG_MESSAGE_TELEPORT_TO_LAST 11
#define GUI_DIALOG_MESSAGE_TELEPORT_TO_HOME 12

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10

int GetDialogCtxPtr() //virtual
{ }

int GetDialogCtx() //virtual
{ }

int GenericHash() //virtual
{ }

int GetWeaponSpecialProperty(int n) //virtual
{ }

string GetPopupMessage(int index) //virtual
{ }

int GetSpecialWeaponPay(int cur) //virtual
{}

string GetSpecialWeaponName(int cur) //virtual
{}

int GetSpecialWeaponMessage(int cur) //override
{ }

int GetSpecialWeaponFunction(int cur) //virtual
{ }

void SetSpecialWeaponNameArray(string *p, int count) //virtual
{ }

void SetSpecialWeaponPayAndMessage(int *pPay, int *pMessages) //virtual
{ }

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16

int GetUserRespawnMark(int pIndex) //virtual
{ }

void ResetUserRespawnMarkPos(int pIndex)
{
    int mark=GetUserRespawnMark(pIndex);
    int initPos = mark+1;

    if (!ToInt(GetObjectX(initPos)))
    {
        WriteLog("ResetUserRespawnMarkPos::error");
        return;
    }
    MoveObject(mark, GetObjectX(initPos), GetObjectY(initPos));
}



