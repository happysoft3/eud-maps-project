
#include "libs/define.h"
#include "libs/format.h"
#include "libs/indexloop.h"
#include "libs/fxeffect.h"
#include "libs/waypoint.h"
#include "libs/buff.h"
#include "libs/reaction.h"
#include "libs/sound_define.h"
#include "libs/spellutil.h"
#include "libs/playerupdate.h"
#include "libs/coopteam.h"
#include "libs/spellutil.h"
#include "libs/itemproperty.h"
#include "libs/printutil.h"
#include "libs/logging.h"
#include "libs/fixtellstory.h"
#include "libs/weapon_effect.h"
#include "libs/objectIDdefines.h"
#include "libs/potionpickup.h"

#define PLAYER_DEATH_FLAG 0x80000000
#define PLAYER_FLAG_ALL_BUFF 32
#define PLAYER_FLAG_SKILL_1 2
#define PLAYER_FLAG_SKILL_2 4
#define PLAYER_FLAG_SKILL_3 8
#define PLAYER_FLAG_SKILL_4 16
#define PLAYER_FLAG_SKILL_5 64
#define PLAYER_REVIVE_LOCATION 226

#define PLAYER_LAST_TRANSMISSION 36

#define PLAYER_SKILLCOOLDOWN_S 20
#define PLAYER_SKILLCOOLDOWN_R 16
#define PLAYER_SKILLCOOLDOWN_J 20
#define PLAYER_SKILLCOOLDOWN_D 8

int m_monPropertyHash;

int *m_pWeaponUserSpecialPropertyD;
int *m_pWeaponUserSpecialPropertyExecutorD;
int *m_pWeaponUserSpecialPropertyFxCodeD;

int *m_pWeaponUserSpecialProperty5;
int *m_pWeaponUserSpecialPropertyExecutor5;
int *m_pWeaponUserSpecialPropertyFxCode5;

int *m_pWeaponUserSpecialProperty7;
int *m_pWeaponUserSpecialPropertyExecutor7;
int *m_pWeaponUserSpecialPropertyFxCode7;

int *m_pWeaponUserSpecialPropertyB;
int *m_pWeaponUserSpecialPropertyExecutorB;
int *m_pWeaponUserSpecialPropertyFxCodeB;

int *m_pWeaponUserSpecialPropertyA;
int *m_pWeaponUserSpecialPropertyExecutorA;
int *m_pWeaponUserSpecialPropertyFxCodeA;
int m_genericHash;
int *m_pWeaponUserSpecialPropertyE;
int *m_pWeaponUserSpecialPropertyExecutorE;
int *m_pWeaponUserSpecialPropertyFxCodeE;
string *m_specialWeaponNameArray;
int *m_specialWeaponPays;
int *m_specialWeaponFn;

int m_player[10];
int m_pFlag[10];
short m_pDeath[10];
int m_model[10];
int m_block[10];
int m_pCooldown[10];
int m_skillTime1[10];
int m_skillTime2[10];
int m_skillTime3[10];
int m_skillTime4[10];
int m_skillTime5[10];
short m_monProcedure[100];
short m_mobHP[97]; //monster_health
short m_monCount[97]; //monster_count
int STAGE; //getArea
short *m_areaMarks1;
short *m_areaMarks2;
short *m_areaMarks3;
short *m_areaMarks4;

static void initWeaponEffect()
{
    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeE);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyE);
    SpecialWeaponPropertyExecuteScriptCodeGen(TakeShotSpiderWeb, &m_pWeaponUserSpecialPropertyExecutorE);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyE, m_pWeaponUserSpecialPropertyFxCodeE);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyE, m_pWeaponUserSpecialPropertyExecutorE);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_YELLOW_SPARKS, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeA);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyA);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &m_pWeaponUserSpecialPropertyExecutorA);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyA, m_pWeaponUserSpecialPropertyFxCodeA);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyA, m_pWeaponUserSpecialPropertyExecutorA);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeB);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyB);
    SpecialWeaponPropertyExecuteScriptCodeGen(MultipleThrowingStone, &m_pWeaponUserSpecialPropertyExecutorB);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyB, m_pWeaponUserSpecialPropertyFxCodeB);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyB, m_pWeaponUserSpecialPropertyExecutorB);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_THIN_EXPLOSION, SOUND_MagicMissileDetonate, &m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty5);
    SpecialWeaponPropertyExecuteScriptCodeGen(StartFallMeteor, &m_pWeaponUserSpecialPropertyExecutor5);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyFxCode5);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty5, m_pWeaponUserSpecialPropertyExecutor5);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SMOKE_BLAST, SOUND_TrollHurt, &m_pWeaponUserSpecialPropertyFxCode7);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialProperty7);
    SpecialWeaponPropertyExecuteScriptCodeGen(FlatusHammer, &m_pWeaponUserSpecialPropertyExecutor7);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialProperty7, m_pWeaponUserSpecialPropertyFxCode7);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialProperty7, m_pWeaponUserSpecialPropertyExecutor7);

    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_DAMAGE_POOF, SOUND_MetalHitEarth, &m_pWeaponUserSpecialPropertyFxCodeD);
    SpecialWeaponPropertyCreate(&m_pWeaponUserSpecialPropertyD);
    SpecialWeaponPropertyExecuteScriptCodeGen(AutoDetectingTriggered, &m_pWeaponUserSpecialPropertyExecutorD);
    SpecialWeaponPropertySetFXCode(m_pWeaponUserSpecialPropertyD, m_pWeaponUserSpecialPropertyFxCodeD);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponUserSpecialPropertyD, m_pWeaponUserSpecialPropertyExecutorD);
}

static void initSpawnMarker()
{
    //41-66
    short area1[]={248,249,250,
    41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,
    238,239,251,252,253,254,255,256,257,258,373,374,375,376,377,378,379,380,381,382,383,384,385
    ,0};
    //71-122
    short area2[]={71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,
        101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122
        ,0};

    //131-163
    short area3[]={131,132,133,134,135,136,137,138,139,260, 261,262,263,264,140,141,142,143,144,145,146,147,148,149,150,151,
        152,153,154,155
        ,0};

    short area4[]={
        156,157,158,159,160,161,162,163,235,236,237,233,234,265,266,
        242,243,244,245,246,247,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,
        283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,
        308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,
        331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,
        354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372
        ,0};
    m_areaMarks1=area1;
    m_areaMarks2=area2;
    m_areaMarks3=area3;
    m_areaMarks4=area4;
}

void ThisMapInformationPrintMessage(int plrUnit)
{
    if (CurrentHealth(plrUnit))
    {
        UniPrint(plrUnit, "이 지도는 \"잔도 키우기\" 입니다. 플레이 해주셔서 고맙고요 - 제작자. 237");
        UniPrint(plrUnit, "아무튼 \"잔도 키우기\". 열심히 우리의 잔도를 키워보아요~~ 좋은 아이템 많이 넣어놓았음^^");
        UniPrint(plrUnit, "잔도키우기V1.0 - 버그제보는 blog.naver.com/ky10613 으로 하세요");
    }
}

int NoncrushAll(int unit)
{
    int inv = GetLastItem(unit), count = 0;

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

#define REPAIR_PAY 3000

static void tradeRepair()
{
    int result = GetAnswer(SELF);

    if (result==1)
    {
        if (GetGold(OTHER)>=REPAIR_PAY)
        {
            NoncrushAll(OTHER);
            ChangeGold(OTHER, -REPAIR_PAY);
        }
        else
        {
            UniPrint(OTHER, "이런, 금화가 부족해요");
        }
        
    }
}

static void descRepair()
{
    UniPrint(OTHER, "당신이 가진 모든 아이템을 파괴불가하게 만드실래요? 3,000골드 필요");
    TellStoryUnitName("AA", "bindevent:NullEvent", "아이템파괴불능");
}

static void placeRepairShop(int locationId)
{
    int s=DummyUnitCreateAt("Swordsman", LocationX(locationId), LocationY(locationId));

    SetDialog(s, "YESNO", descRepair, tradeRepair);
}


int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}

int CheckPlayerWithId(int pUnit)
{
    int rep=sizeof(m_player);

    while (--rep>=0)
    {
        if (m_player[rep]^pUnit)
            continue;       
        break;
    }
    return rep;
}

int CheckPlayer()
{
    int rep = sizeof(m_player);

    while(--rep>=0)
    {
        if (IsCaller(m_player[rep]))
            break;
    }
    return rep;
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

static void reportWarAbilityCooldown(int user, short abilityId, char cop)
{
    char code[]={
         0x56, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0xF0, 0xB8, 
         0x00, 0x81, 0x4D, 0x00, 0xFF, 0x36, 0xFF, 0x76, 0x04, 0xFF, 0x76, 0x08, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5E, 0x31, 0xC0, 0xC3 };

    // int args[]={0, abilityId, UnitToPtr(user)};
    int args[]={cop, abilityId, UnitToPtr(user)};
    int *pExec=0x5c336c;
		int *pOld=pExec[0];
		pExec[0]= code;
		Unknownb8(args);
		pExec[0]=pOld;
}

static void setAbilityCooldown(int user, int abilityId, int cooldown)
{
    SpellUtilSetPlayerAbilityCooldown(user, abilityId, cooldown);
    reportWarAbilityCooldown(user, abilityId, cooldown==0);
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4;
		arr[53] = 1128792064; arr[54] = 4; 
		link=arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1852796743;
		arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link=arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146;
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link=arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 270; 
		arr[19] = 55; arr[21] = 1065353216; arr[24] = 1071225242; arr[26] = 4; arr[28] = 1101004800; 
		arr[29] = 15; arr[31] = 3; arr[32] = 5; arr[33] = 10; arr[59] = 5542784; 
		arr[60] = 1388; arr[61] = 46915072; 
		link=arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;		
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link=arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064; 
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link=arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link=arr;
	}
	return link;
}

int WoundedApprenticeBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		
		arr[17] = 50; arr[18] = 10; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[28] = 1112014848; arr[29] = 40; 
		arr[30] = 1106247680; arr[31] = 2; arr[32] = 12; arr[33] = 20;arr[57] = 5548112;
        arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int NecromancerBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919116622; arr[1] = 1851878767; arr[2] = 7497059; arr[24] = 1065353216; arr[25] = 1; 
		arr[26] = 2; arr[28] = 1103626240; arr[29] = 30; arr[30] = 1092616192; arr[31] = 11; 
		arr[32] = 7; arr[33] = 15; arr[34] = 1; arr[35] = 1; arr[36] = 10; 
		arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int LichLord2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; 
		arr[27] = 7; arr[28] = 1108082688; arr[29] = 50; arr[30] = 1106247680; arr[31] = 11; 
		arr[32] = 13; arr[33] = 21; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int HecubahWithOrbBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 250; 
		arr[18] = 100; arr[19] = 90; arr[21] = 1065353216; arr[24] = 1066192077; arr[25] = 1; 
		arr[26] = 6; arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; 
		arr[40] = 1852140903; arr[41] = 116; arr[53] = 1133903872; arr[55] = 19; arr[56] = 25; 
		link=arr;
	}
	return link;
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

void MonsterWoundedApprentice(int unit)
{
    //TODO: Index. 9, ThingName= WoundedApprentice
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedApprenticeBinTable());
        SetUnitMaxHealth(unit, 230);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}

void MonsterClassNecromancer(int sUnit)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, NecromancerBinTable());
        SetUnitMaxHealth(sUnit, 325);
        RetreatLevel(sUnit, 0.0);
        ResumeLevel(sUnit, 1.0);
        SetUnitStatus(sUnit, GetUnitStatus(sUnit) ^ 0x20);
    }
}

void MonsterClassLichLord(int sUnit)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichLord2BinTable());
        SetUnitMaxHealth(sUnit, 400);
        RetreatLevel(sUnit, 0.0);
        ResumeLevel(sUnit, 1.0);
        SetUnitStatus(sUnit, GetUnitStatus(sUnit) ^ 0x20);
    }
}

void MonsterClassOrbHecubah(int sUnit)
{
    int ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahWithOrbBinTable());
        SetUnitMaxHealth(sUnit, 325);
        RetreatLevel(sUnit, 0.0);
        ResumeLevel(sUnit, 1.0);
        SetUnitStatus(sUnit, GetUnitStatus(sUnit) ^ 0x10000);
    }
}

static void onUnitGhostImageLoop(int img)
{
    int owner=GetOwner(img);

    if (owner)
    {
        if (CurrentHealth(owner))
        {
            if (ToInt( DistanceUnitToUnit(img, owner)) )
                MoveObject(img, GetObjectX(owner), GetObjectY(owner));
            FrameTimerWithArg(1, img, onUnitGhostImageLoop);
            return;
        }
        Delete(img);
    }
}

static void unitGhost(int unit)
{
    int img=CreateObjectById(OBJ_SMALL_FIST, GetObjectX(unit), GetObjectY(unit));

    FrameTimerWithArg(1, img, onUnitGhostImageLoop);
    UnitNoCollide(img);
    Frozen(img, TRUE);
    SetOwner(unit, img);
}

static void initMonsterProcedure()
{
    m_monProcedure[5]=MonsterGoonProcess;
    m_monProcedure[72]=MonsterStrongWhiteWizProcess;
    m_monProcedure[30]=MonsterWeirdlingBeastProcess;
    m_monProcedure[34]=MonsterBlackWidowProcess;
    m_monProcedure[6]=MonsterBear2Process;
    m_monProcedure[12]=MonsterFireSpriteProcess;
    m_monProcedure[73]=MonsterWizardRedProcess;
    m_monProcedure[29]=MonsterAirshipCaptainProcess;
    m_monProcedure[39]=MonsterWoundedApprentice;
    m_monProcedure[33]=MonsterClassNecromancer;
    m_monProcedure[80]=MonsterClassLichLord;
    m_monProcedure[26]=MonsterClassOrbHecubah;
    initMonStruct();

    HashCreateInstance(&m_monPropertyHash);
    HashPushback(m_monPropertyHash, OBJ_WASP, UnitSpeedSlow);
    HashPushback(m_monPropertyHash, OBJ_SMALL_ALBINO_SPIDER, UnitSpeedSlow);
    HashPushback(m_monPropertyHash, OBJ_SMALL_SPIDER, UnitSpeedSlow);
    HashPushback(m_monPropertyHash, OBJ_IMP, UnitTheImp);
    HashPushback(m_monPropertyHash, OBJ_WIZARD, UnitDoesntTeleport);
    HashPushback(m_monPropertyHash, OBJ_OGRE_WARLORD, UnitVampProperty);
    HashPushback(m_monPropertyHash, OBJ_SKELETON_LORD, UnitVampProperty);
    HashPushback(m_monPropertyHash, OBJ_GHOST, unitGhost);
    HashPushback(m_monPropertyHash, OBJ_CARNIVOROUS_PLANT, onPlantProperty);
}

void CheckMonsterThing(int unit)
{
    int key = GetUnitThingID(unit) % 97;

    if (m_monProcedure[key])
        CallFunctionWithArg(m_monProcedure[key], unit);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void MapInitialize()
{
    MusicEvent();
    HashCreateInstance(&m_genericHash);
    GetMaster();
    initMonsterProcedure();
    CreateLogFile("protoss-log.txt");
    STAGE = 1;
    initializeMapsetting();
    initSpawnMarker();

    //loop_plugin
    FrameTimer(1, PlayerClassOnLoop);

    //delay_run
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(1, DelayInit);
}

int DrawSpellIcon(float x, float y)
{
    int unit = CreateObjectAt("AirshipBasketShadow", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, 1416);
    return unit;
}

int DrawAbilityIcon(float x, float y)
{
    int unit = CreateObjectAt("AirshipBasketShadow", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, 1417);
    return unit;
}

#define SPECIAL_WEAPON_MAX_COUNT 7

static void initspecialWeapon()
{
    string desc[]={"망각의 지팡이", "거미줄 모닝스타", "라이트닝서드", "멀티스톤서드",
    "운석서드", "방귀망치", "데스레이서드"};
    int pay[]={30000, 30000, 30000, 30000,30000,20000,38000};

    m_specialWeaponNameArray = desc;
    m_specialWeaponPays=pay;
    int specialFn[]={SummonOblivionStaff, DispositionSpiderSpitSword, DispositionThunderSword,DispositionMultipleThrowingStone,
    DispositionUserMeteorSword, DispositionFlatusHammer, DispositionAutodetectSword};

    m_specialWeaponFn=specialFn;
}

void DelayInit()
{
    initspecialWeapon();
    initWeaponEffect();
    DrawSpellIcon(LocationX(213), LocationY(213));
    TeleportLocationVector(213, 23.0, -23.0);
    DrawAbilityIcon(LocationX(213), LocationY(213));

    PlaceSpecialWeaponShop(227);
    placeRepairShop(259);
    FrameTimer(30, putBaseItems);
    FrameTimerWithArg(150, m_areaMarks1, spawnMonsterArea);
    FrameTimer(3, MapSignInit);
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 190);
        Frozen(unit, 1);
    }
    return unit;
}

void getTest()
{
    MoveObject(OTHER, GetWaypointX(189), GetWaypointY(189));
}

void initializeMapsetting()
{
    int k = 0;
    int ptr;

    CreateObject("InvisibleLightBlueHigh", 37);
    for (k = 6 ; k >= 0 ; k --)
    {
        CreateObject("PlayerWaypoint", 37);
        ptr = CreateObject("MovableStatueVictory4SW", 38);
        Enchant(ptr, "ENCHANT_FREEZE", 0.0);
        CreateObject("InvisibleLightBlueHigh", 38);
        MoveWaypoint(38, GetWaypointX(38) + 47.0, GetWaypointY(38) + 47.0);
        ptr = CreateObject("MovableStatueVictory4NE", 39);
        Enchant(ptr, "ENCHANT_FREEZE", 0.0);
        CreateObject("InvisibleLightBlueHigh", 39);
        MoveWaypoint(39, GetWaypointX(39) + 47.0, GetWaypointY(39) + 47.0);
    }
}

static void PlayerSetAllBuff(int user)
{
    UnitSetEnchantTime(user, ENCHANT_VAMPIRISM, 0);
    UnitSetEnchantTime(user, ENCHANT_HASTED, 0);
}

void PlayerClassOnJoin(int plr)
{
    int user = m_player[plr];

    if (!CurrentHealth(user))
        return;

    WriteLog("PlayerClassOnJoin");
    
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
        PlayerSetAllBuff(user);
    onRespawnUser(plr, user);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
    WriteLog("PlayerClassOnJoin - end");
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(67), LocationY(67));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int plr)
{
    MoveObject(user, LocationX(474), LocationY(474));
    Enchant(user, "ENCHANT_INVULNERABLE", 3.0);
    FrameTimerWithArg(3, plr, PlayerClassOnJoin);
    UniPrint(user, "초기 유저입니다, 입장을 시도하고 있습니다, 기다려주세요...");
}

static void doInitialUser(int plr, int user)
{
    int model =CreateObjectAt("AirshipCaptain", GetObjectX(user), GetObjectY(user));

    UnitNoCollide(model);
    ObjectOff(model);
    int block=CreateObjectAt("Rock6", GetObjectX(user), GetObjectY(user));
    SetUnitFlags(block, GetUnitFlags(block)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    m_model[plr]=model;
    m_block[plr]=block;
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

int PlayerClassOnInit(int plr, int pUnit, char *userInit)
{
    m_player[plr]=pUnit;
    m_pFlag[plr]=1;
    m_pDeath[plr]=0;
    m_pCooldown[plr]=0;
    m_skillTime1[plr]=0;
    m_skillTime2[plr]=0;
    m_skillTime3[plr]=0;
    m_skillTime4[plr]=0;
    m_skillTime5[plr]=0;

    // if (ValidPlayerCheck(pUnit))
    // {
    //     if (GetHost() ^ pUnit)
    //         NetworkUtilClientEntry(pUnit);
    //     else
    //         ClientsideProcess();
    //     // FrameTimerWithArg(60, pUnit, NetPlayCustomBgm);
    //     userInit[0] = TRUE;
    // }
    // Enchant(pUnit, "ENCHANT_BLINDED", 0.8);
    doInitialUser(plr, pUnit);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));
    EmptyAll(pUnit);
    return plr;
}

void PlayerClassOnEntry(int plrUnit)
{
    while (TRUE)
    {
        if (!CurrentHealth(plrUnit))
            break;
        int plr = CheckPlayerWithId(plrUnit), rep = sizeof(m_player);
        char initialUser=FALSE;

        while (--rep>=0&&plr<0)
        {
            if (!MaxHealth(m_player[rep]))
            {
                WriteLog("do playerclassoninit");
                plr = PlayerClassOnInit(rep, plrUnit, &initialUser);
                FrameTimerWithArg(66, plrUnit, ThisMapInformationPrintMessage);
                break;
            }
        }
        if (plr >= 0)
        {
            if (initialUser)
                OnPlayerDeferredJoin(plrUnit, plr);
            else
                PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(plrUnit);
        break;
    }
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (CheckPlayer() >= 0)
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(67), LocationY(67));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

void PlayerClassOnShutdown(int plr)
{
    if (MaxHealth(m_model[plr]))
        Delete(m_model[plr]);
    Delete(m_block[plr]);
    m_player[plr]=0;
    m_pDeath[plr]=0;
    m_pFlag[plr]=0;
}

void PlayerClassOnDeath(int rep, int user)
{
    char dieMsg[128];
    int params[]={StringUtilGetScriptStringPtr(PlayerIngameNick(user)), ++m_pDeath[rep]};

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다 (%d번째 사망)", params, sizeof(params));
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

static void reducePlayerSpeed(int user)
{
    if (!(GetPlayerAction(user) ^ 0x5))
        PushObjectTo(user, UnitAngleCos(user, -2.0), UnitAngleSin(user, -2.0));
}

static void userModelHandler(int plr, int user)
{
    int model=m_model[plr];

    if (!ToInt(Distance(GetObjectX(user), GetObjectY(user) + 4.0, GetObjectX(model), GetObjectY(model))))
    {
        //motion_stop
        if (UnitCheckEnchant(model, GetLShift(ENCHANT_VILLAIN)))
        {
            PauseObject(model, 1);
            EnchantOff(model, "ENCHANT_VILLAIN");
        }
        return;
    }
    Frozen(model, FALSE);
    Enchant(model, "ENCHANT_VILLAIN", 0.0);
    if (!UnitCheckEnchant(model, GetLShift(ENCHANT_DETECTING)))
    {
        PlaySoundAround(model, SOUND_EmberDemonMove);
        Enchant(model, "ENCHANT_DETECTING", 0.4);
    }
    Frozen(model, TRUE);
    LookWithAngle(model, GetDirection(user));
    Walk(model, GetObjectX(model), GetObjectY(model));
    MoveObject(model, GetObjectX(user), GetObjectY(user) + 4.0);
}

int WarAbilityTable(int aSlot, int pIndex)
{
    int value=GetMemory(0x753600 + (pIndex * 24) + (aSlot * 4));
    short *p=&value;

    if (p[1])
        return 0;
    return p[0];
}

void WarAbilityUse(int pUnit, int aSlot, int actionFunction)
{
    int chk[160], pIndex = GetPlayerIndex(pUnit), cTime;
    int arrPic;

    if (!(pIndex >> 0x10))
    {
        arrPic = (pIndex * 5 + aSlot)-1; //EyeOf=5, harpoon=3, sneak=4, berserker=1
        cTime = WarAbilityTable(aSlot, pIndex);
        if (cTime ^ chk[arrPic])
        {
            if (!chk[arrPic])
            {
                CallFunctionWithArg(actionFunction, pUnit);
            }
            chk[arrPic] = cTime;
        }
    }
}

static void userSkillHandler(int plr, int user)
{
    WarAbilityUse(user, 5, castSkillTypeS);
    WarAbilityUse(user, 4, castSkillTypeR);
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_SKILL_4))
        WarAbilityUse(user, 2, castSkillTypeJ);
}

static void userSkillHandler222(int plr, int flag, int id, int *pCool)
{
    int cool=0x0fffff;
    if (PlayerClassCheckFlag(plr, flag))
    {
        if (pCool[plr]<0)
            return;
        if (!pCool[plr])
            cool=0;
        --pCool[plr];
    }
    setAbilityCooldown(m_player[plr], id, cool);
}

static void playerSetcooldown(int plr, int user)
{
    int c=m_pCooldown[plr];

    if (c)
    {
        m_pCooldown[plr]=c-1;
        return;
    }
    m_pCooldown[plr]=30;
    userSkillHandler222(plr, PLAYER_FLAG_SKILL_1, 5, m_skillTime1);
    userSkillHandler222(plr, PLAYER_FLAG_SKILL_2, 3, m_skillTime2);
    userSkillHandler222(plr, PLAYER_FLAG_SKILL_3, 4, m_skillTime3);
    userSkillHandler222(plr, PLAYER_FLAG_SKILL_4, 2, m_skillTime4);
    userSkillHandler222(plr, PLAYER_FLAG_SKILL_5, 1, m_skillTime5);
    // setAbilityCooldown(user, 1, 0x808080);
}

static void checkUserbook( int user)
{
    int item=GetLastItem(user);

    if (GetUnitThingID(item)==2676)
    {
        onBookDetected(item, user);
        Delete(item);
    }
    
}

void PlayerClassOnAlive(int plr, int user)
{
    checkUserbook(user);
    playerSetcooldown(plr, user);
    reducePlayerSpeed(user);
    userModelHandler(plr, user);
    userSkillHandler(plr, user);
}

void PlayerClassOnLoop()
{
    int rep = sizeof(m_player);

    while (--rep>=0)
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[rep]))
            {
                if (GetUnitFlags(m_player[rep]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[rep]))
                {
                    PlayerClassOnAlive(rep, m_player[rep]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(rep, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(rep, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(rep, m_player[rep]);
                    }
                    break;
                }                
            }
            if (m_pFlag[rep])
                PlayerClassOnShutdown(rep);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

static void onAwardMagic(int plr, int user, int flag, char type)
{
    if (PlayerClassCheckFlag(plr, flag))
    {
        PlaySoundAround(user, SOUND_NoCanDo);
        UniPrint(user, "당신에게 이미 그 능력이 있어요");
        return;
    }
    PlayerClassSetFlag(plr, flag);
    GreenSparkFxAt(GetObjectX(user), GetObjectY(user));
    PlaySoundAround(user, SOUND_AwardSpell);
    // setAbilityCooldown(user, type, 0);
    UniPrint(user, "새 능력을 가지셨습니다");
}

static void onBookDetected(int cur, int owner)
{
    int ptr = UnitToPtr(cur);
    char type = GetMemory(GetMemory(ptr + 0x2e0));
    int plr =CheckPlayerWithId(owner);

    if (type == 5)      //TODO: ABILITY_EYE_OF_THE_WOLF
        onAwardMagic(plr, owner, PLAYER_FLAG_SKILL_1, type);
    else if (type == 3) //TODO: ABILITY_HARPOON
        onAwardMagic(plr, owner,PLAYER_FLAG_SKILL_2, type);
    else if (type == 4) //TODO: ABILITY_TREAD_LIGHTLY
        onAwardMagic(plr,owner, PLAYER_FLAG_SKILL_3, type);
    else if (type == 2) //TODO: ABILITY_warcry
        onAwardMagic(plr,owner,PLAYER_FLAG_SKILL_4, type);
        else if (type == 1)
        onAwardMagic(plr,owner,PLAYER_FLAG_SKILL_5, type);
}

void ChakramCopyProperies(int src, int dest)
{
    int *srcPtr = UnitToPtr(src), *destPtr = UnitToPtr(dest);
    
    int *srcProperty = srcPtr[173], *destProperty = destPtr[173];

    int rep=-1;

    while (++rep<4)
        destProperty[rep] = srcProperty[rep];

    int rep2=-1;

    while (++rep2<32)
        destPtr[140+rep2] = srcPtr[140+rep2];
}

static void CreateChakram(float xpos, float ypos, int *pDest, int owner)
{
    int mis = CreateObjectAt("RoundChakramInMotion", xpos, ypos);
    int *ptr=UnitToPtr(mis);

    if (pDest)
        pDest[0] = mis;

    ptr[174] = 5158032;
    ptr[186] = 5483536;
    SetOwner(owner, mis);

    int *collideDataMb = MemAlloc(8);
    collideDataMb[0]=0;
    collideDataMb[1] = UnitToPtr(owner);
    ptr[175] = collideDataMb;
}

void OnChakramTracking(int owner, int cur)
{
    int mis;

    CreateChakram(GetObjectX(owner) + UnitAngleCos(owner, 11.0), GetObjectY(owner) + UnitAngleSin(owner, 11.0), &mis, owner);
    ChakramCopyProperies(cur, mis);

    PushObject(mis, 30.0, GetObjectX(owner), GetObjectY(owner));
}

void AfterChakramTracking(int *pMem)
{
    if (!pMem)
        return;

    int inv = pMem[0], owner = pMem[1];

    if (CurrentHealth(owner) && IsObjectOn(inv))
    {
        Pickup(owner, inv);
    }
    FreeSmartMemEx(pMem);
}

static void HookChakrm(int cur, int owner)
{
    // UnitSetEnchantTime(cur, ENCHANT_RUN, 0);
    // Enchant(cur, EnchantList(ENCHANT_ETHEREAL),0.0);

    int *ptr=UnitToPtr(cur);

    if (ptr[186] == 5496000)
    {
        if (!CurrentHealth(owner))
            return;

        int *pMem;

        AllocSmartMemEx(8, &pMem);
        pMem[0] = GetLastItem(cur);
        pMem[1] = owner;
        OnChakramTracking(owner, cur); //이게 여기로 옮겨지고 cur 인자를 더 전달합니다
        Delete(cur);
        FrameTimerWithArg(2, pMem, AfterChakramTracking);
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 526, castSkillTypeD);
    HashPushback(pInstance, 1177, HookChakrm);
}

static void onRespawnUser(int plr, int pUnit)
{
    Enchant(pUnit, "ENCHANT_AFRAID", 0.0);
    // Enchant(pUnit, "ENCHANT_HASTED", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    MoveObject(pUnit, LocationX(PLAYER_REVIVE_LOCATION), LocationY(PLAYER_REVIVE_LOCATION));
    Effect("TELEPORT", LocationX(PLAYER_REVIVE_LOCATION), LocationY(PLAYER_REVIVE_LOCATION), 0.0, 0.0);
    PlaySoundAround(pUnit, SOUND_BlindOff);
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

static void onConnectionCollide()
{
    if (CurrentHealth(OTHER))
    {
        int dpoint=GetTrigger()+1;

        if (ToInt(GetObjectX(dpoint)))
        {
            Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            MoveObject(OTHER, GetObjectX(dpoint), GetObjectY(dpoint));
            Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            PlaySoundAround(OTHER, SOUND_BlindOff);
        }
    }
}

static void setupConnection(int srcLocation, int destLocation)
{
    int t=DummyUnitCreateAt("AirshipCaptain", LocationX(srcLocation), LocationY(srcLocation));

    CreateObjectAt("InvisibleLightBlueLow", LocationX(destLocation), LocationY(destLocation));
    SetCallback(t, 9, onConnectionCollide);
}

void StrikeFlameCollide()
{
    while (TRUE)
    {
        if (!GetTrigger())
            break;
        int owner = GetOwner(SELF);

        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 150, DAMAGE_TYPE_EXPLOSION);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.2);
        }
        break;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

int BurningZombieFx(float xProfile, float yProfile)
{
    int fx;

    CreateObjectAtEx("Zombie", xProfile, yProfile, &fx);
    UnitNoCollide(fx);
    ObjectOff(fx);
    Damage(fx, 0, 999, DAMAGE_TYPE_FLAME);
    return fx;
}

#define STRIKE_FLAME_MARK_UNIT 0
#define STRIKE_FLAME_XVECT 1
#define STRIKE_FLAME_YVECT 2
#define STRIKE_FLAME_COUNT 3
#define STRIKE_FLAME_OWNER 4
#define STRIKE_FLAME_MAX 5

void StrikeFlameLoop(int *pArgs)
{
    int owner=pArgs[STRIKE_FLAME_OWNER];
    while (TRUE)
    {
        if (CurrentHealth(owner))
        {
            int *count=ArrayRefN(pArgs, STRIKE_FLAME_COUNT), mark = pArgs[STRIKE_FLAME_MARK_UNIT];

            if (count[0] && IsVisibleTo(mark, mark+1))
            {
                FrameTimerWithArg(1, pArgs, StrikeFlameLoop);
                float xvect = pArgs[STRIKE_FLAME_XVECT], yvect=pArgs[STRIKE_FLAME_YVECT];

                MoveObjectVector(mark, xvect, yvect);
                MoveObjectVector(mark+1, xvect, yvect);
                int dum=DummyUnitCreateAt("Demon", GetObjectX(mark), GetObjectY(mark));

                SetCallback(dum, 9, StrikeFlameCollide);
                SetOwner(owner, dum);
                DeleteObjectTimer(dum, 1);
                DeleteObjectTimer( BurningZombieFx(GetObjectX(mark), GetObjectY(mark)), 12);
                
                --count[0];
                break;
            }
        }
        Delete(pArgs[STRIKE_FLAME_MARK_UNIT]++);
        Delete(pArgs[STRIKE_FLAME_MARK_UNIT]++);
        FreeSmartMemEx(pArgs);
        break;
    }
}

void StrikeFlameTriggered(int caster)
{
    float xvect=UnitAngleCos(caster, 17.0), yvect=UnitAngleSin(caster, 17.0);
    int mark = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) + xvect, GetObjectY(caster) + yvect);
    int visCheck=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) - xvect, GetObjectY(caster) - yvect);

    int *args;

    AllocSmartMemEx(STRIKE_FLAME_MAX*4, &args);
    args[STRIKE_FLAME_MARK_UNIT]=mark;
    args[STRIKE_FLAME_XVECT]=xvect;
    args[STRIKE_FLAME_YVECT]=yvect;
    args[STRIKE_FLAME_COUNT]=30; //count
    args[STRIKE_FLAME_OWNER]=caster;
    FrameTimerWithArg(1, args, StrikeFlameLoop);
}

void castSkillTypeS(int user)
{
    StrikeFlameTriggered(user);
    int plr=CheckPlayerWithId(user);
    m_skillTime1[plr]=PLAYER_SKILLCOOLDOWN_S;
}

void OnIcecrystalCollide()
{
    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(GetTrigger()+1);

            if (!IsAttackedBy(OTHER, owner))
                break;

            Damage(OTHER, owner, 80, DAMAGE_TYPE_ELECTRIC);
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
        }
        else if (GetCaller())
            break;

        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(GetTrigger() + 1);
        Delete(SELF);
        break;
    }
}

int CreateIceCrystal(int owner, float gap, int lifetime)
{
    int cry=CreateObjectAt("GameBall", GetObjectX(owner) + UnitAngleCos(owner, gap), GetObjectY(owner) + UnitAngleSin(owner, gap));

    SetOwner(owner, CreateObjectAt("ImaginaryCaster", GetObjectX(cry), GetObjectY(cry)));
    SetUnitCallbackOnCollide(cry, OnIcecrystalCollide);
    DeleteObjectTimer(cry, lifetime);
    DeleteObjectTimer(cry+1, lifetime + 15);
    return cry;
}

void CUserWeaponShootIceCrystal(int caster)
{
    PushObject(CreateIceCrystal(caster, 19.0, 75), 22.0, GetObjectX(caster), GetObjectY(caster));
    WispDestroyFX(GetObjectX(caster), GetObjectY(caster));
    PlaySoundAround(caster, SOUND_LightningWand);
    PlaySoundAround(caster, SOUND_ElevLOTDDown);
}

void castSkillTypeD(int sCur, int owner)
{
    Delete(sCur);
    CUserWeaponShootIceCrystal(owner);
    // setAbilityCooldown(owner, 3, PLAYER_SKILLCOOLDOWN_D);
    int plr=CheckPlayerWithId(owner);
    m_skillTime2[plr]=PLAYER_SKILLCOOLDOWN_D;
}

static void dropRainSingle(float xpos, float ypos)
{
    int angle = Random(0, 359);
        float    rgap=RandomFloat(10.0, 200.0);
        int    arw= CreateObjectAt("CherubArrow", xpos+MathSine(angle+90, rgap),ypos+MathSine(angle,rgap));
            LookWithAngle(arw, 64);
            UnitNoCollide(arw);
            Raise(arw, 250.0);
            DeleteObjectTimer(arw, 20);
            PlaySoundAround(arw, SOUND_CrossBowShoot);
}

static void arrowRainLoop(int sub)
{
    float xpos=GetObjectX(sub),ypos=GetObjectY(sub);
    int dur;

    while (ToInt(xpos))
    {
        dur=GetDirection(sub);
        if (dur)
        {
            dropRainSingle(xpos, ypos);
            if (dur&1)                
                SplashDamageAt(GetOwner(sub), 8, xpos, ypos, 200.0);
            FrameTimerWithArg(1, sub, arrowRainLoop);
            LookWithAngle(sub, dur-1);
            break;
        }
        WispDestroyFX(xpos, ypos);
        Delete(sub);
        break;
    }
}

static int CheckCoorValidate(float x, float y)
{
    if (x<100.0)
        return FALSE;
    if (y<100.0)
        return FALSE;
    if (x>5750.0)
        return FALSE;
    if (y>5750.0)
        return FALSE;
    return TRUE;
}

static void startArrowRain(int caster)
{
    int ix,iy;

    if(! GetPlayerMouseXY(caster, &ix,&iy))
        return;
    
    float x=IntToFloat(ix), y=IntToFloat(iy);

    if (!CheckCoorValidate(x,y))
        return;

    int sub=CreateObjectAt("PlayerWaypoint", x,y);

    FrameTimerWithArg(1, sub, arrowRainLoop);
    SetOwner(caster, sub);
    LookWithAngle(sub, 60);
    PlaySoundAround(sub, SOUND_MetallicBong);
}

void castSkillTypeR(int user)
{
    RemoveTreadLightly(user);
    startArrowRain(user);
    int plr=CheckPlayerWithId(user);
    m_skillTime3[plr]=PLAYER_SKILLCOOLDOWN_R;
}

static void onyellowRushCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, GetOwner(SELF)))
        {
            int hash = GetUnit1C(SELF);

            if (!HashGet(hash, GetCaller(), NULLPTR, FALSE))
            {
                Damage(OTHER, GetOwner(SELF), 225, DAMAGE_TYPE_CRUSH);
                if (CurrentHealth(OTHER))
                    HashPushback(hash, GetCaller(), TRUE);
            }
        }
    }
}

static void onyellowRushHandler(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int dur = GetDirection(sub), owner=GetOwner(sub), hash=GetUnit1C(sub);
        if (dur && CurrentHealth(owner))
        {
            float x=GetObjectX(owner),y=GetObjectY(owner);
            float xvect=GetObjectZ(sub), yvect=GetObjectZ(sub+1);
            if (IsVisibleTo(sub, sub+1))
            {
                LookWithAngle(sub, dur-1);
                FrameTimerWithArg(1, sub, onyellowRushHandler);
                MoveObject(sub, x-xvect, y-yvect);
                MoveObject(sub+1, x+xvect, y+yvect);
                int dam=DummyUnitCreateAt("Demon", x-(xvect/14.0), y-(yvect/14.0));

                SetOwner(owner, dam);
                SetUnit1C(dam, hash);
                SetCallback(dam, 9, onyellowRushCollide);
                DeleteObjectTimer(dam, 1);
                Effect("YELLOW_SPARKS", x,y,0.0,0.0);
                return;
            }
        }
        HashDeleteInstance(hash);
        Delete(sub++);
        Delete(sub++);
    }
}

void StartYellowRush(int caster)
{
    float xvect=UnitAngleCos(caster, 14.0), yvect=UnitAngleSin(caster, 14.0);
    int sub=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster)-xvect, GetObjectY(caster)-yvect);

    Raise( CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster)+xvect, GetObjectY(caster)+yvect), yvect);
    Raise(sub, xvect);
    LookWithAngle(sub, 21);
    SetOwner(caster, sub);
    int hash;
    HashCreateInstance(&hash);
    SetUnit1C(sub, hash);
    PlaySoundAround(sub, SOUND_SummonAbort);
    FrameTimerWithArg(1, sub, onyellowRushHandler);
}

void castSkillTypeJ(int user)
{
    StartYellowRush(user);
    int plr=CheckPlayerWithId(user);
    m_skillTime4[plr]=PLAYER_SKILLCOOLDOWN_J;
}

void goLastTransmission()
{
    MoveObject(OTHER, LocationX(PLAYER_LAST_TRANSMISSION), LocationY(PLAYER_LAST_TRANSMISSION));
    Effect("COUNTERSPELL_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    DeleteObjectTimer(CreateObjectAt("blueRain", GetObjectX(OTHER), GetObjectY(OTHER)), 10);
    PlaySoundAround(OTHER, SOUND_BlindOff);
}

int GetUnitTopParent(int unit)
{
    int next;

    while (TRUE)
    {
        next = GetOwner(unit);
        if (next)
            unit = next;
        else
            break;
    }
    return unit;
}

int GetKillCreditTopParent()
{
    int *victim = GetMemory(0x979724);

    if (victim != NULLPTR)
    {
        int *attacker = victim[130];

        if (attacker != NULLPTR)
            return GetUnitTopParent(attacker[11]);
    }
    return NULLPTR;
}

int checkWaypointLimitLine(int wp)
{
    float var_0 = GetWaypointX(wp);
    float var_1 = GetWaypointY(wp);

    if (var_0 > 100.0 && var_1 > 100.0 && var_0 < 5532.0 && var_1 < 5532.0)
        return 1;
    else
        return 0;
}

void SetDeaths()
{
    int kill = GetKillCreditTopParent();

    if (IsPlayerUnit(kill))
        ChangeGold(kill, Random(120 * STAGE, 600 + (350 * (STAGE - 1))));
    DeleteObjectTimer(SELF, 90);
}

int MobType(int n)
{
    int name[] = {
        //Stage. 1
            OBJ_WASP, OBJ_BAT, OBJ_GIANT_LEECH, OBJ_SMALL_ALBINO_SPIDER, OBJ_SMALL_SPIDER,
            OBJ_WHITE_WOLF, OBJ_SWORDSMAN, OBJ_ARCHER, OBJ_WEIRDLING_BEAST, OBJ_BLACK_BEAR,
            
            OBJ_BEAR_2, OBJ_BLACK_WIDOW, OBJ_GOON, OBJ_GRUNT_AXE, OBJ_OGRE_BRUTE,
            OBJ_OGRE_WARLORD, OBJ_SCORPION, OBJ_SHADE, OBJ_IMP, OBJ_SKELETON,

            OBJ_TROLL, OBJ_GOON, OBJ_OGRE_WARLORD, OBJ_SCORPION, OBJ_BLACK_WIDOW,
            OBJ_CARNIVOROUS_PLANT, OBJ_WILL_O_WISP, OBJ_MIMIC, OBJ_FLYING_GOLEM,
            
            OBJ_EVIL_CHERUB, OBJ_SKELETON_LORD, OBJ_VILE_ZOMBIE, OBJ_LICH_LORD, OBJ_WIZARD,
            OBJ_BLACK_WOLF, OBJ_NECROMANCER, OBJ_EMBER_DEMON, OBJ_GHOST,
        };
    return name[n];
}

static void setMonsterStruct(short id, short hp, short count)
{
    m_mobHP[id%97]=hp;
    m_monCount[id%97]=count;
}

static void initMonStruct()
{
    setMonsterStruct(OBJ_WASP, 50, 6);
    setMonsterStruct(OBJ_BAT, 70, 6);
    setMonsterStruct(OBJ_GIANT_LEECH, 90, 5);
    setMonsterStruct(OBJ_SMALL_ALBINO_SPIDER, 75, 5);
    setMonsterStruct(OBJ_SMALL_SPIDER, 55, 5);
    setMonsterStruct(OBJ_WHITE_WOLF, 110, 2);
    setMonsterStruct(OBJ_SWORDSMAN, 150, 2);
    setMonsterStruct(OBJ_ARCHER, 85, 3);
    setMonsterStruct(OBJ_WEIRDLING_BEAST, 108, 4);

    setMonsterStruct(OBJ_BLACK_BEAR, 306, 2);
    setMonsterStruct(OBJ_BEAR_2, 225, 3);
    setMonsterStruct(OBJ_BLACK_WIDOW, 160, 3);
    setMonsterStruct(OBJ_GOON, 98, 3);
    setMonsterStruct(OBJ_GRUNT_AXE, 225, 3);
    setMonsterStruct(OBJ_OGRE_BRUTE, 306, 2);
    setMonsterStruct(OBJ_OGRE_WARLORD, 325, 1);
    setMonsterStruct(OBJ_SCORPION, 240, 2);
    setMonsterStruct(OBJ_SHADE, 128, 3);
    setMonsterStruct(OBJ_IMP, 96, 4);

    setMonsterStruct(OBJ_SKELETON, 225, 5);
    setMonsterStruct(OBJ_TROLL, 380, 1);
    setMonsterStruct(OBJ_CARNIVOROUS_PLANT, 425, 1);
    setMonsterStruct(OBJ_WILL_O_WISP, 260, 2);
    setMonsterStruct(OBJ_MIMIC, 480, 1);
    setMonsterStruct(OBJ_FLYING_GOLEM, 480, 5);

    setMonsterStruct(OBJ_EVIL_CHERUB, 130, 6);
    setMonsterStruct(OBJ_SKELETON_LORD, 308, 4);
    setMonsterStruct(OBJ_VILE_ZOMBIE, 325, 3);
    setMonsterStruct(OBJ_LICH_LORD, 360, 1);
    setMonsterStruct(OBJ_WIZARD, 225, 2);
    setMonsterStruct(OBJ_BLACK_WOLF, 295, 4);
    setMonsterStruct(OBJ_NECROMANCER, 338, 2);
    setMonsterStruct(OBJ_EMBER_DEMON, 225, 1);
    setMonsterStruct(OBJ_GHOST, 165, 3);
}

void CheckUnitSpecialFlag(int unit)
{
    int fn;

    if (HashGet(m_monPropertyHash, GetUnitThingID(unit), &fn, FALSE))
        CallFunctionWithArg(fn, unit);
}
void spawnMonsterArea(short *pLocations)
{
    int pic, unit;

    if (pLocations[0])
    {
        if (STAGE == 1)
            pic = Random(0, 7);
        else if (STAGE == 2)
            pic = Random(8, 18);
        else if (STAGE == 3)
            pic = Random(19, 28);
        else if (STAGE==4)
            pic=Random(29, 37);

        int type = MobType(pic)%97;
        char buff[128];
        int args[]={pic, pLocations[0]};
        WriteLog("spawnMonsterArea");
        int count = m_monCount[type];
        while (--count>=0)
        {
            WriteLog("spawnMonsterArea-1");
            NoxSprintfString(buff, "spawnMonsterArea. pic-%d, location-%d", args, sizeof(args));
            WriteLog(ReadStringAddressEx(buff));
            unit = CreateObjectById(MobType(pic), LocationX( pLocations[0]), LocationY(pLocations[0]));
            WriteLog("spawnMonsterArea-2");
            CheckMonsterThing(unit);
            WriteLog("spawnMonsterArea-3");
            SetUnitMaxHealth(unit, m_mobHP[type]);
            WriteLog("spawnMonsterArea-4");
            SetCallback(unit, 5, SetDeaths);
            SetOwner(GetMaster(), unit);
            CheckUnitSpecialFlag(unit);
            UnitZeroFleeRange(unit);
            RetreatLevel(unit, 0.0);
            AggressionLevel(unit, 1.0);
            SetUnitScanRange(unit, 400.0);
        }
        FrameTimerWithArg(1, pLocations + 4, spawnMonsterArea);
        WriteLog("spawnMonsterArea-end");
    }
    else
    {
        if (STAGE == 1)
            spawnArea1Boss();
        else if (STAGE == 2)
            spawnArea2Boss();
    }
}

void weaponToFrog()
{
    if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
    {
        Enchant(SELF, "ENCHANT_VILLAIN", 0.8);
        int mis = CreateObjectAt("ThrowingStone", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 17.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 17.0));
        LookAtObject(CreateObjectAt("SpiderSpit", GetObjectX(mis), GetObjectY(mis)), OTHER);
        PlaySoundAround(mis, SOUND_EggBreak);
        SetOwner(SELF, mis);
        SetOwner(SELF, mis + 1);
        PushObjectTo(mis, UnitRatioX(OTHER, SELF, 20.0), UnitRatioY(OTHER, SELF, 20.0));
        PushObjectTo(mis + 1, UnitRatioY(OTHER, SELF, 20.0), UnitRatioY(OTHER, SELF, 20.0));
    }
    CheckResetSight(GetTrigger(), 20);
}

void impWeapon()
{
    if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
    {
        Enchant(SELF, "ENCHANT_VILLAIN", 1.5);
        int mis = CreateObjectAt("PitifulFireball", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 17.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 17.0));
        SetOwner(SELF, mis);
        DeleteObjectTimer(mis, 12);
        PushObjectTo(mis, UnitRatioX(SELF, OTHER, -20.0), UnitRatioY(SELF, OTHER, -20.0));
    }
    CheckResetSight(GetTrigger(), 30);
}

void nullPointer()
{
    //_null_function
}

void playerDamage(int unit, int location, float range, int damage)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(m_player[k]))
        {
            if (Distance(GetObjectX(m_player[k]), GetObjectY(m_player[k]), GetWaypointX(location), GetWaypointY(location)) < range)
                Damage(m_player[k], unit, damage, 14);
        }
    }
}

void spawnArea1Boss()
{
    int boss = CreateObject("Bear", 69);

    Enchant(boss, "ENCHANT_VAMPIRISM", 0.0);
    SetCallback(boss, 5, area1BossDead);
    SetCallback(boss, 3, giveWeaponToBoss1);
    SetUnitMaxHealth(boss, 1200);
    SetOwner(GetMaster(), boss);
    SetUnitScanRange(boss, 400.0);
    FrameTimerWithArg(1, boss, LoopBossEffect);
}

void startArea2()
{
    STAGE = 2;
    WallGroupOpen(1);
    FrameTimerWithArg(30, m_areaMarks2, spawnMonsterArea);
}

void area1BossDead()
{
    DeleteObjectTimer(SELF, 30);
    startArea2();
}

void hitElectronicStone()
{
    int unit = CreateObjectAt("CaveRocksHuge", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 23.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 23.0));
    SetOwner(SELF, unit);
    Enchant(unit, "ENCHANT_SHOCK", 0.0);
    Enchant(unit, "ENCHANT_HASTED", 0.0);
    PushObject(unit, -150.0, GetObjectX(OTHER), GetObjectY(OTHER));
    DeleteObjectTimer(unit, 20);
}

void powerHit()
{
    int step;
    int unit;

    if (!step)
    {
        unit = GetTrigger();
        Enchant(unit, "ENCHANT_FREEZE", 0.0);
        Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
        ObjectOff(unit);
        step ++;
        FrameTimer(35, powerHit);
    }
    else if (step == 1)
    {
        Raise(unit, 180.0);
        step ++;
        FrameTimer(20, powerHit);
    }
    else
    {
        ObjectOn(unit);
        CheckResetSight(unit, 30);
        EnchantOff(unit, "ENCHANT_FREEZE");
        EnchantOff(unit, "ENCHANT_INVULNERABLE");
        Effect("JIGGLE", GetObjectX(unit), GetObjectY(unit), 80.0, 0.0);
        MoveWaypoint(70, GetObjectX(unit), GetObjectY(unit));
        DeleteObjectTimer(CreateObject("BigSmoke", 70), 15);
        AudioEvent("HammerMissing", 70);
        playerDamage(unit, 70, 300.0, 100);
        step = 0;
    }
}

void giveWeaponToBoss1()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_HASTED"))
        {
            Enchant(SELF, "ENCHANT_HASTED", 5.0);
            if (!Random(0, 2))
                powerHit();
            else
                hitElectronicStone();
        }
    }
    CheckResetSight(GetTrigger(), 40);
}

void spawnArea2Boss()
{
    int boss = CreateObject("WizardGreen", 123);
    
    Enchant(boss, "ENCHANT_ANCHORED", 0.0);
    Enchant(boss, "ENCHANT_VAMPIRISM", 0.0);
    SetUnitMaxHealth(boss, 2000);
    SetCallback(boss, 5, area2bossDead);
    SetCallback(boss, 3, giveWeaponToBoss2);
    SetOwner(GetMaster(), boss);
    SetUnitScanRange(boss, 400.0);
}

void startArea3()
{
    STAGE = 3;
    WallGroupOpen(2);
    spawnFinalBoss();
    FrameTimerWithArg(30, m_areaMarks3, spawnMonsterArea);
}

void area2bossDead()
{
    DeleteObjectTimer(SELF, 30);
    startArea3();
}

void giveWeaponToBoss2()
{
    int rnd;
    float time;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_HASTED"))
        {
            rnd = Random(0, 2);
            if (!rnd)
            {
                time = 7.0;
                shotSummoning(GetTrigger());
            }
            else if (rnd == 1)
            {
                time = 8.0;
                spreadFlags(GetTrigger());
            }
            else
            {
                time = 5.0;
                dropRock(GetTrigger());
            }

            Enchant(SELF, "ENCHANT_HASTED", time);
        }
    }
    CheckResetSight(GetTrigger(), 50);
}

void GWizTouched()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            int hash = GetUnit1C(SELF);

            if (!HashGet(hash, GetCaller(), NULLPTR, FALSE))
            {
                Damage(OTHER, GetOwner(SELF), 100, DAMAGE_TYPE_CRUSH);
                if (CurrentHealth(OTHER))
                    HashPushback(hash, GetCaller(), TRUE);
            }
        }
    }
}

void straightSummonCarnivous(int ptr)
{
    int count = GetDirection(ptr);
    int boss = GetOwner(ptr), hash=GetUnit1C(ptr);

    if (CurrentHealth(boss) && count)
    {
        if (IsVisibleTo(ptr, ptr+1))
        {
            FrameTimerWithArg(1, ptr, straightSummonCarnivous);
            LookWithAngle(ptr, count-1);
            MoveObjectVector(ptr, GetObjectZ(ptr+1), GetObjectZ(ptr+2));
            MoveObjectVector(ptr+1, GetObjectZ(ptr+1), GetObjectZ(ptr+2));
            int unit = DummyUnitCreateAt("CarnivorousPlant", GetObjectX(ptr), GetObjectY(ptr));
            Frozen(unit, TRUE);
            DeleteObjectTimer(unit, 1);
            SetUnit1C(unit, hash);
            SetCallback(unit, 9, GWizTouched);
            DeleteObjectTimer(CreateObjectAt("BigSmoke", GetObjectX(unit), GetObjectY(unit)), 7);
            PlaySoundAround(unit, SOUND_FirewalkCast);
            LookWithAngle(ptr, count + 1);
            return;
        }
    }
    HashDeleteInstance(hash);
    Delete(ptr);
    Delete(ptr + 1);
    Delete(ptr + 2);
}

void shotSummoning(int boss)
{
    float xvect=UnitRatioX(OTHER, SELF, 17.0), yvect=UnitRatioY(OTHER, SELF, 17.0);
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(boss)+xvect, GetObjectY(boss)+yvect);

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit)-xvect,GetObjectY(unit)-yvect), xvect);
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), yvect);
    SetOwner(boss, unit);
    LookWithAngle(unit, 18);

    int hash;

    HashCreateInstance(&hash);
    SetUnit1C(unit, hash);
    FrameTimerWithArg(1, unit, straightSummonCarnivous); //here
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

static void onXFlagCollide(){
    if (!GetTrigger())
        return;
    
    if (!GetCaller())
    {
        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(SELF);
        return;
    }

    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(owner, OTHER))
            Damage(OTHER, 0, 9, DAMAGE_TYPE_FLAME);
    }
}

#define SPREAD_FLAG_COUNT 18
void pullFlagToCenter(int *pArr)
{
    int k;

    if (pArr[0])
    {
        for (k = SPREAD_FLAG_COUNT ; k > 0 ; k --)
        {
            if (ToInt(GetObjectX(pArr[k])))
                PushObjectTo(pArr[k], UnitAngleCos(pArr[k], 1.2), UnitAngleSin(pArr[k], 1.2));
        }
        pArr[0]-=1;
        FrameTimerWithArg(1, pArr, pullFlagToCenter);
        return;
    }
    for (k = SPREAD_FLAG_COUNT ; k > 0 ; k --)
        Delete(pArr[k]);
    FreeSmartMemEx(pArr);
}


void spawnFlagAround(int sub)
{
    int boss=GetOwner(sub), *pArr, count;
    string name = "SpikeBrazier";
    float x=GetObjectX(sub),y=GetObjectY(sub);

    if (CurrentHealth(boss))
    {
        AllocSmartMemEx((SPREAD_FLAG_COUNT+1)*4, &pArr);
        pArr[0]=60;
        for (count = SPREAD_FLAG_COUNT ; count ; --count)
        {
            pArr[count]=CreateObjectAt(name, x+MathSine(count*20+90, 180.0), y+MathSine(count*20, 180.0));
            SetOwner(boss, pArr[count]);
            Enchant(pArr[count], "ENCHANT_RUN", 0.0);
            LookAtObject(pArr[count], sub);
            SetUnitCallbackOnCollide(pArr[count], onXFlagCollide);
        }
        FrameTimerWithArg(1, pArr, pullFlagToCenter);
    }
    Delete(sub);
}

void spreadFlags(int boss)
{
    int sub = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));

    PlaySoundAround(sub, SOUND_LightCast);
    SetOwner(boss, sub);
    FrameTimerWithArg(1, sub, spawnFlagAround);
}

static void ondropRockHandler(int sub)
{
    if (ToInt(GetObjectX(sub)) )
    {
        if (GetObjectZ(sub) > 36.0)
        {
            FrameTimerWithArg(1, sub, ondropRockHandler);
            Raise(sub, GetObjectZ(sub) - 18.0);
            return;
        }
        float x=GetObjectX(sub),y=GetObjectY(sub);
        int owner =GetOwner(sub);
        PlaySoundAround(sub, SOUND_HitStoneBreakable);
        Delete(sub);
        SplashDamageAt(GetMaster(), 135, x,y, 200.0);
        // DeleteObjectTimer( CreateObjectAt("BigSmoke", x,y), 9);
        GreenExplosion(x,y);
        Effect("JIGGLE",x,y,60.0,0.0);
    }
}

void dropRock(int boss)
{
    int unit = CreateObjectAt("MonsterGenerator", GetObjectX(OTHER), GetObjectY(OTHER));

    UnitNoCollide(unit);
    ObjectOff(unit);
    Frozen(unit, TRUE);
    Raise(unit, 240.0);
    SetOwner(boss, unit);
    FrameTimerWithArg(1, unit, ondropRockHandler);
}

void RPotionPick()
{
    int ptr = ToInt(GetObjectZ(GetTrigger() + 1));

    LookWithAngle(ptr, GetDirection(ptr) - 1);
    Delete(GetTrigger() + 1);
}

void spawnPotions(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 12)
    {
        AudioEvent("PotionDrop", 167);
        int pot=CreateObjectAt("RedPotion", GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(pot, SOUND_PotionDrop);
        RegistItemPickupCallback(pot, RPotionPick);
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(pot), GetObjectY(ptr)), ptr);
        LookWithAngle(ptr, count + 1);
        PotionPickupRegist(pot);
    }
}

void poisonTalk()
{
    int ptr = GetLastItem(SELF);
    int count = GetDirection(ptr);

    if (count < 12)
    {
        int pot=PotionPickupRegist( CreateObjectAt("CurePoisonPotion", GetObjectX(OTHER), GetObjectY(OTHER)) );
        PlaySoundAround(pot, SOUND_PotionDrop);
        RegistItemPickupCallback(pot, RPotionPick);
        Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER)), ptr);
        LookWithAngle(ptr, count + 1);
    }
}

void healingTalk()
{
    if (MaxHealth(OTHER) - CurrentHealth(OTHER))
    {
        UniChatMessage(SELF, "이런, 다치셨군요 ... 제가 치료해 드리겠습니다 ...", 150);
        Effect("GREATER_HEAL", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(SELF), GetObjectY(SELF));
        RestoreHealth(OTHER, 100);
        PlaySoundAround(SELF, SOUND_GreaterHealEffect);
    }
    spawnPotions(GetLastItem(SELF));
}

void putBaseItems()
{
    int unit[2], ptr, k;
    string weapon = "GreatSword";
    string armor = "PlateArms";

    CreateObject("InvisibleLightBlueHigh", 129);
    CreateObject("InvisibleLightBlueHigh", 130);

    for (k = 9 ; k >= 0 ; k --)
    {
        CreateObject(weapon, 129);
        CreateObject(armor, 130);
        TeleportLocationVector(129, 23.0, -23.0);
        TeleportLocationVector(130, -23.0, 23.0);
    }
    unit[0] = Object("specialMedic");
    unit[1] = Object("PoisonMedic");
    SetDialog(unit[0], "NORMAL", healingTalk, nullPointer);
    SetDialog(unit[1], "NORMAL", poisonTalk, nullPointer);
    ptr = CreateObject("InvisibleLightBlueLow", 1);
    Delete(ptr);
    ptr ++;
    FrameTimerWithArg(1, unit[0], DelayGiveToUnit);
    FrameTimerWithArg(1, CreateObject("ManaCrystalLarge", 129), DelayGiveToUnit);
    FrameTimerWithArg(1, unit[1], DelayGiveToUnit);
    FrameTimerWithArg(1, CreateObject("ManaCrystalLarge", 129), DelayGiveToUnit);
    putWarpGate();
}

void putWarpGate()
{
    int ptr = 
    SpawnHomeBaseTeleport(790.0, 782.0);
    SpawnHomeBaseTeleport(348.0, 2667.0);
    SpawnHomeBaseTeleport(2416.0, 2641.0);
    SpawnHomeBaseTeleport(4863.0, 1583.0);
    SpawnHomeBaseTeleport(5411.0, 1490.0);
    SpawnHomeBaseTeleport(5148.0, 5073.0);

    SpawnFirelit(521.0, 2726.0);
    SpawnFirelit(2521.0, 2795.0);
    SpawnFirelit(4729.0, 1464.0);
    SpawnFirelit(5540.0, 1615.0);
    SpawnFirelit(5254.0, 5318.0);
    SpawnFirelit(5254.0, 5318.0);
    setupConnection(230, 229);
    setupConnection(232, 231);
}

int SpawnHomeBaseTeleport(float x, float y)
{
    int unit = CreateObjectAt("GauntletExitA", x, y);

    SetUnitCallbackOnCollide(unit, teleportHome);
    return unit;
}

int SpawnFirelit(float x, float y)
{
    int unit = CreateObjectAt("Maiden", x, y);
    Frozen(unit, 1);
    SetCallback(unit, 9, rememberCurrentPos);
    return unit;
}

void teleportHome()
{
    if(IsPlayerUnit(OTHER))
    {
        MoveObject(OTHER, LocationX(226), LocationY(226));
        float x=GetObjectX(OTHER),y=GetObjectY(OTHER);
        DeleteObjectTimer(CreateObjectAt("blueRain", x,y), 10);
        PlaySoundAround(OTHER, SOUND_BlindOff);
        Effect("COUNTERSPELL_EXPLOSION", x,y, 0.0, 0.0);
        UniChatMessage(OTHER, "베이스 캠프로 귀환합니다 ....", 150);
    }
}

void rememberCurrentPos()
{
    if (IsPlayerUnit(OTHER) && CurrentHealth(SELF) == 75)
    {
        TeleportLocation(PLAYER_LAST_TRANSMISSION, GetObjectX(OTHER), GetObjectY(OTHER));
        Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        UniPrint(OTHER, "현재 위치가 저장 되었습니다 .");
        PlaySoundAround(SELF, SOUND_SoulGateTouch);
        Frozen(SELF, 0);
        SetUnitMaxHealth(SELF, 100);
        Frozen(SELF, 1);
    }
    else if (CurrentHealth(SELF) == 100 && !IsObjectOn(OTHER))
    {
        ObjectOn(OTHER);
        Delete(SELF);
    }
}

int HecubahBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 1500; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 2060; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 120; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; 
		arr[57] = 5548288; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

void HecubahSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2060);
		SetMemory(GetMemory(ptr + 0x22c), 1500);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 1500);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void spawnFinalBoss()
{
    int boss = CreateObject("Hecubah", 164);

    HecubahSubProcess(boss);
    Enchant(boss, "ENCHANT_VAMPIRISM", 0.0);
    SetCallback(boss, 5, hecubahDead);
    SetCallback(boss, 3, HecubahWeapons);
    SetOwner(GetMaster(), boss);
    SetUnitScanRange(boss, 400.0);
    FrameTimerWithArg(1, boss, LoopBossEffect);
}

void HecubahWeapons()
{
    int rnd;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 5.0);
            rnd = Random(0, 3);
            if (!rnd)
            {
                ObjectOff(SELF);
                Enchant(SELF, "ENCHANT_FREEZE", 0.0);
                Enchant(SELF, "ENCHANT_INVULNERABLE", 0.0);
                Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
                FrameTimerWithArg(18, GetTrigger(), ShotDeathRay);
                FrameTimerWithArg(18, GetCaller(), ShotDeathRay);
            }
            else if (rnd == 1)
            {
                // ObjectOff(SELF);
                // Enchant(SELF, "ENCHANT_FREEZE", 0.0);
                // Enchant(SELF, "ENCHANT_INVULNERABLE", 0.0);
                // FrameTimerWithArg(3, GetTrigger(), FireShooter);
                StartFireShooter(GetTrigger());
            }
            else if (rnd == 2)
            {
                HecubahSkillMagicReady(SELF, OTHER, 24);
            }
            else
            {
                PauseObject(SELF, 60);
                CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
                if (Random(0, 1))
                    CastSpellObjectObject("SPELL_CHAIN_LIGHTNING", SELF, OTHER);
                else
                    CastSpellObjectObject("SPELL_MAGIC_MISSILE", SELF, OTHER);
            }
        }
    }
    CheckResetSight(GetTrigger(), 64);
}

void hecubahDead()
{
    PlaySoundAround(SELF, SOUND_StaffOblivionAchieve1);
    PlaySoundAround(SELF, SOUND_BigGong);
    Effect("WHITE_FLASH", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    UniPrintToAll(" 승리 !__ 최종 보스 헤쿠바를 죽였습니다");
    UniPrintToAll(" 선장 RPG 맵을 플레이 해 주셔서 감사합니다");
}

void ShotDeathRay(int ptr)
{
    int unit, shot, k;
    float pos_x, pos_y;
    string name = "Maiden";

    if (!unit)
        unit = ptr;
    else if (CurrentHealth(unit))
    {
        pos_x = UnitRatioX(unit, ptr, 25.0);
        pos_y = UnitRatioY(unit, ptr, 25.0);
        MoveWaypoint(164, GetObjectX(unit) - pos_x, GetObjectY(unit) - pos_y);
        shot = CreateObject("InvisibleLightBlueLow", 164);
        Delete(shot);
        for (k = 1 ; k <= 16 ; k ++)
        {
            if (!checkWaypointLimitLine(164))
                break;
            MoveWaypoint(164, GetWaypointX(164) - pos_x, GetWaypointY(164) - pos_y);
            CreateObject(name, 164);
            Frozen(shot + k, 1);
            DeleteObjectTimer(shot + k, 1);
            SetCallback(shot + k, 9, DeathrayTouched);
        }
        Effect("DEATH_RAY", GetObjectX(unit), GetObjectY(unit), GetWaypointX(164), GetWaypointY(164));
        AudioEvent("DeathRayCast", 164);
        EnchantOff(unit, "ENCHANT_FREEZE");
        EnchantOff(unit, "ENCHANT_INVULNERABLE");
        ObjectOn(unit);
    }
}

void DeathrayTouched()
{
    if (HasClass(OTHER, "PLAYER"))
    {
        Damage(OTHER, SELF, 100, 16);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.9);
    }
}
void UnitVisibleSplashA()
{
    int parent = GetOwner(SELF);
    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (CurrentHealth(GetOwner(parent)))
    {
        if (GetUnit1C(OTHER) ^ spIdx)
        {
            if (DistanceUnitToUnit(SELF, OTHER) <= GetObjectX(parent))
            {
                Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
                SetUnit1C(OTHER, spIdx);
            }
        }
    }
}

void SplashDamageAt(int owner, int dam, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, SplashIdx;

    SplashIdx ++;
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(dam));

    int k = -1;
    while (++k < 4)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 64);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplashA);
    }
    DeleteObjectTimer(ptr - 1, 2);
    DeleteObjectTimer(ptr - 2, 2);
}

void HecubahSkillMagicCollide()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        SplashDamageAt(owner, 120, GetObjectX(SELF), GetObjectY(SELF), 100.0);
        GreenExplosion(GetObjectX(SELF), GetObjectY(SELF));
        DeleteObjectTimer(CreateObjectAt("ForceOfNatureCharge", GetObjectX(SELF), GetObjectY(SELF)), 24);
        Delete(SELF);
    }
}

void HecubahSkillMagic(int sUnit)
{
    int caster = GetOwner(sUnit), enemy = ToInt(GetObjectZ(sUnit));

    if (CurrentHealth(caster) && CurrentHealth(enemy))
    {
        float x=GetObjectX(caster),y=GetObjectY(caster);
        int mis=CreateObjectAt("Mover", x,y);
        Delete(mis++);
        CastSpellObjectLocation("SPELL_ANCHOR", caster, GetObjectX(caster) + UnitRatioX(enemy, caster, 17.0), GetObjectY(caster) + UnitRatioY(enemy, caster, 17.0));
        SetUnitCallbackOnCollide(mis, HecubahSkillMagicCollide);
        PlaySoundAround(caster, 38);
        Enchant(mis, "ENCHANT_RUN", 0.0);
        Enchant(mis, "ENCHANT_HASTED", 0.0);
    }
    Delete(sUnit);
}

void HecubahSkillMagicReady(int sCaster, int sEnemy, int sDelay)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sCaster), GetObjectY(sCaster));

    SetOwner(sCaster, unit);
    Raise(unit, sEnemy);
    FrameTimerWithArg(sDelay, unit, HecubahSkillMagic);
}

static void onfireShooterHandler(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int count = GetDirection(sub), owner=GetOwner(sub);

        if (count)
        {
            FrameTimerWithArg(1, sub, onfireShooterHandler);
            LookWithAngle(sub, count-1);
            int mis = CreateObjectAt("WeakFireball", GetObjectX(sub) + MathSine(count * 8 + 90, 30.0), GetObjectY(sub) + MathSine(count * 8, 30.0));

            PlaySoundAround(mis, SOUND_PushCast);
            SetOwner(owner, mis);
            PushObject(mis, 20.0, GetObjectX(sub), GetObjectY(sub));
            return;
        }
        Delete(sub);
    }
}

void StartFireShooter(int caster)
{
    int sub=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster), GetObjectY(caster));

    FrameTimerWithArg(1, sub, onfireShooterHandler);
    LookWithAngle(sub, 45);
    SetOwner(caster, sub);
}

void entranceLOTD()
{
    if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
    {
        if (GetObjectX(OTHER) > 3573.0)
            MoveObject(OTHER, 2464.0, 5657.0);
        else
            MoveObject(OTHER, 5472.0, 5122.0);
    }
}

void LoopWizardStatus(int unit)
{
    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit, "ENCHANT_INVISIBLE"))
        {
            EnchantOff(unit, "ENCHANT_INVISIBLE");
            Enchant(unit, "ENCHANT_SHIELD", 0.0);
            Enchant(unit, "ENCHANT_SHOCK", 12.0);
        }
        FrameTimerWithArg(1, unit, LoopWizardStatus);
    }
}

void spawnGateKeeper()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("mechanicalGolem", 166);
        Raise(unit, 180.0);
        SetUnitMaxHealth(unit, 900);
        AggressionLevel(unit, 0.83);
        LookAtObject(unit, OTHER);
        SetCallback(unit, 5, keeperDead);
        AudioEvent("MechGolemPowerUp", 166);
    }
    ObjectOff(SELF);
}

void keeperDead()
{
    DeleteObjectTimer(SELF, 30);
    WallGroupOpen(3);
}

void LoopBossEffect(int unit)
{
    if (CurrentHealth(unit))
    {
        Effect("DAMAGE_POOF", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        FrameTimerWithArg(1, unit, LoopBossEffect);
    }
}

void destroyArea2BossRoomWalls()
{
    int count;

    if (!count)
    {
        WallGroupOpen(4);
        while(count < 5)
        {
            DeleteObjectTimer(CreateObject("BigSmoke", 189), 5);
            MoveWaypoint(189, GetWaypointX(189), GetWaypointY(189) + 23.0);
            Effect("JIGGLE", GetWaypointX(189), GetWaypointY(189), 30.0, 0.0);
            AudioEvent("WallDestroyedWood", 189);
            count += 1;
        }
    }
}

void DelayGiveToUnit(int ptr)
{
    int unit;

    if (!unit)  unit = ptr;
    else
    {
        Pickup(unit, ptr);
        unit = 0;
    }
}

void CheckResetSight(int unit, int delay)
{
    if (!HasEnchant(unit, "ENCHANT_DETECTING"))
    {
        Enchant(unit, "ENCHANT_DETECTING", 0.0);
        FrameTimerWithArg(delay, unit, ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.06);
    AggressionLevel(unit, 1.0);
}

static void onPlantProperty(int mon)
{
    SetUnitSpeed(mon, 2.5);
    AggressionLevel(mon, 1.0);
}

int UnitFlagTable(int idx)
{
    int tb[31];

    if (idx < 0)
    {
        tb[0] = UnitSpeedSlow; tb[3] = UnitSpeedSlow; tb[4] = UnitSpeedSlow; tb[8] = UnitTheFrog;// tb[25] = 0;
        tb[18] = UnitTheImp; tb[24] = UnitDoesntTeleport; tb[15] = UnitVampProperty; tb[21] = UnitVampProperty; tb[30] = MaidenMonster;
    }
    return tb[idx];
}

void UnitNoFlag(int unit)
{
    return;
}

void UnitSpeedSlow(int unit)
{
    Enchant(unit, "ENCHANT_SLOWED", 0.0);
}

void UnitTheFrog(int unit)
{
    return;
}

void UnitTheImp(int unit)
{
    SetCallback(unit, 3, impWeapon);
    Enchant(unit, "ENCHANT_SLOWED", 0.0);
}

void UnitDoesntTeleport(int unit)
{
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    LoopWizardStatus(unit);
}

void CollideFishBig()
{
    int owner = GetTrigger() + 1;

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, GetOwner(SELF), 4, 11);
        Delete(SELF);
    }
}

void FrogMissileAttackLoop(int ptr)
{
    if (MaxHealth(ptr))
    {
        int owner = GetOwner(ptr + 1), count = GetDirection(ptr + 1);

        if (count)
        {
            if (IsVisibleTo(owner, ptr))
            {
                FrameTimerWithArg(1, ptr, FrogMissileAttackLoop);
                LookWithAngle(ptr+1, count-1);
                MoveObjectVector(ptr, -GetObjectZ(ptr+1), -GetObjectZ(ptr+2));
                return;
            }
        }
        Delete(ptr);
    }
    Delete(ptr + 1);
    Delete(ptr + 2);
}

void FrogSightEvent()
{
    int ptr;

    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        LookAtObject(SELF, OTHER);
        LookAtObject(GetTrigger() + 1, OTHER);
        Walk(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
        Enchant(SELF, "ENCHANT_BURNING", 0.9);
        ptr = CreateObjectAt("FishBig", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 16.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 16.0));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), UnitRatioX(SELF, OTHER, 22.0));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), UnitRatioY(SELF, OTHER, 22.0));
        ObjectOff(ptr);
        LookAtObject(ptr, OTHER);
        LookWithAngle(ptr+1, 20);
        Frozen(ptr, TRUE);
        SetCallback(ptr, 9, CollideFishBig);
        SetOwner(SELF, ptr + 1);
        FrameTimerWithArg(1, ptr, FrogMissileAttackLoop);
    }
    CheckResetSight(GetTrigger(), 25);
}

void WrapFrogImage(int unit)
{
    if (CurrentHealth(unit))
    {
        if (ToInt(Distance(GetObjectX(unit), GetObjectY(unit), GetObjectX(unit + 1), GetObjectY(unit + 1))))
        {
            LookWithAngle(unit + 1, GetDirection(unit));
            Walk(unit + 1, GetObjectX(unit + 1), GetObjectY(unit + 1));
            MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
        }
        else
            PauseObject(unit + 1, 60);
        FrameTimerWithArg(1, unit, WrapFrogImage);
    }
    else
    {
        Frozen(unit + 1, 0);
        ObjectOn(unit + 1);
        Damage(unit + 1, 0, 999, 14);
        DeleteObjectTimer(unit + 1, 90);
    }
}

void SetUnitFrogImage(int unit)
{
    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    SetCallback(unit, 3, FrogSightEvent);
    FrameTimerWithArg(1, unit, WrapFrogImage);
}

void UnitVampProperty(int unit)
{
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
}

void MaidenMonster(int unit)
{
    float x= GetObjectX(unit),y= GetObjectY(unit);
    Delete(unit);
    int ptr = ColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), x,y);
    SetUnitMaxHealth(ptr, 470);
    SetCallback(ptr, 5, SetDeaths);
    SetCallback(ptr, 3, MaidenSightEvent);
    AggressionLevel(ptr, 1.0);
    SetUnitScanRange(ptr, 400.0);
}

int ColorMaiden(int red, int grn, int blue, float xpos, float ypos)
{
    int unit = CreateObjectAt("Bear2", xpos, ypos);
    int ptr = GetMemory(0x750710), k;

    SetMemory(ptr + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(7));
    return unit;
}

void SplashDamage(int owner, int dam, float range, int wp)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1, k;

    SetOwner(owner, ptr - 1);
    MoveObject(ptr - 1, range, GetObjectY(ptr - 1));
    Raise(ptr - 1, ToFloat(dam));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObject("WeirdlingBeast", wp), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 32);
        SetOwner(ptr - 1, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(ptr - 1, 2);
}

void UnitVisibleSplash()
{
    int parent;

    if (!HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC"))
    {
        parent = GetOwner(SELF);
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
        {
            Enchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC", 0.1);
            Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
        }
    }
}

void FlyGreenBoltLoop(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner) && count < 20)
        {
            if (Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr + 1), GetObjectY(ptr)) > 23.0)
            {
                MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr), GetObjectY(ptr) - GetObjectZ(ptr + 1));
                MoveObject(ptr + 2, GetObjectX(ptr), GetObjectY(ptr));
                LookWithAngle(ptr, count + 1);
            }
            else
            {
                GreenSparkFxAt(GetObjectX(ptr), GetObjectY(ptr));
                SplashDamageAt(owner, 20, GetObjectX(ptr), GetObjectY(ptr), 100.0);
                LookWithAngle(ptr, 200);
                Delete(ptr);
            }
        }
        FrameTimerWithArg(1, ptr, FlyGreenBoltLoop);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void MaidenSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
    {
        Enchant(SELF, "ENCHANT_VILLAIN", 2.0);
        if (Distance(GetObjectX(SELF), GetObjectX(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) > 100.0)
        {
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 17.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 17.0));
            float x=GetObjectX(ptr),y=GetObjectY(ptr);
            WispDestroyFX(x,y);
            MoveObject(CreateObjectAt("InvisibleLightBlueHigh", x,y), GetObjectX(OTHER), GetObjectY(OTHER));
            Frozen(CreateObjectAt("GreenOrb", x,y), TRUE);
            UnitNoCollide(ptr + 2);
            SetOwner(SELF, ptr);
            Raise(ptr, UnitRatioX(SELF, OTHER, 22.0));
            Raise(ptr + 1, UnitRatioY(SELF, OTHER, 22.0));
            FrameTimerWithArg(1, ptr, FlyGreenBoltLoop);
        }
    }
    CheckResetSight(GetTrigger(), 35);
}

void GreenSparkFxAt(float sX, float sY)
{
    int fxUnit = CreateObjectAt("MonsterGenerator", sX, sY);

    Damage(fxUnit, 0, 10, 100);
    Delete(fxUnit);
}

void MapSignInit()
{
    RegistSignMessage(Object("mapPick1"), "붉은 마법출구로 들어가면 처음 시작위치였던 마을로 되돌아 갈 수 있습니다");
    RegistSignMessage(Object("mapPick2"), "필드 안내문: 이곳은 초급 사냥터 입니다~~ 초보자 분들이 놀기에 아주 적합합니다");
    RegistSignMessage(Object("mapPick3"), "이 횃불을 만지면 마을에서 필드로 나올 때 이곳으로 도착하게 됩니다");
    RegistSignMessage(Object("mapPick4"), "출입 시 주의!: 이 동굴안에는 곰의 모습을 한 괴수가 살고있음");
    RegistSignMessage(Object("mapPick5"), "필드 안내문: 이곳부터는 중급 사냥터 입니다. 슬슬 상대하기 힘든 괴물들이 등장하니 조심하세요");
    RegistSignMessage(Object("mapPick6"), "조금 더 가면 온몸이 초록색인 마녀가 거주중인 늪지대가 나옵니다");
    RegistSignMessage(Object("mapPick7"), "필드 안내문: 숙련생 전용 사냥터 입니다. 여러가지 마법을 부릴 줄 아는 강력한 전사만 추천해요");
    RegistSignMessage(Object("mapPick8"), "간호장교 예비 후보생들이에요. 당신에게 유용한 물약과 의료 서비스를 제공해드릴 거에요");
    RegistSignMessage(Object("mapPick9"), "최대 4개의 스킬을 습득할 수 있습니다. 미스틱을 클릭하여 습득하세요. 첫번째 스킬은 8천골드 입니다");
    RegistSignMessage(Object("mapPick10"), "필드로 나가는 비콘입니다. 나가기 전 물약과 장비를 든든하게 챙기세요");
    RegistSignMessage(Object("mapPick11"), "잔도 키우기-                              지도제작: 237");
    RegistSignMessage(Object("mapPick12"), "잔도 키우기- 베타테스트 시작               지도제작: 237");
}

void BlueMissileCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            GreenSparkFxAt(GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, owner, 185, 14);
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

int BlueOrbSummon(int sOwner)
{
    int unit = CreateObjectAt("MagicMissile", GetObjectX(sOwner) + UnitAngleCos(sOwner, 14.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 14.0));
    int ptr = GetMemory(0x750710);

    if (ptr)
        SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetOwner(sOwner, unit);
    SetUnitCallbackOnCollide(unit, BlueMissileCollide);
    return unit;
}

void OblivionUseHandler()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 20)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        PushObject(BlueOrbSummon(OTHER), 20.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 221);
    }
}

void DelayGiveToOwner(int sTarget)
{
    int sOwner = GetOwner(sTarget);

    if (IsObjectOn(sTarget) && CurrentHealth(sOwner))
        Pickup(sOwner, sTarget);
    else
        Delete(sTarget);
}

int SummonOblivionStaff(float sX, float sY)
{
    int unit = CreateObjectAt("OblivionOrb", sX, sY);

    SetItemPropertyAllowAllDrop(unit);
    DisableOblivionItemPickupEvent(unit);
    SetUnitCallbackOnUseItem(unit, OblivionUseHandler);
    return unit;
}

void OnSpiderWebCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsCaller(owner))
            return;

        if (IsAttackedBy(OTHER, owner))
            Damage(OTHER, owner, 180, DAMAGE_TYPE_POISON);
        Enchant(OTHER, "ENCHANT_SLOWED", 2.0);
    }
    WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
    Delete(SELF);
}

void TakeShotSpiderWeb()
{
    int web=CreateObjectAt("SpiderSpit", GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));

    SetOwner(OTHER, web);
    LookWithAngle(web, GetDirection(OTHER));
    SetUnitCallbackOnCollide(web, OnSpiderWebCollide);
    PushObject(web, 44.0, GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(web, SOUND_LargeSpiderSpit);
}

int DispositionSpiderSpitSword(float xpos, float ypos)
{
    int sd=CreateObjectAt("MorningStar", xpos, ypos);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_confuse1, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyE);
    return sd;
}

#define _AUTODETECT_SUBUNIT_ 0
#define _AUTODETECT_XVECT_ 1
#define _AUTODETECT_YVECT_ 2
#define _AUTODETECT_MAX_ 4

void OnAutoDetectorEnemySighted()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner))
    {
        Damage(OTHER, SELF, 100, DAMAGE_TYPE_ZAP_RAY);
        PlaySoundAround(OTHER, SOUND_DeathRayKill);
        Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    }
    if (IsObjectOn(GetTrigger()+1))
        Delete(GetTrigger() + 1);
}

void ConfirmAutoDetectingInvalidTarget(int *pMem)
{
    int sub1 = pMem[_AUTODETECT_SUBUNIT_];

    if (IsObjectOn(sub1))
    {
        float *vect = ArrayRefN(pMem, _AUTODETECT_XVECT_);
        float xpos = GetObjectX(sub1), ypos = GetObjectY(sub1);

        Delete(sub1);
        Effect("DEATH_RAY", xpos, ypos, xpos+(vect[0]*64.0), ypos+(vect[1]*64.0));
    }
    FreeSmartMemEx(pMem);
}

void AutoDetectingTriggered()
{
    float xvect=UnitAngleCos(OTHER, 3.0),yvect=UnitAngleSin(OTHER, 3.0);
    int sub = CreateObjectAt("WeirdlingBeast", GetObjectX(OTHER) + xvect, GetObjectY(OTHER) + yvect);
    int validate = CreateObjectAt("BlueSummons", GetObjectX(sub), GetObjectY(sub));

    int *pMem;
    AllocSmartMemEx(_AUTODETECT_MAX_*4, &pMem);
    FrameTimerWithArg(2, pMem, ConfirmAutoDetectingInvalidTarget);
    pMem[_AUTODETECT_SUBUNIT_]=sub+1;
    pMem[_AUTODETECT_XVECT_]=xvect;
    pMem[_AUTODETECT_YVECT_]=yvect;
    SetCallback(sub, 3, OnAutoDetectorEnemySighted);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, GetDirection(OTHER));
    SetUnitScanRange(sub, 420.0);
    DeleteObjectTimer(sub, 1);
    SetUnitFlags(sub, GetUnitFlags(sub) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    PlaySoundAround(OTHER, SOUND_FirewalkOff);
}

int DispositionAutodetectSword(float x, float y)
{
    int sd=CreateObjectAt("GreatSword", x,y);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyD);
    return sd;
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float *pFrom, float *pTo, int dur)
{
    float xyVect[2];
    ComputePointRatio(pTo, pFrom, xyVect, 32.0);
    int count = FloatToInt(Distance(pFrom[0],pFrom[1],pTo[0],pTo[1])/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", pFrom[0], pFrom[1]);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	FrameTimerWithArg(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 130, DAMAGE_TYPE_ELECTRIC);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.1);
            Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
    }
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 19.0), yvect = UnitAngleSin(OTHER, 19.0);
    int arr[30], u=0;

    arr[u] = DummyUnitCreateAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateAt("Demon", GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        SetUnitFlags(arr[u], GetUnitFlags(arr[u]) ^ UNIT_FLAG_NO_PUSH_CHARACTERS);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    float fromPoint[]={GetObjectX(arr[0]), GetObjectY(arr[0])};
    float toPoint[]={GetObjectX(arr[u-1]), GetObjectY(arr[u-1])};
    DrawYellowLightningFx(fromPoint, toPoint, 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}

int DispositionThunderSword(float xpos, float ypos)
{
    int ham=CreateObjectAt("BattleAxe", xpos, ypos);

    SetWeaponPropertiesDirect(ham, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(ham, 3, m_pWeaponUserSpecialPropertyA);
    return ham;
}

int CreateToxicCloud(float xpos, float ypos, int owner, short time)
{
    int cloud=CreateObjectAt("ToxicCloud", xpos, ypos);
    int *ptr=UnitToPtr(cloud);
    int *pEC=ptr[187];

    pEC[0]=time;
    SetOwner(owner, cloud);
}

void FlatusHammer()
{
    CreateToxicCloud(GetObjectX(OTHER), GetObjectY(OTHER), OTHER, 180);
    PlaySoundAround(OTHER, SOUND_TrollFlatus);
}

int DispositionFlatusHammer(float x,float y)
{
    int weapon=CreateObjectAt("WarHammer", x,y);

    SetWeaponPropertiesDirect(weapon, ITEM_PROPERTY_PoisonProtect4, ITEM_PROPERTY_venom4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialProperty7);
    return weapon;
}

void OnThrowingStoneCollide()
{
    if (!GetTrigger())
        return;

    while (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            Damage(OTHER, SELF, 90, DAMAGE_TYPE_IMPACT);
            break;
        }
        return;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    PlaySoundAround(SELF, SOUND_HitEarthBreakable);
    Delete(SELF);
}

void MultipleThrowingStone()
{
    float xvect = UnitAngleCos(OTHER, 9.0), yvect = UnitAngleSin(OTHER, 9.0);
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + xvect -(yvect*2.0), GetObjectY(OTHER) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(OTHER);

    while (++rep<5)
    {
        CreateObjectAtEx("ThrowingStone", GetObjectX(posUnit), GetObjectY(posUnit), &single);
        SetOwner(OTHER, single);
        SetUnitEnchantCopy(single, GetLShift(ENCHANT_SLOWED));
        SetUnitCallbackOnCollide(single, OnThrowingStoneCollide);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
    PlaySoundAround(OTHER, SOUND_UrchinThrow);
}

int DispositionMultipleThrowingStone(float x, float y)
{
    int sd=CreateObjectAt("GreatSword", x,y);

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_confuse1, ITEM_PROPERTY_impact4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, m_pWeaponUserSpecialPropertyB);
    return sd;
}

int FallingMeteor(float sX, float sY, int sDamage, float sSpeed)
{
    int mUnit = CreateObjectAt("Meteor", sX, sY);
    int *ptr = UnitToPtr(mUnit);

    if (ptr)
    {
        int *pEC=ptr[187];

        pEC[0]=sDamage;
        ptr[5]|=0x20;
        Raise(mUnit, 255.0);
        ptr[27]=sSpeed;
    }
    return mUnit;
}

void StartFallMeteor()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int ixpos, iypos;

    if (!GetPlayerMouseXY(OTHER, &ixpos, &iypos))
        return;
    float xpos = IntToFloat(ixpos), ypos = IntToFloat(iypos);

    if (!CheckCoorValidate(xpos, ypos))
        return;

    int test=CreateObjectAt("ImaginaryCaster", xpos, ypos);

    if (IsVisibleTo(OTHER, test))
    {
        SetOwner(OTHER, FallingMeteor(xpos, ypos, 220, -8.0));
        PlaySoundAround(OTHER, SOUND_MeteorCast);
    }
    else
    {
        UniPrint(OTHER, "마우스 커서 지역은 볼 수 없습니다- 마법 캐스팅 실패입니다");
    }    
    Delete(test);
}

int DispositionUserMeteorSword(float x, float y)
{
    int weapon=CreateObjectAt("GreatSword", x,y);

    SetWeaponPropertiesDirect(weapon, 0, ITEM_PROPERTY_fire1, ITEM_PROPERTY_fire2, 0);
    SpecialWeaponPropertySetWeapon(weapon, 3, m_pWeaponUserSpecialProperty5);
    return weapon;
}

static void specialWeaponDesc()
{
    int pIndex=GetPlayerIndex(OTHER);
    int *p;

    if (!HashGet(m_genericHash, GetTrigger(), &p, FALSE))
        return;
    int cur= p[pIndex];
    int args[]={StringUtilGetScriptStringPtr( m_specialWeaponNameArray[cur]), m_specialWeaponPays[cur]};
    char buff[192];
    NoxSprintfString(buff, "제품 %s 을 구입하려면, 예를 누르세요. 가격은 %d입니다", args, sizeof(args));
    UniPrint(OTHER, ReadStringAddressEx(buff));
    TellStoryUnitName("AA", "bindevent:NullEvent", m_specialWeaponNameArray[cur]);
}

static int CreateSpecialWeaponProto(int functionId, float x,float y)
{
    StopScript( Bind(functionId, &functionId + 4) );
}

static void specialWeaponTrade()
{
    int pIndex=GetPlayerIndex(OTHER), dlgRes = GetAnswer(SELF);
    int *p;

    if (!HashGet(m_genericHash, GetTrigger(), &p, FALSE))
        return;
    int cur=p[pIndex];
    if (dlgRes==2)
    {
        UniPrint(OTHER, "'아니오'을 누르셨어요. 다음 항목을 보여드리겠어요");
        p[pIndex]=(cur+1)%SPECIAL_WEAPON_MAX_COUNT;
        specialWeaponDesc();
    }
    else if (dlgRes==1)
    {
        if (GetGold(OTHER)>=m_specialWeaponPays[cur])
        {
            ChangeGold(OTHER, -m_specialWeaponPays[cur]);
            CreateSpecialWeaponProto(m_specialWeaponFn[cur], GetObjectX(OTHER), GetObjectY(OTHER));
            UniPrint(OTHER, "거래완료!- 당신 아래에 있어요");
        }
        else
            UniPrint(OTHER, "거래실패!- 잔액이 부족합니다");
    }
}

int PlaceSpecialWeaponShop(int location)
{
    int s=DummyUnitCreateAt("WizardWhite", LocationX(location), LocationY(location));

    SetDialog(s, "YESNO", specialWeaponDesc, specialWeaponTrade);
    int *p;

    AllocSmartMemEx(32*4, &p);
    NoxDwordMemset(p, 32, 0);
    HashPushback(m_genericHash, s, p);
    return s;
}

void startArea4()
{
    STAGE = 4;
    FrameTimerWithArg(30, m_areaMarks4, spawnMonsterArea);
}

void StartFinalArea()
{
    ObjectOff(SELF);
    startArea4();
}

void UnlockPart4Gate()
{
    ObjectOff(SELF);
    UnlockDoor(Object("part4g1"));
    UnlockDoor(Object("part4g11"));
    UniPrint(OTHER, "게이트의 잠금이 해제되었습니다");
}

void SplshTest()
{
    int boss = CreateObjectAt("Bat", GetObjectX(OTHER), GetObjectY(OTHER));

    dropRock(boss);
}
