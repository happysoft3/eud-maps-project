
#include "libs/define.h"
#include "libs/wallutil.h"
#include "libs/unitstruct.h"
#include "libs/buff.h"
#include "libs/fxeffect.h"
#include "libs/mathlab.h"
// #include "libs/magicmissile.h"
#include "libs/queueTimer.h"
#include "libs/playerinfo.h"
#include "libs/printutil.h"
#include "libs/array.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

void OnIcecrystalCollide()
{
    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(GetTrigger()+1);

            if (!IsAttackedBy(OTHER, owner))
                break;

            Damage(OTHER, owner, 180, DAMAGE_TYPE_ELECTRIC);
        }
        else if (GetCaller())
            break;

        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(GetTrigger() + 1);
        Delete(SELF);
        break;
    }
}

 int CheckCoorValidate(float x, float y)
{
    if (x<100.0)
        return FALSE;
    if (y<100.0)
        return FALSE;
    if (x>5750.0)
        return FALSE;
    if (y>5750.0)
        return FALSE;
    return TRUE;
}
int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

int BurningZombieFx(float xProfile, float yProfile)
{
    int fx;

    CreateObjectAtEx("Zombie", xProfile, yProfile, &fx);
    UnitNoCollide(fx);
    ObjectOff(fx);
    Damage(fx, 0, 999, DAMAGE_TYPE_FLAME);
    return fx;
}

void UnitVisibleSplashA()
{
    int parent = GetOwner(SELF);
    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (CurrentHealth(GetOwner(parent)))
    {
        if (GetUnit1C(OTHER) ^ spIdx)
        {
            if (DistanceUnitToUnit(SELF, OTHER) <= GetObjectX(parent))
            {
                Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
                SetUnit1C(OTHER, spIdx);
            }
        }
    }
}

void SplashDamageAt(int owner, int dam, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, SplashIdx;

    SplashIdx ++;
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(dam));

    int k = -1;
    while (++k < 4)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 64);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplashA);
    }
    DeleteObjectTimer(ptr - 1, 2);
    DeleteObjectTimer(ptr - 2, 2);
}

// void FlareExplosion()
// {
//     if (!GetTrigger())
//     {
//         if (WallUtilGetWallAtObjectPosition(SELF))
//         {
//             Effect("EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
//         }
//         Delete(SELF);
//         return;
//     }

//     int owner = GetOwner(SELF);
//     if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
//     {
//         SplashDamageAt(owner, 80, GetObjectX(SELF), GetObjectY(SELF), 60.0);
//         Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
//         Delete(SELF);
//     }
// }

void UserSpecialWeaponCbFirst()
{
    // float xvect=UnitAngleCos(OTHER, 11.0), yvect=UnitAngleSin(OTHER, 11.0);
    // int mis=MagicMissileUtilSummon("ImpShot", OTHER, 3.9, xvect, yvect);

    // UnitSetEnchantTime(mis, GetLShift(ENCHANT_RUN) | GetLShift(ENCHANT_HASTED), 0);
    // SetOwner(OTHER, mis);
    // SetUnitCallbackOnCollide(mis, FlareExplosion);
    // // PushObjectTo(mis, xvect*3.0, yvect*3.0);
    // PlaySoundAround(OTHER, SOUND_FlareWand);
    // DeleteObjectTimer(mis, 150);
    float x=GetObjectX(OTHER),y=GetObjectY(OTHER);
    float vectX=UnitAngleCos(OTHER, 9.0),vectY=UnitAngleSin(OTHER, 9.0);

    int a=CreateObjectById(OBJ_ARCHER_BOLT,x+vectX,y+vectY);

    SetOwner(OTHER,a);
    LookWithAngle(a,GetDirection(OTHER));
    PushObjectTo(a, vectX*4.0,vectY*4.0);
    SetUnitEnchantCopy(a,GetLShift(ENCHANT_INFRAVISION)|GetLShift(ENCHANT_REFLECTIVE_SHIELD)|GetLShift(ENCHANT_SHOCK));
}

void ForceDestroyBoulder(int boulder)
{
    if (CurrentHealth(boulder))
        Damage(boulder, 0, 999, DAMAGE_TYPE_PLASMA);
}

void UserSpecialWeaponCbSecond()
{
    int checker=CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + UnitAngleCos(OTHER, 26.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 26.0));

    if (!IsVisibleTo(OTHER, checker))
        return;

    int roll=CreateObjectAt("Boulder", GetObjectX(checker), GetObjectY(checker));

    SetOwner(OTHER, roll);
    Enchant(roll, "ENCHANT_SHOCK", 0.0);
    PushObject(roll, 60.0, GetObjectX(OTHER), GetObjectY(OTHER));
    PushTimerQueue(60, roll, ForceDestroyBoulder);
    PlaySoundAround(OTHER, SOUND_ElevStoneUp);
}

void ThirdShurikenCollide()
{
    while (TRUE)
    {
        if (!GetTrigger())
            break;
        int owner = GetOwner(SELF);

        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 161, DAMAGE_TYPE_BLADE);
            Enchant(OTHER, EnchantList(ENCHANT_SLOWED), 1.0);
        }
        break;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

void UserSpecialWeaponCbThird()
{
    int shuriken=CreateObjectAt("OgreShuriken", GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));

    SetOwner(OTHER, shuriken);
    SetUnitEnchantCopy(shuriken, GetLShift(ENCHANT_SHOCK) | GetLShift(ENCHANT_SHIELD));
    SetUnitCallbackOnCollide(shuriken, ThirdShurikenCollide);
    PushObject(shuriken, 50.0, GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(OTHER, SOUND_ChakramThrow);
}

void StrikeFlameCollide()
{
    while (TRUE)
    {
        if (!GetTrigger())
            break;
        int owner = GetOwner(SELF);

        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 140, DAMAGE_TYPE_EXPLOSION);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.2);
        }
        break;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

#define STRIKE_FLAME_MARK_UNIT 0
#define STRIKE_FLAME_XVECT 1
#define STRIKE_FLAME_YVECT 2
#define STRIKE_FLAME_COUNT 3
#define STRIKE_FLAME_OWNER 4
#define STRIKE_FLAME_MAX 5

void StrikeFlameLoop(int *pArgs)
{
    int owner=pArgs[STRIKE_FLAME_OWNER];
    while (TRUE)
    {
        if (CurrentHealth(owner))
        {
            int *count=ArrayRefN(pArgs, STRIKE_FLAME_COUNT), mark = pArgs[STRIKE_FLAME_MARK_UNIT];

            if (count[0] && IsVisibleTo(mark, mark+1))
            {
                FrameTimerWithArg(1, pArgs, StrikeFlameLoop);
                float xvect = pArgs[STRIKE_FLAME_XVECT], yvect=pArgs[STRIKE_FLAME_YVECT];

                MoveObjectVector(mark, xvect, yvect);
                MoveObjectVector(mark+1, xvect, yvect);
                int dum=DummyUnitCreateAt("Demon", GetObjectX(mark), GetObjectY(mark));

                SetCallback(dum, 9, StrikeFlameCollide);
                SetOwner(owner, dum);
                DeleteObjectTimer(dum, 1);
                DeleteObjectTimer( BurningZombieFx(GetObjectX(mark), GetObjectY(mark)), 12);
                
                --count[0];
                break;
            }
        }
        Delete(pArgs[STRIKE_FLAME_MARK_UNIT]++);
        Delete(pArgs[STRIKE_FLAME_MARK_UNIT]++);
        FreeSmartMemEx(pArgs);
        break;
    }
}

void StrikeFlameTriggered(int caster)
{
    float xvect=UnitAngleCos(caster, 17.0), yvect=UnitAngleSin(caster, 17.0);
    int mark = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) + xvect, GetObjectY(caster) + yvect);
    int visCheck=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) - xvect, GetObjectY(caster) - yvect);

    int *args;

    AllocSmartMemEx(STRIKE_FLAME_MAX*4, &args);
    args[STRIKE_FLAME_MARK_UNIT]=mark;
    args[STRIKE_FLAME_XVECT]=xvect;
    args[STRIKE_FLAME_YVECT]=yvect;
    args[STRIKE_FLAME_COUNT]=30; //count
    args[STRIKE_FLAME_OWNER]=caster;
    FrameTimerWithArg(1, args, StrikeFlameLoop);
}

void UserSpecialWeaponCb4()
{
    StrikeFlameTriggered(GetCaller());
    PlaySoundAround(OTHER, SOUND_MeteorShowerCast);
}

int FallingMeteor(float sX, float sY, int sDamage, float sSpeed)
{
    int mUnit = CreateObjectAt("Meteor", sX, sY);
    int *ptr = UnitToPtr(mUnit);

    if (ptr)
    {
        int *pEC=ptr[187];

        pEC[0]=sDamage;
        ptr[5]|=0x20;
        Raise(mUnit, 255.0);
        ptr[27]=sSpeed;
    }
    return mUnit;
}

void UserSpecialWeaponCb5()
{
    if (!IsPlayerUnit(OTHER))
        return;

    int ixpos, iypos;

    if (!GetPlayerMouseXY(OTHER, &ixpos, &iypos))
        return;
    float xpos = IntToFloat(ixpos), ypos = IntToFloat(iypos);

    if (!CheckCoorValidate(xpos, ypos))
        return;

    int test=CreateObjectAt("ImaginaryCaster", xpos, ypos);

    if (IsVisibleTo(OTHER, test))
    {
        SetOwner(OTHER, FallingMeteor(xpos, ypos, 228, -8.0));
        PlaySoundAround(OTHER, SOUND_MeteorCast);
    }
    else
    {
        UniPrint(OTHER, "마우스 커서 지역은 볼 수 없습니다- 마법 캐스팅 실패입니다");
    }    
    Delete(test);
}

#define BACKSTEP_DISTANCE 100.0
void BackstepHammer()
{
    float backVectX = UnitAngleCos(OTHER, -BACKSTEP_DISTANCE), backVectY = UnitAngleSin(OTHER, -BACKSTEP_DISTANCE);

    SplashDamageAt(OTHER, 150, GetObjectX(OTHER), GetObjectY(OTHER), 180.0);
    PushObjectTo(OTHER, backVectX, backVectY);
    DeleteObjectTimer(CreateObjectAt("ForceOfNatureCharge", GetObjectX(OTHER), GetObjectY(OTHER)), 21);
    PlaySoundAround(OTHER, SOUND_FireballExplode);
    Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 33.0, 0.0);
}

int CreateToxicCloud(float xpos, float ypos, int owner, short time)
{
    int cloud=CreateObjectAt("ToxicCloud", xpos, ypos);
    int *ptr=UnitToPtr(cloud);
    int *pEC=ptr[187];

    pEC[0]=time;
    SetOwner(owner, cloud);
}

void FlatusHammer()
{
    float x=GetObjectX(OTHER), y=GetObjectY(OTHER);
    int count=8;

    while (--count>=0)
        CreateToxicCloud(x+MathSine(count*45+90, 69.0), y+MathSine(count*45, 69.0), OTHER, 150);
    PlaySoundAround(OTHER, SOUND_TrollFlatus);
}

int CreateIceCrystal(int owner, float gap, int lifetime)
{
    int cry=CreateObjectAt("GameBall", GetObjectX(owner) + UnitAngleCos(owner, gap), GetObjectY(owner) + UnitAngleSin(owner, gap));

    SetOwner(owner, CreateObjectAt("ImaginaryCaster", GetObjectX(cry), GetObjectY(cry)));
    SetUnitCallbackOnCollide(cry, OnIcecrystalCollide);
    DeleteObjectTimer(cry, lifetime);
    DeleteObjectTimer(cry+1, lifetime + 15);
    return cry;
}

void CUserWeaponShootIceCrystal()
{
    PushObject(CreateIceCrystal(OTHER, 19.0, 75), 22.0, GetObjectX(OTHER), GetObjectY(OTHER));
    WispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(OTHER, SOUND_LightningWand);
    PlaySoundAround(OTHER, SOUND_ElevLOTDDown);
}

void OnSpikeRingCollide()
{
    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(SELF);

            if (!IsAttackedBy(OTHER, owner))
                return;

            Damage(OTHER, owner, 83, DAMAGE_TYPE_BLADE);
        }
        
        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(SELF);
        break;
    }
}

void LaunchSpikeRingSingle(int owner, float xBase, float yBase, int angle, float gap, float force)
{
    int sp=CreateObjectAt("OgreShuriken", xBase + MathSine(angle +90, gap), yBase + MathSine(angle, gap));

    SetOwner(owner, sp);
    PushObject(sp, force, xBase, yBase);
    SetUnitCallbackOnCollide(sp, OnSpikeRingCollide);
}

void CUserWeaponShotSpikeRing()
{
    int repeat=36;
    float xProfile = GetObjectX(OTHER), yProfile = GetObjectY(OTHER);

    while (--repeat>=0)
        LaunchSpikeRingSingle(OTHER, xProfile, yProfile, repeat * 10, 13.0, 33.0);

    PlaySoundAround(OTHER, SOUND_MonsterGeneratorSpawn);
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float *pFrom, float* pTo, int dur)
{
    float xyVect[2];
    ComputePointRatio(pTo, pFrom, xyVect, 32.0);
    int count = FloatToInt(Distance(pFrom[0],pFrom[1],pTo[0],pTo[1])/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", pFrom[0], pFrom[1]);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	FrameTimerWithArg(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 115, DAMAGE_TYPE_ELECTRIC);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.1);
            Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        }
    }
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 19.0), yvect = UnitAngleSin(OTHER, 19.0);
    int arr[30], u=0;

    arr[u] = DummyUnitCreateAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateAt("Urchin", GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    float fromPoint[]={GetObjectX(arr[0]), GetObjectY(arr[0])};
    float toPoint[]={GetObjectX(arr[u-1]), GetObjectY(arr[u-1])};
    DrawYellowLightningFx(fromPoint, toPoint, 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}

void OnThrowingStoneCollide()
{
    if (!GetTrigger())
        return;

    while (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            Damage(OTHER, SELF, 82, DAMAGE_TYPE_IMPACT);
            break;
        }
        return;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    PlaySoundAround(SELF, SOUND_HitEarthBreakable);
    Delete(SELF);
}

void MultipleThrowingStone()
{
    float xvect = UnitAngleCos(OTHER, 9.0), yvect = UnitAngleSin(OTHER, 9.0);
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + xvect -(yvect*2.0), GetObjectY(OTHER) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(OTHER);

    while (++rep<5)
    {
        CreateObjectAtEx("ThrowingStone", GetObjectX(posUnit), GetObjectY(posUnit), &single);
        SetOwner(OTHER, single);
        SetUnitEnchantCopy(single, GetLShift(ENCHANT_SLOWED));
        SetUnitCallbackOnCollide(single, OnThrowingStoneCollide);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
    PlaySoundAround(OTHER, SOUND_UrchinThrow);
}

void OnThrowingFrogCollide()
{
    if (!CurrentHealth(SELF))
        return;

    while (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);
        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 173, DAMAGE_TYPE_POISON);
            break;
        }
        return;
    }
    float xpos=GetObjectX(SELF), ypos =GetObjectY(SELF);
    PlaySoundAround(SELF, SOUND_PoisonEffect);
    EnchantOff(SELF, "ENCHANT_INVULNERABLE");
    Damage(SELF, 0, 999, DAMAGE_TYPE_PLASMA);
    DeleteObjectTimer(CreateObjectAt("GreenSmoke", xpos, ypos), 18);
}

void ThrowingFrogTriggered()
{
    int frg=CreateObjectAt("GreenFrog", GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));

    SetUnitEnchantCopy(frg, GetLShift(ENCHANT_INVULNERABLE) | GetLShift(ENCHANT_HASTED) | GetLShift(ENCHANT_RUN));
    SetOwner(OTHER, frg);
    SetUnitFlags(frg, GetUnitFlags(frg) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    SetCallback(frg, 9, OnThrowingFrogCollide);
    LookWithAngle(frg, GetDirection(OTHER));
    SetUnitMaxHealth(frg, 500);
    DeleteObjectTimer(frg, 22);
    PushObject(frg, 200.0, GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(frg, SOUND_GlyphCast);
}

#define _AUTODETECT_SUBUNIT_ 0
#define _AUTODETECT_XVECT_ 1
#define _AUTODETECT_YVECT_ 2
#define _AUTODETECT_MAX_ 4

void OnAutoDetectorEnemySighted()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner))
    {
        Damage(OTHER, SELF, 92, DAMAGE_TYPE_ZAP_RAY);
        PlaySoundAround(OTHER, SOUND_DeathRayKill);
        Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    }
    if (IsObjectOn(GetTrigger()+1))
        Delete(GetTrigger() + 1);
}

void ConfirmAutoDetectingInvalidTarget(int *pMem)
{
    int sub1 = pMem[_AUTODETECT_SUBUNIT_];

    if (IsObjectOn(sub1))
    {
        float *vect = ArrayRefN(pMem, _AUTODETECT_XVECT_);
        float xpos = GetObjectX(sub1), ypos = GetObjectY(sub1);

        Delete(sub1);
        Effect("DEATH_RAY", xpos, ypos, xpos+(vect[0]*64.0), ypos+(vect[1]*64.0));
    }
    FreeSmartMemEx(pMem);
}

void AutoDetectingTriggered()
{
    float xvect=UnitAngleCos(OTHER, 3.0),yvect=UnitAngleSin(OTHER, 3.0);
    int sub = CreateObjectAt("WeirdlingBeast", GetObjectX(OTHER) + xvect, GetObjectY(OTHER) + yvect);
    int validate = CreateObjectAt("BlueSummons", GetObjectX(sub), GetObjectY(sub));

    int *pMem;
    AllocSmartMemEx(_AUTODETECT_MAX_*4, &pMem);
    FrameTimerWithArg(2, pMem, ConfirmAutoDetectingInvalidTarget);
    pMem[_AUTODETECT_SUBUNIT_]=sub+1;
    pMem[_AUTODETECT_XVECT_]=xvect;
    pMem[_AUTODETECT_YVECT_]=yvect;
    SetCallback(sub, 3, OnAutoDetectorEnemySighted);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, GetDirection(OTHER));
    SetUnitScanRange(sub, 420.0);
    DeleteObjectTimer(sub, 1);
    SetUnitFlags(sub, GetUnitFlags(sub) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    PlaySoundAround(OTHER, SOUND_FirewalkOff);
}

void OnSpiderWebCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsCaller(owner))
            return;

        if (IsAttackedBy(OTHER, owner))
            Damage(OTHER, owner, 155, DAMAGE_TYPE_POISON);
        Enchant(OTHER, "ENCHANT_SLOWED", 2.0);
    }
    WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
    Delete(SELF);
}

void TakeShotSpiderWeb()
{
    int web=CreateObjectAt("SpiderSpit", GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));

    SetOwner(OTHER, web);
    LookWithAngle(web, GetDirection(OTHER));
    SetUnitCallbackOnCollide(web, OnSpiderWebCollide);
    PushObject(web, 44.0, GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(web, SOUND_LargeSpiderSpit);
}

#define _MECAFLY_UNIT_ 0
#define _MECAFLY_COUNT_ 1
#define _MECAFLY_XVECT_ 2
#define _MECAFLY_YVECT_ 3
#define _MECAFLY_MAX_ 4

int CheckVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

void OnMecaFlierCollide()
{
    int owner = GetOwner(SELF);

    if (!owner)
        return;

    if (CurrentHealth(OTHER))
    {
        if (!IsAttackedBy(OTHER, owner))
            return;

        Damage(OTHER, owner, 185, DAMAGE_TYPE_EXPLOSION);
        Effect("SPARK_EXPLOSION",GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0 );
    }
    Frozen(SELF, FALSE);
    Damage(SELF, 0, 9999, 14);
    ClearOwner(SELF);
}

void OnMecaFlierLoop(int *pMem)
{
    int fly=pMem[_MECAFLY_UNIT_];

    if (CurrentHealth(fly))
    {
        int owner=GetOwner(fly), *count = ArrayRefN(pMem, _MECAFLY_COUNT_);

        if (CurrentHealth(owner) && CheckVisibleOr(owner, fly) && --count[0]>=0)
        {
            FrameTimerWithArg(1, pMem, OnMecaFlierLoop);

            float *vect=ArrayRefN(pMem, _MECAFLY_XVECT_);

            MoveObjectVector(fly, vect[0], vect[1]);
            return;
        }
        PlaySoundAround(fly, SOUND_PowderBarrelExplode);
        DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(fly), GetObjectY(fly)), 18);
        Delete(fly);
    }
    FreeSmartMemEx(pMem);
}

void TakeoffMecaFlying()
{
    float xvect = UnitAngleCos(OTHER, 13.0), yvect=UnitAngleSin(OTHER, 13.0);
    int fly=CreateObjectAt("FlyingGolem", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);

    Frozen(fly, TRUE);
    LookWithAngle(fly, GetDirection(OTHER));
    SetOwner(OTHER, fly);
    SetCallback(fly, 9, OnMecaFlierCollide);
    int *pMem;
    AllocSmartMemEx(_MECAFLY_MAX_*4, &pMem);
    pMem[_MECAFLY_YVECT_]=yvect;
    pMem[_MECAFLY_XVECT_]=xvect;
    pMem[_MECAFLY_UNIT_]=fly;
    pMem[_MECAFLY_COUNT_]=32;
    FrameTimerWithArg(1, pMem, OnMecaFlierLoop);
    PlaySoundAround(fly, SOUND_MechGolemPowerUp);
}

void TeleportHammerTriggered()
{
    int cursorX, cursorY;

    if (!GetPlayerMouseXY(OTHER, &cursorX, &cursorY))
        return;

    float cx=IntToFloat(cursorX), cy = IntToFloat(cursorY);

    if (!CheckCoorValidate(cx, cy))
        return;

    int validate = CreateObjectAt("ImaginaryCaster", cx,cy);

    if (!IsVisibleTo(OTHER, validate))
    {
        Delete(validate);
        UniPrint(OTHER, "커서 지역을 볼 수 없습니다, 작업이 완료되지 않았습니다");
        PlaySoundAround(OTHER, SOUND_NoCanDo);
        return;
    }
    float xpos=GetObjectX(OTHER),ypos=GetObjectY(OTHER);
    PlaySoundAround(OTHER, SOUND_InversionCast);
    Effect("TELEPORT", xpos,ypos, 0.0, 0.0);
    MoveObject(OTHER, GetObjectX(validate), GetObjectY(validate));
    PlaySoundAround(OTHER, SOUND_BlindOff);
    Effect("TELEPORT", GetObjectX(validate), GetObjectY(validate), 0.0, 0.0);
    Effect("LIGHTNING", xpos, ypos, GetObjectX(validate), GetObjectY(validate));
    Delete(validate);

    RestoreHealth(OTHER, 30);
    Effect("GREATER_HEAL", GetObjectX(OTHER),GetObjectY(OTHER),GetObjectX(OTHER),GetObjectY(OTHER) -100.0);

    int cureCount=5;

    while (--cureCount>=0)
    {
        if (IsPoisonedUnit(OTHER))
            CastSpellObjectObject("SPELL_CURE_POISON", OTHER,OTHER);
    }
}

#define _THROWINGPIXIE_GLOWPOINT 0
#define _THROWINGPIXIE_COUNT_ 1
#define _THROWINGPIXIE_MAX_ 2

void OnPixieCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsCaller(owner))
            return;

        if (IsAttackedBy(OTHER, owner))
            Damage(OTHER, owner, 14, DAMAGE_TYPE_DRAIN);
    }
    Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

int CreatePixie(float xpos, float ypos, int owner)
{
    int pix=CreateObjectAt("Pixie", xpos, ypos);
    int *ptr=UnitToPtr(pix);

    ptr[186]=5483536;       //projectile update
    SetUnitCallbackOnCollide(pix, OnPixieCollide);
    SetOwner(owner, pix);
    return pix;
}

void OnThrowingPixieLoop(int *pArgs)
{
    int point=pArgs[_THROWINGPIXIE_GLOWPOINT];

    if (IsObjectOn(point))
    {
        int owner = GetOwner(point), *count=ArrayRefN(pArgs, _THROWINGPIXIE_COUNT_);
        if (CurrentHealth(owner) && --count[0]>=0)
        {
            FrameTimerWithArg(1, pArgs, OnThrowingPixieLoop);
            float xvect=UnitRatioX(point, owner, 13.0), yvect=UnitRatioY(point, owner, 13.0);
            int pix=CreatePixie(GetObjectX(owner) + xvect, GetObjectY(owner) + yvect, owner);

            PushObjectTo(pix, xvect*3.0, yvect*3.0);
            return;
        }
        Delete(point);
    }
    FreeSmartMemEx(pArgs);
}

void StartContinuousPixie()
{
    int glowPoint = CreateObjectAt("Moonglow", GetObjectX(OTHER), GetObjectY(OTHER)), *args;

    SetOwner(OTHER, glowPoint);
    AllocSmartMemEx(_THROWINGPIXIE_MAX_*4, &args);
    args[_THROWINGPIXIE_GLOWPOINT]=glowPoint;
    args[_THROWINGPIXIE_COUNT_]=30;
    FrameTimerWithArg(1, args, OnThrowingPixieLoop);
    PlaySoundAround(glowPoint, SOUND_ShellMouseBoom);
}

 void collideBreakingPotion()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 240, DAMAGE_TYPE_CRUSH);
            Enchant(OTHER, EnchantList(ENCHANT_FREEZE), 3.0);
        }
    }
}

 void endThrowingPotion(int potionfx)
{
    float xpos=GetObjectX(potionfx), ypos= GetObjectY(potionfx);
    int owner = GetOwner(potionfx);

    CreateObjectAt("BreakingPotion", xpos, ypos);
    int dam=DummyUnitCreateAt("CarnivorousPlant", xpos, ypos);

    DeleteObjectTimer(dam, 1);
    SetOwner(owner, dam);
    SetCallback(dam, 9, collideBreakingPotion);
}

 void throwingPotionLoop(int subunit)
{
    while (IsObjectOn(subunit))
    {
        int owner = GetOwner(subunit + 2), durate = GetDirection(subunit);

        if (CurrentHealth(owner))
        {
            if (durate)
            {
                FrameTimerWithArg(1, subunit, throwingPotionLoop);
                MoveObjectVector(subunit + 2, GetObjectZ(subunit), GetObjectZ(subunit + 1));
                
                int angle = GetDirection(subunit + 2);
                Raise(subunit + 2, MathSine(angle * 6, 240.0));
                LookWithAngle(subunit + 2, ++angle);
                LookWithAngle(subunit, --durate);
                break;
            }
            else
            {
                endThrowingPotion(subunit + 2);
            }
            
        }
        Delete(subunit++);
        Delete(subunit++);
        Delete(subunit++);
        break;
    }
}

 void throwingStuffImpl(int caster, int target)
{
    float cdist = DistanceUnitToUnit(caster, target) / 30.0;
    float xvect = UnitRatioX(target, caster, cdist), yvect = UnitRatioY(target, caster, cdist);
    int subunit = CreateObjectAt("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster));

    Raise(CreateObjectAt("ImaginaryCaster", GetObjectX(caster), GetObjectY(caster)), yvect);
    UnitNoCollide(CreateObjectAt("BottleCandleUnlit", GetObjectX(subunit), GetObjectY(subunit)));
    Raise(subunit, xvect);
    SetOwner(caster, subunit + 2);
    LookWithAngle(subunit, 30);
    FrameTimerWithArg(1, subunit, throwingPotionLoop);
}

 void StartThrowingStuff()
{
    int mouseX, mouseY;

    GetPlayerMouseXY(OTHER, &mouseX, &mouseY);
    float cx=IntToFloat(mouseX), cy = IntToFloat(mouseY);

    if (!CheckCoorValidate(cx, cy))
        return;
    int sub = CreateObjectAt("RedPotion", cx,cy);
    throwingStuffImpl( OTHER, sub);
    Delete(sub);
}

 void dropRainSingle(float xpos, float ypos)
{
    int angle = Random(0, 359);
        float    rgap=RandomFloat(10.0, 200.0);
        int    arw= CreateObjectAt("CherubArrow", xpos+MathSine(angle+90, rgap),ypos+MathSine(angle,rgap));
            LookWithAngle(arw, 64);
            UnitNoCollide(arw);
            Raise(arw, 250.0);
            DeleteObjectTimer(arw, 20);
            PlaySoundAround(arw, SOUND_CrossBowShoot);
}

 void arrowRainLoop(int sub)
{
    float xpos=GetObjectX(sub),ypos=GetObjectY(sub);
    int dur;

    while (ToInt(xpos))
    {
        dur=GetDirection(sub);
        if (dur)
        {
            dropRainSingle(xpos, ypos);
            if (dur&1)                
                SplashDamageAt(GetOwner(sub), Random(1, 3), xpos, ypos, 190.0);
            FrameTimerWithArg(1, sub, arrowRainLoop);
            LookWithAngle(sub, dur-1);
            break;
        }
        WispDestroyFX(xpos, ypos);
        Delete(sub);
        break;
    }
}

 void startArrowRain()
{
    int ix,iy;

    if(! GetPlayerMouseXY(OTHER, &ix,&iy))
        return;
    
    float x=IntToFloat(ix), y=IntToFloat(iy);

    if (!CheckCoorValidate(x,y))
        return;

    int sub=CreateObjectAt("PlayerWaypoint", x,y);

    FrameTimerWithArg(1, sub, arrowRainLoop);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, 42);
    PlaySoundAround(sub, SOUND_MetallicBong);
}

 int placingSmallFist(float x, float y)
{
    int mark=CreateObjectAt("InvisibleLightBlueLow", x,y);
    CastSpellLocationLocation("SPELL_FIST", x-1.0,y,x,y);
    Delete(mark);
    return ++mark;
}

void StrikeFistLine()
{
    float xvect=UnitAngleCos(OTHER, 46.0), yvect=UnitAngleSin(OTHER, 46.0);
    int mark=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);
    int count = 12,fist;

    while (--count>=0)
    {
        if (!IsVisibleTo(OTHER, mark))
            break;
        fist=placingSmallFist(GetObjectX(mark), GetObjectY(mark));
        SetOwner(OTHER, fist);
        MoveObjectVector(mark, xvect, yvect);
    }
}

 void deferredEarthquake(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int owner=GetOwner(sub);
        float x=GetObjectX(sub),y=GetObjectY(sub);
        if (CurrentHealth(owner))
        {
            SplashDamageAt(owner, 165, x,y, 240.0);
            GreenExplosion(x,y);
            CastSpellLocationLocation("SPELL_TURN_UNDEAD",x,y,x,y);
            CastSpellObjectObject("SPELL_PUSH", sub, sub);
        }
        Delete(sub);
    }
}

 void strikeEarthquake()
{
    int sub=CreateObjectAt("PlayerWaypoint", GetObjectX(OTHER),GetObjectY(OTHER));

    SetOwner(OTHER, sub);
    FrameTimerWithArg(24, sub,deferredEarthquake);
    PlaySoundAround(sub, SOUND_TelekinesisOn);
}

#define PUSH_KILLERIAN_DM 0
#define PUSH_KILLERIAN_COUNT 1
#define PUSH_KILLERIAN_XVECT 2
#define PUSH_KILLERIAN_YVECT 3
#define PUSH_KILLERIAN_MAX 4

void onPushKillerianLoop(int *p)
{
    int count=p[PUSH_KILLERIAN_COUNT];
    int dm=p[PUSH_KILLERIAN_DM];

    if (count)
    {
        float xvect= p[PUSH_KILLERIAN_XVECT], yvect=p[PUSH_KILLERIAN_YVECT];
        int sub=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(dm)+xvect,GetObjectY(dm)+yvect);

        DeleteObjectTimer(sub, 1);
        if (IsVisibleTo(dm, sub))
        {
            FrameTimerWithArg(1, p, onPushKillerianLoop);
            MoveObjectVector(dm,xvect, yvect);
            --p[PUSH_KILLERIAN_COUNT];
            return;
        }
    }
    Frozen(dm,FALSE);
    ObjectOn(dm);
    FreeSmartMemEx(p);
}

void PushKillerian()
{
    int cx,cy;

    if(! GetPlayerMouseXY(OTHER, &cx, &cy))
        return;

    float fromPoint[]={GetObjectX(OTHER), GetObjectY(OTHER)};
    float toPoint[]={ IntToFloat(cx),IntToFloat(cy) };
    float vect[2];
    float scale = Distance(fromPoint[0], fromPoint[1], toPoint[0], toPoint[1]);

    ComputePointRatio(  toPoint,fromPoint, vect, scale/9.0);
    int *m;

    AllocSmartMemEx(PUSH_KILLERIAN_MAX*4, &m);
    FrameTimerWithArg(1, m, onPushKillerianLoop);
    int dm=DummyUnitCreateAt("Demon", fromPoint[0], fromPoint[1]);
    LookWithAngle(dm,GetDirection(OTHER));
    SetOwner(OTHER,dm);
    SetUnitFlags(dm, GetUnitFlags(dm)^UNIT_FLAG_NO_COLLIDE_OWNER);
    m[PUSH_KILLERIAN_COUNT]=9;
    m[PUSH_KILLERIAN_DM]=dm;
    m[PUSH_KILLERIAN_XVECT]=vect[0];
    m[PUSH_KILLERIAN_YVECT]=vect[1];
}

void showChainArrowSingle(int posUnit)
{
    int arrow=CreateObjectAt("WeakArcherArrow", GetObjectX(posUnit), GetObjectY(posUnit));

    SetOwner(GetOwner(posUnit), arrow);
    LookWithAngle(arrow, GetDirection(posUnit));
    PushObjectTo(arrow, UnitAngleCos(posUnit, 33.0), UnitAngleSin(posUnit, 33.0));
}

 void onChainArrow(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int count = GetObjectZ(sub);

        if (count)
        {
            int owner = GetOwner(sub);
            if (CurrentHealth(owner))
            {
                FrameTimerWithArg(1, sub, onChainArrow);
                showChainArrowSingle(sub);
                Raise(sub, count-1);
                return;
            }
        }
        Delete(sub);
    }
}

void CastingChainArrow()
{
    float xvect=UnitAngleCos(OTHER, 13.0), yvect=UnitAngleSin(OTHER, 13.0);
    int sub=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER)+xvect, GetObjectY(OTHER)+yvect);

    FrameTimerWithArg(1, sub, onChainArrow);
    SetOwner(OTHER, sub);
    LookWithAngle(sub, GetDirection(OTHER));
    Raise(sub, 24);
}

void ForceOfNatureCollide()
{
    if (!GetTrigger())
        return;

    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 77, DAMAGE_TYPE_POISON);
            Effect("RICOCHET", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            Delete(SELF);
        }
    }
}

void CastingBlueTail()
{
    CastSpellObjectLocation("SPELL_SLOW", OTHER, GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));
    int *uptr = 0x750710;

    if (uptr[0] == NULLPTR)
        return;
    int *ptr = uptr[0];
    int mis = ptr[11];
    //SetMemory(ptr + 0x2e8, 5483536); //projectile update

    PlaySoundAround(OTHER, SOUND_ForceOfNatureRelease);
    Enchant(mis, "ENCHANT_RUN", 0.0);
    Enchant(mis, "ENCHANT_HASTED", 0.0);
    SetUnitCallbackOnCollide(mis, ForceOfNatureCollide);
}

 void stopChainLightning(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        WispDestroyFX(GetObjectX(sub), GetObjectY(sub));
        Delete(sub);
        Delete(sub+1);
    }
}

 void doChainLightning(int owner)
{
    float x=GetObjectX(owner),y=GetObjectY(owner);
    int sub=CreateObjectAt("MovableStatueVictory4S", x,y);
    int to= CreateObjectAt("Orb", x+UnitAngleCos(owner, 14.0),y+UnitAngleSin(owner,14.0));

    LookWithAngle(sub, GetDirection(owner));
    SecondTimerWithArg(3, sub, stopChainLightning);
    Frozen(to, TRUE);
    UnitNoCollide(sub);
    UnitNoCollide(to);
    SetOwner(owner, sub);
    CastSpellObjectObject("SPELL_CHAIN_LIGHTNING", sub, to);
}

 void startChainLightning()
{
    if (!UnitCheckEnchant(OTHER,GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    {
        Enchant(OTHER, EnchantList(ENCHANT_PROTECT_FROM_MAGIC), 1.5);
        doChainLightning(OTHER);
    }
}

void Entity2WindStrikeCollide()
{
    if (!GetTrigger())
        return;
    int owner=GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 150, DAMAGE_TYPE_CRUSH);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.1);
    }
}

#define _ENTITY2_WIND_STRIKE_MARK_ 0
#define _ENTITY2_WIND_STRIKE_XVECT_ 1
#define _ENTITY2_WIND_STRIKE_YVECT_ 2
#define _ENTITY2_WIND_STRIKE_COUNT_ 3
#define _ENTITY2_WIND_STRIKE_CASTER_ 4
#define _ENTITY2_WIND_STRIKE_MAX_ 5

void Entity2WindStrikeLoop(int *pArgs)
{
    int caster=pArgs[_ENTITY2_WIND_STRIKE_CASTER_];

    if (CurrentHealth(caster))
    {
        int mark=pArgs[_ENTITY2_WIND_STRIKE_MARK_], *counter=ArrayRefN(pArgs, _ENTITY2_WIND_STRIKE_COUNT_);

        if (IsVisibleTo(mark, mark+1) && counter[0])
        {
            --counter[0];
            float xvect=pArgs[_ENTITY2_WIND_STRIKE_XVECT_], yvect=pArgs[_ENTITY2_WIND_STRIKE_YVECT_];

            MoveObjectVector(mark, xvect, yvect);
            MoveObjectVector(mark+1, xvect, yvect);
            int dum=DummyUnitCreateAt("Demon", GetObjectX(mark), GetObjectY(mark));

            SetOwner(caster, dum);
            DeleteObjectTimer(dum, 1);
            SetCallback(dum, 9, Entity2WindStrikeCollide);

            int fx=CreateObjectAt("WhirlWind", GetObjectX(mark), GetObjectY(mark));

            DeleteObjectTimer(fx, 18);
            PlaySoundAround(fx, SOUND_SwordsmanAttackInit);
            FrameTimerWithArg(1, pArgs, Entity2WindStrikeLoop);
            return;
        }
    }
    Delete(pArgs[_ENTITY2_WIND_STRIKE_MARK_]);
    Delete(pArgs[_ENTITY2_WIND_STRIKE_MARK_]+1);
    FreeSmartMemEx(pArgs);
}

void Entity2WindStrike(int entity)
{
    float xvect=UnitAngleCos(entity, 14.0), yvect=UnitAngleSin(entity, 14.0);
    int mark = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(entity) + xvect, GetObjectY(entity) + yvect);
    int visCheck=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(entity)-xvect, GetObjectY(entity)-yvect);

    int *args;

    AllocSmartMemEx(_ENTITY2_WIND_STRIKE_MAX_*4, &args);
    args[_ENTITY2_WIND_STRIKE_COUNT_]=36;
    args[_ENTITY2_WIND_STRIKE_MARK_]=mark;
    args[_ENTITY2_WIND_STRIKE_XVECT_]=xvect;
    args[_ENTITY2_WIND_STRIKE_YVECT_]=yvect;
    args[_ENTITY2_WIND_STRIKE_CASTER_]=entity;
    FrameTimerWithArg(1, args, Entity2WindStrikeLoop);
}

void EntityStartWindStrike()
{
    Entity2WindStrike(GetCaller());
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

void onHealthbarProgress(int t)
{
    int owner=GetOwner(t);

    if (MaxHealth(t)){
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,t,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (GetUnit1C(t)!=perc)
            {
                controlHealthbarMotion(t,perc);
                SetUnit1C(t,perc);
            }
            return;
        }
    }
    Delete(t);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));

    UnitNoCollide(bar);
    SetOwner(unit,bar);
    SetUnit1C(bar,0);
    PushTimerQueue(1,bar,onHealthbarProgress);
}
