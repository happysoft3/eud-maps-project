
#include "libs\define.h"
#include "libs\printutil.h"
#include "libs\unitstruct.h"
#include "libs\mathlab.h"
#include "libs\fxeffect.h"
#include "libs\waypoint.h"
#include "libs\observer.h"
#include "libs\reaction.h"
#include "libs\weaponcapacity.h"
#include "libs\opcodehelper.h"
#include "libs\playerinfo.h"

int KING_FLAG = 0, KingPtr;
int player[20], START_SW, KING_SRC, PLR_SRC;

#define KING_SPEED 4.0 //5.0

#define PLAYER_DEATH_FLAG 0x80000000

int PlayerClassDeathFlagCheck(int plr)
{
    return player[plr + 10] & PLAYER_DEATH_FLAG;
}

void PlayerClassDeathFlagSet(int plr)
{
    player[plr + 10] ^= PLAYER_DEATH_FLAG;
}

int MaidenBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void InitRelativeKing()
{
    KingPtr = CreateObject("InvulnerabilityPotion", 86);
    CreateObject("InvulnerabilityPotion", 87);
    Enchant(CreateObject("InvulnerabilityPotion", 88), "ENCHANT_RUN", 0.0);
    Enchant(KingPtr, "ENCHANT_VAMPIRISM", 0.0);
    Enchant(KingPtr + 1, "ENCHANT_VAMPIRISM", 0.0);
    Enchant(CreateObject("InvisibleLightBlueHigh", 15), "ENCHANT_VAMPIRISM", 0.0);
    Enchant(CreateObject("InvisibleLightBlueHigh", 85), "ENCHANT_VAMPIRISM", 0.0);
    Enchant(CreateObject("InvisibleLightBlueHigh", 77), "ENCHANT_VAMPIRISM", 0.0);
}

void MapInitialize()
{
    GetMasterUnit();
    MusicEvent();
    InitRelativeKing();
    HPBarPos(0);

    int unit = CreateObject("Magic", 67);
    Frozen(unit, 1);
    SetUnitSubclass(unit, GetUnitSubclass(unit) ^ 1);
    CreateObject("AirshipCaptain", 68);
    Frozen(unit + 1, 1);
    LookWithAngle(unit + 1, 64);
    
    Enchant(CreateObject("SulphorousFlareWand", 74), "ENCHANT_SLOWED", 0.0);
    Enchant(CreateObject("SulphorousFlareWand", 75), "ENCHANT_SLOWED", 0.0);

    //Loop_run
    FrameTimer(10, PlayerClassOnLoop);
    FrameTimer(50, LoopSearchIndex);

    //Delay_run
    FrameTimer(1, StrSelectBoss);
    FrameTimer(1, PutPotions);
    FrameTimerWithArg(30, unit + 1, GameStandBy);
    FrameTimerWithArg(90, 91, InitRainyDay);
    SecondTimerWithArg(2, 0, StartMent);

    RegistSignMessage(Object("mapSign1"), "보스 유닛으로 시작하려면 캡틴 아메리카를 클릭하여라");
}

void MapExit()
{
    MusicEvent();
}

void StartMent(int num)
{
    string table[] = {
        "한놈만 다굴까기                                                                                               -제작. 237",
        "게임방법: 플레이어 중 한명은 보스 몬스터가 되어야 합니다, 나머지 플레이어는 보스 몬스터를 집중적으로 다굴치세요                 ",
        "해당 맵을 플레이 할 때에는 플레이어 전부 같은 팀으로 배치 후 플레이 하세요, 보스를 고르신 플레이어는 자동으로 적으로 간주 됩니다",
        "보스로 플레이 중인 유저는 관객모드 전환을 통해 보스 자리를 다른 플레이어에게 양도할 수 있습니다                               ",
        "이동하려는 지역을 클릭하면 보스유닛을 이동시킬 수 있습니다, 보스유닛은 3개의 마법을 부릴 수 있습니다",
        "보스가 사용하는 마법 중 가장 주의해야 하는 마법 하나는 '자동 타겟 데스레이' 입니다 (캐릭터 전방 시야 내 전 유닛 타겟대상 -부채꼴)",
        "이모티콘 버튼 J, K, L 을 누르면 마법이 발동되며 각 마법마다 고유한 재사용 대기시간이 존재합니다",
        "보스 유닛의 최대체력은 240 입니다!!",
        "이상으로 게임설명을 마칩니다, 즐거운 시간 되십시오 (PC방 해택 적용)"};
    if (num < sizeof(table))
    {
        UniPrintToAll(table[num]);
        SecondTimerWithArg(6, ++num, StartMent);
    }
}

void GameStandBy(int ptr)
{
    int unit = CreateObject("AirshipCaptain", 69);

    Frozen(unit, 1);
    LookWithAngle(unit, 64);
    SetDialog(unit, "NORMAL", ClickGoToBack, DummyFunction);
    START_SW = Object("StartSwitch");
    UniChatMessage(ptr, "게임을 하려면 나에게 오라!", 150);
    SetDialog(ptr, "NORMAL", ClickCaptain, DummyFunction);
    AudioEvent("SwordsmanTaunt", 68);
    TeleportLocation(68, GetObjectX(START_SW), GetObjectY(START_SW));
}

void DummyFunction()
{
    return;
}

void ClickCaptain()
{
    if (HasEnchant(OTHER, "ENCHANT_CROWN"))
        EnchantOff(OTHER, "ENCHANT_CROWN");
    Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
    MoveObject(OTHER, LocationX(66), LocationY(66));
    UniPrint(OTHER, "킹을 하시려면 중앙 비콘을 밟으세요");
}

void ClickGoToBack()
{
    if (CurrentHealth(GetOwner(KingPtr)))
    {
        UniChatMessage(SELF, "건투를 비네", 120);
        MoveObject(OTHER, LocationX(68), LocationY(68));
    }
    else
        UniChatMessage(SELF, "아직 킹이 정해지지 않았어, 나도 알아 킹이 힘들다는거... 하지만 누군가는 킹 역할을 해줘야 만 해... 그래야 이 게임은 시작되니까!", 120);
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    UniPrintToAll(PlayerIngameNick(pUnit) + " 님께서 입장하십니다");

    return plr;
}

void StartPlayer()
{
    int k, plr;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            if (IsCaller(GetOwner(KingPtr))) //is_King
            {
                KingEntry(GetOwner(KingPtr));
                break;
            }
            plr = CheckPlayer();
            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    plr = PlayerClassOnInit(k, GetCaller());
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerEntry(plr);
                break;
            }
        }
        CantJoin();
        break;
    }
}

void PlayerEntry(int plr)
{
    if (PlayerClassDeathFlagCheck(plr))
        PlayerClassDeathFlagSet(plr);

    int rnd_loc = Random(50, 63);
    
    if (HasEnchant(player[plr], "ENCHANT_CROWN"))
        EnchantOff(player[plr], "ENCHANT_CROWN");
    //SetMemory(player[plr + 20] + 0x34, 1); //1번 팀 등록 --Not use
    MoveObject(player[plr], LocationX(rnd_loc), LocationY(rnd_loc));
    Effect("TELEPORT", LocationX(rnd_loc), LocationY(rnd_loc), 0.0, 0.0);
}

void KingEntry(int unit)
{
    int rnd_loc = Random(50, 63);

    KING_FLAG = 1;
    MoveObject(unit, LocationX(70), LocationY(70));
    SetOwner(CreateKing(unit, rnd_loc), KingPtr + 1); //ST
    Enchant(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.6);
}

int CreateKing(int owner, int wp)
{
    int unit = ColorMaiden(128, 16, 225, wp);

    KingDetailSetting(unit);
    SetOwner(owner, CreateObject("InvisibleLightBlueHigh", wp));
    CreateObject("InvisibleLightBlueHigh", wp);
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    //SetOwner(owner, unit);
    //GiveUnit(owner, unit);
    Enchant(unit, "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
    SetCallback(unit, 5, CreatureDead);
    SetCallback(unit, 9, UsePassPotion);
    SetUnitHealthBar(unit, 73);
    return unit;
}

void deferredNoAi(int unit)
{
    UnitLinkBinScript(unit, 0);
}

void KingDetailSetting(int unit)
{
    SetUnitMaxHealth(unit, 225);
    UnitLinkBinScript(unit, MaidenBinTable());
    // SetUnitSpeed(unit, 2.5);
    // FrameTimerWithArg(1, unit, deferredNoAi);
    UniPrintToAll(IntToString(UnitToPtr(unit)));
}

void FreeKingUnitPtr(int unit)
{
    if (CurrentHealth(unit))
        Delete(unit);
    Delete(unit + 1);
    Delete(GetOwner(unit + 2) + 1);
    Delete(GetOwner(unit + 2));
    Delete(unit + 2);
}

void CreatureDead()
{
    int kill = GetKillCredit(), plr;

    Damage(GetOwner(GetTrigger() + 1), 0, 999, 14);
    FreeKingUnitPtr(GetTrigger());
    if (!IsPlayerUnit(kill))
        kill = GetOwner(kill);
    if (IsPlayerUnit(kill))
    {
        UniChatMessage(kill, "내가 보스 죽임 ㅋㅋㅋ", 100);
        ChangeScore(kill, 1);
        AddScorePlayer();
        //PLR_SRC ++; //이거 넣으면 팅김...
    }
    DeleteObjectTimer(SELF, 180);
}

void AddScorePlayer()
{
    PLR_SRC ++;
}

void CantJoin()
{
    UniPrint(OTHER, "맵 입장실패_ 맵에 플레이어가 너무 많습니다");
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    MoveObject(OTHER, 1415.0, 1393.0);
}

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnAlive(int plr, int pUnit)
{ }

void PlayerClassOnDeath(int plr)
{
    int pUnit = player[plr];

    UniPrintToAll(PlayerIngameNick(pUnit) + " 님께서 적에게 격추되었습니다");
    AddScoreToKing(pUnit);
}

void PlayerClassOnLoop()
{
    int u;

    for (u = 0 ; u < 10 ; u += 1)
    {
        while (true)
        {
            if (MaxHealth(player[u]))
            {
                if (GetUnitFlags(player[u]) & 0x40)
                    1;
                else if (CurrentHealth(player[u]))
                {
                    PlayerClassOnAlive(u, player[u]);
                    break;
                }
                else
                {
                    if (!PlayerClassDeathFlagCheck(u))
                    {
                        PlayerClassDeathFlagSet(u);
                        PlayerClassOnDeath(u);
                    }
                    break;
                }
                
            }
            if (player[u + 10])
                PlayerClassOnExit(u);
            break;
        }
    }

    FrameTimer(1, PlayerClassOnLoop);
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void TeleportAllPlayers(int wp)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
        {
            MoveObject(player[k], LocationX(wp), LocationY(wp));
            player[k] = 0;
        }
    }
}

void SelectKingPlayer()
{
    int ptr;
    
    if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
    {
        SetOwner(OTHER, KingPtr);
        ptr = GetMemory(0x979720);
        if (!(GetOwner(KingPtr) ^ GetCaller()))
        {
            Raise(KingPtr, ToFloat(ptr)); //KING = ptr
            ObjectOff(SELF);
            UniPrintToAll(PlayerIngameNick(OTHER) + "님께서 킹을 고르셨습니다");
            UniChatMessage(OTHER, "내가 킹이다!", 120);
            ObserverPlayerCameraLock(OTHER);
            Enchant(OTHER, "ENCHANT_INFRAVISION", 0.0);
            KING_SRC = 0;
            PLR_SRC = 0;
            KingEntry(GetOwner(KingPtr));
            KingStartSw(GetTrigger());
            ObjectOn(START_SW);
            FrameTimerWithArg(1, KingPtr, LoopKingStatus);
        }
        else
            UniPrint(OTHER, "로직 에러가 났습니다");
    }
    else
        UniPrint(OTHER, "죄송합니다, 킹은 오직 전사만 가능합니다");
}

void KingStartSw(int ptr)
{
    int sw;
    if (!sw)
        sw = ptr;
    else
    {
        ObjectOn(sw);
        sw = 0;
    }
}

void LoopKingStatus(int p)
{
    int glow, ptr = ToInt(GetObjectZ(p)), cre = GetOwner(p + 1), unit = GetOwner(p);

    if (MaxHealth(unit))
    {
        if (GetMemory(ptr + 0x10) & 0x40)
            SetOwner(p + 2, p);
        else if (CurrentHealth(unit))
        {
            if (CurrentHealth(cre))
            {
                if (CheckWatchFocus(unit))
                {
                    PlayerLook(unit, cre);
                    BossDefaultAttack(p);
                }
                else
                {
                    int act = CheckPlayerInput(unit);
                    MovingPlayerCreature(p, act);
                    if (act == 49)
                        AutoTargetDeathray(unit, cre);
                    else if (act == 48)
                        CrashAround(unit, cre);
                    else if (act == 47)
                        StrikeTrackingMis(unit, cre);
                }
            }
        }
        FrameTimerWithArg(1, p, LoopKingStatus);
    }
    else
    {
        FreeKingUnitPtr(cre);
        UniPrintToAll("킹이 사라졌습니다, 킹을 다시 선택합니다");
        KING_FLAG = 0;
        KingStartSw(0);
        ObjectOff(START_SW);
        FrameTimerWithArg(30, 68, TeleportAllPlayers);
    }
}

void MovingPlayerCreature(int p, int keyHandler)
{
    int cre = GetOwner(p + 1), owner = GetOwner(p);

    if (!(keyHandler ^ 0x02))
    {
        GoCreatureMove(cre, owner);
    }
    else
        StopCreature(cre);
}

void GetRatioDetail(float *src, float *dest, float size)
{
    float d = Distance(src[0], src[1], src[2], src[3]);
    dest[0] = ((src[0]-src[2])*size)/d;
    dest[1] = ((src[1]-src[3])*size)/d;
}

void GoCreatureMove(int cre, int owner)
{
    float point[] = {IntToFloat(GetPlayerMouseX(owner)), IntToFloat(GetPlayerMouseY(owner)),
        GetObjectX(cre), GetObjectY(cre)};
    float ratio[2];

    GetRatioDetail(&point, &ratio, 1.0);
    if (CurrentHealth(cre))
    {
        Walk(cre, GetObjectX(cre)+ratio[0], GetObjectY(cre)+ratio[1]);
        PushObjectTo(cre, ratio[0]*KING_SPEED, ratio[1]*KING_SPEED);
    }
}

void StopCreature(int cre)
{
    CreatureIdle(cre);
}

void BossDefaultAttack(int p)
{
    int glow;
    
    if (!(CheckPlayerInput(GetOwner(p)) ^ 0x06))
    {
        glow = CreateObject("Moonglow", 1);

        SetOwner(GetOwner(p), glow);
        SetUnit1C(glow, p);
        FrameTimerWithArg(1, glow, ShockWave);
    }
}

void ShockWave(int glow)
{
    int link = GetUnit1C(glow);
    int unit = GetOwner(glow), cre = GetOwner(link + 1);

    if (CurrentHealth(unit) && CurrentHealth(cre))
    {
        float xvect = UnitRatioX(glow, cre, 13.0), yvect = UnitRatioY(glow, cre, 13.0);
        int mis = CreateObjectAt("DeathBallFragment", GetObjectX(cre) + xvect, GetObjectY(cre) + yvect);

        SetOwner(cre, mis);
        GreenSparkFxAt(GetObjectX(mis), GetObjectY(mis));
        Enchant(mis, "ENCHANT_SHOCK", 0.0);
        PushObjectTo(mis, xvect * 3.0, yvect * 3.0);
    }
    Delete(glow);
}

void GoTarget(int glow)
{
	int mark, owner = GetOwner(glow), cre = GetOwner(glow + 1);

	if (CurrentHealth(owner) && !HasEnchant(owner, "ENCHANT_PROTECT_FROM_ELECTRICITY"))
	{
		if (CurrentHealth(cre))
        {
            mark = CreateObjectAt("TeleportGlyph1", GetObjectX(glow), GetObjectY(glow));
            CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(mark), GetObjectY(mark));
            if (IsObjectOn(ToInt(GetObjectZ(cre + 1))))
            {
                Delete(ToInt(GetObjectZ(cre + 1)));
                Delete(ToInt(GetObjectZ(cre + 1)) + 1);
            }
            Raise(cre + 1, ToFloat(mark));
            SetOwner(owner, mark);
            SetOwner(cre, mark + 1);
            SetOwner(mark, cre + 2);
			LookAtObject(cre, mark);
            FrameTimerWithArg(1, mark, OrderToMove);
        }
	}
	Delete(glow++);
    Delete(glow++);
}

void OrderToMove(int ptr)
{
    int cre = GetOwner(ptr + 1);

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(cre))
        {
            if (Distance(GetObjectX(cre), GetObjectY(cre), GetObjectX(ptr), GetObjectY(ptr)) > 17.0)
            {
                LookAtObject(cre, ptr);
                Walk(cre, GetObjectX(cre), GetObjectY(cre));
                MoveObject(cre, GetObjectX(cre) + UnitRatioX(ptr, cre, 8.0), GetObjectY(cre) + UnitRatioY(ptr, cre, 8.0));
                FrameTimerWithArg(1, ptr, OrderToMove);
            }
            else
            {
                CreatureIdle(cre);
                LookAtObject(cre, ptr);
                Delete(ptr ++);
                Delete(ptr ++);
            }
        }
    }
}

void KingDeadMotion(int parent)
{
    int unit = CreateObjectAt("Demon", GetObjectX(parent), GetObjectY(parent));

    LookWithAngle(unit, GetDirection(parent));
    Damage(unit, 0, 999, 14);
}

void DetectedSpecificIndex(int curId)
{
    if (!IsMissileUnit(curId))
        return;

    int thingId = GetUnitThingID(curId);

    if (thingId == 709)
        ShotMagicMissile(GetOwner(curId), curId);
    else if (thingId == 706)
    {
        ShotSmallDeathBall(GetOwner(curId));
        Delete(curId);
    }
    else if (thingId == 1179)
        ShurikenEvent(GetOwner(curId), curId);
}

void LoopSearchIndex()
{
    int curId, tempId;
    int *ptr = 0x750710;

    ptr = ptr[0];
    while (ptr != NULLPTR)
    {
        tempId = ptr[11];
        if (curId)
        {
            while (curId < tempId)
            {
                curId += 1;
                DetectedSpecificIndex(curId);
            }
            break;
        }
        curId = tempId;
        break;
    }
    FrameTimer(1, LoopSearchIndex);
}

void ShurikenEvent(int owner, int cur)
{
    if (CurrentHealth(owner))
    {
        int mis = CreateObjectAt("WeakFireball", GetObjectX(cur), GetObjectY(cur));

        SetOwner(owner, mis);
        PushObjectTo(mis, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
    }
}

void ShotMagicMissile(int owner, int cur)
{
    if (CurrentHealth(owner))
    {
        int mis = CreateObjectAt("LightningBolt", GetObjectX(cur), GetObjectY(cur));

        SetOwner(owner, mis);
        LookWithAngle(mis, GetDirection(owner));
        PushObjectTo(mis, UnitRatioX(cur, owner, 32.0), UnitRatioY(cur, owner, 32.0));
    }
    Delete(cur);
}

void DeathChunkTouch()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner) && CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        DeleteObjectTimer(CreateObjectAt("GreenSmoke", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
        Damage(OTHER, owner, 150, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.3);
    }
}

void FlyingDeathball(int subUnit)
{
    int count = GetDirection(subUnit), owner = GetOwner(subUnit);

    if (CurrentHealth(owner) && count < 30)
    {
        if (IsVisibleTo(owner, subUnit))
        {
            MoveObjectVector(subUnit, GetObjectZ(subUnit), GetObjectZ(subUnit + 1));
            LookWithAngle(subUnit, ++count);
            Effect("SENTRY_RAY", GetObjectX(subUnit + 1), GetObjectY(subUnit + 1), GetObjectX(subUnit), GetObjectY(subUnit));

            int unit = CreateObjectAt("ShopkeeperConjurerRealm", GetObjectX(subUnit), GetObjectY(subUnit));

            SetOwner(owner, unit);
            Frozen(unit, 1);
            SetCallback(unit, 9, DeathChunkTouch);
            DeleteObjectTimer(unit, 1);
        }
        else
            LookWithAngle(subUnit, 200);
        FrameTimerWithArg(1, subUnit, FlyingDeathball);
    }
    else
    {
        Delete(subUnit++);
        Delete(subUnit++);
    }
}

void ShotSmallDeathBall(int owner)
{
    if (CurrentHealth(owner))
    {
        float x_vect = UnitAngleCos(owner, 20.0), y_vect = UnitAngleSin(owner, 20.0);
        int subUnit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) + x_vect, GetObjectY(owner) + y_vect);

        SetOwner(owner, subUnit);
        Raise(subUnit, x_vect);
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(subUnit), GetObjectY(subUnit)), y_vect);
        FrameTimerWithArg(1, subUnit, FlyingDeathball);
    }
}

void ShotDeathRay(int glow)
{
    int cre = GetOwner(glow + 1);

    if (CurrentHealth(cre))
    {
        int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(cre), GetObjectY(cre));
        UnitNoCollide(unit);
        SetOwner(cre, unit);
        SetCallback(unit, 3, TakeShotRay);
        LookAtObject(unit, glow);
        SetUnitScanRange(unit, 600.0);
        DeleteObjectTimer(unit, 1);
        GreenSparkFxAt(GetObjectX(cre) + UnitRatioX(glow, cre, 20.0), GetObjectY(cre) + UnitRatioY(glow, cre, 20.0));
        PlaySoundAround(unit, 594);
    }
    Delete(glow++);
    Delete(glow++);
}

void AutoTargetDeathray(int owner, int cre)
{
    if (CurrentHealth(owner) && CurrentHealth(cre))
    {
        if (!HasEnchant(owner, "ENCHANT_ANTI_MAGIC"))
        {
            int plr_ptr = UnitToPtr(owner);
            Enchant(owner, "ENCHANT_ANTI_MAGIC", 9.0);
            SecondTimerWithArg(9, SetSkillCooldownNofication(owner, plr_ptr, 0), NotifySkillCooldown);
            int glow = CreateObject("Moonglow", 22);
            SetOwner(cre, CreateObject("InvisibleLightBlueHigh", 22));
            SetOwner(owner, glow);
            UniChatMessage(cre, "마법시전:\n자동 타게팅 데스레이!!", 108);
            FrameTimerWithArg(1, glow, ShotDeathRay);
        }
    }
}

void CrashAround(int owner, int cre)
{
    if (CurrentHealth(owner) && CurrentHealth(cre))
    {
        if (!HasEnchant(owner, "ENCHANT_HASTED"))
        {
            int plr_ptr = UnitToPtr(owner);
            CastSpellObjectObject("SPELL_TURN_UNDEAD", cre, cre);
            TeleportLocation(46, GetObjectX(cre), GetObjectY(cre));
            SplashDamage(cre, 100, 200.0, 46);
            Enchant(owner, "ENCHANT_HASTED", 5.0);
            SecondTimerWithArg(5, SetSkillCooldownNofication(owner, plr_ptr, 1), NotifySkillCooldown);
            UniChatMessage(cre, "마법시전:\n화이트 비아풀!!", 108);
        }
    }
}

void TakeShotRay()
{
    CastSpellObjectObject("SPELL_DEATH_RAY", GetOwner(SELF), OTHER);
}

void PoisonBrass(int parent)
{
    if (CurrentHealth(parent) && !HasEnchant(parent, "ENCHANT_ANCHORED"))
    {
        UniChatMessage(parent, "내 독에 범벅이 되어 죽어라!", 108);
        Enchant(parent, "ENCHANT_ANCHORED", 15.0);
        int base = CreateObject("InvisibleLightBlueLow", 22);
        CastSpellObjectLocation("SPELL_FREEZE", parent, GetObjectX(parent) + UnitAngleCos(parent, 50.0), GetObjectY(parent) + UnitAngleSin(parent, 50.0));
        Enchant(base + 1, "ENCHANT_RUN", 0.0);
        Enchant(base + 1, "ENCHANT_HASTED", 0.0);
        Enchant(base + 1, "ENCHANT_INFRAVISION", 0.0);
        Enchant(base + 1, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
        Enchant(base + 1, "ENCHANT_SHOCK", 0.0);
        Delete(base);
    }
}

void MissileAttack(int parent)
{
    if (CurrentHealth(parent))
    {
        int mis = CreateObjectAt("StrongFireball", GetObjectX(parent) + UnitAngleCos(parent, 19.0), GetObjectY(parent) + UnitAngleSin(parent, 19.0));

        SetOwner(parent, mis);
        LookWithAngle(mis, GetDirection(parent));
        PushObject(mis, 60.0, GetObjectX(parent), GetObjectY(parent));
        DeleteObjectTimer(mis, 24);
    }
}

void EmptyAll(int unit)
{
    while (IsObjectOn(GetLastItem(unit)))
        Delete(GetLastItem(unit));
}

void AutoPickWeapon(int parent)
{
    int weapon;

    if (!weapon)
    {
        weapon = CreateObject("FanChakram", 23);

        SetConsumablesWeaponCapacity(weapon, 255, 255);
        FrameTimerWithArg(1, parent, AutoPickWeapon);
    }
    else
    {
        if (CurrentHealth(parent))
            Pickup(parent, weapon);
        else
            Delete(weapon);
        weapon = 0;
    }
}

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr1 = GetMemory(0x750710), k;

    SetMemory(ptr1 + 4, 1385);
    for (k = 0 ; k < 32 ; k += 1)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetUnitVoice(unit, 7);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);

    return unit;
}

void SplashDamage(int owner, int dam, float range, int wp)
{
    int baseunit = CreateObject("InvisibleLightBlueHigh", wp) + 1, k;

    SetOwner(owner, baseunit - 1);
    MoveObject(baseunit - 1, range, GetObjectY(baseunit - 1));
    Raise(baseunit - 1, ToFloat(dam));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObject("WeirdlingBeast", wp), 1);
        UnitNoCollide(baseunit + k);
        LookWithAngle(baseunit + k, k * 32);
        SetOwner(baseunit - 1, baseunit + k);
        SetCallback(baseunit + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(baseunit - 1, 2);
}

void UnitVisibleSplash()
{
    if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    {
        int parent = GetOwner(SELF);
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
        {
            Enchant(OTHER, "ENCHANT_VILLAIN", 0.1);
            Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
        }
    }
}

void StrikeTrackingMis(int owner, int cre)
{
    if (CurrentHealth(cre) && CurrentHealth(owner))
    {
        if (!HasEnchant(owner, "ENCHANT_LIGHT"))
        {
            int plr_ptr = UnitToPtr(owner);

            Enchant(owner, "ENCHANT_LIGHT", 3.0);
            SecondTimerWithArg(3, SetSkillCooldownNofication(owner, plr_ptr, 2), NotifySkillCooldown);
            int glow = CreateObject("Moonglow", 46);

            SetOwner(cre, CreateObject("InvisibleLightBlueHigh", 46));
            AudioEvent("HecubahDieFrame98", 46);
            SetOwner(owner, glow);
            FrameTimerWithArg(1, glow, AutoTrackingMissile);
            UniChatMessage(cre, "마법시전:\n극사 나나야!!", 108);
        }
    }
}

void AutoTrackingMissile(int glow)
{
    int owner = GetOwner(glow), cre = GetOwner(glow + 1);

    if (CurrentHealth(owner) && CurrentHealth(cre))
    {
        int subUnit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(cre) + UnitRatioX(glow, cre, 18.0), GetObjectY(cre) + UnitRatioY(glow, cre, 18.0));
        LookAtObject(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(subUnit), GetObjectY(subUnit)), glow);
        PlaySoundAround(subUnit, 899);
        Raise(subUnit, 250.0);
        SetOwner(cre, subUnit);
        TrackingProgress(subUnit);
    }
    Delete(glow++);
    Delete(glow++);
}

void TrackingProgress(int subUnit)
{
    int owner = GetOwner(subUnit), count = GetDirection(subUnit);

    if (CurrentHealth(owner) && count < 30)
    {
        if (IsVisibleTo(owner, subUnit))
        {
            MoveObjectVector(subUnit, UnitAngleCos(subUnit + 1, 23.0), UnitAngleSin(subUnit + 1, 23.0));

            int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(subUnit), GetObjectY(subUnit));

            Frozen(CreateObjectAt("HarpoonBolt", GetObjectX(unit), GetObjectY(unit)), 1);
            LookWithAngle(unit, GetDirection(subUnit + 1));
            LookWithAngle(unit + 1, GetDirection(subUnit + 1));
            SetOwner(subUnit, unit);
            SetOwner(subUnit, unit + 1);
            SetCallback(unit, 3, EnemyDetection);
            SetCallback(unit, 9, MissileTouched);
            DeleteObjectTimer(unit + 1, 2);
            DeleteObjectTimer(unit, 1);
            LookWithAngle(subUnit, ++count);
        }
        else
            LookWithAngle(subUnit, 200);
        FrameTimerWithArg(1, subUnit, TrackingProgress);
    }
    else
    {
        Delete(subUnit++);
        Delete(subUnit++);
        Delete(subUnit++);
    }
}

void MissileTouched()
{
    int ptr = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, GetOwner(ptr)))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, GetOwner(ptr), 60, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        Delete(ptr);
    }
}

void EnemyDetection()
{
    int ptr = GetOwner(SELF);
    int tg = ToInt(GetObjectZ(ptr + 1));

    if (CurrentHealth(tg))
    {
        if (IsVisibleTo(tg, ptr))
        {
            LookAtObject(SELF, tg);
            LookWithAngle(ptr + 1, GetDirection(SELF));
        }
        else
            Raise(ptr + 1, ToFloat(0));
    }
    else
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < GetObjectZ(ptr))
            Raise(ptr + 1, ToFloat(GetCaller()));
    }
}

void GreenSparkFxAt(float xProfile, float yProfile)
{
    int genfx = CreateObjectAt("MonsterGenerator", xProfile, yProfile);

    Damage(genfx, 0, 10, 100);
    Delete(genfx);
}

void GreenSparkFx(int wp, int sound)
{
    int ptr = CreateObject("MonsterGenerator", wp);

    Damage(ptr, 0, 10, 100);
    if (sound)
        AudioEvent("HecubahDieFrame439", wp);
    Delete(ptr);
}

void NotifySkillCooldown(int ptr)
{
    int owner = GetOwner(ptr), idx = GetDirection(ptr);
    string skillname[] = {"자동 타겟 데스레이[발동: K 키]", "화이트 아웃[발동: L 키]", "극사 나나야[발동: J 키]"};

    if (CurrentHealth(owner))
    {
        UniPrint(owner, "지금부터 " + skillname[idx] + " 스킬을 사용할 수 있습니다");
    }
    Delete(ptr);
}

int SetSkillCooldownNofication(int unit, int addr, int idx)
{
    int res = CreateObject("InvisibleLightBlueHigh", 71);

    SetOwner(unit, res);
    Raise(res, ToFloat(addr));
    LookWithAngle(res, idx);
    return res;
}

void SetUnitHealthBar(int unit, int loc)
{
	int ptr = CreateObject("InvisibleLightBlueLow", loc), k;

	Raise(ptr, ToFloat(unit));
	LookWithAngle(ptr, loc);
	for (k = 9 ; k >= 0 ; k --)
		CreateObject("CharmOrb", loc);
	FrameTimerWithArg(1, ptr, DisplayHealthBar);
}

float HPBarPos(int num)
{
	float arr[2];
	if (!ToInt(arr[0]))
	{
		arr[0] = -10.0;
		arr[1] = 40.0;
		return ToFloat(0);
	}
	return arr[num];
}

void DisplayHealthBar(int ptr)
{
	int unit = ToInt(GetObjectZ(ptr)), k;

	if (CurrentHealth(unit))
	{
		float pos_x = GetObjectX(unit) + HPBarPos(0), pos_y = GetObjectY(unit) + HPBarPos(1);
		int ps = CurrentHealth(unit) * 10 / MaxHealth(unit) + 1;
		for (k = 0 ; k < 10 ; k += 1)
		{
			if (k < ps)
			{
				MoveObject(ptr + k + 1, pos_x, pos_y);
				pos_x += 3.0;
			}
			else
				MoveObject(ptr + k + 1, 100.0, 100.0);
		}
		FrameTimerWithArg(1, ptr, DisplayHealthBar);
	}
	else
	{
		Delete(ptr);
		for (k = 9 ; k >= 0 ; k -= 1)
			Delete(ptr + k + 1);
	}
}

void AddScoreToKing(int pUnit)
{
    int ptr = UnitToPtr(pUnit);

    if (!ptr)
        return;

    int kill = GetKillCreditPtr(ptr), plr;

    if (CurrentHealth(GetOwner(KingPtr + 1)))
    {
        if (IsOwnedBy(kill, GetOwner(KingPtr + 1)))
        {
            UniChatMessage(GetOwner(KingPtr + 1), "잡았다 요녀석~!", 108);
            ChangeScore(GetOwner(KingPtr), 1);
            KING_SRC ++;
        }
        else
            UniChatMessage(GetOwner(KingPtr + 1), "ERROR" + IntToString(kill) + " " + IntToString(GetOwner(kill)), 125);
    }
}

int GetMasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectAt("Hecubah", 5500.0, 100.0);
        Frozen(CreateObjectAt("BlackPowder", GetObjectX(unit), GetObjectY(unit)), 1);
        Frozen(unit, 1);
        LookWithAngle(unit, 0);
        SetCallback(unit, 9, DisplayLadderBoard);
    }
    return unit;
}

void DisplayLadderBoard()
{
    if (GetDirection(SELF) < 20)
        LookWithAngle(SELF, GetDirection(SELF) + 1);
    else
    {
        LookWithAngle(SELF, 0);
        UniChatMessage(SELF, "킬 현황판\n플레이어 킬 수: " + IntToString(PLR_SRC) + "\n보스유닛 킬 수: " + IntToString(KING_SRC), 30);
    }
}

void StrSelectBoss()
{
	int arr[] = {
        268698116, 67652548, 277364752, 2105313, 541318717,
        71631168, 71336201, 2115113252, 1652842691, 32843,
        2056, 268435458, 263164, 1140850816, 18872831, 4096, 537132032
    }, i = 0;
	string name = "DrainManaOrb";

	while(i < 17)
	{
		drawStrSelectBoss(arr[i], name);
		i ++;
	}
}

void drawStrSelectBoss(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = LocationX(76);
		pos_y = LocationY(76);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 76);
		if (count % 48 == 47)
			TeleportLocationVector(76, -94.000000, 2.000000);
		else
			TeleportLocationVector(76, 2.000000, 0.0);
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		TeleportLocation(76, pos_x, pos_y);
	}
}

//77~

void PutPotions()
{
    int k;

    for (k = 77 ; k <= 84 ; k ++)
    {
        PassPotion(k);
    }
}

int PassPotion(int wp)
{
    int unit = CreateObject("Diamond", wp);

    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    Raise(CreateObject("InvisibleLightBlueHigh", wp), ToFloat(1280));
    Enchant(unit + 1, "ENCHANT_SHOCK", 0.0);
    RegistItemPickupCallback(unit, AutoDropEvent);
    Frozen(unit, 1);
    return unit;
}

void AutoDropEvent()
{
    Delete(SELF);
    Delete(GetTrigger() + 1);
    SecondTimerWithArg(40, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF)), PutPasPotion);
}

void PutPasPotion(int ptr)
{
    TeleportLocation(64, GetObjectX(ptr), GetObjectY(ptr));
    PassPotion(64);
    Delete(ptr);
}

void UsePassPotion()
{
    if (CurrentHealth(SELF) && HasEnchant(OTHER, "ENCHANT_FREEZE"))
    {
        if (ToInt(GetObjectZ(GetCaller() + 1)) == 1280)
        {
            TeleportLocation(64, GetObjectX(SELF), GetObjectY(SELF));
            AudioEvent("LongBellsDown", 64);
            CureFx(64);
            RestoreHealth(SELF, 85);
            Delete(OTHER);
            Delete(GetCaller() + 1);
            SecondTimerWithArg(40, CreateObject("InvisibleLightBlueHigh", 64), PutPasPotion);
        }
    }
}

void CureFx(int wp)
{
    int k;

    for (k = 0 ; k < 60 ; k ++)
    {
        Effect("GREATER_HEAL", LocationX(wp), LocationY(wp), LocationX(wp) + MathSine(k * 6 + 90, 96.0), LocationY(wp) + MathSine(k * 6, 96.0));
    }
}

void RainDrop(int rainsub)
{
    if (IsObjectOn(rainsub - 1))
    {
        int idx = GetDirection(rainsub - 1);
        float x = GetObjectX(rainsub + idx), y = GetObjectY(rainsub + idx);
        int rnd = Random(0, 359);

        MoveObject(rainsub + idx, GetObjectX(rainsub - 1) + MathSine(rnd + 90, RandomFloat(20.0, 600.0)), GetObjectY(rainsub - 1) + MathSine(rnd, RandomFloat(20.0, 600.0)));
        if (!IsVisibleTo(rainsub - 1, rainsub + idx))
            MoveObject(rainsub + idx, x, y);
        Raise(rainsub + idx, 280.0);
        LookWithAngle(rainsub - 1, (idx + 1) % 30);
        FrameTimerWithArg(1, rainsub, RainDrop);
    }
}

void RainyLocation(int wp)
{
    int rainsub = CreateObject("InvisibleLightBlueLow", wp) + 1, k;

    for (k = 0 ; k < 30 ; ++k)
    {
        CreateObject("CorpseRightUpperArmE", wp);
    }
    FrameTimerWithArg(1, rainsub, RainDrop);
}

void InitRainyDay(int val)
{
    //91~112
    if (val <= 112)
    {
        RainyLocation(val);
        FrameTimerWithArg(10, val + 2, InitRainyDay);
    }
}

int GetKillCreditPtr(int ptr)
{
    int ptr2;

    if (ptr)
    {
        ptr2 = GetMemory(ptr + 0x208);
        if (ptr2)
        {
            return GetMemory(ptr2 + 0x2c);
        }
    }
    return 0;
}

int GetKillCredit()
{
    int ptr = GetMemory(0x979724), ptr2;

    if (ptr)
    {
        ptr2 = GetMemory(ptr + 0x208);
        if (ptr2)
        {
            return GetMemory(ptr2 + 0x2c);
        }
    }
    return 0;
}
