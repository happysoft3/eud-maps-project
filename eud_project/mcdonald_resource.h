
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"

#define IMAGE_VECTOR_SIZE 2048

void GRPDump_WindowOniOutput(){}
#define WINDOW_ONI_OUTPUT_BASE_IMAGE_ID 133955
void initializeImageFrameWindowOniOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_WindowOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, WINDOW_ONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, WINDOW_ONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     WINDOW_ONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, WINDOW_ONI_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     WINDOW_ONI_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     WINDOW_ONI_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     WINDOW_ONI_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     WINDOW_ONI_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     WINDOW_ONI_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     WINDOW_ONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_cent1_output(){}

void initializeCentAnimation(int imgVec)
{
    int *frames, *sizes, thingId=OBJ_TREASURE_CHEST;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_cent1_output)+4, thingId, xyInc, &frames, &sizes);
    AppendImageFrame(imgVec,frames[0], 112596);
    AppendImageFrame(imgVec,frames[0], 112597);
    AppendImageFrame(imgVec,frames[1], 112598);
    AppendImageFrame(imgVec,frames[2], 112599);
    AppendImageFrame(imgVec,frames[3], 112600);
    AppendImageFrame(imgVec,frames[4], 112601);
    AppendImageFrame(imgVec,frames[5], 112602);
    AppendImageFrame(imgVec,frames[0], 112603);
}

/*
DrawTextSetupBottmText(2463, drawFn, 0x981f, "맥도날드~~");
    DrawTextSetupBottmText(2507, drawFn, 0x59f, "지하창고");
    DrawTextSetupBottmText(2516, drawFn, 0xf804, "보스 구역");
    DrawTextSetupBottmText(2517, drawFn, 0xffe0, "승리하셨습니다!!");
    **/
void initializeMapText(int imgVector){
    short message1[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("*다크 포레스트*"),message1);
    AppendTextSprite(imgVector,0x7e1,message1,115273);

    short message2[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("*인페르날*"),message2);
    AppendTextSprite(imgVector,0x7e1,message2,115276);

    short message3[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("*만년설산*"),message3);
    AppendTextSprite(imgVector,0x7e1,message3,115275);

    short message4[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("*황량함의 늪지대*"),message4);
    AppendTextSprite(imgVector,0x7e1,message4,115274);

    short message5[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("*저주받은 신전*"),message5);
    AppendTextSprite(imgVector,0x7e1,message5,115272);

    short msg21[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("~맥도날드~"),msg21);
    AppendTextSprite(imgVector,0x981f,msg21,115269);
    short msg22[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("지하 자재창고"),msg22);
    AppendTextSprite(imgVector,0x59f,msg22,115270);
    short msg23[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("!주의! 보스구역"),msg23);
    AppendTextSprite(imgVector,0xf804,msg23,115271);
    short msg24[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("~승리하셨습니다~!!~"),msg24);
    AppendTextSprite(imgVector,0xffe0,msg24,115281);
    short msg31[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("아이템 내구도 무한 비콘"),msg31);
    AppendTextSprite(imgVector,0xffe0,msg31,115284);

    ChangeMonsterSpriteImageCount(OBJ_FISH_BIG, IMAGE_SPRITE_MON_ACTION_SLAIN, 2);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_UP_LEFT,1,115277);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_UP,1,115278);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_UP_RIGHT,1,115279);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,1,115280);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,115281);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_DOWN_LEFT,1,115282);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_DOWN,1,115283);
    ChangeMonsterSpriteImage(OBJ_FISH_BIG,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_DOWN_RIGHT,1,115284);
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void GRPDumpRectangleOutput(){}
void initializeImageRectangle(int v){
    int *frames, *sizes, thingId=OBJ_CORPSE_SKULL_SW;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRectangleOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AppendImageFrame(v,frames[0],132568);
}
void GRPDumpMcLogoOutput(){}
void initializeImageMcLogo(int v){
    int *frames, *sizes, thingId=OBJ_TORTURE_RACK_1;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpMcLogoOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AppendImageFrame(v,frames[0],111855);
    AppendImageFrame(v,frames[1],111856);
}

void initializeInnerImageMovingGargoyleSouth()
{
    int imgs[]={17594, 17595,17596,17597,17598,17599,};
    ChangeSpriteAnimateImage(OBJ_VANDEGRAF_LARGE_MOVABLE, 2, imgs,sizeof(imgs));
}
void initializeInnerImageMcGirl(){
    int imgs[]={17141, 17142,17143,17144,17145,17146,17147,17148,};
    ChangeSpriteAnimateImage(OBJ_ORRERY_1_SHADOW, 3, imgs,sizeof(imgs));
}

void initializeInnerImages(){
    initializeInnerImageMovingGargoyleSouth();
    initializeInnerImageMcGirl();
}

void GRPDumpHambergerOutput(){}
void initializeImageHambergerOutput(int v){
    int *frames, *sizes, thingId=OBJ_WIERDLING_POOL_WATERFALL;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHambergerOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AppendImageFrame(v,frames[0],133514);
    AppendImageFrame(v,frames[0],133515);
    AppendImageFrame(v,frames[0],133516);
    AppendImageFrame(v,frames[1],133517);
    AppendImageFrame(v,frames[1],133518);
    AppendImageFrame(v,frames[1],133519);
    AppendImageFrame(v,frames[2],133520);
    AppendImageFrame(v,frames[2],133521);
    AppendImageFrame(v,frames[2],133522);
    AppendImageFrame(v,frames[3],133523);
    AppendImageFrame(v,frames[3],133524);
    AppendImageFrame(v,frames[3],133525);
}
void GRPDumpMcGirlOutput(){}
void initializeImageMcGirlOutput(int v){
    int *frames, *sizes, thingId=OBJ_ORRERY_1_SHADOW;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpMcGirlOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AppendImageFrame(v,frames[0],17141);
    AppendImageFrame(v,frames[1],17142);
    AppendImageFrame(v,frames[2],17143);
    AppendImageFrame(v,frames[3],17144);
    AppendImageFrame(v,frames[4],17145);
    AppendImageFrame(v,frames[5],17146);
    AppendImageFrame(v,frames[6],17147);
    AppendImageFrame(v,frames[7],17148);
}

void GRPDump_TalesweaverWallensteinOutput(){}

#define TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT 131415

void initializeImageFrameTalesweavergirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_NECROMANCER;
    int xyInc[]={0,-13};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_TalesweaverWallensteinOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,7,2,2);
    AnimFrameAssign8Directions(0,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(30, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameAssign8Directions(35, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameAssign8Directions(40, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameAssign8Directions(75, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 0);
    AnimFrameAssign8Directions(70, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 1);
    AnimFrameAssign8Directions(65, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 2);
    AnimFrameAssign8Directions(60, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+96, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 3);
    AnimFrameAssign8Directions(55, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 4);
    AnimFrameAssign8Directions(50, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 5);
    AnimFrameAssign8Directions(45, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);
}

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 114160
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_MECHANICAL_GOLEM;
    int xyInc[]={0,-30};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount-4);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}
void GRPDump_WhiteManOutput(){}

#define WHITEMAN_OUTPUT_BASE_IMAGE_ID 122915
void initializeImageFrameWhiteMan(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_WhiteManOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  WHITEMAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(5,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(10,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(15,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(20,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(25,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(30,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(35,  WHITEMAN_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions( WHITEMAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions( WHITEMAN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(  WHITEMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_LonghairWomanOutput(){}

#define LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID 129717
void initializeImageFrameLonghairWoman(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_LonghairWomanOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,13,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,13,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(5,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(10,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(15,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(20,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(25,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(30,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(35,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameAssign8Directions(40,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_WALK, 8);
    AnimFrameAssign8Directions(45,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_WALK, 9);
    AnimFrameAssign8Directions(50,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_WALK, 10);
    AnimFrameAssign8Directions(55,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+88, IMAGE_SPRITE_MON_ACTION_WALK, 11);
    AnimFrameAssign8Directions(60,  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+96, IMAGE_SPRITE_MON_ACTION_WALK, 12);
    AnimFrameCopy8Directions( LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions( LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_RUN, 8);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_RUN, 9);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_RUN, 10);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+88, IMAGE_SPRITE_MON_ACTION_RUN, 11);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+96, IMAGE_SPRITE_MON_ACTION_RUN, 12);
    AnimFrameCopy8Directions( LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(  LONGHAIRWOMAN_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void ImageTitleCard(){}

int makeSolidTileAdv(char r, char g, char b){
    short *ret;

    AllocSmartMemEx(2116,&ret);
    int t=1058, colr=b|(g<<5)|(r<<10);
    while (--t>=0)
        ret[t]=colr;
    return ret;
}

void EnableTileTexture(int enabled)
{
    //0x5acd50
    int *p = 0x5acd50;

    if (p[0]==enabled)
        return;
        
    char code[]={0xB8, 0x15, 0xB7, 0x4C, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC3,};

    invokeRawCode(code, NULLPTR);
    
}

void onUpdateTileChanged()
{
    EnableTileTexture(FALSE);
    // SolidTile(628, 6,0x07FA);
    EnableTileTexture(TRUE);
}

void GRPDumpTileset(){}

void initializeCustomTileset(int imgVector){
    int *pack=GetScrCodeField(GRPDumpTileset)+4;
    int *off=&pack[1];
    int frames[]={pack+off[0],pack+off[1], pack+off[2],pack+off[3],};

    AppendImageFrame(imgVector, frames[0], 361);
    AppendImageFrame(imgVector, frames[1], 362);
    AppendImageFrame(imgVector, frames[2], 363);
    AppendImageFrame(imgVector, frames[3], 370);
    onUpdateTileChanged();
}

void InitializeResources(){
    int imgVector = CreateImageVector(IMAGE_VECTOR_SIZE);

    initializeCentAnimation(imgVector);
    initializeMapText(imgVector);
    initializeImageHealthBar(imgVector);
    initializeImageRectangle(imgVector);
    initializeImageMcLogo(imgVector);
    initializeInnerImages();
    initializeImageHambergerOutput(imgVector);
    initializeImageMcGirlOutput(imgVector);
    initializeImageFrameTalesweavergirl(imgVector);
    initializeImageFrameBillyOni(imgVector);
    AppendImageFrame(imgVector,GetScrCodeField(ImageTitleCard)+4,14614);
    initializeImageFrameWhiteMan(imgVector);
    initializeImageFrameLonghairWoman(imgVector);
    initializeImageFrameWindowOniOutput(imgVector);
    initializeCustomTileset(imgVector);
    DoImageDataExchange(imgVector);
}

