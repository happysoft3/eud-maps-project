
#include "r231103_player.h"
#include "r231103_sub.h"
#include "r231103_mob.h"
#include "r231103_wand.h"
#include "r231103_initscan.h"
#include "r231103_item.h"
#include "r231103_utils.h"
#include "r231103_gvar.h"
#include "r231103_resource.h"
#include "libs/coopteam.h"
// #include "libs/indexloop.h"

int m_initScanHash;
int m_entityDeathCount=600;

int m_player[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{
    return m_player[pIndex];
}

static void SetPlayer(int pIndex, int user) //virtual
{
    m_player[pIndex]=user;
}

int m_pFlags[MAX_PLAYER_COUNT];

static int GetPlayerFlags(int pIndex) //virtual
{
    return m_pFlags[pIndex];
}

static void SetPlayerFlags(int pIndex, int flags) //virtual
{
    m_pFlags[pIndex]=flags;
}

int m_masterUnit;

static int GetMasterUnit() //virtual
{
    return m_masterUnit;
}

int m_dummyDamageCode;

static int GetDummyDamageHandler() //virtual
{
    return m_dummyDamageCode;
}

short m_mobAttackFunctions[97];
short m_mobHpTable[97];
short m_mobTypes[6];

static int GetMobAttackFunction(int n)
{
    return m_mobAttackFunctions[n];
}

static int GetMobHP(int n) //virtual
{
    return m_mobHpTable[n];
}

static int GetMobType() //virtual
{
    return m_mobTypes[Random(0, sizeof(m_mobTypes)-1)];
}

int m_dialogCtx;

static int GetDialogCtx() //virtual
{
    return m_dialogCtx;
}

static void SetDialogCtx(int ctx) //virtual
{ m_dialogCtx=ctx; }

string m_popupMessage[8];

static string GetPopupMessage(int index) //virtual
{
    return m_popupMessage[index];
}

int m_genericHash;

static int GenericHash() //virtual
{
    return m_genericHash;
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void deferredInitializeMap()
{
    MakeCoopTeam();
    InitializeSubPart();
    FrameTimer(30, StartPlacingEntityMultiple);
    InitItemNames();
    StartUnitScan(Object("firstscan"), m_masterUnit, m_initScanHash);
}

void MapInitialize()
{
    MusicEvent();
    m_masterUnit = DummyUnitCreateById(OBJ_HECUBAH, 100.0, 100.0);
    FrameTimer(1, deferredInitializeMap);
    m_dummyDamageCode = CreateDeadDamageHandler(OnEntityCoreDead);
    HashCreateInstance(&m_initScanHash);
    HashPushback(m_initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashCreateInstance(&m_potionHash);
    FillElemPotionHash(m_potionHash);
    InitialSetMobData(m_mobAttackFunctions, m_mobHpTable, m_mobTypes);
    HashCreateInstance(&m_genericHash);
    InitialPlaceWandShop();
}

static void TryAttackMonsterToPlayer(int mon, int user) //virtual
{
    EntityGoAttack(mon, user); 
}

static void commonServerClientProcedure()
{
    InitializeCommonImage();
    InitPopupMessage(m_popupMessage);
}

int m_potionHash;
static int GetPotionHash() //override
{
    return m_potionHash;
}

string *m_weaponNameTable;
int m_weaponNameTableLength;
string *m_armorNameTable;
int m_armorNameTableLength;
string *m_potionNameTable;
int m_potionNameTableLength;
string *m_staffNameTable;
int m_staffNameTableLength;
int *m_itemFunctionTable;
int m_itemFunctionTableLength;

static void SetItemNameWeapon(int *p, int count)
{
    m_weaponNameTable=p;
    m_weaponNameTableLength=count;
}
static void SetItemNameArmor(int *p, int count)
{
    m_armorNameTable=p;
    m_armorNameTableLength=count;
}
static void SetItemNamePotion(int *p,int count)
{
    m_potionNameTable=p;
    m_potionNameTableLength=count;
}
static void SetItemNameStaff(int *p,int count)
{
    m_staffNameTable=p;
    m_staffNameTableLength=count;
}
static void SetItemFunctionPtr(int *p,int count) //override
{
    m_itemFunctionTable=p;
    m_itemFunctionTableLength=count;
}
static string GetWeaponName()
{
    return m_weaponNameTable[Random(0, m_weaponNameTableLength-1)];
}
static string GetArmorName()
{
    return m_armorNameTable[Random(0, m_armorNameTableLength-1)];
}
static string GetPotionName()
{
    return m_potionNameTable[Random(0, m_potionNameTableLength-1)];
}
static string GetWandName()
{return m_staffNameTable [Random(0, m_staffNameTableLength-1)];}

static int GetItemCreateFunction()
{
    return m_itemFunctionTable[Random(0,m_itemFunctionTableLength-1)];
}

void youAreWinner()
{
    TeleportAllPlayer(15);
    UniPrintToAll("!---당신의 승리---!");
    float x=LocationX(15),y=LocationY(15);
    CreateObjectById(OBJ_LEVEL_UP, x,y);
    CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT, x,y);
    CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT, x,y);
    CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT, x,y);
    CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT, x,y);
    CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT, x,y);
}

static void OnEntityDead(int entity, float x, float y) //virtual
{
    if (GetPercentRate(40, 100))
        CreateRandomItemCommon(CreateObjectById(OBJ_MOVER, x,y));
    
    if(--m_entityDeathCount==0)
    {
        youAreWinner();
        return;
    }
    PrintMessageFormatOne(PRINT_ALL, "남은 엔티티 수: %d", m_entityDeathCount);
}

// static void IntroducedIndexLoopHashCondition(int *pInstance)
// {
//     HashPushback(pInstance, OBJ_ROUND_CHAKRAM_IN_MOTION, HookChakrm);
// }


static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    commonServerClientProcedure();
    FrameTimer(10, ClientProcLoop);
}
