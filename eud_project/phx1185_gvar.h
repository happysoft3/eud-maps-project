
#define MAX_PLAYER_COUNT 32

#define BASE_MOTION_IDLE 0
#define BASE_MOTION_JUMP 1
#define BASE_MOTION_MOVE 2
#define BASE_MOTION_ATTACK 3
#define BASE_MOTION_ATTACK_2 4

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10

void TryUpdatePlayerGold(int user) //virtual
{ }

int GetMasterUnit() //virtual
{ }

int GetAbandondedItemOwner() //virtual
{ }

int GetClientKeyState(int pIndex) //virtual
{ }

void SetClientKeyState(int pIndex, int value) //virtual
{ }

int GetAvataHash() //virtual
{ }

void SetMonsterInfoPtr(short *pHp, short *pCredit, short *pAmmo, short *pDeathFn) //virtual
{ }

int GetMonsterInitHP(int n) //virtual
{ }

int GetMonsterCredit(int n) //virtual
{ }

int GetMonsterArmor(int n) //virtual
{ }

int GetUserHandler(int pIndex) //virtual
{ }

void SetUserHandler(int pIndex, int handler) //virtual
{ }

int GetDialogCtx() //virtual
{ }

void SetDialogCtx(int ctx) //virtual
{ }

string GetPopupMessage(int index) //virtual
{ }

int GetPlayerEquipment(int pIndex) //virtual
{ }

int GetPlayerEquipmentCopy(int pIndex) //virtual
{ }

int GetPlayer(int pIndex) //virtual
{ }

int GetPlayerAvata(int pIndex) //virtual
{ }

void SetPlayerAvata(int pIndex, int avata) //virtual
{ }

void TryItemUse(int pIndex, int item) //virtual
{ }

#define EFFECT_KEY_DAMAGE 0
#define EFFECT_KEY_ATTACK_DELAY 1
#define EFFECT_KEY_USER 2
#define EFFECT_KEY_VICTIM 3
#define EFFECT_KEY_CRITICAL_RATE 4
#define EFFECT_KEY_MULTIPLE 5
#define EFFECT_KEY_DROP_GOLD_RATE 6

int GetWeaponEffectHash() //virtual
{ }

void FetchWeaponEffect(int weapon, int effectHash) //virtual
{ }

void OnItemDropped(int pIndex, int item) //virtual
{ }

int TrySummonBossMonster(int counterUnit) //virtual
{ }

int GetBossMonster() //virtual
{ }

int GetMonsterDeathFunction(short thingId) //virtual
{ }

int GetSellWeaponHash() //virtual
{ }

void CreateMeso(float x, float y, int amount) //virtual
{ }
