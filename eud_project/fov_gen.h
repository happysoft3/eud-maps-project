
#include "fov_utils.h"
#include "libs/format.h"
#include "libs/sound_define.h"
#include "libs/waypoint.h"
#include "fov_reward.h"

void OrangeMaidenSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.8);
        int missile = CreateObjectAt("ThrowingStone", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 17.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 17.0));
        SetOwner(SELF, missile);
        Enchant(missile, "ENCHANT_HASTED", 0.0);
        Enchant(missile, "ENCHANT_SHOCK", 0.0);
        PushObjectTo(missile, UnitRatioX(OTHER, SELF, 80.0), UnitRatioY(OTHER, SELF, 80.0));
        PlaySoundAround(SELF, 500);
    }
    CheckResetSight(GetTrigger(), 20);
}

void OrangeMaidenDeath()
{
    SummonedUnitDeath();
    DeleteObjectTimer(SELF, 1);
}

void BomberDeadFx()
{
    DeleteObjectTimer(SELF, 1);
    TeleportLocation(26, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(CreateObject("GreenSmoke", 26), 12);
    AudioEvent("EggBreak", 26);
    AudioEvent("PolypExplode", 26);
    SummonedUnitDeath();
}

void DelayAttackOrder(int unit, int tg)
{
    int ptr = CreateObject("AmbBeachBirds", 25);
    SetOwner(unit, ptr);
    Raise(ptr, ToFloat(tg));
    FrameTimerWithArg(1, ptr, UnitAttackOrder);
}

void UnitAttackOrder(int ptr)
{
    if (CurrentHealth(GetOwner(ptr)) && CurrentHealth(ToInt(GetObjectZ(ptr))))
    {
        CreatureFollow(GetOwner(ptr), ToInt(GetObjectZ(ptr)));
        AggressionLevel(GetOwner(ptr), 1.0);
    }
    Delete(ptr);
}

void BlackSpiderDeadEvent()
{
    float x= GetObjectX(SELF),y= GetObjectY(SELF);
    DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE,x,y), 3);
    DeleteObjectTimer(CreateObjectById(OBJ_WATER_BARREL_BREAKING,x,y), 3);
    // CreateObject("ArachnaphobiaFocus", 26);
    SummonedUnitDeath();
}

void CheckResetSight(int unit, int delay)
{
    if (!HasEnchant(unit, "ENCHANT_DETECTING"))
    {
        Enchant(unit, "ENCHANT_DETECTING", 0.0);
        FrameTimerWithArg(delay, unit, ResetUnitSight);
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.06);
    AggressionLevel(unit, 1.0);
}

void ImpSightEvent()
{
    int ptr;

    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
        float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 23.0),y= GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 23.0);
        ptr = CreateObjectById(OBJ_AMB_BEACH_BIRDS, x,y);
        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", SELF, x,y);
        Delete(ptr - 2);
        Delete(ptr - 3);
        Delete(ptr - 4);
        Delete(ptr);
    }
    CheckResetSight(GetTrigger(), 20);
}

int SummonBat(int wp)
{
    int unit = CreateObject("Bat", wp);

    CreateObject("AmbBeachWaves", wp);
    SetUnitMaxHealth(unit, 96);

    return unit;
}

int SummonImp(int wp)
{
    int unit = CreateObject("Imp", wp);

    CreateObject("AmbBeachWaves", wp);
    SetUnitMaxHealth(unit, 96);
    // SetCallback(unit, 3, ImpSightEvent);
    SetUnitScanRange(unit, 600.0);

    return unit;
}

int SummonSkeleton(int wp)
{
    int unit = CreateObject("Skeleton", wp);
    CreateObject("AmbBeachBirds", wp);
    SetUnitMaxHealth(unit, 250);
    return unit;
}

int SummonSkeletonLord(int wp)
{
    int unit = CreateObject("SkeletonLord", wp);

    CreateObject("AmbBeachBirds", wp);
    SetUnitMaxHealth(unit, 295);
    return unit;
}

int SummonZombie(int wp)
{
    int unit = CreateObject("Zombie", wp);
    CreateObject("AmbBeachBirds", wp);
    SetUnitMaxHealth(unit, 125);
    return unit;
}

int SummonVileZombie(int wp)
{
    int unit = CreateObject("VileZombie", wp);
    CreateObject("AmbBeachBirds", wp);
    SetUnitMaxHealth(unit, 325);
    return unit;
}

int SummonGhost(int wp)
{
    int unit = CreateObject("Ghost", wp);
    CreateObject("AmbBeachBirds", wp);
    SetUnitMaxHealth(unit, 96);
    return unit;
}

int SummonOgreAxe(int wp)
{
    int unit = CreateObject("GruntAxe", wp);

    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 225);
    return unit;
}

int SummonOgreBrute(int wp)
{
    int unit = CreateObject("OgreBrute", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 305);
    return unit;
}

int SummonOgreWarlord(int wp)
{
    int unit = CreateObject("OgreWarlord", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 325);
    return unit;
}

int SummonSpider(int wp)
{
    int unit = CreateObject("BlackWidow", wp);
    int ptr = LatestUnitPtr();
    
    UnitLinkBinScript(CreateObject("AmbBeachBirds", wp) - 1, BlackWidowBinTable());
    SetUnitMaxHealth(unit, 180);
    SetUnitVoice(unit, 33);
    Enchant(unit, "ENCHANT_BURNING", 0.0);
    SetCallback(unit, 5, BlackSpiderDeadEvent);
    return unit;
}

int SummonWhiteSpider(int wp)
{
    int unit = CreateObject("AlbinoSpider", wp);

    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 128);
    return unit;
}

int SummonSlowSpider(int wp)
{
    int unit = CreateObject("SpittingSpider", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 128);
    return unit;
}

int SummonMinionSpider(int wp)
{
    int unit = CreateObject("SmallSpider", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 64);
    return unit;
}

int SummonMinionWhiteSpider(int wp)
{
    int unit = CreateObject("SmallAlbinoSpider", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 32);
    Enchant(unit, "ENCHANT_HASTED", 0.0);
    return unit;
}

int SummonScorpion(int wp)
{
    int unit = CreateObject("Scorpion", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 250);
    return unit;
}

int SummonBear(int wp)
{
    int unit = CreateObject("Bear", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 325);
    return unit;
}

int SummonBlackBear(int wp)
{
    int unit = CreateObject("BlackBear", wp);

    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 290);
    return unit;
}

int SummonWizard(int wp)
{
    int unit = CreateObject("Wizard", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 175);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SummonBeholder(int wp)
{
    int unit = CreateObject("Beholder", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 275);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SummonUrchin(int wp)
{
    int unit = CreateObject("Urchin", wp);

    UnitZeroFleeRange(CreateObject("AmbBeachBirds", wp) - 1);
    SetUnitMaxHealth(unit, 64);
    return unit;
}

int SummonShaman(int wp)
{
    int unit = CreateObject("UrchinShaman", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 84);
    return unit;
}

int SummonDryad(int wp)
{
    int unit = CreateObject("WizardGreen", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 128);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SummonGirl(int wp)
{
    int unit = ColorMaiden(255, 0, 0, wp);

    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 306);
    return unit;
}

int SummonSwordsman(int wp)
{
    int unit = CreateObject("Swordsman", wp);
    int ptr = LatestUnitPtr();
    
    RetreatLevel(CreateObject("AmbBeachBirds", wp) - 1, 0.0);
    SetUnitMaxHealth(unit, 325);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(3));
    return unit;
}

int SummonArcher(int wp)
{
    int unit = CreateObject("Archer", wp);
    int ptr = LatestUnitPtr();

    RetreatLevel(CreateObject("AmbBeachBirds", wp) - 1, 0.0);
    SetUnitMaxHealth(unit, 98);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(5));
    return unit;
}

int SummonMecaFly(int wp)
{
    int unit = CreateObject("FlyingGolem", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 64);
    return unit;
}

int SummonGargoyle(int wp)
{
    int unit = CreateObject("EvilCherub", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 64);
    return unit;
}

int SummonEmberDemon(int wp)
{
    int unit = CreateObject("EmberDemon", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 128);
    return unit;
}

int SummonMeleeDemon(int wp)
{
    int unit = CreateObject("MeleeDemon", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 150);
    return unit;
}

int SummonWhiteWolf(int wp)
{
    int unit = CreateObject("WhiteWolf", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 192);
    return unit;
}

int SummonBlackWolf(int wp)
{
    int unit = CreateObject("BlackWolf", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 225);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    return unit;
}

int SummonGrayWolf(int wp)
{
    int unit = CreateObject("Wolf", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 162);
    return unit;
}

int SummonShade(int wp)
{
    int unit = CreateObject("Shade", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 195);
    return unit;
}

int SummonWisp(int wp)
{
    int unit = CreateObject("WillOWisp", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 420);
    return unit;
}

int SummonMecaGolem(int wp)
{
    int unit = CreateObject("MechanicalGolem", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 1280);
    return unit;
}

int SummonStoneGolem(int wp)
{
    int unit = CreateObject("StoneGolem", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 1344);
    return unit;
}

int SummonTroll(int wp)
{
    int unit = CreateObject("Troll", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 480);
    return unit;
}

int SummonDemon(int wp)
{
    int unit = CreateObject("Demon", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 640);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SummonBomber(int wp)
{
    int unit = CreateObject("BomberGreen", wp);
    int ptr = LatestUnitPtr();

    UnitLinkBinScript(CreateObject("AmbBeachBirds", wp) - 1, BomberGreenBinTable());
    SetUnitMaxHealth(unit, 240);
    SetMemory(ptr + 0x2b8, 0x4e83b0);
    SetUnitVoice(unit, 55);
    Enchant(unit, "ENCHANT_BURNING", 0.0);
    SetCallback(unit, 5, BomberDeadFx); 
    return unit;
}

int SummonLeech(int wp)
{
    int unit = CreateObject("GiantLeech", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 160);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    return unit;
}

int SummonBee(int wp)
{
    int unit = CreateObject("Wasp", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 64);
    return unit;
}

int SummonOrgSpider(int wp)
{
    int unit = CreateObject("Spider", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 160);
    return unit;
}

int SummonOrangeMaiden(int wp)
{
    int unit = ColorMaiden(255, 127, 39, wp);

    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 225);
    Enchant(unit, "ENCHANT_BURNING", 0.0);
    SetCallback(unit, 5, OrangeMaidenDeath);
    SetCallback(unit, 3, OrangeMaidenSightEvent);
    return unit;
}

int SummonHovarth(int wp)
{
    int unit = CreateObject("WizardWhite", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 192);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

int SummonPlant(int wp)
{
    int unit = CreateObject("CarnivorousPlant", wp);
    int ptr = LatestUnitPtr();
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 320);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    SetMemory(ptr + 0x224, ToInt(1.4));
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    return unit;
}

// int SummonCaptain(int wp)
// {
//     int unit = CreateObject("AirshipCaptain", wp);

//     UnitLinkBinScript(CreateObject("AmbBeachBirds", wp) - 1, AirshipCaptainBinTable());
//     SetUnitMaxHealth(unit, 480);
//     return unit;
// }

int SummonHorrendous(int wp)
{
    int unit = CreateObject("Horrendous", wp);
    int ptr = LatestUnitPtr();
    
    SetMemory(ptr + 0x224, ToInt(1.6));
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 600);
    return unit;
}

int SummonMimic(int wp)
{
    int unit = CreateObject("Mimic", wp);
    
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 800);
    return unit;
}

int SummonLich(int wp)
{
    int unit = CreateObject("Lich", wp);

    UnitZeroFleeRange(unit);
    UnitLinkBinScript(unit, LichLordBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    SetUnitMaxHealth(CreateObject("AmbBeachBirds", wp) - 1, 325);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    return unit;
}

int CheckSightPlayer(int unit)
{
    int k=32;

    while (--k>=0)
    {
        if (GetPlayerUnit(k))
        {            
            if (IsVisibleOr(GetPlayerUnit(k) , unit))
                return k;
        }
    }
    return -1;
}

int GetNearlyPlayer(int unit)
{
    int k=32, res = -1;
    float temp = 9999.0, cur;

    while (--k>=0)
    {
        if (CurrentHealth(GetPlayerUnit(k)))
        {
            cur = DistanceUnitToUnit(unit, GetPlayerUnit(k));
            if (cur < temp)
            {
                temp = cur;
                res = k;
            }
        }
    }
    return res;
}

void CheckInSightPlayer(int mobgen)
{
    if (CurrentHealth(mobgen))
    {
        if (CheckSightPlayer(mobgen) + 1)
        {
            int *cFps = 0x84ea04;

            if (cFps[0]>ToInt(GetObjectZ(mobgen + 1)))
            {
                int target = GetNearlyPlayer(mobgen);
                if (target + 1)
                {
                    SummonMonster(mobgen, GetPlayerUnit(target));
                }
            }
        }
        PushTimerQueue(31, mobgen, CheckInSightPlayer);
    }
}

void SummonMonster(int mobgen, int tg)
{
    int cur_cnt = GetDirection(mobgen + 3), max_cnt = GetDirection(mobgen + 2);

    if (cur_cnt < max_cnt)
    {
        float xy[]={GetObjectX(mobgen) + UnitRatioX(tg, mobgen, 30.0), GetObjectY(mobgen) + UnitRatioY(tg, mobgen, 30.0)};
        GreenLightningEffect(GetObjectX(mobgen), GetObjectY(mobgen), GetObjectX(mobgen) + UnitRatioX(tg, mobgen, 63.0), GetObjectY(mobgen) + UnitRatioY(tg, mobgen, 63.0));
        PlaySoundAround(mobgen, SOUND_MonsterGeneratorSpawn);
        GreenSparkAt(xy[0],xy[1]);
        int *cFps = 0x84ea04;
        int cool = cFps[0]+(ToInt( GetObjectZ(mobgen + 2))*30);

        Raise(mobgen + 1,cool);

        TeleportLocation(25, xy[0], xy[1]);
        int unit = CallFunctionWithArgInt(MonsterFunctionList(GetDirection(mobgen + 1)), 25);
        SummonUnitProperties(unit, mobgen);
        LookWithAngle(mobgen + 3, cur_cnt + 1);
        LookAtObject(unit, tg);
        DelayAttackOrder(unit, tg);
    }
}

string GenType(int num)
{
    string montable[] = {
            //0~4
        "AlbinoSpider", "Bat", "Bear", "Archer", "Swordsman",
            //5~9
        "Wizard", "WizardGreen", "WizardWhite", "Mimic", "EmberDemon",
            //10~14
        "Demon", "MechanicalGolem", "StoneGolem", "Necromancer", "Skeleton",
            //15~19
        "SkeletonLord", "Lich", "Ghost", "GiantLeech", "Scorpion",
            //20~24
        "Shade", "Wolf", "Spider", "SmallAlbinoSpider", "Bomber",
            //25~29
        "Beholder", "WillOWisp", "GruntAxe", "OgreBrute", "OgreWarlord",
            //30~34
        "SpittingSpider", "EvilCherub", "Imp", "FlyingGolem", "Horrendous",
            //35~39
        "SmallSpider", "Troll", "Urchin", "UrchinShaman", "VileZombie",
            //40~41
        "Wasp", "Zombie"};
    return montable[num];
}

void DestroyGenerator()
{
    float xy[]={GetObjectX(SELF), GetObjectY(SELF)};

    PushTimerQueue(9, CreateObjectById(OBJ_EXPLOSION,xy[0],xy[1]), CreateRandomItemCommon);
    Effect("DAMAGE_POOF", xy[0],xy[1], 0.0, 0.0);
    Effect("JIGGLE", xy[0],xy[1], 7.0, 0.0);
    PlaySoundAround(SELF, SOUND_MechGolemPowerUp);
    int unit = GetTrigger();
    Delete(unit++);
    Delete(unit++);
    Delete(unit++);
    Delete(unit++);
}

void SummonedUnitHurt()
{
    if (GetCaller())
        return;

    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 3, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectById(OBJ_GREEN_PUFF, GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void SummonUnitProperties(int unit, int mobgen)
{
    if (CurrentHealth(unit))
    {
        SetOwner(GetMaster(), unit);
        Raise(unit + 1, ToFloat(mobgen));
        if (!HasEnchant(unit, "ENCHANT_BURNING"))
            SetCallback(unit, 5, SummonedUnitDeath);
        SetCallback(unit, 7, SummonedUnitHurt);
        RetreatLevel(unit, 0.0);
        AggressionLevel(unit, 1.0);
    }
}

void SummonedUnitDeath()
{
    int summonedUnit = ToInt(GetObjectZ(GetTrigger() + 1));

    if (CurrentHealth(summonedUnit))
    {
        LookWithAngle(summonedUnit + 3, GetDirection(summonedUnit + 3) - 1);
    }
    DeleteObjectTimer(SELF, 80);
    Delete(GetTrigger() + 1);
}

int MonsterFunctionList(int order)
{
    int *tablptr;

    if (tablptr == NULLPTR)
    {
        int actions[] = {
            SummonBat, SummonImp, SummonSkeleton, SummonSkeletonLord, SummonZombie,
            SummonVileZombie, SummonGhost,            SummonOgreAxe, SummonOgreBrute, SummonOgreWarlord,
            //10~19
             SummonSpider, SummonWhiteSpider, SummonSlowSpider,            SummonMinionSpider, SummonMinionWhiteSpider,
              SummonScorpion, SummonBear, SummonBlackBear,            SummonWizard, SummonBeholder, 

              //20~29
              SummonUrchin, SummonShaman, SummonDryad, SummonGirl, SummonSwordsman,
            SummonArcher, SummonMecaFly, SummonGargoyle, SummonEmberDemon, SummonMeleeDemon, 

            //30~39
            SummonWhiteWolf,            SummonBlackWolf, SummonGrayWolf, SummonShade, SummonWisp, 
            SummonMecaGolem, SummonStoneGolem,            SummonTroll, SummonDemon, SummonBomber, 
            SummonLeech, SummonBee, SummonOrgSpider,            SummonOrangeMaiden, SummonHovarth, 
            SummonPlant, SummonSkeletonLord, SummonHorrendous,            SummonMimic, SummonLich};

        tablptr = actions;
    }
    return tablptr[order];
}

int Generator(int loc, int type, int im, int spd, int mx)
{
    char uName[128], *p=StringUtilGetScriptStringPtr( GenType(im) );

    NoxSprintfString(uName, "%sGenerator", &p, 1);
    int mobgen = CreateObjectAt(ReadStringAddressEx(uName), LocationX(loc), LocationY(loc));
    int ptr = GetMemory(0x750710);
    /*
    (ptr+1)->z     = cur speed
    (ptr+2)->z = set speed
    (ptr+1)->dir    = unit_type
    (ptr+2)->dir= max_count
    (ptr+3)->dir= cur_count
    */
    LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", LocationX(loc), LocationY(loc)), type);
    Raise(CreateObjectAt("InvisibleLightBlueHigh", LocationX(loc), LocationY(loc)), ToFloat(spd));
    CreateObjectAt("InvisibleLightBlueHigh", LocationX(loc), LocationY(loc));
    LookWithAngle(mobgen + 2, mx);
    ObjectOff(mobgen);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x34, SummonedUnitHurt);
    SetUnitCallbackOnDeath(mobgen, DestroyGenerator);
    SetUnitMaxHealth(mobgen, 480);
    SetOwner(GetMaster(), mobgen);
    FrameTimerWithArg(1, mobgen, CheckInSightPlayer);

    return mobgen;
}

void GeneratorEx(int *dest, int loc, int type, int im, int speed, int max)
{
    int tmp = Generator(loc, type, im, speed, max);

    if (dest != NULLPTR)
        dest[0] = tmp;
}
