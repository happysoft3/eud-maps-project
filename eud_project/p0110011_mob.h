
#include "p0110011_player.h"
#include "libs/bind.h"

#define MONSTER_UNIQUE_SIZE 97

void monsterInfoLocalStorage(char **pExp, char **pMoney)
{
    char exp[MONSTER_UNIQUE_SIZE];
    char money[MONSTER_UNIQUE_SIZE];
    int *pe=pExp,*pm=pMoney;

    if (pe) pe[0]=exp;
    if (pm) pm[0]=money;
}

void giveKillCreditToPlayer()
{
    int parent = GetUnitTopParent( GetKillCredit() );

    if (IsPlayerUnit(parent))
    {
        int uniqId=GetUnitThingID(SELF)%MONSTER_UNIQUE_SIZE;
        int pIndex=GetPlayerIndex(parent);
        char *pExp, *pGold;

        monsterInfoLocalStorage(&pExp, &pGold);
        GiveExpToPlayer(pIndex, pExp[uniqId]);
        ChangeGold(parent,pGold[uniqId]);
        UpdatePlayerGold(parent);
    }
}

void onMonsterDeath()
{
    giveKillCreditToPlayer();
    DeleteObjectTimer(SELF,30);
}

void monsterCommonSettings(int mob)
{
    RetreatLevel(mob,0.0);
    AggressionLevel(mob,1.0);
    SetCallback(mob,5,onMonsterDeath);
    LookWithAngle(mob,Random(0,255));
    SetOwner(MasterUnit(),mob);
}

int putFieldMobWolf(float x, float y)
{
    int mob=CreateObjectById(OBJ_BLACK_WOLF,x,y);

    SetUnitMaxHealth(mob, 60);
    return mob;
}

int WhiteWolfBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1953065047; arr[1] = 1819236197; arr[2] = 102; arr[17] = 85; arr[19] = 120; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1114636288; arr[29] = 20; arr[31] = 8; arr[32] = 3; 
		arr[33] = 6; arr[59] = 5542784; arr[60] = 1367; arr[61] = 46902016; 
	pArr = arr;
	return pArr;
}

void WhiteWolfSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 85;	hpTable[1] = 85;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WhiteWolfBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobTurtleneckOni(float x,float y)
{
    int mon=CreateObjectById(OBJ_WHITE_WOLF,x,y);
    WhiteWolfSubProcess(mon);
    return mon;
}

int ScorpionBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919902547; arr[1] = 1852795248; arr[17] = 200; arr[19] = 70; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1067869798; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1106247680; arr[29] = 30; arr[30] = 1101004800; arr[31] = 3; arr[32] = 20; 
		arr[33] = 30; arr[34] = 25; arr[35] = 2; arr[36] = 4; arr[59] = 5543344; 
		arr[60] = 1373; arr[61] = 46895952; 
	pArr = arr;
	return pArr;
}

void ScorpionSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = ScorpionBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobScorpion(float x,float y)
{
    int mob=CreateObjectById(OBJ_SCORPION,x,y);
    ScorpionSubProcess(mob);
    SetUnitEnchantCopy(mob,GetLShift(ENCHANT_VAMPIRISM));
    return mob;
}

int SmallSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1768969068; arr[2] = 7497060; arr[17] = 300; arr[18] = 1; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1069547520; arr[26] = 4; 
		arr[27] = 5; arr[28] = 1097859072; arr[29] = 12; arr[31] = 8; arr[59] = 5542784; 
		arr[60] = 1354; arr[61] = 46906880; 
	pArr = arr;
	return pArr;
}

void SmallSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SmallSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobSmallSpider(float x,float y)
{
    int mob=CreateObjectById(OBJ_SMALL_SPIDER,x,y);
    SmallSpiderSubProcess(mob);
    return mob;
}

int ImpBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 7368009; arr[16] = 13000; arr[17] = 25; arr[18] = 1; arr[19] = 120; 
		arr[21] = 1065353216; arr[23] = 67584; arr[24] = 1067869798; arr[25] = 1; arr[26] = 5; 
		arr[37] = 1399876937; arr[38] = 7630696; arr[53] = 1128792064; arr[54] = 1; arr[55] = 3; 
		arr[56] = 6; arr[60] = 1328; arr[61] = 46904064; 
	pArr = arr;
	return pArr;
}

void ImpSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 275;	hpTable[1] = 275;
	int *uec = ptr[187];
	uec[360] = 67584;		uec[121] = ImpBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobImp(float x,float y)
{
    int mob=CreateObjectById(OBJ_IMP,x,y);
    ImpSubProcess(mob);
    SetUnitEnchantCopy(mob,GetLShift(ENCHANT_SHIELD));
    return mob;
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 220; arr[19] = 110; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; arr[35] = 3; 
		arr[36] = 20; arr[59] = 5542784; 
	pArr = arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1079194419;		ptr[137] = 1079194419;
	int *hpTable = ptr[139];
	hpTable[0] = 220;	hpTable[1] = 220;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobGoon(float x,float y)
{
    int mob=CreateObjectById(OBJ_GOON,x,y);

    GoonSubProcess(mob);
    return mob;
}

int EvilCherubBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818850885; arr[1] = 1919248451; arr[2] = 25205; arr[17] = 98; arr[19] = 75; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1066947052; arr[26] = 4; arr[37] = 1919248451; 
		arr[38] = 1916887669; arr[39] = 7827314; arr[53] = 1128792064; arr[54] = 4; arr[55] = 18; 
		arr[56] = 29; arr[60] = 1317; arr[61] = 46913536; 
	pArr = arr;
	return pArr;
}

void EvilCherubSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074790400;		ptr[137] = 1074790400;
	int *hpTable = ptr[139];
	hpTable[0] = 98;	hpTable[1] = 98;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = EvilCherubBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobGargoyle(float x, float y)
{
    int mob=CreateObjectById(OBJ_EVIL_CHERUB,x,y);

    EvilCherubSubProcess(mob);
    return mob;
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 225; 
		arr[19] = 55; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1071225242; arr[26] = 4; 
		arr[28] = 1104150528; arr[29] = 40; arr[31] = 3; arr[32] = 6; arr[33] = 15; 
		arr[59] = 5542784; arr[60] = 1388; arr[61] = 46915072; 
	pArr = arr;
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074790400;		ptr[137] = 1074790400;
	int *hpTable = ptr[139];
	hpTable[0] = 375;	hpTable[1] = 375;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobBeast(float x, float y)
{
    int mob=CreateObjectById(OBJ_WEIRDLING_BEAST,x,y);

    WeirdlingBeastSubProcess(mob);
    return mob;
}

int HecubahWithOrbBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 600; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; arr[40] = 1852140903; 
		arr[41] = 116; arr[53] = 1128792064; arr[55] = 16; arr[56] = 26; arr[60] = 1384; 
		arr[61] = 46914560; 
	pArr = arr;
	return pArr;
}

void HecubahWithOrbSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 420;	hpTable[1] = 420;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = HecubahWithOrbBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int putFieldMobHecubah(float x,float y)
{
    int mob=CreateObjectById(OBJ_HECUBAH_WITH_ORB,x,y);

    HecubahWithOrbSubProcess(mob);
    SetUnitEnchantCopy(mob,GetLShift(ENCHANT_PROTECT_FROM_FIRE)|GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY)|GetLShift(ENCHANT_PROTECT_FROM_POISON)|GetLShift(ENCHANT_REFLECTIVE_SHIELD));
    SetUnitScanRange(mob,400.0);
    return mob;
}

int putFieldMobGiganticStone(float x,float y)
{
    int gol=CreateObjectById(OBJ_STONE_GOLEM,x,y);

    SetUnitMaxHealth(gol,500);
    return gol;
}
int StrongWizardWhiteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[17] = 295; arr[19] = 60; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; arr[55] = 20; arr[56] = 30; 
	pArr = arr;
	return pArr;
}

void StrongWizardWhiteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 295;	hpTable[1] = 295;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = StrongWizardWhiteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}
int putFieldMobWhiteWiz(float x,float y)
{
    int wiz=CreateObjectById(OBJ_STRONG_WIZARD_WHITE,x,y);
    StrongWizardWhiteSubProcess(wiz);
    return wiz;
}
int putFieldMobEmberDemon(float x,float y)
{
    int mon=CreateObjectById(OBJ_EMBER_DEMON,x,y);

    SetUnitMaxHealth(mon,275);
    return mon;
}

int SpawnFieldMonster(int fn, float x, float y)
{
    int mob=Bind(fn,&fn+4);

    monsterCommonSettings(mob);
    return mob;
}

void InitializeMonsters()
{
    char *pExp, *pGold;
    monsterInfoLocalStorage(&pExp, &pGold);

    pExp[OBJ_BLACK_WOLF%MONSTER_UNIQUE_SIZE]=3;
    pGold[OBJ_BLACK_WOLF%MONSTER_UNIQUE_SIZE]=2;
    pExp[OBJ_WHITE_WOLF%MONSTER_UNIQUE_SIZE]=4;
    pGold[OBJ_WHITE_WOLF%MONSTER_UNIQUE_SIZE]=3;
    pExp[OBJ_EVIL_CHERUB%MONSTER_UNIQUE_SIZE]=5;
    pGold[OBJ_EVIL_CHERUB%MONSTER_UNIQUE_SIZE]=4;
    pExp[OBJ_SCORPION%MONSTER_UNIQUE_SIZE]=6;
    pGold[OBJ_SCORPION%MONSTER_UNIQUE_SIZE]=5;
    pExp[OBJ_GOON%MONSTER_UNIQUE_SIZE]=6;
    pGold[OBJ_GOON%MONSTER_UNIQUE_SIZE]=6;
    pExp[OBJ_IMP%MONSTER_UNIQUE_SIZE]=7;
    pGold[OBJ_IMP%MONSTER_UNIQUE_SIZE]=7;
    pExp[OBJ_SMALL_SPIDER%MONSTER_UNIQUE_SIZE]=7;
    pGold[OBJ_SMALL_SPIDER%MONSTER_UNIQUE_SIZE]=8;
    pExp[OBJ_WEIRDLING_BEAST%MONSTER_UNIQUE_SIZE]=8;
    pGold[OBJ_WEIRDLING_BEAST%MONSTER_UNIQUE_SIZE]=9;
    pExp[OBJ_HECUBAH_WITH_ORB%MONSTER_UNIQUE_SIZE]=9;
    pGold[OBJ_HECUBAH_WITH_ORB%MONSTER_UNIQUE_SIZE]=10;
    pExp[OBJ_EMBER_DEMON%MONSTER_UNIQUE_SIZE]=10;
    pGold[OBJ_EMBER_DEMON%MONSTER_UNIQUE_SIZE]=11;
    pExp[OBJ_STONE_GOLEM%MONSTER_UNIQUE_SIZE]=12;
    pGold[OBJ_STONE_GOLEM%MONSTER_UNIQUE_SIZE]=13;
    pExp[OBJ_STRONG_WIZARD_WHITE%MONSTER_UNIQUE_SIZE]=13;
    pGold[OBJ_STRONG_WIZARD_WHITE%MONSTER_UNIQUE_SIZE]=15;
}

void PlaceMobRoom3()
{
    short loc[]={37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,
    457,462,461,460,459,458,471,470,469,468,467,466,465,464,463,};
    int r=sizeof(loc);
    int fn[]={putFieldMobTurtleneckOni,putFieldMobWolf,putFieldMobWolf,};

    while (--r>=0)
        SpawnFieldMonster(fn[Random(0,2)], LocationX(loc[r]),LocationY(loc[r]));
}
void PlaceMobRoom2()
{
    short loc[]={22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,438,439,440,450,441,442,443,444,445,446,447,448,449,456,455,454,453,452,451,450,};
    int r=sizeof(loc);

    while (--r>=0)
        SpawnFieldMonster(putFieldMobWolf, LocationX(loc[r]),LocationY(loc[r]));
}

void PlaceMobRoom1()
{
    short loc[]={15,16,17,18,19,20,21,429,430,431,432,433,434,435,436,437,};
    int r=sizeof(loc);

    while (--r>=0)
        SpawnFieldMonster(putFieldMobWolf, LocationX(loc[r]),LocationY(loc[r]));
}

void PlaceMobRoom4()
{
    short loc[]={65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,};
    int r=sizeof(loc), mobFn[]={putFieldMobScorpion,putFieldMobWolf,putFieldMobWolf,putFieldMobTurtleneckOni,};

    while (--r>=0)
        SpawnFieldMonster(mobFn[Random(0,3)],LocationX(loc[r]),LocationY(loc[r]));
}
void PlaceMobRoom5()
{
    short loc[]={92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,
    121,122,123,124,125};
    int r=sizeof(loc);

    while (--r>=0)
        SpawnFieldMonster(putFieldMobScorpion,LocationX(loc[r]),LocationY(loc[r]));
}

void PlaceMobRoom6()
{
    //126~164
    int start=126,end=164;
    int mobFn[]={putFieldMobGargoyle,putFieldMobScorpion,};

    while (start<=end)
    {
        SpawnFieldMonster(mobFn[Random(0,1)],LocationX(start),LocationY(start));
        start+=1;
    }
}

void PlaceMobRoom7()
{
    int start=175,end=195;
    while (start<=end)
    {
        SpawnFieldMonster(putFieldMobGargoyle,LocationX(start),LocationY(start));
        start+=1;
    }
}

void PlaceMobRoom8()
{
    int start=196,end=220;
    int mobFn[]={putFieldMobGargoyle,putFieldMobScorpion,putFieldMobGoon};
    while (start<=end)
    {
        SpawnFieldMonster(mobFn[Random(0,2)],LocationX(start),LocationY(start));
        start+=1;
    }
}

void PlaceMobRoom9()
{
    int start=223,end=282;
    int mobFn[]={putFieldMobScorpion,putFieldMobGoon};
    while (start<=end)
    {
        SpawnFieldMonster(mobFn[Random(0,1)],LocationX(start),LocationY(start));
        start+=1;
    }
}
void PlaceMobRoomA()
{
    int start=283,end=297;
    while (start<=end)
    {
        SpawnFieldMonster(putFieldMobGoon,LocationX(start),LocationY(start));
        start+=1;
    }
}

void PlaceMobRoomB()
{
    int start=298,end=324;
    int mobFn[]={putFieldMobGargoyle,putFieldMobScorpion,putFieldMobGoon,putFieldMobSmallSpider,putFieldMobImp};
    while (start<=end)
    {
        SpawnFieldMonster(mobFn[Random(0,sizeof(mobFn)-1)],LocationX(start),LocationY(start));
        start+=1;
    }
}

void PlaceMobRoomC()
{
    int start=325,end=336;
    int mobFn[]={putFieldMobGoon,putFieldMobSmallSpider,putFieldMobImp};
    while (start<=end)
    {
        SpawnFieldMonster(mobFn[Random(0,sizeof(mobFn)-1)],LocationX(start),LocationY(start));
        start+=1;
    }
}

void PlaceMobRoomD()
{
    int start=337,end=345;
    int mobFn[]={putFieldMobBeast,putFieldMobSmallSpider,putFieldMobImp};
    while (start<=end)
    {
        SpawnFieldMonster(mobFn[Random(0,sizeof(mobFn)-1)],LocationX(start),LocationY(start));
        start+=1;
    }
}

void PlaceMobRoomE()
{
    SpawnFieldMonster(putFieldMobHecubah, LocationX(347),LocationY(347));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(348),LocationY(348));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(349),LocationY(349));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(350),LocationY(350));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(351),LocationY(351));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(352),LocationY(352));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(353),LocationY(353));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(354),LocationY(354));
}

void PlaceMobRoomF()
{
    SpawnFieldMonster(putFieldMobHecubah, LocationX(355),LocationY(355));
    SpawnFieldMonster(putFieldMobImp, LocationX(356),LocationY(356));
    SpawnFieldMonster(putFieldMobImp, LocationX(359),LocationY(359));
    SpawnFieldMonster(putFieldMobImp, LocationX(362),LocationY(362));
    SpawnFieldMonster(putFieldMobImp, LocationX(365),LocationY(365));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(357),LocationY(357));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(358),LocationY(358));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(360),LocationY(360));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(361),LocationY(361));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(363),LocationY(363));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(366),LocationY(366));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(364),LocationY(364));
}

void PlaceMobRoomG()
{
    SpawnFieldMonster(putFieldMobGiganticStone,LocationX(383),LocationY(383));
    SpawnFieldMonster(putFieldMobGiganticStone,LocationX(384),LocationY(384));

    SpawnFieldMonster(putFieldMobHecubah, LocationX(367),LocationY(367));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(379),LocationY(379));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(375),LocationY(375));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(371),LocationY(371));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(370),LocationY(370));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(382),LocationY(382));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(378),LocationY(378));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(374),LocationY(374));

    SpawnFieldMonster(putFieldMobImp, LocationX(368),LocationY(368));
    SpawnFieldMonster(putFieldMobImp, LocationX(380),LocationY(380));
    SpawnFieldMonster(putFieldMobImp, LocationX(369),LocationY(369));
    SpawnFieldMonster(putFieldMobImp, LocationX(381),LocationY(381));
    SpawnFieldMonster(putFieldMobImp, LocationX(376),LocationY(376));
    SpawnFieldMonster(putFieldMobImp, LocationX(372),LocationY(372));
    SpawnFieldMonster(putFieldMobImp, LocationX(377),LocationY(377));
    SpawnFieldMonster(putFieldMobImp, LocationX(373),LocationY(373));
}

void PlaceMobRoomH()
{
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(394),LocationY(394));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(395),LocationY(395));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(396),LocationY(396));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(397),LocationY(397));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(398),LocationY(398));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(399),LocationY(399));
    SpawnFieldMonster(putFieldMobGiganticStone, LocationX(393),LocationY(393));
    SpawnFieldMonster(putFieldMobGiganticStone, LocationX(408),LocationY(408));
    SpawnFieldMonster(putFieldMobGiganticStone, LocationX(409),LocationY(409));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(385),LocationY(385));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(386),LocationY(386));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(387),LocationY(387));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(388),LocationY(388));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(389),LocationY(389));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(390),LocationY(390));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(391),LocationY(391));
    SpawnFieldMonster(putFieldMobHecubah, LocationX(392),LocationY(392));
}

void PlaceMobRoomI()
{
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(410),LocationY(410));
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(411),LocationY(411));
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(412),LocationY(412));
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(413),LocationY(413));
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(414),LocationY(414));
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(415),LocationY(415));
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(416),LocationY(416));
    SpawnFieldMonster(putFieldMobWhiteWiz, LocationX(417),LocationY(417));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(418),LocationY(418));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(419),LocationY(419));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(420),LocationY(420));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(421),LocationY(421));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(422),LocationY(422));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(423),LocationY(423));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(424),LocationY(424));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(425),LocationY(425));
    SpawnFieldMonster(putFieldMobEmberDemon, LocationX(426),LocationY(426));
}
