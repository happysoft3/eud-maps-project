
#include "exted_resource.h"
#include "libs/networkrev.h"
#include "libs/unitstruct.h"
#include "libs/voicelist.h"
#include "libs/cmdline.h"

int m_mobCount;

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 350; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 34817; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1101004800; arr[29] = 20; arr[32] = 6; arr[33] = 12; arr[58] = 5546320; 
		arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328;
        link=arr;
	}
	return link;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076677837);
		SetMemory(ptr + 0x224, 1076677837);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34817);
		SetMemory(GetMemory(ptr + 0x22c), 350);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 350);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void onBeachGirlDeath()
{
    m_mobCount-=1;
}

int summonBeachGirl(float x,float y){
	int mon=CreateObjectById(OBJ_AIRSHIP_CAPTAIN,x,y);
	AirshipCaptainSubProcess(mon);
	SetUnitVoice(mon,MONSTER_VOICE__Maiden2);
    SetCallback(mon,5,onBeachGirlDeath);
	return mon;
}

void summonMon()
{
    if (m_mobCount>0)
        return;

    float xy[]={GetWaypointX(66),GetWaypointY(66),};
    summonBeachGirl(xy[0],xy[1]);
    summonBeachGirl(xy[0],xy[1]);
    summonBeachGirl(xy[0],xy[1]);
    summonBeachGirl(xy[0],xy[1]);
    summonBeachGirl(xy[0],xy[1]);
    m_mobCount+=5;
}

static void NetworkUtilClientMain()
{
    InitializeResources();
    CmdLine("say i came in", FALSE); //debug
}


static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    // if (pUnit)
    //     ShowQuestIntroOne(pUnit, 9999, "WarriorChapterBegin9", "Noxworld.wnd:Ready");
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}


void MapInitialize()
{
    MusicEvent();
}

void MapExit()
{
    MusicEvent();
}
