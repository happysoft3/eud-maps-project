
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/format.h"
#include "libs/printutil.h"
#include "libs/weapon_effect.h"
#include "libs/itemproperty.h"
#include "libs/queueTimer.h"
#include "libs/linked_list.h"
#include "libs/objectIDdefines.h"

int m_myFlame;
int m_srcLift;
int m_destPit;
short m_myArray[10];
short *m_mysss;
short m_mobHP[97]; //monster_health
short m_monCount[97]; //monster_count
short *m_areaMarks1;
short *m_areaMarks2;
short *m_areaMarks3;
short *m_areaMarks4;

#define CUSTOM_HIT_PROPERTY_PTR 2
#define CUSTOM_HIT_WEAPON_PTR 3
#define CUSTOM_HIT_USER_PTR 4
#define CUSTOM_HIT_VICTIM_PTR 5

int CreateCustomHitWeaponProperty(int execId)
{
    int ret;
    char code[]={
        0x56, 0x8B, 0x74, 0x24, 0x14, 0xF6, 0x46, 0x08, 0x06, 0x74, 0x1E, 0x8B, 
        0xF4, 0xB8, 0x30, 0x72, 0x50, 0x00, 0x56, 0xFF, 0xD0, 0x5E, 0x6A, 0x00, 
        0x6A, 0x00, 0x68, 0x80, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5E, 0xC3, 0x90
    };
    AllocSmartMemEx(sizeof(code), &ret);
    NoxByteMemCopy(code, ret, sizeof(code));
    int*p=ret+27;
    p[0]=execId;
    return ret;
}

#define ENCHANTMENT_FIELD_LENGTH 144
int CloneEquipmentEnchantment(int *ordinaryOff)
{
    int ret;

    AllocSmartMemEx(ENCHANTMENT_FIELD_LENGTH, &ret);
    NoxByteMemCopy(ordinaryOff[0], ret, ENCHANTMENT_FIELD_LENGTH);
    return ret;
}

static void replaceElevatorDestination(int srcelev, int dest)
{
    int *ptr=UnitToPtr(srcelev);

    SetMemory(GetMemory(ptr+0x2ec)+4, UnitToPtr(dest));
}

static void deferredArray(short *p)
{
    UniPrintToAll(IntToString(p[0]));
    UniPrintToAll(IntToString(p[1]));
    UniPrintToAll(IntToString(p[2]));
}

static void initSpawnMarker()
{
    //41-66
    short area1[]={248,249,250,
    41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,
    238,239,251,252,253,254,255,256,257,258,373,374,375,376,377,378,379,380,381,382,383,384,385
    ,0};
    //71-122
    short area2[]={71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,
        101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122
        ,0};

    //131-163
    short area3[]={131,132,133,134,135,136,137,138,139,260, 261,262,263,264,140,141,142,143,144,145,146,147,148,149,150,151,
        152,153,154,155
        ,0};

    short area4[]={
        156,157,158,159,160,161,162,163,235,236,237,233,234,265,266,
        242,243,244,245,246,247,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,
        283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,
        308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,
        331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,
        354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372
        ,0};
    m_areaMarks1=area1;
    m_areaMarks2=area2;
    m_areaMarks3=area3;
    m_areaMarks4=area4;
}

#define HORRENDOUS_SPECIAL_UNIT 0
#define HORRENDOUS_SPECIAL_ARRAY 1
#define HORRENDOUS_SPECIAL_COUNT 2
#define HORRENDOUS_SPECIAL_MAX 3

#define MAX_HORRENDOUS_MIRRORCOUNT 4

static void procHorrendousSpecial(int *p)
{
    int mon = p[HORRENDOUS_SPECIAL_UNIT];
    int mirror = p[HORRENDOUS_SPECIAL_ARRAY];
    int count;

    if (MaxHealth(mon))
    {
        if (CurrentHealth(mon))
        {
            FrameTimerWithArg(1, p, procHorrendousSpecial);
            float height=10.0;
            count=p[HORRENDOUS_SPECIAL_COUNT];
            while (--count>=0)
            {
                Raise(mirror+count, height);
                height+=20.0;
            }
            if (ToInt(DistanceUnitToUnit(mon, mirror)))
            {
                float x=GetObjectX(mon), y=GetObjectY(mon);
                count=p[HORRENDOUS_SPECIAL_COUNT];
                while (--count>=0)
                    MoveObject(mirror+count, x,y);
            }
            return;
        }
        count = p[HORRENDOUS_SPECIAL_COUNT];
        while (--count>=0)
        {
            SetUnitMaxHealth(mirror+count, 1);
            Damage(mirror+count, 0, MaxHealth(mon)+1, DAMAGE_TYPE_PLASMA);
        }
    }
    FreeSmartMemEx(p);
}

static int createTallHorrendous(float x, float y)
{
    int mon=CreateObjectById(OBJ_HORRENDOUS, x,y);
    int mirror=mon+1;
    int count = MAX_HORRENDOUS_MIRRORCOUNT;

    while (--count>=0)
        CreateObjectById(OBJ_HORRENDOUS, x,y);
    int *pmem;

    AllocSmartMemEx(HORRENDOUS_SPECIAL_MAX*4, &pmem);
    pmem[HORRENDOUS_SPECIAL_UNIT]=mon;
    pmem[HORRENDOUS_SPECIAL_ARRAY]=mirror;
    pmem[HORRENDOUS_SPECIAL_COUNT]=MAX_HORRENDOUS_MIRRORCOUNT;
    // FrameTimerWithArg(1, pmem, procHorrendousSpecial);
    PushTimerQueue(1, pmem, procHorrendousSpecial);
    count = MAX_HORRENDOUS_MIRRORCOUNT;

    while (--count>=0)
    {
        SetUnitMaxHealth(mirror+count, 0); //invincible
        UnitNoCollide(mirror+count);
        Enchant(mirror+count, "ENCHANT_HELD", 0.0);
    }
    return mon;
}

int WillOWispBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1819044183; arr[1] = 1936283471; arr[2] = 112; arr[17] = 50; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 32769; arr[24] = 1069547520; arr[27] = 1; arr[28] = 1113325568; 
		arr[29] = 2; arr[31] = 9; arr[32] = 16; arr[33] = 22; arr[53] = 1128792064; 
		arr[54] = 4; arr[59] = 5542784; arr[60] = 1326; arr[61] = 46913280; 
	pArr = &arr;
	return pArr;
}

void WillOWispSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = 325;	hpTable[1] = 325;
	int *uec = ptr[187];
	uec[360] = 32769;		uec[121] = WillOWispBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

static void onWispCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(SELF))
    {
        if (GetUnitThingID(OTHER)==OBJ_HARPOON_BOLT)
        {
            DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION, GetObjectX(SELF), GetObjectY(SELF)), 12);
            Damage(SELF, OTHER, 10, DAMAGE_TYPE_BLADE);
            Enchant(SELF, "ENCHANT_HELD", 0.1);
        }
    }
}

void MapTesting()
{
    // createTallHorrendous(GetObjectX(OTHER), GetObjectY(OTHER));
    int wisp=CreateObjectById(OBJ_WILL_O_WISP, GetObjectX(OTHER), GetObjectY(OTHER));

    WillOWispSubProcess(wisp);
    SetCallback(wisp, 9, onWispCollide);
}

static void createEquipments(string *p)
{
    int count, item;
    string n = p[count++];
    char buff[128];

    if (SToInt( n) )
    {
        item= CreateObject(n, 46);
        UnitNoCollide(item);
        FrameTimerWithArg(1, p, createEquipments);
        int args[]={StringUtilGetScriptStringPtr(n), MaxHealth(item)};
        NoxSprintfString(buff, "itemName=%s. HP=%d", &args,sizeof(args));
        UniPrintToAll(ReadStringAddressEx(buff));
    }
}

static void placingEquipments()
{
    string list[]={
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "ChainCoif",
        "ChainLeggings", "ChainTunic", "ConjurerHelm", "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots",
        "LeatherBoots", "LeatherHelm", "LeatherLeggings", "LesserFireballWand", "WizardRobe", "WizardHelm",
        "MedievalCloak", "WarHammer", "GreatSword", "Longsword", "Sword", "MorningStar", "BattleAxe","OgreAxe", 0
    };
    FrameTimerWithArg(1, list, createEquipments);
}

static void onCustomHit(int *p)
{
    int *tb=p[CUSTOM_HIT_PROPERTY_PTR];
    int *ptr=p[CUSTOM_HIT_VICTIM_PTR];
    int *ownerPtr=p[CUSTOM_HIT_USER_PTR];
    float durate[]={0.1, 0.4, 0.8, 1.2, 1.6};

    Enchant( ptr[11], "ENCHANT_AFRAID", durate[tb[15]]);
    Damage(ptr[11], ownerPtr[11], tb[15], DAMAGE_TYPE_PLASMA);
    float xy[]={ptr[14],ptr[15]};
    Effect("CYAN_SPARKS", xy[0],xy[1],0.0,0.0);
}

static void initProperties()
{
    int *property = CloneEquipmentEnchantment(ITEM_PROPERTY_confuse1);

    property[13]= CreateCustomHitWeaponProperty(onCustomHit);
    property[15]=1;

    int h=CreateObject("WarHammer", 27);

    SetWeaponPropertiesDirect(h, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, &property, 0);


    int *property2 = CloneEquipmentEnchantment(ITEM_PROPERTY_confuse4);

    property2[13]= CreateCustomHitWeaponProperty(onCustomHit);
    property2[15]=4;

    int h2=CreateObject("WarHammer", 52);

    SetWeaponPropertiesDirect(h2, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, &property2, 0);
}

static void deferredReferenceArray(int *pArr)
{
    int i;
    for (i = 0 ; i < 10 ; i ++)
        UniPrintToAll(IntToString(pArr[i]));
}

#define X_MARK_MOVING 4.0

static void drawXToLeft(int left)
{
    int count=GetDirection(left);

    if (count)
    {
        FrameTimerWithArg(1, left, drawXToLeft);
        DeleteObjectTimer(CreateObjectAt("HealOrb", GetObjectX(left), GetObjectY(left)), 20);
        LookWithAngle(left, count-1);
        MoveObjectVector(left, X_MARK_MOVING, X_MARK_MOVING);
        return;
    }
    Delete(left);
}

static void drawXToRight(int right)
{
    int count=GetDirection(right);

    if (count)
    {
        FrameTimerWithArg(1, right, drawXToRight);
        DeleteObjectTimer(CreateObjectAt("HealOrb", GetObjectX(right), GetObjectY(right)), 20);
        MoveObjectVector(right, -X_MARK_MOVING, X_MARK_MOVING);
        LookWithAngle(right, count-1);
        return;
    }
    Delete(right);
}

#define X_MARK_START_ALIGN 40.0
static void drawXMark(float x, float y)
{
    int left=CreateObjectAt("InvisibleLightBlueLow", x-X_MARK_START_ALIGN,y-X_MARK_START_ALIGN);
    int right=CreateObjectAt("InvisibleLightBlueLow", x+X_MARK_START_ALIGN,y-X_MARK_START_ALIGN);

    FrameTimerWithArg(1, left, drawXToLeft);
    FrameTimerWithArg(1, right, drawXToRight);
    LookWithAngle(left, 20);
    LookWithAngle(right, 20);
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

static void MapExit()
{
    ResetHostileCritter();
}

static void MapInitialize()
{
    // CreateLogFile("test1109-log.txt");
    SetHostileCritter();
    initSpawnMarker();
    
    m_myFlame=Object("myflame");
    m_srcLift=Object("srcLift");
    m_destPit=Object("destPit");
    FrameTimer(30, placingEquipments);
    FrameTimer(60, initProperties);
}

static void wrappedDeleteObject(int obj)
{
    if (ToInt(GetObjectX(obj)))
        Delete(obj);
}

static void startThunderBolt(int target)
{
    float x=GetObjectX(target),y=GetObjectY(target);
    int sub=CreateObjectAt("MovableStatueVictory4S", x,y);
    int to= CreateObjectAt("Orb", x+UnitAngleCos(target, 11.0),y+UnitAngleSin(target,11.0));

    SecondTimerWithArg(3, sub, wrappedDeleteObject);
    SecondTimerWithArg(3, to, wrappedDeleteObject);
    Frozen(to, TRUE);
    UnitNoCollide(sub);
    UnitNoCollide(to);
    CastSpellObjectObject("SPELL_CHAIN_LIGHTNING", sub, to);
}

static void showMessage(int s)
{
    char buff[64];
    NoxSprintfString(buff, "%d초 후...", &s, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

void ReportCompleteTest()
{
    startThunderBolt(OTHER);
    drawXMark(GetObjectX(OTHER), GetObjectY(OTHER));
    PushTimerQueue(30, 44, showMessage);
}
