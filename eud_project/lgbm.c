
#include "lgbm_utils.h"
#include "lgbm_resource.h"
#include "libs/printutil.h"
#include "libs/monsteraction.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/waypoint.h"
#include "libs/playerupdate.h"
#include "libs/coopteam.h"
#include "libs/wallutil.h"
#include "libs/potionex.h"
#include "libs/fixtellstory.h"
#include "libs/itemproperty.h"
#include "libs/weaponcapacity.h"
#include "libs/weapon_effect.h"
#include "libs/shop.h"
#include "libs/mathlab.h"
#include "libs/objectIDdefines.h"
#include "libs/game_flags.h"
#include "libs/groupUtils.h"
#include "libs/queueTimer.h"

object m_elev2f;
int m_player[10];
int m_playerFlag[10];
int m_maplastUnit;

string *m_weaponNameTable;
int m_weaponNameTableLength;
string *m_armorNameTable;
int m_armorNameTableLength;
string *m_potionNameTable;
int m_potionNameTableLength;

string *m_staffNameTable;
int m_staffNameTableLength;

int *m_itemFunctionTable;
int m_itemFunctionTableLength;

string *m_defaultItemNameTable;
int m_defaultItemNameTableLength;

int m_orbRed;
int m_orbBlue;
int m_orbGreen;

int m_keyBase[3];

int *m_dungeonLocations1;
int m_dungeonLocationLength1;

int *m_dungeonMobFunctions1;
int m_dungeonMobFunctionLength1;

int *m_weaponProperty1;
// int *m_weaponPropertyFx1;
int *m_weaponPropertyExec1;

int *m_weaponProperty2;
// int *m_weaponPropertyFx2;
int *m_weaponPropertyExec2;

int m_stoneGiantKills;


#define PLAYER_DEATH_FLAG 2
#define PLAYER_EXCHANGE_ABILITY 4
#define PLAYER_FLAG_ALL_BUFF 8


int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);

    return x;
}

void Start2FloorElev()
{
    ObjectOff(SELF);
    ObjectOn(m_elev2f);
    UniPrint(OTHER, "2층으로 가는 엘리베이터가 작동을 시작합니다");
}

int StoneGolemBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852798035; arr[1] = 1819232101; arr[2] = 28005; arr[17] = 800; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 5; 
		arr[28] = 1112014848; arr[29] = 100; arr[31] = 2; arr[32] = 9; arr[33] = 13; 
		arr[58] = 5545888; arr[59] = 5543904; arr[60] = 1324; arr[61] = 46901248; 
	pArr = &arr;
	return pArr;
}

void StoneGolemSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = 800;	hpTable[1] = 800;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = StoneGolemBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonStoneGiant(int a0, int *a1)
{
    int stone = CreateObjectAt("StoneGolem", LocationX(a0), LocationY(a0));

    if (a1)
        a1[0] = stone;
    StoneGolemSubProcess(stone);
    MonsterCommonProcess(stone);
}

void CreateWasp(float xpos, float ypos, int *pDest)
{
    int wasp = CreateObjectAt("Wasp", xpos, ypos);

    if (pDest)
        pDest[0] = wasp;
    SetUnitMaxHealth(wasp, 151);
    MonsterCommonProcess(wasp);
}

void SummonMultitudeWasp(int source)
{
    while (IsObjectOn(source))
    {
        int remain = GetDirection(source);

        if (remain)
        {
            FrameTimerWithArg(3, source, SummonMultitudeWasp);
            LookWithAngle(source, --remain);
            CreateWasp(GetObjectX(source), GetObjectY(source), 0);
            Effect("SMOKE_BLAST", GetObjectX(source), GetObjectY(source), 0.0, 0.0);
            break;
        }
        Delete(source);
        break;
    }
}

void WaspNestOnDestroy()
{
    int source = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));

    LookWithAngle(source, 32);
    DeleteObjectTimer(CreateObjectAt("BigSmoke", GetObjectX(source), GetObjectY(source)), 12);
    PlaySoundAround(source, SOUND_TrollFlatus);
    PlaySoundAround(source, SOUND_PoisonTrapTriggered);
    FrameTimerWithArg(1, source, SummonMultitudeWasp);
    CreateObjectAt("WaspNestDestroy", GetObjectX(SELF), GetObjectY(SELF));
    Delete(SELF);
}

void PlacingWaspNest(int locationId, int *pDest)
{
    int nest = CreateObjectAt("WaspNest", LocationX(locationId), LocationY(locationId));

    if (pDest)
        pDest[0] = nest;
    SetUnitMaxHealth(nest, 240);
    SetUnitCallbackOnDeath(nest, WaspNestOnDestroy);
}

int LichLordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 530; arr[19] = 86; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1113325568; arr[29] = 100; arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; 
		arr[57] = 5548288; arr[59] = 5542784; 
	pArr = &arr;
	return pArr;
}

int HecubahBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 650; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 100; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; 
		arr[57] = 5548288; arr[59] = 5542784; 
	pArr = &arr;
	return pArr;
}

void HecubahSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 650;	hpTable[1] = 650;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = HecubahBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonLv3Hecubah(int lid, int *parg0)
{
    int mob = CreateObjectAt("Hecubah", LocationX(lid), LocationY(lid));

    if (parg0)
        parg0[0] = mob;
    HecubahSubProcess(mob);
    MonsterCommonProcess(mob);
}

int WhiteWolfBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1953065047; arr[1] = 1819236197; arr[2] = 102; arr[17] = 250; arr[19] = 62; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069211976; arr[25] = 1; arr[26] = 5; 
		arr[27] = 5; arr[28] = 1103626240; arr[29] = 60; arr[31] = 8; arr[32] = 3; 
		arr[33] = 6; arr[59] = 5542784; arr[60] = 1367; arr[61] = 46902016; 
	pArr = &arr;
	return pArr;
}

void WhiteWolfSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072567419;		ptr[137] = 1072567419;
	int *hpTable = ptr[139];
	hpTable[0] = 250;	hpTable[1] = 250;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WhiteWolfBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void CreateDeepForestWolf(int locationId)
{
    int mon = CreateObjectAt("WhiteWolf", LocationX(locationId), LocationY(locationId));

    WhiteWolfSubProcess(mon);
    MonsterCommonProcess(mon);
}

int OgreWarlordBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996367; arr[1] = 1819435351; arr[2] = 6582895; arr[17] = 360; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1068540887; arr[25] = 2; arr[26] = 10; 
		arr[27] = 7; arr[28] = 1109393408; arr[29] = 57; arr[30] = 1112014848; arr[31] = 11; 
		arr[32] = 3; arr[33] = 3; arr[37] = 1701996367; arr[38] = 1920297043; arr[39] = 1852140393; 
		arr[53] = 1128792064; arr[54] = 4; arr[55] = 5; arr[56] = 11; arr[57] = 5548368; 
		arr[59] = 5542784; arr[60] = 1389; arr[61] = 46914048; 
	pArr = &arr;
	return pArr;
}

void OgreWarlordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 360;	hpTable[1] = 360;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = OgreWarlordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void CreateDeepForestOgre(int locationId)
{
    int mon = CreateObjectAt("OgreWarlord", LocationX(locationId), LocationY(locationId));

    OgreWarlordSubProcess(mon);
    MonsterCommonProcess(mon);
}

int StoneGolemBinTable2()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852798035; arr[1] = 1819232101; arr[2] = 28005; arr[17] = 1200; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[27] = 5; 
		arr[28] = 1116471296; arr[29] = 100; arr[31] = 2; arr[32] = 3; arr[33] = 3; 
		arr[58] = 5545888; arr[59] = 5543904; arr[60] = 1324; arr[61] = 46901248; 
	pArr = &arr;
	return pArr;
}

void StoneGolemSubProcess2(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 1200;	hpTable[1] = 1200;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = StoneGolemBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonStoneGiant2(float xpos, float ypos, int *pDest)
{
    int mon = CreateObjectAt("StoneGolem", xpos, ypos);

    if (pDest)
        pDest[0] = mon;

    StoneGolemSubProcess2(mon);
}

int HorrendousBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 600; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 9; 
		arr[27] = 5; arr[28] = 1112014848; arr[29] = 34; arr[54] = 4; arr[59] = 5542784; 
		arr[60] = 1386; arr[61] = 46907648; 
	pArr = &arr;
	return pArr;
}

void HorrendousSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 600;	hpTable[1] = 600;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = HorrendousBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonHorrendous(int locationId, int *pDest)
{
    int mob = CreateObjectAt("Horrendous", LocationX(locationId), LocationY(locationId));

    if (pDest)
        pDest[0] = mob;
    HorrendousSubProcess(mob);
    MonsterCommonProcess(mob);
}

int FireSpriteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 64; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 20; arr[56] = 28; 
		arr[58] = 5545472; 
	pArr = &arr;
	return pArr;
}

void FireSpriteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 192;	hpTable[1] = 192;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = FireSpriteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonLv1FireFairy(int lid, int *parg0)
{
    int mob = CreateObjectAt("FireSprite", LocationX(lid), LocationY(lid));

    if (parg0)
        parg0[0] = mob;

    FireSpriteSubProcess(mob);
    MonsterCommonProcess(mob);
}

int CarnivorousPlantBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 666; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[27] = 5; 
		arr[28] = 1109393408; arr[29] = 100; arr[31] = 8; arr[59] = 5542784; arr[60] = 1371; 
		arr[61] = 46901760; 
	pArr = &arr;
	return pArr;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 666;	hpTable[1] = 666;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = CarnivorousPlantBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonLv3Plant(int lid, int *parg0)
{
    int mob = CreateObjectAt("CarnivorousPlant", LocationX(lid), LocationY(lid));

    if (parg0)
        parg0[0] = mob;

    CarnivorousPlantSubProcess(mob);
    SetUnitScanRange(mob, 300.0);
    MonsterCommonProcess(mob);
}

int GoonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1852796743; arr[17] = 130; arr[19] = 80; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 30; arr[35] = 5; 
		arr[36] = 100; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
	pArr = &arr;
	return pArr;
}

void GoonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GoonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonLv1Goon(int lid, int *parg0)
{
    int mob = CreateObjectAt("Goon", LocationX(lid), LocationY(lid));

    if (parg0)
        parg0[0] = mob;

    GoonSubProcess(mob);
    MonsterCommonProcess(mob);
}

int Bear2BinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1918985538; arr[1] = 50; arr[17] = 175; arr[19] = 88; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[27] = 1; arr[28] = 1106247680; arr[29] = 20; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30; arr[58] = 5547856; 
		arr[59] = 5542784; 
	pArr = &arr;
	return pArr;
}

void Bear2SubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076426178;		ptr[137] = 1076426178;
	int *hpTable = ptr[139];
	hpTable[0] = 175;	hpTable[1] = 175;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = Bear2BinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void SummonLv1Maiden(int locationId, int *pDest)
{
    int mob = CreateSingleColorMaidenAt(Random(0, 255), Random(0, 255), Random(0, 255), LocationX(locationId), LocationY(locationId));

    if (pDest)
        pDest[0] = mob;
    Bear2SubProcess(mob);
}

void LichLordSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076174520;		ptr[137] = 1076174520;
	int *hpTable = ptr[139];
	hpTable[0] = 530;	hpTable[1] = 530;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = LichLordBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void OnHearEnemy()
{
    if (MonsterGetCurrentAction(SELF) != 0)
        return;

    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);

        Attack(SELF, OTHER);
    }
}

void MonsterCommonProcess(int mon)
{
    SetCallback(mon, 5, MonsterOnDeath);
    SetCallback(mon, 7, MonsterCommonHitEvent);
    SetCallback(mon, 10, OnHearEnemy);
    AggressionLevel(mon, 1.0);
    RetreatLevel(mon, 0.0);
}

void SummonLv3DeadWalker(int locId, int *pDest)
{
    int mob = CreateObjectAt("LichLord", LocationX(locId), LocationY(locId));

    if (pDest)
        pDest[0] = mob;
    LichLordSubProcess(mob);
    MonsterCommonProcess(mob);
}

void DungeonMonCreateProto(int functionId, int locationId, int *pDest)
{
    Bind(functionId, &functionId+4);
}

void OncePlacingDungeonMob1()
{
    int count;

    if (count < m_dungeonLocationLength1)
    {
        DungeonMonCreateProto(m_dungeonMobFunctions1[Random(0, m_dungeonMobFunctionLength1-1)], m_dungeonLocations1[count], NULLPTR);
        ++ count;
        FrameTimer(1, OncePlacingDungeonMob1);
    }
}

int SwordsmanBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919907667; arr[1] = 1634562916; arr[2] = 110; arr[17] = 250; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1069547520; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1106247680; arr[29] = 20; arr[57] = 5547984; arr[59] = 5542784; arr[60] = 1346; 
		arr[61] = 46900480; 
	pArr = &arr;
	return pArr;
}

void SwordsmanSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 250;	hpTable[1] = 250;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = SwordsmanBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void KeybaseCollide()
{
    if (!GetTrigger())
        return;

    if (IsPlayerUnit(OTHER))
        return;

    if (IsMonsterUnit(OTHER))
        return;

    int thingId = GetUnitThingID(OTHER);

    if (thingId>=245 && thingId<=247)
    {
        Delete(OTHER);
        PlaySoundAround(SELF, SOUND_AwardSpell);
        Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        int fxglow = CreateObjectAt("TeleportWake", GetObjectX(SELF), GetObjectY(SELF));
        Frozen(fxglow, TRUE);
        UnitNoCollide(fxglow);
        int index= GetDirection(SELF) &3;
        ObjectOn(m_keyBase[index]);
        Delete(SELF);
    }
}

void DoDataExchange()
{
    m_elev2f = Object("elev2f");
    m_keyBase[0] = Object("keybase1");
    m_keyBase[1] = Object("keybase2");
    m_keyBase[2] = Object("keybase3");

    int baseu1 = CreateObjectAt("WeirdlingBeast", GetObjectX(m_keyBase[0]), GetObjectY(m_keyBase[0]));
    int baseu2 = CreateObjectAt("WeirdlingBeast", GetObjectX(m_keyBase[1]), GetObjectY(m_keyBase[1]));
    int baseu3 = CreateObjectAt("WeirdlingBeast", GetObjectX(m_keyBase[2]), GetObjectY(m_keyBase[2]));

    Damage(baseu1, 0, MaxHealth(baseu1)+1, -1);
    Damage(baseu2, 0, MaxHealth(baseu2)+1, -1);
    Damage(baseu3, 0, MaxHealth(baseu3)+1, -1);
    SetCallback(baseu1, 9, KeybaseCollide);
    SetCallback(baseu2, 9, KeybaseCollide);
    SetCallback(baseu3, 9, KeybaseCollide);
    LookWithAngle(baseu1, 0);
    LookWithAngle(baseu2, 1);
    LookWithAngle(baseu3, 2);
}

void MapExit()
{
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}

void CreateRandomPotion(float xpos, float ypos, int *pDest)
{
    if (!SToInt(m_potionNameTable))
        return;

    int potion = CreateObjectAt(m_potionNameTable[Random(0, m_potionNameTableLength-1)], xpos, ypos);

    CheckPotionThingID(potion);
    if (pDest)
        pDest[0] = potion;
    if (MaxHealth(potion))
        SetUnitMaxHealth(potion, 0);
}

void CreateHotPotion(float xpos, float ypos, int *pDest)
{
    int potion = CreateObjectAt("RedPotion", xpos, ypos);

    if (pDest)
        pDest[0] = potion;
}

void CreateMagicalStaff(float xpos, float ypos, int *pDest)
{
    int wand = CreateObjectAt(m_staffNameTable[Random(0, m_staffNameTableLength-1)], xpos, ypos);

    if (pDest)
        pDest[0] = wand;
    // Frozen(wand, TRUE);
}

void CreateRandomWeapon(float xpos, float ypos, int *pDest)
{
    if (!SToInt(m_weaponNameTable))
        return;

    int weapon = CreateObjectAt(m_weaponNameTable[Random(0, m_weaponNameTableLength-1)], xpos, ypos);

    if (pDest)
        pDest[0] = weapon;
    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    
    int thingId = GetUnitThingID(weapon);

    if (thingId>=222&&thingId<=225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId==1178||1168==thingId)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
}

void CreateRandomArmor(float xpos, float ypos, int *pDest)
{
    if (!SToInt(m_armorNameTable))
        return;

    int armor = CreateObjectAt(m_armorNameTable[Random(0, m_armorNameTableLength-1)], xpos, ypos);

    if (pDest)
        pDest[0] = armor;

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
}

void CreateCoin(float xpos, float ypos, int *pDest)
{
    int coin = CreateObjectAt("Gold", xpos, ypos);

    if (pDest)
        pDest[0] = coin;
    UnitStructSetGoldAmount(coin, Random(1000, 5000));
}

void GreenRoomEnemies()
{
    SummonLv1Goon(102, 0);
    SummonLv1Goon(103, 0);
    SummonLv1Goon(101, 0);
    SummonLv1Goon(104, 0);
    SummonLv1Goon(105, 0);
    SummonLv1Goon(106, 0);
    SummonLv1Goon(107, 0);
}

void PlacingHorrens()
{
    int locations[] = {108, 109,110,111,112,113,114,115};
    int u=0;

    for (TRUE ; u < sizeof(locations) ; Nop(++u))
        SummonHorrendous(locations[u], 0);
}

void PlacingSpecialShop()
{
    PlacingInvincibleItemShop(122);
    PlacingFireswordShop(123);
    PlacingWindboostShop(124);
    PlacingAllEnchantShop(125);
    PlacingDeathraySwordShop(126);
}

void PlacingEntranceGlow()
{
    int glow = CreateObjectAt("TeleportWake", LocationX(22), LocationY(22));

    Frozen(glow, TRUE);
    UnitNoCollide(glow);

    SetUnitEnchantCopy( CreateObjectAt("InvisibleLightBlueLow", GetObjectX(glow), GetObjectY(glow)), GetLShift(ENCHANT_ANCHORED));
}

void PlacingDeepForestEnemies()
{
    PlacingWaspNest(132, 0);
    PlacingWaspNest(133, 0);
    PlacingWaspNest(134, 0);
    PlacingWaspNest(135, 0);
    PlacingWaspNest(136, 0);
    PlacingWaspNest(137, 0);
    PlacingWaspNest(138, 0);
    PlacingWaspNest(139, 0);
    PlacingWaspNest(140, 0);
    PlacingWaspNest(141, 0);

    int rep=-1;

    while (++rep<16)
        MonSwordsman(142+rep, 0);
}

void PlacingDeepForestEnemies2()
{
    CreateDeepForestOgre(158);
    CreateDeepForestOgre(159);
    CreateDeepForestOgre(160);
    CreateDeepForestOgre(161);
    CreateDeepForestOgre(162);
    CreateDeepForestOgre(163);
    CreateDeepForestOgre(164);
    CreateDeepForestOgre(165);
    CreateDeepForestOgre(166);

    CreateDeepForestWolf(167);
    CreateDeepForestWolf(168);
    CreateDeepForestWolf(169);
    CreateDeepForestWolf(170);
    CreateDeepForestWolf(171);
    CreateDeepForestWolf(172);
    CreateDeepForestWolf(173);
    CreateDeepForestWolf(174);
    CreateDeepForestWolf(175);
    CreateDeepForestWolf(176);
    CreateDeepForestWolf(177);
    CreateDeepForestWolf(178);
    CreateDeepForestWolf(179);
    CreateDeepForestWolf(180);
    CreateDeepForestWolf(181);
    CreateDeepForestWolf(182);
    CreateDeepForestWolf(183);
    CreateDeepForestWolf(184);
    CreateDeepForestWolf(185);
    CreateDeepForestWolf(186);
    CreateDeepForestWolf(187);
    CreateDeepForestWolf(188);
    CreateDeepForestWolf(189);
    CreateDeepForestWolf(190);
    CreateDeepForestWolf(191);
}

void PlacingMapEnemies()
{
    EntranceEnemies();
    OncePlacingDungeonMob1();
    GreenRoomEnemies();
    PlacingHorrens();
    PlacingSpecialShop();
    PlacingEntranceGlow();
    FrameTimer(1, PlacingDeepForestEnemies);
    FrameTimer(2, PlacingDeepForestEnemies2);
    BanditEventStandby();
}

void BanditOnDetectedEnemyNothing()
{ }

void BanditOnDetectedEnemy()
{
    Attack(SELF, OTHER);
    SetCallback(SELF, 3, BanditOnDetectedEnemyNothing);
    UniChatMessage(SELF, "난 강도다! 너의 돈과 명예를 내놔라!", 120);
}

void BanditEventStandby()
{
    int mon = CreateObjectAt("Swordsman", LocationX(192), LocationY(192));

    MonsterCommonProcess(mon);
    SetCallback(mon, 3, BanditOnDetectedEnemy);
    SetUnitMaxHealth(mon, 325);
    LookWithAngle(mon, 160);
    SetUnitScanRange(mon, 400.0);
}

void CreateGerm(float xpos, float ypos, int *pDest)
{
    string germs[] = {"Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Emerald", "Diamond"};
    int pic = CreateObjectAt(germs[Random(0, sizeof(germs) - 1)], xpos, ypos);

    if (pDest)
        pDest[0] = pic;
}

void SetFumbleDisabler(int weaponItem)
{
    int *ptr=UnitToPtr(weaponItem);

    ptr[178] = 0x408fdb;
}

void SetInvincibleAllItemNPCOnly(int owner)
{
    int inv = GetLastItem(owner);

    while (inv)
    {
        SetFumbleDisabler(inv);
        SetUnitEnchantCopy(inv, GetLShift(ENCHANT_INVULNERABLE));
        inv = GetPreviousItem(inv);
    }
}

#define THING_ID_NPC 1343

void RemoveEquipments(int unit)
{
	int inven = unit + 2;
	
	while (HasClass(inven, "WEAPON") || HasClass(inven, "ARMOR"))
    {
		Delete(inven);
		inven += 2;
	}
}

void MonsterOnDeath()
{
    int me = GetTrigger();

    CreateRandomItem(CreateObjectAt("AmbBeachBirds", GetObjectX(me), GetObjectY(me)));
    DeleteObjectTimer(me, 300);
}

void NPCOnDeath()
{
    int me = GetTrigger();

    RemoveEquipments(me);
    CreateRandomItem(CreateObjectAt("AmbBeachBirds", GetObjectX(me), GetObjectY(me)));
}

void MonsterCommonHitEvent()
{
    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 2, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void DetectedNPC(int npc, int *pCount)
{
    if (CurrentHealth(npc))
    {
        SetInvincibleAllItemNPCOnly(npc);
        SetCallback(npc, 5, NPCOnDeath);
        SetCallback(npc, 7, MonsterCommonHitEvent);
        ++pCount[0];
    }
}

#define THING_ID_REWARDMARKER 2672

void DetectedRewardMarker(int mark)
{
    int sub = CreateObjectAt("AmbBeachBirds", GetObjectX(mark), GetObjectY(mark));

    Delete(mark);
    CreateRandomItem(sub);
    Delete(sub);
}

void DetectCompleted(int count)
{
    FrameTimer(1, PlacingMapEnemies);
    UniPrintToAll("debug.c: end all npc : " + IntToString(count));
}

void StartNPCScanner(int cur)
{
    int rep=30, count;

    while (rep--)
    {
        cur+=2;
        if (cur < m_maplastUnit)
        {
            int thingId = GetUnitThingID(cur);

            if (thingId == THING_ID_NPC)
                DetectedNPC(cur, &count);
            else if (thingId == THING_ID_REWARDMARKER)
                DetectedRewardMarker(cur);
        }
        else
        {
            DetectCompleted(count);
            return;
        }
    }
    FrameTimerWithArg(1, cur, StartNPCScanner);
}

void InitItemNames()
{
    int weapons[]={"WarHammer", "GreatSword", "Sword", "Longsword", "StaffWooden", 
    "OgreAxe", "BattleAxe", "FanChakram", "RoundChakram",
            "OblivionHalberd", "OblivionHeart", "OblivionWierdling"};
    int armors[]={"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", 
    "PlateLeggings", "ChainCoif", "ChainLeggings", "ChainTunic",
            "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", 
            "LeatherHelm", "LeatherLeggings", "MedievalCloak",
            "MedievalPants", "MedievalShirt", "StreetShirt", "StreetSneakers", "StreetPants"};
    int pots[]={"RedPotion", "RedPotion2", "CurePoisonPotion", "VampirismPotion", 
    "ShieldPotion", "InvisibilityPotion", "InvulnerabilityPotion",
            "ShieldPotion", "HastePotion", "FireProtectPotion", "ShockProtectPotion", 
            "PoisonProtectPotion", "YellowPotion", "BlackPotion"};
    int wands[]={"DeathRayWand", "LesserFireballWand", "FireStormWand", "ForceWand", 
    "InfinitePainWand", "SulphorousFlareWand", "SulphorousShowerWand"};

    m_weaponNameTable=&weapons;
    m_armorNameTable=&armors;
    m_potionNameTable=&pots;
    m_staffNameTable=&wands;
    m_weaponNameTableLength=sizeof(weapons);
    m_armorNameTableLength=sizeof(armors);
    m_potionNameTableLength=sizeof(pots);
    m_staffNameTableLength=sizeof(wands);
}

void CreateRandomItemProto(int functionId, float xpos, float ypos, int *ptr)
{
    Bind(functionId, &functionId+4);
}

void CreateRandomItemCommon(int posUnit, int *itemTablePtr, int tableLength)
{
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    Delete(posUnit);

    if (!itemTablePtr || !tableLength)
        return;

    if (!m_itemFunctionTable)
        return;

    int getitem = 0;

    CreateRandomItemProto(itemTablePtr[Random(0, tableLength-1)], xpos, ypos, &getitem);

    if (getitem)
    {
        if (GetUnitFlags(getitem) & UNIT_FLAG_NO_COLLIDE)
            UnitNoCollide(getitem);
    }
}

void CreateRandomItem(int posUnit)
{
    CreateRandomItemCommon(posUnit, m_itemFunctionTable, m_itemFunctionTableLength);
}

void InitItemFunctionTable()
{
    int items[]={CreateCoin, CreateHotPotion, CreateGerm, CreateMagicalStaff, CreateRandomArmor, CreateRandomPotion, CreateRandomWeapon};

    m_itemFunctionTable=&items;
    m_itemFunctionTableLength=sizeof(items);
}

void InitDefaultItemName()
{
    string itemName[] = {"RedPotion", "CurePoisonPotion", "SteelHelm", "ChainLeggings", "ChainTunic", "PlateBoots", "GreatSword"};

    m_defaultItemNameTable = &itemName;
    m_defaultItemNameTableLength = sizeof(itemName);
}

void PlacingDefaultOneLine(int writer, string name, int count)
{
    while (count--)
    {
        CreateObjectAtEx(name, GetObjectX(writer), GetObjectY(writer), 0);
        MoveObjectVector(writer, 23.0, 23.0);
    }
    Delete(writer);
}

void PlacingDefaultItems(int count)
{
    int innerIndex;

    PlacingDefaultOneLine(CreateObjectAt("InvisibleLightBlueLow", LocationX(46), LocationY(46)), m_defaultItemNameTable[(innerIndex++) % m_defaultItemNameTableLength], 10);
    TeleportLocationVector(46, -23.0, 23.0);
    if (count)
        FrameTimerWithArg(1, count-1, PlacingDefaultItems);
}

void MonSwordsman(int locationId, int *pDest)
{
    int mon = CreateObjectAt("Swordsman", LocationX(locationId), LocationY(locationId));

    if (pDest)
        pDest[0] = mon;
    SwordsmanSubProcess(mon);
    MonsterCommonProcess(mon);
}

void EntranceEnemies()
{
    MonSwordsman(56, 0);
    MonSwordsman(57, 0);
    MonSwordsman(58, 0);
    MonSwordsman(59, 0);
    MonSwordsman(60, 0);
    MonSwordsman(61, 0);
    MonSwordsman(62, 0);
    MonSwordsman(63, 0);

    MonSwordsman(116, 0);
    MonSwordsman(117, 0);
    MonSwordsman(118, 0);
    MonSwordsman(119, 0);
    MonSwordsman(120, 0);
    MonSwordsman(121, 0);
}

void DrawImageAtEx(float x, float y, int thingId)
{
    int *ptr = UnitToPtr(CreateObjectAt("AirshipBasketShadow", x, y));

    ptr[1] = thingId;
}

void PlacingSomeDecorations()
{
    CreateObjectAtEx("RedApple", LocationX(39), LocationY(39), 0);
    CreateObjectAtEx("RedApple", LocationX(40), LocationY(40), 0);
    CreateObjectAtEx("RedApple", LocationX(41), LocationY(41), 0);
    CreateObjectAtEx("RedApple", LocationX(42), LocationY(42), 0);

    CreateObjectAtEx("Meat", LocationX(43), LocationY(43), 0);
    CreateObjectAtEx("RedApple", LocationX(44), LocationY(44), 0);
    CreateObjectAtEx("RedApple", LocationX(45), LocationY(45), 0);
    PlacingDefaultItems(m_defaultItemNameTableLength+1);

    int text1 = CreateObjectById(OBJ_CANDLE_1_UNLIT, LocationX(128), LocationY(128));
    UnitNoCollide(text1);
    int text2=CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT,LocationX(129), LocationY(129));
    UnitNoCollide(text2);
    int text3=CreateObjectById(OBJ_FOIL_CANDLE_UNLIT,LocationX(130), LocationY(130));
    UnitNoCollide(text2);
    WallGroupSetting(WallGroup("roachWalls"), WALL_GROUP_ACTION_OPEN);
}

void InitColorOrb()
{
    m_orbRed = CreateObject("RedOrb", 47);
    m_orbBlue = CreateObject("BlueOrb", 48);
    m_orbGreen = CreateObject("GreenOrb", 49);
}

void InitAssociatedShop2()
{
    int weaponShop = CreateObjectAt("ShopkeeperWizardRealm", LocationX(53), LocationY(53));

    ShopUtilAppendItemWithProperties(weaponShop, 1175, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_fire4, ITEM_PROPERTY_stun3);
    ShopUtilAppendItemWithProperties(weaponShop, 1175, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_vampirism3);
    ShopUtilAppendItemWithProperties(weaponShop, 1175, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_impact4, ITEM_PROPERTY_confuse3);
    ShopUtilAppendItemWithProperties(weaponShop, 1175, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_venom3);
    ShopUtilAppendItemWithProperties(weaponShop, 1176, 20, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_vampirism3);
    ShopUtilSetTradePrice(weaponShop, 2.3, 0.0);

    int armorShop = CreateObjectAt("ShopkeeperYellow", LocationX(54), LocationY(54));
    
    ShopUtilAppendItemWithProperties(armorShop, 1157, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1152, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1144, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1143, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1142, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1137, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1136, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1135, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect3);
    ShopUtilSetTradePrice(armorShop, 2.0, 0.0);

    int armorShop2 = CreateObjectAt("ShopkeeperYellow", LocationX(55), LocationY(55));
    
    ShopUtilAppendItemWithProperties(armorShop2, 1157, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilAppendItemWithProperties(armorShop2, 1152, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilAppendItemWithProperties(armorShop2, 1144, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilAppendItemWithProperties(armorShop2, 1143, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilAppendItemWithProperties(armorShop2, 1142, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilAppendItemWithProperties(armorShop2, 1137, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilAppendItemWithProperties(armorShop2, 1136, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilAppendItemWithProperties(armorShop2, 1135, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Regeneration4, ITEM_PROPERTY_Speed3);
    ShopUtilSetTradePrice(armorShop2, 2.0, 0.0);

    LookWithAngle(weaponShop, 32);
    LookWithAngle(armorShop2, 32);
    LookWithAngle(armorShop, 32);
    UnitNoCollide(weaponShop);
    UnitNoCollide(armorShop2);
    UnitNoCollide(armorShop);
}

void InitAssociatedShop()
{
    int weaponShop = CreateObjectAt("ShopkeeperWizardRealm", LocationX(50), LocationY(50));

    ShopUtilAppendItemWithProperties(weaponShop, 1170, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_fire4, ITEM_PROPERTY_stun3);
    ShopUtilAppendItemWithProperties(weaponShop, 1170, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_vampirism3);
    ShopUtilAppendItemWithProperties(weaponShop, 1170, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_impact4, ITEM_PROPERTY_confuse3);
    ShopUtilAppendItemWithProperties(weaponShop, 1170, 6, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_venom3);
    ShopUtilAppendItemWithProperties(weaponShop, 1176, 20, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_fire4, ITEM_PROPERTY_vampirism3);
    ShopUtilSetTradePrice(weaponShop, 2.3, 0.0);

    int armorShop = CreateObjectAt("ShopkeeperYellow", LocationX(51), LocationY(51));
    
    ShopUtilAppendItemWithProperties(armorShop, 1157, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1152, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1144, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1143, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1142, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1137, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1136, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilAppendItemWithProperties(armorShop, 1135, 6, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect3);
    ShopUtilSetTradePrice(armorShop, 2.0, 0.0);

    int potShop = CreateObjectAt("ShopkeeperMagicShop", LocationX(52), LocationY(52));

    ShopUtilAppendItem(potShop, 636, 32);
    ShopUtilAppendItem(potShop, 638, 32);
    ShopUtilAppendItem(potShop, 631, 32);

    int magicPotionId = 2679;

    while (magicPotionId <= 2687)
        ShopUtilAppendItem(potShop, magicPotionId++, 32);
    
    ShopUtilSetTradePrice(potShop, 3.0, 0.0);

    LookWithAngle(weaponShop, 32);
    LookWithAngle(armorShop, 32);
    LookWithAngle(potShop, 32);
    UnitNoCollide(weaponShop);
    UnitNoCollide(armorShop);
    UnitNoCollide(potShop);
}

void InitDungeonMobLocationTable()
{
    int dun1[] = {69, 70, 72, 73, 74,75,76,77,78,79,80,81,82,83,84,85,86,87,88, 98,99,100};

    m_dungeonLocations1=&dun1;
    m_dungeonLocationLength1=sizeof(dun1);

    int dunMonFunctions1[] = {SummonLv3Hecubah, SummonLv3DeadWalker, SummonLv1FireFairy, SummonLv1Maiden};
    m_dungeonMobFunctions1 = &dunMonFunctions1;
    m_dungeonMobFunctionLength1 = sizeof(dunMonFunctions1);
}

void InitUserDefinedPropertise()
{
    SpecialWeaponPropertyCreate(&m_weaponProperty1);
    SpecialWeaponPropertyExecuteCastSpellCodeGen("SPELL_FIREBALL", &m_weaponPropertyExec1);
    SpecialWeaponPropertySetExecuteCode(m_weaponProperty1, m_weaponPropertyExec1);
    SpecialWeaponPropertySetId(m_weaponProperty1, 'K');

    SpecialWeaponPropertyCreate(&m_weaponProperty2);
    SpecialWeaponPropertyExecuteScriptCodeGen(AutoTargetDeathray, &m_weaponPropertyExec2);
    SpecialWeaponPropertySetExecuteCode(m_weaponProperty2, m_weaponPropertyExec2);
    SpecialWeaponPropertySetId(m_weaponProperty2, 'Z');
}

void InitMapSign()
{
    RegistSignMessage(Object("msign1"), "삼색 오펜스- 오브 구슬을 가져와 여기에 꽂으세요");
    RegistSignMessage(Object("msign2"), "삼색 오펜스- 원작:Lore Copi. 수정:noxgameremaster");
    RegistSignMessage(Object("msign3"), "삼색 오펜스- 화염구슬의 요새. 화염기사의 요새라고도 함");
    RegistSignMessage(Object("msign4"), "파란색 구슬의 요새 입니다");
    RegistSignMessage(Object("msign5"), "초록색 구슬의 요새 입니다. 용기 있는 자만이 들어올 것");
}

void MapInitialize()
{
    MusicEvent();
    int hostp = GetHost();
    m_maplastUnit=CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(hostp), GetObjectY(hostp));

    DoDataExchange();
    FrameTimer(10, PlayerClassOnLoop);
    FrameTimerWithArg(10, Object("mapfirst"), StartNPCScanner);

    InitItemNames();
    InitItemFunctionTable();
    InitColorOrb();
    InitDefaultItemName();
    InitAssociatedShop();
    InitAssociatedShop2();
    InitDungeonMobLocationTable();
    FrameTimer(1, MakeCoopTeam);
    InitUserDefinedPropertise();
    InitMapSign();
    SetGameSettingForceRespawn(TRUE);
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);

    FrameTimer(30, PlacingSomeDecorations);
    blockObserverMode();
}

int CheckPlayer()
{
    int u;

    for (u = 0 ; u < sizeof(m_player) ; Nop(u++))
    {
        if (IsCaller(m_player[u]))
            return u;
    }
    return -1;
}

int CheckPlayerWithId(int pUnit)
{
    int rep=-1;

    while ((++rep)<sizeof(m_player))
    {
        if (m_player[rep]^pUnit)
            continue;
        
        return rep;
    }
    return -1;
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_playerFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_playerFlag[plr] ^= flags;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    m_player[plr]=pUnit;
    m_playerFlag[plr]=1;
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    while (GetLastItem(pUnit))
        Delete(GetLastItem(pUnit));
    ChangeGold(pUnit, -GetGold(pUnit));
    return plr;
}

void PlayerClassOnEntry(int pUnit)
{
    while (TRUE)
    {
        if (!CurrentHealth(pUnit))
            break;
        int plr = CheckPlayerWithId(pUnit), u;

        for (u = sizeof(m_player)-1; u >= 0 && plr < 0 ; Nop(--u))
        {
            if (!MaxHealth(m_player[u]))
            {
                plr = PlayerClassOnInit(u, pUnit);
                break;
            }
        }
        if (plr >= 0)
        {
            PlayerClassOnJoin(plr);
            break;
        }
        PlayerClassOnEntryFailure(pUnit);
        break;
    }
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void PlayerSetAllBuff(int pUnit)
{
    Enchant(pUnit, EnchantList(ENCHANT_VAMPIRISM), 0.0);
    SetUnitEnchantCopy(pUnit, GetLShift(ENCHANT_PROTECT_FROM_FIRE) | GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY) | GetLShift(ENCHANT_PROTECT_FROM_POISON)
        | GetLShift(ENCHANT_INFRAVISION) | GetLShift(ENCHANT_REFLECTIVE_SHIELD));
}

void PlayerClassOnJoin(int plr)
{
    int pUnit = m_player[plr], destLocation = 23;
    
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
        PlayerSetAllBuff(pUnit);
    MoveObject(pUnit, LocationX(destLocation), LocationY(destLocation));
    Effect("TELEPORT", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    PlaySoundAround(pUnit, SOUND_BlindOff);
}

void PlayerClassOnDeath(int plr)
{
    int pUnit = m_player[plr];

    UniPrintToAll(PlayerIngameNick(pUnit) + "님께서 적에게 격추되었습니다");
    PlaySoundAround(pUnit, SOUND_HecubahDieFrame98);
    Effect("EXPLOSION", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void PlayerClassOnShutdown(int plr)
{
    m_player[plr] = 0;
    m_playerFlag[plr] = 0;
}

void PlayerPoisonHandler(int plr, int pUnit)
{
    int poisonBuffer[32];

    if (IsPoisonedUnit(pUnit))
    {
        if (poisonBuffer[plr] < 30)
            poisonBuffer[plr] += IsPoisonedUnit(pUnit);
        else
        {
            Damage(pUnit, 0, IsPoisonedUnit(pUnit), DAMAGE_TYPE_POISON);
            poisonBuffer[plr] = 0;
        }
    }
    else if (poisonBuffer[plr])
        poisonBuffer[plr] = 0;
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_SNEAK)))
    {
        if (PlayerClassCheckFlag(plr, PLAYER_EXCHANGE_ABILITY))
            WindCharging(pUnit);
        RemoveTreadLightly(pUnit);
        EnchantOff(pUnit, EnchantList(ENCHANT_SNEAK));
        EnchantOff(pUnit, EnchantList(ENCHANT_INVULNERABLE));
    }
    PlayerPoisonHandler(plr, pUnit);
}

void PlayerClassOnLoop()
{
    int u;

    for (u = sizeof(m_player)-1 ; u >= 0 ; Nop(--u))
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[u]))
            {
                if (GetUnitFlags(m_player[u]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[u]))
                {
                    PlayerClassOnAlive(u, m_player[u]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(u, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassOnDeath(u);
                        PlayerClassSetFlag(u, PLAYER_DEATH_FLAG);
                    }
                    break;
                }
            }
            if (m_playerFlag[u])
                PlayerClassOnShutdown(u);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        Enchant(OTHER, EnchantList(ENCHANT_ANCHORED), 0.0);
        if (CheckPlayer() >= 0)
        {
            // MoveObject(OTHER, LocationX(17), LocationY(17));
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
        {
            MoveObject(OTHER, LocationX(21), LocationY(21));
        }
    }
}

void RegistPlayer()
{
    PlayerClassOnEntry(GetCaller());
}

void TeleportAllPlayer(int locationId)
{
    int rep=-1;

    while (++ rep<sizeof(m_player))
    {
        if (CurrentHealth(m_player[rep]))
            MoveObject(m_player[rep], LocationX(locationId), LocationY(locationId));
    }
}

void RemoveWizardWalls()
{
    ObjectOff(SELF);
    WallUtilOpenWallAtObjectPosition(37);
    WallUtilOpenWallAtObjectPosition(38);
    UniPrint(OTHER, "앞을 막고있던 비밀벽이 사라집니다");
}

void DrawVictoryMent()
{
    int rep=-1;

    while (++rep<10)
    {
        CreateObjectById(OBJ_OGRE_STOOL_1, LocationX(131), LocationY(131));
        TeleportLocationVector(131, 2.0, 23.0);
    }
}

void VictoryEvent()
{
    int once;

    if (once)
        return;

    once=TRUE;

    DrawVictoryMent();
    FrameTimerWithArg(3, 127, TeleportAllPlayer);
    UniPrintToAll("승리했어요! 모든 자이언트를 파괴했기 때문이죠!");
}

void StoneGiantOnDeath()
{
    UniChatMessage(SELF, "분하다~!", 90);
    MonsterOnDeath();

    UniPrintToAll("남은 자이언트 수: " + IntToString(m_stoneGiantKills--));

    if (!m_stoneGiantKills)
        VictoryEvent();
}

void SummonGiants(int gaga)
{
    while (IsObjectOn(gaga))
    {
        if (GetDirection(gaga))
        {
            int golem;
            SummonStoneGiant2(GetObjectX(gaga), GetObjectY(gaga), &golem);
            MonsterCommonProcess(golem);
            SetUnitScanRange(golem, 600.0);
            SetCallback(golem, 5, StoneGiantOnDeath);
            FrameTimerWithArg(3, gaga, SummonGiants);
            LookWithAngle(gaga, GetDirection(gaga)-1);
            Effect("SMOKE_BLAST", GetObjectX(gaga), GetObjectY(gaga), 0.0, 0.0);
            PlaySoundAround(gaga, SOUND_BlindOff);
            break;
        }
        Delete(gaga);
        break;
    }
}

void RealFinalTask(int gaga)
{
    int kills = 32;

    LookWithAngle(gaga, kills);
    m_stoneGiantKills=kills;
    UniChatMessage(gaga, "끝나긴 뭘 끝나?? 아직 안 끝났어!!", 150);
    UniPrintToAll("아직 게임이 끝나지 않았군요... 이제부터 시작인 걸 까요??");
    Effect("SMOKE_BLAST", GetObjectX(gaga), GetObjectY(gaga), 0.0, 0.0);
    FrameTimerWithArg(180, gaga, SummonGiants);
}

void CheckEndGame()
{
    if (IsObjectOn(m_keyBase[0]) && IsObjectOn(m_keyBase[1]) && IsObjectOn(m_keyBase[2]))
    {
        ObjectOff(SELF);
        CreateObjectAtEx("LevelUp", GetObjectX(OTHER), GetObjectY(OTHER), 0);
        UniPrintToAll("3개 구슬을 다 모았어요! 수고하셨어요~ 플레이 해주셔서 감사합니다");
        Effect("WHITE_FLASH", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        PlaySoundAround(OTHER, SOUND_LevelUp);

        int gaga = CreateObjectAt("RewardMarkerPlus", GetObjectX(OTHER), GetObjectY(OTHER));

        FrameTimerWithArg(60, gaga, RealFinalTask);
    }
}

void PlacingLichs(int enemy, int locationId)
{
    int lich;

    SummonLv3DeadWalker(locationId, &lich);
    LookAtObject(lich, enemy);
    PlaySoundAround(lich, SOUND_BlindOff);
    Effect("SMOKE_BLAST", LocationX(locationId), LocationY(locationId), 0.0, 0.0);
}

void SurpriseE1()
{
    ObjectOff(SELF);

    int once;

    if (once)
        return;
    once = TRUE;

    PlacingLichs(OTHER, 64);
    PlacingLichs(OTHER, 65);
    PlacingLichs(OTHER, 66);
    PlacingLichs(OTHER, 67);
    PlacingLichs(OTHER, 68);
    PlacingLichs(OTHER, 71);
}

// void PlacingPlant(int enemy, int locationId)
// {
//     int plant;

//     SummonLv3Plant(locationId, &plant);
//     LookAtObject(plant, enemy);
//     PlaySoundAround(plant, SOUND_InvisibilityOn);
//     Effect("SMOKE_BLAST", LocationX(locationId), LocationY(locationId), 0.0, 0.0);
// }

void onDvaDie()
{
    float x=GetObjectX(SELF),y=GetObjectY(SELF);

    Delete(SELF);
    PushTimerQueue(9, CreateObjectById(OBJ_RELEASED_SOUL,x,y), CreateRandomItem);
    Effect("COUNTERSPELL_EXPLOSION",x,y,0.0,0.0);
}

void placeDva(int enemy, int locationId)
{
    int dva=CreateObjectById(OBJ_WIZARD_GREEN,LocationX(locationId),LocationY(locationId));

    SetUnitMaxHealth(dva,275);
    SpellUtilSetUnitSpell(dva,"SPELL_BLINK",0);
    SpellUtilSetUnitSpell(dva,"SPELL_INVISIBILITY",SPELLFLAG_ON_ESCAPE);
    LookAtObject(dva, enemy);
    PlaySoundAround(dva, SOUND_InvisibilityOn);
    Effect("SMOKE_BLAST", LocationX(locationId), LocationY(locationId), 0.0, 0.0);
    SetCallback(dva,5,onDvaDie);
}

void SurpriseE11()
{
    ObjectOff(SELF);

    int once;

    if (once)
        return;

    once= TRUE;
    int rep=-1;
    while (++rep<9)
        placeDva(OTHER, 89+rep);
}

static void NetworkUtilClientMain()
{
    InitializeMapResources();
}

void InvincibleItemDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:InvulnerabilityPotionDescription", "아이템 무적화");
    UniPrint(OTHER, "아이템을 무적으로 만들어드릴 까요? 금액은 1회 시행 시 7,000 골드 입니다");
}

void InvincibleItemTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) < 7000)
    {
        UniPrint(OTHER, "잔액 부족입니다 손님");
        return;
    }
    int count = 0;

    AllItemSetInvincible(OTHER, &count);
    if (count)
    {
        ChangeGold(OTHER, -7000);
        UniPrint(OTHER, "처리되었습니다~");
    }
    else
    {
        UniPrint(OTHER, "처리할 것이 없네요. 처리되지 않았어요");
    }    
}

void PlacingInvincibleItemShop(int locationId)
{
    int special = DummyUnitCreateAt("UrchinShaman", LocationX(locationId), LocationY(locationId));

    StoryPic(special, "DrunkPic");
    SetDialog(special, "YESNO", InvincibleItemDesc, InvincibleItemTrade);
}

void PlacingFireSword(float xpos, float ypos, int *pDest)
{
    int sd = CreateObjectAt("GreatSword", xpos, ypos);

    if (pDest)
        pDest[0] = sd;

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_fire4, ITEM_PROPERTY_Regeneration3);
    SpecialWeaponPropertySetWeapon(sd, 3, m_weaponProperty1);
}

void PlacingDeathraySword(float xpos, float ypos, int *pDest)
{
    int sd = CreateObjectAt("GreatSword", xpos, ypos);

    if (pDest)
        pDest[0] = sd;

    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_lightning4, ITEM_PROPERTY_Regeneration3);
    SpecialWeaponPropertySetWeapon(sd, 3, m_weaponProperty2);
}

void FireSwordDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:InvulnerabilityPotionDescription", "화이어 서드");
    UniPrint(OTHER, "화이어 서드를 구입할래요? 3만 골드입니다");
}

void DeathraySwordTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) < 50000)
    {
        UniPrint(OTHER, "잔액 부족입니다 손님");
        return;
    }

    PlacingDeathraySword(GetObjectX(OTHER), GetObjectY(OTHER), NULLPTR);
    ChangeGold(OTHER, -50000);
    UniPrint(OTHER, "처리되었습니다~ 데스레이 서드는 당신 아래에 놓여져 있어요");
}

void DeathraySwordDesc()
{
    TellStoryUnitName("AmuletDrop", "thing.db:InvulnerabilityPotionDescription", "데스레이 서드");
    UniPrint(OTHER, "데스레이 서드를 구입할래요? 5만 골드입니다");
}

void FireSwordTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) < 30000)
    {
        UniPrint(OTHER, "잔액 부족입니다 손님");
        return;
    }

    PlacingFireSword(GetObjectX(OTHER), GetObjectY(OTHER), NULLPTR);
    ChangeGold(OTHER, -30000);
    UniPrint(OTHER, "처리되었습니다~ 화이어 서드는 당신 아래에 놓여져 있어요");
}

void PlacingFireswordShop(int locationId)
{
    int special = DummyUnitCreateAt("Demon", LocationX(locationId), LocationY(locationId));

    StoryPic(special, "DrunkPic");
    SetDialog(special, "YESNO", FireSwordDesc, FireSwordTrade);
}

void PlacingDeathraySwordShop(int locationId)
{
    int special = DummyUnitCreateAt("Hecubah", LocationX(locationId), LocationY(locationId));

    StoryPic(special, "DrunkPic");
    SetDialog(special, "YESNO", DeathraySwordDesc, DeathraySwordTrade);
}

void WindChargingCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, GetOwner(SELF)))
        {
            Damage(OTHER, GetOwner(SELF), 65, DAMAGE_TYPE_ZAP_RAY);
            Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
        }
    }
}

void WindChargingLoop(int sub)
{
    while (IsObjectOn(sub))
    {
        int count = GetDirection(sub), owner = GetOwner(sub);

        if (CurrentHealth(owner) && count)
        {
            LookWithAngle(sub, --count);
            FrameTimerWithArg(1, sub, WindChargingLoop);

            int pusher = DummyUnitCreateAt("CarnivorousPlant", GetObjectX(owner) - GetObjectZ(sub), GetObjectY(owner) - GetObjectZ(sub+1));

            DeleteObjectTimer(pusher, 1);
            SetOwner(owner, pusher);
            SetCallback(pusher, 9, WindChargingCollide);
            Effect("YELLOW_SPARKS", GetObjectX(pusher), GetObjectY(pusher), 0.0, 0.0);
            PlaySoundAround(pusher, SOUND_PixieHit);            
            break;
        }
        Delete(sub++);
        Delete(sub++);
        break;
    }
}

void WindCharging(int pUnit)
{
    float xvect = UnitAngleCos(pUnit, 19.0), yvect = UnitAngleSin(pUnit, 19.0);
    int sub = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(pUnit) + xvect, GetObjectY(pUnit) + yvect);

    Raise(sub, xvect);
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sub), GetObjectY(sub)), yvect);
    LookWithAngle(sub, 16);
    SetOwner(pUnit, sub);
    PlaySoundAround(sub, SOUND_SummonAbort);
    FrameTimerWithArg(1, sub, WindChargingLoop);
}

void ExpensionAbilityDesc()
{
    TellStoryUnitName("AmuletDrop", "GeneralPrint:InformExpansion", "조심스럽게 걷기\n업그레이드\n2만");
    UniPrint(OTHER, "조심스럽게 걷기 능력을 강화하시겠어요? 금액은 2만 골드 입니다");
}

void ExpensionAbilityTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    int plr = CheckPlayer();

    if (plr < 0)
        return;

    if (GetGold(OTHER) >= 20000)
    {
        if (PlayerClassCheckFlag(plr, PLAYER_EXCHANGE_ABILITY))
        {
            UniPrint(OTHER, "당신은 이미 그 능력을 가졌습니다");
            return;
        }
        PlayerClassSetFlag(plr, PLAYER_EXCHANGE_ABILITY);
        ChangeGold(OTHER, -20000);
        UniPrint(OTHER, "이 능력은 이제 당신의 것 입니다");
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        PlaySoundAround(OTHER, SOUND_AwardSpell);
    }
    else
        UniPrint(OTHER, "이런!, 금액이 부족하군요...");
}

void PlacingWindboostShop(int locationId)
{
    int special = DummyUnitCreateAt("Horrendous", LocationX(locationId), LocationY(locationId));

    StoryPic(special, "DrunkPic");
    SetDialog(special, "YESNO", ExpensionAbilityDesc, ExpensionAbilityTrade);
}

void AllEnchantMarketDesc()
{
    TellStoryUnitName("AmuletDrop", "armrlook.c:EnchantedChainmailName", "올 엔첸 3만");
    UniPrint(OTHER, "올 엔첸트를 구입하시겠어요? 3만 골드입니다");
}

void AllEnchantMarketTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    int plr = CheckPlayer();

    if (plr < 0)
        return;

    if (GetGold(OTHER) >= 30000)
    {
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
        {
            UniPrint(OTHER, "당신은 이미 그 능력을 가졌습니다");
            return;
        }
        PlayerClassSetFlag(plr, PLAYER_FLAG_ALL_BUFF);
        PlayerSetAllBuff(OTHER);
        ChangeGold(OTHER, -30000);
        UniPrint(OTHER, "이 능력은 이제 당신의 것 입니다");
        Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        PlaySoundAround(OTHER, SOUND_AwardSpell);
    }
    else
    {
        UniPrint(OTHER, "금화가 부족합니다");
    }
}

void PlacingAllEnchantShop(int locationId)
{
    int special = DummyUnitCreateAt("WizardGreen", LocationX(locationId), LocationY(locationId));

    StoryPic(special, "DrunkPic");
    SetDialog(special, "YESNO", AllEnchantMarketDesc, AllEnchantMarketTrade);
}

void AutoTargetOnVisible()
{
    Damage(OTHER, SELF, 100, DAMAGE_TYPE_ZAP_RAY);
    PlaySoundAround(OTHER, SOUND_DeathRayKill);
    Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
}

void AutoTargetDeathray()
{
    int sub = CreateObjectAt("WeirdlingBeast", GetObjectX(OTHER) + UnitAngleCos(OTHER, 6.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 6.0));

    SetOwner(OTHER, sub);
    SetCallback(sub, 3, AutoTargetOnVisible);
    LookWithAngle(sub, GetDirection(OTHER));
    SetUnitScanRange(sub, 300.0);
    DeleteObjectTimer(sub, 1);
    UnitNoCollide(sub);
    PlaySoundAround(OTHER, SOUND_FirewalkOn);
    Effect("VIOLET_SPARKS", GetObjectX(sub), GetObjectY(sub), 0.0, 0.0);
}

void onGiantDeath()
{
    float x=GetObjectX(SELF),y=GetObjectY(SELF);

    Delete(SELF);
    CreateRandomItem(CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y));
    int fx=CreateObjectById(OBJ_BIG_SMOKE, x,y);
    DeleteObjectTimer( fx, 12);
    PlaySoundAround(fx,SOUND_Maiden2Die);
}

void summonGiantWoman(int wall)
{
    float xy[2];
    WallCoorToUnitCoor(wall, xy);
    WallOpen(wall);
    int mob=CreateObjectById(OBJ_HECUBAH_WITH_ORB, xy[0],xy[1]);

    HecubahWithOrbSubProcess(mob);
    UniChatMessage(mob, "내가 큰거냐 아니면 네가 꼬맹이 인거냐?", 150);
    SetCallback(mob,5,onGiantDeath);
}

void openGiantRoom()
{
    ObjectOff(SELF);
    WallGroupSetting(WallGroup("war2fWalls"), WALL_GROUP_ACTION_OPEN);
    RegistWallGroupCallback(summonGiantWoman);
    WallGroupSetting(WallGroup("giantWalls"),WALL_GROUP_ACTION_CALLBACK);
}

void openLichKeyroom()
{
    ObjectOff(SELF);
    WallGroupSetting(WallGroup("keyWalls"), WALL_GROUP_ACTION_OPEN);
}

void spawnGiantRoom2()
{
    ObjectOff(SELF);
    RegistWallGroupCallback(summonGiantWoman);
    WallGroupSetting(WallGroup("giantWalls2"), WALL_GROUP_ACTION_CALLBACK);
}

void onRoachDie()
{
    float x=GetObjectX(SELF), y=GetObjectY(SELF);

    CreateObjectById(OBJ_BREAKING_SOUP, x,y);
    PushTimerQueue(6, CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y), CreateRandomItem);
}

void spawnRoachSingle(int wall)
{
    float xy[2];
    WallCoorToUnitCoor(wall, xy);
    WallOpen(wall);
    int mob=CreateObjectById(OBJ_WEIRDLING_BEAST, xy[0],xy[1]);

    WeirdlingBeastSubProcess(mob);
    SetCallback(mob, 5,onRoachDie);
}

void spawnRoaches()
{
    ObjectOff(SELF);
    RegistWallGroupCallback(spawnRoachSingle);
    WallGroupSetting(WallGroup("roachWalls"), WALL_GROUP_ACTION_CALLBACK);
}

static void OnPlayerEntryMap(int pInfo){
    int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
    {
        pUnit = GetMemory(ptr+0x2c);
        int pIndex = GetMemory(pInfo+0x810);
        PlayerClassOnInit   (pIndex, pUnit);
        ShowQuestIntroOne(pUnit, 237, "CopyrightScreen", "Wolchat.c:PleaseWait");
    }
    
    if (pInfo==0x653a7c)
    {
        InitializeMapResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
