
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/buff.h"
#include "libs/networkEx.h"

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

void AllItemSetInvincible(int holder, int *pCount)
{
    int cur = GetLastItem(holder), count = 0;

    while (cur)
    {
        if (!UnitCheckEnchant(cur, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(cur, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            Nop(++count);
        }
        cur = GetPreviousItem(cur);
    }
    if (pCount)
        pCount[0] = count;
}


void WallCoorToUnitCoor(int wall, float *destXy)
{
    int x=wall>>0x10, y=wall&0xff;

    destXy[0]=IntToFloat((x*23) + 11);
    destXy[1]=IntToFloat((y*23)+11);
}


int CreateSingleColorMaidenAt(int red, int grn, int blue, float xpos, float ypos)
{
    int unit = CreateObjectAt("Bear2", xpos, ypos);
    int *ptr = UnitToPtr(unit);

    if (ptr == NULLPTR)
        return NULLPTR;
    ptr[1] = 1385;

    int u = 0;
    while (u < 32)
        ptr[140 + (u++)] = 0x400;

    int *ecptr = ptr[187];

    ecptr[94] = 0xa0;
    ecptr[122] = VoiceList(7);

    int *colrarr = ecptr + 0x81c;

    colrarr[0] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[1] = grn | (blue << 8) | (red << 16) | (grn << 24);
    colrarr[2] = blue | (red << 8) | (grn << 16) | (blue << 24);
    colrarr[3] = red | (grn << 8) | (blue << 16) | (red << 24);
    colrarr[4] = grn | (blue << 8);

    return unit;
}

int HecubahWithOrbBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 600; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; arr[40] = 1852140903; 
		arr[41] = 116; arr[53] = 1128792064; arr[55] = 16; arr[56] = 26; arr[60] = 1384; 
		arr[61] = 46914560; 
	pArr = arr;
	return pArr;
}

void HecubahWithOrbSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 600;	hpTable[1] = 600;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = HecubahWithOrbBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 225; 
		arr[19] = 55; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1071225242; arr[26] = 4; 
		arr[28] = 1104150528; arr[29] = 18; arr[31] = 3; arr[32] = 6; arr[33] = 15; 
		arr[59] = 5542784; arr[60] = 1388; arr[61] = 46915072; 
	pArr = arr;
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1070805811;		ptr[137] = 1070805811;
	int *hpTable = ptr[139];
	hpTable[0] = 225;	hpTable[1] = 225;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}
