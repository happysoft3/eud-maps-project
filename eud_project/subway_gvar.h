
#include "libs/define.h"
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"

#define MAX_PLAYER_COUNT 32

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_HARPOON 32
#define PLAYER_FLAG_THREADLIGHTLY 64
#define PLAYER_FLAG_ALL_BUFF 128

#define ABILITY_ID_BERSERKER_CHARGE 	1
#define ABILITY_ID_WARCRY 			2
#define ABILITY_ID_HARPOON 			3
#define ABILITY_ID_TREAD_LIGHTLY		4
#define ABILITY_ID_EYE_OF_WOLF		5

int GetMasterUnit(){
    int master;

    if (!master){
        master=CreateObjectById(OBJ_HECUBAH,100.0,100.0);
        Frozen(master,TRUE);
    }
    return master;
}

#define PLAYER_START_LOCATION_AT 19

void QueryLUnit(int *get, int set, int n){}


#define DEMON_MAX_COUNT 20
void QueryDLord(int *get,int set, int n){}
 void QueryAGate(int *get,int set,int n){}

void CheckDemonDeathCount(int count){}


#define GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM 1
#define GUI_DIALOG_MESSAGE_AWARD_FAST_MOVE 2
#define GUI_DIALOG_MESSAGE_AWARD_BERSERKER 3
#define GUI_DIALOG_MESSAGE_WOLF_RUN_SWORD 4
#define GUI_DIALOG_MESSAGE_MONEY_EXCHANGER 5
#define GUI_DIALOG_MESSAGE_THUNDER_SWORD 6
#define GUI_DIALOG_MESSAGE_BACKSTEP_HAMMER 7
#define GUI_DIALOG_MESSAGE_ALL_ENCHANTMENT 8
#define GUI_DIALOG_MESSAGE_FLY_SWATTER 9
#define GUI_DIALOG_MESSAGE_BEAR_GRYLLS 10
#define GUI_DIALOG_MESSAGE_PESTICIDE 11
#define GUI_DIALOG_MESSAGE_BLUE_GIANT 12
#define GUI_DIALOG_MESSAGE_STELS_SHIP 13
#define GUI_DIALOG_MESSAGE_ICE_CRYSTAL_AXE 14
#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10
void GetCreature1DataPtr(int pIndex,int**pget){}//virtual
