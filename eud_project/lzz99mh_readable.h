
#include "libs/define.h"
#include "libs/printutil.h"

void InitializeReadables()
{
    RegistSignMessage(Object("readable1"), "이 구간은 그냥 만들었습니다");
    RegistSignMessage(Object("readable2"), "이 구간은 그냥 만들었습니다. 마찬가지");
    RegistSignMessage(Object("readable3"), "멋지다 코니! 의 퀘스트 던전! 여러분 멋지다 코니, 많은 시청바랍니다  -제작. 237");
    RegistSignMessage(Object("readable4"), "멋지다 코니! 의 퀘스트 던전! 모든 구간을 클리어 하셨습니다!  -제작. 237");
    RegistSignMessage(Object("readable5"), "멋지다 코니! 의 퀘스트 던전! 여기 구간이 마지막 구간입니다. 조금만 더 힘을 내세요!  -제작. 237");
    RegistSignMessage(Object("readable6"), "멋지다 코니! 의 퀘스트 던전! 이 리프트는 3개 층을 운행합니다. 마침내 이 지도에서 상용화~  -제작. 237");
    RegistSignMessage(Object("readable7"), "멋지다 코니! 의 퀘스트 던전! 이것은 영혼의 문입니다. 만지면 위치가 저장됩니다 -제작. 237");
    RegistSignMessage(Object("readable8"), "멋지다 코니! 의 퀘스트 던전! 이것은 마을로 보내주는 NPC 입니다.  -제작. 237");
}

