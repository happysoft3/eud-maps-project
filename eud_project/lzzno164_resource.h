
#include "lzz99no164_utils.h"
#include "lzz99no164_gvar.h"
#include "libs/animFrame.h"
#include "libs/grplib.h"

void GRPDump_BlueAooniOutput(){}

#define BLUE_ONI_BASE_IMAGE_ID 133943
void initializeImageFrameBlueOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BlueAooniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BLUE_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BLUE_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BLUE_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_RogueMercenaryOutput(){}

#define ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID 118946
void initializeImageFrameRogueMercenary(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_ARCHER;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_RogueMercenaryOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,10,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,10,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(25,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(30,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(35,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(40,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameAssign8Directions(45,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_WALK, 8);
    AnimFrameAssign8Directions(50,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_WALK, 9);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_RUN, 8);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_RUN, 9);
    AnimFrameAssign8Directions(55,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+88, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
    AnimFrameAssign8Directions(60,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+96, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 1);
    AnimFrameAssign8Directions(65,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+104, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 2);
    AnimFrameAssign8Directions(70,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+112, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 3);
    AnimFrameAssign8Directions(75,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+120, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 4);
    AnimFrameAssign8Directions(80,  ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+128, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 5);
    AnimFrameCopy8Directions(ROGUE_MERCENARY_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_SLAIN,0);
}

void GRPDump_GobdungMediumOutput(){}

#define MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT 120459

void initializeImageFrameMediumGopdung(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SPIDER;
    int xyInc[]={0,-2};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GobdungMediumOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 114160
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_MECHANICAL_GOLEM;
    int xyInc[]={0,-30};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount-4);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}

void GRPDump_WomanOutput(){}
#define GIANT_WOMAN_IMAGE_START_AT 128403
void initializeImageFrameWoman1(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH_WITH_ORB;
    int frameCount=UnpackAllFromGrp(GetScrCodeField(GRPDump_WomanOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);

    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8);

    AnimFrameAssign8Directions(0,GIANT_WOMAN_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,GIANT_WOMAN_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameAssign8Directions(10,GIANT_WOMAN_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameAssign8Directions(15,GIANT_WOMAN_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameAssign8Directions(20,GIANT_WOMAN_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameAssign8Directions(25,GIANT_WOMAN_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
    AnimFrameAssign8Directions(30,GIANT_WOMAN_IMAGE_START_AT+48,IMAGE_SPRITE_MON_ACTION_WALK,5);
    AnimFrameAssign8Directions(35,GIANT_WOMAN_IMAGE_START_AT+56,IMAGE_SPRITE_MON_ACTION_WALK,6);
    AnimFrameAssign8Directions(40,GIANT_WOMAN_IMAGE_START_AT+64,IMAGE_SPRITE_MON_ACTION_WALK,7);
}

void GRPDump_GobdungBigOutput(){}

#define LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT 123388

void initializeImageFrameBigGopdung(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BLACK_BEAR;
    // int xyInc[]={0,-2};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GobdungBigOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDump_SlimeDumpOutput(){}

#define SLIME_DUMP_OUTPUT_IMAGE_START_AT 121568

void initializeImageSlimeOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GIANT_LEECH;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SlimeDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,5,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign4Directions(6,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign4Directions(9,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign4Directions(12,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);

    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDumpSantaClothOutput(){}
#define SANTA_CLOTH_IMAGE_START_AT 129717
void initializeImageSantaClothOutput(int imgVector){
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDumpSantaClothOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,9,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,9,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  SANTA_CLOTH_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy4Directions( SANTA_CLOTH_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign4Directions(3,  SANTA_CLOTH_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  SANTA_CLOTH_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign4Directions(9,  SANTA_CLOTH_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign4Directions(12,  SANTA_CLOTH_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign4Directions(15,  SANTA_CLOTH_IMAGE_START_AT+20,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign4Directions(18,  SANTA_CLOTH_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign4Directions(21,  SANTA_CLOTH_IMAGE_START_AT+28,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameAssign4Directions(24,  SANTA_CLOTH_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_WALK, 8);
    AnimFrameCopy4Directions( SANTA_CLOTH_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions( SANTA_CLOTH_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions( SANTA_CLOTH_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions( SANTA_CLOTH_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(  SANTA_CLOTH_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy4Directions(  SANTA_CLOTH_IMAGE_START_AT+20,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy4Directions(  SANTA_CLOTH_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy4Directions(  SANTA_CLOTH_IMAGE_START_AT+28,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy4Directions(  SANTA_CLOTH_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_RUN, 8);
    AnimFrameCopy4Directions(  SANTA_CLOTH_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
    AnimFrameCopy4Directions(  SANTA_CLOTH_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDumpYoureWinner(){}

void initializeImageFrameYoureWinner(int imgvec)
{
    int *frames, *sizes, thingId=OBJ_CANDLE_1;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDumpYoureWinner)+4, thingId, xyInc, &frames,&sizes);

    AppendImageFrame(imgvec,frames[0],112984);
    AppendImageFrame(imgvec,frames[1],112985);
    AppendImageFrame(imgvec,frames[2],112986);
}

void GRPDumpChrismasTree(){}
void initializeImageFrameXMas(int imgvec){
    int *frames, *sizes, thingId=OBJ_SPIKE_BLOCK;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDumpChrismasTree)+4, thingId, xyInc, &frames,&sizes);

    AppendImageFrame(imgvec,frames[0],16211);
}

void onUpdateTileChanged()
{
    EnableTileTexture(FALSE);
    // SolidTile(628, 6,0x07FA);
    EnableTileTexture(TRUE);
}

void GRPDumpTileSet(){}

void initializeCustomTileset(int imgVector){
    int *pack=GetScrCodeField(GRPDumpTileSet)+4;
    int *off=&pack[1];
    int frames[]={
        pack+off[0],
        pack+off[1], 
        pack+off[2],
        pack+off[3],
        pack+off[4],
        pack+off[5],
        pack+off[6],
        pack+off[7],
        pack+off[8],
        };

    AppendImageFrame(imgVector, frames[0], 135820);
    AppendImageFrame(imgVector, frames[1], 135821);
    AppendImageFrame(imgVector, frames[2], 135822);
    AppendImageFrame(imgVector, frames[3], 135823);
    AppendImageFrame(imgVector, frames[4], 135824);
    AppendImageFrame(imgVector, frames[5], 135825);
    AppendImageFrame(imgVector, frames[6], 135826);
    AppendImageFrame(imgVector, frames[7], 135827);
    AppendImageFrame(imgVector, frames[8], 135828);
    onUpdateTileChanged();
}

void InitializeResources(){
    int imgVector=CreateImageVector(2048);

    initializeImageFrameBlueOni(imgVector);
    initializeImageFrameRogueMercenary(imgVector);
    initializeImageFrameSoldier(imgVector);
    initializeImageFrameMediumGopdung(imgVector);
    initializeImageFrameBillyOni(imgVector);
    initializeImageFrameWoman1(imgVector);
    initializeImageFrameBigGopdung(imgVector);
    initializeImageSlimeOutput(imgVector);
    initializeImageFrameYoureWinner(imgVector);
    initializeImageSantaClothOutput(imgVector);
    initializeImageFrameXMas(imgVector);
    initializeCustomTileset(imgVector);
    DoImageDataExchange(imgVector);
}
