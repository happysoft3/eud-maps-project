
#include "lsdcomp_gvar.h"
#include "libs/define.h"
#include "libs/spriteDb.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"

#define IMAGE_VECTOR_SIZE 256

void ImageResourceIcon1()
{ }
void ImageResourceIcon2()
{ }
void ImageResourceIcon3()
{ }
void ImageResourceDlink()
{ }

void ImageResourceDlink2()
{ }

void ImageResourceVictory()
{ }
void ImageResourceBucher()
{ }

void ImageResourceNuclearExplosion1()
{ }
void ImageResourceNuclearExplosion2()
{ }
void ImageResourceNuclearExplosion3()
{ }
void ImageResourceNuclearExplosion4()
{ }
void ImageResourceNuclearExplosion5()
{ }
void ImageResourceNuclearExplosion6()
{ }
void ImageResourceNuclearExplosion7()
{ }
void ImageResourceNuclearExplosion8()
{ }
void ImageResourceNuclearExplosion9()
{ }
void ImageResourceNuclearExplosion10()
{ }
void ImageResourceNuclearExplosion11()
{ }
void ImageResourceNuclearExplosion12()
{ }
void ImageResourceIconBonus()
{ }

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void initializeImageVector(int imgVector)
{
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceIcon1)+4, 15039);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceIcon2)+4, 15041);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceIcon3)+4, 15040);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceDlink)+4, 112965);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceDlink2)+4, 17866);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("초보자"), GetLevelBuffer());
    AppendTextSprite(imgVector, 0xfc40, GetLevelBuffer(), 16210);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceVictory)+4, 112992);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion1)+4, 112415);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion2)+4, 112416);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion3)+4, 112417);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion4)+4, 112418);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion5)+4, 112419);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion6)+4, 112420);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion7)+4, 112421);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion8)+4, 112422);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion9)+4, 112423);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion10)+4, 112424);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion11)+4, 112425);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceNuclearExplosion12)+4, 112426);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceBucher)+4, 112414);
    AppendImageFrame(imgVector, GetScrCodeField(ImageResourceIconBonus)+4, 15037);
    initializeImageHealthBar(imgVector);
}

void CommonInitializeImage()
{
    int imgVector=CreateImageVector(IMAGE_VECTOR_SIZE);

    initializeImageVector(imgVector);
    DoImageDataExchange(imgVector);
}

