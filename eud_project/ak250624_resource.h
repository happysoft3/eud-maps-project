
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"
#include "libs/spriteDb.h"

void GRPDumpImageRedDotOutput(){}
void GRPDumpImageBLUEDotOutput(){}

void InitializeImageRedDot(int v){
    int *frames, *sizes, thingId=OBJ_RAT;
    int xyinc[]={0,22};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpImageRedDotOutput)+4, thingId, xyinc, &frames,&sizes);
    AppendImageFrame(v,frames[0],121808);
}
void InitializeImageBlueDot(int v){
    int *frames, *sizes, thingId=OBJ_LARGE_BLUE_FLAME;
    int xyinc[]={0,32};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpImageBLUEDotOutput)+4, thingId, xyinc, &frames,&sizes);

    int image[]={15647,15647,};
    ChangeSpriteAnimateImage(thingId,2,image,2);
    AppendImageFrame(v,frames[0],15647);
}
void GRPDumpNothingOutput(){}
void InitializeImageNothing(int v){
    int *frames, *sizes, thingId=OBJ_FISH_BIG;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpNothingOutput)+4, thingId, xyinc, &frames,&sizes);
    AppendImageFrame(v,frames[0],115273);
}
void GRPDumpImageGGOverOutput(){}
void InitializeImageGGOver(int v){
    int *frames, *sizes, thingId=OBJ_LARGE_FLAME;
    int xyinc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpImageGGOverOutput)+4, thingId, xyinc, &frames,&sizes);

    int image[]={15695,15696,15697,};
    ChangeSpriteAnimateImage(thingId,4,image,3);
    AppendImageFrame(v,frames[0],15695);
    AppendImageFrame(v,frames[1],15696);
    AppendImageFrame(v,frames[2],15697);
}
void GRPDumpImageSymbolOutput(){}
void InitializeImageSymbols(int v){
    int *frames, *sizes, thingId=OBJ_WIZARD;
    int xyinc[]={0,23};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpImageSymbolOutput)+4, thingId, xyinc, &frames,&sizes);

    AppendImageFrame(v,frames[1],114956);
    AppendImageFrame(v,frames[0],114992);
}

void GRPDumpImagePuyoClear(){}
void InitializeImageClear(int v){
    int *frames, *sizes, thingId=OBJ_LOTD_CANDLE_GROUP_LARGE_1;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpImagePuyoClear)+4, thingId, xyinc, &frames,&sizes);
    int imgs[]={133363,133364,133365,133366,};
    AppendImageFrame(v,frames[1],133363);
    AppendImageFrame(v,frames[0],133364);
    AppendImageFrame(v,frames[2],133365);
    AppendImageFrame(v,frames[3],133366);
    ChangeSpriteAnimateImage(thingId,2,imgs,4);
}

void GRPDumpImageTitleCard(){}
void InitializeImageTitleCard(int v){
    int *frames, *sizes, thingId=OBJ_CARNIVOROUS_PLANT;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpImageTitleCard)+4, thingId, xyinc, &frames,&sizes);
    
    AppendImageFrame(v,frames[0],14614);
    AppendImageFrame(v,frames[1],14613);
}

//@brief. 서버, 클라이언트측이 공통적으로 수행하는 부분입니다
void InitializeResources(){
    int v=CreateImageVector(1024);

    InitializeImageRedDot(v);
    InitializeImageBlueDot(v);
    InitializeImageGGOver(v);
    InitializeImageSymbols(v);
    InitializeImageClear(v);
    InitializeImageNothing(v);
    InitializeImageTitleCard(v);
    DoImageDataExchange(v);
}
