
#include "libs/define.h"
#include "libs/unitutil.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/memutil.h"
#include "libs/recovery.h"
#include "libs/winapi.h"
#include "libs/networkEx.h"

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        /*
        push ebp
        mov ebp,esp
        push [ebp+0C]
        push [ebp+08]
        push 00000001
        mov eax,game.exe+107310
        call eax
        add esp,0C
        pop ebp
        ret 
        */
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        /*
        mov eax,game.exe+107250
        call eax
        push [eax]
        push [eax+04]
        push [eax+08]
        push [eax+0C]
        mov eax,game.exe+117F90
        call eax
        add esp,10
        xor eax,eax
        ret 
        */
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

void NPCRemoveInventory(int sUnit)
{
    int iCount = GetUnit1C(sUnit);
    int cur = sUnit + 2, i;

    for (i = 0 ; i < iCount ; i ++)
        Delete(cur + (i * 2));
}

void NPCSetInventoryCount(int sUnit)
{
    int *ptr = UnitToPtr(sUnit);

    if (ptr)
    {
        int cur = GetLastItem(sUnit), count=0;
        while (cur)
        {
            // if (GetUnitClass(cur)&UNIT_CLASS_ARMOR)
            //     NpcArmorProperty(cur);
            Enchant(cur,"ENCHANT_INVULNERABLE", 0.0);
            count ++;
            cur = GetPreviousItem(cur);
        }
        ptr[7]=count;
    }
}

void HookFireballTraps(int trapUnit, short missileTy)
{
    int ptr = UnitToPtr(trapUnit);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2ec) + 0x0c, missileTy);
}


void BomberSetMonsterCollide(int bombUnit)
{
    int ptr = UnitToPtr(bombUnit);

    if (ptr)
        SetMemory(ptr + 0x2b8, 0x4e83b0);
}

int BomberBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339074; arr[1] = 29285; arr[17] = 160; arr[18] = 12; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 1; arr[24] = 1065353216; arr[26] = 4; arr[37] = 1399876937; 
		arr[38] = 7630696; arr[53] = 1128792064; arr[55] = 8; arr[56] = 14; arr[58] = 5547856; 
		arr[60] = 1348; arr[61] = 46899968; 
        link=arr;
	}
	return link;
}

void BomberSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 1);
		SetMemory(GetMemory(ptr + 0x22c), 160);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 160);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BomberBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int GreenFrogBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[17] = 130; arr[19] = 64; 
		arr[21] = 1065353216; arr[23] = 2049; arr[24] = 1066192077; arr[26] = 4; arr[28] = 1103101952; 
		arr[29] = 15; arr[31] = 8; arr[32] = 7; arr[33] = 11; arr[59] = 5542784; 
		arr[60] = 1313; arr[61] = 46905856; 
		link=arr;
	}
	return link;
}

void GreenFrogSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1073070735);
		SetMemory(ptr + 0x224, 1073070735);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2049);
		SetMemory(GetMemory(ptr + 0x22c), 130);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 130);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GreenFrogBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 160; arr[19] = 85; arr[21] = 1065353216; arr[23] = 2056; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 3; arr[28] = 1103626240; arr[29] = 28; 
		arr[30] = 1090519040; arr[31] = 3; arr[32] = 14; arr[33] = 24; arr[34] = 1; 
		arr[35] = 1; arr[36] = 20; arr[57] = 5548368; arr[58] = 5546608; arr[59] = 5544320;
        link=arr;
	}
	return link;
}

void GoonSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 160);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 160);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int HorrendousBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 250; arr[19] = 85; 
		arr[21] = 1049918177; arr[24] = 1065353216; arr[25] = 1; arr[26] = 3; arr[27] = 5; 
		arr[28] = 1106247680; arr[29] = 34; arr[32] = 14; arr[33] = 24; arr[54] = 4; 
		arr[59] = 5542784; arr[60] = 1386; arr[61] = 46907648;
        link=arr;
	}
	return link;
}

void HorrendousSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 250);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 250);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HorrendousBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1049918177);
	}
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 135; arr[19] = 65; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 13; arr[56] = 21; 
		arr[58] = 5546320; 
        link=arr;
	}
	return link;
}

void FireSpriteSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1073322393);
		SetMemory(ptr + 0x224, 1073322393);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 135);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 135);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 250; arr[19] = 45; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1065353216; arr[25] = 3; arr[26] = 9; arr[27] = 5; 
		arr[28] = 1106247680; arr[29] = 35; arr[32] = 9; arr[33] = 17; arr[59] = 5542432; 
		arr[60] = 1385; arr[61] = 46907904; 
        link=arr;
	}
	return link;
}

void MaidenSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1068289229);
		SetMemory(ptr + 0x224, 1068289229);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 250);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 250);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 260; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1069547520; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1097859072; arr[29] = 25; arr[31] = 8; arr[32] = 13; arr[33] = 21; 
		arr[34] = 50; arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; 
		arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360;
        link=arr;
	}
	return link;
}

void BlackWidowSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 260);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 260);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int NecromancerBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919116622; arr[1] = 1851878767; arr[2] = 7497059; arr[17] = 275; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 2; 
		arr[28] = 1103626240; arr[29] = 30; arr[30] = 1092616192; arr[31] = 11; arr[32] = 7; 
		arr[33] = 15; arr[34] = 1; arr[35] = 1; arr[36] = 10; arr[59] = 5542784;
		link=arr;
	}
	return link;
}

void NecromancerSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 275);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 275);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, NecromancerBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 325; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 2060; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 40; arr[30] = 1092616192; arr[32] = 13; arr[33] = 21; 
		arr[57] = 5548288; arr[59] = 5542784;
        link=arr;
	}
	return link;
}

void LichLordSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34817);
		SetMemory(GetMemory(ptr + 0x22c), 325);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 325);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichLordBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int HecubahWithOrbBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 320; 
		arr[19] = 80; arr[21] = 1051931443; arr[23] = 34816; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; arr[40] = 1852140903; 
		arr[41] = 116; arr[53] = 1128792064; arr[55] = 16; arr[56] = 26; arr[60] = 1384; 
		arr[61] = 46914560; 
        link=arr;
	}
	return link;
}

void HecubahWithOrbSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1075419545);
		SetMemory(ptr + 0x224, 1075419545);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 320);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 320);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahWithOrbBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1051931443);
	}
}

int MechanicalGolemBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751344461; arr[1] = 1667853921; arr[2] = 1866951777; arr[3] = 7169388; arr[17] = 275; 
		arr[19] = 55; arr[21] = 1065353216; arr[23] = 2061; arr[24] = 1066192077; arr[26] = 1; 
		arr[27] = 5; arr[28] = 1108082688; arr[29] = 40; arr[30] = 1092616192; arr[31] = 2; 
		arr[32] = 20; arr[33] = 28; arr[58] = 5545344; arr[59] = 5542784; arr[60] = 1318; 
		arr[61] = 46900992;
        link=arr;
	}
	return link;
}

void MechanicalGolemSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2061);
		SetMemory(GetMemory(ptr + 0x22c), 275);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 275);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MechanicalGolemBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int CarnivorousPlantBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852989763; arr[1] = 1919907433; arr[2] = 1349743983; arr[3] = 1953390956; arr[17] = 200; 
		arr[19] = 80; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[27] = 5; 
		arr[28] = 1109393408; arr[29] = 30; arr[31] = 8; arr[32] = 9; arr[33] = 18; 
		arr[59] = 5542784; arr[60] = 1371; arr[61] = 46901760;
        link=arr;
	}
	return link;
}

void CarnivorousPlantSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1075419545);
		SetMemory(ptr + 0x224, 1075419545);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 200);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 200);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, CarnivorousPlantBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int DemonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869440324; arr[1] = 110; arr[17] = 250; arr[18] = 200; arr[19] = 96; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[26] = 4; arr[37] = 1869771859; 
		arr[38] = 1766221678; arr[39] = 1633838450; arr[40] = 27756; arr[53] = 1128792064; arr[55] = 14; 
		arr[56] = 24; arr[58] = 5545472; arr[60] = 1347; arr[61] = 46910976;
        link=arr;
	}
	return link;
}

void DemonSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077432811);
		SetMemory(ptr + 0x224, 1077432811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 250);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 250);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, DemonBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 350; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 34817; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1101004800; arr[29] = 20; arr[32] = 6; arr[33] = 12; arr[58] = 5546320; 
		arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328;
        link=arr;
	}
	return link;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076677837);
		SetMemory(ptr + 0x224, 1076677837);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34817);
		SetMemory(GetMemory(ptr + 0x22c), 350);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 350);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 270; 
		arr[19] = 55; arr[21] = 1065353216; arr[24] = 1071225242; arr[26] = 4; arr[28] = 1101004800; 
		arr[29] = 21; arr[31] = 3; arr[32] = 5; arr[33] = 10; arr[59] = 5542784; 
		arr[60] = 1388; arr[61] = 46915072;
        link=arr;
	}
	return link;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 270);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 270);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int HecubahBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 400; arr[19] = 100; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; 
		arr[29] = 60; arr[30] = 1092616192; arr[31] = 11; arr[32] = 10; arr[33] = 18; 
		arr[57] = 5548288; arr[59] = 5542784;
        link=arr;
	}
	return link;
}

void HecubahSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 400);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[17] = 300; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 2056; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; 
		arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; arr[40] = 116; arr[53] = 1128792064; 
		arr[54] = 4; arr[55] = 14; arr[56] = 24;
        link=arr;
	}
	return link;
}

void WizardRedSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1069547520);
		SetMemory(ptr + 0x224, 1069547520);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2056);
		SetMemory(GetMemory(ptr + 0x22c), 300);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int ColorMaidenAt(int red, int grn, int blue, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("Bear2", xProfile, yProfile);
    int ptr1 = GetMemory(0x750710), k;

    SetMemory(ptr1 + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    return unit;
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	//SetMemory(0x833e70, 1329);		//FishBig
	//SetMemory(0x833e74, 1330);		//FishSmall
	//SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	//SetMemory(0x833e70, 0x540);		//FishBig
	//SetMemory(0x833e74, 0x540);		//FishSmall
	//SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

int DrawMagicIconAt(float sX, float sY)
{
    int unit = CreateObjectAt("AirshipBasketShadow", sX, sY);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, 1416);
    return unit;
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}
