
#include "libs\define.h"
#include "libs\printutil.h"
#include "libs\unitstruct.h"
#include "libs\potionex.h"
#include "libs\spellutil.h"
#include "libs\coopteam.h"
#include "libs\playerupdate.h"
#include "libs\itemproperty.h"
#include "libs\weaponcapacity.h"
#include "libs\potionpickup.h"
#include "libs\absolutelypickup.h"
#include "libs\buff.h"
#include "libs\reaction.h"
#include "libs\username.h"
#include "libs\fxeffect.h"
#include "libs\mathlab.h"
#include "libs\fixtellstory.h"
#include "libs\network.h"

int m_beachGirlPos;
int player[20];
int g_waveMonster[180];


int GreenFrogBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[17] = 1; arr[18] = 1; 
		arr[19] = 72; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1112014848; arr[29] = 3; arr[32] = 18; arr[33] = 22; arr[59] = 5542784; 
		arr[60] = 1313; arr[61] = 46905856; 
	pArr = &arr;
	return pArr;
}

void GreenFrogSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074412912;		ptr[137] = 1074412912;
	int *hpTable = ptr[139];
	hpTable[0] = 1;	hpTable[1] = 1;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = GreenFrogBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void ResetHostileCritter()
{
    int *targetbase = 0x833e64;

    targetbase[0] = 0x55b;      //CarnivorousPlant
    targetbase[3] = 1329;       //FishBig
    targetbase[4] = 1330;       //FishSmall
    targetbase[5] = 1359;       //Rat
    targetbase[6] = 1313;       //GreenFrog
}

void SetHostileCritter()
{
    int *targetbase = 0x833e64;

    targetbase[0] = 0x540;      //CarnivorousPlant
    targetbase[3] = 0x540;       //FishBig
    targetbase[4] = 0x540;       //FishSmall
    targetbase[5] = 0x540;       //Rat
    targetbase[6] = 0x540;       //GreenFrog
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int WizardRedBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 40; arr[24] = 1067869798; 
		arr[25] = 0; arr[26] = 4; arr[27] = 4; arr[53] = 1128792064; arr[54] = 4;
		link = &arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1065688760; 
		arr[25] = 0; arr[26] = 0; arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 15; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 1; 
		arr[35] = 2; arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = &arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101;
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146;
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
        arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 		
        link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; 
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896;  arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
        arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146;
		arr[53] = 1128792064; 
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = &arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link = &arr;
	}
	return link;
}

int WoundedApprenticeBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101;
		arr[17] = 50; arr[18] = 10; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[28] = 1112014848; arr[29] = 40; 
		arr[30] = 1106247680; arr[31] = 2; arr[32] = 12; arr[33] = 20;
		arr[57] = 5548112; arr[59] = 5542784;
		link = &arr;
	}
	return link;
}

int Bear2BinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50;
		arr[16] = 20000; arr[17] = 90; arr[18] = 100; arr[19] = 40; 
		arr[21] = 1065353216; arr[23] = 65545; arr[24] = 1067450368; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30; 
		arr[58] = 5547856; arr[59] = 5542784;
		link = &arr;
	}
	return link;
}

void CheckMonsterThing(int unit)
{
    int arr[97], init;
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (!init)
    {
        init = 1;
        arr[5] = MonsterGoonProcess; arr[72] = MonsterStrongWhiteWizProcess; arr[30] = MonsterWeirdlingBeastProcess; arr[34] = MonsterBlackWidowProcess; arr[6] = MonsterBear2Process;
        arr[12] = MonsterFireSpriteProcess; arr[73] = MonsterWizardRedProcess; arr[29] = MonsterAirshipCaptainProcess; arr[52] = GreenFrogSubProcess;
    }
    if (thingID)
    {
        if (arr[key])
            CallFunctionWithArg(arr[key], unit);
    }
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

void MonsterWoundedApprentice(int unit)
{
    //TODO: Index. 9, ThingName= WoundedApprentice
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedApprenticeBinTable());
        SetUnitMaxHealth(unit, 230);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}

void EmptyAll(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);
    PotionPickupRegist(x);
    return x;
}

int DeadUnitCreate(string name, int wp)
{
    int unit = CreateObject(name, wp);

    if (CurrentHealth(unit))
    {
        ObjectOff(unit);
        Damage(unit, 0, MaxHealth(unit) + 1, -1);
        Frozen(unit, 1);
    }
    return unit;
}

void PutExtentsionMarket()
{
    int unit = DeadUnitCreate("Hecubah", 39);

    SetDialog(DeadUnitCreate("WizardWhite", 38), "aa", BuyPowerMagicField, nullPointer);
    SetDialog(unit, "aa", BuyOblivionStaff, nullPointer);
}

void UserMapSetting()
{
    SetMemory(0x5d5330, 0x2000);
    SetMemory(0x5d5394, 1);
}

void MapInitialize()
{
    m_beachGirlPos = CreateObject("InvisibleLightBlueHigh", 1);
    
    CheckMonsterThing(0);
    
    initMap(-1);
    ObjectOn(Object("startTrg"));
    setupMap();
    PutExtentsionMarket();
    FrameTimer(50, LoopSearchIndex);
    FrameTimer(2, LoopPreservePlayer);
    FrameTimer(20, strBuySoldier);
    FrameTimer(20, strGameBegin);
    FrameTimer(40, runLoopTriggers);
    FrameTimer(100, beginMent);
    FrameTimer(1, UserMapSetting);

    FrameTimer(1, MakeCoopTeam);
    SetHostileCritter();
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
    ResetHostileCritter();
}

void beginMent()
{
    UniPrintToAll("게임의 목표_!! 플로리다 비치의 섹시한 그녀...");
    UniPrintToAll("'비키니 걸' 이 죽지않도록 보호하라...!! 만약, 이 그녀가 죽으면 게임오버..!!");
    UniChatMessage(beachGirl(), "비키니 걸", 180);
    FrameTimerWithArg(50, beachGirl(), NearGirlPos);
    FrameTimerWithArg(30, 40 | (20 << 0x10), PlaceDefaultItem);
    FrameTimerWithArg(30, 41, WarAbilityShopClassCreate);
}

void initMap(int arg_0) {
    mobHp(arg_0);
    mobType(arg_0);
    getDeaths(arg_0 * 10);
}

void runLoopTriggers()
{
    int subunit = CreateObject("CarnivorousPlant", 25);

    SetUnitHealth(subunit, 4196);
    Frozen(subunit, 1);
}

void setLoopTriggers()
{
    if (CurrentHealth(other) == 4196)
        LoopMonsterStatus();
}

void PlayerClassUseSkillCheck(int plr)
{
    int pUnit = player[plr];

    if (UnitCheckEnchant(pUnit, GetLShift(31)) && PlayerClassSkillFlag1Check(plr))
    {
        EnchantOff(pUnit, EnchantList(31));
        RemoveTreadLightly(pUnit);
        WarriorSkillR(plr);
    }
    else if (PlayerClassSkillFlag2Check(plr))
    {
        if (!UnitCheckEnchant(pUnit, GetLShift(15)) && CheckPlayerInput(pUnit) == 47)
        {
            UnitSetEnchantTime(pUnit, 15, 30 * 10);
            warriorSkillS(plr);
        }
    }
}

int PlayerClassDeathFlagCheck(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassDeathFlagSet(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

int PlayerClassSkillFlag1Check(int plr)
{
    return player[plr + 10] & 0x02;
}

void PlayerClassSkillFlag1Set(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x02;
}

int PlayerClassSkillFlag2Check(int plr)
{
    return player[plr + 10] & 0x04;
}

void PlayerClassSkillFlag2Set(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x04;
}

int PlayerClassSkillFlag3Check(int plr)
{
    return player[plr + 10] & 0x08;
}

void PlayerClassSkillFlag3Set(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x08;
}

void PlayerClassOnFree(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

void PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;
    ChangeGold(pUnit, -GetGold(pUnit));
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
}

void PlayerClassOnJoin(int plr)
{
    int unit = player[plr], ptr;

    if (PlayerClassDeathFlagCheck(plr))
    {
        PlayerClassDeathFlagSet(plr);
    }
    MoveObject(unit, GetWaypointX(1), GetWaypointY(1));
    DeleteObjectTimer(CreateObject("BlueRain", 1), 30);
    Effect("TELEPORT", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
    AudioEvent("BlindOff", 1);
}

void PlayerClassFailedJoin(int pUnit)
{
    MoveObject(pUnit, GetWaypointX(23), GetWaypointY(23));

    UnitSetEnchantTime(pUnit, 2, 0);
    UniPrint(pUnit, "맵에 입장하지 못하였습니다. 잠시 후 다시 시도해보십시오");
}

void getPlayer()
{
    int i, plr;

    if (CurrentHealth(other))
    {
        plr = CheckPlayer();
        for (i = 9 ; i >= 0 && plr < 0 ; i --)
        {
            if (!MaxHealth(player[i]))
            {
                PlayerClassOnInit(i, GetCaller());
                plr = i;
                break;
            }
        }
        if (plr >= 0)
            PlayerClassOnJoin(plr);
        else
            PlayerClassFailedJoin(other);
    }
}

void LoopPreservePlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassUseSkillCheck(i);
                    break;
                }
                else
                {
                    if (PlayerClassDeathFlagCheck(i)) break;
                    else
                    {
                        PlayerClassDeathFlagSet(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayer);
}

void WarriorSkillR(int plr)
{
    int trap[10], pUnit = player[plr];

    PlaySoundAround(pUnit, 39);
    if (IsObjectOn(trap[plr]))
        MoveObject(trap[plr], GetObjectX(pUnit), GetObjectY(pUnit));
    else
        trap[plr] = CreateObjectAt("PoisonGasTrap", GetObjectX(pUnit), GetObjectY(pUnit));
    SetOwner(pUnit, trap[plr]);
    BerserkerNoDelayCore(plr);
}

void RemoveCoreUnits(int ptr)
{
    Delete(ptr);
    Delete(++ptr);
}

void BerserkerNoDelayCore(int plr)
{
    int arr[10];

    if (!MaxHealth(arr[plr]))
    {
        arr[plr] = CreateObject("Bear2", 28 + plr);
        UnitLinkBinScript(CreateObjectAt("Rat", GetObjectX(arr[plr]), GetObjectY(arr[plr]) + 20.0) - 1, Bear2BinTable());
        SetOwner(player[plr], arr[plr]);
        LookAtObject(arr[plr], arr[plr] + 1);
        HitLocation(arr[plr], GetObjectX(arr[plr]), GetObjectY(arr[plr]));
        FrameTimerWithArg(3, arr[plr], RemoveCoreUnits);
    }
}

void warriorSkillS(int arg_0)
{
    int var_0[10];
    int var_1;
    int i;
    float var_2;
    float var_3;

    if (CurrentHealth(player[arg_0]) && var_0[arg_0] < 18)
    {
        var_2 = MathSine(var_0[arg_0] * 20, 38.0);
        var_3 = MathSine(var_0[arg_0] * 20 + 90, 38.0);
        MoveWaypoint(14, GetObjectX(player[arg_0]) + var_2, GetObjectY(player[arg_0]) + var_3);
        AudioEvent("GolemHitting", 14);
        for (i = 5 ; i >= 0 ; i --)
        {
            var_1 = CreateObject("CarnivorousPlant", 14);
            Frozen(var_1, 1);
            DeleteObjectTimer(var_1, 1);
            SetCallback(var_1, 9, touchedFire);
            DeleteObjectTimer(CreateObject("FireBoom", 14), 7);
            MoveWaypoint(14, GetWaypointX(14) + var_2, GetWaypointY(14) + var_3);
        }
        var_0[arg_0] ++;
        FrameTimerWithArg(1, arg_0, warriorSkillS);
    }
    else {
        var_0[arg_0] = 0;
    }
}

void UnitVisibleSplash()
{
    int parent = GetOwner(self);
    int spIdx = ToInt(GetObjectZ(parent + 1)), ptr = UnitToPtr(other);

    if (ptr)
    {
        if (GetMemory(ptr + 0x1c) ^ spIdx && CurrentHealth(GetOwner(parent)))
        {
            if (Distance(GetObjectX(self), GetObjectY(self), GetObjectX(other), GetObjectY(other)) <= GetObjectX(parent))
            {
                CallFunction(ToInt(GetObjectZ(parent)));
                SetMemory(ptr + 0x1c, spIdx);
            }
        }
    }
}

void SplashHandler(int owner, int func, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, k, SplashIdx;

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(func));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        CheckMonsterThing(ptr + k);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 32);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(ptr - 2, 2);
    SplashIdx ++;
}

void FlameRingSkillDamage()
{
    Damage(other, GetOwner(GetOwner(self)), 150, 14);
}

void WarriorFlameRing(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit, i;

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner))
        {
            for (i = 0 ; i < 18 ; i ++)
            {
                DeleteObjectTimer(CreateObjectAt("MediumFireBoom", GetObjectX(ptr) + MathSine(i * 20 + 90, 38.0), GetObjectY(ptr) + MathSine(i * 20, 38.0)), 20);
                DeleteObjectTimer(CreateObjectAt("MediumFireBoom", GetObjectX(ptr) + MathSine(i * 20 + 90, 76.0), GetObjectY(ptr) + MathSine(i * 20, 76.0)), 20);
                DeleteObjectTimer(CreateObjectAt("MediumFireBoom", GetObjectX(ptr) + MathSine(i * 20 + 90, 114.0), GetObjectY(ptr) + MathSine(i * 20, 114.0)), 20);
            }
        }
        Delete(ptr);
    }
}

void WarriorSkillS2(int owner)
{
    int unit;

    if (CurrentHealth(owner))
    {
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));
        SetOwner(owner, unit);
        LookWithAngle(unit, 18);
        Raise(unit, FlameRingSkillDamage);
        SplashHandler(owner, ToInt(GetObjectZ(unit)), GetObjectX(unit), GetObjectY(unit), 300.0);
        FrameTimerWithArg(1, unit, WarriorFlameRing);
    }
}

void touchedFire()
{
    int plr = GetDirection(self);

    if (CurrentHealth(other) && IsAttackedBy(other, player[plr]) && !HasEnchant(other, "ENCHANT_BURNING"))
    {
        Damage(other, player[plr], 120, 14);
        Enchant(other, "ENCHANT_BURNING", 0.8);
    }
}

int TripleArrowCreate(int owner, float sX, float sY)
{
    int unit = CreateObjectAt("ArcherArrow", sX, sY);

    SetOwner(owner, unit);
    LookAtObject(unit, owner);
    LookWithAngle(unit, GetDirection(unit) + 128);
    PushObject(unit, 20.0, GetObjectX(owner), GetObjectY(owner));
    return unit;
}

void WarriorSkillD(int owner)
{
    float vectX, vectY, thrdHold = 0.1;
    int misUnit, plr = PlayerClassScrIndexGet(owner);
    
    if (plr < 0) return;
    if (CurrentHealth(owner) && PlayerClassSkillFlag3Check(plr))
    {
        vectX = UnitAngleCos(owner, -7.0);
        vectY = UnitAngleSin(owner, -7.0);
        misUnit = TripleArrowCreate(owner, GetObjectX(owner) - vectX, GetObjectY(owner) - vectY);
        while (thrdHold < 0.4)
        { 
            TripleArrowCreate(owner, (thrdHold * vectY) + GetObjectX(owner) - vectX, (-thrdHold * vectX) + GetObjectY(owner) - vectY);
            TripleArrowCreate(owner, (-thrdHold * vectY) + GetObjectX(owner) - vectX, (thrdHold * vectX) + GetObjectY(owner) - vectY);
            thrdHold += 0.1;
        }
    }
}

void ShurikenEvent(int cur)
{
    int mis, owner = GetOwner(cur);

    if (CurrentHealth(owner))
    {
        MoveWaypoint(17, GetObjectX(cur), GetObjectY(cur));
        mis = CreateObject("WeakFireball", 17);
        SetOwner(owner, mis);
        PushObjectTo(mis, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
    }
}

void DetectedSpecificIndex(int curId)
{
    int owner = GetOwner(curId), thingID;

    thingID = GetUnitThingID(curId);
    if (thingID == 526)
    {
        WarriorSkillD(owner);
        Delete(curId);
    }
    else if (thingID == 1179)
        ShurikenEvent(owner);
}

void LoopSearchIndex()
{
    int curId, tempId;

    if (GetMemory(0x750710))
    {
        tempId = GetMemory(GetMemory(0x750710) + 0x2c);
        if (curId)
        {
            while (curId < tempId)
            {
                curId += 1;
                DetectedSpecificIndex(curId);
            }
        }
        else
            curId = tempId;
    }
    FrameTimer(1, LoopSearchIndex);
}

void ThisMapSignInit()
{
    RegistSignMessage(Object("SignElectricBuff"), "전기 뱀장어[60초간 지속적으로 쇼크 엔첸트가 걸립니다] 3만골드");
    RegistSignMessage(Object("SignOblivion"), "망각의 지팡이를 구입하기 위한 가격은 5만 골드입니다!");
    RegistSignMessage(Object("SignBodyGuard"), "용병구입 2만 골드입니다! 최대 12개 까지 보유하실 수 있습니다");
    RegistSignMessage(Object("SignStartButn"), "시작버튼입니다. 누르면 게임이 시작되고 맵에 깔린 아이템이 모두 지워집니다");
}

void setupMap()
{
    SetOwner(GetHost(), beachGirl());
    SetUnitHealth(beachGirl(), 5000);
    SetCallback(beachGirl(), 5, deadGirl);
    SetCallback(beachGirl(), 7, hurtGirl);
    AggressionLevel(beachGirl(), 0.0);
    ThisMapSignInit();
    Enchant(beachGirl(), "ENCHANT_BLINDED", 0.0);
    Enchant(beachGirl(), "ENCHANT_ANCHORED", 0.0);
}

void deadGirl() {
    UniPrintToAll("미션실패!-- 비키니걸 이 죽었습니다...!!");
    UniPrintToAll("미션실패!-- 비키니걸 이 죽었습니다...!!");
    UniPrintToAll("미션실패!-- 비키니걸 이 죽었습니다...!!");
    DeleteObjectTimer(self, 1);
    ObjectOff(Object("startTrg"));
    TeleportAllPlayers(9);
}

void victoryMission() {
    UniPrintToAll("승리_!!-- 20개의 모든 스테이지를 클리어 했습니다.");
    TeleportAllPlayers(20);
    AudioEvent("BigGong", 20);
    AudioEvent("FlagCapture", 20);
    Effect("WHITE_FLASH", GetWaypointX(20), GetWaypointY(20), 0.0, 0.0);
    FrameTimer(1, strVictory);
}

void TeleportAllPlayers(int wp)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]))
            MoveObject(player[i], GetWaypointX(wp), GetWaypointY(wp));
    }
}

void hurtGirl()
{
    UniChatMessage(self, "섹시한 비키니걸\n남은체력: " + IntToString(CurrentHealth(self)), 99);
}

void NearGirlPos(int unit)
{
    int unitPos = m_beachGirlPos;

    if (CurrentHealth(unit))
    {
        if (Distance(GetObjectX(unitPos), GetObjectY(unitPos), GetObjectX(unit), GetObjectY(unit)) > 200.0)
        {
            MoveObject(unit, GetObjectX(unitPos), GetObjectY(unitPos));
            Effect("TELEPORT", GetObjectX(unitPos), GetObjectY(unitPos), 0.0, 0.0);
        }
        SecondTimerWithArg(5, unit, NearGirlPos);
    }
}

int beachGirl()
{
    int unit;

    if (!unit) unit = Object("finalMaiden");

    return unit;
}

string mobType(int order)
{
    string unitname[60];
    
    if (order == -1)
    {
        //area.1
        unitname[0] = "GreenFrog";
        unitname[1] = "GreenFrog";
        unitname[2] = "GreenFrog";
        //area.2
        unitname[3] = "giantLeech";
        unitname[4] = "flyingGolem";
        unitname[5] = "imp";
        //area.3
        unitname[6] = "urchin";
        unitname[7] = "wolf";
        unitname[8] = "flyingGolem";
        //area.4
        unitname[9] = "swordsman";
        unitname[10] = "archer";
        unitname[11] = "whiteWolf";
        //area.5
        unitname[12] = "spider";
        unitname[13] = "urchinShaman";
        unitname[14] = "scorpion";
        //area.6
        unitname[15] = "bear";
        unitname[16] = "blackBear";
        unitname[17] = "blackWolf";
        //area.7
        unitname[18] = "WeirdlingBeast";
        unitname[19] = "troll";
        unitname[20] = "albinoSpider";
        //area.8
        unitname[21] = "wizardGreen";
        unitname[22] = "goon";
        unitname[23] = "scorpion";
        //area.9
        unitname[24] = "SpittingSpider";
        unitname[25] = "shade";
        unitname[26] = "bear2";
        //area.10
        unitname[27] = "beholder";
        unitname[28] = "shade";
        unitname[29] = "willOWisp";
        //area.11
        unitname[30] = "gruntAxe";
        unitname[31] = "ogreBrute";
        unitname[32] = "ogreWarlord";
        //area.12
        unitname[33] = "vileZombie";
        unitname[34] = "zombie";
        unitname[35] = "SkeletonLord";
        //area.13
        unitname[36] = "Skeleton";
        unitname[37] = "EvilCherub";
        unitname[38] = "SkeletonLord";
        //area.14
        unitname[39] = "wizard";
        unitname[40] = "VileZombie";
        unitname[41] = "SkeletonLord";
        //area.15
        unitname[42] = "meleeDemon";
        unitname[43] = "FireSprite";
        unitname[44] = "imp";
        //area.16
        unitname[45] = "EmberDemon";
        unitname[46] = "Mimic";
        unitname[47] = "FireSprite";
        //area.17
        unitname[48] = "MechanicalGolem";
        unitname[49] = "Swordsman";
        unitname[50] = "bear2";
        //area.18
        unitname[51] = "WeirdlingBeast";
        unitname[52] = "EmberDemon";
        unitname[53] = "EvilCherub";
        //area.19
        unitname[54] = "Wizard";
        unitname[55] = "WillOWisp";
        unitname[56] = "OgreWarlord";
        //area.20
        unitname[57] = "HecubahWithOrb";
        unitname[58] = "StoneGolem";
        unitname[59] = "TalkingSkull";
        return unitname[0];
    }
    return unitname[order];
}

int mobHp(int order)
{
    int healthPoint[60];

    if (order == -1)
    {
        //area.1
        healthPoint[0] = 1;
        healthPoint[1] = 1;
        healthPoint[2] = 1;
        //area.2
        healthPoint[3] = 60;
        healthPoint[4] = 50;
        healthPoint[5] = 50;
        //area.3
        healthPoint[6] = 60;
        healthPoint[7] = 80;
        healthPoint[8] = 60;
        //area.4
        healthPoint[9] = 160;
        healthPoint[10] = 75;
        healthPoint[11] = 105;
        //area.5
        healthPoint[12] = 125;
        healthPoint[13] = 90;
        healthPoint[14] = 170;
        //area.6
        healthPoint[15] = 200;
        healthPoint[16] = 140;
        healthPoint[17] = 125;
        //area.7
        healthPoint[18] = 130;
        healthPoint[19] = 200;
        healthPoint[20] = 135;
        //area.8
        healthPoint[21] = 135;
        healthPoint[22] = 120;
        healthPoint[23] = 210;
        //area.9
        healthPoint[24] = 150;
        healthPoint[25] = 160;
        healthPoint[26] = 225;
        //area.10
        healthPoint[27] = 250;
        healthPoint[28] = 175;
        healthPoint[29] = 148;
        //area.11
        healthPoint[30] = 175;
        healthPoint[31] = 225;
        healthPoint[32] = 260;
        //area.12
        healthPoint[33] = 306;
        healthPoint[34] = 225;
        healthPoint[35] = 225;
        //area.13
        healthPoint[36] = 255;
        healthPoint[37] = 130;
        healthPoint[38] = 275;
        //area.14
        healthPoint[39] = 225;
        healthPoint[40] = 325;
        healthPoint[41] = 295;
        //area.15
        healthPoint[42] = 230;
        healthPoint[43] = 175;
        healthPoint[44] = 200;
        //area.16
        healthPoint[45] = 240;
        healthPoint[46] = 350;
        healthPoint[47] = 175;
        //area.17
        healthPoint[48] = 500;
        healthPoint[49] = 325;
        healthPoint[50] = 290;
        //area.18
        healthPoint[51] = 225;
        healthPoint[52] = 225;
        healthPoint[53] = 96;
        //area.19
        healthPoint[54] = 195;
        healthPoint[55] = 210;
        healthPoint[56] = 325;
        //area.20
        healthPoint[57] = 295;
        healthPoint[58] = 550;
        healthPoint[59] = 180;
        return healthPoint[0];
    }
    return healthPoint[order];
}

void loadNextStage()
{
    if (getDeaths(-1) == 180)
        controlStage();
    else
        UniPrintToAll("맵에 남아있는 적 유닛이 존재합니다. 모두 처리하신 후 다시 시도하십시오");
}

void controlStage()
{
    MainSwitchHandler(FALSE);
    getDeaths(0);
    UniPrintToAll("방금 시작스위치가 눌려졌습니다, 현재 스테이지는 " + IntToString(getStage(1)) + " 입니다");
    SaveInvIndex(180);
    if (getStage(0) > 20)
        victoryMission();
    else
        summoningStart(0);
}

int getStage(int arg_0) {
    int var_0;

    if (arg_0 == 1)
        var_0 += 1;
    return var_0;
}

int getDeaths(int arg_0) {
    int var_0;

    if (!arg_0)
        var_0 = 0;
    else if (arg_0 == 1)
        var_0 += 1;
    else if (arg_0 == -10)
        var_0 = 180;
    return var_0;
}

void summoningStart(int arg_0) {
    if (arg_0 < 180) {
        spawnUnit(arg_0);
        if (arg_0 % 18 == 17)
            FrameTimerWithArg(300, arg_0 + 1, summoningStart);
        else
            FrameTimerWithArg(1, arg_0 + 1, summoningStart);
    }
}

void spawnUnit(int order)
{
    int rndPick = Random((getStage(0) - 1) * 3, (getStage(0) - 1) * 3 + 2);

    g_waveMonster[order] = CreateObject(mobType(rndPick), (order % 3) + 3);
    CheckMonsterThing(g_waveMonster[order]);
    SetUnitMaxHealth(g_waveMonster[order], mobHp(rndPick));
    SetCallback(g_waveMonster[order], 5, setDeaths);
    setSpecialUnit(rndPick, g_waveMonster[order]);
    FrameTimerWithArg(1, g_waveMonster[order], targetToGirl);
}

void setSpecialUnit(int uniqId, int unit)
{
    if (uniqId == 0 || uniqId == 1 || uniqId == 2 || uniqId == 4 || uniqId == 5 || uniqId == 43 || uniqId == 47)
        Enchant(unit, "ENCHANT_SLOWED", 0.0);
    else if (uniqId == 21 || uniqId == 39 || uniqId == 54 || uniqId == 27)
        Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    else if (uniqId == 29 || uniqId == 55)
    {
        SetUnitScanRange(unit, 300.0);
        SetCallback(unit, 3, wispWeapon);
    }
}

void targetToGirl(int unit)
{
    if (CurrentHealth(beachGirl()))
    {
        AggressionLevel(unit, 1.0);
        CreatureFollow(unit, beachGirl());
    }
}

void wispWeapon()
{
    if (CurrentHealth(self))
    {
        Effect("SENTRY_RAY", GetObjectX(self), GetObjectY(self), GetObjectX(other), GetObjectY(other));
        Damage(other, self, 7, 16);
        Enchant(self, "ENCHANT_BLINDED", 0.1);
        Attack(self, other);
    }
}

void MainSwitchHandler(int tof)
{
    int unit;

    if (!unit) unit = Object("mainStart");
    if (tof) ObjectOn(unit);
    else ObjectOff(unit);
}

void setDeaths()
{
    MoveWaypoint(6, GetObjectX(self), GetObjectY(self));
    SaveInvIndex(CallFunctionWithArgInt(FieldItemFuncTable(Random(0, 7)), 6));
    DeleteObjectTimer(self, 35);
    if (getDeaths(1) == 180 && CurrentHealth(beachGirl()))
    {
        MainSwitchHandler(TRUE);
        FrameTimerWithArg(1, beachGirl(), HealingBeachGirl);
        UniPrintToAll("맵 내의 모든 적 유닛을 처치했습니다__!! 다음 판 시작 가능합니다.");
        UniPrintToAll("비키니걸 의 체력이 회복되었습니다");
    }
}

void HealingFx(int unit)
{
    float posX = GetObjectX(unit), posY = GetObjectY(unit);
    int i;

    if (CurrentHealth(unit))
    {
        for (i = 0 ; i < 36 ; i += 1)
            Effect("CHARM", posX, posY, posX + MathSine(i * 10 + 90, 120.0), posY + MathSine(i * 10, 120.0));
    }
}

void HealingBeachGirl(int unit)
{
    if (CurrentHealth(unit))
    {
        CastSpellObjectObject("SPELL_CURE_POISON", unit, unit);
        CastSpellObjectObject("SPELL_CURE_POISON", unit, unit);
        CastSpellObjectObject("SPELL_CURE_POISON", unit, unit);
        SetUnitHealth(unit, 5000);
        FrameTimerWithArg(3, unit, HealingFx);
    }
}

void SaveInvIndex(int someindex)
{
    int somearr[180], i;
    
    if (someindex == 180)
    {
        for (i = 0 ; i < 180 ; i += 1)
        {
            if (IsObjectOn(somearr[i]) && HasEnchant(somearr[i], "ENCHANT_BURNING"))
                Delete(somearr[i]);
        }
    }
    else
    {
        if (i == 180)
            i = 0;
        Enchant(someindex, "ENCHANT_BURNING", 0.0);
        somearr[i] = someindex;
        i += 1;
    }
}

void LoopMonsterStatus()
{
    int i, k;

    for (k = 0 ; k < 3 ; k += 1)
    {
        if (CurrentHealth(g_waveMonster[i]))
        {
            if (HasEnchant(g_waveMonster[i], "ENCHANT_CHARMING"))
            {
                UniChatMessage(g_waveMonster[i], "아 뭐야~ 참크리쳐 개오바잖아 ㅡㅡ;;", 120);
                MoveObject(reventCharm(), GetObjectX(g_waveMonster[i]), GetObjectY(g_waveMonster[i]));
                CastSpellObjectObject("SPELL_COUNTERSPELL", reventCharm(), reventCharm());
            }
        }
        i = (i + 1) % sizeof(g_waveMonster);
    }
}

void GreenSparkFxAtWaypoint(int wp, float x, float y)
{
    int ptr = CreateObjectAt("MonsterGenerator", x, y);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void ElectricField(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner) && HasEnchant(owner, "ENCHANT_CROWN"))
    {
        if (!HasEnchant(owner, "ENCHANT_SHOCK"))
        {
            GreenSparkFxAtWaypoint(11, GetObjectX(owner), GetObjectY(owner));
            if (CurrentHealth(owner) ^ MaxHealth(owner))
                RestoreHealth(owner, 7);
            Enchant(owner, "ENCHANT_SHOCK", 0.0);
        }
        MoveObject(ptr + 1, GetObjectX(owner), GetObjectY(owner));
        FrameTimerWithArg(1, ptr, ElectricField);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void BuyPowerMagicField()
{
    int unit;

    if (HasEnchant(other, "ENCHANT_AFRAID"))
    {
        EnchantOff(other, "ENCHANT_AFRAID");
        if (GetGold(other) >= 30000)
        {
            if (HasEnchant(other, "ENCHANT_CROWN"))
            {
                UniPrint(other, "거래가 취소되었습니다, 아직 마법이 지속되고 있는 상태입니다");
                return;
            }
            MoveWaypoint(11, GetObjectX(other), GetObjectY(other));
            unit = CreateObject("InvisibleLightBlueHigh", 11);
            Enchant(CreateObject("InvisibleLightBlueHigh", 11), "ENCHANT_SHIELD", 0.0);
            Enchant(unit + 1, "ENCHANT_RUN", 0.0);
            SetOwner(other, unit);
            FrameTimerWithArg(1, unit, ElectricField);
            AudioEvent("LichRecognize", 11);
            Enchant(other, "ENCHANT_CROWN", 60.0);
            ChangeGold(other, -30000);
            UniPrint(other, "거래완료! 1분동안 전기적 방어막이 당신을 보호해 줄 것입니다");
        }
        else
            UniPrint(other, "거래가 취소되었습니다, 잔액이 부족합니다");
    }
    else
    {
        Enchant(other, "ENCHANT_AFRAID", 0.3);
        UniChatMessage(self, "전기뱀장어 섭취: 3만원", 120);
        UniPrint(other, "전기뱀장어를 섭취하면 1분동안 지속적으로 쇼크엔첸트가 걸립니다. 이 작업은 3만원이 요구됩니다");
        UniPrint(other, "계속 거래를 원하시면 더블클릭 하십시오");
    }
}

int SummonOblivionStaff(float xProfile, float yProfile)
{
    int staff = CreateObjectAt("OblivionOrb", xProfile, yProfile);

    DisableOblivionItemPickupEvent(staff);
    SetItemPropertyAllowAllDrop(staff);
    return staff;
}

void BuyOblivionStaff()
{
    if (HasEnchant(other, "ENCHANT_AFRAID"))
    {
        EnchantOff(other, "ENCHANT_AFRAID");
        if (GetGold(other) >= 50000)
        {
            // MoveWaypoint(11, GetObjectX(other), GetObjectY(other));
            ChangeGold(other, -50000);
            Enchant(SummonOblivionStaff(GetObjectX(other), GetObjectY(other)), "ENCHANT_ANCHORED", 0.0);
            UniPrint(other, "망각의 지팡이를 구입했습니다, 구입하신 물품은 캐릭터 아래에 있습니다");
        }
        else
        {
            UniPrint(other, "거래가 취소되었습니다, 잔액이 부족합니다");
        }
    }
    else
    {
        Enchant(other, "ENCHANT_AFRAID", 0.3);
        UniChatMessage(self, "망각의 지팡이 구입: 5만원", 120);
        UniPrint(other, "망각의 지팡이를 구입하시겠습니까?, 이 작업은 5만원이 요구됩니다");
        UniPrint(other, "계속 거래를 원하시면 더블클릭 하십시오");
    }
}

void GuardianHuntHandler()
{
    if (!HasEnchant(self, "ENCHANT_SHOCK"))
    {
        Enchant(self, "ENCHANT_SHOCK", 0.0);
    }
}

void buySoldier()
{
    int unit;

    if (GetGold(other) >= 20000 && soldierCount(0) < 12)
    {
        UniPrintToAll("용병을 구입했습니다. 현재 용병개수: " + IntToString(soldierCount(1)));
        ChangeGold(other, -20000);
        MoveWaypoint(21, GetObjectX(other), GetObjectY(other));
        unit = CreateObject("horrendous", 21);
        SetUnitHealth(unit, 2000);
        SetOwner(beachGirl(), unit);
        RetreatLevel(unit, 0.0);
        SetCallback(unit, 5, deadOfSoldier);
        SetCallback(unit, 7, GuardianHuntHandler);
        SetDialog(unit, "NORMAL", soldierGuardMode, nullPointer);
        Enchant(unit, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
    }
    else {
        UniPrintToAll("처리실패__!: 금화 2만원이 부족하거나 용병 개수제한 12 개를 초과하였습니다.");
    }
}

void soldierGuardMode() {
    CreatureFollow(self, other);
    AggressionLevel(self, 1.0);
}

void nullPointer()
{
    return;
}

void deadOfSoldier() {
    UniChatMessage(self, "용병: 나의 죽음을 적에게 알리지마라~~!", 150);
    soldierCount(-1);
    DeleteObjectTimer(self, 30);
}

int soldierCount(int arg_0) {
    int count;

    if (arg_0 == 1) count ++;
    else if (arg_0 == -1) count --;

    return count;
}

int reventCharm()
{
    int var_0;

    if (!var_0) var_0 = CreateObject("InvisibleLightBlueHigh", 1);
    return var_0;
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObject("RottenMeat", 10));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

int CheckPlayer()
{
    int i;

    for(i = 9 ; i >= 0 ; i --)
        if (IsCaller(player[i])) return i;
    return -1;
}

int PlayerClassScrIndexGet(int pUnit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (pUnit ^ player[i])
            continue;
        else
            return i;
    }
    return -1;
}

void CheckSpecialItem(int special)
{
    int thingid = GetUnitThingID(special);

    if (thingid >= 222 && thingid <= 225)
    {
        DisableOblivionItemPickupEvent(special);
        SetItemPropertyAllowAllDrop(special);
    }
    else if (thingid == 1178 || thingid == 1168)
        SetConsumablesWeaponCapacity(special, 255, 255);
}

int FieldItemFuncTable(int order)
{
    int items[] = {HotPotion, PotionItemDrop, NormalWeaponItemDrop, NormalArmorItemDrop,
        MoneyDrop, SomeGermDrop, WeaponItemDrop, ArmorItemDrop};

    return items[order % sizeof(items)];
}

int HotPotion(int wp)
{
    return PotionPickupRegist(CreateObject("RedPotion", wp));
}

int PotionItemDrop(int wp)
{
    return CheckPotionThingID(CreateObject(PotionList(Random(0, 15)), wp));
}

int NormalWeaponItemDrop(int wp)
{
    int weapon = CreateObject(WeaponList(Random(0, 7)), wp);

    CheckSpecialItem(weapon);
    return weapon;
}

int NormalArmorItemDrop(int wp)
{
    return CreateObject(ArmorList(Random(0, 17)), wp);
}

int MoneyDrop(int wp)
{
    string name[] = {"QuestGoldChest", "QuestGoldPile", "Gold"};
    int money = CreateObject(name[Random(0, 2)], wp);

    UnitStructSetGoldAmount(money, Random(500, 3000));
    return money;
}

int SomeGermDrop(int wp)
{
    string germs[] = {"Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Diamond"};

    return CreateObject(germs[Random(0, 5)], wp);
}

int WeaponItemDrop(int wp)
{
    int weapon = CreateObject(WeaponList(Random(0, 12)), wp);

    CheckSpecialItem(weapon);
    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    return weapon;
}

int ArmorItemDrop(int wp)
{
    int armor = CreateObject(ArmorList(Random(0, 17)), wp);

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return armor;
}

string PotionList(int index)
{
    string name[] = {
        "RedPotion", "CurePoisonPotion", "YellowPotion", "BlackPotion",
        "VampirismPotion", "Mushroom", "PoisonProtectPotion", "ShockProtectPotion",
        "FireProtectPotion", "HastePotion", "ShieldPotion", "InvulnerabilityPotion",
        "WhitePotion", "BluePotion", "InfravisionPotion", "InvisibilityPotion"
    };
    return name[index];
}

string WeaponList(int index)
{
    string name[] = {
        "GreatSword", "Longsword", "Sword", "MorningStar",
        "OgreAxe", "StaffWooden", "BattleAxe", "FanChakram",
        "RoundChakram", "WarHammer", "OblivionHalberd", "OblivionWierdling",
        "OblivionHeart"
    };
    return name[index];
}

string ArmorList(int index)
{
    string name[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots",
        "PlateLeggings", "MedievalCloak", "ChainCoif", "ChainLeggings",
        "ChainTunic", "SteelHelm", "LeatherArmbands", "LeatherArmor",
        "LeatherArmoredBoots", "LeatherBoots", "LeatherHelm", "LeatherLeggings",
        "MedievalPants", "MedievalShirt"
    };
    return name[index];
}

void strBuySoldier()
{
	int arr[30];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 1076109816; arr[1] = 1937785344; arr[2] = 541210663; arr[3] = 2080900996; arr[4] = 1111656459; arr[5] = 1338247240; arr[6] = 9044224; arr[7] = 76040273; arr[8] = 672073854; arr[9] = 83890720; 
	arr[10] = 69242425; arr[11] = 470319393; arr[12] = 9474062; arr[13] = 334757648; arr[14] = 2164236; arr[15] = 25105393; arr[16] = 67108864; arr[17] = 525820; arr[18] = 33038594; arr[19] = 1073774655; 
	arr[20] = 270532736; arr[21] = 67649671; arr[22] = 134221840; arr[23] = 138444808; arr[24] = 536379656; arr[25] = 10420226; arr[26] = 1618870288; arr[27] = 534773775; arr[28] = 2131750912; arr[29] = 1; 
	
	while(i < 30)
	{
		drawstrBuySoldier(arr[i], name);
		i ++;
	}
}

void drawstrBuySoldier(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(22);
		pos_y = GetWaypointY(22);
	}
	for (i = 1 ; i > 0 && count < 930 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 22);
		if (count % 82 == 81)
			MoveWaypoint(22, GetWaypointX(22) - 162.000000, GetWaypointY(22) + 2.000000);
		else
			MoveWaypoint(22, GetWaypointX(22) + 2.000000, GetWaypointY(22));
		count ++;
	}
	if (count >= 930)
	{
		count = 0;
		MoveWaypoint(22, pos_x, pos_y);
	}
}

void strGameBegin()
{
	int arr[18];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 270779038; arr[1] = 171990980; arr[2] = 135299361; arr[3] = 75780610; arr[4] = 1208500356; arr[5] = 34612258; arr[6] = 252428741; arr[7] = 19009665; arr[8] = 1157890130; arr[9] = 21236232; 
	arr[10] = 141312; arr[11] = 285149728; arr[12] = 675413522; arr[13] = 4752392; arr[14] = 270614792; arr[15] = 2097794; arr[16] = 134283018; arr[17] = 256; 
	while(i < 18)
	{
		drawstrGameBegin(arr[i], name);
		i ++;
	}
}

void drawstrGameBegin(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(13);
		pos_y = GetWaypointY(13);
	}
	for (i = 1 ; i > 0 && count < 558 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 13);
		if (count % 49 == 48)
			MoveWaypoint(13, GetWaypointX(13) - 96.000000, GetWaypointY(13) + 2.000000);
		else
			MoveWaypoint(13, GetWaypointX(13) + 2.000000, GetWaypointY(13));
		count ++;
	}
	if (count >= 558)
	{
		count = 0;
		MoveWaypoint(13, pos_x, pos_y);
	}
}

void strVictory()
{
	int arr[13];
	string name = "HealOrb";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
	{
		drawstrVictory(arr[i], name);
		i ++;
	}
}

void drawstrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(20);
		pos_y = GetWaypointY(20);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 20);
		if (count % 38 == 37)
			MoveWaypoint(20, GetWaypointX(20) - 74.000000, GetWaypointY(20) + 2.000000);
		else
			MoveWaypoint(20, GetWaypointX(20) + 2.000000, GetWaypointY(20));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(20, pos_x, pos_y);
	}
}

int PowerShortSword(float sX, float sY)
{
    int sword = CreateObjectAt("Sword", sX, sY);

    SetWeaponPropertiesDirect(sword, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_lightning3, ITEM_PROPERTY_venom3);
    return sword;
}

int PlaceHotPotion(float sX, float sY)
{
    int potion = CreateObjectAt("RedPotion", sX, sY);

    return PotionPickupRegist(potion);
}

void PlaceDefaultItem(int arg0)
{
    int location = arg0 & 0xffff, count = arg0 >> 0x10;

    if (count)
    {
        PowerShortSword(GetWaypointX(location), GetWaypointY(location));
        PlaceHotPotion(GetWaypointX(location) - 23.0, GetWaypointY(location) + 23.0);
        MoveWaypoint(location, GetWaypointX(location) + 23.0, GetWaypointY(location) + 23.0);
        FrameTimerWithArg(1, location | ((count - 1) << 0x10), PlaceDefaultItem);
    }
}

int DummyUnitCreate(string name, float locX, float locY)
{
    int unit = CreateObjectAt(name, locX, locY);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    ObjectOff(unit);
    Frozen(unit, 1);
    return unit;
}

void GreenSparkFx(float x, float y)
{
    int ptr = CreateObjectAt("MonsterGenerator", x, y);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void WarAbilityShopClassDesc()
{
    int idx = GetDirection(self);
    string abilityName[] = {":향상된 조심스럽게 걷기", ":불의고리", ":향상된 작살"};

    TellStoryUnitName("aa", "NoxDemo:BuyNox", abilityName[idx]);
    Raise(GetTrigger() + 1, abilityName[idx]);
    UniPrint(other, "MISSING.NoxScriptParse: " + abilityName[idx] + "능력을 구입하시겠어요? 7000천 골드 밖에 안해요!");
}

void WarAbilityShopClassTrade()
{
    int dlgRes = GetAnswer(self), plr = CheckPlayer();
    int aFlag = 2 << GetDirection(self), aName = ToInt(GetObjectZ(GetTrigger() + 1));

    if (plr < 0) return;
    if (dlgRes == 1)
    {
        if (player[plr + 10] & aFlag)
            UniPrint(other, "거래가 취소되었습니다. 당신은 이미 " + ToStr(aName) + "능력을 가졌습니다");
        else if (GetGold(other) >= 7000)
        {
            ChangeGold(other, -7000);
            player[plr + 10] = player[plr + 10] ^ aFlag;
            GreenSparkFx(GetObjectX(other), GetObjectY(other));
            UniPrint(other, "결제가 완료되었습니다. " + ToStr(aName) + " 능력을 가졌습니다 (7천골드 차감)");
        }
        else
        {
            UniChatMessage(self, "잔액이 부족합니다", 180);
            UniPrint(other, "거래가 취소되었습니다. 그것의 가격은 7천골드 입니다");
        }
    }
    else if (dlgRes == 2)
    {
        UniPrint(other, "MISSING: '아니오'를 선택하셨습니다. 다른 판매 품목을 보여드립니다");
        LookWithAngle(self, (GetDirection(self) + 1) % 3);
        WarAbilityShopClassDesc();
    }
}

void WarAbilityShopClassCreate(int location)
{
    int shop = DummyUnitCreate("Horrendous", GetWaypointX(41), GetWaypointY(41));

    CreateObjectAt("ImaginaryCaster", GetObjectX(shop), GetObjectY(shop));
    LookWithAngle(shop, 0);
    SetDialog(shop, "YESNO", WarAbilityShopClassDesc, WarAbilityShopClassTrade);
}