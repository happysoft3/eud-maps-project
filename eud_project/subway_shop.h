
#include "subway_popup.h"
#include "subway_player.h"
#include "subway_specialweapon.h"
#include "subway_mon.h"
#include "libs/fixtellstory.h"
#include "libs/networkRev.h"

void onPopupMessageChanged(int messageId)
{
    int ctx,*pMsg;
    QueryDialogCtx(&ctx,NULLPTR);
    queryPopMessage(&pMsg);
    string message = pMsg[messageId];
    GUISetWindowScrollListboxText(ctx, message, NULLPTR);
}

void deferredChangeDialogContext(int *pParams) //override
{
    int user=pParams[0];
    int message=pParams[1];

    FreeSmartMemEx(pParams);
    if (!CurrentHealth(user))
        return;

    if (user==GetHost())
    {
        onPopupMessageChanged(message);
        return;
    }
    ClientSetMemory(user, _CLIENT_OPTION_TYPE_OFF_, message);
}

void sendChangeDialogCtx(int user, int param)
{
    int params[]={
        user,
        param,
    };
    int *pMsg;

    AllocSmartMemEx(8, &pMsg);
    NoxDwordMemCopy(params, pMsg, sizeof(params));
    PushTimerQueue(1, pMsg, deferredChangeDialogContext);
}
int placingItemShop(short location, int descFn, int tradeFn){
    int s=DummyUnitCreateById(OBJ_RAT, LocationX(location),LocationY(location));
    SetDialog(s,"YESNO",descFn,tradeFn);
    StoryPic(s,"WarriorPic");
    LookWithAngle(s,192);
    return s;
}

int SetInvulnerabilityItem(int unit)
{
    int count = 0, inv = GetLastItem(unit);

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            count+=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void tradeInvincibleItem()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=5000)
    {
        ChangeGold(OTHER, -5000);
        int count = SetInvulnerabilityItem(OTHER);
        char buff[128];

        NoxSprintfString(buff, "%d개 아이템이 처리되었습니다", &count,1);
        UniPrint(OTHER,ReadStringAddressEx(buff));
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descInvincibleItem()
{
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "인벤토리 무적화");
}

void descAwardWindboost(){
    sendChangeDialogCtx(GetCaller(), GUI_DIALOG_MESSAGE_AWARD_FAST_MOVE);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "순보능력\n배우기");
}

void tradeAwardWindboost()
{
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=20000)
    {
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WINDBOOST))
        {
            UniPrint(OTHER, "이미 그 능력을 가졌습니다!");
            return;
        }
        ChangeGold(OTHER, -20000);
        PlayerClassSetFlag(pIndex,PLAYER_FLAG_WINDBOOST);
        GreenExplosion(GetObjectX(OTHER),GetObjectY(OTHER));
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descAwardBerserker(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_AWARD_BERSERKER);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "버저커차지\n배우기");
}
void tradeAwardBerser(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=20000){
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_BERSERKER_CHARGE))        {
            UniPrint(OTHER, "이미 그 능력을 가졌습니다!");
            return;        }
        ChangeGold(OTHER, -20000);
        PlayerClassSetFlag(pIndex,PLAYER_FLAG_BERSERKER_CHARGE);
        GreenExplosion(GetObjectX(OTHER),GetObjectY(OTHER));
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void descWolfRunSword(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_WOLF_RUN_SWORD);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "늑데개돌 서드");
}void tradeWolfRunsword(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=60000){
        ChangeGold(OTHER, -60000);
        CreateWolfSword(GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER,"무기를 구입하였고, 당신 아래에 무기가 있습니다");
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descMoneyExchanger(){
sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_MONEY_EXCHANGER);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "코인교환기");
}
int SellGerm(int inv)
{
    int thingId = GetUnitThingID(inv), pic;

    if (thingId >= OBJ_DIAMOND && thingId <= OBJ_RUBY)
    {
        Delete(inv);

        int pay[] = {1000, 5000, 10000};
        return pay[OBJ_RUBY - thingId];
    }
    else
        return 0;
}

int FindItemGerm(int holder)
{
    int inv = GetLastItem(holder), res = 0, nextInv;

    while (inv)
    {
        nextInv = GetPreviousItem(inv);
        res += SellGerm(inv);
        inv = nextInv;
    }
    return res;
}
void tradeMoneyExchanger(){
    if (GetAnswer(SELF)^1)
        return;
    int trdRes = FindItemGerm(OTHER);
    char msg[128];

    if (trdRes)
    {
        ChangeGold(OTHER, trdRes);
        NoxSprintfString(msg, "가지고 있던 모든 보석을 팔아서 %d골드를 추가했습니다", &trdRes, 1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
    else
        UniPrint(OTHER, "당신은 보석을 하나도 가지고 있지 않아요. 거래를 계속할 수 없습니다");
}
int placingMoneyExchanger(short location){
    int s=DummyUnitCreateById(OBJ_RAT, LocationX(location),LocationY(location));
    SetDialog(s,"YESNO",descMoneyExchanger,tradeMoneyExchanger);
    StoryPic(s,"WarriorPic");
    LookWithAngle(s,160);
}
void descThunderBoltSword(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_THUNDER_SWORD);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "백만벌트서드");
}void tradeThunderboltsword(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=60000){
        ChangeGold(OTHER, -60000);
        CreateThunderSword(GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER,"무기를 구입하였고, 당신 아래에 무기가 있습니다");
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descBackstepHammer(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_BACKSTEP_HAMMER);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "백스텝 엑스구입");
}void tradeBackstepHammer(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=60000){
        ChangeGold(OTHER, -60000);
        CreateBackstepAxe(GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER,"무기를 구입하였고, 당신 아래에 무기가 있습니다");
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descAllEnchantment(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_ALL_ENCHANTMENT);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "올엔첸트 구입");
}
void tradeAllEnchant(){
    if (GetAnswer(SELF)^1)
        return;
    if (GetGold(OTHER)>=45000){
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_ALL_BUFF))        {
            UniPrint(OTHER, "이미 그 능력을 가졌습니다!");
            return;        }
        ChangeGold(OTHER, -45000);
        PlayerClassSetFlag(pIndex,PLAYER_FLAG_ALL_BUFF);
        GreenExplosion(GetObjectX(OTHER),GetObjectY(OTHER));
        PlayerSetAllBuff(OTHER);
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}
void descToothOni(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_FLY_SWATTER);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "이빨오니 구입");
}
void tradeToothOni(){
    if (GetAnswer(SELF)^1)
        return;
    if (GetGold(OTHER)>=60000){
        int pIndex=GetPlayerIndex(OTHER);
        if (pIndex<0)
            return;
        int *credata;
        GetCreature1DataPtr(pIndex, &credata);
        if (credata[0])        {
            UniPrint(OTHER, "이미 이빨오니를 소유중입니다");
            return;        }
        ChangeGold(OTHER, -60000);
        SummonBodyGuard(GetCaller(),credata,CreateThoothOni);
        return;
    }
    UniPrint(OTHER, "골드가 부족합니다");
}
// void descBearGrylls(){
//     sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_BEAR_GRYLLS);
//     TellStoryUnitName("null", "MainBG.wnd:Loading", "베어그릴스 구입");
// }
// void tradeBearGrylls(){
//     if (GetAnswer(SELF)^1)
//         return;
//     if (GetGold(OTHER)>=61000){
//         int pIndex=GetPlayerIndex(OTHER);
//         if (pIndex<0)
//             return;
//         int *credata;
//         GetCreature2DataPtr(pIndex, &credata);
//         if (credata[0])        {
//             UniPrint(OTHER, "이미 베어그릴스를 소유중입니다");
//             return;        }
//         ChangeGold(OTHER, -61000);
//         SummonBodyGuard(GetCaller(),credata,CreateBearGrylls);
//         return;
//     }
//     UniPrint(OTHER, "골드가 부족합니다");
// }
// void descPesticide(){
//     sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_PESTICIDE);
//     TellStoryUnitName("null", "MainBG.wnd:Loading", "살충제 구입");
// }
// void tradePesticide(){
//     if (GetAnswer(SELF)^1)
//         return;
//     if (GetGold(OTHER)>=60000){
//         int pIndex=GetPlayerIndex(OTHER);
//         if (pIndex<0)
//             return;
//         int *credata;
//         GetCreature3DataPtr(pIndex, &credata);
//         if (credata[0])        {
//             UniPrint(OTHER, "이미 살충제를 소유중입니다");
//             return;        }
//         ChangeGold(OTHER, -60000);
//         SummonBodyGuard(GetCaller(),credata,CreatePesticide);
//         return;
//     }
//     UniPrint(OTHER, "골드가 부족합니다");
// }
// void descBlueGiant(){
//     sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_BLUE_GIANT);
//     TellStoryUnitName("null", "MainBG.wnd:Loading", "파랑거인 구입");
// }
// void tradeBlueGiant(){
//     if (GetAnswer(SELF)^1)
//         return;
//     if (GetGold(OTHER)>=66000){
//         int pIndex=GetPlayerIndex(OTHER);
//         if (pIndex<0)
//             return;
//         int *credata;
//         GetCreature4DataPtr(pIndex, &credata);
//         if (credata[0])        {
//             UniPrint(OTHER, "이미 파랑거인를 소유중입니다");
//             return;        }
//         ChangeGold(OTHER, -66000);
//         SummonBodyGuard(GetCaller(),credata,CreateBlueGiant);
//         return;
//     }
//     UniPrint(OTHER, "골드가 부족합니다");
// }
// void descStelsShip(){
//     sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_STELS_SHIP);
//     TellStoryUnitName("null", "MainBG.wnd:Loading", "스텔스십 구입");
// }
// void tradeStelsShip(){
//     if (GetAnswer(SELF)^1)
//         return;
//     if (GetGold(OTHER)>=77000){
//         int pIndex=GetPlayerIndex(OTHER);
//         if (pIndex<0)
//             return;
//         int *credata;
//         GetCreature5DataPtr(pIndex, &credata);
//         if (credata[0])        {
//             UniPrint(OTHER, "이미 스텔스십를 소유중입니다");
//             return;        }
//         ChangeGold(OTHER, -77000);
//         SummonBodyGuard(GetCaller(),credata,CreateBlueGiant);
//         return;
//     }
//     UniPrint(OTHER, "골드가 부족합니다");
// }

int placingCreatShop(short monId, short location, int descFn, int tradeFn){
    int s=CreateObjectById(monId, LocationX(location),LocationY(location));

    Frozen(s,TRUE);
    SetOwner(GetHost(),s);
    SetDialog(s,"YESNO",descFn,tradeFn);
    StoryPic(s,"WarriorPic");
    LookWithAngle(s,64);
    return s;
}

void descIceCrystalAxe(){
    sendChangeDialogCtx(GetCaller(),GUI_DIALOG_MESSAGE_ICE_CRYSTAL_AXE);
    TellStoryUnitName("null", "MainBG.wnd:Loading", "아이스 엑스구입");
}void tradeIceCrystalAxe(){
    if (GetAnswer(SELF)^1)
        return;

    if (GetGold(OTHER)>=64000){
        ChangeGold(OTHER, -64000);
        PlacingIceCrystalAxe(GetObjectX(OTHER),GetObjectY(OTHER));
        UniPrint(OTHER,"무기를 구입하였고, 당신 아래에 무기가 있습니다");
        return;    }
    UniPrint(OTHER, "골드가 부족합니다");
}

void InitializeShopSystem(){
    placingItemShop(54,descInvincibleItem,tradeInvincibleItem);
    placingItemShop(55,descAwardWindboost,tradeAwardWindboost);
    placingItemShop(67,descAwardBerserker,tradeAwardBerser);
    placingItemShop(68,descWolfRunSword,tradeWolfRunsword);
    placingItemShop(73,descThunderBoltSword,tradeThunderboltsword);
    placingItemShop(53,descBackstepHammer,tradeBackstepHammer);
    placingItemShop(70,descAllEnchantment,tradeAllEnchant);
    placingItemShop(69,descIceCrystalAxe,tradeIceCrystalAxe);
    // placingCreatShop(OBJ_WOUNDED_APPRENTICE,131,descFlySwatter,tradeFlySwatter);
    placingCreatShop(OBJ_WOUNDED_WARRIOR,75,descToothOni,tradeToothOni);
    // placingCreatShop(OBJ_WOUNDED_CONJURER,132,descBearGrylls,tradeBearGrylls);
    // placingCreatShop(OBJ_HECUBAH,134,descBlueGiant,tradeBlueGiant);
    // placingCreatShop(OBJ_EVIL_CHERUB,136,descStelsShip,tradeStelsShip);
    placingMoneyExchanger(74);
    InitializePopupMessages();
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[1])
    {
        onPopupMessageChanged(type[1]);
        type[1]=0;
    }
    if (type[0])
    {
        type[1]=type[0];
        type[0]=0;
    }
    FrameTimer(1, ClientProcLoop);
}

