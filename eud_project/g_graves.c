
#include "g_graves_resource.h"
#include "libs\define.h"
#include "libs\unitstruct.h"
#include "libs\printutil.h"
#include "libs\mathlab.h"
#include "libs\waypoint.h"
#include "libs\wallutil.h"

#include "libs\fxeffect.h"
#include "libs\sound_define.h"
#include "libs/indexloop.h"
#include "libs/networkRev.h"

int PIC, LastUnitID = 4176;
int FIST_TRP[6];
int ARR_TP[25];
int ARR_TRP2[15];
int DROT[3];
int DROT2[4];
int SQ_ROW[24];
int STY_WLL[12];
int UNDER_ROT[36];


int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, LastUnitID);
        }
    }
    LastUnitID ++;

    return unitMover;
}

void InitArrowRows()
{
    int k;

    for (k = 15 ; k ; Nop(k --))
        ARR_TRP2[k - 1] = Object("ArrowTrap" + IntToString(k));
    TombLeftArrowRow();
    TombRightArrowRow();
    TombDownArrowRow();
}

void InitFistTraps(int max)
{
    int k;

    for (k = 0 ; k < max ; Nop(k ++))
    {
        LookWithAngle(Object("FistPlate" + IntToString(k + 1)), k);
        FIST_TRP[k] = Object("FistTrap" + IntToString(k + 1));
    }
}

void InitArrowTraps()
{
    int k;

    for (k = 0 ; k < 7 ; Nop(k ++))
    {
        if (k < 5)
        {
            ARR_TP[k] = Object("WestGroup1Row" + IntToString(k + 1));
            ARR_TP[k + 5] = Object("WestGroup2Row" + IntToString(k + 1));
        }
        if (k < 4)
        {
            ARR_TP[k + 17] = Object("WestGroup4Row" + IntToString(k + 1));
            ARR_TP[k + 21] = Object("WestGroup5Row" + IntToString(k + 1));
        }
        ARR_TP[k + 10] = Object("WestGroup3Row" + IntToString(k + 1));
    }
}

void MagicHurricane()
{
    int unit = CreateObject("Maiden", 8);
    CreateObject("Maiden", 133);
    Frozen(unit, 1);
    Frozen(unit + 1, 1);
    SetDialog(unit, "NORMAL", WellOfRestoration, DummyFunction);
    SetDialog(unit + 1, "NORMAL", WellOfRestoration, DummyFunction);
}

void DummyFunction()
{
    return;
}

void MapSignInit()
{
    RegistSignMessage(Object("MapSign1"), "세븐일레븐_ 공동묘지 점");
    RegistSignMessage(Object("MapSign2"), "인벤토리 무적화 비콘입니다. 무기/갑옷/지팡이 3개 분류로 나뉘어져 있어요");
    FrameTimer(2, PlayerGlyphScanStart);
}

void MapInitialize()
{
    MusicEvent();
    PIC = Random(0, 1);
    InitArrowRows();
    InitFistTraps(6);
    InitArrowTraps();

    FrameTimer(1, DelayInit);
    FrameTimer(2, InitSentryWalls);
    FrameTimer(10, InitDestroyRots);
    FrameTimer(10, MovingGenerators);
    FrameTimer(30, InitSquareRows);
}

void DelayInit()
{
    InitFireway();
    CreateObject("RedPotion", 274);
    CreateObject("SilverKey", 285);
    CreateObject("SilverKey", 113);
    CreateObject("SilverKey", 393);
    CreateObject("SilverKey", 131);
    FrameTimer(1, MapSignInit);
    FrameTimer(100, MagicHurricane);
    //StartDrawRedRing(GetWaypointX(133), GetWaypointY(133));
    //StartDrawRedRing(GetWaypointX(8), GetWaypointY(8));
}

void OpenEastWalls()
{
    int count;

    ObjectOff(self);
    UniPrint(other, "벽 하나가 열렸습니다");
    if (!count)
    {
        count = 1;
        WallOpen(Wall(65, 35));
        WallOpen(Wall(66, 36));
        WallOpen(Wall(67, 37));
    }
    else
    {
        WallOpen(Wall(63, 37));
        WallOpen(Wall(64, 38));
        WallOpen(Wall(65, 39));
    }
}

void ActivateWestRowsGroup1()
{
    ObjectOn(ARR_TP[0]);
    ObjectOn(ARR_TP[1]);
    ObjectOn(ARR_TP[2]);
    ObjectOn(ARR_TP[3]);
    ObjectOn(ARR_TP[4]);
    FrameTimerWithArg(1, 0 | 0x500, DisableArrowTraps);
}

void ActivateWestRowsGroup2()
{
    ObjectOn(ARR_TP[5]);
    ObjectOn(ARR_TP[6]);
    ObjectOn(ARR_TP[7]);
    ObjectOn(ARR_TP[8]);
    ObjectOn(ARR_TP[9]);
    FrameTimerWithArg(1, 5 | 0x500, DisableArrowTraps);
}

void ActivateWestRowsGroup3()
{
    ObjectOn(ARR_TP[10]);
    ObjectOn(ARR_TP[11]);
    ObjectOn(ARR_TP[12]);
    ObjectOn(ARR_TP[13]);
    ObjectOn(ARR_TP[14]);
    ObjectOn(ARR_TP[15]);
    ObjectOn(ARR_TP[16]);
    FrameTimerWithArg(1, 10 | (0x7 << 8), DisableArrowTraps);
}

void DisableArrowTraps(int ptr)
{
    int base = ptr & 0xff;
    int max = ptr >> 8;
    int k;

    for (k = 0 ; k < max ; k ++)
        ObjectOff(ARR_TP[k + base]);
}

void StartCryptBlocks()
{
    int unit[2];

    ObjectOff(self);
    if (!unit[0])
    {
        unit[0] = Object("CryptBlock1");
        unit[1] = Object("CryptBlock2");
        Move(unit[0], 10);
        Move(unit[1], 11);
    }
}

void ActivateMonoBlock()
{
    int flag;
    ObjectOff(self);
    if (!flag)
    {
        Move(Object("MonoBlock"), 35);
    }
    else
    {
        Move(Object("MonoBlock"), 38);
    }
    AudioEvent("SpikeBlockMove", 35);
    flag = (flag + 1) % 2;
    SecondTimerWithArg(5, GetTrigger(), DelayEnableUnit);
}

void DelayEnableUnit(int unit)
{
    ObjectOn(unit);
}

void ActivateDropFist()
{
    int cur = GetDirection(self);
    ObjectOff(self);
    ObjectOff(FIST_TRP[cur]);
    CastSpellObjectObject("SPELL_FIST", self, self);
    FrameTimerWithArg(120, GetTrigger(), ResetFistTrap);
}

void ResetFistTrap(int ptr)
{
    int cur = GetDirection(ptr);
    ObjectOn(ptr);
    ObjectOn(FIST_TRP[cur]);
    MoveWaypoint(16, GetObjectX(ptr), GetObjectY(ptr));
    AudioEvent("TriggerReleased", 16);
}

void OpenGenWalls()
{
    ObjectOff(self);
    WallOpen(Wall(183, 79));
    WallOpen(Wall(184, 80));
    WallOpen(Wall(185, 79));

    WallOpen(Wall(177, 85));
    WallOpen(Wall(177, 87));
    WallOpen(Wall(178, 86));

    WallOpen(Wall(192, 86));
    WallOpen(Wall(191, 87));
    WallOpen(Wall(192, 88));

    WallOpen(Wall(184, 94));
    WallOpen(Wall(185, 93));
    WallOpen(Wall(186, 94));
}

void WellOfRestoration()
{
    if (!HasEnchant(other, "ENCHANT_DETECTING"))
    {
        UniPrint(other, "이 우물이 잠시동안 당신의 체력을 지속적으로 회복시켜 줄 겁니다");
        Enchant(other, "ENCHANT_DETECTING", 12.0);
        MoveWaypoint(16, GetObjectX(other), GetObjectY(other));
        AudioEvent("LongBellsDown", 16);
        AudioEvent("RestoreHealth", 16);
        HealUnit(GetCaller());
    }
}

void HealUnit(int unit)
{
    if (CurrentHealth(unit) && HasEnchant(unit, "ENCHANT_DETECTING"))
    {
        RestoreHealth(unit, 3);
        Effect("GREATER_HEAL", GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit) - 100.0);
        FrameTimerWithArg(3, unit, HealUnit);
    }
}

void DrawFistTrapBottom(int ptr)
{
	int count = GetDirection(ptr), unit;

	if (IsObjectOn(ptr))
	{
		if (count)
		{
			unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr), GetObjectY(ptr));
			Enchant(unit, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
			LookWithAngle(ptr, count - 1);
			FrameTimerWithArg(1, ptr, DrawFistTrapBottom);
		}
		else
			Delete(ptr);
	}
}

void StartDrawRedRing(float x, float y)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", x, y);

    LookWithAngle(unit, 12);
    //FrameTimerWithArg(1, unit, DrawFistTrapBottom);
}

void MovingSkull(int flag)
{
    int rep = flag & 0xff;
    int wp = (flag >> 8) & 0xff;
    int go = (flag >> 16) & 0xff;
    int unit;
    if (rep)
    {
        unit = CreateObject("SpinningSkull", wp);
        Frozen(unit, 1);
        CreateMoverFix(unit, go, 22.0);
        FrameTimerWithArg(10, flag - 1, MovingSkull);
    }
}

void ShotSouthArrowPart1()
{
    ObjectOn(ARR_TRP2[0]);
    ObjectOn(ARR_TRP2[1]);
    ObjectOn(ARR_TRP2[2]);
    ObjectOn(ARR_TRP2[3]);
    FrameTimerWithArg(1, 0 | 0x400, DisableSouthArrowTraps);
}

void ShotSouthArrowPart2()
{
    ObjectOn(ARR_TRP2[4]);
    ObjectOn(ARR_TRP2[5]);
    ObjectOn(ARR_TRP2[6]);
    ObjectOn(ARR_TRP2[7]);
    ObjectOn(ARR_TRP2[8]);
    FrameTimerWithArg(1, 4 | 0x500, DisableSouthArrowTraps);
}

void ShotSouthArrowPart3()
{
    ObjectOn(ARR_TRP2[9]);
    ObjectOn(ARR_TRP2[10]);
    ObjectOn(ARR_TRP2[11]);
    ObjectOn(ARR_TRP2[12]);
    ObjectOn(ARR_TRP2[13]);
    ObjectOn(ARR_TRP2[14]);
    FrameTimerWithArg(1, 9 | 0x600, DisableSouthArrowTraps);
}

void DisableSouthArrowTraps(int ptr)
{
    int max = ptr >> 8;
    int base = ptr & 0xff;
    int k;

    for (k = 0 ; k < max ; k ++)
        ObjectOff(ARR_TRP2[base + k]);
}

void InitDestroyRots()
{
    DROT[0] = Object("DestroyGenRot1");
    DROT[1] = Object("DestroyGenRot2");
    DROT[2] = Object("DestroyGenRot3");
    Frozen(DROT[0], 1);
    Frozen(DROT[1], 1);
    Frozen(DROT[2], 1);
    DROT2[0] = Object("SouthGenRot1");
    DROT2[1] = Object("SouthGenRot2");
    DROT2[2] = Object("SouthGenRot3");
    DROT2[3] = Object("SouthGenRot4");
    Frozen(DROT2[0], 1);
    Frozen(DROT2[1], 1);
    Frozen(DROT2[2], 1);
    Frozen(DROT2[3], 1);
}

void ActivateDestroyGenRots()
{
    if (!GetDirection(DROT[0]))
    {
        UniPrint(other, "함정이 작동되었습니다");
        LookWithAngle(DROT[0], 1);
        FrameTimer(10, GoDestroyGenRots);
    }
}

void GoDestroyGenRots()
{
    if (GetObjectX(DROT[0]) <= 3990.0)
        MoveObject(DROT[0], GetObjectX(DROT[0]) + 2.0, GetObjectY(DROT[0]) - 2.0);
    if (GetObjectX(DROT[1]) <= 4036.0)
        MoveObject(DROT[1], GetObjectX(DROT[1]) + 2.0, GetObjectY(DROT[1]) - 2.0);
    if (GetObjectX(DROT[2]) <= 4082.0)
    {
        MoveObject(DROT[2], GetObjectX(DROT[2]) + 2.0, GetObjectY(DROT[2]) - 2.0);
        FrameTimer(1, GoDestroyGenRots);
    }
    else
        FrameTimer(1, GoDestroyRot2);
}

void GoDestroyRot2() //4013, 4036
{
    if (GetObjectX(DROT[1]) >= 4013.0)
        MoveObject(DROT[1], GetObjectX(DROT[1]) - 1.0, GetObjectY(DROT[1]) - 1.0);
    if (GetObjectX(DROT[2]) >= 4036.0)
    {
        MoveObject(DROT[2], GetObjectX(DROT[2]) - 2.0, GetObjectY(DROT[2]) - 2.0);
        FrameTimer(1, GoDestroyRot2);
    }
    else
        FrameTimer(1, GoDestroyGenRot3);
}

void GoDestroyGenRot3()
{
    if (GetObjectX(DROT[0]) >= 3576.0)
    {
        MoveObject(DROT[0], GetObjectX(DROT[0]) - 2.0, GetObjectY(DROT[0]) - 2.0);
        MoveObject(DROT[1], GetObjectX(DROT[1]) - 2.0, GetObjectY(DROT[1]) - 2.0);
        MoveObject(DROT[2], GetObjectX(DROT[2]) - 2.0, GetObjectY(DROT[2]) - 2.0);
        FrameTimer(1, GoDestroyGenRot3);
    }
    else
        FrameTimer(1, DestroyGenRotsGoHome);
}

void DestroyGenRotsGoHome()
{
    if (GetObjectY(DROT[0]) <= 4565.0)
        MoveObject(DROT[0], GetObjectX(DROT[0]) + 2.0, GetObjectY(DROT[0]) + 2.0);
    if (GetObjectY(DROT[1]) <= 4565.0)
        MoveObject(DROT[1], GetObjectX(DROT[1]) + 2.0, GetObjectY(DROT[1]) + 2.0);
    if (GetObjectY(DROT[2]) <= 4565.0)
    {
        MoveObject(DROT[2], GetObjectX(DROT[2]) + 2.0, GetObjectY(DROT[2]) + 2.0);
        FrameTimer(1, DestroyGenRotsGoHome);
    }
    else
    {
        Move(DROT[0], 5);
        Move(DROT[1], 6);
        Move(DROT[2], 7);
        FrameTimer(75, StandByDestroyGenRots);
    }
}

void StandByDestroyGenRots()
{
    if (Distance(GetObjectX(DROT[0]), GetObjectY(DROT[0]), LocationX(5), LocationY(5)) < 30.0)
        LookWithAngle(DROT[0], 0);
}

void MovingGenerators()
{
    Move(Object("GenGrid01Generator01"), 32);
    Move(Object("GenGrid01Generator02"), 33);
}

void GenGrid01Back01Destroy()
{
    ObjectOff(Object("HecubahGenMover1"));
}

void GenGrid01Back02Destroy()
{
    ObjectOff(Object("HecubahGenMover2"));
}

void DestroyGenByRots()
{
    if (CurrentHealth(self) && IsCaller(DROT[0]))
    {
        Damage(self, 0, 999, 14);
    }
}

void StartMovingFiretrapTowerGens()
{
    int exec;

    if (GetTrigger())
    {
        ObjectOff(self);
    }
    if (!exec)
    {
        exec = FireGenPtr(Object("FireTrapMovingGen1"), 0);
        FireGenPtr(Object("FireTrapMovingGen2"), 1);
        ObjectOn(FireGenPtr(0, 0));
        ObjectOn(FireGenPtr(0, 1));
        Move(FireGenPtr(0, 0), 129);
        Move(FireGenPtr(0, 1), 127);
    }
}

int FireGenPtr(int num, int idx)
{
    int arr[2];

    if (!arr[idx])
    {
        arr[idx] = num;
    }
    return arr[idx];
}

void FiretowerGenDestroy()
{
    if (IsTrigger(FireGenPtr(0, 0)))
        ObjectOff(Object("FiretrapGenMover1"));
    else if (IsTrigger(FireGenPtr(0, 1)))
        ObjectOff(Object("FiretrapGenMover2"));
}

void shotArrowTower()
{
    int tower, unit;

    if (!tower) tower = Object("ArrowTower");

    if (CurrentHealth(OTHER))
    {
        CreateObjectAtEx("OgreShuriken", GetObjectX(tower) - UnitRatioX(tower, OTHER, 32.0), GetObjectY(tower) - UnitRatioY(tower, OTHER, 32.0), &unit);
        CreateObjectAtEx("WeakFireball", GetObjectX(unit), GetObjectY(unit), NULLPTR);
        PlaySoundAround(unit, SOUND_FireGrate);
        PushObject(unit, -40.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PushObject(unit + 1, -40.0, GetObjectX(OTHER), GetObjectY(OTHER));
    }
}

void RemoveGenWalls()
{
    ObjectOff(self);
    if (PIC)
    {
        WallOpen(Wall(99, 129));
        WallOpen(Wall(100, 130));
        WallOpen(Wall(101, 131));
        WallOpen(Wall(102, 132));
        WallOpen(Wall(103, 133));
        WallOpen(Wall(104, 132));
        WallOpen(Wall(105, 131));
        WallOpen(Wall(106, 130));
        WallOpen(Wall(107, 129));
        PIC = 0;
    }
    else
    {
        WallOpen(Wall(99, 141));
        WallOpen(Wall(100, 140));
        WallOpen(Wall(101, 139));
        WallOpen(Wall(102, 138));
        WallOpen(Wall(103, 137));
        WallOpen(Wall(104, 138));
        WallOpen(Wall(105, 139));
        WallOpen(Wall(106, 140));
        WallOpen(Wall(107, 141));
        PIC = 1;
    }
}

void ActivateWestRowsGroup4()
{
    ObjectOn(ARR_TP[17]);
    ObjectOn(ARR_TP[18]);
    ObjectOn(ARR_TP[19]);
    ObjectOn(ARR_TP[20]);
    FrameTimerWithArg(1, 17 | 0x400, DisableArrowTraps);
}

void ActivateWestRowsGroup5()
{
    ObjectOn(ARR_TP[21]);
    ObjectOn(ARR_TP[22]);
    ObjectOn(ARR_TP[23]);
    ObjectOn(ARR_TP[24]);
    FrameTimerWithArg(1, 21 | 0x400, DisableArrowTraps);
}

void ActivateRotSouthRows()
{
    int k;
    if (!GetDirection(DROT2[0]))
    {
        for (k = 3 ; k >= 0 ; k --)
            Move(DROT2[k], Waypoint("SouthGenRotWP" + IntToString(k + 1)));
        LookWithAngle(DROT2[0], 1);
        FrameTimer(210, TurnSouthRots);
    }
}

void TurnSouthRots()
{
    int k;

    for (k = 3 ; k >= 0 ; k --)
        Move(DROT2[k], Waypoint("SouthGenRotBack" + IntToString(k + 1)));
    FrameTimerWithArg(210, DROT2[0], ResetSouthRow);
}

void ResetSouthRow(int unit)
{
    LookWithAngle(unit, 0);
}

void InitSquareRows()
{
    int k;

    for (k = 11 ; k >= 0 ; k --)
    {
        SQ_ROW[k] = Object("SquareBlock" + IntToString(k + 1));
        SQ_ROW[k + 12] = Waypoint("squareGo" + IntToString(k + 1));
    }
    SimpleRow();
    AnnexRow();
    FrameTimer(200, ControlSquareBlocks);
}

void ControlSquareBlocks()
{
    int k;

    for (k = 11 ; k >= 0 ; k --)
        Move(SQ_ROW[k], SQ_ROW[k + 12]);
    FrameTimer(240, ControlSquareBlocks);
}

int SimpleRow()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("IronBlock", 89);
        CreateObject("IronBlock", 75);
        CreateObject("IronBlock", 74);
    }
    return ptr;
}

void ActivateSouthRows()
{
    int ptr;

    ObjectOff(self);
    if (!ptr)
    {
        ptr = SimpleRow();
        LookWithAngle(ptr, 120);
        LookWithAngle(ptr + 1, 119);
        LookWithAngle(ptr + 2, 117);
        CreateMoverFix(ptr, 0, 15.0);
        CreateMoverFix(ptr + 1, 0, 15.0);
        CreateMoverFix(ptr + 2, 0, 15.0);
        FrameTimerWithArg(1, ptr, DelayMoveUnit);
        //89,75,74
    }
}       

void DelayMoveUnit(int unit)
{
    Move(unit, GetDirection(unit));
    Move(unit + 1, GetDirection(unit + 1));
    Move(unit + 2, GetDirection(unit + 2));
}

int AnnexRow()
{
    int ptr;

    if (!ptr)
    {
        ptr = CreateObject("SpikeBlock", 106);
        CreateObject("SpikeBlock", 107);
        CreateObject("SpikeBlock", 108);
        CreateObject("SpikeBlock", 109);
        CreateObject("SpikeBlock", 110);
        CreateMoverFix(ptr, 0, 20.0);
        CreateMoverFix(ptr + 1, 0, 20.0);
        CreateMoverFix(ptr + 2, 0, 20.0);
        CreateMoverFix(ptr + 3, 0, 20.0);
        CreateMoverFix(ptr + 4, 0, 20.0);
    }
    return ptr;
}

void ActivateBlockGroup01()
{
    if (!GetDirection(AnnexRow()))
    {
        UniPrint(other, "동력장치가 작동되었습니다");
        LookWithAngle(AnnexRow(), 1);
        FrameTimer(10, ControlSouthRowsSR);
    }
}

void ControlSouthRowsSR()
{
    int ptr = AnnexRow();
    int wp = GetDirection(ptr + 1) * 5;
    Move(ptr, 90 + wp);
    Move(ptr + 1, 91 + wp);
    Move(ptr + 2, 92 + wp);
    Move(ptr + 3, 93 + wp);
    Move(ptr + 4, 94 + wp);
    LookWithAngle(ptr + 2, wp);
    LookWithAngle(ptr + 1, (GetDirection(ptr + 1) + 1) % 2);
    FrameTimerWithArg(1, AnnexRow(), ResetSR);
}

void ResetSR(int ptr)
{
    int wp = GetDirection(ptr + 2);
    if (Distance(GetObjectX(ptr + 1), GetObjectY(ptr + 1), LocationX(102 + wp), LocationY(102 + wp)) > 20.0)
        FrameTimerWithArg(1, ptr, ResetSR);
    else
        LookWithAngle(ptr, 0);
}

void setBoundarySentry()
{
    int trap;
    int ptr;
    ObjectOff(self);
    if (!ptr)
    {
        trap = Object("SouthSentry");
        Move(trap, 115);
        ObjectOn(trap);
        TempData(ptr);
    }
}

int TempData(int num)
{
    int data;
    if (num)
        data = num;
    return data;
}

void InitSentryWalls()
{
    int k;

    for (k = 0 ; k < 12 ; k ++)
    {
        STY_WLL[k] = Wall(238 - k, 34 + k);
        WallOpen(STY_WLL[k]);
    }
}

void EnableSentryWallsRow1()
{
    SafeWalls(0);
}

void EnableSentryWallsRow2()
{
    SafeWalls(1);
}

void EnableSentryWallsRow3()
{
    SafeWalls(2);
}

void SafeWalls(int num)
{
    int k;

    for (k = 11 ; k >= 0 ; k --)
    {
        if ((k / 4) == num)
            WallClose(STY_WLL[k]);
        else
            WallOpen(STY_WLL[k]);
    }
}

void EastGenWallOpen1()
{
    ObjectOff(self);
    WallOpen(Wall(230, 16));
    OpenSentryGenWalls();
}

void EastGenWallOpen2()
{
    ObjectOff(self);
    WallOpen(Wall(237, 23));
    OpenSentryGenWalls();
}

void OpenSentryGenWalls()
{
    int count;

    count ++;
    if (count == 2)
    {
        WallOpen(Wall(231, 17));
        WallOpen(Wall(232, 18));
        WallOpen(Wall(233, 19));
        WallOpen(Wall(234, 20));
        WallOpen(Wall(235, 21));
        WallOpen(Wall(236, 22));
    }
}

void DisableBoundaryTrap()
{
    ObjectOff(self);
    ObjectOff(TempData(0));
    ObjectOff(Object("SouthSentry"));
    WallOpen(Wall(224, 50));
    WallOpen(Wall(225, 51));
    WallOpen(Wall(226, 52));
    WallOpen(Wall(228, 52));
    WallOpen(Wall(229, 51));
    WallOpen(Wall(230, 50));
}

void InitUnderFoot()
{
    int k;
    for (k = 5 ; k >= 0 ; k --)
    {
        UNDER_ROT[k] = Object("UnderRightRow" + IntToString(k + 1));
        UNDER_ROT[k + 6] = Waypoint("UnderRightGo" + IntToString(k + 1));
        UNDER_ROT[k + 12] = Waypoint("UnderRightHome" + IntToString(k + 1));
        UNDER_ROT[k + 18] = Object("UnderLeftRow" + IntToString(k + 1));
        UNDER_ROT[k + 24] = Waypoint("UnderLeftGo" + IntToString(k + 1));
        UNDER_ROT[k + 30] = Waypoint("UnderLeftHome" + IntToString(k + 1));
    }
    LookWithAngle(UNDER_ROT[0], 0);
}

void UnderfootToggleRots()
{
    int wp = GetDirection(UNDER_ROT[0]) * 6;
    int k;

    for (k = 5 ; k >= 0 ; k --)
    {
        Move(UNDER_ROT[k], UNDER_ROT[wp + k + 6]);
        Move(UNDER_ROT[k + 18], UNDER_ROT[wp + k + 24]);
    }
    LookWithAngle(UNDER_ROT[0], (GetDirection(UNDER_ROT[0]) + 1) % 2);
    FrameTimer(90, UnderfootToggleRots);
}

void OpenHiddenGen()
{
    ObjectOff(self);
    WallOpen(Wall(43, 193));
    WallOpen(Wall(42, 194));
}

void StartFireway()
{
    ObjectOff(self);
    InitUnderFoot();
    FrameTimer(10, UnderfootToggleRots);
    FrameTimer(100, FirewalkTrap);
}

void SummonCreature()
{
    float xpos = GetObjectX(OTHER), ypos = GetObjectY(OTHER);

    Delete(OTHER);
    int unit = CreateObjectAt("WeirdlingBeast", xpos, ypos);
    SetOwner(SELF, unit);
    LookAtObject(unit, SELF);
    LookWithAngle(unit, GetDirection(unit) + 128);
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit) + UnitAngleCos(unit, 20.0), GetObjectY(unit) + UnitAngleSin(unit, 20.0), 450.0);
    SetCallback(unit, 3, ShotWeapon);
    DeleteObjectTimer(unit, 1);
    ObjectOff(SELF);
    FrameTimerWithArg(60, GetTrigger(), MissileTowerCooldown);
}

void MissileTowerCooldown(int unit)
{
    if (CurrentHealth(unit))
    {
        ObjectOn(unit);
    }
}

void ShotWeapon()
{
    int mis = CreateObjectAt("DeathBallFragment", GetObjectX(self) - UnitRatioX(self, other, 20.0), GetObjectY(self) - UnitRatioY(self, other, 20.0));
    SetOwner(self, mis);
    PushObject(mis, -20.0, GetObjectX(other), GetObjectY(other));
    DeleteObjectTimer(mis, 60);
}

void RemoveExitWalls()
{
    WallOpen(Wall(33, 133));
    WallOpen(Wall(34, 134));
    WallOpen(Wall(33, 135));
    WallOpen(Wall(32, 136));
    WallOpen(Wall(31, 137));
    WallOpen(Wall(30, 138));
    WallOpen(Wall(29, 139));
    WallOpen(Wall(28, 140));
    WallOpen(Wall(27, 139));
}

void NotCrashStaff()
{
    int inv = GetLastItem(other), res = 0;

    while (inv)
    {
        if (HasEnchant(inv, "ENCHANT_INVULNERABLE"))
            1;
        else if (HasClass(inv, "WAND"))
        {
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
            res +=1;
        }
        inv = GetPreviousItem(inv);
    }
    if (res)
    {
        UniPrint(other, "처리결과: 총 " + IntToString(res) + " 개의 지팡이 아이템을 무적화 했습니다");
    }
}

void NotCrashWeapon()
{
    int inv = GetLastItem(other), res = 0;

    while (inv)
    {
        if (HasEnchant(inv, "ENCHANT_INVULNERABLE"))
            1;
        else if (HasClass(inv, "WEAPON"))
        {
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
            res +=1;
        }
        inv = GetPreviousItem(inv);
    }
    if (res)
    {
        UniPrint(other, "처리결과: 총 " + IntToString(res) + " 개의 무기 아이템을 무적화 했습니다");
    }
}

void NotCrashArmor()
{
    int inv = GetLastItem(other), res = 0;

    while (inv)
    {
        if (HasEnchant(inv, "ENCHANT_INVULNERABLE"))
            1;
        else if (HasClass(inv, "ARMOR"))
        {
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
            res ++;
        }
        inv = GetPreviousItem(inv);
    }
    if (res)
    {
        UniPrint(other, "처리결과: 총 " + IntToString(res) + " 개의 갑옷 아이템을 무적화 했습니다");
    }
}

string FlameTable(int num)
{
    string table[] = {"SmallFlame", "MediumFlame", "Flame"};

    return table[num];
}

void InitFireway()
{
    float temp_x, temp_y;

    if (!ToInt(temp_x))
    {
        temp_x = LocationX(126);
        temp_y = LocationY(126);
    }
    else
    {
        TeleportLocation(126, temp_x, temp_y);
    }
}

//9x8
void FirewalkTrap()
{
    int k, line, ptr;

    if (line < 9)
    {
        TeleportLocation(125, LocationX(126), LocationY(126));
        AudioEvent("FireExtinguish", 125);
        ptr = CreateObject("InvisibleLightBlueHigh", 125);
        for (k = 7 ; k >= 0 ; k --)
        {
            CreateObject(FlameTable(0), 125);
            TeleportLocationVector(125, -23.0, 23.0);
        }
        Delete(ptr);
        FrameTimerWithArg(4, ptr + 1, SpawnMedFlame);
        TeleportLocationVector(126, -23.0, -23.0);
        line ++;
        FrameTimer(6, FirewalkTrap);
    }
    else
    {
        line = 0;
        InitFireway();
        FrameTimer(50, FirewalkTrap);
    }
}

void SpawnMedFlame(int ptr)
{
    int k;
    int ptr2 = CreateObject("InvisibleLightBlueHigh", 187);

    for (k = 7 ; k >= 0 ; k --)
    {
        TeleportLocation(187, GetObjectX(ptr + k), GetObjectY(ptr + k));
        Delete(ptr + k);
        CreateObject(FlameTable(1), 187);
    }
    Delete(ptr2);
    FrameTimerWithArg(4, ptr2 + 1, SpawnNormalFlame);
}

void SpawnNormalFlame(int ptr)
{
    int k;
    int ptr2 = CreateObject("InvisibleLightBlueHigh", 187);

    for (k = 7 ; k >= 0 ; k --)
    {
        TeleportLocation(187, GetObjectX(ptr + k), GetObjectY(ptr + k));
        Delete(ptr + k);
        CreateObject(FlameTable(2), 187);
    }
    Delete(ptr2);
    FrameTimerWithArg(4, ptr2 + 1, RemoveAllFlames);
}

void RemoveAllFlames(int ptr)
{
    int k;
    for (k = 7 ; k >= 0 ; k --)
        Delete(ptr + k);
}

void MonsterGirlBurst()
{
    if (GetUnitThingID(other) == 1336 && CurrentQuestLevel() >= 20)
    {
        SetColorMaiden(other, 250, 32, 225);
    }
}

int FireSpriteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 135; arr[19] = 65; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 13; arr[56] = 21; 
		arr[58] = 5546320; 
	pArr = arr;
	return pArr;
}

void FireSpriteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1073322393;		ptr[137] = 1073322393;
	int *hpTable = ptr[139];
	hpTable[0] = 135;	hpTable[1] = 135;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = FireSpriteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

void MonsterFireFairyBurst()
{
    int temp;

    if (GetUnitThingID(other) == 1370)
    {
        // SetMemory(GetMemory(UnitToPtr(other) + 0x2ec) + 0x1e4, GetFireSpriteBin());
        FireSpriteSubProcess(OTHER);
        SetUnitQuestHealth(other, 42);
        SetUnitStatus(other, GetUnitStatus(other) ^ 0x10000);
    }
}

void SetUnitQuestHealth(int unit, int amount)
{
    if (CurrentQuestLevel() >= 20)
        SetUnitMaxHealth(unit, amount * (32 / 10));
}

int MaidenBinTable()
{
	int arr[62];
	if (arr[0]) return arr;
	
	arr[0] = 1684627789; arr[1] = 28261; arr[2] = 0; arr[3] = 0; arr[4] = 0; 
	arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
	arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
	arr[15] = 0; arr[16] = 0; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
	arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1065688760; 
	arr[25] = 0; arr[26] = 0; arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
	arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; arr[34] = 0; 
	arr[35] = 0; arr[36] = 0; arr[37] = 0; arr[38] = 0; arr[39] = 0; 
	arr[40] = 0; arr[41] = 0; arr[42] = 0; arr[43] = 0; arr[44] = 0; 
	arr[45] = 0; arr[46] = 0; arr[47] = 0; arr[48] = 0; arr[49] = 0; 
	arr[50] = 0; arr[51] = 0; arr[52] = 0; arr[53] = 0; arr[54] = 0; 
	arr[55] = 0; arr[56] = 0; arr[57] = 0; arr[58] = 5546320; arr[59] = 5542784; 
	arr[60] = 0; arr[61] = 0; 
	return arr;
}

int CurrentQuestLevel()
{
    return GetMemory(0x69F968);
}

int SetColorMaiden(int unit, int red, int grn, int blue)
{
    int ptr = UnitToPtr(unit), k;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetMemory(ptr + 4, 1385);
        SetUnitQuestHealth(unit, 77);
        for (k = 0 ; k < 32 ; k ++)
            SetMemory(ptr + 0x230 + (k * 4), 0x400);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x178, 0xa0);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
        SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
        SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
        SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
        SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 16, grn | (blue << 8));
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(7));
    }
    return unit;
}

void SecretElevWallOpen()
{
    int k;

    ObjectOff(self);
    StartMovingFiretrapTowerGens();
    for (k = 0 ; k < 4 ; k ++)
        WallOpen(Wall(60 + k, 172 + k));
}

void SecretWallOpen()
{
    ObjectOff(self);
    WallOpen(Wall(39, 157));
    WallOpen(Wall(40, 158));
    WallOpen(Wall(41, 159));
    UniPrint(other, "아래쪽 비밀통로로 향하는 벽이 열렸습니다");
}

void RemoveTombPartGenWalls()
{
    ObjectOff(self);
    WallOpen(Wall(167, 219));
    WallOpen(Wall(168, 218));
    WallOpen(Wall(169, 217));
    WallOpen(Wall(170, 218));
    WallOpen(Wall(171, 219));
    WallOpen(Wall(172, 220));
    WallOpen(Wall(173, 221));
    WallOpen(Wall(172, 222));
    WallOpen(Wall(171, 223));
    UniPrint(other, "전방에 벽이 열립니다");
}

void TombSecretGenDestroy()
{
    int k;
    for (k = 0 ; k < 4 ; k ++)
        WallOpen(Wall(175 + k, 227 - k));
    MoveWaypoint(56, GetObjectX(self), GetObjectY(self));
    AudioEvent("BigGong", 56);
    FrameTimerWithArg(60, Object("TombPartBlock"), MovingTombPartBlock);
}

void MovingTombPartBlock(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 69)
    {
        MoveObject(ptr, GetObjectX(ptr) + 1.0, GetObjectY(ptr) + 1.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, MovingTombPartBlock);
    }
    else
    {
        WallOpen(Wall(172, 226));
        WallOpen(Wall(173, 227));
    }
}

int TombLeftArrowRow()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 277);
        LookWithAngle(ptr, 6);
        for (k = 0 ; k < 6 ; k ++)
        {
            ObjectOff(CreateObject("ArrowTrap1", 277));
            TeleportLocationVector(277, -16.0, 16.0);
        }
    }
    return ptr;
}

int TombRightArrowRow()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 278);
        LookWithAngle(ptr, 6);
        for (k = 0 ; k < 6 ; k ++)
        {
            ObjectOff(CreateObject("ArrowTrap1", 278));
            TeleportLocationVector(278, -16.0, 16.0);
        }
    }
    return ptr;
}

int TombDownArrowRow()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 357);
        LookWithAngle(ptr, 6);
        for (k = 0 ; k < 6 ; k ++)
        {
            ObjectOff(CreateObject("ArrowTrap2", 357));
            TeleportLocationVector(357, 16.0, 16.0);
        }
    }
    return ptr;
}

void EnableTombLeftRow()
{
    int k, ptr = TombLeftArrowRow() + 1;

    for (k = 0 ; k < GetDirection(ptr - 1) ; k ++)
        ObjectOn(ptr + k);
    FrameTimerWithArg(1, ptr, DisableTombArrowTraps);
}

void EnableTombRightRow()
{
    int k, ptr = TombRightArrowRow() + 1;

    for (k = 0 ; k < GetDirection(ptr - 1) ; k ++)
        ObjectOn(ptr + k);
    FrameTimerWithArg(1, ptr, DisableTombArrowTraps);
}

void EnableTombDownRow()
{
    int k, ptr = TombDownArrowRow() + 1;

    for (k = 0 ; k < GetDirection(ptr - 1) ; k ++)
        ObjectOn(ptr + k);
    FrameTimerWithArg(1, ptr, DisableTombArrowTraps);
}

void DisableTombArrowTraps(int ptr)
{
    int k;
    for (k = 0 ; k < GetDirection(ptr - 1) ; k ++)
        ObjectOff(ptr + k);
}

void OpenTombExitZoneWalls()
{
    ObjectOff(self);

    WallOpen(Wall(149, 229));
    WallOpen(Wall(150, 228));
    WallOpen(Wall(151, 227));
    UniPrint(other, "비밀의 벽이 열렸습니다");
}

void QuickTesting()
{
    MoveObject(other, LocationX(444), LocationY(444));
}

void GreenSparkFxAt(float sX, float sY)
{
    int fxUnit = CreateObjectAt("MonsterGenerator", sX, sY);

    Damage(fxUnit, 0, 1, 14);
    Delete(fxUnit);
}

void MobGenClassMissileCollide()
{
    int owner = GetOwner(self);

    while (1)
    {
        if (CurrentHealth(other) && IsAttackedBy(other, owner))
        {
            Damage(other, owner, 62, 14);
            GreenSparkFxAt(GetObjectX(other), GetObjectY(other));
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(self);
        break;
    }
}

void DetectMagicWandMissile(int cur, int owner)
{
    int mis, ptr;

    if (CurrentHealth(owner))
    {
        mis = CreateObjectAt("MagicMissile", GetObjectX(cur), GetObjectY(cur));
        ptr = GetMemory(0x750710);
        SetMemory(ptr + 0x2e8, 5483536); //projectile update
        SetMemory(ptr + 0x2b8, ImportUnitCollideFunc());
        SetMemory(ptr + 0x2fc, MobGenClassMissileCollide);
        SetOwner(owner, mis);
        PushObjectTo(mis, UnitAngleCos(owner, 32.0), UnitAngleSin(owner, 32.0));
    }
    Delete(cur);
}

void FireballCollide()
{
    int owner = GetOwner(self), ptr;

    while (1)
    {
        if (CurrentHealth(other) && IsAttackedBy(other, owner))
        {
            ptr = UnitToPtr(self);
            if (ptr)
            {
                Damage(other, owner, GetMemory(GetMemory(ptr + 0x2bc)) & 0xff, 14);
                Effect("SPARK_EXPLOSION", GetObjectX(other), GetObjectY(other), 0.0, 0.0);
            }
        }
        else if (WallUtilGetWallAtObjectPosition(SELF))
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(self);
        break;
    }
}

void FireballShootHandler(int sCur, int owner)
{
    int ptr;

    if (CurrentHealth(owner))
    {
        if (!IsPlayerUnit(owner))
            return;

        SetUnitCallbackOnCollide(sCur, FireballCollide);
    }
}

void onGlyphDetected(int cur, int owner)
{
    if (IsPlayerUnit(owner))
        Delete(cur);
}

static void IntroducedIndexLoopHashCondition(int pInstance)
{
	HashPushback(pInstance, OBJ_YELLOW_STAR_SHOT, DetectMagicWandMissile);
	HashPushback(pInstance, OBJ_FIREBALL, FireballShootHandler);
	HashPushback(pInstance, OBJ_STRONG_FIREBALL, FireballShootHandler);
	HashPushback(pInstance, OBJ_GLYPH, onGlyphDetected);
}

void RemoveAllGlyphOnInventory(int unit)
{
    int inv = GetLastItem(unit), del;

    while (IsObjectOn(inv))
    {
        if (GetUnitThingID(inv) == 618)
            del = inv;
        else
            del = 0;
        inv = GetPreviousItem(inv);
        if (del) Delete(del);
    }
}

void PlayerScanHasGlyph()
{
    if (CurrentHealth(other))
    {
        if (!HasEnchant(other, "ENCHANT_ETHEREAL"))
        {
            RemoveAllGlyphOnInventory(other);
            Enchant(other, "ENCHANT_ETHEREAL", 0.5);
        }
    }
}

void PlayerGlyphScanAt(int plrPtr)
{
    int plrUnit = GetMemory(plrPtr + 0x2c);

    if (MaxHealth(plrUnit))
        RemoveAllGlyphOnInventory(plrUnit);
}

void PlayerGlyphScanStart()
{
    int plrPtr = 0x62f9e0;
    int plrLen = 0x12dc;

    int rep=-1;
    while ((++rep) < 32)
    {
        if (GetMemory(plrPtr))
            PlayerGlyphScanAt(GetMemory(plrPtr));
        plrPtr += plrLen;
    }
}

static void NetworkUtilClientMain()
{
    InitializeResources();
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    // if (pUnit)
    // {
    //     ShowQuestIntroOne(pUnit, 9999, "ConjurerChapterLoss10", "Journal:GauntletQuest");
    //     PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    // }
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
