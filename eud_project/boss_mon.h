
#include "boss_monProto.h"
#include "boss_reward.h"
#include "boss_bossProto.h"
#include "libs/printutil.h"

void setDefaultDeathEvent(int boss, int fn){
    int *ptr=UnitToPtr(boss);
    if (ptr){
        int *pEC=GetMemory(ptr+0x2ec);
        if (pEC){
            int *pDeathEvent=pEC+0x4f4;
            if (pDeathEvent[0]==-1){
                SetCallback(boss, 5, fn);
                // PrintToAll(IntToString(pDeathEvent[0]));
            }
        }
    }
}

void queryMonItemList(int *get, int set){
    int list;
    if (get){
        get[0]=list;
        return;
    }
    list=set;
}

int createGameResult(float x, float y){
    int fn;
    QueryResultGameFn(&fn,0);
    int *p=&x;
    return Bind(fn, p);
}

#define MON_DATA_ROUND 0
#define MON_DATA_COUNT 1
#define MON_DATA_TOTAL 2
#define MON_DATA_ROUNDFN 3
#define MON_DATA_MAX 4


void queryMonData(int *get, int set){
    int data;

    if (get)
    {
        get[0]=data;
        return ;
    }
    data=set;
}

void getMonsterFunction(short *get){
    short *p;

    if (get==0)
        return;

    if (p==0){
        short proto[]={
            createMonsterSmallSpider,createMonsterFanmadeBear,createMonsterDoraemon,createMonsterWhiteman,createMonsterMisWhitemanSiff,
            createMonsterSiff,
            createMonsterZergZergling,createMonsterFubaoPanda,createMonsterMixFubaoZergling,
             createMonsterBranchOni,createMonsterAckMechine,createMonsterBillyFaceAooni,
            createMonsterGlobglogapglab,createMonsterMixGlobgloAckBilly,createRedmist,
            createGameResult,
            };
        p=proto;
    }
    int *d;
    queryMonData(&d, 0);
    get[0]= p[ d[MON_DATA_ROUND] ];
}

void monMarkerList(int *get, int set){
    int inst;
    if (get)
    {
        get[0]=inst;
        return;
    }
    inst=set;
}

void createMonsterItem(int pos){
    int ret;
    if (Random(0,3))
    {
        CreateHotPotion2(GetObjectX(pos),GetObjectY(pos), &ret);
        Delete(pos);
    }
    else
        ret= CreateRandomItemCommonRet(pos);
    if (ret)
    {
        int list;
        queryMonItemList(&list,0);
        int node;
        LinkedListFront(list, &node);
        int val=LinkedListGetValue(node);
        if (ToInt(GetObjectX( val) ))
        {
            if (!GetOwner(val))
            {
                DeleteObjectTimer( CreateObjectById(OBJ_PUFF, GetObjectX(val),GetObjectY(val)), 9);
                PlaySoundAround(val,SOUND_BlackPowderBurn);
                Delete(val);
            }
        }
        LinkedListPopFront(list);
        LinkedListPushback(list,ret,0);
    }
}

void onMonsterDie(){
    int *d;
    queryMonData(&d, 0);
    if (++d[MON_DATA_COUNT]==d[MON_DATA_TOTAL])
    {
        ++d[MON_DATA_ROUND];
        d[MON_DATA_COUNT]=0;
        UniPrintToAll("MISSING:'라운드 업! - 잠시 후에 다음 라운드가 진행 됩니다. 대기하세요'");
        SecondTimer(15, StartMonSummon);
    }
    DeleteObjectTimer(SELF, 3);
    PushTimerQueue(9, CreateObjectById(OBJ_OLD_BIG_SMOKE, GetObjectX(SELF), GetObjectY(SELF)), createMonsterItem);
}

void onMonsterHurt()
{
    if (GetCaller())
        return;

    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, 0, 3, DAMAGE_TYPE_POISON);
        DeleteObjectTimer(CreateObjectById(OBJ_GREEN_PUFF, GetObjectX(SELF), GetObjectY(SELF)), 9);
    }
}

void commonMonsterSettings(int mon){
    if (IsMonsterUnit(mon)){
        RetreatLevel(mon,0.0);
        AggressionLevel(mon,1.0);
        // SetCallback(mon,5,onMonsterDie);
        setDefaultDeathEvent(mon, onMonsterDie);
        SetCallback(mon,7,onMonsterHurt);
        LookWithAngle(mon, Random(0,255));
    }
}

void summonProgress(int node){
    if (!node)
        return;

    int *point= LinkedListGetValue(node);
    int *d;

    //TODO. create monster here
    queryMonData(&d, 0);
    int mon = Bind(d[MON_DATA_ROUNDFN],point);

    commonMonsterSettings(mon);
    LinkedListNext(node, &node);
    PushTimerQueue(1,node,summonProgress);
}

void StartMonSummon(){
    int list;

    monMarkerList(&list,0);
    int node;
    LinkedListFront(list,&node);
    PushTimerQueue(1,node,summonProgress);
    short fn;
    getMonsterFunction(&fn);
    int *d;
    queryMonData(&d,0);
    d[MON_DATA_ROUNDFN]=fn;
}

void PlaceMonsterMarker(int pos){
    int *point;
    AllocSmartMemEx(8,&point);
    point[0]=GetObjectX(pos);
    point[1]=GetObjectY(pos);
    int list;
    monMarkerList(&list, 0);
    LinkedListPushback(list, point, NULLPTR);
    int *p;
    queryMonData(&p,0);
    ++p[MON_DATA_TOTAL];
    Delete(pos);
}

void spawnItemLimit(int count){
    if (--count>=0)
        PushTimerQueue(1,count, spawnItemLimit);
    int list;
    queryMonItemList(&list, 0);
    LinkedListPushback(list, CreateObjectById(OBJ_SKULL, LocationX(20), LocationY(20)), NULLPTR);
}

void initMonItemLimit(){
    int list;
    LinkedListCreateInstance(&list);
    queryMonItemList(0,list);
    spawnItemLimit(100);
}

void onKnightDeath(){
    int me=GetTrigger(), hash=GenericHash(), count=0;

    HashGet(hash,me,&count,TRUE);
    RemoveNPCEquipmentsByCount(me,count,0);
    onBossDeath();
}

void onKnightZerglingDeath(){
    int k=Object("VampireKnight");

    if (CurrentHealth(k)){
        float x=GetObjectX(SELF),y=GetObjectY(SELF);

        ObjectOn(k);
        MoveObject(k, x,y);
        Effect("SMOKE_BLAST", x,y,0.0,0.0);
        Effect("BLUE_SPARKS", x,y,0.0,0.0);
    }
}

int createBatKnight(float x, float y){
    int k=Object("VampireKnight");

    if (!CurrentHealth(k))
        return 0;

    SetUnitMass(k,200.0);
    SetNPCAntiFumble(k);
    SetUnitMaxHealth(k, 4000);
    HashPushback(GenericHash(),k,InvincibleInventoryItem(k));
    SetCallback(k,5,onKnightDeath);

	int bat=createMonsterZergZergling(x,y);

    commonMonsterSettings(bat);
    SetCallback(bat,5,onKnightZerglingDeath);
	return k;
}

void bossKillGetGold(){
    int r=MAX_PLAYER_COUNT, lv;

    while (--r>=0){
        if (MaxHealth( GetPlayerUnit(r)) ){
            ChangeGold(GetPlayerUnit(r),8500+((lv++)*150));
        }
    }
    UniPrintToAll("방금 보스를 처치하셨습니다. 모든 플레이어에게 금화가 지급되었습니다");
}

#define BOSS_TIMER_RESET 180
#define BOSS_PARAM_COUNTDOWN 0
#define BOSS_PARAM_LEVEL 1
#define BOSS_PARAM_UNIT 2
#define BOSS_PARAM_X 3
#define BOSS_PARAM_Y 4
#define BOSS_PARAM_MAX 5

void onBossDeath(){
    DeleteObjectTimer(SELF, 30);
    float x=GetObjectX(SELF),y= GetObjectY(SELF);
    // PushTimerQueue(9, CreateObjectById(OBJ_OLD_BIG_SMOKE, x,y), createMonsterItem);
    // PushTimerQueue(9, CreateObjectById(OBJ_OLD_BIG_SMOKE, x,y), createMonsterItem);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    PushTimerQueue(9, CreateObjectById(OBJ_BIG_SMOKE, x,y), CreateRandomItemCommon);
    bossKillGetGold();
}
void onFinalBossDeath(){
    int me=GetTrigger(), hash=GenericHash(), count=0;

    HashGet(hash,me,&count,TRUE);
    RemoveNPCEquipmentsByCount(me,count,0);
    onBossDeath();
    // PrintToAll(IntToString(count));
}
int bringFinalBoss(float x,float y){
    int boss=Object("FinalBoss");

    if (MaxHealth(boss)!=3000)
        SetUnitMaxHealth(boss,3000);
        // SetUnitMaxHealth(boss,30); //test
    int hash=GenericHash();

    HashPushback(hash,boss,InvincibleInventoryItem(boss));
    SetUnitMass(boss,200.0);
    SetNPCAntiFumble(boss);
    MoveObject(boss,x,y);
    ObjectOn(boss);
    SetCallback(boss,5,onFinalBossDeath);
    return boss;
}
int summonBoss(int *d){
    short bossFn[]={
        // bringFinalBoss, //test
        createBossAckMech, createBossOgrelord,createBossMaidenGhost,createBossKillerian,bringFinalBoss,
        createBossBillyOni,createBatKnight,bossEnding,};
    int boss=Bind(bossFn[d[BOSS_PARAM_LEVEL]++], &d[BOSS_PARAM_X]);

    if (!boss) return 0;
    setDefaultDeathEvent(boss, onBossDeath);
    commonMonsterSettings(boss);
    AttachHealthbar(boss);
    // SetCallback(boss,5,onBossDeath);
    if (d[BOSS_PARAM_LEVEL]==8)
        return 0;
    return boss;
}
void onCountdownBossTimer(int *d){
    if (d[BOSS_PARAM_COUNTDOWN]>=0)
    {
        if (!CurrentHealth(d[BOSS_PARAM_UNIT]))
        {
            if (d[BOSS_PARAM_UNIT])
                d[BOSS_PARAM_UNIT]=0;
            --d[BOSS_PARAM_COUNTDOWN];
        }
        PushTimerQueue(30,d,onCountdownBossTimer);
        return;
    }
    int boss=summonBoss(d);
    if (boss)
    {
        PushTimerQueue(30,d,onCountdownBossTimer);
        d[BOSS_PARAM_UNIT]=boss;
    }
    d[BOSS_PARAM_COUNTDOWN]=BOSS_TIMER_RESET;
}
void startBossTimer(){
    int d[BOSS_PARAM_MAX];

    d[BOSS_PARAM_COUNTDOWN]=BOSS_TIMER_RESET;
    // d[BOSS_PARAM_COUNTDOWN]=15; //test
    d[BOSS_PARAM_LEVEL]=0;
    PushTimerQueue(15,d,onCountdownBossTimer);
    d[BOSS_PARAM_X]=LocationX(24);
    d[BOSS_PARAM_Y]=LocationY(24);
}

void InitializeMonsterSettings(){
    int list;

    LinkedListCreateInstance(&list);
    monMarkerList(0,list);

    int *p;
    AllocSmartMemEx(MON_DATA_MAX*4,&p);
    p[MON_DATA_ROUND]=0;
    p[MON_DATA_COUNT]=0;
    p[MON_DATA_TOTAL]=0;
    queryMonData(0,p);
    InitializeMonProto();
    initMonItemLimit();
    SecondTimer(15, startBossTimer);
}
