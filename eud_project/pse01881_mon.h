
#include "pse01881_utils.h"
#include "libs\meleeattack.h"
#include "libs/spellutil.h"
#include "libs/waypoint.h"

/*********
* monsterbin script section
*********/

void WizardUnitOnRunAway()
{
    if (UnitCheckEnchant(SELF, GetLShift(ENCHANT_ANTI_MAGIC)))
        EnchantOff(SELF, EnchantList(ENCHANT_ANTI_MAGIC));
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[16] = 80000; arr[17] = 300; 
		arr[18] = 100; arr[19] = 50; arr[21] = 1065353216; arr[24] = 1067869798; arr[26] = 4; 
		arr[27] = 4; arr[53] = 1128792064; arr[54] = 4;
		link = &arr;
	}
	return link;
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 473; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 2048; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 50; arr[30] = 1092616192; arr[32] = 19; arr[33] = 27; 
		arr[57] = 5548288; arr[59] = 5542784;
		link = &arr;
	}
	return link;
}

void LichLordSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2048);
		//SetMemory(GetMemory(ptr + 0x22c), 300);
		//SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichLordBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int FishBigBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1752394054; arr[1] = 6777154; arr[17] = 237; arr[18] = 1; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1073741824; arr[27] = 1; arr[28] = 1112014848; 
		arr[29] = 30; arr[31] = 8; arr[32] = 3; arr[33] = 7; arr[59] = 5542784; 
		arr[60] = 1329; arr[61] = 46905600; 
		link = &arr;
	}
	return link;
}

void FishBigSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 237);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 237);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FishBigBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int DemonBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1869440324; arr[1] = 110; arr[17] = 600; arr[19] = 96; arr[21] = 1065353216; 
		arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; arr[28] = 1112014848; 
		arr[29] = 24; arr[31] = 1; arr[32] = 16; arr[33] = 22; arr[53] = 1128792064; 
		arr[54] = 4; arr[58] = 5545472; arr[59] = 5544288; arr[60] = 1347; arr[61] = 46910976; 
		link = &arr;
	}
	return link;
}

void DemonSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077432811);
		SetMemory(ptr + 0x224, 1077432811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 666);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 666);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, DemonBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int HecubahBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 2500; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1117782016; arr[29] = 222; arr[32] = 10; arr[33] = 18; arr[57] = 5548288; 
		arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void HecubahSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 2500);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 2500);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[19] = 1; arr[24] = 1065772646; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
        link = &arr;
	}
	return link;
}

int ScorpionBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919902547; arr[1] = 1852795248; arr[17] = 275; arr[18] = 75; arr[19] = 75; 
		arr[21] = 1065353216; arr[24] = 1067869798; arr[25] = 1; arr[26] = 5; arr[27] = 5; 
		arr[28] = 1112014848; arr[29] = 77; arr[30] = 1101004800; arr[31] = 3; arr[32] = 22; 
		arr[33] = 30; arr[34] = 3; arr[35] = 5; arr[36] = 30; arr[59] = 5543344; 
		arr[60] = 1373; arr[61] = 46895952; 
		link = &arr;
	}
	return link;
}

void ScorpionSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1074790400);
		SetMemory(ptr + 0x224, 1074790400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 275);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 275);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, ScorpionBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 470; 
		arr[19] = 55; arr[21] = 1065353216; arr[24] = 1071225242; arr[26] = 4; arr[28] = 1106247680; 
		arr[29] = 44; arr[31] = 3; arr[32] = 4; arr[33] = 5; arr[59] = 5542784; 
		arr[60] = 1388; arr[61] = 46915072; 
		link = &arr;
	}
	return link;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1070805811);
		SetMemory(ptr + 0x224, 1070805811);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 470);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 470);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WaspBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1886609751; arr[17] = 98; arr[18] = 1; arr[19] = 120; arr[21] = 1065353216; 
		arr[23] = 1; arr[24] = 1067869798; arr[27] = 5; arr[28] = 1097859072; arr[29] = 20; 
		arr[31] = 3; arr[34] = 2; arr[35] = 3; arr[36] = 20; arr[59] = 5544320; 
		arr[60] = 1331; arr[61] = 46900736; 
		link = &arr;
	}
	return link;
}

void WaspSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1080452710);
		SetMemory(ptr + 0x224, 1080452710);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 1);
		SetMemory(GetMemory(ptr + 0x22c), 98);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 98);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WaspBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int ImpBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 7368009; arr[17] = 180; arr[18] = 1; arr[19] = 110; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1128792064; arr[54] = 1; arr[55] = 9; 
		arr[56] = 17; arr[60] = 1328; arr[61] = 46904064; 
		link = &arr;
	}
	return link;
}

void ImpSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1079194419);
		SetMemory(ptr + 0x224, 1079194419);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 180);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 180);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, ImpBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 80; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[27] = 0; arr[28] = 1106247680; arr[29] = 12; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680;
        link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[17] = 180; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; 
		arr[28] = 1097859072; arr[29] = 25; arr[31] = 8; arr[32] = 13; arr[33] = 21; 
		arr[34] = 4; arr[35] = 2; arr[36] = 9; arr[37] = 1684631635; arr[38] = 1884516965; 
		arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

void BlackWidowSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 260);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 260);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int HorrendousBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[16] = 100000; arr[17] = 400; 
		arr[18] = 400; arr[19] = 100; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 9; arr[27] = 5; arr[28] = 1109393408; arr[29] = 34; 
		arr[54] = 4; arr[59] = 5542784; arr[60] = 1386; arr[61] = 46907648; 
		link = &arr;
	}
	return link;
}

void HorrendousSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 400);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 400);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HorrendousBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int EmberDemonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1700949317; arr[1] = 1835353202; arr[2] = 28271; arr[17] = 225; arr[19] = 85; 
		arr[21] = 1056964608; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; arr[27] = 5; 
		arr[28] = 1103626240; arr[29] = 20; arr[31] = 10; arr[32] = 5; arr[33] = 10; 
		arr[37] = 1869771859; arr[38] = 1766221678; arr[39] = 1633838450; arr[40] = 27756; arr[53] = 1128792064; 
		arr[54] = 4; arr[55] = 15; arr[56] = 30; arr[58] = 5545344; arr[59] = 5542784; 
		arr[60] = 1337; arr[61] = 46903552; 
		link = &arr;
	}
	return link;
}

void EmberDemonSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1076048691);
		SetMemory(ptr + 0x224, 1076048691);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 225);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 225);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, EmberDemonBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1056964608);
	}
}

int SkeletonLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1818585939; arr[1] = 1852798053; arr[2] = 1685221196; arr[16] = 50000; arr[17] = 100; 
		arr[18] = 75; arr[19] = 65; arr[21] = 1065353216; arr[23] = 1; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4; arr[28] = 1106247680; arr[29] = 20; arr[32] = 10; 
		arr[33] = 15; arr[58] = 5547856; arr[59] = 5542784; arr[60] = 1315; arr[61] = 46910464; 
		link = &arr;
	}
	return link;
}

void SkeletonLordSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1073322393);
		SetMemory(ptr + 0x224, 1073322393);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 1);
		SetMemory(GetMemory(ptr + 0x22c), 100);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 100);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, SkeletonLordBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}


//sectionEnd

int WoundedConjurerBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1130653028; arr[2] = 1969909359; arr[3] = 7497074; arr[17] = 200; 
		arr[19] = 120; arr[21] = 1065353216; arr[23] = 32832; arr[24] = 1065353216; arr[26] = 4; 
		arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; arr[40] = 116; arr[53] = 1133903872; 
		arr[55] = 14; arr[56] = 24; arr[60] = 2271; arr[61] = 46912768; 
		link = &arr;
	}
	return link;
}

void WoundedConjurerSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1080452710);
		SetMemory(ptr + 0x224, 1080452710);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32832);
		SetMemory(GetMemory(ptr + 0x22c), 200);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 200);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedConjurerBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int SummonWoundedWiz(int posUnit)
{
    int mob = CreateObjectAtUnit("WoundedApprentice", posUnit);

    WoundedConjurerSubProcess(mob);
    return mob;
}

void WizRunAway()
{
	if (UnitCheckEnchant(SELF, GetLShift(ENCHANT_ANTI_MAGIC)))
		EnchantOff(SELF, EnchantList(ENCHANT_ANTI_MAGIC));
}

int SummonMobRedWizard(int sUnit)
{
    int redWiz = CreateObjectAtUnit("WizardRed", sUnit);
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    UnitLinkBinScript(redWiz, WizardRedBinTable());
    SetUnitMaxHealth(redWiz, 411);
    SetCallback(redWiz, 8, WizRunAway);
    SetUnitStatus(redWiz, GetUnitStatus(redWiz) ^ 0x20);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_LIGHTNING"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x8000000);
    }
    return redWiz;
}

int SummonMobPlant(int sUnit)
{
    int mob = CreateObjectAtUnit("CarnivorousPlant", sUnit);

    UnitZeroFleeRange(mob);
    SetUnitSpeed(mob, 2.5);
    SetUnitMaxHealth(mob, 325);
    AggressionLevel(mob, 1.0);
    RetreatLevel(mob, 0.0);
    ResumeLevel(mob, 1.0);
    return mob;
}

int SummonMobStrongWizard(int sUnit)
{
    int unit = CreateObjectAtUnit("StrongWizardWhite", sUnit);

    UnitLinkBinScript(unit, StrongWizardWhiteBinTable());
    SetUnitMaxHealth(unit, 320);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    return unit;
}

int SummonBeast(int posUnit)
{
    int mob = CreateObjectAtUnit("WeirdlingBeast", posUnit);

    UnitZeroFleeRange(mob);
    WeirdlingBeastSubProcess(mob);
    return mob;
}

int FireSpriteBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 250; arr[19] = 100; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1801545047; arr[38] = 1701996870; 
		arr[39] = 1819042146; arr[53] = 1133903872; arr[55] = 14; arr[56] = 24; arr[58] = 5545472; 
		link = &arr;
	}
	return link;
}

void FireSpriteSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 65536);
		SetMemory(GetMemory(ptr + 0x22c), 250);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 250);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int SummonMobFireFairy(int posUnit)
{
    int fairy = CreateObjectAt("FireSprite", GetObjectX(posUnit), GetObjectY(posUnit));

    FireSpriteSubProcess(fairy);
    return fairy;
}

int SummonMobLich(int sUnit)
{
    int lich = CreateObjectAtUnit("Lich", sUnit);

    SetUnitMaxHealth(lich, 325);
    return lich;
}

int SummonMobEmberDemon(int sUnit)
{
    int mob = CreateObjectAtUnit("EmberDemon", sUnit);

    EmberDemonSubProcess(mob);
    RegistUnitStrikeHook(mob);
    return mob;
}

int SummonMobSkullLord(int posunit)
{
    int mob = CreateObjectAtUnit("SkeletonLord", posunit);

    SkeletonLordSubProcess(mob);
    RegistUnitStrikeHook(mob);
    return mob;
}

int NecromancerBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1919116622; arr[1] = 1851878767; arr[2] = 7497059; arr[17] = 444; arr[19] = 110; 
		arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1065353216; arr[25] = 1; arr[26] = 2; 
		arr[28] = 1114636288; arr[29] = 100; arr[30] = 1092616192; arr[31] = 11; arr[32] = 7; 
		arr[33] = 15; arr[34] = 1; arr[35] = 1; arr[36] = 10; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void NecromancerSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1079194419);
		SetMemory(ptr + 0x224, 1079194419);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 34816);
		SetMemory(GetMemory(ptr + 0x22c), 444);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 444);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, NecromancerBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int SummonNecromancer(int locationId)
{
    int unit = CreateObjectAt("Necromancer", LocationX(locationId), LocationY(locationId));
    int *ptr = UnitToPtr(unit);

    SetUnitMaxHealth(unit, 365);
    SetCallback(unit, 8, WizardUnitOnRunAway);
    if (ptr != NULLPTR)
    {
        int uec = ptr[187];
        
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_LIGHTNING"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x8000000);
    }
    return unit;
}
