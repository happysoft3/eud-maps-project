
#include "subway_gvar.h"
#include "libs/waypoint.h"
#include "libs/mathlab.h"
#include "libs/memutil.h"
#include "libs/playerinfo.h"
#include "libs/queueTimer.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/buff.h"

int ColorMaidenAt(int red, int grn, int blue, float x,float y)
{
    int unit = CreateObjectById(OBJ_BEAR, x,y);
    int ptr1 = GetMemory(0x750710), k, num;

    SetMemory(ptr1 + 4, 1385);
    UnitLinkBinScript(unit, MaidenBinTable());
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetUnitVoice(unit, 7);

    return unit;
}

int HecubahBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 20; arr[19] = 300; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; 
		arr[29] = 85; arr[30] = 1092616192; arr[32] = 10; arr[33] = 18; arr[57] = 5548288; 
		arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 130; 
		arr[19] = 60; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; arr[26] = 4; 
		arr[28] = 1106247680; arr[29] = 33; arr[31] = 8; arr[32] = 12; arr[33] = 20; 
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link = arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 85; 
		arr[18] = 50; arr[19] = 55; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1109393408; arr[29] = 30; arr[31] = 2; arr[32] = 8; 
		arr[33] = 16; arr[57] = 5548112; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int GoonBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 60; arr[21] = 1065353216; arr[23] = 32768; 
		arr[24] = 1065353216; arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; arr[30] = 1092616192; 
		arr[31] = 4; arr[32] = 18; arr[33] = 24; arr[34] = 2; arr[35] = 3; 
		arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = arr;
	}
	return link;
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 20; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; arr[29] = 50; 
		arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; arr[57] = 5548288; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

int WizardRedBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[16] = 80000; arr[17] = 300; 
		arr[18] = 100; arr[19] = 50; arr[21] = 1065353216; arr[23] = 32800; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; 
		arr[54] = 4; arr[55] = 14; arr[56] = 24; 
		link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1065688760; 
		arr[25] = 0; arr[26] = 0; arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; arr[58] = 5546320; arr[59] = 5542784; 
        link = arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 4; arr[24] = 1069547520; 
		arr[25] = 0; arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[30] = 0; arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360;
        link = arr;
	}
	return link;
}

int Bear2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50;
		arr[15] = 0; arr[16] = 20000; arr[17] = 90; arr[18] = 100; arr[19] = 40; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 65545; arr[24] = 1067450368; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30; arr[58] = 5547856; arr[59] = 5542784;
        link = arr;
	}
	return link;
}


void MakeStar(int wp, float size)
{
    float posX = GetWaypointX(wp), posY = GetWaypointY(wp), fArr[10];
    int i;

    for (i = 0; i < 5; i += 1)
    {
        fArr[i * 2] = posX - MathSine(i * 72, size);
        fArr[i * 2 + 1] = posY - MathSine(i * 72 + 90, size);
    }
    Effect("SENTRY_RAY", fArr[0], fArr[1], fArr[4], fArr[5]);
    Effect("SENTRY_RAY", fArr[0], fArr[1], fArr[6], fArr[7]);
    Effect("SENTRY_RAY", fArr[2], fArr[3], fArr[6], fArr[7]);
    Effect("SENTRY_RAY", fArr[2], fArr[3], fArr[8], fArr[9]);
    Effect("SENTRY_RAY", fArr[4], fArr[5], fArr[8], fArr[9]);
}

void WizRunAway()
{
	if (HasEnchant(SELF, "ENCHANT_ANTI_MAGIC"))
	{
		EnchantOff(SELF, "ENCHANT_ANTI_MAGIC");
	}
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObject("RottenMeat", 1));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

int RepairAll(int unit)
{
    int inv = GetLastItem(unit), count = 0;

    while (IsObjectOn(inv))
    {
        if (MaxHealth(inv) ^ CurrentHealth(inv))
        {
            RestoreHealth(inv, MaxHealth(inv) - CurrentHealth(inv));
            count +=1;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void InitiPlayerCamera(int unit)
{
	int ptr = UnitToPtr(unit);

	if (ptr) SetMemory(GetMemory(GetMemory(ptr + 0x2ec) + 0x114) + 0xe58, 0);
}


int UnitScrNameToNumber(int unit)
{
    int ptr = UnitToPtr(unit), raw, num = -1;

    if (ptr)
    {
        ptr = GetMemory(ptr);
        if (ptr)
        {
            raw = GetMemory(ptr);
            if (raw)
            {
                if (CheckVaildNumber(raw))
                    num = StringByteToInt(raw);
            }
        }
    }
    return num;
}

int StringByteToInt(int byte)
{
    return ((byte>>24) & 0xff) - 0x30 + ((((byte>>16) & 0xff) - 0x30) * 10) + ((((byte>>8) & 0xff) - 0x30) * 100) + (((byte & 0xff) - 0x30) * 1000);
}

int CheckVaildNumber(int num)
{
    return (IsNumber(num & 0xff) && IsNumber((num>>8) & 0xff) && IsNumber((num>>16) & 0xff) && IsNumber((num>>24) & 0xff));
}

int IsNumber(int ch)
{
    return (ch >= 0x30 && ch <= 0x39);
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}
void SendPacketAll(char *packet, int packetLength)
{
    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x90, 0x53, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x1C, 0x31, 0xC0, 0xC3,
    };
    int args[]={
        packet,0xff,packetLength,
    }; //0xff or 0x9f
    invokeRawCode(code, args);
}

void SendPacketSpecifyOne(int user, char *packet, int packetLength)
{
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0)
        return;
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
    };
    int args[]={
        packet,pIndex,packetLength,
    };
    invokeRawCode(code, args);
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}

void AbsoluteTargetStrike(int owner, int target, float threshold, int func)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target), GetObjectY(target));
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner)), threshold);

    SetOwner(owner, unit);
    Raise(unit, ToFloat(target));
    FrameTimerWithArg(1, unit, func);
}


void CustomMeleeAttackCode(char *bin, int fn)
{
    char code[]={
        0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
        0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
        0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
        0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
    };
    
    int *pStrike = &bin[0xEC];

    pStrike[0] = code;
    int *hack = &bin[0x94];

    hack[15]=fn;
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}

#define TRANSPORT_SUB 0
#define TRANSPORT_COUNTER 1
#define TRANSPORT_CUSTOMER 2
#define TRANSPORT_EFFECT 3
#define TRANSPORT_MAX 4

void onTransportProcedure(int *pTransp)
{
    int owner = pTransp[TRANSPORT_CUSTOMER];

    if (CurrentHealth(owner))
    {
        int count = pTransp[TRANSPORT_COUNTER];

        if (count)
        {
            int eff=pTransp[TRANSPORT_EFFECT];

            if (DistanceUnitToUnit(owner, eff) < 23.0)
            {
                pTransp[TRANSPORT_COUNTER]-=1;
                PushTimerQueue(1, pTransp, onTransportProcedure);
                return;
            }
        }
        else
        {
            Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            int destination = pTransp[TRANSPORT_SUB]+1;

            MoveObject(owner, GetObjectX(destination), GetObjectY(destination));
            PlaySoundAround(owner, SOUND_BlindOff);
            Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
        }
        EnchantOff(owner, "ENCHANT_BURNING");
    }
    Delete(pTransp[TRANSPORT_EFFECT]);
    FreeSmartMemEx(pTransp);
}

void onTransportCollide()
{
    if (CurrentHealth(OTHER))
    {
        if (!IsPlayerUnit(OTHER)) return;        
        if (!UnitCheckEnchant(OTHER, GetLShift(ENCHANT_BURNING)))
        {
            int *pTransp;            
            AllocSmartMemEx(TRANSPORT_MAX*4, &pTransp);
            pTransp[TRANSPORT_SUB]=GetTrigger();
            pTransp[TRANSPORT_COUNTER] = 48;
            pTransp[TRANSPORT_CUSTOMER]=GetCaller();
            pTransp[TRANSPORT_EFFECT]=CreateObjectAt("VortexSource", GetObjectX(SELF), GetObjectY(SELF));
            PushTimerQueue(1, pTransp, onTransportProcedure);
            GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
            Enchant(OTHER, "ENCHANT_BURNING", 4.0);
        }
    }
}

#undef TRANSPORT_SUB
#undef TRANSPORT_COUNTER
#undef TRANSPORT_CUSTOMER
#undef TRANSPORT_EFFECT
#undef TRANSPORT_MAX

int DispositionTransport(float x,float y, float dx,float dy)
{
    int tp=CreateObjectById(OBJ_WEIRDLING_BEAST, x,y );

    CreateObjectById(OBJ_MAGIC_ENERGY, dx,dy);
    UnitNoCollide(CreateObjectById(OBJ_CORPSE_SKULL_SW, GetObjectX(tp), GetObjectY(tp)));
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH, x,y);
    Frozen(tp+2, TRUE);
    SetCallback(tp, 9, onTransportCollide);
    Damage(tp, 0, MaxHealth(tp)+1, -1);
    return tp;
}

void DrawTextOnBottom(int textId, int textSection, float xpos, float ypos){
    int m=DummyUnitCreateById(OBJ_FISH_BIG,xpos,ypos);
    char ss[]={0,32,64,96,128,160,192,224,};

    MakeEnemy(m);
    controlHealthbarMotion(m,textSection);
    LookWithAngle(m, ss[textId]);
    UnitNoCollide(m);
}
