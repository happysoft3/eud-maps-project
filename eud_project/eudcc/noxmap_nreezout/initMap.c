
#include "mapBasic.h"
#include "../include/team.h"
#include "../include/queueTimer.h"
#include "../include/memoryUtil.h"
#include "../include/logfile.h"
#include "../include/builtins.h"

void MapInitialize()
{
    BuiltinsInitialize();
    CreateLogFile("nreezout-log.txt");
    InitializeSmartMemoryDestructor();
    InitializeQueueTimer();
    WriteLog("ok1");
    OnInitialMap();
    WriteLog("ok2");
    EnableCoopTeamMode();
    WriteLog("ok3");
}

void MapExit()
{
    NoxMusicEvent();
    OnShutdownMap();
    DisableCoopTeamMode();
}

