
#include "initialSearch.h"

#include "randomItem.h"

#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/logfile.h"

static int s_firstunit;
static int s_lastunit;

static void createRandomItem(int unit)
{
    float xy[]={NoxGetObjectX(unit),NoxGetObjectY(unit)};

    NoxDeleteObject(unit);
    DO_CREATE_RANDOMITEM(xy[0], xy[1]);
}

static void checkSearchUnit(int unit)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (!ptr)
        return;

    switch (ptr->thingType)
    {
    case 2672:
        createRandomItem(unit);
        break;
    }
}

#define UNITSEARCH_STEP 40
static void searchTillEnd(int cur)
{
    unsigned int count = UNITSEARCH_STEP;

    while (count > 0)
    {
        checkSearchUnit(cur + count);
        count -= 2;
    }
    if (cur < s_lastunit)
        NoxFrameTimerWithArg(1, cur + UNITSEARCH_STEP, &searchTillEnd);
    else
    {
        NoxPrintToAll("end scan");
    }    
}

void DoInitialSearch()
{
    WriteLog("doinitialsearch-start");
    WriteLog("doinitialsearch-end");
    s_lastunit = CreateObject("Mover", 1);
    s_firstunit=NoxGetObject("firstscan");
    NoxFrameTimerWithArg(1, s_firstunit + 1, &searchTillEnd);
}

