
#include "monster.h"
#include "mapUtility.h"
#include "../include/monsterBinscript.h"
#include "../include/queueTimer.h"
#include "../include/noxobject.h"
#include "../include/mathlab.h"
#include "../include/builtins.h"
#include "../include/memoryUtil.h"
#include "../include/objectIDdefines.h"

static MonsterBinScriptTable *s_horrendousData;

static uint8_t s_customMeleeAttackCode[]={
    0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
    0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
    0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
    0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
};

static void onHorrendousAttack();

static void initializeHorrendousData()
{
    MonsterBinScriptTable *data = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)data, sizeof(MonsterBinScriptTable), 0);
    data->hp = 1300;
    data->speed = 120;
    data->run_multiplier = 1.0f;
    data->damage = 22;
    data->meleeAttackFrame =5;
    data->damage_type = DAMAGE_BLADE;
    data->minMeleeAttackDelay = 6;
    data->maxMeleeAttackDelay = 11;
    data->meleeRange = 60.0f;
    // data->unit_flag = MON_STATUS_ALWAYS_RUN;
    data->strikeEvent = (int)s_customMeleeAttackCode;
    // data->deadEvent = MonsterBinTrollDeadEvent;
    int *hack = (int *)data->missile_name;

    hack[15] = (int)&onHorrendousAttack;
    s_horrendousData = data;
}

static void onHorrendousHurt()
{
    NoxEnchant(SELF, ENCHANT_INVULNERABLE, 8.0f);
    NoxRestoreHealth(SELF, 200);
}

static void onGuardHorrendous(int mon)
{
    if (NoxCurrentHealth(mon))
    {
        int owner = GetOwner(mon);

        if (NoxMaxHealth(owner))
        {
            PushTimerQueue(NoxRandomInteger(60, 90), mon, &onGuardHorrendous);
            if (NoxCurrentHealth(owner))
            {
                if (NoxIsVisible(mon, owner) || NoxIsVisible(owner, mon))
                    return;
                
                NoxPlayFX(FX_SMOKE_BLAST, NoxGetObjectX(mon), NoxGetObjectY(mon), 0, 0);
                NoxMoveObject(mon, NoxGetObjectX(owner) - UnitAngleCos(owner, 17.0f), NoxGetObjectY(owner) - UnitAngleSin(owner, 17.0f));
                NoxPlayFX(FX_TELEPORT, NoxGetObjectX(mon), NoxGetObjectY(mon), 0, 0);
                return;
            }
        }
        NoxDeleteObject(mon);
    }
}

static void onHorrendousSplash()
{
    if (NoxGetTrigger()==NoxGetCaller())
        return;

    if (NoxCurrentHealth(OTHER))
    {
        if (NoxIsAttackedBy(OTHER, SELF))
        {
            NoxDamage(OTHER, SELF, 100, DAMAGE_PLASMA);
            PlaySphericalEffect(OTHER);
            NoxRestoreHealth(SELF, 100);
        }
    }
}

static void onHorrendousAttack()
{
    if (NoxCurrentHealth(OTHER))
    {
        NoxObject *ptr = UnitToPtr(OTHER);

        SplashDamage(SELF, &ptr->unitX, 85.0f, &onHorrendousSplash);
        PlaySphericalEffect(SELF);
        PlaySummonEffect(&ptr->unitX, OBJ_WILL_O_WISP, 24);
    }
}

int CreateGuardianHorrendous(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_HORRENDOUS, xpos, ypos);

    ChangeMonsterBinScript(mon, s_horrendousData);
    SyncWithCustomBinScript(mon);
    NoxSetCallback(mon, UnitCallback_OnHit, &onHorrendousHurt);
    PushTimerQueue(10, mon, &onGuardHorrendous);
    return mon;
}

void InitializeMonster()
{
    initializeHorrendousData();
}
