
#ifndef RANDOM_ITEM_H__
#define RANDOM_ITEM_H__

int CreateRandomWeaponAt(float xpos, float ypos);
int CreateRandomArmorAt(float xpos, float ypos);
int CreateRandomStaffAt(float xpos, float ypos);
int CreateRandomPotionAt(float xpos, float ypos);
// int CreateGoldAt(float xpos, float ypos);
// int CreateTreasureAt(float xpos, float ypos);
int CreateHotpotion(float xpos, float ypos);

typedef int(*random_item_creator)(float, float);
extern random_item_creator *g_pRandItemFn;
extern unsigned int g_randItemFnLastindex;

#define DO_CREATE_RANDOMITEM(x, y) g_pRandItemFn[NoxRandomInteger(0, g_randItemFnLastindex)](x, y)

#endif

