
#include "arrowTraps.h"
#include "../include/stringUtil.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"

#define INTERACTIVE_PROC void

static void initArrowTrap(const char *tag, int *pArray, uint32_t count)
{
    char trpname[20];

    for (uint32_t u = 0 ; u < count ; u ++)
    {
        CopyString(tag, trpname);
        ConcatenateString(trpname, IntToString(u + 1));
        pArray[u] = NoxGetObject(trpname);
    }
}

static int s_deathArrows[11];
static int s_thunderArrowP[5];
static int s_thunderArrowR[5];
static int s_thunderArrow[5];
static int s_westTraps[9];

void InitMapArrowTraps()
{
    initArrowTrap("DeathArrow", s_deathArrows, sizeof(s_deathArrows)/sizeof(int));
    initArrowTrap("ThunderArrow", s_thunderArrow, sizeof(s_thunderArrow)/sizeof(int));
    initArrowTrap("ThunderArrowP", s_thunderArrowP, sizeof(s_thunderArrowP)/sizeof(int));
    initArrowTrap("ThunderArrowR", s_thunderArrowR, sizeof(s_thunderArrowR)/sizeof(int));
    initArrowTrap("WestTrap", s_westTraps, sizeof(s_westTraps)/sizeof(int));
}

typedef struct _arrowTrpHandler
{
    int *pTrps;
    uint32_t count;
} ArrowTrpHandler;

static void deferredTurnOff(ArrowTrpHandler *pHandler)
{
    for (int i = 0 ; i < pHandler->count ; i ++)
        NoxObjectOff(pHandler->pTrps[i]);
    FreeSmartMemory(pHandler);
}

static void triggeredArrowTrapCommon(ArrowTrpHandler *pHandler)
{
    for (int i = 0 ; i < pHandler->count ; i ++)
        NoxObjectOn(pHandler->pTrps[i]);
    NoxFrameTimerWithArg(1, (int)pHandler, (timer_callback)&deferredTurnOff);
}

#define TRAP_TRIGGERED(arry) \
    ArrowTrpHandler *pHandler = (ArrowTrpHandler *)AllocSmartMemory(sizeof(ArrowTrpHandler));   \
    pHandler->pTrps = arry;    \
    pHandler->count = sizeof(arry)/sizeof(int);    \
    triggeredArrowTrapCommon(pHandler); \


INTERACTIVE_PROC EnableArrowTrapGroup01()
{ TRAP_TRIGGERED(s_deathArrows) }

INTERACTIVE_PROC EnableArrowTrapGroup02()
{ TRAP_TRIGGERED(s_thunderArrow) }

INTERACTIVE_PROC EnableArrowTrapGroup03()
{ TRAP_TRIGGERED(s_thunderArrowP)}

INTERACTIVE_PROC EnableArrowTrapGroup04()
{ TRAP_TRIGGERED(s_thunderArrowR )}

INTERACTIVE_PROC ShotWestArrowTraps()
{TRAP_TRIGGERED(s_westTraps)}


