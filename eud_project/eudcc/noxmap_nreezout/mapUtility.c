
#include "mapUtility.h"
#include "../include/memoryUtil.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"

static uint8_t s_splashExecCode[]={
    0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
    0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
};

static uint8_t *createSplashCode(splash_trig_fn n)
{
    uint8_t *ret = (uint8_t *)MemAlloc(sizeof(s_splashExecCode));

    NoxByteMemcopy(s_splashExecCode, ret, sizeof(s_splashExecCode));
    *(int *)&ret[10] = (int)n;
    return ret;
}

typedef struct _splashObject
{
	NoxObject *pAttacker;
    uint8_t *pCode;
	float range;
	float *xyPoint;
} SplashObject;

static uint8_t s_splashHandleCore[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
    0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
};

void SplashDamage(int attacker, float *xy, float range, splash_trig_fn fn)
{
    SplashObject obj;

    obj.pAttacker = UnitToPtr(attacker);
    obj.range = range;
    obj.pCode = createSplashCode(fn);
    obj.xyPoint = xy;

    uint8_t *fnOff = s_splashHandleCore;

    BuiltinSingleArg((int)&obj, ((int)&fnOff - 0x5c308c) / 4);
    MemFree(obj.pCode);
}

static uint8_t s_sphericalFxCode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
    0xB8, 0x70, 0x36, 0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3
};

typedef struct _shpericalFxData
{
    int zero;
    NoxObject *ptr;
} SphericalData;

void PlaySphericalEffect(int unit)
{
    SphericalData data = {0, };
    uint8_t *fnOff = s_sphericalFxCode;

    data.ptr=UnitToPtr(unit);
    BuiltinSingleArg((int)&data, ((int)&fnOff - 0x5c308c) / 4);
}

static uint8_t s_summonFxCode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
    0xFF, 0x70, 0x08, 0xFF, 0x70, 0x0C, 0xFF, 0x70, 0x10, 0xB8, 0xF0, 0x36, 
    0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
};

typedef struct _summonEffectData
{
    int duration;
    short summonData;
    short unknown6;
    int unknown8;
    float *xyCoor;
    int unknown10;    
} SummonEffectData;

void PlaySummonEffect(float *xypos, short thingId, int duration)
{
    SummonEffectData data = {0, };

    data.duration=duration;
    data.summonData=thingId;
    data.xyCoor=xypos;
    uint8_t *fnOff = s_summonFxCode;

    BuiltinSingleArg((int)&data, ((int)&fnOff - 0x5c308c) / 4);
}
