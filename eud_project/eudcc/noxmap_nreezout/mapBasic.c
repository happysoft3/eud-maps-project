
#include "mapBasic.h"
#include "playerControl.h"
#include "npcs.h"
#include "initialSearch.h"
#include "arrowTraps.h"
#include "usershop.h"
#include "../include/noxobject.h"
#include "../include/printUtil.h"
#include "../include/fxeffect.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"
#include "group_info.h"
#include "../include/logfile.h"

#define INTERACTIVE_PROC void

static int s_lightningStatue;
static int s_fireballTrap;

static void showGreet()
{
    WidePrintMessageAll(u"***느리즈아웃- 시즌1 리메이크 -          제작- 태백광노 ***");
    NoxPlayAudioByIndex(SOUND_ManaBombCast, PLAYER_START_LOCATION);
    NoxDeleteObjectTimer(
        CreateObjectById(OBJ_MANA_BOMB_CHARGE, NoxGetWaypointX(PLAYER_START_LOCATION), NoxGetWaypointY(PLAYER_START_LOCATION)),
        150);
}

static void initPickets()
{
    SetPicketText(NoxGetObject("mpic1"), u"느리즈아웃-시즌1 리메이크 판 -- 제작. 태백광노");
    SetPicketText(NoxGetObject("mpic2"), u"게임을 시작하려면, 앞에 보이는 무지개 색 비콘에 들어가시오");
    SetPicketText(NoxGetObject("mpic3"), u"표지판: 설산 호텔 내부 휴게실");
    SetPicketText(NoxGetObject("mpic4"), u"느리즈아웃-시즌1 리메이크되었어요~~ 옛날맛 그대로.. 재미있게 즐겨보세요 - 태백광노");
    SetPicketText(NoxGetObject("mpic5"), u"표지판: 적군기지의 깃발- 저 깃발을 뺏으면 승리합니다");
    SetPicketText(NoxGetObject("mpic6"), u"이곳은 비상식량 창고임. 웨이브 6 이상에서 누르면 벽이 열립니다");
}

static void initialSettings()
{
    int *p=(int *)0x5d5330;

    *p=0x2000;
    p= (int *)0x5d5394;
    *p=1;
}

void OnInitialMap()
{
    NoxMusicEvent();
    initialSettings();
    WriteLog("oninitialmap");
    DoInitialSearch();
    WriteLog("oninitialmap");
    InitialUsershopDisposition();
    InitMapArrowTraps();
    
    WriteLog("oninitialmap");
    s_lightningStatue = NoxGetObject("LightningStatue");
    s_fireballTrap = NoxGetObject("nrFiretrp");
    WriteLog("oninitialmap");
    InitMapNPC();
    InitialPlayerControl();
    NoxSecondTimer(10, &showGreet);
    initPickets();
}

void OnShutdownMap()
{
    DeinitializePlayerControl();
}

INTERACTIVE_PROC YouAreWinner()
{
    NoxObjectOff(SELF);
    WidePrintMessageAll(u"승리__ 축하합니다! 승리하셨습니다!---");
    PlaySoundAround(OTHER, SOUND_FlagCapture);
    NoxPlayFX(FX_WHITE_FLASH, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 0,0);
    CreateObjectById(OBJ_LEVEL_UP, NoxGetObjectX(SELF), NoxGetObjectY(SELF));
}

INTERACTIVE_PROC shotLightningTouch()
{
    NoxDamage(OTHER, s_lightningStatue, 35, DAMAGE_ELECTRIC);
    NoxPlayFX(FX_LIGHTNING, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER), NoxGetObjectX(s_lightningStatue), NoxGetObjectY(s_lightningStatue));
    PlaySoundAround(OTHER, SOUND_LightningBolt);
}

static void deferredTurnOffFiretrap()
{
    NoxObjectOff(s_fireballTrap);
}

INTERACTIVE_PROC shotFireball()
{
    NoxObjectOn(s_fireballTrap);
    NoxFrameTimer(1, &deferredTurnOffFiretrap);
}

INTERACTIVE_PROC WellOfRestoration()
{
    NoxRestoreHealth(OTHER, 70);
    PlaySoundAround(OTHER, SOUND_LongBellsUp);
    NoxPlayFX(FX_GREATER_HEAL, NoxGetObjectX(SELF), NoxGetObjectY(SELF), NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

INTERACTIVE_PROC VictoryEvent()
{
    NoxObjectOff(SELF);
    WidePrintMessageAll(u"***You're Winner! 축하합니다-! 당신들이 이겼습니다***");
    CreateObjectById(OBJ_LEVEL_UP, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
    PlayFxGreenExplosion(NoxGetObjectX(SELF), NoxGetObjectY(SELF));
    PlaySoundAround(OTHER, SOUND_LevelUp);
}

INTERACTIVE_PROC OpenRewardRoom()
{
    if (GetCurrentWave() >= 6)
    {
        NoxObjectOff(SELF);
        WidePrintMessageAll(u"비상식량 창고를 개방하였습니다");
        NoxWallGroupOpen(WALLGROUP_REWARD_ROOM);
    }
    else
    {
        WidePrintMessage(OTHER, u"이 비상식량 창고는 웨이브 6 이상에서 열 수 있습니다");
        PlaySoundAround(OTHER, SOUND_NoCanDo);
    }
    
}
