
#ifndef MAP_UTILITY_H__
#define MAP_UTILITY_H__

typedef void(*splash_trig_fn)(void);
void SplashDamage(int attacker, float *xy, float range, splash_trig_fn fn);
void PlaySphericalEffect(int unit);
void PlaySummonEffect(float *xypos, short thingId, int duration);

#endif
