
#include "usershop.h"
#include "fixdialogsys.h"
// #include "monsters.h"
#include "playerControl.h"
#include "monster.h"
#include "../include/logfile.h"
// #include "../include/equipment.h"
#include "../include/printUtil.h"
#include "../include/fxeffect.h"
#include "../include/noxobject.h"
// #include "../include/memoryUtil.h"
#include "../include/builtins.h"
#include "../include/sound_define.h"
#include "../include/objectIDdefines.h"


static int powerupEquip(int owner)
{
    int inv = NoxGetLastItem(owner), count = 0;

    while (inv)
    {
        if (!NoxHasEnchant(inv, ENCHANT_RUN))   //몇몇 power무기때문에 무적화 하면 안됩니다
        {
            if (!NoxHasEnchant(inv, ENCHANT_INVULNERABLE))
            {
                NoxEnchant(inv, ENCHANT_INVULNERABLE, 0);
                ++count;
            }
        }
        inv = NoxGetPreviousItem(inv);
    }
    return count;
}

static void onRepairTrade()
{
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 8500){
        int count = powerupEquip(OTHER);

        if (!count)
        {
            WideSayMessage(SELF, u"처리실패!- 무적화 할 것이 없네요..", 90);
            return;
        }
        NoxChangeGold(OTHER, -8500);
        WideSayMessage(SELF, u"처리성공!- 장비가 무적화 되었어요. 다음에 또 와요", 120);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(8,500 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }    
}

static void tryRepair()
{
    WideSayMessage(SELF, u"인벤토리를 무적화 하시겠어요? 8,500골드가 필요합니다", 150);
    TellStoryWithName("GuiInv.c:WarriorRepair", u"인벤무적화 8500골드");
    PlaySoundAround(SELF, SOUND_SwordsmanRecognize);
}

static void placeRepairman(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_SWORDSMAN, xpos, ypos);

    WRITE_LOG(IntToString(man));
    NoxSetupDialog(man, DIALOG_YESNO, &tryRepair, &onRepairTrade);
    WRITE_LOG("OK");
    NoxLookWithAngle(man, 225);
}

static void tradeAllbuff()
{
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 45000){
        if (!TryAllBuff(NoxGetCaller()))
            return;
        NoxPlayFX(FX_YELLOW_SPARKS, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER),0,0);
        NoxChangeGold(OTHER, -45000);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(45,000 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }
}

static void descrAllbuff()
{
    WideSayMessage(SELF, u"올엔첸을 구입하시겠어요? 4만5천골드가 필요합니다", 150);
    TellStoryWithName("ParseCmd.c:GodSet", u"올엔첸 구입\n4만5천 골드");
    PlaySoundAround(SELF, SOUND_Maiden2Talkable);
}

static void placeAllbuffshop(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_WIZARD_GREEN, xpos, ypos);

    NoxSetupDialog(man, DIALOG_YESNO, &descrAllbuff, &tradeAllbuff);
    NoxLookWithAngle(man, 225);
}

static void tradeWindboost()
{
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 15000){
        if (!TryAwardSkill(NoxGetCaller()))
            return;
        NoxPlayFX(FX_YELLOW_SPARKS, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER),0,0);
        NoxChangeGold(OTHER, -15000);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(15,000 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }
}

static void descrWindboost()
{
    WideSayMessage(SELF, u"윈드부스터를 구입하시겠어요? (15,000골드)", 150);
    TellStoryWithName("ParseCmd.c:GodSet", u"부스터 구입\n1만5천 골드");
    PlaySoundAround(SELF, SOUND_Maiden2Talkable);
}

static void placeWindboost(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_BLACK_WOLF, xpos, ypos);

    NoxSetupDialog(man, DIALOG_YESNO, &descrWindboost, &tradeWindboost);
    NoxLookWithAngle(man, 225);
}

static void invokeDeferredFollow(int cre)
{
    int owner = GetOwner(cre);

    if (NoxCurrentHealth(owner))
    {
        NoxCreatureFollow(cre, owner);
        NoxAggressionLevel(cre, 1.0f);
    }
}

static void deferredCreatureFollow(int owner, int cre)
{
    NoxSetOwner(owner, cre);
    NoxFrameTimerWithArg(1, cre, &invokeDeferredFollow);
}

static void tradeHorrendous()
{
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 48000){
        int guardian = CreateGuardianHorrendous(NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));

        deferredCreatureFollow(OTHER, guardian);
        NoxPlayFX(FX_YELLOW_SPARKS, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER),0,0);
        NoxChangeGold(OTHER, -48000);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(48,000 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }
}

static void descrHorrendous()
{
    WideSayMessage(SELF, u"호렌더스를 구입하시겠어요? (48,000골드)", 150);
    TellStoryWithName("ParseCmd.c:GodSet", u"호렌더스 구입\n4만8천 골드");
    PlaySoundAround(SELF, SOUND_Maiden2Talkable);
}

static void placeHorrendousSummon(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_HORRENDOUS, xpos, ypos);

    NoxSetupDialog(man, DIALOG_YESNO, &descrHorrendous, &tradeHorrendous);
    NoxLookWithAngle(man, 225);
}

void InitialUsershopDisposition()
{
    InitializeMonster();
    OnInitialFixDialogSys();
    placeRepairman(NoxGetWaypointX(220), NoxGetWaypointY(220));
    placeAllbuffshop(NoxGetWaypointX(221), NoxGetWaypointY(221));
    placeWindboost(NoxGetWaypointX(222), NoxGetWaypointY(222));
    placeHorrendousSummon(NoxGetWaypointX(229), NoxGetWaypointY(229));
}
