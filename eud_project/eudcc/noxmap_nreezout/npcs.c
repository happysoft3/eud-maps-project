
#include "npcs.h"
#include "randomItem.h"
#include "../include/fxeffect.h"
#include "../include/noxobject.h"
#include "../include/memoryUtil.h"
#include "../include/printUtil.h"
#include "../include/stringUtil.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

#include "../include/logfile.h"
#include "group_info.h"

#define INTERACTIVE_PROC void

// #define ERROR_TEST

static char *s_stagePrefix[]={
    "Stage1-",
    "Stage2-",
    "Stage3-",
    "Stage4-",
    "Stage5-",
    "Stage6-",
    "Stage7-",
};

#define LEVEL_1_NPC_COUNT 12
#define LEVEL_2_NPC_COUNT 15
#define LEVEL_3_NPC_COUNT 19
#define LEVEL_4_NPC_COUNT 25
#define LEVEL_5_NPC_COUNT 30
#define LEVEL_6_NPC_COUNT 36
#define LEVEL_7_NPC_COUNT 50

static int s_lv1NpcArray[LEVEL_1_NPC_COUNT];
static int s_lv2NpcArray[LEVEL_2_NPC_COUNT];
static int s_lv3NpcArray[LEVEL_3_NPC_COUNT];
static int s_lv4NpcArray[LEVEL_4_NPC_COUNT];
static int s_lv5NpcArray[LEVEL_5_NPC_COUNT];
static int s_lv6NpcArray[LEVEL_6_NPC_COUNT];
static int s_lv7NpcArray[LEVEL_7_NPC_COUNT];

static void setGodItem(int npc)
{
    NoxObject *ptr = UnitToPtr(NoxGetLastItem(npc));

    while (ptr)
    {
        if (ptr->Class & (UNIT_CLASS_WEAPON | UNIT_CLASS_ARMOR | UNIT_CLASS_WAND))
        {
            NoxEnchant(ptr->globalID, ENCHANT_INVULNERABLE, 0);
            // ptr->dropFnPtr = (void *)0x408fdb;
        }
        ptr = ptr->nextInventoryObj;
    }
}

static void initNpcSingle(char *name)
{
    int npc=NoxGetObject(name);

    if (npc)
    {
        setGodItem(npc);
        #ifdef ERROR_TEST
        NoxEnchant(npc, ENCHANT_VAMPIRISM, 0);
        #endif
        NoxObjectOff(npc);
        NoxFrozen(npc, TRUE);
    }
    else
    {
        WriteLog("error");
    }
        WriteLog(name);
    
}

static void initNpcEachLevel(int lv, int count, int *pNpcArr)
{
    char *prefix = s_stagePrefix[lv-1];
    char npcname[20];

    for (int i = 0 ; i < count ; ++i)
    {
        CopyString(prefix, npcname);
        ConcatenateString(npcname, IntToString(i+1));
        initNpcSingle(npcname);
        pNpcArr[i] = NoxGetObject(npcname);
    }
}

static int s_npcRespLocations[]={
    23,35,89,88,38,39,153,65,207,48,208,24,50,94,95,209,97,98,99,100,101,102,27,103,211,210,203,109,155,33,34,36,37,27,28,
    152,46,165,205,204,113,114,206,116,117,118,87,119,68,59,53,63
};

static void nextNpcHandler();

static void deferredDropItem(int pos)
{
    DO_CREATE_RANDOMITEM(NoxGetObjectX(pos), NoxGetObjectY(pos));
    NoxDeleteObject(pos);
}

static void removeEquipments(int npc)
{
    int inv = npc+2;
    NoxObject *ptr = UnitToPtr(inv);

    while (ptr)
    {
        if (ptr->Class & (UNIT_CLASS_ARMOR|UNIT_CLASS_WAND|UNIT_CLASS_WEAPON|UNIT_CLASS_FOOD))
        {
            NoxDeleteObject(inv);
            inv += 2;
            ptr = UnitToPtr(inv);
        }
        else
        {
            break;
        }
    }
}

INTERACTIVE_PROC OnNPCDead()
{
    nextNpcHandler();
    removeEquipments(NoxGetTrigger());
    NoxFrameTimerWithArg(4, CreateObjectById(OBJ_BIG_SMOKE, NoxGetObjectX(SELF), NoxGetObjectY(SELF)), &deferredDropItem);
}

static void releaseNpc(int npc)
{
    int dest = s_npcRespLocations[NoxRandomInteger(0, (sizeof(s_npcRespLocations)/sizeof(s_npcRespLocations[0]))-1)];

    NoxFrozen(npc, FALSE);
    NoxObjectOn(npc);
    NoxAggressionLevel(npc, 1.0f);
    NoxSetCallback(npc, UnitCallback_OnDeath, &OnNPCDead);
    NoxMoveObject(npc, NoxGetWaypointX(dest), NoxGetWaypointY(dest));
}

static int *s_npcArrayPtr[]={
    s_lv1NpcArray,s_lv2NpcArray,s_lv3NpcArray,s_lv4NpcArray,
    s_lv5NpcArray,s_lv6NpcArray,s_lv7NpcArray
};

static int s_npcCountArray[]={
    0,
    LEVEL_1_NPC_COUNT, LEVEL_2_NPC_COUNT, LEVEL_3_NPC_COUNT,
    LEVEL_4_NPC_COUNT, LEVEL_5_NPC_COUNT, LEVEL_6_NPC_COUNT,
    LEVEL_7_NPC_COUNT,0
};

static void releaseArea(int level)
{
    int *p = s_npcArrayPtr[level];
    int count = s_npcCountArray[level+1];

    for (int i = 0 ; i < count ; i++)
        releaseNpc(p[i]);
}

#ifdef ERROR_TEST
static int s_npcCurrentGage = LEVEL_5_NPC_COUNT;
static int s_currentWave = 5;
#else
static int s_npcCurrentGage;
static int s_currentWave;
#endif

int GetCurrentWave()
{
    return s_currentWave;
}

static void endThisGame()
{
    NoxWallGroupOpen(WALLGROUP_LastFlagwallsgroup);
    WidePrintMessageAll(u"[!!] 파란진영의 깃발을 둘러싸고 있는 마법벽이 무력화되었습니다");
}

static void nextNpcHandler()
{
    if ((++s_npcCurrentGage) >= s_npcCountArray[s_currentWave])
    {
        s_npcCurrentGage = 0;
        //여기에서 다음 에리아 시작합니다
        if (s_currentWave >= 7)
        {
            endThisGame();
            return;
        }
        NoxSecondTimerWithArg(15, s_currentWave++, &releaseArea);
        WidePrintMessageAll(u"잠시 후 다음 에리아가 시작될 것입니다");
    }
}

void InitMapNPC()
{
    NoxSecondTimer(20, &nextNpcHandler);
    initNpcEachLevel(1, LEVEL_1_NPC_COUNT, s_lv1NpcArray);
    initNpcEachLevel(2, LEVEL_2_NPC_COUNT, s_lv2NpcArray);
    initNpcEachLevel(3, LEVEL_3_NPC_COUNT, s_lv3NpcArray);
    initNpcEachLevel(4, LEVEL_4_NPC_COUNT, s_lv4NpcArray);
    initNpcEachLevel(5, LEVEL_5_NPC_COUNT, s_lv5NpcArray);
    initNpcEachLevel(6, LEVEL_6_NPC_COUNT, s_lv6NpcArray);
    initNpcEachLevel(7, LEVEL_7_NPC_COUNT, s_lv7NpcArray);
}

#define SIMPLE_BERSERKER_FORCE 60.0f

static void simpleBerserkerImpl()
{
    float meXY[]={NoxGetObjectX(SELF), NoxGetObjectY(SELF)};
    float targXY[]={NoxGetObjectX(OTHER), NoxGetObjectY(OTHER)};

    if (NoxIsVisible(SELF, OTHER))
    {
        NoxLookAtObject(SELF, OTHER);
        NoxPushObject(SELF, SIMPLE_BERSERKER_FORCE, meXY[0] + meXY[0] - NoxGetObjectX(OTHER), meXY[1]+meXY[1]-NoxGetObjectY(OTHER));
        PlaySoundAround(SELF, SOUND_BerserkerChargeInvoke);
    }
}

INTERACTIVE_PROC WarWhenEnemyIsInSight()
{
    if (NoxHasEnchant(SELF, ENCHANT_DETECTING))
        return;

    if (DistanceUnitToUnit(SELF, OTHER) < SIMPLE_BERSERKER_FORCE)
    {
        simpleBerserkerImpl();
        NoxEnchant(SELF, ENCHANT_DETECTING, 30.0f);
        NoxEnchant(SELF, ENCHANT_BURNING, 0.5f);
    }
}

INTERACTIVE_PROC WarWhenTouched()
{
    if (NoxHasEnchant(SELF, ENCHANT_BURNING))
    {
        NoxEnchantOff(SELF, ENCHANT_BURNING);
        NoxDamage(OTHER, SELF, 150, DAMAGE_BLADE);
    }
}

INTERACTIVE_PROC WarWhenEnemyIsInSight1()
{
    WarWhenEnemyIsInSight();
}

INTERACTIVE_PROC WarWhenTouched1()
{
    WarWhenTouched();
}

static void conjPattern1()
{
    NoxCastSpellObjectObject(SPELL_VAMPIRISM, SELF, SELF);
    NoxCastSpellObjectObject(SPELL_STUN, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_TOXIC_CLOUD, SELF, OTHER);
}

static void conjPattern2()
{
    NoxCastSpellObjectObject(SPELL_STUN, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_FIST, SELF, OTHER);
    NoxHitFarLocation(SELF, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

static void conjPattern3()
{
    NoxCastSpellObjectObject(SPELL_METEOR, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_SLOW, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_SUMMON_DEMON, SELF, OTHER);
}

static void conjPattern4()
{
    NoxCastSpellObjectObject(SPELL_METEOR, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_PIXIE_SWARM, SELF, SELF);
    NoxCastSpellObjectObject(SPELL_BLINK, SELF, SELF);
}

static void conjPattern5()
{
    NoxCastSpellObjectObject(SPELL_PIXIE_SWARM, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_FIST, SELF, OTHER);
    NoxHitFarLocation(SELF, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

static void conjPattern6()
{
    NoxCastSpellObjectObject(SPELL_TOXIC_CLOUD, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_FIST, SELF, OTHER);
    NoxHitFarLocation(SELF, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

static void conjPattern7()
{
    NoxCastSpellObjectObject(SPELL_SUMMON_MECHANICAL_GOLEM, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_VAMPIRISM, SELF, SELF);
    NoxHitFarLocation(SELF, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

static void conjPattern8()
{
    NoxCastSpellObjectObject(SPELL_METEOR, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_BLINK, SELF, SELF);
}

static void conjPattern9()
{
    NoxCastSpellObjectObject(SPELL_SUMMON_GHOST, SELF, OTHER);
    NoxCastSpellObjectObject(SPELL_PIXIE_SWARM, SELF, SELF);
    NoxHitFarLocation(SELF, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

typedef void(*conj_pattern_fn)(void);

static conj_pattern_fn s_conjPattern[]={
    conjPattern1, conjPattern2, conjPattern3, conjPattern4,
    conjPattern5, conjPattern6, conjPattern7, conjPattern8,
    conjPattern9
};

INTERACTIVE_PROC ConWhenEnemyIsInSight0()
{
    if (NoxHasEnchant(SELF, ENCHANT_ANTI_MAGIC))
    {
        NoxRunAway(SELF, OTHER, 5);
        return;
    }
    if (NoxHasEnchant(SELF, ENCHANT_DETECTING))
        return;

    NoxLookAtObject(SELF, OTHER);
    NoxEnchant(SELF, ENCHANT_DETECTING, NoxRandomFloat(5.0f, 10.0f));
    s_conjPattern[NoxRandomInteger(0, (sizeof(s_conjPattern)/sizeof(s_conjPattern[0]))-1) ]();
}

INTERACTIVE_PROC ConWhenEnemyIsInSight1()
{
    ConWhenEnemyIsInSight0();
}

typedef struct _deferredTarget
{
    int cast;
    int target;
} DeferredTarget;

static void triggeredDeathray(DeferredTarget *d)
{
    if (NoxCurrentHealth(d->cast))
    {
        if (NoxIsVisible(d->cast, d->target))
            NoxCastSpellObjectObject(SPELL_DEATH_RAY, d->cast, d->target);
    }
    NoxDeleteObject(d->target);
    FreeSmartMemory(d);
}

static void deferredDeathray(int cast, int target)
{
    DeferredTarget *d = (DeferredTarget *)AllocSmartMemory(sizeof(DeferredTarget));

    d->cast = cast;
    d->target = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, NoxGetObjectX(target), NoxGetObjectY(target));
    NoxFrameTimerWithArg(4, (int)d, (timer_callback)&triggeredDeathray);
}

static void wizPattern1()
{
    NoxCastSpellObjectLocation(SPELL_BURN, SELF, NoxGetObjectX(OTHER) + NoxRandomFloat(-23.0f, 23.0f), NoxGetObjectY(OTHER) + NoxRandomFloat(-23.0f, 23.0f));
    deferredDeathray(NoxGetTrigger(), NoxGetCaller());
}

static void wizPattern2()
{
    NoxCastSpellObjectObject(SPELL_SHIELD, SELF, SELF);
    NoxCastSpellObjectObject(SPELL_FIREBALL, SELF, OTHER);
}

static void wizPattern3()
{
    NoxCastSpellObjectObject(SPELL_SHOCK, SELF, SELF);
    NoxCastSpellObjectObject(SPELL_MAGIC_MISSILE, SELF, OTHER);
}

static void wizPattern4()
{
    NoxCastSpellObjectObject(SPELL_SHIELD, SELF, SELF);
    NoxCastSpellObjectObject(SPELL_MAGIC_MISSILE, SELF, OTHER);
}

static void wizPattern5()
{
    NoxCastSpellObjectObject(SPELL_SHOCK, SELF, SELF);
    NoxPauseObject(SELF, NoxRandomInteger(60, 90));
    NoxCastSpellObjectObject(NoxRandomInteger(0, 1) ? SPELL_LIGHTNING : SPELL_CHAIN_LIGHTNING, SELF, OTHER);
}

typedef void(*wiz_pattern_fn)(void);

static wiz_pattern_fn s_wizPattern[]={
    wizPattern1, wizPattern2, wizPattern3, wizPattern4, wizPattern5
};

INTERACTIVE_PROC WhenEnemyIsInSight0()
{
    if (NoxHasEnchant(SELF, ENCHANT_ANTI_MAGIC))
    {
        NoxRunAway(SELF, OTHER, 5);
        return;
    }
    if (NoxHasEnchant(SELF, ENCHANT_DETECTING))
        return;

    NoxLookAtObject(SELF, OTHER);
    NoxEnchant(SELF, ENCHANT_DETECTING, NoxRandomFloat(5.0f, 10.0f));
    s_wizPattern[NoxRandomInteger(0, (sizeof(s_wizPattern)/sizeof(s_wizPattern[0]))-1) ]();
}

INTERACTIVE_PROC WhenEnemyIsInSight1()
{
    WhenEnemyIsInSight0();
}
