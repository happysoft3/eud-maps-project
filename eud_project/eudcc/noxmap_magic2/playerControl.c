
#include "playerControl.h"
#include "../include/playerHandler.h"
#include "playerUtil2.h"
#include "../include/mathlab.h"
#include "../include/fxeffect.h"
#include "../include/spellUtils.h"
#include "../include/playerInfo.h"
#include "../include/noxobject.h"
#include "../include/stringUtil.h"
#include "../include/printUtil.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

#define NULLPTR (void *)0

#define PLAYER_DEATH_FLAG 2
#define PLAYER_WINDBOOST 4
#define PLAYER_FLAG_ALLBUFF 8

#define PLAYER_DATA_MAX 32
static PlayerData s_playerData[PLAYER_DATA_MAX];
static PlayerData *s_firstPlayerData = NULLPTR;

#include "monExpInfo.h"

static int s_monExpTable[]={
    /*0*/MON_GIANTLEECH_EXP,0, MON_ZOMBIE_EXP, MON_VILEZOMBILE_EXP, MON_SHADE_EXP, /*5*/ MON_GOON_EXP,0,0,MON_BLACKBEAR_EXP, MON_WHITEWOLF_EXP,
    /*10*/0,MON_WOLF_EXP,MON_FIREFAIRY_EXP,MON_PLANT_EXP, MON_MIMIC_EXP, /*15*/ MON_SCORPION_EXP,0,0,0,0,
    /*20*/0,0,0,0,0, /*25*/ MON_HECUBAH_EXP,MON_HECUBAHORB_EXP,0,MON_HORRENDOUS_EXP,MON_XANDOR_EXP,
    /*30*/MON_BEAST_EXP,MON_OGRELORD_EXP,MON_OGRE_EXP,MON_NECROMANCER_EXP,0, /*35*/ 0,0,0,0,0,
    /*40*/0,0,MON_TROLL_EXP,0,0, /*45*/ 0,0,0,0,0,
    /*50*/0,0,0,MON_SKULL_EXP,MON_SKULL_LORD_EXP, /*55*/ 0,MON_GARGOYLE_EXP,MON_MECA_GOLEM_EXP,0,0,
    /*60*/0,0,0,0,MON_GHOST_EXP, /*65*/ 0,0,0,0,0,
    /*70*/MON_WASP_EXP,0,0,MON_REDWIZ_EXP,MON_DRYAD_EXP, /*75*/ MON_OGREAXE_EXP,MON_EMBERDEMON_EXP,MON_EMBERDEMON_EXP,MON_URCHIN_EXP,MON_SHAMAN_EXP,
    /*80*/MON_LICH_EXP,0,0,0,MON_ARCHER_EXP, /*85*/ MON_SWORDSMAN_EXP,0,0,0,0,
    /*90*/0,MON_SPITTING_SPIDE_EXP,MON_SPIDER_EXP,0,MON_ALBINOSPIDER_EXP, /*95*/ MON_SMALLALBINOSPIDER_EXP,MON_BAT_EXP,
    };

static int s_playerExpTable[]={
    15, 40, 100, 205, 343,
    554, 837, 1190, 1617, 2126,
    2750, 3483, 4329, 5321, 6427,
    9999999,
};

static void setPlayerStrength(int plrUnit, int setTo)
{
    int pIndex = GetPlayerIndex(plrUnit);

    if (pIndex != -1)
    {
        char *p = (char *)0x62f1d8 + (pIndex * 0x12dc);

        p[0x8bf]=(char)setTo;
    }
}

static int getPlayerStrength(int plrUnit)
{
    int pIndex = GetPlayerIndex(plrUnit);

    if (pIndex != -1)
    {
        char *p = (char *)0x62f1d8 + (pIndex * 0x12dc);

        return (int)p[0x8bf];
    }
    return 0;
}

#define WAR_RESET_HP 150
#define WAR_RESET_STRENGTH 125

#define WAR_INCREASE_HP 20
#define WAR_INCREASE_STRENGTH 10

#define WAR_INIT_HP 30
#define WAR_INIT_STRENGTH 40

static void resetPlayerState(int plrUnit)
{
    if (NoxMaxHealth(plrUnit))  //TODO. 전사일 경우에만 초기화를 진행한다
    {
        setPlayerStrength(plrUnit, WAR_RESET_STRENGTH);
        SetUnitMaxHealth(plrUnit, WAR_RESET_HP);
        UpdatePlayer(plrUnit);
    }
}

static void onPlayerLevelup(PlayerData *pdata)
{
    int unitId = pdata->unitId;

    if (pdata->allowed)
    {
        WidePrintMessage(unitId, u"--LEVEL-UP-- 레밸이 올라갔습니다");
        NoxDeleteObjectTimer( CreateObjectById(OBJ_LEVEL_UP, NoxGetObjectX(unitId), NoxGetObjectY(unitId)), 60);
        PlaySoundAround(unitId, SOUND_LevelUp);

        //전사일 경우에만 이 로직을 태운다//
        setPlayerStrength(unitId, getPlayerStrength(unitId) + WAR_INCREASE_STRENGTH);
        SetUnitMaxHealth(unitId, NoxMaxHealth(unitId) + WAR_INCREASE_HP);
        UpdatePlayer(unitId);
    }
}

void PlayerKillEvent(int victim, int attacker)
{
    int pIndex = GetPlayerIndex(attacker);
    PlayerData *pdata;
    NoxObject *pVictimPtr = UnitToPtr(victim);

    if (pIndex != -1)
    {
        pdata = &s_playerData[pIndex];
        pdata->expLevel += s_monExpTable[pVictimPtr->thingType % 97];
        while (pdata->expLevel >= s_playerExpTable[pdata->level])
        {
            ++pdata->level;
            onPlayerLevelup(pdata);
        }
    }
}

static void OnPlayerLoop();

void InitialPlayerControl()
{
    InitialPlayerUpdateRewritten();
    InitPlayerUtil2();
    NoxFrameTimer(1, &OnPlayerLoop);
}

static void resetAll()
{
    PlayerData *pdata = s_firstPlayerData;

    while (pdata)
    {
        if (NoxMaxHealth(pdata->unitId))
        {
            if (pdata->allowed)
                resetPlayerState(pdata->unitId);
        }
        pdata = pdata->pNext;
    }
}

void DeinitializePlayerControl()
{
    ResetAllPlayerHandler();
    resetAll();
}

static void doWindboost(int plrUnit)
{
    NoxPushObjectTo(plrUnit, UnitAngleCos(plrUnit, 90.0f), UnitAngleSin(plrUnit, 90.0f));
    NoxPlayFX(FX_RICOCHET, NoxGetObjectX(plrUnit), NoxGetObjectY(plrUnit), 0,0);
}

static void applyAllBuff(int user){
    NoxEnchant(user, ENCHANT_CROWN, 0);
    NoxEnchant(user, ENCHANT_VAMPIRISM, 0);
    NoxEnchant(user, ENCHANT_HASTED, 0);
    NoxEnchant(user, ENCHANT_INFRAVISION, 0);
    NoxEnchant(user, ENCHANT_REFLECTIVE_SHIELD, 0);
    NoxEnchant(user, ENCHANT_PROTECT_FROM_ELECTRICITY, 0);
    NoxEnchant(user, ENCHANT_PROTECT_FROM_FIRE, 0);
    NoxEnchant(user, ENCHANT_PROTECT_FROM_POISON, 0);
}

static void emptyAll(int user)
{
    while (NoxIsObjectOn(NoxGetLastItem(user)))
        NoxDeleteObject(NoxGetLastItem(user));
}

static void playerOnAlive(PlayerData *pdata)
{
    if (NoxHasEnchant(pdata->unitId, ENCHANT_SNEAK))
    {
        NoxEnchantOff(pdata->unitId, ENCHANT_SNEAK);
        RemoveEffectTreadLightly(pdata->unitId);
        if (pdata->flags & PLAYER_WINDBOOST)
            doWindboost(pdata->unitId);
    }
}

static void playerOnDeath(PlayerData *pdata)
{
    if (!pdata->userName)
        return;

    short msg[128];

    CopyWideString(pdata->userName, msg);
    static short *myMent = u" 이(가) 적에게 격추되었습니다";
    ConcatenateWideString(msg, myMent);
    WidePrintMessageAll(msg);
}

//나갔거나 관객모드일 때,
static PlayerData *erasePlayerData(PlayerData *prev, PlayerData *cur)
{
    if (NoxMaxHealth(cur->unitId))
    {
        if (cur->allowed)
            resetPlayerState(cur->unitId);
    }

    cur->userName = NULLPTR;
    if (prev == NULLPTR)
    {
        s_firstPlayerData = cur->pNext;
        return s_firstPlayerData;
    }
    prev->pNext = cur->pNext;
    return cur->pNext;
}

static PlayerData *loadPlayerData(int plrUnit)
{
    PlayerData *pdata = s_firstPlayerData;

    while (pdata)
    {
        if (plrUnit == pdata->unitId)
            return pdata;

        pdata = pdata->pNext;
    }
    return NULLPTR;
}

static void setPlayerInitState(int plrUnit)
{
    PlayerData *pdata = loadPlayerData(plrUnit);

    if (!pdata)
        return;

    if (pdata->allowed)
    {
        SetUnitMaxHealth(plrUnit, WAR_INIT_HP);
        setPlayerStrength(plrUnit, WAR_INIT_STRENGTH);
        UpdatePlayer(plrUnit);
    }
}

static void onPlayerInit(int plrUnit)
{
    emptyAll(plrUnit);
    if (NoxGetGold(plrUnit))
        NoxChangeGold(plrUnit, -NoxGetGold(plrUnit));
    ChangePlayerDieHandler(plrUnit);
    DisablePlayerSelfDamage(plrUnit);

    //. 여기에서 사용자의 초기스텟을 설정한다//
    setPlayerInitState(plrUnit);
}

static PlayerData *appendPlayer(int plrUnit)
{
    int pIndex = GetPlayerIndex(plrUnit);

    if (pIndex < 0)
        return NULLPTR;

    PlayerData *pdata = &s_playerData[pIndex];

    pdata->unitId = plrUnit;
    pdata->flags = 0;
    pdata->pIndex = pIndex;
    pdata->pNext = s_firstPlayerData;
    pdata->userName = PlayerIngameNickname(plrUnit);
    s_firstPlayerData = pdata;
    pdata->level=0;
    pdata->expLevel=0;
    pdata->allowed=(NoxMaxHealth(plrUnit)==150);
    onPlayerInit(plrUnit);
    return pdata;
}

static int playerHandler(PlayerData *pdata)
{
    if (NoxMaxHealth(pdata->unitId))
    {
        NoxObject *ptr = UnitToPtr(pdata->unitId);

        if (ptr->flags & UNIT_FLAG_NO_COLLIDE)
            return 0;

        else if (NoxCurrentHealth(pdata->unitId))
        {
            playerOnAlive(pdata);
        }
        else if (!(pdata->flags & PLAYER_DEATH_FLAG))
        {
            playerOnDeath(pdata);
            pdata->flags ^= PLAYER_DEATH_FLAG;
        }
        return 1;
    }
    return 0;
}

static void OnPlayerLoop()
{
    PlayerData *pdata = s_firstPlayerData, *prev = NULLPTR;
    int res = 0;

    while (pdata)
    {
        if (!playerHandler(pdata))
            pdata = erasePlayerData(prev, pdata);   //다음 노드를 반환한다
        else
        {
            prev = pdata;
            pdata = pdata->pNext;
        }
    }
    NoxFrameTimer(1, &OnPlayerLoop);
}

int GetClosestPlayer(int unit)
{
    PlayerData *pdata = s_firstPlayerData;
    float temp = 8192.0f, r = 0.0f;
    int ret = 0;

    while (pdata)
    {
        if (NoxCurrentHealth(pdata->unitId))
        {
            r = DistanceUnitToUnit(unit, pdata->unitId);
            if (r < temp)
            {
                temp = r;
                ret = pdata->unitId;
            }
        }
        pdata = pdata->pNext;
    }
    return ret;
}

static void playerJoinOnMap(PlayerData *pdata)
{
    int plrUnit = pdata->unitId;

    if (pdata->flags & PLAYER_DEATH_FLAG)
        pdata->flags ^= PLAYER_DEATH_FLAG;
    if (pdata->flags & PLAYER_FLAG_ALLBUFF)
        applyAllBuff(plrUnit);

    NoxMoveObject(plrUnit, NoxGetWaypointX(PLAYER_START_LOCATION), NoxGetWaypointY(PLAYER_START_LOCATION));
    NoxPlayAudioByIndex(SOUND_BlindOff, PLAYER_START_LOCATION);
}

static int registPlayer(int plrUnit, PlayerData *pdata)
{
    if (NoxCurrentHealth(plrUnit))
    {
        if (!pdata) //새로운 사용자인 경우,
            pdata = appendPlayer(plrUnit);

        if (!pdata)
        {
            NoxPrintToAll("error");
            return FALSE;
        }
        playerJoinOnMap(pdata);
        return TRUE;
    }
    return FALSE;
}

#define PLAYER_RETRIEVE_POS 11

void afterGetPlayer()
{
    if (NoxCurrentHealth(OTHER))
    {
        int plrUnit = NoxGetCaller();

        if (registPlayer(plrUnit, loadPlayerData(plrUnit)))
        {
            NoxEnchantOff(plrUnit, ENCHANT_ANCHORED);
            NoxEnchantOff(plrUnit, ENCHANT_ANTI_MAGIC);
            return;
        }
        NoxMoveObject(plrUnit, NoxGetWaypointX(PLAYER_RETRIEVE_POS), NoxGetWaypointY(PLAYER_RETRIEVE_POS));
    }
}

void getPlayer()
{
    if (NoxCurrentHealth(OTHER))
    {
        PlayerData *pdata = loadPlayerData(NoxGetCaller());

        if (pdata)
        {
            if (registPlayer(NoxGetCaller(), pdata))
                return;
        }
        NoxEnchant(OTHER, ENCHANT_ANCHORED, 0);
        NoxEnchant(OTHER, ENCHANT_ANTI_MAGIC, 0);
        NoxMoveObject(OTHER, NoxGetWaypointX(PLAYER_RETRIEVE_POS), NoxGetWaypointY(PLAYER_RETRIEVE_POS));
    }
}

int TryAwardSkill(int plrUnit)
{
    PlayerData *pdata = loadPlayerData(plrUnit);

    if (!pdata)
        return 0;

    if (pdata->flags & PLAYER_WINDBOOST)
        return 0;

    pdata->flags ^= PLAYER_WINDBOOST;
    return 1;
}

int TryAllBuff(int user)
{
    PlayerData *pdata = loadPlayerData(user);

    if (!pdata)
        return 0;
    
    if (pdata->flags &PLAYER_FLAG_ALLBUFF)
        return 0;
    pdata->flags^= PLAYER_FLAG_ALLBUFF;
    applyAllBuff(user);
    return 1;
}
