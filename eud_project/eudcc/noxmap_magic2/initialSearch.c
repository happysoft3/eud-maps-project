
#include "initialSearch.h"

#include "randomItem.h"
#include "monsters.h"

#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static int s_firstunit;
static int s_lastunit;

static void createRandomItem(int unit)
{
    float xy[]={NoxGetObjectX(unit),NoxGetObjectY(unit)};

    NoxDeleteObject(unit);
    DO_CREATE_RANDOMITEM(xy[0], xy[1]);
}

static void checkSearchUnit(int unit)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (!ptr)
        return;

    switch (ptr->thingType)
    {
    case OBJ_REWARD_MARKER:
        createRandomItem(unit);
        break;

    case OBJ_NECROMANCER_MARKER:
        ReplaceToEntity(unit);
        break;
    }
}

#define UNITSEARCH_STEP 40
static void searchTillEnd(int cur)
{
    unsigned int count = UNITSEARCH_STEP;

    while (count > 0)
    {
        checkSearchUnit(cur + count);
        count -= 2;
    }
    if (cur < s_lastunit)
        NoxFrameTimerWithArg(1, cur + UNITSEARCH_STEP, &searchTillEnd);
    else
    {
        NoxPrintToAll("end scan");
    }
}

void DoInitialSearch()
{
    s_firstunit=NoxGetObject("firstscan");
    s_lastunit = CreateObjectById(OBJ_MOVER, NoxGetWaypointX(1), NoxGetWaypointY(1));
    NoxFrameTimerWithArg(1, s_firstunit, &searchTillEnd);
}

