
#ifndef SPECIAL_MONS_H__
#define SPECIAL_MONS_H__

int CreateBeast(float xpos, float ypos);
int CreateGoon(float xpos, float ypos);
int CreateGreenBomber(float xpos, float ypos);
int CreateFiresprite(float xpos, float ypos);
int CreateRedWizard(float xpos, float ypos);
int CreateLichMeleeAttack(float xpos, float ypos);
int CreateNecromancer(float xpos, float ypos);
int CreateXandors(float xpos, float ypos);
int CreateHecubahMelee(float xpos, float ypos);
int CreateHecubahOrb(float xpos, float ypos);
void InitializeMonsterScript();

#endif
