
#ifndef PLAYER_CONTROL_H__
#define PLAYER_CONTROL_H__

#include "../include/intTypes.h"

#define PLAYER_START_LOCATION 12

void PlayerKillEvent(int victim, int attacker);

void InitialPlayerControl();
void DeinitializePlayerControl();

typedef struct _playerData
{
    int unitId;
    int pIndex;
    short *userName;
    struct _playerData *pNext;
    int flags;
    int level;
    int expLevel;
    int allowed;
} PlayerData;

int GetClosestPlayer(int unit);
int TryAwardSkill(int plrUnit);
int TryAllBuff(int user);

#endif

