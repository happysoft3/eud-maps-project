
#include "mytrade.h"
#include "fixdialogsys.h"
#include "../include/fxeffect.h"
#include "../include/printUtil.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static int powerupEquip(int owner)
{
    int inv = NoxGetLastItem(owner), count = 0;

    while (inv)
    {
        if (!NoxHasEnchant(inv, ENCHANT_RUN))   //몇몇 power무기때문에 무적화 하면 안됩니다
        {
            if (!NoxHasEnchant(inv, ENCHANT_INVULNERABLE))
            {
                NoxEnchant(inv, ENCHANT_INVULNERABLE, 0);
                ++count;
            }
        }
        inv = NoxGetPreviousItem(inv);
    }
    return count;
}

static void onRepairTrade()
{
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 8500){
        int count = powerupEquip(OTHER);

        if (!count)
        {
            WideSayMessage(SELF, u"처리실패!- 무적화 할 것이 없네요..", 90);
            return;
        }
        NoxChangeGold(OTHER, -8500);
        WideSayMessage(SELF, u"처리성공!- 장비가 무적화 되었어요. 다음에 또 와요", 120);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(8,500 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }    
}

static void tryRepair()
{
    WideSayMessage(SELF, u"인벤토리를 무적화 하시겠어요? 8,500골드가 필요합니다", 150);
    TellStoryWithName("GuiInv.c:WarriorRepair", u"인벤무적화 8500골드");
    PlaySoundAround(SELF, SOUND_SwordsmanRecognize);
}

static void placeRepairman(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_SWORDSMAN, xpos, ypos);

    NoxSetupDialog(man, DIALOG_YESNO, &tryRepair, &onRepairTrade);
    NoxLookWithAngle(man, 225);
}

void InitMyTrade()
{
    placeRepairman(NoxGetWaypointX(25), NoxGetWaypointY(25));
}

