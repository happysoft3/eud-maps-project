
#include "mapEvent.h"
#include "../include/team.h"
#include "../include/memoryUtil.h"
#include "../include/logfile.h"
#include "../include/builtins.h"

void MapInitialize()
{
    BuiltinsInitialize();
    CreateLogFile("magic2-log.txt");
    InitializeSmartMemoryDestructor();
    WriteLog("ok1");
    OnInitialMap();
    WriteLog("ok2");
    EnableCoopTeamMode();
    WriteLog("ok3");
}

void MapExit()
{
    NoxMusicEvent();
    OnShutdownMap();
    DisableCoopTeamMode();
}

