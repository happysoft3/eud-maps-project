
#ifndef MONSTERS_H__
#define MONSTERS_H__

typedef void(*mob_death_event_cb)(int, int);    //victim, killer

void RegistDeathEvent(mob_death_event_cb e);
void ReplaceToEntity(int unit);
void InitMonsterReplaceFunctions();

#endif
