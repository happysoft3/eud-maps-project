
#include "monsters.h"
#include "specialMons.h"
#include "../include/monster_action.h"

// #define ENABLE_LOG

#ifdef ENABLE_LOG
#include "../include/logfile.h"
#endif

#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static mob_death_event_cb s_deathEvent;

void RegistDeathEvent(mob_death_event_cb e)
{
    s_deathEvent = e;
}

static NoxObject *getKillCredit()
{
    NoxObject *ptr = *(NoxObject **)0x979724;

    if (!ptr)
        return 0;

    NoxObject *killer = ptr->unitDamaged;

    if (!killer)
        return 0;

    return killer;
}

static NoxObject *getTopParent(NoxObject *ptr)
{
    while (ptr->parentUnit)
    {
        ptr = ptr->parentUnit;
    }
    return ptr;
}

static void onMonsterDeath()
{
    NoxObject *ptr = getKillCredit();

    if (ptr)
    {
        NoxObject *parent = getTopParent(ptr);

        if (parent)
            s_deathEvent(NoxGetTrigger(), parent->globalID);
    }
    NoxDamage(SELF, 0, 1, DAMAGE_PLASMA);
    NoxDeleteObjectTimer(SELF, 90);
}

static int s_forestMons[]={
    OBJ_SWORDSMAN, OBJ_ARCHER, OBJ_WASP, 
    OBJ_WOLF, OBJ_WHITE_WOLF, OBJ_BLACK_BEAR};

static int s_forestMonHp[]={
    325, 98, 64,
    160, 225, 360
};

#define GET_ARRAY_LAST_INDEX(arr) ((sizeof(arr)/sizeof(arr[0]))-1)

static void monCommonProc(int mon)
{
    NoxAggressionLevel(mon, 1.0f);
    NoxRetreatLevel(mon, 0);
    NoxResumeLevel(mon, 0);
    NoxLookWithAngle(mon, NoxRandomInteger(0, 254));
    NoxSetCallback(mon, UnitCallback_OnDeath, &onMonsterDeath);
}

typedef void(*mon_replace_fn)(float, float);
static mon_replace_fn s_monFn[128];

static void mobReplaceImpl(float xpos, float ypos, char ty)
{
#ifdef ENABLE_LOG
    WRITE_LOG("mobReplaceImpl - end");
#endif
    if (s_monFn[ty])
        s_monFn[ty](xpos, ypos);
}

static int createForestMons(float xpos, float ypos)
{
    int r = NoxRandomInteger(0, GET_ARRAY_LAST_INDEX(s_forestMons));
    int m=CreateObjectById(s_forestMons[r], xpos, ypos);

    SetUnitMaxHealth(m, s_forestMonHp[r]);
    monCommonProc(m);
    return m;
}

static int s_caveMons[]={
    OBJ_URCHIN, OBJ_BAT, OBJ_SMALL_ALBINO_SPIDER, OBJ_IMP, OBJ_WEIRDLING_BEAST
};

static int s_caveMonHp[]={
    64, 76, 85, 64, 0
};

static int createCaveMons(float xpos, float ypos)
{
    int r = NoxRandomInteger(0, GET_ARRAY_LAST_INDEX(s_caveMons));
    int id = s_caveMons[r], m;

    if (id == OBJ_WEIRDLING_BEAST)
        m = CreateBeast(xpos, ypos);
    else
    {
        /* code */
        m=CreateObjectById(id, xpos, ypos);
        SetUnitMaxHealth(m, s_caveMonHp[r]);
    }
    monCommonProc(m);
    return m;
}

static int s_caveSecondMons[]={
    OBJ_GIANT_LEECH, OBJ_ALBINO_SPIDER, OBJ_SCORPION,
    OBJ_SPIDER, OBJ_SPITTING_SPIDER, OBJ_TROLL
};

static int s_caveSecondHp[]={
    160, 185, 260,
    225, 160, 295
};

static int createSecondaryCaveMons(float xpos, float ypos)
{
    int r=NoxRandomInteger(0, GET_ARRAY_LAST_INDEX(s_caveSecondMons));
    int m=CreateObjectById(s_caveSecondMons[r], xpos, ypos);

    SetUnitMaxHealth(m, s_caveSecondHp[r]);
    monCommonProc(m);
    return m;
}

static int s_sewerMons[]={
    OBJ_URCHIN_SHAMAN, OBJ_SHADE, OBJ_GIANT_LEECH,
    OBJ_SCORPION, OBJ_BOMBER, OBJ_GOON
};

static int s_sewerMonHp[]={
    130, 160, 160,
    260, 0, 0
};

static int createSewerMons(float xpos, float ypos)
{
    int r=NoxRandomInteger(0, GET_ARRAY_LAST_INDEX(s_sewerMons));
    int id = s_sewerMons[r];
    int m;
    
    switch (id)
    {
    case OBJ_BOMBER:
        m=CreateGreenBomber(xpos, ypos);
        break;

    case OBJ_GOON:
        m=CreateGoon(xpos, ypos);
        break;

    default:
        m = CreateObjectById(id, xpos, ypos);

        SetUnitMaxHealth(m, s_sewerMonHp[r]);
    }    
    monCommonProc(m);
    return m;
}

static int s_ogreMons[]={
    OBJ_GRUNT_AXE, OBJ_OGRE_BRUTE, OBJ_OGRE_WARLORD, OBJ_SHADE,
    OBJ_WIZARD_GREEN, OBJ_CARNIVOROUS_PLANT,
};

static int s_ogreMonHp[]={
    225, 290, 325, 180,
    0, 0,
};

static int createFlowerMon(float xpos, float ypos)
{
    int m=CreateObjectById(OBJ_CARNIVOROUS_PLANT, xpos, ypos);
    NoxObject *ptr = UnitToPtr(m);

    ptr->unitSpeed = 3.0f;
    ptr->unitAccelMB = 3.0f;
    SetUnitMaxHealth(m, 475);    
    return m;
}

static int createDryad(float xpos, float ypos)
{
    int m=CreateObjectById(OBJ_WIZARD_GREEN, xpos, ypos);

    SetUnitMaxHealth(m, 275);
    NoxEnchant(m, ENCHANT_ANCHORED, 0);
    return m;
}

static int createSwampMons(float xpos, float ypos)
{
    int r=NoxRandomInteger(0, GET_ARRAY_LAST_INDEX(s_ogreMons));
    int id = s_ogreMons[r];
    int m;
    
    switch (id)
    {
    case OBJ_WIZARD_GREEN:
        m=createDryad(xpos, ypos);
        break;

    case OBJ_CARNIVOROUS_PLANT:
        m=createFlowerMon(xpos, ypos);
        break;

    default:
        m = CreateObjectById(id, xpos, ypos);
        SetUnitMaxHealth(m, s_ogreMonHp[r]);
    }
    monCommonProc(m);
    return m;
}

static int s_lavaMons[]={
    OBJ_EMBER_DEMON, OBJ_MELEE_DEMON, OBJ_IMP, 
    OBJ_FIRE_SPRITE, OBJ_WIZARD_RED
};

static int s_lavaHp[]={
    165, 165, 64,
    0, 0
};

static int createLavaMons(float xpos, float ypos)
{
    int r=NoxRandomInteger(0, GET_ARRAY_LAST_INDEX(s_lavaMons));
    int id = s_lavaMons[r];
    int m;
    
    switch (id)
    {
    case OBJ_WIZARD_RED:
        m=CreateRedWizard(xpos, ypos);
        break;

    case OBJ_FIRE_SPRITE:
        m=CreateFiresprite(xpos, ypos);
        break;

    default:
        m = CreateObjectById(id, xpos, ypos);
        SetUnitMaxHealth(m, s_lavaHp[r]);
    }
    monCommonProc(m);
    return m;
}

static int s_snowMons[]={
    OBJ_SKELETON, OBJ_SKELETON_LORD, OBJ_EVIL_CHERUB,
    OBJ_GHOST, OBJ_VILE_ZOMBIE, OBJ_ZOMBIE,
    OBJ_LICH, OBJ_NECROMANCER,
};

static int s_snowMonHp[]={
    260, 295, 98,
    64, 0, 225,
    0,
};

static int createZombieLord(float xpos, float ypos)
{
    int zb=CreateObjectById(OBJ_VILE_ZOMBIE, xpos, ypos);
    NoxObject *ptr = UnitToPtr(zb);

    SetUnitMaxHealth(zb, 325);
    ptr->unitSpeed = 3.0f;
    ptr->unitAccelMB = 3.0f;
    return zb;
}

static void deferredMimicAction(int mi)
{
    DO_CREATURE_ACTION_ESCORT(mi, mi);
    NoxAggressionLevel(mi, 1.0f);
}

static int createMimic(float xpos, float ypos)
{
    int mon=CreateObjectById(OBJ_MIMIC, xpos, ypos);

    NoxFrameTimerWithArg(1, mon, &deferredMimicAction);
    SetUnitMaxHealth(mon, 375);
    return mon;
}

static int createMystic(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_WIZARD, xpos, ypos);

    NoxEnchant(mon, ENCHANT_ANCHORED, 0);
    SetUnitMaxHealth(mon, 275);
    return mon;
}

static int createIceMons(float xpos, float ypos)
{
    int r=NoxRandomInteger(0, GET_ARRAY_LAST_INDEX(s_snowMons));
    int id = s_snowMons[r];
    int m;
    
    switch (id)
    {
    case OBJ_VILE_ZOMBIE:
        m=createZombieLord(xpos, ypos);
        break;

    case OBJ_LICH:
        m=CreateLichMeleeAttack(xpos, ypos);
        break;

    case OBJ_NECROMANCER:
        m=CreateNecromancer(xpos, ypos);
        break;

    default:
        m = CreateObjectById(id, xpos, ypos);
        SetUnitMaxHealth(m, s_snowMonHp[r]);
    }
    monCommonProc(m);
    return m;
}

static int createHorr(float xpos, float ypos)
{
    int h=CreateObjectById(OBJ_HORRENDOUS, xpos, ypos);
    NoxObject *ptr = UnitToPtr(h);

    ptr->unitSpeed = 2.0f;
    ptr->unitAccelMB=2.0f;
    SetUnitMaxHealth(h, 436);
    return h;
}

static int createMechGolem(float xpos, float ypos)
{
    int g=CreateObjectById(OBJ_MECHANICAL_GOLEM, xpos, ypos);

    SetUnitMaxHealth(g, 700);
    return g;
}

typedef int (*last_mon_create_ty)(float, float);

static last_mon_create_ty s_lastMons[]={
    &createHorr, &createMechGolem, &CreateNecromancer,
    &CreateXandors, &CreateHecubahMelee, &CreateHecubahOrb,
};

static int createLastMons(float xpos, float ypos)
{
    int mon = s_lastMons[0, GET_ARRAY_LAST_INDEX(s_lastMons)](xpos, ypos);

    monCommonProc(mon);
    return mon;
}

static last_mon_create_ty s_ixMons[]={
    &createMystic, &CreateGreenBomber, &createMimic,
    &CreateGoon,
};

static int createIxMons(float xpos, float ypos)
{
    int mon=s_ixMons[0, GET_ARRAY_LAST_INDEX(s_ixMons)](xpos, ypos);

    monCommonProc(mon);
    return mon;
}

void ReplaceToEntity(int unit)
{
#ifdef ENABLE_LOG
WRITE_LOG("replaceToEntity");
#endif
    float xpos = NoxGetObjectX(unit), ypos = NoxGetObjectY(unit);
    NoxObject *ptr = UnitToPtr(unit);
    char *scrName = ptr->ScriptNameMB;

    if (!scrName)
        return;

    char ty = scrName[0];

    NoxDeleteObject(unit);
    //여기에서 괴물 생성루틴
    mobReplaceImpl(xpos, ypos, ty);
}

void InitMonsterReplaceFunctions()
{
    s_monFn['0'] = &createForestMons;
    s_monFn['1'] = &createCaveMons;
    s_monFn['2']=&createSecondaryCaveMons;
    s_monFn['3'] = &createSewerMons;
    s_monFn['4']=&createSwampMons;
    s_monFn['5']=&createLavaMons;
    s_monFn['6']=&createIceMons;
    s_monFn['7']=&createLastMons;
    s_monFn['8']=&createIxMons;

    InitializeMonsterScript();
}
