
#include "mapEvent.h"
#include "playerControl.h"
#include "mytrade.h"
#include "monsters.h"
#include "specialMons.h"
#include "initialSearch.h"
#include "../include/respawnItem.h"
#include "../include/printUtil.h"
#include "../include/fxeffect.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"
#include "groupInfo.h"

#define NULL 0
#define INTERACTIVE_PROC void

static int s_largeFlame;

static void disableRespawnAll()
{
    RespawnData *respData = FIRST_RESPAWNDATA;

    while (respData)
    {
        respData->thingId = 0;
        respData->pObject = NULL;
        respData = respData->next;
    }
}

static void putTelpoWake(int obj)
{
    int wk=CreateObjectById(OBJ_TELEPORT_WAKE, NoxGetObjectX(obj), NoxGetObjectY(obj));
    NoxObject *ptr = UnitToPtr(wk);

    ptr->flags ^= UNIT_FLAG_NO_COLLIDE;
    NoxFrozen(wk, TRUE);
}

static void initReadable()
{
    SetPicketText(NoxGetObject("readable1"), u"--녹스 월드 어드밴처-- 원작: Jeremiah Cohn, 2차 수정: 태백광노");
    SetPicketText(NoxGetObject("readable2"), u"이곳은 공간이동 장치실 입니다- 7개의 던전과 연결되는 곳입니다");
    SetPicketText(NoxGetObject("readable3"), u"첫번째 던전으로 향하는 리프트 입니다. 행운을 빌게요");
    SetPicketText(NoxGetObject("readable4"), u"녹스 월드 어드밴처- 제작자가 잠들어 있는 곳");
    SetPicketText(NoxGetObject("readable5"), u"게임 팁- 괴물을 잡다보면, 레밸업을 합니다. 최대 15레밸까지 올릴 수 있어요");
    SetPicketText(NoxGetObject("readable6"), u"별장주인 xxx 씨, xxxx-xx-xx에 왔다감");
    SetPicketText(NoxGetObject("readable7"), u"이 공간이동 장치는 당신을 안전한 곳으로 이동시켜 줄 것입니다");
    SetPicketText(NoxGetObject("readable8"), u"공사관계자: 1공구 종료지점- 이 위 부터는 2공구 지점입니다");
}

static void doInit()
{
    DoInitialSearch();
    disableRespawnAll();
    InitialPlayerControl();
    InitMonsterReplaceFunctions();

    putTelpoWake(NoxGetObject("pjoinL"));
    putTelpoWake(NoxGetObject("pjoinR"));
    initReadable();
    s_largeFlame = CreateObjectById(OBJ_LARGE_FLAME, NoxGetWaypointX(16), NoxGetWaypointY(16));
    RegistDeathEvent(&PlayerKillEvent);
    InitMyTrade();
}

static void enableDeferredElev()
{
    NoxObjectOn(NoxGetObject("blueElev"));
}

void OnInitialMap()
{
    NoxMusicEvent();
    doInit();
    NoxFrameTimer(30, &enableDeferredElev);
}

void OnShutdownMap()
{
    NoxMusicEvent();
    DeinitializePlayerControl();
}

static uint8_t s_summonFxCode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 
    0xFF, 0x70, 0x08, 0xFF, 0x70, 0x0C, 0xFF, 0x70, 0x10, 0xB8, 0xF0, 0x36, 
    0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
};

typedef struct _summonEffectData
{
    int duration;
    short summonData;
    short unknown6;
    int unknown8;
    float *xyCoor;
    int unknown10;    
} SummonEffectData;

static void playSummonEffect(float *xypos, short thingId, int duration)
{
    SummonEffectData data = {0, };

    data.duration=duration;
    data.summonData=thingId;
    data.xyCoor=xypos;
    uint8_t *fnOff = s_summonFxCode;

    BuiltinSingleArg((int)&data, ((int)&fnOff - 0x5c308c) / 4);
}

static int s_stoneGiantDeath;

static void onStonegiantDeath()
{
    if (--s_stoneGiantDeath <=0)
    {
        WidePrintMessageAll(u"비밀의 벽이 열립니다");
        NoxWallGroupOpen(GROUP_sewerOpenWalls);
    }
}

static void OnRevealStonegiant()
{
    int m1=CreateObjectById(OBJ_STONE_GOLEM, NoxGetWaypointX(13), NoxGetWaypointY(13));
    int m2=CreateObjectById(OBJ_STONE_GOLEM, NoxGetWaypointX(14), NoxGetWaypointY(14));

    NoxSetCallback(m1, UnitCallback_OnDeath, &onStonegiantDeath);
    NoxSetCallback(m2, UnitCallback_OnDeath, &onStonegiantDeath);

    SetUnitMaxHealth(m1, 600);
    SetUnitMaxHealth(m2, 600);
    s_stoneGiantDeath+=2;
}

#define SUM_COMPLETE_DURATION 90
INTERACTIVE_PROC SummonStoneGiant()
{
    NoxObjectOff(SELF);

    NoxFrameTimer(SUM_COMPLETE_DURATION, &OnRevealStonegiant);
    float spot1[]={NoxGetWaypointX(13), NoxGetWaypointY(13)};

    playSummonEffect(spot1, OBJ_STONE_GOLEM, SUM_COMPLETE_DURATION);

    float spot2[]={NoxGetWaypointX(14), NoxGetWaypointY(14)};

    playSummonEffect(spot2, OBJ_STONE_GOLEM, SUM_COMPLETE_DURATION);
}

static void startLavaElev()
{
    NoxObjectOn(NoxGetObject("lavaNextLift"));
    NoxDeleteObject(s_largeFlame);
    WideSayMessage(SELF, u"이런 시발, 내가 죽다니! 억울하다!!", 90);
}

INTERACTIVE_PROC SummonKillerian()
{
    int k=CreateObjectById(OBJ_DEMON, NoxGetWaypointX(15), NoxGetWaypointY(15));

    NoxSetCallback(k, UnitCallback_OnDeath, &startLavaElev);
    SetUnitMaxHealth(k, 600);
    NoxObjectOff(SELF);
}

INTERACTIVE_PROC OpenSwampWalls()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(GROUP_swampWalls);
}

static int s_ixMonDeathcount = 6;

static void onIxMonDeath()
{
    if (--s_ixMonDeathcount == 0)
    {
        int bl = NoxGetObject("ixBlock1");
        NoxMove(bl, 23);
        PlaySoundAround(bl, SOUND_SpikeBlockMove);
    }
}

static void createIxMon(int location)
{
    float xpos = NoxGetWaypointX(location), ypos = NoxGetWaypointY(location);
    int m=CreateObjectById(OBJ_BEHOLDER, xpos, ypos);

    NoxEnchant(m, ENCHANT_ANCHORED, 0);
    SetUnitMaxHealth(m, 360);
    NoxPlayFX(FX_TELEPORT, xpos, ypos, 0,0);
    NoxSetCallback(m, UnitCallback_OnDeath, &onIxMonDeath);
    PlaySoundAround(m, SOUND_FlagRespawn);
}

INTERACTIVE_PROC IxSurpriseMon()
{
    NoxObjectOff(SELF);
    createIxMon(17);
    createIxMon(18);
    createIxMon(19);
    createIxMon(20);
    createIxMon(21);
    createIxMon(22);
}

INTERACTIVE_PROC YouWin()
{
    float xpos = NoxGetWaypointX(24), ypos = NoxGetWaypointY(24);

    NoxObjectOff(SELF);
    WidePrintMessageAll(u"축하합니다! 모든 구간을 완주하셨습니다! 이 게임은 당신의 승리입니다");
    PlayFxGreenExplosion(xpos, ypos);
    NoxDeleteObjectTimer(CreateObjectById(OBJ_MANA_BOMB_CHARGE, xpos, ypos), 90);
    NoxDeleteObjectTimer(CreateObjectById(OBJ_LEVEL_UP, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER)), 180);
    PlaySoundAround(OTHER, SOUND_LevelUp);
}

#define WALL_TO_INDEX(x, y) (y|(x<<0x10))

static int s_telpoWalls[][2]={
    {0,0},
    { WALL_TO_INDEX(7,225), WALL_TO_INDEX(8,224), },
    { WALL_TO_INDEX(10,222), WALL_TO_INDEX(11,221) },
    { WALL_TO_INDEX(13,219), WALL_TO_INDEX(14,218) },
    { WALL_TO_INDEX(16,216), WALL_TO_INDEX(17,215) },
    { WALL_TO_INDEX(22,218), WALL_TO_INDEX(23,219) },
    { WALL_TO_INDEX(25,221), WALL_TO_INDEX(26,222) },
    { WALL_TO_INDEX(9,237), WALL_TO_INDEX(10,238) },
};

#define MAKE_OPEN_TELPO_FN(n) \
INTERACTIVE_PROC openTelpoRoom##n()   \
{   \
    NoxObjectOff(SELF); \
    NoxWallOpen(s_telpoWalls[n][0]);    \
    NoxWallOpen(s_telpoWalls[n][1]);    \
}   \

MAKE_OPEN_TELPO_FN(1)
MAKE_OPEN_TELPO_FN(2)
MAKE_OPEN_TELPO_FN(3)
MAKE_OPEN_TELPO_FN(4)
MAKE_OPEN_TELPO_FN(5)
MAKE_OPEN_TELPO_FN(6)
MAKE_OPEN_TELPO_FN(7)

static void openSnowWalls()
{
    NoxWallGroupOpen(GROUP_snowWalls);
}

static void revealHecubah(int pos)
{
    int h= CreateHecubahMelee(NoxGetObjectX(pos), NoxGetObjectY(pos));

    NoxDeleteObject(pos);
    NoxSetCallback(h, UnitCallback_OnDeath, &openSnowWalls);
    NoxSay(h, "Con10B.scr:HecubahDialog1");
}

INTERACTIVE_PROC openSnow()
{
    float xy[] = {NoxGetObjectX(OTHER), NoxGetObjectY(OTHER)};

    NoxObjectOff(SELF);
    NoxFrameTimerWithArg(89, CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, xy[0], xy[1]), &revealHecubah);
    playSummonEffect(xy, OBJ_HECUBAH, 90);
}


INTERACTIVE_PROC wallTest()
{
    NoxObjectOff(SELF);
    NoxWallOpen(WALL_TO_INDEX(135, 21));
}

