
#include "specialMons.h"
#include "../include/monsterBinscript.h"
#include "../include/spellUtils.h"
#include "../include/memoryUtil.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/stringUtil.h"
#include "../include/objectIDdefines.h"

static MonsterBinScriptTable *s_beastData;

static void initBeastData()
{
    s_beastData = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_beastData, sizeof(MonsterBinScriptTable), 0);

    s_beastData->damage = 15;
    s_beastData->damage_type = DAMAGE_CLAW;
    s_beastData->minMeleeAttackDelay = 6;
    s_beastData->maxMeleeAttackDelay = 12;
    s_beastData->speed = 85;
    s_beastData->run_multiplier = 1.0f;
    s_beastData->meleeRange = 20.0f;
    s_beastData->hp = 135;
    s_beastData->strikeEvent = MonsterBinMonsterStrikeEvent;
}

int CreateBeast(float xpos, float ypos)
{
    int m=CreateObjectById(OBJ_WEIRDLING_BEAST, xpos, ypos);

    ChangeMonsterBinScript(m, s_beastData);
    SyncWithCustomBinScript(m);
    return m;
}

static MonsterBinScriptTable *s_mbinGoon;

static void initGoonData()
{
    s_mbinGoon = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_mbinGoon, sizeof(MonsterBinScriptTable), 0);
    s_mbinGoon->hp = 190;
    s_mbinGoon->speed = 80;
    s_mbinGoon->run_multiplier = 1.0f;
    s_mbinGoon->damage = 22;
    s_mbinGoon->damage_type = DAMAGE_DRAIN;
    s_mbinGoon->minMeleeAttackDelay = 19;
    s_mbinGoon->maxMeleeAttackDelay = 26;
    s_mbinGoon->meleeRange = 50.0f;
    s_mbinGoon->attack_impact = 10.0f;
    s_mbinGoon->poison_chance = 2;
    s_mbinGoon->poison_strength = 3;
    s_mbinGoon->poison_max = 20;
    s_mbinGoon->unit_flag = MON_STATUS_ALWAYS_RUN;
    s_mbinGoon->strikeEvent = MonsterBinVileZombieStrikeEvent;
    s_mbinGoon->deadEvent = MonsterBinTrollDeadEvent;
}

int CreateGoon(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_GOON, xpos, ypos);

    ChangeMonsterBinScript(mon, s_mbinGoon);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_greenBombData;

static void initGreenBomberData()
{
    s_greenBombData = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_greenBombData, sizeof(MonsterBinScriptTable), 0);

    s_greenBombData->hp = 128;
    s_greenBombData->speed=110;
    s_greenBombData->run_multiplier=1.0f;
    s_greenBombData->deadEvent = MonsterBinBomberDeadEvent;
    s_greenBombData->mis_range = 200.0f;
    s_greenBombData->minMissileDelay = 5;
    s_greenBombData->maxMissileDelay = 13;
    CopyString("ImpShot", s_greenBombData->missile_name);
}

int CreateGreenBomber(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_BOMBER_GREEN, xpos, ypos);
    NoxObject *ptr = UnitToPtr(mon);

    ChangeMonsterBinScript(mon, s_greenBombData);
    SyncWithCustomBinScript(mon);
    ptr->collideFn = (void *)0x4e83b0;
    return mon;
}

static MonsterBinScriptTable *s_pFirespriteBin;

static void initFirespriteBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pFirespriteBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->hp = 220;
    pTable->speed=75;
    pTable->mis_range = 300.0f;
    pTable->minMissileDelay = 13;
    pTable->maxMissileDelay = 21;
    pTable->mis_attackFrame = 0;
    pTable->run_multiplier = 1.0f;
    pTable->deadEvent = 5545472;
    pTable->unit_flag = MON_STATUS_NEVER_RUN;
    CopyString("WeakFireball", pTable->missile_name);
}

int CreateFiresprite(float xpos, float ypos)
{
    int mon = CreateObjectAt("FireSprite", xpos, ypos);

    ChangeMonsterBinScript(mon, s_pFirespriteBin);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_pRedWizBin;

static void initRedwizBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pRedWizBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->speed = 50;
    pTable->run_multiplier = 1.3f;
    pTable->hp = 380;
    pTable->unit_flag = MON_STATUS_CAN_CAST_SPELLS | MON_STATUS_CAN_HEAL_SELF;
    pTable->mis_range = 300.0f;
    pTable->mis_attackFrame = 4;
}

int CreateRedWizard(float xpos, float ypos)
{
    int mon = CreateObjectAt("WizardRed", xpos, ypos);
    MonCastSpellData spellData;

    ChangeMonsterBinScript(mon, s_pRedWizBin);    
    EmptyUnitSpellSet(mon);
    GetUnitSpellData(mon, &spellData);
    spellData.aimRate = 1.0f;
    spellData.spellLevel = 3;
    spellData.minDeffensiveDelay = 30;
    spellData.maxDeffensiveDelay = 45;
    spellData.minDisablingDelay = 30;
    spellData.maxDisablingDelay = 45;
    spellData.minEscapeDelay = 30;
    spellData.maxEscapeDelay = 45;
    spellData.minOffensiveDelay = 30;
    spellData.maxOffensiveDelay = 45;
    spellData.minReactionDelay = 3;
    spellData.maxReactionDelay = 6;
    SetUnitSpellData(mon, &spellData);
    SetUnitMaxHealth(mon, 370);
    ApplySpellToUnit(mon, SPELL_FIREBALL, SPELLFLAG_ON_OFFENSIVE);
    ApplySpellToUnit(mon, SPELL_DEATH_RAY, SPELLFLAG_ON_OFFENSIVE);
    ApplySpellToUnit(mon, SPELL_SHIELD, SPELLFLAG_ON_DEFFENSIVE);
    ApplySpellToUnit(mon, SPELL_INVERSION, SPELLFLAG_ON_REACTION);
    ApplySpellToUnit(mon, SPELL_SLOW, SPELLFLAG_ON_DISABLING);
    ApplySpellToUnit(mon, SPELL_SHOCK, SPELLFLAG_ON_ESCAPE);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_lichMeleeData;

static void initLichMeleeAttackData()
{
    s_lichMeleeData = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_lichMeleeData, sizeof(MonsterBinScriptTable), 0);

    s_lichMeleeData->hp = 430;
    s_lichMeleeData->speed = 70;
    s_lichMeleeData->run_multiplier = 1.3f;
    s_lichMeleeData->damage = 60;
    s_lichMeleeData->damage_type = DAMAGE_BLADE;
    s_lichMeleeData->meleeAttackFrame = 7;
    s_lichMeleeData->minMeleeAttackDelay = 18;
    s_lichMeleeData->maxMeleeAttackDelay = 25;
    s_lichMeleeData->meleeRange = 50.0f;
    s_lichMeleeData->attack_impact = 10.0f;
    s_lichMeleeData->unit_flag = MON_STATUS_CAN_HEAL_SELF;
    s_lichMeleeData->strikeEvent = MonsterBinMonsterStrikeEvent;
}

int CreateLichMeleeAttack(float xpos, float ypos)
{
    int m=CreateObjectById(OBJ_LICH_LORD, xpos, ypos);

    ChangeMonsterBinScript(m, s_lichMeleeData);
    SyncWithCustomBinScript(m);
    return m;
}

static MonsterBinScriptTable *s_necroData;

void initNecromancerData()
{
    s_necroData = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_necroData, sizeof(MonsterBinScriptTable), 0);

    s_necroData->hp = 385;
    s_necroData->speed = 120;
    s_necroData->run_multiplier = 1.0f;
    s_necroData->damage = 50;
    s_necroData->damage_type = DAMAGE_BLADE;
    // s_necroData->meleeAttackFrame = 7;
    s_necroData->minMeleeAttackDelay = 18;
    s_necroData->maxMeleeAttackDelay = 25;
    s_necroData->meleeRange = 50.0f;
    s_necroData->attack_impact = 10.0f;
    s_necroData->unit_flag = MON_STATUS_CAN_HEAL_SELF;
    s_necroData->strikeEvent = MonsterBinMonsterStrikeEvent;
}


int CreateNecromancer(float xpos, float ypos)
{
    int m=CreateObjectById(OBJ_NECROMANCER, xpos, ypos);

    ChangeMonsterBinScript(m, s_necroData);
    SyncWithCustomBinScript(m);
    return m;
}

static MonsterBinScriptTable *s_xandorData;

static MonsterBinScriptTable *createXandorsData()
{
    MonsterBinScriptTable *scrData = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)scrData, sizeof(MonsterBinScriptTable), 0);

    scrData->hp = 425;
    scrData->speed = 90;
    scrData->run_multiplier = 1.0f;
    scrData->damage = 35;
    scrData->damage_type = DAMAGE_BLADE;
    // s_necroData->meleeAttackFrame = 7;
    scrData->minMeleeAttackDelay = 6;
    scrData->maxMeleeAttackDelay = 12;
    scrData->meleeRange = 30.0f;
    scrData->unit_flag = MON_STATUS_DESTROY_WHEN_DEAD|MON_STATUS_CAN_HEAL_SELF;
    scrData->strikeEvent = MonsterBinMonsterStrikeEvent;
    return scrData;
}

int CreateXandors(float xpos, float ypos)
{
    int m=CreateObjectById(OBJ_AIRSHIP_CAPTAIN, xpos, ypos);

    ChangeMonsterBinScript(m, s_xandorData);
    SyncWithCustomBinScript(m);
    return m;
}

static MonsterBinScriptTable *createHecubahMeleeData()
{
    MonsterBinScriptTable *scrData = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    NoxByteMemset((uint8_t *)scrData, sizeof(MonsterBinScriptTable), 0);
    scrData->hp = 425;
    scrData->speed = 100;
    scrData->run_multiplier = 1.0f;
    scrData->strikeEvent = MonsterBinMonsterStrikeEvent;
    scrData->minMeleeAttackDelay = 10;
    scrData->maxMeleeAttackDelay = 18;
    scrData->meleeRange = 35.0f;
    scrData->damage = 50;
    scrData->damage_type = DAMAGE_BLADE;
    return scrData;
}

static MonsterBinScriptTable *s_hecubahMeleeData;

int CreateHecubahMelee(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_HECUBAH, xpos, ypos);

    ChangeMonsterBinScript(mon, s_hecubahMeleeData);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *createHecubahOrbData()
{
    MonsterBinScriptTable *scrData = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    NoxByteMemset((uint8_t *)scrData, sizeof(MonsterBinScriptTable), 0);
    scrData->hp = 285;
    scrData->speed = 85;
    scrData->run_multiplier = 1.0f;
    scrData->mis_range = 200.0f;
    scrData->minMissileDelay = 20;
    scrData->maxMeleeAttackDelay = 28;
    CopyString("DeathBallFragment", scrData->missile_name);
    return scrData;
}

static MonsterBinScriptTable *s_hecubahOrbData;

int CreateHecubahOrb(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_HECUBAH_WITH_ORB, xpos, ypos);

    ChangeMonsterBinScript(mon, s_hecubahOrbData);
    SyncWithCustomBinScript(mon);
    return mon;
}

void InitializeMonsterScript()
{
    initBeastData();
    initGoonData();
    initGreenBomberData();
    initFirespriteBin();
    initRedwizBin();
    initLichMeleeAttackData();
    initNecromancerData();
    s_xandorData = createXandorsData();
    s_hecubahMeleeData = createHecubahMeleeData();
    s_hecubahOrbData=createHecubahOrbData();
}
