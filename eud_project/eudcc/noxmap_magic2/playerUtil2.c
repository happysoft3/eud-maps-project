
#include "playerUtil2.h"
#include "../include/opcodeTools.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"

static int s_updateCodeImpl[]={
    0x00011DE9, 0x08EC8300, 0x57565553, 0xCC5634E8, 0x24748BFF, 0x85E88B1C, 0x8BE574F6, 0x0002ECBE,
    0x10006800, 0x9F8B0000, 0x00000114, 0xCB9594E8, 0x04C483FF, 0x0A74C085, 0x0224878B, 0xC0850000,
    0xDB85BE75, 0x83C70A74, 0x0000125C, 0x00000000, 0x016A016A, 0xE77BE856, 0xC483FFD9, 0xB7B0A10C,
    0xC0850085, 0x458B0774, 0x74C0853A, 0x56BF0F11, 0x38468D7C, 0x4AE85052, 0x83FFDEEB, 0x4E8B08C4,
    0x3C568B38, 0x00100068, 0x244C8900, 0x24548914, 0x952FE818, 0xC483FFCB, 0x74C08504, 0x34BF8B17,
    0x85000001, 0x8D0D74FF, 0x50102444, 0x7013E857, 0x0BEBFFDA, 0x10244C8D, 0xF6E85156, 0x83FFDA69,
    0x548D08C4, 0x56521024, 0xD95F48E8, 0xE8106AFF, 0xFFCB94F1, 0x850CC483, 0x6A4774C0, 0x6CC3E804,
    0xC483FFCC, 0x74C08504, 0x08838B39, 0x31000008, 0x34488AC9, 0x79BBE851, 0x408BFFCC, 0x04C4834C,
    0x1E74C085, 0x01EC888B, 0xC9850000, 0x938B1474, 0x00000808, 0x016A016A, 0xE6E85250, 0x83FFDA22,
    0x5E5F10C4, 0xC4835B5D, 0x9090C308, 0x00000090,
};

static int s_updateExecCodeImpl[]={
    0xDB624BE8, 0xE5E850FF, 0x83FFDA6E, 0x90C304C4
};

static void userUpdateImpl(NoxObject *ptr)
{
    int *off = s_updateExecCodeImpl;

    BuiltinSingleArg((int)ptr, ((int)&off - 0x5c308c) / 4);
}

void UpdatePlayer(int plrUnit)
{
    NoxObject *ptr = UnitToPtr(plrUnit);
    float xpos = ptr->unitX, ypos = ptr->unitY;

    if (ptr->Class & UNIT_CLASS_PLAYER)
    {
        userUpdateImpl(ptr);
        int *ec = (int *)ptr->unitController;
        int *pInfo = (int *)ec[69];

        pInfo[1175] = 1;
        NoxMoveObject(plrUnit, xpos, ypos);
    }
}

void InitPlayerUtil2()
{
    char *p = ((char *)s_updateCodeImpl) + 5;
    
    ResolveCallOpcode(p, 7, 0x416640);
    ResolveCallOpcode(p, 0x27, 0x40A5C0);
    ResolveCallOpcode(p, 0x50, 0x4EF7D0);
    ResolveCallOpcode(p, 0x71, 0x53FBC0);
    ResolveCallOpcode(p, 0x8c, 0x40A5C0);
    ResolveCallOpcode(p, 0xa8, 0x4F80C0);
    ResolveCallOpcode(p, 0xb5, 0x4F7AB0);
    ResolveCallOpcode(p, 0xc3, 0x4E7010);
    ResolveCallOpcode(p, 0xca, 0x40A5C0);
    ResolveCallOpcode(p, 0xd8, 0x417DA0);
    ResolveCallOpcode(p, 0xf0, 0x418AB0);
    ResolveCallOpcode(p, 0x115, 0x4F3400);
    ResolveCallOpcode((char *)s_updateExecCodeImpl, 0, 0x507250);
    ResolveCallOpcode((char *)s_updateExecCodeImpl, 6, (int)p);
}
