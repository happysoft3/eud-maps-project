
#include "subsequent.h"
#include "mapMonster.h"
#include "../include/equipment.h"
#include "../include/noxobject.h"
#include "../include/def_spot.h"
#include "../include/builtins.h"
#include "../include/respawnItem.h"

static int s_vampNpc;
static int s_vampLockGate[2];
static char *s_potionName = "RedPotion";

#define NULL 0

static void unlockVampRoom()
{
    if (NoxIsLocked(s_vampLockGate[0]))
    {
        NoxUnlockDoor(s_vampLockGate[0]);
        NoxUnlockDoor(s_vampLockGate[1]);
        NoxFrozen(s_vampNpc, FALSE);
        NoxLookAtObject(s_vampNpc, OTHER);
        NoxAggressionLevel(s_vampNpc, 1.0f);
        NoxObjectOn(NoxGetObject("part1pit"));
    }
}

static void initVampireWarrior()
{
    s_vampNpc = NoxGetObject("vampwar");
    NoxFrozen(s_vampNpc, TRUE);

    SetEquipEnchanment(CreateObject("GreatSword", 62), 
        ITEM_PROPERTY_weaponPower3, ITEM_PROPERTY_LightningProtect3, ITEM_PROPERTY_fire1, ITEM_PROPERTY_vampirism2);
    (GETLASTUNIT)->script_num = (int)unlockVampRoom;
    SetEquipEnchanment(CreateObject("OrnateHelm", 63),
        ITEM_PROPERTY_armorQuality5, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_LightningProtect2, 0);
    (GETLASTUNIT)->script_num = (int)unlockVampRoom;
    SetEquipEnchanment(CreateObject("PlateLeggings", 64),
        ITEM_PROPERTY_armorQuality5, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_LightningProtect2, 0);
    (GETLASTUNIT)->script_num = (int)unlockVampRoom;
    SetEquipEnchanment(CreateObject("PlateBoots", 65),
        ITEM_PROPERTY_armorQuality5, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_LightningProtect2, 0);
    (GETLASTUNIT)->script_num = (int)unlockVampRoom;
    SetEquipEnchanment(CreateObject("PlateArms", 66),
        ITEM_PROPERTY_armorQuality5, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_LightningProtect2, 0);
    (GETLASTUNIT)->script_num = (int)unlockVampRoom;
}

static void supportAdditionalPotion()
{
    PointXY p1 = {NoxGetWaypointX(67), NoxGetWaypointY(67)};
    PointXY p2 = {NoxGetWaypointX(68), NoxGetWaypointY(68)};

    for (int i = 0 ; i < 10 ; i ++)
    {
        CreateObjectAt(s_potionName, p1.x, p1.y);
        CreateObjectAt(s_potionName, p2.x, p2.y);

        p1.x -= 23.0f;
        p1.y += 23.0f;
        p2.x -= 23.0f;
        p2.y += 23.0f;
    }
}

static void disableRespawnAll()
{
    RespawnData *respData = FIRST_RESPAWNDATA;

    while (respData)
    {
        respData->thingId = 0;
        respData->pObject = NULL;
        respData = respData->next;
    }
}

void MyInit()
{
    s_vampLockGate[0] = NoxGetObject("gate-a1");
    s_vampLockGate[1] = NoxGetObject("gate-a11");

    initVampireWarrior();
    supportAdditionalPotion();
    disableRespawnAll();
    SetEquipEnchanment(
        CreateObjectAt("GreatSword", NoxGetWaypointX(69), NoxGetWaypointY(69)),
        EQUIPWEAPON_RANDOM_POWER(), EQUIPMENT_RANDOM_MATERIAL(), EQUIPWEAPON_RANDOM_ENCHANT(), EQUIPWEAPON_RANDOM_ENCHANT());
    InitialPutMonsters();
}

void part3openPit1()
{
    NoxObjectOff(SELF);
    NoxObjectOff(NoxGetObject("part3pit1"));
}

void part3openPit2()
{
    NoxObjectOff(SELF);
    NoxObjectOff(NoxGetObject("part3pit2"));
}

void part3openPit3()
{
    NoxObjectOff(SELF);
    NoxObjectOff(NoxGetObject("part3pit3"));
}

