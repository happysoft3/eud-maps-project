
#include "map_scenario.h"
#include "../include/team.h"
#include "../include/builtins.h"

void MapInitialize()
{
    BuiltinsInitialize();
    EnableCoopTeamMode();
    OnInitialMap();
}

void MapExit()
{
    OnShutdownMap();
    DisableCoopTeamMode();
}
