
#include "map_scenario.h"
#include "initialSearch.h"
#include "subsequent.h"
#include "playerControl.h"
#include "../include/playerHandler.h"
#include "../include/fxeffect.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"

typedef int object;

object gvar31;
object gvar30;
object gvar29;
object gvar28;
object gvar27;
int gvar24;
int gvar23;
int gvar22;
int gvar21;
object gvar20;
int gvar19;
int gvar15;
int gvar13;
object gvar12;
object gvar7;
object gvar6;
int gvar5;

static void Trap1()
{
    NoxSecondTimer(5, &Trap1);
    float var0 = NoxRandomFloat(1400.0f, 1900.0f);
    float var1 = NoxRandomFloat(3300.0f, 3550.0f);
    NoxCastSpellObjectLocation(SPELL_FORCE_OF_NATURE, NoxGetObject("Trap1"), var0, var1);
}

static void doInitialize()
{
    gvar5 = NoxGetObject("MoveThisShit");
    gvar6 = NoxGetWaypoint("Waypoint1");
    gvar7 = NoxGetWaypoint("Waypoint2");
    gvar20 = NoxGetWaypoint("Waypoint5");
    gvar31 = NoxGetWaypoint("DiamondDrop");
    gvar30 = NoxGetWaypoint("SumWolf2");
    gvar27 = NoxGetWallGroup("WizWalls");
    gvar28 = NoxGetWallGroup("WarWalls");
    gvar29 = NoxGetWallGroup("ConWalls");

    int var0 = CreateObject("Mimic", NoxGetWaypoint("MimicSpawn"));
    NoxEnchant(var0, ENCHANT_VAMPIRISM, 0);
    NoxEnchant(var0, ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("WizardBoss"), ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("WizardBoss"), ENCHANT_PROTECT_FROM_FIRE, 0);
    NoxEnchant(NoxGetObject("WizardBoss"), ENCHANT_PROTECT_FROM_ELECTRICITY, 0);
    NoxEnchant(NoxGetObject("WizardBoss"), ENCHANT_PROTECT_FROM_POISON, 0);
    NoxEnchant(NoxGetObject("WizardBoss"), ENCHANT_SHIELD, 0);
    int var1 = CreateObject("WillOWisp", NoxGetWaypoint("Waypoint24"));
    int var2 = NoxGetObject("WizardBoss");
    NoxSetOwner(var2, var1);
    var1 = NoxGetObject("ConjurerBoss");
    var2 = CreateObject("UrchinShaman", NoxGetWaypoint("SumMob1"));
    NoxSetOwner(var1, var2);
    NoxCreatureFollow(var1, var2);
    NoxEnchant(var2, ENCHANT_SHIELD, 0);
    var2 = CreateObject("BlackBear", NoxGetWaypoint("SumMob2"));
    NoxSetOwner(var1, var2);
    NoxCreatureFollow(var1, var2);
    NoxEnchant(var2, ENCHANT_PROTECT_FROM_FIRE, 0);
    var2 = CreateObject("Bear", NoxGetWaypoint("SumMob3"));
    NoxSetOwner(var1, var2);
    NoxCreatureFollow(var1, var2);
    NoxEnchant(var2, ENCHANT_PROTECT_FROM_ELECTRICITY, 0);
    var2 = CreateObject("Ghost", NoxGetWaypoint("SumMob4"));
    NoxSetOwner(var1, var2);
    NoxCreatureFollow(var1, var2);
    NoxEnchant(var2, ENCHANT_SHIELD, 0);
    NoxEnchant(var2, ENCHANT_VAMPIRISM, 0);
    NoxEnchant(var2, ENCHANT_INFRAVISION, 0);
    NoxEnchant(var1, ENCHANT_INFRAVISION, 0);
    NoxEnchant(var1, ENCHANT_VAMPIRISM, 0);
    var1 = NoxGetObject("WarriorBoss");
    var2 = CreateObject("BlackWolf", NoxGetWaypoint("SumWolf1"));
    NoxSetOwner(var1, var2);
    NoxCreatureFollow(var1, var2);
    NoxEnchant(var1, ENCHANT_INFRAVISION, 0);
    NoxEnchant(var1, ENCHANT_VAMPIRISM, 0);
    NoxEnchant(var1, ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("Archer1"), ENCHANT_VAMPIRISM, 0);
    NoxEnchant(NoxGetObject("Archer2"), ENCHANT_VAMPIRISM, 0);
    NoxEnchant(NoxGetObject("Archer3"), ENCHANT_VAMPIRISM, 0);
    NoxEnchant(NoxGetObject("Archer4"), ENCHANT_VAMPIRISM, 0);
    NoxEnchant(NoxGetObject("Archer5"), ENCHANT_VAMPIRISM, 0);
    NoxEnchant(NoxGetObject("Archer6"), ENCHANT_VAMPIRISM, 0);
    NoxEnchant(NoxGetObject("Archer1"), ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("Archer2"), ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("Archer3"), ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("Archer4"), ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("Archer5"), ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("Archer6"), ENCHANT_HASTED, 0);
    NoxEnchant(NoxGetObject("Archer1"), ENCHANT_INVISIBLE, 0);
    NoxEnchant(NoxGetObject("Archer2"), ENCHANT_INVISIBLE, 0);
    NoxEnchant(NoxGetObject("Archer3"), ENCHANT_INVISIBLE, 0);
    NoxEnchant(NoxGetObject("Archer4"), ENCHANT_INVISIBLE, 0);
    NoxEnchant(NoxGetObject("Archer5"), ENCHANT_INVISIBLE, 0);
    NoxEnchant(NoxGetObject("Archer6"), ENCHANT_INVISIBLE, 0);
    CreateObject("Skeleton", NoxGetWaypoint("Skelly1"));
    CreateObject("Skeleton", NoxGetWaypoint("Skelly2"));
    CreateObject("Skeleton", NoxGetWaypoint("Skelly3"));
    CreateObject("Skeleton", NoxGetWaypoint("Skelly4"));
    CreateObject("Skeleton", NoxGetWaypoint("Skelly5"));
    CreateObject("Skeleton", NoxGetWaypoint("Skelly6"));
    CreateObject("SkeletonLord", NoxGetWaypoint("SkellyLord1"));
    CreateObject("SkeletonLord", NoxGetWaypoint("SkellyLord2"));
    CreateObject("SkeletonLord", NoxGetWaypoint("SkellyLord3"));
    CreateObject("SkeletonLord", NoxGetWaypoint("SkellyLord4"));
    CreateObject("SkeletonLord", NoxGetWaypoint("SkellyLord5"));
    CreateObject("SkeletonLord", NoxGetWaypoint("SkellyLord6"));
    CreateObject("WhiteWolf", NoxGetWaypoint("Wolf1"));
    CreateObject("WhiteWolf", NoxGetWaypoint("Wolf2"));
    CreateObject("WhiteWolf", NoxGetWaypoint("Wolf3"));
    CreateObject("WhiteWolf", NoxGetWaypoint("Wolf4"));
    CreateObject("WhiteWolf", NoxGetWaypoint("Wolf5"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider1"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider2"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider3"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider4"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider5"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider6"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider7"));
    CreateObject("SpittingSpider", NoxGetWaypoint("Spider8"));
    Trap1();
}
void EnchantMe()
{
    NoxEnchant(OTHER, ENCHANT_HASTED, 30.0f);
}

static void PushBookcase2()
{
    NoxMove(gvar5, gvar7);
}

void PushBookcase()
{
    if (gvar15 != 2)
    {
        NoxMove(gvar5, gvar6);
        gvar15 = 2;
        NoxPlayAudioByIndex(SOUND_SecretWallStoneOpen, gvar6);
        NoxFrameTimer(12, &PushBookcase2);
    }
}

void UnlockCheckpoint()
{
    if (// FIXME '!=' with wrong type (object is not int)
        gvar12 != 9)
    {
        NoxUnlockDoor(NoxGetObject("CheckpointDoor"));
        gvar13 = NoxGetCaller();
        NoxSay(gvar13, "GeneralPrint:MsgDoorUnlock");
        // FIXME '=' with wrong type (object is not int)
        gvar12 = 9;
    }
}

static void Trap2()
{
    NoxFrameTimer(64, &Trap2);
    NoxCastSpellObjectLocation(SPELL_LIGHTNING, NoxGetObject("Trap1"), NoxGetWaypointX(gvar12), NoxGetWaypointY(gvar12));
}

void SecretTimer()
{
    gvar19 = 1;
}

void SecretTrainingRoom()
{
    if (gvar19 != 2)
    {
        NoxPrintToAll("GeneralPrint:SecretFound");
        PlaySoundAround(OTHER, SOUND_SecretFound);
        gvar19 = 2;
        NoxSecondTimer(10, &SecretTimer);
    }
}

void MakeBoulders3()
{
    if (gvar22 != 5)
    {
        NoxDoWideScreen(300);
        CreateObject("Boulder", NoxGetWaypoint("Waypoint33"));
        NoxRaiseObject( CreateObject("BoulderCave", NoxGetWaypoint("Waypoint33")), 200.0f);
        CreateObject("Boulder", NoxGetWaypoint("Waypoint33"));
        CreateObject("Boulder", NoxGetWaypoint("Waypoint34"));
        NoxRaiseObject( CreateObject("BoulderCave", NoxGetWaypoint("Waypoint34")), 200.0f);
        CreateObject("Boulder", NoxGetWaypoint("Waypoint34"));
        CreateObject("Boulder", NoxGetWaypoint("Waypoint35"));
        NoxRaiseObject( CreateObject("BoulderCave", NoxGetWaypoint("Waypoint35")), 200.0f);
        CreateObject("Boulder", NoxGetWaypoint("Waypoint35"));
        NoxDeleteObjectTimer( CreateObject("ExtentCylinderLarge", NoxGetWaypoint("Waypoint36")), 1200);
        int var6 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint38"));
        NoxRaiseObject(var6, 200.0f);
        NoxDeleteObjectTimer( CreateObject("ExtentCylinderLarge", NoxGetWaypoint("Waypoint37")), 1200);
        int var7 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint39"));
        NoxRaiseObject(var7, 200.0f);
        NoxDeleteObjectTimer(var6, 1200);
        NoxDeleteObjectTimer(var7, 1200);
        float var3 = NoxGetObjectX(NoxGetHost());
        float var4 = NoxGetObjectY(NoxGetHost());
        NoxPlayFX(FX_JIGGLE, var3, var4, 150.0f, 0.0f);
        int var5 = NoxGetWaypoint("Waypoint35");
        NoxPlayAudioByIndex(SOUND_WallDestroyed, var5);
        NoxPlayAudioByIndex(SOUND_FireballExplode, var5);
        NoxPlayAudioByIndex(SOUND_EarthRumbleMajor, var5);
        gvar22 = 5;
    }
}

void BingTimer()
{
    gvar24 = 1;
}

void MakeThings()
{
    if (gvar24 != 5)
    {
        NoxRaiseObject(CreateObject("Diamond", NoxGetWaypoint("DiamondDrop")), 200.0f);
        gvar24 = 5;
        NoxFrameTimer(11, &BingTimer);
    }
}

void MakeBoulders2()
{
    if (gvar21 != 2)
    {
        NoxDoWideScreen(300);
        CreateObject("Boulder", NoxGetWaypoint("Waypoint23"));
        int var0 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint23"));
        CreateObject("Boulder", NoxGetWaypoint("Waypoint23"));
        NoxRaiseObject(var0, 200.0f);
        CreateObject("Boulder", NoxGetWaypoint("Waypoint24"));
        int var1 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint24"));
        CreateObject("Boulder", NoxGetWaypoint("Waypoint24"));
        NoxRaiseObject(var1, 200.0f);
        CreateObject("Boulder", NoxGetWaypoint("Waypoint25"));
        int var2 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint25"));
        CreateObject("Boulder", NoxGetWaypoint("Waypoint25"));
        NoxRaiseObject(var2, 200.0f);
        NoxDeleteObjectTimer(CreateObject("ExtentCylinderLarge", NoxGetWaypoint("Waypoint26")), 1200);
        NoxDeleteObjectTimer(CreateObject("ExtentCylinderLarge", NoxGetWaypoint("Waypoint27")), 1200);
        NoxDeleteObjectTimer(CreateObject("ExtentCylinderLarge", NoxGetWaypoint("Waypoint28")), 1200);
        NoxDeleteObjectTimer(CreateObject("ExtentCylinderLarge", NoxGetWaypoint("Waypoint29")), 1200);
        int var6 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint30"));
        NoxRaiseObject(var6, 200.0f);
        int var7 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint31"));
        NoxRaiseObject(var7, 200.0f);
        int var8 = CreateObject("BoulderCave", NoxGetWaypoint("Waypoint32"));
        NoxRaiseObject(var8, 200.0f);
        NoxDeleteObjectTimer(var6, 1200);
        NoxDeleteObjectTimer(var7, 1200);
        NoxDeleteObjectTimer(var8, 1200);
        float var3 = NoxGetObjectX(NoxGetHost());
        float var4 = NoxGetObjectY(NoxGetHost());
        NoxPlayFX(FX_JIGGLE, var3, var4, 150.0f, 0.0f);
        int var5 = NoxGetWaypoint("Waypoint25");
        NoxPlayAudioByIndex(SOUND_WallDestroyed, var5);
        NoxPlayAudioByIndex(SOUND_FireballExplode, var5);
        NoxPlayAudioByIndex(SOUND_EarthRumbleMajor, var5);
        gvar21 = 2;
    }
}

void Bait()
{
    NoxPrintToAll("GeneralPrint:SecretFound");
    NoxMoveObject(OTHER, 420.0f, 4920.0F);
    NoxSay(OTHER, "War02A.scr:ChickenShirtRuns");
    NoxPlayAudioByIndex(SOUND_BerserkerChargeInvoke, NoxGetWaypoint("Waypoint35"));
    NoxEnchant(OTHER, ENCHANT_DEATH, 60.0f);
}

void ScaredTimer()
{
    gvar23 = 1;
}

void ScaredBitch()
{
    if (gvar23 != 5)
    {
        NoxSay(OTHER, "War02A.scr:ChickenTalk1Start");
        PlaySoundAround(OTHER, SOUND_SwordsmanHurt);
        NoxEnchant(OTHER, ENCHANT_ANCHORED, 90.0f);
        gvar23 = 5;
        NoxFrameTimer(15, &ScaredTimer);
    }
}

void UnlockWar()
{
    NoxWallGroupOpen(gvar28);
    NoxUnlockDoor(NoxGetObject("EndGameDoor"));
    NoxSay(OTHER, "GeneralPrint:MsgWallLowered");
}
void UnlockWiz()
{
    NoxWallGroupOpen(gvar27);
    NoxUnlockDoor(NoxGetObject("EndGameDoor"));
    NoxSay(OTHER, "GeneralPrint:MsgWallLowered");
}

void UnlockCon()
{
    NoxWallGroupOpen(gvar29);
    NoxUnlockDoor(NoxGetObject("EndGameDoor"));
    NoxSay(OTHER, "GeneralPrint:MsgWallLowered");
}

void MakeBoulders3SEG1()
{
    NoxSecondTimer(1, &MakeBoulders3);
}

void MakeBoulders2SEG1()
{
    NoxSecondTimer(1, &MakeBoulders2);
}

void OnInitialMap()
{
    NoxMusicEvent();
    InitializeSmartMemoryDestructor();
    InitialPlayerUpdateRewritten();
    DoInitialSearch();
    doInitialize();
    MyInit();
    
    NoxFrameTimer(30, &OnPlayerLoop);
}

void OnShutdownMap()
{
    NoxMusicEvent();
    ResetAllPlayerHandler();
}

