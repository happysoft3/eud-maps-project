
#include "mapMonster.h"
#include "../include/monsterBinscript.h"
#include "../include/noxobjectEnum.h"
#include "../include/monster_action.h"
#include "../include/noxobject.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"

static MonsterBinScriptTable *s_mbinGoon;
static MonsterBinScriptTable *s_mbinLichlord;

static void initialMonsterCustomScript()
{
    s_mbinGoon = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_mbinGoon, sizeof(MonsterBinScriptTable), 0);
    s_mbinGoon->hp = 180;
    s_mbinGoon->speed = 80;
    s_mbinGoon->run_multiplier = 1.0f;
    s_mbinGoon->damage = 22;
    s_mbinGoon->damage_type = DAMAGE_DRAIN;
    s_mbinGoon->minMeleeAttackDelay = 19;
    s_mbinGoon->maxMeleeAttackDelay = 26;
    s_mbinGoon->meleeRange = 50.0f;
    s_mbinGoon->attack_impact = 10.0f;
    s_mbinGoon->poison_chance = 2;
    s_mbinGoon->poison_strength = 3;
    s_mbinGoon->poison_max = 20;
    s_mbinGoon->unit_flag = MON_STATUS_ALWAYS_RUN;
    s_mbinGoon->strikeEvent = MonsterBinVileZombieStrikeEvent;
    s_mbinGoon->deadEvent = MonsterBinTrollDeadEvent;

    s_mbinLichlord = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_mbinLichlord, sizeof(MonsterBinScriptTable), 0);

    s_mbinLichlord->hp = 385;
    s_mbinLichlord->speed = 70;
    s_mbinLichlord->run_multiplier = 1.3f;
    s_mbinLichlord->damage = 100;
    s_mbinLichlord->damage_type = DAMAGE_BLADE;
    s_mbinLichlord->meleeAttackFrame = 7;
    s_mbinLichlord->minMeleeAttackDelay = 18;
    s_mbinLichlord->maxMeleeAttackDelay = 25;
    s_mbinLichlord->meleeRange = 50.0f;
    s_mbinLichlord->attack_impact = 10.0f;
    s_mbinLichlord->unit_flag = MON_STATUS_CAN_HEAL_SELF;
    s_mbinLichlord->strikeEvent = MonsterBinMonsterStrikeEvent;
}

static int fieldMonsterLichlord(int location)
{
    int unit = CreateObject("LichLord", location);

    ChangeMonsterBinScript(unit, s_mbinLichlord);
    SyncWithCustomBinScript(unit);
    NoxAggressionLevel(unit, 1.0f);
    return unit;
}

static int fieldMonsterGoon(int location)
{
    int goon = CreateObject("Goon", location);

    ChangeMonsterBinScript(goon, s_mbinGoon);
    SyncWithCustomBinScript(goon);
    NoxAggressionLevel(goon, 1.0f);
    return goon;
}

static void deferredMimicAction(int mi)
{
    DO_CREATURE_ACTION_ESCORT(mi, mi);
    NoxAggressionLevel(mi, 1.0f);
}

static int fieldMonsterMimic(int location)
{
    int mi=CreateObject("Mimic", location);

    SetUnitMaxHealth(mi, 600);
    NoxFrameTimerWithArg(1, mi, &deferredMimicAction);
    return mi;
}

static int fieldMonsterHorrendous(int location)
{
    int unit=CreateObject("Horrendous", location);

    SetUnitMaxHealth(unit, 420);
    return unit;
}

static int fieldMonsterPlant(int location)
{
    int unit=CreateObject("CarnivorousPlant", location);
    NoxObject *ptr = GETLASTUNIT;

    SetUnitMaxHealth(unit, 600);
    NoxAggressionLevel(unit, 1.0f);
    ptr->unitSpeed = 2.5f;
    ptr->unitAccelMB = 2.5f;
    return unit;
}

static void field1Monsters()
{
    fieldMonsterGoon(72);
    fieldMonsterGoon(73);
    fieldMonsterGoon(74);
    fieldMonsterGoon(75);
    fieldMonsterGoon(76);
    fieldMonsterGoon(77);
    fieldMonsterGoon(78);
    fieldMonsterGoon(79);
    fieldMonsterGoon(80);
    fieldMonsterGoon(81);
}

static void field2Monsters()
{
    fieldMonsterGoon(82);
    fieldMonsterGoon(83);
    fieldMonsterGoon(84);
    fieldMonsterMimic(88);
    fieldMonsterMimic(89);
    fieldMonsterMimic(90);
    fieldMonsterMimic(91);
    fieldMonsterMimic(92);
    fieldMonsterMimic(93);
    fieldMonsterMimic(94);
    fieldMonsterMimic(95);
    fieldMonsterMimic(96);
    fieldMonsterMimic(97);
    fieldMonsterGoon(85);
    fieldMonsterGoon(86);
    fieldMonsterGoon(87);
    fieldMonsterHorrendous(98);
}

static void field3Monsters()
{
    fieldMonsterPlant(99);
    fieldMonsterPlant(100);
    fieldMonsterPlant(101);
    fieldMonsterPlant(102);
    fieldMonsterPlant(103);
    fieldMonsterPlant(104);
    fieldMonsterPlant(105);
    fieldMonsterPlant(111);
    fieldMonsterPlant(114);
    fieldMonsterMimic(106);
    fieldMonsterMimic(107);
    fieldMonsterMimic(109);
    fieldMonsterMimic(110);
    fieldMonsterMimic(112);
    fieldMonsterMimic(113);
}

static void field4Monsters()
{
    fieldMonsterLichlord(115);
    fieldMonsterLichlord(116);
    fieldMonsterLichlord(117);
    fieldMonsterLichlord(118);
    fieldMonsterLichlord(119);
    fieldMonsterLichlord(120);
    fieldMonsterLichlord(121);
    fieldMonsterLichlord(122);
    fieldMonsterLichlord(123);
    fieldMonsterLichlord(124);
}

static void deferredFieldMonsters()
{
    field1Monsters();
    field2Monsters();
    field3Monsters();
    field4Monsters();
}

void InitialPutMonsters()
{
    initialMonsterCustomScript();
    NoxFrameTimer(1, &deferredFieldMonsters);
}
