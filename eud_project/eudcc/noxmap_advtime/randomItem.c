
#include "randomItem.h"
#include "../include/equipment.h"
#include "../include/xfer.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"

static int s_offRandomWeapons[]=
{
    0x58f3a8, 0x58f3ac, 0x58f3b8, 0x58f3c4, 0x58f3d0, 0x58f3dc,
    0x58f3e8, 0x58f3f0, 0x58f3f8, 0x58f404, 0x58f414, 0x58f420, 0x58f428,
    0x58f4c8, 0x58f4d8, 0x58f4e8
};

//FC F4 58 00
static int s_offRandomStaff[]=
{
    0x58f434, 0x58f448, 0x58f460, 0x58f488, 0x58f498, 0x58f4ac, 0x58f4bc
    ////74 F4 58 00
};

static int s_offRandomArmor[]=
{
    0x5baa08, 0x5baa18, 0x5baa28, 0x5baa38, 0x5baa44, 0x5baa50, 0x5baa60, 0x5baa70, 0x5baa80, 0x5baa8c,
    0x5baa9c, 0x5baaac, 0x5baabc, 0x5baac8, 0x5baad8, 0x5baae4, 0x5baaf8, 0x5bab04, 0x5bab10, 0x5bab1c, 0x5bab28, 0x5bab34,
    0x5bab44
};

static int s_offRandomPots[]={
0x5bab50, 0x5bab5c, 0x5bab68,0x5bab7c,0x5bab88,0x5bab9c,
0x5babac,0x5babbc,0x5babd0,0x5babe4,0x5babf8,0x5bac10
};

static random_item_creator s_randomItemFn[]={
    &CreateHotpotion, &CreateRandomArmorAt, &CreateRandomPotionAt, &CreateRandomStaffAt,
    &CreateRandomWeaponAt, &CreateTreasureAt};
random_item_creator *g_pRandItemFn = s_randomItemFn;
unsigned int g_randItemFnLastindex = (sizeof(s_randomItemFn)/sizeof(s_randomItemFn[0])) - 1;

#define WEAPON_QUIVER 1168
#define WEAPON_SHURIKEN 1178
static void weaponDetailProperties(int weapon)
{
    NoxObject *ptr = UnitToPtr(weapon);
    int id = ptr->thingType;

    if (id == WEAPON_QUIVER || id == WEAPON_SHURIKEN)
    {
        SetConsumablesWeaponCapacity(weapon, 255, 255);
    }
    else if (id >= 222 && id <= 225)
    {
        DisableOblivionPickupEvent(weapon);
        AllowUndroppableItem(weapon);
    }
}

#define GET_ARRAYLASTINDEX(arrName) (sizeof(arrName)/sizeof(arrName[0]))-1

int CreateRandomWeaponAt(float xpos, float ypos)
{
    int item = CreateObjectAt(
        (char *)s_offRandomWeapons[NoxRandomInteger(0, GET_ARRAYLASTINDEX(s_offRandomWeapons))],
        xpos, ypos);

    weaponDetailProperties(item);
    SetEquipEnchanment(item, EQUIPWEAPON_RANDOM_POWER(), EQUIPMENT_RANDOM_MATERIAL(), EQUIPWEAPON_RANDOM_ENCHANT(), EQUIPWEAPON_RANDOM_ENCHANT());
    return item;
}

int CreateRandomArmorAt(float xpos, float ypos)
{
    int item = CreateObjectAt(
        (char *)s_offRandomArmor[NoxRandomInteger(0, GET_ARRAYLASTINDEX(s_offRandomArmor))],
        xpos, ypos);
    
    SetEquipEnchanment(item, EQUIPARMOR_RANDOM_POWER(), EQUIPMENT_RANDOM_MATERIAL(), EQUIPARMOR_RANDOM_ENCHANT(), EQUIPARMOR_RANDOM_ENCHANT());
    return item;
}

int CreateRandomStaffAt(float xpos, float ypos)
{
    int item = CreateObjectAt(
        (char *)s_offRandomStaff[NoxRandomInteger(0, GET_ARRAYLASTINDEX(s_offRandomStaff))],
        xpos, ypos);
    
    SetEquipEnchanment(item, EQUIPWEAPON_RANDOM_POWER(), EQUIPMENT_RANDOM_MATERIAL(), EQUIPWEAPON_RANDOM_ENCHANT(), EQUIPWEAPON_RANDOM_ENCHANT());
    SetConsumablesWeaponCapacity(item, 255, 255);
    return item;
}

int CreateRandomPotionAt(float xpos, float ypos)
{
    return CreateObjectAt(
        (char *)s_offRandomPots[NoxRandomInteger(0, GET_ARRAYLASTINDEX(s_offRandomPots))],
        xpos, ypos);
}

int CreateGoldAt(float xpos, float ypos)
{
    int g=CreateObjectAt("Gold", xpos, ypos);

    GoldXferSetAmount(g, NoxRandomInteger(500, 2000));
    return g;
}

static int createGoldplusAt(float xpos, float ypos)
{
    int g=CreateObjectAt(NoxRandomInteger(0, 1) ? "QuestGoldChest" : "QuestGoldPile", xpos, ypos);

    GoldXferSetAmount(g, NoxRandomInteger(1000, 4000));
    return g;
}

int CreateTreasureAt(float xpos, float ypos)
{
    int rnd = NoxRandomInteger(0, 20);
    
    if (rnd == 20) return CreateObjectAt("Diamond", xpos, ypos);
    else if (rnd >= 16) return CreateObjectAt("Emerald", xpos, ypos);
    else if (rnd >= 7) return CreateObjectAt("Ruby", xpos, ypos);
    return createGoldplusAt(xpos, ypos);
}

int CreateHotpotion(float xpos, float ypos)
{
    return CreateObjectAt("RedPotion", xpos, ypos);
}
