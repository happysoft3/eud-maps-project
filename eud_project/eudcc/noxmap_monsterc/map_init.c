
#include "mapEvent.h"
#include "../include/logfile.h"
#include "../include/team.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"

void MapInitialize()
{
    BuiltinsInitialize();
    CreateLogFile("monsterc_log.txt");
    InitializeSmartMemoryDestructor();
    WRITE_LOG("init-smem-ok");
    EnableCoopTeamMode();
    OnInitialMap();
}

void MapExit()
{
    NoxMusicEvent();
    DisableCoopTeamMode();
    OnShutdownMap();
    CreateLogFile("monsterc_log_end.txt");
}