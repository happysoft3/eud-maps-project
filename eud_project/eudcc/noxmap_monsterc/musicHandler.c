
#include "musicHandler.h"
#include "../include/builtins.h"

static int s_musicSeconds[]=
{
    0, 
    33,33,33,32,60,
    46,86,48,33,38,
    32,39,36,73,264,
    236,216,265,254,239,
    244,274,181,99,112,
    68,150,118,54
};

static int s_playTime;
static int s_enableMusicLoop = TRUE;

static void myMusicLoop(int seconds)
{
    if (!s_enableMusicLoop)
        return;

    if (seconds)
    {
        NoxSecondTimerWithArg(1, seconds - 1, &myMusicLoop);
        return;
    }
    int musicId = NoxRandomInteger(1, 29);

    NoxPlayMusic(musicId, 100);
    NoxFrameTimerWithArg(10, s_musicSeconds[musicId], &myMusicLoop);
}

void StartMusicLoop()
{
    myMusicLoop(0);
}

