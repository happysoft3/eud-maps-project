
#include "monsters.h"
#include "mon_bin.h"
#include "randomItem.h"
#include "../include/printUtil.h"
#include "../include/monsterBinscript.h"
#include "../include/monster_action.h"
#include "../include/logfile.h"
#include "../include/spellUtils.h"
#include "../include/noxobject.h"
#include "../include/stringUtil.h"
#include "../include/builtins.h"
#include "../include/subclassDef.h"

int g_monsterLocations[]=
{
    4, 43, 42, 3, 44,46, 45, 41, 40,36,271,273,39,281,
    32,31,30,26,27,29,33,25,47,46,2, 48, 49, 50, 51, 104,
    53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 91, 92, 93, 94, 95, 96, 97, 98, 99,
    100, 101, 102, 103, 105, 108, 109, 110, 111, 112, 113, 128,129,130,131,132,133,
    134,135,136,137,138,139,140,141,142,143,144,
};

static int s_currentMinimum = 0;
static int s_currentMaximum = 2;

char *g_myItemNameArray[] =
{
    "GreatSword", "CurePoisonPotion", "FanChakram", "SteelShield", "Breastplate",
    "RedPotion", "WarHammer", "RoundChakram", "OrnateHelm", "MedievalCloak", "MedievalPants", "MedievalShirt"
};

static int s_nextRoundArray[] = {
    108, 111, 121, 130, 136, 142, 155, 163, 171, 179, 184, 196, 203, 211, 220, 228, 237, 245, 258, 266, 270
};
static int s_monDeathcount = 0;
static int m_monCount = 0;
static int s_hostMonster;

static int s_myitemList[400];
static uint32_t s_myItemListCount = sizeof(s_myitemList)/sizeof(s_myitemList[0]);
static int s_myItemListCur;

static void commonMobDeath()
{
    NoxPlayFX(FX_TELEPORT, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 0, 0);
    NoxDeleteObjectTimer(SELF, 30);
    --m_monCount;
    ++s_monDeathcount;
    if (s_monDeathcount > s_nextRoundArray[s_currentMinimum])
    {
        s_monDeathcount -= s_nextRoundArray[s_currentMinimum];
        ++s_currentMinimum;
        ++s_currentMaximum;
    }
}

static void decayingNextItem(int item)
{
    if (NoxIsObjectOn(item))
    {
        if (!GetOwner(item))
            NoxDeleteObjectTimer(item, 3);
    }
}

static void pushItemList(int item)
{
    decayingNextItem(s_myitemList[s_myItemListCur]);
    s_myitemList[s_myItemListCur] = item;
    s_myItemListCur = (s_myItemListCur+1)%s_myItemListCount;
}

static void dropItem()
{
    static int itemNameSize = (int)(sizeof(g_myItemNameArray)/sizeof(int));

    pushItemList(
        CreateObjectAt(g_myItemNameArray[NoxRandomInteger(0, itemNameSize - 1)], NoxGetObjectX(SELF), NoxGetObjectY(SELF))
    );
    commonMobDeath();
}

static void dropItemSpecial()
{
    int item = DO_CREATE_RANDOMITEM(NoxGetObjectX(SELF), NoxGetObjectY(SELF));

    if (NoxMaxHealth(item))
        SetUnitMaxHealth(item, NoxMaxHealth(item) * 2);
    pushItemList(item);
    commonMobDeath();
}

static void onMobHurt()
{
    if (NoxGetCaller())
        return;
    
    NoxObject *ptr = UnitToPtr(SELF);

    if (ptr->decelTimeMB)
    {
        NoxDeleteObjectTimer(CreateObjectAt("GreenPuff", NoxGetObjectX(SELF), NoxGetObjectY(SELF)), 12);
        NoxDamage(SELF, 0, (int)ptr->decelTimeMB, DAMAGE_POISON);
    }
}

static void mobCommonProc(int mob)
{
    // NoxSetOwner(s_hostMonster, mob);
    NoxAggressionLevel(mob, 1.0f);
    NoxRetreatLevel(mob, 0);
    NoxResumeLevel(mob, 0);
    NoxObject *ptr = UnitToPtr(mob);
    
    if (!(ptr->SubClass & MON_SUBCLASS_IMMUNE_POISON))
        NoxSetCallback(mob, UnitCallback_OnHit, &onMobHurt);
}

static int s_lastMobCount = 32;

static void decreaseLastMobCount()
{
    short buff[128], nb[16];

    CopyWideString(u"최종보스 남은 개수: ", buff);
    Utf8ToUnicodeString(IntToString(--s_lastMobCount), nb);
    ConcatenateWideString(buff, nb);
    WidePrintMessageAll(buff);
}

static void victoryEvent()
{
    WidePrintMessageAll(u"승리하셨습니다!");
}

static void lastMobDeath()
{
    if (s_lastMobCount > 0)
        decreaseLastMobCount();
    else
        victoryEvent();
    commonMobDeath();
}

static int createLastMonster(int location)
{
    int mob = CreateRedWizard(NoxGetWaypointX(location), NoxGetWaypointY(location));

    NoxSetCallback(mob, UnitCallback_OnDeath, &lastMobDeath);
    return mob;
}

static void startCreateLastmob(int count)
{
    if (count)
    {
        int mob = createLastMonster(106);

        mobCommonProc(mob);
        NoxFrameTimerWithArg(1, count - 1, &startCreateLastmob);
    }
}

static int createSmallSpider(int location)
{
    int mob = CreateObject("SmallAlbinoSpider", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItem);
    return mob;
}

static int createBat(int location)
{
    int mob = CreateObject("Bat", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItem);
    return mob;
}

static int createUrchin(int location)
{
    int mob = CreateObject("Urchin", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItem);
    return mob;
}

static int createAlbinoSpider(int location)
{
    int mob = CreateObject("AlbinoSpider", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItem);
    SetUnitMaxHealth(mob, 135);
    return mob;
}

static int createScorpion(int location)
{
    int mob = CreateObject("Scorpion", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 200);
    return mob;
}

static int createGoon(int location)
{
    int mob = CreateGoon(NoxGetWaypointX(location), NoxGetWaypointY(location));

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    return mob;
}

static int createShaman(int location)
{
    int mob = CreateObject("UrchinShaman", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 211);
    ApplySpellToUnit(mob, SPELL_CONFUSE, 0);
    ApplySpellToUnit(mob, SPELL_MAGIC_MISSILE, SPELLFLAG_ON_OFFENSIVE);
    ApplySpellToUnit(mob, SPELL_PIXIE_SWARM, SPELLFLAG_ON_OFFENSIVE | SPELLFLAG_ON_DISABLING);
    return mob;
}

static int createOgreaxe(int location)
{
    int mob = CreateObject("GruntAxe", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 225);
    return mob;
}

static int createGargoyle(int location)
{
    int mob = CreateObject("EvilCherub", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 98);
    return mob;
}

static int createSkeleton(int location)
{
    int mob = CreateObject("SkeletonLord", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 295);
    return mob;
}

static int createFiresprite(int location)
{
    int mob = CreateFiresprite(NoxGetWaypointX( location), NoxGetWaypointY(location));

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    return mob;
}

static int createOgrewarlord(int location)
{
    int mob = CreateObject("OgreWarlord", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 325);
    return mob;
}

static int createEmberdemon(int location)
{
    int mob = CreateObject("EmberDemon", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 225);
    return mob;
}

static int createBlackwidow(int location)
{
    int mob = CreateBlackwidow(NoxGetWaypointX( location), NoxGetWaypointY(location));

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    return mob;
}

static int createBeholder(int location)
{
    int mob = CreateObject("Beholder", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 480);
    NoxEnchant(mob, ENCHANT_ANCHORED, 0);
    return mob;
}

static void deferredMimicAction(int mi)
{
    DO_CREATURE_ACTION_ESCORT(mi, mi);
    NoxAggressionLevel(mi, 1.0f);
}

static int createMimic(int location)
{
    int mob = CreateObject("Mimic", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 520);
    NoxFrameTimerWithArg(1, mob, &deferredMimicAction);
    return mob;
}

static int createStonegiant(int location)
{
    int mob = CreateObject("StoneGolem", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 600);
    return mob;
}

static int createHovath(int location)
{
    int mob = CreateObject("WizardWhite", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 500);
    NoxEnchant(mob, ENCHANT_ANCHORED, 0);
    return mob;
}

static int createJandor(int location)
{
    int mob = CreateJandor(NoxGetWaypointX(location), NoxGetWaypointY(location));

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    return mob;
}

static int createLichking(int location)
{
    int mob = CreateLichKing(NoxGetWaypointX(location), NoxGetWaypointY(location));

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    return mob;
}

static int createDemon(int location)
{
    int mob = CreateObject("Demon", location);

    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
    SetUnitMaxHealth(mob, 700);
    return mob;
}

typedef int(*monster_create_fn_ty)(int);

static monster_create_fn_ty s_mobFunctions[]={
    createSmallSpider, createBat, createUrchin, createAlbinoSpider, createScorpion, createGargoyle,
    createShaman, createOgreaxe, createGoon, createSkeleton, createFiresprite,
    createOgrewarlord, createEmberdemon, createBlackwidow, createBeholder, 
    createMimic, createStonegiant, createHovath, createJandor, createLichking, createDemon
};

static uint32_t s_mobFunctionCount = sizeof(s_mobFunctions)/sizeof(s_mobFunctions[0]);

int CreateRandomMonster()
{
    if (s_currentMaximum >= s_mobFunctionCount)
        return 0;

    static int locLength = (int)(sizeof(g_monsterLocations)/sizeof(int));
    int mon = s_mobFunctions[NoxRandomInteger(s_currentMinimum, s_currentMaximum)](g_monsterLocations[NoxRandomInteger(0, locLength - 1)]);

    mobCommonProc(mon);
    ++m_monCount;
    return mon;
}

void RespawnMonsterLoop(int count)
{
    if (count > 11)
    {
        count = 0;
        if (m_monCount < 200)
        {
            WRITE_LOG("start respawn monster");
            for (int i = NoxRandomInteger(1, 10) ; i ; --i)
            {
                if (!CreateRandomMonster())
                {
                    WidePrintMessageAll(u"괴물 리스폰이 모두 끝났습니다");
                    NoxSecondTimerWithArg(20, s_lastMobCount, &startCreateLastmob);
                    return;
                }
            }
            WRITE_LOG("end respawn monster");
        }
    }
    else
        count ++;
    NoxSecondTimerWithArg(1, count, &RespawnMonsterLoop);
}

void InitMonsters()
{
    s_hostMonster = CreateObjectAt("Hecubah", 100.0f, 100.0f);

    NoxFrozen(s_hostMonster, TRUE);
    for (int i = 0 ; i < 60 ; i ++)
        CreateRandomMonster();
    NoxSecondTimerWithArg(3, 0, &RespawnMonsterLoop);

    CreateRedWizard(NoxGetWaypointX(88), NoxGetWaypointY(88));
    NoxPrintToAll("Init monsters");
}

int RetrieveSuspect(int attacker)
{
    do
    {
        if (NoxCurrentHealth(attacker))
            return attacker;

        attacker = GetOwner(attacker);
    } while (attacker);
    return 0;
}
