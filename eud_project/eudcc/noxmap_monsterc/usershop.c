
#include "usershop.h"
#include "fixdialogsys.h"
#include "monsters.h"
#include "playerControl.h"
#include "../include/logfile.h"
#include "../include/equipment.h"
#include "../include/printUtil.h"
#include "../include/fxeffect.h"
#include "../include/noxobject.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"
#include "../include/sound_define.h"
#include "../include/objectIDdefines.h"

/*
    push ebp
    lea ebp, [esp+08]
push esi
push edi
mov eax, [0075ae40]
lea edi, [eax*4+00979740]
    mov esi, 00507230
    push [ebp]
    call esi
    push [ebp+04]
    call esi
    push [ebp+08]
    call esi
    push [ebp+0c]
    call esi
    push [ebp+10]
    call esi
    add esp, 14
    mov esi, 00507310
    push 00
    push edi
    push ffffffff
    call esi
    add esp, 0c
pop edi
    pop esi
pop ebp
    push 004e1500
    ret

55 8D 6C 24 08 56 57 A1 40 AE 75 00 8D 3C 85 40 97 97 00 BE 30 72 50 00 FF 75 00 FF 
D6 FF 75 04 FF D6 FF 75 08 FF D6 FF 75 0C FF D6 FF 75 10 FF D6 83 C4 14 BE 10 73 50 
00 6A 00 57 68 FF FF FF FF FF D6 83 C4 0C 5F 5E 5D 68 00 15 4E 00 C3 90
*/

static uint8_t s_damCounterCode[]={
    0x55, 0x8D, 0x6C, 0x24, 0x08, 0x56, 0x57, 0xA1, 0x40, 0xAE, 0x75, 
    0x00, 0x83, 0x05, 0x40, 0xAE, 0x75, 0x00, 0x05, 0x50, 0x8D, 0x3C, 
    0x85, 0x40, 0x97, 0x97, 0x00, 0x8B, 0xC7, 0x8B, 0xF5, 0x51, 0xB9, 
    0x05, 0x00, 0x00, 0x00, 0xF3, 0xA5, 0x59, 0xBE, 0x10, 0x73, 0x50, 
    0x00, 0x6A, 0x00, 0x50, 0x68, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xD6, 
    0x83, 0xC4, 0x0C, 0x58, 0xA3, 0x40, 0xAE, 0x75, 0x00, 0x5F, 0x5E, 
    0x5D, 0x68, 0x00, 0x15, 0x4E, 0x00, 0xC3, 0x90, 0x90, 0x90
};

typedef void(*armor_damage_fn)();

static uint8_t *createArmorDamageHandler(armor_damage_fn fn)
{
    uint8_t *dest = (uint8_t *)AllocSmartMemory(80);

    NoxByteMemcopy(s_damCounterCode, dest, sizeof(s_damCounterCode));
    armor_damage_fn *p = (armor_damage_fn *)(dest+49);

    *p=fn;
    return dest;
}

static void changeArmorDamageHandler(int armor, uint8_t *dmgHandler)
{
    NoxObject *ptr=UnitToPtr(armor);

    ptr->damageFnPtr=dmgHandler;
}

typedef struct _armorDamageHandle
{
    NoxObject *pVictim;
    NoxObject *pAttacker;
    int *pSub;
    int amount;
    int type;
} ArmorDamageHandle;

static void onDamCounterArmor()
{
    ArmorDamageHandle *dmHandle = *((ArmorDamageHandle **)0x979720);
    int armor = dmHandle->pVictim->globalID;
    int owner = GetOwner(armor);

    if (NoxCurrentHealth(owner))
    {
        if (!NoxHasItem(owner, armor))
            return;

        if (!dmHandle->pAttacker)
            return;

        int attacker = RetrieveSuspect( dmHandle->pAttacker->globalID );

        if (attacker)
        {
            NoxDamage(attacker, owner, dmHandle->amount*2, DAMAGE_FLAME);
            NoxPlayFX(FX_LIGHTNING, NoxGetObjectX(owner), NoxGetObjectY(owner), NoxGetObjectX(attacker), NoxGetObjectY(attacker));
        }
    }
    NoxRestoreHealth(armor, 9999);
}

static void mytestArmor(float xpos, float ypos)
{
    int a=CreateObjectById(OBJ_BREASTPLATE, xpos, ypos);
    uint8_t *handler = createArmorDamageHandler(&onDamCounterArmor);

    changeArmorDamageHandler(a, handler);
    SetEquipEnchanment(a, ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_FireProtect4);
    NoxEnchant(a, ENCHANT_RUN, 0);
}

static int powerupEquip(int owner)
{
    int inv = NoxGetLastItem(owner), count = 0;

    while (inv)
    {
        if (!NoxHasEnchant(inv, ENCHANT_RUN))   //몇몇 power무기때문에 무적화 하면 안됩니다
        {
            if (!NoxHasEnchant(inv, ENCHANT_INVULNERABLE))
            {
                NoxEnchant(inv, ENCHANT_INVULNERABLE, 0);
                ++count;
            }
        }
        inv = NoxGetPreviousItem(inv);
    }
    return count;
}

static void onRepairTrade()
{
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 5000){
        int count = powerupEquip(OTHER);

        if (!count)
        {
            WideSayMessage(SELF, u"처리실패!- 무적화 할 것이 없네요..", 90);
            return;
        }
        NoxChangeGold(OTHER, -5000);
        WideSayMessage(SELF, u"처리성공!- 장비가 무적화 되었어요. 다음에 또 와요", 120);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(5,000 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }    
}

static void tryRepair()
{
    WideSayMessage(SELF, u"인벤토리를 무적화 하시겠어요? 5천골드가 필요합니다", 150);
    TellStoryWithName("GuiInv.c:WarriorRepair", u"인벤무적화 5천골드");
    PlaySoundAround(SELF, SOUND_SwordsmanRecognize);
}

static void placeRepairman(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_SWORDSMAN, xpos, ypos);

    WRITE_LOG(IntToString(man));
    NoxSetupDialog(man, DIALOG_YESNO, &tryRepair, &onRepairTrade);
    WRITE_LOG("OK");
    NoxLookWithAngle(man, 92);
}

static void tradeCounterArmor(){
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 40000){
        mytestArmor(NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
        NoxChangeGold(OTHER, -40000);
        WideSayMessage(SELF, u"피해력 돌려주기 갑옷 구입완료!", 150);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(40,000 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }
}

static void descriptionCounterArmor(){
    WideSayMessage(SELF, u"피해력 돌려주기 갑옷을 구입하시겠어요? 4만골드가 필요합니다", 150);
    TellStoryWithName("thing.db:MedievalCloakDescription", u"피해력 돌려주기\n갑옷구입");
    PlaySoundAround(SELF, SOUND_Maiden2Talkable);
}

static void placeCounterCloakShop(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_WIZARD_RED, xpos, ypos);

    NoxSetupDialog(man, DIALOG_YESNO, &descriptionCounterArmor, &tradeCounterArmor);
    NoxLookWithAngle(man, 32);
}

static void tradeAllbuff()
{
    if (NoxGetAnswer(SELF) != 1)
        return;

    if (NoxGetGold(OTHER) >= 45000){
        if (!TryAllBuff(NoxGetCaller()))
            return;
        NoxPlayFX(FX_YELLOW_SPARKS, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER),0,0);
        NoxChangeGold(OTHER, -45000);
    }
    else
    {
        WideSayMessage(SELF, u"처리실패!- 금화 부족(45,000 골드 필요)", 120);
        PlaySoundAround(SELF, SOUND_NoCanDo);
    }
}

static void descrAllbuff()
{
    WideSayMessage(SELF, u"올엔첸을 구입하시겠어요? 4만5천골드가 필요합니다", 150);
    TellStoryWithName("ParseCmd.c:GodSet", u"올엔첸 구입\n4만5천 골드");
    PlaySoundAround(SELF, SOUND_Maiden2Talkable);
}

static void placeAllbuffshop(float xpos, float ypos){
    int man = DummyUnitCreateAt(OBJ_WIZARD_GREEN, xpos, ypos);

    NoxSetupDialog(man, DIALOG_YESNO, &descrAllbuff, &tradeAllbuff);
    NoxLookWithAngle(man, 32);
}

void InitialUsershopDisposition()
{
    OnInitialFixDialogSys();
    WRITE_LOG("INIT FIXDIALOG OK");
    placeRepairman(NoxGetWaypointX(107), NoxGetWaypointY(107));
    placeCounterCloakShop(NoxGetWaypointX(146), NoxGetWaypointY(146));
    placeAllbuffshop(NoxGetWaypointX(147), NoxGetWaypointY(147));
}
