
#include "mapEvent.h"
#include "monsters.h"
#include "mon_bin.h"
#include "initialSearch.h"
#include "patrolRots.h"
#include "usershop.h"
#include "playerControl.h"
#include "musicHandler.h"
#include "../include/fxeffect.h"
#include "../include/playerHandler.h"
#include "../include/unitExtend.h"
#include "../include/equipment.h"
#include "../include/printUtil.h"
#include "../include/logfile.h"
#include "../include/noxobject.h"
#include "../include/mathlab.h"
#include "../include/respawnItem.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

#define NULL 0

static uint32_t s_hitWeaponCode[]=
{
    0x50731068, 0x8B565000, 0x8B102474, 0x0002FC86,
    0x006A5600, 0x2454FF50, 0x0CC48314, 0xC483585E, 0x9090C304
};

void XXPlayBgm(int id)
{
    NoxPlayMusic(id, 100);
    NoxPrintToAll("Play Bgm");
}

void PlacePotions()
{
    float x = NoxGetWaypointX(52), y = NoxGetWaypointY(52);

    for (int u = 0 ; u < 36 ; ++u)
    {
        CreateObjectAt("RedPotion", x + MathSine((u * 10) + 90, 30.0f), y + MathSine(u * 10, 30.0f));
    }
}

static void onArmorDamaged()
{
    NoxPrintToAll(IntToString(NoxGetTrigger()));
    NoxPrintToAll(IntToString(NoxGetCaller()));
}

static void deferredInit()
{
    PlacePotions();
    InitMonsters();
    InitializePatrolRots();
    NoxFrameTimer(1, &OnPlayerLoop);
    SetPicketText(NoxGetObject("mmsign1"), u"공간이동 목걸이 구입하기- 800 골드입니다");
    SetPicketText(NoxGetObject("mmsign2"), u"포스오브네이처 책 구입하기- 1,100 골드입니다");
    SetPicketText(NoxGetObject("mmsign3"), u"헤쿠바소환 구입하기- 99,900 골드입니다");
    SetPicketText(NoxGetObject("mmsign4"), u"윈드부스터 배우기- 60,000 골드입니다");
    SetPicketText(NoxGetObject("mmsign5"), u"이곳은 실험실 입니다. 실험적인 기능을 제공합니다");
    SetPicketText(NoxGetObject("mmsign6"), u"초강력 뿅망치- 69,900 골드입니다");
    SetPicketText(NoxGetObject("mmsign7"), u"헬화이어망치- 71,400 골드입니다");
    SetPicketText(NoxGetObject("mmsign8"), u"망각의 지팡이 구입하기- 36,670 골드입니다");
    SetPicketText(NoxGetObject("mmsign9"), u"장비 무적화- 5,000 골드입니다");
    SetPicketText(NoxGetObject("mmsignA"), u"-익스 공동묘지로 가는 문-");
    SetPicketText(NoxGetObject("mmsignB"), u"-익스 가디언즈- 제작자: noxgameremaster 즐겜하세요~ @버그 및 기타 문의 환영@");
}

void teststsetset()
{
    CreateFiresprite( NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

static void disableRespawnAll()
{
    RespawnData *respData = FIRST_RESPAWNDATA;

    while (respData)
    {
        respData->thingId = 0;
        respData->pObject = NULL;
        respData = respData->next;
    }
}

void onUseteleportAmulet()
{
    NoxDeleteObject(SELF);
    NoxPlayFX(FX_SMOKE_BLAST, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER), 0, 0);
    NoxMoveObject(OTHER, NoxGetWaypointX(418), NoxGetWaypointY(418));
    NoxPlayAudioByIndex(SOUND_BlindOff,418);
    NoxPlayFX(FX_TELEPORT, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER), 0, 0);
}
static void onFonCollide()
{
    int owner = GetOwner(SELF);

    if (NoxCurrentHealth(OTHER) && NoxIsAttackedBy(OTHER, owner))
    {
        NoxDamage(OTHER, owner, 400, DAMAGE_PLASMA);
        NoxPlayFX(FX_YELLOW_SPARKS, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 0,0);
        NoxDeleteObject(SELF);
        NoxDeleteObject(NoxGetTrigger() + 1);
    }
    else if (!NoxGetCaller())
    {
        NoxDeleteObject(SELF);
        NoxDeleteObject(NoxGetTrigger() + 1);
    }

}

static void shotFon(int owner)
{
    int s=CreateObjectById(OBJ_DEATH_BALL, NoxGetObjectX(owner) + UnitAngleCos(owner, 17.0f), NoxGetObjectY(owner) + UnitAngleSin(owner, 17.0f));
    NoxObject *ptr = UnitToPtr(s);

    ptr->onUpdateFn = (int *)5483536;
    SetUnitCallbackExtended(s, &onFonCollide, EXTENDED_ON_COLLIDE);
    NoxSetOwner(owner, s);
    NoxPushObject(s, 27.0f, NoxGetObjectX(owner), NoxGetObjectY(owner));
}

static void onOblivionTriggered()
{
    if (NoxHasEnchant(OTHER, ENCHANT_ETHEREAL))
        return;

    shotFon(OTHER);
    PlaySoundAround(OTHER, SOUND_TelekinesisOff);
    NoxEnchant(OTHER, ENCHANT_ETHEREAL, 0.9f);
}

static void createOblivionStaff(int owner){
    int staff=CreateObjectById(OBJ_OBLIVION_ORB, NoxGetObjectX(owner), NoxGetObjectY(owner));

    DisableOblivionPickupEvent(staff);
    AllowUndroppableItem(staff);
    SetUnitCallbackExtended(staff, &onOblivionTriggered, EXTENDED_ON_USE);
}

void buyOblivionStaff()
{
#define OBLIVION_PAY 36670
    if (NoxGetGold(OTHER) >= OBLIVION_PAY)
    {
        NoxChangeGold(OTHER, -OBLIVION_PAY);
        createOblivionStaff(OTHER);
    }
#undef OBLIVION_PAY
}

void buyTeleportAmul()
{
    if (NoxGetGold(OTHER) >= 800)
    {
        NoxChangeGold(OTHER, -800);
        int aml=CreateObjectAt("AmuletofManipulation",NoxGetObjectX(OTHER),NoxGetObjectY(OTHER));
        SetUnitCallbackExtended(aml, &onUseteleportAmulet, EXTENDED_ON_USE);
        NoxPlayFX(FX_VIOLET_SPARKS, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 0, 0);
    }
    else
    {
        WidePrintMessage(OTHER, u"금액이 부족합니다\n공간이동 목걸이를 구입하려면, 800골드 입니다");
    }
}

void onUseFonAmulet()
{
    NoxDeleteObject(SELF);
    NoxCastSpellObjectObject(SPELL_FORCE_OF_NATURE, OTHER, OTHER);
}

void buyFONAmul()
{
    if (NoxGetGold(OTHER) >= 1100)
    {
        NoxChangeGold(OTHER, -1100);
        int aml=CreateObjectAt("BlackBook1",NoxGetObjectX(OTHER),NoxGetObjectY(OTHER));
        SetUnitCallbackExtended(aml, &onUseFonAmulet, EXTENDED_ON_USE);
        NoxPlayFX(FX_VIOLET_SPARKS, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 0, 0);
    }
    else
    {
        WidePrintMessage(OTHER, u"금액이 부족합니다\n포스오브네이처 책를 구입하려면, 1100골드 입니다");
    }
}

void buyWindbooster()
{
    if (NoxGetGold(OTHER) >= 60000)
    {
        int ret = TryAwardSkill(NoxGetCaller());

        if (!ret)
        {
            WidePrintMessage(OTHER, u"능력을 이미 가졌습니다");
            return;
        }
        NoxChangeGold(OTHER, -60000);
        NoxPlayFX(FX_YELLOW_SPARKS, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER), 0,0);
    }
}

static void onHurtHecubah()
{
    int target = RetrieveSuspect(OTHER);

    if (target)
    {
        if (NoxIsVisible(SELF, target) && NoxIsAttackedBy(SELF, target))
        {
            NoxPlayFX(FX_DEATH_RAY, NoxGetObjectX(SELF), NoxGetObjectY(SELF), NoxGetObjectX(target), NoxGetObjectY(target));
            int prev = NoxCurrentHealth(target);
            NoxDamage(target, SELF, 100, DAMAGE_ZAP_RAY);
            int now = NoxCurrentHealth(target);
            int drain = prev-now;

            if (drain > 0)
            {
                NoxRestoreHealth(SELF, drain/2);
                NoxPlayFX(FX_DRAIN_MANA, NoxGetObjectX(target),NoxGetObjectY(target), NoxGetObjectX(SELF), NoxGetObjectY(SELF));
            }
        }
    }
}

static void deferredEscort(int unit)
{
    int owner = GetOwner(unit);

    if (NoxCurrentHealth(unit))
    {
        NoxCreatureFollow(unit, owner);
        NoxAggressionLevel(unit, 1.0f);
        PlaySoundAround(unit, SOUND_BigGong);
        WideSayMessage(unit, u"경호를 시작합니다", 60);
    }
}

static void escortHecubah()
{
    if (!NoxMaxHealth(GetOwner(SELF)))
    {
        NoxSetOwner(OTHER, SELF);
        deferredEscort(NoxGetTrigger());
    }
}

static void hecubahIsBorn(int owner)
{
    float xpos = NoxGetObjectX(owner), ypos = NoxGetObjectY(owner);
    int mon = CreateHecubah(xpos,ypos);

    NoxDeleteObjectTimer(CreateObjectAt("ManaBombCharge", xpos, ypos), 12);
    NoxSetOwner(owner, mon);
    NoxSetCallback(mon, UnitCallback_OnHit, &onHurtHecubah);
    NoxSetupDialog(mon,DIALOG_NORMAL, &escortHecubah, &escortHecubah);
    NoxFrameTimerWithArg(1, mon, &deferredEscort);
    NoxEnchant(mon, ENCHANT_RUN, 0);
}

void buySummonHecubah()
{
    if (NoxGetGold(OTHER) >= 99900)
    {
        NoxChangeGold(OTHER, -99900);
        hecubahIsBorn(OTHER);
    }
}

static int spawnDummy(const char *name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    NoxObjectOff(unit);
    NoxDamage(unit, 0, 9999, -1);
    NoxFrozen(unit, TRUE);
    return unit;
}

static void powerhammerHit()
{
    if (NoxCurrentHealth(OTHER) && NoxIsAttackedBy(OTHER, SELF))
    {
        PlaySoundAround(OTHER, SOUND_PlasmaCast);
        NoxEnchant(OTHER, ENCHANT_FREEZE, 2.0f);
        NoxDamage(OTHER, SELF, 340, DAMAGE_PLASMA);
        NoxPlayFX(FX_CYAN_SPARKS, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER), 0,0);
    }
}

static void onhammerHit()
{
    if (NoxMaxHealth(SELF) ^ NoxCurrentHealth(SELF))
        NoxRestoreHealth(SELF, NoxMaxHealth(SELF) - NoxCurrentHealth(SELF));

    int owner = GetOwner(SELF);

    if (NoxCurrentHealth(owner))
    {
        int dam = CreateObjectAt("ForceOfNatureCharge",
            NoxGetObjectX(owner) + UnitAngleCos(owner, 27.0f), NoxGetObjectY(owner) + UnitAngleSin(owner, 27.0f));

        NoxSetOwner(owner, spawnDummy("Troll", NoxGetObjectX(dam), NoxGetObjectY(dam)));
        NoxSetCallback(dam + 1, UnitCallback_Collision, &powerhammerHit);
        PlayFxGreenExplosion(NoxGetObjectX(dam), NoxGetObjectY(dam));
        NoxDeleteObjectTimer(dam, 21);
        NoxDeleteObjectTimer(dam+1, 1);
    }
}

static void powerHammer(int owner)
{
    float xpos = NoxGetObjectX(owner), ypos = NoxGetObjectY(owner);
    int weapon = CreateObjectAt("WarHammer", xpos, ypos);
    NoxObject *ptr = UnitToPtr(weapon);

    SetEquipEnchanment(weapon, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_venom4, ITEM_PROPERTY_lightning4);
    NoxEnchant(weapon, ENCHANT_RUN, 0);
    ptr->soundDamageFnPtr = s_hitWeaponCode;
    ptr->event_flag = (int)&onhammerHit;
}

void buyPowerHammer()
{
    if (NoxGetGold(OTHER) >= 69900)
    {
        NoxChangeGold(OTHER, -69900);
        powerHammer(OTHER);
    }
}

static void onHellfireHammerHit()
{
    if (NoxMaxHealth(SELF) ^ NoxCurrentHealth(SELF))
        NoxRestoreHealth(SELF, NoxMaxHealth(SELF) - NoxCurrentHealth(SELF));

    int owner = GetOwner(SELF);

    if (NoxCurrentHealth(owner))
    {
        NoxCastSpellObjectObject(SPELL_METEOR_SHOWER, owner, owner);
        NoxCastSpellObjectObject(SPELL_EARTHQUAKE, owner, owner);
    }
}

static void hellfireHammer(int owner)
{
    float xpos = NoxGetObjectX(owner), ypos = NoxGetObjectY(owner);
    int weapon = CreateObjectAt("WarHammer", xpos, ypos);
    NoxObject *ptr = UnitToPtr(weapon);

    SetEquipEnchanment(weapon, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_fire4);
    NoxEnchant(weapon, ENCHANT_RUN, 0);
    ptr->soundDamageFnPtr = s_hitWeaponCode;
    ptr->event_flag = (int)&onHellfireHammerHit;
}

void buyHellFireHammer()
{
    if (NoxGetGold(OTHER) >= 71400)
    {
        NoxChangeGold(OTHER, -71400);
        hellfireHammer(OTHER);
    }
}

static void fistTrapCountdown(int trp)
{
    int count = NoxGetDirection(trp);

    if (count)
    {
        NoxLookWithAngle(trp, --count);

        NoxFrameTimerWithArg(3, trp, &fistTrapCountdown);
    }
}

static void fistTrapTriggered()
{
    if (NoxCurrentHealth(OTHER) && NoxMaxHealth(SELF))
    {
        if (NoxGetDirection(SELF))
            return;

        int sub = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, NoxGetObjectX(SELF), NoxGetObjectY(SELF));

        NoxDeleteObject(sub);
        NoxCastSpellLocationLocation(SPELL_FIST, NoxGetObjectX(SELF)-2.0f, NoxGetObjectY(SELF), NoxGetObjectX(SELF), NoxGetObjectY(SELF));
        NoxSetOwner(SELF, sub+1);
        NoxLookWithAngle(SELF, 30); // 1 second
        NoxFrameTimerWithArg(1, NoxGetTrigger(), &fistTrapCountdown);
    }
}

static int setupFistTrap(int locationId)
{
    int trp = CreateObjectById(OBJ_WIZARD, NoxGetWaypointX(locationId), NoxGetWaypointY(locationId));

    NoxDamage(trp, 0, NoxMaxHealth(trp) + 1, -1);
    NoxLookWithAngle(trp, 0);
    NoxSetCallback(trp, UnitCallback_Collision, &fistTrapTriggered);
    return trp;
}

void OnInitialMap()
{
    InitialPlayerUpdateRewritten();
    DoInitialSearch();
    disableRespawnAll();
    NoxFrameTimer(1, &deferredInit);
    NoxFrameTimer(30, &StartMusicLoop);
    InitializeBinScript();
    InitialPlayerControl();
    InitialUsershopDisposition();
    WRITE_LOG("oninitmap-ok");
    setupFistTrap(148);
    setupFistTrap(149);
    setupFistTrap(150);
}

void OnShutdownMap()
{
    ResetAllPlayerHandler();
}

void ontestHorrendous()
{
    CreateHorrendous(NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}
