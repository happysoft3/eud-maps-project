
#include "mon_bin.h"
#include "../include/logfile.h"
#include "../include/spellUtils.h"
#include "../include/noxobject.h"
#include "../include/stringUtil.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"
#include "../include/subclassDef.h"

static MonsterBinScriptTable *s_mbinGoon;

static void goonBin()
{
    s_mbinGoon = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_mbinGoon, sizeof(MonsterBinScriptTable), 0);
    s_mbinGoon->hp = 210;
    s_mbinGoon->speed = 80;
    s_mbinGoon->run_multiplier = 1.0f;
    s_mbinGoon->damage = 22;
    s_mbinGoon->damage_type = DAMAGE_DRAIN;
    s_mbinGoon->minMeleeAttackDelay = 19;
    s_mbinGoon->maxMeleeAttackDelay = 26;
    s_mbinGoon->meleeRange = 50.0f;
    s_mbinGoon->attack_impact = 10.0f;
    s_mbinGoon->poison_chance = 2;
    s_mbinGoon->poison_strength = 3;
    s_mbinGoon->poison_max = 20;
    s_mbinGoon->unit_flag = MON_STATUS_ALWAYS_RUN;
    s_mbinGoon->strikeEvent = MonsterBinVileZombieStrikeEvent;
    s_mbinGoon->deadEvent = MonsterBinTrollDeadEvent;
}

int CreateGoon(float xpos, float ypos)
{
    int mon = CreateObjectAt("Goon", xpos, ypos);

    ChangeMonsterBinScript(mon, s_mbinGoon);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_pFirespriteBin;

static void firespriteBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pFirespriteBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->hp = 220;
    pTable->speed=75;
    pTable->mis_range = 300.0f;
    pTable->minMissileDelay = 13;
    pTable->maxMissileDelay = 21;
    pTable->mis_attackFrame = 0;
    pTable->run_multiplier = 1.0f;
    pTable->deadEvent = 5545472;
    pTable->unit_flag = MON_STATUS_NEVER_RUN;
    CopyString("WeakFireball", pTable->missile_name);
}

int CreateFiresprite(float xpos, float ypos)
{
    int mon = CreateObjectAt("FireSprite", xpos, ypos);

    ChangeMonsterBinScript(mon, s_pFirespriteBin);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_pRedWizBin;

static void redwizBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pRedWizBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->speed = 50;
    pTable->run_multiplier = 1.3f;
    pTable->hp = 380;
    pTable->unit_flag = MON_STATUS_CAN_CAST_SPELLS | MON_STATUS_CAN_HEAL_SELF;
    pTable->mis_range = 300.0f;
    pTable->mis_attackFrame = 4;
}

int CreateRedWizard(float xpos, float ypos)
{
    int mon = CreateObjectAt("WizardRed", xpos, ypos);
    MonCastSpellData spellData;

    ChangeMonsterBinScript(mon, s_pRedWizBin);    
    EmptyUnitSpellSet(mon);
    GetUnitSpellData(mon, &spellData);
    spellData.aimRate = 1.0f;
    spellData.spellLevel = 3;
    spellData.minDeffensiveDelay = 30;
    spellData.maxDeffensiveDelay = 45;
    spellData.minDisablingDelay = 30;
    spellData.maxDisablingDelay = 45;
    spellData.minEscapeDelay = 30;
    spellData.maxEscapeDelay = 45;
    spellData.minOffensiveDelay = 30;
    spellData.maxOffensiveDelay = 45;
    spellData.minReactionDelay = 3;
    spellData.maxReactionDelay = 6;
    SetUnitSpellData(mon, &spellData);
    SetUnitMaxHealth(mon, 370);
    ApplySpellToUnit(mon, SPELL_FIREBALL, SPELLFLAG_ON_OFFENSIVE);
    ApplySpellToUnit(mon, SPELL_DEATH_RAY, SPELLFLAG_ON_OFFENSIVE);
    ApplySpellToUnit(mon, SPELL_SHIELD, SPELLFLAG_ON_DEFFENSIVE);
    ApplySpellToUnit(mon, SPELL_INVERSION, SPELLFLAG_ON_REACTION);
    ApplySpellToUnit(mon, SPELL_SLOW, SPELLFLAG_ON_DISABLING);
    ApplySpellToUnit(mon, SPELL_SHOCK, SPELLFLAG_ON_ESCAPE);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_pBlackwidowBin;

static void settingBlackwidowBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pBlackwidowBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->hp = 295;
    pTable->speed=85;
    pTable->mis_range = 300.0f;
    pTable->minMissileDelay = 14;
    pTable->maxMissileDelay = 24;

    pTable->meleeAttackFrame = 3;
    pTable->minMeleeAttackDelay = 13;
    pTable->maxMeleeAttackDelay = 21;
    pTable->damage = 25;
    pTable->meleeRange = 30.0f;
    pTable->damage_type = DAMAGE_BITE;
    pTable->poison_chance = 50;
    pTable->poison_max = 100;
    pTable->poison_strength = 3;
    pTable->run_multiplier = 1.3f;
    pTable->strikeEvent = MonsterBinSpiderStrikeEvent;
    CopyString("SpiderSpit", pTable->missile_name);
}

int CreateBlackwidow(float xpos, float ypos)
{
    int mon = CreateObjectAt("BlackWidow", xpos, ypos);

    ChangeMonsterBinScript(mon, s_pBlackwidowBin);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_pJandorBin;

static void settingJandorBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pJandorBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->hp = 620;
    pTable->speed=90;
    pTable->run_multiplier = 1.0f;

    pTable->meleeAttackFrame = 0;
    pTable->minMeleeAttackDelay = 7;
    pTable->maxMeleeAttackDelay = 13;
    pTable->damage = 100;
    pTable->meleeRange = 30.0f;
    pTable->damage_type = DAMAGE_BLADE;
    pTable->deadEvent = MonsterBinBomberDeadEvent;
    pTable->strikeEvent = MonsterBinMonsterStrikeEvent;
}

int CreateJandor(float xpos, float ypos)
{
    int mon = CreateObjectAt("AirshipCaptain", xpos, ypos);

    ChangeMonsterBinScript(mon, s_pJandorBin);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_mbinLichlord;

static void settingLichlordBin()
{
    s_mbinLichlord = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_mbinLichlord, sizeof(MonsterBinScriptTable), 0);

    s_mbinLichlord->hp = 700;
    s_mbinLichlord->speed = 70;
    s_mbinLichlord->run_multiplier = 1.3f;
    s_mbinLichlord->damage = 150;
    s_mbinLichlord->damage_type = DAMAGE_BLADE;
    s_mbinLichlord->meleeAttackFrame = 7;
    s_mbinLichlord->minMeleeAttackDelay = 18;
    s_mbinLichlord->maxMeleeAttackDelay = 25;
    s_mbinLichlord->meleeRange = 50.0f;
    s_mbinLichlord->attack_impact = 10.0f;
    s_mbinLichlord->unit_flag = MON_STATUS_CAN_HEAL_SELF;
    s_mbinLichlord->strikeEvent = MonsterBinMonsterStrikeEvent;
}

int CreateLichKing(float xpos, float ypos)
{
    int mon = CreateObjectAt("LichLord", xpos, ypos);

    ChangeMonsterBinScript(mon, s_mbinLichlord);
    SyncWithCustomBinScript(mon);
    return mon;
}

static MonsterBinScriptTable *s_pHecubahBin;

static void settingHecubahBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pHecubahBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->hp = 3000;
    pTable->speed=100;
    pTable->run_multiplier = 1.3f;

    pTable->meleeAttackFrame = 7;
    pTable->minMeleeAttackDelay = 10;
    pTable->maxMeleeAttackDelay = 18;
    pTable->damage = 200;
    pTable->meleeRange = 45.0f;
    pTable->damage_type = DAMAGE_BLADE;
    pTable->strikeEvent = MonsterBinMonsterStrikeEvent;
}

int CreateHecubah(float xpos, float ypos)
{
    int mon = CreateObjectAt("Hecubah", xpos, ypos);
    NoxObject *ptr = GETLASTUNIT;

    ChangeMonsterBinScript(mon, s_pHecubahBin);
    SyncWithCustomBinScript(mon);
    ptr->SubClass ^= MON_SUBCLASS_IMMUNE_POISON;
    return mon;
}

static uint8_t s_customMeleeAttackCode[]={
    0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
    0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
    0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
    0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
};

static MonsterBinScriptTable *s_pHorrenBin;

static void onhorrendousAttack()
{
    if (NoxCurrentHealth(OTHER))
    {
        NoxDamage(OTHER, SELF, 30, DAMAGE_BLADE);
        NoxPlayFX(FX_SPARK_EXPLOSION, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER), 0, 0);
    }
}

static void settingHorrendousBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->hp = 600;
    pTable->speed=100;
    pTable->run_multiplier = 1.3f;

    pTable->meleeAttackFrame = 5;
    pTable->minMeleeAttackDelay = 10;
    pTable->maxMeleeAttackDelay = 18;
    pTable->damage = 30;
    pTable->meleeRange = 45.0f;
    pTable->damage_type = DAMAGE_BLADE;
    pTable->strikeEvent = (int)s_customMeleeAttackCode;
    int *hack = (int *)pTable->missile_name;

    hack[15] = (int)&onhorrendousAttack;
    s_pHorrenBin = pTable;
}

int CreateHorrendous(float xpos, float ypos)
{
    int mon = CreateObjectAt("Horrendous", xpos, ypos);

    ChangeMonsterBinScript(mon, s_pHorrenBin);
    SyncWithCustomBinScript(mon);
    return mon;
}

void InitializeBinScript()
{
    WriteLog("initializeBin");
    goonBin();
    WriteLog("initializeBin-1");
    firespriteBin();
    WriteLog("initializeBin-1");
    redwizBin();
    settingBlackwidowBin();
    settingJandorBin();
    settingLichlordBin();
    settingHecubahBin();
    settingHorrendousBin();
}
