
#ifndef MON_BIN_H__
#define MON_BIN_H__

#include "../include/monsterBinscript.h"

int CreateGoon(float xpos, float ypos);
int CreateFiresprite(float xpos, float ypos);
int CreateRedWizard(float xpos, float ypos);
int CreateBlackwidow(float xpos, float ypos);
int CreateJandor(float xpos, float ypos);
int CreateLichKing(float xpos, float ypos);
int CreateHecubah(float xpos, float ypos);
int CreateHorrendous(float xpos, float ypos);
void InitializeBinScript();

#endif
