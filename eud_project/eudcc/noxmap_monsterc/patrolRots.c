
#include "patrolRots.h"
#include "../include/stringUtil.h"
#include "../include/builtins.h"
#include "../include/intTypes.h"

const char *s_patrolRotPrefix = "sewerRot";

typedef struct _patrolRot
{
    int rotObject;
    int go;
    int back;
} PatrolRotObject;

static PatrolRotObject s_rots[12];
static uint32_t s_rotLength = sizeof(s_rots)/sizeof(s_rots[0]);
static int s_rotCount = 0;

static void loopPatrolRots();

static void backpatrolRots()
{
    for (uint32_t i = 0 ; i < s_rotLength ; ++i)
    {
        NoxMove( s_rots[i].rotObject, s_rots[i].back);
    }
    if (s_rotCount)
    {
        if (--s_rotCount)
            NoxSecondTimer(5, &loopPatrolRots);
    }
}

static void loopPatrolRots()
{
    for (uint32_t i = 0 ; i < s_rotLength ; ++i)
    {
        NoxMove( s_rots[i].rotObject, s_rots[i].go);
    }
    NoxSecondTimer(5, &backpatrolRots);
}

void patrolRotTriggered()
{
    if (!s_rotCount)
        NoxSecondTimer(3, &loopPatrolRots);

    if (s_rotCount < 20)
        ++s_rotCount;
}

static void putPatrolRots(int id, int go, int home)
{
    char name[64];

    CopyString(s_patrolRotPrefix, name);
    ConcatenateString(name, IntToString(id+1));
    s_rots[id].rotObject = NoxGetObject(name);
    s_rots[id].go=go;
    s_rots[id].back=home;
}

void InitializePatrolRots()
{
    int id = 0;

    putPatrolRots(id++, 64, 76);
    putPatrolRots(id++, 65, 77);
    putPatrolRots(id++, 66, 78);
    putPatrolRots(id++, 67, 79);
    putPatrolRots(id++, 68, 80);
    putPatrolRots(id++, 69, 81);
    putPatrolRots(id++, 70, 82);
    putPatrolRots(id++, 71, 83);
    putPatrolRots(id++, 72, 84);
    putPatrolRots(id++, 73, 85);
    putPatrolRots(id++, 74, 86);
    putPatrolRots(id++, 75, 87);
}


