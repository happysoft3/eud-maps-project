

#include "mapevent.h"
#include "../include/builtins.h"

void MapInitialize()
{
    BuiltinsInitialize();
    OnInitialMap();
}

void MapExit()
{
    OnShutdownMap();
}

