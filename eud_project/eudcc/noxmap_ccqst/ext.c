
#include "ext.h"
#include "../include/builtins.h"

void openSurpriseWalls()
{
    NoxObjectOff(SELF);

    for (int i = 0 ; i < 5 ; i ++)
    {
        NoxWallOpen(NoxWall(147+i, 79+i));
        NoxWallOpen(NoxWall(145+i, 81+i));
    }
}

void openNewdoor()
{
    NoxObjectOff(SELF);
    NoxUnlockDoor(NoxGetObject("newpartdoor1"));
    NoxWallOpen(NoxWall(109, 59));
    NoxObjectOn(NoxGetObject("mobmecg"));
}

void openWestIxWalls()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(209, 91));
    NoxWallOpen(NoxWall(210, 92));
    NoxWallOpen(NoxWall(211, 93));
}

void openNorthIxWalls()
{
    NoxObjectOff(SELF);
    for (int i = 0 ; i < 3 ; i++)
    {
        NoxWallOpen(NoxWall(194+i, 98-i));
        NoxWallOpen(NoxWall(195+i, 99-i));
    }
}

void openAncientNorthWall()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(169, 83));
}

void wishingWell()
{
    NoxRestoreHealth(OTHER, 50);
    NoxMoveWaypoint(7, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
    NoxPlayAudioByIndex(SOUND_RestoreHealth, 7);
}

void openIxNorthWalls2()
{
    NoxObjectOff(SELF);
    for (int i = 0 ; i < 5 ; i ++)
        NoxWallOpen(NoxWall(207+i, 77-i));
}

#define GROUP_X1 0

void openIxThreeGenWalls()
{
    static int count;

    if (++count >= 3)
        NoxWallGroupOpen(GROUP_X1);
}

void openIxEastWalls2()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(73, 103));
    NoxWallOpen(NoxWall(74, 104));
    NoxWallOpen(NoxWall(78, 108));
    NoxWallOpen(NoxWall(79, 109));
}

void openIxWestWalls2()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(59, 117));
    NoxWallOpen(NoxWall(60, 118));
    NoxWallOpen(NoxWall(64, 122));
    NoxWallOpen(NoxWall(65, 123));
}

void openIxGenLeft()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(180, 74));
    NoxWallOpen(NoxWall(181, 75));
    NoxWallOpen(NoxWall(182, 76));
}

#define GROUP_X2 5
void openIxAll()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(GROUP_X2);
}

void openrightGenWalls()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(112, 46));
    NoxWallOpen(NoxWall(113, 47));
    NoxWallOpen(NoxWall(114, 48));
    NoxWallOpen(NoxWall(115, 49));
}

void teleportTwoGen()
{
    NoxObjectOff(SELF);
    float x1 = NoxGetWaypointX(16), y1 = NoxGetWaypointY(16);
    float x2 = NoxGetWaypointX(17), y2 = NoxGetWaypointY(17);

    int gen1 = NoxGetObject("mgen1"), gen2 = NoxGetObject("mgen2");

    NoxMoveObject(gen1, x1, y1);
    NoxMoveObject(gen2, x2, y2);
    NoxObjectOn(gen1);
    NoxObjectOn(gen2);
    NoxPlayFX(FX_SMOKE_BLAST, x1, y1, 0,0);
    NoxPlayFX(FX_SMOKE_BLAST, x2, y2, 0,0);
    NoxPlayFX(FX_TELEPORT, x1, y1, 0,0);
    NoxPlayFX(FX_TELEPORT, x2, y2, 0,0);
}

