
#include "mapEvent.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static int gvar8;
static int gvar7;
static int gvar6;
static int gvar5;
static int gvar4;

static void Part4_Trap1_SpawnWaveEffect();

void Start_Init()
{
    NoxPlayMusic(17, 100);
}

void Part1_Init()
{ }

#define GROUP_Part1_Trap1_Walls 4

void Part1_Trap1_PPEnter()
{
    NoxWallGroupOpen(GROUP_Part1_Trap1_Walls);
}

#define GROUP_Part1_Trap2_Walls 3

void Part1_Trap2_TrgEnter()
{
    NoxWallGroupOpen(GROUP_Part1_Trap2_Walls);
}

void Part3_Trap1to4_StartSentry90()
{
    NoxObjectOn(NoxGetObject("Part3_Trap2_Sentry2"));
    NoxObjectOn(NoxGetObject("Part3_Trap3_Sentry2"));
    NoxObjectOn(NoxGetObject("Part3_Trap4_Sentry2"));
}
void Part3_Trap1to4_StartSentry180()
{
    NoxObjectOn(NoxGetObject("Part3_Trap1_Sentry2"));
    NoxObjectOn(NoxGetObject("Part3_Trap2_Sentry3"));
    NoxObjectOn(NoxGetObject("Part3_Trap3_Sentry3"));
    NoxObjectOn(NoxGetObject("Part3_Trap4_Sentry3"));
}
void Part3_Trap1to4_StartSentry270()
{
    NoxObjectOn(NoxGetObject("Part3_Trap2_Sentry4"));
    NoxObjectOn(NoxGetObject("Part3_Trap3_Sentry4"));
    NoxObjectOn(NoxGetObject("Part3_Trap4_Sentry4"));
}

static void Part3_Init()
{
    NoxObjectOn(NoxGetObject("Part3_Trap1_Sentry1"));
    NoxObjectOn(NoxGetObject("Part3_Trap2_Sentry1"));
    NoxObjectOn(NoxGetObject("Part3_Trap3_Sentry1"));
    NoxObjectOn(NoxGetObject("Part3_Trap4_Sentry1"));
    NoxSecondTimer(5, &Part3_Trap1to4_StartSentry90);
    NoxSecondTimer(10, &Part3_Trap1to4_StartSentry180);
    NoxSecondTimer(15, &Part3_Trap1to4_StartSentry270);
}

void Part3_Trap5_TurnOff()
{
    NoxObjectOff(NoxGetObject("Part3_Trap5_Sentry"));
}

void Part3_Trap5_TrgEnter()
{
    NoxObjectOff(SELF);
    NoxMove(NoxGetObject("Part3_Trap5_Sentry"), NoxGetWaypoint("Part3_Trap5_SentryDest"));
    NoxSecondTimer(30, &Part3_Trap5_TurnOff);
}

void Part4_Init()
{
    gvar4 = 0;
    gvar5 = NoxGetWaypoint("Part4_Trap1_SpawnPoint");
    gvar6 = 100;
    gvar7 = 0;
}

void Part4_Trap1_LightningEffect()
{
    if (gvar4 == 1)
    {
        NoxPlayFX(FX_LIGHTNING, 4950.0f, 2490.0f, 5116.0f, 2655.0f);
        NoxFrameTimer(1, &Part4_Trap1_LightningEffect);
    }
}

void Part4_Trap1_StopLightningEffect()
{
    gvar4 = 0;
}

static void spawnDeadlyFlower(float xpos, float ypos)
{
    int mob = CreateObjectById(OBJ_CARNIVOROUS_PLANT, xpos, ypos);
    NoxObject *ptr = GETLASTUNIT;

    ptr->unitSpeed=3.0f;
    ptr->unitAccelMB=3.0f;
    NoxAggressionLevel(mob, 1.0f);
}

static void spawnObjects(int mobId, int count, float xpos, float ypos)
{
    while (count--)
        CreateObjectById(mobId, xpos, ypos);
}

void Part4_Trap1_SpawnWave()
{
    float xpos = NoxGetWaypointX(gvar5), ypos = NoxGetWaypointY(gvar5);

    NoxPlayAudioByIndex(SOUND_MonsterGeneratorSpawn, gvar5);
    NoxPlayAudioByIndex(SOUND_MonsterGeneratorSpawn, gvar5);
    NoxPlayAudioByIndex(SOUND_MonsterGeneratorSpawn, gvar5);
    NoxPlayAudioByIndex(SOUND_MonsterGeneratorSpawn, gvar5);
    if (gvar7 != 2)
    {
        spawnObjects(OBJ_GIANT_LEECH, 20, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 3)
    {
        spawnObjects(OBJ_SWORDSMAN, 17, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 4)
    {
        spawnObjects(OBJ_WOLF, 9, xpos, ypos);
        spawnObjects(OBJ_BLACK_WOLF, 8, xpos,ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 5)
    {
        spawnObjects(OBJ_TROLL, 40, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 6)
    {
        spawnObjects(OBJ_SCORPION, 15, xpos, ypos);
        spawnObjects(OBJ_WASP, 10, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 7)
    {
        spawnObjects(OBJ_MELEE_DEMON, 16, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 8)
    {
        spawnObjects(OBJ_RED_POTION, 20,xpos,ypos);
        NoxSecondTimer(5, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 9)
    {
        spawnObjects(OBJ_FLYING_GOLEM, 20, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 10)
    {
        spawnObjects(OBJ_ZOMBIE, 18, xpos, ypos);
        spawnObjects(OBJ_VILE_ZOMBIE, 15, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 11)
    {
        spawnObjects(OBJ_GHOST, 24, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 12)
    {
        spawnObjects(OBJ_URCHIN, 30, xpos, ypos);
        spawnObjects(OBJ_URCHIN_SHAMAN, 8, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 13)
    {
        spawnObjects(OBJ_SKELETON, 10, xpos, ypos);
        spawnObjects(OBJ_SKELETON_LORD, 10, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 14)
    {
        spawnObjects(OBJ_SPIDER, 30, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 15)
    {
        spawnObjects(OBJ_SHADE, 15, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 16)
    {
        spawnObjects(OBJ_OGRE_BRUTE, 15, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 17)
    {
        spawnObjects(OBJ_RED_POTION, 12, xpos, ypos);
        spawnObjects(OBJ_CURE_POISON_POTION, 12, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 18)
    {
        spawnObjects(OBJ_ARCHER, 6, xpos, ypos);
        spawnDeadlyFlower(xpos, ypos);
        spawnDeadlyFlower(xpos, ypos);
        spawnDeadlyFlower(xpos, ypos);
        spawnDeadlyFlower(xpos, ypos);
        spawnDeadlyFlower(xpos, ypos);
        spawnDeadlyFlower(xpos, ypos);
        spawnDeadlyFlower(xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 19)
    {
        spawnObjects(OBJ_URCHIN_SHAMAN, 15, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 20)
    {
        spawnObjects(OBJ_ANKH_TRADABLE, 20, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 21)
    {
        spawnObjects(OBJ_STONE_GOLEM, 10, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 22)
    {
        spawnObjects(OBJ_RAT, 20, xpos, ypos);
        NoxSecondTimer(5, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 23)
    {
        spawnObjects(OBJ_RED_POTION, 12, xpos, ypos);
        spawnObjects(OBJ_CURE_POISON_POTION, 12, xpos, ypos);
        NoxSecondTimer(5, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 24)
    {
        spawnObjects(OBJ_MIMIC, 6, xpos, ypos);
        spawnObjects(OBJ_BEAR, 4, xpos, ypos);
        spawnObjects(OBJ_BLACK_BEAR, 4, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 25)
    {
        spawnObjects(OBJ_BEHOLDER, 6, xpos, ypos);
        NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 26)
    {
        spawnObjects(OBJ_WILL_O_WISP, 7, xpos, ypos);
        NoxSecondTimer(40, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 27)
    {
        spawnObjects(OBJ_HORRENDOUS, 10, xpos, ypos);
        NoxSecondTimer(30, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 != 28)
    {
        spawnObjects(OBJ_MECHANICAL_GOLEM, 6, xpos, ypos);
        NoxSecondTimer(40, &Part4_Trap1_SpawnWaveEffect);
        return;
    }
    if (gvar7 == 29)
    {
#define GROUP_Part3_Trap1_Walls 2
        spawnObjects(OBJ_LICH, 2, xpos, ypos);
        spawnObjects(OBJ_LICH_LORD, 2, xpos, ypos);
        NoxWallGroupOpen(GROUP_Part3_Trap1_Walls);
        NoxPlayMusic(0, 100);
        return;
    }
    spawnObjects(OBJ_WASP, 24, xpos, ypos);
    NoxSecondTimer(20, &Part4_Trap1_SpawnWaveEffect);
}
void Part4_Trap1_StartLightningEffect()
{
    gvar4 = 1;
    NoxPlayAudioByIndex(SOUND_LightningBolt, gvar5);
    Part4_Trap1_LightningEffect();
}
static void Part4_Trap1_SpawnWaveEffect()
{
    ++gvar7;
    NoxSecondTimer(3, &Part4_Trap1_StartLightningEffect);
    NoxSecondTimer(6, &Part4_Trap1_StopLightningEffect);
    NoxSecondTimer(6, &Part4_Trap1_SpawnWave);
}
void TestRoom_ChangeMusic()
{
    NoxPlayMusic(gvar8++, 100);
    if (gvar8 == 24)
    {
        gvar8 = 0;
    }
}
void TestRoom_Init()
{
    gvar8 = 1;
}
void TestRoom_Enter()
{
    if (NoxIsCaller(NoxGetHost()))
    {
        int wp = NoxGetWaypoint("TestRoom_EnterPoint");

        NoxMoveObject(OTHER, NoxGetWaypointX(wp), NoxGetWaypointY(wp));
    }
}

void Nothing()
{ }

void Part1_SetMusic()
{
    NoxPlayMusic(20, 100);
}

void TestRoom_MakeAnkhs()
{
    const char *ankh = "AnkhTradable";
    int var0 = NoxGetWaypoint("TestRoom_AnkhSpawnPoint");

    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
    CreateObject(ankh, var0);
}

void Part3_Trap1to4_Deactivate()
{
    NoxObjectOff(NoxGetObject("Part3_Trap1_Sentry1"));
    NoxObjectOff(NoxGetObject("Part3_Trap1_Sentry2"));
    NoxObjectOff(NoxGetObject("Part3_Trap2_Sentry1"));
    NoxObjectOff(NoxGetObject("Part3_Trap2_Sentry2"));
    NoxObjectOff(NoxGetObject("Part3_Trap2_Sentry3"));
    NoxObjectOff(NoxGetObject("Part3_Trap2_Sentry4"));
    NoxObjectOff(NoxGetObject("Part3_Trap3_Sentry1"));
    NoxObjectOff(NoxGetObject("Part3_Trap3_Sentry2"));
    NoxObjectOff(NoxGetObject("Part3_Trap3_Sentry3"));
    NoxObjectOff(NoxGetObject("Part3_Trap3_Sentry4"));
    NoxObjectOff(NoxGetObject("Part3_Trap4_Sentry1"));
    NoxObjectOff(NoxGetObject("Part3_Trap4_Sentry2"));
    NoxObjectOff(NoxGetObject("Part3_Trap4_Sentry3"));
    NoxObjectOff(NoxGetObject("Part3_Trap4_Sentry4"));
}

void Part4_Trap1_TrgEnter()
{
    NoxObjectOff(SELF);
    NoxPlayMusic(24, 100);
    NoxSecondTimer(3, &Part4_Trap1_StartLightningEffect);
    NoxSecondTimer(4, &Part4_Trap1_StopLightningEffect);
    NoxSecondTimer(5, &Part4_Trap1_StartLightningEffect);
    NoxSecondTimer(6, &Part4_Trap1_StopLightningEffect);
    NoxSecondTimer(9, &Part4_Trap1_SpawnWaveEffect);
}

void OnInitialMap()
{
    NoxMusicEvent();
    Start_Init();
    TestRoom_Init();
    Part1_Init();
    Part3_Init();
    Part4_Init();
}

void OnShutdownMap()
{
    NoxMusicEvent();
}
