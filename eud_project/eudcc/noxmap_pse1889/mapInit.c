
#include "mapEvent.h"

#include "../include/playerhandler.h"
#include "../include/team.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"

void MapInitialize()
{
    BuiltinsInitialize();
    NoxMusicEvent();
    InitializeSmartMemoryDestructor();
    InitialPlayerUpdateRewritten();
    EnableCoopTeamMode();
    OnInitialMap();
}

void MapExit()
{
    NoxMusicEvent();
    DisableCoopTeamMode();
    ResetAllPlayerHandler();
}

