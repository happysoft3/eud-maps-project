
#include "myBarrel.h"
#include "randomItem.h"
#include "monsters.h"
#include "myTraps.h"
#include "../include/fxeffect.h"
#include "../include/mathlab.h"
#include "../include/unitExtend.h"
#include "../include/shop.h"
#define DISABLE_LOGFILE
#include "../include/logfile.h"
#include "../include/noxobject.h"
// #include "../include/memoryUtil.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"
#include "../include/thingDb.h"

typedef void(*rand_create_fn)(float, float);
static int s_myjunks[]={
    OBJ_WIZARD_WORKSTATION_1, OBJ_WIZARD_WORKSTATION_2_D, OBJ_URCHIN_SHELVES_FULL_1, OBJ_STOCKS_5,
    OBJ_SMALL_STONE_BLOCK
    };

static void createJunk(float xpos, float ypos)
{
    static uint32_t lastIndex = (sizeof(s_myjunks)/sizeof(s_myjunks[0]))-1;

    if (NoxRandomInteger(0, 3))
        CreateObjectById(s_myjunks[NoxRandomInteger( 0, lastIndex) ], xpos, ypos);
    else
    {
        int r = CreateObjectById(OBJ_ROTATING_SPIKES, xpos, ypos);
        NoxObject *ptr = GETLASTUNIT;

        ptr->Mass = 21.0f;
    }
    
}

static void createRandomitem(float xpos, float ypos)
{
    WRITE_LOG("<randomitem>");
    NoxFrozen( DO_CREATE_RANDOMITEM(xpos, ypos), TRUE);
}

static void createRandomMonster(float xpos, float ypos)
{
    WRITE_LOG("<randommob>");
    CreateRandomMonster(xpos, ypos);
}

static void createSweet(float xpos, float ypos)
{
    int count = 20;

    while (--count)
    {
        CreateObjectById(OBJ_RED_POTION, xpos + MathSine((count * 18) + 90, 60.0f), ypos + MathSine(count * 18, 60.0f));
    }
}

static int weaponShop(float xpos, float ypos)
{
    int s=CreateObjectById(OBJ_SHOPKEEPER_PURPLE, xpos, ypos);

    NoxLookWithAngle(s, 32);
    NoxFrozen( CreateObjectById(OBJ_FAIRY_JAR, xpos - 19.0f, ypos + 19.0f), TRUE);
    NoxFrozen(s, TRUE);
    ShopUtilSetTradePrice(s, 3.0f, 0);
    ShopUtilAppendItemWithProperties(s, OBJ_GREAT_SWORD, 6, 
        ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_fire4, ITEM_PROPERTY_vampirism4);
    ShopUtilAppendItemWithProperties(s, OBJ_ROUND_CHAKRAM, 6,
        ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_fire4, ITEM_PROPERTY_vampirism4);
    ShopUtilAppendItemWithProperties(s, OBJ_WAR_HAMMER, 6,
        ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_venom4, ITEM_PROPERTY_lightning4);

    return s;
}
static int armorShop(float xpos, float ypos)
{
    int s=CreateObjectById(OBJ_SHOPKEEPER_WARRIORS_REALM, xpos, ypos);

    NoxLookWithAngle(s, 92);
    NoxFrozen( CreateObjectById(OBJ_FAIRY_JAR, xpos + 19.0f, ypos + 19.0f), TRUE);
    NoxFrozen(s, TRUE);
    ShopUtilSetTradePrice(s, 2.0f, 0);
    ShopUtilAppendItemWithProperties(s, OBJ_ORNATE_HELM, 6, 
        ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect4);
    ShopUtilAppendItemWithProperties(s, OBJ_BREASTPLATE, 6,
        ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect4);
    ShopUtilAppendItemWithProperties(s, OBJ_PLATE_ARMS, 6,
        ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect4);
    ShopUtilAppendItemWithProperties(s, OBJ_PLATE_BOOTS, 6,
        ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect4);
    ShopUtilAppendItemWithProperties(s, OBJ_PLATE_LEGGINGS, 6,
        ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_LightningProtect4);
    ShopUtilAppendItemWithProperties(s, OBJ_PLATE_BOOTS, 6,
        ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_Regeneration4);
    ShopUtilAppendItemWithProperties(s, OBJ_PLATE_LEGGINGS, 6,
        ITEM_PROPERTY_armorQuality6, ITEM_PROPERTY_Matrial7, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_Regeneration4);
    return s;
}

static void createShop(float xpos, float ypos)
{
    if (NoxRandomInteger(0, 1))
    {
        NoxRandomInteger(0, 1) ? weaponShop(xpos, ypos) : armorShop(xpos, ypos);
    }
    else
    {
        createRandomMonster(xpos, ypos);
    }
}

static rand_create_fn s_rndCreateFns[]={
    createRandomitem, createRandomMonster, CreateRandomMyTrap, createSweet, 
    createShop, createRandomMonster, CreateRandomMyTrap, createRandomitem, createJunk
};
static uint32_t s_rndCreateFnLength = sizeof(s_rndCreateFns)/sizeof(s_rndCreateFns[0]);

static void onDestroyBarrel()
{
    float xpos = NoxGetObjectX(SELF), ypos = NoxGetObjectY(SELF);

    PlaySoundAround(SELF, SOUND_LOTDBarrelBreak);
    NoxDeleteObject(SELF);
    CreateObjectAt("BarrelBreakingLOTD", xpos, ypos);
    s_rndCreateFns[NoxRandomInteger(0, s_rndCreateFnLength - 1)](xpos, ypos);
    WRITE_LOG("onDestroyBarrel end");
}

static int createMyBarrel(float xpos, float ypos)
{
    int b=CreateObjectAt("BarrelLOTD", xpos, ypos);
    NoxObject *ptr = UnitToPtr(b);

    ptr->Mass = 9999.0f;
    SetUnitCallbackExtended(b, &onDestroyBarrel, EXTENDED_ON_DESTROY);
    SetUnitMaxHealth(b, 100);
    return b;
}

typedef struct _barrelDispos
{
    int locationId;
    int count;
    float vectX;
    float vectY;
} BarrelDispos;

static void deferredBarrelDisposition(BarrelDispos *dispos)
{
    int count = dispos->count;
    int b = createMyBarrel(NoxGetWaypointX(dispos->locationId), NoxGetWaypointY(dispos->locationId));

    while ((--count) >= 0)
    {
        b = createMyBarrel(NoxGetObjectX(b) + dispos->vectX, NoxGetObjectY(b) + dispos->vectY);
    }
}

static void placeBarrelRow(int location, int count)
{
    BarrelDispos disp;

    disp.locationId=location;
    disp.count =count;
    disp.vectX=92.0f;
    disp.vectY=92.0f;
    deferredBarrelDisposition(&disp);
}

static void placeLongLeftUp(int count)
{
    if (count)
    {
        placeBarrelRow(35, 40);
        NoxMoveWaypoint(35, NoxGetWaypointX(35) + 92.0f, NoxGetWaypointY(35) - 92.0f);
        NoxFrameTimerWithArg(1, count - 1, &placeLongLeftUp);
    }
}

void InitializeMyBarrel()
{
    placeLongLeftUp(19);
    placeBarrelRow(23, 32);
    placeBarrelRow(24, 34);
    placeBarrelRow(25, 36);
    placeBarrelRow(26, 38);
    placeBarrelRow(27, 40);
    placeBarrelRow(28, 38);
    placeBarrelRow(29, 37);
    placeBarrelRow(30, 36);
    placeBarrelRow(31, 34);
    placeBarrelRow(32, 32);
    placeBarrelRow(33, 30);
    placeBarrelRow(34, 28);
}
