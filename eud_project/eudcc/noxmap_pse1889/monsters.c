
#include "monsters.h"
#include "mon_bin.h"
#include "randomItem.h"
#include "../include/printUtil.h"
#include "../include/monsterBinscript.h"
#include "../include/monster_action.h"
#include "../include/spellUtils.h"
#include "../include/noxobject.h"
#include "../include/stringUtil.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static int s_hostMonster;

static int s_myitemList[400];
static uint32_t s_myItemListCount = sizeof(s_myitemList)/sizeof(s_myitemList[0]);
static int s_myItemListCur;

static void commonMobDeath()
{
    NoxPlayFX(FX_TELEPORT, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 0, 0);
    NoxDeleteObjectTimer(SELF, 30);
}

static void decayingNextItem(int item)
{
    if (NoxIsObjectOn(item))
    {
        if (!GetOwner(item))
            NoxDeleteObjectTimer(item, 3);
    }
}

static void pushItemList(int item)
{
    decayingNextItem(s_myitemList[s_myItemListCur]);
    s_myitemList[s_myItemListCur] = item;
    s_myItemListCur = (s_myItemListCur+1)%s_myItemListCount;
}

static void dropItemSpecial()
{
    int item = DO_CREATE_RANDOMITEM(NoxGetObjectX(SELF), NoxGetObjectY(SELF));

    if (NoxMaxHealth(item))
        SetUnitMaxHealth(item, NoxMaxHealth(item) * 2);
    pushItemList(item);
    commonMobDeath();
}

static void onMobHurt()
{
    if (NoxGetCaller())
        return;
    
    NoxObject *ptr = UnitToPtr(SELF);

    if (ptr->decelTimeMB)
    {
        NoxDeleteObjectTimer(CreateObjectAt("GreenPuff", NoxGetObjectX(SELF), NoxGetObjectY(SELF)), 12);
        NoxDamage(SELF, 0, (int)ptr->decelTimeMB, DAMAGE_POISON);
    }
}

static void mobCommonProc(int mob)
{
    NoxSetOwner(s_hostMonster, mob);
    NoxAggressionLevel(mob, 1.0f);
    NoxRetreatLevel(mob, 0);
    NoxResumeLevel(mob, 0);
    NoxObject *ptr = UnitToPtr(mob);
    
    if (!(ptr->SubClass & 0x200))
        NoxSetCallback(mob, UnitCallback_OnHit, &onMobHurt);
    NoxSetCallback(mob, UnitCallback_OnDeath, &dropItemSpecial);
}

static int s_lastMobCount = 32;

static void decreaseLastMobCount()
{
    short buff[128], nb[16];

    CopyWideString(u"최종보스 남은 개수: ", buff);
    Utf8ToUnicodeString(IntToString(--s_lastMobCount), nb);
    ConcatenateWideString(buff, nb);
    WidePrintMessageAll(buff);
}

static void victoryEvent()
{
    WidePrintMessageAll(u"승리하셨습니다!");
}

static void lastMobDeath()
{
    if (s_lastMobCount > 0)
        decreaseLastMobCount();
    else
        victoryEvent();
    commonMobDeath();
}

static int createLastMonster(int location)
{
    int mob = CreateRedWizard(NoxGetWaypointX(location), NoxGetWaypointY(location));

    NoxSetCallback(mob, UnitCallback_OnDeath, &lastMobDeath);
    return mob;
}

static void startCreateLastmob(int count)
{
    if (count)
    {
        int mob = createLastMonster(106);

        mobCommonProc(mob);
        NoxFrameTimerWithArg(1, count - 1, &startCreateLastmob);
    }
}

static int createScorpion(float xpos, float ypos)
{
    int mob = CreateObjectById(OBJ_SCORPION, xpos, ypos);

    SetUnitMaxHealth(mob, 225);
    return mob;
}

static int createMystic(float xpos, float ypos)
{
    int mob = CreateObjectById(OBJ_WIZARD, xpos, ypos);

    SetUnitMaxHealth(mob, 275);
    ApplySpellToUnit(mob, SPELL_BLINK, 0);
    ApplySpellToUnit(mob, SPELL_FORCE_FIELD, SPELLFLAG_ON_DEFFENSIVE | SPELLFLAG_ON_ESCAPE);
    return mob;
}

static int createShaman(float xpos, float ypos)
{
    int mob = CreateObjectAt("UrchinShaman", xpos, ypos);

    SetUnitMaxHealth(mob, 175);
    ApplySpellToUnit(mob, SPELL_CONFUSE, 0);
    ApplySpellToUnit(mob, SPELL_MAGIC_MISSILE, SPELLFLAG_ON_OFFENSIVE);
    ApplySpellToUnit(mob, SPELL_PIXIE_SWARM, SPELLFLAG_ON_OFFENSIVE | SPELLFLAG_ON_DISABLING);
    return mob;
}

static int createOgreaxe(float xpos, float ypos)
{
    int mob = CreateObjectAt("GruntAxe", xpos, ypos);

    SetUnitMaxHealth(mob, 225);
    return mob;
}

static int createGargoyle(float xpos, float ypos)
{
    int mob = CreateObjectAt("EvilCherub", xpos, ypos);

    SetUnitMaxHealth(mob, 98);
    return mob;
}

static int createSkeleton(float xpos, float ypos)
{
    int mob = CreateObjectAt("SkeletonLord", xpos, ypos);

    SetUnitMaxHealth(mob, 295);
    return mob;
}

static int createOgrewarlord(float xpos, float ypos)
{
    int mob = CreateObjectAt("OgreWarlord", xpos, ypos);

    SetUnitMaxHealth(mob, 325);
    return mob;
}

static int createEmberdemon(float xpos, float ypos)
{
    int mob = CreateObjectAt("EmberDemon", xpos, ypos);

    SetUnitMaxHealth(mob, 225);
    return mob;
}

static int createBeholder(float xpos, float ypos)
{
    int mob = CreateObjectAt("Beholder", xpos, ypos);

    SetUnitMaxHealth(mob, 380);
    ApplySpellToUnit(mob, SPELL_BLINK, 0);
    ApplySpellToUnit(mob, SPELL_FORCE_FIELD, SPELLFLAG_ON_DEFFENSIVE);
    ApplySpellToUnit(mob, SPELL_SLOW, SPELLFLAG_ON_DISABLING);
    ApplySpellToUnit(mob, SPELL_SHOCK, SPELLFLAG_ON_ESCAPE);
    return mob;
}

static void deferredMimicAction(int mi)
{
    DO_CREATURE_ACTION_ESCORT(mi, mi);
    NoxAggressionLevel(mi, 1.0f);
}

static int createMimic(float xpos, float ypos)
{
    int mob = CreateObjectAt("Mimic", xpos, ypos);

    SetUnitMaxHealth(mob, 425);
    NoxFrameTimerWithArg(1, mob, &deferredMimicAction);
    return mob;
}

typedef int(*monster_create_fn_ty)(float, float);

static monster_create_fn_ty s_mobFunctions[]={
    CreateBlackwidow, CreateFiresprite, CreateGoon, createEmberdemon, createMimic,
    createOgrewarlord, createShaman, createSkeleton, createBeholder, createGargoyle,
    createOgreaxe, createScorpion, createMystic
};

int CreateRandomMonster(float xpos, float ypos)
{
    static int length = sizeof(s_mobFunctions)/sizeof(int);
    int mon = s_mobFunctions[NoxRandomInteger(0, length - 1)](xpos, ypos);

    mobCommonProc(mon);
    return mon;
}

void InitMonsters()
{
    s_hostMonster = CreateObjectAt("Hecubah", 100.0f, 100.0f);

    NoxFrozen(s_hostMonster, TRUE);
    InitializeBinScript();
}
