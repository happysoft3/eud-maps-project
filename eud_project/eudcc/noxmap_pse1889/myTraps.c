
#include "myTraps.h"
#include "../include/fxeffect.h"
#include "../include/xfer.h"
#define DISABLE_LOGFILE
#include "../include/logfile.h"
#include "../include/memoryUtil.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/sound_define.h"
#include "../include/objectIDdefines.h"

static void deferredCreateFonReal(int mark)
{
    int mis = CreateObjectAt("DeathBall", NoxGetObjectX(mark), NoxGetObjectY(mark));
#define FON_RANGE 16.0f
    NoxPushObjectTo(mis, NoxRandomFloat(-FON_RANGE, FON_RANGE), NoxRandomFloat(-FON_RANGE, FON_RANGE));
#undef FON_RANGE
    PlaySoundAround(mark, SOUND_ForceOfNatureRelease);
    NoxDeleteObjectTimer(mark, 9);
}

static void createFonTrap(float xpos, float ypos)
{
    int fx = CreateObjectAt("ForceOfNatureCharge", xpos, ypos);

    NoxFrameTimerWithArg(25, fx, &deferredCreateFonReal);
    PlaySoundAround(fx, SOUND_ForceOfNatureCast);
}

static void createFireboomOverlapped(float xpos, float ypos)
{
    NoxLookWithAngle(CreateObjectAt("TitanFireball", xpos, ypos), 0);
    NoxLookWithAngle(CreateObjectAt("TitanFireball", xpos, ypos), 64);
    NoxLookWithAngle(CreateObjectAt("TitanFireball", xpos, ypos), 128);
    NoxLookWithAngle(CreateObjectAt("TitanFireball", xpos, ypos), 192);
}

static void createBearTrap(float xpos, float ypos)
{
    const char *trp = NoxRandomInteger(0, 3) ? "BearTrap" : "PoisonGasTrap";
    #define BEAR_TRAP_GAP 38.0f
    CreateObjectAt(trp, xpos + BEAR_TRAP_GAP, ypos + BEAR_TRAP_GAP);
    CreateObjectAt(trp, xpos + BEAR_TRAP_GAP, ypos - BEAR_TRAP_GAP);
    CreateObjectAt(trp, xpos - BEAR_TRAP_GAP, ypos - BEAR_TRAP_GAP);
    CreateObjectAt(trp, xpos - BEAR_TRAP_GAP, ypos + BEAR_TRAP_GAP);
}

static void bomblookAround(int bomb)
{
    if (NoxCurrentHealth(bomb))
    {
        NoxLookWithAngle(bomb, NoxGetDirection(bomb) + 128);
    }
}

static void createBomber(float xpos, float ypos)
{
    int b=CreateObjectAt("BomberBlue", xpos, ypos);

    NoxAggressionLevel(b, 1.0f);
    NoxFrameTimerWithArg(3, b, &bomblookAround);
    NoxSetTrapSpells(b, SPELL_METEOR, SPELL_BURN, SPELL_FREEZE);
    NoxEnchant(b, ENCHANT_INVULNERABLE, 10.0f);
}

static void createGlyph(float xpos, float ypos)
{
    CreateGlyph(xpos, ypos, SPELL_MANA_BOMB, SPELL_SLOW, SPELL_EARTHQUAKE);
}

static void createElectricBoulder(float xpos, float ypos)
{
    SetUnitMaxHealth(CreateObjectById(OBJ_ROLLING_BOULDER, xpos, ypos), 400);
}

typedef struct _utilArrow
{
    int obj;
    int dir;
} UtilArrow;

static void changeutilArror(int arrow)
{
    NoxObject *ptr = UnitToPtr(arrow);
    int *ec = (int *)ptr->unitController;

    ec[3] = OBJ_LIGHTNING_BOLT;
}

static void deferredutilArrow(UtilArrow *pArrow)
{
    NoxLookWithAngle(pArrow->obj, pArrow->dir);
    NoxObjectOn(pArrow->obj);
    if (!NoxRandomInteger(0, 3))
        changeutilArror(pArrow->obj);
    FreeSmartMemory(pArrow);
}

static int utilArrow(float xpos, float ypos, int dir)
{
    int t=CreateObjectById(OBJ_ARROW_TRAP_1, xpos, ypos);
    UtilArrow *pArrow = (UtilArrow *)AllocSmartMemory(sizeof(UtilArrow));

    pArrow->obj = t;
    pArrow->dir = dir;
    NoxFrameTimerWithArg(1, (int)pArrow, (timer_callback)&deferredutilArrow);
    NoxObjectOff(t);
    return t;
}

static void putArrowTrap(float xpos, float ypos)
{
    utilArrow(xpos, ypos, 32);
    utilArrow(xpos, ypos, 96);
    utilArrow(xpos, ypos, 160);
    utilArrow(xpos, ypos, 224);
}

typedef void (*my_trap_fn)(float, float);
static my_trap_fn s_trapFns[]={
    createBearTrap, createBomber, createFireboomOverlapped, createFonTrap, createGlyph, createElectricBoulder, putArrowTrap};

void CreateRandomMyTrap(float xpos, float ypos)
{
    static int lastidx = (sizeof(s_trapFns)/sizeof(s_trapFns[0]))-1;
    WRITE_LOG("<randomTrap>");
    s_trapFns[NoxRandomInteger(0, lastidx)](xpos, ypos);
    WRITE_LOG("</randomTrap>");
}

