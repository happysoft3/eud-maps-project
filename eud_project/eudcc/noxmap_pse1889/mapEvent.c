
#include "mapEvent.h"
#include "myBarrel.h"
#include "monsters.h"
#include "../include/equipment.h"
#include "../include/printUtil.h"
#include "../include/logfile.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static void placeSword(int unit)
{
    for (int i = 0 ; i < 10 ; i ++)
    {
        unit = CreateObjectById(OBJ_LONGSWORD, NoxGetObjectX(unit) + 23.0f, NoxGetObjectY(unit) + 23.0f);
        SetEquipEnchanment(unit, ITEM_PROPERTY_weaponPower3, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_fire1, 0);
        NoxFrozen(unit, TRUE);
    }
}

static void openGamewalls()
{
    NoxWallGroupOpen(NoxGetWallGroup("g999"));
    NoxPlayAudioByIndex(SOUND_ArcheryContestBegins, 36);
    WidePrintMessageAll(u"비밀의 벽이 열립니다. 게임을 시작하십시오");
}

static void deferredInit()
{
    SetPicketText(NoxGetObject("mp1"), u"미로를 탐험하는 맵입니다요");
    NoxSecondTimer(5, &openGamewalls);
    placeSword(CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH, NoxGetWaypointX(37), NoxGetWaypointY(37)));
}

void OnInitialMap()
{
    CreateLogFile("pse01889log.txt");
    InitializeMyBarrel();
    InitMonsters();
    NoxFrameTimer(30, &deferredInit);
}


