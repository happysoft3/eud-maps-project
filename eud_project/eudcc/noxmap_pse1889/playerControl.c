
#include "playerControl.h"
#include "../include/playerHandler.h"
#include "../include/playerInfo.h"
#include "../include/noxobject.h"
#include "../include/stringUtil.h"
#include "../include/printUtil.h"
#include "../include/builtins.h"
#include "../include/sound_define.h"

#define FALSE 0
#define TRUE 1
#define NULLPTR 0

#define PLAYER_DEATH_FLAG 2

#define PLAYER_DATA_MAX 32
static PlayerData s_playerData[PLAYER_DATA_MAX];
static PlayerData *s_firstPlayerData = NULLPTR;

static void emptyAll(int user)
{
    while (NoxIsObjectOn(NoxGetLastItem(user)))
        NoxDeleteObject(NoxGetLastItem(user));
}

static void playerOnAlive(PlayerData *pdata)
{
    ///
}

static void playerOnDeath(PlayerData *pdata)
{
    if (!pdata->userName)
        return;

    short msg[128];

    CopyWideString(pdata->userName, msg);
    static short *myMent = u" 이(가) 적에게 격추되었습니다";
    ConcatenateWideString(msg, myMent);
    WidePrintMessageAll(msg);
}

static PlayerData *erasePlayerData(PlayerData *prev, PlayerData *cur)
{
    cur->userName = NULLPTR;
    if (prev == NULLPTR)
    {
        s_firstPlayerData = cur->pNext;
        return s_firstPlayerData;
    }
    prev->pNext = cur->pNext;
    return cur->pNext;
}

static PlayerData *loadPlayerData(int plrUnit)
{
    PlayerData *pdata = s_firstPlayerData;

    while (pdata)
    {
        if (plrUnit == pdata->unitId)
            return pdata;

        pdata = pdata->pNext;
    }
    return NULLPTR;
}

static void onPlayerInit(int plrUnit)
{
    if (NoxGetGold(plrUnit))
        NoxChangeGold(plrUnit, -NoxGetGold(plrUnit));
    ChangePlayerDieHandler(plrUnit);
    DisablePlayerSelfDamage(plrUnit);
}

static PlayerData *appendPlayer(int plrUnit)
{
    int pIndex = GetPlayerIndex(plrUnit);

    if (pIndex < 0)
        return NULLPTR;

    PlayerData *pdata = &s_playerData[pIndex];

    pdata->unitId = plrUnit;
    pdata->flags = 0;
    pdata->pIndex = pIndex;
    pdata->pNext = s_firstPlayerData;
    pdata->userName = PlayerIngameNickname(plrUnit);
    s_firstPlayerData = pdata;
    onPlayerInit(plrUnit);
    return pdata;
}

static int playerHandler(PlayerData *pdata)
{
    if (NoxMaxHealth(pdata->unitId))
    {
        NoxObject *ptr = UnitToPtr(pdata->unitId);

        if (ptr->flags & UNIT_FLAG_NO_COLLIDE)
            return 0;

        else if (NoxCurrentHealth(pdata->unitId))
        {
            playerOnAlive(pdata);
        }
        else if (!(pdata->flags & PLAYER_DEATH_FLAG))
        {
            playerOnDeath(pdata);
            pdata->flags ^= PLAYER_DEATH_FLAG;
        }
        return 1;
    }
    return 0;
}

void OnPlayerLoop()
{
    PlayerData *pdata = s_firstPlayerData, *prev = NULLPTR;
    int res = 0;

    while (pdata)
    {
        if (!playerHandler(pdata))
            pdata = erasePlayerData(prev, pdata);   //다음 노드를 반환한다
        else
        {
            prev = pdata;
            pdata = pdata->pNext;
        }
    }
    NoxFrameTimer(1, &OnPlayerLoop);
}

int GetClosestPlayer(int unit)
{
    PlayerData *pdata = s_firstPlayerData;
    float temp = 8192.0f, r = 0.0f;
    int ret = 0;

    while (pdata)
    {
        if (NoxCurrentHealth(pdata->unitId))
        {
            r = DistanceUnitToUnit(unit, pdata->unitId);
            if (r < temp)
            {
                temp = r;
                ret = pdata->unitId;
            }
        }
        pdata = pdata->pNext;
    }
    return ret;
}

static void playerJoinOnMap(PlayerData *pdata)
{
    static int respawnSpot[]={12,13,14,15,16,17,18,19,20,21,22};
    int spot = respawnSpot[NoxRandomInteger(0, (sizeof(respawnSpot)/sizeof(respawnSpot[0]))-1)];
    int plrUnit = pdata->unitId;

    if (pdata->flags & PLAYER_DEATH_FLAG)
        pdata->flags ^= PLAYER_DEATH_FLAG;

    NoxMoveObject(plrUnit, NoxGetWaypointX(spot), NoxGetWaypointY(spot));
    NoxPlayAudioByIndex(SOUND_BlindOff, spot);
}

static int registPlayer(int plrUnit, PlayerData *pdata)
{
    if (NoxCurrentHealth(plrUnit))
    {
        if (!pdata) //새로운 사용자인 경우,
            pdata = appendPlayer(plrUnit);

        if (!pdata)
        {
            NoxPrintToAll("error");
            return FALSE;
        }
        playerJoinOnMap(pdata);
        return TRUE;
    }
    return FALSE;
}

void afterGetPlayer()
{
    if (NoxCurrentHealth(OTHER))
    {
        int plrUnit = NoxGetCaller();

        if (registPlayer(plrUnit, loadPlayerData(plrUnit)))
        {
            NoxEnchantOff(plrUnit, ENCHANT_ANCHORED);
            NoxEnchantOff(plrUnit, ENCHANT_ANTI_MAGIC);
            return;
        }
        NoxMoveObject(plrUnit, NoxGetWaypointX(11), NoxGetWaypointY(11));
    }
}

void getPlayer()
{
    if (NoxCurrentHealth(OTHER))
    {
        PlayerData *pdata = loadPlayerData(NoxGetCaller());

        if (pdata)
        {
            if (registPlayer(NoxGetCaller(), pdata))
                return;
        }
        emptyAll(OTHER);
        NoxEnchant(OTHER, ENCHANT_ANCHORED, 0);
        NoxEnchant(OTHER, ENCHANT_ANTI_MAGIC, 0);
        NoxMoveObject(OTHER, NoxGetWaypointX(11), NoxGetWaypointY(11));
    }
}
