
#include "linkedList.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"

#define NULLPTR (void*)0

static LinkedNode *createNode(int value)
{
    LinkedNode *node=(LinkedNode *)AllocSmartMemory(sizeof(LinkedNode));

    node->value=value;
    node->next=NULLPTR;
    node->prev=NULLPTR;
    return node;
}

LinkedList *CreateLinkedList()
{
    LinkedList *inst=(LinkedList *)AllocSmartMemory(sizeof(LinkedList));

    inst->count=0;
    LinkedNode *front=createNode(0), *back=createNode(0);

    front->next=back;
    back->prev=front;
    inst->head=front;
    inst->tail=back;
    return inst;
}

void ClearLinkedList(LinkedList *pList)
{
    LinkedNode *node=pList->head, *del;

    while (node)
    {
        del=node;
        node=node->next;
        FreeSmartMemory(del);
    }
    pList->count=0;
}

void DeleteLinkedList(LinkedList *pList)
{
    ClearLinkedList(pList);
    FreeSmartMemory(pList);
}

void PushbackLinkedList(LinkedList *pList, int value)
{
    LinkedNode *node= createNode(value), *tail = pList->tail;

    LinkedNode *prev=tail->prev;
    prev->next=node;
    node->prev=prev;
    node->next=tail;
    tail->prev=node;
    ++(pList->count);
}

void PushfrontLinkedList(LinkedList *pList, int value)
{
    LinkedNode *node=createNode(value), *head=pList->head;
    LinkedNode *next=head->next;

    next->prev=node;
    node->prev=head;
    node->next=next;
    head->next=node;
    ++(pList->count);
}

void LinkedListPop(LinkedList *pList, LinkedNode *node)
{
    LinkedNode *next = node->next, *prev= node->prev;

    if (!next || !prev)
        return;

    prev->next=next;
    next->prev=prev;
    --(pList->count);
    FreeSmartMemory(node);
}

void PopFrontLinkedList(LinkedList *pList)
{
    LinkedListPop(pList, pList->head->next);
}
void PopBackLinkedList(LinkedList *pList)
{
    LinkedListPop(pList, pList->tail->prev);
}

LinkedNode *LinkedListFront(LinkedList *pList)
{
    if (pList->head->next == pList->tail)
        return NULLPTR;

    return pList->head->next;
}
LinkedNode *LinkedListBack(LinkedList *pList)
{
    LinkedNode *tail = pList->tail->prev;

    if (tail==pList->head)
        return NULLPTR;

    return tail;
}

// int FrontLinkedList(LinkedList *pList, int *dest)
// {
//     if (pList->head->next == pList->tail)
//         return FALSE;

//     *dest = pList->head->next->value;
//     return TRUE;
// }

// int BackLinkedList(LinkedList *pList, int *dest)
// {
//     LinkedNode *tail = pList->tail->prev;

//     if (tail==pList->head)
//         return FALSE;

//     *dest=tail->value;
//     return TRUE;
// }
