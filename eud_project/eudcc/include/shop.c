
#include "shop.h"
#include "thingDb.h"
#include "noxobject.h"
#include "builtins.h"

void ShopUtilItemBasePrivate(int shopunit, int **destptr)
{
	NoxObject *ptr = UnitToPtr(shopunit);
	
	*destptr=(int *)ptr->unkFn2B0DataPtr;
}

int ShopUtilCheckAlreadyExist(int shopUnit, short thingId)
{
	int *base;
	
	ShopUtilItemBasePrivate(shopUnit, &base);
	if (!base)
		return FALSE;
	int count =*base;
	base+=1;
	int rep=-1;
	while(++rep<count)
	{
		int *tptr=(int *)(*base);
		if (*tptr==thingId)
			return TRUE;
		base+=7;
	}
	return FALSE;
}

static void ShopUtilFindItem(int shopunit, short thingId, int second, int **destptr)
{
	int *base;
	
	ShopUtilItemBasePrivate(shopunit, &base);
	if (!base)
		return;
	
	int count = *base;	
	base+=1;
	int rep=-1;
	while (++rep<count)
	{
		int *tptr=(int *)(*base);
		if (*tptr==thingId)
		{
			if(!second)
			{
				*destptr=base;
				break;
			}
			else --second;
		}
		base+=7;
	}	
}

void ShopUtilGetItem(int shopUnit, int index, int *pDestPtr)
{
	if (index&(~0x1f))
		return;
	int *base;
	ShopUtilItemBasePrivate(shopUnit, &base);
	if (!base)
		return;
	int count =*base;
	if (index>=count)
		return;
	base+=1;
	*pDestPtr=(int)( base+(index*7) );
}

void ShopUtilAllItemCount(int shopunit, int *pCount)
{
    int *base;

    ShopUtilItemBasePrivate(shopunit, &base);
	*pCount=*base;
}

void ShopUtilItemCount(int shopunit, short thingId, int second, int *pCount)
{
    int *ptr = 0;

    ShopUtilFindItem(shopunit, thingId, second, &ptr);
    if (ptr == 0)
        return;

	*pCount=ptr[1];
}

void ShopUtilSetItemCount(int shopunit, short thingId, int second, int setTo)
{
    int *ptr = 0;

    ShopUtilFindItem(shopunit, thingId, second, &ptr);
    if (ptr == 0)
        return;
	
	setTo = (--setTo)&0x1f;
	ptr[1]=++setTo;
}

void ShopUtilSetTradePrice(int shopunit, float buyPrice, float sellPrice)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (base == 0)
        return;

	float *priceTable = (float *)(base + 429);
	
	priceTable[0]=buyPrice;
	priceTable[1]=sellPrice;
}

void ShopUtilGetTradePrice(int shopunit, float *buyPrice, float *sellPrice)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (base == 0)
        return;

    float *priceTable = (float *)(base + 429);
	
    if (buyPrice)
		*buyPrice=priceTable[0];
    if (sellPrice)
		*sellPrice=priceTable[1];
}

void ShopUtilAppendItem(int shopunit, short thingId, int itemCount)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (!base)
        return;
	if (*base&(~0x1f))
		return;
	
	if (ShopUtilCheckAlreadyExist(shopunit, thingId))
		return;
    
    int *ptr = base + ((7*(base[0]++))+1);
	
    int loadThingdb = (int )GET_THINGDB_INFO(thingId);
    
	ptr[0]=loadThingdb;

    itemCount = (--itemCount) &0x1f;
	ptr[1]=++itemCount;
}

// item_property_... 매크로 사용
void ShopUtilAppendItemWithProperties(int shopunit, short thingId, int itemCount, 
    EquipEnchantment enchnt1, EquipEnchantment enchnt2, EquipEnchantment enchnt3, EquipEnchantment enchnt4)
{
    int *base = 0;

    ShopUtilItemBasePrivate(shopunit, &base);
    if (!base)
        return;
	if (*base&(~0x1f))
		return;
    
    int *ptr = base + ((7*(base[0]++))+1);
    
	ptr[0]=(int )GET_THINGDB_INFO(thingId);
	ptr[1]=itemCount;
	
#define EQUIPENUM_TO_PTR(ptr) *(int *)ptr
	if (enchnt1) ptr[3]=EQUIPENUM_TO_PTR(enchnt1);
	if (enchnt2) ptr[4]=EQUIPENUM_TO_PTR(enchnt2);
	if (enchnt3) ptr[5]=EQUIPENUM_TO_PTR(enchnt3);
	if (enchnt4) ptr[6]=EQUIPENUM_TO_PTR(enchnt4);
#undef EQUIPENUM_TO_PTR
}

