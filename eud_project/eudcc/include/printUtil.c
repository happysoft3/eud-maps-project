
#include "printUtil.h"
#include "noxobject.h"
#include "builtins.h"
#include "stringUtil.h"
#include "memoryUtil.h"

static uint32_t s_uniPrintStream[] =
{
    0x9EB06856, 0x5068004D, 0xFF005072, 0xF08B2414, 0x502414FF,
    0x2454FF56, 0x10C4830C, 0x9090C35E
};

static void widePrintImplementation(NoxObject *pObject, short *wMessage)
{
    uint32_t *pOff = s_uniPrintStream;

    BuiltinTwoArg((int)wMessage, (int)pObject, ((int)&pOff - 0x5c308c) / 4);
}

void WidePrintMessage(int user, short *wMessage)
{
    NoxObject *pObject = UnitToPtr(user);

    if (pObject)
        widePrintImplementation(pObject, wMessage);
}

void WidePrintMessageAsUtf8(int user, char *message)
{
    NoxObject *pObject = UnitToPtr(user);

    if (pObject)
    {
        short wmessage[1024];

        Utf8ToUnicodeString(message, wmessage);
        widePrintImplementation(pObject, wmessage);
    }
}

static uint32_t s_uniSayStream[] =
{
    0xC0685657, 0x6800528A, 0x00507250, 0x8B2414FF, 0x2414FFF8,
    0x14FFF08B, 0x56505724, 0x102454FF, 0x5E14C483, 0x9090C35F
};

static void wideChatImplementation(NoxObject *pObject, short *wMessage, uint16_t duration)
{
    uint32_t *pOff = s_uniSayStream;

    BuiltinThreeArg((int)wMessage, (int)pObject, (int)duration, ((int)&pOff - 0x5c308c) / 4);
}

void WideSayMessage(int unit, short *wMessage, uint16_t duration)
{
    NoxObject *pObject = UnitToPtr(unit);

    if (!pObject)
        return;

    wideChatImplementation(pObject, wMessage, duration);
}

void WideSayMessageAsUtf8(int unit, char *message, uint16_t duration)
{
    NoxObject *pObject = UnitToPtr(unit);

    if (!pObject)
        return;

    short wmessage[1024];

    Utf8ToUnicodeString(message, wmessage);
    wideChatImplementation(pObject, wmessage, duration);
}

void WidePrintMessageAll(short *wMessage)
{
    char *userPtr = (char *)0x62f9e0;
    int rep = -1;
    NoxObject *pObject;

    while (++rep < 32)
    {
        pObject = *(NoxObject **)userPtr;
        if (pObject)
            widePrintImplementation(pObject, wMessage);
        userPtr += 0x12dc;
    }
}

static uint8_t s_unitOnUseCodestream[] =
{
    0x55, 0x8B, 0xEC, 0x56, 0x8B, 0x75, 0x0C, 0x56, 0xFF, 0x75, 0x08, 0x8B, 0x86, 
    0xE0, 0x02, 0x00, 0x00, 0xFF, 0x30, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 
    0x83, 0xC4, 0x0C, 0x5E, 0x8B, 0xE5, 0x5D, 0xC3, 0x90, 0x90
};

typedef struct _picketReadData
{
    UnitCallback cb;
    int timeBuff;
    short wMessage[512];
} PicketReadData;

static int checkSignDelay(PicketReadData *data, uint32_t gap)
{
    uint32_t *cFps = (uint32_t *)0x84ea04;

    if ((*cFps - (uint32_t)data->timeBuff) > gap)
    {
        data->timeBuff = *cFps;
        return 1;
    }
    return 0;
}

static void onPicketRead()
{
    NoxObject **trig = (NoxObject **)0x979724;
    NoxObject **caller = (NoxObject **)0x979720;

    if (*trig && *caller)
    {
        PicketReadData *data = (PicketReadData *)((*trig)->useFnData);
        if (checkSignDelay(data, 60))
            widePrintImplementation(*caller, data->wMessage);
    }
}

void SetPicketText(int picketUnit, const short *wMessage)
{
    NoxObject *ptr = UnitToPtr(picketUnit);

    if (!ptr)
        return;

    ptr->useFnPtr = s_unitOnUseCodestream;
    if (ptr->useFnData)
        MemFree(ptr->useFnData);
    PicketReadData *data = (PicketReadData *)MemAlloc(sizeof(PicketReadData));
    ptr->useFnData = data;
    data->cb = &onPicketRead;
    data->timeBuff = 0;
    CopyWideString(wMessage, data->wMessage);
}
