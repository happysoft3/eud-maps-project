
#ifndef GLOBAL_DATA_H__
#define GLOBAL_DATA_H__

#include "intTypes.h"

typedef struct _globalData
{
    int defVarArr[4];
} GlobalData;

GlobalData *LoadGlobalData();

#endif
