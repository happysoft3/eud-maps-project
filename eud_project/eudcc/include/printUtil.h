
#ifndef PRINT_UTIL_H__
#define PRINT_UTIL_H__

#include "intTypes.h"

void WidePrintMessage(int user, short *wMessage);
void WidePrintMessageAsUtf8(int user, char *message);
void WideSayMessage(int unit, short *wMessage, uint16_t duration);
void WideSayMessageAsUtf8(int unit, char *message, uint16_t duration);
void WidePrintMessageAll(short *wMessage);
void SetPicketText(int picketUnit, const short *wMessage);

#endif
