
#include "opcodeTools.h"

void ResolveCallOpcode(char *setptr, int adder, int target)
{
    setptr += adder;
    int *p = (int *)(setptr + 1);

    *p = target - (int)setptr - 5;
}

#define GET_TARGET_ADDR(target) (*(int *)(target+1)+target+5)

static void opcodeLoadEffectiveAddr(char *loadAddr, int *codeAddr, int offset)
{
    int targetAddr = GET_TARGET_ADDR((int)codeAddr + offset);
    int *dest = (int *)(loadAddr + offset + 1);

    *dest = targetAddr - (int)(loadAddr + offset) - 5;
}

void OpcodeCopiesAdvanced(int *destpt, int *pCallnode, const int *startAddr, const int *endAddr)
{
    char *destbase = (char *)destpt;
    int *curpt = startAddr;

    while (curpt <= endAddr)
    {
        *destpt = *curpt;
        if (*pCallnode)
        {
            if (*pCallnode + 1 <= (int)curpt)
            {
                opcodeLoadEffectiveAddr(destbase, startAddr, *pCallnode - (int)startAddr);
                ++pCallnode;
            }
        }
        ++curpt;
        ++destpt;
    }
}

