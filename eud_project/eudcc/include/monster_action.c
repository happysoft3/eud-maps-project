
#include "monster_action.h"
#include "noxobject.h"
#include "builtins.h"

#define NULL 0

static uint32_t s_monActPushcode[]={
    0x50685650, 0xFF005072, 0x708D2414, 0xA2606804, 0x36FF0050, 0x54FF30FF,
    0xC4830824, 0x7230680C, 0xFF500050, 0x83042454, 0x585E0CC4, 0x909090C3
};

CreatureAction *PushMonsterAction(int mob, int actionType, int a0, int a1, int a2)
{
    uint32_t *fnOff = s_monActPushcode;
    int args[] = {(int)UnitToPtr(mob), actionType};

    CreatureAction *ret = (CreatureAction *)BuiltinSingleArgReturn((int)args, ((int)&fnOff - 0x5c308c) / 4);

    if (!ret)
        return NULL;

    ret->arg0 = a0;
    ret->arg1 = a1;
    ret->arg2 = a2;
    return ret;
}


