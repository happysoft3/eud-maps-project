
#include "builtins.h"
#include "stringUtil.h"
#include "noxscrProto.h"

#define BUILTIN_FALSE	0
#define BUILTIN_TRUE	1

static int s_initalizedBuiltins = BUILTIN_FALSE;

char **g_pAudList = 0;

static void invokeBuiltin(CallBuiltinParam *param)
{ }	//CODE: 0x45 + {special} + 0x48 ���� ����.

typedef void(*Invokable_Ptr)(CallBuiltinParam*);
static Invokable_Ptr s_callerFunction = &invokeBuiltin;

void AudioEvent(char *audio, int locationId)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 34, 2, (int)s_callerFunction, CharPointerToNoxString(audio), locationId };

	invokeBuiltin(&param);
}

int CreateObject(char *unitName, int locationId)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 50, 2, (int)s_callerFunction, CharPointerToNoxString(unitName), locationId };

	invokeBuiltin(&param);
	return param.retValue;
}

void BuiltinNoArg(int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 0, (int)s_callerFunction, };

	invokeBuiltin(&param);
}

int BuiltinNoArgReturn(int targetID)
{
	CallBuiltinParam param = { BUILTIN_TRUE, targetID, 0, (int)s_callerFunction, };

	invokeBuiltin(&param);
	return param.retValue;
}

void BuiltinSingleArg(int arg, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 1, (int)s_callerFunction, arg };

	invokeBuiltin(&param);
}

int BuiltinSingleArgReturn(int arg, int targetID)
{
	CallBuiltinParam param = { BUILTIN_TRUE, targetID, 1, (int)s_callerFunction, arg };

	invokeBuiltin(&param);
	return param.retValue;
}

float BuiltinSingleArgReturnFloat(int arg, int targetID)
{
	int ret = BuiltinSingleArgReturn(arg, targetID);

	return *(float *)&ret;
}

void BuiltinTwoArg(int arg1, int arg2, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 2, (int)s_callerFunction, arg1, arg2 };

	invokeBuiltin(&param);
}

int BuiltinTwoArgReturn(int arg1, int arg2, int targetID)
{
	CallBuiltinParam param = { BUILTIN_TRUE, targetID, 2, (int)s_callerFunction, arg1, arg2 };

	invokeBuiltin(&param);
	return param.retValue;
}

void BuiltinThreeArg(int arg1, int arg2, int arg3, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 3, (int)s_callerFunction, arg1, arg2, arg3 };

	invokeBuiltin(&param);
}

void BuiltinsSingleCharArg(char *s, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 1, (int)s_callerFunction, CharPointerToNoxString(s), };

	invokeBuiltin(&param);
}

// typedef void(*timer_impl_fn)(void);
// 
// typedef struct _NoxScriptTimer
// {
// 	int timeStamp;
// 	timer_impl_fn invokeTarget;
// 	// int argument;
// 	struct _NoxScriptTimer *thisTimer;
// 	int timerId;
// 	// int field16;
// 	timer_callback timerCb;
// 	int argument;
// 	struct _NoxScriptTimer *pNext;
// } NoxScriptTimer;

static int *s_timerArg;
static int *s_timerTargetFn;

static void ScriptTimerInvokable()
{
	//self: arg
	//other: targetId
	timer_callback cb = (timer_callback)(*s_timerTargetFn);
	int arg = *s_timerArg;

	*s_timerTargetFn=0;
	*s_timerArg=0;
	cb(arg);
}

static void scriptTimerInvokeNoArg()
{
	timer_callback_noarg cb = (timer_callback_noarg)(*s_timerTargetFn);

	*s_timerTargetFn=0;
	*s_timerArg=0;
	cb();
}

static char s_timerExCode[] = 
{
0x56, 0x57, 0x55, 0x8B, 0xEC, 0x50, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x89, 0x45, 0xFC, 0xB8, 0x40, 0xAD, 0x51, 0x00, 
0xFF, 0xD0, 0x8B, 0xF0, 0x31, 0xFF, 0x39, 0xFE, 0x75, 0x14, 0x57, 0xB8, 0x30, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x89, 0x45, 0xFC, 
0x83, 0xC4, 0x04, 0x8B, 0xE5, 0x5D, 0x5F, 0x5E, 0xC3, 0x8B, 0x45, 0xFC, 0x8B, 0x00, 0x8B, 0x0D, 0x04, 0xEA, 0x84, 0x00, 0x8B, 
0x55, 0xFC, 0x8B, 0x52, 0x04, 0x01, 0xC1, 0x89, 0x0E, 0x89, 0x56, 0x04, 0x89, 0x76, 0x08, 0xB8, 0x20, 0xAD, 0x51, 0x00, 0xFF, 
0xD0, 0x50, 0x89, 0x46, 0x0C, 0x8B, 0x45, 0xFC, 0x8B, 0x40, 0x0C, 0x89, 0x46, 0x10, 0x8B, 0x45, 0xFC, 0x8B, 0x40, 0x08, 0x89, 
0x46, 0x14, 0x89, 0x7E, 0x18, 0xB8, 0x30, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xA1, 0x58, 0x39, 0x83, 0x00, 0x83, 0xC4, 0x04, 0x39, 
0xF8, 0x75, 0x0F, 0x89, 0x35, 0x58, 0x39, 0x83, 0x00, 0x89, 0x7E, 0x1C, 0x8B, 0xE5, 0x5D, 0x5F, 0x5E, 0xC3, 0x8B, 0x48, 0x18, 
0x39, 0xF9, 0x74, 0x09, 0x8B, 0xC1, 0x8B, 0x48, 0x18, 0x39, 0xF9, 0x75, 0xF7, 0x89, 0x70, 0x18, 0x89, 0x46, 0x1C, 0x8B, 0xE5, 
0x5D, 0x5F, 0x5E, 0xC3
};

int BuiltinRunTimer(int delay, int arg, timer_callback cb)
{
	char *fnOff = s_timerExCode;
	int params[] = {delay, (int)&ScriptTimerInvokable, arg, (int)cb};

	return BuiltinSingleArgReturn((int)params, ((int)&fnOff - 0x5c308c) / 4);
}

int BuiltinRunTimerNoArg(int delay, timer_callback_noarg cb)
{
	char *fnOff = s_timerExCode;
	int params[] = {delay, (int)&scriptTimerInvokeNoArg, 0, (int)cb};

	return BuiltinSingleArgReturn((int)params, ((int)&fnOff - 0x5c308c) >> 2);
}

void BuiltinSayUnit(int unit, const char *msg, int frames, int seconds)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 37, 2, (int)s_callerFunction, unit, CharPointerToNoxString(msg), };

	do
	{
		if (frames)
			param.targetFunctionId = 115;
		else if (seconds)
			param.targetFunctionId = 114;
		else
			break;

		param.args[param.argCount++] = frames ? frames : seconds;
	} while (0);

	invokeBuiltin(&param);
}

float NoxRandomFloat(float min, float max)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 44, 2, (int)s_callerFunction, *((int *)&min), *((int *)&max), };

	invokeBuiltin(&param);
	return *(float *)&param.retValue;
}

char *noxImplToString(CallBuiltinParam *pParam)
{
	char **strTable = (char **)0x97bb40;

	invokeBuiltin(pParam);
	return strTable[pParam->retValue];
}

char *IntToString(int iValue)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 48, 1, (int)s_callerFunction, iValue, };
	
	return noxImplToString(&param);
}

char *FloatToString(float fValue)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 49, 1, (int)s_callerFunction, *(int *)&fValue, };
	
	return noxImplToString(&param);
}

void NoxDamage(int target, int source, int amount, DamageType type)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 51, 4, (int)s_callerFunction, target, source, amount, type };

	invokeBuiltin(&param);
}

static int s_enchantPtrList[] =
{
	0x59724c, 0x597260, 0x597274, 0x597284, 0x597298, 0x5972a8,
	0x5972b8, 0x5972cc, 0x5972e0, 0x5972ec, 0x5972fc, 0x59730c,
	0x59731c, 0x59732c, 0x597340, 0x597354, 0x597364, 0x597374, 
	0x597390, 0x5973ac, 0x5973c8, 0x5973ec, 0x597400, 0x597410, 
	0x597428, 0x59743c, 0x59744c, 0x59745c, 0x597478, 0x59748c,
	0x5974a0, 0x5974b0
};

void NoxEnchant(int unit, Enchantment enchant, float duration)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 57, 3, (int)s_callerFunction, unit, CharPointerToNoxString((char *)s_enchantPtrList[enchant]), *(int *)&duration, };

	invokeBuiltin(&param);
}

int NoxHasEnchant(int unit, Enchantment enchant)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 81, 2, (int)s_callerFunction, unit, CharPointerToNoxString((char *)s_enchantPtrList[enchant]), };

	invokeBuiltin(&param);
	return param.retValue;
}
void NoxEnchantOff(int unit, Enchantment enchant)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 82, 2, (int)s_callerFunction, unit, CharPointerToNoxString((char *)s_enchantPtrList[enchant]), };

	invokeBuiltin(&param);
}

void BuiltinIntFloatFloat(int arg, float farg1, float farg2, int targetId)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetId, 3, (int)s_callerFunction, arg, *(int *)&farg1, *(int *)&farg2 };

	invokeBuiltin(&param);
}

void BuiltinIntFloat(int arg, float farg, int targetId)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetId, 2, (int)s_callerFunction, arg, *(int *)&farg };

	invokeBuiltin(&param);
}

void NoxPushObject(int unit, float magnitude, float x, float y)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 72, 4, (int)s_callerFunction, unit, *(int *)&magnitude, *(int *)&x, *(int *)&y };

	invokeBuiltin(&param);
}

typedef struct _CallBuiltinFloatParam
{
	float retValue;
	int targetFunctionId;
	int argCount;
	int caller;
	float fargs[BUILTIN_MAX_ARG];
} CallBuiltinFloatParam;

float NoxDistance(float x1, float y1, float x2, float y2)
{
	CallBuiltinFloatParam fParam = { 1.0f, 86, 4, (int)s_callerFunction, x1, y1, x2, y2, };

	invokeBuiltin((CallBuiltinParam *)&fParam);
	return fParam.retValue;
}

static int s_fxPtrTable[]=
{
	0x5c2713, 0x5c2727, 0x5c2737, 0x5c2747, 0x5c275f,0x5c276f,0x5c2783,
	0x5c279b, 0x5c27af, 0x5c27c7, 0x5c27db, 0x5c27f3, 0x5c2813, 0x5c282b,
	0x5c283b, 0x5c284f, 0x5c2863, 0x5c2877, 0x5c288b, 0x5c28a7, 0x5c28bb,
	0x5c28cb, 0x5c28df, 0x5c28ef, 0x5c2907, 0x5c291b, 0x5c292f, 0x5c293f,
	0x5c294f, 0x5c2963, 0x5c297b, 0x5c298f, 0x5c29a7, 0x5c29bf, 0x5c29d7,
	0x5c29ef, 0x5c2a0b, 0x5c2a1f, 0x5c2a33, 0x5c2a47, 
};

void NoxPlayFX(NetFxTypes fx_id, float x1, float y1, float x2, float y2)
{
	int strNumber = CharPointerToNoxString((char *)s_fxPtrTable[fx_id]);
	CallBuiltinFloatParam fParam = { BUILTIN_FALSE, 100, 5, (int)s_callerFunction, *(float *)&strNumber, x1, y1, x2, y2 };

	invokeBuiltin((CallBuiltinParam *)&fParam);
}

float NoxGetQuestStatusFloat(const char *name)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 122, 1, (int)s_callerFunction, CharPointerToNoxString(name), };

	invokeBuiltin(&param);
	return *(float *)&param.retValue;
}

// char *s_dialogTypeStr[] =
// {
// 	"NORMAL",
// 	"NEXT",
// 	"YESNO",
// 	"YESNO_NEXT"
// };

static int s_dialogTypes[]={
	0x5cd140,0x5cd148,0x5cd150,0x5cd158
};

void NoxSetupDialog(int unit, DialogType type, dialog_callback startFn, dialog_callback endFn)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 126, 4, (int)s_callerFunction, unit,
		CharPointerToNoxString((char *)s_dialogTypes[type]), (int)startFn, (int)endFn, };

	invokeBuiltin(&param);
}

static int s_spellPtrList[] =
{
	0x5974c0, 0x5974D0, 0x5974E0, 0x5974F4, 0x597500, 0x59750C,
	0x597518, 0x597528, 0x597544, 0x597558, 0x597564, 0x59757C, 0x597598, 0x5975A8, 0x5975BC, 0x5975D0,
	0x5975DC, 0x5975EC, 0x597600, 0x597610, 0x597628, 0x597640, 0x597654, 0x597668, 0x59767C, 0x59768C,
	0x59769C, 0x5976A8, 0x5976B8, 0x5976C8, 0x5976D4, 0x5976E8, 0x597700, 0x597710, 0x597720, 0x59772C,
	0x597740, 0x59774C, 0x597760, 0x597770, 0x597784, 0x59779C, 0x5977B0, 0x5977BC, 0x5977D4, 0x5977E0,
	0x5977EC, 0x5977FC, 0x59780C, 0x59781C, 0x59782C, 0x597840, 0x597850, 0x597860, 0x597874, 0x597884,
	0x597894, 0x5978A4, 0x5978B4, 0x5978C8, 0x5978D8, 0x5978E8, 0x59790C, 0x597928, 0x597944, 0x597964,
	0x597970, 0x59797C, 0x597990, 0x5979A8, 0x5979BC, 0x5979C8, 0x5979D4, 0x5979E0, 0x5979F0, 0x5979FC,
	0x597a10, 0x597a28, 0x597a3C, 0x597a54, 0x597a68, 0x597a88, 0x597aA4, 0x597aC8, 0x597aE4, 0x597b00,
	0x597b14, 0x597b30, 0x597b44, 0x597b64, 0x597b84, 0x597b98, 0x597bAC, 0x597bC4, 0x597bE0, 0x597bF8,
	0x597c0C, 0x597c24, 0x597c40, 0x597c54, 0x597c70, 0x597c90, 0x597cAC, 0x597cC0, 0x597cD4, 0x597cE8,
	0x597d00, 0x597d14, 0x597d2C, 0x597d44, 0x597d58, 0x597d74, 0x597d88, 0x597d9C, 0x597dB0, 0x597dCC,
	0x597dD8, 0x597dE4, 0x597e04, 0x597e24, 0x597e44, 0x597e64, 0x597e78, 0x597e94, 0x597eB0, 0x597eCC,
	0x597eE8, 0x597f04, 0x597f18, 0x597f2C, 0x597f40, 0x597f50, 0x597f60, 0x597f6C, 0x597f78, 0x597f90,
	0x597fA4,
};

int SpellIdToNoxString(SpellTypes spell_id)
{
	return CharPointerToNoxString((char *)s_spellPtrList[spell_id]);
}

void NoxCastSpellObjectLocation(SpellTypes spell_id, int source, float x, float y)
{
	CallBuiltinParam param =
		{
			BUILTIN_FALSE, 132, 4, (int)s_callerFunction, CharPointerToNoxString((char *)s_spellPtrList[spell_id]), source, *(int *)&x, *(int *)&y,
			};

	invokeBuiltin(&param);
}

void NoxCastSpellLocationObject(SpellTypes spell_id, float x, float y, int target)
{
	CallBuiltinParam param = {
		BUILTIN_FALSE, 133, 4, (int)s_callerFunction, CharPointerToNoxString((char *)s_spellPtrList[spell_id]), *(int *)&x, *(int *)&y, target,
		};

	invokeBuiltin(&param);
}
void NoxCastSpellLocationLocation(SpellTypes spell_id, float x1, float y1, float x2, float y2)
{
	CallBuiltinParam param = {
		BUILTIN_FALSE, 134, 5, (int)s_callerFunction, CharPointerToNoxString((char *)s_spellPtrList[spell_id]), *(int *)&x1, *(int *)&y1, *(int *)&x2, *(int *)&y2 };

	invokeBuiltin(&param);
}

void NoxCreatureGuard(int id, float x1, float y1, float x2, float y2, float distance)
{
	CallBuiltinFloatParam fParam = { BUILTIN_FALSE, 139, 6, (int)s_callerFunction, *(float *)&id, x1, y1, x2, y2, };

	invokeBuiltin((CallBuiltinParam *)&fParam);
}

void NoxSetCallback(int mon, UnitCallbackType type, UnitCallback callback)
{
	BuiltinThreeArg(mon, type, (int)callback, 190);
}

void NoxSetTrapSpells(int unit, SpellTypes firstId, SpellTypes secondId, SpellTypes thirdId)
{
	#define SPELLID_TO_STRING(spid)	CharPointerToNoxString((char *)s_spellPtrList[spid])
	CallBuiltinParam param =
		{
			BUILTIN_FALSE, 192, 4, (int)s_callerFunction, unit,
		SPELLID_TO_STRING(firstId), SPELLID_TO_STRING(secondId), SPELLID_TO_STRING(thirdId) };
	#undef SPELLID_TO_STRING

	invokeBuiltin(&param);
}

enum MyBuiltins
{
#define DEF_IMPL_BUILTINS_FUNCTION(id, offset) id = offset,
#include "implBuiltinsFunctions.inc"
#undef DEF_IMPL_BUILTINS_FUNCTION
};

void BuiltinsInitialize()
{
	if (s_initalizedBuiltins)
		return;

	s_initalizedBuiltins = BUILTIN_TRUE;
	int targetId = (int)s_callerFunction;
	NoxScriptFunction *scrTable = (NoxScriptFunction *)(*(int *)0x75ae28);
	int *pCode = scrTable[targetId].codeTable;

	// int *p = (int *)0x751000;

	// p[1] = targetId;
	// p[3] = (int)pCode;
	pCode[0] = 0x45;
	pCode[1] = (BUILTINS_CallBuitin-0x5c308c)/4;
	pCode[2] = 0x48;

	s_timerArg = (int *)0x979720;
	s_timerTargetFn = (int *)0x979724;
	g_pAudList = (char **)0x588440;
}
