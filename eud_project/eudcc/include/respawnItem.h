
#ifndef RESPAWN_ITEM_H__
#define RESPAWN_ITEM_H__

#include "noxobject.h"

typedef struct _respawnData
{
    int thingId;
    NoxObject *pObject;
    float xProfile;
    float yProfile;
    int field10;
    int nextFps;
    int needRespawn;
    int dummy[6];
    struct _respawnData *next;
    struct _respawnData *prev;
} RespawnData;

#define FIRST_RESPAWNDATA (*(RespawnData **)0x7532ac)

#endif

