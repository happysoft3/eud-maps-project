
#include "mathlab.h"
#include "builtins.h"

static int s_sineTable[] ={
0x00000000, 0x3C900000, 0x3D100000, 0x3D580000, 0x3D900000, 0x3DB40000, 0x3DD80000, 0x3DFC0000, 0x3E0FC000, 0x3E218000,
0x3E334000, 0x3E450000, 0x3E56C000, 0x3E684000, 0x3E79C000, 0x3E85A000, 0x3E8E6000, 0x3E970000, 0x3E9FA000, 0x3EA82000,
0x3EB0A000, 0x3EB92000, 0x3EC18000, 0x3EC9E000, 0x3ED20000, 0x3EDA4000, 0x3EE26000, 0x3EEA8000, 0x3EF26000, 0x3EFA4000,
0x3F010000, 0x3F04D000, 0x3F08A000, 0x3F0C7000, 0x3F102000, 0x3F13E000, 0x3F178000, 0x3F1B1000, 0x3F1EA000, 0x3F222000,
0x3F259000, 0x3F290000, 0x3F2C5000, 0x3F2FA000, 0x3F32E000, 0x3F361000, 0x3F393000, 0x3F3C4000, 0x3F3F3000, 0x3F422000,
0x3F450000, 0x3F47D000, 0x3F4A9000, 0x3F4D4000, 0x3F4FE000, 0x3F527000, 0x3F54F000, 0x3F576000, 0x3F59C000, 0x3F5C1000,
0x3F5E5000, 0x3F608000, 0x3F62A000, 0x3F64A000, 0x3F66A000, 0x3F689000, 0x3F6A5000, 0x3F6C1000, 0x3F6DC000, 0x3F6F6000,
0x3F70F000, 0x3F726000, 0x3F73D000, 0x3F751000, 0x3F765000, 0x3F778000, 0x3F78A000, 0x3F79A000, 0x3F7A9000, 0x3F7B7000,
0x3F7C4000, 0x3F7D0000, 0x3F7DB000, 0x3F7E3000, 0x3F7EB000, 0x3F7F2000, 0x3F7F8000, 0x3F7FC000, 0x3F7FF000, 0x3F800800, 
0x3F801000
};

static float loadSineTable(int index)
{
    return *(float *)&s_sineTable[index];
}

float MathSine(int angle, float scale)
{
    int k = angle / 90;
    int i = angle - (k * 90);

    if (k % 2) i = 90 - i;
    return loadSineTable(i) * (((angle / 180) % 2) ? -scale : scale);
}

float UnitAngleCos(int unit, float scale)
{
    return MathSine(((NoxGetDirection(unit) * 45) / 32) + 90, scale);
}

float UnitAngleSin(int unit, float scale)
{
    return MathSine((NoxGetDirection(unit) * 45) / 32, scale);
}

float UnitRatioX(int unit, int target, float scale)
{
    return ((NoxGetObjectX(unit) - NoxGetObjectX(target)) * scale) / NoxDistance(NoxGetObjectX(unit), NoxGetObjectY(unit), NoxGetObjectX(target), NoxGetObjectY(target));
}

float UnitRatioY(int unit, int target, float scale)
{
    return ((NoxGetObjectY(unit) - NoxGetObjectY(target)) * scale) / NoxDistance(NoxGetObjectX(unit), NoxGetObjectY(unit), NoxGetObjectX(target), NoxGetObjectY(target));
}

void ComputeAreaRhombus(PointXY *destSpot, float xBottom, float xRight, float yTop, float yRight)
{
    float computed1 = NoxRandomFloat(yTop, yRight);
    float computed2 = NoxRandomFloat(0, xRight - xBottom);

    destSpot->x = xRight - yRight + computed1 - computed2;
    destSpot->y = computed1 + computed2;
}
