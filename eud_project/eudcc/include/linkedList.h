
#ifndef LINKED_LIST_H__
#define LINKED_LIST_H__

typedef struct _linkedNode
{
    int value;
    struct _linkedNode *prev;
    struct _linkedNode *next;
} LinkedNode;

typedef struct _linkedList
{
    int count;
    LinkedNode *head;
    LinkedNode *tail;
} LinkedList;

LinkedList *CreateLinkedList();
void ClearLinkedList(LinkedList *pList);
void DeleteLinkedList(LinkedList *pList);
void PushbackLinkedList(LinkedList *pList, int value);
void PushfrontLinkedList(LinkedList *pList, int value);
void LinkedListPop(LinkedList *pList, LinkedNode *node);
void PopFrontLinkedList(LinkedList *pList);
void PopBackLinkedList(LinkedList *pList);
LinkedNode *LinkedListFront(LinkedList *pList);
LinkedNode *LinkedListBack(LinkedList *pList);

#endif

