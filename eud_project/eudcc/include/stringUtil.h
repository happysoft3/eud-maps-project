
#ifndef STRING_UTIL_H__
#define STRING_UTIL_H__

int CharPointerToNoxString(char *ptr);
void CopyString(const char* src, char* dest);
void CopyWideString(const short *src, short *dest);
void Utf8ToUnicodeString(const char *src, short *dest);
void UnicodeToUtf8String(const short* src, char* dest);
int StringGetLength(const char* str);
int WideStringGetLength(const short* wstr);
int StringCompare(const char *s1, const char *s2);
int WideStringCompare(const short *ws1, const short *ws2);
void ConcatenateString(char *dest, const char *src);
void ConcatenateWideString(short *dest, const short *src);

#endif


