DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_GetMemory,7670400)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_GetMemoryWord,7670404)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_GetMemoryByte,7670408)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SetMemory,7670412)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SetMemoryWord,7670416)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SetMemoryByte,7670420)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SetMemorySWord,7670424)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SetMemorySByte,7670428)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SetMemoryZWord,7670432)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SetMemoryZByte,7670436)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_ComputedGoto,7670440)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_JumpIf,7670444)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_JumpIfNot,7670448)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SignedMod,7670452)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_UnsignedMod,7670456)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_SignedDiv,7670460)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_UnsignedDiv,7670464)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_Addition,7670468)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_Subtract,7670472)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_Multiply,7670476)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_ShiftLeft,7670480)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_ShiftRight,7670484)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_XOR,7670488)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_AND,7670492)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_OR,7670496)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_IntToFloat,7670500)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_FloatToInt,7670504)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_LoadCompareResult,7670508)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareEqual,7670512)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareNotEqual,7670516)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareFloatLess,7670520)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareFloatLessEqual,7670524)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareLess,7670528)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareLessEqual,7670532)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareB,7670536)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CompareEqualB,7670540)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_TEST,7670544)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_TEST_NOT,7670548)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_UserCall,7670552)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_LoadGlobalAddress,7670556)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_LoadGlobalValue,7670560)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_ZeroFiller,7670564)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_StoreLocal,7670568)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_EUDPush,7670572)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_EUDPop,7670576)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_Memcopy,7670580)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_FloatAdd,7670584)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_FloatSub,7670588)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_FloatMul,7670592)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_FloatDiv,7670596)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_EBP,7670600)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_EAX,7670604)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_ECX,7670608)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_FunctionReturn,7670612)
DEF_IMPL_BUILTINS_FUNCTION(BUILTINS_CallBuitin,7670616)
