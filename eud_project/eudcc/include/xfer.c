
#include "xfer.h"
#include "stringUtil.h"
#include "noxobject.h"
#include "builtins.h"

void GoldXferSetAmount(int gold, int amount)
{
    NoxObject *ptr = UnitToPtr(gold);

    if (ptr)
    {
        if ((int)ptr->pickupFnPtr == 5192288)
        {
            int *amountptr = (int *)ptr->unkFn2B0DataPtr;

            if (amountptr)
                *amountptr = amount;
        }
    }
}
int GoldXferGetAmount(int gold)
{
    NoxObject *ptr = UnitToPtr(gold);

    if (ptr)
    {
        if ((int)ptr->pickupFnPtr == 5192288)
        {
            int *amountptr = (int *)ptr->unkFn2B0DataPtr;

            if (amountptr)
                return *amountptr;
        }
    }
}

typedef struct _glyphXfer
{
    SpellTypes firstSkill;
    SpellTypes secondSkill;
    SpellTypes thirdSkill;
    int unknown[2];
    int spellCount;
    int field18;
    float xTarget;
    float yTarget;
    int field24;
} GlyphXfer;

int CreateGlyph(float xpos, float ypos, SpellTypes first, SpellTypes second, SpellTypes third)
{
    int glyph = CreateObjectAt("Glyph", xpos, ypos);
    NoxObject *ptr = GETLASTUNIT;
    GlyphXfer *xfer = (GlyphXfer *)ptr->unkFn2B0DataPtr;
    int spellCount = 0;

    if (first != SPELL_INVALID)
    {
        xfer->firstSkill = first;
        ++spellCount;
    }
    if (second != SPELL_INVALID)
    {
        xfer->secondSkill = second;
        ++spellCount;
    }
    if (third != SPELL_INVALID)
    {
        xfer->thirdSkill = third;
        ++spellCount;
    }
    xfer->spellCount = spellCount;
    return glyph;
}

void SetGlyphTarget(int glyph, float x, float y)
{
    NoxObject *ptr = UnitToPtr(glyph);

    if (!ptr)
        return;

    GlyphXfer *xfer = (GlyphXfer *)ptr->unkFn2B0DataPtr;

    xfer->xTarget=x;
    xfer->yTarget=y;
}

static uint8_t s_infiniteManastoneCode[]=
{
    0x56, 0x8B, 0x74, 0x24, 0x08, 0x8B, 0x86, 0xEC, 0x02, 0x00, 0x00, 0x83, 
    0x38, 0x32, 0x74, 0x06, 0xC7, 0x00, 0x32, 0x00, 0x00, 0x00, 0x5E, 0x68, 0x80, 0xC5, 0x53, 0x00, 0xC3, 0x90
};

void SetInfiniteManaObelisk(int obelisk)
{
    NoxObject *ptr = UnitToPtr(obelisk);

    if (ptr)
        ptr->onUpdateFn = s_infiniteManastoneCode;
}

int CreateFieldGuide(const char *mobName, float xpos, float ypos)
{
    int ret = CreateObjectAt("FieldGuide", xpos, ypos);
    NoxObject *ptr = GETLASTUNIT;
    char *dest = (char *)ptr->useFnData;

    CopyString(mobName, dest);
    return ret;
}

int ImplementationSpawnBook(int id, int isab, float xpos, float ypos)
{
    int ret = CreateObjectAt(isab ? "AbilityBook" : "SpellBook", xpos, ypos);
    NoxObject *ptr = GETLASTUNIT;
    int *detail = (int *)ptr->useFnData;

    detail[0] = id;
    return ret;
}

int FallMeteor(float xpos, float ypos, int damageAmount, float speed)
{
    int ret = CreateObjectAt("Meteor", xpos, ypos);
    NoxObject *ptr = GETLASTUNIT;
    int *ctl = (int *)ptr->unitController;

    *ctl = damageAmount;
    ptr->field_14 |= 0x20;
    NoxRaiseObject(ret, 255.0f);
    ptr->field_6C = speed;
    return ret;
    
}

static uint8_t s_closeExitCode[]=
{
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xB8, 
    0x70, 0x46, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3};

void CloseQuestExit(int exit)
{
    uint8_t *pOff = s_closeExitCode;
    int args[] = {(int)UnitToPtr(exit), 0};

    BuiltinSingleArg((int)args, ((int)&pOff - 0x5c308c) / 4);
}

typedef struct _autotrapData
{
    int mainTimer;
    int activateTimer;
    int isTriggered;
    int missileId;
    char missileName[24];
} AutoTrapData;

void ChangeTrapMissile(int trp, int missileId)
{
    NoxObject *ptr = UnitToPtr(trp);
    AutoTrapData *trpData = (AutoTrapData *)ptr->unitController;

    trpData->missileId = missileId;
}
