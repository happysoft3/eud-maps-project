
#include "playerInfo.h"

#define NULLPTR 0

int GetPlayerIndex(int plrUnit)
{
    NoxObject *ptr = UnitToPtr(plrUnit);

    if (!ptr)
        return -1;

    if (ptr->Class & UNIT_CLASS_PLAYER)
    {
        int *ec = (int *)ptr->unitController;
        int *pInfo = (int *)ec[69];

        return pInfo[516];
    }
    return -1;
}

short *PlayerIngameNickname(int plrUnit)
{
    NoxObject *ptr = UnitToPtr(plrUnit);

    if (!ptr)
        return NULLPTR;

    if (!(ptr->Class & UNIT_CLASS_PLAYER))
        return NULLPTR;

    int pIndex = GetPlayerIndex(plrUnit);
    int *ec = (int *)ptr->unitController;
    char *pDetail = (char *)ec[69];

    return (short *)( pDetail + 0x889 );
}

void ChangePlayerMana(int plrUnit, int cur, int max)
{
    NoxObject *ptr = UnitToPtr(plrUnit);
    PlayerUnitDetails *details = (PlayerUnitDetails *)ptr->unitController;

    if (cur > 0)
    {
        details->manaAmount = (uint16_t)cur;
        details->manaAmountCopy = (uint16_t)cur;
    }
    if (max > 0)
        details->manaMax = (uint16_t)max;
}
