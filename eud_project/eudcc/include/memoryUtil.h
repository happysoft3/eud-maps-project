
#ifndef MEMORY_UTIL_H__
#define MEMORY_UTIL_H__

#include "intTypes.h"

void *MemAlloc(uint16_t size);
void MemFree(void *mem);
void NoxByteMemset(uint8_t *targetPtr, uint32_t length, uint8_t value);
void NoxWordMemset(uint16_t *targetPtr, uint32_t length, uint16_t value);
void NoxDwordMemset(uint32_t *targetPtr, uint32_t length, uint32_t value);
void NoxByteMemcopy(const uint8_t *srcPtr, uint8_t *destPtr, uint32_t length);
void NoxWordMemcopy(const uint16_t *srcPtr, uint16_t *destPtr, uint32_t length);
void NoxDwordMemcopy(const uint32_t *srcPtr, uint32_t *destPtr, uint32_t length);
void InitializeSmartMemoryDestructor();
void **AllocSmartMemory(uint32_t sz);
void FreeSmartMemory(void *pMem);

#endif
