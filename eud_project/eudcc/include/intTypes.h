
#ifndef INT_TYPES_H__
#define INT_TYPES_H__

typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

#endif
