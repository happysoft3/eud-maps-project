
#ifndef XFER_H__
#define XFER_H__

#include "spellTypes.h"

void GoldXferSetAmount(int gold, int amount);
int GoldXferGetAmount(int gold);
int CreateGlyph(float xpos, float ypos, SpellTypes first, SpellTypes second, SpellTypes third);
void SetGlyphTarget(int glyph, float x, float y);
void SetInfiniteManaObelisk(int obelisk);
int CreateFieldGuide(const char *mobName, float xpos, float ypos);
int ImplementationSpawnBook(int id, int isab, float xpos, float ypos);

#define CREATE_SPELLBOOK(spell_id, unitX, unitY)    ImplementationSpawnBook(spell_id, 0, unitX, unitY)
#define CREATE_ABILITYBOOK(ability_id, unitX, unitY) ImplementationSpawnBook(ability_id + 1, 1, unitX, unitY)
int FallMeteor(float xpos, float ypos, int damageAmount, float speed);
void CloseQuestExit(int exit);
void ChangeTrapMissile(int trp, int missileId);

#endif

