
#ifndef FXEFFECT_H__
#define FXEFFECT_H__

void PlaySoundAround(int sourceUnit, short soundId);
void PlayFxGreenExplosion(float xpos, float ypos);
void MoveLinearOrb(int orb, float xvect, float yvect, float speed, int duration);
void PlayFxGreenLightning(int x1, int y1, int x2, int y2, int duration);

#endif
