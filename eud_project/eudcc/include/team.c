
#include "team.h"
#include "builtins.h"

#define GET_TEAM_COUNT *(int *)0x654D5C

static int s_autoTeamAssignment[] = {
    0x4191D068, 0x50515600, 0x000020B9, 0xF9E0B800, 0xC9850062,
    0x8B492774, 0x12DC0530, 0xF6850000, 0x5150F074, 0x8D24468B,
    0x016A304E, 0x51016A50, 0x54FF016A, 0xC4832824, 0xEB585914,
    0x5E5958D5, 0xC304C483
};

static int s_makeCoopTeam[] = {0x417E1068, 0x2414FF00, 0xC304C483};

void EnableCoopTeamMode()
{
    int teamCount = GET_TEAM_COUNT;

    if (teamCount)
        return;

    int *fnOff = s_makeCoopTeam;

    BuiltinNoArg(((int)&fnOff - 0x5c308c) / 4);

    int *whatThe=(int *)0x5d53a4;
    *whatThe=268640519;

    fnOff = s_autoTeamAssignment;

    BuiltinNoArg(((int)&fnOff - 0x5c308c) / 4);
}

static int s_clearTeam[] = {0x4DB8BE56,0x20680065,0x6A00418F,0x54FF5600,0xC4830824,0x90C35E0C};

void DisableCoopTeamMode()
{
    if (1==GET_TEAM_COUNT)
    {
        int *fnOff = s_clearTeam;

        BuiltinNoArg(((int)&fnOff - 0x5c308c) / 4);
    }    
}

