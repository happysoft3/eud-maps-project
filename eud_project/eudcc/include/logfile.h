
#ifndef LOGFILE_H__
#define LOGFILE_H__

void CreateLogFile(const char *logFileName);
void WriteLog(const char *logMessage);

#ifdef DISABLE_LOGFILE
#define WRITE_LOG(Msg)
#else
#define WRITE_LOG(Msg) WriteLog(Msg)
#endif

#endif

