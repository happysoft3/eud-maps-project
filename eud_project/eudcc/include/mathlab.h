
#ifndef MATH_LAB_H__
#define MATH_LAB_H__

#include "def_spot.h"

#define MATH_ABS(value) (value < 0) ? -value : value

float MathSine(int angle, float scale);
float UnitAngleCos(int unit, float scale);
float UnitAngleSin(int unit, float scale);
float UnitRatioX(int unit, int target, float scale);
float UnitRatioY(int unit, int target, float scale);
#define DIRECTION_TO_DEGREE(direction) ((direction * 45) >> 5)
#define DEGREE_TO_DIRECTION(degree) ((degree * 32) / 45)
void ComputeAreaRhombus(PointXY *destSpot, float xBottom, float xRight, float yTop, float yRight);

#endif

