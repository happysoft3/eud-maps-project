
#include "fxeffect.h"
#include "noxobject.h"
#include "builtins.h"

static int s_playsound_codeStream[]={
			0x50196068, 0x72506800, 0x50560050, 0x082454FF,
			0x54FFF08B, 0x006A0824, 0x5650006A, 0x1C2454FF,
			0x5810C483, 0x08C4835E, 0x909090C3};


void PlaySoundAround(int sourceUnit, short soundId)
{
    NoxObject *ptr = UnitToPtr(sourceUnit);

    if (!ptr)
        return;

    int *fnOff = s_playsound_codeStream;

    BuiltinTwoArg((int)ptr, (int)soundId, ((int)&fnOff - 0x5c308c) / 4);
}
///56 68 00 32 52 00 68 50 72 50 00 68 60 35 40 00 6A 08 FF 54 24 
//04 83 C4 04 8B F0 FF 54 24 04 89 06 FF 54 24 04 89 46 04 68 C8 00 00 00 56 FF 54 24 10 83 C4 14 68 5D 42 40 00 56 FF 54 24 04 83 C4 08 5E C3

static uint8_t s_greenExplosionCode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x68, 0xC8, 0x00, 0x00, 0x00, 
    0x50, 0xB8, 0x00, 0x32, 0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3
};

void PlayFxGreenExplosion(float xpos, float ypos)
{
    float param[] = {xpos, ypos};

    uint8_t *fnOff = s_greenExplosionCode;

    BuiltinSingleArg((int)param, ((int)&fnOff - 0x5c308c)/4);
}

static int s_orbMoveCodeStream[]={
    0x52353068, 0x72506800, 0x14FF0050, 0x54FF5024, 0xC4830824, 0x9090C30C};

void MoveLinearOrb(int orb, float xvect, float yvect, float speed, int duration)
{
    NoxObject *ptr = UnitToPtr(orb);

    if (ptr)
    {
        int *fnOff = s_orbMoveCodeStream;
        float *vectptr = (float *)&ptr->velX;
        
        vectptr[0] = xvect;
        vectptr[1] = yvect;
        vectptr[8] = speed;
        BuiltinSingleArg((int)ptr, ((int)&fnOff - 0x5c308c)/4);
        NoxDeleteObjectTimer(orb, duration);
    }
}

static uint8_t s_glightningCode[]={
    0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0x8D, 0x40, 0x04, 
    0x50, 0xB8, 0x90, 0x37, 0x52, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3
};

void PlayFxGreenLightning(int x1, int y1, int x2, int y2, int duration)
{
    uint8_t *off = s_glightningCode;
    int copied[] = {duration, x1, y1, x2, y2};

    BuiltinSingleArg((int)copied, ((int)&off - 0x5c308c)/4);
}
