
#ifndef QUEUE_TIMER_H__
#define QUEUE_TIMER_H__

typedef void(*queue_timer_cb)(int);

void PushTimerQueue(int fps, int arg, queue_timer_cb cb);
void InitializeQueueTimer();

#endif
