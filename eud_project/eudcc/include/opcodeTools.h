
#ifndef OPCODE_TOOLS_H__
#define OPCODE_TOOLS_H__

void ResolveCallOpcode(char *setptr, int adder, int target);
void OpcodeCopiesAdvanced(int *destpt, int *pCallnode, const int *startAddr, const int *endAddr);

#endif
