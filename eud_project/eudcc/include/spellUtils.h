
#ifndef SPELL_UTILS_H__
#define SPELL_UTILS_H__

#include "intTypes.h"
#include "spellTypes.h"

void RemoveEffectTreadLightly(int plrUnit);
uint32_t GetAbilityCooldown(int plrUnit, AbilityTypes ty);
void SetAbilityCooldown(int plrUnit, AbilityTypes ty, uint32_t setTo);

typedef struct _castSpellData
{
    float aimRate;
    uint16_t minReactionDelay;
    uint16_t maxReactionDelay;
    uint32_t onReactionData;
    uint16_t minDeffensiveDelay;
    uint16_t maxDeffensiveDelay;
    uint32_t onDeffensiveData;
    uint16_t minDisablingDelay;
    uint16_t maxDisablingDelay;
    uint32_t onDisablingData;
    uint16_t minOffensiveDelay;
    uint16_t maxOffensiveDelay;
    uint32_t onOffensiveData;
    uint16_t minEscapeDelay;
    uint16_t maxEscapeDelay;
    uint32_t onEscapeData;
    uint8_t spellLevel;
} MonCastSpellData;

#define SPELLFLAG_ON_OFFENSIVE 0x40000000
#define SPELLFLAG_ON_REACTION 0x08000000
#define SPELLFLAG_ON_ESCAPE 0x80000000
#define SPELLFLAG_ON_DEFFENSIVE 0x10000000
#define SPELLFLAG_ON_DISABLING 0x20000000

void GetUnitSpellData(int unit, MonCastSpellData *outputData);
void SetUnitSpellData(int unit, MonCastSpellData *inputData);
void EmptyUnitSpellSet(int unit);
void ApplySpellToUnit(int unit, SpellTypes spell, int flags);
int GetSpellFlags(int unit, SpellTypes spell);

#endif

