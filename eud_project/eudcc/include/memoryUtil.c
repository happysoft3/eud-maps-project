
#include "memoryUtil.h"
#include "builtins.h"

#define NULLPTR (void *)0

static uint8_t s_memAllocStream[] =
{
    0x51, 0xB9, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD1, 0xB9, 0x60, 0x35, 0x40, 0x00, 0x50, 
    0xFF, 0xD1, 0xB9, 0x30, 0x72, 0x50, 0x00, 0x50, 0xFF, 0xD1, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0x59, 0xC3, 0x90
};

static uint8_t s_memFreeStream[] = 
{
    0x51, 0xB9, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD1, 0x50, 0xB9, 0x5D, 0x42, 0x40, 0x00,
    0xFF, 0xD1, 0x83, 0xC4, 0x04, 0x31, 0xC0, 0x59, 0xC3, 0x90
};

void *MemAlloc(uint16_t size)
{
    uint8_t *fnOff = s_memAllocStream;

    return (void *)BuiltinSingleArgReturn((int)size, ((int)&fnOff - 0x5c308c) / 4);
}

void MemFree(void *mem)
{
    uint8_t *fnOff = s_memFreeStream;

    return BuiltinSingleArg((int)mem, ((int)&fnOff - 0x5c308c) / 4);
}

#define MMSET_IMPL(ptr, length, value) \
    while (length != 0)   \
        ptr[--length] = value;

void NoxByteMemset(uint8_t *targetPtr, uint32_t length, uint8_t value)
{
    MMSET_IMPL(targetPtr, length, value)
}

void NoxWordMemset(uint16_t *targetPtr, uint32_t length, uint16_t value)
{
    MMSET_IMPL(targetPtr, length, value)
}

void NoxDwordMemset(uint32_t *targetPtr, uint32_t length, uint32_t value)
{
    MMSET_IMPL(targetPtr, length, value)
}

#define MMCOPY_IMPL(src, dest, length)  \
    while ((length--) != 0)    \
        dest[length] =src[length];

void NoxByteMemcopy(const uint8_t *srcPtr, uint8_t *destPtr, uint32_t length)
{
    MMCOPY_IMPL(srcPtr, destPtr, length)
}

void NoxWordMemcopy(const uint16_t *srcPtr, uint16_t *destPtr, uint32_t length)
{
    MMCOPY_IMPL(srcPtr, destPtr, length)
}

void NoxDwordMemcopy(const uint32_t *srcPtr, uint32_t *destPtr, uint32_t length)
{
    MMCOPY_IMPL(srcPtr, destPtr, length)
}

static char s_initsmemloaderOnce;
static uint8_t s_smemdestructCode[]={0xA1, 0xDC, 0x56, 0x59, 0x00, 0x85, 0xC0, 0x74, 0x0E, 0xFF, 0x30, 0x50, 
		0xB8, 0x5D, 0x42, 0x40, 0x00, 0xFF, 0xD0, 0x58, 0x58, 0xEB, 0xEE, 0xC7, 0x05, 0x1C, 
		0x82, 0x59, 0x00, 0xE0, 0xA6, 0x42, 0x00, 0x68, 0xE0, 0xA6, 0x42, 0x00, 0xC3};

typedef struct _smartMem
{
    struct _smartMem *pNext;
    struct _smartMem *pPrev;
    void *pUserdata;
} SmartMem;

#define SMEM_FIRST_PTR 0x5956dc
#define SMEM_DESTCODE_PTR 0x59567c
void InitializeSmartMemoryDestructor()
{
    if (s_initsmemloaderOnce)
        return;

    s_initsmemloaderOnce=1;

    *(int *)SMEM_FIRST_PTR = 0;
    NoxByteMemcopy(s_smemdestructCode, (uint8_t *)SMEM_DESTCODE_PTR, sizeof(s_smemdestructCode));
    int *t=(int *)0x59821c;
	int *c = (int *)(SMEM_DESTCODE_PTR + 34);
	*c=*t;
	*t=SMEM_DESTCODE_PTR;
}

void **AllocSmartMemory(uint32_t sz)
{
    SmartMem **pOff = (SmartMem **)SMEM_FIRST_PTR;
    SmartMem *pFirst = *pOff;
    SmartMem *pCur = (SmartMem *)MemAlloc((uint16_t)(sz + 8));

    if (pFirst)
    {
        pCur->pNext = pFirst;
        pFirst->pPrev = pCur;
    }
    else
    {
        pCur->pNext = NULLPTR;
    }
    pCur->pPrev =NULLPTR;
    *pOff = pCur;
    return &pCur->pUserdata;
}

void FreeSmartMemory(void *pMem)
{
    SmartMem *ptr = (SmartMem *)( (int)pMem - 8 );
	SmartMem *next = ptr->pNext, *prev = ptr->pPrev;
	
	if (prev)
	{
		prev->pNext = next;
	}
	else
	{
        *(SmartMem **)SMEM_FIRST_PTR = next;
	}
	if (next)
	{
		next->pPrev = prev;
	}
	MemFree(ptr);
}
