
#include "monsterBinscript.h"
#include "noxobject.h"
#include "builtins.h"

#define MONTB_NULLPTR 0

static MonsterBinScriptTable *getMonsterBinScript(NoxObject *monPtr)
{
    int *ec = (int *)monPtr->unitController;

    return (MonsterBinScriptTable *)ec[121];
}

void ChangeMonsterBinScript(int monId, MonsterBinScriptTable *scr)
{
    NoxObject *ptr = UnitToPtr(monId);

    if (!ptr)
        return;

    if (ptr->Class & UNIT_CLASS_MONSTER)
    {
        int *ec = (int *)ptr->unitController;

        ec[121] = (int)scr;
    }
}

void SyncWithCustomBinScript(int monId)
{
    NoxObject *ptr = UnitToPtr(monId);

    if (!ptr)
        return;

    MonsterBinScriptTable *tb = getMonsterBinScript(ptr);

    SetUnitMaxHealth(monId, tb->hp);
    NoxRetreatLevel(monId, tb->retreat_ratio);
    NoxResumeLevel(monId, tb->resume_ratio);

    float fspeed = ((float)tb->speed) * 0.03f;

    ptr->unitSpeed = fspeed;
    ptr->unitAccelMB = fspeed;

    float *fleeRange = (float *)ptr->unitController + 339;
    
    *fleeRange = tb->flee_range;
    int *unitActionFlags = (int *)ptr->unitController + 360;

    *unitActionFlags = tb->unit_flag;
}
