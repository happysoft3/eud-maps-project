
#include "globalData.h"
#include "noxscrProto.h"

#define GENERIC_GLOBAL_ID 1


GlobalData *LoadGlobalData()
{
    NoxScriptFunction *scrTable = (NoxScriptFunction *)(*(int *)0x75ae28);
    NoxScriptFunction *globalFn = &scrTable[GENERIC_GLOBAL_ID];

    return (GlobalData *)globalFn->varTable;
}


