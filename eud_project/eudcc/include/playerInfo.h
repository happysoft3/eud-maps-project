
#ifndef PLAYER_INFO_H__
#define PLAYER_INFO_H__

#include "noxobject.h"

typedef struct _playerUnitDetails
{
    int field0;
    uint16_t manaAmount;
    uint16_t manaAmountCopy;
    short manaMax;
    uint16_t currentHp;
    uint16_t maxHpbuff[31];
    uint16_t curHp4a;
    int curHp4c;
    int unknown50[2];
    int flagsValue;
    int unknown5c[3];
    NoxObject *currentweapon;
    NoxObject *nextweapon;
    int field70[29];
    float armorValue;
    float armorValueCopy;
    int unknownec[3];
    float xProfile;
    float yProfile;
    int unknownTimer;
    int unknown0104[4];  //104, 108, 10C, 110
    void *pPlayerClass;     //114
    int unknownField118;
    int unknownField11c;
} PlayerUnitDetails;

int GetPlayerIndex(int plrUnit);
short *PlayerIngameNickname(int plrUnit);
void ChangePlayerMana(int plrUnit, int cur, int max);

#endif

