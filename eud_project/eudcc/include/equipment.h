
#ifndef EQUIPMENT_H__
#define EQUIPMENT_H__

typedef enum
{
#define DECLARE_EQ_ENCHANTMENT(index, id, offset) id = offset,
#include "equip_enchants.inc"
#undef DECLARE_EQ_ENCHANTMENT
} EquipEnchantment;

typedef enum
{
#define DECLARE_EQ_ENCHANTMENT(index, id, offset) INDEX_##id = index,
#include "equip_enchants.inc"
#undef DECLARE_EQ_ENCHANTMENT
} EquipEnchantmentIndex;

extern EquipEnchantment *g_pEquipEnchantIndex;

#define SELECT_RANDOM_ENCHANT(FROM, TO) g_pEquipEnchantIndex[NoxRandomInteger(FROM, TO)]
#define EQUIPWEAPON_RANDOM_POWER() SELECT_RANDOM_ENCHANT(INDEX_ITEM_PROPERTY_weaponPower2, INDEX_ITEM_PROPERTY_weaponPower6)
#define EQUIPMENT_RANDOM_MATERIAL() SELECT_RANDOM_ENCHANT(INDEX_ITEM_PROPERTY_Matrial3, INDEX_ITEM_PROPERTY_Matrial7)
#define EQUIPWEAPON_RANDOM_ENCHANT() SELECT_RANDOM_ENCHANT(INDEX_ITEM_PROPERTY_stun1, INDEX_ITEM_PROPERTY_projectileSpeed4)
#define EQUIPARMOR_RANDOM_POWER() SELECT_RANDOM_ENCHANT(INDEX_ITEM_PROPERTY_armorQuality2, INDEX_ITEM_PROPERTY_armorQuality6)
#define EQUIPARMOR_RANDOM_ENCHANT() SELECT_RANDOM_ENCHANT(INDEX_ITEM_PROPERTY_FireProtect1, INDEX_ITEM_PROPERTY_Speed4)

void SetEquipEnchanment(int equip, EquipEnchantment enchnt1, EquipEnchantment enchnt2, EquipEnchantment enchnt3, EquipEnchantment enchnt4);
void AllowUndroppableItem(int item);
void DisableOblivionPickupEvent(int obliv);
int DoCopyEquipEnchantment(int srcItem, int destItem);

int GetConsumablesWeaponMaxCapacity(int weapon);
int GetConsumablesWeaponCurrentAmount(int weapon);
void UpdateConsumablesWeaponCapacity(int plrUnit, int consumable);

//사용하지 않을 경우, -1 을 전달합니다//
void SetConsumablesWeaponCapacity(int weapon, int currentAmount, int maxAmount);

#endif
