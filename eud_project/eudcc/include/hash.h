
#ifndef HASH_H__
#define HASH_H__

typedef struct _hashNode
{
    int key;
    int data;
    struct _hashNode *pNext;
} HashNode;

typedef struct _hashTable
{
    int count;  
    HashNode **table;
} HashTable;

void DeleteHashInstance(HashTable *pInstance);
HashTable *CreateHashInstance();
int HashGet(HashTable *pInstance, int key, int *dest, int bDelete);
void HashPushback(HashTable *pInstance, int key, int data);

#endif
