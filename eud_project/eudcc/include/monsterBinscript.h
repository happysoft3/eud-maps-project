
#ifndef MONSTER_BINSCRIPT_H__
#define MONSTER_BINSCRIPT_H__

typedef struct monsterBinScriptTable
{
	char name[64];
	int exp;
	int hp;
	int quest_hp;
	int speed;
	float retreat_ratio;
	float resume_ratio;
	float flee_range;
	int unit_flag;
	float run_multiplier;
	int moveSoundFrameA;
	int moveSoundFrameB;
	int meleeAttackFrame;
	float meleeRange;
	int damage;
	float attack_impact;
	int damage_type;
	int minMeleeAttackDelay;
	int maxMeleeAttackDelay;
	int poison_chance;
	int poison_strength;
	int poison_max;
	char missile_name[64];
	float mis_range;
	int mis_attackFrame;
	int minMissileDelay;
	int maxMissileDelay;
	int dieEvent;
	int deadEvent;
	int strikeEvent;
	int unit_id;
	struct monsterBinScriptTable *tableAddr;
} MonsterBinScriptTable;

//OnDie
typedef enum _MonsterBinDieHandler
{
	MonsterBinArcherDieEvent=			0x54a890,
	MonsterBinOgreWarlordDieEvent=		0x54a950,
	MonsterBinUrchinShamanDieEvent=		0x54a850,
	MonsterBinOgreDieEvent=			0x54a900,
	MonsterBinSwordsmanDie=		0x54a7d0,
} MonsterBinDieHandler;

typedef enum _MonsterBinDeadHandler
{
	MonsterBinTrollDeadEvent=			0x54a270,
	MonsterBinEmberDemonDeadEvent=		0x549d80,
	MonsterBinDemonDeadEvent=			0x549e00,
	MonsterBinBomberDeadEvent=			0x54a150,
	MonsterBinSkeletonLordDeadEvent=		0x54a750,
	MonsterBinGolemDeadEvent=			0x549fa0,
	MonsterBinMechGolemDeadEvent=		0x549e90,
} MonsterBinDeadHandler;

typedef enum _MonsterBinStrikeHandler
{
	MonsterBinBomberStrikeEvent=			0x549bb0,
	MonsterBinMonsterStrikeEvent=		0x549380,
	MonsterBinOgreStrikeEvent=			0x549220,
	MonsterBinScorpionStrikeEvent=		0x5495b0,
	MonsterBinVileZombieStrikeEvent=		0x549700,
	MonsterBinStoneGolemStrikeEvent=		0x5497e0,
	MonsterBinMechGolemStrikeEvent=		0x549960,
	MonsterBinWaspStrikeEvent=			0x549980,
	MonsterBinGhostStrikeEvent=			0x549a60,
	MonsterBinSpiderStrikeEvent=			0x549bc0,
	MonsterBinSpittingSpiderStrikeEvent=		0x549ca0,
} MonsterBinStrikeHandler;

void ChangeMonsterBinScript(int monId, MonsterBinScriptTable *scr);
void SyncWithCustomBinScript(int monId);

#endif

