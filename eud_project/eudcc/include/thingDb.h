
#ifndef THING_DB_H__
#define THING_DB_H__

typedef struct _thingDbInfo
{
    int thing_id;
    char *real_name;
    int unknown08;
    int prettyImage;
    int unknown10;
    int thingIdCopy;
    int classify;
    int unknown1c;
    int uflags;
    int unknown24;
    int unknown28;
    int unknown2c[2];
    float unknown34[2];
    int extendType; //2- cylinder, 3- rect
    float cylinderDiameter;
    float unknown44;
    float rectWidth;
    float rectHeight;
    float unknown58[6];
    float zsizeMin;
    float zsizeMax;
    int unknown78[25];
    struct _thingDbInfo *prev;
} ThingDbInfo;

#define THINGDB_CORE1   (*(int **)0x7520d4)
#define GET_THINGDB_INFO(id) (ThingDbInfo *)THINGDB_CORE1[id]

#endif

