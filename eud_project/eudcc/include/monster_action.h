
#ifndef MONSTER_ACTION_H__
#define MONSTER_ACTION_H__

typedef enum _creatureActionTypes
{
    ACTION_IDLE,
    ACTION_WAIT,
    ACTION_WAIT_RELATIVE,
    ACTION_ESCORT,
    ACTION_GUARD,
    ACTION_HUNT,
    ACTION_RETREAT,
    ACTION_MOVE_TO,
    ACTION_FAR_MOVE_TO,
    ACTION_DODGE,
    ACTION_ROAM,
    ACTION_PICKUP_OBJECT,
    ACTION_DROP_OBJECT,
    ACTION_FIND_OBJECT,
    ACTION_RETREAT_TO_MASTER,
    ACTION_FIGHT,
    ACTION_MELEE_ATTACK,
    ACTION_MISSILE_ATTACK,
    ACTION_CAST_SPELL_ON_OBJECT,
    ACTION_CAST_SPELL_ON_LOCATION,
    ACTION_CAST_DURATION_SPELL,
    ACTION_BLOCK_ATTACK,
    ACTION_BLOCK_FINISH,
    ACTION_WEAPON_BLOCK,
    ACTION_FLEE,
    ACTION_FACE_LOCATION,
    ACTION_FACE_OBJECT,
    ACTION_FACE_ANGLE,
    ACTION_SET_ANGLE,
    ACTION_RANDOM_WALK,
    ACTION_DYING,
    ACTION_DEAD,
    ACTION_REPORT,
    ACTION_MORPH_INTO_CHEST,
    ACTION_MORPH_BACK_TO_SELF,
    ACTION_GET_UP,
    ACTION_CONFUSED,
    ACTION_MOVE_TO_HOME,
    ACTION_INVALID,
} CreatureActionTypes;

typedef struct _creatureAction
{
	int actionType;
	int arg0;
	int arg1;
	int arg2;
	char gap_10[4];
	int field_14;
} CreatureAction;

CreatureAction *PushMonsterAction(int mob, int actionType, int a0, int a1, int a2);

#define FLOAT_INT_CASTING(fval) (*(int *)&fval)
#define DO_CREATURE_ACTION_CASTSPELL_AT(unit, spell_id, xpos, ypos) PushMonsterAction(unit, ACTION_CAST_SPELL_ON_LOCATION, spell_id, \
    FLOAT_INT_CASTING(xpos), FLOAT_INT_CASTING(ypos))

#define DO_CREATURE_ACTION_GUARD(unit, xpos, ypos, dir) PushMonsterAction(unit, ACTION_GUARD, \
 FLOAT_INT_CASTING(xpos), FLOAT_INT_CASTING(ypos), dir)

#define DO_CREATURE_ACTION_ESCORT(unit, owner) PushMonsterAction(unit, ACTION_ESCORT, 0, 0, (int)UnitToPtr(owner))

#endif

