
#include "hash.h"
#include "../include/memoryUtil.h"
#include "../include/builtins.h"

#define NULLPTR (void *)0
#define HASH_TABLECOUNT 32

static HashNode *createNode(int data, int key)
{
    HashNode *node = (HashNode *)AllocSmartMemory(sizeof(HashNode));

    node->data=data;
    node->key=key;
    node->pNext=NULLPTR;
}

static void eraseNodeAll(HashNode *node)
{
    HashNode *del;

    while (node)
    {
        del=node;
        node=node->pNext;
        FreeSmartMemory(del);
    }
}

void DeleteHashInstance(HashTable *pInstance)
{
    int count=HASH_TABLECOUNT;

    while (--count>=0)
        eraseNodeAll(pInstance->table[count]);
    FreeSmartMemory(pInstance->table);
    FreeSmartMemory(pInstance);
}

HashTable *CreateHashInstance()
{
    HashNode **table = (HashNode **) AllocSmartMemory(sizeof(HashNode *)*HASH_TABLECOUNT);

    NoxDwordMemset((uint32_t *)table, HASH_TABLECOUNT, 0);
    HashTable *instance = (HashTable *)AllocSmartMemory(sizeof(HashTable));

    instance->table = table;
    instance->count=0;
    return instance;
}

static int hashCompareWithKey(HashNode *node, int key, int *dest)
{
    int compare=node->key==key;

    if (compare && dest)
        *dest = node->data;
    return compare;
}

static void removeHashNode(HashNode **ppHead, HashNode *node, HashNode *prev)
{
    HashNode *next = node->pNext;

    if (*ppHead==node)
        *ppHead=next;
    else
        prev->pNext = next;
    FreeSmartMemory(node);
}

static int hashFind(HashNode **ppHead, int key, int *dest, int bDelete)
{
    HashNode *cur = *ppHead, *prev;

    while (cur)
    {
        if (hashCompareWithKey(cur, key, dest))
        {
            if (bDelete)
                removeHashNode(ppHead, cur, prev);
            return TRUE;
        }
        prev=cur;
        cur=cur->pNext;
    }
    return FALSE;
}

int HashGet(HashTable *pInstance, int key, int *dest, int bDelete)
{
    int inKey = key%HASH_TABLECOUNT;

    if (hashFind(&pInstance->table[inKey], key, dest, bDelete))
    {
        if (bDelete)
            pInstance->count-=1;
        return TRUE;
    }
    return FALSE;
}

void HashPushback(HashTable *pInstance, int key, int data)
{
    HashNode *node=createNode(data,key);
    int inkey= key%HASH_TABLECOUNT;

    HashNode **ppHead = &pInstance->table[inkey];

    if (*ppHead)
        node->pNext=*ppHead;
    *ppHead = node;
    pInstance->count+=1;
}
