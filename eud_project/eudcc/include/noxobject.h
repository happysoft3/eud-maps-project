
#ifndef NOX_OBJECT_H__
#define NOX_OBJECT_H__

#include "intTypes.h"
#include "noxobjectEnum.h"

typedef struct _hitPointData
{
    uint16_t amount;
    uint16_t maxAmount;
    int maxAmountCopy;
    int unknownArray[3];
} UnitHpData;

typedef struct _noxObject
{
    char *ScriptNameMB; //+0x0 유닛의 스크립트 이름 포인터
    int thingType;
    //+0x4 유닛타입(Thing) 번호
    //기계골렘 세마리가 있을 때 이들은 서로 같은 타입번호를 지닙니다
    int Class; //+0x8 유닛 클래스
    int SubClass; //+0xc 유닛 서브클래스
    int flags; //+0x10 유닛 플레그
    int field_14; 
    int field_18;
    float unitXP; //+0x1c 유닛 경험치 총 획득량
    int gap20;
    int netCode;
    //+0x24 유닛의 네트윅번호
    //show extents 명령어 입력 시 보이는 번호
    int mapExtentId; //+0x28 유닛의 맵 번호
    int globalID;
    //+0x2c 유닛의 스크립트 번호
    //기본적으로 제공되는 함수들은 이 번호를 사용해야 됩니다
    void *field_30;
    int teamId; //+0x34 유닛의 팀 번호
    float unitX; //+0x38 유닛의 x좌표
    float unitY; //+0x3c 유닛의 y좌표
    int someX_2;
    int someY_2;
    int someX_1;
    float someY_1;
    int velX;
    int velY;
    float field_58;
    float field_5C;
    char gap_60[8];
    float heightFromFloor; //+0x68
    float field_6C;
    int gap_70;
    float field_74;
    float Mass; //+0x78 유닛의 질량
    short unitLookAt; //+0x7c 유닛의 방향
    short direction1; //+0x7e
    int time_80;
    int gap_84;
    int createTime; //+0x88 유닛 생존시간
    int field_8C;
    int field_90;
    int field_94; 
    int unitTargetMB; //+0x98
    float someX_3; //+0x9c
    float someY_3; //+0xa0
    float fallingX; //+0xa4
    float fallingY; //+0xa8
    int moveClassMB; //+0xac
    int unitRadius; //+0xb0
    float unkfield_B4;
    float field_B8;
    float field_BC;
    float field_C0;
    float someY_4;
    float someX_4;
    int field_CC;
    float someX_6;
    float field_D4;
    int field_D8;
    float someY_6;
    float field_E0;
    float field_E4;
    float someX_5;
    float someY_5;
    float someX_7;
    float someY_7;
    char gap_f8[8];
    short shortX; //+0x100
    short shortY; //+0x102
    struct _noxObject *nextBlockStruct; //+0x104 다음 유닛 주소
    struct _noxObject *prevBlockStruct; //+0x108 이전 유닛 주소
    struct _noxObject *objectInBlock;
    //+0x10c 해당 유닛 안에 들어있는 유닛주소
    //상자를 열거나 통을 부수면 나오는 아이템의 주소가 여기 포함됨
    char unkfield_110[68];
    int buffMB;
    short buffTimers[32];
    char buffSomeByte[32];
    int gap_1b8;
    void *nextUnit;
    struct _noxObject *prevUnit;
    int nextDeletedThisFrameUnitMB;
    int deleteTime; //+0x1c8 유닛삭제 딜레이
    char gap_1cc[8];
    int nextDecayPtrMB;
    uint32_t gap_1d8;
    int nextUpdatableUnit;
    int prevUpdatableUnit;
    int isInUpdatableList;
    char unitWeight; //+0x1e8 유닛의 무게
    char gap_1e9;
    short unitCapacity; //+0xea 유닛의 인벤토리 수용량
    struct _noxObject *prevInventoryObj;
    struct _noxObject *nextInventoryObj;
    struct _noxObject *prevSome;
    struct _noxObject *firstInventoryObj;
    //+0x1f8 유닛의 첫번째 인벤토리 유닛 주소
    //가장 최근에 픽업한 인벤토리가 첫번째 인벤토리로 측정되며 GetLastItem 트리거가 이 주소를 참조합니다
    struct _noxObject *parentUnit;
    //+0x1fc 이 유닛의 소유자 유닛 주소
    //SetOwner 트리거는 이 영역을 건드립니다
    struct _noxObject *nextSlave;
    struct _noxObject *firstSlave;
    struct _noxObject *unitDamaged;
    uint32_t gap_20c;
    int damageX;
    int damageY;
    int time_218;
    char decelTimeMB;
    char accelTimeMB;
    short movement_21E;
    float unitSpeed; //+0x220 이동속도
    float unitAccelMB; //+0x224 가속도
    int field_228;
    UnitHpData *pHpData;    // void *field_22C;
    //+0x22c 유닛 체력 담당 포인터
    int field_230[32];
    void *unkFnPtr2B0;
    void *unkFn2B0DataPtr; 
    //+0x2b4 인벤토리 아이템 속성 포인터
    //금화의 수치 설정도 여기서 정해집니다
    void *collideFn; //+0x2b8 유닛 충돌 시 내부함수 포인터
    void *collideDataMB;
    void *XFerFnPtr2C0;
    void *pickupFnPtr;
    //+0x2c4 인벤토리 획득 시 내부함수 포인터
    //하버드 주으면 이펙트 출력되는 것을 여기서 제어할 수 있음
    void *dropFnPtr;
    //+0x2c8 인벤토리 버릴 시 내부함수 포인터
    void *damageFnPtr; // void(*damageFnPtr)(bigUnitStruct* victim, void* attacker, void* dealtBy, int damage, int dmgType);
    void *soundDamageFnPtr;
    //+0x2d0 유닛 공격 효과음 포인터
    void *dieFnPtr; //+0x2d4 유닛 파괴 시 내부 함수 포인터
    void *spawnAtDeath;
    //+0x2d8 유닛 파괴 시 생성할 유닛 포인터
    void *useFnPtr;
    //+0x2dc 인벤토리 사용 시 내부 함수주소
    void *useFnData;
    //+0x2e0 인벤토리 사용 시 내부 함수 데이터, 소모류 무기의 잔량정보 포인터가 이곳에 옵니다
    int gap_2e4;
    void *onUpdateFn; //+0x2e8 유닛 그리기 내부 함수주소
    void *unitController; //+0x2ec 유닛 특수설정 포인터
    //유닛 종류마다 주소공간 구조가 다름을 유의하십시오
    int gap_2f0;
    int field_2F4;
    int gap_2f8;
    int event_flag; //+0x2fc
    int script_num; //+0x300 픽업 이벤트 함수 트리거 번호
} NoxObject;

#define PTR_ISCLASS(ptr, check) (ptr->Class & (check))
#define PTR_IS_THINGID(ptr, thingId) ((thingId) == ptr->thingType)

NoxObject *UnitToPtr(int unitId);
#define GETLASTUNIT     *((NoxObject **)0x750710)
void SetUnitMaxHealth(int unit, int amount);
int CreateObjectAt(const char* unitName, float x, float y);
int CreateObjectById(int thingId, float x, float y);

typedef struct _unitVoiceData
{
    char *voiceName;
    int ontalkable;     //싱글모드 전용: NPC에 마우스 대면 '왓?' 거리는 거랑 같은 상황
    int ontaunt;        //누군가를 죽였을 때
    int unknown3;
    int onidle;     //가만히 정지 상태일 때(쉬고있는 중)
    int onengage;       //공격 준비할 때(아직 공격중은 아님)
    int onattackinit;   //(근접)공격할 때
    int unknown7;
    int onmeleehit;     //(근접)공격에 성공할 때
    int onmeleemiss;        //허공에 (근접)공격할 때
    int onmissileinit;  //미사일 쏘기 직전 ( 궁수로 치면 활 시위 당길 때. )
    int onshoot;        //미사일을 발사했을 때
    int onflee;         //도망갈 때(체력이 부족해서 도망치는 게 아니라 FleeRange 값에 의해 도망침)
    int onretreat;      //도망갈 때(체력부족)
    int onspellinit;        //마법 시전 직전
    int ondie;          //죽었을 때
    int onhurt;         //피해를 입을 때
    int onrecognize;        //적의 소리를 들음
    int onmove;         //이동중일 때
    struct _unitVoiceData *pNext;
} UnitVoiceData, *PUnitVoiceData;

void ChangeMonsterVoice(int mob, PUnitVoiceData voicedata);
void ChangeMonsterDefaultVoice(int mob, MonsterVoice voiceId);

int GetOwner(int unit);
float DistanceUnitToUnit(int unit1, int unit2);
void TeleportObjectVector(int unit, float xvect, float yvect);
int DummyUnitCreateAt(int objId, float xpos, float ypos);

#endif
