
#include "noxobject.h"
#include "thingDb.h"
#include "builtins.h"

static int s_unitToPtrCode[]=
{
    0x50725068, 0x2414FF00, 0x511B6068, 0x54FF5000, 0xC4830424,
    0x7230680C, 0xFF500050, 0x83042454, 0xC03108C4, 0x909090C3
};

static uint8_t s_unitCreateAtCode[]=
{
    0x56, 0x51, 0xB9, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD1, 0x8B, 0xF0, 0xFF, 0x36, 0xFF, 0x76, 0x04, 0xFF, 0x76, 0x08, 0xB9, 0x10,
    0x38, 0x4E, 0x00, 0xFF, 0xD1, 0x83, 0xC4, 0x04, 0x8B, 0xF0, 0x85, 0xF6, 0x74, 0x0D, 0x6A, 0x00, 0x56, 0xB9, 0x50, 0xAA, 0x4D, 
    0x00, 0xFF, 0xD1, 0x83, 0xC4, 0x08, 0xB9, 0x30, 0x72, 0x50, 0x00, 0x56, 0xFF, 0xD1, 0x83, 0xC4, 0x0C, 0x31, 0xC0, 0x59, 0x5E, 0xC3
};

NoxObject *UnitToPtr(int unitId)
{
    int *fnOff = s_unitToPtrCode;

    return (NoxObject *)BuiltinSingleArgReturn(unitId, ((int)&fnOff - 0x5c308c) / 4);
}

void SetUnitMaxHealth(int unit, int amount)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (!ptr)
        return;

    uint16_t casted = (uint16_t)amount;

    ptr->pHpData->amount = casted;
    ptr->pHpData->maxAmount = casted;
    ptr->pHpData->maxAmountCopy = amount;
}

int CreateObjectAt(const char *unitName, float x, float y)
{
    uint8_t *fnOff = s_unitCreateAtCode;
    int params[] = {*(int *)&y, *(int *)&x, (int)unitName};
    NoxObject *ptr = (NoxObject *)BuiltinSingleArgReturn((int)&params, ((int)&fnOff - 0x5c308c) / 4);

    if (!ptr)
        return 0;

    return ptr->globalID;
}

int CreateObjectById(int thingId, float x, float y)
{
    return CreateObjectAt( (GET_THINGDB_INFO(thingId))->real_name, x, y);
}

void MoveObjectVector(int unit, float xvect, float yvect)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (!ptr)
        return;

    NoxMoveObject(unit, ptr->unitX + xvect, ptr->unitY + yvect);
}

static UnitVoiceData *s_monVoiceDataArray[75];
static uint32_t s_monVoiceArrayCount;

static int preloadVoiceList()
{
    UnitVoiceData *target = *(UnitVoiceData **)0x663eec;

    while (target)
    {
        s_monVoiceDataArray[s_monVoiceArrayCount++] = target;
        target = target->pNext;
    }
    return s_monVoiceArrayCount;
}

void ChangeMonsterVoice(int mob, PUnitVoiceData voicedata)
{
    NoxObject *ptr = UnitToPtr(mob);

    if (ptr)
    {
        int *pController = (int *)ptr->unitController;

        pController[122] = (int)voicedata;
    }
}

void ChangeMonsterDefaultVoice(int mob, MonsterVoice voiceId)
{
    if (!s_monVoiceArrayCount)
    {
        if (!preloadVoiceList())
            return;
    }
    ChangeMonsterVoice(mob, s_monVoiceDataArray[voiceId]);
}

int GetOwner(int unit)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (ptr)
    {
        NoxObject *owner = ptr->parentUnit;

        if (owner)
            return owner->globalID;
    }
    return 0;
}

float DistanceUnitToUnit(int unit1, int unit2)
{
    NoxObject *ptr1 = UnitToPtr(unit1), *ptr2 = UnitToPtr(unit2);

    if (ptr1 && ptr2)
    {
        return NoxDistance(ptr1->unitX, ptr1->unitY, ptr2->unitX, ptr2->unitY);
    }
    return 0;
}

void TeleportObjectVector(int unit, float xvect, float yvect)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (ptr)
        NoxMoveObject(unit, ptr->unitX + xvect, ptr->unitY + yvect);
}

int DummyUnitCreateAt(int objId, float xpos, float ypos)
{
    int unit = CreateObjectById(objId, xpos, ypos);

    if (!unit)
        return 0;

    NoxObjectOff(unit);
    NoxDamage(unit, 0, 9999, -1);
    NoxFrozen(unit, TRUE);
    return unit;
}
