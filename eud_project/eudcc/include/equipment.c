
#include "equipment.h"
#include "playerInfo.h"
#include "noxobject.h"
#include "memoryUtil.h"
#include "builtins.h"

#define PROC_FAILURE 0
#define PROC_OK 1

static EquipEnchantment s_preLoaded[]={
    0,
#define DECLARE_EQ_ENCHANTMENT(index, id, offset) id,
#include "equip_enchants.inc"
#undef DECLARE_EQ_ENCHANTMENT
};

EquipEnchantment *g_pEquipEnchantIndex = s_preLoaded;

static void reupdateEquipEnchantment(NoxObject *ptr)
{
    for (int i = 0 ; i < 32 ; i ++)
        ptr->field_230[i] = 0x200;
}

void SetEquipEnchanment(int equip, EquipEnchantment enchnt1, EquipEnchantment enchnt2, EquipEnchantment enchnt3, EquipEnchantment enchnt4)
{
    NoxObject *ptr = UnitToPtr(equip);

    if (ptr)
    {
        int *slotPtr = (int *)ptr->unkFn2B0DataPtr;

#define EQUIPENUM_TO_PTR(ptr) *(int *)ptr
        if (enchnt1) slotPtr[0] = EQUIPENUM_TO_PTR(enchnt1);
        if (enchnt2) slotPtr[1] = EQUIPENUM_TO_PTR(enchnt2);
        if (enchnt3) slotPtr[2] = EQUIPENUM_TO_PTR(enchnt3);
        if (enchnt4) slotPtr[3] = EQUIPENUM_TO_PTR(enchnt4);
#undef EQUIPENUM_TO_PTR
        reupdateEquipEnchantment(ptr);
    }
}

static int s_codeStream[]={
		0x550CEC83, 0x14246C8B, 0x24748B56, 0xECAE391C, 0x74000001, 0xC0315E08, 0x0CC4835D,
		0x0845F6C3, 0x68207404, 0x0053EBF0, 0x2454FF56, 0x08C48304, 0x0F74C085, 0x53EC8068,
		0x56016A00, 0x082454FF, 0x680CC483, 0x004ED301, 0x909090C3};

void AllowUndroppableItem(int item)
{
    NoxObject *ptr = UnitToPtr(item);

    if (ptr)
        ptr->dropFnPtr = s_codeStream;
}

void DisableOblivionPickupEvent(int obliv)
{
    NoxObject *ptr = UnitToPtr(obliv);

    if (ptr)
        ptr->pickupFnPtr = (int *)0x53a720;
}

int DoCopyEquipEnchantment(int srcItem, int destItem)
{
    NoxObject *srcptr = UnitToPtr(srcItem), *destptr = UnitToPtr(destItem);

    if ((!srcptr) || (!destptr))
        return PROC_FAILURE;
    
	NoxDwordMemcopy((uint32_t *)srcptr->unkFn2B0DataPtr, (uint32_t *)destptr->unkFn2B0DataPtr, 4);	
	reupdateEquipEnchantment(destptr);
	return PROC_OK;
}

int GetConsumablesWeaponCurrentAmount(int weapon)
{
    NoxObject *ptr = UnitToPtr(weapon);

    if (ptr)
    {
        uint8_t *amountPtr = (uint8_t *)ptr->useFnData;

        if (amountPtr)
        {
            if (ptr->Class & UNIT_CLASS_WAND)
                return (int)amountPtr[0x6c];
            else if (ptr->Class & UNIT_CLASS_WEAPON)
                return (int)amountPtr[0];
        }
    }
    return -1;
}

int GetConsumablesWeaponMaxCapacity(int weapon)
{
    NoxObject *ptr = UnitToPtr(weapon);

    if (ptr)
    {
        uint8_t *amountPtr = (uint8_t *)ptr->useFnData;

        if (amountPtr)
        {
            if (ptr->Class & UNIT_CLASS_WAND)
                return (int)amountPtr[0x6d];
            else if (ptr->Class & UNIT_CLASS_WEAPON)
                return (int)amountPtr[1];
        }
    }
    return -1;
}

static uint8_t s_updateCharge[] = {
    0x51, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0x48, 0x0C, 0x51, 0x8B, 0x48, 0x08, 
    0x51, 0x8B, 0x48, 0x04, 0x51, 0xFF, 0x30, 0xB8, 0xB2, 0x82, 0x4D, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x59, 0x31, 0xC0, 0xC3};


typedef struct _consumableUpdate
{
    int playerIndex;
    NoxObject *updateTarget;
    int cur;
    int max;
} ConsumableUpdate;

void UpdateConsumablesWeaponCapacity(int plrUnit, int consumable)
{
    NoxObject *ptr = UnitToPtr(consumable);
    int pIndex = GetPlayerIndex(plrUnit);

    if (ptr && pIndex >= 0)
    {
        ConsumableUpdate updateData = {pIndex, ptr, };

        updateData.cur = GetConsumablesWeaponCurrentAmount(consumable);
        updateData.max = GetConsumablesWeaponMaxCapacity(consumable);

        uint8_t *fnOff = s_updateCharge;
        BuiltinSingleArg((int)&updateData, ((int)&fnOff - 0x5c308c) >> 2);
    }
}

void SetConsumablesWeaponCapacity(int weapon, int currentAmount, int maxAmount)
{
    NoxObject *ptr = UnitToPtr(weapon);

    if (!ptr)
        return;

    uint8_t *ammo = (uint8_t *)ptr->useFnData;

    if (!ammo)
        return;

    int toff = (ptr->Class & UNIT_CLASS_WAND) ? 0x6c : 0;

    if (currentAmount != -1)
        ammo[toff] = (uint8_t)currentAmount;
    if (maxAmount != -1)
        ammo[toff+1] = (uint8_t)maxAmount;
}

