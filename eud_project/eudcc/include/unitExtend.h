
#ifndef UNIT_EXTEND_H__
#define UNIT_EXTEND_H__

typedef void(*unit_some_callback_ty)(void);

typedef enum
{
    EXTENDED_ON_USE,
    EXTENDED_ON_PICKUP,
    EXTENDED_ON_DROP,
    EXTENDED_ON_DESTROY,
    EXTENDED_ON_UPDATE,
    EXTENDED_ON_COLLIDE,
} ExtendedCallbackEnum;

void SetUnitCallbackExtended(int unit, unit_some_callback_ty cb, ExtendedCallbackEnum ty);
void SetUnitCallbackOnDropBypass(int unit, unit_some_callback_ty cb);

#define REGIST_PICKUP_FUNCTION(unit, fn) UnitToPtr(unit)->script_num = fn;

#endif
