
#include "queueTimer.h"
#include "linkedList.h"
#include "builtins.h"
#include "memoryUtil.h"
#include "intTypes.h"

static LinkedList *s_timerQueueList;

typedef struct _queueTimerElem
{
    uint32_t fps;
    int arg;
    queue_timer_cb cb;
} QueueTimerElem;

void PushTimerQueue(int fps, int arg, queue_timer_cb cb)
{
    QueueTimerElem *elem = (QueueTimerElem *)AllocSmartMemory(sizeof(QueueTimerElem));

    elem->fps= fps + *((uint32_t *)0x84ea04);
    elem->arg=arg;
    elem->cb = cb;
    PushbackLinkedList(s_timerQueueList, (int)elem);
}

static void queueTimerLoopProc()
{
    LinkedNode *node = LinkedListFront(s_timerQueueList), *next;
    QueueTimerElem *elem;

    while (node)
    {
        elem = (QueueTimerElem *)node->value;
        next = node->next;
        if (!next)
            break;

        if (elem->fps <= *(uint32_t *)0x84ea04)
        {
            LinkedListPop(s_timerQueueList, node);
            elem->cb(elem->arg);
            FreeSmartMemory(elem);
        }
        node=next;
    }
    NoxFrameTimer(1, &queueTimerLoopProc);
}

void InitializeQueueTimer()
{
    if (s_timerQueueList)
        return;

    s_timerQueueList=CreateLinkedList();
    queueTimerLoopProc();
}
