
#include "spellUtils.h"
#include "playerInfo.h"
#include "noxobject.h"
#include "memoryUtil.h"
#include "builtins.h"

#define NULLPTR 0

static uint32_t s_removeTreadLightlyCode[]={
    0x72506850, 0x14FF0050, 0xC3006824, 0x046A004F, 0x2454FF50, 0x10C48308, 0x9090C358
};

void RemoveEffectTreadLightly(int plrUnit)
{
    NoxObject *ptr=UnitToPtr(plrUnit);

    if (ptr)
    {
        if (ptr->Class & UNIT_CLASS_PLAYER)
        {
            uint32_t *pOff = s_removeTreadLightlyCode;

            BuiltinSingleArg((int)ptr, ((int)&pOff - 0x5c308c) / 4);
        }
    }
}

static uint32_t *getAbilityTable(int plrUnit)
{
    NoxObject *ptr = UnitToPtr(plrUnit);

    do
    {
        if (!ptr)
            break;

        if (!(ptr->Class & UNIT_CLASS_PLAYER))
            break;

        int pIndex = GetPlayerIndex(plrUnit);

        if (pIndex < 0)
            break;

        return (uint32_t *)(0x753600 + (pIndex * 24));
    } while (FALSE);
    return NULLPTR;
}

uint32_t GetAbilityCooldown(int plrUnit, AbilityTypes ty)
{
    uint32_t *t = getAbilityTable(plrUnit);

    if (!t)
        return -1;

    return t[(int)ty+1];
}

void SetAbilityCooldown(int plrUnit, AbilityTypes ty, uint32_t setTo)
{
    uint32_t *t = getAbilityTable(plrUnit);

    if (!t)
        return;

    t[(int)ty+1] = setTo;
}

void GetUnitSpellData(int unit, MonCastSpellData *outputData)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (!ptr)
        return;

    uint32_t *ec = (uint32_t *)ptr->unitController;

    outputData->aimRate = *(float *)&ec[330];
    NoxDwordMemcopy(&ec[362], (uint32_t *)&outputData->minReactionDelay, 10);
    outputData->spellLevel = (uint8_t)ec[510];
}

void SetUnitSpellData(int unit, MonCastSpellData *inputData)
{
    NoxObject *ptr = UnitToPtr(unit);

    if (!ptr)
        return;

    uint32_t *ec = (uint32_t *)ptr->unitController;

    ec[330] = *(uint32_t *)&inputData->aimRate;
    NoxDwordMemcopy((const uint32_t *)&inputData->minReactionDelay, &ec[362], 10);
    ec[510] = (uint32_t)inputData->spellLevel;
}

void EmptyUnitSpellSet(int unit)
{
    NoxObject *ptr = UnitToPtr(unit);
    uint32_t *ec = (uint32_t *)ptr->unitController;

    NoxDwordMemset(&ec[372], SPELL_TELEPORT_TO_MARKER, 0);
}

void ApplySpellToUnit(int unit, SpellTypes spell, int flags)
{
    NoxObject *ptr=UnitToPtr(unit);

    if (ptr->Class & UNIT_CLASS_MONSTER)
    {
        int *ec = (int *)ptr->unitController;

        ec[372+spell] = flags;
    }
}

int GetSpellFlags(int unit, SpellTypes spell)
{
    NoxObject *ptr =UnitToPtr(unit);

    if (ptr->Class & UNIT_CLASS_MONSTER)
    {
        int *ec = (int *)ptr->unitController;

        return ec[372+spell];
    }
    return -1;
}
