
#ifndef SHOP_H__
#define SHOP_H__

#include "equipment.h"

void ShopUtilAllItemCount(int shopunit, int *pCount);
void ShopUtilItemCount(int shopunit, short thingId, int second, int *pCount);
void ShopUtilSetItemCount(int shopunit, short thingId, int second, int setTo);
void ShopUtilSetTradePrice(int shopunit, float buyPrice, float sellPrice);
void ShopUtilGetTradePrice(int shopunit, float *buyPrice, float *sellPrice);
void ShopUtilAppendItem(int shopunit, short thingId, int itemCount);
void ShopUtilAppendItemWithProperties(int shopunit, short thingId, int itemCount, 
    EquipEnchantment enchnt1, EquipEnchantment enchnt2, EquipEnchantment enchnt3, EquipEnchantment enchnt4);

#endif

