
#ifndef NOXSCR_PROTO_H__
#define NOXSCR_PROTO_H__

typedef struct _NoxScriptFunction
{
    char *fName;    //28
    int hasReturnVar;   //1- return {n}, 0- return nothing
    int argCount;
    int varCount;
    int varFieldSize;
    int *varSizeTable;
    int *field24;
    int *varTable;
    int *codeTable;
    int field36;
    int field40;
    int field44;
} NoxScriptFunction;

#endif

