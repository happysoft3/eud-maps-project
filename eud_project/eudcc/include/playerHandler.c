
#include "playerHandler.h"
#include "memoryUtil.h"
#include "opcodeTools.h"
#include "playerInfo.h"
#include "sound_define.h"
#include "builtins.h"

#define NULL 0

static int s_onplayerUpdateCallNodes[]=
{
    0x54d2d1,0x54d2e3,0x54d2f3,0x54d30a,0x54d348,
    0x54d375,0x54d3aa,0x54d45c,0x54d47b,0x54d513,
    0x54d527,0x54d566,0x54d574,0x54d588,0x54d594,
    0x54d5a6,0x54d5b2,0x54d5c4,0x54d5d1,0x54d5e2,
    0x54d5f4,0x54d610,0x54d622,0x54d62c,0x54d639,
    0x54d642,0x54d65b,0x54d662,0x54d668,0x54d67a,
    0x54d680,0x54d693,0x54d6a6,0x54d727,0x54d72d,
    0x54d733,0x54d73d,0x54d749,0x54d782,0
};

static int s_callNode4f8100[] = {
    0x4f8145,0x4f816e,0x4f81ad,0x4f820b,
        0x4f8218,0x4f8221,0x4f826b,0x4f8326,
        0x4f836e,0x4f8381,0x4f8387,0x4f838f,
         0x4f83a3,0x4f83ac,0x4f83c3,0x4f83dc,
         0x4f83eb,0x4f8407,0
};

typedef struct _playerHandlerCoreCode
{
    int onRespawn[314];
    int code4f7ef0[116];
    int code4f8100[200];
    void **refptr;
} PlayerHandlerCoreImpl;

static PlayerHandlerCoreImpl *s_coreCodeImpl;

static int s_callNode4f7ef0[] = {
    0x4f7ef7,        0x4f7f1b,        0x4f7f48,        0x4f7f59,
        0x4f7f7c,        0x4f7f8b,        0x4f7f98,        0x4f7fb5,
        0x4f7fd6,        0x4f7ff1,        0x4f800d,        0x4f801a,
        0x4f8028,        0x4f802f,        0x4f803d,        0x4f8055,
        0x4f807a,        0x4f8087,        0x4f80a1,        0
};

static int s_onRespawnCodeImpl[] = {
    0x50EC8B55, 0x758B5651, 0xF88E8B08, 0x85000001, 0x8B2674C9, 0x00251041, 0x85000001, 0x511274C0,
            0x1F8AE856, 0x595EFFDA, 0x41E85651, 0x5EFFDA1F, 0xF0898B59, 0xEB000001, 0x58595ED6, 0x9090C35D
};

static int s_code4f8460[] = {
    0x530CEC83, 0x748B5655, 0x31571C24, 0xECBE8BC0, 0x89000002, 0x89202444, 0x8A142444,
            0xF8835847, 0x83427721, 0x077404F8, 0x8C8524FF, 0x8B004F99, 0x84EA0415, 0x88AE8B00,
            0xA1000000, 0x0085B3FC, 0xE8D1EA29, 0x1776C239, 0x00040068, 0x4CDEE800, 0xC483F28D,
            0x74C08504, 0x90806812, 0x68C3004F, 0x004F9983, 0x850B68C3, 0x68C3004F, 0x00002000,
            0x8D4CBBE8, 0x04C483F2, 0x4A74C085, 0x0114878B, 0x80F60000, 0x00000E60, 0x313B7501,
            0x10888AC9, 0x51000008, 0x9E5227E8, 0x04C483F2, 0x2674C085, 0x000006BB, 0x08583900,
            0x978B3A74, 0x00000114, 0x8AC03150, 0x00081082, 0x71E85000, 0x83F29E52, 0xC08508C4,
            0x95E8DF74, 0x85F28D4C, 0x560F74C0, 0x00011BE8, 0x04C48300, 0x4F850B68, 0x8368C300,
            0xC3004F99, 0x01148F8B, 0xD2310000, 0x0810918A, 0xE8520000, 0xF29E52A8, 0x5604C483,
            0x9C255FE8, 0x04C483F2, 0x4F850B68, 0x9090C300
};

void InitialPlayerUpdateRewritten()
{
    PlayerHandlerCoreImpl *core = (PlayerHandlerCoreImpl *)MemAlloc(sizeof(PlayerHandlerCoreImpl));
    NoxObject *ptr = *(NoxObject **)0x654284;

    OpcodeCopiesAdvanced(core->onRespawn, s_onplayerUpdateCallNodes, (int *)0x54d2b0, (int *)0x54d794);
    OpcodeCopiesAdvanced(core->code4f7ef0, s_callNode4f7ef0, (int *)0x4f7ef0, (int *)0x4f80b4);
    int *patch4 = (int *)((int)core->code4f7ef0 + 0x62);
    *patch4 = 0x16a9090;
    uint16_t *patch2 = (uint16_t *)((int)core->code4f7ef0 + 0x94);
    *patch2 = 0x9090;
    ResolveCallOpcode((char *)s_onRespawnCodeImpl, 0x21, 0x4f2fb0);
    ResolveCallOpcode((char *)s_onRespawnCodeImpl, 0x2a, 0x4f2f70);
    ResolveCallOpcode((char *)core->code4f7ef0, 0x9b, (int)s_onRespawnCodeImpl);

    ResolveCallOpcode((char *)s_code4f8460, 0x4d, 0x40a5c0);
    ResolveCallOpcode((char *)s_code4f8460, 0x70, 0x40a5c0);
    ResolveCallOpcode((char *)s_code4f8460, 0x94, 0x51ab50);
    ResolveCallOpcode((char *)s_code4f8460, 0xba, 0x51abc0);
    ResolveCallOpcode((char *)s_code4f8460, 0xc6, 0x40a5f0);
    ResolveCallOpcode((char *)s_code4f8460, 0xd0, (int)core->code4f7ef0);       //@brief. 서브클래싱 링크
    ResolveCallOpcode((char *)s_code4f8460, 0xf3, 0x51ac30);
    ResolveCallOpcode((char *)s_code4f8460, 0xfc, (int)core->code4f7ef0);

    OpcodeCopiesAdvanced(core->code4f8100, s_callNode4f8100, (int *)0x4f8100, (int *)0x4f8414);
    ResolveCallOpcode((char *)core->code4f8100, 0x287, (int)s_code4f8460);
    patch2 = (uint16_t *)((int)core->onRespawn + 0x386);
    *patch2 = 0x9eb;
    patch4 = (int *)((int)core->onRespawn + 0x258);
    *patch4 = SOUND_QuestFinalDeath;
    ptr->unkFn2B0DataPtr = core;
    core->refptr = &ptr->unkFn2B0DataPtr;
    s_coreCodeImpl = core;
}

void ChangePlayerDieHandler(int user)
{
    NoxObject *ptr = UnitToPtr(user);

    if (!ptr)
        return;
    if (PTR_ISCLASS(ptr, UNIT_CLASS_PLAYER))
    {
        ptr->dieFnPtr = s_coreCodeImpl->onRespawn;
        ptr->onUpdateFn = s_coreCodeImpl->code4f8100;
    }
}

static int s_selfDamCodeStream[]={
    0x4C8B5651, 0xC9850C24, 0x748B2374, 0xF6851024, 0xF1391B74, 0x8B501374, 0x0001FC86,
    0x74C08500, 0x58F08B05, 0xEB58ECEB, 0xC3595E04, 0x68595E90, 0x004E17B0, 0x909090C3};

void DisablePlayerSelfDamage(int user)
{
    NoxObject *ptr = UnitToPtr(user);

    if (ptr)
    {
        if (PTR_ISCLASS(ptr, UNIT_CLASS_PLAYER))
            ptr->damageFnPtr = s_selfDamCodeStream;
    }
}

static void cancelPlayerDialogWithPTR(NoxObject *plrPtr)
{
    PlayerUnitDetails *pDetail = (PlayerUnitDetails *)plrPtr->unitController;
    
    if (pDetail->unknownField11c)
    {
        pDetail->unknownField11c = 0;
        int *pInfo = (int *)pDetail->pPlayerClass;
        pInfo[920]=0x10;
    }
}

void ResetAllPlayerHandler()
{
    int index=32;
    uint32_t *pTbOff = (uint32_t *)0x62f9e0;

    while(--index>=0)
    {
        NoxObject *ptr= (NoxObject *)*pTbOff;

        if (ptr)
        {
            ptr->damageFnPtr = (void *)0x4e17b0;
            ptr->dieFnPtr = (void *)0x54d2b0;
            if (ptr->onUpdateFn != 0x4E62F0)
                ptr->onUpdateFn = (void *)0x4f8100;
            cancelPlayerDialogWithPTR(ptr);
        }
        pTbOff += 0x4b7;
    }
    if (s_coreCodeImpl)
    {
        MemFree(*s_coreCodeImpl->refptr);
        *s_coreCodeImpl->refptr = NULL;
    }
}
