
#include "mapEvent.h"
#include "../include/memoryUtil.h"
#include "../include/team.h"
#include "../include/builtins.h"

void MapInitialize()
{
    BuiltinsInitialize();
    InitializeSmartMemoryDestructor();
    EnableCoopTeamMode();
    OnInitialMap();
}

void MapExit()
{
    DisableCoopTeamMode();
    OnShutdownMap();
}

