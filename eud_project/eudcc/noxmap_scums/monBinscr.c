
#include "monBinscr.h"
#include "../include/spellUtils.h"
#include "../include/memoryUtil.h"
#include "../include/stringUtil.h"
#include "../include/noxobject.h"
#include "../include/monsterBinscript.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static MonsterBinScriptTable *s_mbinGoon;

static void goonBin()
{
    s_mbinGoon = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));
    NoxByteMemset((uint8_t *)s_mbinGoon, sizeof(MonsterBinScriptTable), 0);
    s_mbinGoon->hp = 210;
    s_mbinGoon->speed = 80;
    s_mbinGoon->run_multiplier = 1.0f;
    s_mbinGoon->damage = 22;
    s_mbinGoon->damage_type = DAMAGE_DRAIN;
    s_mbinGoon->minMeleeAttackDelay = 19;
    s_mbinGoon->maxMeleeAttackDelay = 26;
    s_mbinGoon->meleeRange = 50.0f;
    s_mbinGoon->attack_impact = 10.0f;
    s_mbinGoon->poison_chance = 2;
    s_mbinGoon->poison_strength = 3;
    s_mbinGoon->poison_max = 20;
    s_mbinGoon->unit_flag = MON_STATUS_ALWAYS_RUN;
    s_mbinGoon->strikeEvent = MonsterBinVileZombieStrikeEvent;
    s_mbinGoon->deadEvent = MonsterBinTrollDeadEvent;
}

int CreateGoon(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_GOON, xpos, ypos);

    ChangeMonsterBinScript(mon, s_mbinGoon);
    SyncWithCustomBinScript(mon);
    return mon;
}

int CreateLich(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_LICH, xpos, ypos);

    SetUnitMaxHealth(mon, 475);
    ApplySpellToUnit(mon, SPELL_FUMBLE, 0);
    ApplySpellToUnit(mon, SPELL_SHIELD, SPELLFLAG_ON_DEFFENSIVE);
    ApplySpellToUnit(mon, SPELL_POISON, SPELLFLAG_ON_DISABLING);
    return mon;
}

int CreateMystic(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_WIZARD, xpos, ypos);

    SetUnitMaxHealth(mon, 285);
    ApplySpellToUnit(mon, SPELL_BLINK, 0);
    ApplySpellToUnit(mon, SPELL_SHOCK, SPELLFLAG_ON_ESCAPE);
    ApplySpellToUnit(mon, SPELL_SHIELD, SPELLFLAG_ON_DEFFENSIVE);
    return mon;
}

static MonsterBinScriptTable *s_pFirespriteBin;

static void firespriteBin()
{
    MonsterBinScriptTable *pTable = (MonsterBinScriptTable *)AllocSmartMemory(sizeof(MonsterBinScriptTable));

    s_pFirespriteBin = pTable;
    NoxByteMemset((uint8_t *)pTable, sizeof(MonsterBinScriptTable), 0);
    pTable->hp = 160;
    pTable->speed=75;
    pTable->mis_range = 300.0f;
    pTable->minMissileDelay = 13;
    pTable->maxMissileDelay = 21;
    pTable->mis_attackFrame = 0;
    pTable->run_multiplier = 1.0f;
    pTable->deadEvent = 5545472;
    pTable->unit_flag = MON_STATUS_NEVER_RUN;
    CopyString("WeakFireball", pTable->missile_name);
}

int CreateFiresprite(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_FIRE_SPRITE, xpos, ypos);

    ChangeMonsterBinScript(mon, s_pFirespriteBin);
    SyncWithCustomBinScript(mon);
    return mon;
}

void InitializeBinScript()
{
    goonBin();
    firespriteBin();
}
