
#ifndef MON_BINSCR_H__
#define MON_BINSCR_H__

int CreateGoon(float xpos, float ypos);
int CreateLich(float xpos, float ypos);
int CreateMystic(float xpos, float ypos);
int CreateFiresprite(float xpos, float ypos);
void InitializeBinScript();

#endif

