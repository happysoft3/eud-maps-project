
#include "mapEvent.h"
#include "native.h"
#include "../include/printUtil.h"
#include "../include/builtins.h"

static void initPickets()
{
    SetPicketText(NoxGetObject("mpic1"), u"SCumsRPG:  Tavern Of The Rising Sun");
    SetPicketText(NoxGetObject("mpic2"), u"SCumsRPG:     Griswold's Fine Weapons");
    SetPicketText(NoxGetObject("mpic3"), u"SCumsRPG:   Trouble Center");
    SetPicketText(NoxGetObject("mpic4"), u"SCumsRPG:     Adria's Waypoints");
    SetPicketText(NoxGetObject("mpic5"), u"SCumsRPG:  Quest1: Clear the Poisoned Fountain");
    SetPicketText(NoxGetObject("mpic6"), u"SCumsRPG: Quest2: Kill the Butcher!");
    SetPicketText(NoxGetObject("mpic7"), u"SCumsRPG:   Quest3: End the Mad King's Reign");
    SetPicketText(NoxGetObject("mpic8"), u"SCumsRPG:    Quest4: Meet Zhar, the Mad");
    SetPicketText(NoxGetObject("mpic9"), u"SCumsRPG:    Quest5: Slay the Dark Lord");
    SetPicketText(NoxGetObject("mpic10"), u"SCumsRPG:    Quest6: Stop the Evil");
    SetPicketText(NoxGetObject("mpic11"), u"SCumsRPG:   Deposit Block");
    SetPicketText(NoxGetObject("mpic12"), u"SCumsRPG:   Deposit Block");
    SetPicketText(NoxGetObject("mpic13"), u"SCumsRPG:    DO NOT PRESS");
    SetPicketText(NoxGetObject("mpic14"), u"스타크래프트 유즈맵 RPG- 원작.KadenD 두번째 제작. tealc, KITTY  (수정: 신에이)");
}

void OnInitialMap()
{
    NativeInit();
    initPickets();
}

void OnShutdownMap()
{}
