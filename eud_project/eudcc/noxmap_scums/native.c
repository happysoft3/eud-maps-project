
#include "native.h"
#include "monsterSetting.h"
#include "../include/fxeffect.h"
#include "../include/objectIDdefines.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"

static int s_bossman;
static int s_diablo;
static int s_butcher;
static int s_adria;
static int s_ded = 0;
static int s_brawler;
static int s_pepin;
static int s_cain;
static int s_gillian;
static int s_gillianWaypoint;
static int s_kaelRills;
static int s_kaelRillsWaypoint;
static int s_wirt;
static int s_wirtWaypoint;
static int s_zhar;
static int s_archBishop;
static int s_josh;
static int s_lachdanan;
static int s_farnham;
static int s_capedBaldy;
static int s_capedBaldyT;
static int s_waypointCapedBlady;
static int s_thefrog;
static int s_therat;
static int s_frogBuddy;
static int s_queen;
static int s_rocke;

static void NULLFunction() {}

static int s_dungeonMaster;

void EnterArena()
{
    NoxCastSpellObjectObject(SPELL_BLINK, OTHER, OTHER);
}

void LeaveArena()
{
    NoxMoveObject(OTHER, 2770.0f, 2614.0f);
}

void ArenaWaypoints()
{
    NoxWaypointToggle(NoxGetWaypoint("AS1"));
    NoxWaypointToggle(NoxGetWaypoint("AS2"));
    NoxWaypointToggle(NoxGetWaypoint("AS3"));
    NoxWaypointToggle(NoxGetWaypoint("AS4"));
    NoxWaypointToggle(NoxGetWaypoint("AS5"));
    NoxWaypointToggle(NoxGetWaypoint("AS6"));
    if (NoxIsWaypointOn(NoxGetWaypoint("AS1")))
    {
        NoxSay(s_brawler, "SCumsRPG:        Arena is ON! Blinks take you to the arena!");
    }
    if (!NoxIsWaypointOn(NoxGetWaypoint("AS1")))
    {
        NoxSay(s_brawler, "SCumsRPG:        Arena is OFF! Blinks take you to town!");
    }
}


///PlayerSpeak///

void Test()
{
    NoxSay(OTHER,"SCumsRPG:        The sanctity of this place has been fouled.");
    NoxDeleteObject(NoxGetObject("Floor1"));
        
}

void SmellsOfDeath()
{
    NoxSay(OTHER,"SCumsRPG:        The smell of death surrounds me.");
    NoxDeleteObject(NoxGetObject("Floor5"));
}

void ItsHot()
{
    NoxSay(OTHER,"SCumsRPG:        It's hot down here.");
    NoxDeleteObject(NoxGetObject("Floor9"));
}

void NoEnd()
{
    NoxSay(OTHER,"SCumsRPG:        Does this Dungeon have no End?");
    NoxDeleteObject(NoxGetObject("DOTE"));
}

void Close()
{
    NoxSay(OTHER,"SCumsRPG:        I must be getting close.");
    NoxDeleteObject(NoxGetObject("Floor13"));
}

void Huh()
{
    NoxSay(OTHER,"SCumsRPG:        This must be what that Knight wanted..");
}

///Teleporters///

void Ch1Waypoint()
{
    NoxObjectOn(NoxGetObject("Ch1WP"));
    NoxObjectOff(NoxGetObject("WP1Open"));

}

void Ch2Waypoint()
{
    NoxObjectOn(NoxGetObject("Ch2WP"));
    NoxObjectOff(NoxGetObject("WP2Open"));
}

void Ch3Waypoint()
{
    NoxObjectOn(NoxGetObject("Ch3WP"));
    NoxObjectOff(NoxGetObject("WP3Open"));
}

void Ch4Waypoint()
{
    NoxObjectOn(NoxGetObject("Ch4WP"));
    NoxObjectOff(NoxGetObject("WP4Open"));
}

///NPCS///

static void PepinServices()
{
    NoxSay(s_pepin,"SCumsRPG:        What ails you, my friend?");
    NoxPlayAudioByIndex(SOUND_NPCTalkable, NoxGetWaypoint("Pepin"));
    NoxRestoreHealth(OTHER, NoxMaxHealth(OTHER) - NoxCurrentHealth(OTHER));
    NoxCastSpellObjectObject(SPELL_RESTORE_MANA, s_pepin, OTHER);
    NoxCastSpellObjectObject(SPELL_CURE_POISON, s_pepin, OTHER);    
}

static char *s_cainGreet[]=
{
    "SCumsRPG:        Hello, my friend! Stay awhile and listen.",
    "SCumsRPG:        Gillian is a fine woman. Visit her down at the Trouble Center if you need quests.",
    "SCumsRPG:        Ah, Pepin. I count him as a true friend. His skill and knowledge for restoring others is equaled by few, and his door is always open.",
    "SCumsRPG:        The witch, Adria, is an anomaly here. She figured out how to teleport adventurers quickly into the dungeon, provided they know where they're going.",
    "SCumsRPG:        Griswold - a man of great action. He is a skilled craftsman, and if he claims to help you in anyway, you can count on him.",
    "SCumsRPG:        Ogden has owned and run the Rising Sun Tavern for almost four years now. He is a good man with a deep sense of responsibility.",
    "SCumsRPG:        Wirt, the urchin, is around here somewhere. He says it helps him do business; importing 'Quality' goods.",
    "SCumsRPG:        Poor Farnham. He finds comfort only at the bottom of his tankard, sometimes acting out fights he says he had.",
};

static void DeckardCain()
{
    NoxSay(s_cain, s_cainGreet[NoxRandomInteger(0, 7)]);
    NoxPlayAudioByIndex(SOUND_NPCTalkable, NoxGetWaypoint("Cain"));
}

static void GillianInteract()
{
    if (!NoxIsObjectOn(NoxGetObject("q1")))
    {
        if (!NoxIsObjectOn(NoxGetObject("q2")))
        {
            if (!NoxIsObjectOn(NoxGetObject("q3")) )
            {
                if (!NoxIsObjectOn(NoxGetObject("q4")) )
                {
                    if (!NoxIsObjectOn(NoxGetObject("q5")) )
                    {
                        if (!NoxIsObjectOn(NoxGetObject("q6")) )
                        {
                            NoxSay(s_gillian,
                            "SCumsRPG:        You've completed all of the quests here! The door is now unlocked, should you want to face the final challenge.");
                            NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
                            NoxUnlockDoor(NoxGetObject("OPMDoor"));
                            NoxDeleteObject(NoxGetObject("OPMKILL"));
                            NoxObjectOn(NoxGetObject("OPMTeleport"));
                        }
                    }
                }
            }
        }
    }
    else
    {
    NoxSay(s_gillian,
    "SCumsRPG:        Welcome to the Trouble Center! Step on a pressure plate to ask about the quest, use the button to turn it in!");
    NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
    }

}

static void Kael3()
{
    int dead=NoxGetObject("KaelDead");

    NoxDestroyChat(s_kaelRills);
    NoxMoveObject(s_kaelRills, 1690.0f,2886.0f);
    NoxMoveObject(dead, 2185.0f,1863.0f);
    NoxDamage(dead, OTHER, 1, 0);
    NoxPlayAudioByIndex(SOUND_WoundedNPCDie, s_kaelRillsWaypoint);
}

static void Kael2()
{
    NoxSay(s_kaelRills,
    "SCumsRPG:        Now everyone is dead. Killed by a demon he called The Butcher. Avenge us! Find this butcher and slay him so that our souls may finally rest.");
    NoxPlayAudioByIndex(SOUND_WoundedNPCTalkable, s_kaelRillsWaypoint);
    NoxSecondTimer(7,&Kael3);
}

static void Kael()
{
    NoxSay(s_kaelRills,
    "SCumsRPG:        Please, listen to me. The archbishop Lazarus, he led us down here to find the lost prince. The bastard led us into a trap!...");
    NoxPlayAudioByIndex(SOUND_WoundedNPCTalkable, s_kaelRillsWaypoint);
    NoxSecondTimer(7, &Kael2);    
}

static void Wirt()
{
    if (NoxRandomInteger(0, 1))
    {
        NoxSay(s_wirt,"SCumsRPG:        I am an importer of quality goods. For you, a fee of 50 gold to browse my stock.");
        NoxPlayAudioByIndex(SOUND_UrchinFlee, s_wirtWaypoint);
    }
    else
    {
        NoxSay(s_wirt,"SCumsRPG:        If you want to peddle junk, you'll have to see Griswold. I'm sure that he will snap up whatever you can bring him.");
        NoxPlayAudioByIndex(SOUND_UrchinTaunt, s_wirtWaypoint);
    }
}

static void Zhar()
{
    NoxSay(s_zhar,
        "SCumsRPG:        What?! Why are you here? All these interruptions are enough to make one insane! Trouble me no more!");
    NoxMoveObject(OTHER,2298.0f,3921.0f);
    NoxPlayFX(FX_TELEPORT,2298.0f,3921.0f,2298.0f,3921.0f);
    NoxPlayAudioByIndex(SOUND_TeleportIn, NoxGetWaypoint("Floor7"));
    NoxDamage(NoxGetObject("ZharMeet"), OTHER,1,0);
}

static void TheBrawler()
{
    NoxSay(s_brawler, "SCumsRPG:        Ya' lookin' for a fight? Hit the chain to toggle the arena, step on the plate for transport.");
    NoxPlayAudioByIndex(SOUND_MaleNPC2Talkable, NoxGetWaypoint("Brawler"));
}

static void Adria()
{
    NoxSay(s_adria,"SCumsRPG:        I sense a soul in search of answers.");
    PlaySoundAround(s_adria, SOUND_MaidenTalkable);
}

static void Farnham()
{
    NoxSay(s_farnham, "War05A.scr:DrunkGreeting");
    PlaySoundAround(s_farnham, SOUND_FireKnight2Talkable);
    PlaySoundAround(s_farnham, SOUND_HumanMaleDrinkJug);
    NoxEnchant(s_farnham, ENCHANT_CONFUSED, 10.0f);
}

#define MY_WAPOINT_MEAT 57

static void Rocke()
{
    if (!NoxCurrentHealth(NoxGetObject("Bugfix1")))
    {
        NoxSay(s_rocke,"SCumsRPG:        Thank ye' for bringin' me Rocky! Har's sum' mor' meat!");
        CreateObject("RottenMeat", MY_WAPOINT_MEAT);
        PlaySoundAround(s_rocke, SOUND_MaleNPC1Talkable);
    }
    else
    {
        NoxSay(s_rocke,"SCumsRPG:        Oi! Can ye' bring me a rock? Not jus' any rock tho'. Aye special rock!");
        PlaySoundAround(s_rocke, SOUND_MaleNPC1Talkable);
    }
}

static char *s_joshgreet[]={
    "SCumsRPG:        Lovely cave, eh? Use Blocks to power rooms, it'll give you a foothold.",
    "SCumsRPG:        Nice to meet you. Don't rush or you might get overwhelmed.",
    "SCumsRPG:        Seen those lightning balls? Metal armor conducts electricity.",
};

static void Josh()
{
    NoxSay(s_josh, s_joshgreet[NoxRandomInteger(0, 2)]);
    NoxPlayAudioByIndex(SOUND_FireKnight1Talkable, NoxGetWaypoint("Josh"));
}

static void Lachdanan()
{
    if (!NoxCurrentHealth(NoxGetObject("LachdananMeet")))
    {
        int LachdananQ = NoxCurrentHealth(NoxGetObject("LachdananQ"));
        int elixir = NoxGetObject("Elixir");

        if (LachdananQ >= 0)
        {
            if (NoxHasItem(OTHER, elixir))
            {
            NoxSay(s_lachdanan,
            "SCumsRPG:        You have saved my soul from Damnation, and for that I am in your debt. If I can find a better way to repay you, I will. For now take this.");
            PlaySoundAround(s_lachdanan, SOUND_GhostRecognize);
            NoxDamage(NoxGetObject("LachdananQ"),s_lachdanan,100,1);
            CreateObject("InvulnerabilityPotion", NoxGetWaypoint("Potion"));
            }
            else
            {
                NoxSay(s_lachdanan,
                "SCumsRPG:        I have heard of an Elixir that could lift the curse and allow my soul to rest. Please aid me and find the Elixir.");
                PlaySoundAround(s_lachdanan, SOUND_GhostRecognize);
                NoxMoveObject(elixir,4569.0f,2738.0f);
            }
        
        
        }
        if (LachdananQ == 0)
        {
            NoxSay(s_lachdanan,"SCumsRPG:        Thank you for helping me. Here's another one.");
            PlaySoundAround(s_lachdanan, SOUND_GhostRecognize);
            CreateObject("InvulnerabilityPotion", NoxGetWaypoint("Potion"));
        }
    }
    else
    { 
        NoxSay(s_lachdanan,
        "SCumsRPG:        Please, don't kill me, just hear me out. I was once Captain of King Leoric's knights. His curse fell upon us for the part we played in his death.");
        PlaySoundAround(s_lachdanan, SOUND_GhostRecognize);
        NoxDamage(NoxGetObject("LachdananMeet"), s_lachdanan,100,1);
    }
}

static char *s_capedBaldyGreet[]={
    "SCumsRPG:        I thought you would be stronger.",
    "SCumsRPG:        I'll leave tomorrow's problems to tomorrow's me.",
    "SCumsRPG:        Hm.. Maybe eggs over rice tonight.",
    "SCumsRPG:        I've become too strong.",
};

static void CapedBaldy2()
{
    NoxSay(s_capedBaldy, s_capedBaldyGreet[NoxRandomInteger(0, 3)]);
    NoxObjectOn(NoxGetObject("HiddenTele1"));
    NoxEnchantOff(OTHER,ENCHANT_INVULNERABLE);
    PlaySoundAround(s_capedBaldy, SOUND_PunchMissing);
    NoxDamage(OTHER,s_capedBaldy,999999,DAMAGE_CRUSH);
}

static void FROG()
{
    if (!NoxCurrentHealth(s_frogBuddy))
    {
       NoxSay(s_thefrog,"SCumsRPG:        My buddy's been different recently.");
    }
    else
    {
        NoxSay(s_thefrog,"SCumsRPG:        Hey, let me unlock all the waypoints for you.");
        Ch1Waypoint();
        Ch2Waypoint();
        Ch3Waypoint();
        Ch4Waypoint();
    }
}

static void RAT()
{
    if (!NoxCurrentHealth(s_frogBuddy))
    {
        NoxSay(s_therat,"SCumsRPG:        My buddy's been odd.");
    }
    else
    {
        NoxSay(s_therat,"SCumsRPG:        Let me turn on Double Spawns for you.");
        NoxObjectOn(NoxGetObject("SewerM"));
        NoxObjectOn(NoxGetObject("Floor1M"));
        NoxObjectOn(NoxGetObject("Floor2M"));
        NoxObjectOn(NoxGetObject("Floor3M"));
        NoxObjectOn(NoxGetObject("Floor5M"));
        NoxObjectOn(NoxGetObject("Floor7M"));
        NoxObjectOn(NoxGetObject("Floor8M"));
        NoxObjectOn(NoxGetObject("Floor9/10M"));
        NoxObjectOn(NoxGetObject("Floor13M"));
    }
}

static void CapedBaldy3()
{
    NoxSay(s_capedBaldy,"SCumsRPG:        WHO LET THESE BUGS IN HERE!?");
    NoxMoveObject(s_capedBaldy,3441.0f,1111.0f);
    NoxMoveObject(s_capedBaldyT,2818.0f,2085.0f);
    NoxMoveWaypoint(s_waypointCapedBlady,2818.0f,2085.0f);
}

static void Queen()
{
    int qtalk=NoxGetObject("QueenTalk1");

    if (!NoxCurrentHealth(qtalk))
    {
        NoxSay(s_queen,"SCumsRPG:        Bow down to the Wasp Queen! For I will rule the land!");
        PlaySoundAround(s_queen, SOUND_HecubahRecognize);
    }
    else
    {
        NoxSay(s_queen,"SCumsRPG:        Bow down to the Wasp Queen! Since your here, I can finally get my revenge on Caped Baldy!");
        float xpos = NoxGetWaypointX(s_waypointCapedBlady), ypos = NoxGetWaypointY(s_waypointCapedBlady);

        for (int i = 0 ; i < 6 ; i ++)
            CreateObjectById(OBJ_WASP, xpos, ypos);
        PlaySoundAround(s_queen, SOUND_HecubahTaunt);
        NoxDamage(qtalk, s_queen,100, DAMAGE_FLAME);
        NoxSecondTimer(5, &CapedBaldy3);
    }
}

static void CapedBaldyT()
{
    NoxSay(s_capedBaldyT,"SCumsRPG:        Someone flooded my room with BUGS. I'm staying here while they EXTERMINATE them.");
    NoxPlayAudioByIndex(SOUND_NPCTalkable, s_waypointCapedBlady);
    NoxObjectOn(NoxGetObject("EndTele"));
}

static void initNPCTalk()
{
    NoxSetupDialog(s_pepin, DIALOG_NORMAL,&PepinServices,&NULLFunction);
    NoxSetupDialog(s_cain, DIALOG_NORMAL,&DeckardCain,&NULLFunction);
    NoxSetupDialog(s_gillian, DIALOG_NORMAL,&GillianInteract,&NULLFunction);
    NoxSetupDialog(s_kaelRills, DIALOG_NORMAL,&Kael, &NULLFunction);
    NoxSetupDialog(s_wirt, DIALOG_NORMAL,&Wirt, &NULLFunction);
    NoxSetupDialog(s_zhar, DIALOG_NORMAL, &Zhar,&NULLFunction);
    NoxSetupDialog(s_brawler,DIALOG_NORMAL,&TheBrawler,&NULLFunction);
    NoxSetupDialog(s_adria,DIALOG_NORMAL,&Adria,&NULLFunction);
    NoxSetupDialog(s_farnham,DIALOG_NORMAL,&Farnham,&NULLFunction);
    NoxSetupDialog(s_rocke,DIALOG_NORMAL, &Rocke,&NULLFunction);
    NoxSetupDialog(s_josh,DIALOG_NORMAL, &Josh,&NULLFunction);
    NoxSetupDialog(s_lachdanan,DIALOG_NORMAL, &Lachdanan,&NULLFunction);
    NoxSetupDialog(s_capedBaldy,DIALOG_NORMAL, &CapedBaldy2,&NULLFunction);
    NoxSetupDialog(s_thefrog,DIALOG_NORMAL, &FROG,&NULLFunction);
    NoxSetupDialog(s_therat,DIALOG_NORMAL, &RAT,&NULLFunction);
    NoxSetupDialog(s_queen,DIALOG_NORMAL, &Queen,&NULLFunction);
    NoxSetupDialog(s_capedBaldyT,DIALOG_NORMAL, &CapedBaldyT,&NULLFunction);
}


void CapedBaldy()
{
    NoxSay(s_capedBaldy,"SCumsRPG:        Uh, hey man. I'm not gonna fight you, but, don't poke me.");
    PlaySoundAround(s_capedBaldy, SOUND_NPCTalkable);
}

void cheatroom()
{
    if (NoxHasItem(OTHER, NoxGetObject("TheApple")))
    {
        NoxMoveObject(OTHER, 3942.0f, 2624.0f);
    }
}

void WirtPay()
{
    if (NoxGetGold(OTHER) > 50)
    {
        NoxChangeGold(OTHER,-50);
        NoxSay(s_wirt,"SCumsRPG:        Yes! I'm glad you accepted my offer.");
        NoxObjectOn(NoxGetObject("WirtStock"));
        NoxObjectOff(NoxGetObject("WirtPay"));
        NoxPlayAudioByIndex(SOUND_UrchinTaunt, s_wirtWaypoint);
    }
    else
    {
        NoxSay(s_wirt,"SCumsRPG:        You don't have enough gold to browse my selection.");
        NoxPlayAudioByIndex(SOUND_UrchinTaunt, s_wirtWaypoint);
    }
}

///Quests///

void RockQuest()
{
    if (NoxIsCaller(NoxGetObject("Rocky")))
    {
        NoxSay(s_rocke,"SCumsRPG:        Rocky! Thank ye' greatly! Let 'im share sum' meat wit' ye'!");
        CreateObject("RottenMeat", MY_WAPOINT_MEAT);
        PlaySoundAround(s_rocke, SOUND_MaleNPC1Talkable);
        NoxDamage(NoxGetObject("Bugfix1"), s_rocke,1,0);
    }
    else
    {
        NoxSay(s_rocke,"SCumsRPG:        That isn't no Rocky!");
        PlaySoundAround(s_rocke, SOUND_MaleNPC1Talkable);
    }
}

void Quest1Info()
{
    NoxSay(s_gillian,
    "SCumsRPG:        Before heading into the dungeon, you have to prove your worth. Kill the spider poisoning our water supply and I'll give you a key to unlock the gate.");
    NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
}

void Quest1()
{
    if (!IsAliveSewerSpider())
    {
        NoxSay(s_gillian,"SCumsRPG:        Great job! Here's the key, as promised.");
        CreateObjectById(OBJ_SILVER_KEY, NoxGetWaypointX( s_gillianWaypoint), NoxGetWaypointY(s_gillianWaypoint));
        NoxPickup(OTHER, NoxGetObject("SILVERKEY"));
        NoxObjectOff(NoxGetObject("q1"));
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
        NoxPlayAudioByIndex(SOUND_FlagCapture, s_gillianWaypoint);
    }
    else
    {
        NoxSay(s_gillian,"SCumsRPG:        If you can't find where you need to be, check behind the tavern.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
    }
}

void Quest2Info()
{
    NoxSay(s_gillian,"SCumsRPG:        All I got was a bloody note about this quest. Maybe someone else could tell you more?");
    NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
}

void Quest2()
{
    if (!NoxCurrentHealth(s_butcher))
    {
        NoxSay(s_gillian,"SCumsRPG:        May their souls finally rest. I found this old key while cleaning. Maybe you can use it.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
        CreateObject(("GOLDKEY"), s_gillianWaypoint);
        NoxObjectOff(NoxGetObject("q2"));
        NoxPlayAudioByIndex(SOUND_FlagCapture, s_gillianWaypoint);
    }
    else
    {
        NoxSay(s_gillian,"SCumsRPG:        Surely this has something to do with the second floor of the dungeon...");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
    }
}

void Quest3Info()
{
    NoxSay(s_gillian,"SCumsRPG:        Our former King has risen from the grave, commanding an army of skeletons. Please, destroy his cursed form and end his eternal suffering.");
    NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
}

void Quest3()
{
    if (!IsAliveLeoric())
    {
        NoxSay(s_gillian,"SCumsRPG:        The curse of our King has passed, but I fear there's a greater evil at work. Please, take this.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
        NoxPickup(OTHER, NoxGetObject("Staff"));
        NoxObjectOff(NoxGetObject("q3"));
        NoxPlayAudioByIndex(SOUND_FlagCapture, s_gillianWaypoint);
    }
    else
    {
        NoxSay(s_gillian,"SCumsRPG:        I don't like to think about how the King died. His death seemed very wrong, somehow. I'd guess he's still in the crypts.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
    }
}

void Quest4Info()
{
    NoxSay(s_gillian,"SCumsRPG:        One of the town's mages, Zhar, went down into the dungeon a few weeks ago. Could you go find him?");
    NoxPlayAudioByIndex(
        SOUND_MaidenTalkable, s_gillianWaypoint);
}

void Quest4()
{
    if (NoxCurrentHealth(NoxGetObject("ZharMeet")) == s_ded)
    {
        NoxSay(s_gillian,"SCumsRPG:        You found Zhar? Being down there must've driven him mad to reject your help. Here's your reward.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
        NoxPickup(OTHER, NoxGetObject("Shirt"));
        NoxObjectOff(NoxGetObject("q4"));
        NoxPlayAudioByIndex(SOUND_FlagCapture, s_gillianWaypoint);
    }
    else
    {
        NoxSay(s_gillian,"SCumsRPG:        It should be hard to miss Zhar. He enjoys the flicker of blue flames.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
    }
}

void Quest5Info()
{
    NoxSay(s_gillian,"SCumsRPG:        The Witch feels a demonic presence down in the dungeon. The old Arch-Bishop may be down there calling for the Dark Lord's return.");
    NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
}

void Quest5()
{
    if (NoxCurrentHealth(s_diablo) == s_ded)
    {
        NoxSay(s_gillian,"SCumsRPG:        The demonic presence is gone, or so I've heard. May we all be safe tonight.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
        NoxPickup(OTHER, NoxGetObject("Pants"));
        NoxObjectOff(NoxGetObject("q5"));
        NoxPlayAudioByIndex(SOUND_FlagCapture,s_gillianWaypoint);
    }
    else
    {
        NoxSay(s_gillian,"SCumsRPG:        The Dark Lord may need a blood price to come out..");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint); 
    }
}

void Quest6Info()
{
    NoxSay(s_gillian,"SCumsRPG:        An Adventurer, not unlike yourself, came through here not too long ago. I wonder what happened to him.");
    NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
}

void Quest6()
{
    if (!NoxCurrentHealth(s_bossman))
    {
        NoxSay(s_gillian,"SCumsRPG:        The Adventurer was the Lord of the dungeon? I hope you don't succumb to the same fate.");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
        NoxPickup(OTHER, NoxGetObject("Staff2"));
        NoxObjectOff(NoxGetObject("q6"));
        NoxPlayAudioByIndex(SOUND_FlagCapture, s_gillianWaypoint); 
    }
    else
    {
        NoxSay(s_gillian,"SCumsRPG:        He was probably slain down there, like all the others..");
        NoxPlayAudioByIndex(SOUND_MaidenTalkable, s_gillianWaypoint);
    }
}

///Shrines///

void ForcefieldShrine()
{
    NoxEnchant(OTHER, ENCHANT_SHIELD, 120.0f);
    NoxSay(NoxGetObject("MShrine1"),"SCumsRPG:        While the spirit is vigilant the body thrives");
    NoxPlayAudioByIndex(SOUND_ShieldOn, NoxGetWaypoint("FFShrine"));
}

void VisionShrine()
{
    NoxEnchant(OTHER,ENCHANT_INFRAVISION,60.0f);
    NoxSay(NoxGetObject("MShrine2"),"SCumsRPG:        See those which are unseen");
    NoxPlayAudioByIndex(SOUND_InfravisionOn, NoxGetWaypoint("VisionShrine"));
}

void HasteShrine() 
{ 
    NoxEnchant(OTHER, ENCHANT_HASTED, 120.0f);
    NoxSay(NoxGetObject("MShrine3"),"SCumsRPG:        You feel more agile"); 
}

void ShockShrine()
{
    NoxEnchant(OTHER, ENCHANT_SHOCK, 120.0f);
    NoxSay(NoxGetObject("MShrine4"),"SCumsRPG:        Energy comes at the cost of wisdom");
}

///FloorFunctions///

static void Floor5Lasers1Off()
{
    NoxObjectOff(NoxGetObject("Laser1"));
    // NoxObjectOff(NoxGetObject("Laser2"));
}

void Floor5Lasers1()
{
    NoxObjectOn(NoxGetObject("Laser1"));
    // NoxObjectOn(NoxGetObject("Laser"));
    NoxSecondTimer(2, &Floor5Lasers1Off);
}

static int s_flr6laser[3];

static void Floor6Lasers1Off() 
{
    NoxObjectOff(s_flr6laser[0]);
    NoxObjectOff(s_flr6laser[1]);
    NoxObjectOff(s_flr6laser[2]);
}

void Floor6Lasers1() 
{ 
    NoxObjectOn(s_flr6laser[0]);
    NoxObjectOn(s_flr6laser[1]);
    NoxObjectOn(s_flr6laser[2]);
    NoxSecondTimer(1, &Floor6Lasers1Off); 
}

static void initFloor6Lasers()
{
    s_flr6laser[0] = NoxGetObject("F6L1");
    s_flr6laser[1] = NoxGetObject("F6L2");
    s_flr6laser[2] = NoxGetObject("F6L3");
}

void Floor7Fist()
{
    NoxCastSpellObjectObject(SPELL_FIST, SELF, SELF);
}

void Block()
{
    NoxUnlockDoor(NoxGetObject("Floor6Door1"));
    NoxUnlockDoor(NoxGetObject("Floor6Door2"));
}

void CaveDoors()
{
    NoxUnlockDoor(NoxGetObject("Floor9Door1"));
    NoxUnlockDoor(NoxGetObject("Floor9Door2"));
}

///Bosses///

void Butcher1()
{
    NoxSay(SELF,"SCumsRPG:        Ah... Fresh meat!");
    NoxEnchant(SELF, ENCHANT_HASTED, 10.00f);
    
}

void Butcher2()
{
    NoxDestroyChat(SELF);
}

void ArchBishop1()
{
    NoxSay(s_archBishop,"SCumsRPG:        Abandon your foolish quest! All that awaits you is the wrath of my master!");

}

void ArchBishop2()
{
    int wayp=NoxGetWaypoint("DiabloB");
    float xpos = NoxGetWaypointX(wayp), ypos = NoxGetWaypointY(wayp);

    NoxEnchant(CreateObjectById(OBJ_WIZARD_GREEN,xpos, ypos),ENCHANT_ANCHORED, 0);
    NoxEnchant(CreateObjectById(OBJ_WIZARD_GREEN,xpos, ypos),ENCHANT_ANCHORED,0);
}

int ArchBishop3()
{
    NoxMoveObject(s_archBishop,4365.0f,1006.0f);
    NoxSay(s_archBishop,"SCumsRPG:        Argh! The blood price.. has been paid.");
    NoxMoveObject(s_diablo,4223.0f,1176.0f);
    NoxDamage(s_archBishop, NoxGetObject("Cain"),250,12);
}

void Diablo1()
{
    NoxSay(s_diablo,"SCumsRPG:        !laem yreve retfa hsurb dna ,selbategev ruoy taE");
}

void Diablo2()
{
    CreateObject("EMBERDEMON",NoxGetWaypoint("DiabloS1"));
    CreateObject("EMBERDEMON",NoxGetWaypoint("DiabloS2"));
    CreateObject("EMBERDEMON",NoxGetWaypoint("DiabloS1"));
    CreateObject("EMBERDEMON",NoxGetWaypoint("DiabloS2"));    
    if (!NoxCurrentHealth(s_diablo))
    {
        NoxObjectOn(NoxGetObject("DiabloElevator1"));
        NoxObjectOn(NoxGetObject("DiabloTeleport"));
    }
}

void BossMan1()
{
    static char *greet[] = {
        "SCumsRPG:        I'm the lord of the this dungeon, and you can't have it!",
        "SCumsRPG:        I used to be like you! The treasure lures all!",
        "SCumsRPG:        Gillian put you up to this, didn't she!?",
        "SCumsRPG:        Frogs are murdered every time you talk to someone!",
        "SCumsRPG:        The Brawler is everywhere!",
    };
    int br = NoxRandomInteger(0,5);

    if (br)
        NoxSay(s_bossman, greet[br-1]);
}

void BossMan2()
{
    NoxSay(s_bossman,"SCumsRPG:        There's still one more.. that waits.");
    NoxObjectOn(NoxGetObject("TownPortal"));
}

void Bosss()
{
    NoxObjectOn(NoxGetObject("HiddenTele2"));
}

void Godmodeoff()
{
    NoxEnchantOff(OTHER, ENCHANT_INVULNERABLE);
}





///Dungeon of The Endless///

#define MY_WAYPOINT_DOTES1 59
#define MY_WAYPOINT_DOTES2 60
#define MY_WAYPOINT_DOTES2R 65
#define MY_WAYPOINT_DOTES3 61
#define MY_WAYPOINT_DOTES3L 63
#define MY_WAYPOINT_DOTES3R 64
#define MY_WAYPOINT_DOTES4 62
#define MY_WAYPOINT_DOTES41 70
#define MY_WAYPOINT_DOTES4L 67
#define MY_WAYPOINT_DOTES4R 66

void Anchor()
{
    NoxEnchant(OTHER, ENCHANT_ANCHORED,0);
}

void AttackPlayer()
{
    NoxAttack(SELF, NoxGetObject("Crystal"));
}

static void doteShrine1(int host, int caller)
{
    NoxEnchant(caller, ENCHANT_LIGHT, 60.0f);
    NoxSay(host, "SCumsRPG:        Brighter than the sun");
}

static void doteShrine2(int host, int caller)
{
    NoxEnchant(caller,ENCHANT_INVULNERABLE,0.0f);
    NoxSay(host,"SCumsRPG:        Degreelessness Mode = ON");
}

static void doteShrine3(int host, int caller)
{
    NoxEnchant(caller,ENCHANT_ANTI_MAGIC,10.0f);
    NoxSay(host,"SCumsRPG:        . . .  . . .  . . .  . . .");
}

#define MY_WAYPOINT_DOTES4R 66

static void doteShrine4(int host, int caller)
{
    CreateObjectById(OBJ_SMALL_STONE_BLOCK, NoxGetWaypointX(MY_WAYPOINT_DOTES4R), NoxGetWaypointY(MY_WAYPOINT_DOTES4R));
    NoxSay(host,"SCumsRPG:        more power");
    NoxPlayAudioByIndex(SOUND_FlagCapture, MY_WAYPOINT_DOTES4R);
    NoxObjectOff(NoxGetObject("DOTEShrineB"));
}

static void doteShrine5(int host, int caller)
{
    CreateObjectById(OBJ_OBLIVION_ORB, NoxGetWaypointX(MY_WAYPOINT_DOTES4R), NoxGetWaypointY(MY_WAYPOINT_DOTES4R));
    NoxSay(host,"SCumsRPG:        Necromancers love this thing");
}

static void doteShrine6(int host, int caller)
{
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER,
        NoxGetWaypointX(MY_WAYPOINT_DOTES4R),NoxGetWaypointY(MY_WAYPOINT_DOTES4R),
        NoxGetWaypointX(MY_WAYPOINT_DOTES4R),NoxGetWaypointY(MY_WAYPOINT_DOTES4R));
    NoxSay(host,"SCumsRPG:        Oops, I didn't mean to do that");
}

static void doteShrine7(int host, int caller)
{
    NoxEnchant(caller, ENCHANT_FREEZE, 5.0f);
    NoxSay(host, "SCumsRPG:        Your hand turns to stone");
}

static void doteShrine8(int host, int caller)
{
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER,
        NoxGetWaypointX(MY_WAYPOINT_DOTES4R),NoxGetWaypointY(MY_WAYPOINT_DOTES4R),
        NoxGetWaypointX(MY_WAYPOINT_DOTES4R),NoxGetWaypointY(MY_WAYPOINT_DOTES4R));
    NoxSay(host,"SCumsRPG:        sorry");
}

typedef void(*shrine_function)(int, int);

void DOTEShrine()
{
    static shrine_function actions[]={
        doteShrine1, doteShrine2, doteShrine3, doteShrine4,
        doteShrine5, doteShrine6, doteShrine7, doteShrine8
    };
    actions[NoxRandomInteger(0, 7)](NoxGetObject("DOTEShrine"), NoxGetCaller());
}

///DOTEPower//


typedef struct _dotePower
{
    int lits[4];
    int wayp;
} DotePower;

#define DECLARE_DOTE(num) \
    static DotePower s_powerDote##num;

DECLARE_DOTE(1)
DECLARE_DOTE(2)
DECLARE_DOTE(2R)
DECLARE_DOTE(3)
DECLARE_DOTE(3R)
DECLARE_DOTE(3L)
DECLARE_DOTE(4)
DECLARE_DOTE(4R)
DECLARE_DOTE(4L)

#define DO_INIT_DOTE(num) \
  s_powerDote##num.lits[0] = NoxGetObject("DOTE"#num"On"); \
  s_powerDote##num.lits[1] = NoxGetObject("DOTE"#num"On2"); \
  s_powerDote##num.lits[2] = NoxGetObject("DOTE"#num"On3"); \
  s_powerDote##num.lits[3] = NoxGetObject("DOTE"#num"On4"); \
  s_powerDote##num.wayp = NoxGetWaypoint("DOTES"#num); \

static void initDoteFacilities()
{
    DO_INIT_DOTE(1)
    DO_INIT_DOTE(2)
    DO_INIT_DOTE(2R)
    DO_INIT_DOTE(3)
    DO_INIT_DOTE(3R)
    DO_INIT_DOTE(3L)
    DO_INIT_DOTE(4)
    DO_INIT_DOTE(4R)
    DO_INIT_DOTE(4L)
}

static doDoteTurnOn(DotePower *dotepw)
{
    NoxObjectOn( dotepw->lits[0]);
    NoxObjectOn( dotepw->lits[1]);
    NoxObjectOn( dotepw->lits[2]);
    NoxObjectOn( dotepw->lits[3]);
    NoxWaypointOff(dotepw->wayp);
}

static doDoteTurnOff(DotePower *dotepw)
{
    NoxObjectOff( dotepw->lits[0]);
    NoxObjectOff( dotepw->lits[1]);
    NoxObjectOff( dotepw->lits[2]);
    NoxObjectOff( dotepw->lits[3]);
    NoxWaypointOn(dotepw->wayp);
}

void DOTE1On()
{
    doDoteTurnOn(&s_powerDote1);
}

void DOTE1Off()
{
    doDoteTurnOff(&s_powerDote1);   
}

void DOTE2On()
{
    doDoteTurnOn(&s_powerDote2);
}

void DOTE2Off()
{
    doDoteTurnOff(&s_powerDote2);
}

void DOTE2ROn()
{
    doDoteTurnOn(&s_powerDote2R);
}

void DOTE2ROff()
{
    doDoteTurnOff(&s_powerDote2R);
}

void DOTE3On()
{
    doDoteTurnOn(&s_powerDote3);
}

void DOTE3Off()
{
    doDoteTurnOff(&s_powerDote3);
}

void DOTE3ROn()
{
    doDoteTurnOn(&s_powerDote3R);
}

void DOTE3ROff()
{
    doDoteTurnOff(&s_powerDote3R);
}

void DOTE3LOn()
{
    doDoteTurnOn(&s_powerDote3L);
}

void DOTE3LOff()
{
    doDoteTurnOff(&s_powerDote3L);
}

void DOTE4On()
{
    // NoxObjectOn(Object("DOTE4On"));
    // NoxObjectOn(Object("DOTE4On2"));
    // NoxObjectOn(Object("DOTE4On3"));
    // NoxObjectOn(Object("DOTE4On4"));
    // NoxWayPointOff(Waypoint("DOTES41"));
    doDoteTurnOn(&s_powerDote4);
}

void DOTE4Off()
{
    // NoxObjectOff(Object("DOTE4On"));
    // NoxObjectOff(Object("DOTE4On2"));
    // NoxObjectOff(Object("DOTE4On3"));
    // NoxObjectOff(Object("DOTE4On4"));
    // NoxWayPointOn(Waypoint("DOTES41"));
    doDoteTurnOff(&s_powerDote4);
}

void DOTE4ROn()
{
    doDoteTurnOn(&s_powerDote4R);
}

void DOTE4ROff()
{
    doDoteTurnOff(&s_powerDote4R);
}

void DOTE4LOn()
{
    doDoteTurnOn(&s_powerDote4L);
}

void DOTE4LOff()
{
    doDoteTurnOff(&s_powerDote4L);
}

///DOTE Doors + Spawns///

static int DungeonOfTheEndless();

void DOTEDoors()
{
    NoxUnlockDoor(NoxGetObject("DOTEDoor1a"));
    NoxUnlockDoor(NoxGetObject("DOTEDoor1b"));
    NoxWaypointOn(NoxGetWaypoint("DOTES1"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTESwitch"));
} 


void DOTE1Doors()
{
    NoxUnlockDoor(NoxGetObject("DOTE1Door1a"));
    NoxUnlockDoor(NoxGetObject("DOTE1Door1b"));
    NoxWaypointOn(NoxGetWaypoint("DOTES2"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE1Switch"));
}

void DOTE2Doors()
{
    if (NoxIsTrigger(NoxGetObject("DOTE2SwitchM")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE3On")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3"));
        }
    }
    NoxUnlockDoor(NoxGetObject("DOTE2Door1a"));
    NoxUnlockDoor(NoxGetObject("DOTE2Door1b"));
    NoxUnlockDoor(NoxGetObject("DOTE2Door1c"));
    NoxUnlockDoor(NoxGetObject("DOTE2Door1d"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE2SwitchM"));
    NoxObjectOff(NoxGetObject("DOTE2SwitchMb"));
}

void DOTE2DoorsR()
{
    if (NoxIsTrigger(NoxGetObject("DOTE2SwitchR")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE2ROn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES2R"));
        }
    }
    if (NoxIsTrigger(NoxGetObject("DOTE2SwitchRb")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE2On")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES2"));
        }
    }

    NoxUnlockDoor(NoxGetObject("DOTE2Door2a"));
    NoxUnlockDoor(NoxGetObject("DOTE2Door2b"));
    NoxUnlockDoor(NoxGetObject("DOTE2Door2c"));
    NoxUnlockDoor(NoxGetObject("DOTE2Door2d"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE2SwitchR"));
    NoxObjectOff(NoxGetObject("DOTE2SwitchRb"));
}

void DOTE2RDoors()
{
    if (NoxIsTrigger(NoxGetObject("DOTE2RSwitch")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE3ROn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3R"));
        }
    }
    if (NoxIsTrigger(NoxGetObject("DOTE3RSwitch")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE2ROn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES2R"));
        }
    }
    NoxUnlockDoor(NoxGetObject("DOTE2RDoor1a"));
    NoxUnlockDoor(NoxGetObject("DOTE2RDoor1b"));
    NoxUnlockDoor(NoxGetObject("DOTE3RDoor1a"));
    NoxUnlockDoor(NoxGetObject("DOTE3RDoor1b"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE3RSwitch"));
    NoxObjectOff(NoxGetObject("DOTE2RSwitch"));
}

void DOTE3Doors1()
{
    if (NoxIsTrigger(NoxGetObject("DOTE3SwitchM")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE4On")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES41"));
        }
    }
    if (NoxIsTrigger(NoxGetObject("DOTE3SwitchMb")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE3On")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3"));
        }
    }    
    NoxUnlockDoor(NoxGetObject("DOTE3Door1a"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door1b"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door1c"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door1d"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE3SwitchM"));
    NoxObjectOff(NoxGetObject("DOTE3SwitchMb"));
}

void DOTE3Doors2()
{
    if (NoxIsTrigger(NoxGetObject("DOTE3SwitchR")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE3ROn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3R"));
        }
    }
    if (NoxIsTrigger(NoxGetObject("DOTE3SwitchRb")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE3On")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3"));
        }
    }

    NoxUnlockDoor(NoxGetObject("DOTE3Door2a"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door2b"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door2c"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door2d"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE3SwitchR"));
    NoxObjectOff(NoxGetObject("DOTE3SwitchRb"));
}

void DOTE3Doors3()
{
    if (NoxIsTrigger(NoxGetObject("DOTE3SwitchL")))
    {
         if (!NoxIsObjectOn(NoxGetObject("DOTE3LOn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3L"));
        }
    }
    if (NoxIsTrigger(NoxGetObject("DOTE3SwitchLb")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE3On")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3"));
        }
    }
    
    NoxUnlockDoor(NoxGetObject("DOTE3Door3a"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door3b"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door3c"));
    NoxUnlockDoor(NoxGetObject("DOTE3Door3d"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE3SwitchL"));
    NoxObjectOff(NoxGetObject("DOTE3SwitchLb"));
}

void DOTE3LDoors()
{
    if (NoxIsTrigger(NoxGetObject("DOTE3LSwitch")))
    {
         if (!NoxIsObjectOn(NoxGetObject("DOTE4LOn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES4L"));
        }
    }
    if (NoxIsTrigger(NoxGetObject("DOTE4LSwitchB")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE3LOn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES3L"));
        }
    }

    NoxUnlockDoor(NoxGetObject("DOTE3LDoor1a"));
    NoxUnlockDoor(NoxGetObject("DOTE3LDoor1b"));
    NoxUnlockDoor(NoxGetObject("DOTE4LDoor2a"));
    NoxUnlockDoor(NoxGetObject("DOTE4LDoor2b"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE3LSwitch"));
    NoxObjectOff(NoxGetObject("DOTE4LSwitchB"));
}

void DOTE4LDoors()
{
    if (NoxIsTrigger(NoxGetObject("DOTE4SwitchL")))
    {
         if (!NoxIsObjectOn(NoxGetObject("DOTE4LOn")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES4L"));
        }
    }
    if (NoxIsTrigger(NoxGetObject("DOTE4LSwitchR")))
    {
        if (!NoxIsObjectOn(NoxGetObject("DOTE4On")))
        {
            NoxWaypointOn(NoxGetWaypoint("DOTES41"));
        }
    }

    NoxUnlockDoor(NoxGetObject("DOTE4Door3a"));
    NoxUnlockDoor(NoxGetObject("DOTE4Door3b"));
    NoxUnlockDoor(NoxGetObject("DOTE4Door3c"));
    NoxUnlockDoor(NoxGetObject("DOTE4Door3d"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE4SwitchL"));
    NoxObjectOff(NoxGetObject("DOTE4LSwitchR"));
}

void DOTE4Doors2()
{
    NoxUnlockDoor(NoxGetObject("DOTE4Door2a"));
    NoxUnlockDoor(NoxGetObject("DOTE4Door2b"));
    DungeonOfTheEndless();
    NoxObjectOff(NoxGetObject("DOTE4SwitchR"));
}

static void putDungeonMob(int ty, int count, int location)
{
    float xpos = NoxGetWaypointX(location), ypos = NoxGetWaypointY(location);

    while (--count >= 0)
        NoxSetOwner(s_dungeonMaster, CreateObjectById(ty, xpos, ypos));
}

static int DungeonOfTheEndless()
{
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES1))
        putDungeonMob(OBJ_IMP, 3, MY_WAYPOINT_DOTES1);
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES2))
        putDungeonMob(OBJ_SHADE, 3, MY_WAYPOINT_DOTES2);
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES2R))
        putDungeonMob(OBJ_FLYING_GOLEM, 3, MY_WAYPOINT_DOTES2R);
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES3))
        putDungeonMob(OBJ_SCORPION, 3, MY_WAYPOINT_DOTES3);
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES3L))
    {
        NoxSetOwner(s_dungeonMaster, CreateMimic(NoxGetWaypointX(MY_WAYPOINT_DOTES3L), NoxGetWaypointY(MY_WAYPOINT_DOTES3L)));
        NoxSetOwner(s_dungeonMaster, CreateMimic(NoxGetWaypointX(MY_WAYPOINT_DOTES3L), NoxGetWaypointY(MY_WAYPOINT_DOTES3L)));
    }
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES3R))
        putDungeonMob(OBJ_WHITE_WOLF, 3, MY_WAYPOINT_DOTES3R);
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES41))
        putDungeonMob(OBJ_WILL_O_WISP,3, MY_WAYPOINT_DOTES41);
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES4L))
        putDungeonMob(OBJ_MECHANICAL_GOLEM, 1, MY_WAYPOINT_DOTES4L);
    if (NoxIsWaypointOn(MY_WAYPOINT_DOTES4R))
        putDungeonMob(OBJ_STONE_GOLEM, 2, MY_WAYPOINT_DOTES4R);
    AttackPlayer();
}

void DOTEClear()
{
    #define WAYPOINT_XYXY(num) NoxGetWaypointX(num), NoxGetWaypointY(num), NoxGetWaypointX(num), NoxGetWaypointY(num)
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER, WAYPOINT_XYXY(MY_WAYPOINT_DOTES1));
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER, WAYPOINT_XYXY(MY_WAYPOINT_DOTES2));
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER,WAYPOINT_XYXY(MY_WAYPOINT_DOTES2R));
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER, WAYPOINT_XYXY(MY_WAYPOINT_DOTES3));
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER, WAYPOINT_XYXY(MY_WAYPOINT_DOTES3R));
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER, WAYPOINT_XYXY(MY_WAYPOINT_DOTES3L));
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER, WAYPOINT_XYXY(MY_WAYPOINT_DOTES4)); //maybe its an error.
    NoxCastSpellLocationLocation(SPELL_METEOR_SHOWER, WAYPOINT_XYXY(MY_WAYPOINT_DOTES4L));
    NoxObjectOff(NoxGetObject("DOTEClear"));
}

void KILL()
{
    NoxDamage(OTHER, NoxGetObject("KILL"),9999,13);
}

#define MY_WAYPOINT_ENDMEPlEASE 83

static int s_germList[]={OBJ_DIAMOND, OBJ_RUBY, OBJ_EMERALD};

static void placeGerm(int count)
{
    float xpos = NoxGetWaypointX(MY_WAYPOINT_ENDMEPlEASE), ypos = NoxGetWaypointY(MY_WAYPOINT_ENDMEPlEASE);

    if (count >= 0)
    {
        CreateObjectById(s_germList[NoxRandomInteger(0, 2)], xpos, ypos);
        NoxFrameTimerWithArg(1, count - 1, &placeGerm);
    }
}

void DONOTPRESS()
{
    placeGerm(50);   
}

#define MY_WAYPOINT_FROG_WP 104

void NativeInit()
{
    s_dungeonMaster= CreateObjectById(OBJ_HECUBAH, 100.0f, 100.0f);
    NoxFrozen(s_dungeonMaster, TRUE);
    s_brawler = NoxGetObject("TheBrawler");
    s_pepin = NoxGetObject("Pepin");
    s_cain = NoxGetObject("Cain");
    s_gillian = NoxGetObject("Gillian");
    s_gillianWaypoint = NoxGetWaypoint("Gillian");
    s_kaelRills = NoxGetObject("Kael Rills");
    s_kaelRillsWaypoint = NoxGetWaypoint("Kael Rills");
    s_wirt = NoxGetObject("Wirt");
    s_wirtWaypoint = NoxGetWaypoint("Wirt");
    s_zhar = NoxGetObject("Zhar");
    s_archBishop = NoxGetObject("ArchBishop");
    s_diablo = NoxGetObject("Diablo");
    s_bossman = NoxGetObject("BossMan");
    s_josh = NoxGetObject("Josh");
    s_lachdanan = NoxGetObject("Lachdanan");
    s_farnham = NoxGetObject("Farnham");
    s_capedBaldy = NoxGetObject("CapedBaldy");
    s_capedBaldyT = NoxGetObject("CapedBaldyT");
    s_thefrog = NoxGetObject("TheFrog");
    s_therat = NoxGetObject("TheRat");
    s_frogBuddy = CreateObjectById(OBJ_GREEN_FROG, NoxGetWaypointX(MY_WAYPOINT_FROG_WP), NoxGetWaypointY(MY_WAYPOINT_FROG_WP));
    NoxLookWithAngle(s_frogBuddy, 96);
    NoxObjectOff(s_frogBuddy);
    s_queen = NoxGetObject("Queen");
    s_waypointCapedBlady = NoxGetWaypoint("CapedBaldy");
    s_rocke = NoxGetObject("Rocke");
    s_adria = NoxGetObject("Adria");
    s_butcher = NoxGetObject("Butcher");

    NoxMakeFriendly(s_cain);
    NoxMakeFriendly(s_pepin);
    NoxMakeFriendly(s_gillian);
    NoxMakeFriendly(s_kaelRills);
    NoxMakeFriendly(s_wirt);
    NoxMakeFriendly(s_zhar);
    NoxMakeFriendly(s_brawler);
    NoxMakeFriendly(s_josh);
    NoxMakeFriendly(s_lachdanan);
    NoxMakeFriendly(s_farnham);
    NoxMakeFriendly(s_capedBaldyT);
    initNPCTalk();
    initFloor6Lasers();
    OnInitialMonster();
}
