
#include "monsterSetting.h"
#include "monBinscr.h"
#include "../include/monster_action.h"
#include "../include/fxeffect.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

///Monsters///

#define MY_WAYPOINT_SEWER_S1 12
#define MY_WAYPOINT_SEWER_S2 14
#define MY_WAYPOINT_SEWER_S3 15

#define TO_WAYPOINT_XY(wayId) NoxGetWaypointX(wayId), NoxGetWaypointY(wayId)

static int s_sewerSpider;

void SewerMonsters()
{
    static int ids[]={OBJ_GIANT_LEECH, OBJ_BAT};

    CreateObjectById(ids[NoxRandomInteger(0, 1)], TO_WAYPOINT_XY(MY_WAYPOINT_SEWER_S1));
    CreateObjectById(ids[NoxRandomInteger(0, 1)], TO_WAYPOINT_XY(MY_WAYPOINT_SEWER_S2));
    CreateObjectById(ids[NoxRandomInteger(0, 1)], TO_WAYPOINT_XY(MY_WAYPOINT_SEWER_S3));
    CreateObjectById(ids[NoxRandomInteger(0, 1)], TO_WAYPOINT_XY(MY_WAYPOINT_SEWER_S1));
    CreateObjectById(ids[NoxRandomInteger(0, 1)], TO_WAYPOINT_XY(MY_WAYPOINT_SEWER_S2));
    CreateObjectById(ids[NoxRandomInteger(0, 1)], TO_WAYPOINT_XY(MY_WAYPOINT_SEWER_S3));
    NoxObjectOff(SELF);
}

#define MY_WAYPOINT_F1S1 8
#define MY_WAYPOINT_F1S2 13
#define MY_WAYPOINT_F1B 11

void MonstersFloor1()
{
    CreateObjectById(OBJ_IMP, TO_WAYPOINT_XY(MY_WAYPOINT_F1S1));
    CreateObjectById(OBJ_IMP, TO_WAYPOINT_XY(MY_WAYPOINT_F1S1));
    CreateObjectById(OBJ_IMP, TO_WAYPOINT_XY(MY_WAYPOINT_F1S1));
    
    CreateObjectById(OBJ_SMALL_SPIDER, TO_WAYPOINT_XY(MY_WAYPOINT_F1S2));
    CreateObjectById(OBJ_SMALL_SPIDER, TO_WAYPOINT_XY(MY_WAYPOINT_F1S2));
    CreateObjectById(OBJ_SMALL_SPIDER, TO_WAYPOINT_XY(MY_WAYPOINT_F1S2));    
    SetUnitMaxHealth(CreateObjectById(OBJ_TROLL, TO_WAYPOINT_XY(MY_WAYPOINT_F1B)), 325);
    NoxObjectOff(SELF);
}

#define MY_WAYPOINT_F2S1 9
#define MY_WAYPOINT_F2S2 10

void MonsterFloor2()
{
    CreateObjectById(OBJ_IMP, TO_WAYPOINT_XY(MY_WAYPOINT_F2S1));
    CreateObjectById(OBJ_IMP, TO_WAYPOINT_XY(MY_WAYPOINT_F2S1));
    SetUnitMaxHealth( CreateObjectById(OBJ_SPITTING_SPIDER, TO_WAYPOINT_XY(MY_WAYPOINT_F2S2)), 180);
    CreateObjectById(OBJ_SMALL_SPIDER, TO_WAYPOINT_XY(MY_WAYPOINT_F2S2));
    CreateObjectById(OBJ_SMALL_SPIDER, TO_WAYPOINT_XY(MY_WAYPOINT_F2S2));
    NoxObjectOff(SELF);
}

#define MY_WAYPOINT_F3S1 16
#define MY_WAYPOINT_F3S2 17
#define MY_WAYPOINT_F3S3 19
#define MY_WAYPOINT_F3B 18

#define MOB_ZOMBIE_HP 180
#define MOB_SKELETON_HP 225
#define MOB_VILE_ZOMBIE_HP 241
#define MOB_GARGOYLE_HP 98
#define MOB_SHADE_HP 136

void MonsterFloor3()
{
    NoxLookWithAngle(CreateObjectById(OBJ_GHOST, TO_WAYPOINT_XY(85)), 225);
    NoxLookWithAngle(CreateObjectById(OBJ_GHOST, TO_WAYPOINT_XY(86)), 92);
    CreateObjectById(OBJ_GHOST, TO_WAYPOINT_XY(87));
    SetUnitMaxHealth( CreateObjectById(OBJ_ZOMBIE, TO_WAYPOINT_XY(MY_WAYPOINT_F3S1)), MOB_ZOMBIE_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ZOMBIE, TO_WAYPOINT_XY(MY_WAYPOINT_F3S1)), MOB_ZOMBIE_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(MY_WAYPOINT_F3S2)), MOB_SKELETON_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(MY_WAYPOINT_F3S2)), MOB_SKELETON_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ZOMBIE, TO_WAYPOINT_XY(MY_WAYPOINT_F3S3)), MOB_ZOMBIE_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(MY_WAYPOINT_F3S3)), MOB_SKELETON_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_VILE_ZOMBIE, TO_WAYPOINT_XY(MY_WAYPOINT_F3B)), MOB_VILE_ZOMBIE_HP);
    NoxObjectOff(SELF);
}

static int s_leoric;

static void onLeoricDeath()
{
    NoxDestroyChat(SELF);
    NoxObjectOn(NoxGetObject("Ch2"));
}

static void onLeoricSpotted()
{
    NoxSay(SELF,
        "SCumsRPG:        The warmth of life has entered my tomb. Prepare yourself, mortal, to serve my master for eternity!");
    PlaySoundAround(SELF, SOUND_DemonDie);
}

#define MY_WAYPOINT_F4S1  20
#define MY_WAYPOINT_F4S2  21
#define MY_WAYPOINT_F4S3  22
#define MY_WAYPOINT_F4S4  23

static int revealSkull(float xpos, float ypos)
{
    int mob = CreateObjectById(OBJ_SKELETON, xpos, ypos);

    NoxPlayFX(FX_TELEPORT, xpos, ypos, 0,0);
    PlaySoundAround(mob, SOUND_TeleportIn);
    return mob;
}

static void onLeoricActionChanged()
{
    NoxLookAtObject( revealSkull(TO_WAYPOINT_XY(MY_WAYPOINT_F4S1)), SELF);
    NoxLookAtObject( revealSkull(TO_WAYPOINT_XY(MY_WAYPOINT_F4S2)), SELF);
    NoxLookAtObject( revealSkull(TO_WAYPOINT_XY(MY_WAYPOINT_F4S3)), SELF);
    NoxLookAtObject( revealSkull(TO_WAYPOINT_XY(MY_WAYPOINT_F4S4)), SELF);
}

static int createLeoric(float xpos, float ypos)
{
    int mob=CreateObjectById(OBJ_SKELETON_LORD, xpos, ypos);

    NoxEnchant(mob, ENCHANT_CROWN, 100.0f);
    NoxEnchant(mob, ENCHANT_RUN, 0);
    NoxSetCallback(mob, UnitCallback_OnDeath, &onLeoricDeath);
    NoxSetCallback(mob, UnitCallback_EnemySighted, &onLeoricSpotted);
    NoxSetCallback(mob, UnitCallback_OnFocusChanged, &onLeoricActionChanged);
    SetUnitMaxHealth(mob, 431);
    return mob;
}

int IsAliveLeoric()
{
    return NoxCurrentHealth(s_leoric) != 0;
}

void MonsterFloor4()
{
    SetUnitMaxHealth(CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(89)), MOB_SKELETON_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_ZOMBIE, TO_WAYPOINT_XY(91)), MOB_ZOMBIE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(90)), MOB_SKELETON_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(93)), MOB_SKELETON_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(95)), MOB_SKELETON_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SKELETON, TO_WAYPOINT_XY(98)), MOB_SKELETON_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_ZOMBIE, TO_WAYPOINT_XY(94)), MOB_ZOMBIE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_ZOMBIE, TO_WAYPOINT_XY(96)), MOB_ZOMBIE_HP);
    CreateObjectById(OBJ_GHOST, TO_WAYPOINT_XY(97));
    CreateObjectById(OBJ_GHOST, TO_WAYPOINT_XY(92));
    NoxObjectOff(SELF);
}

static void deferredMimicAction(int mi)
{
    DO_CREATURE_ACTION_ESCORT(mi, mi);
    NoxAggressionLevel(mi, 1.0f);
}

int CreateMimic(float xpos, float ypos)
{
    int mon = CreateObjectById(OBJ_MIMIC, xpos, ypos);

    SetUnitMaxHealth(mon, 600);
    NoxFrameTimerWithArg(1, mon, &deferredMimicAction);
    return mon;
}

#define MY_WAYPOINT_F5S1 24
#define MY_WAYPOINT_F5S2 25

void MonsterFloor5()
{
    CreateMimic(TO_WAYPOINT_XY(MY_WAYPOINT_F5S1));
    CreateMimic(TO_WAYPOINT_XY(MY_WAYPOINT_F5S2));
    CreateMimic(TO_WAYPOINT_XY(100));
    CreateGoon(TO_WAYPOINT_XY(99));
    CreateGoon(TO_WAYPOINT_XY(101));
    CreateGoon(TO_WAYPOINT_XY(102));
    NoxObjectOff(SELF);
}

#define MY_WAYPOINT_F7S1 28
#define MY_WAYPOINT_F7S2 29
#define MY_WAYPOINT_F7S4 31
#define MY_WAYPOINT_F7S5 103
#define MY_WAYPOINT_F7S3 30
#define MY_WAYPOINT_F7S6 32
#define MY_WAYPOINT_F7B 33

void MonsterFloor7()
{
    CreateLich(TO_WAYPOINT_XY(MY_WAYPOINT_F7S1));
    SetUnitMaxHealth(CreateObjectById(OBJ_EVIL_CHERUB, TO_WAYPOINT_XY(MY_WAYPOINT_F7S1)), MOB_GARGOYLE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_EVIL_CHERUB, TO_WAYPOINT_XY(MY_WAYPOINT_F7S1)), MOB_GARGOYLE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_EVIL_CHERUB, TO_WAYPOINT_XY(MY_WAYPOINT_F7S1)), MOB_GARGOYLE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SHADE, TO_WAYPOINT_XY(MY_WAYPOINT_F7S2)), MOB_SHADE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SHADE, TO_WAYPOINT_XY(MY_WAYPOINT_F7S2)), MOB_SHADE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SHADE, TO_WAYPOINT_XY(MY_WAYPOINT_F7S2)), MOB_SHADE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SHADE, TO_WAYPOINT_XY(MY_WAYPOINT_F7S2)), MOB_SHADE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_SHADE, TO_WAYPOINT_XY(MY_WAYPOINT_F7S2)), MOB_SHADE_HP);
    CreateLich(TO_WAYPOINT_XY(MY_WAYPOINT_F7S4));
    CreateLich(TO_WAYPOINT_XY(MY_WAYPOINT_F7S5));
    
    SetUnitMaxHealth(CreateObjectById(OBJ_EVIL_CHERUB, TO_WAYPOINT_XY(MY_WAYPOINT_F7S3)), MOB_GARGOYLE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_EVIL_CHERUB, TO_WAYPOINT_XY(MY_WAYPOINT_F7S3)), MOB_GARGOYLE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_EVIL_CHERUB, TO_WAYPOINT_XY(MY_WAYPOINT_F7S6)), MOB_GARGOYLE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_EVIL_CHERUB, TO_WAYPOINT_XY(MY_WAYPOINT_F7S6)), MOB_GARGOYLE_HP);
    SetUnitMaxHealth(CreateObjectById(OBJ_MECHANICAL_GOLEM, TO_WAYPOINT_XY(MY_WAYPOINT_F7B)), 900);
    NoxObjectOff(SELF);
}

#define MOB_SWORDMAN_HP 325
#define MOB_ARCHER_HP 125

#define MY_WAYPOINT_F8S1 34
#define MY_WAYPOINT_F8S2 35
#define MY_WAYPOINT_F8S3 37
#define MY_WAYPOINT_F8B 36
#define MY_WAYPOINT_SU1 38

void MonsterFloor8()
{
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S1)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S1)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S1)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S1)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S1)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S1)), MOB_ARCHER_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S1)), MOB_ARCHER_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S2)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S2)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S2)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S2)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S2)), MOB_ARCHER_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S2)), MOB_ARCHER_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S2)), MOB_ARCHER_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S3)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_SWORDSMAN, TO_WAYPOINT_XY(MY_WAYPOINT_F8S3)), MOB_SWORDMAN_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S3)), MOB_ARCHER_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S3)), MOB_ARCHER_HP);
    SetUnitMaxHealth( CreateObjectById(OBJ_ARCHER, TO_WAYPOINT_XY(MY_WAYPOINT_F8S3)), MOB_ARCHER_HP);
    CreateMystic(TO_WAYPOINT_XY(MY_WAYPOINT_F8B));
    CreateMystic(TO_WAYPOINT_XY(MY_WAYPOINT_SU1));
    NoxObjectOff(SELF);
}

static int s_floor9Monsters[]={
    OBJ_GRUNT_AXE, OBJ_OGRE_BRUTE, OBJ_WHITE_WOLF, OBJ_BEAR,
    OBJ_TROLL, OBJ_BLACK_WOLF, OBJ_BLACK_BEAR, OBJ_OGRE_WARLORD,
};
static int s_floor9MobHp[]={
    275, 325, 225, 425,
    325, 290, 380, 375
};
static int s_floor9Length=sizeof(s_floor9Monsters)/sizeof(s_floor9Monsters[0]);

static void spawnCaveMonsters(int count, float xpos, float ypos)
{
    while (--count >= 0)
    {
        int pic = NoxRandomInteger(0, s_floor9Length-1);

        SetUnitMaxHealth(CreateObjectById(s_floor9Monsters[pic], xpos, ypos), s_floor9MobHp[pic]);
    }
}

#define MY_WAYPOINT_F9S1 39
#define MY_WAYPOINT_F9S2 48
#define MY_WAYPOINT_F9S3 41
#define MY_WAYPOINT_F9S4 40
#define MY_WAYPOINT_F9S5 42
#define MY_WAYPOINT_F10S1 47
#define MY_WAYPOINT_F10S2 46
#define MY_WAYPOINT_F10S3 45
#define MY_WAYPOINT_F10S4 44
#define MY_WAYPOINT_F10S5 43

void F9S1()
{
    spawnCaveMonsters(4, TO_WAYPOINT_XY(MY_WAYPOINT_F9S1));
    spawnCaveMonsters(4, TO_WAYPOINT_XY(MY_WAYPOINT_F9S2));    
    spawnCaveMonsters(NoxRandomInteger(5, 7), TO_WAYPOINT_XY(MY_WAYPOINT_F9S3));
    spawnCaveMonsters(NoxRandomInteger(5, 7), TO_WAYPOINT_XY(MY_WAYPOINT_F9S4));
    spawnCaveMonsters(NoxRandomInteger(5, 8), TO_WAYPOINT_XY(MY_WAYPOINT_F9S5));

    spawnCaveMonsters(NoxRandomInteger(5, 8), TO_WAYPOINT_XY(MY_WAYPOINT_F10S1));
    spawnCaveMonsters(NoxRandomInteger(5, 8), TO_WAYPOINT_XY(MY_WAYPOINT_F10S2));
    spawnCaveMonsters(NoxRandomInteger(5, 8), TO_WAYPOINT_XY(MY_WAYPOINT_F10S3));
    spawnCaveMonsters(NoxRandomInteger(5, 8), TO_WAYPOINT_XY(MY_WAYPOINT_F10S4));
    spawnCaveMonsters(NoxRandomInteger(5, 8), TO_WAYPOINT_XY(MY_WAYPOINT_F10S5));
    NoxObjectOff(SELF);
}

#define MY_WAYPOINT_F13S1 74
#define MY_WAYPOINT_F13S2 75
#define MY_WAYPOINT_F13S3 76
#define MOB_EMBERDEMON_HP 225

static int s_firemons[]={OBJ_EMBER_DEMON, OBJ_MELEE_DEMON, OBJ_FIRE_SPRITE};
static int s_firemonLength = sizeof(s_firemons)/sizeof(s_firemons[0]);

static void spawnFiremonsters(int count, float xpos, float ypos)
{
    while (--count >= 0)
    {
        int pic = NoxRandomInteger(0, s_firemonLength - 1);

        if (s_firemons[pic] == OBJ_FIRE_SPRITE)
        {
            CreateFiresprite(xpos, ypos);
            continue;
        }
        SetUnitMaxHealth( CreateObjectById(s_firemons[pic], xpos, ypos), MOB_EMBERDEMON_HP);
    }
}

void MonsterFloor13()
{
    spawnFiremonsters(NoxRandomInteger(3, 4), TO_WAYPOINT_XY(MY_WAYPOINT_F13S1));
    spawnFiremonsters(NoxRandomInteger(3, 4), TO_WAYPOINT_XY(MY_WAYPOINT_F13S2));
    spawnFiremonsters(NoxRandomInteger(3, 4), TO_WAYPOINT_XY(MY_WAYPOINT_F13S3));
    NoxObjectOff(SELF);
}

int IsAliveSewerSpider()
{
    return NoxCurrentHealth(s_sewerSpider) != 0;
}

void OnInitialMonster()
{
    InitializeBinScript();
    s_sewerSpider = CreateObjectById(OBJ_SPIDER, TO_WAYPOINT_XY(84));
    SetUnitMaxHealth(s_sewerSpider, 260);
    s_leoric = createLeoric(TO_WAYPOINT_XY(88));
}

