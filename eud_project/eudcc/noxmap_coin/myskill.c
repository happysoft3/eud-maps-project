
#include "myskill.h"
#include "../include/noxobject.h"
#include "../include/mathlab.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"

static int s_indexCur;

static void onpushDamage()
{
    int owner = GetOwner(SELF);

    if (NoxIsAttackedBy(OTHER, owner) && NoxCurrentHealth(OTHER))
    {
        NoxDamage(OTHER, owner, 1050, DAMAGE_PLASMA);
        NoxEnchant(OTHER, ENCHANT_CHARMING, 0.3f);
    }
}

static void castStraightSkill(int sub)
{
    int owner = GetOwner(sub), durate = NoxGetDirection(sub);

    if (NoxIsObjectOn(sub))
    {
        if (durate && NoxCurrentHealth(owner))
        {
            TeleportObjectVector(sub, NoxGetObjectZ(sub), NoxGetObjectZ(sub + 1));
            int unit = DummyUnitCreateAt(OBJ_CARNIVOROUS_PLANT, NoxGetObjectX(sub), NoxGetObjectY(sub));

            NoxSetOwner(owner, unit);
            NoxDeleteObjectTimer(unit, 1);
            NoxSetCallback(unit, UnitCallback_Collision, &onpushDamage);
            NoxLookWithAngle(sub, --durate);
            NoxFrameTimerWithArg(1, sub, &castStraightSkill);

            NoxDeleteObjectTimer(CreateObjectAt("BigSmoke", NoxGetObjectX(unit), NoxGetObjectY(unit)), 18);
            return;
        }
        NoxDeleteObject(sub);
        NoxDeleteObject(sub + 1);
    }
}

static void harpoonEvent(int cur, int owner)
{
    if (NoxHasEnchant(owner, ENCHANT_CROWN))
    {
        int unit = CreateObjectAt("InvisibleLightBlueLow", NoxGetObjectX(owner), NoxGetObjectY(owner));

        NoxRaiseObject(CreateObjectAt("InvisibleLightBlueLow", NoxGetObjectX(unit), NoxGetObjectY(unit)), UnitAngleSin(owner, 23.0f));
        NoxRaiseObject(unit, UnitAngleCos(owner, 23.0f));
        NoxLookWithAngle(unit, 19);
        NoxSetOwner(owner, unit);
        NoxEnchantOff(owner, ENCHANT_CROWN);
        NoxFrameTimerWithArg(1, unit, &castStraightSkill);
    }
    NoxDeleteObject(cur);
}

static void doCheckDetecting(int cur)
{
    NoxObject *ptr = UnitToPtr(cur);

    if (!ptr)
        return;

    if (ptr->thingType == 526)
        harpoonEvent(cur, GetOwner(cur));
}

void LoopSearchIndex()
{
    NoxObject *ptr;
    int tempId;

    for (;;)
    {
        ptr = GETLASTUNIT;
        if (!ptr)
            break;

        tempId = ptr->globalID;
        if (s_indexCur)
        {
            while (s_indexCur < tempId)
                doCheckDetecting(++s_indexCur);
        }
        s_indexCur = tempId;
        break;
    }
    NoxFrameTimer(1, &LoopSearchIndex);
}

