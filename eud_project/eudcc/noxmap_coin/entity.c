
#include "entity.h"
#include "playerControl.h"
#include "../include/printUtil.h"
#include "../include/xfer.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"

static int s_doLoop = TRUE;
#define MY_STACK_MAX 10
static int s_zombies[MY_STACK_MAX];
static char s_stackTable[MY_STACK_MAX] = {0,1,2,3,4,5,6,7,8,9};
static int s_myStackCount = MY_STACK_MAX;

static short *s_sayWhenSaw[]=
{
    u"이리와, 징징이 !! 나와 함께 하자꾸나 ...",
    u"징징이... 너는 내 것이야 ...  !",
    u"너는 도망갈 수 없어 ... 그럴 줄 알고 내가 출입문을 돌로 막아두었지... !",
    u"나랑 같이 해주면 월급 올려줄 수 있다 .. 어서와 !!",
    u"징징아 밥먹자~~(하트)",
};

static short *s_sayWhenSplash[]=
{
    u"거봐 내가 뭐랬어?... 잡힌다고 했지 !!",
    u"우우웁.... 쭙 ..",
    u"잡았다 요놈 ...!",
    u"가만히 있어!!",
};

#define PUSHMYSTACK(value)  s_stackTable[s_myStackCount++] = value
#define POPMYSTACK()    s_stackTable[--s_myStackCount]

void RemoveAllZombies()
{
    static uint32_t length = (uint32_t)(sizeof(s_zombies) / sizeof(int));

    for (int i = 0 ; i < length ; ++i)
    {
        if (NoxCurrentHealth(s_zombies[i]))
            NoxDeleteObject(s_zombies[i]);
    }
}

void StopSpawnLoop()
{
    if (s_doLoop)
        s_doLoop = FALSE;
}

static void resetUnitSight(int unit)
{
    if (NoxCurrentHealth(unit))
    {
        NoxEnchantOff(unit, ENCHANT_DETECTING);
        NoxEnchant(unit, ENCHANT_BLINDED, 0.1f);
        NoxAggressionLevel(unit, 1.0f);
    }
}

static void onZombieLookEnemy()
{
    int tick = 3;

    if (NoxCurrentHealth(OTHER))
    {
        if (!NoxHasEnchant(SELF, ENCHANT_ETHEREAL))
        {
            NoxEnchant(SELF, ENCHANT_ETHEREAL, 0.08f);
            if (DistanceUnitToUnit(SELF, OTHER) >= 50.0f)
            {
                NoxPushObject(SELF, -25.0f, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
                if (!NoxHasEnchant(SELF, ENCHANT_MOONGLOW))
                {
                    NoxEnchant(SELF, ENCHANT_MOONGLOW, 10.0f);
                    WideSayMessage(SELF, s_sayWhenSaw[NoxRandomInteger(0, (int)(sizeof(s_sayWhenSaw)/sizeof(int)) - 1)], 180);
                }
            }
            else
            {
                NoxHitLocation(SELF, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
                NoxEnchantOff(OTHER, ENCHANT_INVULNERABLE);
                NoxDamage(OTHER, SELF, 255, 14);
                WideSayMessage(SELF, s_sayWhenSplash[NoxRandomInteger(0, (int)(sizeof(s_sayWhenSplash)/sizeof(int)) - 1)],180);
                tick += 20;
            }
            NoxCreatureFollow(SELF, OTHER);
            NoxAggressionLevel(SELF, 0.83f);
        }
        if (!NoxHasEnchant(SELF, ENCHANT_DETECTING))
        {
            NoxEnchant(SELF, ENCHANT_DETECTING, 0.0f);
            NoxFrameTimerWithArg(tick, NoxGetTrigger(), &resetUnitSight);
        }
    }
}

static void onZombieDeath()
{
    char index = (char)NoxGetDirection(NoxGetTrigger() + 1);

    PUSHMYSTACK(index);
    NoxDeleteObject(NoxGetTrigger() + 1);
}

static int spawnZombie(int locationId)
{
    char index = POPMYSTACK();
    int unit = CreateObject("StoneGolem", locationId);

    NoxLookWithAngle( CreateObject("InvisibleLightBlueHigh", locationId), (int)index);
    s_zombies[index] = unit;
    NoxEnchant(unit, ENCHANT_HASTED, 0);
    NoxEnchant(unit, ENCHANT_VILLAIN, 14.0f);
    SetUnitMaxHealth(unit, 1000);
    NoxSetCallback(unit, UnitCallback_OnDeath, &onZombieDeath);
    NoxSetCallback(unit, UnitCallback_EnemySighted, &onZombieLookEnemy);
}

static void loopSpawnZombie()
{
    if (s_myStackCount)
        spawnZombie(3);
    if (s_doLoop)
        NoxSecondTimer(36, &loopSpawnZombie);
}

static void loopZombieStat(int cur)
{
    int mon= s_zombies[cur];
    if (NoxCurrentHealth(mon))
    {
        int target = GetClosestPlayer(mon);

        if (target)
        {
            NoxCreatureFollow(mon, target);
            NoxAggressionLevel(mon, 1.0f);
        }
        if (!NoxHasEnchant(mon, ENCHANT_VILLAIN))
        {
            NoxEnchant(mon, ENCHANT_VILLAIN, 18.0f);
            int gold = CreateObjectAt("Gold", NoxGetObjectX(mon), NoxGetObjectY(mon));

            NoxDeleteObjectTimer(gold, 1800);
            GoldXferSetAmount(gold, NoxRandomInteger(20, 35));
        }
    }
    if (s_doLoop)
        NoxFrameTimerWithArg(3, (++cur)%MY_STACK_MAX, &loopZombieStat);
}

void StartZombieLoop()
{
    loopSpawnZombie();
    loopZombieStat(0);
}

