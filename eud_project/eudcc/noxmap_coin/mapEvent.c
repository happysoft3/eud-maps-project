
#include "mapEvent.h"
#include "playerControl.h"
#include "myskill.h"
#include "entity.h"
#include "../include/printUtil.h"
#include "../include/xfer.h"
#include "../include/noxobject.h"
#include "../include/logfile.h"
#include "../include/builtins.h"

static int s_twoBouders[2];
static int s_gameflag = TRUE;

#define NULLPTR 0

static int putFrozenBoulder(float xpos, float ypos)
{
    int boulder = CreateObjectAt("Boulder", xpos, ypos);

    NoxEnchant(boulder, ENCHANT_FREEZE, 0);
    NoxFrozen(boulder, TRUE);
    return boulder;
}

static void killFrozenBoulder(int bou)
{
    NoxFrozen(bou, FALSE);
    NoxDamage(bou, 0, 9999, DAMAGE_PLASMA);
}

static void setUnitHealth(int mon, int amount)
{
    NoxDamage(mon, 0, NoxCurrentHealth(mon) - 1, DAMAGE_PLASMA);
    NoxPickup(mon, CreateObjectAt("RottenMeat", NoxGetObjectX(mon), NoxGetObjectY(mon)));
    NoxDamage(mon, 0, NoxCurrentHealth(mon) - amount, DAMAGE_PLASMA);
}

static void onDestroyBoulder()
{
    killFrozenBoulder(s_twoBouders[0]);
    killFrozenBoulder(s_twoBouders[1]);
    NoxDeleteObjectTimer(SELF, 1);
}

static void blocksEntrance()
{
    float alignx = NoxGetWaypointX(7), aligny = NoxGetWaypointY(7);

    s_twoBouders[0] = putFrozenBoulder(alignx, aligny);
    s_twoBouders[1] = putFrozenBoulder(alignx + 23.0f, aligny - 23.0f);
    int mon = CreateObjectAt("CarnivorousPlant", alignx, aligny - 23.0f);

    setUnitHealth(mon, 5000);
    NoxAggressionLevel(mon, 0);
    NoxSetCallback(mon, UnitCallback_OnDeath, &onDestroyBoulder);
}

static void storyPicBuySkills()
{
    if (NoxGetGold(OTHER) >= 300)
    {
		NoxEnchant(OTHER, ENCHANT_CROWN, 0);
		NoxChangeGold(OTHER, -300);
		WideSayMessage(SELF, u"* 스킬을 구입하였습니다", 180);
        WidePrintMessageAll(u"* 캐릭터의 머리 위에 왕관이 있는동안 '작살' 을 사용하여 스킬을 시전할 수 있습니다");
	}
    else
        WidePrintMessageAll(u" 스킬 구입을 위한 골드가 부족합니다, 가격은 300 골드 입니다");
}

static void putWizard()
{
    int wiz = CreateObject("Wizard", 57);

    NoxObjectOff(wiz);
    NoxDamage(wiz, 0, 9999, DAMAGE_PLASMA);
    NoxFrozen(wiz, TRUE);
    NoxSetupDialog(wiz, DIALOG_NORMAL, &storyPicBuySkills, NULLPTR);
}

static void toTeleporting()
{
    if (NoxCurrentHealth(OTHER))
    {
        NoxObject *ptr = UnitToPtr(OTHER);

        if (ptr->Class & (UNIT_CLASS_PLAYER | UNIT_CLASS_MONSTER))
        {
            int destination = NoxGetTrigger() + 1;
            NoxPlayFX(FX_SMOKE_BLAST, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 0, 0);
            NoxMoveObject(OTHER, NoxGetObjectX(destination), NoxGetObjectY(destination));
            NoxPlayFX(FX_LIGHTNING, NoxGetObjectX(SELF), NoxGetObjectY(SELF), NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
            NoxMoveWaypoint(1, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
            AudioEvent("BlindOff", 1);
        }
    }
}

static int putTeleportStuff(int location, int destLocation)
{
    int telp = CreateObject("WeirdlingBeast", location);

    CreateObject("InvisibleLightBlueLow", destLocation);
    int fx = CreateObjectAt("TeleportWake", NoxGetObjectX(telp), NoxGetObjectY(telp));
    NoxObject *fxptr = UnitToPtr(fx);

    fxptr->flags ^= UNIT_FLAG_NO_COLLIDE;
    NoxFrozen(fx, TRUE);
    NoxSetCallback(telp, UnitCallback_Collision, &toTeleporting);
    NoxDamage(telp, 0, 9999, -1);
    return telp;
}

static void initializeReadable()
{
    SetPicketText(NoxGetObject("mpick1"), u"나가는 출입구 입니다");
    SetPicketText(NoxGetObject("mpick2"), u"300골드로 스킬을 구입합니다, '작살'을 시전하면 스킬이 발동됩니다");
    SetPicketText(NoxGetObject("mpick3"), u"집게사장의 방 입니다");
    SetPicketText(NoxGetObject("mpick4"), u"얼굴문양 벽을 만지면, 비밀공간이 나타납니다");
    SetPicketText(NoxGetObject("mpick5"), u"집게리아! 집게버거의 고장");
    SetPicketText(NoxGetObject("mpick6"), u"동전줍는 징징이- 제작. new-a");
    SetPicketText(NoxGetObject("mpick7"), u"동전줍는 징징이- 시작하려면 붉은 벽돌위에 선다");
    SetPicketText(NoxGetObject("mpick8"), u"집게사장을 피해서 출구 벽을 부수고 탈출하면 성공합니다");
}

static int putGlyphWithoutEud(int location, SpellTypes first, SpellTypes second, SpellTypes third){
    int bb = CreateObject("Bomber", location);
    int ret = NoxGetLastItem(bb);

    NoxSetTrapSpells(bb, first, second, third);
    NoxDeleteObject(bb);
    return ret;
}

static int putKillerian(int locationId){
    int k=CreateObject("Demon", locationId);

    SetUnitMaxHealth(k, 400);
    return k;
}

static void summonDemons()
{
    putKillerian(67);
    putKillerian(68);
    putKillerian(69);
}

static void dispositionStuff()
{
    WriteLog("dispositionStuff");
    blocksEntrance();
    putWizard();
    putTeleportStuff(52, 57);
    initializeReadable();
    putGlyphWithoutEud(64, SPELL_FORCE_OF_NATURE, SPELL_BURN, 0);
    CreateGlyph(NoxGetWaypointX(66), NoxGetWaypointY(66), SPELL_FORCE_OF_NATURE, SPELL_BURN, 0);
    summonDemons();
    WriteLog("dispositionStuff");
}

static void loopSpawnGoldInRoom(int index)
{
    static int money[5];

    if (NoxIsObjectOn(money[index]))
        NoxDeleteObjectTimer(money[index], 3);
    money[index] = CreateObject(NoxRandomInteger(0, 1) ? "QuestGoldChest" : "QuestGoldPile", 6);
    GoldXferSetAmount(money[index], NoxRandomInteger(30, 90));
    AudioEvent("TreasureDrop", 6);
    if (s_gameflag)
        NoxFrameTimerWithArg(70, (++index) % 5, &loopSpawnGoldInRoom);
}

void StartLoop()
{
    static int startFlag = FALSE;

    NoxObjectOff(SELF);
    if (!startFlag)
    {
        startFlag = TRUE;
        NoxFrameTimerWithArg(300, 0, &loopSpawnGoldInRoom);
        WriteLog("NoxFrameTimerWithArg");
        NoxFrameTimer(150, &StartZombieLoop);
        WriteLog("NoxFrameTimer");
        WidePrintMessageAll(u"===동전줍는 징징이.. 시작합니다===");
        WriteLog("WidePrintMessageAll");
    }
}

void OnInitialMap()
{
    NoxMusicEvent();
    CreateLogFile("maplog_coin.txt");
    NoxFrameTimer(1, &dispositionStuff);
    NoxFrameTimer(3, &OnPlayerLoop);
    NoxFrameTimer(30, &LoopSearchIndex);
    int *pGameFlags=(int*)0x5d5394;

    *pGameFlags=TRUE;
}

void escapeSucess()
{
    WidePrintMessageAll(u"집게리아 에서 탈출하셨습니다 ...!!");
	WidePrintMessageAll(u"이제 당신은 집게 사장으로 부터 안전합니다 ... !!");
    s_gameflag = FALSE;
    StopSpawnLoop();
	RemoveAllZombies();
    CreateObjectAt("LevelUp", NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
    AudioEvent("StaffOblivionAchieve1", 48);
    NoxPlayFX(FX_WHITE_FLASH, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER), 0, 0);
    NoxObjectOff(SELF);
    putTeleportStuff(59, 60);
    putTeleportStuff(61, 62);
}

void spellTestFirst()
{
    if (DistanceUnitToUnit(SELF, OTHER) > 0)
        NoxCastSpellLocationLocation(SPELL_FIREBALL, NoxGetObjectX(SELF), NoxGetObjectY(SELF), NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

void spellTestSecond()
{
    NoxCastSpellObjectLocation(SPELL_METEOR, OTHER, NoxGetObjectX(OTHER), NoxGetObjectY(OTHER));
}

void spellTestThird()
{
    NoxCastSpellObjectObject(SPELL_FREEZE, SELF, OTHER);
}

void spellTestFourth()
{
    int trp = CreateObject("BomberGreen", 63);

    NoxPlayFX(FX_SMOKE_BLAST, NoxGetObjectX(trp), NoxGetObjectY(trp), 0, 0);
    NoxSetTrapSpells(trp, SPELL_FORCE_OF_NATURE, SPELL_BURN, 0);
}
