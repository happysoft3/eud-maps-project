
#include "../include/builtins.h"
#include "mapEvent.h"
#include "../include/team.h"

static void DeferredInit()
{
    EnableCoopTeamMode();
}

void MapInitialize()
{
    BuiltinsInitialize();
    OnInitialMap();
    NoxFrameTimer(1, &DeferredInit);
}

void MapExit()
{
    NoxMusicEvent();
    DisableCoopTeamMode();
}

