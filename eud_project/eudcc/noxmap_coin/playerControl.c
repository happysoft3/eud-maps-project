
#include "playerControl.h"
#include "../include/playerInfo.h"
#include "../include/equipment.h"
#include "../include/noxobject.h"
#include "../include/stringUtil.h"
#include "../include/printUtil.h"
#include "../include/builtins.h"
#include "../include/objectIDdefines.h"
#include "../include/fxeffect.h"
#include "../include/mathlab.h"
#include "../include/unitExtend.h"
#include "../include/spellUtils.h"

#define FALSE 0
#define TRUE 1
#define NULLPTR 0

#define PLAYER_DEATH_FLAG 2

#define PLAYER_DATA_MAX 32
static PlayerData s_playerData[PLAYER_DATA_MAX];
static PlayerData *s_firstPlayerData = NULLPTR;
static int s_playerAnchors[PLAYER_DATA_MAX];

static void emptyAll(int user)
{
    while (NoxIsObjectOn(NoxGetLastItem(user)))
        NoxDeleteObject(NoxGetLastItem(user));
}

static int s_playerTraps[PLAYER_DATA_MAX];

static void onBearTrapExplosion()
{
    int owner=GetOwner(SELF);

    if (!NoxGetTrigger())
        return;

    if (NoxCurrentHealth(OTHER))
    {
        if (NoxCurrentHealth(owner) && NoxIsAttackedBy(OTHER, owner))
        {
            float x=NoxGetObjectX(SELF),y=NoxGetObjectY(SELF);

            PlaySoundAround(SELF, SOUND_HammerMissing);
            PlaySoundAround(SELF, SOUND_BearTrapTriggered);
            NoxPlayFX(FX_EXPLOSION, x,y,0,0);
            if (NoxGetGold(owner))
            {
                NoxEnchant(OTHER, ENCHANT_HELD, 2.0f);
                NoxDamage(OTHER, owner, NoxGetGold(owner), DAMAGE_PLASMA);
            }
            CreateObjectById(OBJ_CLOSED_BEAR_TRAP,x,y);
            NoxDeleteObject(SELF);
        }
    }
}

static void putBearTrap(int user, int pIndex)
{
    if (NoxHasEnchant(user, ENCHANT_AFRAID))
        return;
    if (!NoxGetGold(user))
        return;

    NoxEnchant(user, ENCHANT_AFRAID, 5.0f);

    int trap=s_playerTraps[pIndex];

    if (NoxIsObjectOn(trap))
        NoxDeleteObject(trap);

    float x=NoxGetObjectX(user), y=NoxGetObjectY(user);

    trap=CreateObjectById(OBJ_BEAR_TRAP, x,y);
    NoxSetOwner(user,trap);
    PlaySoundAround(user, SOUND_Clank2);
    NoxDeleteObjectTimer(CreateObjectById(OBJ_MAGIC_SPARK, x,y),12);
    NoxPushObjectTo(user,UnitAngleCos(user,60.0f),UnitAngleSin(user,60.0f));
    SetUnitCallbackExtended(trap,&onBearTrapExplosion,EXTENDED_ON_COLLIDE);
}

static void playerOnAlive(PlayerData *pdata)
{
    NoxMoveObject(s_playerAnchors[pdata->pIndex], NoxGetObjectX(pdata->unitId), NoxGetObjectY(pdata->unitId));
    if (NoxHasEnchant(pdata->unitId, ENCHANT_SNEAK))
    {
        NoxEnchantOff(pdata->unitId, ENCHANT_SNEAK);
        RemoveEffectTreadLightly(pdata->unitId);
        putBearTrap(pdata->unitId, pdata->pIndex);
    }
}

static void playerOnDeath(PlayerData *pdata)
{
    // NoxPrintToAll("test print on death");
    if (!pdata->userName)
        return;

    short msg[128];

    CopyWideString(pdata->userName, msg);
    static short *myMent = u" 이(가) 적에게 격추되었습니다";
    ConcatenateWideString(msg, myMent);
    WidePrintMessageAll(msg);
}

static PlayerData *erasePlayerData(PlayerData *prev, PlayerData *cur)
{
    cur->userName = NULLPTR;
    if (prev == NULLPTR)
    {
        s_firstPlayerData = cur->pNext;
        return s_firstPlayerData;
    }
    prev->pNext = cur->pNext;
    return cur->pNext;
}

static PlayerData *loadPlayerData(int plrUnit)
{
    PlayerData *pdata = s_firstPlayerData;

    while (pdata)
    {
        if (plrUnit == pdata->unitId)
            return pdata;

        pdata = pdata->pNext;
    }
    return NULLPTR;
}

static PlayerData *appendPlayer(int plrUnit)
{
    int pIndex = GetPlayerIndex(plrUnit);

    if (pIndex < 0)
        return NULLPTR;

    PlayerData *pdata = &s_playerData[pIndex];

    pdata->unitId = plrUnit;
    pdata->flags = 0;
    pdata->pIndex = pIndex;
    pdata->pNext = s_firstPlayerData;
    pdata->userName = PlayerIngameNickname(plrUnit);
    s_firstPlayerData = pdata;
    return pdata;
}

static int playerHandler(PlayerData *pdata)
{
    if (NoxMaxHealth(pdata->unitId))
    {
        NoxObject *ptr = UnitToPtr(pdata->unitId);

        if (ptr->flags & UNIT_FLAG_NO_COLLIDE)
            return 0;

        else if (NoxCurrentHealth(pdata->unitId))
        {
            playerOnAlive(pdata);
        }
        else if (!(pdata->flags & PLAYER_DEATH_FLAG))
        {
            playerOnDeath(pdata);
            pdata->flags ^= PLAYER_DEATH_FLAG;
        }
        return 1;
    }
    return 0;
}

void OnPlayerLoop()
{
    PlayerData *pdata = s_firstPlayerData, *prev = NULLPTR;
    int res = 0;

    while (pdata)
    {
        if (!playerHandler(pdata))
            pdata = erasePlayerData(prev, pdata);   //다음 노드를 반환한다
        else
        {
            prev = pdata;
            pdata = pdata->pNext;
        }
    }
    NoxFrameTimer(1, &OnPlayerLoop);
}

int GetClosestPlayer(int unit)
{
    PlayerData *pdata = s_firstPlayerData;
    float temp = 8192.0f, r = 0.0f;
    int ret = 0;

    while (pdata)
    {
        if (NoxCurrentHealth(pdata->unitId))
        {
            r = DistanceUnitToUnit(unit, pdata->unitId);
            if (r < temp)
            {
                temp = r;
                ret = pdata->unitId;
            }
        }
        pdata = pdata->pNext;
    }
    return ret;
}

static void deferredPickup(int unit)
{
    int owner = GetOwner(unit);

    if (!NoxCurrentHealth(owner))
        NoxDeleteObject(unit);
    else
    {
        NoxPickup(owner, unit);
        NoxDeleteObjectTimer(unit, 3);        
    }
}

static int createPlayerAnchor(int plrUnit)
{
    int anch = CreateObjectAt("Flag", NoxGetObjectX(plrUnit), NoxGetObjectY(plrUnit));
    NoxObject *ptr = UnitToPtr(anch);

    SetEquipEnchanment(anch, 0, ITEM_PROPERTY_vampirism4, 0, 0);
    ptr->flags ^= UNIT_FLAG_NO_PUSH_CHARACTERS;
    return anch;
}

static void giveSpeedbootsToPlayer(int plrUnit)
{
    int boots = CreateObjectAt("LeatherBoots", NoxGetObjectX(plrUnit), NoxGetObjectY(plrUnit));

    SetEquipEnchanment(boots, 0, 0, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_Speed3);
    NoxSetOwner(plrUnit, boots);
    NoxFrameTimerWithArg(1, boots, &deferredPickup);
}

static void playerJoinOnMap(PlayerData *pdata)
{
    int plrUnit = pdata->unitId;

    if (pdata->flags & PLAYER_DEATH_FLAG)
        pdata->flags ^= PLAYER_DEATH_FLAG;

    emptyAll(plrUnit);
    NoxEnchant(plrUnit, ENCHANT_ANCHORED, 0);
    NoxEnchant(plrUnit, ENCHANT_ANTI_MAGIC, 0);
    NoxEnchant(plrUnit, ENCHANT_HASTED, 0);
    NoxEnchant(plrUnit, ENCHANT_SLOWED, 0);
    if (NoxGetGold(plrUnit))
        NoxChangeGold(plrUnit, -NoxGetGold(plrUnit));
    giveSpeedbootsToPlayer(plrUnit);
    NoxDeleteObject(s_playerAnchors[pdata->pIndex]);
    s_playerAnchors[pdata->pIndex] = createPlayerAnchor(plrUnit);
    
    int startLoc = NoxGetObject("mystartlocation");

    NoxMoveObject(plrUnit, NoxGetObjectX(startLoc), NoxGetObjectY(startLoc));
}

static int registPlayer(int plrUnit, PlayerData *pdata)
{
    if (NoxCurrentHealth(plrUnit))
    {
        // PlayerData *pdata = loadPlayerData(plrUnit);

        if (!pdata) //새로운 사용자인 경우,
            pdata = appendPlayer(plrUnit);

        if (!pdata)
        {
            NoxPrintToAll("error");
            return FALSE;
        }
        playerJoinOnMap(pdata);
        return TRUE;
    }
    return FALSE;
}

void afterGetPlayer()
{
    if (NoxCurrentHealth(OTHER))
    {
        int plrUnit = NoxGetCaller();

        if (!registPlayer(plrUnit, loadPlayerData(plrUnit)))
            NoxMoveObject(plrUnit, NoxGetWaypointX(58), NoxGetWaypointY(58));
    }
}

void getPlayer()
{
    if (NoxCurrentHealth(OTHER))
    {
        PlayerData *pdata = loadPlayerData(NoxGetCaller());

        if (pdata)
        {
            if (registPlayer(NoxGetCaller(), pdata))
                return;
        }
        NoxMoveObject(OTHER, NoxGetWaypointX(58), NoxGetWaypointY(58));
    }
}
