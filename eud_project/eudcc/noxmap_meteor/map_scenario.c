
#include "map_scenario.h"
#include "firewayTrap.h"
#include "../include/stringUtil.h"
#include "../include/builtins.h"

typedef int object;

int gvar158;
int gvar157;
int gvar156;
int gvar155;
int gvar154;
int gvar153;
int gvar152;
int gvar151;
int gvar150;
int gvar149;
int gvar148;
int gvar147;
int gvar146;
int gvar145;
object gvar144;
object gvar143;
object gvar142;
object gvar141;
object gvar140;
int gvar139[32];
object gvar138;
object gvar137;
object gvar136;
object gvar135;
object gvar134;
object gvar133;
int gvar132;
int gvar131;
int gvar130;
object gvar129;
object gvar128;
object gvar127;
object gvar126;
object gvar125;
int gvar124;
int gvar123;
int gvar122;
object gvar120;
object gvar119;
object gvar118;
object gvar117;
object gvar116;
object gvar115;
object gvar114;
object gvar113;
object gvar112;
object gvar111;
object gvar110;
object gvar109;
object gvar108;
object gvar107;
object gvar106;
object gvar105;
object gvar104;
object gvar103;
object gvar102;
object gvar101;
object gvar100;
object gvar99;
object gvar98;
object gvar97;
object gvar96;
object gvar95;
object gvar94;
object gvar93;
object gvar92;
object gvar91;
object gvar90;
object gvar89;
object gvar88;
object gvar87;
object gvar86;
object gvar85;
object gvar84;
object gvar83;
object gvar82;
object gvar81;
object gvar80;
object gvar79;
object gvar78;
object gvar77;
object gvar76;
int gvar75;
int gvar74;
int gvar73;
int gvar72;
int gvar71;
int gvar70;
int gvar69;
int gvar68;
int gvar67;
int gvar66;
int gvar65;
int gvar64;
int gvar63;
int gvar62;
int gvar61;
int gvar60;
int gvar59;
int gvar58;
int gvar57;
int gvar56;
int gvar55;
int gvar54;
int gvar53;
int gvar52;
int gvar51;
int gvar50;
int gvar49;
int gvar48;
int gvar47;
int gvar46;
int gvar45;
object gvar39;
object gvar38;
object gvar37;
object gvar36;
object gvar35;
object gvar34;
object gvar33;
object gvar32;
object gvar31;
object gvar30;
object gvar29;
object gvar28;
object gvar27;
object gvar26;
object gvar25;
object gvar24;
object gvar23;
object gvar22;
object gvar21;
object gvar20;
object gvar19;
object gvar18;
object gvar17;
object gvar16;
object gvar15;
object gvar14;
object gvar13;
object gvar12;
object gvar11;
object gvar10;
object gvar9;
object gvar8;
object gvar7;
object gvar6;
object gvar5;
object gvar4;
int gvar40 = 0;
int gvar41 = 1;
int gvar42 = 0;
int gvar43 = 150;
int gvar44 = FALSE;
int gvar121 = FALSE;

static void InitializeArrowTraps()
{
    gvar4 = NoxGetObjectGroup("ArrowTrap01Row01");
    gvar5 = NoxGetObjectGroup("ArrowTrap01Row02");
    gvar6 = NoxGetObjectGroup("ArrowTrap01Row03");
    gvar7 = NoxGetObjectGroup("ArrowTrap02Row01");
    gvar8 = NoxGetObjectGroup("ArrowTrap03Row01");
    gvar9 = NoxGetObjectGroup("ArrowTrap04Row01");
    gvar10 = NoxGetObjectGroup("ArrowTrap05Row01");
    gvar11 = NoxGetObjectGroup("ArrowTrap05Row02");
    gvar12 = NoxGetObjectGroup("ArrowTrap05Row03");
    gvar13 = NoxGetObjectGroup("ArrowTrap06Row01");
    gvar14 = NoxGetObjectGroup("ArrowTrap07Row01");
    gvar15 = NoxGetObjectGroup("ArrowTrap08Row01");
    gvar16 = NoxGetObjectGroup("ArrowTrap08Row02");
    gvar17 = NoxGetObjectGroup("ArrowTrap09Row01");
    gvar18 = NoxGetObjectGroup("ArrowTrap09Row02");
    gvar19 = NoxGetObjectGroup("ArrowTrap09Row03");
    gvar20 = NoxGetObjectGroup("ArrowTrap09Row04");
    gvar21 = NoxGetObjectGroup("ArrowTrap09Row05");
    gvar22 = NoxGetObjectGroup("ArrowTrap09Row06");
    gvar23 = NoxGetObjectGroup("ArrowTrap09Row07");
    gvar24 = NoxGetObjectGroup("ArrowTrap09Row08");
    gvar25 = NoxGetObjectGroup("ArrowTrap09Row09");
    gvar26 = NoxGetObjectGroup("ArrowTrap09Row10");
    gvar27 = NoxGetObjectGroup("ArrowTrap09Row11");
    gvar28 = NoxGetObjectGroup("ArrowTrap09Row12");
    gvar29 = NoxGetObjectGroup("ArrowTrap09Row13");
    gvar30 = NoxGetObjectGroup("ArrowTrap09Row14");
    gvar31 = NoxGetObjectGroup("ArrowTrap09Row15");
    gvar32 = NoxGetObjectGroup("ArrowTrap09Row16");
    gvar33 = NoxGetObjectGroup("ArrowTrap09Row17");
    gvar34 = NoxGetObjectGroup("ArrowTrap09Row18");
    gvar35 = NoxGetObjectGroup("ArrowTrap09Row19");
    gvar36 = NoxGetObjectGroup("ArrowTrap09Row20");
    gvar37 = NoxGetObjectGroup("ArrowTrap09Row21");
    gvar38 = NoxGetObjectGroup("ArrowTrap09Row22");
    gvar39 = NoxGetObjectGroup("ArrowTrap10Row01");
}

void DisableArrowTrap01Row01()
{
    NoxObjectGroupOff(gvar4);
}

void ArrowTrap01Row01()
{
    NoxObjectGroupOn(gvar4);
    NoxFrameTimer(1, &DisableArrowTrap01Row01);
}

void DisableArrowTrap01Row02()
{
    NoxObjectGroupOff(gvar5);
}

void ArrowTrap01Row02()
{
    NoxObjectGroupOn(gvar5);
    NoxFrameTimer(1, &DisableArrowTrap01Row02);
}

void DisableArrowTrap01Row03()
{
    NoxObjectGroupOff(gvar6);
}

void ArrowTrap01Row03()
{
    NoxObjectGroupOn(gvar6);
    NoxFrameTimer(1, &DisableArrowTrap01Row03);
}

void DisableArrowTrap02Row01()
{
    NoxObjectGroupOff(gvar7);
}

void ArrowTrap02Row01()
{
    NoxObjectGroupOn(gvar7);
    NoxFrameTimer(1, &DisableArrowTrap02Row01);
}

void DisableArrowTrap03Row01()
{
    NoxObjectGroupOff(gvar8);
}

void ArrowTrap03Row01()
{
    NoxObjectGroupOn(gvar8);
}

void DisableArrowTrap04Row01()
{
    NoxObjectGroupOff(gvar9);
}

void ArrowTrap04Row01()
{
    NoxObjectGroupOn(gvar9);
}

void DisableArrowTrap05Row01()
{
    NoxObjectGroupOff(gvar10);
}

void ArrowTrap05Row01()
{
    NoxObjectGroupOn(gvar10);
    NoxFrameTimer(1, &DisableArrowTrap05Row01);
}

void DisableArrowTrap05Row02()
{
    NoxObjectGroupOff(gvar11);
}

void ArrowTrap05Row02()
{
    NoxObjectGroupOn(gvar11);
    NoxFrameTimer(1, &DisableArrowTrap05Row02);
}

void DisableArrowTrap05Row03()
{
    NoxObjectGroupOff(gvar12);
}

void ArrowTrap05Row03()
{
    NoxObjectGroupOn(gvar12);
    NoxFrameTimer(1, &DisableArrowTrap05Row03);
}

void DisableArrowTrap06Row01()
{
    NoxObjectGroupOff(gvar13);
}

void ArrowTrap06Row01()
{
    NoxObjectGroupOn(gvar13);
    NoxFrameTimer(1, &DisableArrowTrap06Row01);
}

void DisableArrowTrap07Row01()
{
    NoxObjectGroupOff(gvar14);
}

void ArrowTrap07Row01()
{
    NoxObjectGroupOn(gvar14);
    NoxFrameTimer(1, &DisableArrowTrap07Row01);
}

void DisableArrowTrap08Row01()
{
    NoxObjectGroupOff(gvar15);
}

void ArrowTrap08Row01()
{
    NoxObjectGroupOn(gvar15);
    NoxFrameTimer(1, &DisableArrowTrap08Row01);
}

void DisableArrowTrap08Row02()
{
    NoxObjectGroupOff(gvar16);
}

void ArrowTrap08Row02()
{
    NoxObjectGroupOn(gvar16);
    NoxFrameTimer(1, &DisableArrowTrap08Row02);
}

void DisableArrowTrap09Row01()
{
    NoxObjectGroupOff(gvar17);
}

void ArrowTrap09Row01()
{
    NoxObjectGroupOn(gvar17);
    NoxFrameTimer(1, &DisableArrowTrap09Row01);
}

void DisableArrowTrap09Row02()
{
    NoxObjectGroupOff(gvar18);
}

void ArrowTrap09Row02()
{
    NoxObjectGroupOn(gvar18);
    NoxFrameTimer(1, &DisableArrowTrap09Row02);
}

void DisableArrowTrap09Row03()
{
    NoxObjectGroupOff(gvar19);
}

void ArrowTrap09Row03()
{
    NoxObjectGroupOn(gvar19);
    NoxFrameTimer(1, &DisableArrowTrap09Row03);
}

void DisableArrowTrap09Row04()
{
    NoxObjectGroupOff(gvar20);
}
void ArrowTrap09Row04()
{
    NoxObjectGroupOn(gvar20);
    NoxFrameTimer(1, &DisableArrowTrap09Row04);
}

void DisableArrowTrap09Row05()
{
    NoxObjectGroupOff(gvar21);
}

void ArrowTrap09Row05()
{
    NoxObjectGroupOn(gvar21);
    NoxFrameTimer(1, &DisableArrowTrap09Row05);
}

void DisableArrowTrap09Row06()
{
    NoxObjectGroupOff(gvar22);
}

void ArrowTrap09Row06()
{
    NoxObjectGroupOn(gvar22);
    NoxFrameTimer(1, &DisableArrowTrap09Row06);
}

void DisableArrowTrap09Row07()
{
    NoxObjectGroupOff(gvar23);
}

void ArrowTrap09Row07()
{
    NoxObjectGroupOn(gvar23);
    NoxFrameTimer(1, &DisableArrowTrap09Row07);
}

void DisableArrowTrap09Row08()
{
    NoxObjectGroupOff(gvar24);
}

void ArrowTrap09Row08()
{
    NoxObjectGroupOn(gvar24);
    NoxFrameTimer(1, &DisableArrowTrap09Row08);
}

void DisableArrowTrap09Row09()
{
    NoxObjectGroupOff(gvar25);
}

void ArrowTrap09Row09()
{
    NoxObjectGroupOn(gvar25);
    NoxFrameTimer(1, &DisableArrowTrap09Row09);
}

void DisableArrowTrap09Row10()
{
    NoxObjectGroupOff(gvar26);
}

void ArrowTrap09Row10()
{
    NoxObjectGroupOn(gvar26);
    NoxFrameTimer(1, &DisableArrowTrap09Row10);
}

void DisableArrowTrap09Row11()
{
    NoxObjectGroupOff(gvar27);
}
void ArrowTrap09Row11()
{
    NoxObjectGroupOn(gvar27);
    NoxFrameTimer(1, &DisableArrowTrap09Row11);
}
void DisableArrowTrap09Row12()
{
    NoxObjectGroupOff(gvar28);
}
void ArrowTrap09Row12()
{
    NoxObjectGroupOn(gvar28);
    NoxFrameTimer(1, &DisableArrowTrap09Row12);
}

void DisableArrowTrap09Row13()
{
    NoxObjectGroupOff(gvar29);
}

void ArrowTrap09Row13()
{
    NoxObjectGroupOn(gvar29);
    NoxFrameTimer(1, &DisableArrowTrap09Row13);
}
void DisableArrowTrap09Row14()
{
    NoxObjectGroupOff(gvar30);
}

void ArrowTrap09Row14()
{
    NoxObjectGroupOn(gvar30);
    NoxFrameTimer(1, &DisableArrowTrap09Row14);
}

void DisableArrowTrap09Row15()
{
    NoxObjectGroupOff(gvar31);
}

void ArrowTrap09Row15()
{
    NoxObjectGroupOn(gvar31);
    NoxFrameTimer(1, &DisableArrowTrap09Row15);
}
void DisableArrowTrap09Row16()
{
    NoxObjectGroupOff(gvar32);
}
void ArrowTrap09Row16()
{
    NoxObjectGroupOn(gvar32);
    NoxFrameTimer(1, &DisableArrowTrap09Row16);
}
void DisableArrowTrap09Row17()
{
    NoxObjectGroupOff(gvar33);
}
void ArrowTrap09Row17()
{
    NoxObjectGroupOn(gvar33);
    NoxFrameTimer(1, &DisableArrowTrap09Row17);
}
void DisableArrowTrap09Row18()
{
    NoxObjectGroupOff(gvar34);
}
void ArrowTrap09Row18()
{
    NoxObjectGroupOn(gvar34);
    NoxFrameTimer(1, &DisableArrowTrap09Row18);
}
void DisableArrowTrap09Row19()
{
    NoxObjectGroupOff(gvar35);
}
void ArrowTrap09Row19()
{
    NoxObjectGroupOn(gvar35);
    NoxFrameTimer(1, &DisableArrowTrap09Row19);
}
void DisableArrowTrap09Row20()
{
    NoxObjectGroupOff(gvar36);
}
void ArrowTrap09Row20()
{
    NoxObjectGroupOn(gvar36);
    NoxFrameTimer(1, &DisableArrowTrap09Row20);
}
void DisableArrowTrap09Row21()
{
    NoxObjectGroupOff(gvar37);
}
void ArrowTrap09Row21()
{
    NoxObjectGroupOn(gvar37);
    NoxFrameTimer(1, &DisableArrowTrap09Row21);
}
void DisableArrowTrap09Row22()
{
    NoxObjectGroupOff(gvar38);
}
void ArrowTrap09Row22()
{
    NoxObjectGroupOn(gvar38);
    NoxFrameTimer(1, &DisableArrowTrap09Row22);
}
void DisableArrowTrap10Row01()
{
    NoxObjectGroupOff(gvar39);
}
void ArrowTrap10Row01()
{
    NoxObjectGroupOn(gvar39);
    NoxFrameTimer(1, &DisableArrowTrap10Row01);
}
void SpikeBlock01Loop()
{
    int var0 = gvar42;
    if (var0 == gvar40)
    {
        NoxMove(gvar45, gvar79);
        NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar78);
        gvar42 = gvar41;
    }
    else if (var0 == gvar41)
    {
        NoxMove(gvar45, gvar78);
        NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar79);
        gvar42 = gvar40;
    }
    NoxFrameTimer(gvar43, &SpikeBlock01Loop);
}

static void ActivateSpikeBlockGroup01()
{
    NoxMove(gvar46, gvar80);
    NoxMove(gvar47, gvar81);
    NoxMove(gvar48, gvar82);
}

static void InitializeBlocks()
{
    gvar45 = NoxGetObject("SpikeBlock01");
    gvar46 = NoxGetObject("SpikeBlock02");
    gvar47 = NoxGetObject("SpikeBlock03");
    gvar48 = NoxGetObject("SpikeBlock04");
    gvar49 = NoxGetObject("TempleEntrance");
    gvar50 = NoxGetObject("StoneBlock01");
    gvar51 = NoxGetObject("StoneBlock02");
    gvar52 = NoxGetObject("StoneBlock03");
    gvar53 = NoxGetObject("StoneBlock04");
    gvar54 = NoxGetObject("StoneBlock05");
    gvar55 = NoxGetObject("StoneBlock06");
    gvar56 = NoxGetObject("StoneBlock07");
    gvar57 = NoxGetObject("StoneBlock08");
    gvar58 = NoxGetObject("StoneBlock09");
    gvar59 = NoxGetObject("StoneBlock10");
    gvar60 = NoxGetObject("StoneBlock11");
    gvar61 = NoxGetObject("StoneBlock12");
    gvar62 = NoxGetObject("StoneBlock13");
    gvar63 = NoxGetObject("StoneBlock14");
    gvar64 = NoxGetObject("StoneBlock15");
    gvar65 = NoxGetObject("StoneBlock16");
    gvar66 = NoxGetObject("StoneBlock17");
    gvar67 = NoxGetObject("StoneBlock18");
    gvar68 = NoxGetObject("GeneratorPusher01");
    gvar69 = NoxGetObject("GeneratorPusher02");
    gvar70 = NoxGetObject("GeneratorPusher03");
    gvar71 = NoxGetObject("GeneratorPusher04");
    gvar72 = NoxGetObject("GeneratorPusherMover01");
    gvar73 = NoxGetObject("GeneratorPusherMover02");
    gvar74 = NoxGetObject("GeneratorPusherMover03");
    gvar75 = NoxGetObject("GeneratorPusherMover04");
    gvar76 = NoxGetObjectGroup("BlockGroup04Phase2Triggers");
    gvar77 = NoxGetObjectGroup("PusherTriggers");
    gvar78 = NoxGetWaypoint("SpikeBlock01Home");
    gvar79 = NoxGetWaypoint("SpikeBlock01Dest");
    gvar80 = NoxGetWaypoint("SpikeBlock02WP");
    gvar81 = NoxGetWaypoint("SpikeBlock03WP");
    gvar82 = NoxGetWaypoint("SpikeBlock04WP");
    gvar83 = NoxGetWaypoint("StoneBlock01WP");
    gvar84 = NoxGetWaypoint("StoneBlock02WP");
    gvar85 = NoxGetWaypoint("StoneBlock03WP");
    gvar86 = NoxGetWaypoint("StoneBlock04WP");
    gvar87 = NoxGetWaypoint("StoneBlock05WP");
    gvar88 = NoxGetWaypoint("StoneBlock06WP");
    gvar89 = NoxGetWaypoint("StoneBlock07WP");
    gvar90 = NoxGetWaypoint("StoneBlock08WP");
    gvar91 = NoxGetWaypoint("StoneBlock09WP");
    gvar92 = NoxGetWaypoint("StoneBlock10WP");
    gvar93 = NoxGetWaypoint("StoneBlock11WP");
    gvar94 = NoxGetWaypoint("StoneBlock12WP");
    gvar95 = NoxGetWaypoint("StoneBlock13WP");
    gvar96 = NoxGetWaypoint("StoneBlock14WP");
    gvar97 = NoxGetWaypoint("StoneBlock15WP");
    gvar98 = NoxGetWaypoint("StoneBlock16WP");
    gvar99 = NoxGetWaypoint("StoneBlock15WP2");
    gvar100 = NoxGetWaypoint("StoneBlock16WP2");
    gvar101 = NoxGetWaypoint("StoneBlock17WP");
    gvar102 = NoxGetWaypoint("StoneBlock18WP");
    gvar103 = NoxGetWaypoint("GeneratorPusher01WP");
    gvar104 = NoxGetWaypoint("GeneratorPusher02WP");
    gvar105 = NoxGetWaypoint("GeneratorPusher03WP");
    gvar106 = NoxGetWaypoint("GeneratorPusher04WP");
    gvar107 = NoxGetWaypoint("StoneBlockAudioOrigin01");
    gvar108 = NoxGetWaypoint("StoneBlockAudioOrigin02");
    gvar109 = NoxGetWaypoint("StoneBlockAudioOrigin03");
    gvar110 = NoxGetWaypoint("StoneBlockAudioOrigin04");
    gvar111 = NoxGetWaypoint("StoneBlockAudioOrigin05");
    gvar112 = NoxGetWaypoint("StoneBlockAudioOrigin06");
    gvar113 = NoxGetWaypoint("TempleEntranceAudioOrigin");
    gvar114 = NoxGetWallGroup("BlockGroup01Walls");
    gvar115 = NoxGetWallGroup("BlockGroup02Walls");
    gvar116 = NoxGetWallGroup("ElevatorWallgroup01");
    gvar117 = NoxGetWallGroup("ElevatorWallgroup02");
    gvar118 = NoxGetWallGroup("ElevatorWallgroup03");
    gvar119 = NoxGetWallGroup("ElevatorWallgroup04");
    gvar120 = NoxGetWallGroup("GeneratorPusherWalls");
    NoxMove(gvar70, gvar105);
    NoxMove(gvar71, gvar106);
}
void ActivateBlockGroup01()
{
    NoxObjectOff(SELF);
    NoxMove(gvar50, gvar83);
    NoxMove(gvar51, gvar84);
    NoxWallGroupOpen(gvar114);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar107);
}
void ActivateBlockGroup02()
{
    NoxObjectOff(SELF);
    NoxPrintMessage("GeneralPrint:MsgSeWallOpen");
    NoxMove(gvar52, gvar85);
    NoxMove(gvar53, gvar86);
    NoxMove(gvar54, gvar87);
    NoxWallGroupOpen(gvar115);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar108);
}

void OpenElevator01Walls()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar116);
}

void OpenPitSecretWall()
{
    if (NoxIsCaller(gvar53))
    {
        NoxObjectOff(SELF);
        NoxWallOpen(NoxWall(164, 150));
        NoxWallOpen(NoxWall(163, 151));
        NoxWallOpen(NoxWall(162, 152));
    }
}

void ActivateStoneBlock06()
{
    NoxObjectOff(SELF);
    NoxMove(gvar55, gvar88);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar109);
}

void ActivateStoneBlock07()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(157, 139));
    NoxWallOpen(NoxWall(143, 151));
    NoxMove(gvar56, gvar89);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar88);
}

void ActivateStoneBlockGroup03()
{
    NoxObjectOff(SELF);
    NoxMove(gvar57, gvar90);
    NoxMove(gvar58, gvar91);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar90);
}

void ActivateSpikeBlock01()
{
    NoxObjectOff(SELF);
    NoxMove(gvar45, gvar79);
    gvar42 = gvar41;
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar78);
    NoxFrameTimer(gvar43, &SpikeBlock01Loop);
}

void OpenElevatorWallgroup02()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar117);
}

void ActivateGeneratorHallPushers()
{
    if (!gvar44)
    {
        NoxObjectGroupOff(gvar77);
        gvar44 = TRUE;
        NoxWallGroupOpen(gvar120);
        NoxMove(gvar68, gvar103);
        NoxMove(gvar69, gvar104);
    }
}

void OpenElevatorWallgroup03()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar118);
}

void GeneratorPusher01Die()
{
    NoxObjectOff(gvar72);
}

void GeneratorPusher02Die()
{
    NoxObjectOff(gvar73);
}

void GeneratorPusher03Die()
{
    NoxObjectOff(gvar74);
}

void GeneratorPusher04Die()
{
    NoxObjectOff(gvar75);
}

void OpenTempleEntrance()
{
    NoxMove(gvar63, gvar96);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar113);
    NoxPlayAudioByIndex(SOUND_BigGong, gvar113);
}

void EnableTempleEntrance()
{
    if (NoxIsCaller(gvar63))
    {
        NoxObjectOff(SELF);
        NoxObjectOn(gvar49);
    }
}
void ActivateBlockGroup04Phase1()
{
    NoxObjectOff(SELF);
    NoxMove(gvar64, gvar97);
    NoxMove(gvar65, gvar98);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar112);
    ActivateSpikeBlockGroup01();
}

void EnableBlockGroup04Phase2()
{
    NoxObjectGroupOn(gvar76);
}

void ActivateBlockGroup04Phase2()
{
    NoxObjectGroupOff(gvar76);
    NoxMove(gvar64, gvar99);
    NoxMove(gvar65, gvar100);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar99);
    NoxFrameTimer(150, &EnableBlockGroup04Phase2);
}

void OpenElevatorWallgroup04()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar119);
}

void ActivateStoneBlockGroup05()
{
    NoxObjectOff(SELF);
    NoxMove(gvar66, gvar101);
    NoxMove(gvar67, gvar102);
    NoxPlayAudioByIndex(SOUND_SpikeBlockMove, gvar111);
}

static void FONTrap01Loop()
{
    if (gvar121)
    {
        NoxCastSpellObjectLocation(SPELL_FORCE_OF_NATURE, gvar122, NoxGetWaypointX(gvar125), NoxGetWaypointY(gvar125));
        NoxSecondTimer(5, &FONTrap01Loop);
    }
}

static void FireFONTrap02()
{
    NoxCastSpellObjectLocation(SPELL_FORCE_OF_NATURE, gvar123, NoxGetWaypointX(gvar126), NoxGetWaypointY(gvar126));
    NoxCastSpellObjectLocation(SPELL_FORCE_OF_NATURE, gvar124, NoxGetWaypointX(gvar127), NoxGetWaypointY(gvar127));
    NoxSecondTimer(5, &FireFONTrap02);
}

static void InitializeFONtraps()
{
    gvar122 = NoxGetObject("FON_Origin01");
    gvar123 = NoxGetObject("FON_Origin02");
    gvar124 = NoxGetObject("FON_Origin03");
    gvar125 = NoxGetWaypoint("FON_Target01");
    gvar126 = NoxGetWaypoint("FON_Target02");
    gvar127 = NoxGetWaypoint("FON_Target03");
}

void FireFONTrap01()
{
    NoxObjectOff(SELF);
    NoxPrintMessage("GeneralPrint:MsgTrapOn");
    gvar121 = TRUE;
    NoxCastSpellObjectLocation(SPELL_FORCE_OF_NATURE, gvar122, NoxGetWaypointX(gvar125), NoxGetWaypointY(gvar125));
    NoxSecondTimer(5, &FONTrap01Loop);
}

void DisableFONTrap01()
{
    gvar121 = FALSE;
}

void OpenGoldKeyExitWall()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(94, 152));
}

void ActivateFONtrap()
{
    NoxObjectOff(SELF);
    FireFONTrap02();
}

static void InitializeMaze()
{
    gvar130 = NoxGetObject("MazeSpike01");
    gvar131 = NoxGetObject("MazeSpike02");
    gvar132 = NoxGetObject("MazeSpike03");
    gvar133 = NoxGetWaypoint("MazeSpike01WP");
    gvar134 = NoxGetWaypoint("MazeSpike02WP");
    gvar135 = NoxGetWaypoint("MazeSpike03WP");
}

static void InitializeMiscRooms()
{
    gvar136 = NoxGetWallGroup("SnipeRoomWalls");
    gvar138 = NoxGetWallGroup("SnipeRoomExitWalls");
    gvar137 = NoxGetWallGroup("PusherRoomExitWalls");
}

static void InitializeTemple()
{
    gvar142 = NoxGetWallGroup("ElevatorWallgroup05");
    gvar143 = NoxGetWallGroup("BossRoomSecretWalls");
    gvar144 = NoxGetWallGroup("FireRoomWalls");
}

static void InitializeGoodiePits()
{
    char buff[128];
    int var0 = 0;
    
    CopyString("GoodiePit", buff);
    int length = StringGetLength(buff);

    while (var0 < 32)
    {
        CopyString(IntToString(var0 + 1), buff + length);
        gvar139[var0] = NoxGetObject(buff);
        var0 += 1;
    }
    gvar140 = NoxGetWallGroup("GoodiePitExitWalls");
    gvar141 = NoxGetWallGroup("ElevatorWallgroup06");
}

static void InitializeGeneratorGrids()
{
    gvar145 = NoxGetObject("GenGrid01Generator01");
    gvar146 = NoxGetObject("GenGrid01Generator02");
    gvar147 = NoxGetObject("GenGrid01Generator03");
    gvar148 = NoxGetObject("GenGrid02Generator01");
    gvar149 = NoxGetObject("GenGrid02Generator02");
    gvar150 = NoxGetObject("GenGrid02Generator03");
    gvar151 = NoxGetObject("GenGrid02Generator04");
    gvar152 = NoxGetObject("GenGrid02Generator05");
    gvar153 = NoxGetObject("GenGrid02Generator06");
    gvar154 = NoxGetObject("GenGrid02Generator07");
    gvar155 = NoxGetObject("GenGrid03Generator01");
    gvar156 = NoxGetObject("GenGrid03Generator02");
    gvar157 = NoxGetObject("GenGrid03Generator03");
    gvar158 = NoxGetObject("GenGrid02Golem");
    NoxObjectOff(gvar145);
    NoxObjectOff(gvar146);
    NoxObjectOff(gvar147);
    NoxObjectOff(gvar149);
    NoxObjectOff(gvar150);
    NoxObjectOff(gvar151);
    NoxObjectOff(gvar152);
    NoxObjectOff(gvar153);
    NoxObjectOff(gvar154);
    NoxObjectOff(gvar156);
    NoxObjectOff(gvar157);
}

void WellOfRestoration()
{
    NoxRestoreHealth(OTHER, NoxMaxHealth(OTHER) - NoxCurrentHealth(OTHER));
    NoxPlayAudioByIndex(SOUND_RestoreHealth, gvar128);
    NoxPrintMessage("GeneralPrint:WellSignRefresh");
}

void WellOfRestoration2()
{
    NoxRestoreHealth(OTHER, NoxMaxHealth(OTHER) - NoxCurrentHealth(OTHER));
    NoxPlayAudioByIndex(SOUND_RestoreHealth, gvar129);
    NoxPrintMessage("GeneralPrint:WellSignRefresh");
}

void OpenSlidingGeneratorWall()
{
    NoxObjectOff(SELF);
    NoxWallOpen(NoxWall(137, 111));
}

void ActivateMazePath01()
{
    NoxObjectOff(SELF);
    NoxMove(gvar130, gvar133);
}

void ActivateMazePath02()
{
    NoxObjectOff(SELF);
    NoxMove(gvar131, gvar134);
}

void ActivateMazePath03()
{
    NoxObjectOff(SELF);
    NoxMove(gvar132, gvar135);
}

void OpenSnipeRoomWalls()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar136);
    NoxPrintMessage("GeneralPrint:MsgSeWallOpen");
}

void OpenSnipeRoomExitWalls()
{
    NoxWallGroupOpen(gvar138);
    NoxWallGroupClose(gvar137);
}

void OpenPusherRoomExitWalls()
{
    NoxWallGroupClose(gvar138);
    NoxWallGroupOpen(gvar137);
}

void OpenGoodiePitExitWalls()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar140);
}

void OpenElevatorWallgroup06()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar141);
}

#define OPEN_GOODIE_PIT(index) NoxObjectOn(gvar139[index-1])

#define MAKE_GOODIEPIT_FUNCTION(index) \
    static void GoodiePit##index() \
    {   \
        NoxObjectOff(SELF); \
        OPEN_GOODIE_PIT(index); \
    }   \

MAKE_GOODIEPIT_FUNCTION(1)
MAKE_GOODIEPIT_FUNCTION(2)
MAKE_GOODIEPIT_FUNCTION(3)
MAKE_GOODIEPIT_FUNCTION(4)
MAKE_GOODIEPIT_FUNCTION(5)
MAKE_GOODIEPIT_FUNCTION(6)
MAKE_GOODIEPIT_FUNCTION(7)
MAKE_GOODIEPIT_FUNCTION(8)
MAKE_GOODIEPIT_FUNCTION(9)
MAKE_GOODIEPIT_FUNCTION(10)
MAKE_GOODIEPIT_FUNCTION(11)
MAKE_GOODIEPIT_FUNCTION(12)
MAKE_GOODIEPIT_FUNCTION(13)
MAKE_GOODIEPIT_FUNCTION(14)
MAKE_GOODIEPIT_FUNCTION(15)
MAKE_GOODIEPIT_FUNCTION(16)
MAKE_GOODIEPIT_FUNCTION(17)
MAKE_GOODIEPIT_FUNCTION(18)
MAKE_GOODIEPIT_FUNCTION(19)
MAKE_GOODIEPIT_FUNCTION(20)
MAKE_GOODIEPIT_FUNCTION(21)
MAKE_GOODIEPIT_FUNCTION(22)
MAKE_GOODIEPIT_FUNCTION(23)
MAKE_GOODIEPIT_FUNCTION(24)
MAKE_GOODIEPIT_FUNCTION(25)
MAKE_GOODIEPIT_FUNCTION(26)
MAKE_GOODIEPIT_FUNCTION(27)
MAKE_GOODIEPIT_FUNCTION(28)
MAKE_GOODIEPIT_FUNCTION(29)
MAKE_GOODIEPIT_FUNCTION(30)
MAKE_GOODIEPIT_FUNCTION(31)
MAKE_GOODIEPIT_FUNCTION(32)

#undef MAKE_GOODIEPIT_FUNCTION
#undef OPEN_GOODIE_PIT

void OpenElevatorWallgroup05()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar142);
}
void OpenBossRoomWalls()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar143);
}
void OpenFireRoomWalls()
{
    NoxObjectOff(SELF);
    NoxWallGroupOpen(gvar144);
}

void HiddenElvOn()
{
    int elev = NoxGetObject("HiddenElevator");

    if (!NoxIsObjectOn(elev) && elev)
    {
        NoxPlayAudioByIndex(SOUND_BigGong, 376);
        NoxObjectOn(elev);
    }
}

void GenGrid01Front01Destroy()
{
    NoxObjectOn(gvar145);
}
void GenGrid01Front02Destroy()
{
    NoxObjectOn(gvar146);
}
void GenGrid01Front03Destroy()
{
    NoxObjectOn(gvar147);
}
void GenGrid01Back01Destroy()
{
    NoxObjectOn(gvar146);
}
void GenGrid01Back02Destroy()
{
    NoxObjectOn(gvar145);
    NoxObjectOn(gvar147);
}
void GenGrid01Back03Destroy()
{
    NoxObjectOn(gvar146);
}
void GenGrid02Gen01Destroy()
{
    NoxObjectOn(gvar149);
}
void GenGrid02Gen02Destroy()
{
    NoxObjectOn(gvar150);
}
void GenGrid02Gen03Destroy()
{
    NoxObjectOn(gvar151);
}
void GenGrid02Gen04Destroy()
{
    NoxObjectOn(gvar152);
}
void GenGrid02Gen05Destroy()
{
    NoxObjectOn(gvar153);
}
void GenGrid02Gen06Destroy()
{
    NoxObjectOn(gvar154);
}
void GenGrid02Gen07Destroy()
{
    NoxAggressionLevel(gvar158, 0.8299999833106995f);
}
void GenGrid03Gen01Destroy()
{
    NoxObjectOn(gvar156);
}
void GenGrid03Gen02Destroy()
{
    NoxObjectOn(gvar155);
    NoxObjectOn(gvar157);
}
void GenGrid03Gen03Destroy()
{
    NoxObjectOn(gvar156);
}

void OnInitialMap()
{
    NoxMusicEvent();
    InitializeArrowTraps();
    InitializeBlocks();
    InitializeMaze();
    InitializeMiscRooms();
    InitializeFONtraps();
    InitializeTemple();
    InitializeGoodiePits();
    InitializeGeneratorGrids();
    gvar128 = NoxGetWaypoint("WellWP");
    gvar129 = NoxGetWaypoint("WellWP2");
}

void OnExitMap()
{
    NoxMusicEvent();
}

void StartFireway()
{
    NoxObjectOff(SELF);
    EnableFirewayTrap();
}

void StopFireway()
{
    NoxObjectOff(SELF);
    DisableFirewayTrap();
    NoxPrintMessage("GeneralPrint:MsgTrapOff");
}

void SurpriseTriggered()
{
    NoxObjectOff(SELF);
    for (int i = 0 ; i < 3 ; i++)
    {
        NoxWallOpen( NoxWall(110+i, 34+i) );
        NoxWallBreak( NoxWall(111+i, 33+i));
        NoxWallBreak( NoxWall(112+i, 32+i));
    }
    NoxPlayFX(FX_JIGGLE, NoxGetObjectX(SELF), NoxGetObjectY(SELF), 33.0f, 0);
}
