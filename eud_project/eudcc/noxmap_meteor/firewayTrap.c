
#include "firewayTrap.h"
#include "../include/noxobject.h"
#include "../include/builtins.h"

static int s_firewaySpot[4];
static char *s_firewayName = "InvisibleLightBlueLow";
static char *s_flameName = "FireGrateFlame";
static int s_enabledTrap = FALSE;

static void startFireway();
static void stopFireway();

static void oneShotloop(int count)
{
    static uint32_t length = sizeof(s_firewaySpot)/sizeof(int);

    for (int i = 0 ; i < length ; i ++)
    {
        NoxDeleteObjectTimer(CreateObjectAt(s_flameName, NoxGetObjectX(s_firewaySpot[i]), NoxGetObjectY(s_firewaySpot[i])), 12);
        TeleportObjectVector(s_firewaySpot[i], 23.0f, -23.0f);
    }
    if (count)
        NoxFrameTimerWithArg(4, count - 1, &oneShotloop);
    else
        stopFireway();
}

static void stopFireway()
{
    for (int i = 0 ; i < sizeof(s_firewaySpot)/sizeof(int) ; i ++)
        NoxDeleteObject(s_firewaySpot[i]);

    if (s_enabledTrap)
        NoxSecondTimer(2, &startFireway);
}

static void startFireway()
{
    s_firewaySpot[0] = CreateObject(s_firewayName, 207);
    s_firewaySpot[1] = CreateObject(s_firewayName, 209);
    s_firewaySpot[2] = CreateObject(s_firewayName, 478);
    s_firewaySpot[3] = CreateObject(s_firewayName, 479);
    NoxFrameTimerWithArg(3, 10, &oneShotloop);
}

void EnableFirewayTrap()
{
    if (s_enabledTrap)
        return;

    s_enabledTrap = TRUE;
    NoxSecondTimer(2, &startFireway);
}

void DisableFirewayTrap()
{
    s_enabledTrap = FALSE;
}
