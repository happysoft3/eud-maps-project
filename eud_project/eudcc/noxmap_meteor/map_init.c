
#include "../include/builtins.h"
#include "map_scenario.h"

void MapInitialize()
{
    BuiltinsInitialize();
    OnInitialMap();
}

void MapExit()
{
    OnExitMap();
}

