
#include "test_monscr.h"
#include "../include/stringUtil.h"

static MonsterBinScriptTable s_fireSpriteBin;
MonsterBinScriptTable *s_pFirespriteBinPtr = &s_fireSpriteBin;

void InitializeBinScript()
{
    s_pFirespriteBinPtr->speed=50;
    s_pFirespriteBinPtr->mis_range = 300.0f;
    s_pFirespriteBinPtr->minMissileDelay = 13;
    s_pFirespriteBinPtr->maxMissileDelay = 21;
    s_pFirespriteBinPtr->mis_attackFrame = 0;
    s_pFirespriteBinPtr->run_multiplier = 1.0f;
    s_pFirespriteBinPtr->deadEvent = 5545472;
    s_pFirespriteBinPtr->unit_flag = 65537;
    s_pFirespriteBinPtr->hp = 100;
    CopyString("WeakFireball", s_pFirespriteBinPtr->missile_name);
}
