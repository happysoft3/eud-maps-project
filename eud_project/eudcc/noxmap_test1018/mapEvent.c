
#include "mapEvent.h"
#include "../include/builtins.h"

static int s_firstunit;
static int s_lastunit;

#define P_TO_STRING_BUFFER 64
static int charPointerToNoxString(char *ptr)
{
	static int arr[P_TO_STRING_BUFFER];
    static int cursor=5;

	if (cursor >= P_TO_STRING_BUFFER)
		cursor = 0;

	arr[cursor] = (int)ptr;
	return ((int)(&arr[cursor++]) - 0x97bb40) >> 2;
}
#undef P_TO_STRING_BUFFER

void TestTest22()
{
	s_firstunit=NoxGetObject("firstscan");
    s_lastunit = CreateObject("Diamond", 1);
}

void OnInitialMap()
{
    // g_preservLoop = TRUE;
    // NoxFrameTimerWithArg(90, 0, &loopSpawnGoldInRoom);
}

