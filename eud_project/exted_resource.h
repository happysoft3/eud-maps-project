

#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"

void EnableTileTexture(int enabled)
{
    //0x5acd50
    int *p = 0x5acd50;

    if (p[0]==enabled)
        return;
        
    char code[]={0xB8, 0x15, 0xB7, 0x4C, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC3,};

    invokeRawCode(code, NULLPTR);
    
}

void onUpdateTileChanged()
{
    EnableTileTexture(FALSE);
    // SolidTile(628, 6,0x07FA);
    EnableTileTexture(TRUE);
}
void GRPDumpTileSet(){}

void initializeCustomTileset(int imgVector){
    int *pack=GetScrCodeField(GRPDumpTileSet)+4;
    int *off=&pack[1];
    int frames[]={pack+off[0],pack+off[1], pack+off[2],pack+off[3],pack+off[4],pack+off[5],};

    AppendImageFrame(imgVector, frames[0], 9694);
    AppendImageFrame(imgVector, frames[1], 9695);
    AppendImageFrame(imgVector, frames[2], 9696);
    AppendImageFrame(imgVector, frames[3], 8747);
    AppendImageFrame(imgVector, frames[4], 8748);
    AppendImageFrame(imgVector, frames[5], 8749);
    onUpdateTileChanged();
}

void GRPDump_FemaleBeachGirl(){}

#define FEMALE_BEACH_GIRL_IMAGE_ID 129717

void initializeImageFrameBeachGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_FemaleBeachGirl)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameAssign8Directions(0, FEMALE_BEACH_GIRL_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  FEMALE_BEACH_GIRL_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10, FEMALE_BEACH_GIRL_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15, FEMALE_BEACH_GIRL_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20, FEMALE_BEACH_GIRL_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,FEMALE_BEACH_GIRL_IMAGE_ID+40,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 114160
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_MECHANICAL_GOLEM;
    int xyInc[]={0,-30};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount-4);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}

void GRPDump_MaidenGhostOutput(){}
#define MAIDENGHOST_OUTPUT_BASE_IMAGE_ID 114546
void initializeImageFrameMaidenGhost(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GHOST;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MaidenGhostOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign8Directions(10, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign8Directions(15, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign8Directions(20, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);
    AnimFrameAssign8Directions(25, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_IDLE, 5);
    AnimFrameAssign8Directions(30, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_IDLE, 6);
    AnimFrameAssign8Directions(35, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_IDLE, 7);

    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void InitializeResources()
{
    int vec=CreateImageVector(1024);

    initializeCustomTileset(vec);
    initializeImageFrameBeachGirl(vec);
    initializeImageFrameBillyOni(vec);
    initializeImageFrameMaidenGhost(vec);
    DoImageDataExchange(vec);
}

