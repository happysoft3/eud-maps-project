
#include "libs/define.h"

#include "libs\typecast.h"
#include "libs\callmethod.h"
#include "libs\unitutil.h"
#include "libs\unitstruct.h"
#include "libs\printutil.h"

#include "libs\coopteam.h"
#include "libs\username.h"

#include "libs\waypoint.h"

#include "libs\fxeffect.h"
#include "libs\itemproperty.h"

#include "libs\mathlab.h"

#define self -2
#define other -1

int YOUR_LIFE = 20;
int player[20];
int AREA = 1;
int BOSS_TIMER[5];
float pi = 3.1415926535;
int RATS[100];
float D_RAND[3];

#define GROUP_ixjumpwalls 0
#define GROUP_ixrestroomwalls 3

void WispDeathFx(float xProfile, float yProfile)
{
    int unit = CreateObjectAt("WillOWisp", xProfile, yProfile);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int Boss1MagicalZone(int unit, int count)
{
    float xProfile = UnitAngleCos(unit, 31.0), yProfile = UnitAngleSin(unit, 31.0);
    int ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit) + xProfile, GetObjectY(unit) + yProfile), i;

    LookWithAngle(ptr, count);
    for (i = 0 ; i < count ; i ++)
    {
        DrawMagicIcon(GetObjectX(ptr), GetObjectY(ptr));
        MoveObject(ptr, GetObjectX(ptr) + xProfile, GetObjectY(ptr) + yProfile);
    }
    return ptr;
}

void RemoveMagicalZone(int ptr)
{
    int count = GetDirection(ptr), i;

    for (i = 0 ; i <= count ; i ++)
    {
        WispDeathFx(GetObjectX(ptr + i), GetObjectY(ptr + i));
        Delete(ptr + i);
    }
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		
		arr[37] = 1701996870; arr[38] = 1819042146;
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link = &arr;
	}
	return link;
}

int Bear2BinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50;
		arr[16] = 20000; arr[17] = 90; arr[18] = 100; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 65545; arr[24] = 1067450368; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30;
		arr[58] = 5547856; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int FireSpriteBinTable()
{
	int arr[62], link, unit;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
        arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = &arr;
	}
	return link;
}

int DrawMagicIcon(float x, float y)
{
    int unit = CreateObjectAt("AirshipBasketShadow", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, 1416);
    return unit;
}

int DrawAbilityIcon(int wp)
{
    int unit = CreateObject("AirshipBasketShadow", wp);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, 1417);
    return unit;
}

void MapInitialize()
{
    PlayerFlag(-1);
    
    BOSS_TIMER[0] = 2;
    D_RAND[0] = -27.0;
    D_RAND[1] = 27.0;
    D_RAND[2] = -10.0;
    fonStatue(-1);
    westArrowTrap(-1);
    initArea1();

    //delay_run
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(29, initDecoretion);
    FrameTimer(200, startupMent);

    //loop_run
    FrameTimer(3, LoopSearchIndex);
    FrameTimer(4, LoopPreservePlayer);
}

void MapExit()
{
    // SetMemory(0x5cb394, 6075528);
    // SetMemory(0x5cb3a0, 6075544);
    // SetMemory(0x5cb3b8, 6075580);
    // SetMemory(0x5cb3ac, 6075560);
    // SetMemory(0x5cb3c4, 0x5cb4c8);
    RemoveCoopTeamMode();
}

void startupMent()
{
    UniPrintToAll("맵 이름: I Wanna Be The Hero                - 제작자: 패닉");
    UniPrintToAll("녹스의 모든 맵을 두는 공간 blog.daum.net/ky10613");
    UniPrintToAll("미궁에서 탈출하는 맵입니다, 총 3개의 지역이 존재하며 각 지역마다 보스가 존재합니다.");
    UniPrintToAll("이 미궁안에서 생존자는 살짝만 스쳐도 죽게 됩니다, 다른 플레이어가 다음 지역으로 이동해야만 부활 되므로 신중히 플레이 하십시오.");
    UniPrintToAll("이 맵은 오로지 데스레이만 쏠 수 있도록 설계되어 있습니다, 따라서 데스레이를 잘 쏘시는 마법사 분들께 이 맵을 바칩니다.");
    UniPrintToAll("긴 설명으로 넘어가버린 문구는 콘솔창(F1)을 누르시면 확인이 가능합니다.");
    deathStatue(-1);
    FrameTimer(30, DisplayLadderBoard);
}

void signMessage()
{
    UniPrint(other, "음... 불가능하게 만들고 싶지는 않았는데... 여긴 정말로 불가능해, 그러니 여기서 포기해 주었으면 해!");
}

void initDecoretion()
{
    int i;

    TeleportLocation(20, GetWaypointX(162), GetWaypointY(162));
    for (i = 15 ; i >= 0 ; i -= 1)
    {
        DrawMagicIcon(GetWaypointX(20), GetWaypointY(20));
        TeleportLocationVector(20, 23.0, -23.0);
    }
    SignDescriptionSet();
    FrameTimer(1, area2Decoretion);
    FrameTimer(3, GauntletInitSupport);
}

void area2Decoretion()
{
    int i;

    for (i = 15 ; i >= 0 ; i -= 1)
    {
        DrawAbilityIcon(161);
        TeleportLocationVector(161, 23.0, 23.0);
    }
}

void GauntletInitSupport()
{
    FrameTimerWithArg(1, 19, PutOblivionGroup);
    FrameTimerWithArg(3, 175, PutOblivionGroup);
    FrameTimerWithArg(5, 179, PutOblivionGroup);
    FrameTimerWithArg(10, 176, PutOblivionGroup);
    FrameTimerWithArg(15, 177, PutOblivionGroup);
    FrameTimerWithArg(20, 178, PutOblivionGroup);
    UniPrintToAll("알림_ 방금 맵에 보급품 배치를 완료하였습니다.");
    UniPrintToAll("맵을 정상적으로 플레이 하시려면 '갑옷 및 무기 되살리기' 기능을 해제하십시오.");
}

void movingRotGroup()
{
    int i, order;

    if (AREA == 1)
    {
        for (i = 0 ; i < 9 ; i ++)
            Move(Object("movingRot" + IntToString(i * 2 + 1)), i * 2 + (order + 1));
        if (!order)
            order = 39;
        else
            order = 0;
        FrameTimer(40, movingRotGroupPart2);
    }
}

void movingRotGroupPart2()
{
    int i;
    int order;

    if (AREA == 1)
    {
        for (i = 0 ; i < 9 ; i ++)
            Move(Object("movingRot" + IntToString(i * 2 + 2)), i * 2 + (order + 2));
        if (!order)
            order = 39;
        else
            order = 0;
        FrameTimer(50, movingRotGroup);
    }
}

void removeWallArea1()
{
    int var_0, i;

    ObjectOff(self);
    if (var_0)
        return;
    MoveWaypoint(146, 1809.0, 1189.0);

    for (i = 15 ; i >= 0 ; i --)
    {
        DeleteObjectTimer(CreateObject("BigSmoke", 146), 7);
        MoveWaypoint(146, GetWaypointX(146) - 23.0, GetWaypointY(146) + 23.0);
    }
    WallGroupOpen(1);
    WallGroupBreak(2);

    var_0 = spawnZombie(174);
    LookAtObject(var_0, other);
    var_0 = spawnZombie(174);
    LookAtObject(var_0, other);

    summonEffect(CreateObject("InvisibleLightBlueHigh", 174));
}

int spawnZombie(int wp)
{
    int deadwalker = CreateObject("VileZombie", wp);

    SetUnitMaxHealth(deadwalker, 200);
    SetUnitScanRange(deadwalker, 450.0);
    SetCallback(deadwalker, 3, zombieAttack);
    SetCallback(deadwalker, 5, setDeaths);
    
    return deadwalker;
}

void spawnFlying(int wp)
{
    int fUnit = CreateObject("FlyingGolem", wp);
    
    SetUnitMaxHealth(fUnit, 500);
    SetUnitFlags(fUnit, GetUnitFlags(fUnit) ^ 0x4000); //remove 'AIRBORNE' flag
    Enchant(fUnit, "ENCHANT_SLOWED", 0.0);
    CreatureGuard(fUnit, GetObjectX(fUnit), GetObjectY(fUnit), GetObjectX(fUnit), GetObjectY(fUnit), 450.0);
    SetCallback(fUnit, 3, flyingAttack);
    SetCallback(fUnit, 5, setDeaths);
}

void summonEffect(int unit)
{
    if (!GetDirection(unit))
    {
        CastSpellObjectLocation("SPELL_SUMMON_VILE_ZOMBIE", unit, GetObjectX(unit), GetObjectY(unit));
        LookWithAngle(unit, 1);
        FrameTimerWithArg(10, unit, summonEffect);
    }
    else
        Delete(unit);
}

int fonStatue(int orderId)
{
    int statue[3];

    if (orderId == -1)
    {
        statue[0] = CreateObject("MovableStatueVictory4NW", 61);
        Enchant(statue[0], "ENCHANT_FREEZE", 0.0);
        statue[1] = CreateObject("MovableStatueVictory4NW", 62);
        Enchant(statue[1], "ENCHANT_FREEZE", 0.0);
        statue[2] = CreateObject("MovableStatueVictory4NW", 63);
        Enchant(statue[2], "ENCHANT_FREEZE", 0.0);
        return statue[0];
    }
    return statue[orderId];
}

void controlFonTraps()
{
    int ctlorder;

    if (AREA == 1)
    {
        if (ctlorder < 3)
        {
            CastSpellObjectLocation("SPELL_FORCE_OF_NATURE", fonStatue(ctlorder), GetObjectX(fonStatue(ctlorder)) - 40.0, GetObjectY(fonStatue(ctlorder)) - 40.0);
            ctlorder ++;
            FrameTimer(15, controlFonTraps);
        }
        else
        {
            ctlorder = 0;
            FrameTimer(130, controlFonTraps);
        }
    }
}

void area1finalTrp(int unit)
{
    float pos;
    int i = GetDirection(unit);

    if (!(i / 5))
        pos = 23.0;
    else
        pos = -23.0;
    explosionEffect(unit);
    MoveObject(unit, GetObjectX(unit) - pos, GetObjectY(unit) + pos);
    if (i % 5 == 4)
        MoveObject(unit, GetObjectX(unit) + (pos * 2.0), GetObjectY(unit));
    i ++;
    if (i == 10) i = 0;
    LookWithAngle(unit, i);
    if (AREA == 1)
        FrameTimerWithArg(7, unit, area1finalTrp);
}

void explosionEffect(int unit)
{
    int subUnit = CreateObjectAt("CarnivorousPlant", GetObjectX(unit), GetObjectY(unit));

    Frozen(subUnit, 1);
    SetCallback(subUnit, 9, deathTouched);
    DeleteObjectTimer(subUnit, 1);
    Effect("EXPLOSION", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    PlaySoundAround(subUnit, 882);
}

void deathTouched()
{
    if (CurrentHealth(other) > 0 && IsPlayerUnit(other))
        Damage(other, self, 150, 14);
}

void setDeaths()
{
    Effect("SPARK_EXPLOSION", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
    DeleteObjectTimer(self, 1);
}

void initArea1()
{
    Move(Object("movingFlame"), 21);
    AREA = 1;
    BOSS_TIMER[1] = 12;
    BOSS_TIMER[2] = 3;
    BOSS_TIMER[3] = 8;
    spawnZombie(65);
    spawnZombie(65);
    spawnZombie(66);
    spawnZombie(66);

    spawnFlying(64);
    spawnFlying(67);
    
    movingRotGroup();
    controlFonTraps();

    FrameTimerWithArg(10, CreateObject("InvisibleLightBlueHigh", 68), area1finalTrp);
    FrameTimerWithArg(12, CreateObject("InvisibleLightBlueHigh", 69), area1finalTrp);
    FrameTimerWithArg(14, CreateObject("InvisibleLightBlueHigh", 70), area1finalTrp);
}

void controlArea1BossPatton()
{
    int cpic = 0;

    if (CurrentHealth(getBoss(0)) && AREA == 1)
    {
        if (islookAtPlayer(getBoss(0)) >= 0)
        {
            cpic = Random(1, 3);
            if (cpic == 1)
            {
                MoveObject(getBoss(0), GetWaypointX(93), GetWaypointY(93));
                Frozen(getBoss(0), 1);
                //areaPatton11(getBoss(0));
                PredictPattern11BeforeAttack(getBoss(0));
            }
            else if (cpic == 2)
            {
                Enchant(getBoss(0), "ENCHANT_FREEZE", 1.8);
                FrameTimer(50, areaPatton12);
            }
            else
            {
                MoveObject(getBoss(0), GetWaypointX(93), GetWaypointY(93));
                Frozen(getBoss(0), 1);
                areaPatton13();
            }
        }
        SecondTimer(BOSS_TIMER[cpic], controlArea1BossPatton);
    }
}

int GetNealryPlayer(int unit)
{
    float dist, tempDist = 9999.0;
    int i, plr = -1;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        if (CurrentHealth(player[i]))
        {
            if (IsVisibleTo(player[i], unit))
            {
                dist = DistanceUnitToUnit(player[i], unit);
                if (dist < tempDist)
                {
                    tempDist = dist;
                    plr = i;
                }
            }
        }
    }
    return plr;
}

int DirToAngle(int num)
{
    return num * 45 / 32;
}

void PredictPattern11BeforeAttack(int unit)
{
    int plr = GetNealryPlayer(unit), ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit));

    SetOwner(unit, ptr);
    if (plr + 1)
    {
        LookAtObject(unit, player[plr]);
        FrameTimerWithArg(30, Boss1MagicalZone(unit, 12), RemoveMagicalZone);
        FrameTimerWithArg(24, ptr, areaPatton11);
    }
    else
    {
        LookWithAngle(unit, 0);
        FrameTimerWithArg(1, ptr, areaPatton11);
    }
    Raise(ptr, ToFloat(DirToAngle(GetDirection(unit))));
}

void areaPatton11(int ptr)
{
    int angle = ToInt(GetObjectZ(ptr)), mis, unit = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(unit) && count < 180)
    {
        MoveWaypoint(83, GetObjectX(unit) + MathSine(angle + 90, 17.0), GetObjectY(unit) + MathSine(angle, 17.0));
        mis = CreateObject("ImpShot", 83);
        SetOwner(unit, mis);
        PushObject(mis, 36.0, GetObjectX(unit), GetObjectY(unit));
        FrameTimerWithArg(1, ptr, areaPatton11);
        Raise(ptr, angle + 2);
        LookWithAngle(ptr, count + 1);
    }
    else
    {
        Frozen(unit, 0);
        Delete(ptr);
    }
}

void areaPatton12()
{
    int i;

    if (CurrentHealth(getBoss(0)) > 0)
    {
        for (i = 9 ; i >= 0 ; i --)
        {
            if (CurrentHealth(player[i]) > 0 && IsVisibleTo(getBoss(0), player[i]))
            {
                int shuriken = CreateObjectAt("OgreShuriken", GetObjectX(getBoss(0)) - UnitRatioX(getBoss(0), player[i], 20.0), GetObjectY(getBoss(0)) - UnitRatioY(getBoss(0), player[i], 20.0));
                SetOwner(getBoss(0), shuriken);
                Enchant(shuriken, "ENCHANT_SHOCK", 0.0);
                PushObject(shuriken, -20.0, GetObjectX(player[i]), GetObjectY(player[i]));
            }
        }
        Frozen(getBoss(0), 0);
    }
}

void areaPatton13()
{
    int repeat;

    if (repeat < 48)
    {
        rhombusPut(83, 1207.0, 1669.0, 1231.0, 1694.0);
        FrameTimerWithArg(50, CreateObject("CharmOrb", 83), delayExplosion);
        ++repeat;
        FrameTimer(1, areaPatton13);
    }
    else
    {
        Frozen(getBoss(0), 0);
        repeat = 0;
    }
}

void delayExplosion(int unit)
{
    int fireboom = CreateObjectAt("StrongFireball", GetObjectX(unit), GetObjectY(unit));
    SetOwner(getBoss(0), fireboom);
    CreateObjectAt("barrel", GetObjectX(fireboom), GetObjectY(fireboom));
    Delete(unit);
}

int islookAtPlayer(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        if (CurrentHealth(player[i]) > 0 && IsVisibleTo(player[i], unit))
            return i;
    }
    return -1;
}

void entranceBossZoneA1()
{
    ObjectOff(self);
    MoveWaypoint(19, GetObjectX(other), GetObjectY(other));

    RetreatLevel(getBoss(CreateObject("Swordsman", 93)), 0.0);
    SetUnitMaxHealth(getBoss(0), 2000);
    CreatureGuard(getBoss(0), GetObjectX(getBoss(0)), GetObjectY(getBoss(0)), GetObjectX(getBoss(0)), GetObjectY(getBoss(0)), 500.0);
    SetCallback(getBoss(0), 5, area1BossDeath);
    SetCallback(getBoss(0), 3, findEnemy);

    UniPrintToAll("[!!] 제 1 구역_ 보스가 출현했습니다. (라이프 + 5)");
    AudioEvent("HecubahDieFrame283", 93);

    CreateObject("InvisibleLightBlueHigh", 93);
    MoveWaypoint(83, GetWaypointX(93) - 150.0, GetWaypointY(93) - 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(93) - 150.0, GetWaypointY(93) + 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(93) + 150.0, GetWaypointY(93) - 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(93) + 150.0, GetWaypointY(93) + 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    YOUR_LIFE += 5;
    FrameTimer(30, controlArea1BossPatton);
}

void testTeleport()
{
    MoveObject(other, GetWaypointX(163), GetWaypointY(163));
    MoveWaypoint(163, 4689.0, 2288.0);
}

void area1BossDeath()
{
    DeleteObjectTimer(self, 1);
    WallOpen(2228300);
    WallOpen(2293837);
    WallOpen(2359374);
	WallOpen(2424911);
	WallOpen(2490448);
    UniPrintToAll("제 1 구역 보스가 격추되었습니다.");
    UniPrintToAll("제 2 구역으로 연결된 비밀통로가 열립니다.");
}

void findEnemy()
{
    if (CurrentHealth(other) > 0)
    {
        CreatureFollow(self, other);
        AggressionLevel(self, 1.0);
    }
}

void flyingAttack()
{
    if (CurrentHealth(self))
    {
        if (IsVisibleTo(other, self) && !HasEnchant(self, "ENCHANT_DETECTING"))
        {
            int arrowshot = CreateObjectAt("GolemArrow", GetObjectX(self) - UnitRatioX(self, other, 17.0), GetObjectY(self) - UnitRatioY(self, other, 17.0));
            SetOwner(self, arrowshot);
            LookAtObject(arrowshot, other);
            PushObject(arrowshot, -20.0, GetObjectX(other), GetObjectY(other));
            Enchant(self, "ENCHANT_DETECTING", 2.0);
        }
        Enchant(self, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(self, 1.0);
    }
}

void zombieAttack()
{
    int urchinstone;

    if (CurrentHealth(self) > 0)
    {
        if (IsVisibleTo(other, self))
        {
            if (Distance(GetObjectX(self), GetObjectY(self), GetObjectX(other), GetObjectY(other)) < 100.0)
            {
                urchinstone = CreateObjectAt("ThrowingStone", GetObjectX(other), GetObjectY(other));
                SetOwner(self, urchinstone);
            }
            else
            {
                LookAtObject(self, other);
                Effect("DAMAGE_POOF", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
                PushObjectTo(self, UnitRatioX(self, other, D_RAND[2]), UnitRatioY(self, other, D_RAND[2]));
            }
        }
        Enchant(self, "ENCHANT_BLINDED", 0.08);
        AggressionLevel(self, 1.0);
    }
}

void spawnRats()
{
    int i;

    if (i < 100)
    {
        RATS[i] = CreateObject("rat", 85);
        Enchant(RATS[i], "ENCHANT_INVULNERABLE", 0.0);
        SetCallback(RATS[i], 9, deathTouched);
        i ++;
        FrameTimer(1, spawnRats);
    }
    else
        loopRatStats();
}

void loopRatStats()
{
    int order;
    int i;

    if (order == 100)
        order = 0;

    for (i = 0 ; i < 10 ; i += 1)
    {
        if (HasEnchant(RATS[order], "ENCHANT_HELD"))
            EnchantOff(RATS[order], "ENCHANT_HELD");
        order ++;
    }
    if (AREA == 2)
        FrameTimer(1, loopRatStats);
}

void getArea2()
{
    ObjectOff(self);
    initArea2();
}

void getArea3()
{
    ObjectOff(self);
    initArea3();
}

static void drawMagicIconMultiple(int locationId, int count, float *xyVector)
{
    while (--count >= 0)
    {
        DrawMagicIcon(LocationX(locationId), LocationY(locationId));
        TeleportLocationVector(locationId, xyVector[0], xyVector[1]);
    }
}

static void deferredInitArea2()
{
    float xyvect[] = { 46.0, -46.0};

    drawMagicIconMultiple(182, 8, &xyvect);
    drawMagicIconMultiple(183, 8, &xyvect);
    drawMagicIconMultiple(184, 8, &xyvect);
    drawMagicIconMultiple(185, 8, &xyvect);
    drawMagicIconMultiple(186, 8, &xyvect);
    drawMagicIconMultiple(187, 8, &xyvect);
    drawMagicIconMultiple(188, 8, &xyvect);
}

void initArea2()
{
    FrameTimer(1, deferredInitArea2);
    AREA = 2;
    D_RAND[2] = -3.0;
    BOSS_TIMER[1] = 12;
    BOSS_TIMER[2] = 12;
    BOSS_TIMER[3] = 15;
    BOSS_TIMER[4] = 15;
    putArrowTower(0);
    spawnGuardTower(87);
    spawnRats();
    spawnSpirit(91);
    spawnSpirit(94);
    spawnSpirit(95);
    Move(Object("area2block1"), 101);
    Move(Object("area2block2"), 104);
    Move(Object("area2block3"), 106);
    FrameTimer(75, delayFlameTrp);
    MoveWaypoint(19, GetObjectX(self), GetObjectY(self));
    spawnSpirit(180);
    spawnSpirit(181);
}

void initArea3()
{
    AREA = 3;
    LockDoor(Object("thirdBossGate"));
    BOSS_TIMER[1] = 7;
    BOSS_TIMER[2] = 15;
    BOSS_TIMER[3] = 15;
    BOSS_TIMER[4] = 15;
    spawnWizard(118);
    spawnSpirit(119);
    spawnSpirit(120);
    spawnSpirit(121);
    spawnFister(122);
    spawnFister(123);
    SpawnMonsterGirl(136);
    SpawnMonsterGirl(136);
    spawnSpirit(137);
    spawnSpirit(138);
    spawnGuardTower(140);
    spawnGuardTower(141);
    spawnWizard(142);
    putGlyph();
    FrameTimer(1, settingFlameMazeLeft);
    FrameTimer(1, settingFlameMazeRight);
    MoveWaypoint(19, GetObjectX(self), GetObjectY(self));
}

void putGlyph()
{
    int i;

    for (i = 2 ; i >= 0 ; i --)
    {
        spawnGlyph(139, "SPELL_CLEANSING_FLAME", "NULL", "NULL");
        TeleportLocationVector(139, 114.0, -114.0);
    }
    spawnGlyph(164, "SPELL_CLEANSING_FLAME", "NULL", "NULL");
}

int westArrowTrap(int order)
{
    int arrwTraps[9];

    if (order == -1)
    {
        arrwTraps[0] = Object("deathArrowPointer");
        ObjectOff(arrwTraps[0]);
        int i;
        for (i = 1 ; i < 9 ; i ++)
        {
            arrwTraps[i] = arrwTraps[0] + (i * 2);
            ObjectOff(arrwTraps[i]);
        }
        return arrwTraps[0];
    }
    return arrwTraps[order];
}

void enableDeathArrows()
{
    int i;

    for (i = 8; i >= 0 ; i -= 1)
        ObjectOn(westArrowTrap(i));
    FrameTimer(1, disableDeathArrows);
}

void disableDeathArrows()
{
    int i;

    for (i = 8 ; i >= 0 ; i --)
        ObjectOff(westArrowTrap(i));
}

void settingFlameMazeLeft()
{
    string trapname;
    int i;

    CreateObject("MovableStatueVictory3SW", 135);
    MoveWaypoint(135, GetWaypointX(135) - 13.0, GetWaypointY(135) + 13.0);
    CreateObject("skull3", 135);
    CreateObject("MovableStatueVictory3NE", 134);
    MoveWaypoint(134, GetWaypointX(134) + 13.0, GetWaypointY(134) - 13.0);
    CreateObject("skull2", 134);

    for (i = 0 ; i < 7 ; i += 1)
    {
        if (i == 3) trapname = "LargeFlame";
        else trapname = "RotatingSpikes";
        CreateObject(trapname, 124);
        CreateObject(trapname, 126);
        CreateObject(trapname, 128);
        CreateObject(trapname, 130);
        CreateObject(trapname, 132);
        TeleportLocationVector(124, 23.0, 23.0);
        TeleportLocationVector(126, 23.0, 23.0);
        TeleportLocationVector(128, 23.0, 23.0);
        TeleportLocationVector(130, 23.0, 23.0);
        TeleportLocationVector(132, 23.0, 23.0);
    }
}

void settingFlameMazeRight()
{
    string trapname;
    int i;

    for (i = 0 ; i < 7 ; i += 1)
    {
        if (i == 4) trapname = "LargeFlame";
        else trapname = "RotatingSpikes";
        CreateObject(trapname, 125);
        CreateObject(trapname, 127);
        CreateObject(trapname, 129);
        CreateObject(trapname, 131);
        CreateObject(trapname, 133);
        TeleportLocationVector(125, -23.0, -23.0);
        TeleportLocationVector(127, -23.0, -23.0);
        TeleportLocationVector(129, -23.0, -23.0);
        TeleportLocationVector(131, -23.0, -23.0);
        TeleportLocationVector(133, -23.0, -23.0);
    }
}

void disableDoorLock()
{
    ObjectOff(self);
    UniPrintToAll("맞은편 문의 잠금이 해제되었습니다.");
    UnlockDoor(Object("thirdBossGate"));
}

int SpawnMonsterGirl(int location)
{
    int unit = CreateObject("Bear2", location);

    ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
    UnitLinkBinScript(unit, Bear2BinTable());
    SetUnitMaxHealth(unit, 600);
    SetCallback(unit, 5, setDeaths);
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit), GetObjectY(unit), 450.0);

    return unit;
}

int spawnSpirit(int wp)
{
    int unit = CreateObject("FireSprite", wp);
    
    UnitLinkBinScript(unit, FireSpriteBinTable());
    SetUnitVoice(unit, 57);    //brief: Bomber Voice
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    SetUnitMaxHealth(unit, 400);
    SetCallback(unit, 3, spritWeapon);
    SetCallback(unit, 5, setDeaths);
    SetUnitScanRange(unit, 450.0);
    return unit;
}

int spawnWizard(int wp)
{
    int unit = CreateObject("StrongWizardWhite", wp);

    UnitLinkBinScript(unit, StrongWizardWhiteBinTable());
    SetUnitMaxHealth(unit, 700);
    SetCallback(unit, 3, wizardAttack);
    SetCallback(unit, 5, setDeaths);
    SetUnitScanRange(unit, 450.0);

    return unit;
}

int spawnFister(int wp)
{
    int unit = CreateObject("StoneGolem", wp);

    SetUnitMaxHealth(unit, 600);
    SetCallback(unit, 3, fistAttack);
    SetCallback(unit, 5, setDeaths);
    SetUnitScanRange(unit, 450.0);

    return unit;
}

void putArrowTower(int order)
{
    int thetower;

    if (order < 4)
    {
        thetower = Object("area2Tower" + IntToString(order + 1));
        spawnArrowTower(CreateObject("InvisibleLightBlueLow", order + 96), GetObjectX(thetower), GetObjectY(thetower));
        FrameTimerWithArg(20, order + 1, putArrowTower);
    }
    else
    {
        thetower = Object("area2Tower5");
        spawnArrowTower(CreateObject("InvisibleLightBlueLow", 99), GetObjectX(thetower), GetObjectY(thetower));
    }
}

void spawnArrowTower(int unit, float pos_x, float pos_y)
{
    int subUnit = CreateObjectAt("maiden", pos_x, pos_y);

    LookWithAngle(subUnit, 0);
    Frozen(subUnit, 1);
    SetCallback(subUnit, 9, area2ArrowTower);
    CreateObjectAt("SmallStoneBlock", GetObjectX(subUnit), GetObjectY(subUnit));
    Frozen(subUnit + 1, 1);
    LookAtObject(subUnit + 1, unit);
}

void area2ArrowTower()
{
    if (IsCaller(GetTrigger() + 1))
    {
        if (GetDirection(self) < 32)
            LookWithAngle(self, GetDirection(self) + 1);
        else
        {
            int towerMissile = CreateObjectAt("CherubArrow", GetObjectX(self) + UnitAngleCos(GetTrigger() + 1, 17.0), GetObjectY(self) + UnitAngleSin(GetTrigger() + 1, 17.0));

            LookWithAngle(towerMissile, GetDirection(GetTrigger() + 1));
            PushObject(towerMissile, 20.0, GetObjectX(self), GetObjectY(self));
            LookWithAngle(self, 0);
        }
    }
    MoveObject(self, GetObjectX(self), GetObjectY(self));
}

void MechGolemOnDeath()
{
    DeleteObjectTimer(self, 1);
    Delete(GetTrigger() + 1);
}

void summonGolem()
{
    summonEffect(CreateObject("InvisibleLightBlueLow", 92));
    int unit = CreateObject("MechanicalGolem", 92);

    SetUnitMaxHealth(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)) - 1, 800);
    Enchant(unit, "ENCHANT_HASTED", 0.0);
    SetCallback(unit, 5, MechGolemOnDeath);
    ObjectOff(SELF);
}

void displayFlyingMonsters()
{
    int unit;

    ObjectOff(self);
    if (!unit)
    {
        unit = spawnSpirit(107);

        Raise(unit, 200.0);
        spawnSpirit(108);
        Raise(unit + 1, 200.0);
    }
}

void delayFlameTrp()
{
    int grate = CreateObject("FireGrateFlame", 90);

    Enchant(grate, "ENCHANT_FREEZE", 0.0);
    MoveObject(grate, GetWaypointX(109), GetWaypointY(109));
    CreateObject("FireGrateFlame", 90);
    Enchant(grate + 1, "ENCHANT_FREEZE", 0.0);
    MoveObject(grate + 1, GetWaypointX(110), GetWaypointY(110));
    AudioEvent("FireballCast", 109);

    if (AREA == 2)
        FrameTimer(75, delayFlameTrp);
}

void preserveGuardTower()
{
    int missile, i;

    if (IsCaller(GetTrigger() - 1))
    {
        if (GetDirection(self) < 40)
            LookWithAngle(self, GetDirection(self) + 1);
        else
        {
            for (i = 9 ; i >= 0 ; i -= 1)
            {
                if (CurrentHealth(player[i]) > 0 && IsVisibleTo(self, player[i]))
                {
                    missile = CreateObjectAt("DeathBallFragment", GetObjectX(self) - UnitRatioX(self, player[i], 40.0), GetObjectY(self) - UnitRatioY(self, player[i], 40.0));
                    PushObject(missile, -20.0, GetObjectX(player[i]), GetObjectY(player[i]));
                    DeleteObjectTimer(missile, 20);
                }
            }
            LookWithAngle(self, 0);
        }
    }
    MoveObject(self, GetObjectX(self), GetObjectY(self));
}

void spawnGuardTower(int wp)
{
    int guardTower = CreateObject("ImpGenerator", wp);

    ObjectOff(guardTower);
    Frozen(guardTower, 1);
    CreateObject("maiden", wp);
    LookWithAngle(guardTower + 1, 0);
    Frozen(guardTower + 1, 1);
    SetCallback(guardTower + 1, 9, preserveGuardTower);
}

void entranceBossZoneA2()
{
    ObjectOff(self);

    getBoss(CreateObject("SkeletonLord", 111));
    SetUnitMaxHealth(getBoss(0), 3000);
    SetCallback(getBoss(0), 5, area2BossDeath);
    SetCallback(getBoss(0), 3, findEnemy);
    CreatureGuard(getBoss(0), GetObjectX(getBoss(0)), GetObjectY(getBoss(0)), GetObjectX(getBoss(0)), GetObjectY(getBoss(0)), 500.0);
    AudioEvent("HecubahDieFrame283", 111);

    MoveWaypoint(19, GetObjectX(other), GetObjectY(other));

    CreateObject("InvisibleLightBlueHigh", 111);
    MoveWaypoint(83, GetWaypointX(111) - 150.0, GetWaypointY(111) - 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(111) - 150.0, GetWaypointY(111) + 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(111) + 150.0, GetWaypointY(111) - 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(111) + 150.0, GetWaypointY(111) + 150.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    YOUR_LIFE += 5;
    UniPrintToAll("[!!] 제 2 구역 보스가 출현했습니다 !!(라이프 +5)");
    FrameTimer(40, controlArea2BossPatton);
}

void area2BossDeath()
{
    DeleteObjectTimer(self, 1);
    WallOpen(8454201);
	WallOpen(8388666);
	WallOpen(8323131);
	WallOpen(8257596);
    UniPrintToAll("2 번째 보스가 격추되었습니다.");
    UniPrintToAll("3 구역으로 연결된 비밀통로가 열립니다.");
}

void controlArea2BossPatton()
{
    int cpic = 0;

    if (CurrentHealth(getBoss(0)) > 0 && AREA == 2)
    {
        if (islookAtPlayer(getBoss(0)) >= 0)
        {
            cpic = Random(1, 4);
            if (cpic == 1)
            {
                MoveObject(getBoss(0), GetWaypointX(111), GetWaypointY(111));
                Frozen(getBoss(0), 1);
                Effect("SENTRY_RAY", GetWaypointX(111), GetWaypointY(111), GetWaypointX(111) + 200.0, GetWaypointY(111) + 200.0);
                FrameTimer(80, areaPatton21);
            }
            else if (cpic == 2)
            {
                MoveObject(getBoss(0), GetWaypointX(111), GetWaypointY(111));
                Frozen(getBoss(0), 1);
                Effect("SENTRY_RAY", GetWaypointX(111), GetWaypointY(111), GetWaypointX(111) - 200.0, GetWaypointY(111) + 200.0);
                FrameTimer(80, areaPatton22);
            }
            else if (cpic == 3)
            {
                Frozen(getBoss(0), 1);
                areaPatton23();
            }
            else {
                areaPatton24();
            }
        }
        SecondTimer(BOSS_TIMER[cpic], controlArea2BossPatton);
    }
}

void areaPatton21()
{
    int repeat, missile;

    if (CurrentHealth(getBoss(0)) > 0 && repeat < 30)
    {
        int i;

        for (i = 0 ; i < 12 ; i ++)
        {
            missile = CreateObjectAt("LightningBolt", GetWaypointX(112) - IntToFloat(i * 46), GetWaypointY(112) + IntToFloat(i * 46));
            Enchant(missile, "ENCHANT_FREEZE", 0.0);
            SetOwner(getBoss(0), missile);
            LookWithAngle(missile, 160);
            PushObjectTo(missile, -19.0, -19.0);
        }
        repeat ++;
        FrameTimer(3, areaPatton21);
    }
    else
    {
        repeat = 0;
        Frozen(getBoss(0), 0);
    }
}

void areaPatton22()
{
    int repeat, missile;

    if (CurrentHealth(getBoss(0)) > 0 && repeat < 30)
    {
        int i;

        for (i = 0 ; i < 12 ; i += 1)
        {
            missile = CreateObjectAt("LightningBolt", GetWaypointX(113) + IntToFloat(i * 46), GetWaypointY(113) + IntToFloat(i * 46));
            Enchant(missile, "ENCHANT_FREEZE", 0.0);
            SetOwner(getBoss(0), missile);
            LookWithAngle(missile, 225);
            PushObjectTo(missile, 19.0, -19.0);
        }
        repeat ++;
        FrameTimer(3, areaPatton22);
    }
    else
    {
        repeat = 0;
        Frozen(getBoss(0), 0);
    }
}

void areaPatton23()
{
    int repeat, i;

    if (CurrentHealth(getBoss(0)) > 0 && repeat < 150)
    {
        int subUnit;

        for (i = 9 ; i >= 0 ; i --)
        {
            if (CurrentHealth(player[i]) > 0 && IsVisibleTo(getBoss(0), player[i]))
            {
                PushObject(player[i], -4.0, GetObjectX(getBoss(0)), GetObjectY(getBoss(0)));
                Effect("CHARM", GetObjectX(player[i]), GetObjectY(player[i]), GetObjectX(getBoss(0)), GetObjectY(getBoss(0)));
            }
        }
        if (!(repeat % 3))
        {
            for (i = 0 ; i < 8 ; i ++)
            {
                subUnit = CreateObjectAt("CarnivorousPlant", GetObjectX(getBoss(0)) - MathSine(i * 45 + 90, 36.0), GetObjectY(getBoss(0)) - MathSine(i * 45, 36.0));
                Frozen(subUnit, 1);
                SetOwner(getBoss(0), subUnit);
                SetCallback(subUnit, 9, deathTouched);
                DeleteObjectTimer(subUnit, 1);
                Effect("EXPLOSION", GetObjectX(subUnit), GetObjectY(subUnit), 0.0, 0.0);
            }
            PlaySoundAround(getBoss(0), 928);
        }
        repeat ++;
        FrameTimer(1, areaPatton23);
    }
    else
    {
        repeat = 0;
        Frozen(getBoss(0), 0);
    }
}

void areaPatton24()
{
    int count, i;

    if (CurrentHealth(getBoss(0)) && count < 8)
    {
        for (i = 9 ; i >= 0 ; i --)
        {
            if (CurrentHealth(player[i]) && IsVisibleTo(getBoss(0), player[i]))
            {
                MoveWaypoint(83, GetObjectX(player[i]) + D_RAND[Random(0, 1)], GetObjectY(player[i]) + D_RAND[Random(0, 1)]);
                AudioEvent("GlyphCast", 83);
                SummonFireBoom(83);
            }
        }
        count ++;
        FrameTimer(27, areaPatton24);
    }
    else
    {
        count = 0;
    }
}

int SummonFireBoom(int location)
{
    int unit = CreateObject("FireSprite", location);

    UnitLinkBinScript(unit, FireSpriteBinTable());
    SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x40);
    Enchant(unit, "ENCHANT_BLINDED", 0.0);
    Enchant(unit, "ENCHANT_DEATH", 0.7);
    Raise(unit, 200.0);
    return unit;
}

void putZombieAbovewall()
{
    int unit;

    ObjectOff(self);
    if (!unit)
    {
        unit = spawnZombie(86);
        spawnZombie(86);
        //SpawnGreenBall(152);
        SpawnSoulStoneAt(GetWaypointX(152), GetWaypointY(152));
        SpawnSoulStoneAt(GetWaypointX(152), GetWaypointY(152));
        SpawnSoulStoneAt(GetWaypointX(152), GetWaypointY(152));
    }
}

void spritWeapon()
{
    if (CurrentHealth(self))
    {
        if (CurrentHealth(other) && !HasEnchant(self, "ENCHANT_DETECTING"))
        {
            int flames = CreateObjectAt("PitifulFireball", GetObjectX(self) - UnitRatioX(self, other, 17.0), GetObjectY(self) - UnitRatioY(self, other, 17.0));
            
            SetOwner(self, flames);
            PushObject(flames, -15.0, GetObjectX(other), GetObjectY(other));
            Enchant(self, "ENCHANT_DETECTING", 1.9);
            PlaySoundAround(flames, 520);
            PlaySoundAround(flames, 6);
        }
        Enchant(self, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(self, 1.0);
    }
}

void wizardAttack()
{
    if (CurrentHealth(self) > 0)
    {
        if (CurrentHealth(other) > 0)
        {
            if (!HasEnchant(self, "ENCHANT_DETECTING"))
            {
                int fireboom = CreateObjectAt("StrongFireball", GetObjectX(self) - UnitRatioX(self, other, 19.0), GetObjectY(self) - UnitRatioY(self, other, 19.0));

                SetOwner(self, fireboom);
                LookAtObject(fireboom, other);
                PushObject(fireboom, -15.0, GetObjectX(other), GetObjectY(other));
                Enchant(self, "ENCHANT_DETECTING", 2.0);
            }
            else if (!HasEnchant(self, "ENCHANT_BURNING"))
            {
                Enchant(self, "ENCHANT_BURNING", 7.0);
                CastSpellObjectObject("SPELL_CLEANSING_FLAME", self, self);
            }
        }
        Enchant(self, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(self, 1.0);
    }
}

void fallStone(int unit)
{
    if (GetObjectZ(unit) > 0.0)
    {
        Raise(unit, GetObjectZ(unit) - 20.0);
        FrameTimerWithArg(1, unit, fallStone);
    }
    else
    {
        MoveWaypoint(164, GetObjectX(unit), GetObjectY(unit));
        AudioEvent("WallDestroyedStone", 164);
        AudioEvent("StoneHitStone", 164);
        Effect("JIGGLE", GetWaypointX(164), GetWaypointY(164), 30.0, 0.0);
        int subUnit = CreateObject("CarnivorousPlant", 164);
        Frozen(subUnit, 1);
        SetCallback(subUnit, 9, deathTouched);
        DeleteObjectTimer(subUnit, 1);
        MoveObject(unit, GetWaypointX(165), GetWaypointY(165));
    }
}

int stones()
{
    int i;

    i ++;
    return Object("golemRock" + IntToString(i % 4 + 1));
}

void fistAttack()
{
    int missile;

    if (CurrentHealth(self) > 0)
    {
        if (CurrentHealth(other) > 0)
        {
            if (!HasEnchant(self, "ENCHANT_DETECTING"))
            {
                missile = stones();
                MoveObject(missile, GetObjectX(other), GetObjectY(other));
                Raise(missile, 200.0);
                FrameTimerWithArg(1, missile, fallStone);
                Enchant(self, "ENCHANT_DETECTING", 2.0);
            }
        }
        Enchant(self, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(self, 1.0);
    }
}

void entranceBossZoneA3()
{
    ObjectOff(self);

    RetreatLevel(getBoss(CreateObject("mimic", 143)), 0.0);
    SetUnitMaxHealth(getBoss(0), 4000);
    SetCallback(getBoss(0), 5, area3BossDeaths);
    SetCallback(getBoss(0), 3, findEnemy);
    CreatureGuard(getBoss(0), GetObjectX(getBoss(0)), GetObjectY(getBoss(0)), GetObjectX(getBoss(0)), GetObjectY(getBoss(0)), 500.0);

    AudioEvent("HecubahDieFrame283", 143);

    UniPrintToAll("제 3 구역의 보스가 등장했습니다 ..!! (라이프 +5)");
    MoveWaypoint(19, GetObjectX(other), GetObjectY(other));

    YOUR_LIFE += 5;
    CreateObject("InvisibleLightBlueHigh", 143);
    MoveWaypoint(83, GetWaypointX(143) - 200.0, GetWaypointY(143) - 200.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(143) - 200.0, GetWaypointY(143) + 200.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(143) + 200.0, GetWaypointY(143) - 200.0);
    CreateObject("InvisibleLightBlueHigh", 83);
    MoveWaypoint(83, GetWaypointX(143) + 200.0, GetWaypointY(143) + 200.0);
    CreateObject("InvisibleLightBlueHigh", 83);

    FrameTimer(15, controlArea3BossPatton);
}

void area3BossDeaths()
{
    MoveWaypoint(166, GetObjectX(self), GetObjectY(self));
    AudioEvent("JournalEntryAdd", 166);
    UniPrintToAll("3 번째 보스가 격추되었습니다, 미궁의 탈출구 비밀통로가 열렸습니다.");
    UniPrintToAll("이곳은 곧 무너지게 됩니다!! 어서 미궁으로 탈출하십시오!!");
    UniPrintToAll("주의! 오직 한 사람만이 미궁에서 탈출하실 수 있습니다.");
    Effect("WHITE_FLASH", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("LevelUp", 158), 900);
    WallOpen(11206717);
	WallOpen(11141182);
	WallOpen(11075647);
	WallOpen(11010112);
	WallOpen(10944577);
	WallOpen(10879042);
	WallOpen(10813507);
	WallOpen(10747972);
    DeleteObjectTimer(self, 1);
    FrameTimer(50, collapseEvent);
}

void collapseEvent()
{
    int repeat;

    if (repeat < 360)
    {
        rhombusPut(143, 3735.0, 4243.0, 724.0, 1367.0);
        int rocks = CreateObject("Rock1", 143);
        Effect("JIGGLE", GetWaypointX(143), GetWaypointY(143), 70.0, 0.0);
        AudioEvent("EarthquakeCast", 143);
        AudioEvent("WallDestroyedStone", 143);
        Frozen(rocks, 1);
        Raise(rocks, 200.0);
        repeat ++;
        FrameTimer(1, collapseEvent);
    }
    else
    {
        UniPrintToAll("미궁이 붕괴되었습니다!!");
        playerDeadInRoom();
        repeat = 0;
    }
}

void playerDeadInRoom()
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (CurrentHealth(player[i]) > 0 && !HasEnchant(player[i], "ENCHANT_CROWN"))
        {
            EnchantOff(player[i], "ENCHANT_INVULNERABLE");
            Damage(player[i], 0, 255, 14);
        }
    }
}

void endAllMission()
{
    MoveObject(other, GetWaypointX(157), GetWaypointY(157));
    Enchant(other, "ENCHANT_CROWN", 0.0);
    MoveWaypoint(83, GetObjectX(other), GetObjectY(other));
    AudioEvent("StaffOblivionAchieve1", 83);
    UniPrintToAll("승리__!! 미궁을 빠져 나오셨습니다 ...!!");
    UniPrintToAll("축하드립니다, 최종적으로 당신의 승리입니다 !!");
    Chat(other, "승리자");
    DeleteObjectTimer(CreateObject("RainOrbBlue", 83), 900);
    MoveObject(Object("playerStartLocation"), GetWaypointX(157), GetWaypointY(157));
}

void controlArea3BossPatton()
{
    int cpic;

    if (CurrentHealth(getBoss(0)) > 0 && AREA == 3)
    {
        if (islookAtPlayer(getBoss(0)) >= 0)
        {
            cpic = Random(1, 4);
            if (cpic == 1)
            {
                MoveObject(getBoss(0), GetWaypointX(143), GetWaypointY(143));
                Frozen(getBoss(0), 1);
                areaPatton31();
            }
            else if (cpic == 2)
            {
                MoveObject(getBoss(0), GetWaypointX(143), GetWaypointY(143));
                areaPatton32();
                Frozen(getBoss(0), 1);
            }
            else if (cpic == 3)
            {
                Enchant(getBoss(0), "ENCHANT_FREEZE", 3.0);
                FrameTimerWithArg(67, multiArgs(144, Random(0, 1)), areaPatton33);
            }
            else
            {
                UniChatMessage(getBoss(0), "캬캬캬캬캭....!", 120);
                areaPatton34();
            }
        }
        SecondTimer(BOSS_TIMER[cpic], controlArea3BossPatton);
    }
}

void areaPatton31()
{
    int i;
    float var_0[20];
    int var_1;
    int var_2[10];

    if (CurrentHealth(getBoss(0)) > 0)
    {
        if (!var_1)
        {
            MoveWaypoint(166, GetWaypointX(143), GetWaypointY(143));
            deathStatue(-1);
            for (i = 9 ; i >= 0 ; i --)
            {
                if (CurrentHealth(player[i]) > 0 && IsVisibleTo(getBoss(0), player[i]))
                {
                    var_0[i * 2] = GetObjectX(player[i]);
                    var_0[i * 2 + 1] = GetObjectY(player[i]);
                    Enchant(player[i], "ENCHANT_FREEZE", 2.0);
                }
            }
            var_1 = 1;
            FrameTimer(67, areaPatton31);
        }
        else
        {
            Effect("VIOLET_SPARKS", GetObjectX(getBoss(0)), GetObjectY(getBoss(0)), 0.0, 0.0);
            for (i = 9 ; i >= 0 ; i --)
                CastSpellObjectLocation("SPELL_DEATH_RAY", deathStatue(i), var_0[i * 2], var_0[i * 2 + 1]);
            Frozen(getBoss(0), 0);
            FrameTimer(10, resetStatues);
            var_1 = 0;
        }
    }
}

void resetStatues()
{
    MoveWaypoint(166, 3117.0, 886.0);
    deathStatue(-1);
}

int deathStatue(int arg_0)
{
    int var_0[10];

    if (arg_0 == -1)
    {
        int i;

        for (i = 0 ; i < 10 ; i ++)
        {
            if (!var_0[i])
            {
                var_0[i] = CreateObject(statueName(i), 166);
                Enchant(var_0[i], "ENCHANT_FREEZE", 0.0);
            }
            MoveObject(var_0[i], GetWaypointX(166) + MathSine(i * 36 + 90, 38.0), GetWaypointY(166) + MathSine(i * 36, 38.0));
        }
        return var_0[0];
    }
    return var_0[arg_0];
}

string statueName(int i)
{
    string name[] = {"MovableStatue2b", "MovableStatue2a", "MovableStatue2a", "MovableStatue2h", "MovableStatue2g",
    "MovableStatue2f", "MovableStatue2e", "MovableStatue2d", "MovableStatue2c", "MovableStatue2c"};
    
    return name[i];
}

void areaPatton32()
{
    int repeat;

    if (CurrentHealth(getBoss(0)) && repeat < 15)
    {
        int subUnit = CreateObjectAt("maiden", GetWaypointX(143) + MathSine(repeat * 24 + 90, 120.0), GetWaypointY(143) + MathSine(repeat * 24, 120.0));
        SetOwner(getBoss(0), subUnit);
        Frozen(subUnit, 1);
        LookWithAngle(subUnit, 0);
        CreateObjectAt("Rock2", GetObjectX(subUnit), GetObjectY(subUnit));
        SetOwner(subUnit, subUnit + 1);
        Enchant(subUnit + 1, "ENCHANT_SHOCK", 0.0);
        SetCallback(subUnit, 9, movingThunderStone);
        repeat ++;
        FrameTimer(1, areaPatton32);
    }
    else
    {
        Frozen(getBoss(0), 0);
        repeat = 0;
    }
}

void areaPatton33(int arg_0)
{
    int progress;
    int wp = arg_0 % 65536; //wp is 144, wp+1 is 145
    int patton = arg_0 / 65536;
    int var_1[2];

    if (CurrentHealth(getBoss(0)) > 0 && progress < 24)
    {
        if (!progress)
        {
            MoveWaypoint(83, GetWaypointX(wp), GetWaypointY(wp));
            MoveWaypoint(84, GetWaypointX(wp + 1), GetWaypointY(wp + 1));
        }
        var_1[0] = CreateObject("archerBolt", 83);
        var_1[1] = CreateObject("archerBolt", 84);
        SetOwner(getBoss(0), var_1[0]);
        SetOwner(getBoss(0), var_1[1]);
        LookAtObject(var_1[0], getBoss(0));
        LookAtObject(var_1[1], getBoss(0));
        PushObject(var_1[0], -13.0, GetObjectX(getBoss(0)), GetObjectY(getBoss(0)));
        PushObject(var_1[1], -13.0, GetObjectX(getBoss(0)), GetObjectY(getBoss(0)));
        if (!patton)
        {
            MoveWaypoint(83, GetWaypointX(83) + 23.0, GetWaypointY(83) + 23.0);
            MoveWaypoint(84, GetWaypointX(84) - 23.0, GetWaypointY(84) - 23.0);
        }
        else
        {
            MoveWaypoint(83, GetWaypointX(83) - 23.0, GetWaypointY(83) + 23.0);
            MoveWaypoint(84, GetWaypointX(84) + 23.0, GetWaypointY(84) - 23.0);
        }
        FrameTimerWithArg(1, arg_0, areaPatton33);
        progress ++;
    }
    else
    {
        progress = 0;
        Frozen(getBoss(0), 0);
    }
}

void areaPatton34()
{
    int progress;
    if (CurrentHealth(getBoss(0)) > 0 && progress < 180)
    {
        float pos_x = MathSine(Random(0, 359) + 90, 34.0), pos_y = MathSine(Random(0, 359), 34.0);
        int bullet = CreateObject("ThrowingStone", 159);

        SetOwner(getBoss(0), bullet);
        Enchant(bullet, "ENCHANT_RUN", 0.0);
        MoveObject(bullet, GetObjectX(getBoss(0)) + (pos_x / 2.0), GetObjectY(getBoss(0)) + (pos_y / 2.0));
        PushObjectTo(bullet, pos_x, pos_y);
        MoveWaypoint(83, GetObjectX(getBoss(0)), GetObjectY(getBoss(0)));
        AudioEvent("ElectricalArc1", 83);
        AudioEvent("LightningBolt", 83);
        progress ++;
        FrameTimer(1, areaPatton34);
    }
    else
    {
        progress = 0;
    }
}

void movingThunderStone()
{
    int subsub = GetTrigger() + 1;

    MoveObject(self, GetObjectX(subsub), GetObjectY(subsub));
    LookWithAngle(self, GetDirection(self) + 1);
    if (GetDirection(self) >= 240)
    {
        Delete(subsub);
        Delete(self);
    }
}

int multiArgs(int arg_0, int arg_1)
{
    return arg_0 + (arg_1 * 65536);
}

void PlayerSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 2;
}

int PlayerCheckOnDeath(int plr)
{
    return player[plr + 10] & 2;
}

void PlayerOnDeath(int plrIndex)
{
    if (YOUR_LIFE)
        YOUR_LIFE --;
    UniPrintToAll(PlayerIngameNick(player[plrIndex]) + "(" + SignColor(plrIndex) + ") 님께서 격추되었습니다");
}

void LoopPreservePlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    MoveObject(PlayerFlag(i), GetObjectX(player[i]), GetObjectY(player[i]));
                    if (CurrentHealth(player[i]) ^ MaxHealth(player[i]))
                        Damage(player[i], 0, 255, 14);
                    break;
                }
                else
                {
                    if (!PlayerCheckOnDeath(i))
                    {
                        PlayerSetDeathFlag(i);
                        PlayerOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
            {
                player[i] = 0;
                player[i + 10] = 0;
            }
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayer);
}

void catchPlayers()
{
    int i, plr;

    if (CurrentHealth(other) && YOUR_LIFE)
    {
        plr = checkPlayer();
        for (i = 9 ; i >= 0 && plr < 0 ; i --)
        {
            if (!MaxHealth(player[i]))
            {
                player[i] = GetCaller();
                player[i + 10] = 1;
                UniPrintToAll(PlayerIngameNick(player[i]) + " 플레이어 님께서 입장하셨습니다.");
                plr = i;
                break;
            }
        }
        if (plr >= 0)
            entryPlayer(plr);
        else
            playerIsFull();
    }
    else
    {
        Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
        Enchant(other, "ENCHANT_FREEZE", 0.0);
        MoveObject(other, GetWaypointX(169), GetWaypointY(169));
        UniPrint(other, "라이프가 남아있지 않아 부활하실 수 없습니다.");
    }
}

void entryPlayer(int arg_0)
{
    if (PlayerCheckOnDeath(arg_0))
        PlayerSetDeathFlag(arg_0);
    if (MaxHealth(player[arg_0]) == 150)
        Delete(GetLastItem(player[arg_0]));
    MoveObject(player[arg_0], GetWaypointX(19), GetWaypointY(19));
    Effect("TELEPORT", GetWaypointX(19), GetWaypointY(19), 0.0, 0.0);
    AudioEvent("BlindOff", 19);
    Enchant(player[arg_0], "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(player[arg_0], "ENCHANT_INVULNERABLE", 5.0);
}

void playerIsFull()
{
    UniPrintToAll("플레이어가 너무 많습니다, 잠시 후 다시 시도하세요.");
    MoveObject(other, GetWaypointX(153), GetWaypointY(153));
    Enchant(other, "ENCHANT_FREEZE", 0.0);
    Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
}

int checkPlayer()
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

void DetectedSpecificIndex(int curId)
{
    if (!IsMissileUnit(curId))
        return;

    if (GetUnitThingID(curId) == 526)
        Delete(curId);
}

void LoopSearchIndex()
{
    int curId, tempId;
    int *ptr = 0x750710;

    ptr = ptr[0];
    while (ptr != NULLPTR)
    {
        tempId = ptr[11];
        if (curId)
        {
            while (curId < tempId)
            {
                curId += 1;
                DetectedSpecificIndex(curId);
            }
            break;
        }
        curId = tempId;
        break;
    }
    FrameTimer(1, LoopSearchIndex);
}

void ShotDeathray(int glow)
{
    int owner;

    if (IsObjectOn(glow))
    {
        owner = GetOwner(glow);
        if (CurrentHealth(owner))
        {
            CastSpellObjectObject("SPELL_DEATH_RAY", owner, glow);
        }
        Delete(glow);
    }
}

void RayTargetingHandler(int owner)
{
    int glow;

    if (CurrentHealth(owner))
    {
        glow = CreateObjectAt("Moonglow", GetObjectX(owner), GetObjectY(owner));
        SetOwner(owner, glow);
        FrameTimerWithArg(1, glow, ShotDeathray);
    }
}

void SpawnGreenBall(int wp)
{
	int unit = CreateObject("BomberYellow", wp);
	CreateObject("WeirdlingBeast", wp);
	CreateObject("GreenOrb", wp);

	Damage(unit, 0, 999, -1);
	Damage(unit + 1, 0, 999, -1);
	SetCallback(unit, 9, JunkYardDog);
	FrameTimerWithArg(1, unit, DelayMove);
}

void DelayMove(int unit)
{
	ObjectOff(unit);
	MoveObject(unit, GetObjectX(unit + 1), GetObjectY(unit + 1));
}

void JunkYardDog()
{
	int ptr = GetTrigger() + 1;
	
	if (IsCaller(ptr + 1))
		LookWithAngle(self, 5);
	else
	{
		LookWithAngle(self, GetDirection(self) - 1);
		MoveObject(self, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
		MoveObject(ptr, GetObjectX(self), GetObjectY(self));
		if (GetDirection(self) == 1)
		{
			MoveWaypoint(152, GetObjectX(self), GetObjectY(self));
			SpawnGreenBall(152);
			Delete(self);
			Delete(ptr);
			Delete(ptr + 1);
		}
	}
}

void TrapSkipPotalWalls()
{
    int unit = CreateObject("Rat", 172);

    Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    Enchant(unit, "ENCHANT_ANTI_MAGIC", 0.0);
    SetCallback(unit, 9, TeleportOnTouched);

    UniPrint(other, "주변에서 비밀의 벽이 열렸습니다.");
    ObjectOff(self);
    WallOpen(Wall(83, 231));
    WallOpen(Wall(84, 232));
    WallOpen(Wall(85, 233));
    WallOpen(Wall(86, 234));
    FrameTimerWithArg(1, unit, PortalStatLoop);
}

void TeleportOnTouched()
{
    if (CurrentHealth(other))
    {
        Effect("COUNTERSPELL_EXPLOSION", GetWaypointX(self), GetWaypointY(self), 0.0, 0.0);
        MoveObject(other, GetWaypointX(173), GetWaypointY(173));
        Effect("COUNTERSPELL_EXPLOSION", GetWaypointX(other), GetWaypointY(other), 0.0, 0.0);
        MoveObject(self, GetWaypointX(172), GetWaypointY(172));
    }
}

void PortalStatLoop(int unit)
{
    if (HasEnchant(unit, "ENCHANT_ANTI_MAGIC"))
    {
        EnchantOff(unit, "ENCHANT_ANTI_MAGIC");
        MoveObject(unit, GetWaypointX(172), GetWaypointY(172));
        Frozen(CreateObject("Magic", 172), 1);
    }
    FrameTimerWithArg(10, unit, PortalStatLoop);
}

void RemoveGolemWalls()
{
    int unit;

    ObjectOff(self);
    if (!unit)
    {
        unit = spawnFister(170);
        spawnFister(171);
        LookWithAngle(unit, 0);
        LookWithAngle(unit + 1, 64);
        WallOpen(Wall(86, 204));
        WallOpen(Wall(87, 205));
        WallOpen(Wall(88, 206));
        WallOpen(Wall(89, 207));
        WallOpen(Wall(90, 208));
        WallOpen(Wall(91, 207));
        WallOpen(Wall(92, 206));
        WallOpen(Wall(93, 205));
        WallOpen(Wall(81, 209));
        WallOpen(Wall(82, 210));
        WallOpen(Wall(83, 211));
        WallOpen(Wall(84, 212));
        WallOpen(Wall(85, 213));
        WallOpen(Wall(84, 214));
        WallOpen(Wall(83, 215));
        WallOpen(Wall(82, 216));
    }
}

void SetUnitCurrentHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

int getBoss(int unit)
{
    int cboss;

    if (unit)
        cboss = unit;
    return cboss;
}

void DisplayLadderBoard()
{
    string show;

    show = "에리아:\t" + IntToString(AREA) + "\nLIFE:\t" + IntToString(YOUR_LIFE);
    if (CurrentHealth(getBoss(0)))
        show += "\n보스 남은체력:\t" + IntToString(CurrentHealth(getBoss(0)));
    UniChatMessage(display(), show, 30);
    SecondTimer(1, DisplayLadderBoard);
}

int display()
{
    int disp;

    if (!disp)
    {
        disp = CreateObject("Hecubah", 154);
        Frozen(disp, 1);
    }
    return disp;
}

void rhombusPut(int wp, float x_low, float x_high, float y_low, float y_high)
{
    float xrnd = RandomFloat(y_low, y_high), yrnd = RandomFloat(0.0, x_high - x_low);

    MoveWaypoint(wp, x_high - y_high + xrnd - yrnd, xrnd + yrnd);
}

void spawnGlyph(int wp, string spell_1, string spell_2, string spell_3)
{
    int deadBomber = CreateObject("bomberYellow", wp);

    CreateObject("Glyph", 157);

    LookWithAngle(deadBomber, wp);
    TrapSpells(deadBomber, spell_1, spell_2, spell_3);
    ObjectOff(deadBomber);
    SetCallback(deadBomber, 9, touchedGlyph);
    Damage(deadBomber, 0, 20, -1);

    FrameTimerWithArg(1, deadBomber + 1, delayTeleportForGlyph);
}

void delayTeleportForGlyph(int unit)
{
    int locationId = GetDirection(unit - 1);

    MoveObject(unit, GetWaypointX(locationId), GetWaypointY(locationId));
}

void touchedGlyph()
{
    int subsub = GetTrigger() + 1;
    
    if (!IsObjectOn(subsub))
    {
        ObjectOn(self);
    }
}

string SignColor(int num)
{
    string color[] = {"빨간색", "오렌지색", "노란색", "초록색", "파란색",
        "청록색", "보라색", "갈색", "하얀색", "검은색"};

    return color[num % 10];
}

int PlayerFlag(int num)
{
    int flag[10];

    if (num < 0)
    {
        flag[0] = Object("flagBase");
        int k;
        for (k = 1 ; k < 10 ; k += 1)
            flag[k] = flag[0] + (k * 2);
        return 0;
    }
    return flag[num];
}

int DummyUnitCreate(string name, float xProfile, float yProfile)
{
    int unit = CreateObjectAt(name, xProfile, yProfile);

    if (unit)
    {
        ObjectOff(unit);
        Damage(unit, 0, MaxHealth(unit) + 1, -1);
        Frozen(unit, 1);
    }
    return unit;
}

void PreserveSoulStone()
{
    int ptr = GetTrigger() + 1;

    if (MaxHealth(self))
    {
        if (IsCaller(ptr))
            MoveObject(self, GetObjectX(ptr), GetObjectY(ptr));
    }
}

int SpawnSoulStoneAt(float xProfile, float yProfile)
{
    int unit = DummyUnitCreate("Maiden", xProfile, yProfile);

    SetUnitMass(CreateObjectAt("Ankh", xProfile, yProfile), 100.0);
    SetCallback(unit, 9, PreserveSoulStone);
    return unit;
}

void DelayRayTarget(int sUnit)
{
    int owner;

    if (IsObjectOn(sUnit))
    {
        owner = GetOwner(sUnit);
        if (CurrentHealth(owner))
            CastSpellObjectLocation("SPELL_DEATH_RAY", owner, GetObjectX(sUnit), GetObjectY(sUnit));
        Delete(sUnit);
    }
}

void OblivionUseHandler()
{
    int glow;

    if (HasEnchant(other, "ENCHANT_ETHEREAL"))
        return;
    else
    {
        glow = CreateObjectAt("Moonglow", GetObjectX(other), GetObjectY(other));
        SetOwner(other, glow);
        FrameTimerWithArg(1, glow, DelayRayTarget);
        Enchant(other, "ENCHANT_ETHEREAL", 0.8);
    }
}

int SummonOblivionStaff(float xProfile, float yProfile)
{
    int staff = CreateObjectAt("OblivionOrb", xProfile, yProfile);
    
    SetUnitCallbackOnUseItem(staff, OblivionUseHandler);
    DisableOblivionItemPickupEvent(staff);
    SetItemPropertyAllowAllDrop(staff);
    return staff;
}

void OblivionSummonHandler(int sUnit)
{
    int count = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (GetDirection(sUnit + 1))
        {
            if (count)
            {
                SummonOblivionStaff(GetObjectX(sUnit), GetObjectY(sUnit));
                LookWithAngle(sUnit, --count);
                MoveObject(sUnit, GetObjectX(sUnit) + 23.0, GetObjectY(sUnit) + 23.0);
            }
            else
            {
                LookWithAngle(sUnit, ToInt(GetObjectZ(sUnit + 1)));
                LookWithAngle(sUnit + 1, GetDirection(sUnit + 1) - 1);
                MoveObject(sUnit + 1, GetObjectX(sUnit + 1) - 23.0, GetObjectY(sUnit + 1) + 23.0);
                MoveObject(sUnit, GetObjectX(sUnit + 1), GetObjectY(sUnit + 1));
            }
            FrameTimerWithArg(1, sUnit, OblivionSummonHandler);
            break;
        }
        Delete(sUnit);
        Delete(++sUnit);
        break;
    }
}

void PutOblivionGroup(int sLocation)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetWaypointX(sLocation), GetWaypointY(sLocation) - 92.0);

    LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), 5);
    Raise(unit + 1, 5);
    LookWithAngle(unit, ToInt(GetObjectZ(unit + 1)));
    FrameTimerWithArg(1, unit, OblivionSummonHandler);
}

void SignDescriptionSet()
{
    RegistSignMessage(Object("SignArea1"), "표지판에는 \"첫번째 에리아\" 라고 쓰여져 있습니다");
    RegistSignMessage(Object("SignArea1Desc"), "참고사항: 이 지도에서 캐릭터는 데미지 1만 닳아도 죽도록 되어있어요!");
    RegistSignMessage(Object("SignArea1BossDesc"), "경고! 이 텔레포트 너머에 미궁의 관리자가 서식하고 있음");
    RegistSignMessage(Object("SignArea2Desc"), "표지판에는 '두번째 에리아' 라고 쓰여져 있습니다");
    RegistSignMessage(Object("SignArea2BossDesc"), "경고! 이 텔레포트 너머에 미궁의 수호신이 존재합니다");
    RegistSignMessage(Object("SignArea3Desc"), "세번째 이면서 \"최종\" 에리아");
    RegistSignMessage(Object("SignArea3BossDesc"), "미궁의 지배자가 있는곳! 그가 있기에 이곳이 존재한다");
    RegistSignMessage(Object("SignClosedDesc"), "미궁 입구가 붕괴되어 더 이상 이 안으로 들어갈 수 없습니다");
    RegistSignMessage(Object("SignArea2PartDesc"), "앞에 보이는 무수히 많은 쥐에게 조금이라도 닿으면 죽음");
    RegistSignMessage(Object("part3sg1"), "힘들게 통과한 구간, 죽어서 처음부터 다시 시작하면 짜증나잖아? 이걸 타라고");
}

void OpenIxJumpWalls()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_ixjumpwalls);
    UniPrint(OTHER, "어디에서 인가 비밀스러운 벽이 하나 열렸습니다");
}

void OpenIxRestroomWalls()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_ixrestroomwalls);
}
