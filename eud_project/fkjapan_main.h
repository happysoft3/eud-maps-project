
#include "fkjapan_reward.h"
#include "fkjapan_initscan.h"
#include "fkjapan_player.h"
#include "fkjapan_desc.h"
#include "fkjapan_resource.h"
#include "fkjapan_mob.h"
#include "libs/indexloop.h"
#include "libs/coopteam.h"
#include "libs/winapi.h"
#include "libs/chatevent.h"
#include "libs/game_flags.h"
#include "libs/networkRev.h"

// void PlayerClassCommonWhenEntry()
// {
//     //ExtractMapBgm("WelcomeHell.mp3");
//     SomeObjectChangeToInnerImage();
//     UserImageInit();
//     ShowMessageBox("대형마트를 털어라", "마트를 습격하고 사장을 죽이세요");
// }

void OnShutdownMap()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
    ResetHostileCritter();
}

void BloodingFxHandler(int sUnit)
{
    int durate = GetDirection(sUnit), angle;

    if (IsObjectOn(sUnit))
    {
        if (durate)
        {
            angle = Random(0, 359);
            DeleteObjectTimer(
                CreateObjectAt("PlayerWaypoint", GetObjectX(sUnit) + MathSine(angle + 90, RandomFloat(3.0, 30.0)), GetObjectY(sUnit) + MathSine(angle, RandomFloat(3.0, 30.0))),
                23
            );
            LookWithAngle(sUnit, durate - 1);
            FrameTimerWithArg(1, sUnit, BloodingFxHandler);
        }
        else
            Delete(sUnit);
    }
}

void BloodSpreadingFx(int sUnit)
{
    int fxMake = CreateObjectAt("Mover", GetObjectX(sUnit), GetObjectY(sUnit));

    LookWithAngle(fxMake, 64);
    FrameTimerWithArg(1, fxMake, BloodingFxHandler);
}

void DisableArrowTraps(int ptr)
{
    int i = GetDirection(ptr) - 1, trap = ptr;

    for (i ; i >= 0 ; i --)
    {
        trap ++;
        ObjectOff(trap);
    }
}

void EnableArrowTraps(int ptr)
{
    int i = GetDirection(ptr) - 1, trap = ptr;

    for (i ; i >= 0 ; i --)
    {
        trap ++;
        ObjectOn(trap);
    }
    FrameTimerWithArg(1, ptr, DisableArrowTraps);
}

void DisableFireballTraps(int ptr)
{
    int i = GetDirection(ptr) - 1, trap = ptr + 1;

    for (i ; i >= 0 ; i --)
    {
        ObjectOff(trap);
        trap += 2;
    }
}

void EnableFireTraps(int ptr)
{
    int i = GetDirection(ptr) - 1, trap = ptr + 1;

    for (i ; i >= 0 ; i --)
    {
        ObjectOn(trap);
        trap += 2;
    }
    FrameTimerWithArg(1, ptr, DisableFireballTraps);
}

int PlaceWestArrowTraps(int wp, int count)
{
    int ptr = CreateObject("InvisibleLightBlueLow", wp), i;
    LookWithAngle(ptr, count);
    MoveWaypoint(1, GetWaypointX(wp), GetWaypointY(wp));
    for (i = 0 ; i < count ; i ++)
    {
        MoveWaypoint(1, GetWaypointX(1) - 11.0, GetWaypointY(1) + 11.0);
        ObjectOff(CreateObject("ArrowTrap1", 1));
    }
    return ptr;
}

int PlaceSouthArrowTraps(int wp, int count)
{
    int ptr = CreateObject("InvisibleLightBlueLow", wp), i;
    LookWithAngle(ptr, count);
    MoveWaypoint(1, GetWaypointX(wp), GetWaypointY(wp));
    for (i = 0 ; i < count ; i ++)
    {
        MoveWaypoint(1, GetWaypointX(1) + 11.0, GetWaypointY(1) + 11.0);
        ObjectOff(CreateObject("ArrowTrap2", 1));
    }
    return ptr;
}

int PlaceSouthFireballTraps(int wp, int count)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", wp), i;

    LookWithAngle(ptr, count);
    MoveWaypoint(1, GetWaypointX(wp), GetWaypointY(wp));
    for (i = 0 ; i < count ; i ++)
    {
        MoveWaypoint(1, GetWaypointX(1) + 46.0, GetWaypointY(1) + 46.0);
        ObjectOff(CreateObject("Skull2", 1));
        MoveWaypoint(1, GetWaypointX(1) - 8.0, GetWaypointY(1) + 8.0);
        Enchant(CreateObject("MovableStatue2c", 1), "ENCHANT_AFRAID", 0.0);
    }
    return ptr;
}

void ActivateArrowTraps1()
{
    int t;
    QueryArrowTrapFirst(&t,0);
    EnableArrowTraps(t);
}

void ActivateArrowTraps2()
{
    int t;
    QueryArrowTrapSecond(&t,0);
    EnableArrowTraps(t);
}

void ActivateArrowTraps3()
{
    int trap;
    QueryArrowTrapThird(&trap,0);
    EnableArrowTraps(trap);
}

void ActivateFireTraps1()
{
    int t;
    QueryFireTrap1(&t,0);
    EnableFireTraps(t);
}

void EnableObject(int unit)
{
    ObjectOn(unit);
}

void UnderfootElevWalls()
{
    int i;

    for (i = 0 ; i < 7 ; i ++)
        WallToggle(Wall(97 + i, 47 + i));
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    Effect("SMOKE_BLAST", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
    AudioEvent("Gear2", 1);
    ObjectOff(SELF);
    FrameTimerWithArg(60, GetTrigger(), EnableObject);
}

void OpenOrbDoors(int arg)
{
    int count;

    count += arg;
    if (count == 2)
    {
        UnlockDoor(Object("MainLock1"));
        UnlockDoor(Object("MainLock11"));
        BossSummonPoint(54);
        UniPrintToAll(MessageDesc(68));
    }
}

void RedBaseOn()
{
    int key;

    QueryMainKeyFirst(&key,0);
    if (IsCaller(key))
    {
        ObjectOff(SELF);
        Delete(key);
        ObjectOn(Object("ROrbBase"));
        OpenOrbDoors(1);
    }
}

void BlueBaseOn()
{
    int key;

    QueryMainKeyFirst(&key,0);
    if (IsCaller(key + 1))
    {
        ObjectOff(SELF);
        Delete(key + 1);
        ObjectOn(Object("BOrbBase"));
        OpenOrbDoors(1);
    }
}

void MovingBack(int rots)
{
    int i = GetDirection(rots) - 1, dest = GetDirection(rots + 2);

    for (i ; i >= 0 ; i --)
    {
        Move(rots + (i * 2), dest + i);
    }
}

void MovingToCenter(int rots)
{
    int i = GetDirection(rots) - 1, dest = GetDirection(rots + 2);

    for (i ; i >= 0 ; i --)
    {
        Move(rots + (i * 2), dest + i);
    }
    LookWithAngle(rots + 2, dest + GetDirection(rots));
    FrameTimerWithArg(120, rots, MovingBack);
}

void ResetUnderfootRot(int ptr)
{
    LookWithAngle(ptr, 1);
}

void StartMovingUnderfootRot()
{
    int ptr, rots;

    QueryUnderfootRots(&ptr,0);
    if (GetDirection(ptr + 1))
    {
        rots = ToInt(GetObjectZ(ptr));
        LookWithAngle(ptr + 1, 0);
        LookWithAngle(rots, 14);
        LookWithAngle(rots + 2, 16);
        FrameTimerWithArg(10, rots, MovingToCenter);
        FrameTimerWithArg(300, ptr + 1, ResetUnderfootRot);
    }
    ObjectOff(SELF);
    FrameTimerWithArg(100, GetTrigger(), EnableObject);
}

void MovingBlockNE(int unit)
{
    int count = GetDirection(unit);

    if (count < 92)
    {
        if (count < 23)
            MoveObject(unit, GetObjectX(unit) - 1.0, GetObjectY(unit) - 1.0);
        else
            MoveObject(unit, GetObjectX(unit) + 1.0, GetObjectY(unit) - 1.0);
        LookWithAngle(unit, count + 1);
        FrameTimerWithArg(1, unit, MovingBlockNE);
    }
}

void MovingBlockSE(int unit)
{
    int count = GetDirection(unit);

    if (count < 92)
    {
        if (count < 23)
            MoveObject(unit, GetObjectX(unit) + 1.0, GetObjectY(unit) + 1.0);
        else
            MoveObject(unit, GetObjectX(unit) + 1.0, GetObjectY(unit) - 1.0);
        LookWithAngle(unit, count + 1);
        FrameTimerWithArg(1, unit, MovingBlockSE);
    }
}

void OpenJailRoom()
{
    int mainKey;

    QueryMainKeySecond(&mainKey,0);
    if (HasItem(OTHER, mainKey))
    {
        UnlockDoor(Object("JGate1"));
        UnlockDoor(Object("JGate2"));
        Delete(mainKey);
        ObjectOff(SELF);
        UniPrint(OTHER, MessageDesc(26));
    }
}

void StartMovingBlocks()
{
    int ptr = CreateObject("RedPotion", 55) + 1;
    Delete(ptr - 1);
    SetUnitMaxHealth(SummonDefaultMob("OgreBrute", 55), 275);
    SetUnitMaxHealth(SummonDefaultMob("OgreBrute", 55), 275);
    SetUnitMaxHealth(SummonDefaultMob("AlbinoSpider", 55), 128);
    Raise(ptr, 250.0);
    Raise(ptr + 1, 250.0);
    Raise(ptr + 2, 250.0);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 67), 98);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 67), 98);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 67), 98);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 67), 98);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 67), 98);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 67), 98);
    int blk;

    QueryMyBlockFirst(&blk,0);
    FrameTimerWithArg(30, blk++, MovingBlockNE);
    FrameTimerWithArg(33, blk++, MovingBlockSE);
    ObjectOff(SELF);
    UniPrintToAll(MessageDesc(69));
}

void SpreadLastRow(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 138)
    {
        MoveObject(ptr, GetObjectX(ptr) - 1.0, GetObjectY(ptr) - 1.0);
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - 1.0, GetObjectY(ptr + 1) - 1.0);
        MoveObject(ptr + 2, GetObjectX(ptr + 2) + 1.0, GetObjectY(ptr + 2) + 1.0);
        MoveObject(ptr + 3, GetObjectX(ptr + 3) + 1.0, GetObjectY(ptr + 3) + 1.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, SpreadLastRow);
    }
    else
    {
        WallOpen(Wall(111, 137));
        WallOpen(Wall(112, 138));
        WallOpen(Wall(113, 139));
        WallOpen(Wall(114, 140));
        WallOpen(Wall(115, 141));
    }
}

void StartMovingLastRow()
{
    int bl;
    QueryMyBlockSecond(&bl,0);
    FrameTimerWithArg(60, bl, SpreadLastRow);
    ObjectOff(SELF);
    UniPrintToAll(MessageDesc(70));
}

int CheckSeeUnit(int unit1, int unit2)
{
    return (IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1));
}

void WarCastHarpoon(int owner, int target)
{
    int unit;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        MoveWaypoint(1, GetObjectX(owner) + UnitRatioX(target, owner, 20.0), GetObjectY(owner) + UnitRatioY(target, owner, 20.0));
        Effect("SMOKE_BLAST", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
        AudioEvent("HarpoonInvoke", 1);
        AudioEvent("PowderBarrelExplode", 1);
        unit = CreateObject("InvisibleLightBlueHigh", 1);
        SetOwner(owner, unit);
        Raise(unit, ToFloat(target));
        FrameTimerWithArg(1, unit, FlyingSword);
    }
}

void FlyingSword(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 19)
    {
        while (1)
        {
            if (CheckSeeUnit(ptr, target))
            {
                MoveObject(ptr, GetObjectX(ptr) + UnitRatioX(target, ptr, 21.0), GetObjectY(ptr) + UnitRatioY(target, ptr, 21.0));
                if (DistanceUnitToUnit(ptr, target) > 50.0)
                {
                    unit = CreateObjectAt("HarpoonBolt", GetObjectX(ptr), GetObjectY(ptr));
                    LookAtObject(unit, target);
                    Frozen(unit, 1);
                    DeleteObjectTimer(unit, 9);
                    LookWithAngle(ptr, count + 1);
                    break;
                }
                else
                {
                    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
                    AudioEvent("HarpoonReel", 1);
                    AudioEvent("StoneHitMetal", 1);
                    GreenSparkAt(GetWaypointX(1), GetWaypointY(1));
                    WarPullEnemy(owner, target);
                }
            }
            LookWithAngle(ptr, 100);
            break;
        }
        FrameTimerWithArg(1, ptr, FlyingSword);
    }
    else
    {
        Delete(ptr);
    }
}

void WarPullEnemy(int owner, int target)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(target), GetObjectY(target));

    ObjectOff(CreateObjectAt("Maiden", GetObjectX(target), GetObjectY(target)));
    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(target), GetObjectY(target)), CurrentHealth(owner));
    Damage(unit + 1, 0, MaxHealth(unit + 1) + 1, -1);
    Frozen(unit + 1, 1);
    SetCallback(unit + 1, 9, PullRiskUnit);
    SetOwner(owner, unit);
    Raise(unit, ToFloat(target));
    PullUp(unit);
}

void PullUp(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 20 && IsObjectOn(owner))
    {
        if (IsVisibleTo(owner, target) && DistanceUnitToUnit(owner, target) > 60.0)
        {
            MoveObject(ptr + 1, GetObjectX(target) + UnitRatioX(target, owner, 12.0), GetObjectY(target) + UnitRatioY(target, owner, 12.0));
            Effect("SENTRY_RAY", GetObjectX(owner), GetObjectY(owner), GetObjectX(target), GetObjectY(target));
            count += CheckCutPulling(owner, ptr + 3);
            LookWithAngle(ptr, count + 1);
        }
        else
            LookWithAngle(ptr, 100);
        FrameTimerWithArg(1, ptr, PullUp);
    }
    else
    {
        Delete(ptr);
        Frozen(ptr + 1, 0);
        Delete(ptr + 1);
        Delete(ptr + 2);
        Delete(ptr + 3);
    }
}

int CheckCutPulling(int owner, int ptr)
{
    int tempHp = ToInt(GetObjectZ(ptr)), curHp = CurrentHealth(owner);

    if (tempHp ^ curHp)
    {
        if (curHp < tempHp)
            return 100;
        Raise(ptr, ToFloat(curHp));
    }
    return 0;
}

void PullRiskUnit()
{
    int owner = GetOwner(GetTrigger() - 1), target = ToInt(GetObjectZ(GetTrigger() - 1));
    int count = GetDirection(GetTrigger() + 1);

    if (IsCaller(target))
    {
        if (!count)
        {
            if (CurrentHealth(owner) && CurrentHealth(target))
            {
                MoveWaypoint(1, GetObjectX(target), GetObjectY(target));
                AudioEvent("SentryRayHit", 1);
                Effect("VIOLET_SPARKS", GetObjectX(target), GetObjectY(target), 0.0, 0.0);
                Damage(target, owner, 15, 14);
            }
        }
        LookWithAngle(GetTrigger() + 1, (count + 1) % 7);
    }
}

void WarCastTripleShot(int owner, int target)
{
    int unit;

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        LookAtObject(owner, target);
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner));
        SetOwner(owner, unit);
        Raise(unit, target);
        FrameTimerWithArg(3, unit, WarriorTripleMissile);
    }
}

void WarriorTripleMissile(int ptr)
{
    int caster = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), i;

    if (CurrentHealth(caster) && CurrentHealth(target))
    {
        LookWithAngle(ptr, GetDirection(caster) - 30);
        for (i = 0 ; i < 13 ; i ++)
        {
            SpawnBullet(caster, GetObjectX(caster) + UnitAngleCos(ptr, 17.0), GetObjectY(caster) + UnitAngleSin(ptr, 17.0), 70, 33.0);
            LookWithAngle(ptr, GetDirection(ptr) + 5);
        }
    }
    Delete(ptr);
}

void EnemyWarHarpoonCollide()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, 0, 80, 0);
            Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        // else
        //     break;
        Delete(SELF);
        break;
    }
}

int SpawnNewBullet(int sOwner, float sX, float sY)
{
    int unit = CreateObjectAt("HarpoonBolt", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2e8, 5483536); //projectile update
        // SetMemory(ptr + 0x2b8, ImportUnitCollideFunc());
        // SetMemory(ptr + 0x2fc, EnemyWarHarpoonCollide);
        SetUnitCallbackOnCollide(unit, EnemyWarHarpoonCollide);
        LookAtObject(unit, sOwner);
        LookWithAngle(unit, GetDirection(unit) + 128);
        SetOwner(sOwner, unit);
    }
    return unit;
}

int SpawnBullet(int owner, float x, float y, int dam, float force)
{
    int unit = CreateObjectAt("OgreShuriken", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(GetMemory(ptr + 0x2bc) + 4, dam);
    LookAtObject(unit, owner);
    LookWithAngle(unit, GetDirection(unit) + 128);
    PushObjectTo(unit, UnitRatioX(unit, owner, force), UnitRatioY(unit, owner, force));
    SetOwner(owner, unit);
    return unit;
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.1);
}

int CheckResetUnitSight(int unit)
{
    if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
    {
        Enchant(SELF, "ENCHANT_DETECTING", 3.0);
        FrameTimerWithArg(88, GetTrigger(), ResetUnitSight);
        return 1;
    }
    return 0;
}

void HorrenDetectEnemy()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING") && CheckResetUnitSight(GetTrigger()))
    {
        Enchant(SELF, "ENCHANT_BURNING", 5.0);
        if (Random(0, 2))
            WarCastTripleShot(GetTrigger(), GetCaller());
        else
            WarCastHarpoon(GetTrigger(), GetCaller());
    }
}

void HorrenDead()
{
    WallOpen(Wall(157, 53));
    WallOpen(Wall(158, 54));
    WallOpen(Wall(159, 55));
    WallOpen(Wall(160, 56));
    DeleteObjectTimer(SELF, 90);
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(CreateObject("VortexSource", 1), 120);
    Effect("YELLOW_SPARKS", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
    AudioEvent("HorrendousIsKilled", 1);
    UniChatMessage(SELF, MessageDesc(1), 120);
    UniPrintToAll(MessageDesc(2));
}

int BossHorren(float x,float y)
{
    int unit = CreateObjectById(OBJ_HORRENDOUS, x,y);

    SetUnitMaxHealth(unit, 1200);
    SetCallback(unit, 3, HorrenDetectEnemy);
    SetCallback(unit, 7, FieldMobHurtHandler);
    SetCallback(unit, 5, HorrenDead);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);

    return unit;
}

void RealSummonBoss(int ptr)
{
    float x=GetObjectX(ptr), y=GetObjectY(ptr);

    Effect("SMOKE_BLAST", x,y, 0.0, 0.0);
    GreenSparkAt(x,y);
    PlaySoundAround(ptr,SOUND_TagOn);
    UniChatMessage(BossHorren(x,y), MessageDesc(3), 180);
    Delete(ptr);
}

void TouchedBossSummonPoint()
{
    if (MaxHealth(SELF))
    {
        MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
        Delete(GetTrigger() + 2);
        Delete(GetTrigger() + 1);
        Delete(SELF);
        FrameTimerWithArg(60, CreateObject("SpinningSkull", 1), RealSummonBoss);
    }
}

int BossSummonPoint(int wp)
{
    int unit = DummyUnitCreateById(OBJ_BOMBER, LocationX(wp),LocationY(wp));

    UnitNoCollide(CreateObject("RewardMarkerPlus", wp));
    Frozen(unit + 1, 1);
    CreateObject("BlueSummons", wp);
    SetCallback(unit, 9, TouchedBossSummonPoint);
    return unit;
}

void ShurikenRecharging(int inv)
{
    int ptr = UnitToPtr(inv);

    if (ptr)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
}

int FindShuriken(int unit)
{
    int inv = GetLastItem(unit);

    while (inv)
    {
        if (GetUnitThingID(inv) == 1178)
        {
            ShurikenRecharging(inv);
            return inv;
        }
        inv = GetPreviousItem(inv);
    }
    return 0;
}

void GuardianHitShuriken(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), mis;
    float dt = Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(target), GetObjectY(target));
    float vectX = UnitRatioX(target, ptr, dt), vectY = UnitRatioY(target, ptr, dt);
    float thresHold;

    if (CurrentHealth(owner) && CurrentHealth(target) && IsObjectOn(owner))
    {
        //mis = CreateObjectAt("OgreShuriken", GetObjectX(owner) + UnitRatioX(target, owner, 17.0), GetObjectY(owner) + UnitRatioY(target, owner, 17.0));
        mis = SpawnNewBullet(owner, GetObjectX(owner) + UnitRatioX(target, owner, 17.0), GetObjectY(owner) + UnitRatioY(target, owner, 17.0));
        //Enchant(mis, "ENCHANT_SHOCK", 0.0);
        //SetOwner(owner, mis);
        thresHold = DistanceUnitToUnit(mis, target) / GetObjectZ(ptr + 1);
        MoveObject(ptr, GetObjectX(target) + UnitRatioX(target, ptr, dt * thresHold), GetObjectY(target) + UnitRatioY(target, ptr, dt * thresHold));
        if (IsVisibleTo(ptr, owner))
        {
            PushObject(mis, -42.0, GetObjectX(ptr), GetObjectY(ptr));
        }
        else
        {
            PushObject(mis, -42.0, GetObjectX(target), GetObjectY(target));
        }
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void AbsoluteTargetStrike(int owner, int target, float threshold, int func)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target), GetObjectY(target));

    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner)), threshold);
    SetOwner(owner, unit);
    Raise(unit, ToFloat(target));
    FrameTimerWithArg(1, unit, func);
}

void AIBotDetectEnemy(int owner)
{
    int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(owner), GetObjectY(owner));
    SetOwner(owner, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner)));
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner)), 5000.0);
    LookWithAngle(unit, GetDirection(owner));
    SetUnitScanRange(unit, 500.0);
    UnitNoCollide(unit);
    SetCallback(unit, 3, GetNealryEnemy);
    DeleteObjectTimer(unit, 1);
    FrameTimerWithArg(1, unit + 1, AIBotShootShuriken);
}

void GetNealryEnemy()
{
    int ptr = GetTrigger() + 1;
    int target = ToInt(GetObjectZ(ptr));
    float dist = DistanceUnitToUnit(SELF, OTHER);

    if (dist < GetObjectZ(ptr + 1))
    {
        Raise(ptr + 1, dist);
        Raise(ptr, ToFloat(GetCaller()));
    }
}

void AIBotShootShuriken(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner) && CurrentHealth(target))
        AbsoluteTargetStrike(owner, target, 33.0, GuardianHitShuriken);
    Delete(ptr);
    Delete(ptr + 1);
}

void ShurikenEvent(int cur, int owner)
{
    if (IsMonsterUnit(owner) && CurrentHealth(owner))
    {
        AIBotDetectEnemy(owner);
        FindShuriken(owner);
        Delete(cur);
    }
}

// void onWarharpoonSplash()
// {
//     if (GetTrigger()==GetCaller())
//         return;

//     if (CurrentHealth(OTHER))
//     {
//         if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
//         {
//             Damage(OTHER, SELF, 180, DAMAGE_TYPE_PLASMA);
//         }
//     }
// }

// void WarHarpoonCollide()
// {
//     int owner = GetOwner(SELF);

//     while (1)
//     {
//         if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
//         {
//             SplashDamageAtEx(owner, GetObjectX(SELF), GetObjectY(SELF), 85.0, onWarharpoonSplash);
//             Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
//             DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(SELF), GetObjectY(SELF)), 20);
//         }
//         else if (!GetCaller())
//             WallUtilDestroyWallAtObjectPosition(SELF);
//         else
//             break;
//         // Delete(SELF);
//         MoveObject(SELF,GetObjectX(SELF),GetObjectY(SELF));
//         break;
//     }
// }

// void PlayerHarpoonHandler(int sCur, int owner)
// {
//     if (!IsPlayerUnit(owner))
//         return;
        
//     int plr = GetPlayerIndex(owner), hPtr = UnitToPtr(sCur);

//     if (plr + 1 && hPtr)
//     {
//         if (CurrentHealth(owner) && PlayerClassCheckFlag(plr, PLAYER_FLAG_HARPOON))
//         {
//             SetMemory(hPtr + 0x2b8, ImportUnitCollideFunc());
//             SetMemory(hPtr + 0x2fc, WarHarpoonCollide);
//             Enchant(hPtr, "ENCHANT_SHOCK", 0.0);
//         }
//     }
// }

string HumanChatMents()
{
    return MessageDesc(Random(4, 11));
}

void HumanBotDeathHandler()
{
    UnitNoCollide(SELF);
    RemoveEquipments(GetTrigger());
    CreateRandomItemCommon(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF)));
    BloodSpreadingFx(SELF);
    DeleteObjectTimer(SELF, 60);
}

void HumanInitSightHandler()
{
    AggressionLevel(SELF, 1.0);
    HumanDetectEnemy();
    SetCallback(SELF, 3, HumanDetectEnemy);
    UniChatMessage(SELF, HumanChatMents(), 150);
}

void HumanHearEnemyNothing()
{
    return;
}

void HumanHearEnemy()
{
    int target = LastParentUnit(OTHER);

    if (CurrentHealth(target))
    {
        Attack(SELF, target);
        SetCallback(SELF, 10, HumanHearEnemyNothing);
    }
}

void HumanDetectEnemy()
{
    LookAtObject(SELF, OTHER);
}

void SpecialUnitProcessing(int unit)
{
    if (CurrentHealth(unit))
    {
        AggressionLevel(unit, 0.0);
        SetCallback(unit, 3, HumanInitSightHandler);
        SetCallback(unit, 5, HumanBotDeathHandler);
        SetCallback(unit, 7, FieldMobHurtHandler);
        SetCallback(unit, 10, HearAroundEnemy);
        InvincibleInventoryItem(unit);
    }
}

void PlaceHealingBeacon()
{
    CureHPBeacon(146);
    CureHPBeacon(147);
    CureHPBeacon(148);
    CureHPBeacon(149);
    CureHPBeacon(150);
    CureHPBeacon(151);
    CureHPBeacon(152);
    CureHPBeacon(153);
    CureHPBeacon(154);
    CureHPBeacon(155);
}

void PlaceLibraryMonster()
{
    SummonMaiden(138, 255, 0, 0);
    SummonMaiden(139, 255, 0, 0);
    SummonMaiden(140, 255, 0, 0);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 141), 225);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 142), 225);
    SummonNecromancer(143);
    SummonNecromancer(144);
    SummonNecromancer(117);
    SummonNecromancer(145);
    FrameTimer(1, PlaceHealingBeacon);
}

void PlaceExMonster()
{
    SummonBranchOni(175);
    SummonBranchOni(176);
    SummonBranchOni(177);
    SummonBranchOni(178);
    SummonBranchOni(179);
    SummonBranchOni(180);
    SummonBranchOni(181);
    SummonBranchOni(182);
    SummonBranchOni(183);
    SummonBranchOni(184);
    SummonBranchOni(185);

    SummonMaiden(186, 0, 225, 128);
    SummonMaiden(187, 0, 225, 128);
    SummonMaiden(188, 0, 225, 128);
    SummonMaiden(189, 0, 225, 128);
    SummonMaiden(190, 0, 225, 128);
    SummonMaiden(191, 225, 0, 225);
    SummonMaiden(192, 0, 225, 128);
    SummonMaiden(193, 225, 0, 128);
    SummonMaiden(118, 225, 0, 128);
    SummonMaiden(119, 225, 0, 128);
    SummonMaiden(120, 225, 0, 128);
    SummonMaiden(121, 225, 0, 128);

    SummonBranchOni(122);
    SummonBranchOni(123);
    SummonBranchOni(124);
    SummonBranchOni(125);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 129), 190);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 126), 225);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 127), 225);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 128), 225);
    SummonBranchOni(130);
    SummonBranchOni(131);
    SummonBranchOni(132);

    SummonMaiden(133, 225, 0, 128);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 134), 225);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 135), 190);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 136), 225);
    SummonMaiden(137, 225, 0, 128);
    FrameTimer(1, PlaceLibraryMonster);
}

void PlaceUnderfootMonster()
{
    SetUnitMaxHealth(SummonDefaultMob("Goon", 97), 175);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 98), 175);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 99), 225);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 84), 225);
    SetUnitMaxHealth(SummonDefaultMob("AlbinoSpider", 85), 160);
    SetUnitMaxHealth(SummonDefaultMob("GruntAxe", 86), 250);
    SetUnitMaxHealth(SummonDefaultMob("GruntAxe", 87), 250);
    SetUnitMaxHealth(SummonDefaultMob("GruntAxe", 88), 250);
    SetUnitMaxHealth(SummonDefaultMob("GruntAxe", 89), 250);
    SetUnitMaxHealth(SummonDefaultMob("OgreBrute", 90), 295);
    SetUnitMaxHealth(SummonDefaultMob("OgreBrute", 91), 295);
    SetUnitMaxHealth(SummonDefaultMob("OgreBrute", 92), 295);
    SetUnitMaxHealth(SummonDefaultMob("OgreWarlord", 93), 325);
    SetUnitMaxHealth(SummonDefaultMob("OgreWarlord", 94), 325);
    SetUnitMaxHealth(SummonDefaultMob("OgreBrute", 95), 295);
    SetUnitMaxHealth(SummonDefaultMob("GruntAxe", 96), 250);
    SummonNecromancer(100);
    SummonNecromancer(101);
    SetUnitMaxHealth(SummonDefaultMob("SkeletonLord", 102), 250);
    SetUnitMaxHealth(SummonDefaultMob("SkeletonLord", 103), 250);
    SetUnitMaxHealth(SummonDefaultMob("SkeletonLord", 104), 250);
    SummonNecromancer(105);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 106), 250);
    SetUnitMaxHealth(SummonDefaultMob("UrchinShaman", 107), 250);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 108), 250);
    SummonHecubah(110);
    SummonHecubah(109);
    //FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 174), SpawnGolemBoss);
    InitLichKings();
    FrameTimer(2, InitJailArea);
    FrameTimer(1, PlaceExMonster);
}

void PlaceMonster()
{
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 56), 128);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 57), 128);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 58), 128);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 59), 128);
    SetUnitMaxHealth(SummonDefaultMob("GruntAxe", 60), 225);
    SetUnitMaxHealth(SummonDefaultMob("GruntAxe", 60), 225);
    SetUnitMaxHealth(SummonDefaultMob("OgreWarlord", 61), 325);
    SetUnitMaxHealth(SummonDefaultMob("OgreWarlord", 62), 325);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 63), 255);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 64), 255);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 65), 255);
    SetUnitMaxHealth(SummonDefaultMob("Scorpion", 66), 275);
    SetUnitMaxHealth(SummonDefaultMob("EvilCherub", 66), 225);
    SetUnitMaxHealth(SummonDefaultMob("OgreWarlord", 68), 325);
    SetUnitMaxHealth(SummonDefaultMob("OgreWarlord", 69), 325);
    SetUnitMaxHealth(SummonDefaultMob("OgreWarlord", 70), 325);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 71), 128);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 71), 128);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 72), 128);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 72), 128);
    SetUnitMaxHealth(SummonDefaultMob("SkeletonLord", 73), 295);
    SetUnitMaxHealth(SummonDefaultMob("SkeletonLord", 74), 295);
    SummonHecubah(75);
    SummonHecubah(76);
    SummonHecubah(77);
    SummonHecubah(78);
    SummonNecromancer(79);
    SummonNecromancer(80);
    SummonNecromancer(81);
    SummonNecromancer(82);
    SummonNecromancer(83);
    FrameTimer(1, PlaceUnderfootMonster);
}

void MapDecorations()
{
    QueryMainKeySecond(0,CreateObject("RedOrbKeyOfTheLich", 47));
    QueryArrowTrapFirst(0, PlaceSouthArrowTraps(13, 10) );
    QueryArrowTrapSecond(0, PlaceSouthArrowTraps(44, 8) );
    QueryArrowTrapThird(0, PlaceWestArrowTraps(52, 10) );
    QueryFireTrap1(0, PlaceSouthFireballTraps(53, 3) );
    QueryMainKeyFirst(0,CreateObject("RedOrbKeyOfTheLich", 14));
    CreateObject("BlueOrbKeyOfTheLich", 15);
    int ufrot=CreateObject("InvisibleLightBlueHigh", 16) ;
    QueryUnderfootRots(0, ufrot);
    LookWithAngle(CreateObject("InvisibleLightBlueHigh", 17), 1);
    Raise(ufrot, ToFloat(Object("UnderfootRotPtr")));
    LookWithAngle(ufrot, 16);
    int b1 = CreateObject("IronBlock", 45);
    QueryMyBlockFirst(0, b1 );
    Frozen(CreateObject("IronBlock", 46), 1);
    Frozen(b1, TRUE);
    int b2= CreateObject("IronBlock", 48);
    QueryMyBlockSecond(0,b2);
    Frozen(CreateObject("IronBlock", 49), 1);
    Frozen(CreateObject("IronBlock", 50), 1);
    Frozen(CreateObject("IronBlock", 51), 1);
    Frozen(b2, TRUE);
    FrameTimerWithArg(1, CheckGameKorLanguage(), MapSignInit);
    FrameTimer(94, PlacingSpecialMarketBeacon);
    FrameTimer(2, SetHostileCritter);
    // DrawImageAt(LocationX(242), LocationY(242), 2461);
    // DrawImageAt(LocationX(245), LocationY(245), 1427);
    // DrawImageAt(LocationX(244), LocationY(244), 1418);
    // DrawImageAt(LocationX(246), LocationY(246), 2516);
    // DrawImageAt(LocationX(247), LocationY(247), 1428);
    // DrawImageAt(LocationX(248), LocationY(248), 1426);
    // //DrawImageAt(LocationX(249), LocationY(249), 1430);
    // DrawImageAt(LocationX(250), LocationY(250), 1440);
}

void onEndInitialScan(int *pParams)
{
    int count =pParams[UNITSCAN_PARAM_COUNT];
    char msg[128];

    NoxSprintfString(msg, "%d개의 유닛을 스캔했습니다", &count, 1);
    UniPrintToAll(ReadStringAddressEx(msg));

    UnlockDoor(Object("StartGate1"));
    UnlockDoor(Object("StartGate11"));
    QueryCampArea(0, CreateObject("InvisibleLightBlueHigh", 12) );
    Enchant(CreateObject("InvisibleLightBlueHigh", 12), "ENCHANT_SHOCK", 0.0);
    UniPrintToAll(IntToString(count) + MessageDesc(12));
    FrameTimer(1, PlaceMonster);
    FrameTimer(3, DunmirPicketDescInit);
    FrameTimer(30, ChatMessageLoop);

    DrawText(LocationX(242),LocationY(242),0);
    DrawText(LocationX(245),LocationY(245),1);
    DrawText(LocationX(244),LocationY(244),2);
    DrawText(LocationX(327),LocationY(327),3);
}

void startInitscan(){
    int initScanHash, lastUnit=GetMasterUnit();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_NPC, SpecialUnitProcessing);
    StartUnitScanEx(Object("FirstTargetScan"), lastUnit, initScanHash, onEndInitialScan);
}

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

void initGame()
{
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}

void OnInitializeMap()
{
    MusicEvent();
    CreateLogFile("fkjapan-log.txt");
    startInitscan();
    FrameTimer(1, MapDecorations);
    FrameTimer(30, InitLibraryExZone);
    InitializePlayerSystem();
    blockObserverMode();
    ChatMessageEventPushback("//t", PlayerTeleportHandler);
    initGame();
    MakeCoopTeam();
}

void WhenRunout()
{
    EnchantOff(SELF, "ENCHANT_BLINDED");
}

void MecaGolemStrike()
{
    int ptr;
    float dist = DistanceUnitToUnit(SELF, OTHER);

    LookAtObject(SELF, OTHER);
    if (dist > 39.0)
        PushObjectTo(SELF, UnitRatioX(OTHER, SELF, 21.0), UnitRatioY(OTHER, SELF, 21.0));
    if (!HasEnchant(SELF, "ENCHANT_BURNING") && dist < 110.0)
    {
        Enchant(SELF, "ENCHANT_BURNING", 0.3);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        FrameTimerWithArg(6, ptr, RemoveMecaGolemHitDelay);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void RemoveMecaGolemHitDelay(int ptr)
{
    int unit = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));
    int hp = CurrentHealth(unit), mxHp = MaxHealth(unit), pic;

    if (hp)
    {
        if (DistanceUnitToUnit(unit, target) > 27.0)
            MoveObject(ptr, GetObjectX(unit) + UnitRatioX(target, unit, 4.0), GetObjectY(unit) + UnitRatioY(target, unit, 4.0));
        else
            MoveObject(ptr, GetObjectX(unit), GetObjectY(unit));
        Delete(unit);
        pic = SummonPowerMecaGolem(ptr);
        Damage(pic, 0, mxHp - hp, -1);
        LookAtObject(pic, target);
        HitLocation(pic, GetObjectX(pic), GetObjectY(pic));
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        DeleteObjectTimer(CreateObject("ReleasedSoul", 1), 9);
        AudioEvent("HitStoneBreakable", 1);
    }
    Delete(ptr);
}

void MecaGolemDeathEvent()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(CreateObject("OblivionUp", 1), 120);
    AudioEvent("StaffOblivionAchieve1", 1);
    Effect("WHITE_FLASH", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
    StrBossDeath();
    //Music(2, 100);
    //ModifyPlayMusicNumber(2);
}

int SummonPowerMecaGolem(int ptr)
{
    int unit = CreateObjectAt("MechanicalGolem", GetObjectX(ptr), GetObjectY(ptr));

    SetUnitScanRange(unit, 450.0);
    SetUnitMaxHealth(unit, 1500);
    SetCallback(unit, 3, MecaGolemStrike);
    SetCallback(unit, 5, MecaGolemDeathEvent);
    SetCallback(unit, 13, WhenRunout);

    return unit;
}

void LichLordFONCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, 0, 200, 0);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.2);
            GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
            break;
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int LichLordSkillFON(int sOwner, float sX, float sY)
{
    int fon = CreateObjectAt("Deathball", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2e8, 5483536); //projectile update
        SetMemory(ptr + 0x2b8, ImportUnitCollideFunc());
        SetMemory(ptr + 0x2fc, LichLordFONCollide);
        SetOwner(sOwner, fon);
    }
    return fon;
}

void LichClearSight()
{
    EnchantOff(SELF, "ENCHANT_BLINDED");
}

void LichResetUnitSight(int sUnit)
{
    int enemy = GetUnit1C(sUnit);

    if (CurrentHealth(sUnit))
    {
        EnchantOff(sUnit, "ENCHANT_DETECTING");
        Enchant(sUnit, "ENCHANT_BLINDED", 0.06);
        if (CurrentHealth(enemy))
        {
            Attack(sUnit, enemy);
        }
    }
}

void LichLordThrowFON(int sMe, int sEnemy)
{
    int mis = LichLordSkillFON(sMe, GetObjectX(sMe) + UnitRatioX(sEnemy, sMe, 9.0), GetObjectY(sMe) + UnitRatioY(sEnemy, sMe, 9.0));

    PushObject(mis, 31.0, GetObjectX(sMe), GetObjectY(sMe));
    LookAtObject(sMe, sEnemy);
    HitLocation(sMe, GetObjectX(sMe), GetObjectY(sMe));
}

void onStoneFallDownSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 148, DAMAGE_TYPE_PLASMA);
        }
    }
}

void StoneFallDown(int sUnit)
{
    int durate = GetDirection(sUnit), owner = GetOwner(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(owner))
        {
            if (durate)
            {
                Raise(sUnit + 1, GetObjectZ(sUnit + 1) - GetObjectZ(sUnit));
                LookWithAngle(sUnit, durate - 1);
                FrameTimerWithArg(1, sUnit, StoneFallDown);
                break;
            }
            else
            {
                SplashDamageAtEx(owner, GetObjectX(sUnit + 1), GetObjectY(sUnit + 1), 120.0, onStoneFallDownSplash);
                DeleteObjectTimer(CreateObjectAt("BigSmoke", GetObjectX(sUnit + 1), GetObjectY(sUnit + 1)), 18);
                PlaySoundAround(sUnit + 1, 30);
                Effect("JIGGLE", GetObjectX(sUnit + 1), GetObjectY(sUnit + 1), 0.0, 0.0);
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        break;
    }
}

void LichLordRaisingStone(int sMe, int sEnemy)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sEnemy), GetObjectY(sEnemy));

    Raise(unit, 10.0);
    UnitNoCollide(CreateObjectAt("BoulderCave", GetObjectX(unit), GetObjectY(unit)));
    Raise(unit + 1, 250.0);
    SetOwner(sMe, unit);
    LookWithAngle(unit, 25);
    FrameTimerWithArg(3, unit, StoneFallDown);
}

void LichLordSight()
{
    int enemy = GetUnit1C(SELF);

    if (CurrentHealth(OTHER))
    {
        if (GetCaller() ^ enemy)
        {
            SetUnit1C(SELF, GetCaller());
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            if (DistanceUnitToUnit(SELF, OTHER) > 110.0)
            {
                if (Random(0, 4))
                    LichLordThrowFON(SELF, OTHER);
                else
                    LichLordRaisingStone(SELF, OTHER);
            }
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(35, GetTrigger(), LichResetUnitSight);
        }
    }
}

int SummonLichKing(int sUnit)
{
    int mob = CreateObjectAt("LichLord", GetObjectX(sUnit), GetObjectY(sUnit));

    LichLordSubProcess(mob);
    SetCallback(mob, 3, LichLordSight);
    SetCallback(mob, 13, LichClearSight);
    SetUnitScanRange(mob, 450.0);
    SetUnitMaxHealth(mob, 1500);
    SetCallback(mob, 5, MecaGolemDeathEvent);
    return mob;
}

void FrogOnDead()
{
    FrameTimerWithArg(39, GetTrigger(), SpawnGolemBoss);
    UniChatMessage(SELF, MessageDesc(14), 90);
}

void FrogSightFirst()
{
    //Music(27, 100);
    //ModifyPlayMusicNumber(27);
    SetCallback(SELF, 3, DefaultMobSight);
    UniChatMessage(SELF, "절대로 용서 못한다 개굴~", 128);
}

void InitLichKings()
{
    int chest = Object("MobInChest"), frog;

    if (IsObjectOn(chest)) frog = GetLastItem(chest);
    if (CurrentHealth(frog))
    {
        GreenFrogSubProcess(frog);
        SetCallback(frog, 5, FrogOnDead);
        SetCallback(frog, 7, FieldMobHurtHandler);
        SetCallback(frog, 3, FrogSightFirst);
    }
}

void GolemInitSightHandler()
{
    AggressionLevel(SELF, 1.0);
    SetCallback(SELF, 3, LichLordSight);
    LichLordSight();
}

void SpawnGolemBoss(int ptr)
{
    int unit = SummonLichKing(ptr);

    DeleteObjectTimer(CreateObjectAt("GreenSmoke", GetObjectX(unit), GetObjectY(unit)), 21);
    PlaySoundAround(ptr, 825);
    UniChatMessage(unit, MessageDesc(15), 180);
    Delete(ptr);
}

int SummonMaiden(int wp, int r, int g, int b)
{
    int unit = ColorMaiden(r, g, b, wp);

    UnitLinkBinScript(unit, MaidenBinTable());
    SetUnitMaxHealth(unit, 295);
    SetUnitSpeed(unit, 1.8);
    SetCallback(unit, 3, DefaultMobInitSight);
    SetCallback(unit, 5, DefaultMobDead);
    SetCallback(unit, 7, FieldMobHurtHandler);
    SetCallback(unit, 10, HearAroundEnemy);
    SetUnitScanRange(unit, 450.0);
    AggressionLevel(unit, 0.0);

    return unit;
}

int LibraryKey()
{
    int key;

    if (!key)
        key = CreateObject("RedOrbKeyOfTheLich", 111);
    return key;
}

void OpenLibraryEntranceGate()
{
    if (CurrentHealth(OTHER) && HasItem(OTHER, LibraryKey()))
    {
        ObjectOff(SELF);
        UnlockDoor(Object("LibraryLock"));
        Delete(LibraryKey());
        UniPrintToAll(MessageDesc(16));
    }
}

int LibraryBookcase()
{
    int unit = CreateObject("InvisibleLightBlueHigh", 1);
    
    Frozen(CreateObjectAt("MovableBookcase4", 4580.0, 4291.0), 1);
    Frozen(CreateObjectAt("MovableBookcase4", 4605.0, 4316.0), 1);
    return unit;
}

int LibraryBookcase2()
{
    int unit = CreateObject("InvisibleLightBlueHigh", 1);

    Frozen(CreateObjectAt("MovableBookcase3", 2901.0, 4099.0), 1);
    Frozen(CreateObjectAt("MovableBookcase3", 2927.0, 4073.0), 1);
    Frozen(CreateObjectAt("MovableBookcase1", GetObjectX(unit + 1) + 13.0, GetObjectY(unit + 1) + 13.0), 1);
    Frozen(CreateObjectAt("MovableBookcase1", GetObjectX(unit + 2) + 13.0, GetObjectY(unit + 2) + 13.0), 1);

    return unit;
}

void ResetLibraryHandle(int ptr)
{
    LookWithAngle(ptr + 1, 0);
}

void CloseLibraryWalls(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 34)
    {
        if (count < 22)
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) + 1.0, GetObjectY(ptr + 1) + 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) - 1.0, GetObjectY(ptr + 2) - 1.0);
        }
        else
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) - 1.0, GetObjectY(ptr + 1) + 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) - 1.0, GetObjectY(ptr + 2) + 1.0);
        }
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, CloseLibraryWalls);
    }
    else
    {
        LookWithAngle(ptr, 0);
        WallClose(Wall(199, 187));
        FrameTimerWithArg(1, ptr, ResetLibraryHandle);
    }
}

void OpenLibraryWalls(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 34)
    {
        if (count < 12)
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) + 1.0, GetObjectY(ptr + 1) - 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) + 1.0, GetObjectY(ptr + 2) - 1.0);
        }
        else
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) - 1.0, GetObjectY(ptr + 1) - 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) + 1.0, GetObjectY(ptr + 2) + 1.0);
        }
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, OpenLibraryWalls);
    }
    else
    {
        LookWithAngle(ptr, 0);
        WallOpen(Wall(199, 187));
        FrameTimerWithArg(90, ptr, CloseLibraryWalls);
    }
}

void CloseLibraryWalls2(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 34)
    {
        if (count < 22)
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) + 1.0, GetObjectY(ptr + 1) - 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) - 1.0, GetObjectY(ptr + 2) + 1.0);
        }
        else
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) + 1.0, GetObjectY(ptr + 1) + 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) + 1.0, GetObjectY(ptr + 2) + 1.0);
        }
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, CloseLibraryWalls2);
    }
    else
    {
        LookWithAngle(ptr, 0);
    }
}

void OpenLibraryWalls2(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 34)
    {
        if (count < 12)
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) - 1.0, GetObjectY(ptr + 1) - 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) - 1.0, GetObjectY(ptr + 2) - 1.0);
        }
        else
        {
            MoveObject(ptr + 1, GetObjectX(ptr + 1) - 1.0, GetObjectY(ptr + 1) + 1.0);
            MoveObject(ptr + 2, GetObjectX(ptr + 2) + 1.0, GetObjectY(ptr + 2) - 1.0);
        }
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, OpenLibraryWalls2);
    }
    else
    {
        LookWithAngle(ptr, 0);
    }
}

void CloseLibraryWalls21(int ptr)
{
    int count = GetDirection(ptr + 2);

    if (count < 46)
    {
        if (count < 23)
        {
            MoveObject(ptr + 3, GetObjectX(ptr + 3) + 2.0, GetObjectY(ptr + 3) - 2.0);
            MoveObject(ptr + 4, GetObjectX(ptr + 4) - 2.0, GetObjectY(ptr + 4) + 2.0);
        }
        else
        {
            MoveObject(ptr + 3, GetObjectX(ptr + 3) - 2.0, GetObjectY(ptr + 3) - 2.0);
            MoveObject(ptr + 4, GetObjectX(ptr + 4) - 2.0, GetObjectY(ptr + 4) - 2.0);
        }
        LookWithAngle(ptr + 2, count + 1);
        FrameTimerWithArg(1, ptr, CloseLibraryWalls21);
    }
    else
    {
        LookWithAngle(ptr + 2, 0);
        WallClose(Wall(126, 178));
        WallClose(Wall(127, 177));
        FrameTimerWithArg(1, ptr, ResetLibraryHandle);
    }
}

void OpenLibraryWalls21(int ptr)
{
    int count = GetDirection(ptr + 2);

    if (count < 46)
    {
        if (count < 23)
        {
            MoveObject(ptr + 3, GetObjectX(ptr + 3) + 2.0, GetObjectY(ptr + 3) + 2.0);
            MoveObject(ptr + 4, GetObjectX(ptr + 4) + 2.0, GetObjectY(ptr + 4) + 2.0);
        }
        else
        {
            MoveObject(ptr + 3, GetObjectX(ptr + 3) - 2.0, GetObjectY(ptr + 3) + 2.0);
            MoveObject(ptr + 4, GetObjectX(ptr + 4) + 2.0, GetObjectY(ptr + 4) - 2.0);
        }
        LookWithAngle(ptr + 2, count + 1);
        FrameTimerWithArg(1, ptr, OpenLibraryWalls21);
    }
    else
    {
        LookWithAngle(ptr + 2, 0);
        FrameTimerWithArg(180, ptr, CloseLibraryWalls21);
        FrameTimerWithArg(180, ptr, CloseLibraryWalls2);
        WallOpen(Wall(126, 178));
        WallOpen(Wall(127, 177));
    }
}

void StartLibraryWallsHandler()
{
    int ptr;

    QueryLibraryBookcases1(&ptr, 0);
    if (!GetDirection(ptr + 1))
    {
        LookWithAngle(ptr + 1, 1);
        FrameTimerWithArg(1, ptr, OpenLibraryWalls);
    }
}

void StartLibraryWallsHandler2()
{
    int ptr;

    QueryLibraryBookcases2(&ptr,0);
    if (!GetDirection(ptr + 1))
    {
        LookWithAngle(ptr + 1, 1);
        FrameTimerWithArg(1, ptr, OpenLibraryWalls21);
        FrameTimerWithArg(1, ptr, OpenLibraryWalls2);
    }
}

void ClearWestNpcWalls()
{
    int i;

    if (MaxHealth(SELF))
    {
        Delete(SELF);
        for (i = 0 ; i < 15 ; i ++)
        {
            WallOpen(Wall(198 - i, 214 + i));
            WallBreak(Wall(199 - i, 215 + i));
        }
    }
    Delete(GetTrigger() + 1);
}

int WestWallClearMethod(int sLocation)
{
    int unit = CreateObject("Bomber", sLocation);

    Enchant(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), "ENCHANT_RUN", 0.0);
    ObjectOff(unit);
    Damage(unit, 0, 100, -1);
    return unit;
}

void OpenFenceWalls()
{
    int i;

    for (i = 0 ; i < 5 ; i ++)
    {
        WallOpen(Wall(179 + i, 203 + i));
        WallOpen(Wall(173 + i, 207 - i));
        WallOpen(Wall(173 + i, 209 + i));
        WallOpen(Wall(179 + i, 213 - i));
    }
    SetCallback(WestWallClearMethod(112), 9, ClearWestNpcWalls);
}

int SummonFireSprite(int sUnit)
{
    int unit = CreateObjectAt("FireSprite", GetObjectX(sUnit), GetObjectY(sUnit));

    FireSpriteSubProcess(unit);
    return unit;
}

void FenceMobDead()
{
    int count;

    QueryLibraryCount(&count, 0);
    LookWithAngle(count, GetDirection(count) - 1);
    SummonFireSprite(SELF);
    if (!GetDirection(count))
    {
        OpenFenceWalls();
        UniPrintToAll(MessageDesc(71));
    }
}

int StrangePlant(int wp)
{
    int unit = CreateObject("HecubahWithOrb", wp);

    HecubahOrbSubProcess(unit);
    SetUnitScanRange(unit, 450.0);
    AggressionLevel(unit, 0.0);
    SetCallback(unit, 5, FenceMobDead);
    SetCallback(unit, 3, DefaultMobInitSight);
    SetCallback(unit, 10, HearAroundEnemy);
    return unit;
}

void LibraryFenceMob(int count)
{
    if (count >= 0)
    {
        StrangePlant(112);
        int c;
        QueryLibraryCount(&c,0);
        LookWithAngle(c, GetDirection(c) + 1);
        FrameTimerWithArg(1, count - 1, LibraryFenceMob);
    }
}

int PutSlideObelisk(int wp)
{
    int unit = CreateObject("BomberGenerator", wp);

    SetUnitMass(unit, 25.0);
    Enchant(unit, "ENCHANT_INVULNERABLE", 0.0);
    ObjectOff(unit);

    return unit;
}

void OpenEastLibrary()
{
    if (HasClass(OTHER, "MONSTERGENERATOR"))
    {
        ObjectOff(SELF);

        MoveObject(OTHER, GetObjectX(SELF), GetObjectY(SELF));
        Frozen(OTHER, 1);
        GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
        WallOpen(Wall(162, 190));
        WallOpen(Wall(161, 191));
        WallOpen(Wall(160, 192));
        FrameTimerWithArg(37, 208, RespectStrangeWizard);
        UniPrintToAll(MessageDesc(72));
    }
}

void OpenEastCastleWalls()
{
    ObjectOff(SELF);

    WallOpen(Wall(142, 132));
    WallOpen(Wall(141, 133));
    WallOpen(Wall(140, 134));
}

int CandleMob(int sLocation)
{
    int mob = CreateObject("TalkingSkull", sLocation);
    int img = CreateObjectAt("Candleabra5", GetObjectX(mob), GetObjectY(mob));

    TalkingSkull2SubProcess(mob);
    UnitNoCollide(img);
    FrameTimerWithArg(1, mob, MobCandleAttached);
    return mob;
}

void LibraryElevatorToggle()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("Gear2", 1);
    ObjectToggle(Object("LibraryElev"));
    ObjectToggle(Object("LibraryElvGear1"));
    ObjectToggle(Object("LibraryElvGear2"));
    ObjectOff(SELF);
    CandleMob(236);
    CandleMob(237);
    CandleMob(238);
    CreateObject("BearTrap", 239);
    CreateObject("BearTrap", 240);
    UniPrint(OTHER, MessageDesc(27));
}

void TeleportToLocation()
{
    int dest = GetTrigger() + 1;

    if (CurrentHealth(OTHER))
    {
        Effect("COUNTERSPELL_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER, GetObjectX(dest), GetObjectY(dest));
        Effect("COUNTERSPELL_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        DeleteObjectTimer(CreateObject("BlueRain", 1), 42);
        AudioEvent("BlindOff", 1);
    }
}

int TeleportMark(int src, int dest)
{
    int unit = CreateObject("WeirdlingBeast", src);

    SetUnitMaxHealth(unit, 10);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    CreateObject("InvisibleLightBlueHigh", dest);
    Enchant(CreateObject("InvisibleLightBlueHigh", src), "ENCHANT_ANCHORED", 0.0);
    SetCallback(unit, 9, TeleportToLocation);
    return unit;
}

void InitLibraryPack()
{
    int alreadyRun;

    ObjectOff(SELF);
    if (!alreadyRun)
    {
        alreadyRun = PutSlideObelisk(113);
        QueryLibraryBookcases2(0, LibraryBookcase2() );
        QueryLibraryBookcases1(0,LibraryBookcase());
        QueryLibraryCount(0, CreateObject("InvisibleLightBlueHigh", 112) );
        TeleportMark(114, 115);
        TeleportMark(116, 117);
        FrameTimerWithArg(1, 6, LibraryFenceMob);
    }
}

void DunmirPicketDescInit()
{
    //TODO: 0 to 11
    int i;
    string desc[] ={
    "날아올라~ 저 하늘 멋진 별이 될래요~ 깊은 밤 하늘의 빛이 되어 춤을 출거야~",
    "미안해 미안해 하지마 내가 초라해지잖아 빨간 예쁜입술로 어서 나를 죽이고 가 나는 괜찮아",
    "견찰서 가고싶어 이세끼야? 어?",
    "사랑스러운 게이~ 너는 나의 게이",
    "쪼끔만 더 밀어붙이면 우리가 이깁니다!!",
    "나를 따라하는 사람은 참 많아도 내가 따라하는 사람이란 없다고 there's no one like me",
    "누구나 그럴싸한 계획을 갖고있다. 처 맞기 전까지는",
    "인간의 욕심은 끝이없고, 똑같은 실수를 반복한다",
    "번번히 살려내지요?",
    "ㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋ",
    "403 호 돈갚아!!!",
    };

    for (i = 0 ; i < 11 ; i ++)
        RegistSignMessage(Object("DunmirPick" + IntToString(i + 1)), ToStr(SToInt(desc) + Random(0, 11)));
}

void PlayerTeleportHandler(int unit)
{
    int arr[32], camp;

    QueryCampArea(&camp,0);
    if (CurrentHealth(unit))
    {
        int idx=GetPlayerIndex(unit);
        if (GetPlayerUnit(idx)!=unit)
            return;

        if (HasEnchant(unit, "ENCHANT_LIGHT"))
        {
            UniPrint(unit, MessageDesc(28));
            return;
        }
        if (IsVisibleTo(camp, unit))
        {
            if (IsObjectOn(arr[idx]))
            {
                PlayerTeleportAt(unit, GetObjectX(arr[idx]), GetObjectY(arr[idx]));
                UniPrint(unit, MessageDesc(29));
            }
            else
                UniPrint(unit, MessageDesc(30));
        }
        else
        {
            if (IsObjectOn(arr[idx]))
            {
                MoveObject(arr[idx], GetObjectX(unit), GetObjectY(unit));
            }
            else
            {
                arr[idx] = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
                Enchant(arr[idx], "ENCHANT_ANCHORED", 0.0);
            }
            PlayerTeleportAt(unit, GetObjectX(camp), GetObjectY(camp));
            UniPrint(unit, MessageDesc(31));
            UniPrint(unit, MessageDesc(32));
        }
    }
}

void PlayerTeleportAt(int unit, float destX, float destY)
{
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    MoveObject(unit, destX, destY);
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    DeleteObjectTimer(CreateObjectAt("BlueRain", destX, destY), 25);
    Enchant(unit, "ENCHANT_LIGHT", 5.0);
}

void HealingTarget(int ptr)
{
    int unit = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(unit) && count)
    {
        MoveObject(ptr, GetObjectX(unit), GetObjectY(unit));
        if (count % 2)
            RestoreHealth(unit, 1);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, HealingTarget);
    }
    else if (IsObjectOn(ptr))
        Delete(ptr);
}

void CureBeaconTouched()
{
    int ptr;

    if (CurrentHealth(OTHER) && !HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC"))
    {
        Enchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC", 5.0);
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("LongBellsDown", 1);
        ptr = CreateObject("InvisibleLightBlueLow", 1);
        Enchant(ptr, "ENCHANT_RUN", 0.0);
        Enchant(ptr, "ENCHANT_SHIELD", 0.0);
        SetOwner(OTHER, ptr);
        LookWithAngle(ptr, 150);
        FrameTimerWithArg(1, ptr, HealingTarget);
    }
}

int CureHPBeacon(int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 10);
    Enchant(CreateObject("InvisibleLightBlueLow", wp), "ENCHANT_RUN", 0.0);
    Enchant(unit + 1, "ENCHANT_SHIELD", 0.0);
    Enchant(unit + 1, "ENCHANT_ANCHORED", 0.0);
    Enchant(unit + 1, "ENCHANT_SLOWED", 0.0);
    Enchant(unit + 1, "ENCHANT_VAMPIRISM", 0.0);
    Enchant(unit + 1, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
    Enchant(unit + 1, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
    Enchant(unit + 1, "ENCHANT_PROTECT_FROM_POISON", 0.0);
    SetCallback(unit, 9, CureBeaconTouched);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DrawImageAt(LocationX(wp), LocationY(wp), 1430);
    return unit;
}

void PowerItemBeacon()
{
    int res;

    if (GetGold(OTHER) >= 3000)
    {
        res = InvincibleInventoryItem(OTHER);
        if (res)
        {
            ChangeGold(OTHER, -3000);
            UniPrint(OTHER, MessageDesc(33) + IntToString(res) + MessageDesc(34));
        }
        else
            UniPrint(OTHER, MessageDesc(35));
    }
    else
        UniPrint(OTHER, MessageDesc(36));
}

int BeaconCreate(float sX, float sY, int sFunc)
{
    int bc = CreateObjectAt("Ankh", sX, sY);
    int ptr = GetMemory(0x750710);
    
    SetMemory(ptr + 0x2b8, ImportUnitCollideFunc());
    SetMemory(ptr + 0x2fc, sFunc);
    return bc;
}

void WandUsePixieShot(int sOwner)
{
    int mis = MissilePixie(GetObjectX(sOwner) + UnitAngleCos(sOwner, 13.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 13.0), sOwner);

    PushObject(mis, 33.0, GetObjectX(sOwner), GetObjectY(sOwner));
}

void Wand1UseHandler()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 20)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        WandUsePixieShot(OTHER);
        PlaySoundAround(OTHER, 520);
    }
}

int SummonMagicOrbStaff1(float sX, float sY, int sOwner)
{
    int staff = CreateObjectAt("OblivionOrb", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2dc, ImportUseItemFunc());
        SetMemory(ptr + 0x2fc, Wand1UseHandler);
        SetMemory(ptr + 0x2c4, 0x53a720);
        SetMemory(ptr + 0x2c8, ImportAllowAllDrop());
        SetOwner(sOwner, staff);
        Enchant(staff, "ENCHANT_VAMPIRISM", 0.0);
    }
    return staff;
}

void TradeMagicOrbStaff1()
{
    int initPos = GetOwner(SELF), staff;

    if (IsObjectOn(initPos))
    {
        if (GetGold(OTHER) >= 20000)
        {
            ChangeGold(OTHER, -20000);
            FrameTimerWithArg(1, SummonMagicOrbStaff1(GetObjectX(OTHER), GetObjectY(OTHER), OTHER), DelayGiveItemToOwner);
            UniPrint(OTHER, MessageDesc(37));
        }
        else
        {
            UniPrint(OTHER, MessageDesc(38));
        }
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void TradeWizardStaff()
{
    int initPos = GetOwner(SELF), staff;

    if (IsObjectOn(initPos))
    {
        if (GetGold(OTHER) >= 50000)
        {
            ChangeGold(OTHER, -50000);
            staff = MagicStaffClassCreate(OTHER);
            SetOwner(OTHER, staff);
            AbsolutelyWeaponPickupAndEquip(OTHER,staff);
            UniPrint(OTHER, MessageDesc(39));
        }
        else
            UniPrint(OTHER, MessageDesc(40));
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void TradeForceOfStaff()
{
    int initPos = GetOwner(SELF), staff;

    if (IsObjectOn(initPos))
    {
        if (GetGold(OTHER) >= 35000)
        {
            ChangeGold(OTHER, -35000);
            staff = ForceOfNatureStaffCreate(OTHER);
            SetOwner(OTHER, staff);
            AbsolutelyWeaponPickupAndEquip(OTHER, staff);
            UniPrint(OTHER, MessageDesc(41));
        }
        else
            UniPrint(OTHER, MessageDesc(42));
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void TradeMeteorWand()
{
    int initPos = GetOwner(SELF), staff;

    if (IsObjectOn(initPos))
    {
        if (GetGold(OTHER) >= 20000)
        {
            ChangeGold(OTHER, -20000);
            staff = MeteorWandClassCreate(OTHER);
            SetOwner(OTHER, staff);
            FrameTimerWithArg(1, staff, DelayForcePickItemToOwner);
            UniPrint(OTHER, MessageDesc(43));
        }
        else
            UniPrint(OTHER, MessageDesc(44));
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void TradeAllEnchants()
{
    int plr = GetPlayerIndex(OTHER), initPos = GetOwner(SELF);

    if (IsObjectOn(initPos))
    {
        if (plr>=0){
            if (GetGold(OTHER) >= 20000)
            {
                if (PlayerClassCheckFlag(plr,PLAYER_FLAG_ALL_BUFF))
                    UniPrint(OTHER, MessageDesc(45));
                else
                {
                    ChangeGold(OTHER, -20000);
                    PlayerClassSetFlag(plr, PLAYER_FLAG_ALL_BUFF);
                    PlayerSetAllBuff(OTHER);
                    UniPrint(OTHER, MessageDesc(46));
                }
            }
            else
                UniPrint(OTHER, MessageDesc(47));
        }
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void TradeAdvancedHarpoon()
{
    int plr = GetPlayerIndex(OTHER), initPos = GetOwner(SELF);

    if (IsObjectOn(initPos))
    {
        if (plr + 1)
        {
            if (GetGold(OTHER) >= 30000)
            {
                if (PlayerClassCheckFlag(plr,PLAYER_FLAG_THREADLIGHTLY))
                    UniPrint(OTHER, MessageDesc(48));
                else
                {
                    ChangeGold(OTHER, -30000);
                    PlayerClassSetFlag(plr,PLAYER_FLAG_THREADLIGHTLY);
                    UniPrint(OTHER, MessageDesc(49));
                }
            }
            else
                UniPrint(OTHER, MessageDesc(50));
        }
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void HammerSplash()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner) && CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 100, 1);
        Enchant(OTHER, "ENCHANT_HELD", 1.0);
    }
}

void HammerHitting()
{
    int owner = GetOwner(SELF), unit;

    if (MaxHealth(SELF) ^ CurrentHealth(SELF))
        RestoreHealth(SELF, MaxHealth(SELF) - CurrentHealth(SELF));
    if (CurrentHealth(owner))
    {
        unit = CreateObjectAt("ForceOfNatureCharge", GetObjectX(owner) + UnitAngleCos(owner, 27.0), GetObjectY(owner) + UnitAngleSin(owner, 27.0));
        SetOwner(owner, DummyUnitCreateById(OBJ_TROLL, GetObjectX( unit),GetObjectY(unit)));
        SetCallback(unit + 1, 9, HammerSplash);
        GreenExplosion(GetObjectX(unit), GetObjectY(unit));
        DeleteObjectTimer(unit, 21);
        DeleteObjectTimer(unit + 1, 1);
    }
}

int PowerHammerPlacing(int sOwner)
{
    int unit = CreateObjectAt("WarHammer", GetObjectX(sOwner), GetObjectY(sOwner));
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2d0, ImportWeaponHitFunc());
        SetMemory(ptr + 0x2fc, HammerHitting);
        SetOwner(sOwner, unit);
        LookWithAngle(unit, 255);
        UnitSetEnchantTime(unit, 11, 0);
    }
    return unit;
}

void TradePowerHammer()
{
    int initPos = GetOwner(SELF);

    if (IsObjectOn(initPos))
    {
        if (GetGold(OTHER) >= 25000)
        {
            ChangeGold(OTHER, -25000);
            FrameTimerWithArg(1, PowerHammerPlacing(OTHER), DelayGiveItemToOwner);
            UniPrint(OTHER, MessageDesc(51));
        }
        else
            UniPrint(OTHER, MessageDesc(52));
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void TradeFastHealing()
{
    int initPos = GetOwner(SELF), plr = GetPlayerIndex(OTHER);

    if (IsObjectOn(initPos))
    {
        if (plr + 1)
        {
            if (GetGold(OTHER) >= 15000)
            {
                if (PlayerClassCheckFlag(plr, PLAYER_FLAG_FASTHEALING))
                    UniPrint(OTHER, MessageDesc(53));
                else
                {
                    PlayerClassSetFlag(plr, PLAYER_FLAG_FASTHEALING);
                    ChangeGold(OTHER, -15000);
                    UniPrint(OTHER, MessageDesc(54));
                }
            }
            else
                UniPrint(OTHER, MessageDesc(55));
        }
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void ChakramCollide()
{
    int owner = GetOwner(SELF);

    while (1)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 220, 16);
            Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int SingleRingArrow(float sX, float sY, int sOwner)
{
    int unit = CreateObjectAt("RoundChakramInMotion", sX, sY);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2e8, 5483536); //projectile update
        SetMemory(ptr + 0x2b8, ImportUnitCollideFunc());
        SetMemory(ptr + 0x2fc, ChakramCollide);
        SetOwner(sOwner, unit);
    }
    return unit;
}

void CastArrowRing(int sOwner)
{
    float xProfile = GetObjectX(sOwner), yProfile = GetObjectY(sOwner);
    int i;

    if (CurrentHealth(sOwner))
    {
        for (i = 0 ; i < 36 ; i ++)
            PushObject(SingleRingArrow(xProfile + MathSine(i * 10 + 90, 13.0), yProfile + MathSine(i * 10, 13.0), sOwner), 33.0, xProfile, yProfile);
    }
}

void UseKingsAmulet()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 120)
    {
        UniPrint(OTHER, MessageDesc(56));
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        FrameTimerWithArg(1, GetCaller(), CastArrowRing);
        PlaySoundAround(OTHER, 560);
    }
}

int SummonKingsAmulet(int sOwner)
{
    int unit = CreateObjectAt("AmuletofCombat", GetObjectX(sOwner), GetObjectY(sOwner));
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2dc, ImportUseItemFunc());
        SetMemory(ptr + 0x2fc, UseKingsAmulet);
        Enchant(unit, "ENCHANT_INFRAVISION", 0.0);
        SetOwner(sOwner, unit);
    }
    return unit;
}

void TradeKingsAmulet()
{
    int initPos = GetOwner(SELF);

    if (IsObjectOn(initPos))
    {
        if (GetGold(OTHER) >= 30000)
        {
            FrameTimerWithArg(1, SummonKingsAmulet(OTHER), DelayGiveItemToOwner);
            ChangeGold(OTHER, -30000);
            UniPrint(OTHER, MessageDesc(57));
            UniPrint(OTHER, MessageDesc(58));
        }
        else
            UniPrint(OTHER, MessageDesc(59));
        MoveObject(OTHER, GetObjectX(initPos), GetObjectY(initPos));
    }
}

void PlacingSpecialMarketBeacon()
{
    int unit = CreateObject("InvisibleLightBlueLow", 171);

    SetOwner(unit, BeaconCreate(LocationX(170), LocationY(170), TradeMagicOrbStaff1));
    SetOwner(unit, BeaconCreate(LocationX(197), LocationY(197), TradeAllEnchants));
    SetOwner(unit, BeaconCreate(LocationX(198), LocationY(198), TradePowerHammer));
    SetOwner(unit, BeaconCreate(LocationX(199), LocationY(199), TradeFastHealing));
    SetOwner(unit, BeaconCreate(LocationX(200), LocationY(200), TradeKingsAmulet));
    SetOwner(unit, BeaconCreate(LocationX(201), LocationY(201), TradeAdvancedHarpoon));
    SetOwner(unit, BeaconCreate(LocationX(209), LocationY(209), TradeWizardStaff));
    SetOwner(unit, BeaconCreate(LocationX(210), LocationY(210), TradeMeteorWand));
    SetOwner(unit, BeaconCreate(LocationX(211), LocationY(211), TradeForceOfStaff));
    TeleportMark(172, 171);
    TeleportMark(173, 12);
    TeleportMark(195, 12);
    TeleportMark(196, 12);
    UniPrintToAll(MessageDesc(73));
}

void MapSignInit(int langCheck)
{
    if (!langCheck)
    {
        MapSignInitEng();
        return;
    }
    RegistSignMessage(Object("UniSign1"), "대형마트 습격하기: 이마트를 털어라!");
    RegistSignMessage(Object("UniSign2"), "스페셜 샵 입장하는 곳!: 앵크를 만지면 스페셜 샵으로 들어갈 수 있어요");
    RegistSignMessage(Object("UniSign3"), "스페셜 샵에서 나가려면 좀더 앞으로 나오세요");
    RegistSignMessage(Object("UniSign4"), "마법 이펙트 위에 서 있으면 체력이 서서히 오르기 시작할 거에요");
    RegistSignMessage(Object("UniSign5"), "빨간색 열쇠꽂이와 파란색 열쇠꽂이 각각에 해당하는 열쇠를 가져와 꽂으세요");
    RegistSignMessage(Object("UniSign6"), "주변에서 수상한 냄새가 난다... 기분이 뭔가 좋지않아!");
    RegistSignMessage(Object("UniSign7"), "지하 대도서관: 제발 좀 문 팍열고 나가지 마요... 여기엔 적군이 많으니 살살좀... 진짜 ㅡㅡ");
    RegistSignMessage(Object("UniSign8"), "여기 부터는 문학소설 코너");
    RegistSignMessage(Object("UniSign9"), "게임 꿀팁- 리치아이돌 키 이 방 끝에 있어요! 어때요? 개 꿀팁 인정? 어 인정ㅋ");
    RegistSignMessage(Object("UniSign10"), "준코: -청소년 출입금지-");
    RegistSignMessage(Object("UniSign11"), "지하 대도서관으로 내려가는 엘리베이터 입니다. 적 수가 많아 위험할 수 있으니 뛰어들어가지 마세요");
    RegistSignMessage(Object("UniSign12"), "몬스터 소환오벨리스크를 비콘위에 올리면 10시방향 비밀벽이 열려요. 안열리면 버그임");
    RegistSignMessage(Object("UniSign13"), "총독관 대 회의실: 마음에 준비는 단단히 했겠지?");
    RegistSignMessage(Object("UniSign14"), "개꿀팁 드림: 채팅에 '//t' 라고 치면 마을로 공간이동. 마을에서 치면 필드로 되돌아감");
    RegistSignMessage(Object("UniSign15"), "유용한 게임 명령어 '//t' 를 채팅입력 시 마을로 공간이동됩니다. 마을에 있으면 필드로 되돌아 갑니다");
    RegistSignMessage(Object("UniSign16"), "이 엘리베이터를 작동 시킬 스위치는 철문 안쪽 방에 있어요");
    RegistSignMessage(Object("UniSign17"), "도서관 지하2층- 어둠의 현관");

    RegistSignMessage(Object("ShopSign1"), "조심스럽게 걷기 배우기(3만골드): 짧은 거리를 빠르게 이동할 수 있습니다");
    RegistSignMessage(Object("ShopSign2"), "마법의 지팡이(2만골드): 마법의 관통화살을 뿜어내는 요술 지팡이!");
    RegistSignMessage(Object("ShopSign3"), "올 엔첸 능력 배우기(2만골드): 속성저항 및 흡혈 등의 유용한 속성을 항상 적용시켜줍니다");
    RegistSignMessage(Object("ShopSign4"), "초강력 뿅망치(25000골드): 일반 해머보다는 더 강력한 해머 입니다");
    RegistSignMessage(Object("ShopSign5"), "패스트힐링 능력 배우기(15000골드): 체력회복 속도를 증가시켜줍니다");
    RegistSignMessage(Object("ShopSign6"), "전투의 목걸이(3만 골드): 목걸이를 사용할 때마다 특수한 마법이 시전됩니다");
    RegistSignMessage(Object("ShopSign7"), "리치 돌진 지팡이(5만 골드): 말이 필요없는 강력한 지팡이");
    RegistSignMessage(Object("ShopSign8"), "메테오 완드 구입(20000 골드): 타게팅 위치에 메테오 시전");
    RegistSignMessage(Object("ShopSign9"), "포스 오브 네이처 지팡이(35000 골드): 소환술사가 사용했던 포오네 지팡이와 효과가 같습니다");
    RegistSignMessage(Object("ShopSignA"), "인벤토리에 있는 모든 아이템을 무적으로 만들어드려요 (1회당 3천골드)");
}

void MapSignInitEng()
{
    RegistSignMessage(Object("UniSign1"), "Invading the Wall Mart: Rob Rob!");
    RegistSignMessage(Object("UniSign2"), "Special shop enterance: Touch the ankh to get in");
    RegistSignMessage(Object("UniSign3"), "Come closer to get out of the shop");
    RegistSignMessage(Object("UniSign4"), "Once you stand on the magical effect it would gradually restore your health");
    RegistSignMessage(Object("UniSign5"), "Put the right colored keys on each key hole");
    RegistSignMessage(Object("UniSign6"), "I smell something suspicious, not feeling well");
    RegistSignMessage(Object("UniSign7"), "Underground Library: Don't rush to the door and be patient, or you'll be ambushed by neumerous enemies all of a sudden");
    RegistSignMessage(Object("UniSign8"), "From here is the literary novels");
    RegistSignMessage(Object("UniSign9"), "Big spoiler; An Idol key is likely to be at the end of chamber. Have some gratitude for me!");
    RegistSignMessage(Object("UniSign10"), "Not allowed for juvenile");
    RegistSignMessage(Object("UniSign11"), "Elevator down to underground library; Don't rush unless if you want to be enclosed by ferocious enemies");
    RegistSignMessage(Object("UniSign12"), "Put an summoning obleisk to the pressure plate, it will show you the way on 10'o clock, or report me");
    RegistSignMessage(Object("UniSign13"), "Governor's conference room: Make sure you are ready!");
    RegistSignMessage(Object("UniSign14"), "Tip: Enter '//t' on chat, you'll be teleported to the start point, as well as saving current position. Chat again and you go back to the saved point");
    RegistSignMessage(Object("UniSign15"), "Typing command line '//t' on chat makes you to teleport to the start point. If is done on start point, would be teleported to the previous point again");
    RegistSignMessage(Object("UniSign16"), "The activation switch for this elevator is on the chamber over the iron fence");
    RegistSignMessage(Object("UniSign17"), "Library -B2-");

    RegistSignMessage(Object("ShopSign1"), "Enhance Harpoon(30000Gold)): Cause an explosion when harpoon gets to the enemy units");
    RegistSignMessage(Object("ShopSign2"), "Magical Wand(20000Gold): This wand launches magical arrows!");
    RegistSignMessage(Object("ShopSign3"), "All useful enchants(20000Gold): Gives you protection and vampirism enchants permanently");
    RegistSignMessage(Object("ShopSign4"), "Supa Dupa Warhammer(25000Gold): Much more powerful than ordinary warhammers");
    RegistSignMessage(Object("ShopSign5"), "Fast healing ability(15000Gold): Speeds up regeneration");
    RegistSignMessage(Object("ShopSign6"), "Amulet of Combat(30000Gold): By clicking some kinda spells would be casted");
    RegistSignMessage(Object("ShopSign7"), "Lich charging wand50000Gold): Needless to say, just try");
    RegistSignMessage(Object("ShopSign8"), "Meteor wand(20000Gold): Cast meteor spell on target position");
    RegistSignMessage(Object("ShopSign9"), "Force of Nature staff(35000 Gold): Literally FON staff of orignial");
    RegistSignMessage(Object("ShopSignA"), "Make all items on inventory to be invincible(3000Gold each)");
}

void DisableObject(int sUnit)
{
    if (IsObjectOn(sUnit))
        ObjectOff(sUnit);
}

void LibraryExitElevInit()
{
    int elev = Object("LibraryElev");

    ObjectOn(elev);
    FrameTimerWithArg(25, elev, DisableObject);
}

void GolemIsDead()
{
    CreateRandomItemCommon(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF)));
    UniChatMessage(SELF, MessageDesc(17), 90);
}

void HorrorGolemSummon(int sUnit)
{
    int unit;

    if (IsObjectOn(sUnit))
    {
        unit = CreateObjectAt("StoneGolem", GetObjectX(sUnit), GetObjectY(sUnit));

        SetUnitMaxHealth(unit, 1050);
        SetCallback(unit, 5, GolemIsDead);
        SetCallback(unit, 7, FieldMobHurtHandler);
        UniChatMessage(unit, MessageDesc(18), 180);
        Delete(sUnit);
    }
}

void StrangeWizDead()
{
    int mobMake = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));

    UniChatMessage(mobMake, MessageDesc(19), 90);
    DeleteObjectTimer(SELF, 90);
    FrameTimerWithArg(79, mobMake, HorrorGolemSummon);
}

void SummonStrangeWizard(int sUnit)
{
    int wiz;
    
    if (IsObjectOn(sUnit))
    {
        wiz = CreateObjectAt("Wizard", GetObjectX(sUnit), GetObjectY(sUnit));
        WizardSubProcess(wiz);
        SetCallback(wiz, 3, DefaultMobInitSight);
        SetCallback(wiz, 7, FieldMobHurtHandler);
        SetCallback(wiz, 10, HearAroundEnemy);
        SetUnitScanRange(wiz, 450.0);
        AggressionLevel(wiz, 0.5);
        SetCallback(wiz, 5, StrangeWizDead);
        Delete(sUnit);
    }
}

void RespectStrangeWizard(int sLocation)
{
    int mobMake = CreateObject("InvisibleLightBlueLow", sLocation);

    Effect("SMOKE_BLAST", GetObjectX(mobMake), GetObjectY(mobMake), 0.0, 0.0);
    FrameTimerWithArg(8, mobMake, SummonStrangeWizard);
}

void EntryWizRoom()
{
    int i;

    ObjectOff(SELF);
    for (i = 0 ; i < 6 ; i ++)
        RespectStrangeWizard(202 + i);
}

void BerserkerEnergyTouched()
{
    int owner = GetOwner(GetTrigger() - 1);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 500, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 1.0);
        GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
    }
}

void BerserkerEnergyLoop(int sUnit)
{
    int owner = GetOwner(sUnit), durate = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (CurrentHealth(owner) && durate)
        {
            if (IsVisibleTo(sUnit, sUnit + 1))
            {
                DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(sUnit + 1), GetObjectY(sUnit + 1)), 18);
                MoveObject(sUnit + 1, GetObjectX(sUnit + 1) + UnitAngleCos(sUnit + 1, 19.0), GetObjectY(sUnit + 1) + UnitAngleSin(sUnit + 1, 19.0));
                FrameTimerWithArg(1, sUnit, BerserkerEnergyLoop);
                LookWithAngle(sUnit, durate - 1);
                break;
            }
        }
        Delete(sUnit);
        Delete(sUnit + 1);
        break;
    }
}

void CastBerserkerEnergyPar(int sOwner)
{
    int unit;

    if (CurrentHealth(sOwner))
    {
        unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sOwner) + UnitAngleCos(sOwner, 21.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 21.0));
        SetOwner(sOwner, DummyUnitCreateById(OBJ_LICH_LORD, GetObjectX(unit), GetObjectY(unit)) - 1);
        LookWithAngle(unit, 31);
        LookWithAngle(unit + 1, GetDirection(sOwner));
        SetCallback(unit + 1, 9, BerserkerEnergyTouched);
        FrameTimerWithArg(1, unit, BerserkerEnergyLoop);
    }
}

void MagicStaffClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 35)
    {
        UniPrint(OTHER, MessageDesc(60));
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        FrameTimerWithArg(3, GetCaller(), CastBerserkerEnergyPar);
    }
}

void MagicStaffClassPick()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF);

    if (ABS(cFps - cTime) < 10)
        return;
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        AbsolutelyWeaponPickupAndEquip(OTHER, SELF);
        UniPrint(OTHER, MessageDesc(61));
    }
}

int MagicStaffClassCreate(int sUnit)
{
    int unit = CreateObjectAt("SulphorousFlareWand", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitCallbackOnPickup(unit, MagicStaffClassPick);
    SetUnitCallbackOnUseItem(unit, MagicStaffClassUse);
    SetUnit1C(unit,0);
    return unit;
}

void MeteorWandClassStrike(int sGlow)
{
    int owner = GetOwner(sGlow);

    if (CurrentHealth(owner))
    {
        if (IsVisibleTo(owner, sGlow))
        {
            SetOwner(owner, FallingMeteor(GetObjectX(sGlow), GetObjectY(sGlow), 310, -8.0));
            PlaySoundAround(sGlow, 85);
        }
        else
            UniPrint(owner, MessageDesc(62));
    }
    Delete(sGlow);
}

void MeteorWandClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF), glow;

    if (ABS(cFps - cTime) < 25)
    {
        UniPrint(OTHER, MessageDesc(63));
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        glow = CreateObjectAt("Moonglow", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(OTHER, glow);
        FrameTimerWithArg(1, glow, MeteorWandClassStrike);
    }
}

int MeteorWandClassCreate(int sUnit)
{
    int unit = CreateObjectAt("FireStormWand", GetObjectX(sUnit), GetObjectY(sUnit));
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2c4, ImportUnitPickupFunc());
        SetMemory(ptr + 0x2e4, MagicStaffClassPick);
        SetMemory(ptr + 0x2dc, ImportUseItemFunc());
        SetMemory(ptr + 0x2fc, MeteorWandClassUse);
    }
    return unit;
}

void ForceOfNatueStaffClassUse()
{
    int cFps = GetMemory(0x84ea04);
    int cTime = GetUnit1C(SELF), glow;

    if (ABS(cFps - cTime) < 38)
    {
        UniPrint(OTHER, MessageDesc(64));
        return;
    }
    if (CurrentHealth(OTHER))
    {
        SetUnit1C(SELF, cFps);
        CastSpellObjectObject("SPELL_FORCE_OF_NATURE", OTHER, OTHER);
    }
}

int ForceOfNatureStaffCreate(int sUnit)
{
    int unit = CreateObjectAt("InfinitePainWand", GetObjectX(sUnit), GetObjectY(sUnit));
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2c4, ImportUnitPickupFunc());
        SetMemory(ptr + 0x2e4, MagicStaffClassPick);
        SetMemory(ptr + 0x2dc, ImportUseItemFunc());
        SetMemory(ptr + 0x2fc, ForceOfNatueStaffClassUse);
    }
    return unit;
}

int SummonJandor(int sUnit)
{
    int mob = CreateObjectAt("AirshipCaptain", GetObjectX(sUnit), GetObjectY(sUnit));
    
    AirshipCaptainSubProcess(mob);
    return mob;
}

void JandorAttackToTargetUser(int sUnit)
{
    int enemy = ToInt(GetObjectZ(sUnit)), mob = sUnit - 1;

    if (CurrentHealth(enemy) && CurrentHealth(mob))
    {
        CreatureFollow(mob, enemy);
        AggressionLevel(mob, 1.0);
        UniChatMessage(mob, MessageDesc(20), 120);
    }
    Delete(sUnit);
}

void JandorGroupsHandler(int sUnit)
{
    int durate = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        if (durate)
        {
            FrameTimerWithArg(1, SummonJandor(sUnit) + 1, JandorAttackToTargetUser);
            Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(sUnit), GetObjectY(sUnit)), GetOwner(sUnit));
            FrameTimerWithArg(6, sUnit, JandorGroupsHandler);
            LookWithAngle(sUnit, durate - 1);
            break;
        }
        Delete(sUnit);
        break;
    }
}

void JandorRemoveWalls()
{
    WallUtilOpenWallAtObjectPosition(214);
    WallUtilOpenWallAtObjectPosition(215);
    WallUtilOpenWallAtObjectPosition(216);
}

void HiddenBookPick()
{
    int mobMake = CreateObject("InvisibleLightBlueLow", 213);

    LookWithAngle(mobMake, 32);
    SetOwner(OTHER, mobMake);
    FrameTimerWithArg(77, mobMake, JandorGroupsHandler);
    FrameTimer(85, JandorRemoveWalls);
    Delete(SELF);
    UniPrint(OTHER, MessageDesc(65));
}

int LibraryHiddenBook()
{
    int book = CreateObject("CommonSpellBook", 212);
    int ptr = GetMemory(0x750710);

    if (ptr)
    {
        SetMemory(ptr + 0x2c4, ImportUnitPickupFunc());
        SetMemory(ptr + 0x2e4, HiddenBookPick);
    }
    return book;
}

void LibraryPartStart()
{
    int chkSum;

    ObjectOff(SELF);
    if (chkSum) return;
    chkSum = LibraryHiddenBook();
    ChairDirInit();
    LibraryKey();
    LibraryExitElevInit();
}

void TestTeleport()
{
    float xProfile = 5473.0, yProfile = 3582.0;
    int staff;

    MoveObject(OTHER, xProfile, yProfile);
    if (!staff)
    {
        staff = MagicStaffClassCreate(CreateObjectAt("RedPotion", xProfile, yProfile));
    }
}

int ChairDir(int sDir)
{
    int arr[8];

    if (sDir < 0)
    {
        arr[0] = 583; arr[1] = 586; arr[2] = 585; arr[3] = 584; arr[4] = 582;
        arr[5] = 579; arr[6] = 580; arr[7] = 581;
    }
    return arr[sDir];
}

void MobInvisibleChairLoop(int sUnit)
{
    int curDir;
    if (CurrentHealth(sUnit))
    {
        if (ToInt(Distance(GetObjectX(sUnit), GetObjectY(sUnit) + 2.0, GetObjectX(sUnit + 1), GetObjectY(sUnit + 1))))
        {
            curDir = ((GetDirection(sUnit) - 16) % 256) / 32;
            if (GetDirection(sUnit + 1) ^ curDir)
            {
                LookWithAngle(sUnit + 1, curDir);
                SetMemory(UnitToPtr(sUnit + 1) + 4, ChairDir(curDir));
                MoveObject(sUnit + 1, 100.0, 100.0);
            }
            else
                MoveObject(sUnit + 1, GetObjectX(sUnit), GetObjectY(sUnit) + 2.0);
        }
        FrameTimerWithArg(1, sUnit, MobInvisibleChairLoop);
    }
    else
        Delete(sUnit + 1);
}

void MobCandleAttached(int sUnit)
{
    if (CurrentHealth(sUnit))
    {
        if (ToInt(Distance(GetObjectX(sUnit), GetObjectY(sUnit) + 2.0, GetObjectX(sUnit + 1), GetObjectY(sUnit + 1))))
            MoveObject(sUnit + 1, GetObjectX(sUnit), GetObjectY(sUnit) + 2.0);
        FrameTimerWithArg(1, sUnit, MobCandleAttached);
    }
    else
        Delete(sUnit + 1);
}

void TouchSoulChairNothing()
{
    return;
}

void TouchSoulChair()
{
    if (IsObjectOn(SELF)) return;
    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
    {
        Frozen(SELF, 0);
        ObjectOn(SELF);
        AggressionLevel(SELF, 1.0);
        LookAtObject(SELF, OTHER);
        SetCallback(SELF, 5, DefaultMobDead);
        SetCallback(SELF, 7, FieldMobHurtHandler);
        SetCallback(SELF, 9, TouchSoulChairNothing);
        PlaySoundAround(SELF, 657);
        FrameTimerWithArg(1, GetTrigger(), MobInvisibleChairLoop);
        UniChatMessage(SELF, MessageDesc(21), 150);
    }
}

void TouchSoulCandle()
{
    if (IsObjectOn(SELF)) return;
    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
    {
        Frozen(SELF, 0);
        ObjectOn(SELF);
        AggressionLevel(SELF, 1.0);
        LookAtObject(SELF, OTHER);
        SetCallback(SELF, 5, DefaultMobDead);
        SetCallback(SELF, 7, FieldMobHurtHandler);
        SetCallback(SELF, 9, TouchSoulChairNothing);
        PlaySoundAround(SELF, 707);
        FrameTimerWithArg(1, GetTrigger(), MobCandleAttached);
        UniChatMessage(SELF, MessageDesc(22), 150);
    }
}

void ChairDirInit()
{
    string sCandleName = "Candleabra2";
    int mob;

    ChairDir(-1);
    MobClassChairSoul(217, 7);
    MobClassChairSoul(218, 3);
    MobClassChairSoul(219, 1);
    MobClassChairSoul(221, 8);
    MobClassChairSoul(222, 2);
    MobClassChairSoul(220, 4);
    MobClassChairSoul(223, 1);
    MobClassChairSoul(224, 6);
    MobClassChairSoul(225, 5);
    MobClassCandleSoul(226, sCandleName);
    MobClassCandleSoul(227, sCandleName);
    MobClassCandleSoul(228, sCandleName);
    MobClassCandleSoul(229, sCandleName);
    MobClassCandleSoul(230, sCandleName);
    MobClassCandleSoul(231, sCandleName);
    MobClassCandleSoul(234, sCandleName);
    MobClassCandleSoul(235, sCandleName);

    MobClassCandleSoul(232, "Candleabra5");
    MobClassCandleSoul(233, "Candleabra5");
}

int MobClassChairSoul(int sLocation, int sChairDir)
{
    int mob = CreateObject("TalkingSkull", sLocation);

    UnitNoCollide(CreateObjectAt("DarkWoodenChair" + IntToString(sChairDir), GetObjectX(mob), GetObjectY(mob) + 2.0));
    Frozen(mob + 1, 1);
    ObjectOff(mob);
    Frozen(mob, 1);
    TalkingSkullSubProcess(mob);
    SetCallback(mob, 9, TouchSoulChair);

    return mob;
}

int MobClassCandleSoul(int sLocation, string sCandleName)
{
    int mob = CreateObject("TalkingSkull", sLocation);

    UnitNoCollide(CreateObjectAt(sCandleName, GetObjectX(mob), GetObjectY(mob) + 2.0));
    Frozen(mob + 1, 1);
    ObjectOff(mob);
    Frozen(mob, 1);
    TalkingSkull2SubProcess(mob);
    SetCallback(mob, 9, TouchSoulCandle);

    return mob;
}

void WizRunAway()
{
	if (UnitCheckEnchant(SELF, GetLShift(29)))
		EnchantOff(SELF, EnchantList(29));
}

int SummonMobRedWizard(int location)
{
    int redWiz = CreateObjectAt("WizardRed", LocationX(location), LocationY(location));
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    UnitLinkBinScript(redWiz, WizardRedBinTable());
    UnitZeroFleeRange(redWiz);
    SetUnitMaxHealth(redWiz, 484);
    SetCallback(redWiz, 8, WizRunAway);
    SetUnitStatus(redWiz, GetUnitStatus(redWiz) ^ 0x20);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_LIGHTNING"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x8000000);
    }
    return redWiz;
}

int SummonFireballWizard(int location)
{
    int wiz = CreateObjectAt("StrongWizardWhite", LocationX(location), LocationY(location));

    UnitLinkBinScript(wiz, StrongWizardWhiteBinTable());
    SetUnitMaxHealth(wiz, 430);
    return wiz;
}

void UnlockJailLocker()
{
    ObjectOff(SELF);
    UnlockDoor(Object("jailLock1"));
    UnlockDoor(Object("jailLock2"));
    UniPrint(OTHER, "문의 잠금이 해제 되었습니다");
}

void InitJailArea()
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        CreateObjectAt("PiledBarrels3", LocationX(251), LocationY(251));
        TeleportLocationVector(251, -23.0, -23.0);
        if (i < 6)
            CreateObjectAt("PiledBarrels3", LocationX(255 + i), LocationY(255 + i));
    }
    SummonNecromancer(252);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 253), 225);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 253), 225);

    SetUnitMaxHealth(SummonDefaultMob("WeirdlingBeast", 254), 295);
    SetUnitMaxHealth(SummonDefaultMob("WeirdlingBeast", 254), 295);

    SummonNecromancer(261);
    SummonNecromancer(262);

    SummonMaiden(264, 0, 255, 0);
    SummonNecromancer(264);
    SummonMaiden(266, 0, 255, 0);
    SetUnitMaxHealth(SummonDefaultMob("EmberDemon", 267), 130);
    
    RegistSignMessage(Object("jailSign1"), "출입제한 구역- 이곳은 본 건물의 기계실 이므로 관계자외 꺼져");
}

void ClearRightWall()
{
    ObjectOff(SELF);
    WallUtilOpenWallAtObjectPosition(263);
    UniPrint(OTHER, "비밀통로가 열렸습니다");
}

void FireTrapTriggered()
{
    ObjectOn(Object("firetrp1"));
    ObjectOn(Object("firetrp2"));
}

void FireTrapDisabled()
{
    ObjectOff(Object("firetrp1"));
    ObjectOff(Object("firetrp2"));
}

int BookcaseMovingSingle(int single, int location, int finishLocation)
{
    if (LocationX(location) < LocationX(274))
        TeleportLocationVector(location, 1.0, 1.0);
    else if (LocationX(location) < LocationX(finishLocation))
        TeleportLocationVector(location, 1.0, -1.0);
    else
        return TRUE;

    MoveObject(single, LocationX(location), LocationY(location));
    return FALSE;
}

void ClearLibraryExWalls()
{
    int u;

    for (u = 0 ; u < 8 ; u ++)
    {
        WallUtilOpenWallAtObjectPosition(281);
        TeleportLocationVector(281, 23.0, 23.0);
    }
}

void LibraryExPartMobSettings()
{
    SetUnitMaxHealth(SummonDefaultMob("Goon", 290), 225);
    SetUnitMaxHealth(SummonDefaultMob("Goon", 291), 225);
    SetUnitMaxHealth(SummonDefaultMob("Shade", 292), 260);
    SetUnitMaxHealth(SummonDefaultMob("Shade", 293), 260);

    SummonNecromancer(294);
    SummonNecromancer(296);
    SummonNecromancer(297);
    SummonMaiden(295, 225, 64, 240);
    SummonMaiden(297, 225, 64, 240);
}

void BookcaseMovingSecond(int first)
{
    int i, res = 0;

    for (i = 0 ; i < 6 ; i ++)
        res += BookcaseMovingSingle(first + i, 268 + i, 275 + i);
    if (res ^ 6)
        FrameTimerWithArg(1, first, BookcaseMovingSecond);
    else
        ClearLibraryExWalls(); //@brief. end of action
}

void BookcaseMovingFirst(int first)
{
    int i, totalCount;

    if (totalCount < 23)
    {
        for (i = 0 ; i < 6 ; i ++)
        {
            TeleportLocationVector(268 + i, -1.0, 1.0);
            MoveObject(first + i, LocationX(268 + i), LocationY(268 + i));
        }
        totalCount ++;
        FrameTimerWithArg(1, first, BookcaseMovingFirst);
    }
    else
    {
        WallUtilOpenWallAtObjectPosition(282);
        FrameTimerWithArg(3, first, BookcaseMovingSecond);
        LibraryExPartMobSettings();
    }
}

void StartLibraryExBookcase()
{
    ObjectOff(SELF);
    int t;
    QueryLibraryBookEx(&t,0);
    FrameTimerWithArg(1, t, BookcaseMovingFirst);
}

void ClearEastWalls()
{
    int u;

    ObjectOff(SELF);
    for (u = 0 ; u < 8 ; u ++)
    {
        WallUtilOpenWallAtObjectPosition(283);
        TeleportLocationVector(283, 23.0, 23.0);
    }
    SummonMobRedWizard(287);
    SummonNecromancer(288);
    SummonNecromancer(289);
}

void ClearSouthWalls()
{
    int u;

    for (u = 0 ; u < 14 ; u ++)
    {
        WallUtilOpenWallAtObjectPosition(284);
        TeleportLocationVector(284, -23.0, 23.0);
    }
}

void RespectSurpriseWiz(int location)
{
    int wiz = SummonDefaultMob("Demon", location);

    SetUnitMaxHealth(wiz, 600);
    Effect("SMOKE_BLAST", LocationX(location), LocationY(location), 0.0, 0.0);
}

void ClearEWWalls()
{
    int u;

    ObjectOff(SELF);
    for (u = 0 ; u < 8 ; u ++)
    {
        WallUtilOpenWallAtObjectPosition(285);
        WallUtilOpenWallAtObjectPosition(286);
        TeleportLocationVector(285, 23.0, 23.0);
        TeleportLocationVector(286, 23.0, 23.0);
    }
    FrameTimerWithArg(8, 298, RespectSurpriseWiz);
    FrameTimerWithArg(8, 299, RespectSurpriseWiz);
    FrameTimerWithArg(8, 300, RespectSurpriseWiz);
    SummonMobRedWizard(301);
    SummonMaiden(302, 255, 0, 0);
    SummonMaiden(305, 255, 0, 0);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 303), 325);
    SetUnitMaxHealth(SummonDefaultMob("BlackWidow", 304), 325);

    CreateObjectAt("BearTrap", LocationX(314), LocationY(314));
}

int InitPlacingBookcase()
{
    int i, baseUnit = CreateObjectAt("ImaginaryCaster", LocationX(268), LocationY(268));

    Delete(baseUnit);
    for (i = 0 ; i < 6 ; i ++)
        Frozen(CreateObjectAt("MovableBookcase2", LocationX(268 + i), LocationY(268 + i)), TRUE);
    return baseUnit + 1;
}

void InitLibraryExZone()
{
    QueryLibraryBookEx(0, InitPlacingBookcase() );
    InitLibraryArrowTraps();
}

void InitLibraryArrowTraps()
{
    int u,t;

    for (u = 0 ; u < 9 ; u ++)
    {
        t=Object("libarrowtrap" + IntToString(u + 1));
        QueryLibraryArrowTraps(0, t, u);
        ObjectOff(t);
        HookArrowTraps(t);
    }
}

void ActivateLibArrowTrp()
{
    int u,t;

    for (u = 0 ; u < 9 ; u ++)
    {
        QueryLibraryArrowTraps(&t, 0, u);
        ObjectOn(t);
    }
}

void DisabledLibArrowTrp()
{
    int u,t;

    for (u = 0 ; u < 9 ; u ++)
    {
        QueryLibraryArrowTraps(&t,0,u);
        ObjectOff(t);
    }
}

int RespectFireWizSingle(int location)
{
    int wiz = SummonFireballWizard(location);

    Enchant(wiz, "ENCHANT_SHOCK", 0.0);
    UniChatMessage(wiz, "갑툭튀 시전!!\n깜놀했지? ㅋㅋㅋ", 107);
    Effect("TELEPORT", LocationX(location), LocationY(location), 0.0, 0.0);
    Effect("COUNTERSPELL_EXPLOSION", LocationX(location), LocationY(location), 0.0, 0.0);
    Effect("SMOKE_BLAST", LocationX(location), LocationY(location), 0.0, 0.0);
    return wiz;
}

void RespectFireWizs()
{
    int u;

    ObjectOff(SELF);
    for (u = 0 ; u < 6 ; u ++)
        LookAtObject(RespectFireWizSingle(306 + u), OTHER);
    SummonNecromancer(312);
    SummonNecromancer(313);

    UniPrintToAll("**깜짝출연 -호바스- ");
}

void BeforeExecIndexLoop(int pInstance){
    HashPushback(pInstance, OBJ_FAN_CHAKRAM_IN_MOTION, ShurikenEvent);
    // HashPushback(pInstance, OBJ_HARPOON_BOLT, PlayerHarpoonHandler);
}

void OnPlayerEnteredMap(int pInfo){
    int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
    {
        pUnit = GetMemory(ptr+0x2c);
        int pIndex = GetMemory(pInfo+0x810);
        PlayerClassOnInit   (pIndex, pUnit);
        ShowQuestIntroOne(pUnit, 237, "WarriorChapterBegin5", "GeneralPrint:MapNameHecubahLair");
    }
    
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
