
#include "mcdonald_utils.h"
#include "mcdonald_resource.h"
#include "libs/playerupdate.h"
#include "libs/networkRev.h"
#include "libs/coopteam.h"
#include "libs/indexloop.h"
#include "libs/mathlab.h"
#include "libs/buff.h"
#include "libs/sound_define.h"
#include "libs/format.h"
#include "libs/clientside.h"
#include "libs/spellutil.h"
#include "libs/reaction.h"
#include "libs/itemproperty.h"
#include "libs/wallutil.h"
#include "libs/logging.h"
#include"libs/weaponcapacity.h"
#include "libs/voiceList.h"

int g_DungeonCnt = 5;
int g_DungeonClear = 0;
int g_DungeonMobCnt = 150;
int g_player[20];
int g_Pcre[40];
int *g_pItemFn;
int g_itemFnCount;
string *g_pListPotions;
string *g_pListWeapons;
string *g_pListArmors;

#define PLAYER_FLAG_WINDBOOSTER 2
#define PLAYER_FLAG_AUTODEATHRAY 4
#define PLAYER_FLAG_ELECTRIC_LIGHTNING 8
#define PLAYER_FLAG_DEATH_FLAG 16

static void NetworkUtilClientMain()
{
    InitializeResources();
}

void RecoveryUnitSplashSection(int unit)
{
    if (MaxHealth(unit))
    {
        SetMemory(UnitToPtr(unit) + 0x1c, 0);
    }
}

void ArrayInsert(int data, int idx)
{
    g_Pcre[idx] = data;
}

int ArraySearch(int plr)
{
    int idx = plr * 4, i;

    for (i = 0 ; i < 4 ; i ++)
    {
        if (CurrentHealth(g_Pcre[idx + i])) continue;
        else    return idx + i;
    }
    return -1;
}

void EmptyArray(int plr)
{
    int i, idx = plr * 4;
    for (i = 0 ; i < 4 ; i ++)
    {
        if (CurrentHealth(g_Pcre[idx + i]))
        {
            Delete(g_Pcre[idx + i]);
            Delete(g_Pcre[idx + i] + 1);
        }
    }
}

void BringArray(int plr)
{
    int i, idx = plr * 4;

    for (i = 0 ; i < 4 ; i ++)
    {
        if (CurrentHealth(g_Pcre[idx + i]) && !IsVisibleTo(g_Pcre[idx + i], g_player[plr]))
            MoveObject(g_Pcre[idx + i], GetObjectX(g_player[plr]), GetObjectY(g_player[plr]));
    }
}

void ThunderBoltCollideHandler()
{
    int owner = ToInt(GetObjectZ(GetOwner(SELF)));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 85, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.09);
    }
}

void ThunderBoltCritical(int ptr)
{
    float vectX = GetObjectZ(ptr), vectY = GetObjectZ(ptr + 1);
    int owner = GetOwner(ptr), i;

    if (CurrentHealth(owner))
    {
        int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)) + 1;
        Raise(unit - 1, ToFloat(owner));
        for (i = 0 ; i < 13 ; i ++)
        {
            Frozen(CreateObjectAt("ShopkeeperConjurerRealm", GetObjectX(ptr), GetObjectY(ptr)), 1);
            ObjectOff(unit + i);
            SetOwner(unit - 1, unit + i);
            DeleteObjectTimer(unit + i, 1);
            SetCallback(unit + i, 9, ThunderBoltCollideHandler);
            MoveObject(ptr, GetObjectX(ptr) + vectX, GetObjectY(ptr) + vectY);
            if (!IsVisibleTo(ptr, ptr + 1))
                break;
        }
        DrawYellowLightningFx(GetObjectX(ptr + 1), GetObjectY(ptr + 1), GetObjectX(ptr), GetObjectY(ptr), 24);
        DeleteObjectTimer(unit - 1, 3);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void ThunderBolt(int unit)
{
    float vectX = UnitAngleCos(unit, 38.0), vectY = UnitAngleSin(unit, 38.0);
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit) + vectX, GetObjectY(unit) + vectY);

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), vectY);
    Raise(ptr, vectX);
    SetOwner(unit, ptr);
    FrameTimerWithArg(3, ptr, ThunderBoltCritical);
}

int CheckPlayerIndex(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (g_player[i] ^ unit)
            continue;
        else
            return i;
    }
    return -1;
}

int ThunderStormHandler(int unit)
{
    int plr;

    if (CurrentHealth(unit))
    {
        plr = CheckPlayerIndex(unit);
        if (plr >= 0)
        {
            if (g_player[plr + 10] & 8)
            {
                ThunderBolt(unit);
                return 1;
            }
        }
    }
    return 0;
}

static void OnDetectedHarpoon(int cur, int owner)
{
    if (ThunderStormHandler(owner))
        Delete(cur);
}

void SubUnitDetectedNearlyEnemy()
{
    float dist = DistanceUnitToUnit(SELF, OTHER);

    if (dist < GetObjectZ(GetTrigger() + 1))
    {
        Raise(GetTrigger() + 1, dist);
        Raise(GetTrigger() + 2, GetCaller());
    }
}

void CollideMasterSkill()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 60, DAMAGE_TYPE_ZAP_RAY);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.3);
        Effect("LESSER_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void MovingMasterSkill(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    while (1)
    {
        if (CurrentHealth(owner) && IsVisibleTo(ptr, ptr + 1))
        {
            if (count)
            {
                MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr), GetObjectY(ptr) + GetObjectZ(ptr + 1));
                Effect("SENTRY_RAY", GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr) - (GetObjectZ(ptr) * 2.0), GetObjectY(ptr) - (GetObjectZ(ptr + 1) * 2.0));
                int unit = CreateObjectAt("Shopkeeper", GetObjectX(ptr), GetObjectY(ptr));
                ObjectOff(unit);
                SetCallback(unit, 9, CollideMasterSkill);
                Frozen(unit, TRUE);
                SetOwner(owner, unit);
                DeleteObjectTimer(unit, 1);
                LookWithAngle(ptr, count - 1);
                FrameTimerWithArg(1, ptr, MovingMasterSkill);
                break;
            }
        }
        Delete(ptr);
        Delete(ptr + 1);
        break;
    }
}

void MasterSkillAfter(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr + 1)), unit;
    float xVect, yVect;

    if (CurrentHealth(owner))
    {
        if (CurrentHealth(target))
        {
            xVect = UnitRatioX(target, ptr, 18.0);
            yVect = UnitRatioY(target, ptr, 18.0);
        }
        else
        {
            xVect = UnitAngleCos(owner, 18.0);
            yVect = UnitAngleSin(owner, 18.0);
        }
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr) + xVect, GetObjectY(ptr) + yVect);
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), yVect);
        SetOwner(owner, unit);
        LookWithAngle(unit, 20);
        Raise(unit, xVect);
        FrameTimerWithArg(1, unit, MovingMasterSkill);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void MasterSkill(int owner)
{
    int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(owner), GetObjectY(owner));

    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), 5000.0);
    CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
    UnitNoCollide(unit);
    CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit) + UnitAngleCos(unit, 100.0), GetObjectY(unit) + UnitAngleSin(unit, 100.0), 450.0);
    SetOwner(owner, unit);
    SetOwner(owner, unit + 1);
    SetCallback(unit, 3, SubUnitDetectedNearlyEnemy);
    DeleteObjectTimer(unit, 1);
    FrameTimerWithArg(1, unit + 1, MasterSkillAfter);
}

void MasterShotArrow(int cur, int owner)
{
    if (CurrentHealth(owner))
    {
        if (IsMonsterUnit(owner))
        {
            MasterSkill(owner);
            Delete(cur);
        }
    }
}

void FireHandler(int cur, int owner)
{
    if (CurrentHealth(owner))
    {
        if (IsPlayerUnit(owner))
        {
            int unit = CreateObjectAt("OgreShuriken", GetObjectX(cur), GetObjectY(cur));
            Delete(cur);
            SetOwner(owner, unit);
            PushObjectTo(unit, UnitRatioX(unit, owner, 7.0), UnitRatioY(unit, owner, 7.0));
        }
    }
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 526, OnDetectedHarpoon);
    HashPushback(pInstance, 527, MasterShotArrow);
    HashPushback(pInstance, 192, FireHandler);
    HashPushback(pInstance, 193, FireHandler);
    HashPushback(pInstance, 194, FireHandler);
}

void EnableCreatureSight()
{
    EnchantOff(SELF, "ENCHANT_BLINDED");
}

int SummonNecromancer(int wp)
{
    int unit = CreateObject("Necromancer", wp);
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    SetUnitMaxHealth(unit, 325);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(450.0));
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_LIGHTNING"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
		SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x8000000);
    }
    SetUnitVoice(unit,MONSTER_VOICE__Maiden2);
    DungeonMonsterCommon(unit);
    return unit;
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(g_player[i]))
            return i;
    }
    return -1;
}

int VaildPlayerCheck(int plrUnit)
{
    int plrArr[32], pIndex = GetPlayerIndex(plrUnit);

    if (pIndex >= 0)
    {
        if (plrUnit ^ plrArr[pIndex])
        {
            plrArr[pIndex] = plrUnit;
            return 1;
        }
    }
    return 0;
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    if (pUnit)
        ShowQuestIntroOne(pUnit, 9999, "WarriorChapterBegin9", "Noxworld.wnd:Ready");
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

int PlayerHandlerInit(int plr, int pUnit)
{
    int res = plr;

    if (!MaxHealth(g_player[plr]))
    {
        g_player[plr] = pUnit;
        g_player[plr + 10] = 1;
        if (VaildPlayerCheck(pUnit))
        {
            //
            // if (pUnit ^ GetHost())
            //     NetworkUtilClientEntry(pUnit);
            // else
            //     InitializeResources();
            res = plr | (1 << 0x8);
        }
        SelfDamageClassEntry(pUnit);
        DiePlayerHandlerEntry(pUnit);
        ChangeGold(pUnit, -GetGold(pUnit));
        UniPrint(g_player[plr], "환영인사:: 맥도날드에 오신것을 환영합니다");
        UniPrint(g_player[plr], "I'm lovin'it McDonald's");
    }
    return res;
}

void PlayerHandlerKeepout()
{
    MoveObject(OTHER, LocationX(12), LocationY(12));
    UniPrint(OTHER, "맵에 들어오지 못했습니다");
    UniPrint(OTHER, "이 맵이 수용가능한 인원 10을 넘었거나 전사만 참가 가능합니다");
}

void PlayerHandlerEntry(int plr)
{
    if (PlayerCheckDeathFlag(plr))
        PlayerSetDeathFlag(plr);
    MoveObject(g_player[plr], LocationX(11), LocationY(11));
    DeleteObjectTimer(CreateObject("BlueRain", 11), 9);
    AudioEvent("BlindOff", 11);
}

void PlayerFastJoin()
{
    int plr;

    if (CurrentHealth(OTHER))
    {
        plr = CheckPlayer();
        if (plr >= 0)
            PlayerHandlerEntry(plr);
    }
}

void PlayerFirstJoin(int pUnit)
{
    MoveObject(pUnit, LocationX(112), LocationY(112));
}

void PlayerHandlerJoin()
{
    int plr, i;

    while (1)
    {
        if (CurrentHealth(OTHER) && MaxHealth(OTHER) == 150)
        {
            plr = CheckPlayer();
            for (i = 9; i >= 0 && plr < 0 ; i --)
            {
                if (!MaxHealth(g_player[i]))
                {
                    plr = PlayerHandlerInit(i, GetCaller());
                    break;
                }
            }
            if (plr + 1)
            {
                if (plr >> 0x08)
                    PlayerFirstJoin(OTHER);
                else
                    PlayerHandlerEntry(plr);
                break;
            }
        }
        PlayerHandlerKeepout();
        break;
    }
}

void PlayerHandlerFree(int plr)
{
    EmptyArray(plr);
    g_player[plr] = 0;
    g_player[plr + 10] = 0;
}

void LearnNewSkill(int unit)
{
    float x=GetObjectX(unit), y=GetObjectY(unit);

    Effect("WHITE_FLASH", x,y, 0.0, 0.0);
    Effect("YELLOW_SPARKS", x,y, 0.0, 0.0);
    PlaySoundAround(SOUND_AwardSpell, unit);
}

void RayTarget()
{
    Effect("GREATER_HEAL", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
    RestoreHealth(GetOwner(SELF), 4);
}

void AutoTargetDeathray(int plr)
{
    EnchantOff(g_player[plr], "ENCHANT_INFRAVISION");
    if (g_player[plr + 10] & PLAYER_FLAG_AUTODEATHRAY)
    {
        int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(g_player[plr]), GetObjectY(g_player[plr]));

        SetUnitScanRange(unit, 600.0);
        UnitNoCollide(unit);
        LookWithAngle(unit, GetDirection(g_player[plr]));
        SetOwner(g_player[plr], unit);
        SetCallback(unit, 3, RayTarget);
        DeleteObjectTimer(unit, 1);
        PlaySoundAround(unit, SOUND_WallCast);
        PlaySoundAround(unit, SOUND_GlyphCast);
    }
}

void GiveWindBooster(int plr)
{
    if (g_player[plr + 10] & PLAYER_FLAG_WINDBOOSTER)
        UniPrint(g_player[plr], "이미 윈드부스터 기술을 습득하셨습니다");
    else
    {
        LearnNewSkill(g_player[plr]);
        g_player[plr + 10] ^= PLAYER_FLAG_WINDBOOSTER;
        UniPrint(g_player[plr], "윈드 부스터 기술을 습득했습니다, 사용은 '조심스럽게 걷기' 시전 입니다");
    }
}

void GiveAutoTarget(int plr)
{
    if (g_player[plr + 10] & PLAYER_FLAG_AUTODEATHRAY)
        UniPrint(g_player[plr], "이미 자동타게팅 데스레이 기술을 습득하셨습니다");
    else
    {
        LearnNewSkill(g_player[plr]);
        g_player[plr + 10] ^= PLAYER_FLAG_AUTODEATHRAY;
        UniPrint(g_player[plr], "자동 타게팅 데스레이 기술을 습득했습니다, 사용은 '늑데의 눈' 시전 입니다");
    }
}

void GiveThunderBolt(int plr)
{
    if (g_player[plr + 10] & PLAYER_FLAG_ELECTRIC_LIGHTNING)
        UniPrint(g_player[plr], "이미 백만볼트 기술을 습득하셨습니다");
    else
    {
        LearnNewSkill(g_player[plr]);
        g_player[plr + 10] ^= PLAYER_FLAG_ELECTRIC_LIGHTNING;
        UniPrint(g_player[plr], "백만볼트 기술을 습득했습니다, 기존 '작살' 기술이 바뀌므로 참고바랍니다");
    }
}

void PlayerInventoryHandler(int plr, int unit)
{
    int inv = GetLastItem(unit), ptr, type;

    if (IsObjectOn(inv))
    {
        if (GetUnitThingID(inv) == 2676)
        {
            ptr = UnitToPtr(inv);
            if (ptr)
            {
                type = GetMemory(GetMemory(ptr + 0x2e0)) & 0xff;
                if (type == 5)      //TODO: ABILITY_EYE_OF_THE_WOLF
                    GiveAutoTarget(plr);
                else if (type == 3) //TODO: ABILITY_HARPOON
                    GiveThunderBolt(plr);
                else if (type == 4) //TODO: ABILITY_TREAD_LIGHTLY
                    GiveWindBooster(plr);
            }
            Delete(inv);
        }
    }
}

void WindBooster(int unit)
{
    EnchantOff(unit, "ENCHANT_SNEAK");
    RemoveTreadLightly(unit);
    Effect("RICOCHET", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    PushObjectTo(unit, UnitAngleCos(unit, 90.0), UnitAngleSin(unit, 90.0));
    Enchant(unit, "ENCHANT_RUN", 0.2);
}

void TeleportSafeZone(int unit)
{
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    MoveObject(unit, LocationX(94), LocationY(94));
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    PlaySoundAround(unit, 6);
    UniPrint(unit, "블링크 물약을 사용하여 안전한 곳으로 귀환했습니다");
}

void UseTeleportItem()
{
    if (CurrentHealth(OTHER))
    {
        Delete(SELF);
        TeleportSafeZone(OTHER);
    }
}

void UseBlackBook()
{
    Delete(SELF);
    
    CastSpellObjectLocation("SPELL_METEOR_SHOWER", OTHER, GetObjectX(OTHER), GetObjectY(OTHER));
}

void UseShockBuffPotion()
{
    Enchant(OTHER, EnchantList(22), 70.0);
    CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
    CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
    CastSpellObjectObject("SPELL_CURE_POISON", OTHER, OTHER);
    RestoreHealth(OTHER, 100);
    Delete(SELF);
}

void PlayerHandlerProcess(int plr, int unit)
{
    int arr[10];

    if (HasEnchant(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY"))
    {
        EnchantOff(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY");
        Enchant(unit, "ENCHANT_SHOCK", 0.0);
    }
    if (HasEnchant(unit, "ENCHANT_SNEAK") && g_player[plr + 10] & 2)
        WindBooster(unit);
    if (HasEnchant(unit, "ENCHANT_INFRAVISION"))
        AutoTargetDeathray(plr);
    if (CheckPlayerInput(unit) == 48)
        BringArray(plr);
    if (IsPoisonedUnit(unit))
    {
        if (arr[plr] < 30)
            arr[plr] += IsPoisonedUnit(unit);
        else
        {
            Damage(unit, 0, IsPoisonedUnit(unit), 5);
            arr[plr] = 0;
        }
    }
    else if (arr[plr])
        arr[plr] = 0;
    PlayerInventoryHandler(plr, unit);
}

void PlayerSetDeathFlag(int plr)
{
    g_player[plr + 10] ^= PLAYER_FLAG_DEATH_FLAG;
}

int PlayerCheckDeathFlag(int plr)
{
    return g_player[plr + 10] & PLAYER_FLAG_DEATH_FLAG;
}

void PlayerOnDeath(int plrUnit)
{
    char msg[192];

    NoxSprintfOne(msg, "방금 %s 님께서 적에게 격추되었습니다", StringUtilGetScriptStringPtr(PlayerIngameNick(plrUnit)));
    UniPrintToAll(ReadStringAddressEx(msg));
}

void PlayerHandlerMainLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (1)
        {
            if (MaxHealth(g_player[i]))
            {
                if (GetUnitFlags(g_player[i]) & 0x40)
                    1;
                else if (CurrentHealth(g_player[i]))
                {
                    PlayerHandlerProcess(i, g_player[i]);
                    break;
                }
                else
                {
                    if (!PlayerCheckDeathFlag(i))
                    {
                        PlayerSetDeathFlag(i);
                        PlayerOnDeath(g_player[i]);
                    }
                    break;
                }
            }
            if (g_player[i + 10])
                PlayerHandlerFree(i);
            break;
        }
    }
    FrameTimer(1, PlayerHandlerMainLoop);
}

int DummyUnitCreate(string name, int wp)
{
    int unit = CreateObject(name, wp);

    if (CurrentHealth(unit))
    {
        ObjectOff(unit);
        Damage(unit, 0, MaxHealth(unit) + 1, -1);
        Frozen(unit, 1);
    }
    return unit;
}

int FindoutDungeonKey(int unit)
{
    int inv = GetLastItem(unit);

    while (IsObjectOn(inv))
    {
        if (GetUnitThingID(inv) ^ 2182) //RedOrbKey
            inv = GetPreviousItem(inv);
        else
            return inv;
    }
    return 0;
}

void TeleportProgress(int ptr)
{
    int owner = GetOwner(ptr), dest = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                if (DistanceUnitToUnit(ptr, owner) < 23.0)
                {
                    LookWithAngle(ptr, count - 1);
                    FrameTimerWithArg(1, ptr, TeleportProgress);
                    break;
                }
            }
            else
            {
                Effect("TELEPORT", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
                Effect("SMOKE_BLAST", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
                MoveObject(owner, GetObjectX(dest), GetObjectY(dest));
                PlaySoundAround(owner, SOUND_BlindOff);
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            }
            EnchantOff(owner, "ENCHANT_BURNING");
        }
        Delete(ptr);
        Delete(ptr + 1);
        break;
    }
}

void TeleportPortal()
{
    if (CurrentHealth(OTHER) && IsObjectOn(OTHER))
    {
        if (!UnitCheckEnchant(OTHER, GetLShift(ENCHANT_BURNING)))
        {
            Enchant(OTHER, "ENCHANT_BURNING", 4.0);
            int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));
            Raise(unit, GetTrigger() + 1);
            LookWithAngle(unit, 48); //TODO: 1.XX seconds...
            CreateObjectAt("VortexSource", GetObjectX(unit), GetObjectY(unit));
            Effect("YELLOW_SPARKS", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
            SetOwner(OTHER, unit);
            TeleportProgress(unit);
            PlaySoundAround(unit, SOUND_LongBellsUp);
            UniPrint(OTHER, "공간이동을 준비 중 입니다. 취소하려면 움직이세요");
        }
    }
}

int TeleportSetup(int srcWp, int dstWp)
{
    int unit = CreateObject("WeirdlingBeast", srcWp);

    SetUnitMaxHealth(CreateObject("InvisibleLightBlueHigh", dstWp) - 1, 10);
    Enchant(CreateObject("InvisibleLightBlueHigh", srcWp), "ENCHANT_ANCHORED", 0.0);
    int fx= CreateObjectById(OBJ_CORPSE_SKULL_SW, LocationX(srcWp),LocationY(srcWp));
    Frozen(fx,TRUE);
    UnitNoCollide( fx);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, TeleportPortal);

    return unit;
}

void ObjectPusher(int owner, int target, float gap)
{
    int unit = CreateObjectAt("BoulderCave", GetObjectX(owner) + UnitRatioX(owner, target, gap), GetObjectY(owner) + UnitRatioY(owner, target, gap));

    Frozen(unit, 1);
    DeleteObjectTimer(unit, 1);
    DeleteObjectTimer(CreateObjectAt("MagicEnergy", GetObjectX(target), GetObjectY(target)), 9);
}

void CheckSpecialItem(int item)
{
    int id = GetUnitThingID(item);

    if (id >= OBJ_OBLIVION_HALBERD && id <= OBJ_OBLIVION_ORB)
    {
        SetItemPropertyAllowAllDrop(item);
        DisableOblivionItemPickupEvent(item);
    }
    else if (id == OBJ_FAN_CHAKRAM||id==OBJ_QUIVER)
        SetConsumablesWeaponCapacity(item,255,255);
}

void SpecialClassTeleportAmulet(int item)
{
    SetUnitCallbackOnUseItem(item,UseTeleportItem);
}
void onCiderUse(){
    if (CurrentHealth(OTHER)){
        RestoreHealth(OTHER,130);
        Enchant(OTHER,EnchantList(ENCHANT_CONFUSED),2.0);
        PlaySoundAround(OTHER,SOUND_PotionUse);
        Delete(SELF);
    }
}
void specialItemCider(int item){
    SetUnitCallbackOnUseItem(item,onCiderUse);
}

void SpecialClassBlackBook(int item)
{
    SetUnitCallbackOnUseItem(item,UseBlackBook);
}

void SpecialClassShockBuff(int item)
{
    SetUnitCallbackOnUseItem(item,UseShockBuffPotion);
}

static void initFieldItemFunctions()
{
    int itemFn[]={
        HotPotion, PotionItemDrop, NormalWeaponItemDrop, NormalArmorItemDrop,
        MoneyDrop, SomeGermDrop,WeaponItemDrop, ArmorItemDrop,SpecialItemDrop
    };
    g_pItemFn = &itemFn;
    g_itemFnCount = sizeof(itemFn);
    initPotionList();
    initArmorList();
    initWeaponList();
}

int HotPotion(int wp)
{
    return CreateObjectAt("RedPotion", LocationX(wp), LocationY(wp));
}

int PotionItemDrop(int wp)
{
    return CheckPotionThingID(CreateObjectAt(g_pListPotions[ Random(0, 12) ], LocationX(wp), LocationY(wp)));
}

int NormalWeaponItemDrop(int wp)
{
    int unit = CreateObjectAt(g_pListWeapons[ Random(0, 7) ], LocationX(wp), LocationY(wp));

    CheckSpecialItem(unit);
    return unit;
}

int NormalArmorItemDrop(int wp)
{
    return CreateObjectAt(g_pListArmors[ Random(0, 17)], LocationX(wp), LocationY(wp));
}

int MoneyDrop(int wp)
{
    int money = CreateObjectAt("QuestGoldChest", LocationX(wp), LocationY(wp));

    UnitStructSetGoldAmount(money, Random(1000, 8000));
    return money;
}

int SomeGermDrop(int wp)
{
    string name[] = {"Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Diamond"};

    return CreateObjectAt(name[ Random(0, 5)], LocationX(wp), LocationY(wp));
}

int WeaponItemDrop(int wp)
{
    int unit = CreateObjectAt(g_pListWeapons[ Random(0, 12) ], LocationX(wp), LocationY(wp));

    SetWeaponProperties(unit, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    CheckSpecialItem(unit);
    return unit;
}

int ArmorItemDrop(int wp)
{
    int unit = CreateObjectAt(g_pListArmors[ Random(0, 17) ], LocationX(wp), LocationY(wp));

    SetArmorProperties(unit, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return unit;
}

int SpecialItemDrop(int location)
{
    int pic = Random(0, 3);
    int item = CreateObjectAt(SpecialItemList(pic), LocationX(location), LocationY(location));
    int spfn[]={
        SpecialClassBlackBook,
        SpecialClassShockBuff,
        SpecialClassTeleportAmulet,
        specialItemCider,
        };

    CallFunctionWithArg(spfn[pic], item);
    return item;
}

string SpecialItemList(int num)
{
    string special[] = {
        "AmuletofManipulation", "AmuletofNature", "BlackBook1", "Cider",
    };
    return special[num];
}

static void initPotionList()
{
    string name[] = {
        "RedPotion", "CurePoisonPotion", "YellowPotion", "BlackPotion",
        "VampirismPotion", "Mushroom", "PoisonProtectPotion", "ShockProtectPotion",
        "FireProtectPotion", "HastePotion", "ShieldPotion", "InvulnerabilityPotion",
        "RedPotion"
    };
    g_pListPotions=&name;
}

static void initWeaponList()
{
    string name[] = {
        "GreatSword", "Longsword", "Sword", "MorningStar",
        "OgreAxe", "StaffWooden", "BattleAxe", "FanChakram",
        "RoundChakram", "WarHammer", "OblivionHalberd", "OblivionWierdling",
        "OblivionHeart"
    };
    g_pListWeapons=&name;
}

static void initArmorList()
{
    string name[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots",
        "PlateLeggings", "MedievalCloak", "ChainCoif", "ChainLeggings",
        "ChainTunic", "SteelHelm", "LeatherArmbands", "LeatherArmor",
        "LeatherArmoredBoots", "LeatherBoots", "LeatherHelm", "LeatherLeggings",
        "MedievalPants", "MedievalShirt"
    };
    g_pListArmors=&name;
}

void ObjectPusherWithDirection(int owner, float gap)
{
    int unit = CreateObjectAt("RedOrb", GetObjectX(owner) - UnitAngleCos(owner, gap), GetObjectY(owner) - UnitAngleSin(owner, gap));

    Frozen(unit, 1);
    DeleteObjectTimer(unit, 1);
}

void AbsoluteTargetStrike(int owner, int target, float threshold, int func)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(target), GetObjectY(target));

    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner)), threshold);

    SetOwner(owner, unit);
    Raise(unit, ToFloat(target));
    FrameTimerWithArg(1, unit, func);
}

void DryadThrowMissile(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));
    float dt = DistanceUnitToUnit(ptr, target);
    float vectX = UnitRatioX(target, ptr, dt), vectY = UnitRatioY(target, ptr, dt);
    float thresHold;

    if (CurrentHealth(owner) && CurrentHealth(target) && IsObjectOn(owner))
    {
        int mis = CreateObjectAt("SpiderSpit",
            GetObjectX(owner) + UnitRatioX(target, owner, 17.0), GetObjectY(owner) + UnitRatioY(target, owner, 17.0));
        UserDamageArrowCreate(owner, GetObjectX(mis), GetObjectY(mis), 20);
        Enchant(mis, "ENCHANT_SHIELD", 0.0);
        SetOwner(owner, mis);
        SetOwner(owner, mis + 1);
        thresHold = DistanceUnitToUnit(mis, target) / GetObjectZ(ptr + 1);
        MoveObject(ptr, GetObjectX(target) + UnitRatioX(target, ptr, dt * thresHold), GetObjectY(target) + UnitRatioY(target, ptr, dt * thresHold));
        if (IsVisibleTo(ptr, owner))
        {
			LookAtObject(mis, ptr);
            PushObject(mis, -40.0, GetObjectX(ptr), GetObjectY(ptr));
            PushObject(mis + 1, -40.0, GetObjectX(ptr), GetObjectY(ptr));
        }
        else
        {
			LookAtObject(mis, target);
            PushObject(mis, -40.0, GetObjectX(target), GetObjectY(target));
            PushObject(mis + 1, -40.0, GetObjectX(target), GetObjectY(target));
        }
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void DryadSightHandler()
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));
	
	Raise(unit, DryadThrowMissile);
	AbsoluteTargetStrike(GetTrigger(), GetCaller(), DistanceUnitToUnit(OTHER, SELF) / 5.0, ToInt(GetObjectZ(unit)));
	Delete(unit);
	AggressionLevel(SELF, 1.0);
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

int FlyTreasure(int wp)
{
    int angle = Random(0, 359);
    float fRnd = RandomFloat(10.0, 100.0);
    int unit = CreateObjectAt("RewardMarker", GetWaypointX(wp) + MathSine(angle + 90, fRnd), GetWaypointY(wp) + MathSine(angle, fRnd));

    UniChatMessage(unit, "맥도날드 빅맥세트", 150);
    Raise(unit, 250.0);
    return unit;
}

void FDropTreasure(int ptr)
{
    int count = GetDirection(ptr);

    if (count)
    {
        FlyTreasure(84);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, FDropTreasure);
    }
    else
        Delete(ptr);
}

void RespawnMC(int ptr)
{
    int boss = Object("MCSniper");

    if (CurrentHealth(boss))
    {
        float x= GetObjectX(ptr), y= GetObjectY(ptr);

        PlaySoundAround(ptr, SOUND_HecubahDieFrame194);
        Effect("SMOKE_BLAST", x,y, 0.0, 0.0);
        Effect("TELEPORT", x,y, 0.0, 0.0);
        MoveObject(boss, x,y);
        AttachedCounterUnit(boss);
        ObjectOn(boss);
        AttachHealthbar(boss);
        UniChatMessage(boss, "맥도날드 사장:\n아 왜이리 소란이야~ 한참 좋았었는데~ 로날드가 죽었다고? 그래, 넌 오늘이 마지막날이 될 것이다!", 180);
    }
    Delete(ptr);
}

void JandorDie()
{
    float x=GetObjectX(SELF),y= GetObjectY(SELF);

    DeleteObjectTimer(CreateObjectAt("LevelUp", x,y), 600);
    UniChatMessage(SELF, "맥도날드 마스코트 로날드:\n란란루~ 제법이군... 내가 죽는걸로 끝날줄 알았나? 크크큭...", 180);
    FrameTimerWithArg(60, CreateObjectAt("InvisibleLightBlueHigh", x,y), RespawnMC);
    DeleteObjectTimer(SELF, 60);
    UniPrintToAll("방금 로널드가 격추되었습니다");
}

void JandorSayHealth()
{
    char buff[64];

    if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_ETHEREAL )))
    {
        int hp = CurrentHealth(SELF);

        NoxSprintfString(&buff, "맥도날드 주인공 '로날드'\n남은체력: %d", &hp, 1);
        UniChatMessage(SELF, ReadStringAddressEx(&buff), 135);
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.0);
        if (CurrentHealth(OTHER))
        {
            if (IsAttackedBy(SELF, OTHER))
            {
                WispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
                Damage(OTHER, SELF, 15, 14);
                PlaySoundAround(OTHER, SOUND_PlasmaSustain);
            }
        }
    }
}

void JandorSightHandler()
{
    if (UnitCheckEnchant(SELF,GetLShift(ENCHANT_ETHEREAL )))
        return;
    if (DistanceUnitToUnit(SELF, OTHER) < 80.0)
    {
        PlaySoundAround(SELF, SOUND_WillOWispEngage);
        GreenLightningFx(
            FloatToInt(GetObjectX(SELF)), FloatToInt(GetObjectY(SELF)), FloatToInt(GetObjectX(OTHER)), FloatToInt(GetObjectY(OTHER)), 15);
        Damage(OTHER, 0, 45, 16);
    }
    else
        ObjectPusherWithDirection(SELF, 1.0);
    Enchant(SELF, "ENCHANT_BLINDED", 0.0);
    AggressionLevel(SELF, 1.0);
    Enchant(SELF,EnchantList( ENCHANT_ETHEREAL ),0.05);
}

int GetHuman(int unit)
{
	int owner = unit;

	while (CurrentHealth(owner))
	{
        if (IsPlayerUnit(owner) || IsMonsterUnit(owner))
			return owner;
		else
			owner = GetOwner(owner);
	}
	return 0;
}

void GirlHurt()
{
    if (CurrentHealth(SELF) ^ MaxHealth(SELF))
    {
		int risk = GetHuman(OTHER);
        if (CurrentHealth(risk))
        {
            DeleteObjectTimer(CreateObjectAt("MagicEnergy", GetObjectX(SELF), GetObjectY(SELF)), 9);
            PlaySoundAround(SELF, SOUND_PushCast);
            Effect("DAMAGE_POOF", GetObjectX(risk), GetObjectY(risk), 0.0, 0.0);
            Damage(risk, SELF, MaxHealth(SELF) - CurrentHealth(SELF), 11);
            PushObjectTo(risk, UnitRatioX(risk, SELF, 60.0), UnitRatioY(risk, SELF, 60.0));
        }
        if (CurrentHealth(GetOwner(SELF)))
        {
            Effect("GREATER_HEAL", GetObjectX(SELF), GetObjectY(SELF) - 100.0, GetObjectX(SELF), GetObjectY(SELF));
            RestoreHealth(GetOwner(SELF), (MaxHealth(SELF) - CurrentHealth(SELF)) / 2);
        }
        RestoreHealth(SELF, MaxHealth(SELF) - CurrentHealth(SELF));
    }
}

void AttachedCounter(int ptr)
{
	int owner = GetOwner(ptr), woman = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);
	
	if (CurrentHealth(owner) && CurrentHealth(woman))
	{
		if (count)
			LookWithAngle(ptr, count - 1);
		else
		{
			LookWithAngle(ptr, 10);
            PlaySoundAround(ptr, SOUND_MaidenHurt);
		}
		MoveObject(ptr, GetObjectX(owner), GetObjectY(owner));
		MoveObject(woman, GetObjectX(ptr) + UnitAngleCos(owner, 26.0), GetObjectY(ptr) + UnitAngleSin(owner, 26.0));
		LookAtObject(woman, owner);
		FrameTimerWithArg(1, ptr, AttachedCounter);
	}
	else
	{
		Delete(ptr);
        if (CurrentHealth(woman))
            Delete(woman);
	}
}

void MCMasterDie()
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));

    DeleteObjectTimer(CreateObjectAt("LevelUp", GetObjectX(unit), GetObjectY(unit)), 600);
    // DrawTextOnBottom(1,3,GetObjectX(unit) - 30.0, GetObjectY(unit));
    LookWithAngle(unit, 50);
    UniChatMessage(SELF, "맥도날드 사장:\n 지분을 너에게 넘겨줄 수 없는데!!!", 210);
    DeleteObjectTimer(SELF, 60);
    FrameTimerWithArg(1, unit, FDropTreasure);
    UniPrintToAll("승리! 맥도날드 사장이 죽었습니다");
}

void AttachedCounterUnit(int unit)
{
	int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));

	SetOwner(unit, ptr);
	Raise(ptr, ColorMaiden(255, 0, 0, 1));
	SetOwner(unit, ptr + 1);
    SetCallback(unit, 5, MCMasterDie);
    // SetCallback(unit, 7, MCMasterHurt);
    SetCallback(ptr + 1, 7, GirlHurt);
	SetUnitMaxHealth(ptr + 1, 1000);
	SetUnitFlags(ptr + 1, GetUnitFlags(ptr + 1) ^ 0x10);
	SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x4000);
	AggressionLevel(ptr + 1, 0.0);
	Enchant(ptr + 1, "ENCHANT_BLINDED", 0.0);
	FrameTimerWithArg(1, ptr, AttachedCounter);
}

void MagicMissileCollide()
{
    int owner = GetOwner(SELF);

    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, 0, 10, DAMAGE_TYPE_FLAME);
            Effect("SPARK_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

int RedWizShotMissile(int caster, int target)
{
    int mis = CreateObjectAt("MagicMissile", 
        GetObjectX(caster) + UnitRatioX(target, caster, 17.0), GetObjectY(caster) + UnitRatioY(target, caster, 17.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetOwner(caster, mis);
    SetMemory(ptr + 0x2b8, ImportUnitCollideFunc());
    SetMemory(ptr + 0x2fc, MagicMissileCollide);
    return mis;
}

void RedWizCastSingleMissile(int sUnit)
{
    int owner = GetOwner(sUnit), target = ToInt(GetObjectZ(sUnit));
    float dt = Distance(GetObjectX(sUnit), GetObjectY(sUnit), GetObjectX(target), GetObjectY(target));
    float vectX = UnitRatioX(target, sUnit, dt), vectY = UnitRatioY(target, sUnit, dt);

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        int mis = RedWizShotMissile(owner, target);
        float thresHold = DistanceUnitToUnit(mis, target) / GetObjectZ(sUnit + 1);
        MoveObject(sUnit, GetObjectX(target) + UnitRatioX(target, sUnit, dt * thresHold), GetObjectY(target) + UnitRatioY(target, sUnit, dt * thresHold));
        if (IsVisibleTo(sUnit, owner))
            PushObject(mis, -33.0, GetObjectX(sUnit), GetObjectY(sUnit));
        else
            PushObject(mis, -33.0, GetObjectX(target), GetObjectY(target));
    }
    Delete(sUnit);
    Delete(sUnit + 1);
}

void SpitMiniSpider(int ptr)
{
    int count = GetDirection(ptr);

    if (count)
    {
        float x=GetObjectX(ptr), y=GetObjectY(ptr);
        PlaySoundAround(ptr, SOUND_PoisonTrapTriggered);
        DeleteObjectTimer(CreateObjectAt("GreenSmoke", x,y), 9);
        CreateObjectAt("SmallSpider", x,y);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, SpitMiniSpider);
    }
    else
        Delete(ptr);
}

void SpiderDeadHandler()
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));

    LookWithAngle(unit, 10);
    FrameTimerWithArg(1, unit, SpitMiniSpider);
    DeleteObjectTimer(CreateObjectAt("WaterBarrelBreaking", GetObjectX(unit), GetObjectY(unit)), 24);
    PlaySoundAround(unit, SOUND_BeholderDie);
}

void FireWizSightHandler()
{
    if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_CROWN)) )
    {
        AbsoluteTargetStrike(GetTrigger(), GetCaller(), 36.0, RedWizCastSingleMissile);
        Enchant(SELF, "ENCHANT_CROWN", 0.7);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void ZombieWasDead()
{
    DungeonMobCommonDeadHandler();
    Effect("SPARK_EXPLOSION", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
    AudioEvent("BurnCast", 1);
    DeleteObjectTimer(CreateObject("MediumFlame", 1), 90);
}

void DungeonMobCommonDeadHandler()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    CallFunctionWithArgInt(g_pItemFn[ Random(0, g_itemFnCount-1)], 1);
    DeleteObjectTimer(SELF, 60);
}

void DungeonMobCommonHurtHandler()
{
    if (IsPoisonedUnit(SELF))
    {
        Damage(SELF, OTHER, IsPoisonedUnit(SELF), 5);
    }
}

void DungeonMonsterCommon(int unit)
{
    SetOwner(MasterUnit(), unit);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    SetCallback(unit, 5, DungeonMobCommonDeadHandler);
    SetCallback(unit, 7, DungeonMobCommonHurtHandler);
}

int DungeonSwordsman(int wp)
{
    int unit = CreateObject("Swordsman", wp);

    SetUnitMaxHealth(unit, 325);
    DungeonMonsterCommon(unit);

    return unit;
}

int DungeonArcher(int wp)
{
    int unit = CreateObject("Archer", wp);

    SetUnitMaxHealth(unit, 98);
    DungeonMonsterCommon(unit);

    return unit;
}

int DungeonWolf(int wp)
{
    int unit = CreateObject("WhiteWolf", wp);

    SetUnitMaxHealth(unit, 175);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonEmberDemon(int wp)
{
    string name[] = {"EmberDemon", "MeleeDemon", "MeleeDemon"};
    int unit = CreateObject(name[ Random(0, 2)], wp);

    SetUnitMaxHealth(unit, 175);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonFireFairy(int wp)
{
    int unit = CreateObject("FireSprite", wp);

    UnitLinkBinScript(unit, FireSpriteBinTable());
    SetUnitMaxHealth(unit, 96);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonFireWizard(int wp)
{
    int unit = CreateObject("WizardRed", wp);
    UnitLinkBinScript(unit, WizardRedBinTable());
    UnitZeroFleeRange(unit);
    SetUnitMaxHealth(unit, 225);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000); //TODO: disable casting a magic and always running
    SetCallback(unit, 3, FireWizSightHandler);
    SetCallback(unit, 13, EnableCreatureSight);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonImp(int wp)
{
    int unit = CreateObject("Imp", wp);
    SetUnitMaxHealth(unit, 64);
    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonSkeleton(int wp)
{
    int unit = CreateObject("Skeleton", wp);
    SetUnitMaxHealth(unit, 225);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonSkeletonLord(int wp)
{
    int unit = CreateObject("SkeletonLord", wp);
    SetUnitMaxHealth(unit, 295);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonGargoyle(int wp)
{
    int unit = CreateObject("EvilCherub", wp);
    SetUnitMaxHealth(unit, 96);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonBear(int wp)
{
    int unit = CreateObject("Bear", wp);
    SetUnitMaxHealth(unit, 350);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonBlackBear(int wp)
{
    int unit = CreateObject("BlackBear", wp);
    SetUnitMaxHealth(unit, 295);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonHecubah(int wp)
{
    int unit = CreateObject("HecubahWithOrb", wp);

    UnitLinkBinScript(unit, HecubahWithOrbBinTable());
    UnitZeroFleeRange(unit);
    SetUnitMaxHealth(unit, 275);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonBeggar(int wp)
{
    int unit = CreateObject("Lich", wp);
    UnitLinkBinScript(unit, LichLordBinTable());
    UnitZeroFleeRange(unit);
    SetUnitMaxHealth(unit, 325);
    DungeonMonsterCommon(unit);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    return unit;
}

int DungeonMecaGolem(int wp)
{
    int unit = CreateObject("MechanicalGolem", wp);
    SetUnitMaxHealth(unit, 700);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonOgreaxe(int wp)
{
    int unit = CreateObject("GruntAxe", wp);
    SetUnitMaxHealth(unit, 225);
    DungeonMonsterCommon(unit);
    return unit;
}
int DungeonOgre(int wp)
{
    int unit = CreateObject("OgreBrute", wp);
    SetUnitMaxHealth(unit, 295);
    DungeonMonsterCommon(unit);
    return unit;
}
int DungeonOgreLord(int wp)
{
    int unit = CreateObject("OgreWarlord", wp);
    SetUnitMaxHealth(unit, 350);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonBee(int wp)
{
    int unit = CreateObject("Wasp", wp);

    SetUnitMaxHealth(unit, 64);
    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonGoon(int wp)
{
    int unit = CreateObject("Goon", wp);

    UnitLinkBinScript(unit, GoonBinTable());
    SetUnitMaxHealth(unit, 225);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonZombie(int wp)
{
    int unit = CreateObject("VileZombie", wp);

    SetUnitMaxHealth(unit, 325);
    SetUnitSpeed(unit, 3.0);
    DungeonMonsterCommon(unit);
    SetCallback(unit, 5, ZombieWasDead);
    return unit;
}

int DungeonDryad(int wp)
{
    int unit = CreateObject("WizardGreen", wp);

    SetUnitMaxHealth(unit, 275);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020); //TODO: disable cast magic and applies always running.
    SetCallback(unit, 3, DryadSightHandler);
    SetCallback(unit, 13, EnableCreatureSight);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonShadow(int wp)
{
    int unit = CreateObject("Shade", wp);

    SetUnitMaxHealth(unit, 200);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonScorpion(int wp)
{
    int unit = CreateObject("Scorpion", wp);

    SetUnitMaxHealth(unit, 275);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonMaiden(int wp)
{
    int unit = ColorMaiden(250, 16, 225, wp);

    SetUnitSpeed(unit, 1.7);
    SetUnitMaxHealth(unit, 306);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonSpider(int wp)
{
    int unit = CreateObject("BlackWidow", wp);
    UnitLinkBinScript(unit, BlackWidowBinTable());
    SetUnitMaxHealth(unit, 275);
    DungeonMonsterCommon(unit);
    SetCallback(unit, 5, SpiderDeadHandler);
    return unit;
}

int DungeonBeholder(int wp)
{
    int unit = CreateObject("Beholder", wp);

    SetUnitMaxHealth(unit, 325);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonMonsterTroll(int wp)
{
    int unit = CreateObject("Troll", wp);

    SetUnitMaxHealth(unit, 350);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonWindowAooni(int wp)
{
    int unit = CreateObjectById(OBJ_WOUNDED_CONJURER, LocationX(wp), LocationY(wp));
    
    WoundedConjurerSubProcess(unit);
    DungeonMonsterCommon(unit);
    return unit;
}

int DungeonStoneGolem(int wp)
{
    int unit = CreateObject("StoneGolem", wp);

    SetUnitMaxHealth(unit, 600);
    DungeonMonsterCommon(unit);
    return unit;
}

int SummonHecubah(int wp)
{
    int unit = CreateObject("Hecubah", wp);
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    UnitZeroFleeRange(unit);
    DungeonMonsterCommon(unit);
    SetUnitMaxHealth(unit, 1050);
    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        SetMemory(uec + 0x5a8, 0x0f0000);
        SetMemory(uec + 0x5b0, 0x0f0000);
        SetMemory(uec + 0x5c0, 0x0f0000);
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_CHAIN_LIGHTNING"), 0x40000000);
    }
    return unit;
}

int FinalJandorsLoad(int wp)
{
    int unit = CreateObject("AirshipCaptain", wp);

    UnitZeroFleeRange(unit);
    UnitLinkBinScript(unit, AirshipCaptainBinTable());
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    AggressionLevel(unit, 1.0);
    SetUnitScanRange(unit, 450.0);
    SetUnitMaxHealth(unit, 2400);
    SetCallback(unit, 3, JandorSightHandler);
    SetCallback(unit, 5, JandorDie);
    SetCallback(unit, 7, JandorSayHealth);
    SetCallback(unit, 13, EnableCreatureSight);
    SetUnitVoice(unit,MONSTER_VOICE__Maiden2);
    return unit;
}

int DungeonAllign(int dunIdx)
{
    int arr[5];

    if (dunIdx < 0) //Init
    {
        /*
        TODO:
            +0->Z = Function
            +0->D = Max
        */
        arr[0] = CreateObjectAt("InvisibleLightBlueHigh", 1276.0, 3599.0);
        CreateObjectAt("InvisibleLightBlueHigh", 2495.0, 4818.0);
        CreateObjectAt("InvisibleLightBlueHigh", 1575.0, 5738.0);
        arr[1] = CreateObjectAt("InvisibleLightBlueHigh", 2380.0, 2495.0);
        CreateObjectAt("InvisibleLightBlueHigh", 3622.0, 3737.0);
        CreateObjectAt("InvisibleLightBlueHigh", 2633.0, 4726.0);
        arr[2] = CreateObjectAt("InvisibleLightBlueHigh", 3507.0, 1414.0);
        CreateObjectAt("InvisibleLightBlueHigh", 4841.0, 2748.0);
        CreateObjectAt("InvisibleLightBlueHigh", 3852.0, 3737.0);
        arr[3] = CreateObjectAt("InvisibleLightBlueHigh", 4450.0, 80.0);
        CreateObjectAt("InvisibleLightBlueHigh", 5761.0, 1391.0);
        CreateObjectAt("InvisibleLightBlueHigh", 4703.0, 2449.0);
        arr[4] = CreateObjectAt("InvisibleLightBlueHigh", 1920.0, 540.0);
        CreateObjectAt("InvisibleLightBlueHigh", 3001.0, 1621.0);
        CreateObjectAt("InvisibleLightBlueHigh", 1667.0, 2932.0);
        Raise(arr[0], DungeonSwordsman);
        LookWithAngle(arr[0], 3);
        Raise(arr[1], DungeonEmberDemon);
        LookWithAngle(arr[1], 4);
        Raise(arr[2], DungeonSkeleton);
        LookWithAngle(arr[2], 7);
        Raise(arr[3], DungeonOgreaxe);
        LookWithAngle(arr[3], 9);
        Raise(arr[4], DungeonShadow);
        LookWithAngle(arr[4], 8);
        return 0;
    }
    return arr[dunIdx];
}

void BlueKeyRoomEntranceMonsters(int ptr)
{
    int count = GetDirection(ptr);

    if (count)
    {
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        AudioEvent("SummonCast", 1);
        AudioEvent("MonsterGeneratorSpawn", 1);
        Effect("THIN_EXPLOSION", GetWaypointX(1), GetWaypointY(1), 0.0, 0.0);
        GreenSparkAt(GetWaypointX(1), GetWaypointY(1));
        CallFunctionWithArgInt(ToInt(GetObjectZ(ptr)), 1);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(3, ptr, BlueKeyRoomEntranceMonsters);
    }
    else
        Delete(ptr);
}

void RespectLBos(int pUnit)
{
    int unit = FinalJandorsLoad(50);

    if (IsVisibleTo(unit, pUnit))
    {
        LookAtObject(unit, pUnit);
        UniChatMessage(unit, "맥도널드 마스코트 로널드:\n내가 죽어도 너에게 햄버거를 주지 않을거야!!\n아 물론 내가 죽을일은 없겠지만...", 180);
    }
    AttachHealthbar(unit);
}

void OpenForestWalls()
{
    WallOpen(Wall(227, 197));
    WallOpen(Wall(226, 198));
    WallOpen(Wall(227, 199));
    WallOpen(Wall(228, 200));
    WallOpen(Wall(229, 201));
    WallOpen(Wall(230, 202));
    WallOpen(Wall(231, 201));
}

int CheckCardKey(int unit)
{
    int inv = GetLastItem(unit);

    while (IsObjectOn(inv))
    {
        if (GetUnitThingID(inv) ^ 626)
            inv = GetPreviousItem(inv);
        else
            return inv;
    }
    return 0;
}

void OpenInnerBlueKeyRoom()
{
    int i, key = CheckCardKey(OTHER);

    if (key)
    {
        Delete(key);
        ObjectOff(SELF);
        OpenForestWalls();
        for (i = 0 ; i < 6 ; i ++)
            WallOpen(Wall(180 - i, 196 + i));
        FrameTimerWithArg(7, GetCaller(), RespectLBos);
        FrameTimer(30, BlueKeyRoomDecorations);
        UniPrint(OTHER, "카드키를 사용하여 비밀벽을 개방하였습니다, 이 키는 더이상 사용되지 않으므로 삭제됩니다");
    }
    else
        UniPrint(OTHER, "이 스위치를 조작하려면 카드키가 필요합니다");
}

void OpenBlueKeyRoom()
{
    int i, unit = CreateObject("InvisibleLightBlueHigh", 39);
    Raise(unit, DungeonSkeletonLord);
    Raise(CreateObject("InvisibleLightBlueHigh", 40), DungeonMecaGolem);
    Raise(CreateObject("InvisibleLightBlueHigh", 41), SummonNecromancer);
    LookWithAngle(unit, 20);
    LookWithAngle(unit + 1, 10);
    LookWithAngle(unit + 2, 12);
    FrameTimerWithArg(1, unit, BlueKeyRoomEntranceMonsters);
    FrameTimerWithArg(6, unit + 1, BlueKeyRoomEntranceMonsters);
    FrameTimerWithArg(12, unit + 2, BlueKeyRoomEntranceMonsters);
    ObjectOn(Object("BlueKeyZone")); //TODO: ThingID=2180
    for (i = 0 ; i < 4 ; i ++)
        WallOpen(Wall(197 - i, 215 + i));
}

void BeaconCheckBlueKey()
{
    if (IsObjectOn(SELF))
    {
        if (GetUnitThingID(OTHER) ^ 2180)
        {
            if (CurrentHealth(OTHER))
            {
                ObjectPusher(OTHER, SELF, 2.0);
            }
        }
        else
        {
            OpenBlueKeyRoom();
            Delete(OTHER);
            Delete(SELF);
        }
    }
}

int BlueKeyCheckerPlace(int unit)
{
    int ptr = CreateObjectAt("WeirdlingBeast", GetObjectX(unit), GetObjectY(unit));

    SetUnitMaxHealth(ptr, 30);
    Frozen(CreateObjectAt("RoundChakramInMotion", GetObjectX(ptr), GetObjectY(ptr)), TRUE);
    Enchant(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr), GetObjectY(ptr)), "ENCHANT_SHOCK", 0.0);
    Damage(ptr, 0, MaxHealth(ptr) + 1, -1);
    SetCallback(ptr, 9, BeaconCheckBlueKey);
    return ptr;
}

void SpecialUnitDeadHandler()
{
    float x = GetObjectX(SELF), y=GetObjectY(SELF);
    PlaySoundAround(SELF, SOUND_KeyDrop);
    DungeonMobCommonDeadHandler();
    if (g_DungeonClear < g_DungeonCnt - 1)
    {
        CreateObjectAt("RedOrbKeyOfTheLich", x,y);
        g_DungeonClear ++;
    }
    else
    {
        Frozen(CreateObjectAt("BlueOrbKeyOfTheLich", x,y), TRUE);
        DeleteObjectTimer(CreateObjectAt("LevelUp", x,y), 60);
        UniPrintToAll("지금 마지막 방의 열쇠가 드롭되었습니다!");
    }
}

void DungeonMobCreate(int ptr)
{
    int count = ToInt(GetObjectZ(ptr)), idx = GetDirection(ptr), rnd = ToInt(GetObjectZ(ptr + 1));
    int mark = DungeonAllign(idx), unit;

    if (count)
    {
        float point[2];
        ComputeAreaRhombus(point,GetObjectX(mark + 2), GetObjectX(mark + 1), GetObjectY(mark), GetObjectY(mark + 1));
        TeleportLocation(1,point[0],point[1]);
        unit = CallFunctionWithArgInt(ToInt(GetObjectZ(mark)) + Random(0, GetDirection(mark) - 1), 1);
        if (!(count ^ rnd))
            SetCallback(unit, 5, SpecialUnitDeadHandler);
        Raise(ptr, count - 1);
        FrameTimerWithArg(1, ptr, DungeonMobCreate);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void DungeonSetup()
{
    int idx = GetDirection(SELF), unit;

    if (MaxHealth(SELF) && CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
    {
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), Random(1, g_DungeonMobCnt));
        Raise(unit, g_DungeonMobCnt);
        LookWithAngle(unit, idx);
        FrameTimerWithArg(1, unit, DungeonMobCreate);
        Delete(SELF);
    }
}

void OpenDungeonPortal()
{
    int idx = GetDirection(SELF);

    if (CurrentHealth(OTHER) && MaxHealth(SELF) && IsPlayerUnit(OTHER))
    {
        int key = FindoutDungeonKey(OTHER);
        if (IsObjectOn(key))
        {
            ObjectOn(Object("DungeonT" + IntToString(idx + 1)));
            TeleportSetup(13 + idx, 24 + idx);
            Frozen(CreateObject("TraderAppleCrate", idx + 18), 1);
            int unit = CreateObject("WeirdlingBeast", 24 + idx);
            SetUnitMaxHealth(unit, 10);
            Damage(unit, 0, MaxHealth(unit) + 1, -1);
            LookWithAngle(unit, idx);
            SetCallback(unit, 9, DungeonSetup);
            GreenSparkAt(GetObjectX(SELF), GetObjectY(SELF));
            Delete(SELF);
            Delete(key);
        }
        else
            ObjectPusher(OTHER, SELF, 2.0);
    }
}

void InitDungeonExitPortal()
{
    int i;

    for (i = 0 ; i < 5 ; i ++)
        TeleportSetup(29 + i, 34 + i);
}

void PutCoffins(int ptr)
{
    int count = GetDirection(ptr);

    if (count)
    {
        Frozen(CreateObjectAt("Coffin4", GetObjectX(ptr), GetObjectY(ptr)), 1);
        MoveObject(ptr, GetObjectX(ptr) - 92.0, GetObjectY(ptr) + 92.0);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, PutCoffins);
    }
    else
        Delete(ptr);
}

void BlueKeyRoomDecorations()
{
    int unit = CreateObject("RedPotion", 1);

    Delete(unit++);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 42), PutCoffins);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 43), PutCoffins);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 44), PutCoffins);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 45), PutCoffins);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 46), PutCoffins);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 47), PutCoffins);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 48), PutCoffins);
    FrameTimerWithArg(1, CreateObject("InvisibleLightBlueHigh", 49), PutCoffins);
    LookWithAngle(unit, 6);
    LookWithAngle(unit + 1, 6);
    LookWithAngle(unit + 2, 6);
    LookWithAngle(unit + 3, 6);
    LookWithAngle(unit + 4, 6);
    LookWithAngle(unit + 5, 6);
    LookWithAngle(unit + 6, 6);
    LookWithAngle(unit + 7, 6);
}

void InitDungeonPortal()
{
    int unit = CreateObject("RedOrbKeyOfTheLich", 23), i;

    Enchant(unit, "ENCHANT_SHIELD", 0.0);
    for (i = 0 ; i < 5 ; i ++)
    {
        LookWithAngle(DummyUnitCreate("Necromancer", i + 13), i);
        SetCallback(unit + i + 1, 9, OpenDungeonPortal);
    }
    FrameTimer(3, InitDungeonExitPortal);
}

void PotionRespawn(int ptr)
{
    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
    GreenSparkAt(LocationX(1), LocationY(1));
    PlaySoundAround(ptr,SOUND_FlagRespawn);
    PotionDrop(ToStr(GetDirection(ptr)), 1);
    Delete(ptr);
}

void PotionPic()
{
    int spot = GetTrigger()+1;

    WispDestroyFX(GetObjectX(spot), GetObjectY(spot));
    FrameTimerWithArg(180, GetTrigger() + 1, PotionRespawn);
}

int PotionDrop(string name, int wp)
{
    int unit = CreateObject(name, wp);

    LookWithAngle(CreateObject("InvisibleLightBlueLow", wp), SToInt(name));
    Raise(unit + 1, PotionPic);
    RegistItemPickupCallback(unit, ToInt(GetObjectZ(unit + 1)));    
    return unit;
}

void PlaceHotPotions()
{
    string pName = "RedPotion";
    int i;
    for (i = 51 ; i <= 77 ; i ++)
        PotionDrop(pName, i);
}

void InvincibleMyItems()
{
    int res = SetInvincibleInventoryItems(OTHER);
    char buff[64];

    if (res)
    {
        NoxSprintfString(&buff, "%d 개 아이템이 처리되었습니다", &res, 1);
        UniPrint(OTHER, ReadStringAddressEx(&buff));
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
    }
}

int RedKeyUrchin(int spMark)
{
    int unit = CreateObjectAt("Urchin", GetObjectX(spMark), GetObjectY(spMark));

    SetUnitMaxHealth(unit, 62);
    UnitZeroFleeRange(unit);
    DungeonMonsterCommon(unit);

    return unit;
}

int RedKeyBomber(int spMark)
{
    string bomberName[] = {"Bomber", "BomberBlue", "BomberGreen", "BomberYellow"};
    int unit = CreateObjectAt(bomberName[Random(0, 3)], GetObjectX(spMark), GetObjectY(spMark));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2b8, 0x4e83b0);
    SetUnitMaxHealth(unit, 98);
    UnitLinkBinScript(unit, BomberGreenBinTable());
    DungeonMonsterCommon(unit);

    return unit;
}

int RedKeySmallSpider(int spMark)
{
    int unit = CreateObjectAt("SmallAlbinoSpider", GetObjectX(spMark), GetObjectY(spMark));

    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    SetUnitMaxHealth(unit, 110);
    DungeonMonsterCommon(unit);

    return unit;
}

int RedKeyBat(int spMark)
{
    int unit = CreateObjectAt("Bat", GetObjectX(spMark), GetObjectY(spMark));

    SetUnitMaxHealth(unit, 75);
    DungeonMonsterCommon(unit);
    return unit;
}

int RedKeyWisp(int spMark)
{
    int unit = CreateObjectAt("WillOWisp", GetObjectX(spMark), GetObjectY(spMark));

    SetUnitMaxHealth(unit, 85);
    UnitLinkBinScript(unit, wispBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    DungeonMonsterCommon(unit);
    return unit;
}

int getRedKeyDungeonMonFn(){
    short *p;

    if (!p)
    {
        short fns[]={
            RedKeyUrchin,
            RedKeyBomber,
            RedKeySmallSpider,
            RedKeyBat,
            RedKeyWisp,
        };
        p=fns;
    }
    return p[Random(0,4)];
}

void ActiveMonsterCreateMarker(int spMark)
{
    int amount = GetDirection(spMark);

    if (IsObjectOn(spMark))
    {
        if (amount)
        {
            Bind(getRedKeyDungeonMonFn(), &spMark);
            FrameTimerWithArg(3, spMark, ActiveMonsterCreateMarker);
            LookWithAngle(spMark, amount - 1);
        }
    }
}

void PlaceRedKeyDungeonMob(int location, int amount)
{
    int spMark = CreateObject("InvisibleLightBlueLow", location);

    LookWithAngle(spMark, amount);
    FrameTimerWithArg(1, spMark, ActiveMonsterCreateMarker);
}

void OpenShopWalls()
{
    WallOpen(Wall(194, 246));
    WallOpen(Wall(193, 247));
    ObjectOn(Object("RedKeyElevator"));
    PlaceRedKeyDungeonMob(87, 10);
    PlaceRedKeyDungeonMob(88, 10);
    PlaceRedKeyDungeonMob(89, 15);
    PlaceRedKeyDungeonMob(90, 20);
    PlaceRedKeyDungeonMob(92, 20);
    PlaceRedKeyDungeonMob(93, 20);
    ObjectOff(SELF);
}

void DisableLockedGates()
{
    UnlockDoor(Object("MainGate1"));
    UnlockDoor(Object("MainGate11"));
    UnlockDoor(Object("MainGate2"));
    UnlockDoor(Object("MainGate21"));
    ObjectOff(SELF);
}

void DelayCreatureFollow(int ptr)
{
    int cre = ToInt(GetObjectZ(ptr)), owner = GetOwner(ptr);

    if (CurrentHealth(cre) && CurrentHealth(owner))
    {
        CreatureFollow(cre, owner);
        AggressionLevel(cre, 1.0);
    }
    Delete(ptr);
}

void CreatureEscort(int owner, int cre)
{
    int unit = CreateObject("InvisibleLightBlueHigh", 1);

    SetOwner(owner, unit);
    Raise(unit, cre);
    FrameTimerWithArg(1, unit, DelayCreatureFollow);
}

void MyCreatureIsDead()
{
    int ptr = GetTrigger() + 1;
    int owner = GetOwner(ptr);

    PlaySoundAround(SELF, SOUND_SpellPopOffBook);
    WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
    if (CurrentHealth(owner))
        UniPrint(owner, "방금 당신의 크리쳐 하나가 적에게 격추되었습니다");
    UniChatMessage(SELF, "나의 죽음을 적에게 알리지 마라~", 180);
    DeleteObjectTimer(SELF, 60);
    Delete(GetTrigger() + 1);
}

int NewMyCreature(string name, int owner, int plr)
{
    int idx = ArraySearch(plr);

    if (idx + 1)
    {
        int unit = CreateObjectAt(name, GetObjectX(owner), GetObjectY(owner));
        LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), plr);
        SetOwner(owner, unit + 1);
        SetOwner(owner, unit);
        GiveCreatureToPlayer(owner, unit);
        CreatureEscort(owner, unit);
        ArrayInsert(unit, idx);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        AggressionLevel(unit, 1.0);
        SetUnitScanRange(unit, 400.0);
        SetCallback(unit, 5, MyCreatureIsDead);
        AttachHealthbar(unit);
        return unit;
    }
    return 0;
}

void HorrendousHitHandler()
{
    if (!UnitCheckEnchant(SELF, GetLShift( ENCHANT_SHOCK)))
        Enchant(SELF, "ENCHANT_SHOCK", 10.0);
}

void DemonSightEvent()
{
    if (DistanceUnitToUnit(SELF, OTHER) < 98.0)
	{
		DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
		Damage(OTHER, SELF, 30, 14);
	}
	MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void CreatureDemonProcess(int unit, int owner)
{
    SetUnitMaxHealth(unit, 1100);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020); //TODO: disable cast magic and applies always running.
    SetCallback(unit, 3, DemonSightEvent);
    SetCallback(unit, 13, EnableCreatureSight);
    SetCallback(unit, 7, onUserCreatureHurt);
}

void PoisonFrogVomit(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    while (1)
    {
        if (CurrentHealth(owner) && CurrentHealth(target))
        {
            if (count)
            {
                if (DistanceUnitToUnit(ptr, target) > 31.0)
                {
                    MoveObject(ptr, GetObjectX(ptr) + UnitRatioX(target, ptr, 11.0), GetObjectY(ptr) + UnitRatioY(target, ptr, 11.0));
                    MoveObject(ptr + 1, GetObjectX(ptr), GetObjectY(ptr));
                    LookAtObject(ptr + 1, target);
                    Walk(ptr + 1, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
                    LookWithAngle(ptr, count - 1);
                    FrameTimerWithArg(1, ptr, PoisonFrogVomit);
                    break;
                }
                else
                    Damage(target, owner, 10, 5);
            }
            else
                Effect("SMOKE_BLAST", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
        }
        Delete(ptr);
        Delete(ptr + 1);
        break;
    }
}

void PlantDetectHandler()
{
    if (CurrentHealth(SELF) && CurrentHealth(OTHER))
    {
        int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(SELF), GetObjectY(SELF));

        PlaySoundAround(unit, SOUND_GruntIdle);
        PlaySoundAround(unit, SOUND_EggBreak);
        UnitNoCollide(CreateObjectAt("GreenFrog", GetObjectX(unit), GetObjectY(unit)));
        ObjectOff(unit + 1);
        SetOwner(SELF, unit);
        LookAtObject(unit + 1, OTHER);
        Raise(unit, GetCaller());
        LookWithAngle(unit, 40);
        FrameTimerWithArg(1, unit, PoisonFrogVomit);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void onManaExplosionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 150, DAMAGE_TYPE_PLASMA);
        }
    }
}

void MyHecubahManabomb(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        float x=GetObjectX(ptr), y=GetObjectY(ptr);
        SplashDamageAtEx(GetOwner(owner), x,y, 135.0, onManaExplosionSplash);
        Effect("WHITE_FLASH", x,y, 0.0, 0.0);
        Effect("JIGGLE", x,y, 30.0, 0.0);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void MyHecubahHitHandler()
{
    onUserCreatureHurt();
    if (UnitCheckEnchant(SELF, GetLShift(ENCHANT_PROTECT_FROM_FIRE)))
    {
        EnchantOff(SELF, "ENCHANT_PROTECT_FROM_FIRE");
        if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY)))
        {
            int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));

            PlaySoundAround(unit, SOUND_ManaBombCast);
            CreateObjectAt("ManaBombCharge", GetObjectX(SELF), GetObjectY(SELF));
            SetOwner(SELF, unit);
            FrameTimerWithArg(48, unit, MyHecubahManabomb);
            Enchant(SELF, "ENCHANT_PROTECT_FROM_ELECTRICITY", 9.0);
            UniChatMessage(SELF, "이셋끼들이!!!", 180);
        }
    }
    else
    {
        Enchant(SELF, "ENCHANT_PROTECT_FROM_FIRE", 0.6);
    }
}

void RestRoomForCreatures()
{
    if (CurrentHealth(OTHER))
    {
        if (MaxHealth(OTHER) ^ CurrentHealth(OTHER))
        {
            int fx=CreateObjectAt("MagicSpark", GetObjectX(OTHER), GetObjectY(OTHER));
            
            DeleteObjectTimer(fx, 9);
            Effect("LESSER_EXPLOSION", GetObjectX(fx), GetObjectY(fx), 0.0, 0.0);
            PlaySoundAround(fx, SOUND_ShellMouseBoom);
            RestoreHealth(OTHER, MaxHealth(OTHER) - CurrentHealth(OTHER));
        }
    }
}

int MyHecubahCreate(int owner, int plr)
{
    int unit = NewMyCreature("Hecubah", owner, plr), ptr;
    
    if (unit)
    {
        UnitLinkBinScript(unit, HecubahBinTable());
        UnitZeroFleeRange(unit);
        SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x200);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
        SetCallback(unit, 7, MyHecubahHitHandler);
        SetUnitMaxHealth(unit, 1050);
        Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    }
    return unit;
}

static void showNotEnoughGold(char *itemName, int pay)
{
    int args[]={itemName, pay};
    char buff[128];

    NoxSprintfString(&buff, "%s을 소환합니다, 이 작업은 %d달러가 필요합니다. 계속하려면 한번 더 클릭해주세요", &args, 2);
    UniPrint(OTHER, ReadStringAddressEx(&buff));
}

static void onUserCreatureHurt()
{
    if (CurrentHealth(SELF))
    {
        if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(SELF, EnchantList(ENCHANT_INVULNERABLE), 5.0);
            RestoreHealth(SELF, 15);
        }
    }
}

void BuyCreatureHecubah()
{
    int pay = ToInt(GetObjectZ(GetTrigger() + 1)), unit, plr = CheckPlayer();

    if (plr < 0) return;
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= pay)
        {
            unit = MyHecubahCreate(GetCaller(), plr);
            if (unit)
            {
                ChangeGold(OTHER, -pay);
                UniPrint(OTHER, "거래성공! 헤쿠바가 소환되었습니다");
                UniPrint(OTHER, "소환된 유닛을 데려오려면 웃기(L) 키를 누르시면 됩니다");
            }
            else
                UniPrint(OTHER, "거래실패! 소환된 유닛이 너무 많습니다 -최대 4기까지만 소환 가능합니다");
        }
        else
            UniPrint(OTHER, "거래실패! 잔액이 부족합니다");
    }
    else
    {
        showNotEnoughGold(StringUtilGetScriptStringPtr("악녀"), pay);
        Enchant(OTHER, "ENCHANT_AFRAID", 0.3);
    }
}

void BuyCreaturePlant()
{
    int pay = ToInt(GetObjectZ(GetTrigger() + 1)), unit, plr = CheckPlayer();

    if (plr < 0) return;
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= pay)
        {
            unit = NewMyCreature("CarnivorousPlant", GetCaller(), plr);
            if (unit)
            {
                SetUnitMaxHealth(unit, 1200);
                Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
                SetUnitSpeed(unit, 2.2);
                SetCallback(unit, 3, PlantDetectHandler);
                SetCallback(unit, 7, onUserCreatureHurt);
                AggressionLevel(unit, 1.0);
                ChangeGold(OTHER, -pay);
                UniPrint(OTHER, "거래성공! 식인식물가 소환되었습니다");
                UniPrint(OTHER, "소환된 유닛을 데려오려면 웃기(L) 키를 누르시면 됩니다");
            }
            else
                UniPrint(OTHER, "거래실패! 소환된 유닛이 너무 많습니다 -최대 4기까지만 소환 가능합니다");
        }
        else
            UniPrint(OTHER, "거래실패! 잔액이 부족합니다");
    }
    else
    {
        showNotEnoughGold(StringUtilGetScriptStringPtr("식인식물"), pay);
        Enchant(OTHER, "ENCHANT_AFRAID", 0.3);
    }
}

void BuyCreatureDemon()
{
    int pay = ToInt(GetObjectZ(GetTrigger() + 1)), unit, plr = CheckPlayer();

    if (plr < 0) return;
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= pay)
        {
            unit = NewMyCreature("Demon", GetCaller(), plr);
            if (unit)
            {
                CreatureDemonProcess(unit, GetCaller());
                ChangeGold(OTHER, -pay);
                UniPrint(OTHER, "거래성공! 데몬가 소환되었습니다");
                UniPrint(OTHER, "소환된 유닛을 데려오려면 웃기(L) 키를 누르시면 됩니다");
            }
            else
                UniPrint(OTHER, "거래실패! 소환된 유닛이 너무 많습니다 -최대 4기까지만 소환 가능합니다");
        }
        else
            UniPrint(OTHER, "거래실패! 잔액이 부족합니다");
    }
    else
    {
        showNotEnoughGold(StringUtilGetScriptStringPtr("키러리언"), pay);
        Enchant(OTHER, "ENCHANT_AFRAID", 0.3);
    }
}

void BuyCreatureHorrendous()
{
    int pay = ToInt(GetObjectZ(GetTrigger() + 1)), plr = CheckPlayer();

    if (plr < 0) return;
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= pay)
        {
            int unit = NewMyCreature("Horrendous", GetCaller(), plr);
            if (unit)
            {
                Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
                SetUnitMaxHealth(unit, 1100);
                SetCallback(unit, 7, HorrendousHitHandler);
                ChangeGold(OTHER, -pay);
                UniPrint(OTHER, "거래성공! 호렌더스가 소환되었습니다");
                UniPrint(OTHER, "소환된 유닛을 데려오려면 웃기(L) 키를 누르시면 됩니다");
            }
            else
                UniPrint(OTHER, "거래실패! 소환된 유닛이 너무 많습니다 -최대 4기까지만 소환 가능합니다");
        }
        else
            UniPrint(OTHER, "거래실패! 잔액이 부족합니다");
    }
    else
    {
        showNotEnoughGold(StringUtilGetScriptStringPtr("호렌더스"), pay);
        Enchant(OTHER, "ENCHANT_AFRAID", 0.3);
    }
}

void PlaceCreatureShop()
{
    int unit = CreateObject("RedPotion", 1) + 1;

    Delete(unit - 1);
    SetDialog(DummyUnitCreate("Horrendous", 79), "aa", BuyCreatureHorrendous, PlaceCreatureShop);
    Raise(CreateObject("InvisibleLightBlueLow", 79), 100000);
    SetDialog(DummyUnitCreate("Demon", 80), "aa", BuyCreatureDemon, PlaceCreatureShop);
    Raise(CreateObject("InvisibleLightBlueLow", 80), 120000);
    SetDialog(DummyUnitCreate("CarnivorousPlant", 81), "aa", BuyCreaturePlant, PlaceCreatureShop);
    Raise(CreateObject("InvisibleLightBlueLow", 81), 140000);
    SetDialog(DummyUnitCreate("Hecubah", 82), "aa", BuyCreatureHecubah, PlaceCreatureShop);
    Raise(CreateObject("InvisibleLightBlueLow", 82), 140000);
    LookWithAngle(unit, 160);
    LookWithAngle(unit + 2, 160);
    LookWithAngle(unit + 4, 160);
    LookWithAngle(unit + 6, 160);
}

void ShowMapTitle()
{
    PlaySoundAround(SELF, SOUND_JournalEntryAdd);
    ObjectOff(SELF);
    UniPrintToAll("긴말은 마치 잔소리와 같아서 안하겠습니다... 제발 이곳의 주인인 [로날드] 만 죽여주세요!!");
}

void ExitMarkSetting(int wp)
{
    float point[2];
    ComputeAreaRhombus(point,931.0, 1621.0, 103.0, 655.0);
    TeleportLocation(wp, point[0],point[1]);
}

void ExitObstacleCreate(int count)
{
    string table[] = {
        "GauntletExitA", "GauntletExitB", "GauntletWarpExitA", "GauntletWarpExitB"
    };

    if (count)
    {
        ExitMarkSetting(1);
        int unit = CreateObject(table[Random(0, 3)], 1);
        SetUnitMass(unit, 4.0);
        FrameTimerWithArg(1, count - 1, ExitObstacleCreate);
    }
}

void BossRoomCardKeyCreate()
{
    ExitMarkSetting(1);
    int cardKey = CreateAnyKeyAtLocation(1, 0, "대표이사 실 열쇠");
    Enchant(cardKey, "ENCHANT_SHIELD", 0.0);
}

void UrchinWillGiveHint()
{
    PlaySoundAround(OTHER, SOUND_JournalEntryAdd);
    UniChatMessage(SELF, "맥도날드 알바생:\n열쇠를 잃어버렸어! 분명히 이 지하창고 어딘가에 있을거란 말이야", 180);
}

void CaveAreaInit()
{
    int unit = DummyUnitCreate("Urchin", 85);

    LookWithAngle(unit, 32);
    SetDialog(unit, "aa", UrchinWillGiveHint, PlaceCreatureShop);
    FrameTimerWithArg(1, 200, ExitObstacleCreate);
    BossRoomCardKeyCreate();
}

void DisableObject(int unit)
{
    if (IsObjectOn(unit))
        ObjectOff(unit);
}

void SomeReadableSetup()
{
    RegistSignMessage(Object("GarageEntryDesc"), "이 엘리베이터는 지하 식료품 창고로 갑니다");
    RegistSignMessage(Object("Reader1"), "이 표지판 위치에서 오른쪽 상단에 비밀벽이 있습니다");
    RegistSignMessage(Object("Reader2"), "붉은 열쇠가 이곳 아래층에 있어요");
    RegistSignMessage(Object("Reader3"), "이 비콘을 밟으면 소유하고 계신 모든 인벤토리의 내구도가 무한이 됩니다");
    RegistSignMessage(Object("Reader4"), "주의! 보스 구역:: 이 표지판 뒤쪽 벽을 열기 위해 푸른 열쇠를 바닥에 꽂아라");

    RegistSignMessage(Object("DunReader1"), "던전설명: 저주받은 신전");
    RegistSignMessage(Object("DunReader2"), "던전설명: 세계 최대 열대 습지 브라질 '판타나우'");
    RegistSignMessage(Object("DunReader3"), "던전설명: 만년설산");
    RegistSignMessage(Object("DunReader4"), "던전설명: 인페르날");
    RegistSignMessage(Object("DunReader5"), "다크 포레스트:: 제일 쉬운 던전이므로 이곳 먼저 탐험해 보세요");

    RegistSignMessage(Object("BossZoneReader"), "로날드 사무실: 이곳은 보안구역으로 관계자 외 출입을 금지합니다");
    RegistSignMessage(Object("GarageReader"), "이곳은 지하창고 입니다");

    RegistSignMessage(Object("UnderReader1"), "이 붉은 열쇠로 5개의 던전을 엽니다. 이것은 그것중 1개 입니다");
    RegistSignMessage(Object("UnderReader2"), "우선 이 붉은 열쇠 1개로 첫번째 던전을 열어보세요");
}

void DelayInitiRun()
{
    MasterUnit();
    BlueKeyCheckerPlace(Object("BlueKeyZone"));
    DrawTextOnMap();
    DungeonAllign(-1);
    FrameTimer(1, PlaceHotPotions);
    FrameTimer(2, PlaceCreatureShop);
    FrameTimer(4, CaveAreaInit);
    FrameTimer(1, SetGameTypeCoopMode);
    MakeCoopTeam();
    FrameTimer(3, SomeReadableSetup);

    // FrameTimerWithArg(60, CreateObjectAt("InvisibleLightBlueHigh", LocationX(114),LocationY(114)), RespawnMC);
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("mcdornaldse-log.txt");
    initFieldItemFunctions();
    InitDungeonPortal();
    PlayerHandlerMainLoop();
    FrameTimer(1, DelayInitiRun);
    FrameTimerWithArg(30, Object("RedKeyElevator"), DisableObject);
    blockObserverMode();
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void DrawTextOnMap()
{
    DrawTextOnBottom(0,0,LocationX(103), LocationY(103));
    DrawTextOnBottom(1,0,LocationX(104), LocationY(104));
    DrawTextOnBottom(2,0,LocationX(105), LocationY(105));
    DrawTextOnBottom(3,0,LocationX(106), LocationY(106));
    DrawTextOnBottom(4,0,LocationX(107), LocationY(107));

    DrawTextOnBottom(5,0,LocationX(108), LocationY(108));
    DrawTextOnBottom(6,0,LocationX(109), LocationY(109));
    DrawTextOnBottom(7,0,LocationX(110), LocationY(110));
    DrawTextOnBottom(1,1,LocationX(86), LocationY(86));
}
