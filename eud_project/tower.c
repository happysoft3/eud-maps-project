
#include "libs/define.h"
#include "libs\unitstruct.h"
#include "libs\printutil.h"
#include "libs\waypoint.h"
#include "libs\itemproperty.h"
#include "libs\spellutil.h"
#include "libs\mathlab.h"
#include "libs\buff.h"
#include "libs\fxeffect.h"
#include "libs\fixtellstory.h"
#include "libs\playerupdate.h"
#include "libs/spelldb.h"

int PLAY_BGM = 0, player[20];
int PlrInven[10], PlrNodePtr[10];
int MapLang;
float TOWER_X[20], TOWER_Y[20];
int Gvar_6[10], GUARD_TOWER[20], TeamFlag[4];
int RED_TEAM[201]; //redTeamMonsters
int BLUE_TEAM[201]; //blueTeamMonsters
//monster_route_way
int Gvar_14[9], Gvar_15[9], Gvar_16[9], Gvar_17[9];
//end
int GUARDIAN[40]; //guardUnits
int END_GAME = 0; //red_blue 승리트리거 반대로 된듯, 포션계열 아이템이 안나옴

#define PLAYER_FLAG_DEATH 0x80000000

int StrongWizardWhiteBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146; 
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link = &arr;
	}
	return link;
}

int CreateYellowPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 639); //YellowPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateBlackPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 641); //BlackPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateWhitePotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 640); //WhitePotion
    SetMemory(ptr + 12, GetMemory(ptr + 12) ^ 0x20);
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = CreateYellowPotion(125, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 640)
        x = CreateWhitePotion(100, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 641)
        x = CreateBlackPotion(85, GetObjectX(unit), GetObjectY(unit));
    if (x ^ unit) Delete(unit);
    return x;
}

int CreateMagicPotion(int location)
{
    return CheckPotionThingID(CreateObject(PotionTable(), location));
}

void forbidSpellWall(int id)
{
    if (SpelldbIsAllowed(id))
        SpelldbSetAllowed(id, FALSE);
    FrameTimerWithArg(1, id, forbidSpellWall);
}

void MapInitialize()
{
    FrameTimerWithArg(1, SpellUtilGetId("SPELL_WALL"), forbidSpellWall);
    MusicEvent();
    ObjectOn(Object("mainSwitch1"));
    ObjectOn(Object("mainSwitch2"));
    initializeGeneratorPos();
    initializeMaps();
    changedImageShopkeepers();
    //spawnBlinkPotions(-1);
    MagicMissileParent(0);

    //loop_Function
    FrameTimer(2, LoopSearchIndex);
    FrameTimer(2, PlayerClassOnLoop);
    //delay_call
    FrameTimer(30, bufferCalls);
    //FrameTimerWithArg(50, 0, spawnBlinkPotions);
}

void MapExit()
{
    MusicEvent();

    ResetPlayerHandlerWhenExitMap();
}

void bufferCalls()
{
    redQueue(-2);
    blueQueue(-2);
    
    FrameTimer(1, HostplayerSelectLanguage);
}

void showMesage()
{
    if (MapLang)
        UniBroadcast("**GameGuide**\nDestroy all the enemy team's towers and kill the team's king.\nwarrior can cast new ability by eye of wolf or TreadLightly");
    else
        UniBroadcast("**게임 설명**\n상대팀 타워와 최종보스를 먼저 파괴한 팀이 승리합니다\n전사는 늑데의 눈 이나 조심스럽게 걷기를 쓰면 새로운 기술이 발동됩니다");
    CheckWall(-1);
    loopCheckWall();
    StaffShopPlace();
}

void MainGameStart()
{
    PlacingTeleportBook();
    FrameTimer(1, PicketDescription);
    FrameTimer(3, spawnGuardTower);
    SecondTimer(10, showMesage);
}

int MagicMissileParent(int num)
{
    int ptr[10], k;

    if (!ptr[0])
    {
        for (k = 9 ; k >= 0 ; k --)
        {
            CreateObject("Hecubah", k + 157);
            ptr[k] = GetMemory(0x750710);
            Frozen(GetMemory(ptr[k] + 0x2c), 1);
        }
        return 0;
    }
    return ptr[num];
}

void initializeMaps() {
    TeamFlag[0] = Object("redMaster");
    TeamFlag[1] = Object("blueMaster");
    spawnFinalBoss();
    initializeMonsterRoute();
}

string PotionTable()
{
    string table[] = {"RedPotion", "CurePoisonPotion", "BluePotion", "VampirismPotion", "HastePotion", "ShieldPotion", "WhitePotion", "BlackPotion", "PoisonProtectPotion",
    "ShockProtectPotion", "FireProtectPotion"};

    return table[Random(0, 10)];
}

string WeaponTable()
{
    string table[] = {"WarHammer", "GreatSword", "RoundChakram", "FanChakram", "Quiver", "CrossBow", "Bow", "StaffWooden", "OgreAxe",
    "OblivionWierdling", "OblivionHalberd", "OblivionHeart"};

    return table[Random(0, 11)];
}

string StaffTable()
{
    string table[] = {"LesserFireballWand", "FireStormWand", "ForceWand", "InfinitePainWand", "SulphorousFlareWand", "DeathRayWand"};

    return table[Random(0, 5)];
}

string ArmorTable()
{
    string table[] = {"ConjurerHelm", "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", "LeatherLeggings", "MedievalCloak", "WizardRobe", "WizardHelm",
    "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "OrnateHelm", "SteelHelm", "SteelShield"};

    return table[Random(0, 15)];
}

string GoldTable()
{
    string table[] = {"Gold", "QuestGoldChest", "QuestGoldPile"};
    
    return table[Random(0, 2)];
}

void initializeGeneratorPos() {
    TOWER_X[0] = 425.0; TOWER_Y[0] = 1253.0; //RED golem
	TOWER_X[1] = 875.0; TOWER_Y[1] = 1657.0; //wizard
	TOWER_X[2] = 449.0; TOWER_Y[2] = 2080.0; //melee
	TOWER_X[3] = 868.0; TOWER_Y[3] = 2489.0; //melee2
	TOWER_X[4] = 1279.0; TOWER_Y[4] = 2898.0; //archer
	TOWER_X[5] = 1279.0; TOWER_Y[5] = 402.0; //golem
	TOWER_X[6] = 1702.0; TOWER_Y[6] = 817.0; //wizard
	TOWER_X[7] = 2117.0; TOWER_Y[7] = 415.0; //melee
	TOWER_X[8] = 2530.0; TOWER_Y[8] = 809.0; //melee
	TOWER_X[9] = 2932.0; TOWER_Y[9] = 1233.0; //archer
	TOWER_X[10] = 4385.0; TOWER_Y[10] = 5185.0; //BLUE golem
	TOWER_X[11] = 3968.0; TOWER_Y[11] = 4764.0; //wizard
	TOWER_X[12] = 3563.0; TOWER_Y[12] = 5186.0; //melee
	TOWER_X[13] = 3129.0; TOWER_Y[13] = 4765.0; //melee2
	TOWER_X[14] = 2729.0; TOWER_Y[14] = 4357.0; //archer
	TOWER_X[15] = 5213.0; TOWER_Y[15] = 4357.0; //golem
	TOWER_X[16] = 4802.0; TOWER_Y[16] = 3916.0; //wizard
	TOWER_X[17] = 5213.0; TOWER_Y[17] = 3524.0; //melee
	TOWER_X[18] = 4798.0; TOWER_Y[18] = 3095.0; //melee2
	TOWER_X[19] = 4404.0; TOWER_Y[19] = 2690.0; //archer
    FrameTimer(1, putGeneratorInMap);
}

void putGeneratorInMap()
{
    int genIndex;

    if (genIndex < 20)
    {
        GUARD_TOWER[genIndex] = Object("guardTower" + IntToString(genIndex + 1));
        MoveObject(GUARD_TOWER[genIndex], TOWER_X[genIndex], TOWER_Y[genIndex]);
        ObjectOff(GUARD_TOWER[genIndex]);
        SetUnitMaxHealth(GUARD_TOWER[genIndex], 1000);
        genIndex ++;
        FrameTimer(1, putGeneratorInMap);
    }
    else
    {
        setLimitRun();
        FrameTimer(1, coverPowerShieldWithGenerator);
        FrameTimerWithArg(50, 0, SummoningMonster);
        FrameTimer(51, LoopMonsterControl);
    }
}

void coverPowerShieldWithGenerator()
{
    int genIndex = 0;
    
    while (genIndex < 10)
    {
        Enchant(GUARD_TOWER[genIndex], "ENCHANT_INVULNERABLE", 0.0);
        genIndex += 1;
    }
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & PLAYER_FLAG_DEATH;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ PLAYER_FLAG_DEATH;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);
    if (MapLang)
        UniPrintToAll(PlayerIngameNick(player[plr]) + " aa has entered the battlefield");
    else
        UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 전장에 입장하셨습니다");
    return plr;
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k -= 1)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int checkTeam(int unit)
{
    if (!IsAttackedBy(unit, TeamFlag[0]))
        return 1;
    else if (!IsAttackedBy(unit, TeamFlag[1]))
        return 2;
    else
        return 0;
}

void playerTeleport(int playerUnit)
{
    int team = checkTeam(playerUnit);

    Enchant(playerUnit, "ENCHANT_ANCHORED", 0.0);
    if (team == 1)
    {
        MoveObject(playerUnit, GetWaypointX(168), GetWaypointY(168));
        DeleteObjectTimer(CreateObject("bluerain", 168), 10);
    }
    else if (team == 2)
    {
        MoveObject(playerUnit, GetWaypointX(167), GetWaypointY(167));
        DeleteObjectTimer(CreateObject("bluerain", 167), 10);
    }
    else
    {
        Enchant(playerUnit, "ENCHANT_FREEZE", 0.0);
        Enchant(playerUnit, "ENCHANT_ANTI_MAGIC", 0.0);
        LookWithAngle(playerUnit, 0);
        UniPrint(OTHER, "팀이 생성되지 않았습니다, 팀을 생성하여 다시 시도해주세요.");
        UniPrint(OTHER, "Team not created, please create a team and try again.");
        MoveObject(playerUnit, GetWaypointX(103), GetWaypointY(103));
    }
}

void PlayerJoin(int plr)
{
    int playerUnit = player[plr];

    if (PlayerClassCheckDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    playerTeleport(playerUnit);
}

void playerIsFull(int plrUnit)
{
    if (MapLang)
        UniPrintToAll("There are so many players on the battlefield now that we can no longer participate.");
    else
        UniPrintToAll("지금 현재 너무 많은 플레이어가 전장에 있기 때문에 더 이상 참가할 수 없습니다.");
    MoveObject(plrUnit, LocationX(14), LocationY(14));
    Enchant(plrUnit, "ENCHANT_ANTI_MAGIC", 0.0);
}

void getPlayer()
{
    while (CurrentHealth(OTHER))
    {
        int plr = CheckPlayer(), i;

        for (i = 9 ; i >= 0 && plr < 0 ; i -= 1)
        {
            if (!MaxHealth(player[i]))
            {
                plr = PlayerClassOnInit(i, GetCaller());
                break;
            }
        }
        if (plr >= 0)
        {
            PlayerJoin(plr);
            break;
        }
        playerIsFull(OTHER);
        break;
    }
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

void PlayerClassOnUseWarcry(int plr, int pUnit)
{
    getStraightFire(plr);
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_SNEAK)))
    {
        EnchantOff(pUnit, "ENCHANT_SNEAK");
        RemoveTreadLightly(pUnit);
        windbooster(plr);
    }
    int warcryCArr[10], cTime = SpellUtilGetPlayerAbilityCooldown(pUnit, 2);

    if (warcryCArr[plr] ^ cTime)
    {
        if (warcryCArr[plr] == 0)
        {
            PlayerClassOnUseWarcry(plr, pUnit);
        }
        warcryCArr[plr] = cTime;
    }
}

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (true)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void spawnFinalBoss()
{
    TeamFlag[2] = CreateObject("horrendous", 17);
    CreateObject("InvisibleLightBlueHigh", 101);
    TeamFlag[3] = CreateObject("horrendous", 18);
    CreateObject("InvisibleLightBlueHigh", 102);
    setUnitHealth(TeamFlag[2], 3000);
    setUnitHealth(TeamFlag[3], 3000);
    SetCallback(TeamFlag[2], 3, getWeaponToHorrendous);
    SetCallback(TeamFlag[3], 3, getWeaponToHorrendous);
    SetCallback(TeamFlag[2], 5, setHorrendousDeathRed);
    SetCallback(TeamFlag[3], 5, setHorrendousDeathBlue);
    SetOwner(TeamFlag[0], TeamFlag[2]);
    SetOwner(TeamFlag[1], TeamFlag[3]);
    Frozen(TeamFlag[2], 1);
    Frozen(TeamFlag[3], 1);
}

void noKeepBlueMonster()
{
    if (CurrentHealth(OTHER))
    {
        if (CurrentHealth(TeamFlag[2]) && IsAttackedBy(OTHER, TeamFlag[2]) && !GetDirection(TeamFlag[2] + 1))
        {
            Effect("SENTRY_RAY", GetObjectX(TeamFlag[2]), GetObjectY(TeamFlag[2]), GetObjectX(OTHER), GetObjectY(OTHER));
            EnchantOff(OTHER, "ENCHANT_SHIELD");
            EnchantOff(OTHER, "ENCHANT_INVULNERABLE");
            Damage(OTHER, TeamFlag[2], 9999, 14);
        }
    }
}

void noKeepRedMonster()
{
    if (CurrentHealth(OTHER))
    {
        if (CurrentHealth(TeamFlag[3]) && IsAttackedBy(OTHER, TeamFlag[3]) && !GetDirection(TeamFlag[3] + 1))
        {
            Effect("SENTRY_RAY", GetObjectX(TeamFlag[3]), GetObjectY(TeamFlag[3]), GetObjectX(OTHER), GetObjectY(OTHER));
            EnchantOff(OTHER, "ENCHANT_SHIELD");
            EnchantOff(OTHER, "ENCHANT_INVULNERABLE");
            Damage(OTHER, TeamFlag[3], 9999, 14);
        }
    }
}

void getWeaponToHorrendous()
{
    if (CurrentHealth(SELF) && CurrentHealth(OTHER))
    {
        Enchant(SELF, "ENCHANT_BLINDED", 0.34);
        doActionTripleMissile();
    }
}

void doActionTripleMissile() {
    float pos_x, pos_y, var_4 = 0.4;
    int var_2[9]; //arrows
    int i; //repeat_value

    pos_x = UnitAngleCos(SELF, -20.0); //unitXForward(SELF, 1, -20.0);
    pos_y = UnitAngleSin(SELF, -20.0); //unitXForward(SELF, 0, -20.0);
    
    for(i = 0 ; i < 9 ; i ++) {
        MoveWaypoint(20, GetObjectX(SELF) - pos_x + (var_4 * pos_y), GetObjectY(SELF) - pos_y - (var_4 * pos_x));
        var_2[i] = CreateObject("OgreShuriken", 20);
        SetOwner(SELF, var_2[i]);
        LookAtObject(var_2[i], SELF);
        LookWithAngle(var_2[i], GetDirection(var_2[i]) + 128);
        var_4 -= 0.1;
        PushObject(var_2[i], -20.0, GetObjectX(SELF), GetObjectY(SELF));
    }
}

void setHorrendousDeathRed()
{
    if (MapLang)
        UniPrintToAll("Red Team's final boss was shot down !!");
    else
        UniPrintToAll("Red 팀 진영의 최종보스가 격추되었습니다!!");
    END_GAME = 1;
    FrameTimerWithArg(120, 2, gameResult);
}

void setHorrendousDeathBlue()
{
    if (MapLang)
        UniPrintToAll("Blue Team's final boss was shot down !!");
    else
        UniPrintToAll("Blue 팀 진영의 최종보스가 격추되었습니다!!");
    END_GAME = 1;
    FrameTimerWithArg(120, 1, gameResult);
}

void gameResult(int teamId)
{
    string teamName;

    teleportPlayers(teamId);
    MoveObject(Object("startLocation1"), GetWaypointX(56), GetWaypointY(56));
    MoveObject(Object("startLocation2"), GetWaypointX(56), GetWaypointY(56));
    if (teamId == 1)
        teamName = "RED";
    else if (teamId == 2)
        teamName = "BLUE";
    if (MapLang)
        UniPrintToAll(teamName + " Team WIN ...!!");
    else
        UniPrintToAll(teamName + " 팀이 승리하였습니다 ...!!");
    AudioEvent("StaffOblivionAchieve2", 55);
    AudioEvent("StaffOblivionAchieve2", 56);
    Effect("WHITE_FLASH", LocationX(55), LocationY(55), 0.0, 0.0);
    FrameTimer(3, strWinnerTeam);
}

void teleportPlayers(int teamId)
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        if (CurrentHealth(player[i]))
        {
            Enchant(player[i], "ENCHANT_FREEZE", 0.0);
            Enchant(player[i], "ENCHANT_INVULNERABLE", 0.0);
            if (checkTeam(player[i]) == teamId)
            {
                MoveObject(player[i], LocationX(55), LocationY(55));
                TeleportLocationVector(55, 23.0, 0.0);
            }
            else
            {
                MoveObject(player[i], LocationX(56), LocationY(56));
                TeleportLocationVector(56, 23.0, 0.0);
            }
        }
    }
}

void loopHorrendousStat()
{
    int repeat = 0;

    while (repeat < 2)
    {
        if (CurrentHealth(TeamFlag[repeat + 2]))
        {
            float cDist = Distance(GetWaypointX(repeat + 17), GetWaypointY(repeat + 17), GetObjectX(TeamFlag[repeat + 2]), GetObjectY(TeamFlag[repeat + 2]));
            if (cDist > 300.0)
            {
                Effect("LIGHTNING", GetObjectX(TeamFlag[repeat + 2]), GetObjectY(TeamFlag[repeat + 2]), GetWaypointX(repeat + 17), GetWaypointY(repeat + 17));
                MoveObject(TeamFlag[repeat + 2], GetWaypointX(repeat + 17), GetWaypointY(repeat + 17));
                AudioEvent("BlindOff", repeat + 17);
            }
        }
        ++repeat;
    }
    FrameTimer(10, loopHorrendousStat);
}

string LineUnitName(int num)
{
    string table[] = {"MechanicalGolem", "OgreWarlord", "EvilCherub", "EvilCherub", "Skeleton"};

    return table[num];
}

int LineUnitHealth(int num)
{
    int arr[5] = {500, 306, 96, 96, 225};

    return arr[num];
}

int LineMobControlFunc(int binaryCond)
{
    int actions[] = {RedLineMonsterMove, BlueLineMonsterMove};
    
    return actions[binaryCond & 1];
}

void RedLineMonsterMove(int unit)
{
    int target;

    if (HasEnchant(unit,"ENCHANT_MOONGLOW"))
        target = SelectRouteLineRL(unit);
    else
        target = SelectRouteLineRR(unit);
    SetOwner(target, unit + 1);
    CreatureFollow(unit, target);
    AggressionLevel(unit, 0.83);
}

void BlueLineMonsterMove(int unit)
{
    int target;

    if (HasEnchant(unit, "ENCHANT_MOONGLOW"))
        target = SelectRouteLineBL(unit);
    else
        target = SelectRouteLineBR(unit);
    SetOwner(target, unit + 1);
    CreatureFollow(unit, target);
    AggressionLevel(unit, 0.83);
}

int SeprateTeamPartSummon(int cond, int towerUnit, string unitName)
{
    string teamSign[] = {"ENCHANT_PROTECT_FROM_FIRE", "ENCHANT_PROTECT_FROM_ELECTRICITY"};
    int pic;
    //TODO: cond==0 >> RedTeam, cond==1 >> BlueTeam
    if (!cond)
    {
        if (RED_TEAM[200] >= 200)
            return 0;
        else
            pic = redQueue(-1);
    }
    else
    {
        if (BLUE_TEAM[200] >= 200)
            return 0;
        else
            pic = blueQueue(-1);
    }
    MoveWaypoint(1, GetObjectX(towerUnit), GetObjectY(towerUnit));
    DeleteObjectTimer(CreateObject("ForceOfNatureCharge", 1), 20);
    AudioEvent("MonsterGeneratorSpawn", 1);
    int unit = CreateObject(unitName, 1);
    SetUnitData(pic, unit);
    SetOwner(towerUnit, unit);
    Enchant(unit, teamSign[cond], 0.0);
    if (!cond)
    {
        SetCallback(unit, 5, redMonsterSetDeaths);
        RED_TEAM[pic] = unit;
    }
    else
    {
        SetCallback(unit, 5, blueMonsterSetDeaths);
        BLUE_TEAM[pic] = unit;
    }

    SetCallback(unit, 7, RegistanceDeathBall);
    FrameTimerWithArg(1, unit, LineMobControlFunc(cond));
    return unit;
}

void SummoningMonster(int indexNum)
{
    int towerUnit = GUARD_TOWER[indexNum], unit, type;

    if (CurrentHealth(GUARD_TOWER[indexNum]))
    {
        unit = SeprateTeamPartSummon(indexNum >= 10, towerUnit, LineUnitName(type));
        if (unit)
        {
            if ((indexNum % 10) < 5)
                Enchant(unit, "ENCHANT_MOONGLOW", 0.0);
            SetUnitMaxHealth(unit, LineUnitHealth(type));
        }
    }
    type = (type + 1) % 5;
    if (!END_GAME)
    {
        if (indexNum ^ 19)
            FrameTimerWithArg(1, indexNum + 1, SummoningMonster);
        else
            SecondTimerWithArg(15, 0, SummoningMonster);
    }
}

void SetUnitData(int data, int targetUnit)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(targetUnit), GetObjectY(targetUnit));

    Raise(unit, ToFloat(data));
}

void setUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void redMonsterSetDeaths()
{
    int ptr = GetTrigger() + 1;

    --RED_TEAM[200];
    redQueue(ToInt(GetObjectZ(ptr)));
    Delete(ptr);
    DeleteObjectTimer(SELF, 30);
    TeleportLocation(57, GetObjectX(SELF), GetObjectY(SELF));
    PutRandomReward(57);
}

void blueMonsterSetDeaths()
{
    int ptr = GetTrigger() + 1;

    --BLUE_TEAM[200];
    blueQueue(ToInt(GetObjectZ(ptr)));
    Delete(ptr);
    DeleteObjectTimer(SELF, 30);
    TeleportLocation(57, GetObjectX(SELF), GetObjectY(SELF));
    PutRandomReward(57);
}

void SetSpecialWeapon(int itemUnit)
{
    int thingId = GetUnitThingID(itemUnit);
    int ptr = UnitToPtr(itemUnit);

    if (thingId >= 222 && thingId <= 225)
    {
        SetItemPropertyAllowAllDrop(itemUnit);
        DisableOblivionItemPickupEvent(itemUnit);
    }
    else if (thingId == 1178)
        SetMemory(GetMemory(ptr + 0x2e0), 0x6464);
    else if (thingId == 1168)
        SetMemory(GetMemory(ptr + 0x2e0), 0x3232);
}

int CreateRandomWeapon(int wp)
{
    int weapon = CreateObject(WeaponTable(), wp);

    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    SetSpecialWeapon(weapon);
    return weapon;
}

int CreateRandomArmor(int wp)
{
    int armor = CreateObject(ArmorTable(), wp);

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return armor;
}

int CreateGold(int wp)
{
    int gold = CreateObject(GoldTable(), wp);
    int *ptr = UnitToPtr(gold);

    if (ptr != NULLPTR)
    {
        int *goldAmount = ptr[173];

        if (goldAmount != NULLPTR)
            goldAmount[0] = Random(1200, 4800);
        return ptr[11];
    }
    return 0;
}

int PutStaff(int locationId)
{
    return CreateObject(StaffTable(), locationId);
}

int PutHotPotion(int locationId)
{
    return CreateObject("RedPotion", locationId);
}

int NothingElse(int no)
{
    return 0;
}

int RandomRewardTable(int actIndex)
{
    int actions[] = {CreateGold, CreateRandomArmor, PutStaff, CreateRandomWeapon, CreateMagicPotion, PutHotPotion, NothingElse, NothingElse};

    return actions[actIndex & 7];
}

void PutRandomReward(int locationId)
{
    int unit = CallFunctionWithArgInt(RandomRewardTable(Random(0, 7)), locationId);

    if (unit)
        DeleteObjectTimer(unit, 3000);
}

int redQueue(int num)
{
    int data[200];
    int cur_in, cur_out;

    if (cur_in == 200)
        cur_in = 0;
    if (cur_out == 200)
        cur_out = 0;

    if (num >= 0) //push_queue
    {
        data[cur_in++] = num;
        return 0;
    }
    else if (num == -1)
    {
        return data[(++cur_out) - 1];
    }
    else //init_set
    {
        int i;

        for (i = 199 ; i >= 0 ; i -= 1)
            data[i] = i;
        return 0;
    }
}

int blueQueue(int num)
{
    int data[200];
    int cur_in, cur_out;

    if (cur_in == 200)
        cur_in = 0;
    if (cur_out == 200)
        cur_out = 0;

    if (num >= 0) //push_queue
    {
        data[cur_in++] = num;
        return 0;
    }
    else if (num == -1)
    {
        return data[(++cur_out) - 1];
    }
    else //init_set
    {
        int i;

        for (i = 199 ; i >= 0 ; i -= 1)
            data[i] = i;
        return 0;
    }
}

void killTower()
{
    int redDeath; //red_deaths
    int blueDeath; //blue_deaths
    string msgArray[] = {
        "방금 RED 팀의 모든 소환 오벨리스트가 파괴 되었습니다!",
        "방금 BLUE 팀의 모든 소환 오벨리스크가 파괴 되었습니다!",
        "RED 팀의 호렌더스의 무적화 상태가 해제 되었습니다!",
        "BLUE 팀의 호렌더스의 무적화 상태가 해제 되었습니다!",
        "All the summoning obelisks of the RED team have just been destroyed!",
        "All the summoning obelisks of the BLUE team have just been destroyed!",
        "The Red team's BOSS invincibility has now been disabled.",
        "The Blue team's BOSS invincibility has now been disabled."};
    string *msg = SToInt( &msgArray ) + ((MapLang&1) << 4);

    if (!IsAttackedBy(SELF, TeamFlag[2]))
    {
        if (++redDeath == 10) {
            unsetFinalBossGodMod(0);
            UniPrintToAll(msg[0]);
            UniPrintToAll(msg[2]);
        }
    }
    else if (!IsAttackedBy(SELF, TeamFlag[3]))
    {
        if (++blueDeath == 10)
        {
            unsetFinalBossGodMod(1);
            UniPrintToAll(msg[1]);
            UniPrintToAll(msg[3]);
        }
    }
}
void RegistanceDeathBall()
{
    if (IsMissileUnit(OTHER))
    {
        if (GetUnitThingID(OTHER) == 1177)
            MoveObject(OTHER, GetObjectX(SELF), GetObjectY(SELF));
    }
}

void RedLineMonsterControl(int unit)
{
    int target;

    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit,"ENCHANT_CHARMING"))
            CastSpellObjectObject("SPELL_COUNTERSPELL", unit, unit);
        if (HasEnchant(unit,"ENCHANT_PROTECT_FROM_FIRE"))
        {
            if (HasEnchant(unit,"ENCHANT_MOONGLOW"))
                target = SelectRouteLineRL(unit);
            else
                target = SelectRouteLineRR(unit);
        }
        if (GetOwner(unit + 1) ^ target)
        {
            SetOwner(target, unit + 1);
            CreatureFollow(unit, target);
            AggressionLevel(unit, 0.83);
        }
    }
}

void BlueLineMonsterControl(int unit)
{
    int target;

    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit, "ENCHANT_CHARMING"))
            CastSpellObjectObject("SPELL_COUNTERSPELL", unit, unit);
        if (HasEnchant(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY"))
        {
            if (HasEnchant(unit, "ENCHANT_MOONGLOW"))
                target = SelectRouteLineBL(unit);
            else
                target = SelectRouteLineBR(unit);
        }
        if (GetOwner(unit + 1) ^ target)
        {
            SetOwner(target, unit + 1);
            CreatureFollow(unit, target);
            AggressionLevel(unit, 0.83);
        }
    }
}

void LoopMonsterControl()
{
    //v11, 12
    int index, i;

    //while 30, frame 3
    for (i = 9 ; i >= 0 ; i -= 1)
    {
        RedLineMonsterControl(RED_TEAM[index + i]);
        BlueLineMonsterControl(BLUE_TEAM[index + i]);
    }
    index = (index + 10) % 200;
    if (!END_GAME)
        FrameTimer(1, LoopMonsterControl);
}

int SelectRouteLineRL(int unit)
{
    float xPos = GetObjectX(unit), yPos = GetObjectY(unit);
    
    if (xPos < 868.0 && yPos < 1700.0)
        return Gvar_14[0];
    else if (xPos > 402.0 && yPos < 2166.0)
        return Gvar_14[1];
    else if (xPos < 1327.0 && yPos < 2963.0)
        return Gvar_14[2];
    else if (xPos < 1914.0 && yPos > 2385.0)
        return Gvar_14[3];
    else if (xPos < 3330.0 && yPos < 3789.0)
        return Gvar_14[4];
    else if (xPos > 2661.0 && yPos < 4441.0)
        return Gvar_14[5];
    else if (xPos < 3611.0 && yPos < 5235.0)
        return Gvar_14[6];
    else if (xPos < 4034.0 && yPos > 4784.0)
        return Gvar_14[7];
    else if (xPos < 4406.0 && yPos < 5196.0)
        return Gvar_14[8];
    else
        return TeamFlag[3];
}
int SelectRouteLineRR(int unit)
{
    float xPos = GetObjectX(unit), yPos = GetObjectY(unit);

    if (xPos < 1742.0 && yPos < 821.0)
        return Gvar_15[0];
    else if (xPos < 2160.0 && yPos > 451.0)
        return Gvar_15[1];
    else if (xPos < 2975.0 && yPos < 1307.0)
        return Gvar_15[2];
    else if (xPos > 2350.0 && yPos < 1916.0)
        return Gvar_15[3];
    else if (xPos < 3747.0 && yPos < 3292.0)
        return Gvar_15[4];
    else if (xPos < 4424.0 && yPos > 2677.0)
        return Gvar_15[5];
    else if (xPos < 5259.0 && yPos < 3558.0)
        return Gvar_15[6];
    else if (xPos > 4770.0 && yPos < 3975.0)
        return Gvar_15[7];
    else if (xPos < 5250.0 && yPos < 4403.0)
        return Gvar_15[8];
    else
        return TeamFlag[3];
}

int SelectRouteLineBL(int unit)
{
    float xPos = GetObjectX(unit), yPos = GetObjectY(unit);

    if (xPos > 4005.0 && yPos > 4792.0)
        return Gvar_16[0];
    else if (xPos > 3562.0 && yPos < 5211.0)
        return Gvar_16[1];
    else if (xPos > 2666.0 && yPos > 4358.0)
        return Gvar_16[2];
    else if (xPos < 3315.0 && yPos > 3738.0)
        return Gvar_16[3];
    else if (xPos > 1918.0 && yPos > 2368.0)
        return Gvar_16[4];
    else if (xPos > 1306.0 && yPos < 2926.0)
        return Gvar_16[5];
    else if (xPos > 461.0 && yPos > 2095.0)
        return Gvar_16[6];
    else if (xPos < 893.0 && yPos > 1683.0)
        return Gvar_16[7];
    else if (xPos > 425.0 && yPos > 1245.0)
        return Gvar_16[8];
    else
        return TeamFlag[2];
}

int SelectRouteLineBR(int unit)
{
    float xPos = GetObjectX(unit), yPos = GetObjectY(unit);

    if (xPos > 4770.0 && yPos > 3916.0)
        return Gvar_17[0];
    else if (xPos < 5217.0 && yPos > 3560.0)
        return Gvar_17[1];
    else if (xPos > 4385.0 && yPos > 2686.0)
        return Gvar_17[2];
    else if (xPos > 3760.0 && yPos < 3302.0)
        return Gvar_17[3];
    else if (xPos > 2360.0 && yPos > 1868.0)
        return Gvar_17[4];
    else if (xPos < 2974.0 && yPos > 1280.0)
        return Gvar_17[5];
    else if (xPos > 2130.0 && yPos > 435.0)
        return Gvar_17[6];
    else if (xPos < 1733.0 && yPos > 846.0)
        return Gvar_17[7];
    else if (xPos > 1297.0 && yPos > 447.0)
        return Gvar_17[8];
    else
        return TeamFlag[2];
}

void changedImageShopkeepers()
{
    int var_0[2];

	var_0[0] = Waypoint("shopkeeperWP");
	var_0[1] = Object("warArmorShop");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory3SW",var_0[0]);
	var_0[1] = Object("warWeaponShop");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory3SW",var_0[0]);
	var_0[1] = Object("conArmor");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory4SE",var_0[0]);
	var_0[1] = Object("conWeaponShop");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory4SE",var_0[0]);
	var_0[1] = Object("magicStaffShop");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory4NE",var_0[0]);
	var_0[1] = Object("magicShop2");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory5NE",var_0[0]);
	var_0[1] = Object("conArmorShop2");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory5NW",var_0[0]);
	var_0[1] = Object("conWeaponShop2");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory5NW",var_0[0]);
	var_0[1] = Object("warWeaponShop2");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory5SW",var_0[0]);
	var_0[1] = Object("warArmorShop2");
	MoveWaypoint(var_0[0],GetObjectX(var_0[1]),GetObjectY(var_0[1]));
	CreateObject("MovableStatueVictory5SW",var_0[0]);
}
void initializeMonsterRoute() {
    int i;

    //while(var_0 < 9) {
    for (i = 0 ; i < 9 ; i += 1)
    {
        Gvar_14[i] = Object("redWay" + IntToString(i + 1));
        Gvar_15[i] = Object("redWayA" + IntToString(i + 1));
        Gvar_16[i] = Object("blueWay" + IntToString(i + 1));
        Gvar_17[i] = Object("blueWayA" + IntToString(i + 1));
    }
}

void spawnGuardTower() {
    int i = 0; //1 - 10: red, 11 - 20: blue

    while (i < 20)
    {
        GUARDIAN[i + 20] = Waypoint("guardUnitWP" + IntToString(i + 1));
        GUARDIAN[i] = CreateObject("StrongWizardWhite", GUARDIAN[i + 20]);
        SetUnitMaxHealth(GUARDIAN[i], 3000);
        Enchant(GUARDIAN[i], "ENCHANT_ANCHORED", 0.0);
        Enchant(GUARDIAN[i], "ENCHANT_FREEZE", 0.0);
        //propertys
        UnitLinkBinScript(GUARDIAN[i], StrongWizardWhiteBinTable());
        SetCallback(GUARDIAN[i], 3, lookForEnemyForLich);
        //end
        SetUnitScanRange(GUARDIAN[i], 350.0);
        AggressionLevel(GUARDIAN[i], 1.0);
        if (i < 10)
            SetOwner(TeamFlag[2], GUARDIAN[i]);
        else
            SetOwner(TeamFlag[3], GUARDIAN[i]);
        i += 1;
    }
    for(i = 0; i < 4; i += 1) {
        SetCallback(GUARDIAN[i], 5, setDeathLeftRedGuardUnit);
        SetCallback(GUARDIAN[i + 4], 5, setDeathRightRedGuardUnit);
        SetCallback(GUARDIAN[i + 10], 5, setDeathLeftBlueGuardUnit);
        SetCallback(GUARDIAN[i + 14], 5, setDeathRightBlueGuardUnit);
    }
    FrameTimer(1, loopGuardUnits);
}

void unsetFinalBossGodMod(int teamId)
{
    if (teamId == 0)
    {
        Frozen(TeamFlag[2], 0);
        LookWithAngle(TeamFlag[2] + 1, 1);
    }
    else
    {
        Frozen(TeamFlag[3], 0);
        LookWithAngle(TeamFlag[3] + 1, 1);
    }
    int execOnce;

    if (!execOnce)
    {
        FrameTimer(1, loopHorrendousStat);
        execOnce = 1;
    }
}
void unsetGodModForObelisk(int index)
{
    int var_0 = index + 5;
    string teamName, position;
    
    if (index < 10)
        teamName = "RED";
    else
        teamName = "BLUE";
    if (index == 5 || index == 15)
        position = "RIGHT";
    else
        position = "LEFT";

    while(index < var_0) {
        EnchantOff(GUARD_TOWER[index], "ENCHANT_INVULNERABLE");
        index ++;
    }
    if (MapLang)
        UniPrintToAll(teamName + "Team " + position + " Part Generator invincibility has now been disabled.");
    else
        UniPrintToAll(teamName + "팀 의 " + position + " 소환 오벨리스크의 무적상태가 해제 되었습니다");
}
void setDeathLeftRedGuardUnit()
{
    int count;

    if (++count == 4) {
        unsetGodModForObelisk(0);
    }
    else
    {
        if (MapLang)
            UniPrintToAll("The Red Team's Guardian God (Lich) has just been shot down by an enemy");
        else
            UniPrintToAll(" 방금 Red 팀의 수호신(리치) 이 적에게 격추 되었습니다 .");
    }
}
void setDeathRightRedGuardUnit()
{
    int count;

    if (++count == 4) {
        unsetGodModForObelisk(5);
    }
    else
    {
        if (MapLang)
            UniPrintToAll("The Red Team's Guardian God (Lich) has just been shot down by an enemy");
        else
            UniPrintToAll(" 방금 Red 팀의 수호신(리치) 이 적에게 격추 되었습니다 .");
    }
}

void setDeathLeftBlueGuardUnit() {
    int count;
    
    if (++count == 4)
    {
        unsetGodModForObelisk(10);
    }
    else
    {
        if (MapLang)
            UniPrintToAll("The Blue Team's Guardian God (Lich) has just been shot down by an enemy");
        else
            UniPrintToAll(" 방금 Blue 팀의 수호신(리치) 이 적에게 격추 되었습니다 .");
    }
}
void setDeathRightBlueGuardUnit() {
    int count;
    
    if (++count == 4)
    {
        unsetGodModForObelisk(15);
    }
    else
    {
        if (MapLang)
            UniPrintToAll("The Blue Team's Guardian God (Lich) has just been shot down by an enemy");
        else
            UniPrintToAll(" 방금 Blue 팀의 수호신(리치) 이 적에게 격추 되었습니다 .");
    }
}

void lookForEnemyForLich()
{
    if (CurrentHealth(SELF))
    {
        if (CurrentHealth(OTHER) && IsVisibleTo(OTHER, SELF))
        {
            Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, SELF, 25, 2);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.4);
        AggressionLevel(SELF, 1.0);
    }
}

void windbooster(int arg_0) {
    int var_0[10];
    float var_1[10];
    float var_2[10];
    int var_3;

    if (CurrentHealth(player[arg_0]) && var_0[arg_0] < 7)
    {
        if (!var_0[arg_0])
        {
            var_1[arg_0] = UnitAngleCos(player[arg_0], 1.0);
            var_2[arg_0] = UnitAngleSin(player[arg_0], 1.0);
            Enchant(player[arg_0], "ENCHANT_FREEZE", 0.0);
            Effect("RICOCHET", GetObjectX(player[arg_0]), GetObjectY(player[arg_0]), 0.0, 0.0);
        }
        MoveWaypoint(107, GetObjectX(player[arg_0]) - var_1[arg_0], GetObjectY(player[arg_0]) - var_2[arg_0]);
        var_3 = CreateObject("ShopkeeperConjurerRealm", 107);
        LookWithAngle(var_3, arg_0);
        Frozen(var_3, 1);
        SetCallback(var_3, 9, windTouched);
        DeleteObjectTimer(var_3, 1);
        Effect("YELLOW_SPARKS", GetWaypointX(107), GetWaypointY(107), 0.0, 0.0);
        AudioEvent("FirewalkOn", 107);
        var_0[arg_0] ++;
        FrameTimerWithArg(1, arg_0, windbooster);
    }
    else
    {
        EnchantOff(player[arg_0], "ENCHANT_FREEZE");
        var_0[arg_0] = 0;
    }
}

void windTouched()
{
    int plr = GetDirection(SELF);

    if (IsAttackedBy(OTHER, player[plr]) && CurrentHealth(OTHER) > 0 && !HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC"))
    {
        Enchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC", 0.5);
        Damage(OTHER, player[plr], 35, 14);
    }
}

void getStraightFire(int arg_0) {
    int var_0[10];
    float pos_x[10];
    float pos_y[10];
    int var_1;

    if (CurrentHealth(player[arg_0]) && var_0[arg_0] < 30)
    {
        if (!var_0[arg_0]) {
            pos_x[arg_0] = UnitAngleCos(player[arg_0], 20.0);
            pos_y[arg_0] = UnitAngleSin(player[arg_0], 20.0);
            MoveWaypoint(72 + arg_0, GetObjectX(player[arg_0]) + pos_x[arg_0], GetObjectY(player[arg_0]) + pos_y[arg_0]);
        }
        MoveWaypoint(arg_0 + 72, GetWaypointX(arg_0 + 72) + pos_x[arg_0], GetWaypointY(arg_0 + 72) + pos_y[arg_0]);
        if (checkMapBoundary(arg_0 + 72))
        {
            var_1 = CreateObject("CarnivorousPlant", arg_0 + 72);
            LookWithAngle(var_1, arg_0);
            ObjectOff(var_1);
            Damage(var_1, 0, 999, 9);
            Frozen(var_1, 1);
            SetCallback(var_1, 9, deathTouch);
            DeleteObjectTimer(var_1, 1);
            DeleteObjectTimer(CreateObject("MediumFireBoom", arg_0 + 72), 5);
            AudioEvent("HammerMissing", arg_0 + 72);
            var_0[arg_0] ++;
        }
        else
            var_0[arg_0] = 255;
        FrameTimerWithArg(1, arg_0, getStraightFire);
    }
    else
        var_0[arg_0] = 0;
}

int checkMapBoundary(int arg_0) {
    float var_0 = GetWaypointX(arg_0);
    float var_1 = GetWaypointY(arg_0);

    if (var_0 > 100.0 && var_1 > 100.0 && var_0 < 5532.0 && var_1 < 5532.0)
        return 1;
    else
        return 0;
}
void deathTouch() {
    int var_0 = GetDirection(SELF);

    if (IsAttackedBy(OTHER, player[var_0]) && !HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC") && CurrentHealth(player[var_0]) > 0) {
        Damage(OTHER, player[var_0], 125, 14);
        Enchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC", 0.5);
    }
}
void loopGuardUnits() {
    int i;

    for (i = 19 ; i >= 0 ; i --)
    {
        if (CurrentHealth(GUARDIAN[i]))
        {
            if (Distance(GetObjectX(GUARDIAN[i]), GetObjectY(GUARDIAN[i]), GetWaypointX(GUARDIAN[i + 20]), GetWaypointY(GUARDIAN[i + 20])) > 20.0)
                MoveObject(GUARDIAN[i], GetWaypointX(GUARDIAN[i + 20]), GetWaypointY(GUARDIAN[i + 20]));
        }
    }
    FrameTimer(3, loopGuardUnits);
}

void HarpoonCollide()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner) && CurrentHealth(owner))
    {
        Damage(OTHER, owner, 100, 0);
        DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(OTHER), GetObjectY(OTHER)), 18);
        Effect("SENTRY_RAY", GetObjectX(owner), GetObjectY(owner), GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 285);
    }
    Delete(SELF);
}

void WarHarpoonHandler(int sOwner, int sCur)
{
    SetUnitCallbackOnCollide(sCur, HarpoonCollide);
    Enchant(sCur, "ENCHANT_SHOCK", 0.0);
}

void DetectedSpecficIndex(int curId)
{
    if (IsMissileUnit(curId))
    {
        int owner = GetOwner(curId);
        int thingID = GetUnitThingID(curId);

        if (thingID == 706)
            DeathballEvent(owner, curId);
        else if (thingID == 709)
            ShotMagicMissile(owner, curId);
        else if (thingID == 531)
            ShotCrossBow(owner, curId);
        else if (thingID == 526)
            WarHarpoonHandler(owner, curId);
    }
}

void LoopSearchIndex()
{
    int curId, tempId;

    if (GetMemory(0x750710))
    {
        tempId = GetMemory(GetMemory(0x750710) + 0x2c);
        if (curId)
        {
            while (curId < tempId)
            {
                curId += 1;
                DetectedSpecficIndex(curId);
            }
        }
        else
            curId = tempId;
    }
    FrameTimer(1, LoopSearchIndex);
}

int CheckOwner(int unit) {
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsOwnedBy(unit, player[i]))
            return i;
    }
    return -1;
}

void DeathballEvent(int owner, int cur)
{
    int mis;

    if (CurrentHealth(owner))
    {
        mis = CreateObjectAt("titanFireball", GetObjectX(cur), GetObjectY(cur));
        SetOwner(owner, mis);
        LookWithAngle(mis, GetDirection(owner));
        PushObject(mis, 30.0, GetObjectX(owner), GetObjectY(owner));
    }
    Delete(cur);
}

void ShotCrossBow(int owner, int cur)
{
    if (CurrentHealth(owner))
    {
        Enchant(cur, "ENCHANT_HASTED", 0.0);
        MoveObject(cur, GetObjectX(cur), GetObjectY(cur));
        PushObject(cur, -25.0, GetObjectX(owner), GetObjectY(owner));
        DeleteObjectTimer(cur, 20);
    }
    else
        Delete(cur);
}

void ShotMagicMissile(int owner, int cur)
{
    int ptr;

    if (CurrentHealth(owner))
    {
        ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(cur), GetObjectY(cur));
        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", owner, GetObjectX(owner) + UnitAngleCos(owner, 23.0), GetObjectY(owner) + UnitAngleSin(owner, 23.0));
        Delete(ptr);
        Delete(ptr + 2);
        Delete(ptr + 3);
        Delete(ptr + 4);
    }
    Delete(cur);
}

void castBlink(int arg_0)
{
    if (MapLang)
        UniChatMessage(player[arg_0], "We are preparing to move the space to the starting position", 90);
    else
        UniChatMessage(player[arg_0], "시작위치로 공간이동을 준비중 입니다 ...", 90);
    MoveWaypoint(93 + arg_0, GetObjectX(player[arg_0]), GetObjectY(player[arg_0]));
    AudioEvent("BlindOff", 93 + arg_0);
    FrameTimerWithArg(1, (30 * 256) + arg_0, blinkToHome);
}

void blinkToHome(int arg_0) {
    int var_0 = arg_0 / 256; //cooldown
    int var_1 = arg_0 - (256 * var_0); //player_index
    int var_2[10];
    float var_3;

    if (CurrentHealth(player[var_1]) > 0 && var_0 > 0) {
        if (var_0 == 1) {
            playerTeleport(player[var_1]);
            resetBlink(var_2[var_1]);
        }
        else {
            if (!IsObjectOn(var_2[var_1])) {
                var_2[var_1] = CreateObject("VortexSource", 93 + var_1);
            }
            var_3 = Distance(GetWaypointX(var_1 + 93), GetWaypointY(var_1 + 93), GetObjectX(player[var_1]), GetObjectY(player[var_1]));
            if (var_3 < 50.0) {
                var_0 --;
                FrameTimerWithArg(1, (var_0 * 256) + var_1, blinkToHome);
            }
            else
                resetBlink(var_2[var_1]);
        }
    }
    else
        resetBlink(var_2[var_1]);
}

void resetBlink(int arg_0) {
    if (IsObjectOn(arg_0))
        Delete(arg_0);
}

void setLimitRun()
{
    int var_0;

    var_0 = CreateObject("maiden", 106);
    SetCallback(var_0, 9, limitRun);
    Frozen(var_0, 1);
    Frozen(CreateObject("FishSmall", 106), 1);
}

void loopGuardTower()
{
    int i;

    if (i == 20) i = 0;

    if (CurrentHealth(GUARD_TOWER[i]))
    {
        GuardTowerSeeEnemy(GUARD_TOWER[i]);
    }
    i ++;
}

void GuardTowerSeeEnemy(int unit)
{
    int i, mis;

    for (i = 0 ; i < 10 ; i += 1)
    {
        if (!IsVisibleTo(unit, player[i]))
            continue;
        if (!IsAttackedBy(unit, player[i]))
            continue;
        if (CurrentHealth(player[i]))
        {
            mis = CreateObjectAt("DeathBallFragment", GetObjectX(unit) - UnitRatioX(unit, player[i], 40.0), GetObjectY(unit) - UnitRatioY(unit, player[i], 40.0));
            SetOwner(TeamFlag[checkTeam(unit) + 1], mis);
            PushObject(mis, -47.0, GetObjectX(player[i]), GetObjectY(player[i]));
        }
    }
}

void limitRun()
{
    int subUnit = GetTrigger() + 1;

    Effect("RICOCHET", LocationX(104), LocationY(104), 0.0, 0.0);
    loopGuardTower();
    MoveObject(subUnit, GetObjectX(SELF), GetObjectY(SELF));
}

void strWinnerTeam()
{
	int arr[16];
	string name = "drainManaOrb";
	int i = 0;
	arr[0] = 2116542528; arr[1] = 525380; arr[2] = 1082687625; arr[3] = 334569485; arr[4] = 604376098; arr[5] = 279556; arr[6] = 142641656; arr[7] = 1895907313; arr[8] = 536879663; arr[9] = 71303232; 
	arr[10] = 2131232894; arr[11] = 17833985; arr[12] = 69345346; arr[13] = 1141399492; arr[14] = 134249992; arr[15] = 510; 
	while(i < 16)
	{
		drawstrWinnerTeam(arr[i], name);
		i += 1;
	}
}

void drawstrWinnerTeam(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(105);
		pos_y = GetWaypointY(105);
	}
	for (i = 1 ; i > 0 && count < 496 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 105);
		if (count % 44 == 43)
			MoveWaypoint(105, GetWaypointX(105) - 86.000000, GetWaypointY(105) + 2.000000);
		else
			MoveWaypoint(105, GetWaypointX(105) + 2.000000, GetWaypointY(105));
		count += 1;
	}
	if (count >= 496)
	{
		count = 0;
		MoveWaypoint(105, pos_x, pos_y);
	}
}

int CheckWall(int num)
{
    int unit[36];
    int i;

    if (num == -1)
    {
        for (i = 0 ; i < 36 ; i ++)
            unit[i] = CreateObject("InvisibleLightBlueHigh", i + 119);
        return 0;
    }
    return unit[num];
}

void loopCheckWall()
{
    int i;

    if (i == 6)
        i = 0;
    if (!IsVisibleTo(CheckWall(i), CheckWall(i + 1)))
        ForceCastWall();
    else if (!IsVisibleTo(CheckWall(i + 7), CheckWall(i + 8)))
        ForceCastWall();
    else if (!IsVisibleTo(CheckWall(i + 14), CheckWall(i + 15)))
        ForceCastWall();
    else if (!IsVisibleTo(CheckWall(i + 21), CheckWall(i + 22)))
        ForceCastWall();
    else if (i < 4 && !IsVisibleTo(CheckWall(i * 2 + 28), CheckWall(i * 2 + 29)))
        ForceCastWall();
    i ++;
    FrameTimer(3, loopCheckWall);
}

void ForceCastWall()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]) && MaxHealth(player[i]) == 75)
            CastSpellObjectLocation("SPELL_WALL", player[i], 0.0, 0.0);
    }
}

void GreenSparkFx(float xProfile, float yProfile)
{
    int fxTarget = CreateObjectAt("MonsterGenerator", xProfile, yProfile);

    Damage(fxTarget, 0, 1, -1);
    Delete(fxTarget);
}

void RespawnTeleportBook(int posUnit)
{
    if (IsObjectOn(posUnit))
    {
        GreenSparkFx(GetObjectX(posUnit), GetObjectY(posUnit));
        TeleportBookCreate(posUnit);
        Delete(posUnit);
    }
}

void TeleportBookPickEvent()
{
    if (IsObjectOn(SELF))
    {
        SecondTimerWithArg(5, CreateObjectAt("PlayerWaypoint", GetObjectX(SELF), GetObjectY(SELF)), RespawnTeleportBook);
    }
}

void PlayerTeleportAtHome(int owner)
{
    int team = checkTeam(owner);

    if (team)
    {
        if (team == 1)
            MoveObject(owner, GetWaypointX(168), GetWaypointY(168));
        else if (team == 2)
            MoveObject(owner, GetWaypointX(167), GetWaypointY(167));
    }
}

void TeleportingProcess(int point)
{
    int owner = GetOwner(point), count = GetDirection(point);

    while (IsObjectOn(point))
    {
        if (CurrentHealth(owner))
        {
            if (count)
            {
                if (DistanceUnitToUnit(point, owner) < 23.0)
                {
                    LookWithAngle(point, count - 1);
                    FrameTimerWithArg(1, point, TeleportingProcess);
                    break;
                }
            }
            else
            {
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                PlayerTeleportAtHome(owner);
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
                DeleteObjectTimer(CreateObjectAt("BlueRain", GetObjectX(owner), GetObjectY(owner)), 30);
            }
            EnchantOff(owner, "ENCHANT_CROWN");
        }
        Delete(point);
        Delete(point + 1);
        break;
    }
}

void TeleportBookUse()
{
    int point;

    if (CurrentHealth(OTHER))
    {
        SecondTimerWithArg(5, GetTrigger() + 1, RespawnTeleportBook);
        Delete(SELF);
        if (HasEnchant(OTHER, "ENCHANT_CROWN")) return;
        point = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(OTHER, CreateObjectAt("VortexSource", GetObjectX(point), GetObjectY(point)) - 1);
        LookWithAngle(point, 48);
        FrameTimerWithArg(3, point, TeleportingProcess);
        GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("LongBellsDown", 1);
        Enchant(OTHER, "ENCHANT_CROWN", 4.0);
        if (MapLang)
            UniPrint(OTHER, "Preparing to teleport... Move your character to cancel");
        else
            UniPrint(OTHER, "공간이동을 준비중입니다... 취소하려면 캐릭터를 움직이세요");
    }
}

int TeleportBookCreate(int posUnit)
{
    int tBook = CreateObjectAt("BlackBook1", GetObjectX(posUnit), GetObjectY(posUnit));

    CreateObjectAt("InvisibleLightBlueLow", GetObjectX(tBook), GetObjectY(tBook));
    RegistItemPickupCallback(tBook, TeleportBookPickEvent);
    SetUnitCallbackOnUseItem(tBook, TeleportBookUse);
    return tBook;
}

void PlacingTeleportBook()
{
    TeleportBookCreate(CreateObject("BlueSummons", 169));
    TeleportBookCreate(CreateObject("BlueSummons", 170));
    TeleportBookCreate(CreateObject("BlueSummons", 171));
    TeleportBookCreate(CreateObject("BlueSummons", 172));
    TeleportBookCreate(CreateObject("BlueSummons", 173));
    TeleportBookCreate(CreateObject("BlueSummons", 174));
    TeleportBookCreate(CreateObject("BlueSummons", 175));
    TeleportBookCreate(CreateObject("BlueSummons", 176));
}

void PicketDescription()
{
    string pMsg[] = {
        "돈을 모아 상인들에게서 아이템을 구입하여 캐릭터를 강하게 만드세요!",
        "검은책을 사용하면 이곳으로 순간이동 할 수 있어요!",
        "Accumulate money and buy things from merchants to make your character stronger",
        "If you use a black book, you can teleport here!"};
    string *signMsg = SToInt( &pMsg ) + (MapLang << 3);

    RegistSignMessage(Object("SetGameSign1"), signMsg[0]);
    RegistSignMessage(Object("SetGameSign2"), signMsg[0]);
    RegistSignMessage(Object("SetGameSign3"), signMsg[1]);
    RegistSignMessage(Object("SetGameSign4"), signMsg[1]);
}

void ForceOfNatureCollide()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 320, 14);
        Delete(SELF);
    }
    else if (!GetCaller())
        Delete(SELF);
}

int ForceOfNature(int sOwner)
{
    int mis = CreateObjectAt("DeathBall", GetObjectX(sOwner) + UnitAngleCos(sOwner, 19.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 19.0));
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetUnitCallbackOnCollide(mis, ForceOfNatureCollide);
    SetOwner(sOwner, mis);
    return mis;
}

void OblivionUseHandler()
{
    if (HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
        return;
    else if (CurrentHealth(OTHER))
    {
        PushObject(ForceOfNature(OTHER), 20.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 204);
        Enchant(OTHER, "ENCHANT_ETHEREAL", 0.9);
    }
}

void DelayGiveToOwner(int sTarget)
{
    int sOwner = GetOwner(sTarget);

    if (IsObjectOn(sTarget) && CurrentHealth(sOwner))
        Pickup(sOwner, sTarget);
    else
        Delete(sTarget);
}

int OblivionOrbSummon(float sX, float sY, int sOwner)
{
    int oblivionStaff = CreateObjectAt("OblivionOrb", sX, sY);
    
    SetUnitCallbackOnUseItem(oblivionStaff, OblivionUseHandler);
    SetOwner(sOwner, oblivionStaff);
    FrameTimerWithArg(1, oblivionStaff, DelayGiveToOwner);
    DisableOblivionItemPickupEvent(oblivionStaff);
    SetItemPropertyAllowAllDrop(oblivionStaff);
    return oblivionStaff;
}

void OblivionShopDesc()
{
    if (MapLang)
        UniPrint(OTHER, "Click the 'Yes' button to purchase the magic wand. This work requires 30,000 gold");
    else
        UniPrint(OTHER, "마법 지팡이를 구입하려면 '예' 버튼을 누르세요. 이 작업은 3만 골드가 필요합니다");
    TellStoryUnitName("oAo", "War06a:HorrendousPride", "마법지팡이 구입\n3만골드");
}

void OblivionShopTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    if (GetGold(OTHER) >= 30000)
    {
        OblivionOrbSummon(GetObjectX(OTHER), GetObjectY(OTHER), OTHER);
        GreenSparkFx(GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, 505);
        ChangeGold(OTHER, -30000);
        if (MapLang)
            UniPrint(OTHER, "I bought a magic wand!");
        else
            UniPrint(OTHER, "마법지팡이를 구입했습니다 !");
    }
    else
        UniPrint(OTHER, "잔액이 부족합니다");
}

int PutOblivionShop(float sX, float sY)
{
    int unit = CreateObjectAt("Wizard", sX, sY);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    SetDialog(unit, "YESNO", OblivionShopDesc, OblivionShopTrade);
    return unit;
}

void StaffShopPlace()
{
    PutOblivionShop(LocationX(177), LocationY(177));
    PutOblivionShop(LocationX(178), LocationY(178));
}

void SelectLanguage(int sUnit)
{
    int team = checkTeam(sUnit), i;

    if (team)
    {
        for (i = 0 ; i < 4 ; i ++)
        {
            WallOpen(Wall(29 - i, 25 + i));
            WallOpen(Wall(219 - i, 215 + i));
        }
        team --;
        MoveObject(sUnit, GetWaypointX(168 - team), GetWaypointY(168 - team));
        FrameTimer(1, MainGameStart);
    }
}

void LanguagePickKor()
{
    UniPrintToAll("한글 게임모드로 선택되었습니다");
    SelectLanguage(OTHER);
}

void LanguagePickUs()
{
    MapLang = 1;
    UniPrintToAll("Selected English game mode play");
    SelectLanguage(OTHER);
}

void HostplayerSelectLanguage()
{
    int hostPlayer = GetHost();

    if (checkTeam(hostPlayer))
    {
        MoveObject(hostPlayer, GetWaypointX(179), GetWaypointY(179));
        UniPrint(GetHost(), "언어를 선택하세요");
        UniPrint(GetHost(), "Please select map language");
        RegistSignMessage(Object("LangSign1"), "Language: Korea");
        RegistSignMessage(Object("LangSign2"), "Language: US");
    }
    else
    {
        UniPrintToAll("The team has not been created. Create a team and reload this map");
        UniPrintToAll("팀이 생성되지 않았어요. 팀을 생성하신 후 이 지도를 다시 로드하세요");
    }
}