
#include "generate_utils.h"
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"

void GRPDump_cent1_output(){}

void initializeCentAnimation(int imgVec)
{
    int *frames, *sizes, thingId=OBJ_TREASURE_CHEST;
    int xyInc[]={0,-3};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_cent1_output)+4, thingId, xyInc, &frames, &sizes);
    AppendImageFrame(imgVec,frames[0], 112596);
    AppendImageFrame(imgVec,frames[0], 112597);
    AppendImageFrame(imgVec,frames[1], 112598);
    AppendImageFrame(imgVec,frames[2], 112599);
    AppendImageFrame(imgVec,frames[3], 112600);
    AppendImageFrame(imgVec,frames[4], 112601);
    AppendImageFrame(imgVec,frames[5], 112602);
    AppendImageFrame(imgVec,frames[0], 112603);
}

void GRPDump_LowsRPGOutput(){}
// #define LOWSRPG_OUTPUT_IMAGE_ID 
void initializeImageFrameLowsRPG(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WIZARD_RED;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_LowsRPGOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,3,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,3,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, 115913, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(115913,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,0);
    AnimFrameAssign8Directions(5, 115846, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10, 115846+8, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15, 115846+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(115846, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions( 115846+8, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions( 115846+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
}

void GRPDump_FemaleBeachGirl(){}

#define FEMALE_BEACH_GIRL_IMAGE_ID 129717

void initializeImageFrameBeachGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_FemaleBeachGirl)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameAssign8Directions(0, FEMALE_BEACH_GIRL_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  FEMALE_BEACH_GIRL_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10, FEMALE_BEACH_GIRL_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15, FEMALE_BEACH_GIRL_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20, FEMALE_BEACH_GIRL_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,FEMALE_BEACH_GIRL_IMAGE_ID+40,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_SlimeDumpOutput(){}

#define SLIME_DUMP_OUTPUT_IMAGE_START_AT 130005

void initializeImageSlimeOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SlimeDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,5,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign4Directions(6,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign4Directions(9,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign4Directions(12,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);

    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_AckMechanical(){}

#define ACK_MECHANICAL_IMAGE_START_AT 124381

void initializeImageACKMECHANICAL(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOLF;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_AckMechanical)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,2,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    ACK_MECHANICAL_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);

    AnimFrameAssign8Directions(5,    ACK_MECHANICAL_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10,    ACK_MECHANICAL_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15,    ACK_MECHANICAL_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20,    ACK_MECHANICAL_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(25,    ACK_MECHANICAL_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(30,    ACK_MECHANICAL_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(35,    ACK_MECHANICAL_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(40,    ACK_MECHANICAL_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(        ACK_MECHANICAL_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(         ACK_MECHANICAL_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameAssign8Directions(45,    ACK_MECHANICAL_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssign8Directions(50,    ACK_MECHANICAL_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
}

void GRPDump_BabyOniOutput(){}

#define BABY_ONI_BASE_IMAGE_ID 121568
void initializeImageFrameBabyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GIANT_LEECH;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BabyOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign4Directions(0, BABY_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BABY_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BABY_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BABY_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_SC1Zealot(){}

#define PROTOSS_ZEALOT_IMAGE_START_AT 122643

void initializeImageFrameZealot(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SHADE;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SC1Zealot)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_IDLE,2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,7);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_WALK,2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,7);
    ChangeMonsterSpriteImageUnkValue(thingId,IMAGE_SPRITE_MON_ACTION_RUN, 2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,5);
    ChangeMonsterSpriteImageUnkValue(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign8Directions(0,  PROTOSS_ZEALOT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  PROTOSS_ZEALOT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10, PROTOSS_ZEALOT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15, PROTOSS_ZEALOT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20, PROTOSS_ZEALOT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(25, PROTOSS_ZEALOT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(30, PROTOSS_ZEALOT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(35, PROTOSS_ZEALOT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions(      PROTOSS_ZEALOT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(      PROTOSS_ZEALOT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(      PROTOSS_ZEALOT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(      PROTOSS_ZEALOT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameCopy8Directions(      PROTOSS_ZEALOT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN,4);
    AnimFrameCopy8Directions(      PROTOSS_ZEALOT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN,5);
    AnimFrameCopy8Directions(      PROTOSS_ZEALOT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_RUN,6);
    AnimFrameAssign8Directions(40, PROTOSS_ZEALOT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
    AnimFrameAssign8Directions(45, PROTOSS_ZEALOT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameAssign8Directions(50, PROTOSS_ZEALOT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,2);
    AnimFrameAssign8Directions(55, PROTOSS_ZEALOT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,3);
    AnimFrameAssign8Directions(60, PROTOSS_ZEALOT_IMAGE_START_AT+96, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,4);
}

void GRPDump_ToothOniPurpleOutput(){}

#define TOOTH_ONI_PURPLE_BASE_IMAGE_ID 120829
void initializeImageFrameToothOniPurple(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_ALBINO_SPIDER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_ToothOniPurpleOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign4Directions(0, TOOTH_ONI_PURPLE_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, TOOTH_ONI_PURPLE_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, TOOTH_ONI_PURPLE_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     TOOTH_ONI_PURPLE_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void initTextStrings(int imgvec)
{
    short wMessage1[32];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("여기 부터는 몬스터 사냥터입니다"), wMessage1);
    AppendTextSprite(imgvec, 0xF880, wMessage1, 112988);
    AppendTextSprite(imgvec, 0x019F, wMessage1, 112989);
    AppendTextSprite(imgvec, 0x5F02, wMessage1, 112990);
    AppendTextSprite(imgvec, 0xE0BA, wMessage1, 112991);
}

#include "libs/logging.h"
void initializeResources(int imgvec)
{
    initTextStrings(imgvec);
    WriteLog("1");
    initializeCentAnimation(imgvec);
    WriteLog("1");
    initializeImageFrameLowsRPG(imgvec);
    // WriteLog("1");
    initializeImageFrameBeachGirl(imgvec);
    // WriteLog("1");
    initializeImageSlimeOutput(imgvec);
    // WriteLog("1");
    initializeImageACKMECHANICAL(imgvec);
    // WriteLog("1");
    initializeImageFrameBabyOni(imgvec);
    // WriteLog("1");
    initializeImageFrameZealot(imgvec);
    // WriteLog("1");
    initializeImageFrameToothOniPurple(imgvec);
    // WriteLog("1");
    initializeImageHealthBar(imgvec);
}

void GRPDumpTileSet(){}

void initializeCustomTileset(int imgVector){
    int *pack=GetScrCodeField(GRPDumpTileSet)+4;
    int *off=&pack[1];
    int frames[]={pack+off[0],pack+off[1], pack+off[2],pack+off[3],pack+off[4],pack+off[5],};

    AppendImageFrame(imgVector, frames[0], 12195);
    AppendImageFrame(imgVector, frames[1], 12196);
    AppendImageFrame(imgVector, frames[2], 12197);
    AppendImageFrame(imgVector, frames[3], 12198);
    AppendImageFrame(imgVector, frames[4], 12199);
    AppendImageFrame(imgVector, frames[5], 12200);
    AppendImageFrame(imgVector, frames[0], 12201);
    AppendImageFrame(imgVector, frames[1], 12202);
    AppendImageFrame(imgVector, frames[2], 12203);
    ForceUpdateTileChanged();
}

void OnInitializeResources()
{
    int imgVec = CreateImageVector(1024);

    initializeResources(imgVec);
    EnableOblivionTooltipDetail();
    initializeCustomTileset(imgVec);
    DoImageDataExchange(imgVec);
}
