
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/networkEx.h"
#include "libs/fxeffect.h"
#include "libs/waypoint.h"

int PlayerRespawnMark(int idx)
{
    int ptr, k=32;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 11) + 1;
        Delete(ptr - 1);
        while (--k>=0)
            ObjectOff(CreateObject("InvisibleLightBlueHigh", 11));
    }
    return ptr + idx;
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

int GetPlayerUnit(int pIndex){
    int *pInfo=0x62f9e0 + (0x12dc*pIndex);

    if (!pInfo[0])
        return 0;

    int *ptr=pInfo[0];
    return ptr[11];
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectAt("Hecubah", 5500.0, 100.0);
        Frozen(unit, 1);
    }
    return unit;
}

void CustomMeleeAttackCode(char *bin, int fn)
{
    char code[]={
        0x56, 0x8B, 0x44, 0x24, 0x08, 0x6A, 0x00, 0x50, 0xB8, 0x40, 0x94, 0x54, 0x00, 0xFF, 
        0xD0, 0x8B, 0xF0, 0x58, 0x89, 0x04, 0x24, 0x56, 0x8B, 0x80, 0xEC, 0x02, 0x00, 0x00, 
        0x8B, 0x80, 0xE4, 0x01, 0x00, 0x00, 0x8B, 0x80, 0xD0, 0x00, 0x00, 0x00, 0x50, 0xB8, 
        0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0x5E, 0xC3
    };
    
    int *pStrike = &bin[0xEC];

    pStrike[0] = code;
    int *hack = &bin[0x94];

    hack[15]=fn;
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196;
		arr[17] = 20; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 4; arr[27] = 1; arr[28] = 1108082688; arr[29] = 100; 
		arr[30] = 1092616192; arr[31] = 0; arr[32] = 9; arr[33] = 17;
        arr[57] = 5548288;arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 90; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = arr;
	}
	return link;
}

// int AirshipCaptainBinTable()
// {
// 	int arr[62], link;
// 	if (!link)
// 	{
// 		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
// 		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
// 		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
// 		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
// 		arr[31] = 8; arr[32] = 12; arr[33] = 20;
// 		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
// 		link = arr;
// 	}
// 	return link;
// }

int BomberGreenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339074; arr[1] = 1917284965; arr[2] = 7234917;
		arr[17] = 20; arr[19] = 80; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[31] = 4; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
        arr[53] = 1128792064; 
		arr[55] = 11; arr[56] = 17; arr[57] = 5548112; arr[58] = 5545344; arr[59] = 5543344; 
		link = arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801;
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; 
		arr[58] = 5546320; arr[59] = 5542784;
		link =arr;
	}
	return link;
}

int LatestUnitPtr()
{
    int *latestunit = 0x750710;

    return latestunit[0];
}

void DisableObject(int unit)
{
    if (IsObjectOn(unit))
        ObjectOff(unit);
}

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr = GetMemory(0x750710), k;

    SetMemory(ptr + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(7));
    UnitLinkBinScript(unit, MaidenBinTable());

    return unit;
}

int GetListPrev(int cur)
{
    return GetOwner(cur);
}

int GetListNext(int cur)
{
    return ToInt(GetObjectZ(cur));
}

void SetListPrev(int cur, int tg)
{
    SetOwner(tg, cur);
}

void SetListNext(int cur, int tg)
{
    Raise(cur, ToFloat(tg));
}

int ImportSetPlayerActionFunc()
{
    int *codestream;

    if (codestream == NULLPTR)
    {
        int codebyte[] = {0x4FA02068, 0x72506800, 0x14FF0050, 0x54FF5024, 0xFF500424,
            0x830C2454, 0x90C310C4, 0x9090C390, 0xC03108C4, 0x909090C3};
        
        codestream = codebyte;
    }
    return codestream;
}

void SetPlayerActionFunc(int pUnit, int val)
{
    if (!IsPlayerUnit(pUnit))
        return;

    int *ptr = UnitToPtr(pUnit);

    if (ptr == NULLPTR)
        return;
    int *btbase = 0x5c308c;
    int *target = btbase[0x5a];
    int prev = target;

    target[0] = ImportSetPlayerActionFunc();
    Unused5a(ptr, val);
    target[0] = prev;
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuardedX(0x443f46,patch,sizeof(patch));
}

int DrawMagicIcon(float x, float y, short unitId)
{
    int unit = CreateObjectById(OBJ_AIRSHIP_BASKET_SHADOW, x, y);
    int *ptr = UnitToPtr(unit);

    ptr[1]= unitId;
    return unit;
}

void DrawGeometryRing(short orbType, float x, float y)
{
    int u=60;
    float speed = 2.3;

    while (--u>=0)
        LinearOrbMove(CreateObjectById(orbType, x,y), MathSine((u * 6) + 90, -12.0), MathSine(u * 6, -12.0), speed, 10);
}

void GreenLightningEffect(float x1, float y1, float x2, float y2)
{
    GreenLightningFx(FloatToInt(x1), FloatToInt(y1), FloatToInt(x2), FloatToInt(y2), 25);
}
void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}

// int ImportPlayerObserverModeEscape()
// {
//    //E8 4B 62 DB/ FF 50 E8 95/ 5A D9 FF 58/ 31 C0 C3 90

//    int code[] = {0xdb624be8, 0x95e850ff, 0x58ffd95a, 0x90c3c031};
//    char *ptr = code;
//    FixCallOpcode(ptr, 0x507250);
//    FixCallOpcode(ptr + 6, 0x4e6aa0);
//    return ptr;
// }

// void PlayerEscapeObserverMode(int plrUnit)
// {
//    if (IsPlayerUnit(plrUnit))
//    {
//       int ptr = UnitToPtr(plrUnit);
//       int pst = GetMemory(GetMemory(ptr + 0x2ec) + 0x114);
//       int temp = GetMemory(0x5c3108);

//       SetMemory(0x5c3108, ImportPlayerObserverModeEscape());
//       Unused1f(pst);
//       SetMemory(0x5c3108, temp);
//    }
// }

void RiseBlueSpark(float x, float y)
{
    int fx = CreateObjectById(OBJ_TELEPORT_WAKE, x, y);
    Frozen(fx, TRUE);
    UnitNoCollide(fx);
}

void onCollideInvisbleTelepo(){
    if (CurrentHealth(OTHER))
    {
        int dest=GetTrigger()+1;

        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
        MoveObject(OTHER, GetObjectX(dest),GetObjectY(dest));
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0,0.0);
    }
}

void DeleteInvisibleTelpo(int t){
    if (GetUnitThingID(t) == OBJ_STORM_CLOUD)
    {
        if (GetUnitThingID(t+2)==OBJ_TELEPORT_WAKE){
        Delete(t++);
        Delete(t++);
        Delete(t++);
        }
    }
}

int PlacingInvisibleTeleporting(short srcLoc, short destLoc){
    int s=CreateObjectById(OBJ_STORM_CLOUD, LocationX(srcLoc),LocationY(srcLoc));

    SetUnitCallbackOnCollide(s, onCollideInvisbleTelepo);
    SetUnitFlags(s, GetUnitFlags(s) ^ (UNIT_FLAG_NO_COLLIDE|UNIT_FLAG_NO_PUSH_CHARACTERS));
    Frozen(s, TRUE);

    DrawMagicIcon(LocationX(destLoc),LocationY(destLoc),OBJ_SPELL_ICONS);
    RiseBlueSpark(GetObjectX(s),GetObjectY(s));
    return s;
}

