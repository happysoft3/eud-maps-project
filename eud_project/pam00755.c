
#include "pam00755_gvar.h"
#include "pam00755_player.h"
#include "pam00755_mapSetting.h"

int m_player[MAX_PLAYER_COUNT];
int m_pFlags[MAX_PLAYER_COUNT];
int m_pLastItem[MAX_PLAYER_COUNT];

static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex]; }

static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user; }

static int GetPlayerFlags(int pIndex) //virtual
{ return m_pFlags[pIndex]; }

static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_pFlags[pIndex]=flags; }

static int GetPlayerItem(int pIndex) //virtual
{ return m_pLastItem[pIndex]; }

static void SetPlayerItem(int pIndex, int item) //virtual
{ m_pLastItem[pIndex]=item; }

int m_userRespawnMark[MAX_PLAYER_COUNT];

static int GetUserRespawnMark(int pIndex) //virtual
{ return m_userRespawnMark[pIndex];}
static void SetUserRespawnMark(int pIndex, int mark) //virtual
{ m_userRespawnMark[pIndex]=mark; }

void MapExit()
{
    MusicEvent();
    OnShutdownMap();
}

void MapInitialize()
{
    MusicEvent();
    OnEnterMap();
}

int m_firewayControl = TRUE;

static int FirewayControlPtr() //virtual
{ return &m_firewayControl; }

int m_fonTrapControl=75;

static int FonTrapControlPtr() //virtual
{ return &m_fonTrapControl; }

int m_sentryWallControl=TRUE;

static int SentryWallsControlPtr() //virtual
{ return &m_sentryWallControl; }

int m_mobElevators[4];

static void MonsterElevator(int n, int setTo, int *get) //virtual
{
    if (setTo)
        m_mobElevators[n]=setTo;
    if (get)
        get[0]=m_mobElevators[n];
}

int m_warBoss;

static int GetWarBoss() //virtual
{ return m_warBoss; }

static void SetWarBoss(int boss) //virtual
{ m_warBoss=boss; }

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    initDialog();
    initPopupMessages();
    InitializeCommonImage();
    FrameTimer(1, ClientProcLoop);
}

static void OnPlayerEntryMap(int pInfo)
{
    OnPlayerEntryMapProc(pInfo);
}
