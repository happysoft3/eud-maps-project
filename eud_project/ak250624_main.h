
#include "ak250624_player.h"
#include"ak250624_resource.h"
#include"ak250624_initscan.h"
#include "libs/coopteam.h"
#include"libs/winapi.h"
#include"libs/recovery.h"

int createObstacleWallReal(float x,float y){
    int wall =CreateObjectById(OBJ_LARGE_BLUE_FLAME,x,y);
    
    SetUnitCallbackOnCollide(wall,onWallCollide);
    return wall;
}

#define PATROL_BLOCK_UNIT 0
#define PATROL_BLOCK_COUNT 1
#define PATROL_BLOCK_XVECT 2
#define PATROL_BLOCK_YVECT 3
#define PATROL_BLOCK_INIT_VALUE 4
#define PATROL_BLOCK_MAX 5

void onBlockPatrolBackward(int *d){
    int b=d[PATROL_BLOCK_UNIT];
    if (ToInt(GetObjectX(b))){
        if (--d[PATROL_BLOCK_COUNT]>=0){
            PushTimerQueue(1,d,onBlockPatrolBackward);
            float *pv=&d[PATROL_BLOCK_XVECT];
            MoveObjectVector(b,-pv[0],-pv[1]);
            return;
        }
        d[PATROL_BLOCK_COUNT]=d[PATROL_BLOCK_INIT_VALUE];
        PushTimerQueue(1,d,onBlockPatrolForward);
        return;
    }
    FreeSmartMemEx(d);
}

void onBlockPatrolForward(int *d){
    int b=d[PATROL_BLOCK_UNIT];
    if (ToInt(GetObjectX(b))){
        if (--d[PATROL_BLOCK_COUNT]>=0){
            PushTimerQueue(1,d,onBlockPatrolForward);
            float *pv=&d[PATROL_BLOCK_XVECT];
            MoveObjectVector(b,pv[0],pv[1]);
            return;
        }
        d[PATROL_BLOCK_COUNT]=d[PATROL_BLOCK_INIT_VALUE];
        PushTimerQueue(1,d,onBlockPatrolBackward);
        return;
    }
    FreeSmartMemEx(d);
}

void placingPatrolBlock(short loc, short destLoc, float speed){
    int b=createObstacleWallReal(LocationX(loc),LocationY(loc));
    int *d;
    float points[]={LocationX(loc),LocationY(loc),LocationX(destLoc),LocationY(destLoc)},vect[2];
    float dist=Distance(points[0],points[1],points[2],points[3]);

    ComputePointRatio(&points[2],points,vect,speed);
    AllocSmartMemEx(PATROL_BLOCK_MAX*4,&d);
    d[PATROL_BLOCK_UNIT]=b;
    d[PATROL_BLOCK_INIT_VALUE]=FloatToInt( dist/speed );
    d[PATROL_BLOCK_COUNT]=d[PATROL_BLOCK_INIT_VALUE];
    d[PATROL_BLOCK_XVECT]=vect[0];
    d[PATROL_BLOCK_YVECT]=vect[1];
    PushTimerQueue(1,d,onBlockPatrolForward);
}

#define YOU_PROC_PINDEX 0
#define YOU_PROC_UNIT 1
#define YOU_PROC_MAX 2

void onYouProc(int *d){
    int you=d[YOU_PROC_UNIT];
    if (MaxHealth(you)){
        int owner=GetOwner(you);

        if (CurrentHealth(owner)){
            PushTimerQueue(1,d,onYouProc);
            int point[2];
            GetPlayerMouseXY(owner,&point[0],&point[1]);
            float fpoint[]={IntToFloat(point[0]), IntToFloat(point[1]),};
            float ypoint[]={GetObjectX(you),GetObjectY(you),};
            float ratio[]={0,0,};
            if (ToInt( Distance(fpoint[0],fpoint[1],ypoint[0],ypoint[1]) )){
                ComputePointRatio(fpoint,ypoint,ratio,2.0);
                PushObjectTo(you,ratio[0],ratio[1]);
            }
            return;
        }
        Delete(you);
    }
    SetYOU(d[YOU_PROC_PINDEX],0);
    FreeSmartMemEx(d);
}

void createYou(float x, float y, int user){
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0) return;
    int you=GetYOU(pIndex);
    if (you) return;
    you=DummyUnitCreateById(OBJ_RAT,x,y);
    SetUnitFlags(you,GetUnitFlags(you)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetOwner(user,you);
    SetYOU(pIndex, you);
    Frozen(you,FALSE);
    int *d;

    AllocSmartMemEx(YOU_PROC_MAX*4,&d);
    d[YOU_PROC_PINDEX]=pIndex;
    d[YOU_PROC_UNIT]=you;
    PushTimerQueue(1, d,onYouProc);
}

int isYou(int unit){
    if (MaxHealth(unit)){
        if (GetUnitThingID(unit)==OBJ_RAT){
            return IsPlayerUnit( GetOwner(unit) );
        }
    }
    return FALSE;
}

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

void gameStartButtonTriggered(){
    int user=IsPlayerPoint(OTHER);

    if (CurrentHealth(user)){
        int you=GetYOU(GetPlayerIndex(user));

        if (you) return;
        float x=GetObjectX(SELF),y=GetObjectY(SELF);
        createYou(x,y,user);
        UniPrint(user, "게임시작 버튼이 눌러졌습니다");
        Effect("YELLOW_SPARKS",x,y,0.0,0.0);
    }
}

void createStartButton(short location){
    int btn=DummyUnitCreateById(OBJ_WIZARD,LocationX(location),LocationY(location));

    SetUnitFlags(btn,GetUnitFlags(btn)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(btn,9,gameStartButtonTriggered);
}

int getLevelStartPoint(int lv){
    short *p;

    if (!p)
    {
        short points[]={14,18,192,23,26,35,82,191,86,211
        ,85,};
        p=points;
    }
    return p[lv];
}

void exitTriggered(){
    if (isYou(OTHER)){
        int user=GetOwner(OTHER);
        int pIndex=GetPlayerIndex(user);
        if (pIndex<0) return;
        int lv;

        QueryPlayerLevel(pIndex, &lv, 0);
        short loc=getLevelStartPoint(++lv);
        ShowQuestIntroOne(user, lv, "WarriorChapterBegin8", "War05B.scr:SisterReturned");
        QueryPlayerLevel(pIndex, 0, lv);
        Delete(OTHER);
        MoveObject(GetUserCamera(pIndex), LocationX(loc),LocationY(loc));
    }
}

void createExitButton(short location){
    int btn=DummyUnitCreateById(OBJ_WIZARD,LocationX(location),LocationY(location));

    LookWithAngle(btn, 32);
    SetUnitFlags(btn,GetUnitFlags(btn)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(btn,9,exitTriggered);
}

// int checkDetailColl(int you,int wall){
//     float point1[]={GetObjectX(you),GetObjectY(you),};
//     float point2[]={GetObjectX(wall),GetObjectY(wall),};

//     float fx=FABS( point2[0]-point1[0] );
//     float fy=FABS( point2[1]-point1[1]);

//     return (fx<=16.0)&&(fy<=16.0);
// }

#define GGOVER_XPOS 0
#define GGOVER_YPOS 1
#define GGOVER_CAM 2
#define GGOVER_USER 3
#define GGOVER_DUR 4
#define GGOVER_MAX 5

void onGGOver(int *d){
    int cam=d[GGOVER_CAM];

    while (MaxHealth(cam)){
        int user=d[GGOVER_USER];

        if (CurrentHealth(user)){
            if (--d[GGOVER_DUR]>=0){
                PushTimerQueue(1,d,onGGOver);
                return;
            }
            float *xy=&d[GGOVER_XPOS];
            
            MoveObject(cam,xy[0],xy[1]);
            break;
        }
        Delete(cam);
        break;
    }
    FreeSmartMemEx(d);
}

void playGGOver(int you){
    int user=GetOwner(you);
    if (!IsPlayerUnit(user))
        return;
    if(!CurrentHealth(user))
        return;
    int *d, pIndex=GetPlayerIndex(user);

    AllocSmartMemEx(GGOVER_MAX*4,&d);
    PushTimerQueue(1,d,onGGOver);
    int cam=GetUserCamera(pIndex);
    d[GGOVER_XPOS]=GetObjectX(cam);
    d[GGOVER_YPOS]=GetObjectY(cam);
    d[GGOVER_CAM]=cam;
    d[GGOVER_USER]=user;
    d[GGOVER_DUR]=180;
    MoveObject(cam,LocationX(16),LocationY(16));
}

void onWallCollide(){
    if (MaxHealth(OTHER)){
        if (GetUnitThingID(OTHER)==OBJ_RAT){
            playGGOver(GetCaller());
            // if (!checkDetailColl(OTHER,SELF)) return;
            DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION,GetObjectX(OTHER),GetObjectY(OTHER)), 12);
            Delete(OTHER);
        }
    }
}

void createObstacleWall(int pos){
    float x=GetObjectX(pos),y=GetObjectY(pos);
    Delete(pos);
    createObstacleWallReal(x,y);
    // int wall=CreateObjectById(OBJ_AIRSHIP_BASKET_SHADOW,x,y);
    // UnitNoCollide(wall);
    // SetUnitFlags(wall,GetUnitFlags(wall)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    // SetUnitMass(wall, 99999.0);
}

void speedPatrolBlock3(){
    float fspeed=4.0;
    placingPatrolBlock(214,228,fspeed);
    placingPatrolBlock(215,229,fspeed);
    placingPatrolBlock(230,216,fspeed);
    placingPatrolBlock(231,217,fspeed);
    placingPatrolBlock(218,232,fspeed);
    placingPatrolBlock(219,233,fspeed);
    placingPatrolBlock(234,220,fspeed);
    placingPatrolBlock(235,221,fspeed);
    placingPatrolBlock(222,236,fspeed);
    placingPatrolBlock(223,237,fspeed);
    placingPatrolBlock(238,224,fspeed);
    placingPatrolBlock(239,225,fspeed);
    placingPatrolBlock(226,240,fspeed);
    placingPatrolBlock(227,241,fspeed);
}

void deferredInit2(){
    float fspeed=2.0;
    placingPatrolBlock(89,97,fspeed);
    placingPatrolBlock(90,98,fspeed);
    placingPatrolBlock(93,101,fspeed);
    placingPatrolBlock(94,102,fspeed);
    placingPatrolBlock(99,91,fspeed);
    placingPatrolBlock(100,92,fspeed);
    placingPatrolBlock(103,95,fspeed);
    placingPatrolBlock(104,96,fspeed);
    placingPatrolBlock(105,107,fspeed);
    placingPatrolBlock(106,108,fspeed);
    placingPatrolBlock(109,117,fspeed);
    placingPatrolBlock(110,118,fspeed);
    placingPatrolBlock(119,111,fspeed);
    placingPatrolBlock(120,112,fspeed);
    placingPatrolBlock(113,121,fspeed);
    placingPatrolBlock(114,122,fspeed);
    placingPatrolBlock(123,115,fspeed);
    placingPatrolBlock(124,116,fspeed);
    placingPatrolBlock(143,141,fspeed);
    placingPatrolBlock(144,142,fspeed);
    placingPatrolBlock(131,139,fspeed);
    placingPatrolBlock(132,140,fspeed);
    placingPatrolBlock(138,130,fspeed);
    placingPatrolBlock(137,129,fspeed);
    placingPatrolBlock(127,135,fspeed);
    placingPatrolBlock(128,136,fspeed);
    placingPatrolBlock(133,125,fspeed);
    placingPatrolBlock(134,126,fspeed);

    float fspeed2=4.0;
    placingPatrolBlock(145,165,fspeed2);
    placingPatrolBlock(146,166,fspeed2);
    placingPatrolBlock(167,147,fspeed2);
    placingPatrolBlock(168,148,fspeed2);
    placingPatrolBlock(149,169,fspeed2);
    placingPatrolBlock(150,170,fspeed2);
    placingPatrolBlock(171,151,fspeed2);
    placingPatrolBlock(172,152,fspeed2);
    placingPatrolBlock(153,173,fspeed2);
    placingPatrolBlock(154,174,fspeed2);
    placingPatrolBlock(175,155,fspeed2);
    placingPatrolBlock(176,156,fspeed2);
    placingPatrolBlock(157,177,fspeed2);
    placingPatrolBlock(158,178,fspeed2);
    placingPatrolBlock(179,159,fspeed2);
    placingPatrolBlock(180,160,fspeed2);
    placingPatrolBlock(161,181,fspeed2);
    placingPatrolBlock(162,182,fspeed2);
    placingPatrolBlock(183,163,fspeed2);
    placingPatrolBlock(184,164,fspeed2);

    placingPatrolBlock(185,187,fspeed2+2.0);
    placingPatrolBlock(186,188,fspeed2+2.0);

    float fspeed3=3.0;
    placingPatrolBlock(195,203,fspeed3);
    placingPatrolBlock(196,204,fspeed3);
    placingPatrolBlock(199,207,fspeed3);
    placingPatrolBlock(200,208,fspeed3);
    placingPatrolBlock(205,197,fspeed3);
    placingPatrolBlock(206,198,fspeed3);
    placingPatrolBlock(210,202,fspeed3);
    placingPatrolBlock(209,201,fspeed3);

    placingPatrolBlock(243,251,fspeed3);
    placingPatrolBlock(244,252,fspeed3);
    placingPatrolBlock(253,245,fspeed3);
    placingPatrolBlock(254,246,fspeed3);
    placingPatrolBlock(247,255,fspeed3);
    placingPatrolBlock(248,256,fspeed3);
    placingPatrolBlock(257,249,fspeed3);
    placingPatrolBlock(258,250,fspeed3);
}

void initReadables(){
    RegistSignMessage(Object("read1"), "-미니게임 천국 제 1탄- 마우스 피하기 !!");
    RegistSignMessage(Object("read2"), "이 맵을 시작하려면 왼쪽 위쪽으로 걸어가세요. 원활하게 하려면 최대 해상도 쓰세요");
    RegistSignMessage(Object("read3"), "The Scary Maze (마우스 피하기) ... 제작 237");
    RegistSignMessage(Object("read4"), "이봐 자네! 이곳은 함부로 들어올 수가 없지");
}

void deferredInit(){
    FrameTimer(1,deferredInit2);
    FrameTimer(2,speedPatrolBlock3);
    initReadables();
    createStartButton(15);
    createStartButton(24);
    createStartButton(36);
    createStartButton(84);
    createStartButton(87);
    createStartButton(190);
    createStartButton(212);
    createStartButton(193);
    createExitButton(19);
    createExitButton(88);
    createExitButton(189);
    createExitButton(213);
    createExitButton(37);
    createExitButton(194);
    createExitButton(83);
    createExitButton(25);
    createStartButton(17);
    createExitButton(20);
    createExitButton(22);
    createStartButton(21);
    float fspeed5=5.0;
    placingPatrolBlock(27,31,fspeed5);
    placingPatrolBlock(28,32,fspeed5);
    placingPatrolBlock(29,33,fspeed5);
    placingPatrolBlock(30,34,fspeed5);

    float speed=3.0;
    placingPatrolBlock(38,60,speed);
    placingPatrolBlock(39,61,speed);
    placingPatrolBlock(42,64,speed);
    placingPatrolBlock(43,65,speed);
    placingPatrolBlock(46,68,speed);
    placingPatrolBlock(47,69,speed);
    placingPatrolBlock(50,72,speed);
    placingPatrolBlock(51,73,speed);
    placingPatrolBlock(54,76,speed);
    placingPatrolBlock(55,77,speed);
    placingPatrolBlock(58,80,speed);
    placingPatrolBlock(59,81,speed);

    placingPatrolBlock(62,40,speed);
    placingPatrolBlock(63,41,speed);
    placingPatrolBlock(66,44,speed);
    placingPatrolBlock(67,45,speed);
    placingPatrolBlock(70,48,speed);
    placingPatrolBlock(71,49,speed);
    placingPatrolBlock(74,52,speed);
    placingPatrolBlock(75,53,speed);
    placingPatrolBlock(78,56,speed);
    placingPatrolBlock(79,57,speed);
}

void OnInitializeMap(){
    MusicEvent();
    blockObserverMode();
    MakeCoopTeam();
    InitializePlayerSystem();
    FrameTimer(1, deferredInit);
    int initScanHash, lastUnit=GetMasterUnit();
    HashCreateInstance(&initScanHash);
    HashPushback(initScanHash, OBJ_BLUE_FLAME, createObstacleWall);
    StartUnitScan(Object("firstscan"), lastUnit, initScanHash);
}
void OnShutdownMap(){
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

