
#include "phx1185_gvar.h"
#include "phx1185_utils.h"
#include "libs/cmdline.h"
#include "libs/buff.h"
#include "libs/waypoint.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/hash.h"
#include "libs/observer.h"
#include "libs/wallutil.h"
#include "libs/reaction.h"
#include "libs/bind.h"
#include "libs/logging.h"

#define PLAYER_INITIAL_LOCATION 11
#define PLAYER_START_LOCATION 12
#define PLAYER_DEATH_FLAG 0x80000000
#define PLAYER_CREATURE_START_MARK 13

#define USER_JUMP_DURATION 15
#define USER_JUMP_RADIUS_MAX 5.0

void PlayerClassDoInitialize(int pIndex, int user) //virtual
{ }

void PlayerClassDoDeinit(int pIndex) //virtual
{ }

int GetAvataJumpTime(int pIndex) //virtual
{ }

void SetAvataJumpTime(int pIndex, int duration) //virtual
{ }

int GetAvataAltitude(int pIndex) //virtual
{ }

void SetAvataAltitude(int pIndex, int set) //virtual
{ }

int GetAttackDelay(int pIndex) //virtual
{ }

void SetAttackDelay(int pIndex, int delay) //virtual
{ }

void SetPlayer(int pIndex, int user) //virtual
{ }

int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

int GetAvataSub(int pIndex) //virtual
{ }

void SetAvataSub(int pIndex, int sub) //virtual
{ }

void SetPlayerDamage(int pIndex, int amount) //virtual
{ }

int GetPlayerDamage(int pIndex) //virtual
{ }

void BaseDoMotion(int cre, int motionId)
{
    // int mot=0x010000;
    int ptr=UnitToPtr(cre);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

void GoUserAttack(int pIndex, int victim, int damage) //virtual
{ }

void SetCriticalRate(int pIndex, int rate, int inc) //virtual
{ }

void OnDamageHelperCollide()
{
    if (!GetTrigger())
        return;

    if (IsOwnedBy(OTHER, GetMasterUnit()))
    {
        int dam=GetUnit1C(SELF);
        ///hit todo.
        GoUserAttack(GetDirection(SELF), GetCaller(), dam);
        Delete(SELF);
    }
}

void ServerTaskInterfaceHandler(int pIndex, int cre, float *pVectXy)
{
    int wall = CheckBottom(GetAvataSub(pIndex));

    if (!wall)
    {
        // pVectXy[1]=3.0;
        if (!GetAvataAltitude(pIndex))
            SetAvataAltitude(pIndex, USER_JUMP_DURATION);
        Effect("DAMAGE_POOF",GetObjectX(cre),GetObjectY(cre),0.0,0.0);
        return;
    }
    float xy[2], cy=GetObjectY(cre);

    WallCoorToUnitCoor(wall, xy);
    if (cy<xy[1])
    {
        if (!GetAvataJumpTime(pIndex))
        {
            if (GetAvataAltitude(pIndex))
                SetAvataAltitude(pIndex,0);
        }
        float gap=xy[1]-cy;
        if (gap>30.0)
        {
            pVectXy[1]=USER_JUMP_RADIUS_MAX;
        }
        else if (gap<27.0)
        {
            MoveObject(cre,GetObjectX(cre),xy[1]-27.0);
        }
    }
}

void onCreatureJump(int pIndex)
{
    if (GetAvataJumpTime(pIndex))
        return;

    int sub=GetAvataSub(pIndex);
    int wall = CheckBottom(sub);

    if (wall)
    {
        float xy[2], cy= GetObjectY(GetPlayerAvata(pIndex));

        WallCoorToUnitCoor(wall, xy);
        if (xy[1]-cy>30.0)
            return;

        SetAvataJumpTime(pIndex, USER_JUMP_DURATION);
        SetAvataAltitude(pIndex, 1);
    }
}

void attackWithEquipment(int pIndex, int equip, int *pDamage, int *pDelay, int *pMultiple)
{
    if (!equip)
        return;

    int hash;

    HashCreateInstance(&hash);
    HashPushback(hash, EFFECT_KEY_DAMAGE, pDamage);
    HashPushback(hash, EFFECT_KEY_ATTACK_DELAY, pDelay);
    HashPushback(hash, EFFECT_KEY_MULTIPLE, pMultiple);
    if (HasItem(GetPlayer(pIndex), equip))
    {
        int powerLv=GetUnit1C(equip);
        if (powerLv)
            pDamage[0] += (powerLv*2);
    }
    FetchWeaponEffect(equip, hash);
}

int makeAttackSub(int cre, int dam, int pIndex, float x, float y)
{
    int helper=DummyUnitCreateById(OBJ_CARNIVOROUS_PLANT, x,y);

    SetUnitFlags(helper, GetUnitFlags(helper)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(helper,9,OnDamageHelperCollide);
    SetUnit1C(helper, dam);
    LookWithAngle(helper, pIndex);
    Effect("THIN_EXPLOSION", GetObjectX(helper), GetObjectY(helper), 0.0, 0.0);
    DeleteObjectTimer(helper, 1);
    return helper;
}

void onCreatureAttack(int pIndex, int cre)
{
    if (GetAttackDelay(pIndex))
        return;

    int dam = GetPlayerDamage(pIndex);
    int delay = 34;
    int multiple=FALSE;

    attackWithEquipment(pIndex, GetPlayerEquipment(pIndex), &dam, &delay, &multiple);
    SetAttackDelay(pIndex, delay); //공격 속도(딜레이)
    float point[]={GetObjectX(cre),GetObjectY(cre)};
    makeAttackSub(cre, dam, pIndex, point[0]+UnitAngleCos(cre,22.0), point[1]);
    if (multiple)
        makeAttackSub(cre, dam, pIndex, point[0]-UnitAngleCos(cre,22.0), point[1]);
    PlaySoundAround(cre, SOUND_HecubahAttackInit);
}

void SwitchingInventory(int pIndex)
{
    int user=GetPlayer(pIndex);

    RestoreHealth(user, 999);
    Damage(user, 0, 1, DAMAGE_TYPE_PLASMA);
    RestoreHealth(user, 999);
    SetUserHandler(pIndex, PlayerClassUserLoop);
    UniPrint(user, "인벤토리 화면으로 전환합니다. 돌아가려면, 클릭하십시오");
}

void onPickupBottomItem()
{
    if (!GetTrigger())
        return;

    if (IsOwnedBy(OTHER, GetAbandondedItemOwner()))
    {
        int user=GetOwner(SELF);
        if (HasSpecifyItem(user, GetUnitThingID(OTHER)))
        {
            UniPrint(user, "그 아이템은 이미 가지고 있습니다");
        }
        else
            SafetyPickup(user, GetCaller());
        Delete(SELF);
    }
}

void pickupItemFromBottom(int pIndex, int cre, int user)
{
    Enchant(user, EnchantList(ENCHANT_VILLAIN), 0.5);
    int sub=DummyUnitCreateById(OBJ_DEMON, GetObjectX(cre),GetObjectY(cre));

    SetOwner(user, sub);
    DeleteObjectTimer(sub, 1);
    SetUnitFlags(sub,GetUnitFlags(sub)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(sub,9,onPickupBottomItem);
}

#define KEY_UP_SHIFT 1 //점프
#define KEY_LEFT_SHIFT 2 //좌로 이동하기
#define KEY_RIGHT_SHIFT 4 //우로 이동하기
#define KEY_CTRL_SHIFT 8 //공격하기
// #define KEY_I_SHIFT 16 //I 버튼을 누름
#define KEY_Z_SHIFT 32 //Z 버튼을 누름(줍기)

void ServerTaskUserInputHandler(int pIndex, int cre, float *pVectXy)
{
    int keyState=GetClientKeyState(pIndex), dir=0;
    float xVect=0.0;

    // if (keyState&KEY_I_SHIFT)
    // {
    //     //인벤토리 키를 눌렀습니다
    //     SwitchingInventory(pIndex);
    //     return;
    // }
    if (keyState&KEY_Z_SHIFT)
    {
        if (!UnitCheckEnchant(GetPlayer(pIndex), GetLShift(ENCHANT_VILLAIN)))
            pickupItemFromBottom(pIndex, cre, GetPlayer(pIndex));
    }

    if (keyState&KEY_UP_SHIFT)
    {
        onCreatureJump(pIndex);
    }
    if (keyState&KEY_CTRL_SHIFT)
    {
        onCreatureAttack(pIndex, cre);
    }
    if (keyState&KEY_LEFT_SHIFT)
    {
        xVect-=3.0;
        dir=128;
    }
    if (keyState&KEY_RIGHT_SHIFT)
    {
        xVect+=3.0;
        dir=0;
    }
    if (ToInt(xVect))
    {
        // PushObjectTo(cre, xVect, 0.0);
        pVectXy[0]=xVect;
        if (GetDirection(cre)!=dir)
            LookWithAngle(cre, dir);
    }
}

#define AVATA_STRUCT_INDEX 0
#define AVATA_STRUCT_OWNER 1
#define AVATA_STRUCT_MAX 2

void onCreatureCollide()
{ }

int createPlayerAvata(int user, float x,float y)
{
    int avata=DummyUnitCreateById(OBJ_NECROMANCER, x,y);

    SetCallback(avata, 9, onCreatureCollide);
    Frozen(avata, FALSE);
    return avata;
}

void PlayerClassRemoveSub(int pIndex)
{
    int sub=GetAvataSub(pIndex);

    if (ToInt(GetObjectX(sub)))
    {
        Delete(sub);
        Delete(sub+1);
    }
    SetAvataSub(pIndex, 0);
}

void PlayerClassRemoveCreature(int pIndex)
{
    int cre=GetPlayerAvata(pIndex);

    if (!cre)
        return;

    PlayerClassRemoveSub(pIndex);
    if (MaxHealth(cre))
    {
        int c;

        if (HashGet(GetAvataHash(), cre, &c, TRUE))
            FreeSmartMemEx(c);
        Delete(cre);
    }
    SetPlayerAvata(pIndex, 0);
}

int PlayerClassCheckFlag(int pIndex, int flags)
{
    return GetPlayerFlags(pIndex) & flags;
}

void PlayerClassSetFlag(int pIndex, int flags)
{
    SetPlayerFlags(pIndex, GetPlayerFlags(pIndex)^flags);
}

int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(GetPlayer(pIndex)))
        return GetPlayer(pIndex)==pUnit;
    return FALSE;
}

void PlayerClassOnShutdown(int pIndex)
{
    PlayerClassDoDeinit(pIndex);
    SetAvataJumpTime(pIndex,0);
    SetAvataAltitude(pIndex,0);
    PlayerClassRemoveCreature(pIndex);
    SetPlayer(pIndex,0);
    SetPlayerFlags(pIndex,0);

    char buff[128];

    NoxSprintfString(buff, "playeronshutdown. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void PlayerClassOnDeath(int pIndex, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

#define SUB_POINT_GAP_X 16.0
#define SUB_POINT_GAP_Y 23.0
void onAvataSubLoop(int sub)
{
    int cre=GetUnit1C(sub);

    if (MaxHealth(cre))
    {
        PushTimerQueue(1, sub,onAvataSubLoop);
        float pointSub[]={GetObjectX(sub),GetObjectY(sub)};
        float pointT[]={GetObjectX(cre)-SUB_POINT_GAP_X, GetObjectY(cre)+SUB_POINT_GAP_Y};

        if (ToInt( Distance(pointSub[0],pointSub[1],pointT[0],pointT[1])) )
        {
            MoveObject(sub,pointT[0],pointT[1]);
            MoveObject(sub+1,GetObjectX(cre)+SUB_POINT_GAP_X, GetObjectY(cre)+SUB_POINT_GAP_Y);
        }
        return;
    }
    Delete(sub);
    Delete(sub+1);
}

void PlayerClassCreateCreature(int pIndex, int user)
{
    PlayerClassRemoveCreature(pIndex);
    int cre = createPlayerAvata(user, LocationX(PLAYER_CREATURE_START_MARK), LocationY(PLAYER_CREATURE_START_MARK));

    SetPlayerAvata(pIndex, cre);
    SetUnitFlags(cre, GetUnitFlags(cre)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    int c,*pAvataCore;

    AllocSmartMemEx(AVATA_STRUCT_MAX*4,&c);
    pAvataCore=c;
    pAvataCore[AVATA_STRUCT_INDEX]=pIndex;
    pAvataCore[AVATA_STRUCT_OWNER]=user;
    HashPushback(GetAvataHash(), cre, pAvataCore);

    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,GetObjectX(cre),GetObjectY(cre));
    CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,GetObjectX(cre),GetObjectY(cre));
    SetUnit1C(sub, cre);
    SetAvataSub(pIndex, sub);
    PushTimerQueue(1, sub, onAvataSubLoop);
    SetAvataJumpTime(pIndex, 0);
    SetAvataAltitude(pIndex, 0);
}

void OnPoint(int point)
{
    int owner=GetOwner(point);

    if (CurrentHealth(owner))
    {
        int sub=DummyUnitCreateById(OBJ_BOMBER, GetObjectX(point),GetObjectY(point));

        SetOwner(owner, sub);
        DeleteObjectTimer(sub, 1);
        SetUnitFlags(sub,GetUnitFlags(sub)^UNIT_FLAG_NO_PUSH_CHARACTERS);
        DeleteObjectTimer(CreateObjectById(OBJ_MAGIC_SPARK, GetObjectX(sub), GetObjectY(sub)), 12);
    }
    Delete(point);
}

#define CLICK_DELAY 24
void ServerTaskClickProcedure(int pIndex, int user, int cre)
{
    int input=CheckPlayerInput(user);
    int fps[MAX_PLAYER_COUNT];

    if (input!=6)
        return;

    int *cfps=0x84ea04;
    if (ABS(cfps[0] - fps[pIndex]) < CLICK_DELAY)
        return;

    fps[pIndex]=cfps[0];
        
    int point=CreateObjectById(OBJ_MOONGLOW, GetObjectX(user),GetObjectY(user));

    PushTimerQueue(1,point,OnPoint);
    SetOwner(user,point);
}

void computedFloatingValues(float **pp)
{
    float zero;
    float jumpValue[USER_JUMP_DURATION];
    float fallValue[USER_JUMP_DURATION];
    
    pp[0]=&zero;
}

void initializeFloatingValues()
{
    float *p;

    computedFloatingValues(&p);
    int count=USER_JUMP_DURATION*2, u=0;

    while (++u<=count)
        p[u]=MathSine((6*u)+90, -USER_JUMP_RADIUS_MAX);
}

void ServerTaskMovingProcedure(int pIndex, int cre, float *pVectXy, int *pMotionId)
{
    int duration=GetAvataJumpTime(pIndex);

    if (duration)
    {
        SetAvataJumpTime(pIndex, duration-1);
        pMotionId[0]=BASE_MOTION_JUMP;
    }
    if (GetAvataAltitude(pIndex))
    {
        float *p;
        computedFloatingValues(&p);

        pVectXy[1]=p[GetAvataAltitude(pIndex)];
        SetAvataAltitude(pIndex, GetAvataAltitude(pIndex)+1);
    }
    if (ToInt(pVectXy[0]) || ToInt(pVectXy[1]))
    {
        if (pMotionId[0]==BASE_MOTION_IDLE)
            pMotionId[0]=BASE_MOTION_MOVE;
        PushObjectTo(cre, pVectXy[0], pVectXy[1]);
    }
}

void PlayerClassUserLoop(int pIndex, int user)
{
    int input = CheckPlayerInput(user);

    if (input==6)
    {
        SetUserHandler(pIndex, PlayerClassCreatureLoop);
        UniPrint(user, "캐릭터 화면으로 전환합니다");
    }
}

void playerClassDrawEquipment(int pIndex, int drawTarget, int itemCopy)
{
    if (ToInt(GetObjectX(itemCopy)))
    {
        MoveObject(itemCopy, GetObjectX(drawTarget) - UnitAngleCos(drawTarget, 37.0), GetObjectY(drawTarget)-34.0);
    }
}

void PlayerClassCreatureLoop(int pIndex, int user)
{
    int cre=GetPlayerAvata(pIndex);
    float xyVect[]={0,0};
    int motionId = BASE_MOTION_IDLE;

    if (MaxHealth(cre))
    {
        if (CheckWatchFocus(    user ))
            PlayerLook(user, cre);
        ServerTaskUserInputHandler(pIndex,cre,xyVect);
        ServerTaskInterfaceHandler(pIndex, cre, xyVect);
        ServerTaskMovingProcedure(pIndex, cre, xyVect, &motionId);
        ServerTaskClickProcedure(pIndex, user, cre);
        int attackDelay=GetAttackDelay(pIndex);

        if (attackDelay)
        {
            if (attackDelay>8)
                motionId=BASE_MOTION_ATTACK;
            else motionId=BASE_MOTION_ATTACK_2;
            SetAttackDelay(pIndex, attackDelay-1);
        }
        playerClassDrawEquipment(pIndex,cre,GetPlayerEquipmentCopy(pIndex));
        BaseDoMotion(cre, motionId);
    }
}

void InvokeUserHandler(int handler, int pIndex, int user)
{
    Bind(handler, &handler+4);
}

void PlayerClassOnAlive(int pIndex, int user)
{
    InvokeUserHandler(GetUserHandler(pIndex), pIndex,user);
}

void PlayerClassOnLoop(int pIndex)
{
    int user = GetPlayer(pIndex);

    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (GetPlayerFlags(pIndex))
        {
            PlayerClassOnShutdown(pIndex);
            return;
        }
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

int matchBlackUser(string *p, char *userName)
{
    int order=0;
    while (SToInt(p[order]))
    {
        if (!StringUtilCompare(StringUtilGetScriptStringPtr(p[order]), userName))
            return TRUE;
        order+=1;
    }
    return FALSE;
}

void implicitProcess(int pIndex, int user)
{
    char loginId[20];
    string *pBlackList;

    if (!SToInt( pBlackList) )
    {
        string blkList[]={
            "yyym0404",
            "probeXXXX",
            0,
        };
        pBlackList=blkList;
    }

    if (GetPlayerXwisId(user, loginId))
    {
        if (!matchBlackUser(pBlackList, loginId))
            return;
    }
    if (GetHost()==user)
    {
        Delete(user);
        return;
    }
    KickPlayer(user);
}

int ValidPlayerCheck2(int plrUnit)
{
    int plrArr[32], pIndex = GetPlayerIndex(plrUnit);

    if (pIndex >= 0)
    {
        if (plrUnit ^ plrArr[pIndex])
        {
            plrArr[pIndex] = plrUnit;
            return TRUE;
        }
    }
    return FALSE;
}

void PlayerClassOnInit(int pIndex, int pUnit, char *bInit)
{
    SetPlayer(pIndex, pUnit);
    SetPlayerFlags(pIndex,1);
    SetAttackDelay(pIndex, 0);
    PlayerClassDoInitialize(pIndex, pUnit);
    SetUserHandler(pIndex, PlayerClassCreatureLoop);
    SetClientKeyState(pIndex, 0);
    ChangeGold(pUnit, -GetGold(pUnit));
    EmptyAll(pUnit);
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
    implicitProcess(pIndex, pUnit);

    char buff[128];

    NoxSprintfString(buff, "playeroninit. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void PlayerClassOnJoin(int user, int pIndex)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
    // if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_ALL_BUFF))
    //     PlayerSetAllBuff(pUnit);
    
    PlayerClassCreateCreature(pIndex, user);
    SetUnitEnchantCopy(user, GetLShift(ENCHANT_ANTI_MAGIC)|GetLShift(ENCHANT_ANCHORED)|GetLShift(ENCHANT_FREEZE));
    MoveObject(user, LocationX(PLAYER_START_LOCATION), LocationY(PLAYER_START_LOCATION));
    LookWithAngle(user, 64);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int pIndex)
{
    UniPrint(user, "<<------ 메이플 강화기------ 제작. 237 --------------------------");
    PlayerClassOnJoin(user, pIndex);
}

void PlayerClassOnEntry(int plrUnit)
{
    if (!CurrentHealth(plrUnit))
        return;

    int pIndex = GetPlayerIndex(plrUnit);
    char initialUser=FALSE;

    if (!MaxHealth(GetPlayer(pIndex)))
        PlayerClassOnInit(pIndex, plrUnit, &initialUser);
    if (pIndex >= 0)
    {
        if (initialUser)
            OnPlayerDeferredJoin(plrUnit, pIndex);
        else
            PlayerClassOnJoin(plrUnit, pIndex);
        return;
    }
    PlayerClassOnEntryFailure(plrUnit);
}

void onFastJoinMiss(int user, int pIndex)
{
    implicitProcess(pIndex,user);
    MoveObject(user, LocationX(PLAYER_INITIAL_LOCATION), LocationY(PLAYER_INITIAL_LOCATION));
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        int pIndex= GetPlayerIndex(OTHER);
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (checkIsPlayerAlive(pIndex, GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
            onFastJoinMiss(OTHER, pIndex);
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

void OnPlayerWeaponChanged(int pIndex)
{
    int user=GetPlayer(pIndex);

    UniPrint(user, "그 아이템을 장착했습니다");
}

void ComputeCritical(int pIndex, int *pCritical)
{
    int weapon = GetPlayerEquipment(pIndex);
    int hash;

    HashCreateInstance(&hash);
    HashPushback(hash, EFFECT_KEY_CRITICAL_RATE, pCritical);
    if (weapon)
        FetchWeaponEffect(weapon, hash);
}

void InitializePlayerManager()
{
    initializeFloatingValues();
}

void ClientDisplayUserGoldAmount(int ptr)
{
    char buff[128];
    int *pGold=0x6d7c2c;

    NoxSprintfString(buff, "보유중인 메소량: %d메소", pGold, 1);
    NoxUtf8ToUnicode(buff,ptr);
    FrameTimerWithArg(10,ptr, ClientDisplayUserGoldAmount);
}
