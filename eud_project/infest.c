

#include "libs/define.h"
#include "libs\unitstruct.h"
#include "libs\printutil.h"

#include "libs\waypoint.h"
#include "libs\fxeffect.h"
#include "libs\mathlab.h"
#include "libs\reaction.h"
#include "libs/network.h"
#include "libs/imageutil.h"
#include "libs/buff.h"

int player[30];
int GameTheEnd = 0;
int SecretCount;
int GameStart = 0;
int AlivePlayerCount = 0;
int *m_pImgVector;


void GreenSparkFx(float x, float y)
{
    int ptr = CreateObjectAt("MonsterGenerator", x, y);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void ImageBillyoniUp()
{}
void ImageBillyoniLeft()
{}
void ImageBillyoniRight()
{}
void ImageBillyoniDown()
{}

void CommonProcedure()
{
    m_pImgVector = CreateImageVector(32);

    InitializeImageHandlerProcedure(m_pImgVector);
    int index = 0;
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniUp) + 4, 120459, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniUp) + 4, 120460, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniRight) + 4, 120461, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniLeft) + 4, 120462, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniRight) + 4, 120463, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniLeft) + 4, 120464, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniDown) + 4, 120465, index++);
    AddImageFromResource(m_pImgVector, GetScrCodeField(ImageBillyoniDown) + 4, 120466, index++);
}

void MapInitialize()
{
    initiMain(-1);
    //delay_call_function
    startCountdown(-1);

    //loop_function
    FrameTimer(3, LoopPreservePlayer);

    setMapLighting(23, 60);
    FrameTimer(20, PlaceStampString);
    FrameTimer(10, putBookcase1F);
    FrameTimer(10, putBookcase3F);
}

int PlayerRocks(int num)
{
    int ptr, i;

    if (num < 0)
    {
        ptr = CreateObject("InvisibleLightBlueLow", 1) + 1;
        for (i = 0 ; i < 10 ; i ++)
        {
            CreateObject("Rock5", 70 + i);
            SetUnitFlags(ptr + i, GetUnitFlags(ptr + i) ^ 0x2000);
        }
    }
    return ptr + num;
}

void PlayerSetDeathFlag(int plr)
{
    player[plr + 10] ^= 2;
}

int PlayerCheckOnDeath(int plr)
{
    return player[plr + 10] & 2;
}

void PlayerSetIngameFlag(int plr)
{
    player[plr + 10] ^= 4;
}

int PlayerCheckIngameFlag(int plr)
{
    return player[plr + 10] & 4;
}

int PlayerClassZombieFlagCheck(int plr)
{
    return player[plr + 10] & 8;
}

void PlayerClassZombieFlagSet(int plr)
{
    player[plr + 10] ^= 8;
}

void UpdateAlivePlayerCount()
{
    int i;

    AlivePlayerCount = 0;
    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]))
        {
            if (PlayerCheckIngameFlag(i))
            {
                if (!PlayerClassZombieFlagCheck(i))
                    AlivePlayerCount ++;
            }
        }
    }
}

void PlayerOnDeath(int plrIndex)
{
    UpdateAlivePlayerCount();
    if (PlayerCheckIngameFlag(plrIndex))
        PlayerSetIngameFlag(plrIndex);
}

void initiMain(int arg_0)
{
    movingCase(arg_0);
    preSwitch(arg_0);
    PlayerRocks(-1);
    //onJumpSwitch(arg_0);
    playerGrp(arg_0);
    counterUnit();
    return;
}

void PlayerOnJoin(int plrIndex)
{
    if (PlayerCheckOnDeath(plrIndex))
        PlayerSetDeathFlag(plrIndex);
    if (!PlayerCheckIngameFlag(plrIndex))
        PlayerSetIngameFlag(plrIndex);
    Delete(GetLastItem(player[plrIndex]));
    Delete(GetLastItem(player[plrIndex]));
    Enchant(player[plrIndex], "ENCHANT_SLOWED", 0.0);
    Enchant(player[plrIndex], "ENCHANT_HASTED", 0.0);
    MoveObject(player[plrIndex], LocationX(67), LocationY(67));
    DeleteObjectTimer(CreateObject("ReleasedSoul", 67), 15);
    AudioEvent("BlindOff", 67);
}

void PlayerOnJoinFail()
{
    Enchant(other, "ENCHANT_FREEZE", 0.0);
    Enchant(other, "ENCHANT_ANCHORED", 0.0);
    Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(other, "ENCHANT_INVULNERABLE", 0.0);
    if (GameStart)
    {
        MoveObject(other, LocationX(80), LocationY(80));
        UniPrint(other, "이미 게임이 시작되었습니다. 이번 게임이 끝날 때 까지 관객모드로 전환하셔서 다른 유저를 구경하세요");
    }
    else
    {
        MoveObject(other, 672.0, 2325.0);
        UniPrint(other, "지도입장 거부됨- 맵에 입장한 유저가 너무 많거나 이 지도는 전사만 입장할 수 있습니다");
    }
}

int PlayerOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
            CommonProcedure();
    }
    return plr;
}

static void NetworkUtilClientMain()
{
    CommonProcedure();
}

int GetPlayerScrIndex()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

void getPlayers()
{
    while (1)
    {
        if (CurrentHealth(other))
        {
            if (MaxHealth(other) == 150 && !GameStart)
            {
                int plr = GetPlayerScrIndex(), i;

                for (i = 9 ; i >= 0 && plr < 0 ; i --)
                {
                    if (!MaxHealth(player[i]))
                    {
                        plr = PlayerOnInit(i, GetCaller());
                        break;
                    }
                }
                if (plr + 1)
                {
                    PlayerOnJoin(plr);
                    break;
                }
            }
            PlayerOnJoinFail();
        }
        break;
    }
}

void PlayerOnShutdown(int plrIndex)
{
    UpdateAlivePlayerCount();
    teleportGRP(plrIndex);
    UniPrintToAll(PlayerIngameNick(player[plrIndex]) + " 님께서 게임을 떠나셨습니다");

    player[plrIndex] = 0;
    player[plrIndex + 10] = 0;
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    MoveObject(preSwitch(plr), GetObjectX(pUnit), GetObjectY(pUnit));
    MoveObject(PlayerRocks(plr), GetObjectX(pUnit), GetObjectY(pUnit));
    if (PlayerClassZombieFlagCheck(plr))
    {
        SetWalkingMotion(plr, pUnit);
        GodModAndInvisible(plr);
    }
    else
    {
        if (CheckPlayerInput(pUnit) == 7)
            PlayerOnJump(pUnit);
    }
}

void LoopPreservePlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (!PlayerCheckOnDeath(i))
                    {
                        PlayerSetDeathFlag(i);
                        PlayerOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
            {
                PlayerOnShutdown(i);
            }
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayer);
}

void playerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (GetPlayerScrIndex() >= 0)
        {
            getPlayers();
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
            MoveObject(OTHER, LocationX(81), LocationY(81));
    }
}

void SetWalkingMotion(int plr, int pUnit)
{
    float dist = Distance(GetObjectX(pUnit), GetObjectY(pUnit) + 4.0, GetObjectX(playerGrp(plr)), GetObjectY(playerGrp(plr)));

    if (dist <= 1.0)
    {
        if (HasEnchant(playerGrp(plr), "ENCHANT_VILLAIN"))
        {
            PauseObject(playerGrp(plr), 1);
            EnchantOff(playerGrp(plr), "ENCHANT_VILLAIN");
        }
    }
    else
    {
        Frozen(playerGrp(plr), 0);
        Enchant(playerGrp(plr), "ENCHANT_VILLAIN", 0.0);
        if (HasEnchant(playerGrp(plr), "ENCHANT_DETECTING") != true)
        {
            TeleportLocation(16, GetObjectX(pUnit), GetObjectY(pUnit));
            AudioEvent("MechGolemMove", 16);
            Enchant(playerGrp(plr), "ENCHANT_DETECTING", 0.4);
        }
        Frozen(playerGrp(plr), 1);
        LookWithAngle(playerGrp(plr), GetDirection(pUnit));
        // Walk(playerGrp(plr), GetObjectX(playerGrp(plr)), GetObjectY(playerGrp(plr)));
        MoveObject(playerGrp(plr), GetObjectX(pUnit), GetObjectY(pUnit) + 4.0);
    }
}

void GodModAndInvisible(int plr)
{
    int pUnit = player[plr];

    if (!HasEnchant(pUnit, "ENCHANT_INVULNERABLE"))
        Enchant(pUnit, "ENCHANT_INVULNERABLE", 0.0);
    if (!HasEnchant(pUnit, "ENCHANT_INVISIBLE"))
        Enchant(pUnit, "ENCHANT_INVISIBLE", 0.0);
}

void teleportGRP(int plr)
{
    MoveObject(playerGrp(plr), LocationX(68), LocationY(68));
}

void FastedRun(int ptr)
{
    while (IsObjectOn(ptr))
    {
        int owner = GetOwner(ptr), count = GetDirection(ptr);

        if (CurrentHealth(owner) && count)
        {
            int pusher = CreateObjectAt("CarnivorousPlant", GetObjectX(owner) - GetObjectZ(ptr), GetObjectY(owner) - GetObjectZ(ptr + 1));

            Frozen(pusher, 1);
            DeleteObjectTimer(pusher, 1);
            GreenSparkFx(GetObjectX(pusher), GetObjectY(pusher));
            LookWithAngle(ptr, --count);
            FrameTimerWithArg(1, ptr, FastedRun);
            break;
        }
        Delete(ptr);
        Delete(++ptr);
        break;
    }
}

void PlayerOnJump(int owner)
{
    if (!HasEnchant(owner, "ENCHANT_AFRAID"))
    {
        Enchant(owner, "ENCHANT_AFRAID", 14.0);

        int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner));

        Raise(unit, UnitAngleCos(owner, 20.0));
        Raise(CreateObject("InvisibleLightBlueLow", 1), UnitAngleSin(owner, 20.0));
        SetOwner(owner, unit);
        LookWithAngle(unit, 16);
        PlaySoundAround(owner, 900);
        FastedRun(unit);

    }
}


void makeStar(int locationId, float gapsize)
{
    string fxname = "SENTRY_RAY";

    float xprofile = LocationX(locationId), yprofile = LocationY(locationId);
    float fxpoints[10];
    int i;



    for(i = 0; i < 5; i += 1)
    {


        fxpoints[i * 2] = xprofile - MathSine(i * 72, gapsize);
        fxpoints[i * 2 + 1] = yprofile - MathSine(i * 72 + 90, gapsize);

    }




    Effect(fxname, fxpoints[0], fxpoints[1], fxpoints[4], fxpoints[5]);
    Effect(fxname, fxpoints[0], fxpoints[1], fxpoints[6], fxpoints[7]);
    Effect(fxname, fxpoints[2], fxpoints[3], fxpoints[6], fxpoints[7]);
    Effect(fxname, fxpoints[2], fxpoints[3], fxpoints[8], fxpoints[9]);
    Effect(fxname, fxpoints[4], fxpoints[5], fxpoints[8], fxpoints[9]);





}


void madeZombie(int pUnit)
{
    int plr = checkPlayer(pUnit);

    if (plr < 0)
        return;

    if (!PlayerClassZombieFlagCheck(plr))
    {
        PlayerClassZombieFlagSet(plr);
        UpdateAlivePlayerCount();
        TeleportLocation(17, GetObjectX(pUnit), GetObjectY(pUnit));
        makeStar(17, 100.0);
        AudioEvent("DemonDie", 17);
        UniPrintToAll("방금 " + PlayerIngameNick(player[plr]) + " 님께서 좀비에게 잡혔습니다");
        if (CheckGolemWin())
        {
            AllPlayerFreeze();
            FrameTimer(70, golemWin);

        }
        else
        {
            UniPrintToAll("좀비가 된 플레이어는 생존한 플레이어를 잡아 좀비로 만드세요 ...!");
            UniPrintToAll("생존한 플레이어는 남은 시간동안 좀비를 피해 버티세요...!");

        }
    }

    //
}



void preservePlayer()
{
    if (CurrentHealth(other))
    {
        if (!IsPlayerUnit(other))
            return;

        int plr = GetDirection(self);

        if (PlayerClassZombieFlagCheck(plr))
        {
            if (!HasEnchant(other, "ENCHANT_LIGHT") && !HasEnchant(player[plr], "ENCHANT_FREEZE"))
            {
                Enchant(other, "ENCHANT_LIGHT", 0.0);
                madeZombie(GetCaller());
            }
        }
    }
}


int checkPlayer(int plrUnit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (player[i] ^ plrUnit)
            continue;
        else
            return i;
    }
    return -1;
}



void SetUnitSubClass(int x, int y)
{
    //unit, class_index
    int thg = CreateObject("redPotion2", 2);
    int ttt;

    Damage(thg, 0, CurrentHealth(thg) - y, -1);
    storageVar(x, ttt);
    storageVar(thg, ttt + 10);

    FrameTimerWithArg(1, ttt, safePickup);
    ttt += 1;
    if (ttt == 10)
        ttt = 0;
}


void safePickup(int rrr)
{
    Pickup(storageVar(100, rrr), storageVar(100, rrr + 10));
}



int storageVar(int x, int y)
{
    
    //unit, mod(0: store, 1: reference)
    int var_0[20];

    if (x != 100)
    {
        var_0[y] = x;


        return 0;
    }
    else
    {
        return var_0[y];
    }


}

int GetUnitSubClass(int x)

{


    int lala = GetLastItem(x);
    int i = 0;

    while (MaxHealth(lala) != 255 && i < 3)
    {
        i += 1;
        lala = GetPreviousItem(lala);


    }

    if (i == 3)
    {
        return -1;
    }

    else
    {
        return MaxHealth(lala) - CurrentHealth(lala);
    }
}




int playerGrp(int plr)
{
    int arrr[10];
    int base = Object("zombieBaseGrp");
    int i;

    if (plr == -1)
    {
        for(i = 0 ; i < 10 ; i += 1)
        {
            arrr[i] = base + (i * 2);
            Frozen(arrr[i], 1);
            SetUnitSubClass(arrr[i], i);

        }

        return 0;

    }

    return arrr[plr];
}


void SwitchProcess(int unit, int index)
{
    int ptr = UnitToPtr(unit);

    if (GetUnitThingID(unit) == 735 && ptr)
    {
        SetMemory(ptr + 8, GetMemory(ptr + 8) ^ 0x400000);
        LookWithAngle(unit, index);
    }
}

int preSwitch(int plr)
{
    int prepre[10], i;

    if (plr == -1)
    {
        prepre[0] = Object("preSwitchBase");
        SwitchProcess(prepre[0], 0);
        for (i = 1 ; i < 10 ; i += 1)
        {
            prepre[i] = prepre[0] + (i * 2);
            SwitchProcess(prepre[i], i);
        }
        return 0;
    }
    return prepre[plr];
}

void startCountdown(int order)
{
    if (order == -1)
    {

        FrameTimerWithArg(90, 0, mentList);

    }
    else if (order < 10)
    {

        UniPrintToAll("좀비 플레이어 생성" + IntToString(10 - order) + " 초 전...");
        FrameTimerWithArg(30, order + 1, startCountdown);

    }
    else
    {

        chooseZombiePlayer();

    }
}

void mentList(int order2)
{
    if (order2 == 0)
        UniPrintToAll("귀신의 저택에 들어오신 여러분 모두 환영합니다 .");
    else if (order2 == 1)
        UniPrintToAll("잠시 후 여러분들 중 한명이 좀비로 변하게 될 것입니다 !!");
    else if (order2 == 2)
        UniPrintToAll("그러니 서로와 붙어있지 마시고 멀리 떨어져 도망가시기 바랍니다 .");
    else if (order2 == 3)
        UniPrintToAll("이건 경고 입니다 .... 그럼.. 게임을 시작하죠 ...");
    else if (order2 == 4)
    {
        FrameTimer(90, DoNotEntryMore);
        FrameTimerWithArg(250, 0, startCountdown);
        return;
    }
    FrameTimerWithArg(90, order2 + 1, mentList);
}

void DoNotEntryMore()
{
    GameStart = 1;
    UniPrintToAll("지금부터 플레이어들이 더 이상 맵에 입장할 수 없게 됩니다");
    UniPrintToAll("참가하지 못한 유저분들은 관객모드로 전환하여 다른 유저를 구경해보세요");
}

void golemWin()
{
    FrameTimerWithArg(15, 1, PlaceEndGameText);
    globalWinEvent(67);
    GameTheEnd = 1;
    UniPrintToAll("좀비팀 승리 -- 모든 생존자들이 좀비가 되었습니다 ...!");
    UniPrintToAll("좀비팀 승리 -- 모든 생존자들이 좀비가 되었습니다 ...!");
    UniPrintToAll("좀비팀 승리 -- 모든 생존자들이 좀비가 되었습니다 ...!");
}

void AllPlayerFreeze()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (PlayerCheckIngameFlag(i))
        {
            Enchant(player[i], "ENCHANT_INVULNERABLE", 0.0);
            Enchant(player[i], "ENCHANT_FREEZE", 0.0);
        }
    }
}

int CheckGolemWin()
{
    int i, allCount = 0, zCount = 0; 
    
    for (i = 9 ; i >= 0 ; i --)
    {
        if (PlayerCheckIngameFlag(i))
        {
            allCount ++;
            if (PlayerClassZombieFlagCheck(i))
                zCount ++;
        }
    }
    if (allCount && zCount)
        return (allCount == zCount);
    return 0;
}

void globalWinEvent(int location)
{
    int i;
    
    for (i = 0 ; i < 10 ; i ++)
        MoveObject(player[i], GetWaypointX(location) + IntToFloat(i * 23), GetWaypointY(location));
}

void setCountdown(int countdown)
{
    if (GameTheEnd == 0)
    {
        if (countdown > 1) {
            UniChatMessage(counterUnit(),
                "생존자 구출까지 남은시간: " + IntToString(countdown) + " 초\n현재 생존자 " + IntToString(AlivePlayerCount) + "명", 42);
            SecondTimerWithArg(1, countdown - 1, setCountdown);
        }
        else if (countdown == 1)
        {
            GameTheEnd = 1;
            escapeAooniHouse();
        }
    }
}

void escapeAooniHouse()
{
    UniPrintToAll("인간팀 승리! -- 모든 생존자들이 저택에서 구출되셨습니다 .");
    globalWinEvent(67);
    FrameTimerWithArg(15, 0, PlaceEndGameText);
    AudioEvent("FlagCapture", 67);
    AudioEvent("BigGong", 67);
    ObjectOff(self);
}


















int counterUnit()
{
    int master;

    if (master == 0)
    {
        master = CreateObjectAt("willOWisp", 5407.0, 388.0);
        Frozen(master, 1);
    }
    return master;
}













int GetArrayPtr(int func)
{
    return GetMemory(GetMemory(0x75ae28) + (0x30 * func + 0x1c)) + 4;
}



void SwapArrayMix(int arrPtr, int arrSize)
{
    int i, temp, rnd;

    for (i = arrSize - 1 ; i >= 0 ; i --)
    {
        rnd = Random(0, arrSize - 1);
        temp = GetMemory(arrPtr + (4 * i));
        SetMemory(arrPtr + (4 * i), GetMemory(arrPtr + (4 * rnd)));
        SetMemory(arrPtr + (4 * rnd), temp);
    }
}

int SelectZombiePlayer()
{
    int arr[11], pic, i, func;

    if (!arr[10])
    {
        for (i = 0 ; i < 10 ; i ++)
            arr[i] = i;
        CancelTimer(FrameTimerWithArg(10, SelectZombiePlayer, SelectZombiePlayer));
        func = GetMemory(GetMemory(0x83395c) + 8);
    }
    SwapArrayMix(GetArrayPtr(func), 10);
    SwapArrayMix(GetArrayPtr(func), 10);
    SwapArrayMix(GetArrayPtr(func), 10);
    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[arr[i]]))
            return arr[i];
    }
    return -1;
}

void chooseZombiePlayer()
{
    int zPic = SelectZombiePlayer();

    if (zPic + 1)
    {
        UniPrintToAll("누군가 좀비가 되었습니다 ...!!");
        PlayerClassZombieFlagSet(zPic);
        UpdateAlivePlayerCount();
        Enchant(player[zPic], "ENCHANT_LIGHT", 0.0);
        setCountdown(500);
    }
}

int movingCase(int arg_0)
{
    int pack[6], i;

    if (arg_0 == -1)
    {




        for(i = 0 ; i < 3; i += 1)
        {




            pack[i] = CreateObject("MovableBookcase2", 3);
            pack[i + 3] = CreateObject("MovableBookcase2", 4);
            Frozen(pack[i], 1);
            Frozen(pack[i + 3], 1);
            TeleportLocationVector(3, 23.0, 23.0);
            TeleportLocationVector(4, 23.0, 23.0);
        }
        return 0;
    }
    return pack[arg_0];
}





int getTempVar(int unk1, int unk2)
{

    //0: reference, 1:change, 2:reset

    int unkarr[2];

    if (unk1 == 0)
        return unkarr[unk2];

    else if (unk1 == 1)
    {
        unkarr[unk2] = 1;
        return 0;
    }

    unkarr[unk2] = 0;
    return 0;


}



void closeBookcaseWalls(int order)
{
    if (order == 3)
    {
        WallClose(Wall(109, 145));
        WallClose(Wall(110, 146));
        WallClose(Wall(111, 147));
    }
    else
    {
        WallClose(Wall(95, 131));
        WallClose(Wall(96, 132));
        WallClose(Wall(97, 133));
    }
}





void openLeftBookcase()
{
    if (getTempVar(0, 0) == 0)
    {


        WallOpen(Wall(95, 131));
        WallOpen(Wall(96, 132));
        WallOpen(Wall(97, 133));
        FrameTimerWithArg(1, 0, movingLeftCase);
        getTempVar(1, 0);


    }


}


float maxRun(int value)
{
    if (value == 0)
    {
        return 2190.0;

    }

    else
    {
        return 2513.0;

    }
}


void movingLeftCase(int aaaa)
{
    int var_0;

    if (GetObjectX(movingCase(aaaa)) >= maxRun(aaaa) - 80.0)
    {
        int i;
        for (i = 0 ; i < 3; i += 1)
        {
            MoveObjectVector(movingCase(i + aaaa), -2.0, -2.0);
        }
        FrameTimerWithArg(1, aaaa, movingLeftCase);
    }
    else
    {
        FrameTimerWithArg(100, aaaa, backLeftcase);
        FrameTimerWithArg(100, aaaa, closeBookcaseWalls);
    }
}






void backLeftcase(int bbb)
{
    int i;
    
    if (GetObjectX(movingCase(bbb)) <= maxRun(bbb))
    {
        for(i = 0 ; i < 3; i += 1)
        {
            MoveObject(movingCase(i + bbb), 2.0, 2.0);


        }
        FrameTimerWithArg(1, bbb, backLeftcase);

    }

    else
    {
        getTempVar(2, bbb / 3);

    }
}


void openRightBookcase()
{
    if (getTempVar(0, 1) == 0)
    {
        WallOpen(Wall(109, 145));
        WallOpen(Wall(110, 146));
        WallOpen(Wall(111, 147));
        getTempVar(1, 1);
        FrameTimerWithArg(1, 3, movingLeftCase);

    }
}

void ResetToiletSecretWall()
{
    WallClose(Wall(66, 80));
    WallClose(Wall(67, 81));
    WallClose(Wall(68, 82));
}

void CountdownResetToiletWalls(int ptr)
{
    int count = GetDirection(ptr);

    while (IsObjectOn(ptr))
    {
        if (count)
        {
            FrameTimerWithArg(1, ptr, CountdownResetToiletWalls);
            LookWithAngle(ptr, count - 1);
            break;
        }
        ResetToiletSecretWall();
        Delete(ptr);
        break;
    }
}

void removeSecretWall()
{
    int ptr = SecretCount;

    if (IsObjectOn(ptr))
    {
        if (GetDirection(ptr))
            LookWithAngle(ptr, 200);
    }
    else
    {
        ptr = CreateObject("InvisibleLightBlueLow", 5);
        WallOpen(Wall(66, 80));
        WallOpen(Wall(67, 81));
        WallOpen(Wall(68, 82));
        LookWithAngle(ptr, 200);
        SecretCount = ptr;
        FrameTimerWithArg(3, ptr, CountdownResetToiletWalls);
    }
}

void teleport1a()
{
    MoveObject(other, 2751.0, 2655.0);
}

void teleport1b()
{
    MoveObject(other, 3057.0, 4465.0);
}

void teleport2a()
{
    MoveObject(other, 3437.0, 1698.0);
}



void teleport2b()
{
    MoveObject(other, 1848.0, 3990.0);
}

void teleport2c()
{
    MoveObject(other, 4940.0, 2744.0);
}



void teleport2d()
{
    MoveObject(other, 3466.0, 3651.0);
}


void teleport2e()
{
    MoveObject(other, 3270.0, 4251.0);
}


void teleport2f()
{
    MoveObject(other, 2768.0, 4780.0);
}


void teleport3a()
{
    MoveObject(other, 2772.0, 2547.0);
}


void teleport3b()
{
    MoveObject(other, 3645.0, 3464.0);
}


void teleport3c()
{
    MoveObject(other, 4090.0, 4078.0);
}


void teleport4a()
{
    MoveObject(other, 4847.0, 2619.0);
}



void putBookcase1F()
{
    int var_0 = 0;
    int var_1;

    if (var_1 == 1)
        MoveWaypoint(19, 2488.0, 3839.0);
    else if (var_1 == 2)
        MoveWaypoint(19, 2376.0, 3953.0);
    else if (var_1 == 3)
        MoveWaypoint(19, 2652.0, 4228.0);
    else if (var_1 == 4)
        MoveWaypoint(19, 2767.0, 4113.0);
    else if (var_1 == 5)
        MoveWaypoint(19, 2881.0, 3998.0);
    else if (var_1 == 6)
        return;
    FrameTimer(1, putBookcase1F);
    var_1 += 1;

    TeleportLocation(20, LocationX(19) + 13.0, LocationY(19 ) - 13.0);

    while (var_0 < 6)
    {
        CreateObjectAt("MovableBookcase2", LocationX(19), LocationY(19));
        TeleportLocationVector(19, 23.0, 23.0);
        CreateObjectAt("MovableBookcase4", LocationX(20), LocationY(20));
        TeleportLocationVector(20, 23.0, 23.0);
        var_0 += 1;

    }

}
void putBookcase3F()
{


    int i;
    int var_1;

    if (var_1 < 13)
    
    {
        var_1 += 1;
        FrameTimer(1, putBookcase3F);
    }

    MoveWaypoint(22, GetWaypointX(21) - 13.0, GetWaypointY(21) - 13.0);

    for (i = 0 ; i < 5; i += 1)
    {
        CreateObjectAt("MovableBookcase1", LocationX(21), LocationY(21));
        TeleportLocationVector(21, -23.0, 23.0);
        CreateObjectAt("MovableBookcase3", LocationX(22), LocationY(22));
        TeleportLocationVector(22, -23.0, 23.0);
        
    }


    setBookcase3F(var_1, 21);


}





void setBookcase3F(int someorder, int wp)
{
    if (someorder == 1)
    {
        MoveWaypoint(wp, 3191.0, 1170.0);
    }
    else if (someorder == 2)
    {
        MoveWaypoint(wp, 3285.0, 1261.0);
    }
    else if (someorder == 3)
    {
        MoveWaypoint(wp, 3536.0, 1006.0);
    }
    else if (someorder == 4)
    {
        MoveWaypoint(wp, 3628.0, 1102.0);
    }
    else if (someorder == 5)
    {
        MoveWaypoint(wp, 3378.0, 1354.0);
    }
    else if (someorder == 6)
    {
        MoveWaypoint(wp, 3468.0, 1444.0);
    }
    else if (someorder == 7)
    {
        MoveWaypoint(wp, 3722.0, 1191.0);
    }
    else if (someorder == 8)
    {
        MoveWaypoint(wp, 3903.0, 1375.0);
    }
    else if (someorder == 9)
    {
        MoveWaypoint(wp, 3654.0, 1627.0);
    }
    else if (someorder == 10)
    {
        MoveWaypoint(wp, 3745.0, 1719.0);
    }
    else if (someorder == 11)
    {
        MoveWaypoint(wp, 3998.0, 1468.0);
    }
    else if (someorder == 12)
    {
        MoveWaypoint(wp, 4090.0, 1562.0);
    }
    else if (someorder == 13)
    {
        MoveWaypoint(wp, 3837.0, 1812.0);
    }
}


void setMapLighting(int cur, int max)
{
    int i;

    for(i = cur; i < max ; i += 1)
    {
        CreateObject("InvisibleLightBlueHigh", i);

    }
}

void StrZombieTeamWin()
{
	int arr[23], i, count = 0;
	string name = "HealOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 2116297724; arr[1] = 1338015748; arr[2] = 143688064; arr[3] = 537002000; arr[4] = 1648494850; arr[5] = 3539023; arr[6] = 151569161; arr[7] = 50725121; 
	arr[8] = 669007908; arr[9] = 1073742852; arr[10] = 278919327; arr[11] = 1072699377; arr[12] = 1107296770; arr[13] = 4; arr[14] = 150931464; arr[15] = 2080505362; 
	arr[16] = 570695713; arr[17] = 134481992; arr[18] = 135299208; arr[19] = 537927999; arr[20] = 1069743648; arr[21] = 8356864; arr[22] = 524414; 
	for (i = 0 ; i < 23 ; i ++)
		count = DrawStrZombieTeamWin(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrZombieTeamWin(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 713 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 64 == 63)
			MoveWaypoint(1, GetWaypointX(1) - 126.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void StrHumanTeamWin()
{
	int arr[23], i, count = 0;
	string name = "HealOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 2116543036; arr[1] = 1338015748; arr[2] = 143657224; arr[3] = 537002000; arr[4] = 1648370722; arr[5] = 3539023; arr[6] = 956338313; arr[7] = 50725121; 
	arr[8] = 570573348; arr[9] = 1073742852; arr[10] = 67694751; arr[11] = 1072699377; arr[12] = 236978690; arr[13] = 4; arr[14] = 8390664; arr[15] = 2080505360; 
	arr[16] = 1074012193; arr[17] = 134481920; arr[18] = 1081480; arr[19] = 537927682; arr[20] = 1069743648; arr[21] = 8357880; arr[22] = 524414; 
	for (i = 0 ; i < 23 ; i ++)
		count = DrawStrHumanTeamWin(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrHumanTeamWin(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 713 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 64 == 63)
			MoveWaypoint(1, GetWaypointX(1) - 126.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void Str1Floor()
{
	int arr[7], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 3161096; arr[1] = 69198080; arr[2] = 60563480; arr[3] = 2143551489; arr[4] = 1619001377; arr[5] = 4228111; arr[6] = 504; 
	for (i = 0 ; i < 7 ; i ++)
		count = DrawStr1Floor(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStr1Floor(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 217 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 18 == 17)
			MoveWaypoint(1, GetWaypointX(1) - 34.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void Str2Floor()
{
	int arr[7], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 8928284; arr[1] = 270525504; arr[2] = 60571672; arr[3] = 2143420417; arr[4] = 1612709897; arr[5] = 4235023; arr[6] = 504; 
	for (i = 0 ; i < 7 ; i ++)
		count = DrawStr2Floor(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStr2Floor(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 217 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 18 == 17)
			MoveWaypoint(1, GetWaypointX(1) - 34.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void Str3Floor()
{
	int arr[7], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 8928284; arr[1] = 270525440; arr[2] = 60583960; arr[3] = 2144337924; arr[4] = 1646264449; arr[5] = 4230671; arr[6] = 504; 
	for (i = 0 ; i < 7 ; i ++)
		count = DrawStr3Floor(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStr3Floor(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 217 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 18 == 17)
			MoveWaypoint(1, GetWaypointX(1) - 34.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void Str4Floor()
{
	int arr[13], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 1109910142; arr[1] = 151027844; arr[2] = 2116034849; arr[3] = 134762436; arr[4] = 41031425; arr[5] = 75776068; arr[6] = 1058535169; arr[7] = 16450; 
	arr[8] = 2114981888; arr[9] = 67379075; arr[10] = 2131234944; arr[11] = 33824770; arr[12] = 2114453632; 
	for (i = 0 ; i < 13 ; i ++)
		count = DrawStr4Floor(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStr4Floor(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 37 == 36)
			MoveWaypoint(1, GetWaypointX(1) - 72.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void PlaceStampString()
{
    MoveWaypoint(1, GetWaypointX(63), GetWaypointY(63));
    Str4Floor();
    MoveWaypoint(1, GetWaypointX(18), GetWaypointY(18));
    Str1Floor();
    MoveWaypoint(1, GetWaypointX(61), GetWaypointY(61));
    Str2Floor();
    MoveWaypoint(1, GetWaypointX(62), GetWaypointY(62));
    Str3Floor();
}

void PlaceEndGameText(int zombieTeamWin)
{
    MoveWaypoint(1, GetWaypointX(69), GetWaypointY(69));
    if (zombieTeamWin)
        StrZombieTeamWin();
    else
        StrHumanTeamWin();
}