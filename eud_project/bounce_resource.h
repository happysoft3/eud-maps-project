
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"
#include "libs/clientside.h"
#include "libs/playerinfo.h"

void ImageRightArrowYellow()
{ }

void ImageRightArrowBlue()
{ }

void ImageLeftArrowYellow()
{ }

void ImageLeftArrowBlue()
{ }

void imageBrick()
{ }

void imageBrick2()
{ }

void imageCongrate()
{ }

void imageKeylayout()
{ }

void MapBgmData()
{ }

int makeSolidTile(short colr){
    short *ret;

    AllocSmartMemEx(2116,&ret);
    int r=1058;
    while (--r>=0)
        ret[r]=colr;
    return ret;
}

// void EnableTileTexture(int enabled)
// {
//     //0x5acd50
//     int *p = 0x5acd50;

//     if (p[0]==enabled)
//         return;
        
//     char code[]={0xB8, 0x15, 0xB7, 0x4C, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC3,};

//     invokeRawCode(code, NULLPTR);
    
// }

// void onUpdateTileChanged()
// {
//     EnableTileTexture(FALSE);
//     // SolidTile(628, 6,0x07FA);
//     EnableTileTexture(TRUE);
// }

void initializeCustomTileset(int imgVector){
    AppendImageFrame(imgVector, makeSolidTile(0x067E), 101);
    AppendImageFrame(imgVector, makeSolidTile(0x07A8), 95);
    AppendImageFrame(imgVector, makeSolidTile(0x8C1D), 96);
    AppendImageFrame(imgVector, makeSolidTile(0x875B), 97);
    AppendImageFrame(imgVector, makeSolidTile(0x3634), 98);
    ForceUpdateTileChanged();
}

#define BOUNDBALL_IMAGE_START_AT 133943
void GRPDump_BounceBallOutput(){}
void initializeBounceballOutputImageFrame(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WIZARD;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BounceBallOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  BOUNDBALL_IMAGE_START_AT, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy4Directions( BOUNDBALL_IMAGE_START_AT, IMAGE_SPRITE_MON_ACTION_SLAIN, 0);
}

void GRPDumpStartPanel(){}
void initializeStartPanelImage(int vec){
    int *frames, *sizes, thingId=OBJ_WHITE_WIZARD_TALK_3;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDumpStartPanel)+4, thingId, NULLPTR, &frames,&sizes);
    int imgNo=113047;
    AppendImageFrame(vec,frames[0], imgNo++);
    AppendImageFrame(vec,frames[0], imgNo++);
    AppendImageFrame(vec,frames[1], imgNo++);
    AppendImageFrame(vec,frames[1], imgNo++);
    AppendImageFrame(vec,frames[2], imgNo++);
    AppendImageFrame(vec,frames[2], imgNo++);
    AppendImageFrame(vec,frames[3], imgNo++);
    AppendImageFrame(vec,frames[3], imgNo++);
}

void InitializeResources()
{
    int imgVector=CreateImageVector(2048);

    AppendImageFrame(imgVector, GetScrCodeField(imageBrick)+4, 125167);
    AppendImageFrame(imgVector, GetScrCodeField(imageBrick2)+4, 125154);
    AppendImageFrame(imgVector, GetScrCodeField(imageCongrate) + 4, 132315);
    AppendImageFrame(imgVector, GetScrCodeField(imageKeylayout) + 4, 131930);
    AppendImageFrame(imgVector, GetScrCodeField(ImageRightArrowBlue) + 4, 133682);
    AppendImageFrame(imgVector, GetScrCodeField(ImageRightArrowYellow) + 4, 133683);
    AppendImageFrame(imgVector, GetScrCodeField(ImageLeftArrowBlue) + 4, 133680);
    AppendImageFrame(imgVector, GetScrCodeField(ImageLeftArrowYellow) + 4, 133681);
    initializeBounceballOutputImageFrame(imgVector);
    initializeCustomTileset(imgVector);
    initializeStartPanelImage(imgVector);
    DoImageDataExchange(imgVector);
    ExtractMapBgm("..\\bounceball.mp3", MapBgmData);
}
