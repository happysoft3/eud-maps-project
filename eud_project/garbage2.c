
#include "libs/define.h"
#include "libs/coopteam.h"
#include "libs/playerupdate.h"
#include "libs/printutil.h"
#include "libs/format.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/waypoint.h"
#include "libs/fixtellstory.h"
#include "libs/playerinfo.h"
#include "libs/buff.h"
#include "libs/spellutil.h"
#include "libs/unitstruct.h"
#include "libs/itemproperty.h"
#include "libs/hash.h"
#include "libs/potionex.h"
#include "libs/bind.h"
#include "libs/weaponcapacity.h"

#define PAY_METEOR_SHOWER 50000

int m_potHash;
int m_HCenterUnit;
string *m_weaponNameTable;
int m_weaponNameTableLength;
string *m_armorNameTable;
int m_armorNameTableLength;
short m_monPropertyFn[97];
string *m_potionNameTable;
int m_potionNameTableLength;
string *m_staffNameTable;
int m_staffNameTableLength;

int player[20], LastUnitID = 1165;
string StageUnit[90];
int StageHp[90];
int m_CurStage = 1;
int OrbPtr, Skill_time, Skill_marker;
int Num_fst, Num_scd;

int g_dropItemFunction[10];

int g_itemList;

/*
Waypoints Setting
1~4: 플레이어 입장
5: 입장불가 시 플레이어 이동지점
6: 트리거 웨이포인트(초기: MathSine)
7: 트리거 웨이포인트
8~32: 25개 유닛소환 지점
33: 스탬프 스트링: 운석소나기
34: 스탬프 스트링: 상점
35,36: redOrb
37,38: blueOrb
39,40: greenOrb
41: (맵에서 안보이는 곳에 배치) 망각의 지팡이 배치될 지점
*/

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int WizardRedBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;  
		arr[17] = 100; arr[18] = 30; arr[19] = 50; 
		arr[20] = 1045220557; arr[21] = 1061158912; arr[23] = 32; arr[24] = 1067869798; 
		arr[26] = 4;
		arr[37] = 1751607628; arr[38] = 1852403316; arr[39] = 1819230823; 
		arr[40] = 116; arr[53] = 1073741824;
		arr[55] = 12; arr[56] = 20;
		link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 80; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[27] = 0; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680;
        link = arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101;
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146;
		arr[50] = 0; arr[51] = 0; arr[52] = 0; arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;

		link = arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[58] = 0; arr[59] = 5542784;  
		link = arr;
	}
	return link;
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 300; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 2048; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 50; arr[30] = 1092616192; arr[32] = 19; arr[33] = 27; 
		arr[57] = 5548288; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void LichLordSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2048);
		//SetMemory(GetMemory(ptr + 0x22c), 300);
		//SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichLordBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int BlackWidowBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801;
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; arr[61] = 45071360; 
		link = arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 64; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; 
		arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 20; arr[56] = 28; 
		arr[58] = 5545472; 
	pArr = &arr;
	return pArr;
}

void FireSpriteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 64;	hpTable[1] = 64;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = FireSpriteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

static void initMonsterProperties()
{
    m_monPropertyFn[5] = MonsterGoonProcess;
    m_monPropertyFn[72] = MonsterStrongWhiteWizProcess;
    m_monPropertyFn[30] = MonsterWeirdlingBeastProcess;
    m_monPropertyFn[34] = MonsterBlackWidowProcess;
    m_monPropertyFn[6] = MonsterBear2Process;
    m_monPropertyFn[12] = FireSpriteSubProcess;
    m_monPropertyFn[73] = MonsterWizardRedProcess;
    m_monPropertyFn[80] = LichLordSubProcess;
}

void PlayAll(short soundId)
{
    int u=10;

    while (--u>=0)
    {
        if (CurrentHealth(player[u]))
            PlaySoundAround(player[u], soundId);
    }
}

void MonsterProcessProto(int fid, int unit)
{
    Bind(fid, &fid+4);
}

void CheckMonsterThing(int unit)
{
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (thingID)
    {
        if (m_monPropertyFn[key] != 0)
            MonsterProcessProto(m_monPropertyFn[key], unit);
    }
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

int CreateMoverFix(int targetUnit, int destLocation, float speed)
{
    int unitMover = CreateMover(targetUnit, destLocation, speed), unitPtr = UnitToPtr(targetUnit);
    int movPtr;

    if (unitMover)
    {
        movPtr = UnitToPtr(unitMover);
        if (movPtr && unitPtr)
        {
            SetMemory(unitPtr + 0x28, LastUnitID);
            movPtr = GetMemory(movPtr + 0x2ec);
            SetMemory(movPtr + 0x20, LastUnitID);
        }
    }
    LastUnitID ++;

    return unitMover;
}

static void InitItemNames()
{
    int weapons[]={"WarHammer", "GreatSword", "Sword", "Longsword", "StaffWooden", 
    "OgreAxe", "BattleAxe", "FanChakram", "RoundChakram",
            "OblivionHalberd", "OblivionHeart", "OblivionWierdling", "CrossBow", "Quiver", "Bow", "MorningStar"};
    int armors[]={"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", 
    "PlateLeggings", "ChainCoif", "ChainLeggings", "ChainTunic",
            "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", 
            "LeatherHelm", "LeatherLeggings", "MedievalCloak",
            "MedievalPants", "MedievalShirt", "StreetShirt", "StreetSneakers", "StreetPants", "ConjurerHelm", "WizardHelm", "WizardRobe"};
    int pots[]={"RedPotion", "RedPotion", "CurePoisonPotion", "VampirismPotion", 
    "ShieldPotion", "InvisibilityPotion", "InvulnerabilityPotion",
            "ShieldPotion", "HastePotion", "FireProtectPotion", "ShockProtectPotion", 
            "PoisonProtectPotion", "YellowPotion", "BlackPotion", 
            "AmuletofManipulation", "BottleCandle", "Fear", "FoilCandleLit"};
    int wands[]={"DeathRayWand", "LesserFireballWand", "FireStormWand", "ForceWand", 
    "InfinitePainWand", "SulphorousFlareWand", "SulphorousShowerWand"};

    m_weaponNameTable=weapons;
    m_armorNameTable=armors;
    m_potionNameTable=pots;
    m_staffNameTable=wands;
    m_weaponNameTableLength=sizeof(weapons);
    m_armorNameTableLength=sizeof(armors);
    m_potionNameTableLength=sizeof(pots);
    m_staffNameTableLength=sizeof(wands);
}

static void putEnchantedWeapon(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
    Delete(posUnit);
    int weapon=CreateObjectAt(m_weaponNameTable[Random(0, m_weaponNameTableLength-1)], x,y);

    if (m_CurStage>7)
        SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    
    int thingId = GetUnitThingID(weapon);

    if (thingId>=222&&thingId<=225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId==1178||1168==thingId)
        SetConsumablesWeaponCapacity(weapon, 255, 255);

    DropItemHandler(weapon);
}

static void putEnchantedArmor(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
    Delete(posUnit);
    int armor=CreateObjectAt(m_armorNameTable[Random(0, m_armorNameTableLength-1)], x,y);

    if (m_CurStage>9)
        SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));

    DropItemHandler(armor);
}

void InitDropItemFunctions()
{
    g_dropItemFunction[0] = putEnchantedWeapon;
    g_dropItemFunction[6] = PutPotion;
    g_dropItemFunction[1] = PutGold;
    g_dropItemFunction[2] = PutArmor;
    g_dropItemFunction[3] = PutWeapon;
    g_dropItemFunction[4] = PutMagicalStaff;
    g_dropItemFunction[5] = PutHpPotion;
    g_dropItemFunction[7] = PutSilverKey;
    g_dropItemFunction[8] = putEnchantedArmor;
    g_dropItemFunction[9] = PutHpPotion;
}

void DropItemHandler(int item)
{
    int node = CreateObjectAt("ImaginaryCaster", GetObjectX(item), GetObjectY(item));

    Raise(CreateObjectAt("ImaginaryCaster", GetObjectX(node), GetObjectY(node)), item);
    NoxListAppend(g_itemList, node);
}

static void initMapPicket()
{
    int pay=
    PAY_METEOR_SHOWER;
    char buff[128];

    NoxSprintfString(buff, "전장에 메테오샤워 뿌리기. %d 골드입니다", &pay, 1);
    RegistSignMessage(Object("mpic1"), ReadStringAddressEx(buff));
    RegistSignMessage(Object("mpic2"), "망각의 지팡이를 제공하는 우물- 그대신 3색 구슬을 가져와야 한다");
    RegistSignMessage(Object("mpic3"), "-각종 상점과 휴게실이 있는 곳-");
    RegistSignMessage(Object("mpic4"), "표지판에는 \"전사 탈의실\"이라고 적혀있네요");
    RegistSignMessage(Object("mpic5"), "지친 마술사를 위하여 준비해 보았어~ 마술사 전용 휴게실");
    RegistSignMessage(Object("mpic6"), "여기는 소환술사 휴게공간 입니다");
    RegistSignMessage(Object("mpic7"), "-가비지-시즌2-   원작- Joel Kirk (Prophet). 2차 수정- DEMONOPHOBIA. 최종 수정- 231");
    RegistSignMessage(Object("mpic8"), "-가비지-시즌2- 이 벽을 열었다는 것은, 이 지도를 클리어 하셨군요! 대단한 열정이었어요");
}

static int hashYellowPotion(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
    Delete(posUnit);
    return PotionExCreateYellowPotion(x,y, 125);
}

static int hashWhitePotion(int posUnit)
{
    float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
    Delete(posUnit);
    return PotionExCreateWhitePotion(x,y, 100);
}

static int hashBlackPotion(int posUnit)
{
    int ret= PotionExCreateBlackPotion(GetObjectX(posUnit),GetObjectY(posUnit), 85);
    Delete(posUnit);
    return ret;
}

static void onUseGoHomeAmulet()
{
    int home = m_HCenterUnit;

    if (IsVisibleTo(OTHER, home) || IsVisibleTo(home, OTHER))
    {
        UniPrint(OTHER, "공간이동 실패: 이미 목적지 주변에 있음");
        return;
    }
    Delete(SELF);
    Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    MoveObject(OTHER, GetObjectX(home), GetObjectY(home));
    Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    PlaySoundAround(OTHER, SOUND_BlindOff);
}

static int hashGoHomeAmulet(int amulet)
{
    SetUnitCallbackOnUseItem(amulet, onUseGoHomeAmulet);
    return amulet;
}

void UnitTeleportToUnitAndHealBuffFx(int sUnit, int dUnit)
{
    int *dUnitPtr = UnitToPtr(dUnit);

    if (dUnitPtr != NULLPTR)
    {
        float xProfile = ToFloat(dUnitPtr[14]);
        float yProfile = ToFloat(dUnitPtr[15]);
        MoveObject(sUnit, xProfile, yProfile);
        Effect("GREATER_HEAL", xProfile, yProfile, xProfile, yProfile - 160.0);
    }
}

void BuffHealingHealth(int sUnit)
{
    int owner, count = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        owner = GetOwner(sUnit);
        if (CurrentHealth(owner) && count)
        {
            UnitTeleportToUnitAndHealBuffFx(sUnit, owner);
            RestoreHealth(owner, 2);
            LookWithAngle(sUnit, count - 1);
            FrameTimerWithArg(2, sUnit, BuffHealingHealth);
            break;
        }
        Delete(sUnit);
        break;
    }
}

void onUseHealingPotion()
{
    int sUnit;

    if (CurrentHealth(OTHER))
    {
        Delete(SELF);
        if (HasEnchant(OTHER, "ENCHANT_VILLAIN")) return;
        sUnit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(OTHER, sUnit);
        LookWithAngle(sUnit, 225);
        FrameTimerWithArg(1, sUnit, BuffHealingHealth);
        Enchant(OTHER, "ENCHANT_VILLAIN", 15.0);
        Enchant(sUnit, "ENCHANT_RUN", 0.0);
        UniPrint(OTHER, "힘을내요 미스터리~ (15 초간 체력회복 속도 증가)");
    }
}

static int hashHealingPotion(int amulet)
{
    SetUnitCallbackOnUseItem(amulet, onUseHealingPotion);
    return amulet;
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectAt("WillOWisp", xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

static void onFearUse()
{
    Delete(SELF);
    Enchant(OTHER, "ENCHANT_SHOCK", 120.0);
    CastSpellObjectObject("SPELL_CURE_POISON",OTHER,OTHER);
    RestoreHealth(OTHER, 45);
    WispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
}

static int hashFear(int fear)
{
    SetUnitCallbackOnUseItem(fear, onFearUse);
    return fear;
}

static void onUseFoilCandle()
{
    Delete(SELF);
    CastSpellObjectObject("SPELL_FORCE_OF_NATURE",OTHER, OTHER);
    UniChatMessage(OTHER,"괴물녀석, 이거나 먹어라!", 150);
}

static int hashFoilCandle(int item)
{
    SetUnitCallbackOnUseItem(item, onUseFoilCandle);
    return item;
}

static void initpotHash()
{
    HashCreateInstance(&m_potHash);
    HashPushback(m_potHash, 639, hashYellowPotion);
    HashPushback(m_potHash, 640, hashWhitePotion);
    HashPushback(m_potHash, 641, hashBlackPotion);
    HashPushback(m_potHash, 1184, hashGoHomeAmulet);
    HashPushback(m_potHash, 1197, hashHealingPotion);
    HashPushback(m_potHash, 239, hashFear);
    HashPushback(m_potHash, 1199, hashFoilCandle);
}

void MapInitialize()
{
    //Init_
    MusicEvent();
    GetMaster();
    m_HCenterUnit=CreateObject("BlueRain", 52);
    InitItemNames();
    initMonsterProperties();
    InitDropItemFunctions();
    DefStageUnitName();
    initpotHash();

    //delay_run
    FrameTimer(1, InitOrbPlace);
    FrameTimer(2, InitMeteorSkill);
    FrameTimer(3, InitNumberDisplay);
    SecondTimerWithArg(30, 0, StartCurrentStage);

    FrameTimer(1, MakeCoopTeam);
    FrameTimer(3, SpecialMarketInit);
    initMapPicket();

    g_itemList = NoxListInstance();

    //Loop_run
    FrameTimer(20, PlayerClassOnLoop);
}

void MapExit()
{
    MusicEvent();
    HashDeleteInstance(m_potHash);
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void InitNumberDisplay()
{
    FloatTable(-1);
    NumberData(0);
    Num_fst = NumberOrb(44);
    Num_scd = NumberOrb(45);
    PutNumberOnMap(0);
}

void PutNumberOnMap(int num)
{
    if (num < 100)
        DisplayNumber(Num_fst, 43, NumberData(num / 10));
    else
        DisplayNumber(Num_fst, 43, NumberData(0));
    DisplayNumber(Num_scd, 46, NumberData(num % 10));
}

int NumberOrb(int location)
{
	int orb, k;

	if (location)
	{
		orb = CreateObjectAt("InvisibleLightBlueHigh", LocationX(location), LocationY(location));
		for (k = 0 ; k < 28 ; k ++)
			ObjectOff(CreateObjectAt("ManaBombOrb", GetObjectX(orb + k) + 1.0, GetObjectY(orb + k)));
        Raise(orb, ToFloat(location));
	}
    else
        return 0;

	return orb + 1;
}

void DisplayNumber(int orb, int loc, int bytes)
{
	float pos_x = LocationX(loc), pos_y = LocationY(loc);
	int idx = 0, k, wp = ToInt(GetObjectZ(orb - 1));

	for (k = 1 ; !(k & 0x10000000) ; k <<= 1)
	{
		if (bytes & k)
			MoveObject(orb + idx, pos_x, pos_y);
		else
			MoveObject(orb + idx, GetWaypointX(wp) + FloatTable(idx), GetWaypointY(wp));
		if (idx % 4 == 3)
		{
			pos_x = GetWaypointX(loc);
			pos_y += 2.0;
		}
		else
			pos_x += 2.0;
		idx ++;
	}
}

float FloatTable(int num)
{
	float arr[28], count;
	int k;

	if (num < 0)
	{
		count = 27.0;
		for (k = 27 ; k >= 0 ; k --)
		{
			arr[k] = count;
			count -= 1.0;
		}
		return ToFloat(0);
	}
	return arr[num];
}

int NumberData(int num)
{
	int data[10];

	if (!data[0])
	{
		data[0] = 110729622; data[1] = 239354980; data[2] = 252799126; data[3] = 110643350; data[4] = 143194521;
		data[5] = 110659359; data[6] = 110719382; data[7] = 71583903; data[8] = 110717334; data[9] = 110684566;
		return 0;
	}
	return data[num];
}

void InitMeteorSkill()
{
    int target = Object("Buy_getmonster");

    Skill_time = 0;
    MoveWaypoint(6, GetObjectX(target), GetObjectY(target));
    Skill_marker = CreateObject("BlueSummons", 6);
}

void ActivateMeteorShower()
{
    char buff[128];

    if (!Skill_time)
    {
        if (GetGold(OTHER) >= PAY_METEOR_SHOWER)
        {
            ChangeGold(OTHER, -PAY_METEOR_SHOWER);
            Skill_time = 1;
            Delete(Skill_marker);
            SecondTimerWithArg(5, 60, StartMeteorShowerTime);
            int pay = PAY_METEOR_SHOWER;

            NoxSprintfString(buff, "전장에 메테오 샤워를 시전합니다, 가격: %d골드, 쿨다운: 300초", &pay, 1);
            UniPrintToAll(ReadStringAddressEx(buff));
            FrameTimerWithArg(3, GetCaller(), ShotMeteor);
        }
        else
        {
            int require = PAY_METEOR_SHOWER-GetGold(OTHER);

            NoxSprintfString(buff, "금액이 %d 더 필요합니다", &require, 1);
            UniPrint(OTHER, ReadStringAddressEx(buff));
        }
    }
}

void StartMeteorShowerTime(int time)
{
    if (time)
    {
        PutNumberOnMap(time);
        SecondTimerWithArg(5, time - 1, StartMeteorShowerTime);
    }
    else
    {
        char buff[128];
        int pay = PAY_METEOR_SHOWER;

        InitMeteorSkill();
        NoxSprintfString(buff, "메테오 샤워를 사용가능 합니다, 사용금액: %d만원, 쿨다운: 300초", &pay, 1);
        UniPrintToAll(ReadStringAddressEx(buff));
    }
}

void DelayCastShower(int unit)
{
    CastSpellObjectObject("SPELL_METEOR_SHOWER", unit, unit);
    Delete(unit);
}

void ShotMeteor(int caster)
{
    int k, trap = CreateObject("InvisibleLightBlueHigh", 6);

    Delete(trap++);
    if (CurrentHealth(caster))
    {
        for (k = 0 ; k < 25 ; k ++)
        {
            SetOwner(caster, CreateObject("Wizard", k + 8));
            FrameTimerWithArg(1, trap + k, DelayCastShower);
        }
    }
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 42);
        Frozen(unit, 1);
    }
    return unit;
}

void InitOrbPlace()
{
    OrbPtr = CreateObject("InvisibleLightBlueHigh", 1) + 1;
    CreateObject("RedOrb", Random(0, 1) + 35);
    CreateObject("BlueOrb", Random(0, 1) + 37);
    CreateObject("GreenOrb", Random(0, 1) + 39);
    Delete(OrbPtr - 1);
}

void conbineFinalStaff()
{
    int k, res = 0;

    for (k = 0 ; k < 3 ; k ++)
        res += HasItem(OTHER, OrbPtr + k);
    if (res == 3)
    {
        UniPrint(OTHER, "오브를 모두 모으셨습니다");
        Delete(OrbPtr);
        Delete(OrbPtr + 1);
        Delete(OrbPtr + 2);
        SetUnitHealth(OTHER, 2000);
        int ptr = CreateObject("InvisibleLightBlueHigh", 1);
        CreateObject("InvisibleLightBlueHigh", 1);

        int staff=CreateObjectAt("OblivionOrb", GetObjectX(OTHER), GetObjectY(OTHER));

        SetItemPropertyAllowAllDrop(staff);
        DisableOblivionItemPickupEvent(staff);
        Raise(ptr, ToFloat(staff));
        Raise(ptr + 1, ToFloat(GetCaller()));
        FrameTimerWithArg(1, ptr, DelayPickToPlayer);
    }
    else
    {
        UniPrint(OTHER, "빨간색, 파란색, 녹색 구슬을 모두 가져오시오");
    }
}

void DelayPickToPlayer(int ptr)
{
    int owner = ToInt(GetObjectZ(ptr + 1));
    if (CurrentHealth(owner))
    {
        Pickup(owner, ToInt(GetObjectZ(ptr)));
    }
    else
        Delete(ToInt(GetObjectZ(ptr)));
    Delete(ptr);
    Delete(ptr + 1);
}

void DefStageUnitName()
{
    StageUnit[0] = "Imp"; StageUnit[1] = "Bat"; StageUnit[2] = "SmallAlbinoSpider";
    StageHp[0] = 20; StageHp[1] = 30; StageHp[2] = 30 | (1 << 16);

    StageUnit[3] = "Urchin"; StageUnit[4] = "Wasp"; StageUnit[5] = "Imp";
    StageHp[3] = 50; StageHp[4] = 50; StageHp[5] = 40;

    StageUnit[6] = "Urchin"; StageUnit[7] = "FlyingGolem"; StageUnit[8] = "GiantLeech";
    StageHp[6] = 50; StageHp[7] = 50 | ((2 | 4) << 16); StageHp[8] = 60;

    StageUnit[9] = "GiantLeech"; StageUnit[10] = "AlbinoSpider"; StageUnit[11] = "Spider";
    StageHp[9] = 90; StageHp[10] = 80; StageHp[11] = 90;

    StageUnit[12] = "Urchin"; StageUnit[13] = "Spider"; StageUnit[14] = "UrchinShaman";
    StageHp[12] = 90; StageHp[13] = 100; StageHp[14] = 85;

    StageUnit[15] = "UrchinShaman"; StageUnit[16] = "AlbinoSpider"; StageUnit[17] = "Scorpion";
    StageHp[15] = 97; StageHp[16] = 120; StageHp[17] = 150;

    StageUnit[18] = "Troll"; StageUnit[19] = "Scorpion"; StageUnit[20] = "SpittingSpider";
    StageHp[18] = 200; StageHp[19] = 200; StageHp[20] = 140;

    StageUnit[21] = "SpittingSpider"; StageUnit[22] = "Wolf"; StageUnit[23] = "FlyingGolem";
    StageHp[21] = 135; StageHp[22] = 150; StageHp[23] = 108 | ((2 | 4) << 16);

    StageUnit[24] = "Swordsman"; StageUnit[25] = "Archer"; StageUnit[26] = "WhiteWolf";
    StageHp[24] = 250 | (0x420 << 16); StageHp[25] = 120 | (0x520 << 16); StageHp[26] = 175;

    StageUnit[27] = "Swordsman"; StageUnit[28] = "Archer"; StageUnit[29] = "FlyingGolem";
    StageHp[27] = 275 | (0x420 << 16); StageHp[28] = 130 | (0x520 << 16); StageHp[29] = 120 | ((2 | 4) << 16);

    StageUnit[30] = "GruntAxe"; StageUnit[31] = "WhiteWolf"; StageUnit[32] = "BlackBear";
    StageHp[30] = 225; StageHp[31] = 180; StageHp[32] = 280;

    StageUnit[33] = "GruntAxe"; StageUnit[34] = "OgreBrute"; StageUnit[35] = "Bear";
    StageHp[33] = 250; StageHp[34] = 290; StageHp[35] = 325;

    StageUnit[36] = "OgreBrute"; StageUnit[37] = "OgreWarlord"; StageUnit[38] = "Bear";
    StageHp[36] = 225; StageHp[37] = 290; StageHp[38] = 350;

    StageUnit[39] = "WizardGreen"; StageUnit[40] = "OgreWarlord"; StageUnit[41] = "Bear";
    StageHp[39] = 128; StageHp[40] = 325; StageHp[41] = 350;

    StageUnit[42] = "WizardGreen"; StageUnit[43] = "BlackWidow"; StageUnit[44] = "Goon";    //error
    StageHp[42] = 175; StageHp[43] = 225 | (0x1d28 << 16); StageHp[44] = 120 | (0x6420 << 16);

    StageUnit[45] = "WillOWisp"; StageUnit[46] = "Shade"; StageUnit[47] = "BlackWidow";
    StageHp[45] = 225; StageHp[46] = 128; StageHp[47] = 250 | (0x1d28 << 16);

    StageUnit[48] = "Ghost"; StageUnit[49] = "Shade"; StageUnit[50] = "WillOWisp";
    StageHp[48] = 98 | (0x40 << 16); StageHp[49] = 192; StageHp[50] = 225;

    StageUnit[51] = "Beholder"; StageUnit[52] = "Ghost"; StageUnit[53] = "Zombie";
    StageHp[51] = 306; StageHp[52] = 125 | (0x40 << 16); StageHp[53] = 192;

    StageUnit[54] = "EvilCherub"; StageUnit[55] = "Skeleton"; StageUnit[56] = "VileZombie";
    StageHp[54] = 128; StageHp[55] = 295; StageHp[56] = 340;

    StageUnit[57] = "EvilCherub"; StageUnit[58] = "MeleeDemon"; StageUnit[59] = "SkeletonLord";
    StageHp[57] = 128; StageHp[58] = 192; StageHp[59] = 275;

    StageUnit[60] = "FireSprite"; StageUnit[61] = "MeleeDemon"; StageUnit[62] = "SkeletonLord";
    StageHp[60] = 98; StageHp[61] = 192; StageHp[62] = 275;

    StageUnit[63] = "FireSprite"; StageUnit[64] = "EvilCherub"; StageUnit[65] = "EmberDemon";
    StageHp[63] = 108; StageHp[64] = 98; StageHp[65] = 192;

    StageUnit[66] = "FireSprite"; StageUnit[67] = "EmberDemon"; StageUnit[68] = "VileZombie";
    StageHp[66] = 120; StageHp[67] = 192; StageHp[68] = 306;

    StageUnit[69] = "Wizard"; StageUnit[70] = "SkeletonLord"; StageUnit[71] = "Ghost";
    StageHp[69] = 192; StageHp[70] = 275; StageHp[71] = 130 | (0x40 << 16);

    StageUnit[72] = "Wizard"; StageUnit[73] = "EvilCherub"; StageUnit[74] = "MechanicalGolem";
    StageHp[72] = 192; StageHp[73] = 98; StageHp[74] = 625;

    StageUnit[75] = "Demon"; StageUnit[76] = "MeleeDemon"; StageUnit[77] = "MechanicalGolem";
    StageHp[75] = 400; StageHp[76] = 175; StageHp[77] = 650;

    StageUnit[78] = "Demon"; StageUnit[79] = "Mimic"; StageUnit[80] = "EvilCherub";
    StageHp[78] = 400; StageHp[79] = 700; StageHp[80] = 98;

    StageUnit[81] = "Horrendous"; StageUnit[82] = "Mimic"; StageUnit[83] = "StoneGolem";
    StageHp[81] = 400; StageHp[82] = 700; StageHp[83] = 650;

    StageUnit[84] = "WizardWhite"; StageUnit[85] = "MechanicalGolem"; StageUnit[86] = "StoneGolem";
    StageHp[84] = 350; StageHp[85] = 650; StageHp[86] = 650;

    StageUnit[87] = "Demon"; StageUnit[88] = "Horrendous"; StageUnit[89] = "LichLord";
    StageHp[87] = 400; StageHp[88] = 600; StageHp[89] = 485;
}

void GhostImage(int unit)
{
    if (CurrentHealth(unit))
    {
        MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
        FrameTimerWithArg(1, unit, GhostImage);
    }
    else
    {
        Delete(unit + 1);
    }
}

void StartCurrentStage(int num)
{
    if (m_CurStage < 30)
    {
        if (num < 25)
        {
            SummonUnit(num + 8);
            FrameTimerWithArg(1, num + 1, StartCurrentStage);
        }
    }
    else
        VictoryEvent();
}

void VictoryEvent()
{
    int k;
    DeleteObjectTimer(CreateObject("LevelUp", 20), 1800);
    DeleteObjectTimer(CreateObject("LevelUp", 8), 1800);
    DeleteObjectTimer(CreateObject("LevelUp", 32), 1800);
    DeleteObjectTimer(CreateObject("LevelUp", 12), 1800);
    DeleteObjectTimer(CreateObject("LevelUp", 28), 1800);
    for (k = 4 ; k >= 0 ; k --)
        WallOpen(Wall(110 + k, 138 + k));
    UniPrintToAll("축하합니다, 모든 스테이지를 클리어하셨습니다!, 최종 스테이지: 30");
    PlayAll(SOUND_FlagCapture);
}

void CheckUnitFlags(int unit, int idx)
{
    int flags = StageHp[idx] >> 16;

    if (flags & 0x1) //slowed
        Enchant(unit, "ENCHANT_SLOWED", 0.0);
    if (flags & 0x2) //for_flying_golem
        SetCallback(unit, 3, MecaFlierWeapon);
    if (flags & 0x4) //wide_sight
        SetUnitScanRange(unit, 400.0);
    if (flags & 0x8)
        SetCallback(unit, 5, SummonSmallSpiderWhenDead);
    // if (flags & 0x10)
    //     SetCallback(unit, 3, RedWizWeapon);
    if (flags & 0x40)
    {
        int fist = CreateObjectAt("SmallFist", GetObjectX(unit), GetObjectY(unit));

        UnitNoCollide(fist);
        Frozen(fist, TRUE);
        FrameTimerWithArg(1, unit, GhostImage);
    }
}

void SummonUnit(int wp)
{
    int idx = m_CurStage * 3 + Random(0, 2);
    int unit = CreateObject(StageUnit[idx], wp);

    SetOwner(GetMaster(), unit);
    CheckMonsterThing(unit);
    Raise(unit, 200.0);
    CheckUnitFlags(unit, idx);
    SetCallback(unit, 5, UnitDeaths);
    SetUnitMaxHealth(unit, StageHp[idx] & 0xffff);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
}

void UnitDeaths()
{
    int rnd = Random(0, sizeof(g_dropItemFunction)-1), itemMake;

    DeleteObjectTimer(SELF, 60);
    CheckPass();
    itemMake = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
    FrameTimerWithArg(1, itemMake, g_dropItemFunction[rnd]);
}

void MecaFlierWeapon()
{
    int mis;
    float pos_x, pos_y;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            pos_x = UnitRatioX(SELF, OTHER, 17.0);
            pos_y = UnitRatioY(SELF, OTHER, 17.0);
            TeleportLocation(6, GetObjectX(SELF), GetObjectY(SELF));
            Effect("DAMAGE_POOF", LocationX(6), LocationY(6), 0.0, 0.0);
            mis = CreateObjectAt("WeakArcherArrow", LocationX(6), LocationY(6));
            SetOwner(SELF, mis);
            LookAtObject(mis, OTHER);
            PushObjectTo(mis, -pos_x * 2.0, -pos_y * 2.0);
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.7);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(22, GetTrigger(), ResetUnitSight);
        }
    }
}

void RedWizWeapon()
{
    int unit;
    float pos_x, pos_y;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            pos_x = UnitRatioX(SELF, OTHER, 20.0);
            pos_y = UnitRatioY(SELF, OTHER, 20.0);
            unit = CreateObjectAt("Maiden", GetObjectX(SELF) - pos_x, GetObjectY(SELF) - pos_y);
            CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
            CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
            Frozen(unit, 1);
            SetCallback(unit, 9, ParserGunTouched);
            SetOwner(SELF, unit + 1);
            Raise(unit + 1, pos_x);
            Raise(unit + 2, pos_y);
            Enchant(SELF, "ENCHANT_ETHEREAL", 2.0);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(30, GetTrigger(), ResetUnitSight);
        }
    }
}

void RWizParserGun(int ptr)
{
    int owner = GetOwner(ptr + 1), count = GetDirection(ptr + 1);

    if (CurrentHealth(owner) && count < 30)
    {
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr + 1), GetObjectY(ptr) - GetObjectZ(ptr + 2));
        Effect("SENTRY_RAY", GetObjectX(ptr) + GetObjectZ(ptr + 1), GetObjectY(ptr) + GetObjectZ(ptr + 2), GetObjectX(ptr), GetObjectY(ptr));
        LookWithAngle(ptr + 1, count + 1);
        FrameTimerWithArg(1, ptr, RWizParserGun);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void ParserGunTouched()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner) && !HasEnchant(OTHER, "ENCHANT_FREEZE"))
    {
        Damage(OTHER, owner, 30, 16);
        Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
    }
}

void SummonSmallSpiderWhenDead()
{
    float xpos = GetObjectX(SELF), ypos = GetObjectY(SELF);

    CreateObjectAt("ArachnaphobiaFocus", xpos, ypos);
    DeleteObjectTimer(CreateObjectAt("BigSmoke", xpos, ypos), 9);
    PlaySoundAround(SELF, SOUND_BeholderDie);
    PlaySoundAround(SELF, SOUND_PoisonTrapTriggered);
    UnitDeaths();
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.07);
    AggressionLevel(unit, 1.0);
}

int SetIndestructibleItemAll(int unit)
{
    int inv = GetLastItem(unit), resultCount = 0;

    while (IsObjectOn(inv))
    {
        if (!UnitCheckEnchant(inv, GetLShift(23)))
        {
            Enchant(inv, EnchantList(23), 0.0);
            resultCount +=1;
        }
        inv = GetPreviousItem(inv);
    }
    return resultCount;
}

static void notifyEraseItemAfterAWhile()
{
    UniPrintToAll("잠시 후 전장이 청소될 예정이니 필요한 아이템을 어서 챙기세요");
    PlayAll(SOUND_JournalEntryAdd);
}

void CheckPass()
{
    int count;
    char buff[128];

    if (++count >= 25)
    {
        m_CurStage ++;
        NoxSprintfString(buff, "이번 스테이지를 완료했습니다, 잠시 후 다음 스테이지 %d이 시작됩니다", &m_CurStage, 1);
        UniPrintToAll(ReadStringAddressEx(buff));
        SecondTimerWithArg(15, 0, StartCurrentStage);
        SecondTimer(14, ClearAllItems);
        SecondTimer(5, notifyEraseItemAfterAWhile);
        count = 0;
        PlayAll(SOUND_SoulGateTouch);
    }
}

static int createPotProto(int fId, int posUnit)
{
    return Bind(fId, &fId+4);
}

void PutPotion(int itemMake)
{
    float x=GetObjectX(itemMake),y=GetObjectY(itemMake);

    Delete(itemMake);
    int pot = CreateObjectAt(m_potionNameTable[Random(0, m_potionNameTableLength-1)], x,y);
    int features;
    if (HashGet(m_potHash, GetUnitThingID(pot), &features, FALSE))
    {
        pot = createPotProto(features, pot);
    }
    DropItemHandler(pot);
}

void PutGold(int itemMake)
{
    int gold = CreateObjectAt(TreasureList(), GetObjectX(itemMake), GetObjectY(itemMake));

    UnitStructSetGoldAmount(gold, Random(500, 3000));
    DropItemHandler(gold);
    Delete(itemMake);
}

void PutArmor(int itemMake)
{
    DropItemHandler(CreateObjectAt(ArmorTable(Random(0, 16)), GetObjectX(itemMake), GetObjectY(itemMake)));
    Delete(itemMake);
}

void PutWeapon(int itemMake)
{
    DropItemHandler(CreateObjectAt(WeaponTable(Random(0, 11)), GetObjectX(itemMake), GetObjectY(itemMake)));
    Delete(itemMake);
}

void PutMagicalStaff(int itemMake)
{
    float x=GetObjectX(itemMake),y=GetObjectY(itemMake);
    Delete(itemMake);
    DropItemHandler(CreateObjectAt(m_staffNameTable[Random(0, m_staffNameTableLength-1)],x,y));
}

void PutHpPotion(int itemMake)
{
    DropItemHandler(CreateObjectAt("RedPotion", GetObjectX(itemMake), GetObjectY(itemMake)));
    Delete(itemMake);
}

void PutSilverKey(int itemMake)
{
    int item = 0;

    if (Random(0, 4) == 0)
        item = CreateAnyKey(itemMake, 1, "실버열쇠가 떨어집니다");

    Delete(itemMake);
    // if (item)
    //     DropItemHandler(item);
}

void PlayerClassGiveAllEnchant(int plr)
{
    int pUnit = player[plr];

    Enchant(pUnit, EnchantList(9), 0.0);
    Enchant(pUnit, EnchantList(13), 0.0);
    Enchant(pUnit, EnchantList(21), 0.0);
    Enchant(pUnit, EnchantList(17), 0.0);
    Enchant(pUnit, EnchantList(18), 0.0);
    Enchant(pUnit, EnchantList(20), 0.0);
    Enchant(pUnit, EnchantList(27), 0.0);
}

static void showInitialMessage(int pUnit)
{
    if (CurrentHealth(pUnit))
    {
        UniPrint(pUnit, "가비지 시즌2--  원작- Joel Kirk (Prophet). 제작- DEMONOPHOBIA");
        UniPrint(pUnit, "가비지 시즌2--  전장에 등장하는 모든 괴물을 처치하세요. 모두 30탄 까지 있습니다");
    }
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    if (GetGold(pUnit))
        ChangeGold(pUnit, -GetGold(pUnit));
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    SecondTimerWithArg(5, pUnit, showInitialMessage);

    return plr;
}

void PlayerRegist()
{
    int k, plr;

    while (TRUE)
    {
        if (CurrentHealth(OTHER))
        {
            plr = CheckPlayer();
            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    plr = PlayerClassOnInit(k, GetCaller());
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerJoin(plr);
                break;
            }
        }
        CantJoin();
        break;
    }
}

void PlayerJoin(int plr)
{
    int wp = Random(1, 4);

    if (PlayerClassDeathFlagCheck(plr))
        PlayerClassDeathFlagSet(plr);
    if (PlayerClassAllEnchantFlagCheck(plr))
        PlayerClassGiveAllEnchant(plr);

    MoveObject(player[plr], GetWaypointX(wp), GetWaypointY(wp));
    DeleteObjectTimer(CreateObject("BlueRain", wp), 12);
    AudioEvent("BlindOff", wp);
}

void CantJoin()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    MoveObject(OTHER, GetWaypointX(5), GetWaypointY(5));
    UniPrint(OTHER, "이 맵은 지원되지 않습니다");
}

void PlayerClassOnAlive(int plr)
{
    int pUnit = player[plr], arr[10];

    if (UnitCheckEnchant(pUnit, GetLShift(31)))
    {
        WindBoost(pUnit);
        RemoveTreadLightly(pUnit);
        EnchantOff(pUnit, EnchantList(31));
    }

    if (IsPoisonedUnit(pUnit))
    {
        if (PlayerClassAllEnchantFlagCheck(plr))
        {
            CastSpellObjectObject("SPELL_CURE_POISON", player[plr], player[plr]);
            Effect("VIOLET_SPARKS", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
        }
        else if (arr[plr] < 30)
            arr[plr] += IsPoisonedUnit(pUnit);
        else
        {
            Damage(pUnit, 0, IsPoisonedUnit(pUnit), 5);
            arr[plr] = 0;
        }
    }
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (TRUE)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i);
                    break;
                }
                else
                {
                    if (PlayerClassDeathFlagCheck(i)) 1;
                    else
                    {
                        PlayerClassDeathFlagSet(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void WindBoost(int caster)
{
    Enchant(caster, "ENCHANT_RUN", 0.3);
    PushObjectTo(caster, UnitAngleCos(caster, 77.0), UnitAngleSin(caster, 77.0));
    Effect("RICOCHET", GetObjectX(caster), GetObjectY(caster), 0.0, 0.0);
}

int PlayerClassDeathFlagCheck(int plr)
{
    return player[plr + 10] & 2;
}

void PlayerClassDeathFlagSet(int plr)
{
    player[plr + 10] ^= 2;
}

int PlayerClassAllEnchantFlagCheck(int plr)
{
    return player[plr + 10] & 4;
}

void PlayerClassAllEnchantFlagSet(int plr)
{
    player[plr + 10] ^= 4;
}

int CheckPlayer()
{
    int k;
    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

int DummyUnitCreateAt(string unitname, object location)
{
    int unit = CreateObjectAt(unitname, LocationX(location), LocationY(location));

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

string TreasureList()
{
    string table[] = {"QuestGoldChest", "QuestGoldPile", "Gold"};

    return table[Random(0, 2)];
}

string WeaponTable(int num)
{
    string table[] = {"WarHammer", "GreatSword", "RoundChakram", "Quiver", "OgreAxe", "MorningStar", "Sword", "Longsword",
    "CrossBow", "Bow", "StaffWooden", "FanChakram"};
    return table[ num]; //amount: 0-11
}

string ArmorTable(int num)
{
    string table[] = {"ConjurerHelm", "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", "LeatherLeggings",
    "WizardHelm", "WizardRobe", "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "MedievalCloak",
    "MedievalPants", "MedievalShirt", "SteelShield"};

    return table[num]; //amount: 0-16
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObject("RottenMeat", 41));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void SpreadOrb(string orbName, object location)
{
    int u;
    float speed = 2.3;

    for ( u = 0 ; u < 60 ; u+=1 )
        LinearOrbMove(CreateObjectAt(orbName, LocationX(location), LocationY(location)), MathSine((u * 6) + 90, -12.0), MathSine(u * 6, -12.0), speed, 10);
}

void ShopClassAllEnchantDesc()
{
    TellStoryUnitName("aa", "cmd_token:ability", "올엔첸트 배우기");
    UniPrint(OTHER, "유용한 엔첸트를 항상 지속시키는 능력 '올 엔첸트' 을 구입하시겠어요?");
    UniPrint(OTHER, "이 작업은 5만 골드를 요구합니다. 계속하려면 '예'를 누르세요");
}

void ShopClassAllEnchantTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;
    
    if (GetGold(OTHER) >= 50000)
    {
        int plr = CheckPlayer();

        if (plr >= 0)
        {
            if (PlayerClassAllEnchantFlagCheck(plr))
                UniPrint(OTHER, "당신은 이미 이 능력을 지녔습니다");
            else
            {
                ChangeGold(OTHER, -50000);
                PlayerClassAllEnchantFlagSet(plr);
                PlayerClassGiveAllEnchant(plr);
                TeleportLocation(48, GetObjectX(OTHER), GetObjectY(OTHER));
                SpreadOrb("CharmOrb", 48);
                UniPrint(OTHER, "올 엔첸트 능력을 구입하셨습니다");
            }
        }
    }
    else
        UniPrint(OTHER, "거래가 취소 되었습니다. 잔액이 부족합니다");
}

void ShopClassIndestructibleItemDesc()
{
    TellStoryUnitName("aa", "cmd_token:ability", "인벤토리 무적");
    UniPrint(OTHER, "당신이 소유한 모든 아이템을 파괴불가 상태로 만들어 줄 것입니다");
    UniPrint(OTHER, "이 작업은 15000골드가 필요합니다. 계속하려면 '예'를 누르세요");
}

void ShopClassIndestructibleItemTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;
    if (GetGold(OTHER) >= 15000)
    {
        if (SetIndestructibleItemAll(OTHER))
        {
            ChangeGold(OTHER, -15000);
            UniPrint(OTHER, "거래완료!");
        }
        else
            UniPrint(OTHER, "더 이상 처리할 아이템이 없습니다");
    }
    else
        UniPrint(OTHER, "거래가 취소 되었습니다. 잔액이 부족합니다");
}

void SpecialMarketInit()
{
    int allenchant = DummyUnitCreateAt("WizardGreen", 50);

    SetDialog(allenchant, "YESNO", ShopClassAllEnchantDesc, ShopClassAllEnchantTrade);

    int invShop = DummyUnitCreateAt("Swordsman", 51);

    SetDialog(invShop, "YESNO", ShopClassIndestructibleItemDesc, ShopClassIndestructibleItemTrade);
}

string ItemClassKeyType(int typeNumb)
{
    string keys[] = {"SapphireKey", "SilverKey", "GoldKey", "RubyKey"};

    return keys[typeNumb % 4];
}

void ItemClassKeyDropFunction()
{
    int owner = GetOwner(SELF), keyType = GetDirection(GetTrigger() + 1), keyDesc = ToInt(GetObjectZ(GetTrigger() + 1));
    int key, kDir = GetDirection(SELF);

    Delete(SELF);
    Delete(GetTrigger() + 1);
    key = CreateAnyKey(owner, keyType, ToStr(keyDesc));
    UniChatMessage(key, ToStr(keyDesc), 150);
    Raise(key, 100.0);
    LookWithAngle(key, kDir);
    PlaySoundAround(key, 821);
}

int CreateAnyKey(int sUnit, int keyType, string keyDescFunc)
{
    int sKey = CreateObjectAt(ItemClassKeyType(keyType), GetObjectX(sUnit), GetObjectY(sUnit));

    LookWithAngle(CreateObjectAt("ImaginaryCaster", GetObjectX(sKey), GetObjectY(sKey)), keyType);
    Raise(sKey + 1, keyDescFunc);
    SetUnitCallbackOnDiscard(sKey, ItemClassKeyDropFunction);
    return sKey;
}

int CreateAnyKeyAtLocation(int sLocation, int keyType, string keyDescFunc)
{
    int sKey = CreateObjectAt(ItemClassKeyType(keyType), LocationX(sLocation), LocationY(sLocation));

    LookWithAngle(CreateObjectAt("ImaginaryCaster", GetObjectX(sKey), GetObjectY(sKey)), keyType);
    Raise(sKey + 1, keyDescFunc);
    SetUnitCallbackOnDiscard(sKey, ItemClassKeyDropFunction);
    return sKey;
}

void ClearItemSingle(int node)
{
    int data = ToInt(GetObjectZ(node + 1));

    if (!GetOwner(data))
        Delete(data);
    Delete(node);
    Delete(node + 1);
}

void ClearAllItems()
{
    NoxListForeach(g_itemList, ClearItemSingle);
}

int NoxListInstance()
{
    int head = CreateObjectAt("ImaginaryCaster", LocationX(1), LocationY(1));

    return head;
}

int NoxListNext(int node)
{
    StopScript(GetObjectZ(node));
}

void NoxListSetNext(int node, int nextNode)
{
    Raise(node, nextNode);
}

void NoxListAppend(int head, int node)
{
    NoxListSetNext(node, NoxListNext(head));
    NoxListSetNext(head, node);
}

int NoxListGetFirst(int head)
{
    return NoxListNext(head);
}

void NoxListForeach(int head, object function)
{
    int cur = NoxListNext(head), temp;

    while (cur)
    {
        temp = cur;
        cur = NoxListNext(cur);
        CallFunctionWithArg(function, temp);
    }
}
