
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"

#define IMAGE_VECTOR_MAX_COUNT 2048

void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
}

void GRPDump_GoldChikenWingsOutput(){}
#define GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID 113171
void initializeImageFrameGoldChikenWings(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GREEN_FROG;
    int inc[]={0,-13};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_GoldChikenWingsOutput)+4, thingId, inc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     GOLDCHIKENWINGS_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void initializeRenameItems(){
    ChangeSpriteItemNameAsString(OBJ_WHITE_POTION, "백색 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_WHITE_POTION, "마력과 체력을 100 회복한다");
    ChangeSpriteItemNameAsString(OBJ_BLACK_POTION, "검은 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_BLACK_POTION, "체력을 85회복한다");
    ChangeSpriteItemNameAsString(OBJ_YELLOW_POTION, "노란 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_YELLOW_POTION, "체력을 125 회복한다");
    ChangeSpriteItemNameAsString(OBJ_FEAR, "초사이언");
    ChangeSpriteItemDescriptionAsString(OBJ_FEAR, "쇼크 엔첸트를 걸어줍니다");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_MANIPULATION, "안전지대로 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_MANIPULATION, "안전지대로 이동합니다");
    ChangeSpriteItemNameAsString(OBJ_ANKH_TRADABLE, "정의의 십자가");
    ChangeSpriteItemDescriptionAsString(OBJ_ANKH_TRADABLE, "주변 범위에 피해를 줍니다");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_COMBAT, "곰덫 교환권");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_COMBAT, "사용 시 바닥에 곰덫을 설치합니다");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_NATURE, "독가스함정 교환권");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_NATURE, "사용 시 바닥에 독가스 함정을 설치합니다");
    ChangeSpriteItemNameAsString(OBJ_CANDLE_1, "광화문 촛불");
    ChangeSpriteItemDescriptionAsString(OBJ_CANDLE_1, "체력 풀 회복+맹독치료 효능을 지녔다");
    ChangeSpriteItemNameAsString(OBJ_BOTTLE_CANDLE, "힘을내요 미스터리");
    ChangeSpriteItemDescriptionAsString(OBJ_BOTTLE_CANDLE, "잠시동안 체력 회복속도 업!");
}

void InitializeResources()
{
    int vec=CreateImageVector(IMAGE_VECTOR_MAX_COUNT);

    initializeImageFrameSoldier(vec);
    initializeImageFrameGoldChikenWings(vec);
    DoImageDataExchange(vec);
    initializeRenameItems();
}
