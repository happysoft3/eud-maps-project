
#include "r231103_utils.h"
#include "Libs/buff.h"
#include "libs/bind.h"
#include "libs/unitstruct.h"
#include "libs/mathlab.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"

void fixDamage(int target,int me,int dam,int ty)
{
    int *p=0x979720;
    int t[]={p[0],p[1]};
    Damage(target,me,dam,ty);
    p[0]=t[0];
    p[1]=t[1];
}

void onFlareCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 42, DAMAGE_TYPE_PLASMA);
            Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}
#include"libs/wallutil.h "
void shotLevel1(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_IMP_SHOT, GetObjectX(owner),GetObjectY(owner));

    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onFlareCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,19.0),UnitAngleSin(owner,19.0));
    PlaySoundAround(owner, SOUND_FlareWand);
}

void onWeakArrowCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 47, DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void shotLevel2(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_WEAK_ARCHER_ARROW, GetObjectX(owner),GetObjectY(owner));

    SetOwner(owner,shot);
    LookWithAngle(shot,GetDirection(owner));
    SetUnitCallbackOnCollide(shot, onWeakArrowCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,22.0),UnitAngleSin(owner,22.0));
    PlaySoundAround(owner, SOUND_BowShoot);
}

void onLv3Collide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 51, DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION,GetObjectX(SELF),GetObjectY(SELF)), 9);
        Delete(SELF);
        break;
    }
}

void shotLevel3(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_WEAK_FIREBALL, GetObjectX(owner),GetObjectY(owner));

    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onLv3Collide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,26.0),UnitAngleSin(owner,26.0));
    PlaySoundAround(owner, SOUND_EmberDemonHitting);
}

void onFireballSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(OTHER, SELF))
                fixDamage(OTHER, SELF, 52, DAMAGE_TYPE_FLAME);
        }
    }
}

void onFireballCollide()
{
    int owner;

    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            owner=GetOwner(SELF);

            if (CurrentHealth(owner))
            {
                SplashDamageAtEx(owner, GetObjectX(SELF),GetObjectY(SELF), 48.0, onFireballSplash);
            }
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        DeleteObjectTimer( CreateObjectById(OBJ_EXPLOSION,GetObjectX(SELF),GetObjectY(SELF)), 9);
        Delete(SELF);
        break;
    }
}

void shotLevel4(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_FIREBALL, GetObjectX(owner),GetObjectY(owner));

    SetOwner(owner,shot);
    LookWithAngle(shot,GetDirection(owner));
    SetUnitCallbackOnCollide(shot, onFireballCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,28.0),UnitAngleSin(owner,28.0));
    PlaySoundAround(owner, SOUND_EmberDemonHitting);
}

void onLv5Collide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 58, DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void shotLevel5(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_MERC_ARCHER_ARROW, GetObjectX(owner),GetObjectY(owner));

    LookWithAngle(shot,GetDirection(owner));
    SetUnitEnchantCopy(shot,GetLShift(ENCHANT_RUN));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onLv5Collide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,31.0),UnitAngleSin(owner,31.0));
    PlaySoundAround(owner, SOUND_BowShoot);
}

void onLv6Collide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 64, DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void shotLevel6(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_MERC_ARCHER_ARROW, GetObjectX(owner),GetObjectY(owner));

    LookWithAngle(shot,GetDirection(owner));
    SetUnitEnchantCopy(shot,GetLShift(ENCHANT_HASTED));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onLv6Collide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,35.0),UnitAngleSin(owner,35.0));
    PlaySoundAround(owner, SOUND_ForceOfNatureRelease);
}

void onLv7Collide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 68, DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        WispDestroyFX(GetObjectX(SELF),GetObjectY(SELF));
        Delete(SELF);
        break;
    }
}

void shotLevel7(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_SPIDER_SPIT, GetObjectX(owner),GetObjectY(owner));

    LookWithAngle(shot,GetDirection(owner));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onLv7Collide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,38.0),UnitAngleSin(owner,38.0));
    PlaySoundAround(owner, SOUND_LargeSpiderSpit);
}

void onLv8Collide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 72, DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void shotLevel8(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_DEATH_BALL_FRAGMENT, GetObjectX(owner),GetObjectY(owner));

    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onLv8Collide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,40.0),UnitAngleSin(owner,40.0));
    PlaySoundAround(owner, SOUND_ForceOfNatureRelease);
}

void onPixieCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 74, DAMAGE_TYPE_PLASMA);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void shotLevel9(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_PIXIE, GetObjectX(owner),GetObjectY(owner));
    int ptr=UnitToPtr(shot);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onPixieCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,41.0),UnitAngleSin(owner,41.0));
    PlaySoundAround(owner, SOUND_PixieHit);
}

void onBigFireSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(OTHER, SELF))
                fixDamage(OTHER, SELF, 75, DAMAGE_TYPE_FLAME);
        }
    }
}

void onBigFireCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            int owner=GetOwner(SELF);
            if (CurrentHealth(owner))
                SplashDamageAtEx(owner,GetObjectX(SELF),GetObjectY(SELF),61.0, onBigFireSplash);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        DeleteObjectTimer(CreateObjectById(OBJ_EXPLOSION, GetObjectX(SELF),GetObjectY(SELF)), 9);
        Delete(SELF);
        break;
    }
}

void shotLevelA(int wand, int owner)
{
    int shot=CreateObjectById(OBJ_TITAN_FIREBALL, GetObjectX(owner),GetObjectY(owner));

    LookWithAngle(shot,GetDirection(owner));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onPixieCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,42.0),UnitAngleSin(owner,42.0));
    PlaySoundAround(owner, SOUND_FireballWand);
}

void onHarpoonCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 76, DAMAGE_TYPE_PLASMA);
            break;
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Effect("SPARK_EXPLOSION",GetObjectX(SELF),GetObjectY(SELF),0.0,0.0);
        Delete(SELF);
        break;
    }
}

void shotLevelB(int wand,int owner)
{
    int shot=CreateObjectById(OBJ_HARPOON_BOLT, GetObjectX(owner),GetObjectY(owner));
    int ptr=UnitToPtr(shot);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    LookWithAngle(shot,GetDirection(owner));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onHarpoonCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,43.0),UnitAngleSin(owner,43.0));
    PlaySoundAround(owner, SOUND_CrossBowShoot);
}

void onFonCollide()
{
    float x,y;
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER, SELF, 80, DAMAGE_TYPE_DRAIN);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        x=GetObjectX(SELF);
        y=GetObjectY(SELF);
        GreenSparkFx(x,y);
        CreateToxicCloud(x,y,GetOwner( SELF),18);
        Delete(SELF);
        break;
    }
}

void shotLevelC(int wand,int owner)
{
    int shot=CreateObjectById(OBJ_DEATH_BALL, GetObjectX(owner),GetObjectY(owner));
    int ptr=UnitToPtr(shot);

    SetMemory(ptr + 0x2e8, 5483536); //projectile update
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onFonCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,43.0),UnitAngleSin(owner,43.0));
    PlaySoundAround(owner, SOUND_ForceOfNatureRelease);
}

void onLightSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(OTHER, SELF))
            {
                Effect("LIGHTNING",GetObjectX(SELF),GetObjectY(SELF),GetObjectX(OTHER),GetObjectY(OTHER));
                fixDamage(OTHER, SELF, 84, 9);
            }
        }
    }
}

void onLightingBoltCollide()
{
    float x,y;
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            if (CurrentHealth(GetOwner(SELF)))
                SplashDamageAtEx(GetOwner(SELF),GetObjectX(SELF),GetObjectY(SELF),40.0, onLightSplash);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        x=GetObjectX(SELF);
        y=GetObjectY(SELF);
        GreenSparkFx(x,y);
        CreateToxicCloud(x,y,GetOwner( SELF),18);
        Delete(SELF);
        break;
    }
}

void shotLevelD(int wand,int owner)
{
    int shot=CreateObjectById(OBJ_LIGHTNING_BOLT, GetObjectX(owner),GetObjectY(owner));

    LookWithAngle(shot,GetDirection(owner));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onLightingBoltCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,46.0),UnitAngleSin(owner,46.0));

}

void onArcherBoltCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER,SELF,120,DAMAGE_TYPE_CRUSH);
            break;
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void shotLevelE(int wand,int owner)
{
    int shot=CreateObjectById(OBJ_ARCHER_BOLT, GetObjectX(owner),GetObjectY(owner));

    LookWithAngle(shot,GetDirection(owner));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onArcherBoltCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,52.0),UnitAngleSin(owner,52.0));
    PlaySoundAround(owner, SOUND_CrossBowShoot);
}

void onOgreShurikenCollide()
{
    while (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, SELF))
        {
            fixDamage(OTHER,SELF,100,DAMAGE_TYPE_CRUSH);
        }
        else if (!GetCaller())
            WallUtilDestroyWallAtObjectPosition(SELF);
        else
            break;
        Delete(SELF);
        break;
    }
}

void shotLevelF(int wand,int owner)
{
    int shot=CreateObjectById(OBJ_OGRE_SHURIKEN, GetObjectX(owner),GetObjectY(owner));

    LookWithAngle(shot,GetDirection(owner));
    SetOwner(owner,shot);
    SetUnitCallbackOnCollide(shot, onOgreShurikenCollide);
    SetUnitFlags(shot,GetUnitFlags(shot)^UNIT_FLAG_NO_COLLIDE_OWNER);
    PushObjectTo(shot,UnitAngleCos(owner,52.0),UnitAngleSin(owner,52.0));
}

void invokeShotLevel(short fn, int wand, int owner)
{
    Bind(fn,&fn+4);
}

void getOblivionProc(int lv, short *pDest)
{
    short *p;

    if (!p)
    {
        short proc[]={
            shotLevel1,shotLevel2,shotLevel3,shotLevel4,
            shotLevel5,shotLevel6,shotLevel7,shotLevel8,
            shotLevel9,shotLevelA,shotLevelB,shotLevelC,
            shotLevelD,shotLevelF,shotLevelE,shotLevelE,

        };
        p=proc;
    }
    if (pDest)
        pDest[0]=p[lv];
}

void ShotOblivionStaff(int wand, int owner)
{
    int curLv=GetUnit1C(wand);
    short proc=0;

    getOblivionProc(curLv, &proc);
    invokeShotLevel(proc, wand, owner);
}
