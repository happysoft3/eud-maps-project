
#include "ak180716_player.h"
#include "ak180716_reward.h"
#include "ak180716_resource.h"
#include "ak180716_mon.h"
#include "ak180716_shop.h"
#include "subway_initscan.h"
#include "libs/coopteam.h"
#include "libs/groupUtils.h"
#include "libs/game_flags.h"
#include "libs/winapi.h"

void placingStartTrigger(short locationId, short fn){
    int tr=DummyUnitCreateById(OBJ_BOMBER_GREEN,LocationX(locationId),LocationY(locationId));

    SetCallback(tr,9,fn);
}

void EndScan(int *pParams)
{
    int count =pParams[UNITSCAN_PARAM_COUNT];
    char msg[128];

    NoxSprintfString(msg, "%d개의 유닛을 스캔했습니다", &count, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
    // WallOpen(Wall(15, 235));
    // WallOpen(Wall(16, 236));
    // int lunit;
    // QueryLUnit(&lunit,0,0);
    // AwakeLineMonster(lunit);
    // SecondTimer(2, StartGameMent);
}

void startInitscan(){
    int initScanHash, lastUnit=GetMasterUnit();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, OnMonsterMarker);
    StartUnitScanEx(Object("firstscan"), lastUnit, initScanHash, EndScan);
}

void onWallOpened(int wall){
    float xy[2];

    WallOpen(wall);
    WallCoorToUnitCoor(wall,xy);
    Effect("SMOKE_BLAST",xy[0],xy[1],0.0,0.0);
}

void startRoom1(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom1Enter();
        }
    }
}

void startRoom2(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom2Enter();
        }
    }
}
void startRoom3(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom3Enter();
        }
    }
}
void startRoom4(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom4Enter();
        }
    }
}
void startRoom5(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom5Enter();
        }
    }
}
void startRoom6(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom6Enter();
        }
    }
}
void startRoom7(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom7Enter();
        }
    }
}
void startRoom8(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom8Enter();
        }
    }
}
void startRoom9(){
    if (GetTrigger()){
        if (IsPlayerUnit(OTHER)){
            Delete(SELF);
            OnRoom9Enter();
        }
    }
}

void decoWithDiamond(int count){
    float point[2];

    ComputeAreaRhombus(point, LocationX(63), LocationX(64), LocationY(65), LocationY(64));
    CreateObjectById(OBJ_DIAMOND,point[0],point[1]);
    if (--count>=0)
        PushTimerQueue(1,count,decoWithDiamond);
}

#define GROUP_rewardRoomWalls 11

void bossIsDead(){
    decoWithDiamond(64);
    RegistWallGroupCallback(onWallOpened);
    WallGroupSetting(GROUP_rewardRoomWalls, WALL_GROUP_ACTION_CALLBACK);
    UniPrintToAll("*  * ** 보스를 죽이셨네요! 승리! ** *  *");
}

void finalBossCheckLoop(int b){
    if (CurrentHealth(b)){
        PushTimerQueue(60,b,finalBossCheckLoop);
        return;
    }
    bossIsDead();
}

void initializeFinalBoss(){
    int b = CreateFinalMonster(LocationX(62),LocationY(62));

    AttachHealthbar(b);
    PushTimerQueue(3,b,finalBossCheckLoop);
}

#define GROUP_finalWalls 9

void tryOpenFinalWalls(){
    int r=9,b;
    while (--r>=0){
        QueryBaseOrb(r, &b,0);
        if (!IsObjectOn(b))
            return;
    }
    OnCenterHallEnter();
    initializeFinalBoss();
    WallGroupSetting(GROUP_finalWalls, WALL_GROUP_ACTION_OPEN);
    UniPrintToAll("** *  *지금 최종벽이 열렸습니다*  * **");
}

void turnOnPad1(){
    int b;
    QueryBaseOrb(0,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad2(){
    int b;
    QueryBaseOrb(1,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad3(){
    int b;
    QueryBaseOrb(2,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad4(){
    int b;
    QueryBaseOrb(3,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad5(){
    int b;
    QueryBaseOrb(4,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad6(){
    int b;
    QueryBaseOrb(5,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad7(){
    int b;
    QueryBaseOrb(6,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad8(){
    int b;
    QueryBaseOrb(7,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
void turnOnPad9(){
    int b;
    QueryBaseOrb(8,&b,0);
    ObjectOn(b);
    tryOpenFinalWalls();
}
#define GROUP_realFinalWalls 10
void turnOnCenterHallPad(){
    int count;

    if (++count >= 4){
        WallGroupSetting(GROUP_realFinalWalls,WALL_GROUP_ACTION_OPEN);
    }
}

void dispositionTeleporting(){
    DispositionTransport(LocationX(13),LocationY(13),LocationX(14),LocationY(14));
    DispositionTransport(LocationX(15),LocationY(15),LocationX(16),LocationY(16));
    placingStartTrigger(14, startRoom1);
    DispositionTransport(LocationX(17),LocationY(17),LocationX(18),LocationY(18));
    DispositionTransport(LocationX(19),LocationY(19),LocationX(20),LocationY(20));
    placingStartTrigger(18, startRoom2);
    DispositionTransport(LocationX(22),LocationY(22),LocationX(24),LocationY(24));
    DispositionTransport(LocationX(25),LocationY(25),LocationX(23),LocationY(23));
    placingStartTrigger(24, startRoom3);
    DispositionTransport(LocationX(26),LocationY(26),LocationX(28),LocationY(28));
    DispositionTransport(LocationX(29),LocationY(29),LocationX(27),LocationY(27));
    placingStartTrigger(28, startRoom4);
    DispositionTransport(LocationX(30),LocationY(30),LocationX(38),LocationY(38));
    DispositionTransport(LocationX(39),LocationY(39),LocationX(31),LocationY(31));
    placingStartTrigger(38, startRoom5);
    DispositionTransport(LocationX(41),LocationY(41),LocationX(33),LocationY(33));
    DispositionTransport(LocationX(32),LocationY(32),LocationX(40),LocationY(40));
    placingStartTrigger(40, startRoom6);
    DispositionTransport(LocationX(34),LocationY(34),LocationX(42),LocationY(42));
    DispositionTransport(LocationX(43),LocationY(43),LocationX(35),LocationY(35));
    placingStartTrigger(42, startRoom7);
    DispositionTransport(LocationX(36),LocationY(36),LocationX(44),LocationY(44));
    DispositionTransport(LocationX(45),LocationY(45),LocationX(37),LocationY(37));
    placingStartTrigger(44, startRoom8);
    DispositionTransport(LocationX(79),LocationY(79),LocationX(80),LocationY(80));
    DispositionTransport(LocationX(81),LocationY(81),LocationX(78),LocationY(78));
    placingStartTrigger(80, startRoom9);

    DispositionRectangle(LocationX(46),LocationY(46),turnOnPad1);
    DispositionRectangle(LocationX(47),LocationY(47),turnOnPad2);
    DispositionRectangle(LocationX(48),LocationY(48),turnOnPad3);
    DispositionRectangle(LocationX(49),LocationY(49),turnOnPad4);
    DispositionRectangle(LocationX(50),LocationY(50),turnOnPad5);
    DispositionRectangle(LocationX(51),LocationY(51),turnOnPad6);
    DispositionRectangle(LocationX(52),LocationY(52),turnOnPad7);
    DispositionRectangle(LocationX(53),LocationY(53),turnOnPad8);
    DispositionRectangle(LocationX(82),LocationY(82),turnOnPad9);

    DispositionRectangle(LocationX(54),LocationY(54),turnOnCenterHallPad);
    DispositionRectangle(LocationX(55),LocationY(55),turnOnCenterHallPad);
    DispositionRectangle(LocationX(56),LocationY(56),turnOnCenterHallPad);
    DispositionRectangle(LocationX(57),LocationY(57),turnOnCenterHallPad);

    DispositionTransport(LocationX(76),LocationY(76),LocationX(77),LocationY(77));
}

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

void initializeBaseOrb(){
    char buff[64];
    int count=9,r= 1;

    while (--count>=0){
        NoxSprintfString(buff,"baseorb%d", &r, 1);
        QueryBaseOrb((r++)-1, 0, Object(ReadStringAddressEx(buff)));
    }
}

void initializeReadables(){
    RegistSignMessage(Object("rd1"), "이 표지판에는 \"몬스터 박물관에 오신 것을 환영합니다 - 제작 237 - 버그제보 환영 ");
    RegistSignMessage(Object("rd2"), "지도에 입장을 하려면 이 길을 쭉 따라가시오");
    RegistSignMessage(Object("rd3"), "계속 인내하고 앞으로 가시오");
    RegistSignMessage(Object("rd4"), "거의 다 왔소, 조금 만 더 참고서 앞으로 가시오");
    RegistSignMessage(Object("rd5"), "!!몬스터 박물관으로 입장하는 곳!! 몬스터 박물관 주식회사 일동");
    RegistSignMessage(Object("rd6"), "보석 교환기 및 아이템을 내구도 무한으로 만들어 주는 곳");
    RegistSignMessage(Object("rd7"), "이 표지판에는 \"못배운 전사기술 마저 배우는 곳!!\"이라고 적혀져 있습니다");
    RegistSignMessage(Object("rd8"), "이 표지판에는 \"특수무기 상점\"이라고 적혀져 있습니다");
    RegistSignMessage(Object("rd9"), "이 표지판에는 \"이동속도 효과 갑옷\"이라고 적혀져 있습니다");
    RegistSignMessage(Object("rda"), "이 마술벽을 열기 위해서는, 9개의 문 뒤에 있는 던전에서 파란색 네모를 밟아 빨간색 네모로 만들면 된다");
    RegistSignMessage(Object("rdb"), "공간이동 장치를 사용하려면, 네모 위에서 가만히 서 있으세요. 취소하려면 이탈하시고");
    RegistSignMessage(Object("rdc"), "*    *  * ** 몬스터 박물관 v1.0 (제작 237) ** *  *    *");
    RegistSignMessage(Object("rdd"), "마술벽을 모두 열었겠다, 이제 보스 녀석을 잡으로 가 볼까요~!?");
    RegistSignMessage(Object("rde"), "이 맵을 클리어하셨습니다! 축하합니다!");
}

void OnInitializeMap(){
    MusicEvent();
    CreateLogFile("ak180716-log.txt");
    startInitscan();
    MakeCoopTeam();
    blockObserverMode();
    InitializePlayerSystem();
    dispositionTeleporting();
    InitializeMonster();
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
    initializeBaseOrb();
    placingInvisibleTeleport(58,60);
    placingInvisibleTeleport(61,59);
    InitializeShopSystem();
    initializeReadables();
    WriteLog("end OnInitializeMap");
}
void OnShutdownMap(){
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void InitializeClientOnly(){
    InitializeResource();
    InitializePopupMessages();
    ClientProcLoop();
}

#define GROUP_part9Walls 12

void OpenPart9(){
    ObjectOff(SELF);
    RegistWallGroupCallback(onWallOpened);
    WallGroupSetting(GROUP_part9Walls,WALL_GROUP_ACTION_CALLBACK);
}

#define part1SwitchWalls 0

void OpenWall1(){
    ObjectOff(SELF);
    WallGroupSetting(part1SwitchWalls, WALL_GROUP_ACTION_OPEN);
}
#define part1EastWalls 1
void OpenWallA1(){
    ObjectOff(SELF);
    WallGroupSetting(part1EastWalls, WALL_GROUP_ACTION_OPEN);
}
#define part1BlueWalls 2
void OpenWallA11(){
    ObjectOff(SELF);
    WallGroupSetting(part1BlueWalls, WALL_GROUP_ACTION_OPEN);
}

#define GROUP_part2LastWalls 3
#define GROUP_part2SecondWalls 4
#define GROUP_part2FirstWalls 5

void OpenWall2(){
    ObjectOff(SELF);
    RegistWallGroupCallback(onWallOpened);
    WallGroupSetting(GROUP_part2FirstWalls,WALL_GROUP_ACTION_CALLBACK);
}
void OpenWall21(){
    ObjectOff(SELF);
    RegistWallGroupCallback(onWallOpened);
    WallGroupSetting(GROUP_part2SecondWalls,WALL_GROUP_ACTION_CALLBACK);
}
void OpenWall22(){
    ObjectOff(SELF);
    RegistWallGroupCallback(onWallOpened);
    WallGroupSetting(GROUP_part2LastWalls,WALL_GROUP_ACTION_CALLBACK);
}
#define GROUP_part3HighWalls 6
#define GROUP_part3LowWalls 7

void OpenWalls3(){
    ObjectOff(SELF);
    int count;

    if (++count>=2){   
        WallGroupSetting(GROUP_part3LowWalls,WALL_GROUP_ACTION_OPEN);
        WallGroupSetting(GROUP_part3HighWalls,WALL_GROUP_ACTION_OPEN);
    }
}

#define GROUP_part4Walls 8

void OpenWalls4(){
    ObjectOff(SELF);
    int count;

    if (++count>=2){   
        WallGroupSetting(GROUP_part4Walls,WALL_GROUP_ACTION_OPEN);
    }
}

void ConfirmPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);

    if (pUnit)
        ShowQuestIntroOne(pUnit, 9999, "WizardChapterBegin11", "GeneralPrint:MapNameHecubahLair");
    if (pInfo==0x653a7c)
    {
        InitializeResource();
        InitializePopupMessages();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
