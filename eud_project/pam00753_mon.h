
#include "pam00753_mob_altspec.h"
#include "pam00753_gvar.h"

void onMonsterDeath()
{
	DeleteObjectTimer(SELF, 60);
}

int commonSpawnMonster(short id, float x,float y)
{
	int r=CreateObjectById(id,x,y);
	
	LoadMonsterAlternativeSpec(r);
	SetCallback(r,5,onMonsterDeath);
	RetreatLevel(r,0.0);
	AggressionLevel(r,1.0);
	SetOwner(GetMasterUnit(), r);
	return r;
}

void SpawnRoomSecond(int posUnit)
{
	short ids[]={OBJ_SWORDSMAN,OBJ_ARCHER,OBJ_WOLF,};
	
	commonSpawnMonster(ids[Random(0, sizeof(ids)-1)], GetObjectX(posUnit),GetObjectY(posUnit));
}

void SpawnRoomFirst(int posUnit)
{
	short ids[]={OBJ_WASP, OBJ_BAT, OBJ_URCHIN, OBJ_SMALL_ALBINO_SPIDER,};
	
	commonSpawnMonster(ids[Random(0, sizeof(ids)-1)], GetObjectX(posUnit),GetObjectY(posUnit));
}

void invokeSpawnRoom(int fn, int posUnit)
{
	Bind(fn, &posUnit);
}

void CreateMonsterMarker(int posUnit)
{
	float x=GetObjectX(posUnit),y=GetObjectY(posUnit);
	int *p=UnitToPtr(posUnit);
	int id = 0;
	
	if (p[0])
		id = GetMemory(p[0]);
	
	Delete(posUnit);
	if (id<'0')
		return;
		
	StoreMonsterMarker(id-'0', CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x, y));
}

void AppearMonsterAtMarker(int id)
{
	int mark= ReleaseMonsterMarker(id), cur;
	short fns[]={SpawnRoomFirst, SpawnRoomSecond,};
	
	while (mark)
	{
		cur=mark;
		mark=GetUnit1C(cur);
		invokeSpawnRoom(fns[id],cur);		
		Delete(cur);
	}
}
