
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/queuetimer.h"
#include "libs/hash.h"
#include "libs/objectIDdefines.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"

void GreenSparkFx(float xpos,float ypos)
{
    int ptr = CreateObjectAt("MonsterGenerator", xpos, ypos);

    PlaySoundAround(ptr, SOUND_AwardSpell);
    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void DeferredInvokeDisableObject(int obj)
{
    if (ToInt(GetObjectX(obj)))
        ObjectOff(obj);
}

void DeferredDisableObject(char delay, int obj)
{
    PushTimerQueue(delay, obj, DeferredInvokeDisableObject);
}

void onSplashUnitVisible()
{
    int core=GetUnit1C(SELF);

    if (ToInt(GetObjectX(core)))
    {
        if (DistanceUnitToUnit(SELF, OTHER)<=GetObjectZ(core))
        {
            int hash=GetUnit1C(core);

            if (!HashGet(hash, GetCaller(), NULLPTR, FALSE))
            {
                HashPushback(hash, GetCaller(), TRUE);
                Damage(OTHER, GetOwner(core), GetUnit1C(core+1), DAMAGE_TYPE_PLASMA);
            }
        }
    }
}

 void endSplashDamage(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int hash=GetUnit1C(sub);

        if (hash)
            HashDeleteInstance(hash);
        Delete(sub);
        Delete(sub+1);
    }
}

void SplashDamageAt(int owner, int dam, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
    int ptr2=CreateObjectAt("InvisibleLightBlueHigh", x,y);
    int hash;

    HashCreateInstance(&hash);
    SetUnit1C(ptr, hash);
    SetOwner(owner, ptr);
    PushTimerQueue(1, ptr, endSplashDamage);
    SetUnit1C(ptr2, dam);
    Raise(ptr, range);

    int sub[4];
    int k=sizeof(sub);
    while (--k>=0)
    {
        sub[k]=CreateObjectById(OBJ_WEIRDLING_BEAST, x, y);
        DeleteObjectTimer(sub[k], 1);
        UnitNoCollide(sub[k]);
        LookWithAngle(sub[k], k * 64);
        SetOwner(ptr, sub[k]);
        SetUnit1C(sub[k], ptr);
        SetCallback(sub[k], 3, onSplashUnitVisible);
    }
}
