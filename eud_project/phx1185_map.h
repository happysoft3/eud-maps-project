
#include "phx1185_utils.h"
#include "libs/waypoint.h"

int drawLineSingle(short locationId)
{
    int wall = checkAvataAroundWall(locationId);
    if (!wall)
        return 0;

    // WallOpen(wall);
    int bot=DummyUnitCreateById(OBJ_BENCH_6, LocationX(locationId),LocationY(locationId));

    UnitNoCollide(bot);
    Frozen(bot, TRUE);
    return bot;
}

void drawMapLine(short locationId)
{
    while (drawLineSingle(locationId))
        TeleportLocationVector(locationId, 46.0, 0.0);
}

void DrawMapPart3()
{
    PushTimerQueue(1,55,drawMapLine);
    PushTimerQueue(1,56,drawMapLine);
    PushTimerQueue(1,57,drawMapLine);
    PushTimerQueue(1,58,drawMapLine);
    PushTimerQueue(2,59,drawMapLine);
    PushTimerQueue(2,60,drawMapLine);
    PushTimerQueue(2,61,drawMapLine);
    PushTimerQueue(2,62,drawMapLine);
    PushTimerQueue(3,63,drawMapLine);
    PushTimerQueue(3,64,drawMapLine);
    PushTimerQueue(3,65,drawMapLine);
    PushTimerQueue(3,66,drawMapLine);
    PushTimerQueue(4,67,drawMapLine);
    PushTimerQueue(4,68,drawMapLine);
    PushTimerQueue(4,69,drawMapLine);
    PushTimerQueue(4,70,drawMapLine);
    PushTimerQueue(5,71,drawMapLine);
    PushTimerQueue(5,72,drawMapLine);
    PushTimerQueue(5,73,drawMapLine);
}

void DrawMapPart2()
{
    drawMapLine(36);
    drawMapLine(37);
    drawMapLine(38);
    drawMapLine(39);
    drawMapLine(40);
    PushTimerQueue(1,41,drawMapLine);
    PushTimerQueue(1,42,drawMapLine);
    PushTimerQueue(1,43,drawMapLine);
    PushTimerQueue(1,44,drawMapLine);
    PushTimerQueue(2,45,drawMapLine);
    PushTimerQueue(2,46,drawMapLine);
    PushTimerQueue(2,47,drawMapLine);
    PushTimerQueue(2,48,drawMapLine);
    PushTimerQueue(2,49,drawMapLine);
    PushTimerQueue(3,50,drawMapLine);
    PushTimerQueue(3,51,drawMapLine);
    PushTimerQueue(3,52,drawMapLine);
    PushTimerQueue(3,53,drawMapLine);
    PushTimerQueue(3,54,drawMapLine);
    PushTimerQueue(1,0,DrawMapPart3);
}

void DrawMapSub()
{
    drawMapLine(91);
    drawMapLine(90);
    drawMapLine(95);
    drawMapLine(96);
    drawMapLine(97);
    drawMapLine(98);
    drawMapLine(99);
    drawMapLine(100);
    PushTimerQueue(1, 0,DrawMapPart2);
}

void DrawMapPart1()
{
    drawMapLine(35);
    drawMapLine(74);
    drawMapLine(83);
    drawMapLine(84);
    drawMapLine(85);
    drawMapLine(86);
    drawMapLine(89);
    drawMapLine(92);
    PushTimerQueue(1, 0,DrawMapSub);
}

void DrawMapLobby()
{
    drawMapLine(114);
    drawMapLine(115);
    drawMapLine(116);
    drawMapLine(117);
    drawMapLine(118);
    PushTimerQueue(1, 0,DrawMapPart1);
}

void StartDrawMap()
{
    DrawMapLobby();
}
