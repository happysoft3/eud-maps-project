
#include "boss_utils.h"
#include "libs/buff.h"
#include "libs/spellutil.h"
#include "libs/printutil.h"

#define WINDBOOSTER_DISTANCE 70.0

void SkillSetWindBooster(int pUnit)
{
    EnchantOff(pUnit,EnchantList(ENCHANT_SNEAK));
    RemoveTreadLightly(pUnit);    
    PushObjectTo(pUnit, UnitAngleCos(pUnit, WINDBOOSTER_DISTANCE), UnitAngleSin(pUnit, WINDBOOSTER_DISTANCE));
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void onAutoDeathrayShot(){
    Effect("GREATER_HEAL", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
    RestoreHealth(GetOwner(SELF), 3);
}

void SkillAutoDeathray(int pUnit){
    UniChatMessage(pUnit, "이거나 받아라!", 30);
    EnchantOff(pUnit, EnchantList(ENCHANT_INFRAVISION));
    int sub=CreateObjectById(OBJ_WEIRDLING_BEAST,GetObjectX(pUnit),GetObjectY(pUnit));
    UnitNoCollide(sub);
    LookWithAngle(sub,GetDirection(pUnit));
    DeleteObjectTimer(sub,1);
    SetOwner(pUnit,sub);
    SetUnitScanRange(sub,400.0);
    SetCallback(sub,3,onAutoDeathrayShot);
}

void removeWarAbilityEffect(int pUnit, int aid)
{
    char *pcode;

    if (pcode==NULLPTR){
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x04, 0xFF, 0x30, 0xB8, 0x00, 0xC3, 0x4F, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x08, 0x31, 0xC0, 0xC3, 0x90, 0x90, 0x90
        };
        pcode=code;
    }
    int args[]={UnitToPtr(pUnit), aid,};
    invokeRawCode(pcode, args);
}

void SkillArrowTrap(int pUnit)
{
    float xvect = UnitAngleCos(pUnit, 9.0), yvect = UnitAngleSin(pUnit, 9.0);
    int posUnit = CreateObjectById(OBJ_MOVER, GetObjectX(pUnit) + xvect -(yvect*2.0), GetObjectY(pUnit) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(pUnit);

    while (++rep<5)
    {
        single= CreateObjectById(OBJ_ARCHER_ARROW, GetObjectX(posUnit), GetObjectY(posUnit));
        SetOwner(pUnit, single);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        LookWithAngle(single, dir);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
    UnitSetEnchantTime(pUnit,ENCHANT_FREEZE,5);
    removeWarAbilityEffect(pUnit,1);
}
