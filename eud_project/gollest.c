
#include "gollest_resource.h"
#include "gollest_bintable.h"
#include "gollest_sub.h"
#include "gollestSpecialWeapon.h"
#include "libs\networkRev.h"
#include "libs\playerupdate.h"
#include "libs\coopteam.h"
#include "libs\clientside.h"
#include "libs\weaponcapacity.h"
#include "libs\fixtellstory.h"
#include "libs\reaction.h"
#include "libs\spellutil.h"
#include "libs\wallutil.h"
#include "libs\cweaponproperty.h"
// #include "libs\chatevent.h" //오류가 있어서 제외함 2023-09-12-23-46
#include "libs/format.h"
#include "libs/voiceList.h"
#include "libs/indexloop.h"

int m_initialSearchHash;
int BaseCampUnit;
int m_elapsedTime;
int player[20], LastUnit, GenCnt, PlrDeadCnt;
int PlrPtr, PlrSkill[10];

static void PlayerClassCommonWhenEntry()
{
    ExtractMapBgm("..\\Granpoly.mp3", ResourceBGMdata);
    InitializeResource();
    ShowMessageBox("게임방법 설명드립니다", "*골렘타워 부수기*\n필드에 있는 모든 오벨리스크를 부수면 승리합니다! 생각보다 간단해서 놀라셨죠?");
}

static void NetworkUtilClientMain()
{
    PlayerClassCommonWhenEntry();
}

void InitPlayer(int plr, int pUnit) 
{
    player[plr] = pUnit;
    player[plr + 10] = 1;
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    PlrSkill[plr] = 0;
    if (GetGold(player[plr]))
        ChangeGold(player[plr], -GetGold(player[plr]));

    char buff[128], *pUsername = StringUtilGetScriptStringPtr(PlayerIngameNick(player[plr]));

    NoxSprintfString(buff, "%s 님께서 입장하셨습니다", &pUsername, 1);
    WriteLog(ReadStringAddressEx(buff));
    UniPrintToAll(ReadStringAddressEx(buff));
}

static void IntroducedIndexLoopHashCondition(int *pInstance)
{
    HashPushback(pInstance, 1179, ChakramEvent);
    HashPushback(pInstance, 706, RemoveForone);
}

static void deferredInit()
{
    WriteLog("deferredInit");
    ProcessMapReadableText();
    WriteLog("deferredInit-2");
    PotionHurricans(GetWaypointX(47), GetWaypointY(47));
    WarShieldSpawn();
    PutGameTipUnit();
    PutMapDecorations();
    WriteLog("deferredInit-end");
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("gollest-log.txt");
    HashCreateInstance(&m_initialSearchHash);
    LastUnit = CreateObject("RedPotion", 1);
    Delete(LastUnit);
    FrameTimer(1, deferredInit);
    
    HashPushback(m_initialSearchHash, 2672, initialRewardMarker);
    HashPushback(m_initialSearchHash, 2675, initialNecromarker);
    HashPushback(m_initialSearchHash, 2634, GeneratorProperties);

    // ChatMessageEventPushback("/s", ChatEventSavePoint); //error
    // ChatMessageEventPushback("/l", ChatEventLoadPoint); //error
    FrameTimerWithArg(1, Object("FirstGenerator"), InitiGame);
    FrameTimer(1, MakeCoopTeam);
    FrameTimer(1, LoopPreservePlayers);

    RegistSignMessage(Object("mappick1"), "이런 비밀스런 공간을 발견하다니.. 에잇 보상받아라!!");
    RegistSignMessage(Object("mappick2"), "골렘오벨 쳐부수기 최신버전.      제작. 녹스게임리마스터");
    WriteLog("end-mapinitialize");
    blockObserverMode();
}

void ShowProcessResult(int count)
{
    int params[]={count, m_elapsedTime};
    char printMsg[200];

    NoxSprintfString(printMsg, "오벨리스크 %d개를 처리했습니다, 작업은 %d프레임이 소요되었습니다", params, sizeof(params));
    WriteLog(ReadStringAddressEx(printMsg));
    UniPrintToAll(ReadStringAddressEx(printMsg));
}

void FinishGeneratorSetting(int count) //here
{
    int k;

    for (k = 0 ; k < 5 ; k += 1)
    {
        WallUtilOpenWallAtObjectPosition(205);
        TeleportLocationVector(205, -23.0, 23.0);
    }
    GenCnt = count;    
    MasterUnit();    
    FrameTimer(60, PutDefaultItems);
    ShowProcessResult(count);
    BaseCampUnit = CreateObject("BlueSummons", 186);

    DrawImageAtEx(LocationX(206), LocationY(206), 1566);
    // DrawImageAtEx(LocationX(207), LocationY(207), 1567);
}

void GeneratorProperties(int generatr, int *pCount)
{
    ++pCount[0];
    Enchant(generatr, "ENCHANT_FREEZE", 0.0);
    SetUnitMaxHealth(generatr, 500);
    ObjectOff(generatr);
    SetUnitCallbackOnDeath(generatr, GeneratorOnDestroyed);
}

static void initialRewardMarker(int unit, int *pCount)
{
    TeleportLocation(1, GetObjectX(unit), GetObjectY(unit));
    Delete(unit);
    ItemSpawn(1);
}

static void initialNecromarker(int unit, int *pCount)
{
    float x=GetObjectX(unit), y=GetObjectY(unit);

    Delete(unit);
    if (Random(0, 10))
    {
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        WizUnitSummon(ptr);
        Delete(ptr);
    }
}

static int initialSearchProto(int fId, int unit, int *pCount)
{
    return Bind(fId, &fId + 4);
}

void InitiGame(int cur)
{
    int count, k;

    if (cur < LastUnit)
    {
        int fn;

        for (k = 0 ; k < 30 ; k += 1)
        {
            if (HashGet(m_initialSearchHash, GetUnitThingID(cur + (k * 2)), &fn, FALSE))
                initialSearchProto(fn, cur + (k*2), &count);
        }
        ++m_elapsedTime;
        FrameTimerWithArg(1, cur + 60, InitiGame);
        return;
    }
    WriteLog("end-initiGame-before finishgen...");
    FinishGeneratorSetting(count);
    WriteLog("end-initiGame");
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

void PlayerClassFastJoin()
{
    int plr = CheckPlayer();

    if (plr + 1)
        MoveObject(player[plr], LocationX(203), LocationY(203));
    else
        MoveObject(OTHER, LocationX(202), LocationY(202));
}

void GetPlayers()
{
    int k, plr;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            plr = CheckPlayer();
            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    plr = k;
                    InitPlayer(plr, GetCaller());
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerEntry(plr);
                break;
            }
        }
        CatchMapError();
        break;
    }
}

void PlayerEntry(int plr)
{
    if (PlayerClassCheckDeathFlag(plr))
        PlayerClassSetDeathFlag(plr);
    DeleteObjectTimer(CreateObject("BlueRain", 186), 20);
    MoveObject(player[plr], LocationX(186), LocationY(186));
    AudioEvent("QuestIntroScreen", 186);
    UniChatMessage(player[plr], "후훗, 한번 싸워 볼까나", 150);
}

void CatchMapError()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_BLINDED", 0.0);
    MoveObject(OTHER, LocationX(185), LocationY(185));
    UniPrint(OTHER, "죄송합니다. 요청을 처리하지 못했습니다. 맵 로드 중 오류가 발생했습니다");
}

void PlayerClassOnFree(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + "님께서 적에게 격추되었습니다");
}

void LoopPreservePlayers()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    if (MaxHealth(player[i]) == 150)
                    {
                        if (CheckPlayerInput(player[i]) == 0x2f)
                            CastShockWave(i);
                    }
                    WindBooster(i);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i)) break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, LoopPreservePlayers);
}

void WindBooster(int plr)
{
    if (UnitCheckEnchant(player[plr], GetLShift(31)))
    {
        EnchantOff(player[plr], EnchantList(31));
        RemoveTreadLightly(player[plr]);
        if (PlrSkill[plr] & 0x01)
        {
            UnitSetEnchantTime(player[plr], 8, 8);
            PushObjectTo(player[plr], UnitAngleCos(player[plr], 72.0), UnitAngleSin(player[plr], 72.0));
            Effect("RICOCHET", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
        }
    }
}

int CheckPlayer()
{
    int k;
    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void RemoveForone(int sCur, int sOwner)
{
    if (IsPlayerUnit(sOwner))
        Delete(sCur);
}

void ChakramEvent(int cur, int owner)
{
    int mis;
    
    if (CurrentHealth(owner) && !HasEnchant(cur, "ENCHANT_SHOCK"))
    {
        mis = CreateObjectAt("WeakFireball", GetObjectX(cur), GetObjectY(cur));

        SetOwner(owner, mis);
        PushObjectTo(mis, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
    }
}

void CastShockWave(int plr)
{
    int ptr;

    if (CurrentHealth(player[plr]))
    {
        if (!UnitCheckEnchant(player[plr], GetLShift(11)))
        {
            UnitSetEnchantTime(player[plr], 11, 180);
            ptr = CreateObjectAt("AmbBeachBirds", GetObjectX(player[plr]), GetObjectY(player[plr]));
            PushObjectTo(WarArrowCreate(player[plr], GetObjectX(player[plr]) + UnitAngleCos(player[plr], 13.0), GetObjectY(player[plr]) + UnitAngleSin(player[plr], 13.0)),
                UnitAngleCos(player[plr], 22.0), UnitAngleSin(player[plr], 22.0));
            LookWithAngle(ptr + 1, GetDirection(player[plr]));
            Enchant(ptr + 1, "ENCHANT_SLOWED", 0.0);
            Enchant(ptr + 1, "ENCHANT_INFRAVISION", 0.0);
            Delete(ptr);
        }
    }
}

void MovingAnkh(int ptr)
{
    int rep[10];
    int plr = GetDirection(ptr + 1);
    if (CurrentHealth(player[plr]) && IsObjectOn(ptr + 1) && rep[plr] < 90)
    {
        if (!rep[plr])
        {
            Move(ptr, plr + 189);
        }
        else if (!HasEnchant(ptr, "ENCHANT_SHOCK"))
            rep[plr] = 100;
        rep[plr] ++;
        FrameTimerWithArg(1, ptr, MovingAnkh);
    }
    else
    {
        rep[plr] = 0;
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void PlayerDeath()
{
    PlrDeadCnt ++;
}

void OpenFluffyExit()
{
    GenCnt --;
    if (!GenCnt)
        VictoryEvent();
}

void GeneratorOnDestroyed()
{
    OpenFluffyExit();
    TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
    CallFunctionWithArgInt(GenKillDropFuncTable(Random(0, 6)), 1);
    GreenExplosion(GetObjectX(SELF), GetObjectY(SELF));
    Delete(SELF);
}

void UnitDeathEvent()
{
    TeleportLocation(1, GetObjectX(SELF), GetObjectY(SELF));
    ItemSpawn(1);
    Effect("VIOLET_SPARKS", LocationX(1), LocationY(1), 0.0, 0.0);
}

void VictoryEvent()
{
    TeleportAllPlayerToLocation(72);
    Effect("WHITE_FLASH", LocationX(72), LocationY(72), 0.0, 0.0);
    UniPrintToAll("축하합니다, 맵에 있는 모든 소환오벨리스크를 파괴하셨습니다!");
    FrameTimer(1, StrYourWinner);
    FrameTimer(120, OpenWinPlaceWalls);
}

void OpenWinPlaceWalls()
{
    int k;

    for (k = 0 ; k < 13 ; k ++)
        WallOpen(Wall(183 - k, 49 + k));
}

void TeleportAllPlayerToLocation(int loc)
{
    int k;
    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
            MoveObject(player[k], LocationX(loc), LocationY(loc));
    }
}

void ShowGeneratorCount(int count)
{
    char buff[128];
    NoxSprintfString(buff,"남은 소환 오벨리스크 수: %d", &count, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

int GenKillDropFuncTable(int pickIndex)
{
    int table[] = {ItemSpawn, PotionSpawn, WeaponSpawn, WeakGolemSpawn, WeakGolemSpawn,
        StrongGolemSpawn, NothingAnymore};

    return table[pickIndex];
}

int ItemSpawn(int wp)
{
    return CallFunctionWithArgInt(ItemFuncTable(Random(0, 12)), wp);
}

int PotionSpawn(int wp)
{
    return SpawnPotions(wp);
}

int WeaponSpawn(int wp)
{
    return SpawnWeapon(wp);
}

int commonCreateGolem(int wp, int hp){
    int unit=CreateObjectById(OBJ_MECHANICAL_GOLEM,LocationX(wp),LocationY(wp));

    SetCallback(unit,5,GolemUnitDead);
    SetUnitVoice(unit,MONSTER_VOICE_Demon);
    SetUnitMaxHealth(unit,hp);
    LookWithAngle(unit,Random(0,255));
    AggressionLevel(unit,1.0);
    return unit;
}

int WeakGolemSpawn(int wp)
{
    return commonCreateGolem(wp,150);
}

int StrongGolemSpawn(int wp)
{
    int unit = commonCreateGolem(wp,900);

    Enchant(unit, "ENCHANT_HASTED", 0.0);
    return unit;
}

int NothingAnymore(int wp)
{
    return 0;
}

int ItemFuncTable(int pickIndex)
{
    int itemTable[] = {SpawnPotions, SpecialPotion, SpawnArmors, SpawnWeapon, SpawnFanchakOrQuiver, SpawnBowOrXBow,
        SpawnOblivion, MagicStaff, ManaPotions, OnlyManaHpPotion, CheatGold, SpecialItemCreate, GermDrop};

    return itemTable[pickIndex];
}

int SpawnPotions(int wp)
{
    string pot[] = {"RedPotion", "BluePotion", "CurePoisonPotion", "VampirismPotion", "HastePotion", "ShieldPotion", "Cider", "RedApple", "Meat", "ManaCrystalLarge",
        "FireProtectPotion", "ShockProtectPotion", "PoisonProtectPotion", "InvisibilityPotion", "YellowPotion", "Mushroom", "WhitePotion", "BlackPotion",
        "InvulnerabilityPotion"};
    int potion = CheckPotionThingID(CreateObject(pot[Random(0, 18)], wp));

    Frozen(potion, 1);
    return potion;
}

int SpecialPotion(int wp)
{
    return CreateObject("RedPotion", wp);
}

int SpawnArmors(int wp)
{
    string armorname[] = {"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "MedievalCloak", "MedievalPants", "MedievalShirt",
        "WizardHelm", "ChainCoif", "ChainLeggings", "ChainTunic", "WizardRobe", "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots",
        "LeatherBoots", "LeatherHelm", "LeatherLeggings", "ConjurerHelm", "SteelHelm", "SteelShield", "WoodenShield", "SteelShield", "WoodenShield"};
    int armor = CreateObject(armorname[Random(0, 24)], wp);

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    Frozen(armor, 1);
    return armor;
}

int SpawnWeapon(int wp)
{
    string weaponName[] = {"StaffWooden", "OgreAxe", "RoundChakram", "WarHammer", "GreatSword", "Sword", "Longsword", "BattleAxe", "MorningStar", 
        "RoundChakram", "RoundChakram", "RoundChakram", "GreatSword", "WarHammer", "RoundChakram"};
    int weaponUnit = CreateObject(weaponName[Random(0, 13)], wp);

    SetWeaponProperties(weaponUnit, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    Frozen(weaponUnit, 1);
    return weaponUnit;
}

int SpawnFanchakOrQuiver(int wp)
{
    string name[] = {"Quiver", "Quiver", "Quiver", "FanChakram", "FanChakram"};
    int consume = CreateObject(name[Random(0, 4)], wp);

    SetConsumablesWeaponCapacity(consume, 255, 255);
    SetWeaponProperties(consume, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));

    return consume;
}

int SpawnBowOrXBow(int wp)
{
    string name[] = {"Bow", "CrossBow"};
    int bowitem = CreateObject(name[Random(0, 1)], wp);

    SetWeaponProperties(bowitem, 0, 0, Random(29, 36), Random(29, 36));
    return bowitem;
}

int SpawnOblivion(int wp)
{
    string oblvname[] = {"OblivionHalberd", "OblivionHeart", "OblivionWierdling", "OblivionOrb"};
    int oblvitem = CreateObject(oblvname[Random(0, 2)], wp);

    DisableOblivionItemPickupEvent(oblvitem);
    SetItemPropertyAllowAllDrop(oblvitem);
    SetWeaponProperties(oblvitem, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    return oblvitem;
}

int MagicStaff(int wp)
{
    string staffname[] = {"DeathRayWand", "ForceWand", "LesserFireballWand", "FireStormWand", "InfinitePainWand",
        "SulphorousFlareWand", "SulphorousFlareWand"};
    int staffunit = CreateObject(staffname[Random(0, 6)], wp);

    if (GetUnitThingID(staffunit) == 215)
        SetConsumablesWeaponCapacity(staffunit, 30, 30);
    return staffunit;
}

int ManaPotions(int wp)
{
    string manapotname[] = {"BluePotion", "ManaCrystalCluster", "ManaCrystalLarge", "ManaCrystalSmall", "WhitePotion", "RedPotion", "CurePoisonPotion"};
    int manapot = CheckPotionThingID(CreateObject(manapotname[Random(0, 6)], wp));

    Frozen(manapot, 1);
    return manapot;
}

int OnlyManaHpPotion(int wp)
{
    string alltypePotname[] = {"RedPotion", "BluePotion", "CurePoisonPotion"};
    int alltypepot = CreateObject(alltypePotname[Random(0, 2)], wp);

    return alltypepot;
}

int CheatGold(int wp)
{
    string gName[] = {"QuestGoldChest", "QuestGoldPile", "Gold"};
    int gUnit = CreateObject(gName[Random(0, 2)], wp);

    UnitStructSetGoldAmount(gUnit, Random(2000, 5000));
    return gUnit;
}

int SpecialItemActions(int specialOrder)
{
    int specialActions[] = {SpecialProperty1, SpecialProperty2, SpecialProperty3, SpecialProperty4,
        SpecialProperty5, SpecialProperty6, SpecialProperty7, SpecialProperty8};

    return specialActions[specialOrder];
}

int SpecialItemCreate(int wp)
{
    string specialname[] = {"FoilCandleLit", "AmuletofManipulation", "BlackBook1", "Corn", "BraceletofHealth", "Candle1",
        "AmuletOfClarity", "AmuletofCombat"
    };
    int specialOrder = Random(0, 7);
    int specialunit = CreateObject(specialname[specialOrder], wp);
    
    SetItemPropertyAllowAllDrop(specialunit);
    SetUnitCallbackOnUseItem(specialunit, SpecialItemActions(specialOrder));

    return specialunit;
}

int GermDrop(int wp)
{
    string germItemname[] = {"Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Diamond"};

    return CreateObject(germItemname[Random(0, 5)], wp);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetHostileCritter();
    ResetPlayerHandlerWhenExitMap();
}

void PotionRail(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 180)
    {
        CreateObjectAt(ToStr(GetUnit1C(ptr)), GetObjectX(ptr) + MathSine(count * 12 + 90, GetObjectZ(ptr)), GetObjectY(ptr) + MathSine(count * 12, GetObjectZ(ptr)));
        LookWithAngle(ptr, count + 1);
        Raise(ptr, GetObjectZ(ptr) + 0.4);
        FrameTimerWithArg(1, ptr, PotionRail);
    }
    else
        Delete(ptr);
}

void PotionHurricans(float x, float y)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", x, y);
    SetUnit1C(unit, SToInt( "RedPotion") );
    FrameTimerWithArg(1, unit, PotionRail);
}

void PutMapDecorations()
{
    int k;
    
    Frozen(CreateObject("DunMirScaleTorch2", 93), 1);
    Frozen(CreateObject("DunMirScaleTorch2", 94), 1);
    Frozen(CreateObject("Candleabra2", 44), 1);
    Frozen(CreateObject("Candleabra2", 45), 1);
    Frozen(CreateObject("VandegrafLargeMovable", 46), 1);
    for (k = 0 ; k < 8 ; k += 1)
    {
        ObjectOn(CreateObject("MovableStatueVictory3SW", 39));
        ObjectOn(CreateObject("MovableStatueVictory3NE", 40));
        TeleportLocationVector(39, -23.0, -23.0);
        TeleportLocationVector(40, -23.0, -23.0);
    }
}

void WarArrowCollide()
{
    int owner = GetOwner(SELF), tempHp;

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        if (HasEnchant(OTHER, "ENCHANT_FREEZE"))
            return;
        tempHp = CurrentHealth(OTHER);
        Damage(OTHER, owner, 135, 14);
        Enchant(OTHER, "ENCHANT_FREEZE", 1.5);
        if (CurrentHealth(owner))
        {
            Effect("VIOLET_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            Effect("GREATER_HEAL", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(owner), GetObjectY(owner));
            RestoreHealth(owner, (tempHp - CurrentHealth(OTHER)) / 2);
        }
    }
}

int WarArrowCreate(int owner, float x, float y)
{
    int unit = CreateObjectAt("HarpoonBolt", x, y);
    int ptr = GetMemory(0x750710);

    SetOwner(owner, unit);
    SetMemory(ptr + 0x2e8, 5483536);
    SetUnitCallbackOnCollide(unit, WarArrowCollide);
    return unit;
}

void DisplayLadderBoard()
{
    string fmt = "남은 소환오벨리스크:\n%d\n\n데스: %d\n경과시간:\n%d시간%d분%d초";
    int scd, min, hor;

    if (IsCaller(GetTrigger() + 1))
    {
        if (GetDirection(SELF) < 30)
        {
            LookWithAngle(SELF, GetDirection(SELF) + 1);
            return;
        }
        LookWithAngle(SELF, 0);
        char buff[256];
        int args[]={GenCnt, PlrDeadCnt, hor, min, scd};
        
        scd ++;
        if (scd == 60)
        {
            scd = 0;
            min ++;
            if (min == 60)
            {
                min = 0;
                hor ++;
            }
        }
        NoxSprintfString(buff, fmt, args, sizeof(args));
        UniChatMessage(SELF, ReadStringAddressEx(buff), 32);
    }
}

int MasterUnit()
{
    int unit;
    
    if (!unit)
    {
        unit = CreateObjectAt("Hecubah", 5500.0, 100.0);
        CreateObjectAt("BlackPowder", GetObjectX(unit), GetObjectY(unit));
        Frozen(unit, 1);
        SetCallback(unit, 9, DisplayLadderBoard);
    }
    return unit;
}

void GolemUnitDead()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    ItemSpawn(1);
}

void WarShieldSpawn()
{
    int k;

    for (k = 0 ; k < 10 ; k += 1)
    {
        SetArmorProperties(CreateObjectAt("SteelShield", 3723.0, 2186.0), 4, 3, 16, 7);
        SetArmorProperties(CreateObjectAt("SteelShield", 2240.0, 1810.0), 4, 3, 18, 15);
    }
}

void UserSavePointLocalStore(int *pDest)
{
    int points[32];

    pDest[0] = &points;
}

int ChatEventSavePoint(int pUnit)
{
    if (!MaxHealth(pUnit))
        return TRUE;

    int *points;
    int pIndex = GetPlayerIndex(pUnit);

    UserSavePointLocalStore(&points);
    if (IsObjectOn(points[pIndex]))
        MoveObject(points[pIndex], GetObjectX(pUnit), GetObjectY(pUnit));
    else
    {
        points[pIndex] = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(pUnit), GetObjectY(pUnit));
    }
    DeleteObjectTimer(CreateObjectAt("ReleasedSoul", GetObjectX(pUnit), GetObjectY(pUnit)), 25);
    UniPrint(pUnit, "현재 위치가 저장되었습니다");
    return TRUE;
}

int ChatEventLoadPoint(int pUnit)
{
    if (!MaxHealth(pUnit))
        return TRUE;

    int *points;
    int pIndex = GetPlayerIndex(pUnit);

    UserSavePointLocalStore(&points);
    if (!IsObjectOn(points[pIndex]))
    {
        UniPrint(pUnit, "아직 저장된 위치가 없습니다");
        return TRUE;
    }
    Effect("SMOKE_BLAST", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    Effect("TELEPORT", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    MoveObject(pUnit, GetObjectX(points[pIndex]), GetObjectY(points[pIndex]));
    Delete(points[pIndex]);
    Effect("COUNTERSPELL_EXPLOSION", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    Effect("VIOLET_SPARKS", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
    UniPrint(pUnit, "저장된 위치로 이동되었습니다");
    return TRUE;
}

void PutDefaultItems()
{
    WriteLog("PutDefaultItems");
    DefaultItem("GreatSword", 12, 4036.0, 1667.0, 20.0, 20.0);
    DefaultItem("RoundChakram", 12, 4013.0, 1690.0, 20.0, 20.0);
    SetShopkeeperText(Object("WarWeaponShop"), "전사 무기상점");
    SetShopkeeperText(Object("WarArmorShop"), "전사 갑옷상점");
    SetShopkeeperText(Object("SpecialWarShop"), "전사 레어템");
    HiddenShopDecorations();
    FrameTimer(30, PutSkillShop);
    WriteLog("PutDefaultItems-end");
}

void DefaultItem(string name, int max, float x, float y, float vect_x, float vect_y)
{
    int k;

    MoveWaypoint(1, x, y);
    for (k = 0 ; k < max ; k += 1)
    {
        Frozen(CreateObject(name, 1), 1);
        TeleportLocationVector(1, vect_x, vect_y);
    }
}

int FieldMonsterSummonActions(int actionId)
{
    int actions[] = {SummonRedWiz, SummonNecromancer, SummonLich, SummonGreenWiz, SummonMystic,
        SummonHecubah, SummonLichLord, SummonFrog, SummonFireFairy, SummonBlackSpider, SummonGoon,
        SummonStrongWizWhite, SummonBomber, SummonJandor, SummonOrbHecubah, SummonFemale,
        SummonMovingPlant, SummonEmberDemon, SummonStoneGolem, SummonBeast, SummonSwordsman,
        SummonArcher, SummonOgreAxe, SummonSkeleton, SummonOgreLord,
        SummonMimic, SummonLeech, SummonSmallAlbiSpider, SummonSmallSpider, SummonMimiFlier, 
        SummonHorrendous, SummonMobRat, SummonMobFish, SummonMecaGolem, DummyEndSigFunc};

    return actions[actionId];
}

void WizUnitSummon(int target)
{
    if (ToInt(GetObjectX(target)))
    {
        Effect("SMOKE_BLAST", GetObjectX(target), GetObjectY(target), 0.0, 0.0);
        Effect("SPARK_EXPLOSION", GetObjectX(target), GetObjectY(target), 0.0, 0.0);
        int fieldmob = CallFunctionWithArgInt(FieldMonsterSummonActions(Random(0, 33)), target);

        SetCallback(fieldmob, 5, UnitDeathEvent);
        RetreatLevel(fieldmob, 0.0);
        AggressionLevel(fieldmob, 1.0);
    }
}

int DummyEndSigFunc(int endSig)
{
    return CreateObjectAt("Urchin", GetObjectX(endSig), GetObjectY(endSig));
}

void PutSkillShop()
{
    int unit = CreateObject("Horrendous", 42);

    Frozen(CreateObject("LichLord", 74), 1);
    LookWithAngle(unit + 1, 96);
    Frozen(unit, 1);
    LookWithAngle(unit, 96);
    SetDialog(unit, "a", DescriptionDashSkill, BuyDash);
    SetDialog(unit + 1, "a", DescriptionLaiserSword, BuyLaiserSword);

    PutOblivionShop(183);
    //FrameTimer(77, StartBgmLoop);
}

void DescriptionDashSkill()
{
    if (HasEnchant(OTHER, "ENCHANT_BURNING"))
    {
        EnchantOff(OTHER, "ENCHANT_BURNING");
        BuyDash();
    }
    else
    {
        Enchant(OTHER, "ENCHANT_BURNING", 0.8);
        UniPrint(OTHER, "대쉬(윈드 부스터): 짧은거리를 빠른속도로 이동하는 기술입니다                                              ");
        UniPrint(OTHER, "대쉬 기술을 배웁니다, 이 기술을 습득하게 되면 조심스럽게 걷기 능력이 대쉬 능력으로 전환되며 되돌릴 수 없습니다");
        UniPrint(OTHER, "이 작업은 3만 골드를 요구합니다, 작업을 계속하려면 더블클릭 하십시오");
    }
}

void BuyDash()
{
    int plr = CheckPlayer();

    if (CurrentHealth(OTHER))
    {
        if (plr + 1)
        {
            if (GetGold(OTHER) >= 30000)
            {
                if (PlrSkill[plr] & 0x1)
                {
                    UniPrint(OTHER, "거래실패: 이 능력을 이미 가졌습니다");
                }
                else
                {
                    PlrSkill[plr] = PlrSkill[plr] ^ 0x01;
                    ChangeGold(OTHER, -30000);
                    UniPrint(OTHER, "거래성공: 당신은 이 능력을 가졌습니다");
                }
            }
            else
                UniPrint(OTHER, "거래실패: 금화가 부족합니다");
        }
    }
}

void DescriptionLaiserSword()
{
    if (HasEnchant(OTHER, "ENCHANT_BURNING"))
    {
        BuyLaiserSword();
        EnchantOff(OTHER, "ENCHANT_BURNING");
    }
    else
    {
        Enchant(OTHER, "ENCHANT_BURNING", 0.8);
        UniPrint(OTHER, "광선검을 구입합니다, 이 작업은 6만 골드를 요구합니다. 계속하려면 [더블클릭] 을 하십시오");
    }
}

void BuyLaiserSword()
{
    int plr = CheckPlayer();

    if (CurrentHealth(OTHER))
    {
        if (plr + 1)
        {
            if (GetGold(OTHER) >= 60000)
            {
                LaiserSwordCreate(GetObjectX(OTHER), GetObjectY(OTHER));
                ChangeGold(OTHER, -60000);
                UniPrint(OTHER, "광선검을 구입하셨습니다, 구입하신 광선검은 캐릭터 아래에 생성되었습니다");
            }
            else
                UniPrint(OTHER, "거래실패: 금화가 부족합니다");
        }
    }
}

void HiddenShopDecorations()
{
    Enchant(CreateObjectAt("InvisibleLightBlueLow", 4610.0, 2228.0), "ENCHANT_SLOWED", 0.0);
}

void OblivionShopDesc()
{
    UniPrint(OTHER, "망각의 지팡이를 구입하시겠어요? 거래금액: 4만원");
    TellStoryUnitName("AmuletDrop", "War08a:AldwynGreet", "포오네 지팡이\n판매");
}

void OblivionShopTrade()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) >= 40000)
    {
        SummonOblivion(OTHER);
        ChangeGold(OTHER, -40000);
        UniPrint(OTHER, "거래완료!");
        Effect("WHITE_FLASH", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        return;
    }
    char buff[192];
    int pay=40000-GetGold(OTHER);

    NoxSprintfString(buff, "거래가 취소되었습니다: 잔액이 %d 부족합니다", &pay, 1);
    UniPrint(OTHER, ReadStringAddressEx(buff));
}

void PutOblivionShop(int sLocation)
{
    int sUnit = CreateObject("WizardWhite", sLocation);

    ObjectOff(sUnit);
    Damage(sUnit, 0, MaxHealth(sUnit) + 1, -1);
    Frozen(sUnit, 1);
    SetDialog(sUnit, "YESNO", OblivionShopDesc, OblivionShopTrade);
}

void PutGameTipUnit()
{
    int unit = CreateObjectAt("Wizard", 4280.0, 1696.0);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, ShowGameTips);
}

void ShowGameTips()
{
    if (IsPlayerUnit(OTHER) && !UnitCheckEnchant(OTHER, GetLShift(19)))
    {
        Enchant(OTHER, EnchantList(19), 5.0);
        // UniPrint(OTHER, "게임 팁_ 채팅 창에 '/s' 라고 입력하시면 캐릭터의 현재 위치를 저장할 수 있습니다, '/g' 을 입력하여 저장된 위치로 이동할 수 있습니다");
        UniPrint(OTHER, "게임 팁_ 소환 오벨리스크를 부수면 랜덤으로 아이템이나 아오오니가 등장합니다. 특별히, 헤이스트걸린 아오오니는 체력이 더 많으니 조심하세요");
    }
}

int UserDamageArrowCreateThing(int owner, float x, float y, int dam, int thingID)
{
    int unit = CreateObjectAt("MercArcherArrow", x, y);
    int ptr = GetMemory(0x750710);

    SetOwner(owner, unit);
    //SetMemory(ptr + 0x14, 0x32);
    if (thingID)
        SetMemory(ptr + 0x04, thingID);
    SetMemory(GetMemory(ptr + 0x2bc) + 4, dam);
    return unit;
}

int SpinArrowCreate(int sOwner, int sAngle, float sDist)
{
    int arw = UserDamageArrowCreateThing(sOwner, GetObjectX(sOwner) + MathSine(sAngle + 90, sDist), GetObjectY(sOwner) + MathSine(sAngle, sDist), 85, 526);

    LookAtObject(arw, sOwner);
    LookWithAngle(arw, GetDirection(arw) + 128);
    return arw;
}

void SpinArrowShot(int sOwner)
{
    int i;

    if (CurrentHealth(sOwner))
    {
        for (i = 36 ; i ; i --)
            PushObject(SpinArrowCreate(sOwner, i * 10, 17.0), 16.0, GetObjectX(sOwner), GetObjectY(sOwner));
    }
}

void UnitTeleportToUnit(int sUnit, int dUnit)
{
    int dUnitPtr = UnitToPtr(dUnit);

    if (dUnitPtr)
        MoveObject(sUnit, ToFloat(GetMemory(dUnitPtr + 0x38)), ToFloat(GetMemory(dUnitPtr + 0x3c)));
}

void UnitTeleportToUnitAndHealBuffFx(int sUnit, int dUnit)
{
    float xProfile, yProfile;
    int dUnitPtr = UnitToPtr(dUnit);

    if (dUnitPtr)
    {
        xProfile = ToFloat(GetMemory(dUnitPtr + 0x38));
        yProfile = ToFloat(GetMemory(dUnitPtr + 0x3c));
        MoveObject(sUnit, xProfile, yProfile);
        Effect("GREATER_HEAL", xProfile, yProfile, xProfile, yProfile - 160.0);
    }
}

void BuffHealingHealth(int sUnit)
{
    int owner, count = GetDirection(sUnit);

    while (IsObjectOn(sUnit))
    {
        owner = GetOwner(sUnit);
        if (CurrentHealth(owner) && count)
        {
            UnitTeleportToUnitAndHealBuffFx(sUnit, owner);
            RestoreHealth(owner, 1);
            LookWithAngle(sUnit, count - 1);
            FrameTimerWithArg(2, sUnit, BuffHealingHealth);
            break;
        }
        Delete(sUnit);
        break;
    }
}

void BuildMagicWall(int pUnit, int angle, float dist)
{
	int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(pUnit), GetObjectY(pUnit));

	SetOwner(pUnit, unit);
	CastSpellObjectLocation("SPELL_WALL", unit, GetObjectX(pUnit) + MathSine(angle + 90, dist), GetObjectY(pUnit) + MathSine(angle, dist));
	DeleteObjectTimer(unit, 120);
}

void SummonedUnitCollide()
{
    int owner;

    if (CurrentHealth(OTHER) && MaxHealth(SELF))
    {
        owner = GetOwner(GetTrigger() + 1);
        if (GetOwner(OTHER) == owner && (GetTrigger() < GetCaller()))
        {
            Enchant(OTHER, "ENCHANT_VAMPIRISM", 0.0);
            Enchant(OTHER, "ENCHANT_HASTED", 0.0);
            Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
            SetUnitMaxHealth(OTHER, 550);
            Delete(SELF);
            Delete(GetTrigger() + 1);
        }
    }
}

void SpecialProperty1()
{
    Delete(SELF);
    if (HasEnchant(OTHER, "ENCHANT_SHOCK"))
        return;
    else
        Enchant(OTHER, "ENCHANT_SHOCK", 60.0);
}

void SpecialProperty2()
{
    int home = BaseCampUnit;

    Delete(SELF);
    if (IsObjectOn(home))
    {
        if (IsVisibleTo(OTHER, home) || IsVisibleTo(home, OTHER))
            UniPrint(OTHER, "이미 대피소로 와 계시네요...");
        else
        {
            Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            UnitTeleportToUnit(OTHER, home);
            Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            UniPrint(OTHER, "대피소로 이동했습니다");
        }
    }
}

void SpecialProperty3()
{
    Delete(SELF);
    if (HasEnchant(OTHER, "ENCHANT_REFLECTIVE_SHIELD"))
        return;
    else
        Enchant(OTHER, "ENCHANT_REFLECTIVE_SHIELD", 120.0);
}

void SpecialProperty4()
{
    PlaySoundAround(OTHER,SOUND_MonsterEatFood);
    RestoreHealth(OTHER,145);
    Delete(SELF);
}

void SpecialProperty5()
{
    int sUnit;

    Delete(SELF);
    if (HasEnchant(OTHER, "ENCHANT_CROWN")) return;
    sUnit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));

    SetOwner(OTHER, sUnit);
    LookWithAngle(sUnit, 225);
    FrameTimerWithArg(1, sUnit, BuffHealingHealth);
    Enchant(OTHER, "ENCHANT_CROWN", 15.0);
    UniPrint(OTHER, "잠시동안 체력회복 속도가 증가됩니다");
}

void SpecialProperty6()
{
    float dist = 108.0;

    Delete(SELF);
    BuildMagicWall(OTHER, 45, dist);
    BuildMagicWall(OTHER, 135, dist);
    BuildMagicWall(OTHER, 225, dist);
    BuildMagicWall(OTHER, 315, dist);
}

void SpecialProperty7()
{
    Delete(SELF);

    FrameTimerWithArg(1, GetCaller(), SpinArrowShot);
}

void SpecialProperty8()
{
    int trp;

    Delete(SELF);
    MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
    AudioEvent("Clank1", 1);
    trp = CreateObjectAt("BearTrap", GetObjectX(OTHER), GetObjectY(OTHER));
    DeleteObjectTimer(CreateObjectAt("Smoke", GetObjectX(trp), GetObjectY(trp)), 24);
    SetOwner(OTHER, trp);
}

void TestDemons()
{
    //SetMemory(0x58f1f0, 0x200000);
    CreateObject("DemonsBreathWand", 204);
}

void ResourceBGMdata() { }

static void OnPlayerEntryMap(int pInfo)
{
    ConfirmPlayerEntryMap(pInfo);
}
