
#include "libs/define.h"
#include "libs/spritedb.h"
#include "libs/objectIDdefines.h"

#define IMAGE_VECTOR_SIZE 32

void ImageResourceKingFrogLeft()
{ }
void ImageResourceKingFrogLeftJump()
{ }
void ImageResourceKingFrogRight()
{ }
void ImageResourceKingFrogRightJump()
{ }

void ImageResourceKingFrogDead()
{ }

void imageFrogUp(){ }
void imageFrogLeft(){ }
void imageFrogDown(){ }
void imageFrogJumpUp(){ }
void imageFrogJumpLeft(){ }
void imageFrogJumpDown(){ }

#define FROG_UP_IMAGE_ID 113175
#define FROG_LEFT_IMAGE_ID 113183
#define FROG_RIGHT_IMAGE_ID 113187
#define FROG_DOWN_IMAGE_ID 113195

#define FROG_UP_JUMP_IMAGE_ID 113211
#define FROG_LEFT_JUMP_IMAGE_ID 113227
#define FROG_RIGHT_JUMP_IMAGE_ID 113235
#define FROG_DOWN_JUMP_IMAGE_ID 113251

void initializeImageVector(int imgVector)
{
    int index=0;

    AddImageFromResource(imgVector, GetScrCodeField(ImageResourceKingFrogLeft)+4, 120608, index++); //left
    AddImageFromResource(imgVector, GetScrCodeField(ImageResourceKingFrogLeftJump)+4, 120609, index++); //left jump
    AddImageFromResource(imgVector, GetScrCodeField(ImageResourceKingFrogRight)+4, 120619, index++); //right
    AddImageFromResource(imgVector, GetScrCodeField(ImageResourceKingFrogRightJump)+4, 120620, index++); //right jump
    AddImageFromResource(imgVector, GetScrCodeField(ImageResourceKingFrogDead)+4, 120621, index++); //dead

    short wMessage2[64];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("-왕 개구리-"), wMessage2);
    AddStringSprite(imgVector, 0xFEE0, wMessage2, 17600,index++);

    short wMessage[64];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("!승리하셨습니다!"), wMessage);
    AddStringSprite(imgVector, 0xf800, wMessage2, 112988,index++);
    AddStringSprite(imgVector, 0x015F, wMessage2, 112989,index++);
    AddStringSprite(imgVector, 0x07E0, wMessage2, 112990,index++);
    AddStringSprite(imgVector, 0xFEE0, wMessage2, 112991,index++);

    AddImageFromResource(imgVector, GetScrCodeField(imageFrogUp)+4, FROG_UP_IMAGE_ID, index++);
    AddImageFromResource(imgVector, GetScrCodeField(imageFrogLeft)+4, FROG_LEFT_IMAGE_ID, index++);
    int *rightMotion = GetInversionGrpStreamFromFunction(imageFrogLeft);
    AddImageFromResource(imgVector, rightMotion, FROG_RIGHT_IMAGE_ID, index++);
    AddImageFromResource(imgVector, GetScrCodeField(imageFrogDown)+4, FROG_DOWN_IMAGE_ID, index++);

    AddImageFromResource(imgVector, GetScrCodeField(imageFrogJumpUp)+4, FROG_UP_JUMP_IMAGE_ID, index++);
    AddImageFromResource(imgVector, GetScrCodeField(imageFrogJumpLeft)+4, FROG_LEFT_JUMP_IMAGE_ID, index++);
    int *rightRunMotion = GetInversionGrpStreamFromFunction(imageFrogJumpLeft);
    AddImageFromResource(imgVector, rightRunMotion, FROG_RIGHT_JUMP_IMAGE_ID, index++);
    AddImageFromResource(imgVector, GetScrCodeField(imageFrogJumpDown)+4, FROG_DOWN_JUMP_IMAGE_ID, index++);

    ChangeMonsterSpriteImageCount(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_IDLE, IMAGE_SPRITE_DIRECTION_UP_LEFT, 0, FROG_UP_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_IDLE, IMAGE_SPRITE_DIRECTION_UP_RIGHT, 0, FROG_RIGHT_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_IDLE, IMAGE_SPRITE_DIRECTION_DOWN_RIGHT, 0, FROG_DOWN_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_IDLE, IMAGE_SPRITE_DIRECTION_DOWN_LEFT, 0, FROG_LEFT_IMAGE_ID);
    ChangeMonsterSpriteImageCount(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_RUN, IMAGE_SPRITE_DIRECTION_UP_LEFT, 0, FROG_UP_JUMP_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_RUN, IMAGE_SPRITE_DIRECTION_UP_RIGHT, 0, FROG_RIGHT_JUMP_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_RUN, IMAGE_SPRITE_DIRECTION_DOWN_RIGHT, 0, FROG_DOWN_JUMP_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_RUN, IMAGE_SPRITE_DIRECTION_DOWN_LEFT, 0, FROG_LEFT_JUMP_IMAGE_ID);
    ChangeMonsterSpriteImageCount(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_WALK, IMAGE_SPRITE_DIRECTION_UP_LEFT, 0, FROG_UP_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_WALK, IMAGE_SPRITE_DIRECTION_UP_RIGHT, 0, FROG_RIGHT_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_WALK, IMAGE_SPRITE_DIRECTION_DOWN_RIGHT, 0, FROG_DOWN_IMAGE_ID);
    ChangeMonsterSpriteImage(OBJ_GREEN_FROG, IMAGE_SPRITE_MON_ACTION_WALK, IMAGE_SPRITE_DIRECTION_DOWN_LEFT, 0, FROG_LEFT_IMAGE_ID);
}

void CommonInitializeImage()
{
    int imgVector=CreateImageVector(IMAGE_VECTOR_SIZE);

    InitializeImageHandlerProcedure(imgVector);
    initializeImageVector(imgVector);
}

