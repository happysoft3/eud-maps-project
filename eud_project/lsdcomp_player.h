

#include "lsdcomp_gvar.h"
#include "lsdcomp_utils.h"
#include "libs/cmdline.h"
#include "libs/buff.h"
#include "libs/waypoint.h"
#include "libs/network.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/hash.h"
#include "libs/observer.h"
#include "libs/reaction.h"
#include "libs/bind.h"
#include "libs/logging.h"
#include "libs/objectIDdefines.h"
#include "libs/wallutil.h"

#define PLAYER_INITIAL_LOCATION 35
#define PLAYER_START_LOCATION 37
#define PLAYER_DEATH_FLAG 0x80000000
#define PLAYER_CREATURE_START_MARK 34

void PlayerClassDoInitialize(int pIndex, int user) //virtual
{ }

void PlayerClassDoDeinit(int pIndex) //virtual
{ }

void commonServerClientProcedure() //virtual
{ }

void reportSpellCooldown(int user)
{
    char flags=0;

    if (UnitCheckEnchant(user, GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY)))
        flags|=1;
    if (UnitCheckEnchant(user, GetLShift(ENCHANT_PROTECT_FROM_FIRE)))
        flags|=2;
    if (UnitCheckEnchant(user, GetLShift(ENCHANT_PROTECT_FROM_POISON)))
        flags|=4;
    if (UnitCheckEnchant(user, GetLShift(ENCHANT_HASTED)))
        flags|=8;
    SendItemEnchantData(user, flags);
}

void onRecoveryCooldownLoop(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int cre = GetUnit1C(sub);

        if (CurrentHealth(cre))
        {
            int dur=GetDirection(sub);

            if (dur)
            {
                PushTimerQueue(3, sub,onRecoveryCooldownLoop);
                LookWithAngle(sub, dur-1);
                return;
            }
            int user = GetOwner(cre);

            if (IsPlayerUnit(user))
            {
                Enchant(user, EnchantList(ToInt( GetObjectZ(sub)) ), 0.0);
                reportSpellCooldown(user);
            }
        }
        Delete(sub);
    }
}

void recoveryCooldown(int cre, int enchant, int duration)
{
    int user=GetOwner(cre);

    if (!CurrentHealth(user))
        return;

    EnchantOff(user, EnchantList(enchant));

    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(cre),GetObjectY(cre));

    PushTimerQueue(1, sub,onRecoveryCooldownLoop);
    LookWithAngle(sub, duration);
    SetUnit1C(sub, cre);
    Raise(sub, enchant);
    reportSpellCooldown(user);
}

void UpdateCooldown(int pIndex)
{
    int user = GetPlayer(pIndex);

    if (!CurrentHealth(user))
        return;

    int cre = GetCreature(pIndex);

    if (!CurrentHealth(cre))
        return;

    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_FIRST_SKILL))
        recoveryCooldown(cre, ENCHANT_PROTECT_FROM_ELECTRICITY, 1);
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_SECOND_SKILL))
        recoveryCooldown(cre, ENCHANT_PROTECT_FROM_FIRE, 1);
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_LAST_SKILL))
        recoveryCooldown(cre, ENCHANT_PROTECT_FROM_POISON, 1);
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_BONUS_SKILL))
        recoveryCooldown(cre, ENCHANT_HASTED, 1);
}

void DetectTrackingMissile()
{
    int ptr = GetOwner(SELF);
    int tg = ToInt(GetObjectZ(ptr + 1));

    if (CurrentHealth(tg))
    {
        if (IsVisibleTo(tg, ptr))
        {
            LookAtObject(SELF, tg);
            LookWithAngle(ptr + 1, GetDirection(SELF));
        }
        else
            Raise(ptr + 1, ToFloat(0));
    }
    else
    {
        if (DistanceUnitToUnit(SELF, OTHER) < GetObjectZ(ptr))
            Raise(ptr + 1, ToFloat(GetCaller()));
    }
}

void CollideTrackingMissile()
{
    int ptr = GetOwner(SELF);
    int owner = GetOwner(ptr);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, owner, 85, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        Delete(SELF);
        Delete(ptr);
    }
}

void AutoTrackingMissile(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && count < 30)
    {
        if (IsVisibleTo(ptr, ptr + 1))
        {
            unit = CreateObjectAt("AirshipCaptain", GetObjectX(ptr), GetObjectY(ptr));
            Frozen(CreateObjectAt("HarpoonBolt", GetObjectX(unit), GetObjectY(unit)), 1);
            SetOwner(ptr, unit);
            LookWithAngle(unit, GetDirection(ptr + 1));
            LookWithAngle(unit + 1, GetDirection(ptr + 1));
            DeleteObjectTimer(unit, 1);
            DeleteObjectTimer(unit + 1, 3);
            SetCallback(unit, 3, DetectTrackingMissile);
            SetCallback(unit, 9, CollideTrackingMissile);
            MoveObject(ptr, GetObjectX(ptr) + UnitAngleCos(ptr + 1, 21.0), GetObjectY(ptr) + UnitAngleSin(ptr + 1, 21.0));
            LookWithAngle(ptr, count + 1);
        }
        else
            LookWithAngle(ptr, 100);
        PushTimerQueue(1, ptr, AutoTrackingMissile);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void ProtossReaverScrap(int owner, int glow)
{
    int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) + UnitRatioX(glow, owner, 21.0), GetObjectY(owner) + UnitRatioY(glow, owner, 21.0));

    LookWithAngle(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), GetDirection(owner));
    Raise(unit, 250.0);
    SetOwner(owner, unit);
    PushTimerQueue(1, unit, AutoTrackingMissile);
}

void ThunderBoltCollideHandler()
{
    int owner = ToInt(GetObjectZ(GetOwner(SELF)));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner) && CurrentHealth(owner))
    {
        Damage(OTHER, owner, 150, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.09);
    }
}

void SkillThunderBoltHandler(int ptr)
{
    float xVect = GetObjectZ(ptr), yVect = GetObjectZ(ptr + 1);
    int owner = GetOwner(ptr), maxRange = GetDirection(ptr), unit, i;

    if (CurrentHealth(owner))
    {
        unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)) + 1;
        Raise(unit - 1, ToFloat(owner));
        for (i = 0 ; i < 13 ; i ++)
        {
            Frozen(CreateObjectAt("ShopkeeperConjurerRealm", GetObjectX(ptr), GetObjectY(ptr)), 1);
            SetOwner(unit - 1, unit + i);
            DeleteObjectTimer(unit + i, 1);
            SetCallback(unit + i, 9, ThunderBoltCollideHandler);
            MoveObject(ptr, GetObjectX(ptr) + xVect, GetObjectY(ptr) + yVect);
            if (!IsVisibleTo(ptr, ptr + 1))
                break;
        }
        float point1[]={GetObjectX(ptr + 1), GetObjectY(ptr + 1),};
        float point2[]={GetObjectX(ptr), GetObjectY(ptr),};
        
        DrawYellowLightningFx(point1, point2, 24);
        DeleteObjectTimer(unit - 1, 3);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void StartThunderBolt(int owner, int glow)
{
    float xVect = UnitRatioX(glow, owner, 32.0), yVect = UnitRatioY(glow, owner, 32.0);
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner) + xVect, GetObjectY(owner) + yVect);

    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), yVect);
    Raise(unit, xVect);
    SetOwner(owner, unit);
    LookWithAngle(unit, 15);
    FrameTimerWithArg(1, unit, SkillThunderBoltHandler);
}

void ManaBombRisk()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF,OTHER))
                Damage(OTHER, GetOwner(GetOwner(SELF)), 300, DAMAGE_TYPE_PLASMA);
            // PlaySphericalEffect(OTHER);
            // NoxRestoreHealth(SELF, 100);
        }
    }
}

void ReachedGreenSoul(int ptr)
{
    MoveWaypoint(1, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
    AudioEvent("HecubahDieFrame283", 1);
    GreenExplosion(GetWaypointX(1), GetWaypointY(1));
    DeleteObjectTimer(CreateObject("ForceOfNatureCharge", 1), 20);
    SplashDamageAt(GetOwner(ptr), LocationX(1), LocationY(1), 250.0, ManaBombRisk);
}

void LastSkillProgress(int ptr)
{
    float xVect = GetObjectZ(ptr), yVect = GetObjectZ(ptr + 1);
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    while (1)
    {
        if (CurrentHealth(owner) && IsObjectOn(ptr))
        {
            if (count)
            {
                MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
                AudioEvent("WallCast", 1);
                GreenExplosion(GetObjectX(ptr), GetObjectY(ptr));
                MoveObject(ptr, GetObjectX(ptr) + xVect, GetObjectY(ptr) + yVect);
                LookWithAngle(ptr, count - 1);
                FrameTimerWithArg(1, ptr, LastSkillProgress);
                break;
            }
            else
            {
                ReachedGreenSoul(ptr);
            }
        }
        Delete(ptr);
        Delete(ptr + 1);
        break;
    }
}

void ManaExplosion(int owner, int glow)
{
    float dist = DistanceUnitToUnit(owner, glow) / 15.0;
    float xVect = UnitRatioX(glow, owner, dist), yVect = UnitRatioY(glow, owner, dist);
    
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner));
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(glow), GetObjectY(glow)), yVect);
    Raise(unit, xVect);
    SetOwner(owner, unit);
    LookWithAngle(unit, 15);
    FrameTimerWithArg(1, unit, LastSkillProgress);
}

void onNuclearSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, GetOwner(GetOwner(SELF)), 900, DAMAGE_TYPE_PLASMA);
            // PlaySphericalEffect(OTHER);
            // NoxRestoreHealth(SELF, 100);
        }
    }
}

void throwingNuclear(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        if (GetObjectZ(sub)> 30.0)
        {
            PushTimerQueue(1,sub,throwingNuclear);
            Raise(sub,GetObjectZ(sub)-10.0);
            return;
        }
        int owner=GetOwner(sub);
        if (CurrentHealth(owner))
            SplashDamageAt(owner,GetObjectX(sub),GetObjectY(sub),220.0, onNuclearSplash);
        Frozen(sub,FALSE);
        Damage(sub,0,999,DAMAGE_TYPE_PLASMA);
    }
}

void strikeNuclear(int cre)
{
    int hak=CreateObjectById(OBJ_WASP_NEST, GetObjectX(cre),GetObjectY(cre));

    PushTimerQueue(1, hak, throwingNuclear);
    UnitNoCollide(hak);
    Frozen(hak, TRUE);
    Raise(hak, 250.0);
    SetOwner(cre, hak);
}

void ReviveFx(int unit)
{
    float x=GetObjectX(unit), y=GetObjectY(unit);

    Effect("THIN_EXPLOSION", x,y, 0.0, 0.0);
    GreenSparkFx(x,y);
    PlaySoundAround(unit, SOUND_StaffOblivionAchieve1);
}

void onShootingCollide()
{
    if (!GetTrigger())
        return;

    if (!GetCaller())
    {
        int wall =WallUtilGetWallAtObjectPosition(SELF);

        if (wall)
            WallBreak(wall);
        Effect("RICOCHET", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Delete(SELF);
        return;
    }

    int owner = GetTopParentUnit(SELF);

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (CurrentHealth(owner))
            {
                if (!IsPlayerUnit(owner))
                    return;
                int pIndex = GetPlayerIndex(owner);

                if (pIndex<0)
                    return;

                Damage(OTHER, SELF, GetPetLevel(pIndex) + 10, DAMAGE_TYPE_PLASMA);
            }
            Delete(SELF);
        }
    }
}

void ShootDefaultMissile(int pIndex, int cre, int glow)
{
    MoveWaypoint(1, GetObjectX(cre) + UnitRatioX(glow, cre, 16.0), GetObjectY(cre) + UnitRatioY(glow, cre, 16.0));
    if (Distance(GetWaypointX(1), GetWaypointY(1), GetObjectX(glow), GetObjectY(glow)) > 3.0)
    {
        AudioEvent("FlareWand", 1);
        int mis = CreateObject("ImpShot", 1);
        SetOwner(cre, mis);
        DeleteObjectTimer(mis, 24);
        PushObject(mis, 30.0, GetObjectX(cre), GetObjectY(cre));
        SetUnitCallbackOnCollide(mis, onShootingCollide);
    }
}

void PetMakeHealth(int unit, int lv)
{
    int owner = GetOwner(unit);

    if (CurrentHealth(owner))
    {
        SetUnitMaxHealth(unit, 50 + (lv * 10));
        GiveCreatureToPlayer(owner, unit);
    }
}

int GetPlayerCursor(int pIndex)
{
    int glow[MAX_PLAYER_COUNT];

    if (!IsObjectOn(glow[pIndex]))
    {
        glow[pIndex] = CreateObjectAt("Moonglow", GetObjectX(GetPlayer(pIndex)), GetObjectY(GetPlayer(pIndex)));
        SetOwner(GetPlayer(pIndex), glow[pIndex]);
    }
    return glow[pIndex];
}

void CreatureBonusSkillHandler(int pIndex)
{
    int cre = GetCreature(pIndex);

    if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_BONUS_SKILL))
        return;

    if (!UnitCheckEnchant(GetPlayer(pIndex), GetLShift(ENCHANT_HASTED)))
        return;

    strikeNuclear(cre);
    recoveryCooldown(cre, ENCHANT_HASTED, 180);
}

void CreatureLastSkillHandler(int pIndex)
{
    int cre = GetCreature(pIndex), glow = GetPlayerCursor(pIndex);

    if (!ToInt(GetObjectX(glow)))
        return;

    if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_LAST_SKILL))
        return;

    if (!UnitCheckEnchant(GetPlayer(pIndex), GetLShift(ENCHANT_PROTECT_FROM_POISON)))
        return;

    if (IsVisibleTo(cre, glow))
    {
        ManaExplosion(cre, glow);
        recoveryCooldown(cre, ENCHANT_PROTECT_FROM_POISON, 240);
        return;
    }
    UniPrint(GetPlayer(pIndex), "타겟 위치는 캐릭터가 볼 수 없는 구역입니다. 다시 시도해주세요");
}

void CreatureSecondSkillHandler(int pIndex)
{
    int cre = GetCreature(pIndex), glow = GetPlayerCursor(pIndex);

    if (!ToInt(GetObjectX(glow)))
        return;

    if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_SECOND_SKILL))
        return;

    if (!UnitCheckEnchant(GetPlayer(pIndex), GetLShift(ENCHANT_PROTECT_FROM_FIRE)))
        return;

    StartThunderBolt(cre, glow);
    recoveryCooldown(cre, ENCHANT_PROTECT_FROM_FIRE, 120);
}

void CreatureFirstSkillHandler(int pIndex)
{
    int cre = GetCreature(pIndex), glow = GetPlayerCursor(pIndex);

    if (!ToInt(GetObjectX(glow)))
        return;

    if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_FIRST_SKILL))
        return;

    if (!UnitCheckEnchant(GetPlayer(pIndex), GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY)))
        return;

    float x=GetObjectX(cre),y= GetObjectY(cre);

    PlaySoundAround(cre, SOUND_BeholderMove);
    PlaySoundAround(cre, SOUND_ElevLOTDUp);
    PlaySoundAround(cre, SOUND_ElevLOTDDown);
    GreenSparkFx(x,y);
    Effect("COUNTERSPELL_EXPLOSION", x,y, 0.0, 0.0);
    ProtossReaverScrap(cre, glow);
    recoveryCooldown(cre, ENCHANT_PROTECT_FROM_ELECTRICITY, 80);
}

void CreatureStrikeHandler(int pIndex)
{
    int cre = GetCreature(pIndex), glow = GetPlayerCursor(pIndex);

    if (!ToInt(GetObjectX(glow)))
        return;

    if (UnitCheckEnchant(cre, GetLShift(ENCHANT_BURNING)))
        return;

    Enchant(cre, "ENCHANT_BURNING", 0.1);
    ShootDefaultMissile(pIndex, cre, glow);
}

#define CREATURE_MOVING_SPEED 3.0

void CreatureWalkingHandler(int cre, int glow)
{
    if (DistanceUnitToUnit(cre, glow) > 8.0)
    {
        LookAtObject(cre, glow);
        Walk(cre, GetObjectX(cre), GetObjectY(cre));
        PushObjectTo(cre, UnitRatioX(glow, cre, CREATURE_MOVING_SPEED), UnitRatioY(glow, cre, CREATURE_MOVING_SPEED));
    }
}

void CreatureMotionStop(int pIndex)
{
    int cursor = GetPlayerCursor(pIndex);

    if (ToInt(GetObjectX(cursor)))
        LookAtObject(GetCreature(pIndex), cursor);
}

void CreatureMoving(int pIndex)
{
    int cursor = GetPlayerCursor(pIndex);

    if (!ToInt(GetObjectX(cursor)))
        return;

    CreatureWalkingHandler(GetCreature(pIndex), cursor);
}

void CallbackProcNothing(int arg)
{
    CreatureMotionStop(arg);
}

void CallbackProcLButnDown(int arg) //TODO: Strike
{
    CreatureStrikeHandler(arg);
}

void CallbackProcRButnDown(int arg) //TODO: Walking
{
    CreatureMoving(arg);
}

void CallbackProcJumpKeyDown(int arg)   //TODO: First Skill Key Handle
{
    CreatureFirstSkillHandler(arg);
}

void CallbackProcLKeyDown(int arg)
{
    return;
}

void CallbackProcJKeyDown(int arg)
{
    CreatureLastSkillHandler(arg);
}

void CallbackProcKKeyDown(int arg)
{
    CreatureSecondSkillHandler(arg);
}

int PlayerInputTable(int idx)
{
    int packet[72];

    if (idx < 0)
    {
        packet[0x06] = CallbackProcLButnDown; //Strike
        packet[0x02] = CallbackProcRButnDown; //Walk
        // packet[0x07] = CallbackProcJumpKeyDown; //Jump
        // packet[48] = CallbackProcLKeyDown; //laugh
        // packet[47] = CallbackProcJKeyDown; //taugh
        // packet[49] = CallbackProcKKeyDown; //point
        return 0;
    }
    int act = packet[idx];

    if (act)
        return act;

    return CallbackProcNothing;
}

#define KEY_A_SHIFT 1 //스킬1
#define KEY_S_SHIFT 2 //스킬2
#define KEY_D_SHIFT 4 //스킬3
#define KEY_F_SHIFT 8 //스킬4

void PlayerInputHandler(int lParam, int pIndex)
{
    int keyState=GetClientKeyState(pIndex);

    if (keyState & KEY_A_SHIFT)
    {
        CreatureFirstSkillHandler(pIndex);
    }
    if (keyState & KEY_S_SHIFT)
    {
        CreatureSecondSkillHandler(pIndex);
    }
    if (keyState & KEY_D_SHIFT)
    {
        CreatureLastSkillHandler(pIndex);
    }
    if (keyState & KEY_F_SHIFT)
    {
        CreatureBonusSkillHandler(pIndex);
    }
    CallFunctionWithArg(PlayerInputTable(lParam), pIndex);
}

void PlayerClassRemoveCreature(int pIndex)
{
    int cre=GetCreature(pIndex);

    if (!cre)
        return;

    Delete(cre);
    Delete(cre+1);
    SetCreature(pIndex, 0);
}

int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(GetPlayer(pIndex)))
        return GetPlayer(pIndex)==pUnit;
    return FALSE;
}

void PlayerClassOnShutdown(int pIndex)
{
    PlayerClassDoDeinit(pIndex);
    PlayerClassRemoveCreature(pIndex);
    SetPlayer(pIndex,0);
    SetPlayerFlags(pIndex,0);

    char buff[128];

    NoxSprintfString(buff, "playeronshutdown. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);

    int resp = GetRespawnSpot(pIndex);

    Delete(resp);
    SetRespawnSpot(pIndex, 0);
}

void PlayerClassOnDeath(int pIndex, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

void WaitForRevive(int sub)
{
    int count = GetDirection(sub), pIndex = GetDirection(sub + 1);

    if (CurrentHealth(GetPlayer(pIndex)))
    {
        if (count)
        {
            LookWithAngle(sub, count - 1);
            PushTimerQueue(1, sub, WaitForRevive);
            return;
        }
        // int hero = RevivePlayerCreature(pIndex, RespPosPtr + pIndex);
        PlayerClassCreateCreature(pIndex, GetPlayer(pIndex));
        int hero = GetCreature(pIndex);
        
        PetMakeHealth(hero, GetPetLevel(pIndex));
        UpdateCooldown(pIndex);
    }
    Delete(sub);
    Delete(sub + 1);
}

void DecreasePlayerKillScore(int pIndex)
{
    int user = GetPlayer(pIndex);

    if (GetPetLevel(pIndex) >= 3 && GetCreatureStatExp(pIndex) >= 3)
    {
        SetCreatureStatExp(pIndex, GetCreatureStatExp(pIndex)-3);
        UniPrint(user, "패널티 부여 - 킬 스코어 3 차감");
    }
    else
        UniPrint(user, "레밸 3 이상부터 영웅 사망 시 패널티가 부과됩니다");
}

void PlayerCreatureDeath()
{
    int owner = GetOwner(GetTrigger() + 1);

    UniPrintToAll("PlayercreatureDeath");
    if (!CurrentHealth(owner))
        return;
    int pIndex = GetPlayerIndex(owner);
    float x=GetObjectX(SELF), y=GetObjectY(SELF);
    int sub = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y);
    LookWithAngle(CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y), pIndex);
    SetOwner(owner, sub);
    LookWithAngle(sub, 180);
    PushTimerQueue(1, sub, WaitForRevive);
    UniPrint(owner, "당신의 캐릭터가 적에게 격추 되었습니다, 잠시 후 다시 부활됩니다");
    PushTimerQueue(1, pIndex, DecreasePlayerKillScore);
}

int WizardGreenBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 1917281394; arr[2] = 7234917; arr[16] = 95000; arr[17] = 160; 
		arr[18] = 50; arr[19] = 80; arr[23] = 32768; arr[24] = 1066947052; arr[26] = 4; 
		arr[28] = 1092616192; arr[29] = 1; arr[32] = 9; arr[33] = 17; arr[53] = 1128792064; 
		arr[54] = 4; arr[59] = 5542784; arr[60] = 1335; arr[61] = 46914816; 
	pArr = arr;
	return pArr;
}

void WizardGreenSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 0;		ptr[137] = 0;
	int *hpTable = ptr[139];
	hpTable[0] = 160;	hpTable[1] = 160;
	int *uec = ptr[187];
	uec[360] = 32768;		uec[121] = WizardGreenBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 0;
}

void onCreatureCollide()
{
    if (MaxHealth(SELF))
    {
        int user = GetOwner(SELF);

        if (!CurrentHealth(user))
            return;

        if (IsPickupableItem(OTHER))
        {
            Pickup(user, OTHER);
            UniPrint(user, "아이템을 획득하였습니다");
            PlaySoundAround(SELF, SOUND_KeyPickup);
            Delete(SELF);
        }
    }
}

void onLevelMeterMoving(int meter)
{
    if (ToInt(GetObjectX(meter)))
    {
        int cre=GetUnit1C(meter);

        if (CurrentHealth(cre))
        {
            PushTimerQueue(1, meter,onLevelMeterMoving);
            if (ToInt(DistanceUnitToUnit(meter,cre)))
                MoveObject(meter,GetObjectX(cre),GetObjectY(cre));
            return;
        }
        Delete(meter);
    }
}

void attachLevelMeter(int cre, float x, float y)
{
    int meter=CreateObjectById(OBJ_TELEPORT_GLYPH_1, x,y);

    PushTimerQueue(1, meter, onLevelMeterMoving);
    SetOwner(GetOwner(cre), meter);
    SetUnit1C(meter, cre);
}

int createUserCreature(int pIndex, float x, float y)
{
    int user = GetPlayer(pIndex);
    int unit = CreateObjectById(OBJ_WIZARD_GREEN, x,y);

    WizardGreenSubProcess(unit);
    SetOwner(user, CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH, x,y));
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020);  //ALWAYS_RUN
    LookWithAngle(unit + 1, pIndex);
    SetOwner(user, unit);
    SetCallback(unit, 5, PlayerCreatureDeath);
    // SetCallback(unit, 7, PlayerCreatureRisk);
    Enchant(unit, "ENCHANT_INVULNERABLE", 1.0);
    attachLevelMeter(unit,x,y);
    AttachHealthbar(unit);
    return unit;
}

void PlayerClassCreateCreature(int pIndex, int user)
{
    PlayerClassRemoveCreature(pIndex);
    int resp = GetRespawnSpot(pIndex);
    int cre = createUserCreature(pIndex, GetObjectX(resp), GetObjectY(resp));
    
    ReviveFx(cre);
    PetMakeHealth(cre, GetPetLevel(pIndex));
    SetCreature(pIndex, cre);
}

void putClickDetector(int user, float x, float y)
{
    int sub=DummyUnitCreateById(OBJ_BOMBER, x,y);

    SetUnitFlags(sub,GetUnitFlags(sub)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetCallback(sub,9, onCreatureCollide);
    SetOwner(user, sub);
    DeleteObjectTimer(sub, 1);
}

void tryPickupItem(int pIndex, int user)
{
    int cursor=GetPlayerCursor(pIndex);

    if (!ToInt(GetObjectX(cursor)))
        return;

    int cre = GetCreature(pIndex);

    if (DistanceUnitToUnit(cursor, cre) < 92.0)
    {
        float x=GetObjectX(cursor),y=GetObjectY(cursor);

        putClickDetector(user, x,y);
        DeleteObjectTimer(CreateObjectById(OBJ_MAGIC_SPARK, x,y), 18);
    }
}

void PlayerClassCreatureLoop(int pIndex, int user)
{
    int cre = GetCreature(pIndex);

    if (MaxHealth(cre))
    {
        if (CheckWatchFocus(    user ))
        {
            PlayerLook(user, cre);
            if (CheckPlayerInput(user)== 6)
                tryPickupItem(pIndex, user);
        }

        if (CurrentHealth(cre))
        {
            PlayerInputHandler(CheckPlayerInput(user), pIndex);
            if (UnitCheckEnchant(cre, GetLShift(ENCHANT_SLOWED)))
            {
                EnchantOff(cre, EnchantList(ENCHANT_SLOWED));
                Damage(cre, 0, 13, DAMAGE_TYPE_POISON);
            }
        }
    }
}

void PlayerClassOnAlive(int pIndex, int user)
{
    PlayerClassCreatureLoop(pIndex, user);
}

void PlayerClassOnLoop(int pIndex)
{
    int user = GetPlayer(pIndex);

    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (GetPlayerFlags(pIndex))
        {
            PlayerClassOnShutdown(pIndex);
            return;
        }
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

void PlayerClassOnInit(int pIndex, int pUnit, char *bInit)
{
    SetPlayer(pIndex, pUnit);
    SetPlayerFlags(pIndex,1);
    PlayerClassDoInitialize(pIndex, pUnit);

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
            commonServerClientProcedure();
        // PushTimerQueue(60, pUnit, NetPlayCustomBgm);
        bInit[0]=TRUE;
    }
    SetClientKeyState(pIndex, 0);
    ChangeGold(pUnit, -GetGold(pUnit));
    EmptyAll(pUnit);
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);

    char buff[128];

    NoxSprintfString(buff, "playeroninit. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);

    InitiPlayerCamera(pUnit);
    SetCreatureStatExp(pIndex, 0);

    int spot = CreateObjectById(OBJ_PLAYER_WAYPOINT, LocationX(PLAYER_CREATURE_START_MARK), LocationY(PLAYER_CREATURE_START_MARK));

    SetRespawnSpot(pIndex, spot);
}

void PlayerClassOnJoin(int user, int pIndex)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
    // if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_ALL_BUFF))
    //     PlayerSetAllBuff(pUnit);
    
    PlayerClassCreateCreature(pIndex, user);
    SetUnitEnchantCopy(user, GetLShift(ENCHANT_ANTI_MAGIC)|GetLShift(ENCHANT_ANCHORED)|GetLShift(ENCHANT_FREEZE)|GetLShift(ENCHANT_INVULNERABLE));
    MoveObject(user, LocationX(PLAYER_START_LOCATION), LocationY(PLAYER_START_LOCATION));
    LookWithAngle(user, 64);
    Effect("TELEPORT", GetObjectX(user), GetObjectY(user), 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int pIndex)
{
    // ShowQuestIntroOne(user, 999, "WarriorChapterBegin6", "GeneralPrint:MapNameSanturary");
    UniPrint(user, "<<------ 여신 키우기------ 제작. 237 --------------------------");
    PlayerClassOnJoin(user, pIndex);
}

void PlayerClassOnEntry(int plrUnit)
{
    if (!CurrentHealth(plrUnit))
        return;

    int pIndex = GetPlayerIndex(plrUnit);
    char initialUser=FALSE;

    if (!MaxHealth(GetPlayer(pIndex)))
        PlayerClassOnInit(pIndex, plrUnit, &initialUser);
    if (pIndex >= 0)
    {
        if (initialUser)
            OnPlayerDeferredJoin(plrUnit, pIndex);
        else
            PlayerClassOnJoin(plrUnit, pIndex);
        return;
    }
    PlayerClassOnEntryFailure(plrUnit);
}

void onFastJoinMiss(int user, int pIndex)
{
    MoveObject(user, LocationX(PLAYER_INITIAL_LOCATION), LocationY(PLAYER_INITIAL_LOCATION));
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        int pIndex= GetPlayerIndex(OTHER);
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (checkIsPlayerAlive(pIndex, GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
            onFastJoinMiss(OTHER, pIndex);
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

void ResetAllPlayerResources()
{
    SendItemEnchantData(-1, 0);
}

void ChangeLevelMeter(int lv)
{
    char buff[64];

    NoxSprintfString(buff, "레밸: %d", &lv, 1);
    NoxUtf8ToUnicode(buff, GetLevelBuffer());
}

void UpdateLevelMeter(int pIndex)
{
    int lv=GetPetLevel(pIndex);

    if (pIndex==31)
    {
        ChangeLevelMeter(lv);
        return;
    }
    ClientSetMemory(GetPlayer(pIndex), _CLIENT_OPTION_TYPE_OFF_, lv);
}
