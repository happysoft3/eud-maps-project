
#include "fov_resource.h"
#include "fov_player.h"
#include "fov_disposGen.h"
#include "fov_shop.h"
#include "subway_initscan.h"
#include "libs/format.h"
#include "libs\coopteam.h"
#include "libs\playerupdate.h"
#include "libs\reaction.h"
#include "libs\potionex.h"
#include "libs\weaponcapacity.h"
#include "libs\fixtellstory.h"
#include "libs\spellutil.h"
#include "libs/groupUtils.h"

int LastProcUnit;

#define PLAYER_SKILL_FLAG 4
#define PLAYER_ALL_ENCHANT 8


void EndScan(int *pParams)
{
    int count =pParams[UNITSCAN_PARAM_COUNT];
    char msg[128];

    NoxSprintfString(msg, "%d개의 유닛을 스캔했습니다", &count, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
    // WallOpen(Wall(15, 235));
    // WallOpen(Wall(16, 236));
    // int lunit;
    // QueryLUnit(&lunit,0,0);
    // AwakeLineMonster(lunit);
    // SecondTimer(2, StartGameMent);
}

void startInitscan(){
    int initScanHash, lastUnit=GetMaster();
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    // HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, OnMonsterMarker);
    StartUnitScanEx(Object("firstscan"), lastUnit, initScanHash, EndScan);
}

void OnShutdownMap()
{
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
}
int fireHellEnterStorage(){
    int ent[2];

    if (!ent[0]){
        ent[0]=PlacingInvisibleTeleporting(271,270);
        ent[1]=PlacingInvisibleTeleporting(272,270);
    }
    return ent;
}

void onExitFireHell(){
    if(CurrentHealth(OTHER)){
    int* ents=fireHellEnterStorage();

    DeleteInvisibleTelpo(ents[0]);
    DeleteInvisibleTelpo(ents[1]);
    onCollideInvisbleTelepo();
    }
}

void initEntryFireHell()
{
    SetUnitCallbackOnCollide( PlacingInvisibleTeleporting(268,269), onExitFireHell);

    fireHellEnterStorage();
}

void DelayInitRun()
{
    JailRow();
    FrameTimer(1, InitArrowTraps);
    FrameTimer(2, InitForestSecretZone);
    IceBlock();
    RegistSignMessage(Object("Sign01"), "특별한 물건을 취급하는 장소입니다. 현질 요소가 많은 곳이기도 하죠");
    initEntryFireHell();
}

void OnInitializeMap()
{
    LastProcUnit = CreateObject("InvisibleLightBlueLow", 1);

    MusicEvent();
    startInitscan();
    PlayerRespawnMark(0);
    
    FrameTimerWithArg(30, Random(0, 2), SelectThemaPart);
    int unit = CreateObject("Wizard", 48);
    SetCallback(unit, 9, UndergroundWallsOpen);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    FrameTimerWithArg(30, Object("PartitionElev"), DisableObject);
    FrameTimer(1, DelayInitRun);
    FrameTimer(1, MakeCoopTeam);
    blockObserverMode();
    InitializePlayerSystem();
    InitializeShopSystem();
}

void SelectThemaPart(int rnd)
{
    int thema[] = {PlaceGensPart1A, PlaceGensPart1B, PlaceGensPart1C};

    CallFunction(thema[rnd]);
}

void Part1RightGateOpen()
{
    if (IsLocked(Object("Part1LockedDoor")))
    {
        UnlockDoor(Object("Part1LockedDoor"));
        UniPrint(OTHER, "맞은편 게이트의 잠금이 해제되었습니다");
        ObjectOff(SELF);
    }
}

void Part1BGateOpen()
{
    UnlockDoor(Object("AAALockdoor2"));
    ObjectOff(SELF);
    UniPrint(OTHER, "주변 어딘가 문의 잠금이 해제되었습니다");
}

void LeftGateOpen()
{
    PlaySoundAround(SELF, 782);
    UnlockDoor(Object("AAAGate1"));
    UnlockDoor(Object("AAAGate2"));
    ObjectOff(SELF);
    UniPrint(OTHER, "좌측 게이트의 잠금이 해제되었습니다");
}

void Part1AllGateOpen()
{
    PlaySoundAround(SELF, 782);

    int k;
    for (k = 0 ; k < 5 ; k ++)
        UnlockDoor(Object("AALockdoor" + IntToString(k + 1)));
    MidBoss(224);
    ObjectOff(SELF);
}

void UndergroundWallsOpen()
{
    if (IsObjectOn(SELF) && IsPlayerUnit(OTHER))
    {
        PlaySoundAround(OTHER, 505);
        DeleteObjectTimer(CreateObjectAt("ReleasedSoul", GetObjectX(OTHER), GetObjectY(OTHER)), 12);
        int k;

        for (k = 0 ; k < 6 ; Nop(k ++))
        {
            WallOpen(Wall(185 + k, 159 + k));
            WallBreak(Wall(184 + k, 160 + k));
        }
        ObjectOn(Object("FireLit"));
        ObjectOff(SELF);
    }
}

void ClearSewerSecretZoneWall()
{
    ObjectOff(SELF);
    WallOpen(Wall(19, 115));
}

void ClearForestSecretZoneWall()
{
    WallOpen(Wall(167, 57));
    WallOpen(Wall(168, 58));
    ObjectOff(SELF);
    UniPrint(OTHER, "비밀의 벽이 열립니다");
}

void EnableObject(int unit)
{
    ObjectOn(unit);
}

void PartitionElevatorControl()
{
    ObjectToggle(Object("PartitionElev"));
    ObjectOff(SELF);
    FrameTimerWithArg(90, GetTrigger(), EnableObject);
}

int InvisibleBeacon(int location)
{
    int unit = CreateObject("WeirdlingBeast", location);

    SetUnitMaxHealth(unit, 30);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    return unit;
}

void MovingSecretLibrary(int unit)
{
    int count = GetDirection(unit);

    while (IsObjectOn(unit))
    {
        if (count < 69)
        {
            if (count < 23)
                MoveObjectVector(unit, 1.0, 1.0);
            else
                MoveObjectVector(unit, 1.0, -1.0);
            LookWithAngle(unit, ++count);
            FrameTimerWithArg(1, unit, MovingSecretLibrary);
            break;
        }
        WallOpen(Wall(167, 45));
        Delete(unit);
        break;
    }
}

void OnCollideOpenSecretLibrary()
{
    if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER) && MaxHealth(SELF))
    {
        FrameTimerWithArg(30, GetTrigger() + 1, MovingSecretLibrary);
        Delete(SELF);
        UniPrint(OTHER, "비밀의 벽장이 개방됩니다");
    }
}

void DelayPlacingCandleOnBeacon(int posUnit)
{
    if (MaxHealth(posUnit))
    {
        CreateObjectAt("Candleabra5", GetObjectX(posUnit), GetObjectY(posUnit));
    }
}

void StartPointWayBlocks()
{
    int var0 = Object("PointWayTrap1");

    if (Distance(GetObjectX(var0), GetObjectY(var0), GetWaypointX(243), GetWaypointY(243)) < 23.0)
    {
        Move(var0, 245);
        Move(Object("PointWayTrap12"), 244);
    }
}

void BackPointwayBlocks()
{
    int var0 = Object("PointWayTrap1");

    if (Distance(GetObjectX(var0), GetObjectY(var0), GetWaypointX(243), GetWaypointY(243)) > 100.0)
    {
        Move(var0, 243);
        Move(Object("PointWayTrap12"), 242);
    }
}

#define GROUP_fireHellWalls 6
void openFireWalls(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_fireHellWalls, WALL_GROUP_ACTION_OPEN);
}

#define GROUP_part2Walls 0
void openPart2Walls()
{
    ObjectOff(SELF);
    WallGroupSetting(GROUP_part2Walls, WALL_GROUP_ACTION_OPEN);
}

#define GROUP_hiddenWalls 1
void openHiddenWalls()
{
    ObjectOff(SELF);
    WallGroupSetting(GROUP_hiddenWalls,WALL_GROUP_ACTION_OPEN);
}

#define GROUP_secretWalls 2
void openSecretws(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_secretWalls,WALL_GROUP_ACTION_OPEN);
}

#define GROUP_blueSecret 3
void openBlueSecret(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_blueSecret,WALL_GROUP_ACTION_OPEN);
}

#define GROUP_singleWalls 4
void openSingleWall(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_singleWalls,WALL_GROUP_ACTION_OPEN);
}
#define GROUP_leverExitWalls 5
void openLeverExit(){
    ObjectOff(SELF);
    WallGroupSetting(GROUP_leverExitWalls,WALL_GROUP_ACTION_OPEN);
}
void PartitionWayPlacingGen()
{
    int firstgen;

    ObjectOff(SELF);
    if (!firstgen)
    {
        GeneratorEx(&firstgen, 240, 10, 22, 8, 4);
        Generator(241, 31, 21, 8, 4);
        Generator(246, 24, 4, 4, 16);
        Generator(247, 25, 3, 4, 16);
        Generator(248, 26, 33, 4, 16);
        Generator(249, 35, 11, 10, 1);

        Generator(250, 3, 15, 7, 4);
        Generator(251, 27, 31, 7, 4);

        Generator(252, 27, 31, 7, 4);
        Generator(253, 49, 16, 14, 4);

        Generator(254, 27, 31, 7, 4);
        Generator(255, 27, 31, 7, 4);

        Generator(256, 2, 14, 7, 4);
        Generator(257, 3, 15, 7, 4);

        Generator(258, 49, 16, 10, 4);
        Generator(259, 18, 5, 10, 4);
    }
}

void ClearPointWayExitWalls()
{
    int i;

    ObjectOff(SELF);
    for (i = 0 ; i < 4 ; i ++)
        WallOpen(Wall(81 - i, 189 + i));
}

void InitForestSecretZone()
{
    int switchUnit = InvisibleBeacon(239);
    Frozen(CreateObject("MovableBookcase1", 238), 1);

    SetCallback(switchUnit, 9, OnCollideOpenSecretLibrary);
    FrameTimerWithArg(90, switchUnit, DelayPlacingCandleOnBeacon);
}

void InitArrowTraps()
{
    Enchant(CreateObject("RubyKey", 79), "ENCHANT_FREEZE", 0.0);
    Enchant(Object("BossEastGate"), "ENCHANT_FREEZE", 0.0);
    Enchant(Object("BossWestGate"), "ENCHANT_FREEZE", 0.0);
    NorthArrowTrp();
    SouthArrowTrp();
    SouthArrowTrp2();
    SouthArrowTrp3();
    SouthArrowTrp4();
    EastSouthArrow();
    FrameTimer(1, InitSoulGatePlaced);
    FrameTimer(2, JustDecorations);
}

int NorthArrowTrp()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("AmbBeachBirds", 53);
        for (k = 0 ; k < 7 ; k ++)
        {
            ObjectOff(CreateObjectAt("ArrowTrap1", LocationX(53), LocationY(53)));
            TeleportLocationVector(53, 12.0, -12.0);
        }
        LookWithAngle(ptr, 7);
        Raise(ptr, ToFloat(160));
    }
    return ptr;
}

int SouthArrowTrp()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("AmbBeachBirds", 54);
        for (k = 0 ; k < 8 ; k ++)
        {
            ObjectOff(CreateObjectAt("ArrowTrap1", LocationX(54), LocationY(54)));
            TeleportLocationVector(54, -12.0, 12.0);
        }
        LookWithAngle(ptr, 8);
        Raise(ptr, ToFloat(32));
    }
    return ptr;
}

int SouthArrowTrp2()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("AmbBeachBirds", 55);
        LookWithAngle(ptr, 10);
        Raise(ptr, ToFloat(32));
        for (k = 0 ; k < GetDirection(ptr) ; k ++)
        {
            ObjectOff(CreateObjectAt("ArrowTrap1", LocationX(55), LocationY(55)));
            TeleportLocationVector(55, -12.0, 12.0);
        }
    }
    return ptr;
}

int SouthArrowTrp3()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("AmbBeachBirds", 56);
        LookWithAngle(ptr, 7);
        Raise(ptr, ToFloat(32));
        for (k = 0 ; k < GetDirection(ptr) ; k ++)
        {
            ObjectOff(CreateObjectAt("ArrowTrap1", LocationX(56), LocationY(56)));
            TeleportLocationVector(56, -12.0, 12.0);
        }
    }
    return ptr;
}

int SouthArrowTrp4()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("AmbBeachBirds", 57);
        LookWithAngle(ptr, 7);
        Raise(ptr, ToFloat(32));
        for (k = 0 ; k < GetDirection(ptr) ; k ++)
        {
            ObjectOff(CreateObjectAt("ArrowTrap1", LocationX(57), LocationY(57)));
            TeleportLocationVector(57, -12.0, 12.0);
        }
    }
    return ptr;
}

void ShotNorthArrow()
{
    EnableArrowTraps(NorthArrowTrp());
}

void ShotSouthArrow()
{
    EnableArrowTraps(SouthArrowTrp());
}

void ShotSouthArrow2()
{
    EnableArrowTraps(SouthArrowTrp2());
}

void ShotSouthArrow3()
{
    EnableArrowTraps(SouthArrowTrp3());
}
void ShotSouthArrow4()
{
    EnableArrowTraps(SouthArrowTrp4());
}

void EnableArrowTraps(int ptr)
{
    int count = GetDirection(ptr), k;

    for (k = 0 ; k < count ; k ++)
    {
        LookWithAngle(ptr + 1 + k, ToInt(GetObjectZ(ptr)));
        ObjectOn(ptr + 1 + k);
    }
    FrameTimerWithArg(1, ptr, DisableArrowTraps);
}

void DisableArrowTraps(int ptr)
{
    int count = GetDirection(ptr), k;

    for (k = 0 ; k < count ; k ++)
        ObjectOff(ptr + 1 + k);
}

void Gen1WallOpen()
{
    int k;

    ObjectOff(SELF);
    UniPrint(OTHER, "벽이 열립니다");
    for (k = 0 ; k < 5 ; k ++)
        WallOpen(Wall(67 + k, 63 + k));
    Generator(99, 8, 28, 5, 8);
    Generator(100, 33, 20, 5, 4);
    Generator(101, 6, 17, 5, 8);
}

void Gen2WallOpen()
{
    int count, k;

    ObjectOff(SELF);
    if ((++count) == 3)
    {
        for (k = 0 ; k < 5 ; k ++)
            WallOpen(Wall(65 + k, 65 + k));
        if (!Random(0, 2))
        {
            Generator(102, 9, 29, 10, 4);
            Generator(103, 10, 22, 15, 4);
            Generator(104, 39, 24, 10, 4);
        }
        else
        {
            Generator(102, 12, 30, 5, 8);
            Generator(103, 27, 31, 5, 8);
            Generator(104, 20, 27, 1, 8);
        }
    }
}

void Gen3WallOpen()
{
    int count, k;

    ObjectOff(SELF);
    if ((++count) == 3)
    {
        for (k = 0 ; k < 5 ; k ++)
            WallOpen(Wall(63 + k, 67 + k));
        Generator(105, 19, 25, 10, 2);
        if (Random(0, 2))
            Generator(106, 28, 9, 10, 2);
        else
            Generator(106, 18, 5, 10, 4);
        Generator(107, 15, 19, 10, 4);
    }
}

void LastGenWallOpen()
{
    int count, k;

    ObjectOff(SELF);
    if ((++count) == 3)
    {
        for (k = 0 ; k < 5 ; k ++)
            WallOpen(Wall(61 + k, 69 + k));
        
        int genf[] = {PlaceGensPart7A, PlaceGensPart7B, PlaceGensPart7C};

        FrameTimer(1, genf[Random(0, 2)]);
    }
}

void OpenWestWalls()
{
    int k;
    ObjectOff(SELF);
    for (k = 0 ; k < 6 ; k ++)
        WallOpen(Wall(93 + k, 119 + k));
    UniPrint(OTHER, "벽이 열렸습니다");
}

int EastSouthArrow()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("AmbBeachBirds", 142);
        LookWithAngle(ptr, 32);
        for (k = 0 ; k < 32 ; k ++)
        {
            ObjectOff(CreateObject("ArrowTrap1", 142));
        }
        FrameTimerWithArg(1, ptr, SetESArrowDirection);
        FrameTimerWithArg(2, ptr, ESArrowOff);
    }
    return ptr;
}

void SetESArrowDirection(int ptr)
{
    int k, max = GetDirection(ptr);

    for (k = 0 ; k < max ; k ++)
    {
        LookWithAngle(ptr + 1 + k, 225 + (k * 2));
        ObjectOn(ptr + 1 + k);
    }
}

void ESArrowOn(int ptr)
{
    int k, max = GetDirection(ptr);

    for (k = 0 ; k < max ; k ++)
        ObjectOn(ptr + 1 + k);
}

void ESArrowOff(int ptr)
{
    int k, max = GetDirection(ptr);

    for (k = 0 ; k < max ; k ++)
        ObjectOff(ptr + 1 + k);
}

void EnableRingArrowTrap()
{
    ESArrowOn(EastSouthArrow());
    FrameTimerWithArg(1, EastSouthArrow(), ESArrowOff);
}

int BlueWayArrowTrp()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueLow", 143);
        LookWithAngle(ptr, 15);
        Raise(ptr, ToFloat(96));
        for (k = 0 ; k < 15 ; k ++)
        {
            ObjectOff(CreateObjectAt("ArrowTrap2", LocationX(143), LocationY(143)));
            TeleportLocationVector(143, 12.0, 12.0);
        }
    }
}

void ShotBlueWayTrap()
{
    EnableArrowTraps(BlueWayArrowTrp());
    FrameTimerWithArg(1, BlueWayArrowTrp(), DisableArrowTraps);
}

void HiddenGenWallsOpen1()
{
    ObjectOff(SELF);
    WallOpen(Wall(76, 134));
    WallOpen(Wall(81, 139));
}

void HiddenGenWallsOpen2()
{
    ObjectOff(SELF);
    WallOpen(Wall(72, 138));
    WallOpen(Wall(77, 143));
}

void EastWallOpen()
{
    int k;

    ObjectOff(SELF);
    for (k = 0 ; k < 12 ; k ++)
    {
        if (k < 9)
            WallOpen(Wall(108 + k, 156 + k));
        WallOpen(Wall(105 + k, 159 + k));
    }
    FrameTimerWithArg(1, Random(0, 2), RespectEastWallGens);
    UniPrint(OTHER, "동쪽 벽이 열립니다");
}

void RespectEastWallGens(int type)
{
    if (!type)
    {
        Generator(144, 7, 27, 5, 8);
        Generator(145, 12, 30, 5, 8);
        Generator(146, 9, 29, 10, 4);
    }
    else if (type == 1)
    {
        Generator(144, 2, 24, 5, 8);
        Generator(145, 5, 39, 5, 16);
        Generator(146, 28, 9, 10, 2);
    }
    else
    {
        Generator(144, 24, 4, 5, 8);
        Generator(145, 25, 3, 5, 8);
        Generator(146, 23, 13, 10, 4);
    }
}

void WestWallOpen()
{
    int k;
    ObjectOff(SELF);
    for (k = 0 ; k < 3 ; k ++)
    {
        WallOpen(Wall(96 + k, 178 + k));
        WallOpen(Wall(105 + k, 187 + k));
    }
    UniPrint(OTHER, "서쪽 벽이 열립니다");
}

void FastMovingWalk()
{
    if (CurrentHealth(OTHER))
    {
        int unit;

        if (!HasEnchant(OTHER, "ENCHANT_LIGHT"))
        {
            Enchant(OTHER, "ENCHANT_LIGHT", 6.0);
            if (GetObjectX(OTHER) > GetObjectX(SELF) && GetObjectY(OTHER) > GetObjectY(SELF))
            {
                EnchantOff(OTHER, "ENCHANT_PROTECT_FROM_MAGIC");
                Enchant(OTHER, "ENCHANT_VILLAIN", 0.0);
            }
            else if (GetObjectX(OTHER) < GetObjectX(SELF) && GetObjectY(OTHER) < GetObjectY(SELF))
            {
                EnchantOff(OTHER, "ENCHANT_VILLAIN");
                Enchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
            }
        }
        else if (HasEnchant(OTHER, "ENCHANT_VILLAIN"))
        {
            unit = CreateObjectAt("CarnivorousPlant", GetObjectX(OTHER) + 0.5, GetObjectY(OTHER) + 0.5);
            Frozen(unit, 1);
            DeleteObjectTimer(unit, 1);
        }
        else if (HasEnchant(OTHER, "ENCHANT_PROTECT_FROM_MAGIC"))
        {
            unit = CreateObjectAt("CarnivorousPlant", GetObjectX(OTHER) - 0.5, GetObjectY(OTHER) - 0.5);
            Frozen(unit, 1);
            DeleteObjectTimer(unit, 1);
        }
    }
}

void BackPusher(int subsub)
{
    if (GetObjectX(subsub) >= 3381.0)
    {
        MoveObjectVector(subsub, -5.0, 5.0);
        MoveObjectVector(subsub + 1, -5.0, 5.0);
        FrameTimerWithArg(1, subsub, BackPusher);
    }
    else
        LookWithAngle(subsub, 0);
}

void MovingPusher(int subsub)
{
    if (GetObjectX(subsub) <= 3726.0)
    {
        MoveObjectVector(subsub, 5.0, -5.0);
        MoveObjectVector(subsub + 1, 5.0, -5.0);
        FrameTimerWithArg(1, subsub, MovingPusher);
    }
    else
        FrameTimerWithArg(60, subsub, BackPusher);
}

int JailRow()
{
    int baseunit;

    if (!baseunit)
    {
        baseunit = CreateObject("SpikeBlock", 163);
        Frozen(CreateObject("SpikeBlock", 164), 1);
        Frozen(baseunit, 1);
    }
    return baseunit;
}

void StartPusherBlock()
{
    int baseunit = JailRow();

    if (!GetDirection(baseunit))
    {
        UniPrint(OTHER, "트랩이 작동하기 시작했습니다");
        AudioEvent("Clank2", 163);
        LookWithAngle(baseunit, 1);
        FrameTimerWithArg(1, baseunit, MovingPusher);
    }
}

int IceBlock()
{
    int baseunit;

    if (!baseunit)
    {
        baseunit = CreateObject("StoneBlock", 170);
        Enchant(CreateObject("StoneBlock", 171), "ENCHANT_FREEZE", 0.0);
        Enchant(baseunit, "ENCHANT_FREEZE", 0.0);
        Frozen(baseunit, 1);
        Frozen(baseunit + 1, 1);
    }
    return baseunit;
}

void MovingIceBlock(int subunit)
{
    int count = GetDirection(subunit);

    if (count < 92)
    {
        MoveObjectVector(subunit, 1.0, -1.0);
        MoveObjectVector(subunit + 1, -1.0, 1.0);
        LookWithAngle(subunit, ++count);
        FrameTimerWithArg(1, subunit, MovingIceBlock);
    }
}

void OpenIceBlock()
{
    PlaySoundAround(OTHER, 910);
    PlaySoundAround(OTHER, 781);
    ObjectOff(SELF);
    FrameTimerWithArg(1, IceBlock(), MovingIceBlock);
}

int SpawnCaptainBoss(float x,float y)
{
    int unit = CreateObjectById(OBJ_HORRENDOUS, x,y);

    SetOwner(GetMaster(), unit);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    SetCallback(unit, 3, BossSightEvent);
    CreatureGuard(unit, 0.0, 0.0, 0.0, 0.0, 600.0);
    SetUnitMaxHealth(unit, 930);
    return unit;
}

void BossUnitFreezenRun(int unit)
{
    int ptr;

    if (CurrentHealth(unit))
    {
        ptr = UnitToPtr(unit);
        if (!(GetMemory(GetMemory(ptr + 0x2ec) + 0x228) ^ 15) && !GetMemory(GetMemory(ptr + 0x2ec) + 0x29c))
            MoveObjectVector(unit, UnitAngleCos(unit, 5.0), UnitAngleSin(unit, 5.0));
        FrameTimerWithArg(1, unit, BossUnitFreezenRun);
    }
}

void BossSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_SNEAK"))
    {
        LookAtObject(SELF, OTHER);
        CreatureFollow(SELF, OTHER);
        AggressionLevel(SELF, 1.0);
        FrameTimerWithArg(1, GetTrigger(), BossSightEvent);
        Enchant(SELF, "ENCHANT_SNEAK", 6.0);
    }
    CheckResetSight(GetTrigger(), 25);
}

void ShotTripleShurikens(int unit)
{
    int baseunit = CreateObject("AmbBeachBirds", 1) + 1;

    Delete(baseunit - 1);
    if (CurrentHealth(unit))
    {
        int dir = GetDirection(unit) - 15, k;
        for (k = 0 ; k < 32 ; Nop(k ++))
        {
            SetOwner(unit, CreateObjectAt("OgreShuriken", GetObjectX(unit) + UnitAngleCos(unit, 20.0), GetObjectY(unit) + UnitAngleSin(unit, 20.0)));
            LookWithAngle(unit, dir + k);
            //LookWithAngle(ptr + k, GetDirection(unit));
            PushObjectTo(baseunit + k, UnitRatioX(baseunit + k, unit, 35.0), UnitRatioY(baseunit + k, unit, 35.0));
        }
    }
}

void RespectBossUnit()
{
    ObjectOff(SELF);
    float x= GetObjectX(SELF),y= GetObjectY(SELF);
    int boss=SpawnCaptainBoss(x,y);
    PlaySoundAround(boss,SOUND_QuestRespawn);
    DrawGeometryRing(OBJ_HEAL_ORB,x,y);
    SetCallback(boss, 5, BossDeath);
}

void BossDeath()
{
    UniPrintToAll("[!!] 게이트의 잠금이 해제되었습니다");
    UnlockDoor(Object("BossEastGate"));
    UnlockDoor(Object("BossWestGate"));
    Generator(177, 24, 4, 5, 32);
    Generator(178, 25, 3, 5, 32);
    Generator(179, 2, 14, 5, 32);
    Generator(180, 27, 31, 5, 32);
    Generator(181, 7, 27, 5, 32);
    Generator(182, 9, 29, 5, 32);
}

void OpenGeneratorWalls()
{
    int k, ck;

    ObjectOff(SELF);
    if (!ck)
    {
        ck = 1;
        for (k = 0 ; k < 14 ; Nop(k ++))
        {
            WallOpen(Wall(180 - k, 206 + k));
            if (k < 8)
            {
                if (k < 3)
                    WallOpen(Wall(166 + k, 204 + k));
                WallOpen(Wall(169 - k, 207 + k));
                WallOpen(Wall(172 + k, 198 + k));
            }
        }
    }
}

#define FINALBOSS_XVECT 0
#define FINALBOSS_YVECT 1
#define FINALBOSS_OWNER 2
#define FINALBOSS_DURATION 3
#define FINALBOSS_SUB 4
#define FINALBOSS_MAX 5

void onBossSkillSplash(){
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 100, DAMAGE_TYPE_PLASMA);
        }
    }
}

void onBossSkillCollide(){
    if (!GetTrigger())
        return;

    if (IsAttackedBy(OTHER, SELF))
    {
        if (CurrentHealth(OTHER))
        {
            int owner=GetOwner(SELF);

            if (CurrentHealth(owner))
            {
                SplashDamageAtEx(owner, GetObjectX(OTHER),GetObjectY(OTHER), 100.0, onBossSkillSplash);
                DrawGeometryRing(OBJ_CHARM_ORB, GetObjectX(OTHER),GetObjectY(OTHER));
                Delete(SELF);
            }
        }
    }
}

void onBossSkillDuration(int *d){
    int boss=d[FINALBOSS_OWNER];

    if (CurrentHealth(boss))
    {
        int sub=d[FINALBOSS_SUB];
        if (IsVisibleOr(sub,boss))
        {
            if (--d[FINALBOSS_DURATION]>=0)
            {
                float *vect=&d[FINALBOSS_XVECT];
                MoveObjectVector(sub,vect[0],vect[1]);
                int trp=CreateObjectById(OBJ_BEAR_TRAP, GetObjectX(sub),GetObjectY(sub));
                SetOwner(boss,trp);
                DeleteObjectTimer(trp, 6);
                PushTimerQueue(1,d,onBossSkillDuration);
                return;
            }
        }
        Delete(sub);
    }
    FreeSmartMemEx(d);
}

void bossSkillTriggered()
{
    if (!GetCaller())
        return;

    float xvect=UnitRatioX(OTHER,SELF,14.0),yvect=UnitRatioY(OTHER,SELF,14.0);
    int sub=CreateObjectById(OBJ_PLAYER_WAYPOINT, GetObjectX(SELF)+xvect, GetObjectY(SELF)+yvect);

    int *d;
    AllocSmartMemEx(FINALBOSS_MAX*4,&d);
    d[FINALBOSS_XVECT]=xvect;
    d[FINALBOSS_YVECT]=yvect;
    d[FINALBOSS_OWNER]=GetTrigger();
    d[FINALBOSS_DURATION]=32;
    d[FINALBOSS_SUB]=sub;
    PushTimerQueue(1,d,onBossSkillDuration);
    UnitNoCollide(sub);
    Frozen(sub,TRUE);
    SetOwner(SELF,sub);
    SetUnitFlags(sub,GetUnitFlags(sub)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetUnitCallbackOnCollide(sub,onBossSkillCollide);
}

int AirshipCaptainBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 3000; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1121058816; arr[29] = 1; arr[31] = 17; arr[32] = 12; arr[33] = 20; 
		arr[59] = 5542784; arr[60] = 1387; arr[61] = 46915328; 
	pArr = arr;
    CustomMeleeAttackCode(arr,bossSkillTriggered);
	return pArr;
}

void AirshipCaptainSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 3000;	hpTable[1] = 3000;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = AirshipCaptainBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int SpawnFinalBoss(float x, float y)
{
    int unit = CreateObjectById(OBJ_AIRSHIP_CAPTAIN,x,y);

    AirshipCaptainSubProcess(unit);
    // SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x4000);
    SetOwner(GetMaster(), unit);
    CreateObjectById(OBJ_BLUE_RAIN,x,y);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    SetCallback(unit, 5, FinalBossDead);
    return unit;
}

void FinalBossDead()
{
    UniPrintToAll("당신의 승리! 보스가 죽었습니다");
    float x= GetObjectX(SELF),y= GetObjectY(SELF);
    Effect("WHITE_FLASH", x,y, 0.0, 0.0);
    DrawGeometryRing(OBJ_DRAIN_MANA_ORB, x,y); 
    PlaySoundAround(SELF,SOUND_OgreBruteDie);
    PlaySoundAround(SELF,SOUND_GameOver);
    Delete(GetTrigger() + 1);
    Delete(SELF);
}

void FinalBossFx(int unit)
{
    if (CurrentHealth(unit))
    {
        MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
        FrameTimerWithArg(1, unit, FinalBossFx);
    }
}

void RespectFinalBoss()
{
    ObjectOff(SELF);
    int once;

    if (once) return;
    once=TRUE;
    float x=LocationX(183),y=LocationY(183);
    GreenSparkAt(x,y);
    int boss = SpawnFinalBoss(x,y);

    AttachHealthbar(boss);
    PlaySoundAround(boss, SOUND_ManaBombEffect);
    UniPrintToAll("보스가 등장했다!!");
}

int PlaceSoulGate(int wp)
{
    int unit = CreateObject("Maiden", wp);
    
    UnitNoCollide(unit);
    DrawMagicIcon(GetWaypointX(wp), GetWaypointY(wp),OBJ_SOUL_GATE);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    SetDialog(unit, "AA", ClickSoulGate, DummyTrigger);
    return unit;
}

void ClickSoulGate()
{
    int plr = GetPlayerIndex(OTHER);

    if (plr + 1)
    {
        if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        {
            UniPrint(OTHER, "[!!] 마을로 이동했습니다");
            EnchantOff(OTHER, "ENCHANT_AFRAID");
            MoveObject(OTHER, GetWaypointX(23), GetWaypointY(23));
            Effect("RICOCHET", GetWaypointX(23), GetWaypointY(23), 0.0, 0.0);
            AudioEvent("BlindOff", 23);
        }
        else
        {
            Enchant(OTHER, "ENCHANT_AFRAID", 0.7);
            Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            PlaySoundAround(SELF, 1005);
            MoveObject(PlayerRespawnMark(plr), GetObjectX(SELF), GetObjectY(SELF));
            UniPrint(OTHER, "당신의 생명이 이 소울스톤과 연결되었습니다, 죽게되면 이곳에서 부활하게 됩니다");
            UniPrint(OTHER, "한번 더 클릭하면 마을로 이동됩니다");
        }
    }
}

void GoLastTransmission()
{
    if (CurrentHealth(OTHER))
    {
        int pIndex=GetPlayerIndex(OTHER);

        if (pIndex>=0)
        {
            float x= GetObjectX(PlayerRespawnMark(pIndex)),y= GetObjectY(PlayerRespawnMark(pIndex));
            UniPrint(OTHER, "필드로 이동했습니다");
            MoveObject(OTHER, x,y);
            Effect("RICOCHET", x,y, 0.0, 0.0);
            PlaySoundAround(OTHER,SOUND_BlindOff);
        }
    }
}

void DummyTrigger()
{
    return;
}

void InitSoulGatePlaced()
{
    PlaceSoulGate(186);
    PlaceSoulGate(187);
    PlaceSoulGate(188);
    PlaceSoulGate(189);
    PlaceSoulGate(190);
    PlaceSoulGate(191);
    PlaceSoulGate(192);
    PlaceSoulGate(193);
    PlaceSoulGate(194);
    PlaceSoulGate(195);
    PlaceSoulGate(196);
    PlaceSoulGate(197);
    PlaceSoulGate(267);
    // FrameTimer(1, MarketSetting);
    FrameTimerWithArg(3, 11, DefaultItemSpawn);
}

// void MarketSetting()
// {
//     int marketBase = MyNpc("WizardRed", 198);

//     MyNpc("Swordsman", 208);
//     StoryPic(marketBase, "MaleStaffOblivionOrb");
//     StoryPic(marketBase + 1, "SpellbookEmberDemon");
//     SetDialog(marketBase, "YESNO", DescriptionInvincibleItem, ITemGod);
//     SetDialog(marketBase + 1, "YESNO", DescriptionAwardSkill, BuyNewSkill);
//     SetShopkeeperText(Object("warWeaponMarket"), "전사 무기전문점");
//     SetShopkeeperText(Object("warArmorMarket"), "전사 갑옷한벌 장만혀!");
//     SetShopkeeperText(Object("PotionMarket"), "포션좀 보고가세요");
//     SetShopkeeperText(Object("PotionMarket2"), "포션팔이 중년남성");
//     Enchant(Object("PotionMarket2"), "ENCHANT_FREEZE", 0.0);
//     LookWithAngle(marketBase, 192);
//     FrameTimer(3, SetupCreatureShop);

//     int allEnchantMarket = MyNpc("WizardGreen", 260);

//     StoryPic(allEnchantMarket, "SpellbookEmberDemon");
//     SetDialog(allEnchantMarket, "YESNO", DescriptionAllEnchant, TradeAllEnchant);

//     int fonMarket = MyNpc("Mimic", 261);

//     StoryPic(fonMarket, "SpellbookEmberDemon");
//     SetDialog(fonMarket, "YESNO", DescriptionFonAmulet, TradeFonAmulet);
// }

int MyNpc(string name, int wp)
{
    int unit = CreateObject(name, wp);

    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

void DescriptionAwardSkill()
{
    TellStoryUnitName("AwardGuide", "War05A.scr:ShopKeeperTalk2", "스킬구입?");
    UniPrint(OTHER, "새로운 스킬을 구입합니다, 2만원이 요구되며 계속하려면 '예' 를 누르세요");
    UniChatMessage(SELF, "스킬정보: 자신 범위 내 적에게 데미지 100", 120);
}

void BuyNewSkill()
{
    if (GetAnswer(SELF) ^ 1) return;

    int pIndex = GetPlayerIndex(OTHER);
    if (pIndex>=0)
    {
        if (PlayerClassCheckFlag(pIndex,PLAYER_FLAG_WINDBOOST))
        {
            UniPrint(OTHER, "이미 능력을 배운상태 입니다!");
        }
        else
        {
            if (GetGold(OTHER) >= 20000)
            {
                float x=GetObjectX(OTHER),y= GetObjectY(OTHER);
                GreenSparkAt(x,y);
                // DoubleRingFx(CreateObject("InvisibleLightBlueHigh", 26));
                DrawGeometryRing(OBJ_CHARM_ORB, x,y);
                UniPrint(OTHER, "결제가 완료되었습니다");
                ChangeGold(OTHER, -20000);
                PlayerClassSetFlag(pIndex, PLAYER_FLAG_WINDBOOST);
            }
            else
                UniPrint(OTHER, "잔액이 부족합니다");
        }
    }
}

void DescriptionInvincibleItem()
{
    UniPrint(OTHER, "현재 소유중인 아이템 전체의 내구도를 무한으로 만듭니다, 1회 당 5000골드가 필요합니다");
    UniPrint(OTHER, "작업을 계속하려면 '예'를 누르세요");
    TellStoryUnitName("AwardGuide", "War09a:MysticDialog", "장비 파괴불능");
}

void ITemGod()
{
    if (GetAnswer(SELF) ^ 1) return;
    
    if (GetGold(OTHER) >= 5000)
    {
        PlaySoundAround(OTHER,SOUND_TreasurePickup);
        ChangeGold(OTHER, -5000);
        char msg[256];
        int count = NotDestroyAllItems(OTHER);
        NoxSprintfString(msg, "결제가 완료되었습니다, 총 %d 개 아이템이 처리되었습니다", &count, 1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
    else
        UniPrint(OTHER, "금화가 부족합니다!");
}

void DescriptionAllEnchant()
{
    UniPrint(OTHER, "올 엔첸트 능력을 구입하시겠어요? 4만 골드가 필요합니다");
    UniPrint(OTHER, "올 엔첸트 능력: 흡혈, 헤이스트를 포함한 유용한 엔첸트가 항상 지속됩니다");
    TellStoryUnitName("AwardGuide", "War09a:MysticDialog", "올엔첸트 구입");
}

void TradeAllEnchant()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) >= 40000)
    {
        int plr = GetPlayerIndex(OTHER);

        if (plr < 0)
            return;
        if (PlayerClassCheckFlag(plr, PLAYER_FLAG_ALL_BUFF))
            UniPrint(OTHER, "당신은 이미 이 능력을 가졌습니다");
        else
        {
            ChangeGold(OTHER, -40000);
            PlayerSetAllBuff(OTHER);
            PlayerClassSetFlag(plr, PLAYER_FLAG_ALL_BUFF);
            UniPrint(OTHER, "결제가 완료되었습니다");
        }
    }
    else
        UniPrint(OTHER, "잔액이 부족합니다");
}

void DescriptionFonAmulet()
{
    UniPrint(OTHER, "포오네의 목걸이를 구입하시겠어요? 5만 골드가 필요합니다");
    UniPrint(OTHER, "포오네 목걸이: 사용하면 포스 오브 네이쳐(일명 포오네)를 시전합니다. 약간의 쿨다운이 있습니다");
    TellStoryUnitName("AwardGuide", "War09a:MysticDialog", "포오네목걸이 구입?");
}

void TradeFonAmulet()
{
    if (GetAnswer(SELF) ^ 1) return;

    if (GetGold(OTHER) >= 50000)
    {
        ChangeGold(OTHER, -50000);
        PlacingFonAmulet(OTHER);
        UniPrint(OTHER, "결제가 완료되었습니다");
    }
    else
        UniPrint(OTHER, "잔액이 부족합니다");
}

int NotDestroyAllItems(int unit)
{
    int inv = GetLastItem(unit), res = 0;

    while (IsObjectOn(inv))
    {
        if (!HasEnchant(inv, "ENCHANT_INVULNERABLE"))
        {
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
            Nop(res ++);
        }
        inv = GetPreviousItem(inv);
    }
    return res;
}

void DefaultItemSpawn(int lines)
{
    if (lines)
    {
        Frozen(CreateObjectAt("GreatSword", LocationX(199), LocationY(199)), TRUE);
        Frozen(CreateObjectAt("GreatSword", LocationX(200), LocationY(200)), TRUE);
        Frozen(CreateObjectAt("GreatSword", LocationX(201), LocationY(201)), TRUE);
        Frozen(CreateObjectAt("RedPotion", LocationX(202), LocationY(202)), TRUE);
        Frozen(CreateObjectAt("RedPotion", LocationX(203), LocationY(203)), TRUE);
        Frozen(CreateObjectAt("RedPotion", LocationX(204), LocationY(204)), TRUE);
        Frozen(CreateObjectAt("RedPotion", LocationX(205), LocationY(205)), TRUE);
        Frozen(CreateObjectAt("RedPotion", LocationX(206), LocationY(206)), TRUE);
        Frozen(CreateObjectAt("RedPotion", LocationX(207), LocationY(207)), TRUE);
        int u;

        for (u = 0 ; u < 9 ; u ++)
            TeleportLocationVector(199 + u, -23.0, 23.0);
        FrameTimerWithArg(1, lines - 1, DefaultItemSpawn);
    }
}

void SetupCreatureShop()
{
    int ptr = ColorMaiden(250, 16, 225, 213);

    BecomePet(ptr);
    Frozen(ptr, 1);
    MyNpc("WizardRed", 214);
    MyNpc("OgreWarlord", 215); //not_yet_complete_skill
    MoveWaypoint(215, 4221.0, 1389.0);
    MyNpc("HecubahWithOrb", 215);
    LookWithAngle(ptr, 96);
    LookWithAngle(ptr + 1, 32);
    LookWithAngle(ptr + 2, 32);
    LookWithAngle(ptr + 3, 32);
    SetDialog(ptr, "AA", CreShopText, DummyTrigger);
    StoryPic(ptr + 1, "CreatureCageWizard");
    StoryPic(ptr + 2, "CreatureCageOgreWarlord");
    StoryPic(ptr + 3, "HecubahPic");
    SetDialog(ptr + 1, "YESNO", DescriptionRedWiz, CreatRedWiz);
    SetDialog(ptr + 2, "YESNO", DescriptionOgreLord, CreatOgreLord);
    SetDialog(ptr + 3, "YESNO", DescriptionHecubah, CreatHecOrb);
    PlayerCreList();
}

void CreShopText()
{
    UniChatMessage(SELF, "어서오세요, 여기는 크리쳐 샾 입니다!\n구입하려는 크리쳐에게 다가가 더블클릭을 해보세요~!", 150);
    UniPrint(OTHER, "크리처 관련 팁- [웃기 L] 버튼을 누르면 자신의 위치로 크리처를 순간이동 시킵니다");
    MoveWaypoint(25, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("Maiden1Talkable", 25);
}

int PlayerCreList()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("AmbBeachBirds", 213) + 1;
        Delete(ptr - 1);
        for (k = 9 ; k >= 0 ; k --)
            CreateObject("InvisibleLightBlueHigh", 213);
    }
    return ptr;
}

void RegistPlayerCreature(int plr, int unit)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)), pic = PlayerCreList() + plr;

    SetListPrev(ptr, pic);
    if (IsObjectOn(GetListNext(pic)))
    {
        SetListNext(ptr, GetListNext(pic));
        SetListPrev(GetListNext(pic), ptr);
    }
    SetListNext(pic, ptr);
    SetCallback(unit, 5, PlayerCreatureDeath);
    FrameTimerWithArg(1, unit, DelayCreatFollow);
}

void PlayerCreatureDeath()
{
    int ptr = GetTrigger() + 1;

    DeleteObjectTimer(SELF, 92);
    SetListNext(GetListPrev(ptr), GetListNext(ptr));
    SetListPrev(GetListNext(ptr), GetListPrev(ptr));
    Delete(ptr);
}

void RemoveAllCreature(int plr)
{
    int cur = ToInt(GetObjectZ(PlayerCreList() + plr)), temp;

    while (IsObjectOn(cur))
    {
        temp = cur;
        cur = GetListNext(cur);
        Delete(temp - 1);
        Delete(temp);
    }
}

void DescriptionRedWiz()
{
    UniPrint(OTHER, "붉은 마법사를 소환합니다, 이 작업은 2만골드가 필요하며 계속하려면 '예'를 누르세요");
    TellStoryUnitName("AwardGuide", "Con09a:NecroThreat1", "레드위저드 소환");
}

void CreatRedWiz()
{
    int unit, plr = GetPlayerIndex(OTHER);

    if (GetAnswer(SELF) ^ 1) return;
    if (plr + 1)
    {
        if (GetGold(OTHER) >= 20000)
        {
            MoveWaypoint(25, GetObjectX(OTHER), GetObjectY(OTHER));
            unit = CreateObject("WizardRed", 25);
            RegistPlayerCreature(plr, unit);
            SetUnitMaxHealth(unit, 750);
            GiveCreatureToPlayer(OTHER, unit);
            RetreatLevel(unit, 0.0);
            SetCallback(unit, 3, RWizWeapon);
            SetOwner(OTHER, unit);
            ChangeGold(OTHER, -20000);
        }
        else
            UniPrint(OTHER, "잔액이 부족합니다");
    }
}

void DescriptionOgreLord()
{
    UniPrint(OTHER, "오우거로드를 소환합니다, 이 작업은 금화 20,000 을 요구합니다");
    UniPrint(OTHER, "계속하시려면 '예' 버튼을 누르십시오");
    TellStoryUnitName("AwardGuide", "creature_desc:OgreWarlord", "용병:\n오우거로드");
}

void CreatOgreLord()
{
    int unit, plr = GetPlayerIndex(OTHER);

    if (GetAnswer(SELF) ^ 1) return;
    if (plr + 1)
    {
        if (GetGold(OTHER) >= 20000)
        {
            unit = CreateObjectAt("OgreWarlord", GetObjectX(OTHER), GetObjectY(OTHER));
            RegistPlayerCreature(plr, unit);
            SetUnitMaxHealth(unit, 830);
            GiveCreatureToPlayer(OTHER, unit);
            RetreatLevel(unit, 0.0);
            SetCallback(unit, 3, OgreLordSightEvent);
            SetOwner(OTHER, unit);
            ChangeGold(OTHER, -20000);
        }
        else
            UniPrint(OTHER, "잔액이 부족합니다");
    }
}

void DescriptionHecubah()
{
    UniPrint(OTHER, "헤쿠바 위스오브 유닛을 소환합니다, 이 작업은 금화 25,000 을 요구합니다");
    UniPrint(OTHER, "계속하시려면 '예' 버튼을 누르십시오");
    TellStoryUnitName("AwardGuide", "Con10B.scr:HecubahLine13", "용병\n헤쿠바");
}

void CreatHecOrb()
{
    if (GetAnswer(SELF) ^ 1) return;
    int plr = GetPlayerIndex(OTHER);
    if (plr + 1)
    {
        if (GetGold(OTHER) >= 25000)
        {
            int unit = CreateObjectAt("HecubahWithOrb", GetObjectX(OTHER), GetObjectY(OTHER));
            RegistPlayerCreature(plr, unit);
            SetUnitMaxHealth(unit, 700);
            GiveCreatureToPlayer(OTHER, unit);
            RetreatLevel(unit, 0.0);
            SetCallback(unit, 3, HecubahEnemyDetect);
            SetOwner(OTHER, unit);
            ChangeGold(OTHER, -25000);
        }
        else
            UniPrint(OTHER, "잔액이 부족합니다");
    }
}

void RWizWeapon()
{
    if (CurrentHealth(SELF))
    {
        if (CurrentHealth(OTHER))
        {
            Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, SELF, 35, 16);
        }
        if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
        {
            Enchant(SELF, "ENCHANT_VILLAIN", 0.0);
            FrameTimerWithArg(22, GetTrigger(), RWizResetSight);
        }
    }
}

void RWizResetSight(int unit)
{
    EnchantOff(unit, "ENCHANT_VILLAIN");
    Enchant(unit, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(unit, 1.0);
}

void DelayCreatFollow(int unit)
{
    CreatureFollow(unit, GetOwner(unit));
    AggressionLevel(unit, 1.0);
}

void MidBoss(int wp)
{
    int unit = CreateObject("LichLord", wp);

    UnitLinkBinScript(unit, LichLordBinTable());
    SetUnitMaxHealth(unit, 750);
    SetCallback(unit, 3, MidBossRandomSkill);
    SetCallback(unit, 5, MidBossDeath);
    AggressionLevel(unit, 1.0);
    SetUnitScanRange(unit, 400.0);
    SetUnitVoice(unit, 32);
    UnitZeroFleeRange(unit);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
}

void MidBossDeath()
{
    DeleteObjectTimer(SELF, 30);
}

void MidBossRandomSkill()
{
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        int sub = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        int fn[]={ShootFireBall, ChainBolt, TurnUndead, DelayDeathRayTarget, FallenFireBoom, ShootCrystalBall, };
        SetOwner(SELF, sub);
        Raise(sub, ToFloat(GetCaller()));
        CallFunctionWithArg(fn[ Random(0, 5) ], sub);
    }
    CheckResetSight(GetTrigger(), 39);
}

void ShootFireBall(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr));

    CastSpellObjectObject("SPELL_FIREBALL", owner, tg);
    CastSpellObjectObject("SPELL_PIXIE_SWARM", owner, owner);
    Enchant(owner, "ENCHANT_SHIELD", 0.0);
    Enchant(owner, "ENCHANT_ETHEREAL", 1.0);
    Delete(ptr);
}

void ChainBolt(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr));

    PauseObject(owner, 30);
    CastSpellObjectObject("SPELL_LIGHTNING", owner, tg);
    Enchant(owner, "ENCHANT_SHOCK", 0.0);
    Enchant(owner, "ENCHANT_ETHEREAL", 2.0);
    Delete(ptr);
}

void onTurnUndeadSkillSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 100, DAMAGE_TYPE_PLASMA);
        }
    }
}

void TurnUndead(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr));

    CastSpellObjectObject("SPELL_TURN_UNDEAD", owner, owner);
    SplashDamageAtEx(owner, GetObjectX(owner), GetObjectY(owner), 150.0, onTurnUndeadSkillSplash);
    Enchant(owner, "ENCHANT_ETHEREAL", 2.0);
    Delete(ptr);
}

void DelayDeathRayTarget(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr));
    int loc = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(tg), GetObjectY(tg));

    SetOwner(owner, loc);
    Enchant(loc, "ENCHANT_ANCHORED", 0.0);
    DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", GetObjectX(loc), GetObjectY(loc)), 18);
    DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", GetObjectX(loc), GetObjectY(loc)), 18);
    DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", GetObjectX(loc), GetObjectY(loc)), 18);
    DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", GetObjectX(loc), GetObjectY(loc)), 18);
    DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", GetObjectX(loc), GetObjectY(loc)), 18);
    Enchant(owner, "ENCHANT_INFRAVISION", 5.0);
    Enchant(owner, "ENCHANT_ETHEREAL", 3.0);
    Raise(loc, ToFloat(ptr));
    FrameTimerWithArg(12, loc, TakeShotDeathRay);
}

void FallenFireBoom(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr));
    int unit = CreateObjectAt("FireSprite", GetObjectX(tg), GetObjectY(tg));

    UnitLinkBinScript(unit, FireSpriteBinTable());
    UnitNoCollide(unit);
    SetOwner(owner, unit);
    Raise(unit, 200.0);
    Damage(unit, 0, MaxHealth(unit) + 1, 14);
    PlaySoundAround(unit, 24);
    Enchant(owner, "ENCHANT_SHIELD", 0.0);
    Enchant(owner, "ENCHANT_ETHEREAL", 3.0);
    Delete(ptr);
}

void ShootCrystalBall(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr));

    int unit = CreateObjectAt("GameBall", GetObjectX(owner) + UnitRatioX(tg, owner, 13.0), GetObjectY(owner) + UnitRatioY(tg, owner, 13.0));
    UnitNoCollide(unit);
    SetOwner(owner, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit)));
    Raise(unit + 1, ToFloat(tg));
    Enchant(owner, "ENCHANT_ETHEREAL", 3.0);
    FrameTimerWithArg(1, unit + 1, MovingCristalBall);
    Delete(ptr);
}

void MovingCristalBall(int ptr)
{
    int owner = GetOwner(ptr), tg = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(tg) && count < 30)
    {
        if (Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(tg), GetObjectY(tg)) < 48.0)
        {
            WispDestroyFX(GetObjectX(tg), GetObjectY(tg));
            Damage(tg, owner, 85, 14);
            LookWithAngle(ptr, 200);
        }
        else
        {
            MoveObject(ptr, GetObjectX(ptr) + UnitRatioX(tg, ptr, 38.0), GetObjectY(ptr) + UnitRatioY(tg, ptr, 38.0));
            MoveObject(ptr - 1, GetObjectX(ptr), GetObjectY(ptr));
            LookWithAngle(ptr, ++count);
        }
        FrameTimerWithArg(1, ptr, MovingCristalBall);
    }
    else
    {
        Delete(ptr);
        Delete(ptr - 1);
    }
}

void TakeShotDeathRay(int ptr)
{
    int owner = GetOwner(ptr), org = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner))
    {
        CastSpellObjectLocation("SPELL_DEATH_RAY", org, GetObjectX(ptr), GetObjectY(ptr));
        CastSpellObjectLocation("SPELL_DEATH_RAY", owner, GetObjectX(ptr), GetObjectY(ptr));
    }
    Delete(ptr);
    Delete(org);
}

void JustDecorations()
{
    Frozen(CreateObject("DunMirScaleTorch2", 226), 1);
    RiseBlueSpark(3807.0, 1528.0);
}

void FonAmuletDecreaseCooldown(int amulet)
{
    if (IsObjectOn(amulet) && IsObjectOn(amulet + 1))
    {
        if (GetDirection(amulet + 1))
        {
            LookWithAngle(amulet + 1, GetDirection(amulet + 1) - 1);
            FrameTimerWithArg(2, amulet, FonAmuletDecreaseCooldown);
        }
    }
    else
    {
        Delete(amulet);
        Delete(amulet + 1);
    }
}

void ItemClassUseFonAmulet()
{
    int subUnit = GetTrigger() + 1;

    if (IsObjectOn(subUnit))
    {
        char msg[128];
        int cool=GetDirection(subUnit);
        if (!GetDirection(subUnit))
        {
            FrameTimerWithArg(1, GetTrigger(), FonAmuletDecreaseCooldown);
            LookWithAngle(subUnit, 90);
            RestoreHealth(OTHER, 100);
            float x=GetObjectX(OTHER),y= GetObjectY(OTHER);
            DrawGeometryRing(OBJ_WHITE_ORB , x,y);
            CastSpellObjectObject("SPELL_FORCE_OF_NATURE", OTHER, OTHER);
        }
        else
        {
            NoxSprintfString(msg,"쿨다운 중입니다... %d", &cool, 1);
            UniPrint(OTHER, ReadStringAddressEx(msg));
        }
        MoveObject(subUnit, GetObjectX(SELF), GetObjectY(SELF));
    }
}

int PlacingFonAmulet(int posUnit)
{
    int amulet = CreateObjectAt("AmuletOfClarity", GetObjectX(posUnit), GetObjectY(posUnit));

    CreateObjectAt("ImaginaryCaster", GetObjectX(amulet), GetObjectY(amulet));
    SetUnitCallbackOnUseItem(amulet,ItemClassUseFonAmulet);
    return amulet;
}

void OgreLordSightEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_VILLAIN"))
    {
        Enchant(SELF, "ENCHANT_VILLAIN", 1.2);
        LookAtObject(SELF, OTHER);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        AutoTrackingMissile(GetTrigger());
    }
    CheckResetSight(GetTrigger(), 20);
}

void AutoTrackingMissile(int owner)
{
    int ptr;

    if (CurrentHealth(owner))
    {
        ptr = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner) + UnitAngleCos(owner, 28.0), GetObjectY(owner) + UnitAngleSin(owner, 28.0));
        CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner));
        LookWithAngle(ptr, GetDirection(owner));
        SetOwner(owner, ptr);
        FrameTimerWithArg(1, ptr, FlyingBullet);
    }
}

void FlyingBullet(int ptr)
{
    int owner = GetOwner(ptr), count = ToInt(GetObjectZ(ptr)), target = ToInt(GetObjectZ(ptr + 1));

    if (CurrentHealth(owner) && count < 100)
    {
        if (CurrentHealth(target))
        {
            if (Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(target), GetObjectY(target)) < 41.0)
            {
                MoveWaypoint(26, GetObjectX(ptr), GetObjectY(ptr));
                AudioEvent("HammerMissing", 26);
                GreenExplosion(GetObjectX(ptr), GetObjectY(ptr));
                Damage(target, owner, 75, 14);
                count = 200;
            }
            else
            {
                LookAtObject(ptr, target);
                Effect("RICOCHET", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
                MoveObject(ptr, GetObjectX(ptr) + UnitRatioX(target, ptr, 19.0), GetObjectY(ptr) + UnitRatioY(target, ptr, 19.0));
            }
        }
        else
        {
            MoveObject(ptr, GetObjectX(ptr) + UnitAngleCos(ptr, 19.0), GetObjectY(ptr) + UnitAngleSin(ptr, 19.0));
            DetectAroundEnemy(ptr);
        }
        LookWithAngle(HarpoonFxAtLocation(ptr, 9), GetDirection(ptr));
        if (IsVisibleTo(ptr, ptr + 1))
            Raise(ptr, ToFloat(count + 1));
        else
            Raise(ptr, ToFloat(200));
        FrameTimerWithArg(1, ptr, FlyingBullet);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

int HarpoonFxAtLocation(int unit, int lftime)
{
    int unit2 = CreateObjectAt("HarpoonBolt", GetObjectX(unit), GetObjectY(unit));

    Frozen(unit2, 1);
    DeleteObjectTimer(unit2, lftime);
    return unit2;
}

void DetectAroundEnemy(int ptr)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr), GetObjectY(ptr)) + 1;

    Raise(unit - 1, 5000.0);
    SetOwner(ptr, unit - 1);

    int k;
    for (k = 0 ; k < 8 ; Nop(++k))
    {
        UnitNoCollide(CreateObjectAt("WeirdlingBeast", GetObjectX(ptr), GetObjectY(ptr)));
        SetOwner(unit - 1, unit + k);
        LookWithAngle(unit + k, 32 * k);
        Enchant(unit + k, "ENCHANT_INFRAVISION", 0.0);
        SetCallback(unit + k, 3, UnitDetectEvent);
        DeleteObjectTimer(unit + k, 1);
    }
    DeleteObjectTimer(unit - 1, 1);
}

void UnitDetectEvent()
{
    int ptr = GetOwner(GetOwner(SELF)), unit = GetOwner(SELF);

    if (Distance(GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(SELF), GetObjectY(SELF)) < GetObjectZ(unit))
    {
        Raise(unit, Distance(GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(SELF), GetObjectY(SELF)));
        Raise(ptr + 1, ToFloat(GetCaller()));
    }
}

void HecubahEnemyDetect()
{
    if (!HasEnchant(SELF, "ENCHANT_SHIELD"))
    {
        Enchant(SELF, "ENCHANT_SHIELD", 0.0);
        CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
    }
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
    CheckResetSight(GetTrigger(), 33);
}

void ExecClientPart()
{
    InitializeResources();
    InitializePopupMessages();
    ClientProcLoop();
}

void OnPlayerEnteredMap(int pInfo){
    int *ptr=GetMemory(pInfo+0x808),pUnit = 0;

    if (ptr)
    {
        pUnit = GetMemory(ptr+0x2c);
        int pIndex = GetMemory(pInfo+0x810);
        PlayerClassOnInit   (pIndex, pUnit);
        ShowQuestIntroOne(pUnit, 237, "CopyrightScreen", "Wolchat.c:PleaseWait");
    }
    
    if (pInfo==0x653a7c)
    {
        InitializeResources();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

void TestYou(){
    int user = GetPlayerUnit(GetPlayerIndex(OTHER));

    Print(IntToString(user));
}

void onKillerianDeath(){
    PushTimerQueue(9, CreateObjectById(OBJ_EXPLOSION,GetObjectX(SELF),GetObjectY(SELF)), CreateRandomItemCommon);
}

void createKillerian(short locationId){
    int k=CreateObjectById(OBJ_DEMON,LocationX(locationId),LocationY(locationId));

    SetUnitMaxHealth(k, 800);
    SetCallback(k,5,onKillerianDeath);
}

void startFireHell(){
    int once;
    ObjectOff(SELF);
    if (once) return;
    once=TRUE;
    short locs[]={273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,};
    int c=sizeof(locs);

    while (--c>=0)
        createKillerian(locs[c]);
}
