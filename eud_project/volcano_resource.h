
#include "libs/define.h"
#include "libs/imageutil.h"
#include "libs/clientside.h"

void ImageResourceMrDongseok()
{ }

void ImagePrettyGirl()
{ }

void ImagePrettyGirlRightFace()
{ }

void ImagePrettyGirlLeftFace()
{ }

void ImageYoureWinner()
{ }

void MapBgmResource()
{ }

void InitializeCommonResource(int pImgVector)
{
    int index = 0;

    AddImageFromResource(pImgVector, GetScrCodeField(ImagePrettyGirl) + 4, 112660, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImagePrettyGirlRightFace) + 4, 133333, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageYoureWinner) + 4, 133707, index++);
    AddImageFromResource(pImgVector, GetScrCodeField(ImageResourceMrDongseok) + 4, 132384, index++);
    short wMessage1[128];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("v탄막 슈팅타워 부수기v - 제작. panic"), wMessage1);
    AddStringSprite(pImgVector, 0xf800, wMessage1, 112993,index++);
    AddStringSprite(pImgVector, 0x015F, wMessage1, 112994,index++);
    AddStringSprite(pImgVector, 0x07E0, wMessage1, 112995,index++);
    AddStringSprite(pImgVector, 0xFEE0, wMessage1, 112996,index++);

    short wMessage2[64];
    short wMessage3[64];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("당신이 승리했습니다!!"), wMessage2);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("축하합니다!!"), wMessage3);
    AddStringSprite(pImgVector, 0xf800, wMessage3, 112988,index++);
    AddStringSprite(pImgVector, 0x015F, wMessage3, 112989,index++);
    AddStringSprite(pImgVector, 0x07E0, wMessage2, 112990,index++);
    AddStringSprite(pImgVector, 0xFEE0, wMessage2, 112991,index++);
    ExtractMapBgm("..\\bgm333.mp3", MapBgmResource);
}

