
#include "fov_gvar.h"
#include "libs/gui_window.h"
#include "libs/format.h"

void QueryDialogCtx(int *get, int *set){
    int ctx;
    if (get)get[0]=ctx;
    if (set)ctx=set[0];
}

void initDialog()
{
    int *wndptr=0x6e6a58,ctx;
    GUIFindChild(wndptr[0], 3901, &ctx);
    QueryDialogCtx(NULLPTR, &ctx);
}

void InitFillPopupMessageArray(int *pPopupMessage)
{
    pPopupMessage[GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM]="당신의 인벤토리를 무적화 하시겠어요? 필요한 금액은 7,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_ARROW_AXE]="트리플 화살 손도끼를 구입하시겠어요? 필요한 금액은 20,000골드 입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_THUNDER_SWORD]="피카추의 백만볼트 서드를 구입하려면 예를 누르세요, 검을 휘두르면 전방에 백만볼트를 발사합니다,  25,000골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_SKILL_CRITICAL_HIT]="조심스럽게 걷기 강화를 하실래요? 주변 범위 적에게 일정량의 피해를 줄수 있게됩니다, 15,000골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_MONEY_EXCHANGER]="당신이 가진 여러 보석들을 모두 금화로 환전해 드립니다, 예를 누르시면 보석을 금화로 환전해드릴게요";
    pPopupMessage[GUI_DIALOG_MESSAGE_CREATURE_ARMY]="육군병사 1명을 구입하시겠습니까? 당신을 따라다니면서 도와줄 것입니다,  40,000골드";
    pPopupMessage[GUI_DIALOG_MESSAGE_CREATURE_HOSUNGLEE]="이호성을 구입하시겠습니까?, 가격은 60,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_CREATURE_RUNNER_ONI]="러너오니를 구입하시겠습니까?, 가격은 40,000골드입니다";
    pPopupMessage[GUI_DIALOG_MESSAGE_WEAPON_FON_AXE]="포스오브네이쳐 엑스를 구입하실래요?, 가격은 45,000골드입니다";
    // pPopupMessage[GUI_DIALOG_MESSAGE_BEAR_GRYLLS]="베어그릴스- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 61,000골드입니다";
    // pPopupMessage[GUI_DIALOG_MESSAGE_PESTICIDE]="살충제- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 60,000골드입니다";
    // pPopupMessage[GUI_DIALOG_MESSAGE_BLUE_GIANT]="파랑거인- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 66,000골드입니다";
    // pPopupMessage[GUI_DIALOG_MESSAGE_STELS_SHIP]="스텔스십- 당신을 계속 따라다니며 당신을 돕는다, 유저 마다 오직1개 씩 소유할 수 있다, 가격은 77,000골드입니다";
    // pPopupMessage[GUI_DIALOG_MESSAGE_RANDOM_SPEED_ARMOR]="랜덤으로 이동속도 속성의 갑옷을 드립니다, 가격은 회차 당 17,000골드입니다";
    // pPopupMessage[GUI_DIALOG_MESSAGE_AWARD_WARCRY]="전사의 함성을 활성화 하려면 가격은 20,000골드입니다";
}

void initPopupMsg(int *pPopupMessage)
{
    initDialog();
    InitFillPopupMessageArray(pPopupMessage);
}

void queryPopMessage(int **get){
    int msg[32];

    if (get) get[0]=msg;
}

void InitializePopupMessages(){
    int once;
    if (once) return;
    once=TRUE;
    int *s;
    queryPopMessage(&s);
    initPopupMsg(s);
}

