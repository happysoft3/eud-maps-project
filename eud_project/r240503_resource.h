
#include "r240503_gvar.h"
#include "libs/grplib.h"
#include "libs/animFrame.h"

void GRPDump_GobdungSmallOutput(){}

#define SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT 120669

void initializeImageFrameSmallGopdung(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_SPIDER;
    // int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GobdungSmallOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions( SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( SMALL_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
}

void GRPDump_GobdungMediumOutput(){}

#define MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT 121039

void initializeImageFrameMediumGopdung(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_ALBINO_SPIDER;
    int xyInc[]={0,-2};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GobdungMediumOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MEDIUM_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
}

void GRPDump_GobdungBigOutput(){}

#define LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT 120459

void initializeImageFrameBigGopdung(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BLACK_WIDOW;
    // int xyInc[]={0,-2};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GobdungBigOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
}

void GRPDump_Replenisher(){}

#define REPLENISHER_IMAGE_START_AT 120299

void initializeImageReplenisher(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SPITTING_SPIDER;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_Replenisher)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(   REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions(   REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(   REPLENISHER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
}

void GRPDump_Button(){}
#define BUTTON_IMAGE_START_AT 112382
void initializeImageButton(int imgVector){
    int *frames, *sizes;
    UnpackAllFromGrp(GetScrCodeField(GRPDump_Button)+4, OBJ_CHAIN_PULL_1,NULLPTR,&frames,&sizes);
    AppendImageFrame(imgVector,frames[0],BUTTON_IMAGE_START_AT);
    AppendImageFrame(imgVector,frames[1],BUTTON_IMAGE_START_AT+1);
}
void initializeTextStrings(int imgVector){
    short message1[128];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("!클릭!<<필드로 돌아가기. Return to field"),message1);
    AppendTextSprite(imgVector,0x17E0,message1,112984);
    AppendTextSprite(imgVector,0x077F,message1,112985);
    AppendTextSprite(imgVector,0xF815,message1,112986);

    short message2[64];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("**YOU'RE WINNER!**^,^"),message2);
    AppendTextSprite(imgVector,0xF808,message2,112979);
    AppendTextSprite(imgVector,0xEFE0,message2,112980   );
    AppendTextSprite(imgVector,0x07FA,message2,112981   );
    AppendTextSprite(imgVector,0xCCBC,message2,112982   );
    AppendTextSprite(imgVector,0x37C5,message2,112983   );

    short message3[64];

    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("!!임무실패!!- 패배했습니다"), message3);
    AppendTextSprite(imgVector, 0xEFE0,message3,112997);
}
void GRPDump_VendingMachine(){}
void initializeVendingMachine(int imgVec){
    int *frames,*sizes;
    UnpackAllFromGrp(GetScrCodeField(GRPDump_VendingMachine)+4,OBJ_URCHIN_SHAMAN,NULLPTR,&frames,&sizes);
    AppendImageFrame(imgVec,frames[0],117757);
    AppendImageFrame(imgVec,frames[1],117769);
}
void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void GRPDump_MechanicalOgreOutput(){}

#define MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID 123388
void initializeImageFrameMechanicalOgre(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BLACK_BEAR;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MechanicalOgreOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,12,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,12,1,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(5,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(10,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(15,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(20,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(25,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(30,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(35,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameAssign8Directions(40,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_WALK, 8);
    AnimFrameAssign8Directions(45,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_WALK, 9);
    AnimFrameAssign8Directions(50,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_WALK, 10);
    AnimFrameAssign8Directions(55,  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+88, IMAGE_SPRITE_MON_ACTION_WALK, 11);

    AnimFrameCopy8Directions( MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions( MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_RUN, 8);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_RUN, 9);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+80, IMAGE_SPRITE_MON_ACTION_RUN, 10);
    AnimFrameCopy8Directions(  MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+88, IMAGE_SPRITE_MON_ACTION_RUN, 11);

    AnimFrameCopy8Directions( MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions( MECHANICALOGRE_OUTPUT_BASE_IMAGE_ID+72, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_BugWeaponOutput(){}

#define BUG_WEAPON_IMAGE_AT 133943

void initializeImageFrameBugWeapons(int imgVector, int *frames, int sizes){
    int thingId=OBJ_WOUNDED_APPRENTICE;

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssignAsSingleImage(BUG_WEAPON_IMAGE_AT,frames[0],IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameCopyAsSingleImage(BUG_WEAPON_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopyAsSingleImage(BUG_WEAPON_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopyAsSingleImage(BUG_WEAPON_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}
#define BEAR_GLYLLS_IMAGE_AT 133944
void initializeImageFrameBearGlylls(int imgVector, int *frames, int sizes){
    int thingId=OBJ_WOUNDED_CONJURER;

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssignAsSingleImage(BEAR_GLYLLS_IMAGE_AT,frames[2],IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameCopyAsSingleImage(BEAR_GLYLLS_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopyAsSingleImage(BEAR_GLYLLS_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopyAsSingleImage(BEAR_GLYLLS_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}
#define PESTICIDE_IMAGE_AT 133945
void initializeImageFramePesticide(int imgVector, int *frames, int sizes){
    int thingId=OBJ_WOUNDED_WARRIOR;

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssignAsSingleImage(PESTICIDE_IMAGE_AT,frames[1],IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameCopyAsSingleImage(PESTICIDE_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopyAsSingleImage(PESTICIDE_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopyAsSingleImage(PESTICIDE_IMAGE_AT,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_GreenAmulet(){}
void initializeImageGreenAmulet(int imgVector){
    int *frames, *sizes, thingId=OBJ_AMULETOF_NATURE;
    int xyInc[]={0,13};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GreenAmulet)+4, thingId, xyInc, &frames,&sizes);

    AppendImageFrame(imgVector, frames[0],112960);
}

void initializeImageBugWeaponGeneric(int imgVector){
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BugWeaponOutput)+4, thingId, xyInc, &frames,&sizes);

    initializeImageFrameBugWeapons(imgVector,frames,sizes);
    initializeImageFrameBearGlylls(imgVector,frames,sizes);
    initializeImageFramePesticide(imgVector,frames,sizes);
    AppendImageFrame(imgVector, frames[3],132289);
    // AppendImageFrame(imgVector, frames[4],112960);
}

void GRPDump_BlueAooniOutput(){}

#define BLUE_ONI_BASE_IMAGE_ID 127714
void initializeImageFrameBlueAooni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BlueAooniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign4Directions(0, BLUE_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BLUE_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BLUE_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BLUE_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_ParalysisProbe(){}

#define PARALYSISPROBE_IMAGE_START_AT 120829

void initializeImageParalysisProbe(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_ALBINO_SPIDER;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_ParalysisProbe)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    PARALYSISPROBE_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(PARALYSISPROBE_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy8Directions(PARALYSISPROBE_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(PARALYSISPROBE_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_Supplier(){}

#define SUPPLIER_IMAGE_START_AT 124381

void initializeImageSupplier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOLF;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_Supplier)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    SUPPLIER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(   SUPPLIER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameCopy8Directions(   SUPPLIER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(   SUPPLIER_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
}

void GRPDump_StelsOutput(){}

#define STELS_OUTPUT_BASE_IMAGE_ID 113935
void initializeImageFrameStelsShip(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_EVIL_CHERUB;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_StelsOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK,3,2,2);
    AnimFrameAssign8Directions(0,  STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  STELS_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10,  STELS_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 0);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 1);
    AnimFrameCopy8Directions(       STELS_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_FAR_ATTACK, 2);
}

void ImageTitleCard(){}

void InitializeResource(){
    int vec=CreateImageVector(IMAGE_VECTOR_MAX_COUNT);

    initializeImageFrameMediumGopdung(vec);
    initializeImageFrameSmallGopdung(vec);
    initializeImageFrameBigGopdung(vec);
    initializeImageReplenisher(vec);
    initializeImageButton(vec);
    initializeTextStrings(vec);
    initializeVendingMachine(vec);
    initializeImageHealthBar(vec);
    initializeImageFrameMechanicalOgre(vec);
    initializeImageBugWeaponGeneric(vec);
    initializeImageFrameBlueAooni(vec);
    initializeImageParalysisProbe(vec);
    initializeImageSupplier(vec);
    initializeImageFrameStelsShip(vec);
    initializeImageGreenAmulet(vec);
    AppendImageFrame(vec,GetScrCodeField(ImageTitleCard)+4,14614);
    DoImageDataExchange(vec);
}

