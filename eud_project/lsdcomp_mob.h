
#include "lsdcomp_gvar.h"
#include "lsdcomp_utils.h"
#include "lsdcomp_sub.h"
#include "libs/unitstruct.h"
#include "libs/playerinfo.h"
#include "libs/objectIDdefines.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"

void FieldMonsterKillHandler(int kill, int ptr)
{
    if (CurrentHealth(kill) && IsPlayerUnit(kill))
    {
        int pIndex = GetPlayerIndex(kill);

        if (pIndex<0)
            return;

        OnCreatureKill(pIndex, ToInt(GetObjectZ(ptr + 1)));
    }
}

void FieldMonsterDeath()
{
    OnFieldMobDeath(SELF);
    FieldMonsterKillHandler(GetTopParentUnit(GetKillCredit()), GetTrigger());
    DeleteObjectTimer(SELF, 30);
    Delete(GetTrigger() + 1);
}

void spreadVictoryTropy(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int dur=GetDirection(sub);
        if(dur)
        {
            PushTimerQueue(1,sub,spreadVictoryTropy);
            LookWithAngle(sub,dur-1);
            CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT, GetObjectX(sub),GetObjectY(sub));
            return;
        }
        GreenSparkFx(GetObjectX(sub),GetObjectY(sub));
        Delete(sub);
    }
}

void GolemDeathHandler()
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(SELF),GetObjectY(SELF));

    LookWithAngle(sub, 32);
    PushTimerQueue(1, sub, spreadVictoryTropy);
    FieldMonsterDeath();
    PlaySoundAround(sub, SOUND_FlagCapture);
}

void ZombieDeathHandler()
{
    float x= GetObjectX(SELF),y= GetObjectY(SELF);
    FieldMonsterDeath();
    if (MaxHealth(SELF))
        Damage(SELF, 0, 100, 14);
    DeleteObjectTimer(CreateObjectAt("MediumFlame", x,y), 150);
}

void MaidenDetectEnemy()
{
	MoveWaypoint(1, GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 17.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 17.0));
	MoveObject(GetTrigger() + 1, GetObjectX(SELF), GetObjectY(SELF));
	int mis = CreateObject("ImpShot", 1);
	SetOwner(SELF, mis);
	LookAtObject(mis, OTHER);
	PushObject(mis, 21.0, GetObjectX(SELF), GetObjectY(SELF));
    SetOwner(GetCaller(), GetTrigger() + 1);
	Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

void MaidenLostEnemy()
{
	int enemy = GetOwner(GetTrigger() + 1);

	EnchantOff(SELF, "ENCHANT_BLINDED");
	if (CurrentHealth(enemy) && IsVisibleTo(SELF, enemy))
	{
		LookAtObject(SELF, enemy);
		Attack(SELF, enemy);
	}
}

void DisplayHealthInfo(int unit)
{
    MonsterReportHitpoints(unit, CurrentHealth(unit), MaxHealth(unit), 60);
}

void PlayerCreatureRisk()
{
    if (HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        return;
    else
    {
        DisplayHealthInfo(GetTrigger());
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.1);
    }
}

void CommonFieldMobProperty(int unit)
{
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    AggressionLevel(unit, 1.0);
    SetCallback(unit, 5, FieldMonsterDeath);
    SetUnitScanRange(unit, 500.0);
}

int FieldMonsterCreate(string unitName, int expAmount, int location, int hitPoint)
{
    int unit = CreateObject(unitName, location);

    CheckMonsterThing(unit);
    SetUnitMaxHealth(unit, hitPoint);
    Raise(CreateObject("InvisibleLightBlueLow", location), expAmount);
    CommonFieldMobProperty(unit);

    return unit;
}

int FieldMobVileZombie(int location)
{
    int unit = CreateObject("VileZombie", location);

    SetUnitMaxHealth(unit, 325);
    SetUnitSpeed(unit, 2.8);
    Raise(CreateObject("InvisibleLightBlueLow", location), 4);
    CommonFieldMobProperty(unit);
    SetCallback(unit, 5, ZombieDeathHandler);

    return unit;
}

int FieldMobNormalZombie(int location)
{
    int unit = CreateObject("Zombie", location);

    SetUnitMaxHealth(unit, 230);
    Raise(CreateObject("InvisibleLightBlueLow", location), 2);
    CommonFieldMobProperty(unit);
    SetCallback(unit, 5, ZombieDeathHandler);

    return unit;
}

int FieldMobGhost(int location)
{
    int unit = CreateObject("Ghost", location);

    SetUnitMaxHealth(unit, 70);
    Raise(CreateObject("InvisibleLightBlueLow", location), 2);
    CommonFieldMobProperty(unit);
    Enchant(unit, "ENCHANT_HASTED", 0.0);
    Enchant(unit, "ENCHANT_RUN", 0.0);

    return unit;
}

// int FieldMobMaiden(int location)
// {
//     int unit = CreateObject("Bear2", location);

//     UnitLinkBinScript(unit, MaidenBinTable());
//     SetUnitVoice(unit, 8);
//     SetUnitMaxHealth(unit, 295);
//     Raise(CreateObject("InvisibleLightBlueLow", location), 4);
//     CommonFieldMobProperty(unit);
//     SetUnitScanRange(unit, 400.0);
//     SetCallback(unit, 3, MaidenDetectEnemy);
//     SetCallback(unit, 13, MaidenLostEnemy);

//     return unit;
// }

int FieldMobFireFairy(int location)
{
    int unit = CreateObject("FireSprite", location);

    UnitLinkBinScript(unit, FireSpriteBinTable());
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    SetUnitMaxHealth(unit, 98);
    Raise(CreateObject("InvisibleLightBlueLow", location), 4);
    CommonFieldMobProperty(unit);
    return unit;
}

void GolemLostEnemy()
{
    EnchantOff(SELF, "ENCHANT_BLINDED");
}

void RemoveMecaGolemHitDelay(int ptr)
{
    int unit = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));
    int hp = CurrentHealth(unit), mxHp = MaxHealth(unit), pic; //func;

    if (hp)
    {
        if (DistanceUnitToUnit(unit, target) > 27.0)
            MoveObject(ptr, GetObjectX(unit) + UnitRatioX(target, unit, 4.0), GetObjectY(unit) + UnitRatioY(target, unit, 4.0));
        else
            MoveObject(ptr, GetObjectX(unit), GetObjectY(unit));
        //func = GetUnitDeathFunc(unit);
        Delete(unit);
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        pic = FieldMobStoneGolem(1);
        //SetCallback(pic, 5, func);
        Damage(pic, 0, mxHp - hp, -1);
        LookAtObject(pic, target);
        HitLocation(pic, GetObjectX(pic), GetObjectY(pic));
        DeleteObjectTimer(CreateObject("ReleasedSoul", 1), 9);
        AudioEvent("HitStoneBreakable", 1);
    }
    Delete(ptr);
}

void MecaGolemStrike()
{
    int ptr;
    float dist = DistanceUnitToUnit(SELF, OTHER);

    LookAtObject(SELF, OTHER);
    if (dist > 39.0)
        PushObjectTo(SELF, UnitRatioX(OTHER, SELF, 21.0), UnitRatioY(OTHER, SELF, 21.0));
    if (!HasEnchant(SELF, "ENCHANT_BURNING") && dist < 110.0)
    {
        Enchant(SELF, "ENCHANT_BURNING", 0.3);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF), GetObjectY(SELF));
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        FrameTimerWithArg(6, ptr, RemoveMecaGolemHitDelay);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.0);
}

int FieldMobStoneGolem(int location)
{
    int unit = CreateObject("StoneGolem", location);

    SetUnitMaxHealth(unit, 1800);
    Raise(CreateObject("InvisibleLightBlueLow", location), 20);
    CommonFieldMobProperty(unit);
    SetUnitScanRange(unit, 600.0);
    SetCallback(unit, 3, MecaGolemStrike);
    SetCallback(unit, 5, GolemDeathHandler);
    SetCallback(unit, 7, PlayerCreatureRisk);
    SetCallback(unit, 13, GolemLostEnemy);

    return unit;
}

int StrongWizardWhiteBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[17] = 200; arr[19] = 60; arr[21] = 1065353216; arr[23] = 34816; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; arr[55] = 20; arr[56] = 30; 
	pArr = arr;
	return pArr;
}

void StrongWizardWhiteSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1072064102;		ptr[137] = 1072064102;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 34816;		uec[121] = StrongWizardWhiteBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int FieldMobWhiteWiz(int location)
{
    int unit = CreateObjectById(OBJ_STRONG_WIZARD_WHITE, LocationX(location), LocationY(location));

    Raise(CreateObjectById( OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(unit),GetObjectY(unit)), 11);
    CommonFieldMobProperty(unit);
    return unit;
}

void Part1MonsterPut(int ptr)
{
    int count = GetDirection(ptr);

    if (count)
    {
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        if (Random(0, 1))
            FieldMonsterCreate("Swordsman", 2, 1, 100);
        else
            FieldMonsterCreate("Wolf", 2, 1, 110);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, Part1MonsterPut);
    }
    else
        Delete(ptr);
}

void Part1FlyingMonsterPut(int ptr)
{
    int count = GetDirection(ptr);

    if (count)
    {
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        FieldMonsterCreate(ToStr(GetDirection(ptr + 1)), ToInt(GetObjectZ(ptr + 1)), 1, ToInt(GetObjectZ(ptr)));
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, Part1FlyingMonsterPut);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

int LotsOfFieldMonster(string unitName, int location, int expAmount, int hitPointAmount, int copyCounts)
{
    int unit = CreateObject("InvisibleLightBlueLow", location);
    
    CreateObject("InvisibleLightBlueLow", location);
    Raise(unit, hitPointAmount);
    Raise(unit + 1, expAmount);
    LookWithAngle(unit, copyCounts);
    LookWithAngle(unit + 1, SToInt(unitName));
    Part1FlyingMonsterPut(unit);
    return unit;
}

void FieldMonsterLastPart()
{
    LotsOfFieldMonster("WeirdlingBeast", 123, 4, 160, 10);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    FieldMobWhiteWiz(124);
    LotsOfFieldMonster("LichLord", 26, 4, 275, 10);
    FieldMobFireFairy(12);
    FieldMobFireFairy(12);
    FieldMobFireFairy(12);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobFireFairy(13);
    FieldMobStoneGolem(125);
}

void FieldMonsterPart4()
{
    FieldMobFireFairy(17);
    FieldMobNormalZombie(17);
    FieldMobFireFairy(15);
    FieldMobFireFairy(15);
    FieldMobFireFairy(15);
    LotsOfFieldMonster("SpittingSpider", 113, 2, 175, 3);
    FieldMobFireFairy(14);
    FieldMobFireFairy(114);
    LotsOfFieldMonster("OgreWarlord", 115, 3, 325, 4);
    LotsOfFieldMonster("LichLord", 116, 6, 275, 5);
    FieldMobWhiteWiz(117);
    FieldMobWhiteWiz(117);
    FieldMobWhiteWiz(117);
    FieldMobWhiteWiz(117);
    FieldMobWhiteWiz(117);
    FieldMobFireFairy(118);
    FieldMobFireFairy(119);
    LotsOfFieldMonster("WeirdlingBeast", 120, 4, 160, 3);
    LotsOfFieldMonster("Shade", 121, 3, 185, 3);
    LotsOfFieldMonster("Shade", 122, 3, 185, 3);
    FieldMobFireFairy(11);
    FieldMobFireFairy(11);
}

void FieldMonsterPart3P1()
{
    LotsOfFieldMonster("Troll", 108, 5, 306, 2);
    LotsOfFieldMonster("Urchin", 109, 1, 64, 6);
    LotsOfFieldMonster("SpittingSpider", 148, 3, 175, 3);
    LotsOfFieldMonster("SpittingSpider", 149, 3, 175, 3);
    LotsOfFieldMonster("Urchin", 110, 1, 64, 6);
    LotsOfFieldMonster("Troll", 111, 5, 306, 2);
    LotsOfFieldMonster("Troll", 112, 5, 306, 2);
}

void FieldMonsterPart3P()
{
    FieldMobGhost(98);
    FieldMobGhost(99);
    LotsOfFieldMonster("SkeletonLord", 100, 3, 295, 10);
    LotsOfFieldMonster("Goon", 101, 4, 225, 6);
    LotsOfFieldMonster("EvilCherub", 102, 1, 85, 20);
    LotsOfFieldMonster("EvilCherub", 103, 1, 85, 6);
    LotsOfFieldMonster("LichLord", 103, 5, 295, 2);
    FieldMobVileZombie(103);
    FieldMobVileZombie(103);
    FieldMobVileZombie(103);
    FieldMobVileZombie(104);
    FieldMobVileZombie(104);
    FieldMobVileZombie(104);
    LotsOfFieldMonster("EvilCherub", 107, 1, 85, 10);
    LotsOfFieldMonster("Skeleton", 105, 3, 225, 5);
    LotsOfFieldMonster("SkeletonLord", 105, 3, 295, 5);
    LotsOfFieldMonster("BlackWidow", 105, 5, 260, 5);
    LotsOfFieldMonster("Scorpion", 106, 3, 280, 5);
    LotsOfFieldMonster("SpittingSpider", 106, 3, 180, 5);
    FieldMobGhost(106);
    FieldMobGhost(106);
    FrameTimer(1, FieldMonsterPart3P1);
}

void FieldMonsterPart3()
{
    LotsOfFieldMonster("GruntAxe", 87, 2, 225, 6);
    LotsOfFieldMonster("OgreBrute", 87, 3, 295, 4);
    LotsOfFieldMonster("OgreWarlord", 88, 4, 325, 10);
    LotsOfFieldMonster("Scorpion", 89, 3, 260, 5);
    FieldMobGhost(90);
    FieldMobGhost(90);
    FieldMobGhost(90);
    FieldMobNormalZombie(90);
    FieldMobNormalZombie(90);
    FieldMobNormalZombie(90);
    LotsOfFieldMonster("Bear", 91, 3, 325, 3);
    LotsOfFieldMonster("EvilCherub", 91, 1, 75, 6);
    LotsOfFieldMonster("OgreWarlord", 92, 3, 325, 10);
    LotsOfFieldMonster("SkeletonLord", 93, 2, 275, 4);
    LotsOfFieldMonster("GruntAxe", 93, 1, 225, 4);
    LotsOfFieldMonster("EvilCherub", 93, 1, 75, 4);
    LotsOfFieldMonster("MeleeDemon", 94, 3, 160, 3);
    LotsOfFieldMonster("MeleeDemon", 95, 3, 160, 3);
    LotsOfFieldMonster("EmberDemon", 21, 4, 160, 2);
    LotsOfFieldMonster("Shade", 96, 2, 180, 3);
    LotsOfFieldMonster("SkeletonLord", 96, 2, 275, 3);
    FieldMobVileZombie(96);
    FieldMobGhost(94);
    FieldMobGhost(95);
    LotsOfFieldMonster("Shade", 97, 2, 180, 3);
    LotsOfFieldMonster("SkeletonLord", 97, 2, 275, 3);
    FieldMobVileZombie(97);
    FrameTimer(1, FieldMonsterPart3P);
}

void FieldMonsterPart2()
{
    LotsOfFieldMonster("Wasp", 71, 1, 48, 10);
    LotsOfFieldMonster("Wasp", 72, 1, 48, 10);
    LotsOfFieldMonster("Wasp", 73, 1, 48, 6);
    LotsOfFieldMonster("GruntAxe", 73, 4, 185, 6);
    LotsOfFieldMonster("GruntAxe", 74, 4, 185, 8);
    LotsOfFieldMonster("GruntAxe", 76, 4, 185, 8);
    LotsOfFieldMonster("GruntAxe", 75, 4, 185, 4);
    LotsOfFieldMonster("OgreBrute", 75, 4, 225, 4);
    LotsOfFieldMonster("SpittingSpider", 77, 4, 135, 3);
    LotsOfFieldMonster("GruntAxe", 77, 4, 185, 3);
    LotsOfFieldMonster("OgreBrute", 78, 4, 225, 6);
    LotsOfFieldMonster("Bear", 79, 6, 295, 6);
    LotsOfFieldMonster("Archer", 79, 1, 98, 8);
    FieldMobGhost(80);
    FieldMobGhost(80);
    LotsOfFieldMonster("Skeleton", 80, 3, 225, 5);
    LotsOfFieldMonster("SkeletonLord", 81, 4, 295, 10);
    LotsOfFieldMonster("Skeleton", 82, 3, 225, 4);
    LotsOfFieldMonster("EvilCherub", 82, 1, 64, 4);
    LotsOfFieldMonster("Skeleton", 83, 3, 225, 3);
    LotsOfFieldMonster("EvilCherub", 83, 1, 64, 3);
    FieldMobGhost(83);
    FieldMobGhost(83);
    FieldMobNormalZombie(83);
    FieldMobNormalZombie(83);
    FieldMobNormalZombie(83);
    FieldMobNormalZombie(83);
    FieldMobNormalZombie(83);
    LotsOfFieldMonster("EvilCherub", 84, 1, 64, 10);
    LotsOfFieldMonster("EvilCherub", 85, 1, 64, 10);
    FieldMobGhost(86);
    FieldMobGhost(86);
    FieldMobNormalZombie(86);
    FieldMobNormalZombie(86);
    FieldMobNormalZombie(86);
}

void FieldMonsterPart1P()
{
    LotsOfFieldMonster("Swordsman", 55, 3, 100, 20);
    LotsOfFieldMonster("Archer", 56, 2, 98, 6);
    LotsOfFieldMonster("AlbinoSpider", 57, 4, 125, 3);
    LotsOfFieldMonster("AlbinoSpider", 58, 4, 125, 3);
    LotsOfFieldMonster("Archer", 59, 2, 98, 6);
    LotsOfFieldMonster("FlyingGolem", 60, 2, 64, 5);
    LotsOfFieldMonster("FlyingGolem", 61, 2, 64, 5);
    LotsOfFieldMonster("Urchin", 62, 1, 64, 8);
    LotsOfFieldMonster("SpittingSpider", 63, 4, 100, 4);
    LotsOfFieldMonster("Spider", 64, 4, 135, 3);
    LotsOfFieldMonster("Bat", 65, 1, 32, 30);
    LotsOfFieldMonster("BlackBear", 66, 4, 220, 3);
    LotsOfFieldMonster("SpittingSpider", 66, 4, 100, 4);
    LotsOfFieldMonster("BlackBear", 67, 4, 220, 5);
    LotsOfFieldMonster("SpittingSpider", 68, 4, 100, 5);
    LotsOfFieldMonster("SkeletonLord", 69, 5, 200, 2);
    LotsOfFieldMonster("EvilCherub", 70, 3, 98, 4);
}

void FieldMonsterPart1()
{
    int ptr = CreateObject("InvisibleLightBlueLow", 38);
    LookWithAngle(CreateObject("InvisibleLightBlueLow", 39), 20);
    LookWithAngle(CreateObject("InvisibleLightBlueLow", 40), 20);
    LookWithAngle(ptr, 20);
    Part1MonsterPut(ptr);
    Part1MonsterPut(ptr + 1);
    Part1MonsterPut(ptr + 2);
    LotsOfFieldMonster("FlyingGolem", 18, 2, 64, 8);
    LotsOfFieldMonster("FlyingGolem", 24, 2, 64, 8);
    LotsOfFieldMonster("FlyingGolem", 41, 2, 64, 6);
    LotsOfFieldMonster("Archer", 42, 3, 98, 4);
    LotsOfFieldMonster("Archer", 43, 3, 98, 4);
    LotsOfFieldMonster("FlyingGolem", 44, 2, 64, 6);
    LotsOfFieldMonster("Urchin", 45, 2, 80, 10);
    LotsOfFieldMonster("Urchin", 46, 2, 80, 10);
    LotsOfFieldMonster("Skeleton", 46, 3, 150, 10);
    LotsOfFieldMonster("FlyingGolem", 27, 2, 80, 10);
    LotsOfFieldMonster("Skeleton", 27, 3, 150, 10);
    LotsOfFieldMonster("Imp", 23, 1, 32, 10);
    LotsOfFieldMonster("Imp", 25, 1, 32, 10);
    LotsOfFieldMonster("Skeleton", 54, 3, 150, 10);
    FrameTimer(1, FieldMonsterPart1P);
}
