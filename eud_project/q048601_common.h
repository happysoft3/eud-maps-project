
#include "libs/define.h"
#include "libs/fxeffect.h"
#include "libs/hash.h"
#include "libs/unitstruct.h"
#include "libs/queueTimer.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"

#define PLAYER_START_LOCATION 12
#define MAX_PLAYER_COUNT 32
#define SPECIFY_KEY_ID 2180

int GenericHash() //virtual
{
    return 0;
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void GreenSparkFx(float xpos,float ypos)
{
    int ptr = CreateObjectAt("MonsterGenerator", xpos, ypos);

    PlaySoundAround(ptr, SOUND_AwardSpell);
    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

// void onSplashUnitVisible()
// {
//     int core=GetUnit1C(SELF);

//     if (ToInt(GetObjectX(core)))
//     {
//         if (DistanceUnitToUnit(SELF, OTHER)<=GetObjectZ(core))
//         {
//             int hash=GetUnit1C(core);

//             if (!HashGet(hash, GetCaller(), NULLPTR, FALSE))
//             {
//                 HashPushback(hash, GetCaller(), TRUE);
//                 Damage(OTHER, GetOwner(core), GetUnit1C(core+1), DAMAGE_TYPE_PLASMA);
//             }
//         }
//     }
// }

//  void endSplashDamage(int sub)
// {
//     if (ToInt(GetObjectX(sub)))
//     {
//         int hash=GetUnit1C(sub);

//         if (hash)
//             HashDeleteInstance(hash);
//         Delete(sub);
//         Delete(sub+1);
//     }
// }

// void SplashDamageAt(int owner, int dam, float x, float y, float range)
// {
//     int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
//     int ptr2=CreateObjectAt("InvisibleLightBlueHigh", x,y);
//     int hash;

//     HashCreateInstance(&hash);
//     SetUnit1C(ptr, hash);
//     SetOwner(owner, ptr);
//     PushTimerQueue(1, ptr, endSplashDamage);
//     SetUnit1C(ptr2, dam);
//     Raise(ptr, range);

//     int sub[4];
//     int k=sizeof(sub);
//     while (--k>=0)
//     {
//         sub[k]=CreateObjectById(OBJ_WEIRDLING_BEAST, x, y);
//         DeleteObjectTimer(sub[k], 1);
//         UnitNoCollide(sub[k]);
//         LookWithAngle(sub[k], k * 64);
//         SetOwner(ptr, sub[k]);
//         SetUnit1C(sub[k], ptr);
//         SetCallback(sub[k], 3, onSplashUnitVisible);
//     }
// }

// void invokeRawCode(char *pCode, int *args)
// {
//     int *pBuiltins=0x5c308c;
//     int *pOld = pBuiltins[31];
//     pBuiltins[31]=pCode;
//     Unused1f(args);
//     pBuiltins[31]=pOld;
// }

int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        /*
        push ebp
        mov ebp,esp
        push [ebp+0C]
        push [ebp+08]
        push 00000001
        mov eax,game.exe+107310
        call eax
        add esp,0C
        pop ebp
        ret 
        */
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        /*
        mov eax,game.exe+107250
        call eax
        push [eax]
        push [eax+04]
        push [eax+08]
        push [eax+0C]
        mov eax,game.exe+117F90
        call eax
        add esp,10
        xor eax,eax
        ret 
        */
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

void deferredPickupImpl(int item)
{
    int owner=GetOwner(item);

    if (CurrentHealth(owner))
        Pickup(owner,item);
}

void itemPickupSafety(int user, int item)
{
    if (CurrentHealth(user))
    {
        if (ToInt( GetObjectX(item)) )
        {
            if (HasItem(user,item))
                return;

            SetOwner(user,item);
            PushTimerQueue(1,item,deferredPickupImpl);
        }
    }
}

int retrieveSpecifyInventoryCount(int holder, short specifyId)
{
    int inv=GetLastItem(holder);
    int count=0;

    while (inv)
    {
        if (GetUnitThingID(inv)==specifyId)
            count+=1;
        inv=GetPreviousItem(inv);
    }
    return count;
}

int findOutSpecificInventory(int owner, short thingId)
{
    if (!CurrentHealth(owner))
        return 0;

    int inv=GetLastItem(owner);

    while (inv)
    {
        if (GetUnitThingID(inv)==thingId)
            return inv;
        inv=GetPreviousItem(inv);
    }
    return 0;
}

int ValidCallerCheck()
{
    return GetMemory(0x979724) >= 1000000001;
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

