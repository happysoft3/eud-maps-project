
#include "libs/define.h"
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"
#include "libs/mathlab.h"
#include "libs/fxeffect.h"
#include "libs/buff.h"
#include "libs/memutil.h"

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}
int DummyUnitCreateByName(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int DrawImageAt(float x, float y, int thingId)
{
    int unit = CreateObjectAt("AirshipBasketShadow", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, thingId);
    return unit;
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

void DrawTextOnBottom(int textId, int textSection, float xpos, float ypos){
    int m=DummyUnitCreateById(OBJ_FISH_BIG,xpos,ypos);
    char ss[]={0,32,64,96,128,160,192,224,};

    MakeEnemy(m);
    controlHealthbarMotion(m,textSection);
    LookWithAngle(m, ss[textId]);
    UnitNoCollide(m);
}

int CreateObjectAtUnit(string name, int sUnit)
{
    int *ptr = UnitToPtr(sUnit);
    float *coor = ptr;
    
    if (ptr != NULLPTR)
        return CreateObjectAt(name, coor[14], coor[15]);
    return 0;
}

void DelayGiveToOwner(int sTarget)
{
    int sOwner = GetOwner(sTarget);

    if (IsObjectOn(sTarget) && CurrentHealth(sOwner))
        Pickup(sOwner, sTarget);
    else
        Delete(sTarget);
}
int ShotMagicMissileRet(int owner)
{
    if (CurrentHealth(owner))
    {
        int subunit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner));

        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", owner, GetObjectX(owner) + UnitAngleCos(owner, 23.0), GetObjectY(owner) + UnitAngleSin(owner, 23.0));
        Delete(subunit);
        Delete(subunit + 2);
        Delete(subunit + 3);
        Delete(subunit + 4);
        return subunit+1;
    }
    return 0;
}
void MagicMissileOnCollide()
{
    Damage(OTHER, SELF, 200, 14);
    GreenSparkAt(GetObjectX(OTHER), GetObjectY(OTHER));
    Delete(SELF);
}

void UseMissileWand()
{
    int mis = ShotMagicMissileRet(OTHER);

    SetUnitCallbackOnCollide(mis, MagicMissileOnCollide);
    SetOwner(OTHER,mis);

    PlaySoundAround(OTHER, 221);
    Effect("RICOCHET", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
}

int SummonMissileWand(float xProfile, float yProfile)
{
    int wand = CreateObjectAt("MissileWand", xProfile, yProfile);
    int *ptr = UnitToPtr(wand);

    if (ptr != NULLPTR)
    {
        SetUnitCallbackOnUseItem(wand, UseMissileWand);
        ptr[3] = 65536;
        int *amount = ptr[184];

        if (amount != NULLPTR)
            amount[27] = 0xfafa;
    }
    return wand;
}

int SummonDemonsWand(float xProfile, float yProfile)
{
    int wand = CreateObjectAt("DemonsBreathWand", xProfile, yProfile);
    int *ptr = UnitToPtr(wand);

    if (ptr != NULLPTR)
    {
        int *amount = ptr[184];

        if (amount != NULLPTR)
            amount[27] = 0xfafa;
    }
    return wand;
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}
int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

int MagicWeaponContainer()
{
    int count;
    int array[20];

    return &count;
}

//@brief. 컨테이너가 포함한 노드 수를 반환합니다
int WeaponContainerCount()
{
    return GetMemory(MagicWeaponContainer());
}

//@brief. 인덱스에 해당하는 노드를 컨테이너로 부터 얻습니다
object WeaponContainerNode(int index)
{
    if (index < 20)
    {
        int ptr = MagicWeaponContainer() + 4;
        object functionId = GetMemory(ptr + (index * 4));

        if (functionId)
            return functionId;
    }
    return 0;   //@brief. nullptr;
}

//@brief. 입력된 인덱스에 해당하는 설명 데이터를 가져옵니다
string WeaponContainerDesc(int index)
{
    object nodeId = WeaponContainerNode(index);

    if (nodeId > 0)
    {
        int *ptr = GetScrDataField(nodeId);

        return ToStr(ptr[0]);
    }
    return "null";
}

//@brief. 입력된 인덱스에 해당하는 가격 데이터를 가져옵니다
int WeaponContainerPay(int index)
{
    object nodeId = WeaponContainerNode(index);

    if (nodeId > 0)
    {
        int *ptr = GetScrDataField(nodeId);

        return ptr[1];
    }
    return -1;
}

//@brief. 입력된 인덱스에 해당하는 생성함수 객체를 가져옵니다
object WeaponContainerFunction(int index)
{
    object nodeId = WeaponContainerNode(index);

    if (nodeId > 0)
    {
        int *ptr = GetScrDataField(nodeId);

        return ptr[2];
    }
    return -1;
}

void WeaponContainerPushback(object function)
{
    int *ptr = MagicWeaponContainer();
    int count = ptr[0];

    if (count < 20)
    {
        CallFunction(function);
        ptr[0] = count + 1;
        ptr[1 + count] = function;
    }
}



int FirstItem(int unit)
{
    return GetLastItem(unit);
}

int NextItem(int inv)
{
    return GetPreviousItem(inv);
}

int SetInvulnerabilityItem(int unit)
{
    int count = 0, inv = FirstItem(unit);

    while (inv)
    {
        if (!UnitCheckEnchant(inv, GetLShift(ENCHANT_INVULNERABLE)))
        {
            Enchant(inv, EnchantList(ENCHANT_INVULNERABLE), 0.0);
            ++count;
        }
        inv = NextItem(inv);
    }
    return count;
}
