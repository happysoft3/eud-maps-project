
#include "test1768_utils.h"
#include "libs/unitstruct.h"
#include "libs/objectIDdefines.h"
#include "libs/queueTimer.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"

void onKingFrogSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, GetOwner(GetOwner(SELF)), 100, DAMAGE_TYPE_PLASMA);
            // PlaySphericalEffect(OTHER);
            // NoxRestoreHealth(SELF, 100);
        }
    }
}

void BaseDoMotion(int cre, int motionId)
{
    // int mot=0x010000;
    int ptr=UnitToPtr(cre);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}


void onKingFrogJump(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int king=GetOwner(sub);

        if (MaxHealth(king))
        {
            int dur=GetDirection(sub);

            if (dur)
            {
                PushTimerQueue(1, sub, onKingFrogJump);
                MoveObjectVector(king,GetObjectZ(sub),GetObjectZ(sub+1));
                LookWithAngle(sub, dur-1);
                return;
            }
            float x=GetObjectX(king), y=GetObjectY(king);
            DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE, x,y ), 9);
            SplashDamageAt(king,x,y,90.0, onKingFrogSplash);
            PushTimerQueue(60, king, onKingFrogLoop);
            BaseDoMotion(king, 0);
        }
        Delete(sub);
        Delete(sub+1);
    }
}

int kingDoJump(int king, int target)
{
    float gap=DistanceUnitToUnit(king,target);

    if (gap<13.0)
        return FALSE;

    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(king),GetObjectY(king));
    int sub2=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(king),GetObjectY(king));

    LookWithAngle(sub, 32);
    Raise(sub, UnitRatioX(target,king,gap/32.0));
    Raise(sub+1,UnitRatioY(target,king,gap/32.0));
    SetOwner(king,sub);
    PushTimerQueue(1, sub, onKingFrogJump);
    Raise(king, 250.0);
    int bDirection = GetObjectX(king)>GetObjectX(target); //right

    LookWithAngle(king, 128*bDirection);
    BaseDoMotion(king, 1);
    return TRUE;
}

void onKingMoving(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int king=GetOwner(sub);

        if (MaxHealth(king))
        {
            int dur=GetUnit1C(sub);
            if (dur)
            {
                PushTimerQueue(1, sub,onKingMoving);
                SetUnit1C(sub,dur-1);
                PushObjectTo(king, UnitAngleCos(sub, 2.0),UnitAngleSin(sub,2.0));
                return;
            }
        }
        Delete(sub);
    }
}

void kingDoMoving(int king)
{
    int point=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(king), GetObjectY(king));
    int dir=Random(0,255);

    PushTimerQueue(1,point,onKingMoving);
    LookWithAngle(point, dir);
    SetOwner(king, point);
    SetUnit1C(point, Random(3, 18));
    int bDirection = GetObjectX(king)>(GetObjectX(point)+UnitAngleCos(point, 3.0));

    LookWithAngle(king, 128*bDirection);
}

void onKingFrogLoop(int king)
{
    if (MaxHealth(king))
    {
        int target = GetNearlyPlayer(king);

        if (target)
        {
            if (kingDoJump(king, target))
                return;
        }
        else
            PushTimerQueue(Random(1,60), king, kingDoMoving);
        PushTimerQueue(Random(70,120), king, onKingFrogLoop);
    }
}

void onBottleCandlePickup()
{
    UniPrint(OTHER, "승리-- 왕개구리를 물리치셨습니다!!");
    PlaySoundAround(OTHER, SOUND_JournalEntryAdd);
}

int createBottleCandle(float x, float y)
{
    int bot= CreateObjectById(OBJ_BOTTLE_CANDLE, x,y);

    SetUnitCallbackOnPickup(bot, onBottleCandlePickup);
    return bot;
}

void onKingFrogDead(int king)
{
    if (!MaxHealth(king))
        return;

    float x=GetObjectX(king),y=GetObjectY(king);
    int dummy=DummyUnitCreateById(OBJ_SPIDER, x,y);
    
    BaseDoMotion(dummy, 2);
    UnitNoCollide(dummy);
    Delete(king);
    UniPrintToAll("왕개구리를 죽이셨군요! 승리하셨습니다!!");
    createBottleCandle( x,y);
    createBottleCandle( x,y);
    createBottleCandle( x,y);
    createBottleCandle( x,y);
    createBottleCandle( x,y);
}

void onKingFrogCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, 0, 20, DAMAGE_TYPE_CRUSH);
            return;
        }
    }
}

void onKingFrogCoreDeath()
{
    int owner=GetOwner(SELF);

    onKingFrogDead(owner);
    Delete(SELF);
}

void kingcoreLoop(int orb)
{
    if (ToInt(GetObjectX(orb)))
    {
        int king=GetOwner(orb);

        if(king)
        {
            if (ToInt( DistanceUnitToUnit(orb,king)) )
                MoveObject(orb,GetObjectX(king),GetObjectY(king));
            PushTimerQueue(1, orb, kingcoreLoop);
        }
    }
}

void createKingFrogCore(int king)
{
    int orb=CreateObjectById(OBJ_ORB, GetObjectX(king),GetObjectY(king));
    int *hp=MemAlloc(8), *ptr=UnitToPtr(orb);

    SetMemory(ptr+0x22c, hp);
    SetUnitMaxHealth(orb, 1400);
    SetUnitFlags(orb,GetUnitFlags(orb)^(UNIT_FLAG_SHORT|UNIT_FLAG_NO_PUSH_CHARACTERS) );
    SetOwner(king,orb);
    SetUnitCallbackOnDeath(orb, onKingFrogCoreDeath);
    PushTimerQueue(1, orb, kingcoreLoop);
}

void CreateFrogKing(float x, float y)
{
    int king=DummyUnitCreateById(OBJ_SPIDER, x, y);
    
    createKingFrogCore(king);
    Frozen(king, FALSE);
    SetCallback(king,9,onKingFrogCollide);
    PushTimerQueue(1, king, onKingFrogLoop);
}


