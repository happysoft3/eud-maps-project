
#include "libs/define.h"
#include "libs/objectIDdefines.h"
#include "libs/weapon_effect.h"
#include "libs/itemproperty.h"
#include"libs/hash.h"
#include "libs/sound_define.h"
#include "libs/mathlab.h"
#include "libs/logging.h"
#include "libs/fxeffect.h"

int getWeaponFxHash(){
    int hash;
    if (!hash)
        HashCreateInstance(&hash);
    return hash;
}

void LaiserSwordFx(int unit)
{
	int fx, owner;

	if (IsObjectOn(unit))
	{
		fx = ToInt(GetObjectZ(unit + 1));
        owner = GetOwner(unit);
		if (CurrentHealth(owner))
		{
			if (IsObjectOn(fx))
				Delete(fx);
            else if (MaxHealth(unit) ^ CurrentHealth(unit))
                RestoreHealth(unit, MaxHealth(unit));
		}
		else
		{
			if (!IsObjectOn(fx))
			{
				MoveObject(unit + 1, GetObjectX(unit), GetObjectY(unit));
				Raise(unit + 1, CreateObjectAt("MediumBlueFlame", GetObjectX(unit), GetObjectY(unit)));
				Frozen(ToInt(GetObjectZ(unit + 1)), 1);
			}
		}
		FrameTimerWithArg(1, unit, LaiserSwordFx);
	}
	else
	{
        Delete(ToInt(GetObjectZ(unit + 1)));
		Delete(unit + 1);
	}
}

void LaiserSwordDisplay(int owner)
{
    float x_vect = UnitAngleCos(owner, 30.0), y_vect = UnitAngleSin(owner, 30.0);
    int k, ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner), GetObjectY(owner)) + 1;

    Delete(ptr - 1);
    MoveWaypoint(1, GetObjectX(owner) + x_vect, GetObjectY(owner) + y_vect);
    for (k = 0 ; k < 16 ; k += 1)
    {
        Frozen(CreateObject("Maiden", 1), 1);
        SetOwner(owner, ptr + k);
        DeleteObjectTimer(ptr + k, 1);
        SetCallback(ptr + k, 9, LaiserLifleTouched);
        Effect("SENTRY_RAY", GetObjectX(owner), GetObjectY(owner), GetObjectX(ptr + k), GetObjectY(ptr + k));
        MoveWaypoint(1, GetWaypointX(1) + x_vect, GetWaypointY(1) + y_vect);
        if (!IsVisibleTo(owner, ptr + k))
            break;
    }
}

void LaiserLifleTouched()
{
    int owner = GetOwner(SELF);

    if (owner == GetCaller())
        return;
    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
        AudioEvent("SentryRayHit", 1);
        Damage(OTHER, owner, 225, 16);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.1);
        Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
        Effect("VIOLET_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    }
}

void LaiserSwordStrike()
{
    LaiserSwordDisplay(OTHER);
}

void DeferredPickupUserWeapon(int weapon)
{
    if (IsObjectOn(weapon))
    {
        RegistItemPickupCallback(weapon, OnPickupUserWeapon);
        return;
    }
    int del;

    HashGet(getWeaponFxHash(), weapon, &del, TRUE);
}

void OnPickupUserWeapon()
{
    int *pMem;

    FrameTimerWithArg(1, GetTrigger(), DeferredPickupUserWeapon);
    if (HashGet(getWeaponFxHash(), GetTrigger(), &pMem, FALSE))
    {
        Delete(pMem[0]);
        Effect("CYAN_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void DeferredDiscardUserWeapon(int weapon)
{
    if (!MaxHealth(weapon))
        return;

    int *pMem;
    float xy[]={GetObjectX(weapon), GetObjectY(weapon)};
    if (HashGet(getWeaponFxHash(), weapon, &pMem, FALSE))
    {
        pMem[0]=CreateObjectAt("BlueSummons", xy[0], xy[1]);
        PlaySoundAround(weapon, SOUND_BurnCast);
        Effect("SPARK_EXPLOSION", xy[0], xy[1], 0.0, 0.0);
        WriteLog("deferreddiscarduserweapon-ok");
    }
    else
    {
        WriteLog("fail hash error - deferredDiscardUserWeapon");
    }
    
}

void OnDiscardUserWeapon()
{
    FrameTimerWithArg(1, GetTrigger(), DeferredDiscardUserWeapon);
}

void CreateUserWeaponFX(int weapon)
{
    int *pMem;

    AllocSmartMemEx(4, &pMem);
    pMem[0]=CreateObjectAt("BlueSummons", GetObjectX(weapon), GetObjectY(weapon));
    HashPushback(getWeaponFxHash(), weapon, pMem);
    RegistItemPickupCallback(weapon, OnPickupUserWeapon);
    SetUnitCallbackOnDiscardBypass(weapon, OnDiscardUserWeapon);
}

#define SPECIAL_WEAPON_PROPERTY 0
#define SPECIAL_WEAPON_FXCODE 1
#define SPECIAL_WEAPON_EXECCODE 2
#define SPECIAL_WEAPON_MAX 3

int LaiserSwordCreate(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SPARK_EXPLOSION, SOUND_MetalHitEarth, &property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(LaiserSwordStrike, &property[SPECIAL_WEAPON_EXECCODE]);
    // SpecialWeaponPropertySetFXCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_GREAT_SWORD,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}

