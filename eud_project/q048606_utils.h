
#include "libs/define.h"
#include "libs/printutil.h"
#include "libs/waypoint.h"
#include "libs/format.h"
#include "libs/unitstruct.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"
#include "libs/queueTimer.h"

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr = GetMemory(0x750710), k;

    SetMemory(ptr + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetUnitVoice(unit, 7);

    return unit;
}
int checkCoorValidate(float x, float y)
{
    if (x<100.0)
        return FALSE;
    if (y<100.0)
        return FALSE;
    if (x>5750.0)
        return FALSE;
    if (y>5750.0)
        return FALSE;
    return TRUE;
}
void SmokeRingFx(int flag)
{
    int wp = flag & 0xffff, dir = (flag >> 16) && 0xff, k;
    char buff[64];

    NoxSprintfString(&buff, "ArrowTrap%dSmoke",&dir,1);
    for (k = 0 ; k < 36 ; k +=1)
    {
        DeleteObjectTimer(
            CreateObjectAt(
                ReadStringAddressEx(&buff),
                 LocationX(wp) + MathSine(k * 10 + 90, 69.0),
                  LocationY(wp) + MathSine(k * 10, 69.0)),
                   3);
    }
}int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}void StrWarShop()
{
	int arr[27];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 2116092670; arr[1] = 1107312389; arr[2] = 1082395128; arr[3] = 270600208; arr[4] = 34087168; arr[5] = 1912873074; arr[6] = 144705552; arr[7] = 135348708; arr[8] = 33038978; arr[9] = 272833680; 
	arr[10] = 153125897; arr[11] = 557973636; arr[12] = 1652720674; arr[13] = 1090503945; arr[14] = 8980608; arr[15] = 16847169; arr[16] = 0; arr[17] = 76038144; arr[18] = 1611128960; arr[19] = 2227727; 
	arr[20] = 67125828; arr[21] = 1082394628; arr[22] = 278954000; arr[23] = 33620480; arr[24] = 66854978; arr[25] = 1057486912; arr[26] = 267419136; 
	while(i < 27)
	{
		drawStrWarShop(arr[i], name);
		i ++;
	}
}

void drawStrWarShop(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(43);
		pos_y = LocationY(43);
	}
	for (i = 1 ; i > 0 && count < 837 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 43);
		if (count % 76 == 75)
            TeleportLocationVector(43, -150.0, 2.0);
		else
            TeleportLocationVector(43, 2.0, 0.0);
		count +=1;
	}
	if (count >= 837)
	{
		count = 0;
		TeleportLocation(43, pos_x, pos_y);
	}
}

void StrNewSkills()
{
	int arr[27];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 1378345024; arr[1] = 540420; arr[2] = 1052656; arr[3] = 270608658; arr[4] = 33554436; arr[5] = 1246034952; arr[6] = 16713744; arr[7] = 268763408; arr[8] = 33826082; arr[9] = 539230208; 
	arr[10] = 158893956; arr[11] = 252; arr[12] = 537267202; arr[13] = 2080376740; arr[14] = 2095135; arr[15] = 19472352; arr[16] = 0; arr[17] = 1140850688; arr[18] = 2095252; arr[19] = 8192; 
	arr[20] = 1076175868; arr[21] = 134217856; arr[22] = 2013339640; arr[23] = 2105362; arr[24] = 1073748736; arr[25] = 134512767; arr[26] = 405802992; 
	while(i < 27)
	{
		drawStrNewSkills(arr[i], name);
		i ++;
	}
}

void drawStrNewSkills(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(44);
		pos_y = LocationY(44);
	}
	for (i = 1 ; i > 0 && count < 837 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 44);
		if (count % 76 == 75)
            TeleportLocationVector(44, -150.0, 2.0);
		else
            TeleportLocationVector(44, 2.0, 0.0);
		count +=1;
	}
	if (count >= 837)
	{
		count = 0;
		TeleportLocation(44, pos_x, pos_y);
	}
}

void StrTomb()
{
	int arr[23];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 1009771134; arr[1] = 1338505220; arr[2] = 142870792; arr[3] = 17834002; arr[4] = 571483170; arr[5] = 71336008; arr[6] = 138645646; arr[7] = 285344033; arr[8] = 553811488; arr[9] = 1207436408; 
	arr[10] = 168296576; arr[11] = 4103; arr[12] = 671089278; arr[13] = 16388; arr[14] = 276692992; arr[15] = 2130771986; arr[16] = 1107574727; arr[17] = 523336; arr[18] = 135299329; arr[19] = 1048897; 
	arr[20] = 528614404; arr[21] = 4195328; arr[22] = 1044496; 
	while(i < 23)
	{
		drawStrTomb(arr[i], name);
		i ++;
	}
}

void drawStrTomb(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(45);
		pos_y = LocationY(45);
	}
	for (i = 1 ; i > 0 && count < 713 ; i <<= 1)
	{
		if (i & arg_0)
			Nop( CreateObject(name, 45) );
		if (count % 64 == 63)
            TeleportLocationVector(45, -126.0, 2.0);
		else
            TeleportLocationVector(45,2.0,0.0);
		count +=1;
	}
	if (count >= 713)
	{
		count = 0;
		TeleportLocation(45, pos_x, pos_y);
	}
}

int CheckUnitLimitPos(int unit)
{
    float pos_x = GetObjectX(unit), pos_y = GetObjectY(unit);

    if (pos_x > 100.0 && pos_y > 100.0 && pos_x < 5600.0 && pos_y < 5000.0)
        return 1;
    return 0;
}

void SplashDamage(int owner, int dam, float range, float xpos, float ypos)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", xpos, ypos) + 1, k;

    SetOwner(owner, ptr - 1);
    MoveObject(ptr - 1, range, GetObjectY(ptr - 1));
    Raise(ptr - 1, ToFloat(dam));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", xpos,ypos), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 32);
        SetOwner(ptr - 1, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(ptr - 1, 2);
}

void UnitVisibleSplash()
{
    int parent;

    if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    {
        parent = GetOwner(SELF);
        if (DistanceUnitToUnit(SELF,OTHER) <= GetObjectX(parent))
        {
            Enchant(OTHER, "ENCHANT_VILLAIN", 0.1);
            Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
        }
    }
}

int EquipedWeapon(int ptr)
{
    int pic = GetMemory(GetMemory(ptr + 0x2ec) + 0x68);

    if (pic)
        return GetMemory(pic + 0x2c);
    else
        return 0;
}

void StrWeaponUpgrade()
{
	int arr[27];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 1613227004; arr[1] = 1132752975; arr[2] = 8456696; arr[3] = 1179666; arr[4] = 1111498754; arr[5] = 294976; arr[6] = 167559049; arr[7] = 537927968; arr[8] = 537796610; arr[9] = 2017985536; 
	arr[10] = 67145743; arr[11] = 17699265; arr[12] = 570425380; arr[13] = 1552433600; arr[14] = 557960; arr[15] = 4352; arr[16] = 539107908; arr[17] = 69271492; arr[18] = 143525856; arr[19] = 33622033; 
	arr[20] = 33817096; arr[21] = 1090798658; arr[22] = 8487168; arr[23] = 236450050; arr[24] = 4202594; arr[25] = 4128832; arr[26] = 134250504; 
	while(i < 27)
	{
		drawStrWeaponUpgrade(arr[i], name);
		i ++;
	}
}

void drawStrWeaponUpgrade(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(52);
		pos_y = LocationY(52);
	}
	for (i = 1 ; i > 0 && count < 837 ; i <<= 1)
	{
		if (i & arg_0)
			Nop( CreateObject(name, 52) );
		if (count % 76 == 75)
            TeleportLocationVector(52, -150.0, 2.0);
		else
            TeleportLocationVector(52, 2.0, 0.0);
		count +=1;
	}
	if (count >= 837)
	{
		count = 0;
		TeleportLocation(52, pos_x, pos_y);
	}
}

void StrYourWinner()
{
	int arr[26];
	string name = "CharmOrb";
	int i = 0;
	arr[0] = 16777346; arr[1] = 2656256; arr[2] = 34078720; arr[3] = 536872960; arr[4] = 324; arr[5] = 2176; arr[6] = 17891328; arr[7] = 570425344; arr[8] = 120066332; arr[9] = 126821712; 
	arr[10] = 1149804871; arr[11] = 356533440; arr[12] = 52577417; arr[13] = 285353029; arr[14] = 304392706; arr[15] = 285739282; arr[16] = 672106513; arr[17] = 667455781; arr[18] = 541362240; arr[19] = 1149796356; 
	arr[20] = 553714244; arr[21] = 17891876; arr[22] = 152183876; arr[23] = 252577801; arr[24] = 286263048; arr[25] = 2328721; 
	while(i < 26)
	{
		drawStrYourWinner(arr[i], name);
		i ++;
	}
}

void drawStrYourWinner(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(53);
		pos_y = LocationY(53);
	}
	for (i = 1 ; i > 0 && count < 806 ; i <<= 1)
	{
		if (i & arg_0)
			Nop( CreateObject(name, 53) );
		if (count % 80 == 79)
            TeleportLocationVector(53, -158.0, 2.0);
		else
            TeleportLocationVector(53, 2.0, 0.0);
		count +=1;
	}
	if (count >= 806)
	{
		count = 0;
		TeleportLocation(53, pos_x, pos_y);
	}
}
void PowerWeapon()
{
    int inv = GetLastItem(OTHER);
    int count = 0;

    Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
    while (IsObjectOn(inv))
    {
        if (!HasEnchant(inv, "ENCHANT_INVULNERABLE") && HasClass(inv, "WEAPON"))
        {
            count ++;
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
        }
        inv = GetPreviousItem(inv);
    }
    if (count)
    {
        char buff[128];

        NoxSprintfString(&buff, "%d개의 무기류 아이템이 무적처리 되었습니다", &count, 1);
        UniPrint(OTHER,ReadStringAddressEx(&buff));
    }
}

void PowerArmor()
{
    int inv = GetLastItem(OTHER);
    int count = 0;

    Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
    while (IsObjectOn(inv))
    {
        if (!HasEnchant(inv, "ENCHANT_INVULNERABLE") && HasClass(inv, "ARMOR"))
        {
            count +=1;
            Enchant(inv, "ENCHANT_INVULNERABLE", 0.0);
        }
        inv = GetPreviousItem(inv);
    }
    if (count)
    {
        char buff[128];

        NoxSprintfString(&buff, "%d개의 갑옷류 아이템이 무적처리 되었습니다", &count, 1);
        UniPrint(OTHER,ReadStringAddressEx(&buff));
    }
}
void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

void BloodingFx(int unit)
{
    int k;

    for (k = 0 ; k < 36 ; k +=1)
    {
        DeleteObjectTimer(
            CreateObjectAt("PlayerWaypoint", GetObjectX(unit) + MathSine(k * 10 + 90, 48.0), GetObjectY(unit) + MathSine(k * 10, 48.0)), 6);
    }
}

void NothingAnymore()
{
    return;
}

int WUpgradeTable(int adr)
{
    int hash[100], key;

    if (adr)
        key = GetMemory(adr + 4) % 100;
    if (!hash[0])
    {
        hash[0] = 1;
        //sword,    longsword,  greats_sd,  quiver, crossbow
        //ob_hbd , ob_ht, ob_orb,   ob_wie, war_hammer
        //morning_star, round_chakram, shuriken, battle_axe
        hash[74] = 5; hash[69] = 11; hash[70] = 23; hash[68] = 1; hash[82] = 1;
        hash[22] = 14; hash[23] = 13; hash[25] = 1; hash[24] = 16; hash[75] = 35;
        hash[71] = 6; hash[76] = 3; hash[78] = 5; hash[72] = 20;
        return 0;
    }
    return hash[key];
}

void Notification()
{
    if (IsCaller(GetTrigger() + 2))
    {
        int ptr = ToInt(GetObjectZ(GetTrigger() + 1));
        char buff[128];
        if (IsObjectOn(ptr))
        {
            int args[]={GetDirection(ptr), ToInt(GetObjectZ(ptr))};

            NoxSprintfString(&buff, "현재 %d 웨이브 도전중\n킬수: %d", &args, sizeof(args) );
            UniChatMessage(SELF, ReadStringAddressEx(&buff), 60);
        }
    }
}

int GetMasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 10);

        CreateObject("InvisibleLightBlueHigh", 10);
        Frozen(CreateObject("BlackPowder", 10), 1);
        SetCallback(unit, 9, Notification);
        Frozen(unit, 1);
        MoveObject(unit, 5500.0, 100.0);
        MoveObject(unit + 2, 5500.0, 100.0);
    }
    return unit;
}

void PlayWav(int id){}//virtual
void NextStage(int ptr){}//virtual

void CheckCurrentKills()
{
    int ptr = ToInt(GetObjectZ(GetMasterUnit() + 1));
    int kill = ToInt(GetObjectZ(ptr));
    char buff[128];

    if (IsObjectOn(ptr))
    {
        if (kill < GetDirection(ptr + 1) - 1)
            Raise(ptr, ToFloat(kill + 1));
        else
        {
            PlayWav(SOUND_SoulGateTouch);
            int lv = GetDirection(ptr)+1;

            NoxSprintfString(&buff, "%d 웨이브를 완료했습니다. 잠시 후 다음 웨이브가 시작됩니다", &lv, 1);
            UniPrintToAll(ReadStringAddressEx(&buff));
            LookWithAngle(ptr, GetDirection(ptr) + 1); //Level_up
            Raise(ptr, ToFloat(0));
            SecondTimerWithArg(10, ptr, NextStage);
        }
    }
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

#define HPDATA_OWNER 0
#define HPDATA_PREV_DATA 1
#define HPDATA_HPBAR_UNIT 2
#define HPDATA_MAX 3

void onHealthbarProgress(int *hpdata)
{
    int t=hpdata[HPDATA_HPBAR_UNIT];

    if (MaxHealth(t)){
        int owner=hpdata[HPDATA_OWNER];
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,hpdata,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (hpdata[HPDATA_PREV_DATA] !=perc)
            {
                controlHealthbarMotion(t,perc);
                hpdata[HPDATA_PREV_DATA]=perc;
            }
            return;
        }
    }
    Delete(t);
    FreeSmartMemEx(hpdata);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));
    int *hpdata;

    AllocSmartMemEx(HPDATA_MAX*4,&hpdata);
    hpdata[HPDATA_OWNER]=unit;
    hpdata[HPDATA_PREV_DATA]=0;
    hpdata[HPDATA_HPBAR_UNIT]=bar;
    UnitNoCollide(bar);
    PushTimerQueue(1,hpdata,onHealthbarProgress);
}
