
#include "libs/define.h"
#include "libs/playerinfo.h"
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"
#include "libs/queueTimer.h"
#include "libs/printutil.h"
#include "libs/format.h"

int WarAbilityTable(int aSlot, int pIndex)
{
    int *p=0x753600;
    int *computed=&p[pIndex*6];

    return computed[aSlot];
}

void WarAbilityUse(int pUnit, int aSlot, int actionFunction)
{
    int chk[160], pIndex = GetPlayerIndex(pUnit), cTime;
    int arrPic;

    if (!(pIndex >> 0x10))
    {
        arrPic = pIndex * 5 + aSlot; //EyeOf=5, harpoon=3, sneak=4, berserker=1
        cTime = WarAbilityTable(aSlot, pIndex);
        if (cTime ^ chk[arrPic])
        {
            if (!chk[arrPic])
            {
                CallFunctionWithArg(actionFunction, pUnit);
            }
            chk[arrPic] = cTime;
        }
    }
}

int CreateToxicCloud(float xpos, float ypos, int owner, short time)
{
    int cloud=CreateObjectAt("ToxicCloud", xpos, ypos);
    int *ptr=UnitToPtr(cloud);
    int *pEC=ptr[187];

    pEC[0]=time;
    SetOwner(owner, cloud);
}

void GreenSparkFx(float x, float y)
{
    int gen=CreateObjectById(OBJ_MONSTER_GENERATOR,x,y);
    Damage(gen,0,1,DAMAGE_TYPE_PLASMA);
    Delete(gen);
}

void WispDestroyFX(float xpos, float ypos)
{
    int unit = CreateObjectById(OBJ_WILL_O_WISP, xpos, ypos);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

void invokeRawCode(char *pCode, int *args)
{
    int *pBuiltins=0x5c308c;
    int *pOld = pBuiltins[31];
    pBuiltins[31]=pCode;
    Unused1f(args);
    pBuiltins[31]=pOld;
}

int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAtEx(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

int CreateDeadDamageHandler(int fn)
{
    char code[]={
        0x55, 0x8B, 0xEC, 0x83, 0xEC, 0x14, 0x8B, 0x45, 0x08, 0x89, 0x04, 0x24, 
        0x8B, 0x45, 0x0C, 0x89, 0x44, 0x24, 0x04, 0x8B, 0x45, 0x10, 0x89, 0x44, 
        0x24, 0x08, 0x8B, 0x45, 0x14, 0x89, 0x44, 0x24, 0x0C, 0x8B, 0x45, 0x18, 
        0x89, 0x44, 0x24, 0x10, 0xB8, 0x30, 0x0B, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 
        0xC4, 0x14, 0xFF, 0x75, 0x08, 0xFF, 0x75, 0x0C, 0x68, 0x18, 0x00, 0x00, 
        0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x8B, 0xE5, 0x5D, 0xC3, 0x90
    };
    int c;

    AllocSmartMemEx(sizeof(code), &c);
    char *p=c;
    int *pfn=&p[57];
    NoxByteMemCopy(code, p, sizeof(code));

    pfn[0]=fn;
    return c;
}

void ChakramCopyProperies(int src, int dest)
{
    int *srcPtr = UnitToPtr(src), *destPtr = UnitToPtr(dest);
    
    int *srcProperty = srcPtr[173], *destProperty = destPtr[173];

    int rep=-1;

    while (++rep<4)
        destProperty[rep] = srcProperty[rep];

    int rep2=-1;

    while (++rep2<32)
        destPtr[140+rep2] = srcPtr[140+rep2];
}

void CreateChakram(float xpos, float ypos, int *pDest, int owner)
{
    int mis = CreateObjectAt("RoundChakramInMotion", xpos, ypos);
    int *ptr=UnitToPtr(mis);

    if (pDest)
        pDest[0] = mis;

    ptr[174] = 5158032;
    ptr[186] = 5483536;
    SetOwner(owner, mis);

    int *collideDataMb = MemAlloc(8);
    collideDataMb[0]=0;
    collideDataMb[1] = UnitToPtr(owner);
    ptr[175] = collideDataMb;
}

void OnChakramTracking(int owner, int cur)
{
    int mis;

    CreateChakram(GetObjectX(owner) + UnitAngleCos(owner, 11.0), GetObjectY(owner) + UnitAngleSin(owner, 11.0), &mis, owner);
    ChakramCopyProperies(cur, mis);

    PushObject(mis, 30.0, GetObjectX(owner), GetObjectY(owner));
}

void AfterChakramTracking(int *pMem)
{
    if (!pMem)
        return;

    int inv = pMem[0], owner = pMem[1];

    if (CurrentHealth(owner) && IsObjectOn(inv))
    {
        Pickup(owner, inv);
    }
    FreeSmartMemEx(pMem);
}

void HookChakrm(int cur, int owner)
{
    // UnitSetEnchantTime(cur, ENCHANT_RUN, 0);
    // Enchant(cur, EnchantList(ENCHANT_ETHEREAL),0.0);

    int *ptr=UnitToPtr(cur);

    if (ptr[186] == 5496000)
    {
        if (!CurrentHealth(owner))
            return;

        int *pMem;

        AllocSmartMemEx(8, &pMem);
        pMem[0] = GetLastItem(cur);
        pMem[1] = owner;
        OnChakramTracking(owner, cur); //이게 여기로 옮겨지고 cur 인자를 더 전달합니다
        Delete(cur);
        PushTimerQueue(2, pMem, AfterChakramTracking);
    }
}

int GetPercentRate(int cur, int max)
{
    if (cur>=max)
        return TRUE;

    return Random(1, max)<=cur;
}

int GermToMoney(int user)
{
    if (!IsPlayerUnit(user))
        return 0;

    int sellTable[]={10000, 5000, 1000};
    int inv=GetLastItem(user), cur, id;
    int res=0;

    while (inv)
    {
        cur=inv;
        inv=GetPreviousItem(inv);
        id=GetUnitThingID(cur);
        if (id>=OBJ_DIAMOND&&id<=OBJ_RUBY)
        {
            Delete(cur);
            res+=sellTable[id-OBJ_DIAMOND];
        }
    }
    return res;
}

#define PRINT_ALL -1

void PrintMessageFormatOne(int user, string fmt, int arg)
{
    char message[192];

    NoxSprintfString(message, fmt, &arg, 1);
    if (user==-1)
    {
        UniPrintToAll(ReadStringAddressEx(message));
        return;
    }
    UniPrint(user, ReadStringAddressEx(message));
}

void onDeferredPickup(int item)
{
    if (ToInt(GetObjectX(item)))
    {
        int owner=GetOwner(item);

        if (CurrentHealth(owner))
            Pickup(owner, item);
    }
}

void SafetyPickup(int user, int item)
{
    if (CurrentHealth(user))
    {
        if (ToInt(GetObjectX(item)))
        {
            SetOwner(user,item);
            PushTimerQueue(1, item, onDeferredPickup);
        }
    }
}
