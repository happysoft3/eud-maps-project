
#include "pam00755_utils.h"
#include "libs/queueTimer.h"
#include "libs/waypoint.h"

#define FIREWAY_LOCATION_ID 0
#define FIREWAY_COUNT 1
#define FIREWAY_PTR_CONTROL 2
#define FIREWAY_RETURN_X 3
#define FIREWAY_RETURN_Y 4
#define FIREWAY_CUR_COUNT 5
#define FIREWAY_MAX 6

void onFirewayCollide()
{
    if (CurrentHealth(OTHER))
    {
        Damage(OTHER,0,30,DAMAGE_TYPE_FLAME);
    }
}

void explosionFirewayAt(float x, float y)
{
    int sub=CreateObjectById(OBJ_AIRSHIP_BASKET_SHADOW, x,y);
    int flags = GetUnitFlags(sub);

    if (flags & UNIT_FLAG_NO_COLLIDE)
        SetUnitFlags(sub, flags^UNIT_FLAG_NO_COLLIDE);
    SetUnitCallbackOnCollide(sub,onFirewayCollide);
    DeleteObjectTimer(sub,1);
    Effect("EXPLOSION",x,y,0.0,0.0);
}

void progressFireway(int *pNode)
{
    int *pControl=pNode[FIREWAY_PTR_CONTROL];

    if (!pControl[0])
    {
        FreeSmartMemEx(pNode);
        return;
    }
    int limit = pNode[FIREWAY_COUNT];
    int count = pNode[FIREWAY_CUR_COUNT];
    int locationId=pNode[FIREWAY_LOCATION_ID];
    float *xy=&pNode[FIREWAY_RETURN_X];

    if (count < limit)
    {
        explosionFirewayAt(LocationX(locationId),LocationY(locationId));
        TeleportLocationVector(locationId, -46.0, -46.0);
        pNode[FIREWAY_CUR_COUNT]=++count;
        PushTimerQueue(3, pNode, progressFireway);
        return;
    }
    pNode[FIREWAY_CUR_COUNT]=0;
    TeleportLocation(locationId, xy[0],xy[1]);
    PushTimerQueue(13, pNode, progressFireway);
}

void StartFireway(int locationId, int count, int *pControl, int startDelay)
{
    int *pNode;

    AllocSmartMemEx(FIREWAY_MAX*4, &pNode);
    pNode[FIREWAY_LOCATION_ID]=locationId;
    pNode[FIREWAY_COUNT]=count;
    pNode[FIREWAY_PTR_CONTROL]=pControl;
    pNode[FIREWAY_RETURN_X]=LocationX(locationId);
    pNode[FIREWAY_RETURN_Y]=LocationY(locationId);
    pNode[FIREWAY_CUR_COUNT]=0;
    PushTimerQueue(startDelay, pNode, progressFireway);
}


