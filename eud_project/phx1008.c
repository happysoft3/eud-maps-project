
#include "phx1008_mob.h"
#include "phx1008_resource.h"
#include "phx1008_initscan.h"
#include "libs\waypoint.h"
#include "libs\printutil.h"
#include "libs\itemproperty.h"
#include "libs\buff.h"
#include "libs\fxeffect.h"
#include "libs\sound_define.h"
#include "libs\coopteam.h"
#include "libs\playerupdate.h"
#include "libs\weaponcapacity.h"
#include "libs\potionex.h"
#include "libs\spellutil.h"
#include "libs\mathlab.h"
#include "libs\networkRev.h"
#include "libs/format.h"
#include "libs\reaction.h"
#include "libs/logging.h"
#include "libs/objectIDdefines.h"

int m_countdownUnit;

int m_player[10];
int m_pFlags[10];
int *m_pBooster;
int *m_pBoosterFx;
int m_boostTime[10];


int *m_pRespawnMark;
int m_respawnMarkCount;
int m_lastCreatedUnit;
int m_ramenCount;

// int *m_chatmsgHash;

int m_countdown = 180;
int m_countdownCycle;
int m_gameStarted = FALSE;
int m_gameEnded = FALSE;

#define THING_ID_ANKH 2688
#define THING_ID_NECROMANCER_MARKER 2675

#define PLAYER_DEATH_FLAG 2
#define PLAYER_FLAG_BOOSTER 4

int PlayerClassCheckFlag(int plr, int flag)
{
    return m_pFlags[plr] & flag;
}

void PlayerClassSetFlag(int plr, int flag)
{
    m_pFlags[plr] ^= flag;
}

int CheckPlayerWithId(int pUnit)
{
    int rep = sizeof(m_player);

    while ((--rep) >=0)
    {
        if (m_player[rep] ^ pUnit)
            continue;
        
        return rep;
    }
    return -1;
}

int CheckPlayer()
{
    int rep = sizeof(m_player);

    while ((--rep) >= 0)
    {
        if (IsCaller(m_player[rep]))
            return rep;
    }
    return -1;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    m_player[plr] = pUnit;
    m_pFlags[plr] = 1;
    DiePlayerHandlerEntry(pUnit);
    SelfDamageClassEntry(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));

    return plr;
}

void InitPlayerBooster()
{
    string marker="AmbBeachBirds";
    int locationId = 43;
    int booster[10], u;
    int boosterFx[10];

    for (u=0;u<sizeof(m_player);Nop(++u))
    {
        booster[u] = DummyUnitCreateById(OBJ_AIRSHIP_CAPTAIN, LocationX(locationId), LocationY(locationId));
        CreateObjectAtEx(marker, LocationX(locationId), LocationY(locationId), 0);
        boosterFx[u] = CreateObjectAt("BlackPowder", LocationX(locationId), LocationY(locationId));
        Enchant(boosterFx[u], EnchantList(ENCHANT_RUN), 0.0);
    }
    m_pBooster=&booster;
    m_pBoosterFx=&boosterFx;
}

void TeleportPlayerRandomPosition(int plrUnit)
{
    int destLocation;

    GetRespawnMarkRandomly(&destLocation);
    Effect("SMOKE_BLAST", GetObjectX(plrUnit), GetObjectY(plrUnit), 0.0, 0.0);
    MoveObject(plrUnit, LocationX(destLocation), LocationY(destLocation));
    Effect("TELEPORT", LocationX(destLocation), LocationY(destLocation), 0.0, 0.0);
    PlaySoundAround(plrUnit, SOUND_BlindOff);
}

void TeleportPlayerAllRandomPos()
{
    int rep = -1;

    while ((++rep) < sizeof(m_player))
    {
        if (CurrentHealth(m_player[rep]))
            TeleportPlayerRandomPosition(m_player[rep]);
    }
}

void TeleportAllPlayer(int locationId)
{
    int u;

    for (u = 9 ; u >= 0 ; Nop(--u))
    {
        if (CurrentHealth(m_player[u]))
            MoveObject(m_player[u], LocationX(locationId), LocationY(locationId));
    }
}

void DrawImageAtEx(float x, float y, int thingId)
{
    int *ptr = UnitToPtr(CreateObjectAt("AirshipBasketShadow", x, y));

    ptr[1] = thingId;
}

static void NetworkUtilClientMain()
{
    InitializeResource();
}

void ShowHotKey(int pUnit)
{
    if (MaxHealth(pUnit))
    {
        UniPrint(pUnit, "명령어 정보: /d - 모든 아이템 떨구기(인벤 버그 걸렸을 때 쓰세요), /t - 블링크 시전(12초마다 사용가능)");
    }
}

void PlayerClassOnJoin(int plr, int pUnit)
{
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    if (m_gameStarted)
    {
        if (m_gameEnded)
            MoveObject(pUnit, LocationX(39), LocationY(39));
        else
        {
            TeleportPlayerRandomPosition(pUnit);
            FrameTimerWithArg(20, pUnit, ShowHotKey);
        }
    }
    else
    {
        MoveObject(pUnit, LocationX(37), LocationY(37));
        UniPrint(pUnit, "맵 이름: 3분안에 라면 먹기, 제작자: noxgameremaster, 릴리즈 일자. 23nov-2021");
    }
    
}

void PlayerClassOnFailToJoin(int pUnit)
{
    MoveObject(pUnit, LocationX(13), LocationY(13));
    UniPrint(pUnit, "합류 실패! 다시 시도해보세요");
}

void PlayerClassOnEntry(int pUnit)
{
    while (TRUE)
    {
        if (CurrentHealth(pUnit))
        {
            int plr = CheckPlayerWithId(pUnit);
            int i;

            for (i = sizeof(m_player)-1 ; i >= 0 && plr < 0 ; Nop(--i))
            {
                if (!MaxHealth(m_player[i]))
                {
                    plr = PlayerClassOnInit(i, pUnit);
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerClassOnJoin(plr, pUnit);
                break;
            }
            PlayerClassOnFailToJoin(pUnit);
        }
        break;
    }
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        if (CheckPlayer() >= 0)
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
        {
            MoveObject(OTHER, LocationX(12), LocationY(12));
        }
        
    }
}

void PlayerCastWindbooster(int pUnit)
{
    EnchantOff(pUnit, EnchantList(ENCHANT_SNEAK));
    RemoveTreadLightly(pUnit);
    PushObjectTo(pUnit, UnitAngleCos(pUnit, 50.0), UnitAngleSin(pUnit, 50.0));
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void onExplosionSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 100, DAMAGE_TYPE_PLASMA);
        }
    }
}

void PlayerCastExplosion(int pUnit)
{
    EnchantOff(pUnit, EnchantList(ENCHANT_SNEAK));
    RemoveTreadLightly(pUnit);
    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_ANTI_MAGIC)))
    {
        Enchant(pUnit, EnchantList(ENCHANT_ANTI_MAGIC), 5.0);
        SplashDamageAtEx(pUnit, GetObjectX(pUnit), GetObjectY(pUnit), 140.0, onExplosionSplash);
        DrawGeometryRingAt("CharmOrb", GetObjectX(pUnit), GetObjectY(pUnit), 15);
        // ManaBombCancelFx(pUnit);     //FIXME. caused crash
    }
    else
    {
        UniPrint(pUnit, "쿨 다운 입니다... 5초");
    }
    
}

void ResetBoosterPos(int plr)
{
    if (ToInt( DistanceUnitToUnit(m_pBooster[plr], m_pBooster[plr]+1)) )
    {
        MoveObject(m_pBooster[plr], GetObjectX(m_pBooster[plr] + 1), GetObjectY(m_pBooster[plr] + 1));
        MoveObject(m_pBoosterFx[plr], GetObjectX(m_pBooster[plr] +1), GetObjectY(m_pBooster[plr]+1));
    }
}

void PlayerBoosterEffect(int plr, int pUnit)
{
    while (TRUE)
    {
        if (m_boostTime[plr] > 0)
        {
            --m_boostTime[plr];
            if (CheckPlayerInput(pUnit) == 2)
            {
                float calcx = GetObjectX(pUnit) - UnitAngleCos(pUnit, 10.0), calcy=GetObjectY(pUnit) - UnitAngleSin(pUnit, 10.0);

                MoveObject(m_pBooster[plr], calcx, calcy);
                MoveObject(m_pBoosterFx[plr], calcx, calcy);
                break;
            }
        }
        else
        {
            PlayerClassSetFlag(plr, PLAYER_FLAG_BOOSTER);
            WispDestroyFX(GetObjectX(pUnit), GetObjectY(pUnit));
        }
        ResetBoosterPos(plr);
        break;
    }
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_SNEAK)))
        PlayerCastExplosion(pUnit);
    
    if (PlayerClassCheckFlag(plr, PLAYER_FLAG_BOOSTER))
        PlayerBoosterEffect(plr, pUnit);
}

void PlayerClassOnDeath(int plr, int pUnit)
{
    PutRamen(GetObjectX(pUnit), GetObjectY(pUnit), 0);

    ResetBoosterPos(plr);
    if (m_boostTime[plr])
        m_boostTime[plr]=0;

    char buff[160], *userName=StringUtilGetScriptStringPtr(PlayerIngameNick(pUnit));

    NoxSprintfString(buff, "%s 님께서 전사당하셨습니다. 죽은 위치에 라면 1개가 재 생산됩니다", &userName, 1);
    WriteLog(ReadStringAddressEx(buff));
    UniPrintToAll(ReadStringAddressEx(buff));
}

void PlayerClassOnExit(int plr)
{
    m_player[plr] = 0;
    m_pFlags[plr] = 0;
}

void PlayerClassOnLoop()
{
    int u = sizeof(m_player);

    while (--u>=0)
    {
        while (TRUE)
        {
            if (MaxHealth(m_player[u]))
            {
                if (GetUnitFlags(m_player[u]) & UNIT_FLAG_NO_COLLIDE)
                    1;
                else if (CurrentHealth(m_player[u]))
                {
                    PlayerClassOnAlive(u, m_player[u]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckFlag(u, PLAYER_DEATH_FLAG))
                        break;
                    else
                    {
                        PlayerClassSetFlag(u, PLAYER_DEATH_FLAG);
                        PlayerClassOnDeath(u, m_player[u]);
                    }
                    break;
                }                
            }
            if (m_pFlags[u])
                PlayerClassOnExit(u);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void OnHitEntrypad()
{
    WriteLog("OnHitEntrypad-start");
    PlayerClassOnEntry(GetCaller());
    WriteLog("OnHitEntrypad-end");
}

void MapExit()
{
    MusicEvent();
    ResetPlayerHandlerWhenExitMap();
    RemoveCoopTeamMode();
    ResetHostileCritter();
}

void GetRespawnMarkRandomly(int *pDest)
{
    pDest[0] = m_pRespawnMark[Random(0, m_respawnMarkCount-1)];
}

void InitRespawnMark()
{
    if (!m_pRespawnMark)
    {
        int marks[] = {14, 15, 16, 17, 18, 19, 20, 36, 21, 22, 23, 24,
            25, 26, 27,28,29,30,31,32,33,34,35};

        m_pRespawnMark=marks;
        m_respawnMarkCount=sizeof(marks);
    }
}

void IncreaseCountdownTable(int *pDest)
{
    int hash[97];

    if (pDest)
        pDest[0] = &hash;
}

void SetCountdownTable(int thingId, int setTo)
{
    int *pTable;

    IncreaseCountdownTable(&pTable);
    pTable[thingId % 97] = setTo;
}

void GetCountdownTable(int thingId, int *pDest)
{
    int *pTable;

    IncreaseCountdownTable(&pTable);
    pDest[0] = pTable[thingId % 97];
}

void IncreaseCountdown(int unit)
{
    int thingId = GetUnitThingID(unit);

    if (!thingId)
        return;
    
    int inc=0;

    GetCountdownTable(thingId, &inc);

    if (!inc)
        ++inc;

    m_countdown += inc;
}

void RewardWeaponName(string *pStrArr, int *pLength)
{
    string *nameptr;
    int length;

    if (!SToInt(nameptr))
    {
        string namearr[] = {"WarHammer", "GreatSword", "Sword", "Longsword", "StaffWooden", "OgreAxe", "BattleAxe", "FanChakram", "RoundChakram",
            "OblivionHalberd", "OblivionHeart", "OblivionWierdling"};

        nameptr = &namearr;
        length = sizeof(namearr);
    }
    pStrArr[0] = nameptr;
    if (pLength)
        pLength[0] = length;
}

void RewardArmorName(string *pStrArr, int *pLength)
{
    string *nameptr;
    int length;

    if (!SToInt(nameptr))
    {
        string armorarr[] = {"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "ChainCoif", "ChainLeggings", "ChainTunic",
            "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", "LeatherHelm", "LeatherLeggings", "MedievalCloak",
            "MedievalPants", "MedievalShirt"};

        nameptr=&armorarr;
        length =sizeof(armorarr);
    }
    pStrArr[0] = nameptr;
    if (pLength)
        pLength[0] = length;
}

void RewardPotionName(string *pStrArr, int *pLength)
{
    string *nameptr;
    int length;

    if (!SToInt(nameptr))
    {
        string potions[]={"RedPotion", "Cider", "CurePoisonPotion", "VampirismPotion", "ShieldPotion", "InvisibilityPotion", "InvulnerabilityPotion",
            "ShieldPotion", "HastePotion", "FireProtectPotion", "ShockProtectPotion", "PoisonProtectPotion", "YellowPotion", "BlackPotion"};

        nameptr=&potions;
        length=sizeof(potions);
    }
    pStrArr[0] = nameptr;
    if (pLength)
        pLength[0] = length;
}

void RewardAny(string *pStrArr, int *pLength)
{
    string anyitems[] = {"RedPotion", "Compass", "RedPotion"};

    
    pStrArr[0] = anyitems;
    if (pLength)
        pLength[0] = sizeof(anyitems);
}

void StartupWeaponProperties(int weapon)
{
    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));

    int thingId = GetUnitThingID(weapon);

     if (thingId >= 222 && thingId <= 225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId == 1178 || thingId == 1168)
        SetConsumablesWeaponCapacity(weapon, 255, 255);

    if (MaxHealth(weapon))
        SetUnitMaxHealth(weapon, MaxHealth(weapon)*3);
}

void CreateRewardRandomWeapon(float x,float y)
{
    string *table;
    int length, weapon;

    RewardWeaponName(&table, &length);
    CreateObjectAtEx(table[Random(0, --length)], x,y, &weapon);
    StartupWeaponProperties(weapon);
}

void StartupArmorProperties(int armor)
{
    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    if (MaxHealth(armor))
        SetUnitMaxHealth(armor, MaxHealth(armor)*3);
}

void CreateRewardRandomArmor(float x, float y)
{
    string *table;
    int length, armor;

    RewardArmorName(&table, &length);
    CreateObjectAtEx(table[Random(0, --length)], x,y, &armor);
    StartupArmorProperties(armor);
}

void OnUseSozu()
{
    if (!IsPlayerUnit(OTHER))
        return;
    
    Delete(SELF);
    int plr = CheckPlayer();

    if (plr < 0)
        return;

    if (!PlayerClassCheckFlag(plr, PLAYER_FLAG_BOOSTER))
        PlayerClassSetFlag(plr, PLAYER_FLAG_BOOSTER);

    m_boostTime[plr] = 300;
    UniPrint(OTHER, "소주를 마셨습니다. 잠시동안 이동속도 대폭증가");
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    else if (thingID == OBJ_CIDER)
    {
        int *ptr = UnitToPtr(unit);

        SetUnitCallbackOnUseItem(unit, OnUseSozu);
        Frozen(unit,TRUE);
    }
    if (x ^ unit) Delete(unit);

    return x;
}

void CreateRewardRandomPotion(float x,float y)
{
    string *table;
    int length, pot;

    RewardPotionName(&table, &length);
    CreateObjectAtEx(table[Random(0, --length)], x,y, &pot);
    pot = CheckPotionThingID(pot);
}

void ItemUseTimeIncrease()
{
    Delete(SELF);

    if (m_countdown && !m_gameEnded)
    {
        m_countdown+=7;
        WispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
        UniPrint(OTHER, "아이템 사용: 시간아 멈춰라! 시간 7초 연장!!");
    }
}

void CreateRewardRandomAny(float x, float y)
{
    string *table;
    int length, anyitem;

    RewardAny(&table, &length);
    CreateObjectAtEx(table[Random(0, --length)], x,y, &anyitem);

    int thingId = GetUnitThingID(anyitem);

    if (thingId == 1190)    //compass
    {
        SetUnitCallbackOnUseItem(anyitem, ItemUseTimeIncrease);
    }
}

void CreateRewardBones(int posunit)
{
    string bones[] = {"CorpseLeftLowerArmE", "CorpseLeftLowerArmN", "CorpseLeftLowerArmNE", "CorpseLeftLowerArmNW", "CorpseLeftLowerArmS", "CorpseLeftLowerArmSE",
        "CorpseLeftLowerArmSW", "CorpseLeftLowerArmW", "CorpseLeftLowerLegE", "CorpseLeftLowerLegN", "CorpseLeftLowerLegNE", "CorpseLeftLowerLegNW", "CorpseLeftLowerLegS",
        "CorpseLeftLowerLegSE", "CorpseLeftLowerLegSW", "CorpseLeftLowerLegW", "CorpsePelvisN", "CorpsePelvisS", "CorpsePelvisSW"};
    int size=sizeof(bones);

    CreateObjectAtEx(bones[Random(0, --size)], GetObjectX(posunit), GetObjectY(posunit), NULLPTR);
}

static void createRewardProto(int functionId, float x, float y)
{
    Bind(functionId, &functionId+4);
}

void PlacingRewardItem(int posunit)
{
    int functions[] = { CreateRewardRandomAny, CreateRewardRandomArmor, CreateRewardRandomPotion, CreateRewardRandomWeapon };

    createRewardProto(functions[Random(0, sizeof(functions)-1)], GetObjectX(posunit), GetObjectY(posunit));
    Delete(posunit);
}

void OnEnemyDeath()
{
    int ftable[] = {PlacingRewardItem, CreateRewardBones};

    IncreaseCountdown(SELF);
    DeleteObjectTimer(SELF, 3);
    float x=GetObjectX(SELF),y= GetObjectY(SELF);
    int smoke = CreateObjectAt("OldSmoke", x,y);

    FrameTimerWithArg(9, smoke, ftable[Random(0, 1)]);
    DeleteObjectTimer(smoke, 30);
    DeleteObjectTimer(CreateObjectById(OBJ_EXPLOSION,x,y),9);
    MoveObject(SELF, LocationX(53),LocationY(53));
}

void CommonEnemyAttribution(int enemy)
{
    RetreatLevel(enemy, 0.0);
    SetCallback(enemy, 5, OnEnemyDeath);
}

void SelectRandomMob(int posUnit)
{
    int *pFunction, length;

    if (!pFunction)
        InitMonsterTable(&pFunction, &length);
    
    if (!length)
        return;

    int pic = Random(0, length - 1);

    if (!pFunction[pic])
        return;

    int mob = CallFunctionWithArgInt(pFunction[pic], posUnit);

    CommonEnemyAttribution(mob);
}

void RamenOnUseItem()
{
    if (m_ramenCount > 0)
        --m_ramenCount;

    if (!m_ramenCount)
    {
        DeductionGameResult();
        UniChatMessage(OTHER, "흐음.. 다 먹었군.. 꺼어억!!", 150);
        Effect("WHITE_FLASH", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
    
    int fx= CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER));

    UnitSetEnchantTime(fx, ENCHANT_VAMPIRISM, 30);
    Enchant(fx, EnchantList(ENCHANT_PROTECT_FROM_MAGIC), 0.0);
    DeleteObjectTimer(fx, 21);
    PlaySoundAround(OTHER, SOUND_PotionUse);
    RestoreHealth(OTHER, 40);

    Delete(SELF);
    char buff[128];

    NoxSprintfString(buff, "라면을 섭취하였습니다. 남은 개수: %d개", &m_ramenCount, 1);
    UniPrint(OTHER, ReadStringAddressEx(buff));
}

void PutRamen(float xpos, float ypos, int *pResult)
{
    int ramen;

    CreateObjectAtEx("AmuletOfTeleportation", xpos, ypos, &ramen);
    SetUnitCallbackOnUseItem(ramen, RamenOnUseItem);
    SetItemPropertyAllowAllDrop(ramen);
    ++m_ramenCount;

    if (pResult)
        pResult[0] = ramen;
}

void UnitScanRamen(int posUnit)
{
    PutRamen(GetObjectX(posUnit), GetObjectY(posUnit), NULLPTR);
    Delete(posUnit);
}

void UnitScanMobMarker(int posUnit)
{
    SelectRandomMob(posUnit);
    Delete(posUnit);
}

void UnitScanEnded(int *pParams)
{
    WriteLog("UnitScanEnded");
}

void startInitscan(){
    int initScanHash, lastUnit=CreateObjectById(OBJ_RED_POTION,100.0,100.0);
    HashCreateInstance(&initScanHash);
    // InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, PlacingRewardItem);
    HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, UnitScanMobMarker);
    HashPushback(initScanHash, OBJ_ANKH, UnitScanRamen);
    StartUnitScanEx(Object("mapfirstscan"), lastUnit, initScanHash, UnitScanEnded);
}

void DisplayTextBoard()
{
    if (!IsCaller(GetTrigger() + 1))
        return;

    if ((m_countdownCycle++) < 30)
        return;

    char buff[192];
    int args[]={m_countdown, m_ramenCount};
    m_countdownCycle = 0;

    NoxSprintfString(buff, "3분안에 라면 먹기\n남은 시간: %d 초\n남은 라면 수: %d 개", args, sizeof(args));
    UniChatMessage(SELF, ReadStringAddressEx(buff), 35);
}

void Displayer(int *pDest)
{
    int unit = CreateObjectAt("Hecubah", 5700.0, 100.0);

    CreateObjectAtEx("BlackPowder", GetObjectX(unit), GetObjectY(unit), 0);
    Frozen(unit, TRUE);
    SetCallback(unit, 9, DisplayTextBoard);
    if (pDest)
        pDest[0] = unit;
}

void RemoveCountdownUnit()
{
    Delete(m_countdownUnit++);
    Delete(m_countdownUnit++);
}

void OnPickedFlag()
{
    UniPrint(OTHER, "!!당신은 우승자 에요!!");
}

void PutWinnerFlag(float xpos, float ypos, int *pDest)
{
    int flag = CreateObjectAt("GreenFlag", xpos, ypos);

    UnitNoCollide(flag);
    SetUnitCallbackOnPickup(flag, OnPickedFlag);
    pDest[0]=flag;
}

void PutLargeFlame(float xpos, float ypos, int *pDest)
{
    int flame = CreateObjectAt("LargeFlame", xpos, ypos);

    UnitSetEnchantTime(flame, ENCHANT_FREEZE, 0);
    Enchant(flame, "ENCHANT_BURNING", 0.0);
    Frozen(flame, TRUE);
    pDest[0] =flame;
}

void PutOneLineOf(int location, float gap, int isWin)
{
    int linecount = 8;
    int baseunit = CreateObjectAt("AmbBeachBirds", LocationX(location), LocationY(location));

    if (isWin)
    {
        while ((linecount--) >= 0)
            PutWinnerFlag(GetObjectX(baseunit) - gap, GetObjectY(baseunit) + gap, &baseunit);
        return;
    }
    while ((linecount--)>=0)
        PutLargeFlame(GetObjectX(baseunit) - gap, GetObjectY(baseunit) + gap, &baseunit);
}

void PutResultDecor(int location, int count, int isWin)
{
    float gap = 46.0;

    while (count--)
    {
        PutOneLineOf(location, gap, isWin);
        TeleportLocationVector(location, gap, gap);
    }

    int textcount=20;
    int textId[] = {1567, 1566};
    int seltext = textId[isWin&TRUE];

    while (textcount--)
    {
        DrawImageAtEx(LocationX(42), LocationY(42), seltext);
        TeleportLocationVector(42, 0.0, 23.0);
    }
}

void DeductionGameResult()
{
    if (m_gameEnded)
        return;

    m_gameEnded = TRUE;
    RemoveCountdownUnit();
    TeleportAllPlayer(39);
    PutResultDecor(40, 9, m_ramenCount==0);
    if (m_ramenCount==0)
    {
        UniPrintToAll("승리했습니다! 라면을 다 먹다니.. 대단하군요!!");
    }
    else
    {
        UniPrintToAll("패배했습니다! 아쉽게도 라면이 몇개 남아 있었어요.. ;n;");
    }
}

void Countdown()
{
    if ((--m_countdown) <= 0)
    {
        DeductionGameResult();
        return;
    }
    if (!m_gameEnded)
        SecondTimer(1, Countdown);
}

void StartCountdown()
{
    Displayer(&m_countdownUnit);
    SecondTimer(3, Countdown);
}

void StartGame()
{
    int once;

    if (once)
        return;
    
    WriteLog("StartGame");
    once = TRUE;
    m_gameStarted = once;
    FrameTimer(30, StartCountdown);
    PlaySoundAround(OTHER, SOUND_HecubahTaunt);
    UniPrintToAll("----3분안에 라면먹기----");
    UniPrintToAll("----게임을 시작하지요----");
    FrameTimer(3, TeleportPlayerAllRandomPos);
    WriteLog("StartGame-end");
}

void PutStartButton()
{
    int button = DummyUnitCreateById(OBJ_MIMIC, LocationX(38), LocationY(38));

    SetDialog(button, "NORMAL", StartGame, Nop);
}

void OnPickedYellow()
{
    UniPrint(OTHER, "이거 줍는거 아니에여.. 걍 가까이 오셔서 빨리 시작하십셔");
    PlaySoundAround(OTHER, SOUND_NoCanDo);
}

void PutStartupFlag(float xpos, float ypos)
{
    int yellow = CreateObjectAt("YellowFlag", xpos, ypos);

    UnitNoCollide(yellow);
    SetUnitCallbackOnPickup(yellow, OnPickedYellow);
}

void StartupMapPickets()
{
    RegistSignMessage(Object("sign1"), "3분안에 라면먹기          --제작. noxgameremaster");
    RegistSignMessage(Object("sign2"), "3분안에 라면먹기: 3분은 솔직히 에바라서 몬스터 잡으면 제한시간이 조금 씩 더 증가되는 걸로!");
    RegistSignMessage(Object("sign3"), "3분안에 라면먹기: 맵에 있는 모든 라면을 오직 3분안에 먹어야 승리한다");
}

void MapDelayInvoke()
{
    SetCountdownTable(1365, 8);    //bear
    SetCountdownTable(1346, 4);     //swordsman
    SetCountdownTable(1345, 3);     //archer
    SetCountdownTable(2273, 6);     //Troll
    PutStartButton();

    // DrawImageAtEx(LocationX(41), LocationY(41), 2560);
    PutStartupFlag(LocationX(11), LocationY(11));
}

void DropAll(int pUnit)
{
    while (GetLastItem(pUnit))
        Nop(Drop(pUnit, GetLastItem(pUnit)));
}

int OnChatAllDrop(int pUnit)
{
    if (GetLastItem(pUnit))
    {
        DropAll(pUnit);
        UniPrint(pUnit, "모든 아이템을 드랍합니다. -인벤토리 버그로 인한 유일한 해결 책이었습니다 ;n;");
    }
    return TRUE;
}

int OnChatBlink(int pUnit)
{
    if (!m_gameStarted)
        return TRUE;

    if (m_gameEnded)
        return TRUE;

    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_ANCHORED)))
    {
        UniPrint(pUnit, "블링크 대기 시간입니다");
        return TRUE;
    }
    TeleportPlayerRandomPosition(pUnit);
    Enchant(pUnit, EnchantList(ENCHANT_ANCHORED), 12.0);
    UniPrint(pUnit, "블링크를 시전하셨어요 12초");
    return TRUE;
}

void MapInitialize()
{
    MusicEvent();
    m_lastCreatedUnit = CreateObjectAt("ImaginaryCaster", 5500.0, 100.0);
    CreateLogFile("eat_ramen-log.txt");
    startInitscan();

    FrameTimer(30, MapDelayInvoke);

    InitRespawnMark();
    StartupMapPickets();
    FrameTimer(1, MakeCoopTeam);
    SetHostileCritter();

    InitPlayerBooster();
    // HashCreateInstance(&m_chatmsgHash);
    // ChatMessageEventPushback("/d", OnChatAllDrop);
    // ChatMessageEventPushback("/t", OnChatBlink);

    FrameTimer(10, PlayerClassOnLoop);
    blockObserverMode();
    initGame();
    WriteLog("end mapinitialize");
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);
    
    if (pUnit)
        ShowQuestIntroOne(pUnit, 237, "NoxWorldMap", "Noxworld.c:Forming");

    if (pInfo==0x653a7c)
    {
        InitializeResource();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}

