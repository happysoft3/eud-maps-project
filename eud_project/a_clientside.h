
#include "libs/animFrame.h"
#include "libs/grplib.h"
#include "libs/objectIDdefines.h"

#define MY_IMAGE_VECTOR_SIZE 360
#define MY_TEXT_COLOR 0x07FD

void initializeTextObjects(int imgVector)
{
    short wMessage1[32];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("괴수들의 소굴"), wMessage1);
    AppendTextSprite(imgVector, MY_TEXT_COLOR, wMessage1, 121811);
    short wMessage2[36];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("화이어볼 트랩의 시련"), wMessage2);
    AppendTextSprite(imgVector, MY_TEXT_COLOR, wMessage2, 121819);
    short wMessage3[36];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("방사선 투과실"), wMessage3);
    AppendTextSprite(imgVector, MY_TEXT_COLOR, wMessage3, 121807);
    short wMessage4[32];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("지하 물류창고"), wMessage4);
    AppendTextSprite(imgVector, MY_TEXT_COLOR, wMessage4, 121799);
    short wMessage5[32];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("던전 클리어~~"), wMessage5);
    AppendTextSprite(imgVector, MY_TEXT_COLOR, wMessage5, 121823);
}

void GRPDump_FemaleBeachGirl(){}

#define FEMALE_BEACH_GIRL_IMAGE_ID 129717

void initializeImageFrameBeachGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_FemaleBeachGirl)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4);
    ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    AnimFrameAssign8Directions(0, FEMALE_BEACH_GIRL_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  FEMALE_BEACH_GIRL_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(10, FEMALE_BEACH_GIRL_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(15, FEMALE_BEACH_GIRL_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(20, FEMALE_BEACH_GIRL_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,FEMALE_BEACH_GIRL_IMAGE_ID+40,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_ETOniOutput(){}

#define ETONI_BASE_IMAGE_ID 122915
void initializeImageFrameETOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_ETOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, ETONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, ETONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     ETONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, ETONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     ETONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     ETONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     ETONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     ETONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     ETONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     ETONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_SlimeDumpOutput(){}

#define SLIME_DUMP_OUTPUT_IMAGE_START_AT 121568

void initializeImageSlimeOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GIANT_LEECH;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SlimeDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,5,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign4Directions(6,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign4Directions(9,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign4Directions(12,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);

    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDumpRoachOniOutput(){}

#define ROACH_ONI_IMAGE_ID 130133

void initializeImageFrameRoachOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRoachOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);

    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign4Directions(0, ROACH_ONI_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, ROACH_ONI_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     ROACH_ONI_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, ROACH_ONI_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     ROACH_ONI_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     ROACH_ONI_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     ROACH_ONI_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
}

void GRPDump_Woman2Output(){}

#define WOMAN_2_IMAGE_ID 128403

void initializeImageFrameWoman2(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH_WITH_ORB;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_Woman2Output)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,6,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,6,2,2);
    AnimFrameAssign8Directions(0,WOMAN_2_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,WOMAN_2_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,WOMAN_2_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,WOMAN_2_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,WOMAN_2_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,WOMAN_2_IMAGE_ID+40,IMAGE_SPRITE_MON_ACTION_RUN,4);
    AnimFrameAssign8Directions(30,WOMAN_2_IMAGE_ID+48,IMAGE_SPRITE_MON_ACTION_RUN,5);

    AnimFrameCopy8Directions(WOMAN_2_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy8Directions(WOMAN_2_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(WOMAN_2_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(WOMAN_2_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(WOMAN_2_IMAGE_ID+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
    AnimFrameCopy8Directions(WOMAN_2_IMAGE_ID+48,IMAGE_SPRITE_MON_ACTION_WALK,5);
}

void GRPDump_ProtossArchonOutput(){}

#define PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT 127714

void initializeImageFrameProtossArchon(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_HECUBAH;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_ProtossArchonOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,4,3,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(45, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign8Directions(50, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign8Directions(55, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign8Directions(5,  PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssign8Directions(10, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssign8Directions(15, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign8Directions(20, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameAssign8Directions(25, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameAssign8Directions(30, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameAssign8Directions(35, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameAssign8Directions(40, PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+96, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT,  IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT,  IMAGE_SPRITE_MON_ACTION_WALK,0);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(      PROTOSS_ARCHON_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_WALK,3);
}

void InitializeClientResource(){
    int imgVector=CreateImageVector(MY_IMAGE_VECTOR_SIZE);

    initializeTextObjects(imgVector);
    initializeImageFrameBeachGirl(imgVector);
    initializeImageFrameETOni(imgVector);
    initializeImageSlimeOutput(imgVector);
    initializeImageFrameRoachOni(imgVector);
    initializeImageFrameWoman2(imgVector);
    initializeImageFrameProtossArchon(imgVector);
    DoImageDataExchange(imgVector);
}
