
#include "fkjapan_utils.h"
#include "libs/animFrame.h"
#include "libs/grplib.h"

#define IMAGE_VECTOR_SIZE 2048

void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
}

void GRPDump_TalesweaverWallensteinOutput(){}

#define TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT 131415

void initializeImageFrameTalesweavergirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_NECROMANCER;
    int xyInc[]={0,-13};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_TalesweaverWallensteinOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,7,2,2);
    AnimFrameAssign8Directions(0,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(10, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(15, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(20, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(25, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(30, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameAssign8Directions(35, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameAssign8Directions(40, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameAssign8Directions(75, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 0);
    AnimFrameAssign8Directions(70, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 1);
    AnimFrameAssign8Directions(65, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 2);
    AnimFrameAssign8Directions(60, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+96, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 3);
    AnimFrameAssign8Directions(55, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 4);
    AnimFrameAssign8Directions(50, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 5);
    AnimFrameAssign8Directions(45, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_CAST_SPELL, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);
}

void GRPDump_BranchOniOutput(){}
#define BRANCHONI_OUTPUT_BASE_IMAGE_ID 133955
void initializeImageFrameBranchOniOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BranchOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0, BRANCHONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, BRANCHONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, BRANCHONI_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     BRANCHONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void initializeTextStrings(int imgVector){
    short message1[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("스페셜 샵 들어가기"),message1);
    AppendTextSprite(imgVector,0x17E0,message1,115273);

    short message2[64];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("스페셜 샵 나가기"),message2);
    AppendTextSprite(imgVector,0x17E0,message2,115276);

    short message3[64];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("인벤토리 무적화"),message3);
    AppendTextSprite(imgVector,0x17E0,message3,115275);

    short message4[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("2개의 열쇠를 여기에 꽂으시오"),message4);
    AppendTextSprite(imgVector,0x17E0,message4,115274);
}

void GRPDump_GreenAmulet(){}
void initializeImageGreenAmulet(int imgVector){
    int *frames, *sizes, thingId=OBJ_AMULETOF_NATURE;
    int xyInc[]={0,13};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GreenAmulet)+4, thingId, xyInc, &frames,&sizes);

    AppendImageFrame(imgVector, frames[0],112960);
}

void initializeClientStuff(){
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_NATURE, "패스트힐링 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_NATURE, "잠시동안 체력회복 속도를 대폭 높혀준다");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_MANIPULATION, "운석소나기 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_MANIPULATION, "이 목걸이를 사용한 지역에 운석 소나기가 내리게 한다");
    ChangeSpriteItemNameAsString(OBJ_FEAR, "전기 팬던트");
    ChangeSpriteItemDescriptionAsString(OBJ_FEAR, "이 팬던트를 사용하면 체력회복 및 쇼크 엔첸트가 부여됩니다");
}

void InitializeResources(){
    int vec=CreateImageVector(IMAGE_VECTOR_SIZE);

    initializeTextStrings(vec);
    initializeImageFrameSoldier(vec);
    initializeImageFrameTalesweavergirl(vec);
    initializeImageFrameBranchOniOutput(vec);
    initializeImageGreenAmulet(vec);
    DoImageDataExchange(vec);
    initializeClientStuff();
}

