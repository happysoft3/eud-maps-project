
#include "libs/define.h"
#include "libs/unitutil.h"
#include "libs/unitstruct.h"
#include "libs/queueTimer.h"
#include "libs/mathlab.h"
#include "libs/objectIDdefines.h"

void BoltBugTest()
{
    Damage(OTHER,0,10,DAMAGE_TYPE_POISON);
}

void DeferredPick(int item)
{
    int owner = GetOwner(item);

    if (CurrentHealth(owner))
        Pickup(owner, item);
}

void SafetyPickup(int user, int item)
{
    SetOwner(user, item);
    PushTimerQueue(1, item, DeferredPick);
}

void onArrowAbandonded()
{
    Delete(SELF);
    if (CurrentHealth(OTHER))
    {
        float xvect=UnitAngleCos(OTHER, 11.0),yvect=UnitAngleSin(OTHER,11.0);
        int mis=CreateObjectById(OBJ_ARCHER_BOLT, GetObjectX(OTHER)+xvect,GetObjectY(OTHER)+yvect);

        LookAtObject(mis, OTHER);
        LookWithAngle(mis, GetDirection(mis)+128);
        PushObjectTo(mis, xvect,yvect);
    }
}

void PressBlueBeacon()
{
    int item=CreateObjectById(OBJ_FAN_CHAKRAM_IN_MOTION, GetObjectX(OTHER), GetObjectY(OTHER));

    UnitNoCollide(item);
    SafetyPickup(OTHER, item);
    SetUnitCallbackOnDiscard(item, onArrowAbandonded);
}

int DrawImageAt(float x, float y, int thingId)
{
    int unit = CreateObjectAt("AirshipBasketShadow", x, y);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 0x04, thingId);
    return unit;
}