
#include "subway_utils.h"
#include "libs/playerupdate.h"
#include "libs/cmdline.h"
#include "libs/bind.h"
#include "libs/printutil.h"
#include "libs/spellutil.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/format.h"
#include "libs/buff.h"

#define PLAYER_INITIAL_LOCATION 72
#define PLAYER_DEATH_FLAG 0x80000000

#define WINDBOOSTER_DISTANCE 70.0

int GetPlayer(int pIndex) //virtual
{ }

void SetPlayer(int pIndex, int user) //virtual
{ }

int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

int PlayerClassCheckFlag(int pIndex, int flags)
{
    return GetPlayerFlags(pIndex) & flags;
}

void PlayerClassSetFlag(int pIndex, int flags)
{
    SetPlayerFlags(pIndex, GetPlayerFlags(pIndex)^flags);
}

int WarAbilityTable(int aSlot, int pIndex)
{
    int *p=0x753600;
    int *computed=&p[pIndex*6];

    return computed[aSlot];
}

void WarAbilityUse(int pUnit, int aSlot, int actionFunction)
{
    int chk[160], pIndex = GetPlayerIndex(pUnit), cTime;
    int arrPic;

    if (!(pIndex >> 0x10))
    {
        arrPic = pIndex * 5 + aSlot; //EyeOf=5, harpoon=3, sneak=4, berserker=1
        cTime = WarAbilityTable(aSlot, pIndex);
        if (cTime ^ chk[arrPic])
        {
            if (!chk[arrPic])
            {
                CallFunctionWithArg(actionFunction, pUnit);
            }
            chk[arrPic] = cTime;
        }
    }
}

void skillSetWindBooster(int pUnit)
{
    PushObjectTo(pUnit, UnitAngleCos(pUnit, WINDBOOSTER_DISTANCE), UnitAngleSin(pUnit, WINDBOOSTER_DISTANCE));
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void ManaBombCancelFx(int sUnit)
{
    int caster = CreateObjectAt("ImaginaryCaster", GetObjectX(sUnit), GetObjectY(sUnit));

    CastSpellObjectObject("SPELL_MANA_BOMB", caster, caster);
    Delete(caster);
}

void onWarcrySplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 135, DAMAGE_TYPE_PLASMA);
        }
    }
}

void SkillSetWarCry(int pUnit)
{
    SplashDamageAtEx(pUnit, GetObjectX(pUnit), GetObjectY(pUnit), 200.0, onWarcrySplash);
    ManaBombCancelFx(pUnit);
}

void reportWarAbilityCooldown(int user, short abilityId, char cop)
{
    char code[]={
         0x56, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x8B, 0xF0, 0xB8, 
         0x00, 0x81, 0x4D, 0x00, 0xFF, 0x36, 0xFF, 0x76, 0x04, 0xFF, 0x76, 0x08, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5E, 0x31, 0xC0, 0xC3 };

    // int args[]={0, abilityId, UnitToPtr(user)};
    int args[]={cop, abilityId, UnitToPtr(user)};
    int *pExec=0x5c336c;
		int *pOld=pExec[0];
		pExec[0]= code;
		Unknownb8(args);
		pExec[0]=pOld;
}

void setAbilityCooldown(int user, int abilityId, int cooldown)
{
    SpellUtilSetPlayerAbilityCooldown(user, abilityId, cooldown);
    reportWarAbilityCooldown(user, abilityId, cooldown==0);
}

int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(GetPlayer(pIndex)))
        return GetPlayer(pIndex)==pUnit;
    return FALSE;
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

void PlayerClassOnShutdown(int pIndex)
{
    SetPlayer(pIndex,0);
    SetPlayerFlags(pIndex,0);
    // ResetUserRespawnMarkPos(pIndex);

    char buff[128];

    NoxSprintfString(buff, "playeronshutdown. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void PlayerClassOnDeath(int pIndex, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

int userAwardCommon(int user, int pIndex, int flag)
{
    if (PlayerClassCheckFlag(pIndex, flag))
    {
        UniPrint(user, "이미 그 기술을 배우셨어요");
        return FALSE;
    }
    PlayerClassSetFlag(pIndex,flag);
    float point[]={GetObjectX(user),GetObjectY(user)};
    GreenSparkAt(point[0],point[1]);
    Effect("WHITE_FLASH",point[0],point[1],0.0,0.0);
    PlaySoundAround(user,SOUND_AwardSpell);
    return FALSE;
}

void onAwardThreadLightly(int pIndex)
{
    if (!userAwardCommon(GetPlayer(pIndex), pIndex, PLAYER_FLAG_WINDBOOST))
        return;
}

void onAwardWarcry(int pIndex)
{
    if (!userAwardCommon(GetPlayer(pIndex), pIndex, PLAYER_FLAG_WARCRY))
        return;
}

void onAwardBerserkerCharge(int pIndex)
{
    if (!userAwardCommon(GetPlayer(pIndex), pIndex, PLAYER_FLAG_BERSERKER_CHARGE))
        return;
    setAbilityCooldown(GetPlayer(pIndex),1,0);
}

void userCooldownHandler(int pIndex, int user, int abId, int setCooltime)
{
    int cool=SpellUtilGetPlayerAbilityCooldown(user, abId);

    if (!(cool%4))
    {
        setAbilityCooldown(user, abId, setCooltime);
    }
}

void PlayerClassSkillHandler(int pIndex, int user)
{
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WINDBOOST))
    {
        if (UnitCheckEnchant(user, GetLShift(ENCHANT_SNEAK)))
        {
            skillSetWindBooster(user);
            RemoveTreadLightly(user);
        }
    }
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WARCRY))
        WarAbilityUse(user, 2, SkillSetWarCry);
}

void PlayerClassOnAlive(int pIndex, int user)
{
    if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_BERSERKER_CHARGE))
        userCooldownHandler(pIndex, user, ABILITY_ID_BERSERKER_CHARGE, 254);
    // if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_WARCRY))
    //     userCooldownHandler(pIndex, user, ABILITY_ID_WARCRY, 254);
    // if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_HARPOON))
    //     userCooldownHandler(pIndex, user, ABILITY_ID_HARPOON, 254);
    // if (!PlayerClassCheckFlag(pIndex, PLAYER_FLAG_THREADLIGHTLY))
    //     userCooldownHandler(pIndex, user, ABILITY_ID_TREAD_LIGHTLY, 254);
    PlayerClassSkillHandler(pIndex, user);
}

void PlayerClassOnLoop(int pIndex)
{
    int user = GetPlayer(pIndex);

    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (GetPlayerFlags(pIndex))
        {
            PlayerClassOnShutdown(pIndex);
            return;
        }
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

void InitServerResource(){}//virtual

void PlayerClassOnInit(int pIndex, int pUnit)
{
    SetPlayer(pIndex, pUnit);
    SetPlayerFlags(pIndex,1);    
    ChangeGold(pUnit, -GetGold(pUnit));
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    EmptyAll(pUnit);
    // ResetUserRespawnMarkPos(pIndex);

    char buff[128];

    NoxSprintfString(buff, "playeroninit. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void QueryPlayerJoinFunction(int *get, int set){
    int fn;
    if (get){
        get[0]=fn;
        return;
    }
    fn=set;
}

// void playerJoinGGOver(int user){
//     MoveObject(user,LocationX(PLAYER_GGOVER_POS),LocationY(PLAYER_GGOVER_POS));
//     Enchant(user,EnchantList(ENCHANT_FREEZE),0.0);
//     PlaySoundAround(user,SOUND_PlayerExit);
// }

void playerJoinNormal(int user){
    float x=LocationX(PLAYER_START_LOCATION_AT),y=LocationY(PLAYER_START_LOCATION_AT);
    
    MoveObject(user, x,y);
    Effect("TELEPORT", x,y, 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}

void playerJoinBoss(int user){
    short re[]={114,115,116,117,};
    short pic=re[Random(0,3)];
    float x=LocationX(pic),y=LocationY(pic);
    MoveObject(user,x,y);
    Effect("TELEPORT", x,y, 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}
void PlayerSetAllBuff(int pUnit)
{
    Enchant(pUnit, EnchantList(ENCHANT_VAMPIRISM), 0.0);
    SetUnitEnchantCopy(pUnit, GetLShift(ENCHANT_PROTECT_FROM_FIRE) | GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY) | GetLShift(ENCHANT_PROTECT_FROM_POISON)
        | GetLShift(ENCHANT_INFRAVISION) | GetLShift(ENCHANT_REFLECTIVE_SHIELD));
}

void PlayerClassOnJoin(int user, int pIndex)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
    if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_ALL_BUFF))
        PlayerSetAllBuff(user);
    SetUnitEnchantCopy(user, GetLShift(ENCHANT_ANTI_MAGIC));

    int joinFn;
    QueryPlayerJoinFunction(&joinFn, 0);
    Bind(joinFn, &user);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int pIndex)
{
    PlayerClassOnJoin(user, pIndex);
    UniPrint(user, "<<--- 해충 박멸 대작전 -----------제작. 237------<<<<");
}

void PlayerClassOnEntry(int plrUnit)
{
    if (!CurrentHealth(plrUnit))
        return;

    int pIndex = GetPlayerIndex(plrUnit);
    char initialUser=FALSE;

    if (!MaxHealth(GetPlayer(pIndex)))
        PlayerClassOnInit(pIndex, plrUnit);
    if (pIndex >= 0)
    {
        if (initialUser)
            OnPlayerDeferredJoin(plrUnit, pIndex);
        else
            PlayerClassOnJoin(plrUnit, pIndex);
        return;
    }
    PlayerClassOnEntryFailure(plrUnit);
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC));
        if (checkIsPlayerAlive(GetPlayerIndex(OTHER), GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(PLAYER_INITIAL_LOCATION), LocationY(PLAYER_INITIAL_LOCATION));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

void TeleportPlayerAll(float x, float y){
    int r=MAX_PLAYER_COUNT;
    while (--r>=0){
        if (CurrentHealth(GetPlayer(r))){
            MoveObject(GetPlayer(r),x,y);
        }
    }
}

// void PlayerSetGGOver(){
//     QueryPlayerJoinFunction(NULLPTR,playerJoinGGOver);
//     TeleportPlayerAll(LocationX(PLAYER_GGOVER_POS),LocationY(PLAYER_GGOVER_POS));
// }

void InitializePlayerSystem(){
    QueryPlayerJoinFunction(NULLPTR,playerJoinNormal);
}
