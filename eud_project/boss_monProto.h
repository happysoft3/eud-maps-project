
#include "boss_utils.h"
#include "libs/voiceList.h"
#include "libs/sound_define.h"

void queryMonVoiceData(int myId, int *get, int set){
    int data[100];

    if (get)
    {
        get[0]=data[myId];
        return;
    }
    data[myId]=set;
}

#define FANMADE_VOICE_BABY_ONI 1
#define FANMADE_VOICE_WHITEMAN 2
#define FANMADE_VOICE_SHIFF 3
#define FANMADE_VOICE_ZERGLING 4
#define FANMADE_VOICE_FUBAOPANDA 5
#define FANMADE_VOICE_ACK_MECH 6
#define FANMADE_VOICE_REDMIST 7

int SmallAlbinoSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1651261804; arr[2] = 1399811689; arr[3] = 1701079408; arr[4] = 114; 
		arr[17] = 35; arr[18] = 1; arr[19] = 80; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; arr[28] = 1097859072; arr[29] = 6; 
		arr[31] = 8; arr[32] = 6; arr[33] = 11; arr[59] = 5542784; arr[60] = 1356; 
		arr[61] = 46906624; 
	pArr = arr;
	return pArr;
}

void SmallAlbinoSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075419545;		ptr[137] = 1075419545;
	int *hpTable = ptr[139];
	hpTable[0] = 35;	hpTable[1] = 35;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SmallAlbinoSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}


int createMonsterSmallSpider(float x,float y){
    int m=CreateObjectById(OBJ_SMALL_ALBINO_SPIDER,x,y);

    SmallAlbinoSpiderSubProcess(m);
    SetUnitMass(m, 20.0);
    int vc;
    queryMonVoiceData(FANMADE_VOICE_BABY_ONI, &vc, 0);
    ChangeUnitCustomVoice(m,vc);
    return m;
}

int TalkingSkullBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1802264916; arr[1] = 1399287401; arr[2] = 1819047275; arr[17] = 70; arr[18] = 1; 
		arr[19] = 70; arr[21] = 1065353216; arr[23] = 65537; arr[24] = 1065353216; arr[26] = 4; 
		arr[37] = 1818324307; arr[38] = 1701725548; arr[39] = 1115252594; arr[40] = 7629935; arr[53] = 1128792064; 
		arr[55] = 18; arr[56] = 28; arr[60] = 2303; arr[61] = 46917632; 
	pArr = arr;
	return pArr;
}

void WoundedApprenticeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1074161254;		ptr[137] = 1074161254;
	int *hpTable = ptr[139];
	hpTable[0] = 100;	hpTable[1] = 100;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = TalkingSkullBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterDoraemon(float x,float y){
    int m=CreateObjectById(OBJ_WOUNDED_APPRENTICE,x,y);

    WoundedApprenticeSubProcess(m);
	SetUnitVoice(m,MONSTER_VOICE_Bomber);
    return m;
}

int SmallSpiderBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1818324307; arr[1] = 1768969068; arr[2] = 7497060; arr[17] = 100; arr[18] = 1; 
		arr[19] = 81; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[27] = 5; arr[28] = 1103101952; arr[29] = 8; arr[30] = 1097859072; arr[31] = 11; 
		arr[32] = 5; arr[33] = 11; arr[59] = 5542784; arr[60] = 1354; arr[61] = 46906880; 
	pArr = arr;
	return pArr;
}

void SmallSpiderSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1075545374;		ptr[137] = 1075545374;
	int *hpTable = ptr[139];
	hpTable[0] = 75;	hpTable[1] = 75;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = SmallSpiderBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

#define MON_SUBCLASS_WARCRY_STUN 0x20000

int createMonsterFanmadeBear(float x,float y){
    int m=CreateObjectById(OBJ_SMALL_SPIDER,x,y);

    SmallSpiderSubProcess(m);
    SetUnitMass(m, 30.0);
    SetUnitVoice(m, MONSTER_VOICE_Bear);
	SetUnitSubclass(m,GetUnitSubclass(m)^ MON_SUBCLASS_WARCRY_STUN);
    return m;
}

int WeirdlingBeastBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[17] = 160; 
		arr[19] = 93; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 3; 
		arr[26] = 6; arr[28] = 1106247680; arr[29] = 16; arr[31] = 4; arr[32] = 5; 
		arr[33] = 10; arr[59] = 5542784; arr[60] = 1388; arr[61] = 46915072; 
	pArr = arr;
	return pArr;
}

void WeirdlingBeastSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077055324;		ptr[137] = 1077055324;
	int *hpTable = ptr[139];
	hpTable[0] = 160;	hpTable[1] = 160;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WeirdlingBeastBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterWhiteman(float x,float y){
    int m=CreateObjectById(OBJ_WEIRDLING_BEAST,x,y);

    WeirdlingBeastSubProcess(m);
    SetUnitMass(m, 30.0);
    int vc;
    queryMonVoiceData(FANMADE_VOICE_WHITEMAN, &vc, 0);
    ChangeUnitCustomVoice(m,vc);
	if (GetUnitSubclass(m)&MON_SUBCLASS_WARCRY_STUN)
		SetUnitSubclass(m,GetUnitSubclass(m)^ MON_SUBCLASS_WARCRY_STUN);
    return m;
}

int ImpBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 7368009; arr[17] = 185; arr[18] = 1; arr[19] = 105; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[25] = 3; arr[26] = 5; arr[37] = 1399876937; 
		arr[38] = 7630696; arr[53] = 1125515264; arr[54] = 1; arr[55] = 12; arr[56] = 24; 
		arr[60] = 1328; arr[61] = 46904064; 
	pArr = arr;
	return pArr;
}

void ImpSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1078565273;		ptr[137] = 1078565273;
	int *hpTable = ptr[139];
	hpTable[0] = 185;	hpTable[1] = 185;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = ImpBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterSiff(float x,float y){
    int m=CreateObjectById(OBJ_IMP,x,y);

    ImpSubProcess(m);
    SetUnitMass(m, 30.0);
    SetUnitFlags(m,GetUnitFlags(m)^UNIT_FLAG_AIRBORNE);
    int vc;
    queryMonVoiceData(FANMADE_VOICE_SHIFF, &vc, 0);
    ChangeUnitCustomVoice(m,vc);
    return m;
}

int createMonsterMisWhitemanSiff(float x, float y){
	short fn[]={createMonsterWhiteman, createMonsterSiff,};
	int *p=&x;

	return Bind(fn[ Random(0,1) ], p);
}

int ShadeBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1684105299; arr[1] = 101; arr[17] = 220; arr[19] = 98; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 3; arr[28] = 1106247680; 
		arr[29] = 15; arr[31] = 4; arr[32] = 10; arr[33] = 20; arr[59] = 5543680; //5542784; 
		arr[60] = 1362; arr[61] = 46905088; arr[34]=25; arr[35]=20; arr[36]=100;
	pArr = arr;
	return pArr;
}

void ShadeSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077684469;		ptr[137] = 1077684469;
	int *hpTable = ptr[139];
	hpTable[0] = 220;	hpTable[1] = 220;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = ShadeBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterZergZergling(float x,float y){
    int m=CreateObjectById(OBJ_SHADE,x,y);

    ShadeSubProcess(m);
    SetUnitMass(m, 30.0);
    int vc;
    queryMonVoiceData(FANMADE_VOICE_ZERGLING, &vc, 0);
    ChangeUnitCustomVoice(m,vc);
    return m;
}

int BatBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 7627074; arr[17] = 35; arr[19] = 105; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1106247680; 
		arr[29] = 33; arr[31] = 8; arr[37] = 1399876937; arr[38] = 7630696; arr[53] = 1128792064; 
		arr[55] = 11; arr[56] = 19; arr[59] = 5542784; arr[60] = 1357; arr[61] = 46906368; 
	pArr = arr;
	return pArr;
}

void BatSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1078565273;		ptr[137] = 1078565273;
	int *hpTable = ptr[139];
	hpTable[0] = 237;	hpTable[1] = 237;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = BatBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterFubaoPanda(float x,float y){
    int m=CreateObjectById(OBJ_BAT,x,y);

    BatSubProcess(m);
    SetUnitMass(m, 25.0);
	SetUnitVoice(m,MONSTER_VOICE_Troll);
	if (GetUnitSubclass(m)&MON_SUBCLASS_WARCRY_STUN)
		SetUnitSubclass(m,GetUnitSubclass(m)^ MON_SUBCLASS_WARCRY_STUN);
    return m;
}

int createMonsterMixFubaoZergling(float x, float y){
	short fn[]={createMonsterFubaoPanda, createMonsterZergZergling,};
	int *p=&x;

	return Bind(fn[ Random(0,1) ], p);
}

int WoundedConjurerBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1130653028; arr[2] = 1969909359; arr[3] = 7497074; arr[17] = 255; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; 
		arr[26] = 4; arr[28] = 1110704128; arr[29] = 34; arr[30] = 1112014848; arr[31] = 11; 
		arr[32] = 12; arr[33] = 20; arr[59] = 5542784; arr[60] = 2271; arr[61] = 46912768; 
	pArr = arr;
	return pArr;
}

void WoundedConjurerSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 255;	hpTable[1] = 255;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedConjurerBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterBranchOni(float x,float y){
    int m=CreateObjectById(OBJ_WOUNDED_CONJURER,x,y);

    WoundedConjurerSubProcess(m);
	SetUnitVoice(m,MONSTER_VOICE_Troll);
    return m;
}

void onAckMechineAttackSplash(){
	if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 13, DAMAGE_TYPE_EXPLOSION);
			MakePoisoned(OTHER,SELF);
        }
    }
}

void onAckMechineAttack(){
	if (!GetCaller())
        return;

	SplashDamageAtEx(SELF, GetObjectX(OTHER), GetObjectY(OTHER), 46.0, onAckMechineAttackSplash);
	Effect("DAMAGE_POOF", GetObjectX(OTHER), GetObjectY(OTHER),0.0,0.0);
	PlaySoundAround(OTHER,SOUND_LOTDBarrelBreak);
}

int WolfBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1718382423; arr[17] = 275; arr[19] = 94; arr[21] = 1065353216; arr[23] = 65536; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 5; arr[27] = 5; arr[28] = 1119748096; 
		arr[29] = 15; arr[31] = 8; arr[32] = 8; arr[33] = 15; arr[59] = 5542784; 
		arr[60] = 1369; arr[61] = 46902528; 
		arr[34]=10; arr[35]=10; arr[36]=50;
	pArr = arr;
	CustomMeleeAttackCode(arr,onAckMechineAttack);
	return pArr;
}

void WolfSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077181153;		ptr[137] = 1077181153;
	int *hpTable = ptr[139];
	hpTable[0] = 275;	hpTable[1] = 275;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WolfBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterAckMechine(float x,float y){
	int m=CreateObjectById(OBJ_WOLF,x,y);

    WolfSubProcess(m);
	SetUnitMass(m, 100.0);
	int vc;
	queryMonVoiceData(FANMADE_VOICE_ACK_MECH, &vc,0);
	ChangeUnitCustomVoice(m,vc);
	SetMonsterPoisonImmune(m);
    return m;
}

int WoundedWarriorBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1853189975; arr[1] = 1466197348; arr[2] = 1769108065; arr[3] = 29295; arr[17] = 310; 
		arr[19] = 100; arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; 
		arr[28] = 1114636288; arr[29] = 25; arr[31] = 4; arr[32] = 11; arr[33] = 19; 
		arr[59] = 5542784; arr[60] = 2272; arr[61] = 46912512; 
	pArr = arr;
	return pArr;
}

void WoundedWarriorSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 310;	hpTable[1] = 310;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WoundedWarriorBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterBillyFaceAooni(float x,float y){
	int m=CreateObjectById(OBJ_WOUNDED_WARRIOR,x,y);

    WoundedWarriorSubProcess(m);
	SetUnitVoice(m,MONSTER_VOICE_Troll);
    return m;
}

void onBookExplosionSplash(){
	if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 50, DAMAGE_TYPE_EXPLOSION);
        }
    }
}

#define FLYINGBOOK_SUBUNIT 0
#define FLYINGBOOK_DURATION 1
#define FLYINGBOOK_XVECT 2
#define FLYINGBOOK_YVECT 3
#define FLYINGBOOK_OWNER 4
#define FLYINGBOOK_CASTER 5
#define FLYINGBOOK_MAX 6

void onFlyingBookProc(int *d){
	int caster=d[FLYINGBOOK_CASTER];

	if (ToInt(GetObjectX(caster))){
		int sub=d[FLYINGBOOK_SUBUNIT];

		if (IsVisibleOr(sub,caster)){
			if (--d[FLYINGBOOK_DURATION]>=0)
			{
				float *xyvect=&d[FLYINGBOOK_XVECT];
				MoveObjectVector(sub,xyvect[0],xyvect[1]);
				MoveObjectVector(caster,xyvect[0],xyvect[1]);
				PushTimerQueue(1,d,onFlyingBookProc);
				return;
			}
		}
		SplashDamageAtEx(d[FLYINGBOOK_OWNER], GetObjectX(caster),GetObjectY(caster), 50.0, onBookExplosionSplash);
		DeleteObjectTimer(CreateObjectById(OBJ_EXPLOSION, GetObjectX(caster), GetObjectY(caster)), 9);
		Delete(caster);
		Delete(sub);
	}
	FreeSmartMemEx(d);
}

void onBookExplosion(){
	if (!GetTrigger()) return;
	if (CurrentHealth(OTHER)){
		if (IsAttackedBy(OTHER,GetMaster())){
			Delete(SELF);
		}
	}
}

void onGlobglogabglabAttack(){
	if (!GetCaller())
        return;

	float xvect=UnitRatioX(OTHER,SELF, 10.0), yvect=UnitRatioY(OTHER,SELF,10.0);
	int note=DummyUnitCreateById(OBJ_RAT, GetObjectX(SELF)+xvect, GetObjectY(SELF)+yvect);
	int caster=CreateObjectById(OBJ_PLAYER_WAYPOINT,GetObjectX(note)-xvect,GetObjectY(note)-yvect);

	SetUnitFlags(note,GetUnitFlags(note)^UNIT_FLAG_NO_PUSH_CHARACTERS);
	SetCallback(note,9,onBookExplosion);
	int *d;
	AllocSmartMemEx(FLYINGBOOK_MAX*4,&d);
	d[FLYINGBOOK_SUBUNIT]=note;
	d[FLYINGBOOK_DURATION]=21;
	d[FLYINGBOOK_XVECT]=xvect;
	d[FLYINGBOOK_YVECT]=yvect;
	d[FLYINGBOOK_OWNER]=GetTrigger();
	d[FLYINGBOOK_CASTER]=caster;
	PushTimerQueue(1,d,onFlyingBookProc);
}

int TrollBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1819243092; arr[1] = 108; arr[17] = 330; arr[19] = 85; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 7; arr[27] = 4; arr[28] = 1120272384; 
		arr[29] = 40; arr[31] = 16; arr[32] = 22; arr[33] = 30; arr[59] = 5542784; 
		arr[60] = 2273; arr[61] = 46912256; 
	pArr = arr;
	CustomMeleeAttackCode(arr, onGlobglogabglabAttack);
	return pArr;
}

void TrollSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076048691;		ptr[137] = 1076048691;
	int *hpTable = ptr[139];
	hpTable[0] = 330;	hpTable[1] = 330;
	int *uec = ptr[187];
	uec[360] = 0;		uec[121] = TrollBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createMonsterGlobglogapglab(float x,float y){
	int m=CreateObjectById(OBJ_TROLL,x,y);

    TrollSubProcess(m);
    return m;
}

int createMonsterMixGlobgloAckBilly(float x, float y){
	short fn[]={createMonsterGlobglogapglab, createMonsterBillyFaceAooni,createMonsterAckMechine,};
	int *p=&x;

	return Bind(fn[ Random(0,2) ], p);
}

void onRedMistAttack(){
	if (!GetCaller())
        return;

	Effect("SENTRY_RAY",GetObjectX(OTHER),GetObjectY(OTHER),GetObjectX(SELF),GetObjectY(SELF));
	Damage(OTHER,SELF,25,DAMAGE_TYPE_ZAP_RAY);
}

int WaspBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1886609751; arr[17] = 360; arr[18] = 1; arr[19] = 120; arr[21] = 1065353216; 
		arr[23] = 65537; arr[24] = 1065353216; arr[27] = 5; arr[28] = 1120403456; arr[29] = 5; 
		arr[31] = 4; arr[59] = 5542784; arr[60] = 1331; arr[61] = 46900736; 
		arr[57]=5545472;
	pArr = arr;
	CustomMeleeAttackCode(arr,onRedMistAttack);
	return pArr;
}

void WaspSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1080452710;		ptr[137] = 1080452710;
	int *hpTable = ptr[139];
	hpTable[0] = 360;	hpTable[1] = 360;
	int *uec = ptr[187];
	uec[360] = 65537;		uec[121] = WaspBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int createRedmist(float x,float y){
	int m=CreateObjectById(OBJ_WASP,x,y);

	SetUnitMass(m, 100.0);
	int vc;
	queryMonVoiceData(FANMADE_VOICE_REDMIST, &vc,0);
	ChangeUnitCustomVoice(m,vc);
    WaspSubProcess(m);
    return m;
}

void InitializeMonProto(){
    int *vc;
    CreateVoicesetInstance(&vc,"BabyOni");
    queryMonVoiceData(FANMADE_VOICE_BABY_ONI, 0, vc);
    vc[VOICEOFFSET_ON_MOVE]=SOUND_TrollMove;
    vc[VOICEOFFSET_ON_HURT]=SOUND_UrchinFlee;
    vc[VOICEOFFSET_ON_RECOGNIZE]=SOUND_UrchinRecognize;
    vc[VOICEOFFSET_ON_ATTACKINIT]=SOUND_TowerShoot;
    vc[VOICEOFFSET_ON_DIE]=SOUND_ClothArmorBreak;

    CreateVoicesetInstance(&vc,"WhiteMan");
    queryMonVoiceData(FANMADE_VOICE_WHITEMAN, 0, vc);
    vc[VOICEOFFSET_ON_MOVE]=SOUND_WalkOnMud;
    vc[VOICEOFFSET_ON_HURT]=SOUND_ShieldRepelled;
    vc[VOICEOFFSET_ON_RECOGNIZE]=SOUND_EggBreak;
    vc[VOICEOFFSET_ON_ATTACKINIT]=SOUND_MeteorCast;
    vc[VOICEOFFSET_ON_DIE]=SOUND_MetallicBong;

    CreateVoicesetInstance(&vc,"Shiff");
    queryMonVoiceData(FANMADE_VOICE_SHIFF, 0, vc);
    vc[VOICEOFFSET_ON_MOVE]=SOUND_RunOnWater;
    vc[VOICEOFFSET_ON_HURT]=SOUND_MaidenFlee;
    vc[VOICEOFFSET_ON_RECOGNIZE]=SOUND_MaidenTalkable;
    vc[VOICEOFFSET_ON_ATTACKINIT]=SOUND_TagOff;
    vc[VOICEOFFSET_ON_DIE]=SOUND_Maiden1Die;

    CreateVoicesetInstance(&vc,"Zergring");
    queryMonVoiceData(FANMADE_VOICE_ZERGLING, 0, vc);
    vc[VOICEOFFSET_ON_MOVE]=SOUND_WalkOnStone;
    vc[VOICEOFFSET_ON_HURT]=SOUND_UrchinThrow;
    vc[VOICEOFFSET_ON_RECOGNIZE]=SOUND_EmberDemonTaunt;
    vc[VOICEOFFSET_ON_ATTACKINIT]=SOUND_HitWoodBreakable;
    vc[VOICEOFFSET_ON_DIE]=SOUND_MimicDie;

    CreateVoicesetInstance(&vc,"ACKMECH");
    queryMonVoiceData(FANMADE_VOICE_ACK_MECH, 0, vc);
    vc[VOICEOFFSET_ON_MOVE]=SOUND_MonsterGeneratorHurt;
    vc[VOICEOFFSET_ON_HURT]=SOUND_MetalWeaponBreak;
    vc[VOICEOFFSET_ON_RECOGNIZE]=SOUND_MechGolemPowerUp;
    vc[VOICEOFFSET_ON_ATTACKINIT]=SOUND_SentryRayHit;
    vc[VOICEOFFSET_ON_DIE]=SOUND_LOTDBarrelBreak;

    CreateVoicesetInstance(&vc,"Redmist");
    queryMonVoiceData(FANMADE_VOICE_REDMIST, 0, vc);
    vc[VOICEOFFSET_ON_MOVE]=SOUND_WillOWispMove;
    vc[VOICEOFFSET_ON_HURT]=SOUND_BallBounce;
    vc[VOICEOFFSET_ON_RECOGNIZE]=SOUND_WillOWispEngage;
    vc[VOICEOFFSET_ON_ATTACKINIT]=SOUND_SentryRayHitWall;
    vc[VOICEOFFSET_ON_DIE]=SOUND_FireGrate;
}
