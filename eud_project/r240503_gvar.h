
#include "libs/define.h"
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"

#define LIMIT_MON_COUNT 200

#define MAX_PLAYER_COUNT 32

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_HARPOON 32
#define PLAYER_FLAG_THREADLIGHTLY 64
#define PLAYER_FLAG_ALL_BUFF 128

#define ABILITY_ID_BERSERKER_CHARGE 	1
#define ABILITY_ID_WARCRY 			2
#define ABILITY_ID_HARPOON 			3
#define ABILITY_ID_TREAD_LIGHTLY		4
#define ABILITY_ID_EYE_OF_WOLF		5
void SetRespawnPointSettings(int **points, int *pCount){}//virtual
int GetRespawnPoint(){}//virtual
int GetMonsterCount(){} //virtual
void SetMonsterCount(int count){} //virtual

int GetMonsterTotalCount(){}//virtual
void SetMonsterTotalCount(int count){}//virtual
int GetSpawnPermanentlyFunction(){}//virtual
void SetSpawnPermanentlyFunction(int fn){}//virtual

#define PLAYER_GGOVER_POS 13

int GetMasterUnit(){
    int s;
    
    if (!s){
    s=CreateObjectById(OBJ_HECUBAH,5500.0,100.0);
    Frozen(s,TRUE);
    }
return s;
}
int GenericHash(){}//virtual

#define IMAGE_VECTOR_MAX_COUNT 1024
#define NEXT_MON_COUNT 101

#define GUI_DIALOG_MESSAGE_INVINCIBLE_ITEM 1
#define GUI_DIALOG_MESSAGE_AWARD_FAST_MOVE 2
#define GUI_DIALOG_MESSAGE_AWARD_BERSERKER 3
#define GUI_DIALOG_MESSAGE_WOLF_RUN_SWORD 4
#define GUI_DIALOG_MESSAGE_MONEY_EXCHANGER 5
#define GUI_DIALOG_MESSAGE_THUNDER_SWORD 6
#define GUI_DIALOG_MESSAGE_BACKSTEP_HAMMER 7
#define GUI_DIALOG_MESSAGE_ALL_ENCHANTMENT 8
#define GUI_DIALOG_MESSAGE_FLY_SWATTER 9
#define GUI_DIALOG_MESSAGE_BEAR_GRYLLS 10
#define GUI_DIALOG_MESSAGE_PESTICIDE 11
#define GUI_DIALOG_MESSAGE_BLUE_GIANT 12
#define GUI_DIALOG_MESSAGE_STELS_SHIP 13
#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10

void GetCreature1DataPtr(int pIndex,int**pget){}//virtual
void GetCreature2DataPtr(int pIndex,int**pget){}//virtual
void GetCreature3DataPtr(int pIndex,int**pget){}//virtual
void GetCreature4DataPtr(int pIndex,int**pget){}//virtual
void GetCreature5DataPtr(int pIndex,int**pget){}//virtual


