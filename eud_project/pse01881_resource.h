
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"
#include "libs/abilitydb.h"

void initializeMapText(int imgVector){
    short message1[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("마녀의 성"),message1);
    AppendTextSprite(imgVector,0x7e1,message1,115273);

    short message2[128];
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("최근 저장된 위치로 이동"),message2);
    AppendTextSprite(imgVector,0x7e1,message2,115276);

    // short message3[128];
    // NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("-평촌역 터널-"),message3);
    // AppendTextSprite(imgVector,0x7e1,message3,115275);

    // short message4[128];
    // NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("-현질하는 곳(특수 상점)-"),message4);
    // AppendTextSprite(imgVector,0x7e1,message4,115274);
}

void ImageDrinkTrader() { }
void ImageDrinkTrader2() { }
void Image03(){}

void InitializeResources(){
    int vec=CreateImageVector(2048);

    initializeMapText(vec);
    AppendImageFrame(vec, GetScrCodeField(ImageDrinkTrader) + 4, 132289);
    AppendImageFrame(vec, GetScrCodeField(ImageDrinkTrader2) + 4, 132290);
    AppendImageFrame(vec, GetScrCodeField(Image03) + 4, 132048);
    AbilityDbChangeCastSound(3, 169);
    DoImageDataExchange(vec);
}

