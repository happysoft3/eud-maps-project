
#include "subway_utils.h"
#include "libs/weapon_effect.h"
#include "libs/itemproperty.h"
#include "libs/hash.h"
#include "libs/fxeffect.h"
#include "libs/queueTimer.h"
#include "libs/sound_define.h"
#include "libs/buff.h"

int getUserWeaponFxHash(){
    int hash;
    if (!hash)HashCreateInstance(&hash);
    return hash;
}

void DeferredPickupUserWeapon(int weapon)
{
    if (IsObjectOn(weapon))
    {
        RegistItemPickupCallback(weapon, OnPickupUserWeapon);
        return;
    }
    int del;

    HashGet(getUserWeaponFxHash(), weapon, &del, TRUE);
}

void OnPickupUserWeapon()
{
    int *pMem;

    FrameTimerWithArg(1, GetTrigger(), DeferredPickupUserWeapon);
    if (HashGet(getUserWeaponFxHash(), GetTrigger(), &pMem, FALSE))
    {
        Delete(pMem[0]);
        Effect("CYAN_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void DeferredDiscardUserWeapon(int weapon)
{
    if (!MaxHealth(weapon))
        return;

    int *pMem;
    float xy[]={GetObjectX(weapon), GetObjectY(weapon)};
    if (HashGet(getUserWeaponFxHash(), weapon, &pMem, FALSE))
    {
        pMem[0]=CreateObjectById(OBJ_BLUE_SUMMONS, xy[0], xy[1]);
        PlaySoundAround(weapon, SOUND_BurnCast);
        Effect("SPARK_EXPLOSION", xy[0], xy[1], 0.0, 0.0);
    }    
}

void OnDiscardUserWeapon()
{
    PushTimerQueue(1, GetTrigger(), DeferredDiscardUserWeapon);
}

void CreateUserWeaponFX(int weapon)
{
    int *pMem;

    AllocSmartMemEx(4, &pMem);
    pMem[0]=CreateObjectById(OBJ_BLUE_SUMMONS, GetObjectX(weapon), GetObjectY(weapon));
    HashPushback(getUserWeaponFxHash(), weapon, pMem);
    RegistItemPickupCallback(weapon, OnPickupUserWeapon);
    SetUnitCallbackOnDiscardBypass(weapon, OnDiscardUserWeapon);
}

#define SPECIAL_WEAPON_WOLF_WALKING_DAMAGE 180

void wolfOnWalking(int wolf)
{
    int dur;

    if (IsVisibleTo(wolf+1, wolf))
    {
        dur = GetDirection(wolf+1);
        if (dur)
        {
            PushTimerQueue(1, wolf, wolfOnWalking);
            LookWithAngle(wolf+1, dur-1);
            MoveObjectVector(wolf, UnitAngleCos(wolf, 14.0), UnitAngleSin(wolf, 14.0));
            if (dur&1)
                Walk(wolf, GetObjectX(wolf),GetObjectY(wolf));
            return;
        }
    }
    Delete(wolf++);
    Delete(wolf++);
}

void onWolfSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (!(GetUnitClass(OTHER) & (UNIT_CLASS_MONSTER|UNIT_CLASS_PLAYER)))
            return;

        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER))
                Damage(OTHER, SELF, SPECIAL_WEAPON_WOLF_WALKING_DAMAGE, DAMAGE_TYPE_PLASMA);
        }
    }
}

 void wolfOnCollide()
{
    if (!MaxHealth(SELF))
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(SELF, OTHER))
        {
            float xpos =GetObjectX(SELF),ypos=GetObjectY(SELF);
            GreenExplosion(xpos,ypos);
            DeleteObjectTimer(CreateObjectAt("ForceOfNatureCharge", xpos,ypos), 48);
            SplashDamageAtEx(GetOwner(SELF), xpos,ypos, 160.0, onWolfSplash);
            PlaySoundAround(SELF, SOUND_HecubahDieFrame439);
            Delete(SELF);
        }
    }
}

 void startWolfWalking()
{
    int wolf=CreateObjectAt("WhiteWolf", GetObjectX(OTHER) + UnitAngleCos(OTHER, 13.0), GetObjectY(OTHER) + UnitAngleSin(OTHER, 13.0));

    PushTimerQueue(1, wolf, wolfOnWalking);
    LookWithAngle( CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER), GetObjectY(OTHER)), 42);
    SetOwner(OTHER, wolf);
    SetUnitFlags(wolf, GetUnitFlags(wolf) ^ UNIT_FLAG_NO_COLLIDE_OWNER);
    LookWithAngle(wolf, GetDirection(OTHER));
    Frozen(wolf, TRUE);
    SetCallback(wolf, 9, wolfOnCollide);
    GreenSparkAt(GetObjectX(wolf), GetObjectY(wolf));
}

#define SPECIAL_WEAPON_PROPERTY 0
#define SPECIAL_WEAPON_FXCODE 1
#define SPECIAL_WEAPON_EXECCODE 2
#define SPECIAL_WEAPON_MAX 3
int CreateWolfSword(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_MetalHitEarth, &property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(startWolfWalking, &property[SPECIAL_WEAPON_EXECCODE]);
    // SpecialWeaponPropertySetFXCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_GREAT_SWORD,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            int hash=GetUnit1C(SELF);

            if (hash)
            {
                if (HashGet(hash,GetCaller(),NULLPTR,FALSE))
                    return;
                HashPushback(hash,GetCaller(),TRUE);
                Damage(OTHER, owner, 200, DAMAGE_TYPE_ELECTRIC);
                Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            }
        }
    }
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 17.0), yvect = UnitAngleSin(OTHER, 17.0);
    int arr[30], u=0,hash;

    arr[u] = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(OTHER)+xvect,GetObjectY(OTHER)+yvect);
    HashCreateInstance(&hash);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateById(OBJ_DEMON, GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetUnitFlags(arr[u],GetUnitFlags(arr[u])^UNIT_FLAG_NO_PUSH_CHARACTERS);
        SetUnit1C(arr[u],hash);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    DrawYellowLightningFx(GetObjectX(arr[0]), GetObjectY(arr[0]), GetObjectX(arr[u-1]), GetObjectY(arr[u-1]), 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}
int CreateThunderSword(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_YELLOW_SPARKS, SOUND_MetalHitEarth, &property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &property[SPECIAL_WEAPON_EXECCODE]);
    // SpecialWeaponPropertySetFXCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_GREAT_SWORD,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_stun1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}

void onBackstepHammerSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
            Damage(OTHER, SELF, 200, DAMAGE_TYPE_PLASMA);
    }
}

#define BACKSTEP_DISTANCE 100.0
void earthQuakeAxeTriggered(){
    float backVectX = UnitAngleCos(OTHER, -BACKSTEP_DISTANCE), backVectY = UnitAngleSin(OTHER, -BACKSTEP_DISTANCE);
    SplashDamageAtEx(OTHER, GetObjectX(OTHER), GetObjectY(OTHER), 180.0, onBackstepHammerSplash);
    PushObjectTo(OTHER, backVectX, backVectY);
    DeleteObjectTimer(CreateObjectById(OBJ_FORCE_OF_NATURE_CHARGE, GetObjectX(OTHER), GetObjectY(OTHER)), 18);
    PlaySoundAround(OTHER, SOUND_FireballExplode);
    Effect("JIGGLE", GetObjectX(OTHER), GetObjectY(OTHER), 33.0, 0.0);
}

int CreateBackstepAxe(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    // SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_SPARK_EXPLOSION, SOUND_MetalHitEarth, &property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(earthQuakeAxeTriggered, &property[SPECIAL_WEAPON_EXECCODE]);
    // SpecialWeaponPropertySetFXCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_FXCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_BATTLE_AXE,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_vampirism4, ITEM_PROPERTY_lightning4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}

void onIceSplas()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            if (IsVisibleOr(SELF, OTHER)){
                Enchant(OTHER,EnchantList(ENCHANT_FREEZE),1.0);
                Damage(OTHER, SELF, 225, DAMAGE_TYPE_PLASMA);
            }
        }
    }
}
void OnIcecrystalCollide()
{
    while (TRUE)
    {
        if (!GetCaller())
            1;
        else if (CurrentHealth(OTHER))
        {
            int owner = GetOwner(GetTrigger()+1);

            if (!IsAttackedBy(OTHER, owner))
                break;

            SplashDamageAtEx(owner, GetObjectX(OTHER),GetObjectY(OTHER),81.0,onIceSplas );
        }
        WispDestroyFX(GetObjectX(SELF), GetObjectY(SELF));
        Delete(GetTrigger() + 1);
        Delete(SELF);
        break;
    }
}

int CreateIceCrystal(int owner, float gap, int lifetime)
{
    int cry=CreateObjectAt("GameBall", GetObjectX(owner) + UnitAngleCos(owner, gap), GetObjectY(owner) + UnitAngleSin(owner, gap));

    SetOwner(owner, CreateObjectAt("ImaginaryCaster", GetObjectX(cry), GetObjectY(cry)));
    SetUnitCallbackOnCollide(cry, OnIcecrystalCollide);
    DeleteObjectTimer(cry, lifetime);
    DeleteObjectTimer(cry+1, lifetime + 15);
    return cry;
}

void CUserWeaponShootIceCrystal()
{
    PushObject(CreateIceCrystal(OTHER, 19.0, 75), 22.0, GetObjectX(OTHER), GetObjectY(OTHER));
    WispDestroyFX(GetObjectX(OTHER), GetObjectY(OTHER));
    PlaySoundAround(OTHER, SOUND_LightningWand);
    PlaySoundAround(OTHER, SOUND_ElevLOTDDown);
}

int PlacingIceCrystalAxe(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(CUserWeaponShootIceCrystal, &property[SPECIAL_WEAPON_EXECCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_BATTLE_AXE,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_venom4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}
