
#include "libs/define.h"
#include "libs/stringutil.h"
#include "libs/memutil.h"
#include "libs/imageutil.h"

int AllocIntroData(char *src, int bConv)
{
	int length=StringUtilGetLength(src);

	if (length<0)
		return 0;
	if (length>1024)
		return 0;
	bConv = bConv!=FALSE;

	int c, size=length * (bConv+1);
	AllocSmartMemEx(size+4,&c);
	int *ptr=c;

	if (bConv)
		NoxUtf8ToUnicode(src, ptr);
	else
	{
		StringUtilCopy(src, ptr);
	}
	return ptr;
}

#define INTRO_DATA_IMAGE 0
#define INTRO_DATA_INTRO 1
#define INTRO_DATA_BRIEF 2
#define INTRO_DATA_ALWAYS_ONE 3
int CreateChapterIntro(int imgId, string voiceName, string intro)
{
	int *pBrief = AllocIntroData(StringUtilGetScriptStringPtr(voiceName), FALSE);
	int *pIntro = AllocIntroData(StringUtilGetScriptStringPtr(intro), TRUE);

	if (!pBrief)
	{
		if (pIntro)
			FreeSmartMemEx(pIntro);
		return 0;
	}
	if (!pIntro)
	{
		if (pBrief)
			FreeSmartMemEx(pBrief);
		return 0;
	}
	int* ptr=0x69fb98;
	int pImage;
	ImageUtilGetPtrFromID(imgId, &pImage);
	ptr[INTRO_DATA_IMAGE]=pImage;
	ptr[INTRO_DATA_BRIEF]=pBrief;
	ptr[INTRO_DATA_INTRO]=pIntro;
	ptr[INTRO_DATA_ALWAYS_ONE]=1;
	return 55;
}

void MapInitialize()
{
	int introData = CreateChapterIntro(14611, "f7hec01k", "제 9999장이라네~~\n\n\n\n\n재밌당~~!");

	StartupScreen(introData);
	// ChangeIntroText("제99장 - 팔괘");
}


