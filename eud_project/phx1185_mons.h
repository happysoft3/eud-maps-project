
#include "phx1185_gvar.h"
#include "phx1185_utils.h"
#include "libs/waypoint.h"
#include "libs/cmdline.h"
#include "libs/fxeffect.h"
#include "libs/sound_define.h"
#include "libs/bind.h"

int OnMonsterMoving(int mon)
{
    int sub=mon+1;
    if (DistanceUnitToUnit(mon, sub)>46.0)
    {
        MoveObjectVector(mon,UnitRatioX(sub,mon,3.0),0.0);
        return TRUE;
    }
    return FALSE;
}

void OnMonsterAiLoop(int mon)
{
    if (MaxHealth(mon))
    {
        int sub=mon+1;
        int duration=GetDirection(sub);

        if (duration)
        {
            if (OnMonsterMoving(mon))
            {
                PushTimerQueue(1, mon, OnMonsterAiLoop);
                LookWithAngle(sub, duration-1);
                return;
            }
        }
        TeleportLocation(1, GetObjectX(mon) + RandomFloat(-400.0, 400.0), GetObjectY(mon)+23.0);
        int wall = CheckBottom(1);
        if (wall)
        {
            float point[2], xpos = GetObjectX(mon);
            WallCoorToUnitCoor(wall, point);
            LookWithAngle(mon, (xpos>LocationX(1))*128 );
            MoveObject(mon, xpos, point[1]-23.0);
            MoveObject(sub, LocationX(1),LocationY(1));
            LookWithAngle(sub, Random(10, 90));
            PushTimerQueue(Random(15, 90), mon, OnMonsterAiLoop);
            return;
        }
        PushTimerQueue(3, mon, OnMonsterAiLoop);
        return;
    }
    if (ToInt(GetObjectX(mon+1)))
        Delete(mon+1);
}

void RemoveMonster(int mon)
{
    if (!MaxHealth(mon))
        return;

    Delete(mon);
    Delete(mon+1);
}

int LoadReusableMonster(short unitTy, float x, float y)
{
    int mon = DummyUnitCreateById(unitTy, x,y);

    ObjectOff( CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, x,y) );
    PushTimerQueue(1, mon, OnMonsterAiLoop);
    SetUnitFlags(mon, GetUnitFlags(mon)^UNIT_FLAG_NO_PUSH_CHARACTERS);
    SetOwner(GetMasterUnit(), mon);

    int hp=GetMonsterInitHP(unitTy%97);

    if (!hp)
        hp=1;
    SetUnit1C(mon, hp); //hp
    return mon;
}

void RespawnMonster(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int duration=GetDirection(sub);

        if (duration)
        {
            PushTimerQueue(2, sub, RespawnMonster);
            LookWithAngle(sub, duration-1);
            return;
        }
        LoadReusableMonster(GetUnit1C(sub), GetObjectX(sub),GetObjectY(sub));
        Delete(sub);
    }
}

void MonsterCustomDeathProto(int fn, short thingId, int victim, int resp)
{
    Bind(fn, &fn+4);
}

#define DROP_GOLD_MAX_RATE 105
void monsterDropGold(int mob, int credit, int rate, int user)
{
    int hash, pIndex=GetPlayerIndex(user);

    if (pIndex>=0)
    {
        HashCreateInstance(&hash);
        HashPushback(hash, EFFECT_KEY_DROP_GOLD_RATE, &rate);
        FetchWeaponEffect(GetPlayerEquipment(pIndex), hash);
    }
    if (GetPercentRate(rate, DROP_GOLD_MAX_RATE))
        CreateMeso(GetObjectX(mob), GetObjectY(mob)-7.0, credit);
}

void MonsterDie(int victim, int user)
{
    int thingId=GetUnitThingID(victim);
    int resp=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(victim),GetObjectY(victim));

    // DestroyChat(victim); //이거 쓰면 안될것 같음...
    // UniChatMessage(victim, "  ", 3);
    // MonsterReportHitpoints(victim, 0, GetMonsterInitHP(thingId%97), 15);
    PushTimerQueue(1, resp, RespawnMonster);
    SetUnit1C(resp,GetUnitThingID(victim));
    LookWithAngle(resp,255);
    int deathFn=GetMonsterDeathFunction(thingId), credit=GetMonsterCredit(thingId%97);

    while (TRUE)
    {
        if (deathFn)
        {
            MonsterCustomDeathProto(deathFn, thingId, victim, resp);
            break;
        }
        if (credit==0)
            ++credit;
        monsterDropGold(victim, credit, 1, user);
        ChangeGold(user, credit);
        TryUpdatePlayerGold(user);
        break;
    }
    Delete(victim);
    Delete(victim+1);
}

void HitMonsterByUser(int victim, int user, int damageAmount)
{
    int hp=GetUnit1C(victim);
    int monId=GetUnitThingID(victim)%97;
    // char buff[128];

    // NoxSprintfString(buff, "이 괴물의 체력은 %d 입니다", &hp, 1);
    // NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_LIME);
    if (hp)
    {
        int rAmount = damageAmount - GetMonsterArmor(monId);

        if (rAmount <= 0)
            return;

        if ((hp-rAmount) < 0)
            rAmount = hp;

        hp-=rAmount;
        ReportHealthChangedPrimary(victim, rAmount);
        // ReportHealthChanged(victim, -rAmount);
        if (!hp)
        {
            MonsterDie(victim, user);
            return;
        }
        SetUnit1C(victim, hp);
        // MonsterReportHitpoints(victim, hp, GetMonsterInitHP(monId), 90);
        MonsterReportHitpointsEx(victim, hp, GetMonsterInitHP(monId%97), 120);
        PlaySoundAround(victim, SOUND_HitEarthBreakable);
    }
}

int showRemainTime(int unit, int seconds)
{
    if (seconds<0)
        return FALSE;
    
    char buff[96];

    NoxSprintfString(buff, "보스 남은시간 %d 초 입니다", &seconds, 1);
    UniChatMessage(unit, ReadStringAddressEx(buff), 60);
    return TRUE;
}

void onTimeIsOut(int unit, int boss)
{
    RemoveMonster(boss);
    UniChatMessage(unit, "타임아웃 입니다", 90);
}

#define BOSS_COUNTER_UNIT 0
#define BOSS_COUNTER_SECONDS 1
#define BOSS_COUNTER_BOSS 2
#define BOSS_COUNTER_COUNT_PTR 3
#define BOSS_COUNTER_MAX 4

void onClearBoss(int unit, int *pData)
{
    int *ptr = pData[BOSS_COUNTER_COUNT_PTR];
    ++ptr[0];
    char locations[]={136,137,138,139,140,141};
    int u = sizeof(locations);

    while (--u>=0)
        CreateMeso(LocationX(locations[u]), LocationY(locations[u]), 15);
    UniChatMessage(unit, "보스 몬스터를 처치 하셨습니다. 비록 적지만, 보상금을 드리겠습니다", 90);
}


void onBossCountTimer(int *pData)
{
    int unit=pData[BOSS_COUNTER_UNIT];

    if (ToInt(GetObjectX(unit)))
    {
        if (showRemainTime(unit, --pData[BOSS_COUNTER_SECONDS]) )
        {
            if (MaxHealth(pData[BOSS_COUNTER_BOSS]))
            {
                PushTimerQueue(30, pData, onBossCountTimer);
                return;
            }
            else
                onClearBoss(unit, pData);
        }
        else
        {
            onTimeIsOut(unit, pData[BOSS_COUNTER_BOSS]);
        }
        
    }
    FreeSmartMemEx(pData);
}

int getBossMonsterFromTable(short *ty, int n)
{
    short *p;
    int length;

    if (!p)
    {
        short table[]={
            OBJ_LICH,OBJ_FLYING_GOLEM,OBJ_WIZARD_GREEN,OBJ_SHADE,
            OBJ_DEMON,OBJ_OGRE_WARLORD,OBJ_SCORPION,OBJ_BEHOLDER,
            OBJ_TROLL,
        };
        length=sizeof(table);
        p=table;
    }
    if (n<length)
    {
        ty[0]=p[n];
        return TRUE;
    }
    return FALSE;
}

int SummonBossMonster(int *pMob, int counterUnit, int *n)
{
    short bossTy;

    if (!getBossMonsterFromTable(&bossTy, n[0]))
        return FALSE;

    int mon = LoadReusableMonster(bossTy, RandomFloat(LocationX(93),LocationX(94)), LocationY(94)), c;

    AllocSmartMemEx(BOSS_COUNTER_MAX*4, &c);
    int *pData=c;

    pData[BOSS_COUNTER_UNIT]=counterUnit;
    pData[BOSS_COUNTER_SECONDS]=120;
    pData[BOSS_COUNTER_BOSS] = mon;
    pData[BOSS_COUNTER_COUNT_PTR] = n;
    PushTimerQueue(30, pData, onBossCountTimer);
    pMob[0]=mon;
    return TRUE;
}

void bossCredit(int sub, int credit)
{
    int pIndex= MAX_PLAYER_COUNT, user;
    char message[128];

    NoxSprintfString(message, "보스 처치 보상금을 획득했습니다 (+%d 골드)", &credit, 1);

    while (--pIndex>=0)
    {
        user=GetPlayer(pIndex);

        if (CurrentHealth(user))
        {
            if (IsVisibleTo(sub, GetPlayerAvata(pIndex)))
            {
                ChangeGold(user, credit);
                TryUpdatePlayerGold(user);
                UniPrint(user, ReadStringAddressEx(message));
            }
        }
    }
    Delete(sub);
}

void onBossDeath1(short thingId, int victim, int resp)
{
    int credit = GetMonsterCredit(thingId%97);

    if (credit==0)
        ++credit;
    bossCredit(CreateObjectById(OBJ_RED_POTION, GetObjectX(victim), GetObjectY(victim)), credit);
    Delete(resp);
}

void initBossMonster(short *pDeath, short *pHp, short *pCredit, short* pAmmo)
{
    int 
    key = OBJ_LICH%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=200;
    pAmmo[key]=8;
    pCredit[key]=100;

    key= OBJ_FLYING_GOLEM%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=350;
    pAmmo[key]=13;
    pCredit[key]=150;

    key=OBJ_WIZARD_GREEN%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=630;
    pAmmo[key]=22;
    pCredit[key]=225;

    key=OBJ_SHADE%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=973;
    pAmmo[key]=34;
    pCredit[key]=340;

    key=OBJ_DEMON%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=1438;
    pAmmo[key]=44;
    pCredit[key]=390;

    key=OBJ_OGRE_WARLORD%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=2020;
    pAmmo[key]=55;
    pCredit[key]=450;

    key=OBJ_SCORPION%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=2640;
    pAmmo[key]=71;
    pCredit[key]=520;

    key=OBJ_BEHOLDER%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=3497;
    pAmmo[key]=84;
    pCredit[key]=770;

    key=OBJ_TROLL%97;
    pDeath[key]=onBossDeath1;
    pHp[key]=5098;
    pAmmo[key]=100;
    pCredit[key]=900;
}

void placingDungeon1(short ty, short loc1, short loc2, int count)
{
    while (--count>=0)
        LoadReusableMonster(ty, RandomFloat(LocationX(loc1),LocationX(loc2)), LocationY(loc1));
}

void registMonsterInfo(short *pHp, short *pCredit, short* pAmmo, short thingId, short hp, short credit, short ammo)
{
    int idx=thingId%97;
    pHp[idx]=hp;
    pCredit[idx]=credit;
    pAmmo[idx]=ammo;
}

void InitialPlaceMonsters()
{
    short monHp[97];
    short monCredit[97];
    short monAmmo[97];
    short monDeath[97];

    initBossMonster(monDeath, monHp, monCredit, monAmmo);
    SetMonsterInfoPtr(monHp, monCredit, monAmmo, monDeath);

    //Dungeon Setting - MAINTAIN
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_URCHIN, 5, 1, 0);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_SMALL_ALBINO_SPIDER, 28, 3, 1);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_SMALL_SPIDER, 60, 11, 3);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_GIANT_LEECH, 145, 21, 8);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_ARCHER, 208, 37, 15);

    placingDungeon1(OBJ_URCHIN, 24, 26, 10);
    placingDungeon1(OBJ_SMALL_ALBINO_SPIDER, 27, 28, 10);
    placingDungeon1(OBJ_SMALL_SPIDER, 29, 30, 10);
    placingDungeon1(OBJ_GIANT_LEECH, 31, 32, 10);
    placingDungeon1(OBJ_ARCHER, 33, 34, 10);

    //Dungeon Setting - SECONDARY
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_SPIDER, 220, 42, 21);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_ALBINO_SPIDER, 245, 48, 30);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_WOLF, 260, 59, 39);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_BLACK_WOLF, 293, 80, 51);
    registMonsterInfo(monHp, monCredit, monAmmo, OBJ_WIZARD, 329, 101, 63);

    placingDungeon1(OBJ_SPIDER, 102, 103, 8);
    placingDungeon1(OBJ_ALBINO_SPIDER, 104, 105, 8);
    placingDungeon1(OBJ_WOLF, 106, 107, 8);
    placingDungeon1(OBJ_BLACK_WOLF, 108, 109, 8);
    placingDungeon1(OBJ_WIZARD, 110, 111, 8);
}
