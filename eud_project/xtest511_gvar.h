
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"

void QueryGGOver(int set,int *get){
    int ggover;
    if (get){
        get[0]=ggover;
        return;
    }
    ggover=set;
}

#define DUNGEON_COUNT 6

void GlobalHeadNode(int set, int *get)
{
    int node;
    if (get)
    {
        get[0]=node;
        return;
    }
    if (set) node=set;
}

int GetSafeZone(){
    int z;

    if (!z)
        z=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetWaypointX(12), GetWaypointY(12));
    return z;
}

void GetXTraCoin(int set, int *get)
{
    int coin;
    if (get) {
        get[0]=coin;
    return;
    }
    coin=set;
}
