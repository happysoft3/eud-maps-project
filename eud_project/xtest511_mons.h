
#include "xtest511_sub.h"
#include "xtest511_reward.h"
#include "libs/bind.h"

void FieldMonsterHurt()
{
    int enemy;

    if (GetCaller())
    {
        enemy = GetUnitParent(OTHER);
        if (CurrentHealth(enemy))
            Attack(SELF, enemy);
    }
    else
    {
        if (IsPoisonedUnit(SELF))
        {
            Damage(SELF, 0, IsPoisonedUnit(SELF) * 2, 5);
            DeleteObjectTimer(CreateObjectAt("OldPuff", GetObjectX(SELF), GetObjectY(SELF)), 15);
        }
    }
}

void DeferredSpawnSpider(int unit)
{
    if (!IsObjectOn(unit))
        return;

    float xpos = GetObjectX(unit), ypos = GetObjectY(unit);

    SetCallback(CreateObjectAt("SmallSpider", xpos, ypos), 5, UnitDeathHandler);
    SetCallback(CreateObjectAt("SmallSpider", xpos, ypos), 5, UnitDeathHandler);
    SetCallback(CreateObjectAt("SmallSpider", xpos, ypos), 5, UnitDeathHandler);
    Delete(unit);
}

void BlackSpiderDeathHandler()
{
    float xpos = GetObjectX(SELF), ypos = GetObjectY(SELF);

    PlaySoundAround(SELF, SOUND_BeholderDie);
    DeleteObjectTimer(CreateObjectAt("BigSmoke", xpos, ypos), 9);
    DeleteObjectTimer(CreateObjectAt("WaterBarrelBreaking", xpos, ypos), 9);
    FrameTimerWithArg(25, CreateObjectAt("InvisibleLightBlueLow", xpos, ypos), DeferredSpawnSpider);
    DungeonUnitDeathHandler();
}

void PoisonZombieDeathHandler()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("BurnCast", 1);
    Effect("SPARK_EXPLOSION", LocationX(1), LocationY(1), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("MediumFlame", 1), 90);
    DeleteObjectTimer(CreateObject("ReleasedSoul", 1), 30);
    DungeonUnitDeathHandler();
}

///////////////////////////////////////////

int AirshipCaptainBinTable()
{
	int arr[62], *link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
        link = arr;
	}
	return link;
}

int WizardRedBinTable()
{
	int arr[62], *link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 20; 
		arr[32] = 12; arr[33] = 20;
		arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; 
		arr[40] = 1852140903; arr[41] = 116; arr[42] = 0;
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 8; arr[32] = 8; arr[33] = 16; arr[57] = 5548112; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

int HecubahBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 6840674; arr[17] = 600; arr[19] = 100; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1092616192; 
		arr[29] = 50; arr[31] = 11; arr[57] = 5548288; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 20; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; arr[29] = 50; 
		arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; arr[57] = 5548288; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

int BomberGreenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339074; arr[1] = 1917284965; arr[2] = 7234917;
		arr[17] = 20; arr[19] = 80; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[31] = 4; arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 3; arr[56] = 6; arr[57] = 5548112; arr[58] = 5545344; arr[59] = 5543344;
        link = arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 85; arr[18] = 25; arr[19] = 90; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1769236816; arr[38] = 1181513062; arr[39] = 1650815593; arr[40] = 7105633;
		arr[53] = 1142292480; arr[55] = 9; arr[56] = 12; arr[58] = 5545472;
        link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1065688760; 
		arr[25] = 0; arr[26] = 0; arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; arr[58] = 5546320; arr[59] = 5542784; 
        link = arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 80; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[27] = 0; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680;
        link = arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 4; arr[24] = 1069547520; 
		arr[25] = 0; arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[30] = 0; arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360;
        link = arr;
	}
	return link;
}

int Bear2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1918985538; arr[1] = 50;
		arr[15] = 0; arr[16] = 20000; arr[17] = 90; arr[18] = 100; arr[19] = 40; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 65545; arr[24] = 1067450368; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 50; 
		arr[30] = 1103626240; arr[31] = 2; arr[32] = 20; arr[33] = 30; arr[58] = 5547856; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

int NecromancerBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919116622; arr[1] = 1851878767; arr[2] = 7497059; arr[17] = 350; arr[19] = 93; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1065353216; arr[25] = 1; arr[26] = 2; 
		arr[28] = 1103626240; arr[29] = 80; arr[30] = 1092616192; arr[31] = 11; arr[32] = 7; 
		arr[33] = 15; arr[34] = 1; arr[35] = 2; arr[36] = 30; arr[59] = 5542784;
        link = arr;
	}
	return link;
}

void NecromancerSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077055324);
		SetMemory(ptr + 0x224, 1077055324);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 32768);
		SetMemory(GetMemory(ptr + 0x22c), 350);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 350);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, NecromancerBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void HecubahSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 0);
		SetMemory(GetMemory(ptr + 0x22c), 600);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 600);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, HecubahBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

void UnitDeathHandler()
{
    CreateRandomItemNormalCommon(CreateObjectById(OBJ_MOVER, GetObjectX(SELF), GetObjectY(SELF)));
    DeleteObjectTimer(SELF, 90);
}

void DungeonUnitDeathHandler()
{
    CreateRandomItemCommon(CreateObjectById(OBJ_MOVER, GetObjectX(SELF), GetObjectY(SELF)));
    DeleteObjectTimer(SELF, 90);
}

void WeirdlingDie()
{
    CreateObjectAt("BreakingSoup", GetObjectX(SELF), GetObjectY(SELF));
    DungeonUnitDeathHandler();
}

int SpawnUrchin(int ptr)
{
    int unit = CreateObjectAt("Urchin", GetObjectX(ptr), GetObjectY(ptr));

    UnitZeroFleeRange(unit);
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 64);
    SetCallback(unit, 5, UnitDeathHandler);
    return unit;
}

int SpawnBat(int ptr)
{
    int unit = CreateObjectAt("Bat", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 64);
    SetCallback(unit, 5, UnitDeathHandler);
    return unit;
}

int SpawnSmallWhiteSpider(int ptr)
{
    int unit = CreateObjectAt("SmallAlbinoSpider", GetObjectX(ptr), GetObjectY(ptr));

    Enchant(unit, "ENCHANT_SLOWED", 0.0);
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 74);
    SetCallback(unit, 5, UnitDeathHandler);
    return unit;
}

int SpawnLeech(int ptr)
{
    int unit = CreateObjectAt("GiantLeech", GetObjectX(ptr), GetObjectY(ptr));

    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 120);
    SetCallback(unit, 5, UnitDeathHandler);
    return unit;
}

int SpawnSwordman(int ptr)
{
    int unit = CreateObjectAt("Swordsman", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, 180);
    SetCallback(unit, 5, UnitDeathHandler);
    return unit;
}

int SpawnArcher(int ptr)
{
    int unit = CreateObjectAt("Archer", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 98);
    RetreatLevel(unit, 0.0);
    SetCallback(unit, 5, UnitDeathHandler);
    return unit;
}

int SpawnEmberDemon(int ptr)
{
    int unit = CreateObjectAt("EmberDemon", GetObjectX(ptr), GetObjectY(ptr));
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 170);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnGargoyle(int ptr)
{
    int unit = CreateObjectAt("EvilCherub", GetObjectX(ptr), GetObjectY(ptr));
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 98);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnBlackSpider(int ptr)
{
    int unit = CreateObjectAt("BlackWidow", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, BlackWidowBinTable());
    SetUnitVoice(unit, 19);
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 275);
    SetCallback(unit, 5, BlackSpiderDeathHandler);
    return unit;
}

int SpawnWolf(int ptr)
{
    int unit = CreateObjectAt("Wolf", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, 135);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnBlackWolf(int ptr)
{
    int unit = CreateObjectAt("BlackWolf", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, 225);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnOgre(int ptr)
{
    int unit = CreateObjectAt("OgreBrute", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, 295);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnOgreAxe(int ptr)
{
    int unit = CreateObjectAt("GruntAxe", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, 225);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnDarkBear(int ptr)
{
    int unit = CreateObjectAt("BlackBear", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, 306);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnBrownBear(int ptr)
{
    int unit = CreateObjectAt("Bear", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    RetreatLevel(unit, 0.0);
    SetUnitMaxHealth(unit, 400);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnSkeleton(int ptr)
{
    int unit = CreateObjectAt("Skeleton", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 250);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnSkeletonLord(int ptr)
{
    int unit = CreateObjectAt("SkeletonLord", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 295);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnGhost(int ptr)
{
    int unit = CreateObjectAt("Ghost", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    SetUnitMaxHealth(unit, 98);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnScorpion(int ptr)
{
    int unit = CreateObjectAt("Scorpion", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 260);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnGoon(int ptr)
{
    int unit = CreateObjectAt("Goon", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, GoonBinTable());
    SetUnitVoice(unit, 63);
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 128);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnOgreLord(int ptr)
{
    int unit = CreateObjectAt("OgreWarlord", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 325);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnRedGirl(int ptr)
{
    int unit = ColorMaidenAt(0, 225, 64, ptr);

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 225);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnWhiteSpider(int ptr)
{
    int unit = CreateObjectAt("AlbinoSpider", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 128);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnPoisonZombie(int ptr)
{
    int unit = CreateObjectAt("VileZombie", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 275);
    SetUnitSpeed(unit, 3.0);
    SetCallback(unit, 5, PoisonZombieDeathHandler);
    return unit;
}

int SpawnMeleeLich(int ptr)
{
    int unit = CreateObjectAt("Lich", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, LichLordBinTable());
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 260);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    UnitZeroFleeRange(unit);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    return unit;
}

int SpawnMeleeNecromancer(int ptr)
{
    int unit = CreateObjectAt("Necromancer", GetObjectX(ptr), GetObjectY(ptr));

    NecromancerSubProcess(unit);
    SetOwner(GetMaster(), unit);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    return unit;
}

int SpawnDeadWizard(int ptr)
{
    int unit = CreateObjectAt("WizardRed", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, WizardRedBinTable());
    UnitZeroFleeRange(unit);
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 225);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    return unit;
}

int SpawnCaptain(int ptr)
{
    int unit = CreateObjectAt("AirshipCaptain", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, AirshipCaptainBinTable());
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 325);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    AggressionLevel(unit, 1.0);
    RetreatLevel(unit, 0.0);
    return unit;
}

int SpawnPlant(int ptr)
{
    int unit = CreateObjectAt("CarnivorousPlant", GetObjectX(ptr), GetObjectY(ptr));

    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 300);
    SetCallback(unit, 5, DungeonUnitDeathHandler);
    SetUnitSpeed(unit, 1.6);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    AggressionLevel(unit, 1.0);
    return unit;
}

int SpawnBeast(int ptr)
{
    int unit = CreateObjectAt("WeirdlingBeast", GetObjectX(ptr), GetObjectY(ptr));

    UnitLinkBinScript(unit, WeirdlingBeastBinTable());
    SetOwner(GetMaster(), unit);
    SetUnitMaxHealth(unit, 175);
    //SetCallback(unit, 5, DungeonUnitDeathHandler);
    SetCallback(unit, 5, WeirdlingDie);
    UnitZeroFleeRange(unit);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    AggressionLevel(unit, 1.0);
    return unit;
}

int ColorMaidenAt(int red, int grn, int blue, int ptr)
{
    int unit = CreateObjectAt("Bear2", GetObjectX(ptr), GetObjectY(ptr));
    int ptr1 = GetMemory(0x750710), k, num;

    UnitLinkBinScript(unit, MaidenBinTable());
    SetMemory(ptr1 + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetUnitVoice(unit, 7);

    return unit;
}

int LowLevelMobFunctions(int index)
{
    int lowLevels[] = {SpawnUrchin, SpawnBat, SpawnSmallWhiteSpider, SpawnLeech, SpawnSwordman, SpawnArcher};

    return lowLevels[index];
}

int DungeonMobFunctions(int index)
{
    int medLevels[] = { SpawnSwordman, SpawnArcher, SpawnEmberDemon, SpawnGargoyle, SpawnBlackSpider,
        SpawnWolf, SpawnBlackWolf, SpawnOgre, SpawnOgreAxe, SpawnDarkBear, SpawnBrownBear,
        SpawnSkeleton, SpawnSkeletonLord, SpawnGhost, SpawnScorpion, SpawnGoon, SpawnOgreLord,
        SpawnRedGirl, SpawnWhiteSpider, SpawnPoisonZombie, SpawnMeleeLich, SpawnMeleeNecromancer,
        SpawnDeadWizard, SpawnCaptain, SpawnPlant, SpawnBeast };

    return medLevels[index];
}

void SpawnMarkerMonster(int cur)
{
    int mob = Bind(DungeonMobFunctions(Random(0, 25)), &cur);

    SetCallback(mob, 7, FieldMonsterHurt);
    Delete(cur);
}
