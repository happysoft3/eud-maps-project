
#include "fov_utils.h"
#include "libs/grplib.h"
#include "libs/animFrame.h"

void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
}

void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}

void GRPDump_HosungOniOutput(){}

#define LEE_HO_SUNG_ONI_BASE_IMAGE_ID 133943
void initializeImageFrameHosungOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_HosungOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    ChangeMonsterSpriteImageCountAndFrames(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    ChangeMonsterSpriteImageUnkValue(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign4Directions(0, LEE_HO_SUNG_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3, LEE_HO_SUNG_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6, LEE_HO_SUNG_ONI_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy4Directions(     LEE_HO_SUNG_ONI_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDumpTileSet(){}

void initializeCustomTileset(int imgVector){
    int *pack=GetScrCodeField(GRPDumpTileSet)+4;
    int *off=&pack[1];
    int frames[]={pack+off[0],pack+off[1], pack+off[2],pack+off[3],pack+off[4],
        pack+off[5],pack+off[6],pack+off[7],pack+off[8],};

    AppendImageFrame(imgVector, frames[0], 8636);
    AppendImageFrame(imgVector, frames[1], 8637);
    AppendImageFrame(imgVector, frames[2], 8638);
    AppendImageFrame(imgVector, frames[3], 8639);
    AppendImageFrame(imgVector, frames[4], 8640);
    AppendImageFrame(imgVector, frames[5], 8641);
    AppendImageFrame(imgVector, frames[6], 8642);
    AppendImageFrame(imgVector, frames[7], 8643);
    AppendImageFrame(imgVector, frames[8], 8644);
    ForceUpdateTileChanged();    
}

void GRPDump_RunnerOniOutput(){}

#define RUNNER_ONI_OUTPUT_BASE_IMAGE_ID 130005

void initializeImageFrameRunnerOniOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_RunnerOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);

    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);

    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign4Directions(0, RUNNER_ONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);

    AnimFrameAssign4Directions(3, RUNNER_ONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);

    AnimFrameCopy4Directions(     RUNNER_ONI_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);

    AnimFrameAssign4Directions(6, RUNNER_ONI_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);

    AnimFrameAssign4Directions(9, RUNNER_ONI_OUTPUT_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_WALK, 3);

    AnimFrameCopy4Directions(     RUNNER_ONI_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);

    AnimFrameCopy4Directions(     RUNNER_ONI_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);

    AnimFrameCopy4Directions(     RUNNER_ONI_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);

    AnimFrameCopy4Directions(     RUNNER_ONI_OUTPUT_BASE_IMAGE_ID+12,  IMAGE_SPRITE_MON_ACTION_RUN, 3);

    AnimFrameCopy4Directions(     RUNNER_ONI_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);

}

void GRPDump_YellowShirtOutput(){}

#define YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID 129717
void initializeImageFrameYellowShirt(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int xyInc[]={0,-3};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_YellowShirtOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,9,1,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,9,1,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameAssign8Directions(5,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign8Directions(10,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameAssign8Directions(15,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameAssign8Directions(20,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameAssign8Directions(25,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameAssign8Directions(30,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameAssign8Directions(35,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameAssign8Directions(40,  YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_WALK, 8);

    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+40, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+48, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+56, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+64, IMAGE_SPRITE_MON_ACTION_RUN, 8);
    AnimFrameCopy8Directions( YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions( YELLOW_SHIRT_OUTPUT_BASE_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void InitializeResources(){
    int vec=CreateImageVector(2048);

    initializeImageFrameSoldier(vec);
    initializeImageHealthBar(vec);
    initializeImageFrameHosungOni(vec);
    initializeCustomTileset(vec);
    initializeImageFrameRunnerOniOutput(vec);
    initializeImageFrameYellowShirt(vec);
    DoImageDataExchange(vec);
}
