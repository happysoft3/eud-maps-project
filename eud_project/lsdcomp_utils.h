
#include "libs/define.h"
#include "libs/unitstruct.h"
#include "libs/printutil.h"
#include "libs/format.h"
#include "libs/queueTimer.h"
#include "libs/playerinfo.h"
#include "libs/objectIDdefines.h"

int GetKillCredit()
{
    int ptr = GetMemory(0x979724), ptr2;

    if (ptr)
    {
        ptr2 = GetMemory(ptr + 0x208);
        if (ptr2)
        {
            return GetMemory(ptr2 + 0x2c);
        }
    }
    return 0;
}

int GetTopParentUnit(int unit)
{
    int owner = unit, cur = GetOwner(unit);

    while (CurrentHealth(cur))
    {
        owner = cur;
        cur = GetOwner(cur);
    }
    return owner;
}

int GetPlayerNetID(int ptr)
{
	int res;

	if (ptr)
	{
		res = GetMemory(ptr + 0x2ec);
		if (res)
		{
			res = GetMemory(res + 0x114);
			if (res)
				return GetMemory(res + 0x810) & 0xff;
		}
	}
	return 0;
}

int IsVisibleOr(int unit1, int unit2)
{
    return IsVisibleTo(unit1, unit2) || IsVisibleTo(unit2, unit1);
}

// int GetPlayerPtrByNetCode(int net)
// {
// 	int k, plr_ptr = 0x62f9e4;

// 	for (k = 0 ; k < 32 ; k ++)
// 	{
// 		if (GetMemory(plr_ptr) ^ net)
// 			plr_ptr += 0x12dc;
// 		else
// 			return GetMemory(plr_ptr - 4);
// 	}
// 	return 0;
// }

void GreenSparkFx(float x, float y)
{
    int ptr = CreateObjectAt("MonsterGenerator", x, y);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

#define PRINT_ALL -1

void PrintMessageFormatOne(int user, string fmt, int arg)
{
    char message[192];

    NoxSprintfString(message, fmt, &arg, 1);
    if (user==-1)
    {
        UniPrintToAll(ReadStringAddressEx(message));
        return;
    }
    UniPrint(user, ReadStringAddressEx(message));
}


int createSplashCode(int fn)
{
    char *p;
    int sz;

    if (!p)
    {
        char code[]={
            0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x0c, 0xFF, 0x75, 0x08, 0x68, 
            0x01, 0x00, 0x00, 0x00, 0xB8, 0x10, 0x73, 0x50, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x0C, 0x5D, 0xC3, 0x90, 0x90
        };
        sz=sizeof(code);
        p=code;
    }
    char *a=MemAlloc(sz);
    NoxByteMemCopy(p, a, sz);
    int *pFn = &a[10];
    pFn[0]=fn;
    return a;
}

void SplashDamageAt(int attacker, float x, float y, float range, int fn)
{
    char *excode = createSplashCode(fn);
    int obj[]={UnitToPtr(attacker), excode, range, &x,};
    char *pCode;

    if (!pCode)
    {
        char code[]={
            0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xFF, 
            0x70, 0x08, 0xFF, 0x70, 0x0C, 0xB8, 0x90, 0x7F, 0x51, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x10, 0x31, 0xC0, 0xC3
        };
        pCode=code;
    }
    invokeRawCode(pCode, obj);
    MemFree(excode);
}

void EmptyAll(int user)
{
	while (GetLastItem(user))
		Delete(GetLastItem(user));
}

void onDeferredPickup(int item)
{
    if (ToInt(GetObjectX(item)))
    {
        int owner=GetOwner(item);

        if (CurrentHealth(owner))
            Pickup(owner, item);
    }
}

void SafetyPickup(int user, int item)
{
    if (CurrentHealth(user))
    {
        if (ToInt(GetObjectX(item)))
        {
            SetOwner(user,item);
            PushTimerQueue(1, item, onDeferredPickup);
        }
    }
}

void InitiPlayerCamera(int unit)
{
	int ptr = UnitToPtr(unit);

	if (ptr)
    {
        ptr = GetMemory(ptr + 0x2ec);
        if (ptr)
        {
            ptr = GetMemory(ptr + 0x114);
            if (ptr)
            {
                SetMemory(ptr + 0xe58, 0);
            }
        }
    }
}

int UserDamageArrowCreate(int owner, int wp, int dam)
{
    int unit = CreateObject("MercArcherArrow", wp);
    int ptr = GetMemory(0x750710);

    SetOwner(owner, unit);
    SetMemory(ptr + 0x14, 0x32);
    SetMemory(GetMemory(ptr + 0x2bc) + 4, dam);
    Enchant(unit, "ENCHANT_INVISIBLE", 0.0);
    return unit;
}

void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float *pFrom, float* pTo, int dur)
{
    float xyVect[2];
    ComputePointRatio(pTo, pFrom, xyVect, 32.0);
    int count = FloatToInt(Distance(pFrom[0],pFrom[1],pTo[0],pTo[1])/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", pFrom[0], pFrom[1]);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void SendPacketAll(char *packet, int packetLength)
{
    char code[]={
        0x6A, 0x01, 0x6A, 0x00, 0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x90, 0x53, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x1C, 0x31, 0xC0, 0xC3,
    };
    int args[]={
        packet,0xff,packetLength,
    }; //0xff or 0x9f
    invokeRawCode(code, args);
}

void SendPacketSpecifyOne(int user, char *packet, int packetLength)
{
    int pIndex=GetPlayerIndex(user);
    if (pIndex<0)
        return;
    char code[]={
        0xB8, 0x50, 0x72, 0x50, 0x00, 0xFF, 0xD0, 0x6A, 0x01, 0x6A, 0x00, 0xFF, 0x70, 0x08, 0xFF, 0x30, 0xFF, 0x70, 0x04, 0xB8, 0x20, 0x54, 0x4E, 0x00, 0xFF, 0xD0, 0x83, 0xC4, 0x14, 0x31, 0xC0, 0xC3
    };
    int args[]={
        packet,pIndex,packetLength,
    };
    invokeRawCode(code, args);
}

void ShowQuestIntroOne(int user, int questLv, string introImg, string introTxt)
{
    char packet[]={
        0xF0, 0x0D, 0x05, 0x00, 0x03, 0x57, 0x69, 0x7A, 0x61, 0x72, 0x64, 0x43, 0x68, 0x61, 0x70, 0x74, 0x65, 
        0x72, 0x42, 0x65, 0x67, 0x69, 0x6E, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x53, 0x65, 0x72, 0x76, 0x6F, 0x70, 0x74, 0x73, 0x2E, 0x77, 0x6E, 0x64, 0x3A, 0x47, 
        0x61, 0x6D, 0x65, 0x4E, 0x61, 0x6D, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    short *pLv = &packet[2];
    pLv[0]=questLv;
    StringUtilCopy(StringUtilGetScriptStringPtr(introImg), &packet[5]);
    StringUtilCopy(StringUtilGetScriptStringPtr(introTxt), &packet[0x25]);
    if (user==0)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}

void ShowQuestIntroAll(int questLv, string introImg, string introTxt)
{
    ShowQuestIntroOne(0, questLv, introImg, introTxt);
}

void SendItemEnchantData(int user, int flags)
{
    char packet[]={0x5b, flags, };

    if (user == -1)
    {
        SendPacketAll(packet, sizeof(packet));
        return;
    }
    SendPacketSpecifyOne(user, packet, sizeof(packet));
}

int computePercent(int cur, int max, int perc)
{
    return (cur*perc)/max;
}

void MonsterReportHitpoints(int unit, int cur, int max, int durations)
{
    char slots[11];

    NoxByteMemset(slots,sizeof(slots)-1,' ');
    int perc=computePercent(cur,max, 10);

    while (--perc>=0)
        slots[perc]='#';

    char buff[32], *p=slots;

    NoxSprintfString(buff, "[%s]", &p, 1);
    UniChatMessage(unit, ReadStringAddressEx(buff), durations);
}

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

void onHealthbarProgress(int t)
{
    int owner=GetOwner(t);

    if (MaxHealth(t)){
        if (CurrentHealth(owner)){
            if (ToInt( DistanceUnitToUnit(t,owner)) )
                MoveObject(t,GetObjectX(owner),GetObjectY(owner));
            PushTimerQueue(1,t,onHealthbarProgress);
            int perc= computePercent(CurrentHealth(owner),MaxHealth(owner),10);

            if (GetUnit1C(t)!=perc)
            {
                controlHealthbarMotion(t,perc);
                SetUnit1C(t,perc);
            }
            return;
        }
    }
    Delete(t);
}

void AttachHealthbar(int unit)
{
    int bar=DummyUnitCreateById(OBJ_BEAR_2,GetObjectX(unit),GetObjectY(unit));

    UnitNoCollide(bar);
    SetOwner(unit,bar);
    SetUnit1C(bar,0);
    PushTimerQueue(1,bar,onHealthbarProgress);
}
