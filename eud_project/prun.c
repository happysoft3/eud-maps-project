
#include "noxscript\builtins.h"
#include "libs\typecast.h"
#include "libs\unitutil.h"
#include "libs\unitstruct.h"
#include "libs\printutil.h"
#include "libs\mathlab.h"
#include "libs\username.h"

#include "libs\fxeffect.h"
#include "libs\waypoint.h"

#include "libs\buff.h"
#include "libs\spellutil.h"

#define OTHER   -1
#define TRUE 1

int player[20];
int PLR_PTR;
int TRP_FIRE;
int CAGE_ROW[8];
int GOAL;
int PLR_SCORE[10];

#define ENCHANT_INVISIBLE   0
#define ENCHANT_MOONGLOW    1
#define ENCHANT_BLINDED     2
#define ENCHANT_CONFUSED        3
#define ENCHANT_SLOWED          4
#define ENCHANT_HELD            5
#define ENCHANT_DETECTING       6
#define ENCHANT_ETHEREAL        7
#define ENCHANT_RUN             8
#define ENCHANT_HASTED          9
#define ENCHANT_VILLAIN         10
#define ENCHANT_AFRAID          11
#define ENCHANT_BURNING         12
#define ENCHANT_VAMPIRISM       13
#define ENCHANT_ANCHORED        14
#define ENCHANT_LIGHT           15
#define ENCHANT_DEATH           16
#define ENCHANT_PROTECT_FROM_FIRE       17
#define ENCHANT_PROTECT_FROM_POISON     18
#define ENCHANT_PROTECT_FROM_MAGIC      19  
#define ENCHANT_PROTECT_FROM_ELECTRICITY            20
#define ENCHANT_INFRAVISION         21
#define ENCHANT_SHOCK               22
#define  ENCHANT_INVULNERABLE        23
#define  ENCHANT_TELEKINESIS         24
#define ENCHANT_FREEZE              25
#define ENCHANT_SHIELD              26
#define ENCHANT_REFLECTIVE_SHIELD   27
#define ENCHANT_CHARMING            28
#define ENCHANT_ANTI_MAGIC          29
#define ENCHANT_CROWN               30
#define ENCHANT_SNEAK               31

#define PLAYER_FLAG_DEATH 0x80000000


int FireSpriteBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216;
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28;arr[59] = 5544896; arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}


void MapInitialize()
{
    PlayerFlag(-1);
    MovingRotRows();
    InitPlayerRequirements();
    ExplosionRoutingTrap();
    MeteorFalling();
    MasterUnit();

    SecondTimer(3, SelectZoneOpen);
}

void MainGameLoop()
{
    TRP_FIRE = Object("SouthTrap1");
    Move(Object("MazePatrolBlock"), 179);
    ReventRightObstacle(53, 10);
    ReventRightObstacle(54, 10);
    ReventRightObstacle(55, 10);
    ReventLeftObstacle(56, 10);
    SentryTrapLoop();
    MapLighting();
    LavaPatrolRow();
    RemoveSentryTrapWalls();
    TakeShot();
    FrameTimer(5, InitFlameWalkLocations);
    FrameTimer(5, InitiGenerate);
    FrameTimer(10, PlayerClassOnLoop);
    FrameTimer(10, LoopSearchIndex);
    FrameTimer(10, InitSlideRow);
    FrameTimer(200, InitPutBarrel);
}

void MapLighting()
{
    string name = "InvisibleLightBlueHigh";

    CreateObject(name, 122);
    CreateObject(name, 127);
}

void InitPlayerRequirements()
{
    string name = "InvisibleLightBlueHigh";
    int k;
    PLR_PTR = CreateObject(name, 240);
    LookWithAngle(PLR_PTR, 0);

    for (k = 1 ; k < 10 ; k ++)
    {
        MoveWaypoint(240, GetWaypointX(240) + 23.0, GetWaypointY(240));
        CreateObject(name, 240);
        LookWithAngle(PLR_PTR + k, 0);
    }
}

void InitPutBarrel()
{
    SpawnGolem(49);
    SpawnGolem(50);
    SpawnGolem(189);
    SpawnGolem(189);
    LoopChargeDeathball();
    SpawnSpider(103);
    SpawnSpider(104);
    SpawnSpider(105);
    SpawnBarrel(51);
    DemonSpawn(220);
    DemonSpawn(221);
    SpawnBarrel(51);
    SpawnBarrel(51);
    SpawnBarrel(51);
    SpawnBarrel(51);
    SpawnGhost(102);
    SpawnGhost(102);
    SpawnGhost(102);
    SpawnGhost(102);
    SpawnGhost(102);
    FrameTimerWithArg(3, 101, SpawnBarrel);
    FrameTimerWithArg(3, 101, SpawnBarrel);
    FrameTimerWithArg(3, 101, SpawnBarrel);
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & PLAYER_FLAG_DEATH;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ PLAYER_FLAG_DEATH;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    PLR_SCORE[plr] = 0;

    return plr;
}

void RegistPlayer()
{
    int plr, k;

    while (1)
    {
        if (CurrentHealth(other))
        {
            if (MaxHealth(other) == 150)
            {
                plr = CheckPlayer();

                for (k = 9 ; k >= 0 && plr < 0 ; k --)
                {
                    if (!MaxHealth(player[k]))
                    {
                        plr = PlayerClassOnInit(k, GetCaller());
                        break;
                    }
                }
                if (plr >= 0)
                {
                    JoinTheMap(plr);
                    break;
                }
            }
        }
        CantJoin();
        break;
    }
}

void CantJoin()
{
    MoveObject(other, LocationX(243), LocationY(243));
    UniPrint(OTHER, "전사 플레이어만 맵에 입장하실 수 있습니다.");
    Enchant(other, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(other, "ENCHANT_FREEZE", 0.0);
}

void JoinTheMap(int k)
{
    int sword = CreateObject("GreatSword", 163);
    int ptr;

    if (PlayerClassCheckDeathFlag(k))
        PlayerClassSetDeathFlag(k);
    Enchant(player[k], "ENCHANT_INVULNERABLE", 2.5);
    Frozen(sword, 1);
    Stack(sword);
    EmptyAll(player[k]);
    MoveObject(player[k], GetWaypointX(232), GetWaypointY(232));

    LookWithAngle(PLR_PTR + k, 0);
    ptr = CreateObject("InvisibleLightBlueHigh", 232);
    LookWithAngle(ptr, k);
    CastSpellObjectObject("SPELL_TELEPORT_TO_MARK_1", player[k], player[k]);
    FrameTimerWithArg(1, ptr, DelayTeleportLastLocation);
}

void DelayTeleportLastLocation(int ptr)
{
    int plr = GetDirection(ptr);
    int pop = Stack(0);

    if (CurrentHealth(player[plr]))
    {
        Pickup(player[plr], pop);
        if (IsVisibleTo(ptr, player[plr]))
            MoveObject(player[plr], GetWaypointX(plr + 1), GetWaypointY(plr + 1));
        Delete(ptr);
    }
    else
        Delete(pop);
}

void SpawnGasTrap(int pUnit)
{
    if (!UnitCheckEnchant(pUnit, GetLShift(ENCHANT_AFRAID)))
    {
        CastSpellObjectObject("SPELL_FIREWALK", pUnit, pUnit);
        Enchant(pUnit, EnchantList(ENCHANT_AFRAID), 12.0);
    }
}

void PlayerClassOnAlive(int plr, int pUnit)
{
    MoveObject(PlayerFlag(plr), GetObjectX(pUnit), GetObjectY(pUnit));
    if (UnitCheckEnchant(pUnit, GetLShift(ENCHANT_SNEAK)))
    {
        EnchantOff(pUnit, EnchantList(ENCHANT_SNEAK));
        RemoveTreadLightly(pUnit);
        SpawnGasTrap(pUnit);
    }
}

void PlayerClassOnDeath(int plr)
{
    int pUnit = player[plr];

    UniPrintToAll(PlayerIngameNick(pUnit) + " 플레이어 님이 격추되었습니다.");
    CastSpellObjectLocation("SPELL_MARK_1", pUnit, GetObjectX(PlayerFlag(plr)), GetObjectY(PlayerFlag(plr)));
}

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (TRUE)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnAlive(i, player[i]);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i)) break;
                    else
                    {
                        PlayerClassOnDeath(i);
                        PlayerClassSetDeathFlag(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

void MovingRotRows()
{
    int ptr;
    int k;
    int wp[2];

    if (!ptr) //17
    {
        ptr = Object("RotBlock1");
        wp[0] = 23;
        wp[1] = 17;
    }
    for (k = 0 ; k < 6 ; k ++)
        Move(ptr + (k * 2), wp[0] + k);
    wp[0] = wp[0] ^ wp[1];
    wp[1] = wp[1] ^ wp[0];
    wp[0] = wp[0] ^ wp[1];
    FrameTimer(50, MovingRotRows);
}

void ExplosionRoutingTrap()
{
    int idx;

    DeleteObjectTimer(CreateObject("FireGrateFlame", idx + 29), 10);
    DeleteObjectTimer(CreateObject("FireGrateFlame", 48 - idx), 10);
    idx = (idx + 1) % 20;
    FrameTimer(7, ExplosionRoutingTrap);
}

void SpawnGolem(int wp)
{
    int unit = CreateObject("StoneGolem", wp);
    CreateObject("InvisibleLightBlueHigh", wp);

    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Raise(unit + 1, ToFloat(wp));
    SetCallback(unit, 5, SetDeaths);
}

void SetDeaths()
{
    int ptr = GetTrigger() + 1;
    int wp = ToInt(GetObjectZ(ptr));

    SpawnGolem(wp);
}

void MeteorFalling()
{
    int unit[2];

    if (unit[0])
    {
        RhombusPut(52, 3640.0, 3850.0, 2476.0, 2685.0);
        CastSpellObjectLocation("SPELL_METEOR", unit[0], GetWaypointX(52), GetWaypointY(52));
        unit[0] = unit[0] ^ unit[1];
        unit[1] = unit[1] ^ unit[0];
        unit[0] = unit[0] ^ unit[1];
    }
    else
    {
        unit[0] = Object("CherubStatue");
        unit[1] = Object("CherubStatue2");
    }
    FrameTimer(10, MeteorFalling);
}

void TakeShot()
{
    int ptr[4];

    if (ptr[0])
        CastSpellObjectLocation("SPELL_DEATH_RAY", ptr[Random(0, 3)], GetObjectX(other), GetObjectY(other));
    else
    {
        ptr[0] = CreateObject("MovableStatueVictory1S", 204);
        ptr[1] = CreateObject("MovableStatueVictory2E", 205);
        ptr[2] = CreateObject("MovableStatueVictory2N", 206);
        ptr[3] = CreateObject("MovableStatueVictory1W", 207);
        Enchant(ptr[0], "ENCHANT_FREEZE", 0.0);
        Enchant(ptr[1], "ENCHANT_FREEZE", 0.0);
        Enchant(ptr[2], "ENCHANT_FREEZE", 0.0);
        Enchant(ptr[3], "ENCHANT_FREEZE", 0.0);
    }
}

void DemonSpawn(int wp)
{
    int unit = CreateObject("EmberDemon", wp);
    
    CreateObject("InvisibleLightBlueHigh", wp);
    SetUnitMaxHealth(unit, 128);
    SetCallback(unit, 5, DemonDead);
}

void DemonDead()
{
    FrameTimerWithArg(240, GetTrigger() + 1, DemonRespawn);
}

void DemonRespawn(int ptr)
{
    MoveWaypoint(160, GetObjectX(ptr), GetObjectY(ptr));
    DemonSpawn(160);
    Delete(ptr);
}

void SentryTrapLoop()
{
    int ray;

    if (ray)
    {
        ObjectToggle(ray);
        ObjectToggle(ray + 2);
    }
    else
    {
        ray = Object("SouthSentry2");
        ObjectOn(ray);
    }
    SecondTimer(2, SentryTrapLoop);
}

void OpenEastPitWalls()
{
    WallOpen(Wall(81, 207));
    WallOpen(Wall(82, 208));
    WallClose(Wall(86, 208));
    WallClose(Wall(87, 207));
}

void OpenNorthPitWalls()
{
    WallOpen(Wall(86, 208));
    WallOpen(Wall(87, 207));
    WallClose(Wall(81, 207));
    WallClose(Wall(82, 208));
}

void fireWardTraps()
{
    int ptr;

    if (IsPlayerUnit(OTHER))
    {
        if (!ptr)
            ptr = Object("FireWard1");

        int k;
        for (k = 8 ; k >= 0 ; k --)
            ObjectOn(ptr + (k * 2));
        FrameTimerWithArg(1, ptr, DelayFireTrapOff);
    }
}

void DelayFireTrapOff(int ptr)
{
    int k;

    for (k = 8 ; k >= 0 ; k --)
        ObjectOff(ptr + (k * 2));
}

void SpawnBarrel(int wp)
{
	int unit = CreateObject("Maiden", 131);
	CreateObject("TraderArmorRack1", wp);

    Enchant(unit + 1, "ENCHANT_FREEZE", 0.0);
    SetOwner(MasterUnit(), unit);
    SetOwner(MasterUnit(), unit + 1);
	Frozen(unit, 1);
	SetCallback(unit, 9, JunkYardDog2);
	FrameTimerWithArg(10, unit, DelayMove);
}

void DelayMove(int unit)
{
	ObjectOff(unit);
	MoveObject(unit, GetObjectX(unit + 1), GetObjectY(unit + 1));
}

void JunkYardDog2()
{
	int ptr = GetTrigger() + 1;

    if (IsCaller(ptr))
    {
	    MoveObject(self, GetObjectX(ptr), GetObjectY(ptr));
        if (!HasEnchant(ptr, "ENCHANT_SHOCK"))
            Enchant(ptr, "ENCHANT_SHOCK", 0.0);
    }
}

void ReventLeftObstacle(int wp, int max)
{
    int k;
    for (k = max ; k ; k --)
    {
        Frozen(CreateObject("MovableCrypt1", wp), 1);
        MoveWaypoint(wp, GetWaypointX(wp) - 23.0, GetWaypointY(wp) + 23.0);
    }
}

void ReventRightObstacle(int wp, int max)
{
    //53
    int k;
    for (k = max ; k ; k --)
    {
        Frozen(CreateObject("MovableCrypt2", wp), 1);
        MoveWaypoint(wp, GetWaypointX(wp) + 23.0, GetWaypointY(wp) + 23.0);
    }
}

void EnableSouthArrowTraps1()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 67);
    int k;
    int ptr2 = Object("SouthArrowTrap2");

    Raise(ptr, ToFloat(ptr2));    //send to pointer of trap
    LookWithAngle(ptr, 5);              //send to max
    for (k = 4 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void EnableSouthArrowTraps2()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 67);
    int k;
    int ptr2 = Object("SouthArrowTrap6");

    Raise(ptr, ToFloat(ptr2));    //send to pointer of trap
    LookWithAngle(ptr, 5);              //send to max
    for (k = 4 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void EnableSouthArrowTraps3()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 67);
    int k;
    int ptr2 = Object("SouthArrowTrapP3");

    Raise(ptr, ToFloat(ptr2));    //send to pointer of trap
    LookWithAngle(ptr, 8);              //send to max
    for (k = 7 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void EnableSouthArrowTraps4()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 67);
    int k;
    int ptr2 = Object("SouthArrowTrapB3");

    Raise(ptr, ToFloat(ptr2));    //send to pointer of trap
    LookWithAngle(ptr, 6);              //send to max
    for (k = 5 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void EnableWestArrowTraps1()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 68);
    int k;
    int ptr2 = Object("WestArrowTrap3");

    Raise(ptr, ToFloat(ptr2));    //send to pointer of trap
    LookWithAngle(ptr, 5);              //send to max
    for (k = 4 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void EnableWestArrowTraps2()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 68);
    int k;
    int ptr2 = Object("WestArrowTrap8");

    Raise(ptr, ToFloat(ptr2));    //send to pointer of trap
    LookWithAngle(ptr, 5);              //send to max
    for (k = 4 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void EnableWestArrowTraps3()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 68);
    int k;
    int ptr2 = Object("WestArrowTrap13");

    Raise(ptr, ToFloat(ptr2));    //send to pointer of trap
    LookWithAngle(ptr, 5);              //send to max
    for (k = 4 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void DisableArrowTrap(int ptr)
{
    int k;
    int ptr2 = ToInt(GetObjectZ(ptr));
    int max = GetDirection(ptr);

    for (k = max - 1 ; k >= 0 ; k --)
        ObjectOff(ptr2 + (k * 2));
    Delete(ptr);
}

void ActiveSouthTrap1()
{
    ObjectOn(TRP_FIRE);
    ObjectOn(TRP_FIRE + 2);
    ObjectOn(TRP_FIRE + 4);
    FrameTimerWithArg(1, 0x300, DisableFireballTrap);
}

void ActiveSouthTrap2()
{
    ObjectOn(TRP_FIRE + 6);
    ObjectOn(TRP_FIRE + 8);
    ObjectOn(TRP_FIRE + 10);
    FrameTimerWithArg(1, 0x303, DisableFireballTrap);
}

void ActiveSouthTrap3()
{
    ObjectOn(TRP_FIRE + 12);
    ObjectOn(TRP_FIRE + 14);
    ObjectOn(TRP_FIRE + 16);
    FrameTimerWithArg(1, 0x306, DisableFireballTrap);
}

void ActiveSouthTrap4()
{
    ObjectOn(TRP_FIRE + 18);
    ObjectOn(TRP_FIRE + 20);
    ObjectOn(TRP_FIRE + 22);
    ObjectOn(TRP_FIRE + 24);
    FrameTimerWithArg(1, 0x409, DisableFireballTrap);
}

void DisableFireballTrap(int ptr)
{
    int max = ptr >> 8;
    int ptr2 = ptr & 0xff;
    int k;

    for (k = max - 1 ; k >= 0 ; k --)
        ObjectOff(TRP_FIRE + ((k + ptr2) * 2));
}

void InitFlameWalkLocations()
{
    int k;

    for (k = 13 ; k >= 0 ; k --)
        MoveWaypoint(k + 73, GetWaypointX(k + 87), GetWaypointY(k + 87));
    FrameTimerWithArg(45, 15, ControlFlameWalkTrap);
}

void ControlFlameWalkTrap(int num)
{
    if (num)
    {
        FlameWalk(num - 1);
        FrameTimerWithArg(12, num - 1, ControlFlameWalkTrap);
    }
    else
        FrameTimerWithArg(42, 15, ControlFlameWalkTrap);
}

void FlameWalk(int num)
{
    int count[14];

    if (count[num] < 15)
    {
        MoveWaypoint(num + 73, GetWaypointX(num + 73) - 23.0, GetWaypointY(num + 73) - 23.0);
        DeleteObjectTimer(CreateObject("FireGrateFlame", num + 73), 11);
        AudioEvent("FireGrate", num + 73);
        count[num] ++;
        FrameTimerWithArg(12, num, FlameWalk);
    }
    else
    {
        MoveWaypoint(num + 73, GetWaypointX(num + 87), GetWaypointY(num + 87));
        count[num] = 0;
    }
}

void InitiGenerate()
{
    int k;

    for (k = 3 ; k >= 0 ; k --)
    {
        SpawnObelisk(70);
        SpawnObelisk(71);
        MoveWaypoint(70, GetWaypointX(70) + 46.0, GetWaypointY(70) + 46.0);
        MoveWaypoint(71, GetWaypointX(71) - 46.0, GetWaypointY(71) - 46.0);
    }
    FireSoulSpawn(143);
    FireSoulSpawn(146);
    FireSoulSpawn(144);
    FireSoulSpawn(108);
}

void SpawnObelisk(int wp)
{
	int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 100);
	CreateObject("MonsterGenerator", wp);
	CreateObject("SpinningSkull", wp);
    CreateObject("InvisibleLightBlueHigh", wp);
	Frozen(unit + 2, 1);
	Damage(unit, 0, 999, -1);

	ObjectOff(unit + 1);
	FrameTimerWithArg(1, unit, DelayTeleportUnit);
}

void DelayTeleportUnit(int unit)
{
	if (CurrentHealth(unit + 1))
	{
		SetCallback(unit, 9, CheckGenerateStat);
		MoveObject(unit, GetObjectX(unit + 1), GetObjectY(unit + 1));
		MoveObject(unit + 2, GetObjectX(unit), GetObjectY(unit));
	}
}

void CheckGenerateStat()
{
	int ptr = GetTrigger() + 1;
	int new;

	MoveObject(ptr + 1, GetObjectX(self), GetObjectY(self));
	if (!CurrentHealth(ptr))
	{
		Delete(ptr + 1);
		Delete(self);
        FrameTimerWithArg(60, ptr + 2, RespawnGenerate);
	}
}

void RespawnGenerate(int ptr)
{
    MoveWaypoint(69, GetObjectX(ptr), GetObjectY(ptr));
    SpawnObelisk(69);
    Delete(ptr);
}

void SpawnGhost(int wp)
{
    int unit = CreateObject("Ghost", wp);
    CreateObject("InvisibleLightBlueHigh", wp);

    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    LookWithAngle(unit + 1, wp);
    SetCallback(unit, 5, GhostDead);
}

void GhostDead()
{
    int ptr = GetTrigger() + 1;
    int wp = GetDirection(ptr);

    Delete(ptr);
    FrameTimerWithArg(150, wp, SpawnGhost);
}

void SpawnSpider(int wp)
{
    int unit = CreateObject("BlackWidow", wp);
    CreateObject("InvisibleLightBlueHigh", wp);

    UnitLinkBinScript(unit, BlackWidowBinTable());
    SetUnitVoice(unit, 19);
    LookWithAngle(unit + 1, wp);
    SetUnitMaxHealth(unit, 225);
    SetCallback(unit, 5, SpiderDead);
}

void SpiderDead()
{
    int subUnit = GetTrigger() + 1;
    int wp = GetDirection(subUnit);

    DeleteObjectTimer(self, 30);
    Delete(subUnit);
    FrameTimerWithArg(150, wp, SpawnSpider);
}

void LoopChargeDeathball()
{
    CastSpellLocationLocation("SPELL_FORCE_OF_NATURE", GetWaypointX(106), GetWaypointY(106), GetWaypointX(106) - 128.0, GetWaypointY(106) + 128.0);
    SecondTimer(2, LoopChargeDeathball);
}

void EnableGreenRoomTraps()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 145);
    int k;
    int ptr2 = Object("WestArrowTrap19");

    Raise(ptr, ToFloat(ptr2));
    LookWithAngle(ptr, 7);
    for (k = 6 ; k >= 0 ; k --)
        ObjectOn(ptr2 + (k * 2));

    FrameTimerWithArg(1, ptr, DisableArrowTrap);
}

void TargetLay()
{
    int st;
    int mis;

    if (CurrentHealth(other))
    {
        if (!st)
            st = Object("NorthStatue"); //61, 65
        MoveWaypoint(112, GetObjectX(st) - UnitRatioX(st, other, 18.0), GetObjectY(st) - UnitRatioY(st, other, 18.0));
        mis = CreateObject("ArcherBolt", 112);
        LookAtObject(mis, other);
        PushObject(mis, 15.0, GetObjectX(st), GetObjectY(st));
    }
}

void UnlockBearCage()
{
    int k;
    int call;

    for (k = 8 ; k >= 0 ; k --)
        WallToggle(Wall(k + 118, k + 128));
    if (!call)
        call = FrameTimer(30, InitCageRows);
}

void InitCageRows()
{
    int k;

    for (k = 7 ; k >= 0 ; k --)
        CAGE_ROW[k] = Object("CageBlock" + IntToString(k + 1));
    LoopMovingRows();
}

void LoopMovingRows()
{
    int way[2];
    int k;

    if (!way[0])
    {
        way[0] = 121;
        way[1] = 113;
    }
    for (k = 7 ; k >= 0 ; k --)
        Move(CAGE_ROW[k], way[0] + k);
    way[0] = way[0] ^ way[1];
    way[1] = way[1] ^ way[0];
    way[0] = way[0] ^ way[1];
    SecondTimer(2, LoopMovingRows);
}

void SentryWalls()
{
    UniChatMessage(other, "그 누구도 나보다 앞서갈 수 없어!", 120);
    SpawnSentryTrapWalls();
    FrameTimerWithArg(50, GetTrigger(), EnableSentryTrap);
    ObjectOff(self);
}

void RemoveSentryTrapWalls()
{
    int k;

    for (k = 8 ; k >= 0 ; k --)
    {
        WallOpen(Wall(128 + k, 138 + k));
        WallOpen(Wall(146 - k, 138 + k));
    }
}

void SpawnSentryTrapWalls()
{
    int k;

    for (k = 8 ; k >= 0 ; k --)
    {
        WallClose(Wall(128 + k, 138 + k));
        WallClose(Wall(146 - k, 138 + k));
    }
}

void EnableSentryTrap(int trg)
{
    int arr[2];

    if (!arr[0])
    {
        arr[0] = Object("DeathSentry");
        arr[1] = Object("RightSentry");
        ObjectOn(arr[0]);
        ObjectOn(arr[1]);
        FrameTimerWithArg(100, trg, EnableSentryTrap);
    }
    else
    {
        ObjectOff(arr[0]);
        ObjectOff(arr[1]);
        arr[0] = 0;
        arr[1] = 0;
        FrameTimer(40, RemoveSentryTrapWalls);
        FrameTimerWithArg(50, trg, ResetRaySwitch);
    }
}

void ResetRaySwitch(int trg)
{
    ObjectOn(trg);
}

void ActivateLava_Blocks0()
{
    int ptr = Object("LavaBlock5");
    WallOpen(Wall(127, 157));
    FrameTimerWithArg(35, ptr, MovingLavaRow);
    ObjectOff(self);
    Switch(GetTrigger());
}

void MovingLavaRow(int ptr)
{
    int k;
    if (GetObjectX(ptr) > GetWaypointX(183))
    {
        if (GetObjectX(ptr) > GetWaypointX(178))
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + (k * 2), GetObjectX(ptr + (k * 2)) - 3.0, GetObjectY(ptr + (k * 2)) - 3.0);
        }
        else
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + (k * 2), GetObjectX(ptr + (k * 2)) - 3.0, GetObjectY(ptr + (k * 2)) + 3.0);
        }
        FrameTimerWithArg(1, ptr, MovingLavaRow);
    }
    else
        SecondTimerWithArg(3, ptr, MovingLavaRowRev);
}

void MovingLavaRowRev(int ptr)
{
    int k;
    if (GetObjectX(ptr) < GetWaypointX(173))
    {
        if (GetObjectX(ptr) < GetWaypointX(178))
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + (k * 2), GetObjectX(ptr + (k * 2)) + 3.0, GetObjectY(ptr + (k * 2)) - 3.0);
        }
        else
        {
            for (k = 4 ; k >= 0 ; k --)
                MoveObject(ptr + (k * 2), GetObjectX(ptr + (k * 2)) + 3.0, GetObjectY(ptr + (k * 2)) + 3.0);
        }
        FrameTimerWithArg(1, ptr, MovingLavaRowRev);
    }
    else
    {
        WallClose(Wall(127, 157));
        ObjectOn(Switch(0));
    }
}

int Switch(int num)
{
    int data;

    if (num)
        data = num;
    return data;
}

int Stack(int data)
{
    int arr[10];
    int cur;

    if (data)
    {
        cur ++;
        arr[cur - 1] = data;
        return 0;
    }
    cur --;
    return arr[cur];
}

void shotNorthArrowBolt()
{
    int tower;
    int mis;

    if (!tower)
        tower = Object("ArrowTower");
    if (CurrentHealth(other))
    {
        MoveWaypoint(190, GetObjectX(tower) - UnitRatioX(tower, other, 17.0), GetObjectY(tower) - UnitRatioY(tower, other, 17.0));
        mis = CreateObject("ArcherBolt", 190);
        LookAtObject(mis, other);
        PushObject(mis, 20.0, GetObjectX(tower), GetObjectY(tower));
    }
}

void shotSouthArrowTower()
{
    shotNorthArrowBolt();
}

void LightningHurt()
{
    UniChatMessage(other, "이거나 먹어라!", 120);
    CastSpellObjectObject("SPELL_FORCE_OF_NATURE", other, self);
    ObjectOff(self);
    FrameTimerWithArg(75, GetTrigger(), ResetRaySwitch);
}

void EnableOgreHut01Elevator()
{
    MoveObject(other, GetWaypointX(241), GetWaypointY(241));
    Effect("TELEPORT", GetObjectX(other), GetObjectY(other), 0.0, 0.0);
    AudioEvent("BlinkCast", 241);
}

void LavaPatrolRow()
{
    int ptr;
    int ptr2;
    int k;
    int flag;

    if (ptr)
    {
        for (k = 3 ; k >= 0 ; k --)
        {
            Move(ptr + (k * 2), k + 156 - flag);
            Move(ptr2 + (k * 2), k + 165 - flag);
        }
        AudioEvent("SpikeBlockMove", 209);
        flag = (flag + 1) % 2;
    }
    else
    {
        ptr = Object("PatrolBlock1");
        ptr2 = Object("PatrolBlock1A");
    }
    SecondTimer(3, LavaPatrolRow);
}

void InitSlideRow()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 221);
    int k;

    for (k = 1 ; k <= 5 ; k ++)
    {
        CreateObject("SpikeBlock", 191);
        CreateObject("InvisibleLightBlueHigh", 191);
        Enchant(ptr + (k * 2) - 1, "ENCHANT_FREEZE", 0.0);
        Raise(ptr + (k * 2), RandomFloat(1.0, 10.0));
        MoveWaypoint(191, GetWaypointX(191) - 46.0, GetWaypointY(191) + 46.0);
        SlideBlock(ptr + (k * 2) - 1);
    }
}

void SlideBlock(int unit)
{
    if (GetObjectX(unit) + GetObjectY(unit) > 7244.0)
    {
        MoveObject(unit, GetObjectX(unit) - GetObjectZ(unit + 1), GetObjectY(unit) - GetObjectZ(unit + 1));
    }
    else
    {
        MoveObject(unit, GetObjectX(unit + 1), GetObjectY(unit + 1));
        Raise(unit + 1, RandomFloat(1.0, 8.0));
    }
    FrameTimerWithArg(1, unit, SlideBlock);
}

void SelectZoneOpen()
{
    int unit = CreateObject("InvisibleLightBlueHigh", 145);
    int k;

    MoveObject(GetHost(), GetWaypointX(145), GetWaypointY(145));
    UniChatMessage(GetHost(), "몇바퀴를 도실지 선택하세요.", 150);
    AudioEvent("LongBellsDown", 145);
    
    for (k = 0 ; k < 4 ; k ++)
    {
        CreateObject("WillOWisp", k + 169);
        Frozen(unit + k + 1, 1);
        LookWithAngle(unit + k + 1, k + 1);
        UniChatMessage(unit + k + 1, IntToString(k + 1), 180);
        SetDialog(unit + k+1, "NORMAL", SetLessons, DummyFunction);
    }
}

void SetLessons()
{
    int limit = GetDirection(self);

    if (!GOAL)
    {
        MoveObject(other, 4750.0, 2770.0);
        GOAL = limit;
        ObjectOn(Object("GameMainSwitch"));

        UniPrintToAll("목표치가 " + IntToString(limit) + " 으로 정해졌습니다.");
        FrameTimer(10, MainGameLoop);
        FrameTimer(30, LadderBoard);
    }
}

void GoalFinish()
{
    int win;
    int plr = CheckPlayer();

    if (plr >= 0 && !win)
    {
        PLR_SCORE[plr] ++;
        if (PLR_SCORE[plr] < GOAL)
        {
            MoveObject(player[plr], GetWaypointX(plr + 1), GetWaypointY(plr + 1));
            AudioEvent("FlagCapture", plr + 1);
            UniPrint(other, "트랙에 도착 하셨습니다.");
            UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 트랙을 통과 했습니다.");
        }
        else
        {
            win = 1;
            ObjectOff(Object("GameMainSwitch"));
            MoveObject(Object("StartLocation"), GetWaypointX(171), GetWaypointY(171));
            AllPlayerFreeze();
            UniPrintToAll("방금 승자가 결정되었습니다");
            UniPrint(OTHER, "목표점수에 달성하셨습니다");
            FrameTimerWithArg(60, plr, VictoryEvent);
        }
    }
}

void LadderBoard()
{
    int k;
    string show = "점수 현황판/ 최대: " + IntToString(GOAL);

    for (k = 9 ; k >= 0 ; k -= 1)
        show += ("\n" + SignColor(k) + ":\t" + IntToString(PLR_SCORE[k]));
    UniChatMessage(MasterUnit(), show, 50);
    SecondTimer(1, LadderBoard);
}

void AllPlayerFreeze()
{
    int k;

    for (k = 9 ; k >= 0 ; k-=1)
    {
        if (CurrentHealth(player[k]))
        {
            Enchant(player[k], "ENCHANT_FREEZE", 0.0);
            Enchant(player[k], "ENCHANT_INVULNERABLE", 0.0);
            PlaySoundAround(player[k], 914);
        }
    }
}

void VictoryEvent(int win)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
        {
            if (win == k)
            {
                MoveObject(player[k], GetWaypointX(145), GetWaypointY(145));
                Enchant(player[win], "ENCHANT_CROWN", 0.0);
            }
            else
                MoveObject(player[k], GetWaypointX(169), GetWaypointY(169));
        }
    }
    UniPrintToAll("이번 게임의 승자는 " + PlayerIngameNick(player[win]) + " 플레이어 입니다!");
    Effect("WHITE_FLASH", GetWaypointX(145), GetWaypointY(145), 0.0, 0.0);
    DeleteObjectTimer(CreateObject("OblivionUp", 145), 1800);
    FrameTimer(1, StrVictory);
}

void StrVictory()
{
	int arr[13];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 2613312; arr[1] = 301998097; arr[2] = 7080064; arr[3] = 1099186194; arr[4] = 35653889; arr[5] = 268762112; arr[6] = 33718242; arr[7] = 16777488; arr[8] = 132155394; arr[9] = 134217985; 
	arr[10] = 570458248; arr[11] = 2086650888; arr[12] = 536999970; 
	while(i < 13)
	{
		drawStrVictory(arr[i], name);
		i ++;
	}
}

void drawStrVictory(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(145);
		pos_y = GetWaypointY(145);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			Frozen(CreateObject(name, 145), 1);
		if (count % 38 == 37)
			MoveWaypoint(145, GetWaypointX(145) - 111.000000, GetWaypointY(145) + 3.000000);
		else
			MoveWaypoint(145, GetWaypointX(145) + 3.000000, GetWaypointY(145));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(145, pos_x, pos_y);
	}
}

void FireSoulSpawn(int wp)
{
    int unit = CreateObject("FireSprite", wp);
    int ptr = GetMemory(0x750710);

    CreateObject("InvisibleLightBlueHigh", wp);

    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
    SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(57));
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    SetUnitMaxHealth(unit, 96);
    SetCallback(unit, 5, FireSoulDead);
}

void FireSoulDead()
{
    FrameTimerWithArg(240, GetTrigger() + 1, FireSoulRespawn);
}

void FireSoulRespawn(int ptr)
{
    MoveWaypoint(160, GetObjectX(ptr), GetObjectY(ptr));
    FireSoulSpawn(160);
    Delete(ptr);
}

void LoopSearchIndex()
{
    int cur;
    int last = CreateObject("InvisibleLightBlueHigh", 1);

    if (cur)
    {
        while (cur < last)
        {
            if (HasClass(cur, "MISSILE"))
            {
                if (HasSubclass(cur, "SHURIKEN"))
                    ShurikenEvent(cur);
                else if (GetUnitThingID(cur) == 526)
                    HarpoonHandler(cur);
            }
            else if (HasClass(cur, "OBSTACLE"))
            {
                if (GetUnitThingID(cur) == 2623)
                    Delete(cur);
            }
            cur ++;
        }
    }
    else
        cur = last;
    Delete(last);
    FrameTimer(1, LoopSearchIndex);
}

void HarpoonHandler(int cur)
{
    int owner = GetOwner(cur), mis;

    if (CurrentHealth(owner))
    {
        mis = CreateObjectAt("ArcherBolt", GetObjectX(cur), GetObjectY(cur));
        SetOwner(owner, mis);
        LookAtObject(mis, owner);
        LookWithAngle(mis, GetDirection(mis) + 128);
        PushObject(mis, 25.0, GetObjectX(owner), GetObjectY(owner));
    }
    Delete(cur);
}

void ShurikenEvent(int cur)
{
    int owner = GetOwner(cur), mis;

    if (CurrentHealth(owner))
    {
        mis = CreateObjectAt("SpiderSpit", GetObjectX(cur), GetObjectY(cur));
        SetOwner(owner, CreateObjectAt("ThrowingStone", GetObjectX(cur), GetObjectY(cur)));
        SetOwner(owner, mis);
        PushObject(mis, -28.0, GetObjectX(owner), GetObjectY(owner));
        PushObject(mis + 1, -28.0, GetObjectX(owner), GetObjectY(owner));
    }
    Delete(cur);
}

void PlayerFreeze()
{
    int k;
    if (HasClass(other, "PLAYER"))
    {
        if (!HasEnchant(other, "ENCHANT_FREEZE"))
        {
            UniChatMessage(other, "Ready_!!", 90);
            Enchant(other, "ENCHANT_FREEZE", 0.0);
            LookWithAngle(other, 160);
            if (!k)
            {
                FrameTimer(30, StartGame);
                k = 1;
            }
        }
    }
}

void StartGame()
{
    int k;

    if (!k)
    {
        UniPrintToAll("준비하시고~");
        FrameTimer(75, StartGame);
    }
    else if (k <= 4)
    {
        FrameTimer(0, DummyFunction);
        FrameTimer(0, FuncPtr() + (k * 2));
        AudioEvent("MetallicBong", 129);
        if (k < 4)
            FrameTimer(42, StartGame);
        else
        {
            DisableStartSwitchs();
            UnFreezeAllPlayer();
            OpenStartZoneWalls();
            AudioEvent("GameOver", 129);
            UniPrintToAll("쏘세요!");
        }
    }
    k ++;
}

void DummyFunction()
{
    //
}

void UnFreezeAllPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (HasEnchant(player[k], "ENCHANT_FREEZE"))
            EnchantOff(player[k], "ENCHANT_FREEZE");
    }
}

void OpenStartZoneWalls()
{
    int k;

    for (k = 8 ; k >= 0 ; k --)
        WallOpen(Wall(182 - k, 154 + k));
}

int FuncPtr()
{
    StopScript(FuncPtr);
}

void DisableStartSwitchs()
{
    int ptr = Object("StartSwitchBase");
    int k;
    
    for (k = 0 ; k < 5 ; k ++)
        ObjectOff(ptr + (k * 2));
}

void StrNum3()
{
	int arr[3];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 1141379356; arr[1] = 1149305347; arr[2] = 56; 
	while(i < 3)
	{
		drawStrNum3(arr[i], name);
		i ++;
	}
}

void drawStrNum3(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(129);
		pos_y = GetWaypointY(129);
	}
	for (i = 1 ; i > 0 && count < 93 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 129);
		if (count % 7 == 6)
			MoveWaypoint(129, GetWaypointX(129) - 18.000000, GetWaypointY(129) + 3.000000);
		else
			MoveWaypoint(129, GetWaypointX(129) + 3.000000, GetWaypointY(129));
		count ++;
	}
	if (count >= 93)
	{
		count = 0;
		MoveWaypoint(129, pos_x, pos_y);
	}
}

void StrNum2()
{
	int arr[3];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 67670300; arr[1] = 67641474; arr[2] = 124; 
	while(i < 3)
	{
		drawStrNum2(arr[i], name);
		i ++;
	}
}

void drawStrNum2(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(129);
		pos_y = GetWaypointY(129);
	}
	for (i = 1 ; i > 0 && count < 93 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 129);
		if (count % 7 == 6)
			MoveWaypoint(129, GetWaypointX(129) - 18.000000, GetWaypointY(129) + 3.000000);
		else
			MoveWaypoint(129, GetWaypointX(129) + 3.000000, GetWaypointY(129));
		count ++;
	}
	if (count >= 93)
	{
		count = 0;
		MoveWaypoint(129, pos_x, pos_y);
	}
}

void StrNum1()
{
	int arr[2];
	string name = "SpiderSpit";
	int i = 0;
	arr[0] = 136348424; arr[1] = 68174084; 
	while(i < 2)
	{
		drawStrNum1(arr[i], name);
		i ++;
	}
}

void drawStrNum1(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(129);
		pos_y = GetWaypointY(129);
	}
	for (i = 1 ; i > 0 && count < 62 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 129);
		if (count % 6 == 5)
			MoveWaypoint(129, GetWaypointX(129) - 15.000000, GetWaypointY(129) + 3.000000);
		else
			MoveWaypoint(129, GetWaypointX(129) + 3.000000, GetWaypointY(129));
		count ++;
	}
	if (count >= 62)
	{
		count = 0;
		MoveWaypoint(129, pos_x, pos_y);
	}
}

void StrStandBy()
{
	int arr[13];
	string name = "HealOrb";
	int i = 0;
	arr[0] = 2367552; arr[1] = 334626321; arr[2] = 136841344; arr[3] = 1669611634; arr[4] = 1646266632; arr[5] = 268762367; arr[6] = 1026; arr[7] = 2116018448; arr[8] = 134252547; arr[9] = 2013266176; 
	arr[10] = 570490639; arr[11] = 2080407560; arr[12] = 1069808674; 
	while(i < 13)
	{
		drawStrStandBy(arr[i], name);
		i ++;
	}
}

void drawStrStandBy(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(129);
		pos_y = GetWaypointY(129);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 129);
		if (count % 38 == 37)
			MoveWaypoint(129, GetWaypointX(129) - 74.000000, GetWaypointY(129) + 2.000000);
		else
			MoveWaypoint(129, GetWaypointX(129) + 2.000000, GetWaypointY(129));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(129, pos_x, pos_y);
	}
}

string SignColor(int num)
{
    string color[10] = {"빨간색", "초록색", "파란색", "노란색", "하늘색",
        "보라색", "검은색", "흰색", "오렌지색", "갈색"};
    
    return color[num];
}

int PlayerFlag(int num)
{
    int flag[10];

    if (num < 0)
    {
        flag[0] = Object("flagBase");
        int k;
        for (k = 1 ; k < 10 ; k ++)
            flag[k] = flag[0] + (k * 2);
        return 0;
    }
    return flag[num];
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void RhombusPut(int wp, float x_low, float x_high, float y_low, float y_high)
{
    float pos_x = RandomFloat(y_low, y_high);
	float pos_y = RandomFloat(0.0, x_high - x_low);
    MoveWaypoint(wp, x_high - y_high + pos_x - pos_y, pos_x + pos_y);
}

int MasterUnit()
{
    int master;

    if (!master)
    {
        master = CreateObject("Hecubah", 244);
        Frozen(master, 1);
    }
    return master;
}

void EmptyAll(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}
