
#include "r240503_main.h"

void MapInitialize()
{OnInitializeMap();}
void MapExit()
{OnShutdownMap();}

int m_player[MAX_PLAYER_COUNT];
static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex]; }
static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user; }
int m_playerFlags[MAX_PLAYER_COUNT];
static int GetPlayerFlags(int pIndex) //virtual
{return m_playerFlags[pIndex]; }
static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_playerFlags[pIndex]=flags;}
short* m_respPoint;
int m_respPointCount;
static void SetRespawnPointSettings(int *points, int count){
    m_respPoint=points;
    m_respPointCount=count;
}//virtual
#define ALTERNATIVE_SPOT 15
static int GetRespawnPoint(){
    if (m_respPointCount==0) return ALTERNATIVE_SPOT;
    return m_respPoint[Random(0,m_respPointCount-1)];
}//virtual
int m_monsterCount;
static int GetMonsterCount(){return m_monsterCount;} //virtual
static void SetMonsterCount(int count){m_monsterCount=count;} //virtual

int m_monsterTotalCount;
static int GetMonsterTotalCount(){
    return m_monsterTotalCount;
}//virtual
static void SetMonsterTotalCount(int count){
    m_monsterTotalCount=count;
}//virtual

int m_spawnLoopFn;
static int GetSpawnPermanentlyFunction(){return m_spawnLoopFn;}//virtual
static void SetSpawnPermanentlyFunction(int fn){m_spawnLoopFn=fn;}//virtual
static void NetworkUtilClientMain(){ InitializeClientOnly(); }



static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

















int m_genericHash;
static int GenericHash(){
    if (!m_genericHash)HashCreateInstance(&m_genericHash);
    return m_genericHash;
}//virtual

int m_creature1[MAX_PLAYER_COUNT];
static void GetCreature1DataPtr(int pIndex,int**pget){//virtual
    if (pget)
        pget[0]=&m_creature1[pIndex];
}//virtual

int m_creature2[MAX_PLAYER_COUNT];

static void GetCreature2DataPtr(int pIndex,int**pget){
    if (pget)
        pget[0]=&m_creature2[pIndex];
}//virtual



int m_creature3[MAX_PLAYER_COUNT];

static void GetCreature3DataPtr(int pIndex,int**pget){
    if (pget)
        pget[0]=&m_creature3[pIndex];
}//virtual
int m_creature4[MAX_PLAYER_COUNT];

static void GetCreature4DataPtr(int pIndex,int**pget){
    if (pget)
        pget[0]=&m_creature4[pIndex];
}//virtual











int m_creature5[MAX_PLAYER_COUNT];

static void GetCreature5DataPtr(int pIndex,int**pget){
    if (pget)
        pget[0]=&m_creature5[pIndex];
}//virtual






static void OnPlayerEntryMap(int pInfo)
{
    OnPlayerEntryMapProc(pInfo);
}



























