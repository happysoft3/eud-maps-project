
#define MAX_UPGRADE_LEVEL 14
#include "test1768_boss.h"
#include "libs/unitstruct.h"
#include "libs/waypoint.h"
#include "libs/format.h"
#include "libs/printutil.h"
#include "libs/playerinfo.h"
#include "libs/queueTimer.h"
#include "libs/objectIDdefines.h"
#include "libs/sound_define.h"
#include "libs/hash.h"
#include "libs/fxeffect.h"
#include "libs/logging.h "

void SetFlagColor(int unit, int color)
{
    int ptr = UnitToPtr(unit), k;
    
    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2b4), GetItemColors(color));
        SetMemory(GetMemory(ptr + 0x2b4) + 4, GetItemColors(color));
        SetMemory(GetMemory(ptr + 0x2b4) + 8, GetItemColors(color));
        SetMemory(GetMemory(ptr + 0x2b4) + 12, GetItemColors(color));
        for (k = 31 ; k >= 0 ; k --)
            SetMemory(ptr + 0x230 + (k * 4), 0x200);
    }
}string ChakramPowerLevel(int lv)
{
    string name[] = {"NULL", "ENCHANT_CONFUSED", "ENCHANT_SLOWED", "ENCHANT_INFRAVISION", "ENCHANT_PROTECT_FROM_FIRE", "ENCHANT_PROTECT_FROM_POISON",
        "ENCHANT_VAMPIRISM", "ENCHANT_PROTECT_FROM_ELECTRICITY", "ENCHANT_SHOCK", "ENCHANT_FREEZE", "ENCHANT_ANCHORED", "ENCHANT_RUN",
        "ENCHANT_SHIELD", "ENCHANT_VAMPIRISM", "ENCHANT_VAMPIRISM", "ENCHANT_VAMPIRISM"}; //12
    return name[lv];
}int GetItemColors(int num)
{
	int arr[32], k, ptr;

	if (!arr[0])
	{
		ptr = GetMemory(0x00654634);
		for (k = 0 ; k < 32 ; k ++)
		{
			arr[k] = ptr;
			ptr = GetMemory(ptr + 0x88);
		}
		return 0;
	}
	return arr[num];
}void PlayMapBgm(int num){Music(num, 100);}int BgmTable(int idx)
{
	int misc[30];

	if (idx < 0)
	{
		misc[1] = 33; misc[2] = 33; misc[3] = 33; misc[4] = 32; misc[5] = 60;
		misc[6] = 46; misc[7] = 86; misc[8] = 48; misc[9] = 33; misc[10] = 38;
		misc[11] = 32; misc[12] = 39; misc[13] = 36; misc[14] = 73; misc[15] = 264;
		misc[16] = 236; misc[17] = 216; misc[18] = 265; misc[19] = 254; misc[20] = 239;
		misc[21] = 244; misc[22] = 274; misc[23] = 181; misc[24] = 99; misc[25] = 112;
		misc[26] = 68; misc[27] = 150; misc[28] = 118; misc[29] = 54;
		return 0;
	}
	return misc[idx];
}

void StartBgmLoop()
{
	BgmTable(-1);
	SecondTimer(2, MapBgmLoop);
}

void MapBgmLoop()
{
	int key = Random(1, 29);
	MusicEvent();
	SecondTimerWithArg(3, key, PlayMapBgm);
	SecondTimer(BgmTable(key) + 3, MapBgmLoop);
}int EquipedWeapon(int unit)
{
    int ptr = UnitToPtr(unit), pic;
    
    if (ptr)
    {
        pic = GetMemory(GetMemory(ptr + 0x2ec) + 0x68);
        if (pic)
            return GetMemory(pic + 0x2c);
    }
    return 0;
}
int GetPlayer(int index) //virtual
{ }

int GetPlayerKills(int index) //virtual
{ }

void onDeferredPickup(int item)
{
    if (ToInt(GetObjectX(item)))
    {
        int owner=GetOwner(item);

        if (CurrentHealth(owner))
            Pickup(owner, item);
    }
}

void SafetyPickup(int user, int item)
{
    if (CurrentHealth(user))
    {
        if (ToInt(GetObjectX(item)))
        {
            SetOwner(user,item);
            PushTimerQueue(1, item, onDeferredPickup);
        }
    }
}

void ResetHostileCritter()
{
	SetMemory(0x833e64, 0x55b);		//CarnivorousPlant
	SetMemory(0x833e70, 1329);		//FishBig
	SetMemory(0x833e74, 1330);		//FishSmall
	SetMemory(0x833e78, 1359);		//Rat
	SetMemory(0x833e7c, 1313);		//GreenFrog
}

void SetHostileCritter()
{
	SetMemory(0x833e64, 0x540);		//CarnivorousPlant
	SetMemory(0x833e70, 0x540);		//FishBig
	SetMemory(0x833e74, 0x540);		//FishSmall
	SetMemory(0x833e78, 0x540);		//Rat
	SetMemory(0x833e7c, 0x540);		//GreenFrog
}

int SpawnNest(float x, float y, int func)
{
    int unit = CreateObjectAt("WaspNest", x, y);

    SetUnitMaxHealth(unit, 175);
    SetUnitCallbackOnDeath(unit, func);
    return unit;
}

int CreateYellowPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 639); //YellowPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateBlackPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 641); //BlackPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateWhitePotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 640); //WhitePotion
    SetMemory(ptr + 12, GetMemory(ptr + 12) ^ 0x20);
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = CreateYellowPotion(125, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 640)
        x = CreateWhitePotion(100, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 641)
        x = CreateBlackPotion(85, GetObjectX(unit), GetObjectY(unit));
    if (x ^ unit) Delete(unit);

    return x;
}

int DummyUnitSmall(float x, float y)
{
    int unit = CreateObjectAt("Bomber", x, y);

    ObjectOff(unit);
    Damage(unit, 0, 999, -1);
    Frozen(unit, 1);
    return unit;
}

int GreenFrogBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[24] = 1065353216; arr[28] = 1101004800; 
		arr[29] = 12; arr[31] = 10; arr[32] = 14; arr[33] = 24; arr[59] = 5544320;
		link = &arr;
	}
	return link;
}

void StrSuccessMission()
{
	int arr[60], i, count = 0;
	string name = "HealOrb";
	float pos_x = LocationX(1), pos_y = LocationY(1);

	arr[0] = 491520; arr[1] = 469762048; arr[2] = 0; arr[3] = 1883012992; arr[4] = 1677460479; arr[5] = 1; arr[6] = 31956480; arr[7] = 1022368775; 
	arr[8] = 33553422; arr[9] = 255356928; arr[10] = 1103159352; arr[11] = 234881139; arr[12] = 2072102814; arr[13] = 235340224; arr[14] = 1879049144; arr[15] = 1544436976; 
	arr[16] = 1882721799; arr[17] = 7392; arr[18] = 1618077575; arr[19] = 29388861; arr[20] = 2143348638; arr[21] = 118438975; arr[22] = 268403175; arr[23] = 239017952; 
	arr[24] = 2021253120; arr[25] = 3900; arr[26] = 1882725376; arr[27] = 29818880; arr[28] = 30975; arr[29] = 29417472; arr[30] = 204472327; arr[31] = 245760; 
	arr[32] = 235339776; arr[33] = 1635778616; arr[34] = 2130706432; arr[35] = 2147471359; arr[36] = 203423680; arr[37] = 16776198; arr[38] = 112; arr[39] = 1612476423; 
	arr[40] = 125886512; arr[41] = 896; arr[42] = 14909496; arr[43] = 1007092102; arr[44] = 469769216; arr[45] = 119275968; arr[46] = 1614282752; arr[47] = 1610670083; 
	arr[48] = 954204161; arr[49] = 29360128; arr[50] = 458782; arr[51] = 2146959374; arr[52] = 235380351; arr[53] = 3670256; arr[54] = 112; arr[55] = 1883041792; 
	arr[56] = 29362175; arr[57] = 2097024; arr[58] = 0; arr[59] = 234881024;
	for (i = 0 ; i < 60 ; i ++)
		count = DrawStrSuccessMission(arr[i], name, count);
	TeleportLocation(1, pos_x, pos_y);
}

int DrawStrSuccessMission(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 1891 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 96 == 95)
			TeleportLocationVector(1, -190.0, 3.0);
		else
			TeleportLocationVector(1, 2.0, 0.0);
		count ++;
	}
	return count;
}

void StrNotComplete()
{
	int arr[92], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = LocationX(1), pos_y = LocationY(1);

	arr[0] = 117497856; arr[1] = 28; arr[2] = 458752; arr[3] = 58720312; arr[4] = 0; arr[5] = 1612454912; arr[6] = 66978689; arr[7] = 1088421888; 
	arr[8] = 1882720063; arr[9] = 0; arr[10] = 58949632; arr[11] = 1910534200; arr[12] = 486522881; arr[13] = 117501752; arr[14] = 28; arr[15] = 1886388224; 
	arr[16] = 235800320; arr[17] = 473432176; arr[18] = 1612563335; arr[19] = 897; arr[20] = 234881024; arr[21] = 1103224860; arr[22] = 117444099; arr[23] = 62419175; 
	arr[24] = 32568; arr[25] = 1073741824; arr[26] = 943588227; arr[27] = 1610727536; arr[28] = 1997413601; arr[29] = 921472; arr[30] = 0; arr[31] = 264269936; 
	arr[32] = 1981454; arr[33] = 1938005048; arr[34] = 29618205; arr[35] = 0; arr[36] = 478027520; arr[37] = 33489359; arr[38] = 1886619392; arr[39] = 954671007; 
	arr[40] = 2147483646; arr[41] = 1103356159; arr[42] = 14787; arr[43] = 242278400; arr[44] = 947417312; arr[45] = 1074673678; arr[46] = 954990819; arr[47] = 458808; 
	arr[48] = 1310457856; arr[49] = 118365187; arr[50] = 29819328; arr[51] = 432020592; arr[52] = 2080374798; arr[53] = 1132478463; arr[54] = 33553523; arr[55] = 954204160; 
	arr[56] = 469995008; arr[57] = 536805838; arr[58] = 1879048192; arr[59] = 939527792; arr[60] = 469892864; arr[61] = 1081196558; arr[62] = 15235; arr[63] = 7; 
	arr[64] = 118780; arr[65] = 7925774; arr[66] = 238551495; arr[67] = 516208; arr[68] = 57568; arr[69] = 808976384; arr[70] = 470221248; arr[71] = 1191196896; 
	arr[72] = 14680067; arr[73] = 1843199; arr[74] = 117440512; arr[75] = 14680076; arr[76] = 1611078663; arr[77] = 469762161; arr[78] = 58720480; arr[79] = 1610612736; 
	arr[80] = 469762433; arr[81] = 14909664; arr[82] = 3640; arr[83] = 1879055367; arr[84] = 0; arr[85] = 12344; arr[86] = 477105950; arr[87] = 116480; 
	arr[88] = 268402912; arr[89] = 32764; arr[90] = 1073612544; arr[91] = 65408; 
	for (i = 0 ; i < 92 ; i ++)
		count = DrawStrNotComplete(arr[i], name, count);
	TeleportLocation(1, pos_x, pos_y);
}

int DrawStrNotComplete(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 2852 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 160 == 159)
			TeleportLocationVector(1, -318.0, 3.0);
		else
			TeleportLocation(1, 2.0, 0.0);
		count ++;
	}
	return count;
}

int MasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 1);
        Frozen(unit, 1);
        MoveObject(unit, 5500.0, 100.0);
        LookWithAngle(unit, 0);
        TeleportLocation(1, GetObjectX(unit), GetObjectY(unit));
        CreateObject("BlackPowder", 1);
        SetCallback(unit, 9, DisplayLadderBoard);
    }
    return unit;
}

void DisplayLadderBoard()
{
    if (IsCaller(GetTrigger() + 1))
    {
        if (!GetDirection(SELF))
        {
            LadderBoardText(SELF);
        }
        LookWithAngle(SELF, (GetDirection(SELF) + 1) % 30);
    }
}

void LadderBoardText(int unit)
{
    string txt = "**플레이어 킬 스코어 현황 전광판**\n";
    char bigBuff[1600], uBuff[128];
    int i, scd, min, hor;

    StringUtilCopy(StringUtilGetScriptStringPtr(txt), bigBuff);
    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(GetPlayer(i)))
        {
            int args[]={StringUtilGetScriptStringPtr(PlayerIngameNick(GetPlayer(i))), GetPlayerKills(i)};
            NoxSprintfString(uBuff, "%s: %d\n", args, sizeof(args));
            StringUtilAppend(bigBuff, uBuff);
        }
    }
    if (++scd == 60)
    {
        scd = 0;
        if (++min == 60)
        {
            min = 0;
            hor ++;
        }
    }
    int args2[]={hor, min,scd};
    NoxSprintfString(uBuff, "플레이 타임:\n%d시간 %d분 %d초", args2, sizeof(args2));
    StringUtilAppend(bigBuff, uBuff);    
    UniChatMessage(unit, ReadStringAddressEx(bigBuff), 120);
}

void SpreadExplosionSparks(int ptr)
{
    int count = GetDirection(ptr), i, ptr2 = ptr + 1;

    if (count < 15)
    {
        for (i = 7 ; i >= 0 ; i --)
        {
            Effect("SPARK_EXPLOSION", GetObjectX(ptr2 + i), GetObjectY(ptr2 + i), 0.0, 0.0);
            Effect("THIN_EXPLOSION", GetObjectX(ptr2 + i), GetObjectY(ptr2 + i), 0.0, 0.0);
            MoveObject(ptr2 + i, GetObjectX(ptr2 + i) - UnitRatioX(ptr, ptr2 + i, 2.0), GetObjectY(ptr2 + i) - UnitRatioY(ptr, ptr2 + i, 2.0));
        }
        PlaySoundAround(ptr, SOUND_FireballExplode);
        LookWithAngle(ptr, count + 1);
        PushTimerQueue(1, ptr, SpreadExplosionSparks);
        return;
    }
    Delete(ptr);
    for (i = 7 ; i >= 0 ; i --) Delete(ptr2 + i);
}

void onExplosionBombSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF))
        {
            Damage(OTHER, SELF, 260, DAMAGE_TYPE_PLASMA);
        }
    }
}

void ExplosionBomb(int ptr)
{
    int owner = GetOwner(owner), unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(ptr + 1), GetObjectY(ptr + 1));
    int i;
    float x=GetObjectX(unit),y=GetObjectY(unit);

    for (i = 0 ; i < 8 ; i ++)
        CreateObjectAt("InvisibleLightBlueLow", x + MathSine(i * 45 + 90, 23.0), y + MathSine(i * 45, 23.0));
    SplashDamageAt(owner, x,y, 135.0,onExplosionBombSplash);
    SpreadExplosionSparks(unit);
}

void ShooterFlying(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    WriteLog("ShooterFlying");
    if (CurrentHealth(owner) && count < 36)
    {
        if (IsVisibleTo(ptr, ptr + 1))
        {
            WriteLog("ShooterFlying-1");
            Raise(ptr + 2, MathSine(count * 5, 250.0));
            MoveObjectVector(ptr, GetObjectZ(ptr), GetObjectZ(ptr + 1));
            MoveObject(ptr + 2, GetObjectX(ptr), GetObjectY(ptr));
        }
        else
        {
            WriteLog("ShooterFlying-2");
            LookWithAngle(ptr + 1, 1);
            count = 100;
        }
        LookWithAngle(ptr, count + 1);
        PushTimerQueue(1, ptr, ShooterFlying);
        return;
    }
    WriteLog("ShooterFlying-3");
    if (CurrentHealth(owner) && !GetDirection(ptr + 1))
    {
        ExplosionBomb(ptr);
    }
    Delete(ptr);
    Delete(ptr + 1);
    Delete(ptr + 2);
}

void PowerShooterHandler(int glow)
{
    int owner = GetOwner(glow);

    while (CurrentHealth(owner))
    {
        if (IsVisibleTo(glow, owner))
        {
            float range = DistanceUnitToUnit(owner, glow) / 3  6                .0;
            float vectX = UnitRatioX(glow, owner, range);
            float vectY = UnitRatioY(glow, owner, range);
            int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(owner) + vectX, GetObjectY(owner) + vectY);
            Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(glow), GetObjectY(glow)), vectY);
            Frozen(CreateObjectAt("BlackPowderBarrel", GetObjectX(unit), GetObjectY(unit)), 1);
            UnitNoCollide(unit + 2);
            Raise(unit, vectX);
            SetOwner(owner, unit);
            PushTimerQueue(1, unit, ShooterFlying);
            PlaySoundAround(owner, SOUND_FemaleSpellPhonemeUpLeft);
            UniChatMessage(owner, "수류탄 투척", 150);
            break;
        }
        UniPrint(owner, "그곳은 볼 수 없는 지역이기 때문에 시전되지 않았습니다, 수류탄은 소모되지 않았습니다");
        break;
    }
    Delete(glow);
}

void onCommonBookAbandonded()
{
    int glow=CreateObjectById(OBJ_MOONGLOW, GetObjectX(OTHER), GetObjectY(OTHER));

    SetOwner(OTHER, glow);
    PushTimerQueue(1, glow, PowerShooterHandler);
    Delete(SELF);
}

void DescriptionBuyGrenade()
{
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        TradeGrenadePay();
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.2);
        UniPrint(OTHER, "수류탄 1개를 구입하시겠습니까?, 요구금액: 320골드");
        UniPrint(OTHER, "이 작업을 계속하려면 [예]를 누르세요");
    }
}

void TradeGrenadePay()
{
    if (CurrentHealth(OTHER))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) < 3                            2                              0)
        {
            UniPrint(OTHER, "금화가 부족합니다! 320골드 필요");
            return;
        }
        
        ChangeGold(OTHER, -                                     3                                                     2                                         0);
        TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
        AudioEvent("TreasureDrop", 1);
        AudioEvent("DiamondDrop", 1);
        int item = CreateObject("CommonSpellBook", 1);

        SetUnitCallbackOnDiscard(item, onCommonBookAbandonded);
        SafetyPickup(OTHER, item);
        UniPrint(OTHER, "거래성공!");
    }
}

int GrenadeShop(float x, float y)
{
    int unit = CreateObjectAt("EmberDemon", x, y);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    StoryPic(unit, "DemonPic");
    SetDialog(unit, "AA", DescriptionBuyGrenade, TradeGrenadePay);
    return unit;
}

void onArrowAbandonded()
{
    Delete(SELF);
    if (CurrentHealth(OTHER))
    {
        float xvect=UnitAngleCos(OTHER, 11.0),yvect=UnitAngleSin(OTHER,11.0);
        int mis=CreateObjectById(OBJ_ARCHER_BOLT, GetObjectX(OTHER)+xvect,GetObjectY(OTHER)+yvect);

        LookAtObject(mis, OTHER);
        LookWithAngle(mis, GetDirection(mis)+128);
        SetOwner(OTHER, mis);
        PushObjectTo(mis, xvect*2.0,yvect*2.0);
    }
}

void TradeArrowPay()
{
    if (CurrentHealth(OTHER))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) < 300)
        {
            UniPrint(OTHER, "금화가 부족합니다! 300골드 필요");
            return;
        }
        
        ChangeGold(OTHER, -                                     300);
        PlaySoundAround(OTHER,SOUND_TreasureDrop);
        PlaySoundAround(OTHER,SOUND_DiamondDrop);
        int item = CreateObjectAt("ArcherArrow", GetObjectX(OTHER), GetObjectY(OTHER));
        SetUnitCallbackOnDiscard(item, onArrowAbandonded);
        SafetyPickup(OTHER, item);
        UniPrint(OTHER, "거래성공!");
    }
}

void DescriptionBuyArrow()
{
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        TradeArrowPay();
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.2);
        UniPrint(OTHER, "화살을 구입하시겠습니까?, 요구금액: 300골드");
        UniPrint(OTHER, "이 작업을 계속하려면 [예]를 누르세요");
    }
}

int ArrowShop(float x, float y)
{
    int unit = DummyUnitCreateById(OBJ_ARCHER ,x, y);

    StoryPic(unit, "DemonPic");
    SetDialog(unit, "AA", DescriptionBuyArrow, TradeArrowPay);
    return unit;
}

int RepairAll(int unit)
{
    int inv = GetLastItem(unit), count = 0;

    while (IsObjectOn(inv))
    {
        if (MaxHealth(inv) ^ CurrentHealth(inv))
        {
            RestoreHealth(inv, MaxHealth(inv) - CurrentHealth(inv));
            count ++;
        }
        inv = GetPreviousItem(inv);
    }
    return count;
}

void DescriptionRepairAll()
{
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        RepairAllResult();
    else
    {
        if (IsObjectOn(GetLastItem(OTHER)))
        {
            UniPrint(OTHER, "인벤토리 내 아이템 모두 수리해드립니다, 요구금액: 30 골드");
            UniPrint(OTHER, "[예]를 누르시면 작업을 시작합니다");
            Enchant(OTHER, "ENCHANT_AFRAID", 0.2);
        }
        else
            UniPrint(OTHER, "가진게 없으시군요... ㅠㅠ");
    }
}

void RepairAllResult()
{
    if (CurrentHealth(OTHER))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 30)
        {
            ChangeGold(OTHER, -30);
            TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
            Effect("THIN_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            Effect("DAMAGE_POOF", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            AudioEvent("ShopRepairItem", 1);
            PrintMessageFormatOne(OTHER, "%d 개의 아이템이 수리되었습니다, 30골드 차감됨", RepairAll(OTHER));
        }
        else
            UniPrint(OTHER, "금화가 부족합니다, 이 작업은 30골드가 필요합니다");
    }
}

int MakeSCV(float x, float y)
{
    int unit = CreateObjectAt("Swordsman", x, y);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    StoryPic(unit, "WoundedWarriorPic");
    SetDialog(unit, "AA", DescriptionRepairAll, RepairAllResult);
    return unit;
}

void DescriptionChakramPowerUpTrade()
{
    int inv = EquipedWeapon(OTHER), lv;

    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        PowerupTradeResult();
    else
    {
        if (HasSubclass(inv, "CHAKRAM"))
        {
            lv = GetDirection(inv);
            PrintMessageFormatOne(OTHER, "강화: 현재 착용중인 채크럼을 강화합니다, 강화레밸에 따라 요구금액이 달라집니다. 당신의 채크럼 강화 레밸: %d", lv);
            PrintMessageFormatOne(OTHER, "이 작업은 %d 골드를 요구합니다. 작업을 계속하려면 더블클릭하세요", 300+(lv*100));
            Enchant(OTHER, "ENCHANT_AFRAID", 0.2);
        }
        else
            UniPrint(OTHER, "채크럼 강화기능: 채크럼을 착용중이지 않습니다, 채크럼을 착용하여 다시 시도해보세요");
    }
}

void PowerupTradeResult()
{
    int inv = EquipedWeapon(OTHER);
    int lv = GetDirection(inv);
    int pay = 300 + (lv * 100), unit;

    if (CurrentHealth(OTHER))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= pay)
        {
            if (lv <MAX_UPGRADE_LEVEL)
            {
                TeleportLocation(1, GetObjectX(OTHER), GetObjectY(OTHER));
                AudioEvent("ShopRepairItem", 1);
                ChangeGold(OTHER, -pay);
                Delete(inv);
                unit = CreateObjectAt("RoundChakram", GetObjectX(OTHER), GetObjectY(OTHER));
                if (Random(0, 6))
                {
                    lv ++;
                    Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
                    UniPrint(OTHER, "강화성공! 강화된 무기가 당신의 아래에 있습니다");
                }
                else
                {
                    lv --;
                    Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
                    UniPrint(OTHER, "강화실패! 무기의 강화레밸이 -1 저하되었습니다, 0 레밸에서 실패한 경우 무기가 사라집니다!!");
                }
                if (lv >= 0)
                {
                    LookWithAngle(unit, lv);
                    SetFlagColor(unit, lv);
                    Enchant(unit, ChakramPowerLevel(lv), 0.0);
                }
                else
                    Delete(unit);
            }
            else
                UniPrint(OTHER, "무기가 이미 최대로 강화된 상태입니다!");
        }
        else
        {
            UniPrint(OTHER, "금화가 부족합니다");
        }
    }
}

int MakeTraderChakramPowerUp(float x, float y)
{
    int unit = CreateObjectAt("Horrendous", x, y);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    StoryPic(unit, "HorrendousPic");
    SetDialog(unit, "AA", DescriptionChakramPowerUpTrade, DescriptionChakramPowerUpTrade);
    return unit;
}

void onPixieCollide()
{
    if (!GetTrigger())
        return;

    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (!CurrentHealth(owner))
        {
            Delete(SELF);
            return;
        }
        if (IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 3, DAMAGE_TYPE_PLASMA);
        }
    }
}

int summonSuperPixie(int owner)
{
    int pix=CreateObjectById(OBJ_PIXIE, GetObjectX(owner), GetObjectY(owner));

    SetOwner(owner, pix);
    SetUnitSubclass(pix, GetUnitSubclass(pix) ^ 1);
    SetUnitCallbackOnCollide(pix, onPixieCollide);
    return pix;
}

void DialogTradeSummonPixie()
{
    EnchantOff(OTHER, "ENCHANT_AFRAID");
    if (GetGold(OTHER)>=440)
    {
        ChangeGold(OTHER,-440);
        summonSuperPixie(OTHER);
        UniPrint(OTHER, "거래완료! 픽시 잘 쓰세요~ 주의! 소유자가 죽게되면 픽시가 소멸됩니다");
        return;
    }
    UniPrint(OTHER, "금화가 부족합니다");
}

void DialogExplainSummonPixie()
{
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
        DialogTradeSummonPixie();
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.2);
        UniPrint(OTHER, "픽시 1개를 구입하시겠습니까?, 요구금액: 440골드");
        UniPrint(OTHER, "이 작업을 계속하려면 [예]를 누르세요");
    }
}

int MakeTraderPixieMaster(float x, float y)
{
    int unit = DummyUnitCreateById(OBJ_WIZARD_GREEN, x, y);

    StoryPic(unit, "HorrendousPic");
    SetDialog(unit, "AA", DialogExplainSummonPixie, DialogExplainSummonPixie);
    return unit;
}

void InitializeReadables()
{
    RegistSignMessage(Object("readable1"), "게임 팁- 공간이동 명령어:채팅창에 //t입력 ");
    RegistSignMessage(Object("readable2"), "<프로 개구리 사냥꾼> 제작. panic");
    RegistSignMessage(Object("readable3"), "마을로 되돌아가길 원하면, 채팅창에 //t 을 입력하세요");
    RegistSignMessage(Object("readable4"), "표지판에는 \"신관으로 가는 문\" 이라고 적혀져 있네요");
    RegistSignMessage(Object("readable5"), "표지판에는 \"개구리 해부학 연구실\" 이라고 적혀져 있네요");
    RegistSignMessage(Object("readable6"), "이 리프트는 수동 조작 방식입니다");
    RegistSignMessage(Object("readable7"), "이 리프트는 수동 조작 방식입니다");
    RegistSignMessage(Object("readable8"), "카드키를 스위치 가까이에 내려놓은 후 스위치를 사용해보세요");
    RegistSignMessage(Object("readable9"), "지저분 하고 냄새도 나지만 어쨌든 이 쓰레기 매립지 지역을 통과해야 해");
    RegistSignMessage(Object("readableA"), "이곳은 개구리 균사에 오염된 폐가입니다. 일반인의 출입을 엄격하게 금지합니다");
    RegistSignMessage(Object("readableB"), "개구리 균사 최초 희생자... 고이 잠드소서");
    RegistSignMessage(Object("readableC"), "각 방마다 괴물 소환 오벨리스크 1개 씩 가져다 놓으시오");
    RegistSignMessage(Object("readableD"), "이곳은... 싱크홀에 의해 생긴 지하동굴 이래나 뭐래나... 아무튼 조심하시오");
    RegistSignMessage(Object("readableE"), "이 너머의 지역은 위험합니다. 이미 이곳 바깥 곳곳에 개구리 균사가 퍼진게 틀림 없다고요");
    RegistSignMessage(Object("readableF"), "이곳은 \"개구리 균사 2차 실험장\" 입니다");
    RegistSignMessage(Object("readableG"), "이곳은 왕 개구리 서식처입니다. 주의하세요!");
}

#define GROUP_kingFrogWalls 2

void SummonKingFrog()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_kingFrogWalls);
    CreateFrogKing(LocationX(144),LocationY(144));
}
