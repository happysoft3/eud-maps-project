
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"

void ImageResource2afbc00()
{ }

void RedPotionImage()
{ }

void ImageResource3d96100()
{ }

void ImageResourceTeacher()
{ }

void ImageTileRed()
{}

void ImageTileGrn()
{}
void ImageTileBlu()
{}

#define IMAGE_VECTOR_SIZE 1024

void appendTextStringWrap(int imgvec,int color, string str,int imgid){
    short *p;

    AllocSmartMemEx(256,&p);
    NoxUtf8ToUnicode(StringUtilGetScriptStringPtr(str), p);
    AppendTextSprite(imgvec,color,p,imgid);
}

void initializeTextSprites(int vec){
    // AppendTextSprite(vec, 2558, textDrawF, 0x07F9, "고스트를 소환하려면 비콘을 밟으셈");
    // AppendTextSprite(vec, 2559, textDrawF, 0x07F9, "지금 여기 좌측벽에 비밀벽 있삼");
    // AppendTextSprite(vec, 2560, textDrawF, 0x07F9, "축하드립니다, 당신의 승리입니다!");
    // AppendTextSprite(vec, 2561, textDrawF, 0x07F9, "이곳은 명예의 전당입니다");
    // AppendTextSprite(vec, 2562, textDrawF, 0x07F9, "맵 개발자 쉼터");

    // AppendTextSprite(vec, 2563, textDrawF, 0x07F9, "몬스터 작살 용암 낚시터");
    // AppendTextSprite(vec, 2564, textDrawF, 0xEFE0, "박돌(박아돌리기) 하시오!!");
    // AppendTextSprite(vec, 2565, textDrawF, 0x07F9, "여기 가운데 밟으면 해머줌");
    // AppendTextSprite(vec, 2566, textDrawF, 0x07F9, "작살로 스위치를 당기시오");
    // AppendTextSprite(vec, 2567, textDrawF, 0x07F9, "썩은고기랑 효과 똑같음 ㅇㅇ");

    // AppendTextSprite(vec, 2568, textDrawF, 0x07F9, "전방을 향해 버저커쓰다가 중간 원에서 멈춰라");
    appendTextStringWrap(vec, 0x07F9, "고스트를 소환하려면 비콘을 밟으셈",131777);
    appendTextStringWrap(vec, 0x07F9, "지금 여기 좌측벽에 비밀벽 있삼",131778);
    appendTextStringWrap(vec, 0x07F9, "축하드립니다, 당신의 승리입니다!",131779);
    appendTextStringWrap(vec, 0x07F9, "이곳은 명예의 전당입니다",131780);
    appendTextStringWrap(vec, 0x07F9, "맵 개발자 쉼터",131781);
    appendTextStringWrap(vec, 0x07F9, "몬스터 작살 용암 낚시터",131782);
    appendTextStringWrap(vec, 0xEFE0, "박돌(박아돌리기) 하시오!!",131783);
    appendTextStringWrap(vec, 0x07F9, "여기 가운데 밟으면 해머줌",131784);
    appendTextStringWrap(vec, 0x07F9, "작살로 스위치를 당기시오",131786);
    appendTextStringWrap(vec, 0x07F9, "썩은고기랑 효과 똑같음 ㅇㅇ",131787);
    appendTextStringWrap(vec, 0x07F9, "전방을 향해 버저커쓰다가 중간 원에서 멈춰라",131788);
    appendTextStringWrap(vec, 0xC8CF, "버그탈출 넘버원 - 제작.237",131789);
}

void initializeRawImages(int vec){
    AppendImageFrame(vec, GetScrCodeField(ImageResourceTeacher)+4, 132384);
    AppendImageFrame(vec, GetScrCodeField(ImageResource2afbc00)+4, 132385);
    AppendImageFrame(vec, GetScrCodeField(ImageResource3d96100)+4, 132386);
    AppendImageFrame(vec, GetScrCodeField(RedPotionImage)+4, 17827);
    AppendImageFrame(vec, GetScrCodeField(ImageTileBlu)+4, 111533);
    AppendImageFrame(vec, GetScrCodeField(ImageTileGrn)+4, 111534);
}

void GRPDumpNumberOutput(){}
//12
#define NUMBER_IMAGE_START_AT 113042
void initializeImageNumber(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpNumberOutput)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],NUMBER_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],NUMBER_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],NUMBER_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],NUMBER_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],NUMBER_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],NUMBER_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],NUMBER_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],NUMBER_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],NUMBER_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],NUMBER_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,0,NUMBER_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,1,NUMBER_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,2,NUMBER_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,3,NUMBER_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,4,NUMBER_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,5,NUMBER_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,6,NUMBER_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,7,NUMBER_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,8,NUMBER_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,9,NUMBER_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_LEFT,10,NUMBER_IMAGE_START_AT+9);
}

void GRPDump_ZerglingDumpOutput(){}

#define ZERGLING_IMAGE_START_AT 119258

void initializeImageZergZergling(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_ALBINO_SPIDER;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_ZerglingDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount-7);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,6,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,6,USE_DEFAULT_SETTINGS,2);

    AnimFrameAssign8Directions(0,    ZERGLING_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssign8Directions(5,    ZERGLING_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssign8Directions(10,    ZERGLING_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssign8Directions(15,    ZERGLING_IMAGE_START_AT+24,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameAssign8Directions(20,    ZERGLING_IMAGE_START_AT+32,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameAssign8Directions(25,    ZERGLING_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+40,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(30,    ZERGLING_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameAssign8Directions(35,    ZERGLING_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameAssign8Directions(40,    ZERGLING_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameAssign8Directions(45,    ZERGLING_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssign8Directions(50,    ZERGLING_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameAssign8Directions(55,    ZERGLING_IMAGE_START_AT+88,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+48,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+56,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+64,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+72,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+80,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions(         ZERGLING_IMAGE_START_AT+88,    IMAGE_SPRITE_MON_ACTION_WALK, 5);

    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+96,frames[60],IMAGE_SPRITE_MON_ACTION_SLAIN,0);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+97,frames[61],IMAGE_SPRITE_MON_ACTION_SLAIN,1);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+98,frames[62],IMAGE_SPRITE_MON_ACTION_SLAIN,2);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+99,frames[63],IMAGE_SPRITE_MON_ACTION_SLAIN,3);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+100,frames[64],IMAGE_SPRITE_MON_ACTION_SLAIN,4);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+101,frames[65],IMAGE_SPRITE_MON_ACTION_SLAIN,5);
    AnimFrameAssignAsSingleImage(ZERGLING_IMAGE_START_AT+102,frames[66],IMAGE_SPRITE_MON_ACTION_DUMMY,0);
}

void PlayerClassCommonWhenEntry()
{
    int imgVector = CreateImageVector(IMAGE_VECTOR_SIZE);

    initializeTextSprites(imgVector);
    initializeRawImages(imgVector);
    initializeImageNumber(imgVector);
    initializeImageZergZergling(imgVector);
    DoImageDataExchange(imgVector);
}

