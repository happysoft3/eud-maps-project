
#include "libs/define.h"

int WizardRedBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[3] = 0; arr[4] = 0; 
		arr[5] = 0; arr[6] = 0; arr[7] = 0; arr[8] = 0; arr[9] = 0; 
		arr[10] = 0; arr[11] = 0; arr[12] = 0; arr[13] = 0; arr[14] = 0; 
		arr[15] = 0; arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[20] = 0; arr[21] = 1065353216; arr[22] = 0; arr[23] = 40; arr[24] = 1067869798; 
		arr[25] = 0; arr[26] = 4; arr[27] = 4; arr[53] = 1128792064; arr[54] = 4;
		link = arr;
	}
	return link;
}

int LichLord2BinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; 
		arr[27] = 7; arr[28] = 1108082688; arr[29] = 50; arr[30] = 1106247680; arr[31] = 11; 
		arr[32] = 13; arr[33] = 21; arr[59] = 5542784;
		link = arr;
	}
	return link;
}

int GreenFrogBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701147207; arr[1] = 1869760110; arr[2] = 103; arr[24] = 1065353216; arr[28] = 1101004800; 
		arr[29] = 20; arr[31] = 10; arr[32] = 6; arr[33] = 11; arr[59] = 5544320;
		link = arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 85; arr[18] = 25; 
		arr[19] = 90; arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; arr[37] = 1801545047; 
		arr[38] = 1701996870; arr[39] = 1819042146; arr[53] = 1128792064; arr[55] = 15; arr[56] = 21; 
		arr[58] = 5545472;
		link = arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575; arr[24] = 1069547520; arr[26] = 4; 
		arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; arr[31] = 8; arr[32] = 13; 
		arr[33] = 21; arr[34] = 50; arr[35] = 3; arr[36] = 9; arr[37] = 1684631635; 
		arr[38] = 1884516965; arr[39] = 29801; arr[53] = 1128792064; arr[55] = 20; arr[56] = 28; 
		arr[59] = 5544896; arr[61] = 45071360;
		link = arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[19] = 80; arr[24] = 1066192077; arr[26] = 4; arr[28] = 1106247680; 
		arr[29] = 25; arr[30] = 1092616192; arr[31] = 4; arr[32] = 22; arr[33] = 30; 
		arr[34] = 2; arr[35] = 3; arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; 
		arr[59] = 5543680;
		link = arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[19] = 1; arr[24] = 1065772646; arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984; 
		link = arr;
	}
	return link;
}

int FishBigBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1752394054; arr[1] = 6777154; arr[24] = 1066611507; arr[27] = 1; arr[28] = 1101004800; arr[29] = 35; 
		arr[31] = 3; arr[32] = 7; arr[33] = 15; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int RatBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 7627090; arr[24] = 1065353216; arr[28] = 1101004800; arr[29] = 15; arr[31] = 16; 
		arr[32] = 13; arr[33] = 21; arr[59] = 5544320; 
		link = arr;
	}
	return link;
}

int BomberGreenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339074; arr[1] = 1917284965; arr[2] = 7234917; arr[24] = 1065353216; arr[37] = 1769236816; 
		arr[38] = 1181513062; arr[39] = 1650815593; arr[40] = 7105633; arr[53] = 1133903872; arr[55] = 8; 
		arr[56] = 14; arr[58] = 5547856; 
		link = arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811; arr[24] = 1065353216; 
		arr[26] = 4; arr[28] = 1101004800; arr[29] = 20; arr[31] = 8; arr[32] = 8; 
		arr[33] = 16; arr[57] = 5548112; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[24] = 1065688760; arr[27] = 1; arr[28] = 1106247680; 
		arr[29] = 35; arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30; 
		arr[58] = 5546320; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int HecubahWithOrbBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1969448264; arr[1] = 1466458466; arr[2] = 1332245609; arr[3] = 25202; arr[17] = 250; 
		arr[18] = 100; arr[19] = 90; arr[21] = 1065353216; arr[24] = 1066192077; arr[25] = 1; 
		arr[26] = 6; arr[27] = 5; arr[37] = 1952539972; arr[38] = 1818313320; arr[39] = 1634879084; 
		arr[40] = 1852140903; arr[41] = 116; arr[53] = 1133903872; arr[55] = 19; arr[56] = 25; 
		link = arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265; arr[17] = 130; 
		arr[18] = 100; arr[19] = 60; arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 20; arr[31] = 8; arr[32] = 12; 
		arr[33] = 20; arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link = arr;
	}
	return link;
}
