
#include "libs/define.h"
#include "libs\printutil.h"
#include "libs\waypoint.h"
#include "libs\buff.h"
#include "libs\spellutil.h"
#include "libs\playerupdate.h"
#include "libs\itemproperty.h"
#include "libs\weaponcapacity.h"
#include "libs\potionpickup.h"
#include "libs\coopteam.h"
#include "libs/sound_define.h"
#include "libs\fxeffect.h"
#include "libs\mathlab.h"
#include "libs\network.h"
#include "libs\wandpatch.h"
#include "libs\potionex.h"
#include "libs/format.h"
#include "libs/cweaponproperty.h"
#include "libs/fixtellstory.h"

#define MAZE_ENTRY_POINT 1000

int RewardHead;
int MissionEnd = 0;
int FirstScrCreatedUnit;
int CountMaxNpc = 65;
int NOW_STAGE = 0; //stage
int player[30], Guardian[65]; //npc_array
int DEATH_COUNT; //current_deaths
int MAX_DEATHS; //max_deaths
int PlayerDeathCount, OverDeathCount;


int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = PotionExCreateYellowPotion(GetObjectX(unit), GetObjectY(unit), 125);
    else if (thingID == 640)
        x = PotionExCreateWhitePotion(GetObjectX(unit), GetObjectY(unit), 100);
    else if (thingID == 641)
        x = PotionExCreateBlackPotion(GetObjectX(unit), GetObjectY(unit), 85);
    if (x ^ unit) Delete(unit);
    PotionPickupRegist(x);

    return x;
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int BomberGreenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1651339074; arr[1] = 1917284965; arr[2] = 7234917; arr[17] = 20; arr[19] = 80; 
		arr[21] = 1065353216; arr[24] = 1065353216; arr[37] = 1769236816; arr[38] = 1181513062; arr[39] = 1650815593; 
		arr[40] = 7105633; arr[53] = 1128792064; arr[55] = 9; arr[56] = 15; arr[57] = 5548112; 
		arr[58] = 5546320; 
		link = &arr;
	}
	return link;
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 20; arr[19] = 100; arr[21] = 1065353216; 
		arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; arr[28] = 1108082688; 
		arr[29] = 50; arr[30] = 1092616192; arr[32] = 9; arr[33] = 17; arr[57] = 5548288; 
		arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4; arr[53] = 1128792064; arr[54] = 4;
		link = &arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;

	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261; arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
        arr[58] = 5546320; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20; arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = &arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146; arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984; 
		link = &arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[22] = 0; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[27] = 0; arr[28] = 1082130432; arr[29] = 20; 
		arr[30] = 0; arr[31] = 2; arr[32] = 8; arr[33] = 16;
        arr[57] = 5548112; arr[58] = 0; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064; arr[55] = 20; arr[56] = 28;arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = &arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972; arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064; arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = &arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
        arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
        arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link = &arr;
	}
	return link;
}

int WoundedApprenticeBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 50; arr[18] = 10; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[28] = 1112014848; arr[29] = 40; 
		arr[30] = 1106247680; arr[31] = 2; arr[32] = 12; arr[33] = 20;
		arr[57] = 5548112; arr[58] = 0; arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        uec = GetMemory(ptr + 0x2ec);
        if (uec)
        {
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
            SetMemory(uec + 0x528, ToInt(1.0));
            SetMemory(uec + 0x520, ToInt(300.0));
            uec += 0x5d0;
            SetMemory(uec + GetSpellNumber("SPELL_MAGIC_MISSILE"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_STUN"), 0x20000000);
            SetMemory(uec + GetSpellNumber("SPELL_SHOCK"), 0x10000000);
            SetMemory(uec + GetSpellNumber("SPELL_FIREBALL"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_BURN"), 0x40000000);
            SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x08000000);
            SetMemory(uec + GetSpellNumber("SPELL_COUNTERSPELL"), 0x08000000);
        }
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

void MonsterWoundedApprentice(int unit)
{
    //TODO: Index. 9, ThingName= WoundedApprentice
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedApprenticeBinTable());
        SetUnitMaxHealth(unit, 230);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}

void ExitRespawnRoom()
{
    int dstLocation;

    if (CurrentHealth(OTHER))
    {
        dstLocation = GetTrigger() + 2;
        MoveObject(OTHER, GetObjectX(dstLocation), GetObjectY(dstLocation));
        UniPrint(OTHER, "이동하였습니다");
    }
}

void CreateExit(int srcLocation, int dstLocation)
{
    int unit = CreateObject("Maiden", srcLocation);
    Frozen(CreateObject("GauntletExitA", srcLocation), 1);
    CreateObject("InvisibleLightBlueHigh", dstLocation);
    Frozen(unit, 1);
    SetCallback(unit, 9, ExitRespawnRoom);
}

void DecreaseDeathCount()
{
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (PlayerDeathCount)
        {
            if (GetGold(OTHER) >= 20000)
            {
                PlayerDeathCount --;
                if (PlayerDeathCount < MAZE_ENTRY_POINT && OverDeathCount)
                    OverDeathCount = 0;
                ChangeGold(OTHER, -20000);
                UniPrint(OTHER, "거래성공! 데스 카운트를 1 낮췄습니다. [데스카운트: " + IntToString(PlayerDeathCount) + "]");
            }
            else
                UniPrint(OTHER, "거래실패! 잔액이 부족합니다");
        }
        else
            UniPrint(OTHER, "누적된 데스가 아직 0 입니다! 따라서 이 작업은 의미가 없습니다");
    }
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.2);
        UniPrint(OTHER, "누적 데스카운트를 1 낮춥니다. 2만원이 필요해요");
    }
}

int PutDeathCountShop(int wp)
{
    int unit = CreateObject("Maiden", wp);

    Frozen(CreateObject("Ankh", wp), 1);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);
    SetDialog(unit, "aa", DecreaseDeathCount, DecreaseDeathCount);
    return unit;
}

void BuyDemonsWand()
{
    if (HasEnchant(OTHER, "ENCHANT_AFRAID"))
    {
        EnchantOff(OTHER, "ENCHANT_AFRAID");
        if (GetGold(OTHER) >= 15000)
        {
            int wand = CreateObjectAt("DemonsBreathWand", GetObjectX(OTHER), GetObjectY(OTHER));

            SetConsumablesWeaponCapacity(wand, 255, 255);
            ChangeGold(OTHER, -15000);
            UniPrint(OTHER, "용의 숨결 지팡이를 구입하였습니다");
        }
        else
        {
            UniPrint(OTHER, "거래가 취소되었습니다");
        }
        
    }
    else
    {
        Enchant(OTHER, "ENCHANT_AFRAID", 0.2);
        UniPrint(OTHER, "용의 숨결 지팡이를 구입합니다. 15000 골드가 필요");
    }
}

void PutDemonsWandMarket(int locationId)
{
    int market = DummyUnitCreate("WizardWhite", locationId);

    SetDialog(market, "aa", BuyDemonsWand, BuyDemonsWand);
}

void PlayerDeath()
{
    UniPrintToAll("플레이어가 죽었습니다 [누적 데스 카운트: " + IntToString(++PlayerDeathCount) + "]");
    if (OverDeathCount) return;

    if (PlayerDeathCount >= MAZE_ENTRY_POINT)
    {
        OverDeathCount = 1;
        UniPrintToAll("플레이어 데스 카운트가 " + IntToString(MAZE_ENTRY_POINT) + " 회를 넘어섰습니다, 이제부터는 매크로 방지를 위해 메이즈에서 리스폰됩니다");
    }
}

void BlueDoomTouched()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER))
    {
        if (CurrentHealth(owner) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 135, 14);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.3);
        }
    }
}

int DummyUnitCreate(string name, int wp)
{
    int unit = CreateObject(name, wp);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, 1);

    return unit;
}

void DummyUnitCreateAtEx(string name, float xpos, float ypos, int *pDest)
{
    int dum=CreateObjectAt(name, xpos, ypos);

    ObjectOff(dum);
    Damage(dum, 0, MaxHealth(dum) + 1, -1);
    Frozen(dum, TRUE);

    if (pDest)
        pDest[0] = dum;
}

void FlySkill(int ptr)
{
    int owner = GetOwner(ptr), ltime = GetDirection(ptr), coll;

    while (1)
    {
        if (CurrentHealth(owner))
        {
            if (ltime && (IsVisibleTo(ptr + 1, ptr) || IsVisibleTo(ptr, ptr + 1)))
            {
                MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr), GetObjectY(ptr) + GetObjectZ(ptr + 1));
                MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
                coll = DummyUnitCreate("Demon", 1);
                SetOwner(owner, CreateObject("InvisibleLightBlueLow", 1));
                SetCallback(coll, 9, BlueDoomTouched);
                DeleteObjectTimer(coll, 1);
                DeleteObjectTimer(coll + 1, 1);
                AudioEvent("WallCast", 1);
                GreenSparkFx(1);
                LookWithAngle(ptr, ltime - 1);
                FrameTimerWithArg(1, ptr, FlySkill);
                break;
            }
        }
        Delete(ptr);
        Delete(++ptr);
        break;
    }
}

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr1 = GetMemory(0x750710), k, num;

    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    Delete(unit + 1);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e4, MaidenBinTable());
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));

    return unit;
}

void TargetDuration(int ptr)
{
	int count = GetDirection(ptr), enemy = ToInt(GetObjectZ(ptr)), owner = GetOwner(ptr);

	while (1)
	{
		if (CurrentHealth(enemy) && CurrentHealth(owner))
		{
			if (count)
			{
				MoveObject(ptr, GetObjectX(enemy), GetObjectY(enemy));
				LookWithAngle(ptr, count - 1);
				FrameTimerWithArg(1, ptr, TargetDuration);
				break;
			}
		}
		Delete(ptr);
		break;
	}
}

void TargetUnitTag(int owner, int target)
{
	int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(target), GetObjectY(target));

    GiveCreatureToPlayer(owner, unit);
	Raise(unit, target);
	LookWithAngle(unit, 240);
	FrameTimerWithArg(1, unit, TargetDuration);
}

void PlayerSetTag(int target)
{
    int i;
    for (i = 9 ; i >= 0 ; i --)
    {
        if (CurrentHealth(player[i]))
            TargetUnitTag(player[i], target);
    }
}

void MapInitialize()
{
    MusicEvent();
    
    ObjectOn(Object("playerStartButton"));
    FirstScrCreatedUnit = CreateObject("InvisibleLightBlueHigh", 98);
    initializeStruct(-1);
    SearchInitNpc(Object("stage1War4"));
    FrameTimerWithArg(1, 98, TeleportDecoration);
    FrameTimer(10, DelayRunFunction);
    FrameTimer(2, MakeCoopTeam);
    InitSignDescription();
    UseMapSetting();

    PutDemonsWandMarket(108);

    FixColorPotionThingDbSection(639, 125);
    FixColorPotionThingDbSection(641, 85);

    FrameTimer(30, PlacingSpecialMarket);
}

void TeleportDecoration(int wp)
{
    int k;

    CreateExit(105, 2);
    PutDeathCountShop(106);
    for (k = 7 ; k >= 0 ; k --)
        CreateObject("PlayerWaypoint", wp);
}

void DelayRunFunction()
{
    PlayerClassLoop();
    FrameTimerWithArg(30, Random(0, CountMaxNpc - 1), DisplayEnemyPos);
}

void initializeStruct(int arg_0)
{
    countofCreatures(arg_0);
    UnitFlagsTable(arg_0);
    countofFieldUnits(arg_0);
}

int CheckPlayer()
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (IsCaller(player[k]))
            return k;
    }
    return -1;
}

void EmptyInventory(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

void PlayerClassOnInit(int plr, int pUnit)
{
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    if (ValidPlayerCheck(pUnit))
    {
        if (pUnit ^ GetHost())
            NetworkUtilClientEntry(pUnit);
        else
            PlayerClassCommonWhenEntry();
    }
    player[plr] = pUnit;
    player[plr + 10] = 1;
    ChangeGold(pUnit, -GetGold(pUnit));
}

void catchPlayers()
{
    int k, plr;

    while (1)
    {
        if (CurrentHealth(OTHER))
        {
            plr = CheckPlayer();
            for (k = 9 ; k >= 0 && plr < 0 ; k --)
            {
                if (!MaxHealth(player[k]))
                {
                    PlayerClassOnInit(k, GetCaller());
                    plr = k;
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerClassOnJoin(plr);
                break;
            }
        }
        PlayerCantJoin();
        break;
    }
}

int PlayerClassDeathFlagCheck(int plr)
{
    return player[plr + 10] & 0x80;
}

void PlayerClassDeathFlagSet(int plr)
{
    player[plr + 10] = player[plr + 10] ^ 0x80;
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

void PlayerClassOnUseSkillCheck(int plr)
{
    if (player[plr + 10] & 2)
    {
        if (UnitCheckEnchant(player[plr], GetLShift(31)))
        {
            EnchantOff(player[plr], EnchantList(31));
            RemoveTreadLightly(player[plr]);
            castWindbooster(plr);
        }
    }
}

void PlayerClassOnFree(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassOnUseSkillCheck(i);
                    break;
                }
                else
                {
                    if (PlayerClassDeathFlagCheck(i)) break;
                    else
                    {
                        PlayerClassDeathFlagSet(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnFree(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassLoop);
}

void PlayerClassOnJoin(int plr)
{
    Enchant(player[plr], "ENCHANT_ANCHORED", 0.0);
    if (PlayerClassDeathFlagCheck(plr))
        PlayerClassDeathFlagSet(plr);
    if (player[plr + 10] & 4)
        Enchant(player[plr], "ENCHANT_CROWN", 0.0);
    if (OverDeathCount)
        MoveObject(player[plr], LocationX(104), LocationY(104));
    else
        MoveObject(player[plr], LocationX(2), LocationY(2));
    DeleteObjectTimer(CreateObjectAt("BlueRain", GetObjectX(player[plr]), GetObjectY(player[plr])), 17);
}

void PlayerCantJoin()
{
    Enchant(OTHER, "ENCHANT_FREEZE", 0.0);
    Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(OTHER, "ENCHANT_ANCHORED", 0.0);
    MoveObject(OTHER, LocationX(101), LocationY(101));
    UniPrintToAll("맵 최대 정원 10명을 초과하여 입장하실 수 없습니다, 잠시 후 다시 시도하세요");
}

void LookForEnemy()
{
    float var_0;

    if (CurrentHealth(SELF))
    {
        if (HasEnchant(SELF, "ENCHANT_PROTECT_FROM_FIRE"))
        {
            var_0 = Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            if (var_0 < 250.0)
            {
                MoveWaypoint(19, GetObjectX(SELF), GetObjectY(SELF));
                AudioEvent("WarcryInvoke", 19);
                Enchant(SELF, "ENCHANT_FREEZE", 1.5);
                Enchant(OTHER, "ENCHANT_ANTI_MAGIC", 5.0);
                Effect("CHARM", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            }
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 15.0);
            CreatureFollow(SELF, OTHER);
            AggressionLevel(SELF, 0.83);
        }
    }
}

void RewardMarkerDropItemHandler(int wp, int mark)
{
    int unit = CallFunctionWithArgInt(FieldItemFuncPtr(Random(0, 9)), wp);

    SetOwner(mark, CreateObject("InvisibleLightBlueLow", wp));  //TODO: Linked marker
    RegistItemPickupCallback(unit, RewardItemPick);
}

void DropItemHandler(int wp)
{
    CallFunctionWithArgInt(FieldItemFuncPtr(Random(0, 9)), wp);
}

void setDeaths() //TODO: Callback::NPC DeathEvent
{
    int rnd = Random(0, 5);

    //Waypoint: 94
    MoveWaypoint(94, GetObjectX(SELF), GetObjectY(SELF));
    DropItemHandler(94);
    removeEquipments(GetTrigger());
    AudioEvent("TreasureDrop", 94);
    DeleteObjectTimer(SELF, 30);
    DEATH_COUNT ++;
    controlStage();
}

void removeEquipments(int unit)
{
	int inven = unit + 2;
	
	while (HasClass(inven, "WEAPON") || HasClass(inven, "ARMOR"))
    {
		Delete(inven);
		inven += 2;
	}
}

void hurtNPC()
{
    if (HasClass(OTHER, "MISSILE") && HasSubclass(OTHER, "IMMUNE_FEAR"))
        Delete(OTHER);
}

void perceiveToEnemy()
{
    if (CurrentHealth(SELF) && !HasEnchant(SELF, "ENCHANT_CROWN") && IsVisibleTo(SELF, OTHER))
    {
        Enchant(SELF, "ENCHANT_CROWN", 0.5);
        if (MaxHealth(SELF) == 200) warPatton();
        else if (HasEnchant(SELF, "ENCHANT_PROTECT_FROM_ELECTRICITY")) conjurerPatton();
        else if (HasEnchant(SELF, "ENCHANT_PROTECT_FROM_POISON")) wizardPatton();
    }
}
void touchedNpc()
{
    if (CurrentHealth(SELF) && HasEnchant(SELF, "ENCHANT_PROTECT_FROM_FIRE") && HasEnchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC"))
    {
        if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
        {
            Damage(OTHER, SELF, 150, 2);
            Enchant(OTHER, "ENCHANT_VILLAIN", 0.5);
            EnchantOff(SELF, "ENCHANT_PROTECT_FROM_MAGIC");
        }
    }
}
string NpcClassName(int idx)
{
    string cName[] = {"War", "Con", "Wiz"};
    
    return cName[idx];
}

int amountWar(int currentWave)
{
    int warAmount[7] = {
        7, 8, 9,
        10, 11, 12, 13
    };

    return warAmount[currentWave];
}

int amountCon(int currentWave)
{
    int conAmount[7] = {
        6, 6, 7,
        7, 8, 8, 9
    };

    return conAmount[currentWave];
}
int amountWiz(int currentWave)
{
    int wizAmount[] = {
        5, 6, 7,
        7, 8, 8, 8
    };

    return wizAmount[currentWave];
}

void initializeNPC(int arg_0)
{
    if (arg_0 < 3)
        initializeSettingNPC(arg_0);
    else
    {
        MAX_DEATHS = setLessons(NOW_STAGE);
        NOW_STAGE ++;
        respawnNPC(0);
    }
}
int checkNPCType(int arg_0, int arg_1)
{
    if (!arg_0)
        return amountWar(arg_1);
    else if (arg_0 == 1)
        return amountCon(arg_1);
    else if (arg_0 == 2)
        return amountWiz(arg_1);
}

void initializeSettingNPC(int arg_0)
{
    string cName = NpcClassName(arg_0);
    int count, npcUnit;

    if (count < checkNPCType(arg_0, NOW_STAGE))
    {
        npcUnit = Object("stage" + IntToString(NOW_STAGE + 1) + cName + IntToString(count + 1));
        Frozen(npcUnit, 0);
        ObjectOff(npcUnit);
        npcProperties(npcUnit);
        inputNpcVar(npcUnit);
        count ++;
        FrameTimerWithArg(1, arg_0, initializeSettingNPC);
    }
    else
    {
        count = 0;
        FrameTimerWithArg(1, arg_0 + 1, initializeNPC);
    }
}
void npcProperties(int arg_0)
{
    if (CurrentHealth(arg_0))
    {
        if (MaxHealth(arg_0) == 200) Enchant(arg_0, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
        else
        {
            if (HasClass(GetLastItem(arg_0), "WAND"))
                Enchant(arg_0, "ENCHANT_PROTECT_FROM_POISON", 0.0);
            else
                Enchant(arg_0, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
        }
    }
}

void inputNpcVar(int arg_0)
{
    int k;

    Guardian[k] = arg_0;
    k = (k + 1) % 65;
}

void respawnNPC(int index)
{
    int count = index, rndLoc;

    if (count < 65)
    {
        if (CurrentHealth(Guardian[count]))
        {
            rndLoc = Random(22, 79);
            MoveObject(Guardian[count], LocationX(rndLoc), LocationY(rndLoc));
            ObjectOn(Guardian[count]);
            SetUnitScanRange(Guardian[count], 600.0);
            AggressionLevel(Guardian[count], 1.0);
            Wander(Guardian[count]);
        }
        FrameTimerWithArg(1, count + 1, respawnNPC);
    }
}

void warPatton()
{
    int var_0;
    float var_1;

    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        var_0 = Random(0, 1);
        if (!var_0)
        {
            var_1 = 10.0;
            tripleArrow(SELF);
        }
        else
        {
            var_1 = 20.0;
            Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 4.0);
			Enchant(SELF, "ENCHANT_ANTI_MAGIC", 0.0);
            
            LookAtObject(SELF, OTHER);
            injectorSpecialTable(GetTrigger());
        }
        Enchant(SELF, "ENCHANT_BURNING", var_1);
    }
}

void conjurerPatton()
{
    int var_0;
    float var_1 = 3.0;

    var_0 = Random(0, 6);
    if (!var_0)
    {
        CastSpellObjectLocation("SPELL_FIST", SELF, GetObjectX(OTHER) + RandomFloat(-12.0, 12.0), GetObjectY(OTHER) + RandomFloat(-12.0, 12.0));
        CastSpellObjectObject("SPELL_PIXIE_SWARM", SELF, SELF);
        var_1 = 5.0;
    }
    else if (var_0 == 1)
    {
        CastSpellObjectObject("SPELL_SLOW", SELF, OTHER);
        CastSpellObjectLocation("SPELL_METEOR", SELF, GetObjectX(OTHER) + RandomFloat(-12.0, 12.0), GetObjectY(OTHER) + RandomFloat(-12.0, 12.0));
        CastSpellObjectLocation("SPELL_BURN", SELF, GetObjectX(OTHER) + RandomFloat(-12.0, 12.0), GetObjectY(OTHER) + RandomFloat(-12.0, 12.0));
    }
    else if (var_0 == 2)
    {
        CastSpellObjectLocation("SPELL_TOXIC_CLOUD", SELF, GetObjectX(OTHER) + RandomFloat(-20.0, 20.0), GetObjectY(OTHER) + RandomFloat(-20.0, 20.0));
        CastSpellObjectObject("SPELL_PIXIE_SWARM", SELF, SELF);
    }
    else if (var_0 == 3)
    {
        CastSpellObjectLocation("SPELL_FORCE_OF_NATURE", SELF, GetObjectX(OTHER), GetObjectY(OTHER));
        FrameTimerWithArg(60, GetTrigger(), waitForTeleport);
    }
    else if (var_0 == 4)
    {
        var_0 = Random(0, 2);
        if (var_0 == 0)
            CastSpellObjectLocation("SPELL_SUMMON_GHOST", SELF, GetObjectX(SELF), GetObjectY(SELF));
        else if (var_0 == 1)
            CastSpellObjectLocation("SPELL_SUMMON_MECHANICAL_FLYER", SELF, GetObjectX(SELF), GetObjectY(SELF));
        else if (var_0 == 2)
            CastSpellObjectLocation("SPELL_SUMMON_EMBER_DEMON", SELF, GetObjectX(SELF), GetObjectY(SELF));
        CastSpellObjectLocation("SPELL_BURN", SELF, GetObjectX(OTHER) + RandomFloat(-12.0, 12.0), GetObjectY(OTHER) + RandomFloat(-12.0, 12.0));
        CastSpellObjectLocation("SPELL_FIREBALL", SELF, GetObjectX(OTHER) + RandomFloat(-12.0, 12.0), GetObjectY(OTHER) + RandomFloat(-12.0, 12.0));
        FrameTimerWithArg(60, GetTrigger(), waitForTeleport);
    }
    else if (var_0 == 5)
    {
        CastSpellObjectLocation("SPELL_METEOR", SELF, GetObjectX(OTHER) + RandomFloat(-12.0, 12.0), GetObjectY(OTHER) + RandomFloat(-12.0, 12.0));
        LookAtObject(SELF, OTHER);
        castSpellForConjurer(SELF);
    }
    else if (var_0 == 6)
    {
        CastSpellObjectObject("SPELL_SLOW", SELF, OTHER);
        CastSpellObjectLocation("SPELL_TOXIC_CLOUD", SELF, GetObjectX(OTHER) + RandomFloat(-20.0, 20.0), GetObjectY(OTHER) + RandomFloat(-20.0, 20.0));
        LookAtObject(SELF, OTHER);
        castSpellForConjurer(SELF);
        FrameTimerWithArg(50, GetTrigger(), waitForTeleport);
        var_1 = 5.0;
    }
    Enchant(SELF, "ENCHANT_VILLAIN", var_1);
}

void wizardPatton()
{
    int var_0;
    int var_1;

    if (!HasEnchant(SELF, "ENCHANT_VILLAIN") && !HasEnchant(SELF, "ENCHANT_ANTI_MAGIC"))
    {
        var_0 = Random(0, 4);
        if (var_0 == 0) {
            CastSpellObjectLocation("SPELL_FIREBALL", SELF, GetObjectX(OTHER), GetObjectY(OTHER));
            CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
        }
        else if (var_0 == 1) {
            CastSpellObjectObject("SPELL_SHOCK", SELF, SELF);
            CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
            MoveWaypoint(19, GetObjectX(OTHER), GetObjectY(OTHER));
            var_1 = CreateObject("PlayerWaypoint", 19);
            insertRay(GetTrigger(), var_1);
        }
        else if (var_0 == 2) {
            CastSpellObjectObject("SPELL_MAGIC_MISSILE", SELF, OTHER);
            CastSpellObjectObject("SPELL_SLOW", SELF, OTHER);
            FrameTimerWithArg(50, GetTrigger(), waitForTeleport);
        }
        else if (var_0 == 3) {
            CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
            CastSpellObjectObject("SPELL_CHAIN_LIGHTNING", SELF, OTHER);
            PauseObject(SELF, 50);
        }
        else if (var_0 == 4) {
            CastSpellObjectLocation("SPELL_BURN", SELF, GetObjectX(OTHER), GetObjectY(OTHER));
            CastSpellObjectObject("SPELL_CHAIN_LIGHTNING", SELF, OTHER);
            PauseObject(SELF, 50);
        }
        Enchant(SELF, "ENCHANT_VILLAIN", 3.0);
    }
}

int storageUnitIndex(int arg_0, int arg_1, int arg_2)
{
    //unit_id, mode, index
    int var_0[15];

    if (arg_1 == 0)
        var_0[arg_2] = arg_0;
    return var_0[arg_2];
}
float storageWarXY(float arg_0, int arg_1, int arg_2)
{
    //pos_x, mode, index
    float var_0[30];

    if (arg_1 == 0)
        var_0[arg_2] = arg_0;
    return var_0[arg_2];
}

int storageUnitDirect(int arg_0, int arg_1, int arg_2)
{
    int var_0[15];

    if (arg_1 == 0)
        var_0[arg_2] = arg_0;
    return var_0[arg_2];
}

void injectorSpecialTable(int unit)
{
    int k;
    float x_vect;
    float y_vect;

    x_vect = UnitAngleCos(unit, 20.0);
    y_vect = UnitAngleSin(unit, 20.0);
    storageUnitIndex(unit, 0, k);
    storageWarXY(x_vect, 0, k);
    storageWarXY(y_vect, 0, k + 15);
    storageUnitDirect(GetDirection(unit), 0, k);
    FrameTimerWithArg(1, k, receivedSpecialVar);
    k = (k + 1) % 15;
}

void receivedSpecialVar(int arg_0)
{
    int k;
    int var_1;
    float x_vect;
    float y_vect;
    int var_4;

    var_1 = storageUnitIndex(0, 1, k);
    x_vect = storageWarXY(0.0, 1, k);
    y_vect = storageWarXY(0.0, 1, k + 15);
    var_4 = storageUnitDirect(0, 1, k);
    berserkerCharge(var_1, x_vect, y_vect, var_4);
    if (HasEnchant(var_1, "ENCHANT_PROTECT_FROM_MAGIC"))
        injectorSpecialTable(var_1);
    k = (k + 1) % 15;
}

void berserkerCharge(int arg_0, float x_vect, float y_vect, int arg_3)
{
    int check_unit;

    if (CurrentHealth(arg_0))
    {
        if (HasEnchant(arg_0, "ENCHANT_ANTI_MAGIC"))
        {
            MoveWaypoint(81, GetObjectX(arg_0), GetObjectY(arg_0));
            AudioEvent("BerserkerChargeInvoke", 81);
            EnchantOff(arg_0, "ENCHANT_ANTI_MAGIC");
        }
        MoveWaypoint(81, GetObjectX(arg_0) + x_vect, GetObjectY(arg_0) + y_vect);
        check_unit = CreateObject("Ghost", 81);
        if (IsVisibleTo(arg_0, check_unit))
        {
            MoveObject(arg_0, LocationX(81), LocationY(81));
            LookWithAngle(arg_0, arg_3);
        }
        Delete(check_unit);
    }
}

void tripleArrow(int arg_0)
{
    float x_vect;
    float y_vect;
    int var_2[9];
    float var_3 = 0.1;
    int k = 0;

    if (CurrentHealth(arg_0))
    {
        x_vect = UnitAngleCos(arg_0, -18.0); //dx
        y_vect = UnitAngleSin(arg_0, -18.0); //dy
        MoveWaypoint(19, GetObjectX(arg_0) - x_vect, GetObjectY(arg_0) - y_vect);
        var_2[8] = CreateObject("FanChakramInMotion", 19);
        while (k < 4)
        {
            MoveWaypoint(19, GetObjectX(arg_0) + (var_3 * y_vect) - x_vect, GetObjectY(arg_0) - (var_3 * x_vect) - y_vect);
            var_2[k * 2] = CreateObject("FanChakramInMotion", 19);
            MoveWaypoint(19, GetObjectX(arg_0) - (var_3 * y_vect) - x_vect, GetObjectY(arg_0) + (var_3 * x_vect) - y_vect);
            var_2[k * 2 + 1] = CreateObject("FanChakramInMotion", 19);
            var_3 += 0.1;
            k ++;
        }
        for(k = 0 ; k < 9 ; k ++)
        {
            SetOwner(arg_0, var_2[k]);
            LookAtObject(var_2[k], arg_0);
            LookWithAngle(var_2[k], GetDirection(var_2[k]) + 128);
            PushObject(var_2[k], 25.0, GetObjectX(arg_0), GetObjectY(arg_0));
        }
    }
}

void waitForTeleport(int arg_0)
{
    int var_0 = Random(3, 18);

    if (CurrentHealth(arg_0))
    {
        Effect("TELEPORT", GetObjectX(arg_0), GetObjectY(arg_0), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(arg_0), GetObjectY(arg_0), 0.0, 0.0);
        MoveObject(arg_0, LocationX(var_0), LocationY(var_0));
        Effect("TELEPORT", GetObjectX(arg_0), GetObjectY(arg_0), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(arg_0), GetObjectY(arg_0), 0.0, 0.0);
        AudioEvent("BlindOff", var_0);
    }
}
void castSpellForConjurer(int arg_0)
{
    int var_0;

    if (CurrentHealth(arg_0))
    {
        MoveWaypoint(19, GetObjectX(arg_0) - UnitRatioX(arg_0, OTHER, 16.0), GetObjectY(arg_0) - UnitRatioY(arg_0, OTHER, 16.0));
        var_0 = CreateObject("ArcherBolt", 19);
        SetOwner(arg_0, var_0);
        LookAtObject(var_0, arg_0);
        LookWithAngle(var_0, GetDirection(var_0) + 128);
        HitFarLocation(arg_0, LocationX(19), LocationY(19));
        PushObject(var_0, 50.0, GetObjectX(arg_0), GetObjectY(arg_0));
        AudioEvent("CrossBowShoot", 19);
    }
}

int storageRayTarget(int val, int mode, int index)
{
     //val, mode, index
    int arr[10];

    if (!mode)
        arr[index] = val;
    return arr[index];
}

void insertRay(int arg_0, int arg_1)
{
    int k;

    storageRayTarget(arg_1, 0, k);
    FrameTimerWithArg(3, arg_0, TargetRay);
    k = (k + 1) % 10;
}

void TargetRay(int arg_0)
{
    int k;
    int target;

    target = storageRayTarget(0, 1, k);
    CastSpellObjectLocation("SPELL_DEATH_RAY", arg_0, GetObjectX(target), GetObjectY(target));
    Delete(target);

    k = (k + 1) % 10;
}

void EndSearching(int arg)
{
    WallOpen(3211441);
    controlStage();
    ControlPutSupply();
    FrameTimer(10, StrWellNotify);
    FrameTimer(100, LoopSearchIndex);
    UniPrintToAll("검색된 " + IntToString(arg) + " 개의 Guardian 유닛이 처리 완료되었습니다");
    //FrameTimerWithArg(1, RewardHead, UpdateRewardMarker);
    FrameTimerWithArg(1, Object("FirstMobMarker"), TempSearchMonsterMarker);
}

int GetPrevNode(int cur)
{
    return GetOwner(cur);
}

int GetNextNode(int cur)
{
    return ToInt(GetObjectZ(cur));
}

void SetPrevNode(int cur, int tg)
{
    SetOwner(tg, cur);
}

void SetNextNode(int cur, int tg)
{
    Raise(cur, tg);
}

int AddNewList(int prevNode, int wp)
{
    int unit = CreateObject("InvisibleLightBlueLow", wp);

    if (prevNode)
    {
        SetNextNode(prevNode, unit);
    }
    LookWithAngle(unit, 1);
    return unit;
}

int ProcessRewardMarker(int unit)
{
    int node, prevNode;

    if (GetUnitThingID(unit) == 2672)
    {
        MoveWaypoint(1, GetObjectX(unit), GetObjectY(unit));
        node = AddNewList(prevNode, 1);
        if (!RewardHead)
            RewardHead = node;
        prevNode = node;
        //DropItemHandler(1);
        Delete(unit);
    }
}

void WispDeathFx(int wp)
{
    int unit = CreateObject("WillOWisp", wp);

    UnitNoCollide(unit);
    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    DeleteObjectTimer(unit, 9);
}

void RewardItemPick()
{
    int ptr = GetTrigger() + 1;
    int node = GetOwner(ptr);

    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
    LookWithAngle(node, 1);
    Delete(ptr);
}

void UpdateRewardMarker(int node)
{
    if (IsObjectOn(node))
    {
        if (GetDirection(node))
        {
            LookWithAngle(node, 0);
            MoveWaypoint(1, GetObjectX(node), GetObjectY(node));
            RewardMarkerDropItemHandler(1, node);
        }
        FrameTimerWithArg(1, GetNextNode(node), UpdateRewardMarker);
    }
}

void MarkResetCount(int mark)
{
	int count = GetDirection(mark);

	if (count)
	{
		LookWithAngle(mark, count - 1);
		SecondTimerWithArg(1, mark, MarkResetCount);
	}
	else
	{
        FrameTimerWithArg(75, mark, InitMonsterMarker);
	}
}

void FieldMonsterDeath()
{
	int mark = ToInt(GetObjectZ(GetTrigger() + 1));
	int count = GetDirection(mark + 1), max = ToInt(GetObjectZ(mark + 1));

    MoveWaypoint(98, GetObjectX(SELF), GetObjectY(SELF));
    DeleteObjectTimer(SELF, 60);
    DropItemHandler(98);
	if (count + 1 < max)
	{
		LookWithAngle(mark + 1, count + 1);
	}
	else
	{
		LookWithAngle(mark, ToInt(GetObjectZ(mark)));
		SecondTimerWithArg(1, mark, MarkResetCount);
	}
}

void CommonFieldMonsterProperty(int unit)
{
    RetreatLevel(unit, 0.0);
    AggressionLevel(unit, 1.0);
    SetCallback(unit, 5, FieldMonsterDeath);
}

int MonsterCreatePtr()
{
    StopScript(MonsterCreateBlackWolf);
}

void MonsterCreateBlackWolf(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("BlackWolf", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 225);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateBlackBear(int mark)
{
	int count = GetDirection(mark), unit, amount = 2;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("BlackBear", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 306);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateBear(int mark)
{
	int count = GetDirection(mark), unit, amount = 2;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Bear", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 325);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateOgreLord(int mark)
{
	int count = GetDirection(mark), unit, amount = 2;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("OgreWarlord", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 325);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateOgreAxe(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("GruntAxe", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 225);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateFireFairy(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;
    int ptr;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("FireSprite", 1);
        ptr = GetMemory(0x750710);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 98);
        SetUnitSpeed(unit, 2.2);
        if (ptr)
        {
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(57));
            SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
        }
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateMeleeDemon(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("MeleeDemon", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 135);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateShade(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Shade", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 160);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateSwordsman(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Swordsman", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 325);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateArcher(int mark)
{
	int count = GetDirection(mark), unit, amount = 5;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Archer", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 98);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateDryad(int mark)
{
	int count = GetDirection(mark), unit, amount = 2;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("WizardGreen", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 175);
        Enchant(unit, "ENCHANT_ANCHORED", 0.0);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateGoon(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;
    int ptr;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Goon", 1);
        ptr = GetMemory(0x750710);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 160);
        if (ptr)
        {
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));
        }
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterBlackSpider(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;
    int ptr;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("BlackWidow", 1);
        ptr = GetMemory(0x750710);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 250);
        if (ptr)
        {
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));
        }
        CommonFieldMonsterProperty(unit);
        SetCallback(unit, 5, BlackSpiderDead);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateSkeletonLord(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("SkeletonLord", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 295);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateSkeleton(int mark)
{
	int count = GetDirection(mark), unit, amount = 5;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Skeleton", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 250);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateScorpion(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Scorpion", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 275);
        CommonFieldMonsterProperty(unit);
        SetCallback(unit, 3, ReleasePoison);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateGargoyle(int mark)
{
	int count = GetDirection(mark), unit, amount = 5;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("EvilCherub", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 98);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateVileZombie(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("VileZombie", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 325);
        CommonFieldMonsterProperty(unit);
        SetCallback(unit, 5, ZombieDead);
        SetUnitSpeed(unit, 2.5);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateNormalZombie(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Zombie", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 225);
        CommonFieldMonsterProperty(unit);
        SetUnitSpeed(unit, 1.5);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateOgre(int mark)
{
	int count = GetDirection(mark), unit, amount = 4;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("OgreBrute", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 295);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateNecro(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Necromancer", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 295);
        CommonFieldMonsterProperty(unit);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8020); //TODO: disable casting a magic and always running
        SetCallback(unit, 3, NecromancerWeapon);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateMecaGolem(int mark)
{
	int count = GetDirection(mark), unit, amount = 1;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("MechanicalGolem", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 600);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateCrazyGirl(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = ColorMaiden(Random(0, 255), Random(0, 255), Random(0, 255), 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 275);
        CommonFieldMonsterProperty(unit);
        SetUnitSpeed(unit, 1.3);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateFireDemon(int mark)
{
	int count = GetDirection(mark), unit, amount = 2;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("EmberDemon", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 98);
        CommonFieldMonsterProperty(unit);
        SetUnitSpeed(unit, 1.3);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateUrchin(int mark)
{
	int count = GetDirection(mark), unit, amount = 6;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Urchin", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 98);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateImp(int mark)
{
	int count = GetDirection(mark), unit, amount = 6;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Imp", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 64);
        CommonFieldMonsterProperty(unit);
        Enchant(unit, "ENCHANT_SLOWED", 0.0);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateBat(int mark)
{
	int count = GetDirection(mark), unit, amount = 6;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Bat", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 80);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateBomber(int mark)
{
    string bomberName[] = {"Bomber", "BomberBlue", "BomberGreen", "BomberYellow"};
	int count = GetDirection(mark), unit, amount = 4, ptr;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject(bomberName[Random(0, 3)], 1);
        ptr = GetMemory(0x750710);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 98);
        SetMemory(ptr + 0x2b8, 0x4e83b0);
        UnitLinkBinScript(unit, BomberGreenBinTable());
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateLich(int mark)
{
	int count = GetDirection(mark), unit, amount = 3;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("Lich", 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 325);
        UnitZeroFleeRange(unit);
        UnitLinkBinScript(unit, LichLordBinTable());
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x20);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateJandors(int mark)
{
	int count = GetDirection(mark), unit, amount = 2;
    int ptr;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject("AirshipCaptain", 1);
        ptr = GetMemory(0x750710);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 275);
        if (ptr)
        {
            SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        }
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateHecubah(int mark)
{
	int count = GetDirection(mark), unit, amount = 1;

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = SummonHecubah(1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, 420);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

void MonsterCreateWorms(int mark)
{
    string name[] = {"GiantLeech", "AlbinoSpider", "SpittingSpider", "SmallAlbinoSpider", "Ghost"};
	int count = GetDirection(mark), unit, amount = 5, pic = Random(0, 4);
    int HpArr[] = {135, 135, 225, 96, 98};

	if (count < amount)
	{
		if (!count)
			Raise(mark + 1, amount);    //Lessons Kills
		MoveWaypoint(1, GetObjectX(mark), GetObjectY(mark));
		unit = CreateObject(name[pic], 1);
		Raise(CreateObject("InvisibleLightBlueLow", 1), mark);
        SetUnitMaxHealth(unit, HpArr[pic]);
        CommonFieldMonsterProperty(unit);
		LookWithAngle(mark, count + 1);
		FrameTimerWithArg(3, mark, ToInt(GetObjectZ(mark + 2)));
	}
}

int CheckAroundPlayer(int unit)
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        if (MaxHealth(player[i]))
        {
            if (IsVisibleTo(unit, player[i]) || IsVisibleTo(player[i], unit))
                return 1;
        }
    }
    return 0;
}

void InitMonsterMarker(int mark)
{
    int func = MonsterCreatePtr() + Random(0, 31);

	LookWithAngle(mark, 0);     //TODO: counts for created
	LookWithAngle(mark + 1, 0); //TODO: Kills Counting
	Raise(mark + 1, 0);         //TODO: Monster Amount
    Raise(mark + 2, func);      //TODO: Callback function
    if (!MissionEnd)
    {
        if (CheckAroundPlayer(mark))
        {
            LookWithAngle(mark, 70);
            FrameTimerWithArg(1, mark, StandByMonsterMarker);
        }
        else
	        FrameTimerWithArg(1, mark, func);
    }
}

void StandByMonsterMarker(int mark)
{
    int count = GetDirection(mark);

    if (count)
    {
        LookWithAngle(mark, count - 1);
        SecondTimerWithArg(1, mark, StandByMonsterMarker);
    }
    else
        FrameTimerWithArg(1, mark, InitMonsterMarker);
}

void ProcessMonsterMarker(int cur)
{
	int unit;

	if (GetUnitThingID(cur) == 2674)
	{
		MoveWaypoint(1, GetObjectX(cur), GetObjectY(cur));
		unit = CreateObject("InvisibleLightBlueLow", 1);
		CreateObject("InvisibleLightBlueLow", 1);
        CreateObject("InvisibleLightBlueLow", 1);
		Raise(unit, 80); //TODO: 60 seconds
		InitMonsterMarker(unit);
		Delete(cur);
	}
}

int ProcessNpcUnit(int unit)
{
    if (GetUnitThingID(unit) == 1343)
    {
        godItem(unit);
        ObjectOff(unit);
        Enchant(unit, "ENCHANT_INVISIBLE", 0.0);
        return 1;
    }
    return 0;
}

void SearchInitNpc(int cur)
{
    int i, ptr = cur, res;

    for (i = 0 ; i < 20 ; i ++)
    {
        if (ptr < FirstScrCreatedUnit)
        {
            res += ProcessNpcUnit(ptr);
            ProcessRewardMarker(ptr);
            ptr += 2;
        }
        else
        {
            EndSearching(res);
            return;
        }
    }
    FrameTimerWithArg(1, ptr, SearchInitNpc);
}

void EndMonsterMarkerSearch(int arg)
{
    UniPrintToAll("End Monster marker searching...");
}

void TempSearchMonsterMarker(int cur)
{
    int i, ptr = cur;

    for (i = 0 ; i < 5 ; i ++)
    {
        if (ptr < FirstScrCreatedUnit)
        {
            ProcessMonsterMarker(ptr);
            ptr += 2;
        }
        else
        {
            EndMonsterMarkerSearch(ptr);
            return;
        }
    }
    FrameTimerWithArg(1, ptr, TempSearchMonsterMarker);
}

int setLessons(int arg_0)
{
    return amountWar(arg_0) + amountCon(arg_0) + amountWiz(arg_0);
}

void controlStage()
{
    if (DEATH_COUNT == MAX_DEATHS)
    {
        UniPrintToAll("잠시 후 게임이 시작됩니다 ....");
        if (NOW_STAGE == 7)
            victoryEvent();
        else
        {
            SecondTimerWithArg(10, 0, initializeNPC);
            FrameTimerWithArg(1, RewardHead, UpdateRewardMarker);
        }
        DEATH_COUNT = 0;
    }
}

void victoryEvent()
{
    UniPrintToAll("승리__!! 모든 npc 들을 격추시켰습니다 !!");
    MissionEnd = 1;
    teleportPlayers(76);
    AudioEvent("BallHitGoal", 76);
    FrameTimer(10, MissionSuccess);
}

void teleportPlayers(int location)
{
    int k;

    for (k = 9 ; k >= 0 ; k --)
    {
        if (CurrentHealth(player[k]))
            MoveObject(player[k], LocationX(location), LocationY(location));
    }
}

void AutoTargetDeathRay()
{
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
}

void AutoTargetDeathrayShot(int cur)
{
    int owner = GetOwner(cur), unit;

    if (CurrentHealth(owner))
    {
        MoveWaypoint(1, GetObjectX(cur), GetObjectY(cur));
        unit = CreateObject("WeirdlingBeast", 1);
        UnitNoCollide(unit);
        SetOwner(owner, unit);
        LookWithAngle(unit, GetDirection(owner));
        CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(unit) + UnitAngleCos(unit, 100.0), GetObjectY(unit) + UnitAngleSin(unit, 100.0), 500.0);
        SetCallback(unit, 3, AutoTargetDeathRay);
        Effect("SENTRY_RAY", GetObjectX(unit), GetObjectY(unit), GetObjectX(unit) + UnitAngleCos(unit, 200.0), GetObjectY(unit) + UnitAngleSin(unit, 200.0));
        DeleteObjectTimer(unit, 1);
    }
    Delete(cur);
}

int UserDamageArrowCreate(int owner, int wp, int dam)
{
    int unit = CreateObject("MercArcherArrow", wp);
    int ptr = GetMemory(0x750710);

    SetOwner(owner, unit);
    SetMemory(ptr + 0x14, 0x32);
    SetMemory(GetMemory(ptr + 0x2bc) + 4, dam);
    Enchant(unit, "ENCHANT_INVISIBLE", 0.0);
    return unit;
}

void ThunderMissileCollide()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 200, 9);
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
            Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

void ThunderMissileShot(int cur)
{
    int owner = GetOwner(cur);

    if (CurrentHealth(owner))
    {
        int unit = CreateObjectAt("LightningBolt", GetObjectX(cur), GetObjectY(cur));
        Enchant(unit, "ENCHANT_SHOCK", 0.0);
        SetOwner(owner, unit);
        LookWithAngle(unit, GetDirection(owner));
        PushObjectTo(unit, UnitAngleCos(owner, 50.0), UnitAngleSin(owner, 50.0));
    }
    Delete(cur);
}

void AutoTrackingMissile(int cur)
{
    int ptr, owner = GetOwner(cur);

    if (CurrentHealth(owner))
    {
        MoveWaypoint(1, GetObjectX(cur), GetObjectY(cur));
        ptr = CreateObject("InvisibleLightBlueHigh", 1);
        LookWithAngle(CreateObject("InvisibleLightBlueHigh", 1), GetDirection(owner));
        Raise(ptr, 250.0);
        SetOwner(owner, ptr);
        TrackingProgress(ptr);
    }
    Delete(cur);
}

void TrackingProgress(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && count < 30)
    {
        if (IsVisibleTo(owner, ptr))
        {
            MoveObject(ptr, GetObjectX(ptr) + UnitAngleCos(ptr + 1, 23.0), GetObjectY(ptr) + UnitAngleSin(ptr + 1, 23.0));
            MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
            unit = CreateObject("WeirdlingBeast", 1);
            Frozen(CreateObject("HarpoonBolt", 1), 1);
            LookWithAngle(unit, GetDirection(ptr + 1));
            LookWithAngle(unit + 1, GetDirection(ptr + 1));
            SetOwner(ptr, unit);
            SetOwner(ptr, unit + 1);
            SetCallback(unit, 3, EnemyDetection);
            SetCallback(unit, 9, MissileTouched);
            DeleteObjectTimer(unit + 1, 2);
            DeleteObjectTimer(unit, 1);
            LookWithAngle(ptr, count + 1);
        }
        else
            LookWithAngle(ptr, 200);
        FrameTimerWithArg(1, ptr, TrackingProgress);
    }
    else
    {
        Delete(ptr);
        Delete(++ptr);
        Delete(++ptr);
    }
}

void MissileTouched()
{
    int ptr = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, GetOwner(ptr)))
    {
        Effect("SPARK_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, GetOwner(ptr), 200, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        Delete(ptr);
    }
}

void EnemyDetection()
{
    int ptr = GetOwner(SELF);
    int tg = ToInt(GetObjectZ(ptr + 1));

    if (CurrentHealth(tg))
    {
        if (IsVisibleTo(tg, ptr))
        {
            LookAtObject(SELF, tg);
            CheckTrackingMisDirection(ptr + 1, GetDirection(ptr + 1), GetDirection(SELF), 3);
            //LookWithAngle(ptr + 1, GetDirection(SELF));
        }
        else
            Raise(ptr + 1, ToFloat(0));
    }
    else
    {
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) < GetObjectZ(ptr))
            Raise(ptr + 1, ToFloat(GetCaller()));
    }
}

void CheckTrackingMisDirection(int ptr, int c, int t, int angle)
{
    int tc = t - c, absTc = SetAbs(t - c);

    if (absTc > 128)
    {
        if (tc > 0)
            LookWithAngle(ptr, c - angle);
        else
            LookWithAngle(ptr, c + angle);
    }
    else
    {
        if (tc > 0)
            LookWithAngle(ptr, c + angle);
        else
            LookWithAngle(ptr, c - angle);
    }
}

int SetAbs(int num)
{
    if (num < 0) return -num;
    else return num;
}

void UseMapSetting()
{
    SetMemory(0x5d5330, 0x2000);
    SetMemory(0x5d5394, 1);
}

void ShurikenHandlerDeathray(int cur)
{
    int ptr = UnitToPtr(cur);

    if (ptr)
    {
        int property = GetMemory(GetMemory(ptr + 0x2b4) + 4);
        if (property == WeaponEffect(24))
            AutoTargetDeathrayShot(cur);
        else if (property == WeaponEffect(20))
            ThunderMissileShot(cur);
        else if (property == WeaponEffect(8))
            AutoTrackingMissile(cur);
    }
}

int BlueRingSkill(int owner)
{
    int unit;

    if (CurrentHealth(owner))
    {
        if (!HasEnchant(owner, "ENCHANT_ETHEREAL"))
        {
            MoveWaypoint(1, 5600.0, 5600.0);
            unit = CreateObject("InvisibleLightBlueLow", 1);
            SetOwner(owner, unit);
            LookWithAngle(unit, 24);
            Enchant(unit, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
            Enchant(unit, "ENCHANT_PROTECT_FROM_FIRE", 0.0);
            Enchant(unit, "ENCHANT_PROTECT_FROM_POISON", 0.0);
            Enchant(unit, "ENCHANT_RUN", 0.0);
            MoveObject(unit, GetObjectX(owner) + UnitAngleCos(owner, 29.0), GetObjectY(owner) + UnitAngleSin(owner, 29.0));
            MoveObject(CreateObject("InvisibleLightBlueLow", 1), GetObjectX(unit), GetObjectY(unit));
            Raise(unit, UnitAngleCos(owner, 21.0));
            Raise(unit + 1, UnitAngleSin(owner, 21.0));
            FrameTimerWithArg(1, unit, FlySkill);
            Enchant(owner, "ENCHANT_ETHEREAL", 0.1);
        }
        return 1;
    }
    return 0;
}

void DetectedBlueFire(int cur)
{
    int thingID = GetUnitThingID(cur);

    if (thingID >= 192 && thingID <= 194)
    {
        if (BlueRingSkill(GetOwner(cur)))
            Delete(cur);
    }
}

void DetectedSpecificIndex(int curId)
{
    int thingID = GetUnitThingID(curId);

    if (thingID == 526)
        HarpoonEvent(curId);
    else if (thingID == 1179)
        ShurikenHandlerDeathray(curId);
    else if (thingID == 709)
        MagicMissileWand(curId);
}

void LoopSearchIndex()
{
    int curId, tempId;
    int *ptr = 0x750710;

    ptr = ptr[0];
    while (ptr != NULLPTR)
    {
        tempId = ptr[11];
        if (curId)
        {
            while (curId < tempId)
            {
                curId += 1;
                DetectedSpecificIndex(curId);
            }
            break;
        }
        curId = tempId;
        break;
    }
    FrameTimer(1, LoopSearchIndex);
}

void ControlPutSupply()
{
    int resetCount, repeat;

    if (resetCount < 2)
    {
        if (repeat < 80)
        {
            getSpiral(repeat);
            SpawnSupply(resetCount, 80);
            repeat += 1;
        }
        else
        {
            MoveWaypoint(80, 882.0 , 4361.0);
            resetCount += 1;
            repeat = 0;
        }
        FrameTimer(1, ControlPutSupply);
    }
}

void SpawnSupply(int resetCount, int location)
{
    int k;

    if (!resetCount)
    {
        if (k == 14)
            k = 0;
        CreateObject(WeaponTable(k), location);
    }
    else
    {
        if (k == 15)
            k = 0;
        CreateObject(ArmorTable(k), location);
    }
    k ++;
}

void getSpiral(int arg_0)
{
    //received start value 0
    int var_0; //limit
    int var_1; //index
    int var_2[2];

    if (arg_0 == 0)
    {
        // --reset
        var_2[0] = 0;
        var_2[1] = 0;
        var_0 = 1;
        var_1 = 0;
    }
    else
    {
        getMoving(80, var_1, 20.0);
        var_2[0] += 1;
        if (var_2[0] == var_0)
        {
            var_2[1] += 1;
            var_2[0] = 0;
            var_1 = (var_1 + 1) % 4;
        }
        if (var_2[1] == 2)
        {
            var_2[1] = 0;
            var_0 += 1;
        }
    }
}

void getMoving(int arg_0, int arg_1, float arg_2)
{
    float var_0;
    float var_1;

    if (arg_1 == 0)
    {
        var_0 = -arg_2;
        var_1 = arg_2;
    }
    else if (arg_1 == 1)
    {
        var_0 = arg_2;
        var_1 = arg_2;
    }
    else if (arg_1 == 2)
    {
        var_0 = arg_2;
        var_1 = -arg_2;
    }
    else if (arg_1 == 3)
    {
        var_0 = -arg_2;
        var_1 = -arg_2;
    }
    MoveWaypoint(arg_0, LocationX(arg_0) + var_0, LocationY(arg_0) + var_1);
}

string WeaponTable(int num)
{
    string table[] = {
        "Quiver", "FanChakram", "WarHammer", "GreatSword", "CrossBow",
        "RoundChakram", "DeathRayWand", "SulphorousFlareWand", "ForceWand", "DeathRayWand",
        "LesserFireballWand", "InfinitePainWand", "FireStormWand", "OgreAxe", "Bow"
    };

    return table[num];
}

string ArmorTable(int num)
{
    string table[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings",
        "ConjurerHelm", "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots",
        "WizardHelm", "WizardRobe", "MedievalCloak", "LeatherLeggings", "SteelShield",
        "StreetShirt", "StreetPants", "StreetSneakers"
    };

    return table[num];
}

void castWindbooster(int plr)
{
    EnchantOff(player[plr], "ENCHANT_SNEAK");
    PushObjectTo(player[plr], UnitAngleCos(player[plr], 70.0), UnitAngleSin(player[plr], 70.0));
    Enchant(player[plr], "ENCHANT_RUN", 0.1);
}

void TeleportOperation(int sUnit)
{
    int durate = GetDirection(sUnit), owner = GetOwner(sUnit);

    while (CurrentHealth(owner))
    {
        if (durate)
        {
            if (Distance(GetObjectX(owner), GetObjectY(owner), GetObjectX(sUnit + 1), GetObjectY(sUnit + 1)) < 23.0)
            {
                FrameTimerWithArg(1, sUnit, TeleportOperation);
                LookWithAngle(sUnit, durate - 1);
                return;
            }
            UniPrint(owner, "공간이동이 취소되었습니다");
            break;
        }
        Effect("SMOKE_BLAST", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
        MoveObject(owner, GetObjectX(sUnit), GetObjectY(sUnit));
        Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
        PlaySoundAround(sUnit, 312);
        break;
    }
    Delete(sUnit++);
    Delete(sUnit);
}

void TeleportToStartZone()
{
    int unit = CreateObjectAt("ImaginaryCaster", LocationX(2), LocationY(2));

    PlaySoundAround(CreateObjectAt("VortexSource", GetObjectX(OTHER), GetObjectY(OTHER)), 1003);
    FrameTimerWithArg(1, unit, TeleportOperation);
    Delete(SELF);
    SetOwner(OTHER, unit);
    LookWithAngle(unit, 36);
    UniPrint(OTHER, "안전한 은신처로 공간이동을 준비중입니다. 취소하려면 움직이세요");
}

void TeleportStartLocation(int plr)
{
    CastSpellObjectObject("SPELL_CURE_POISON", player[plr], player[plr]);
    EnchantOff(player[plr], "ENCHANT_PROTECT_FROM_POISON");
    UniChatMessage(player[plr], "시작지점으로 이동합니다.", 150);
    MoveObject(player[plr], LocationX(2), LocationY(2));
    DeleteObjectTimer(CreateObject("LevelUp", 2), 10);
    RestoreHealth(player[plr], MaxHealth(player[plr]));
    AudioEvent("BlindOff", 2);
}

void MagicMissileWand(int curId)
{
    int owner = GetOwner(curId), mgMis;

    Delete(curId);
    if (CurrentHealth(owner))
    {
        mgMis = CreateObject("InvisibleLightBlueHigh", 92);
        CastSpellObjectLocation("SPELL_MAGIC_MISSILE", owner, GetObjectX(owner) + UnitAngleCos(owner, 20.0), GetObjectY(owner) + UnitAngleSin(owner, 20.0));
        Delete(mgMis);
        Delete(mgMis + 2);
        Delete(mgMis + 3);
        Delete(mgMis + 4);
    }
}

void HarpoonEvent(int cur)
{
    int owner = GetOwner(cur), ptr;
    float pos_x, pos_y;

    if (CurrentHealth(owner) && HasClass(owner, "PLAYER"))
    {
        if (!HasEnchant(owner, "ENCHANT_CROWN"))
            return;
        if (!HasEnchant(owner, "ENCHANT_AFRAID"))
        {
            Enchant(owner, "ENCHANT_AFRAID", 10.0);
            pos_x = UnitAngleCos(owner, 23.0);
            pos_y = UnitAngleSin(owner, 23.0);
            MoveWaypoint(92, GetObjectX(owner) + pos_x, GetObjectY(owner) + pos_y);
            ptr = CreateObject("InvisibleLightBlueHigh", 92);   //owner_id
            CreateObject("InvisibleLightBlueHigh", 92);         //x_vect
            CreateObject("InvisibleLightBlueHigh", 92);         //y_vect
            Raise(ptr, ToFloat(owner));
            Raise(ptr + 1, pos_x);
            Raise(ptr + 2, pos_y);
            FrameTimerWithArg(1, ptr, WindSlashLoop);
        }
        else
            UniChatMessage(owner, "쿨다운 입니다...", 150);
        Delete(cur);
    }
}

void WindSlashLoop(int ptr)
{
    int owner = ToInt(GetObjectZ(ptr));
    int count = GetDirection(ptr);
    int unit;

    if (CurrentHealth(owner) && count < 30 && IsVisibleTo(ptr, owner))
    {
        MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr + 1), GetObjectY(ptr) + GetObjectZ(ptr + 2));
        MoveWaypoint(92, GetObjectX(ptr), GetObjectY(ptr));
        unit = CreateObject("CarnivorousPlant", 92);
        CreateObject("InvisibleLightBlueHigh", 92);
        Frozen(unit, 1);
        SetCallback(unit, 9, DeathTouched);
        DeleteObjectTimer(unit, 1);
        DeleteObjectTimer(CreateObject("BigSmoke", 92), 8);
        AudioEvent("DeathOff", 92);
        Raise(unit + 1, ToFloat(owner));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, WindSlashLoop);
    }
    else
    {
        Delete(ptr);
        Delete(++ptr);
        Delete(++ptr);
    }
}

void DeathTouched()
{
    int owner = ToInt(GetObjectZ(GetTrigger() + 1));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 100, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.3);
    }
}

void DelayTeleportToCenter()
{
    int ptr;
    if (!HasEnchant(OTHER, "ENCHANT_BURNING"))
    {
        MoveWaypoint(95, GetObjectX(SELF), GetObjectY(SELF));
        ptr = CreateObject("InvisibleLightBlueHigh", 95);
        Raise(ptr, ToFloat(GetCaller()));
        DeleteObjectTimer(CreateObject("VortexSource", 95), 30 * 3);
        DeleteObjectTimer(CreateObject("VortexSource", 102), 30 * 3);
        UniChatMessage(OTHER, "3 초 후에 센터로 이동됩니다\n그만두려면 비콘에서 벗어나십시오", 150);
        Enchant(OTHER, "ENCHANT_BURNING", 3.0);
        FrameTimerWithArg(90, ptr, TeleportToCenter);
    }
}

void TeleportToCenter(int ptr)
{
    int unit = ToInt(GetObjectZ(ptr));

    if (Distance(GetObjectX(unit), GetObjectY(unit), GetObjectX(ptr), GetObjectY(ptr)) < 42.0)
    {
        MoveObject(unit, LocationX(102), LocationY(102));
        Effect("TELEPORT", LocationX(102), LocationY(102), 0.0, 0.0);
        AudioEvent("BlindOff", 102);
    }
    Delete(ptr);
}

int HorrendousBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1920102216; arr[1] = 1868852837; arr[2] = 29557; arr[17] = 10; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; arr[25] = 1; arr[26] = 9; arr[27] = 5; 
		arr[28] = 1114636288; arr[29] = 100; arr[30] = 1112014848; arr[31] = 11; arr[32] = 6; 
		arr[33] = 8; arr[34] = 5; arr[35] = 5; arr[36] = 10; arr[57] = 5548368; 
		arr[59] = 5542784; 
		link = &arr;
	}
	return link;
}

void HealingOn(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    while (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner) && count)
        {
            if (CurrentHealth(owner) ^ MaxHealth(owner))
                RestoreHealth(owner, 1);
            MoveObject(ptr, GetObjectX(owner), GetObjectY(owner));
            LookWithAngle(ptr, count - 1);
            FrameTimerWithArg(1, ptr, HealingOn);
            break;
        }
        Delete(ptr);
        break;
    }
}

void StartHealing(int owner)
{
    int healUnit;

    if (CurrentHealth(owner))
    {
        healUnit = CreateObjectAt("SpinningCrown", GetObjectX(owner), GetObjectY(owner));
        SetOwner(owner, healUnit);
        UnitNoCollide(healUnit);
        LookWithAngle(healUnit, 120);
        Enchant(healUnit, "ENCHANT_RUN", 0.0);
        FrameTimerWithArg(1, healUnit, HealingOn);
    }
}

void BossOnHurt()
{
    if (!HasEnchant(SELF, "ENCHANT_BURNING"))
    {
        if (HasEnchant(SELF, "ENCHANT_SNEAK"))
            1;
        else
        {
            StartHealing(GetTrigger());
            Enchant(SELF, "ENCHANT_SNEAK", 5.0);
        }
        Enchant(SELF, "ENCHANT_BURNING", 0.8);
        UniChatMessage(SELF, "HP: " + IntToString(CurrentHealth(SELF) * 100 / MaxHealth(SELF)) + "%", 150);
    }
}

void buyCreature()
{
    int unit;

    if (GetGold(OTHER) >= 30000 && countofCreatures(0) < 8)
    {
        ChangeGold(OTHER, -30000);
        MoveWaypoint(95, GetObjectX(OTHER), GetObjectY(OTHER));
        unit = CreateObject("Horrendous", 95);
        SetUnitMaxHealth(unit, 1800);
        CreatureGuard(unit, GetObjectX(unit), GetObjectY(unit), GetObjectX(OTHER), GetObjectY(OTHER), 450.0);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        UnitLinkBinScript(unit, HorrendousBinTable());
        SetCallback(unit, 3, HorrendousAttack);
        SetCallback(unit, 7, BossOnHurt);
        SetCallback(unit, 5, creatureDeath);
        SetOwner(OTHER, unit);
        GiveCreatureToPlayer(OTHER, unit);
        countofCreatures(1);
        SetDialog(unit, "NORMAL", guardMode, nullPointer);
    }
    else
    {
        UniPrintToAll("소환수를 구입할 수 없습니다, 호렌더스- 3만원!, 소환 최대 수: 8개");
        UniPrintToAll("골드가 부족하거나 소환수 개수제한이 초과 되었을 수 있습니다");
    }
}

void creatureDeath()
{
    UniChatMessage(SELF, "나의 죽음이 헛되지 않기를...", 150);
    countofCreatures(2);
    DeleteObjectTimer(SELF, 30);
}

int SpawnToxicCloud(int wp, int time)
{
	int cloud = CreateObject("ToxicCloud", wp);
    int ptr = GetMemory(0x750710);

	SetMemory(GetMemory(ptr + 0x2ec), time);
    return cloud;
}

void ReleasePoison()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_BURNING"))
        {
            Enchant(SELF, "ENCHANT_BURNING", 1.0);
            MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
            SetOwner(SELF, SpawnToxicCloud(1, 20));
            AudioEvent("PoisonTrapTriggered", 1);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
		{
			Enchant(SELF, "ENCHANT_DETECTING", 0.0);
			FrameTimerWithArg(75, GetTrigger(), ResetUnitSight);
		}
    }
}

void HorrendousAttack()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        AggressionLevel(SELF, 1.0);
        if (!HasEnchant(SELF, "ENCHANT_TELEKINESIS"))
        {
            CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
            Enchant(SELF, "ENCHANT_TELEKINESIS", 0.1);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(65, GetTrigger(), ResetUnitSight);
        }
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(unit, 1.0);
}

void guardMode()
{
    SetOwner(OTHER, SELF);
    CreatureFollow(SELF, OTHER);
    AggressionLevel(SELF, 1.0);
    GiveCreatureToPlayer(GetCaller(), GetTrigger());
    UniChatMessage(SELF, "소환수 지정이 완료되었습니다", 150);
    MoveWaypoint(94, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("bigGong", 94);
}

void nullPointer()
{
    return;
}

void turnBackNPC()
{
    if (HasSubclass(OTHER, "NPC") && CurrentHealth(OTHER))
        CastForceBlink(OTHER);
}

void CastForceBlink(int unit)
{
    int destLoc = Random(22, 79);

    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    MoveObject(unit, LocationX(destLoc), LocationY(destLoc));
    Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
}

void loopPutRandomMonsters()
{
    int count;
    int idx;
    int location;

    count ++;
    if (countofFieldUnits(0) < 80 && count == 5)
    {
        count = 0;
        idx = Random(0, 20);
        location = Random(22, 79);
        spawnFieldUnit((idx << 16) | location);
    }
    if (!MissionEnd)
        SecondTimer(20, loopPutRandomMonsters);
}

void spawnFieldUnit(int arg_0)
{
    int location = arg_0 & 0xffff;         //wp
    int idx = (arg_0 >> 16) & 0xffff;      //idx
    int amount;
    int unit;

    if (amount < UnitFlagsTable(idx) & 0xf)
    {
        unit = CreateObject(fieldUnitType(idx), location);
        SetUnitMaxHealth(unit, fieldUnitHealth(idx));
        SetCallback(unit, 5, fieldUnitDeath);
        RetreatLevel(unit, 0.0);
        SpecialUnitFlags(unit, UnitFlagsTable(idx));
        amount ++;
        countofFieldUnits(1);
        FrameTimerWithArg(1, arg_0, spawnFieldUnit);
    }
    else
        amount = 0;
}

void SpecialUnitFlags(int unit, int flags)
{
    int k;

    for (k = 0x10 ; k < 0x400 ; k <<= 1)
    {
        if (flags & k)
        {
            if (k == 0x10)
                Enchant(unit, "ENCHANT_ANCHORED", 0.0);
            if (k == 0x20)
                Enchant(unit, "ENCHANT_SLOWED", 0.0);
            if (k == 0x40)
                SetCallback(unit, 3, NecromancerWeapon);
            if (k == 0x80)
                SetUnitSpeed(unit, 1.5);
            if (k == 0x100)
                SetCallback(unit, 5, BlackSpiderDead);
            if (k == 0x200)
                SetCallback(unit, 3, ReleasePoison);
        }
    }
}

void fieldUnitDeath()
{
    int rnd = Random(0, 4);

    //Waypoint: 98
    MoveWaypoint(98, GetObjectX(SELF), GetObjectY(SELF));
    DropItemHandler(98);
    DeleteObjectTimer(SELF, 30);
    countofFieldUnits(2);
}

string fieldUnitType(int num)
{
    string table[] = {
    "BlackWolf", "BlackBear", "Bear", "OgreWarlord", "GruntAxe",
    "FireSprite", "MeleeDemon", "Shade", "Swordsman", "Archer",
    "WizardGreen", "Goon", "BlackWidow", "SkeletonLord", "Scorpion",
    "EvilCherub", "VileZombie", "Zombie", "OgreBrute", "Necromancer",
    "MechanicalGolem"};

    return table[num];
}

int fieldUnitHealth(int num)
{
    int hp[] = {
        135, 250, 350, 350, 200,
        130, 135, 150, 306, 98,
        135, 130, 200, 250, 250,
        98, 325, 215, 270, 220,
        450
    };

    return hp[num];
}

int UnitFlagsTable(int num)
{
    //(2byte)=amount, 0x10-anchored, 0x20-slowed, 0x40-necromancer, 0x80-fasted, 0x100-dead_spawn_spiders, 0x200-not_retreat
    int flags[21];

    if (num < 0)
    {
        flags[0] = 5; flags[1] = 3; flags[2] = 2; flags[3] = 2; flags[4] = 3;
        flags[5] = 4 | 0x20; flags[6] = 4; flags[7] = 3; flags[8] = 5; flags[9] = 5;
        flags[10] = 2 | 0x10; flags[11] = 3; flags[12] = 3 | 0x100; flags[13] = 3; flags[14] = 3 | 0x200;
        flags[15] = 4; flags[16] = 2 | 0x80; flags[17] = 3; flags[18] = 3; flags[19] = 2 | 0x40;
        flags[20] = 1;
        return 0;
    }
    return flags[num];
}

string PotionTable(int num)
{
    string table[] =
    {
        "RedPotion", "RedPotion2", "CurePoisonPotion", "VampirismPotion", "ShieldPotion",
        "HastePotion", "WhitePotion", "YellowPotion", "FireProtectPotion", "ShockProtectPotion",
        "InfravisionPotion", "PoisonProtectPotion", "BluePotion", "InvisibilityPotion", "RedPotion"
    };
    return table[num];
}

void NecromancerWeapon()
{
    int mis;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_TELEKINESIS"))
        {
            MoveWaypoint(92, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 20.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 20.0));
            AudioEvent("ForceOfNatureRelease", 92);
            mis = CreateObject("DeathBallFragment", 92);
            SetOwner(SELF, mis);
            PushObject(mis, -32.0, GetObjectX(OTHER), GetObjectY(OTHER));
            LookAtObject(SELF, OTHER);
            CreatureGuard(SELF, GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER), 400.0);
            AggressionLevel(SELF, 1.0);
            Enchant(SELF, "ENCHANT_TELEKINESIS", 0.3);
        }
        else if (HasEnchant(SELF, "ENCHANT_ANTI_MAGIC"))
            EnchantOff(SELF, "ENCHANT_ANTI_MAGIC");
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(25, GetTrigger(), ResetUnitSight);
        }
    }
}

void SmallSpiderDead()
{
    Effect("SMOKE_BLAST", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    DeleteObjectTimer(SELF, 1);
}

void SummonSmallSpider(int ptr)
{
    int count = GetDirection(ptr), unit;

    if (count)
    {
        MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
        Effect("DAMAGE_POOF", LocationX(1), LocationY(1), 0.0, 0.0);
        unit = CreateObject("SmallSpider", 1);
        SetUnitMaxHealth(unit, 64);
        SetCallback(unit, 5, SmallSpiderDead);
        LookWithAngle(ptr, count - 1);
        FrameTimerWithArg(1, ptr, SummonSmallSpider);
    }
    else
        Delete(ptr);
}

void ZombieDead()
{
    MoveWaypoint(1, GetObjectX(SELF), GetObjectY(SELF));
    FieldMonsterDeath();
    if (MaxHealth(SELF))
        Damage(SELF, 0, 100, 14);
    DeleteObjectTimer(CreateObject("MediumFlame", 1), 150);
    AudioEvent("BurnCast", 1);
    Effect("SPARK_EXPLOSION", LocationX(1), LocationY(1), 0.0, 0.0);
}

void BlackSpiderDead()
{
    FieldMonsterDeath();
    TeleportLocation(92, GetObjectX(SELF), GetObjectY(SELF));
    Delete(SELF);
    DeleteObjectTimer(CreateObject("WaterBarrelBreaking", 92), 30);
    DeleteObjectTimer(CreateObject("BigSmoke", 92), 7);
    int unit = CreateObject("InvisibleLightBlueHigh", 92);
    LookWithAngle(unit, 4);
    FrameTimerWithArg(1, unit, SummonSmallSpider);
    AudioEvent("PoisonTrapTriggered", 92);
    AudioEvent("BeholderDie", 92);
}

int countofCreatures(int mode)
{
    //mode: reset(-1), gets(0), add(1), sub(2)
    int gets;

    if (mode == -1)
        gets = 0;
    else if (mode == 1)
        return ++gets;
    else if (mode == 2)
        return --gets;

    return gets;
}

int countofFieldUnits(int mode)
{
    //mode: reset(-1), gets(0), add(1), sub(2)
    int count;

    if (mode == -1)
        count = 0;
    else if (mode == 1)
        return ++count;
    else if (mode == 2)
        return --count;
    return count;
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectAt("RottenMeat", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void StrWellNotify()
{
	int arr[28];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 1076109816; arr[1] = 1060651520; arr[2] = 570696578; arr[3] = 1606451320; arr[4] = 608322184; arr[5] = 5227528; arr[6] = 1115720193; arr[7] = 1139820676; arr[8] = 285376752; arr[9] = 1193051393; 
	arr[10] = 1083212832; arr[11] = 82254856; arr[12] = 536379401; arr[13] = 137388351; arr[14] = 1066007042; arr[15] = 1; arr[16] = 33750532; arr[17] = 264308752; arr[18] = 262648; arr[19] = 1209009156; 
	arr[20] = 33824835; arr[21] = 608176136; arr[22] = 541197312; arr[23] = 150979592; arr[24] = 67127040; arr[25] = 66076416; arr[26] = 1074786304; arr[27] = 522367; 
	while(i < 28)
	{
		drawStrWellNotify(arr[i], name);
		i ++;
	}
}

void drawStrWellNotify(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(95);
		pos_y = LocationY(95);
	}
	for (i = 1 ; i > 0 && count < 868 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 95);
		if (count % 78 == 77)
			MoveWaypoint(95, LocationX(95) - 154.000000, LocationY(95) + 2.000000);
		else
			MoveWaypoint(95, LocationX(95) + 2.000000, LocationY(95));
		count ++;
	}
	if (count >= 868)
	{
		count = 0;
		MoveWaypoint(95, pos_x, pos_y);
	}
}

void MissionSuccess()
{
	int arr[22];
	string name = "ManaBombOrb";
	int i = 0;
	arr[0] = 272613948; arr[1] = 285245316; arr[2] = 270549570; arr[3] = 285229060; arr[4] = 1478509122; arr[5] = 285230087; arr[6] = 673202754; arr[7] = 285221892; arr[8] = 1145029180; arr[9] = 285213700; 
	arr[10] = 33554944; arr[11] = 285278149; arr[12] = 0; arr[13] = 285212672; arr[14] = 2021647356; arr[15] = 16131; arr[16] = 67371524; arr[17] = 285229188; arr[18] = 67371524; arr[19] = 293355652; 
	arr[20] = 2013529084; arr[21] = 16131; 
	while(i < 22)
	{
		drawMissionSuccess(arr[i], name);
		i ++;
	}
}

void drawMissionSuccess(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = LocationX(76);
		pos_y = LocationY(76);
	}
	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 76);
		if (count % 62 == 61)
			MoveWaypoint(76, LocationX(76) - 122.000000, LocationY(76) + 2.000000);
		else
			MoveWaypoint(76, LocationX(76) + 2.000000, LocationY(76));
		count ++;
	}
	if (count >= 682)
	{
		count = 0;
		MoveWaypoint(76, pos_x, pos_y);
	}
}

void godItem(int unit)
{
    int invens;

    invens = GetLastItem(unit);
    while (IsObjectOn(invens) && !HasEnchant(invens, "ENCHANT_INVULNERABLE"))
    {
        Enchant(invens, "ENCHANT_INVULNERABLE", 0.0);
        invens = GetPreviousItem(invens);
    }
}

int GetNealryPlayer(int unit)
{
    float temp = 9999.0, dist;
    int res = -1, i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        if (CurrentHealth(player[i]))
        {
            dist = DistanceUnitToUnit(player[i], unit);
            if (dist < temp)
            {
                temp = dist;
                res = i;
            }
        }
    }
    return res;
}

void PrintOutNPCXY(int npc)
{
    char msg[128];
    float xy[] = {GetObjectX(npc), GetObjectY(npc)};

    NoxSprintfString(&msg, "현재 생존중인 적군!\nX: %f,\nY: %f", ToInt( &xy ), 2);
    UniChatMessage(npc, ReadStringAddressEx(&msg), 300);
}

void DisplayEnemyPos(int idx)
{
    int enemy = Guardian[idx], targetIdx;

    if (CurrentHealth(enemy))
    {
        PlayerSetTag(enemy);
        targetIdx = GetNealryPlayer(enemy);
        if (targetIdx + 1)
        {
            CreatureFollow(enemy, player[targetIdx]);
            AggressionLevel(enemy, 1.0);
        }
        PrintOutNPCXY(enemy);
        SecondTimerWithArg(15, (idx + 1) % CountMaxNpc, DisplayEnemyPos);
    }
    else FrameTimerWithArg(1, (idx + 1) % CountMaxNpc, DisplayEnemyPos);
}

int SetInvincibleInventoryItems(int unit)
{
    int cur = GetLastItem(unit), count = 0;

    while (IsObjectOn(cur))
    {
        if (HasEnchant(cur, "ENCHANT_INVULNERABLE"))
            cur = GetPreviousItem(cur);
        else
        {
            Enchant(cur, "ENCHANT_INVULNERABLE", 0.0);
            count += 1;
        }
    }
    return count;
}

void InvincibleMyItems()
{
    int res;

    if (GetGold(OTHER) >= 8000)
    {
        res = SetInvincibleInventoryItems(OTHER);
        if (res)
        {
            ChangeGold(OTHER, -8000);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
            UniPrint(OTHER, "거래성공: " + IntToString(res) + " 개 아이템이 처리되었습니다");
        }
    }
    else
        UniPrint(OTHER, "거래실패: 잔액이 부족합니다, 장비 내구도 파괴불능 설정의 가격은 8천 골드 입니다");
}

void GreenSparkFx(int wp)
{
    int ptr = CreateObject("MonsterGenerator", wp);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void NewSkillDash()
{
    int plr = CheckPlayer();

    if (CurrentHealth(OTHER) && plr >= 0)
    {
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        if (player[plr + 10] & 2)
        {
            AudioEvent("NoCanDo", 1);
            UniPrint(OTHER, "이미 대쉬 기술을 배우셨습니다");
        }
        else if (GetGold(OTHER) >= 10000)
        {
            ChangeGold(OTHER, -10000);
            player[plr + 10] = player[plr + 10] ^ 2;
            GreenSparkFx(1);
            AudioEvent("AwardSpell", 1);
            UniPrint(OTHER, "대쉬 기술을 배우셨습니다, 조심스럽게 걷기 시전을 통해 사용하실 수 있습니다");
        }
    }
}

void AnotherHarpoon()
{
    int plr = CheckPlayer();

    if (CurrentHealth(OTHER) && plr >= 0)
    {
        MoveWaypoint(1, GetObjectX(OTHER), GetObjectY(OTHER));
        if (player[plr + 10] & 4)
        {
            AudioEvent("NoCanDo", 1);
            UniPrint(OTHER, "이미 신규 작살 기술을 배우셨습니다");
        }
        else if (GetGold(OTHER) >= 10000)
        {
            ChangeGold(OTHER, -10000);
            player[plr + 10] = player[plr + 10] ^ 4;
            GreenSparkFx(1);
            Enchant(OTHER, "ENCHANT_CROWN", 0.0);
            AudioEvent("AwardSpell", 1);
            UniPrint(OTHER, "신규 작살 기술을 배우셨습니다, 작살 시전을 통해 기존의 기술과 다른 새로운 기술이 발동됩니다");
        }
    }
}

void CheckSpecialItem(int specials)
{
    int thingId = GetUnitThingID(specials);

    if (thingId >= 222 && thingId <= 225)
    {
        DisableOblivionItemPickupEvent(specials);
        SetItemPropertyAllowAllDrop(specials);
    }
    else if (thingId == 1178 || thingId == 1168)
        SetConsumablesWeaponCapacity(specials, 255, 255);
}

int FieldItemFuncPtr(int index)
{
    int dropitem[] = {HotPotion, PotionItemDrop, NormalWeaponItemDrop, NormalArmorItemDrop, StaffItemDrop, MoneyDrop, SomeGermDrop,
        WeaponItemDrop, ArmorItemDrop, RewardClassTeleportAmuletCreate};

    return dropitem[index % 10];
}

int HotPotion(int wp)
{
    return PotionPickupRegist(CreateObjectAt("RedPotion", LocationX(wp), LocationY(wp)));
}

int PotionItemDrop(int wp)
{
    return CheckPotionThingID(CreateObjectAt(PotionList(Random(0, 18)), LocationX(wp), LocationY(wp)));
}

int NormalWeaponItemDrop(int wp)
{
    int weapon = CreateObjectAt(WeaponList(Random(0, 10)), LocationX(wp), LocationY(wp));

    CheckSpecialItem(weapon);
    return weapon;
}

int NormalArmorItemDrop(int wp)
{
    return CreateObjectAt(ArmorList(Random(0, 20)), LocationX(wp), LocationY(wp));
}

int StaffItemDrop(int wp)
{
    return CreateObjectAt(StaffList(Random(0, 8)), LocationX(wp), LocationY(wp));
}

int MoneyDrop(int wp)
{
    string name[] = {"QuestGoldChest", "QuestGoldPile", "Gold"};
    int money = CreateObjectAt(name[Random(0, 2)], LocationX(wp), LocationY(wp));

    UnitStructSetGoldAmount(money, Random(1000, 7000));
    return money;
}

int SomeGermDrop(int wp)
{
    string name[] = {"Ruby", "Ruby", "Ruby", "Emerald", "Diamond"};

    return CreateObjectAt(name[Random(0, 4)], LocationX(wp), LocationY(wp));
}

int WeaponItemDrop(int wp)
{
    int weapon = CreateObjectAt(WeaponList(Random(0, 15)), LocationX(wp), LocationY(wp));

    CheckSpecialItem(weapon);
    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    return weapon;
}

int ArmorItemDrop(int wp)
{
    int armor = CreateObjectAt(ArmorList(Random(0, 23)), LocationX(wp), LocationY(wp));

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return armor;
}

int RewardClassTeleportAmuletCreate(int location)
{
    int amlet = CreateObjectAt("AmuletofManipulation", LocationX(location), LocationY(location));
    
    SetUnitCallbackOnUseItem(amlet, TeleportToStartZone);
    return amlet;
}

string PotionList(int index)
{
    string name[] = {
        "RedPotion", "CurePoisonPotion", "YellowPotion", "BlackPotion",
        "VampirismPotion", "Mushroom", "PoisonProtectPotion", "ShockProtectPotion",
        "FireProtectPotion", "HastePotion", "ShieldPotion", "InvulnerabilityPotion",
        "BluePotion", "WhitePotion", "ManaCrystalCluster", "ManaCrystalLarge",
        "InvisibilityPotion", "InfravisionPotion", "RedPotion2"
    };
    return name[index % 19];
}

string WeaponList(int index)
{
    string name[] = {
        "GreatSword", "Longsword", "Sword", "MorningStar",
        "OgreAxe", "StaffWooden", "BattleAxe", "FanChakram",
        "CrossBow", "Bow", "Quiver", "OblivionHeart",
        "RoundChakram", "WarHammer", "OblivionHalberd", "OblivionWierdling"
    };
    return name[index % 16];
}

string ArmorList(int index)
{
    string name[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots",
        "PlateLeggings", "MedievalCloak", "ChainCoif", "ChainLeggings",
        "ChainTunic", "SteelHelm", "LeatherArmbands", "LeatherArmor",
        "LeatherArmoredBoots", "LeatherBoots", "LeatherHelm", "LeatherLeggings",
        "MedievalPants", "MedievalShirt", "ConjurerHelm", "WizardHelm",
        "WizardRobe", "StreetShirt", "StreetPants", "StreetSneakers"
    };
    return name[index % 24];
}

string StaffList(int index)
{
    string name[] = {
        "DeathRayWand", "DeathRayWand", "LesserFireballWand", "LesserFireballWand", 
        "FireStormWand", "InfinitePainWand", "ForceWand", "SulphorousFlareWand",
        "SulphorousShowerWand"};

    return name[index % 9];
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

int SummonHecubah(int wp)
{
    int unit = CreateObject("Hecubah", wp);
    int uec = GetMemory(GetMemory(0x750710) + 0x2ec);

    if (uec)
    {
        SetMemory(uec + 0x528, ToInt(1.0));
        SetMemory(uec + 0x520, ToInt(400.0));
        SetMemory(uec + 0x5a8, 0x0f0000);
        SetMemory(uec + 0x5b0, 0x0f0000);
        SetMemory(uec + 0x5c0, 0x0f0000);
        uec += 0x5d0;
        SetMemory(uec + GetSpellNumber("SPELL_DEATH_RAY"), 0x40000000);
		SetMemory(uec + GetSpellNumber("SPELL_SHIELD"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_SLOW"), 0x20000000);
		SetMemory(uec + GetSpellNumber("SPELL_INVISIBILITY"), 0x10000000);
        SetMemory(uec + GetSpellNumber("SPELL_INVERSION"), 0x8000000);
        SetMemory(uec + GetSpellNumber("SPELL_CHAIN_LIGHTNING"), 0x40000000);
    }
    return unit;
}

void InitSignDescription()
{
    RegistSignMessage(Object("CaveSign1"), "향상된 작살을 배울 수 있습니다. 10,000 골드가 요구됩니다");
    RegistSignMessage(Object("CaveSign2"), "소지하고 있는 모든 아이템을 무적화 시켜줍니다. 8,000 골드가 요구됩니다");
    RegistSignMessage(Object("CaveSign3"), "향상된 조심스럽게 걷기을 배울 수 있습니다. 10,000 골드가 요구됩니다");
    RegistSignMessage(Object("CaveSign4"), "마법이 깃든 슈리켄 매점입니다. 비싸지만 그 만큼 값집니다");
    RegistSignMessage(Object("CaveSign5"), "이 비콘은 당신을 필드의 센터지점으로 이동시켜 줄 것입니다");
    RegistSignMessage(Object("CaveSign6"), "얼음동굴 베이스 캠프지역: 괴물이나 타 지역 사람은 출입을 금합니다");
    RegistSignMessage(Object("CaveSign7"), "-잠시 쉬어가는 곳- 죽은자는 부활을 위해 이 복도를 따라 끝으로 이동하시오");
    RegistSignMessage(Object("CaveSign8"), "-호렌더스 고용센터- 가격은 30,000 골드 입니다");
    RegistSignMessage(Object("demonsig1"), "용의 숨결 지팡이 구입 15000골드입니다");
}

static void NetworkUtilClientMain()
{
    PlayerClassCommonWhenEntry();
}

void PlayerClassCommonWhenEntry()
{
    AppendAllDummyStaffsToWeaponList();
}

void FixColorPotionThingDbSection(int thingId, int amount)
{
    int *thingdbBase = 0x7520d4;
    int *thingdbArray = thingdbBase[0];

    int *target = thingdbArray[thingId];
    
    target[7] |= 0x10;
    target[50] = 0x53ef70;
    if (target[51] == 0)
    {
        int *healAmount = MemAlloc(4);
        healAmount[0] = amount;
        target[51] = healAmount;
    }
    target[52] = 4;
}

void ArrowSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 9.0), yvect = UnitAngleSin(OTHER, 9.0);
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + xvect -(yvect*2.0), GetObjectY(OTHER) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(OTHER);

    while (++rep<5)
    {
        CreateObjectAtEx("ArcherArrow", GetObjectX(posUnit), GetObjectY(posUnit), &single);
        SetOwner(OTHER, single);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        LookWithAngle(single, dir);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
}

void ForceOfNatureCollide()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 400, 14);
        Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Delete(SELF);
        Delete(GetTrigger() + 1);
    }
    else if (!GetCaller())
    {
        Delete(SELF);
        Delete(GetTrigger() + 1);
    }
}

int ForceOfNature(int sOwner)
{
    int mis = CreateObjectAt("GameBall", GetObjectX(sOwner) + UnitAngleCos(sOwner, 19.0), GetObjectY(sOwner) + UnitAngleSin(sOwner, 19.0));

    SetUnitCallbackOnCollide(mis, ForceOfNatureCollide);
    SetOwner(sOwner, CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(mis), GetObjectY(mis)));
    DeleteObjectTimer(mis, 90);
    DeleteObjectTimer(mis + 1, 100);
    return mis;
}

void OblivionUseHandler()
{
    if (HasEnchant(OTHER, "ENCHANT_ETHEREAL"))
        return;
    else if (CurrentHealth(OTHER))
    {
        PushObject(ForceOfNature(OTHER), 20.0, GetObjectX(OTHER), GetObjectY(OTHER));
        PlaySoundAround(OTHER, SOUND_ImpShoot);
        Enchant(OTHER, "ENCHANT_ETHEREAL", 0.9);
    }
}

void PlacingArrowSword(float xpos, float ypos, int *pDest)
{
    int sword = CreateObjectAt("GreatSword", xpos, ypos);

    if (pDest)
        pDest[0] = sword;

    SetWeaponPropertiesDirect(sword, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Matrial5, ITEM_PROPERTY_stun4, ITEM_PROPERTY_fire3);
    RegistWeaponCPropertyExecScript(sword, 2, ArrowSwordTriggered);
}

void PlacingOblivionOrb(float xpos, float ypos, int *pDest)
{
    int staff = CreateObjectAt("OblivionOrb", xpos, ypos);

    if (pDest)
        pDest[0] = staff;

    SetUnitCallbackOnUseItem(staff, OblivionUseHandler);
    DisableOblivionItemPickupEvent(staff);
    SetItemPropertyAllowAllDrop(staff);
}

void ArrowSwordDesc()
{
    TellStoryUnitName("AmuletDrop", "", "에로우 서드");
    UniPrint(OTHER, "에로우 서드를 구입하시겠어요? 금액은 8만 입니다");    
}

void ArrowSwordTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 80000)
    {
        ChangeGold(OTHER, -80000);
        int sword;

        PlacingArrowSword(GetObjectX(OTHER), GetObjectY(OTHER), &sword);
        Enchant(sword, EnchantList(ENCHANT_FREEZE), 0.0);
        UniPrint(OTHER, "거래가 완료되었어요!");
    }
    else
    {
        UniPrint(OTHER, "잔액이 부족합니다");
    }
}

void OrbStaffDesc()
{
    TellStoryUnitName("AmuletDrop", "", "망가의 지팡이");
    UniPrint(OTHER, "망가의 지팡이를 구입하시겠어요? 금액은 8만 입니다"); 
}

void OrbStaffTrade()
{
    if (GetAnswer(SELF) ^ 1) return;
    if (GetGold(OTHER) >= 80000)
    {
        ChangeGold(OTHER, -80000);
        int staff;

        PlacingOblivionOrb(GetObjectX(OTHER), GetObjectY(OTHER), &staff);
        Enchant(staff, EnchantList(ENCHANT_FREEZE), 0.0);
        UniPrint(OTHER, "거래가 완료되었어요!");
    }
    else
    {
        UniPrint(OTHER, "잔액이 부족합니다");
    }
}

void PlacingSpecialMarket()
{
    int special1;
    
    DummyUnitCreateAtEx("WizardGreen", LocationX(109), LocationY(109), &special1);

    StoryPic(special1, "CrazyGirlPic");
    SetDialog(special1, "YESNO", ArrowSwordDesc, ArrowSwordTrade);

    int special2;

    DummyUnitCreateAtEx("WizardWhite", LocationX(110), LocationY(110), &special2);

    StoryPic(special2, "CrazyGirlPic");
    SetDialog(special2, "YESNO", OrbStaffDesc, OrbStaffTrade);
}

