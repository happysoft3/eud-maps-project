
#include "xtower_player.h"
#include "xtower_mon.h"
#include "xtower_initscan.h"
#include "xtower_reward.h"
#include "libs/coopteam.h"
#include "libs/indexloop.h"

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObjectById(OBJ_ROTTEN_MEAT, GetObjectX(unit),GetObjectY(unit)));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void doInitializeReward(int lastUnit)
{
    int initScanHash;
    HashCreateInstance(&initScanHash);
    InitializeRewardData();
    HashPushback(initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    // HashPushback(initScanHash, OBJ_HECUBAH_MARKER, CreateFinalBossAmulet);
    // HashPushback(initScanHash, OBJ_NECROMANCER_MARKER, CreateRandomMonster);
    // HashPushback(initScanHash, OBJ_REWARD_MARKER_PLUS, CreateRandomMinion);
    StartUnitScan(Object("firstscan"), lastUnit, initScanHash);
}

int MasterUnit()
{
    int unit;

    if (!unit)
    {
        unit = CreateObjectById(OBJ_HECUBAH, 100.0,100.0);
        Frozen(unit, TRUE);
    }
    return unit;
}

void onWaspHiveDestroyed(){
    float x=GetObjectX(SELF),y=GetObjectY(SELF);
    CreateObjectById(OBJ_WASP_NEST_DESTROY,x,y);
    DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE,x,y), 12);
    int count=Random(1,4);
    while (--count>=0) CreateObjectById(OBJ_WASP, x,y);
    PlaySoundAround(SELF, SOUND_PoisonTrapTriggered);
}

int PlaceWaspNest(int wp)
{
    int unit = CreateObjectById(OBJ_WASP_NEST, LocationX(wp),LocationY(wp));

    SetUnitMaxHealth(unit, 500);
    SetUnitCallbackOnDeath(unit, onWaspHiveDestroyed);
    return unit;
}

int FirstBossBlocks()
{
    int ptr, k;

    if (!ptr)
    {
        ptr = CreateObject("InvisibleLightBlueHigh", 150) + 1;
        CreateObject("SpikeBlock", 150);
        CreateObject("SpikeBlock", 151);
        CreateObject("SpikeBlock", 152);
        CreateObject("SpikeBlock", 153);
        CreateObject("SpikeBlock", 154);
        CreateObject("SpikeBlock", 155);
        CreateObject("SpikeBlock", 156);
        CreateObject("SpikeBlock", 157);
        CreateObject("SpikeBlock", 158);
        CreateObject("SpikeBlock", 159);
        CreateObject("SpikeBlock", 170);
        CreateObject("SpikeBlock", 171);
        CreateObject("SpikeBlock", 172);
        CreateObject("SpikeBlock", 173);
        CreateObject("SpikeBlock", 174);
        CreateObject("SpikeBlock", 175);
        CreateObject("SpikeBlock", 176);
        CreateObject("SpikeBlock", 177);
        CreateObject("SpikeBlock", 178);
        CreateObject("SpikeBlock", 179);
        for (k = 0 ; k < 10 ; k += 1)
        {
            LookWithAngle(ptr + k, 160 + k);
            LookWithAngle(ptr + k + 10, 180 + k);
            CreateMoverFix(ptr + k, 0, 6.0);
            CreateMoverFix(ptr + k + 10, 0, 6.0);
        }
    }
    return ptr;
}

void ResetRow(int ptr)
{
    LookWithAngle(ptr, 0);
}

void StartBlockTriggers()
{
    int ptr = FirstBossBlocks(), k;

    if (!GetDirection(ptr - 1))
    {
        LookWithAngle(ptr - 1, 1);
        AudioEvent("SpikeBlockMove", 164);
        AudioEvent("SpikeBlockMove", 169);
        for (k = 19 ; k >= 0 ; k -= 1)
        {
            Move(ptr + k, GetDirection(ptr + k));
        }
        FrameTimerWithArg(150, ptr - 1, ResetRow);
    }
}

void PutGenerators()
{
    int k, base = CreateObjectAt("InvisibleLightBlueLow", LocationX(326), LocationY(326)) + 1;

    Delete(base);
    for (k = 0 ; k < 5 ; k += 1)
    {
        Enchant(CreateObject("NecromancerGenerator", 326), "ENCHANT_INVULNERABLE", 0.0);
        ObjectOff(base + k);
        SetUnitMass(base + k, 22.0);
        TeleportLocationVector(326, 46.0, 46.0);
    }
}

string GamePlayStr(int num)
{
    string table[] = {
        "게임방법- 오르비스 탑 최상층에 도달하는 것이 당신의 임무에요",
        "총 24층이 존재하며, 한 층에 존재하는 모든 몬스터를 처리해야만 다음 층으로 넘어갈 수 있습니다",
        "참고로 다음층으로 넘어가면 이전 층으로는 더 이상 이동할 수 없습니다",
        "일부 구간에서는 보스 몬스터가 존재합니다, 보스 몬스터는 특수한 스킬을 사용하므로 주의하세요",
        "몬스터를 잡을 때 마다 돈이 증가되며, 돈은 상점에서 여러가지 능력이 깃든 아이템들을 구입하는 데 유용할 것입니다",
        "상점 품목 중 열쇠 아이템은 상점 내 3개의 창고를 여는데 쓰입니다, 일부 품목의 경우 최대 5개 까지만 구입할 수 있습니다",
        "필드에서 마을로 돌아가려면 필드 시작지점에 있는 요정을 클릭하면, 잠시 후 마을로 이동됩니다",
        "필드에서 마을로 이동하는 것은 독저항 물약을 사용해도 가능합니다",
        "설명이 모두 끝났습니다, 이제 게임을 진행하세요"
    };
    
    return table[num];
}

void HowToPlay(int order)
{
    if (order < 9)
    {
        UniPrintToAll(GamePlayStr(order));
        SecondTimerWithArg(4, order + 1, HowToPlay);
    }
}

void GoLastTransmission()
{
    if (IsPlayerUnit(OTHER) && CurrentHealth(OTHER))
    {
        MoveObject(OTHER, GetWaypointX(3), GetWaypointY(3));
        AudioEvent("BlindOff", 3);
        DeleteObjectTimer(CreateObject("BlueRain", 3), 30);
        char msg[128];
        int floor;

        QueryFloorLevel(&floor, 0);
        ++floor;
        NoxSprintfString(msg,"오르비스 탑 %d 층", &floor, 1);
        UniPrint(OTHER,ReadStringAddressEx(msg));
    }
}

void GoShop()
{
    if (IsPlayerUnit(OTHER) && CurrentHealth(OTHER))
    {
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER, GetWaypointX(292), GetWaypointY(292));
        AudioEvent("BlindOff", 292);
        UniPrint(OTHER, "이곳은 상점입니다-!");
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void BackToBasecamp()
{
    if (HasClass(OTHER, "PLAYER") && CurrentHealth(OTHER))
    {
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER, GetWaypointX(333), GetWaypointY(333));
        AudioEvent("BlindOff", 333);
        UniPrint(OTHER, "상점에서 나갔습니다");
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void BackToStartLocation()
{
    int ptr;

    if (!HasEnchant(OTHER, "ENCHANT_BURNING"))
    {
        ptr = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_HIGH, GetObjectX(OTHER), GetObjectY(OTHER));
        CreateObjectById(OBJ_VORTEX_SOURCE, GetObjectX(OTHER), GetObjectY(OTHER));
        Raise(ptr, ToFloat(GetCaller()));
        CheckTeleportLoop(ptr);
        UniPrint(OTHER,"시작위치로 공간이동을 합니다, 취소하려면 비콘에서 벗어나십시오");
        Enchant(OTHER, "ENCHANT_BURNING", 0.0);
    }
}

void CheckTeleportLoop(int ptr)
{
    int count = GetDirection(ptr), unit;

    if (count < 90)
    {
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, CheckTeleportLoop);
    }
    else
    {
        unit = ToInt(GetObjectZ(ptr));
        if (CurrentHealth(unit) && HasEnchant(unit, "ENCHANT_BURNING"))
        {
            EnchantOff(unit, "ENCHANT_BURNING");
            MoveObject(unit, GetWaypointX(8), GetWaypointY(8));
            CrossLightningFx();
            AudioEvent("BlindOff", 8);
            AudioEvent("LightningBolt", 8);
        }
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void CrossLightningFx()
{
    Effect("LIGHTNING", GetWaypointX(4), GetWaypointY(4), GetWaypointY(5), GetWaypointY(5));
    Effect("LIGHTNING", GetWaypointX(5), GetWaypointY(5), GetWaypointY(7), GetWaypointY(7));
    Effect("LIGHTNING", GetWaypointX(6), GetWaypointY(6), GetWaypointY(7), GetWaypointY(7));
    Effect("LIGHTNING", GetWaypointX(4), GetWaypointY(4), GetWaypointY(6), GetWaypointY(6));
    Effect("LIGHTNING", GetWaypointX(4), GetWaypointY(4), GetWaypointY(7), GetWaypointY(7));
    Effect("LIGHTNING", GetWaypointX(6), GetWaypointY(6), GetWaypointY(5), GetWaypointY(5));
}

void AfterTriggersExec()
{
    int entry = CreateObject("Maiden", 78);

    Frozen(entry, 1);
    SetCallback(entry, 9, GoLastTransmission);
    Putchar();
    CmdLine("set spell SPELL_CHARM off", FALSE);
    DefPointerOfFunc();
    PutDecorations();
    InitStatues(0);
    FrameTimer(70, InitShopItems);
    FrameTimer(120, PlacedMobF1);
    FrameTimerWithArg(100, 0, HowToPlay);
}

void Putchar()
{
    int count, k;

    if (count < 12)
    {
        MoveWaypoint(79, GetWaypointX(80), GetWaypointY(80));
        for (k = 9 ; k >= 0 ; k --)
        {
            Frozen(CreateObject(ItemList(count), 79), 1);
            CheckWeaponProperty(GetMemory(0x750710));
            MoveWaypoint(79, GetWaypointX(79) + 32.0, GetWaypointY(79) + 32.0);
        }
        MoveWaypoint(80, GetWaypointX(80) - 25.0, GetWaypointY(80) + 25.0);
        count ++;
        FrameTimer(1, Putchar);
    }
}

void CheckWeaponProperty(int ptr)
{
    int id = GetMemory(ptr + 4);

    if (id >= 222 && id <= 225)
        SetMemory(ptr + 0x2c4, 0x53a720);
    else if (id == 1178)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
    else if (id == 1168)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
}

string ItemList(int num)
{
    string table[] =
    {
        "FanChakram", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings",
        "WarHammer", "GreatSword", "RoundChakram", "OrnateHelm", "SteelShield",
        "RedPotion", "CurePoisonPotion", "OblivionHeart", "OgreAxe"
    };
    return table[num];
}

string PotionList(int num)
{
    string table[] =
    {
        "RedPotion", "CurePoisonPotion","PoisonProtectPotion", "FireProtectPotion", "ShockProtectPotion",
        "HastePotion", "ShieldPotion", "YellowPotion", "BlackPotion"
    };
    return table[num];
}

string GoldList(int num)
{
    string table[] = {"QuestGoldChest", "QuestGoldPile", "Gold"};

    return table[num];
}

int GoldDrop(int posUnit)
{
    int unit = CreateObjectAt(GoldList(Random(0, 2)), GetObjectX(posUnit), GetObjectY(posUnit));
    int ptr = GetMemory(0x750710);

    SetMemory(GetMemory(ptr + 0x2b4), Random(1000, 9000));
    return unit;
}

void DefPointerOfFunc()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 10);

    CreateObject("InvisibleLightBlueHigh", 11);
    Raise(ptr, PlacedMobF1);
    Raise(ptr + 1, OpenWallsArea11);
    // Set_fptr = ToInt(GetObjectZ(ptr));
    Next_fptr = ToInt(GetObjectZ(ptr + 1));
    Delete(ptr);
    Delete(ptr + 1);
}

void PutDecorations()
{
    int ptr = CreateObject("MovableStatue2h", 4);
    CreateObject("MovableStatue2b", 5);
    CreateObject("MovableStatue2f", 6);
    CreateObject("MovableStatue2d", 7);
    Frozen(CreateObject("DunMirScaleTorch1", 191), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 192), 1);

    Frozen(CreateObject("PiledBarrels1", 193), 1);
    MoveWaypoint(193, GetWaypointX(193) + 23.0, GetWaypointY(193) + 23.0);
    Frozen(CreateObject("PiledBarrels1", 193), 1);
    Enchant(ptr, "ENCHANT_FREEZE", 0.0);
    Enchant(ptr + 1, "ENCHANT_FREEZE", 0.0);
    Enchant(ptr + 2, "ENCHANT_FREEZE", 0.0);
    Enchant(ptr + 3, "ENCHANT_FREEZE", 0.0);
    FrameTimer(1, PutDecorations2);
}

void PutDecorations2()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 190);
    Frozen(CreateObject("Maiden", 291), 1);
    Frozen(CreateObject("Maiden", 293), 1);
    SetCallback(ptr + 1, 9, GoShop);
    SetCallback(ptr + 2, 9, BackToBasecamp);

    Frozen(CreateObject("DunMirScaleTorch2", 284), 1);
    Frozen(CreateObject("DunMirScaleTorch2", 285), 1);
    Frozen(CreateObject("DunMirScaleTorch2", 287), 1);
    Frozen(CreateObject("DunMirScaleTorch2", 288), 1);
    Frozen(CreateObject("DunMirScaleTorch1", 286), 1);
    Frozen(CreateObject("TorchPole", 289), 1);
}

void FixGoingBackMark()
{
    FrameTimerWithArg(3, Start_wp, HomePortal);
}

void TeleportToNextArea()
{
    if (CurrentHealth(OTHER) && IsPlayerUnit(OTHER))
    {
        int floor;
        QueryFloorLevel(&floor,0);
        if (!IsObjectOn(SELF))
        {
            TeleportLocation(3, LocationX(Start_wp), LocationY(Start_wp));
            // Floor_lv ++;
            QueryFloorLevel(0,++floor);
            FrameTimer(1, GetPlaceMobFunction(floor));
            ObjectOn(SELF);
            return;
        }
        char msg[128];

        MoveObject(OTHER, LocationX(3), LocationY(3));
        NoxSprintfString(msg,"오르비스 탑 %d층",&floor,1);
        UniPrint(OTHER, ReadStringAddressEx(msg));
    }
}

void GoldEventWhenDeadBoss(int amount)
{
    int r=MAX_PLAYER_COUNT;

    while (r>=0)
    {
        if (CurrentHealth(GetPlayer(r)))
            ChangeGold(GetPlayer(r), amount);
    }
    char msg[192];
    NoxSprintfString(msg, "보스를 처치하셨습니다, 모든 플레이어에게 미네랄 %d이(가) 제공됩니다", &amount, 1);
    UniPrintToAll(ReadStringAddressEx(msg));
}

void MecaFlyingWeapon()
{
    int mis;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.3);
            MoveWaypoint(9, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 7.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 7.0));
            mis = CreateObject("WeakArcherArrow", 9);
            Enchant(mis, "ENCHANT_SHOCK", 0.0);
            SetOwner(SELF, mis);
            LookAtObject(mis, OTHER);
            PushObject(mis, -38.0, GetObjectX(OTHER), GetObjectY(OTHER));
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(21, GetTrigger(), ResetUnitSight);
        }
    }
}

void LichWeapon()
{
    int mis;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.6);
            MoveWaypoint(9, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 7.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 7.0));
            mis = CreateObject("DeathBallFragment", 9);
            SetOwner(SELF, mis);
            PushObject(mis, -17.0, GetObjectX(OTHER), GetObjectY(OTHER));
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(21, GetTrigger(), ResetUnitSight);
        }
    }
}

void BearJumping()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 2.0);
            Raise(SELF, 250.0);
            FrameTimerWithArg(21, GetTrigger(), EarthQuakeFromBear);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(90, GetTrigger(), ResetUnitSight);
        }
    }
}

void DryadDoSummon()
{
    int ptr;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 10.0);
            MoveWaypoint(9, GetObjectX(OTHER), GetObjectY(OTHER));
            ptr = CreateObject("InvisibleLightBlueHigh", 9);
            CreateObject("InvisibleLightBlueHigh", 9);
            Raise(ptr, UnitRatioX(SELF, OTHER, 46.0));
            Raise(ptr + 1, UnitRatioY(SELF, OTHER, 46.0));
            SetOwner(SELF, ptr);
            FrameTimerWithArg(1, GetTrigger(), SpawningPool);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(300, GetTrigger(), ResetUnitSight);
        }
    }
}

void GoonWeapon()
{
    int mis;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.7);
            MoveWaypoint(9, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 12.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 12.0));
            AudioEvent("EggBreak", 9);
            mis = CreateObject("ThrowingStone", 9);
            CreateObject("SpiderSpit", 9);
            SetOwner(SELF, mis);
            SetOwner(SELF, mis + 1);
            PushObject(mis, -30.0, GetObjectX(OTHER), GetObjectY(OTHER));
            PushObject(mis + 1, -30.0, GetObjectX(OTHER), GetObjectY(OTHER));
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.07);
        LookAtObject(SELF, OTHER);
        CreatureFollow(SELF, OTHER);
        AggressionLevel(SELF, 1.0);
    }
}

void PlantAi()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 5.0);
            LookAtObject(SELF, OTHER);
            CreatureFollow(SELF, OTHER);
            AggressionLevel(SELF, 1.0);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(90, GetTrigger(), ResetUnitSight);
        }
    }
}

void BossGolemWeapon()
{
    int rnd, ptr;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 4.0);
            LookAtObject(SELF, OTHER);
            rnd = Random(0, 3);
            if (!rnd)
            {
                MoveWaypoint(9, GetObjectX(OTHER), GetObjectY(OTHER));
                ptr = CreateObject("InvisibleLightBlueHigh", 9);
                SetOwner(SELF, ptr);
                Raise(ptr, ToFloat(GetCaller()));
                Enchant(SELF, "ENCHANT_FREEZE", 1.0);
                FrameTimerWithArg(30, ptr, DropFist);
            }
            else if (rnd == 1)
            {
                MoveWaypoint(9, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0));
                ptr = CreateObject("InvisibleLightBlueHigh", 9);
                CreateObject("InvisibleLightBlueHigh", 9);
                MoveObject(ptr + 1, GetObjectX(OTHER), GetObjectY(OTHER));
                Raise(ptr, UnitRatioX(SELF, OTHER, 15.0));
                Raise(ptr + 1, UnitRatioY(SELF, OTHER, 15.0));
                Enchant(ptr, "ENCHANT_SHOCK", 0.0);
                SetOwner(SELF, ptr);
                FrameTimerWithArg(1, ptr, GolemSkill);
            }
            else if (rnd == 2)
            {
                MoveWaypoint(9, GetObjectX(SELF), GetObjectY(SELF));
                ptr = CreateObject("InvisibleLightBlueHigh", 9);
                CreateObject("InvisibleLightBlueHigh", 9);
                Raise(ptr, UnitRatioX(SELF, OTHER, 10.0));
                Raise(ptr + 1, UnitRatioY(SELF, OTHER, 10.0));
                SetOwner(SELF, ptr);
                ObjectOff(SELF);
                Enchant(SELF, "ENCHANT_VAMPIRISM", 0.0);
                SetCallback(SELF, 9, GolemTouchedOnRush);
                WindRush(ptr);
            }
            else
            {
                LookAtObject(SELF, OTHER);
                HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
                Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
                CastSpellObjectObject("SPELL_EARTHQUAKE", SELF, SELF);
                CastSpellObjectObject("SPELL_EARTHQUAKE", SELF, SELF);
                CastSpellObjectObject("SPELL_EARTHQUAKE", SELF, SELF);
            }
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(150, GetTrigger(), ResetUnitSight);
        }
    }
}

void SpiderWhenDead()
{
    int ptr;
    MoveWaypoint(9, GetObjectX(SELF), GetObjectY(SELF));
    AudioEvent("BeholderDie", 9);
    ptr = CreateObject("WaterBarrel", 9);
    DeleteObjectTimer(CreateObject("BigSmoke", 9), 12);
    Damage(ptr, 0, CurrentHealth(ptr) + 1, -1);
    CreateObject("ArachnaphobiaFocus", 9);
    DeadMonster();
}

void DeadWorkerWeapon()
{
    int ptr;
    float pos_x, pos_y;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
            pos_x = UnitRatioX(SELF, OTHER, 16.0);
            pos_y = UnitRatioY(SELF, OTHER, 16.0);
            MoveWaypoint(9, GetObjectX(SELF) - pos_x * 2.0, GetObjectY(SELF) - pos_y * 2.0);
            ptr = CreateObject("Maiden", 9);
            CreateObject("MagicEnergy", 9);
            CreateObject("InvisibleLightBlueHigh", 9);
            CreateObject("InvisibleLightBlueHigh", 9);
            LookWithAngle(ptr, 0);
            SetOwner(SELF, ptr);
            Raise(ptr + 2, pos_x);
            Raise(ptr + 3, pos_y);
            SetCallback(ptr, 9, TouchWorkMis);
            Frozen(ptr, 1);
            FrameTimerWithArg(1, ptr, WorkerFlyingMissile);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(35, GetTrigger(), ResetUnitSight);
        }
    }
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_DETECTING");
    Enchant(unit, "ENCHANT_BLINDED", 0.07);
    AggressionLevel(unit, 1.0);
}

void EarthQuakeFromBear(int unit)
{
    if (CurrentHealth(unit))
    {
        Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        Effect("DAMAGE_POOF", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        CastSpellObjectObject("SPELL_EARTHQUAKE", unit, unit);
        CastSpellObjectObject("SPELL_EARTHQUAKE", unit, unit);
        CastSpellObjectObject("SPELL_EARTHQUAKE", unit, unit);
    }
}

void SpawningPool(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && count < 4)
    {
        MoveWaypoint(9, GetObjectX(ptr), GetObjectY(ptr));
        MoveWaypoint(41, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        unit = CreateObject("CarnivorousPlant", 9);
        if (count)
        {
            CreateObject("CarnivorousPlant", 41);
            DeleteObjectTimer(CreateObject("BigSmoke", 41), 9);
            Effect("RICOCHET", GetWaypointX(41), GetWaypointY(41), 0.0, 0.0);
            SetOwner(owner, unit + 1);
        }
        DeleteObjectTimer(CreateObject("BigSmoke", 9), 9);
        Effect("RICOCHET", GetWaypointX(9), GetWaypointY(9), 0.0, 0.0);
        SetOwner(owner, unit);
        MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr + 1), GetObjectY(ptr) - GetObjectZ(ptr));
        MoveObject(ptr + 1, GetObjectX(ptr + 1) - GetObjectZ(ptr + 1), GetObjectY(ptr + 1) + GetObjectZ(ptr));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(3, ptr, SpawningPool);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void DropFist(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        CastSpellObjectObject("SPELL_FIST", owner, target);
    }
    Delete(ptr);
}

void WindRush(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    if (count < 60 && CurrentHealth(owner))
    {
        Effect("DAMAGE_POOF", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
        PushObjectTo(owner, -GetObjectZ(ptr), -GetObjectZ(ptr + 1));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, WindRush);
    }
    else
    {
        if (CurrentHealth(owner))
        {
            ObjectOn(owner);
            EnchantOff(owner, "ENCHANT_VAMPIRISM");
            SetCallback(owner, 9, GolemTouchedOnNormal);
        }
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void GolemTouchedOnNormal()
{
    return;
}

void GolemTouchedOnRush()
{
    if (CurrentHealth(SELF) && CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, MasterUnit()) && HasEnchant(SELF, "ENCHANT_AFRAID"))
        {
            Damage(OTHER, SELF, 100, 4);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.5);
        }
    }
}

void WorkerFlyingMissile(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 32 && IsObjectOn(ptr))
    {
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr + 2), GetObjectY(ptr) - GetObjectZ(ptr + 3));
        MoveObject(ptr + 1, GetObjectX(ptr), GetObjectY(ptr));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, WorkerFlyingMissile);
    }
    else
    {
        if (IsObjectOn(ptr))
            Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
        Delete(ptr + 3);
    }
}

void TouchWorkMis()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 20, 4);
        Delete(SELF);
    }
}

void MysticLoop(int unit)
{
    if (CurrentHealth(unit))
    {
        if (HasEnchant(unit, "ENCHANT_INVISIBLE"))
        {
            EnchantOff(unit, "ENCHANT_INVISIBLE");
            Enchant(unit, "ENCHANT_SHOCK", 0.0);
            Enchant(unit, "ENCHANT_SHIELD", 0.0);
        }
        Raise(unit, 100.0);
        FrameTimerWithArg(1, unit, MysticLoop);
    }
}

void ImpShots()
{
    int ptr;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.4);
            MoveWaypoint(9, GetObjectX(SELF), GetObjectY(SELF));
            ptr = CreateObject("InvisibleLightBlueHigh", 9);
            CastSpellObjectObject("SPELL_MAGIC_MISSILE", SELF, OTHER);
            Delete(ptr);
            Delete(ptr + 2);
            Delete(ptr + 3);
            Delete(ptr + 4);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(20, GetTrigger(), ResetUnitSight);
        }
    }
}

void SecondBossWeapon()
{
    int ptr, rnd;
    float pos_x, pos_y;

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 7.0);
            rnd = Random(0, 3);
            if (!rnd)
            {
                MoveWaypoint(95, GetObjectX(OTHER), GetObjectY(OTHER));
                ptr = CreateObject("InvisibleLightBlueHigh", 95);
                CreateObject("BlueSummons", 95);
                SetOwner(SELF, ptr);
                AudioEvent("PullCast", 95);
                Effect("RICOCHET", GetWaypointX(95), GetWaypointY(95), 0.0, 0.0);
                FrameTimerWithArg(21, ptr, DelayTargetDeathray);
            }
            else if (rnd == 1)
            {
                MoveWaypoint(95, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 30.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 30.0));
                ptr = CreateObject("VandegrafLargeMovable", 95);
                SetOwner(ptr, SELF);
                CastSpellObjectObject("SPELL_CHAIN_LIGHTNING", ptr, ptr);
                FrameTimerWithArg(60, ptr, DelayRemoveUnit);
            }
            else if (rnd == 2)
                ThuderStorm();
            else
                Boss2SplashSkill();
        }
        else if (!HasEnchant(SELF, "ENCHANT_LIGHT"))
        {
            Enchant(SELF, "ENCHANT_LIGHT", 0.8);
            Boss2NormalAttack();
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(90, GetTrigger(), ResetUnitSight);
        }
    }
}

void ShootingWhiteCrystal(int ptr)
{
    int count = GetDirection(ptr);

    if (count < 32 && IsObjectOn(ptr))
    {
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr + 2), GetObjectY(ptr) - GetObjectZ(ptr + 3));
        MoveObject(ptr + 1, GetObjectX(ptr), GetObjectY(ptr));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, ShootingWhiteCrystal);
    }
    else
    {
        if (IsObjectOn(ptr))
            Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
        Delete(ptr + 3);
    }
}

void WhiteCrystalExplosion()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 25, 16);
        Delete(SELF);
    }
    else if (!GetCaller())
        Delete(SELF);
}

void DelayTargetDeathray(int ptr)
{
    int owner = GetOwner(ptr);

    if (CurrentHealth(owner))
    {
        CastSpellObjectLocation("SPELL_DEATH_RAY", ptr, GetObjectX(ptr), GetObjectY(ptr));
        CastSpellObjectLocation("SPELL_DEATH_RAY", owner, GetObjectX(ptr), GetObjectY(ptr));
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void DelayRemoveUnit(int unit)
{
    Delete(unit);
}

void ThuderStorm()
{
    int ptr;
    float pos_x = UnitRatioX(SELF, OTHER, 30.0), pos_y = UnitRatioY(SELF, OTHER, 30.0);

    Enchant(SELF, "ENCHANT_FREEZE", 3.0);
    Enchant(SELF, "ENCHANT_INVULNERABLE", 3.0);
    MoveWaypoint(9, GetObjectX(SELF) - pos_x, GetObjectY(SELF) - pos_y);
    ptr = CreateObject("InvisibleLightBlueHigh", 9);
    CreateObject("InvisibleLightBlueHigh", 9);
    Raise(ptr, pos_x);
    Raise(ptr + 1, pos_y);
    SetOwner(SELF, ptr);
}

void GoAwayLightning(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), unit;

    if (count < 32 && IsVisibleTo(ptr, owner) && CurrentHealth(owner))
    {
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr), GetObjectY(ptr) - GetObjectZ(ptr + 1));
        MoveWaypoint(9, GetObjectX(ptr), GetObjectY(ptr));
        unit = CreateObject("InvisibleLightBlueHigh", 9);
        SetOwner(SELF, unit);
        CreateObject("MagicSpark", 9);
        AudioEvent("LightningBolt", 9);
        AudioEvent("LightningWand", 9);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(17, unit, ThunderDown);
        FrameTimerWithArg(1, ptr, GoAwayLightning);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void ThunderDown(int ptr)
{
    int owner = GetOwner(ptr), unit;

    if (CurrentHealth(owner))
    {
        MoveWaypoint(41, GetObjectX(ptr), GetObjectY(ptr));
        Effect("LIGHTNING", GetWaypointX(41), GetWaypointY(41), GetWaypointY(41), GetWaypointY(41) - 120.0);
        Effect("JIGGLE", GetWaypointX(41), GetWaypointY(41), 41.0, 0.0);
        AudioEvent("EarthquakeCast", 41);
        AudioEvent("EnergyBoltCast", 41);
        unit = CreateObject("Maiden", 41);
        Frozen(unit, 1);
        SetCallback(unit, 9, ThunderHurt);
        DeleteObjectTimer(unit, 1);
        SetOwner(owner, unit);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void ThunderHurt()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 99, 9);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.8);
    }
}

void Boss2NormalAttack()
{
    float pos_x, pos_y;
    int ptr;

    pos_x = UnitRatioX(SELF, OTHER, 20.0);
    pos_y = UnitRatioY(SELF, OTHER, 20.0);
    MoveWaypoint(95, GetObjectX(SELF) - pos_x, GetObjectY(SELF) - pos_y);
    AudioEvent("BeholderMove", 95);
    ptr = CreateObject("Maiden", 95);
    CreateObject("GameBall", 95);
    CreateObject("InvisibleLightBlueHigh", 95);
    CreateObject("InvisibleLightBlueHigh", 95);
    SetUnitFlags(ptr + 1, GetUnitFlags(ptr + 1) ^ 0x40);
    Raise(ptr + 2, pos_x);
    Raise(ptr + 3, pos_y);
    LookWithAngle(ptr, 0);
    Enchant(ptr, "ENCHANT_HASTED", 0.0);
    Frozen(ptr, 1);
    SetCallback(ptr, 9, WhiteCrystalExplosion);
    FrameTimerWithArg(1, ptr, ShootingWhiteCrystal);
    CreatureFollow(SELF, OTHER);
    AggressionLevel(SELF, 1.0);
}

void Boss2SplashSkill()
{
    CastSpellObjectObject("SPELL_TURN_UNDEAD", SELF, SELF);
    int k=MAX_PLAYER_COUNT;
    while (--k>=0)
    {
        if (CurrentHealth(GetPlayer(k)))
        {
            if (DistanceUnitToUnit(SELF,GetPlayer(k)) < 175.0)
                Damage(GetPlayer(k), SELF, 100, DAMAGE_TYPE_AIRBORNE_ELECTRIC);
        }
    }
}

void HorrendousDash()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 10.0);
            MoveWaypoint(95, GetObjectX(OTHER) - UnitRatioX(OTHER, SELF, 30.0), GetObjectY(OTHER) - UnitRatioY(OTHER, SELF, 30.0));
            AudioEvent("BlindOff", 95);
            Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetWaypointX(95), GetWaypointY(95));
            Effect("TELEPORT", GetWaypointX(95), GetWaypointY(95), 0.0, 0.0);
            MoveObject(SELF, GetWaypointX(95), GetWaypointY(95));
            CreatureFollow(SELF, OTHER);
            AggressionLevel(SELF, 1.0);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(60, GetTrigger(), ResetUnitSight);
        }
    }
}

void ThirdBossSkills()
{
    int rnd, ptr;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            rnd = Random(0, 5);
            Enchant(SELF, "ENCHANT_ETHEREAL", 7.0);
            if (!rnd)
                CastFireway();
            else if (rnd == 1)
            {
                CastSpellObjectLocation("SPELL_SUMMON_EMBER_DEMON", SELF, GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 30.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 30.0));
                CastSpellObjectObject("SPELL_SHIELD", SELF, SELF);
            }
            else if (rnd == 2)
                FireSpinShot();
            else if (rnd == 3)
                FallenMeteor();
            else
                CastSpellObjectObject("SPELL_FIREBALL", SELF, OTHER);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(60, GetTrigger(), ResetUnitSight);
        }
    }
}

void CastFireway()
{
    float pos_x = UnitRatioX(SELF, OTHER, 23.0), pos_y = UnitRatioY(SELF, OTHER, 23.0);
    int ptr = CreateObject("InvisibleLightBlueHigh", 9);
    CreateObject("InvisibleLightBlueHigh", 9);

    SetOwner(SELF, ptr);
    Raise(ptr, pos_x);
    Raise(ptr + 1, pos_y);
    MoveWaypoint(9, GetObjectX(SELF) - pos_x, GetObjectY(SELF) - pos_y);
}

void StrightFireway(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), fire;

    if (count < 24)
    {
        MoveWaypoint(9, GetObjectX(ptr), GetObjectY(ptr));
        Effect("SPARK_EXPLOSION", GetWaypointX(9), GetWaypointY(9), 0.0, 0.0);
        AudioEvent("BurnCast", 9);
        fire = CreateObject("LargeFlame", 9);
        Enchant(fire, "ENCHANT_FREEZE", 0.0);
        SetOwner(owner, fire);
        FrameTimerWithArg(65, fire, RemoveUnit);
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr), GetObjectY(ptr) - GetObjectZ(ptr + 1));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, StrightFireway);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void FireSpinShot()
{
    int ptr;
    MoveWaypoint(253, GetObjectX(SELF), GetObjectY(SELF));
    ptr = CreateObject("InvisibleLightBlueHigh", 253);
    SetOwner(SELF, ptr);
    Enchant(SELF, "ENCHANT_FREEZE", 0.0);
    FrameTimerWithArg(1, ptr, RiseupFlameTonado);
}

void RiseupFlameTonado(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), k, unit;
    float size = 34.0;

    if (count < 18 && CurrentHealth(owner))
    {
        unit = CreateObject("InvisibleLightBlueHigh", 253) + 1;
        Delete(unit - 1);
        for (k = 0 ; k < 6 ; k ++)
        {
            MoveWaypoint(253, GetObjectX(ptr) + MathSine(k * 20 + 90, size), GetObjectY(ptr) + MathSine(k * 20, size));
            Frozen(CreateObject("CarnivorousPlant", 253), 1);
            DeleteObjectTimer(CreateObject("Explosion", 253), 9);
            DeleteObjectTimer(unit + (k * 2), 1);
            SetCallback(unit + (k * 2), 9, FireSpinTouch);
            SetOwner(owner, unit + (k * 2));
            size *= 2.0;
        }
        AudioEvent("FireballCast", 253);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, RiseupFlameTonado);
    }
    else
    {
        Delete(ptr);
        if (CurrentHealth(owner))
        {
            EnchantOff(owner, "ENCHANT_FREEZE");
        }
    }
}

void FireSpinTouch()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 150, 1);
        Enchant(OTHER, "ENCHANT_CHARMING", 1.0);
    }
}

void FallenMeteor()
{
    int ptr;

    MoveWaypoint(253, GetObjectX(OTHER), GetObjectY(OTHER));
    ptr = CreateObject("InvisibleLightBlueHigh", 253);
    Raise(CreateObject("Flame", 253), 200.0);
    SetOwner(SELF, ptr + 1);
    AudioEvent("MeteorShowerCast", 253);
    FrameTimerWithArg(21, ptr, MeteorHitAtTarget);
}

void MeteorHitAtTarget(int ptr)
{
    int owner = GetOwner(ptr + 1), unit;

    if (CurrentHealth(owner))
    {
        MoveWaypoint(253, GetObjectX(ptr), GetObjectY(ptr));
        unit = CreateObject("TargetBarrel1", 253);
        SetOwner(owner, CreateObject("TitanFireball", 253));
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void KeeperWaepon()
{
    int rnd;
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 7.0);
            rnd = Random(0, 5);
            if (!rnd)
                CastToxicCloud();
            else if (rnd == 1)
                CastOnikiri();
            else if (rnd == 2)
                CastTripleShot();
            else if (rnd == 3)
                CastPoisonBall();
            else
                FrameTimerWithArg(30, GetTrigger(), TeleportInLabyrinth);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(50, GetTrigger(), ResetUnitSight);
        }
    }
}

void CastToxicCloud()
{
    int ptr;

    MoveWaypoint(253, GetObjectX(SELF), GetObjectY(SELF));
    ptr = CreateObject("InvisibleLightBlueHigh", 253);
    SetOwner(SELF, ptr);

    Chat(SELF, "내 독에 범벅이 되어 죽어라-!");
}

void ToxicSpin(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), k, unit;
    float size = 65.0;

    if (count < 15 && CurrentHealth(owner))
    {
        unit = CreateObject("InvisibleLightBlueHigh", 253) + 1;
        Delete(unit - 1);
        for (k = 0 ; k < 5 ; k ++)
        {
            MoveWaypoint(253, GetObjectX(ptr) + MathSine(k * 24 + 90, size), GetObjectY(ptr) + MathSine(k * 24, size));
            SetOwner(owner, CreateObject("Wizard", 253));
            CastSpellObjectObject("SPELL_TOXIC_CLOUD", unit + k, unit + k);
            size *= 2.0;
        }
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, ToxicSpin);
    }
    else
        Delete(ptr);
}

void CastOnikiri()
{
    float pos_x = UnitRatioX(SELF, OTHER, 25.0), pos_y = UnitRatioY(SELF, OTHER, 25.0);
    int ptr;

    MoveWaypoint(253, GetObjectX(SELF) - pos_x, GetObjectY(SELF) - pos_y);
    ptr = CreateObject("InvisibleLightBlueHigh", 253);
    CreateObject("InvisibleLightBlueHigh", 253);
    CreateObject("StormCloud", 253);
    SetOwner(SELF, ptr);
    Raise(ptr, pos_x);
    Raise(ptr + 1, pos_y);
    FrameTimerWithArg(1, ptr, OniCharge);
    Chat(SELF, "도깨비 참수-!");
}

void OniCharge(int ptr)
{
    int count = GetDirection(ptr), owner = GetOwner(ptr), unit;

    if (CurrentHealth(owner) && count < 16)
    {
        MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr), GetObjectY(ptr) - GetObjectZ(ptr + 1));
        MoveObject(ptr + 2, GetObjectX(ptr), GetObjectY(ptr));
        MoveWaypoint(253, GetObjectX(ptr), GetObjectY(ptr));
        AudioEvent("ElectricalArc1", 253);
        AudioEvent("EnergyBoltSustain", 253);
        unit = CreateObject("CarnivorousPlant", 253);
        SetOwner(owner, unit);
        Frozen(unit, 1);
        DeleteObjectTimer(unit, 1);
        SetCallback(unit, 9, OnikiriTouch);
        Effect("VIOLET_SPARKS", GetWaypointX(253), GetWaypointY(253), 0.0, 0.0);
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, OniCharge);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void OnikiriTouch()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 150, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.9);
    }
}

void CastTripleShot()
{
    float pos_x = UnitRatioX(SELF, OTHER, 23.0), pos_y = UnitRatioY(SELF, OTHER, 23.0);
    int ptr;

    MoveWaypoint(253, GetObjectX(SELF), GetObjectY(SELF));
    ptr = CreateObject("InvisibleLightBlueHigh", 253);
    CreateObject("InvisibleLightBlueHigh", 253);
    Raise(ptr, pos_x);
    Raise(ptr + 1, pos_y);
    SetOwner(SELF, ptr);
    Enchant(SELF, "ENCHANT_FREEZE", 0.0);
    FrameTimerWithArg(1, ptr, TripleArrow);
}

void TripleArrow(int ptr)
{
    float pos_x = GetObjectZ(ptr), pos_y = GetObjectZ(ptr + 1);
    int owner = GetOwner(ptr), count = GetDirection(ptr), k, unit;

    if (CurrentHealth(owner) && count < 32)
    {
        MoveWaypoint(253, GetObjectX(ptr) + (1.0 / 1.6 * pos_y) - pos_x, GetObjectY(ptr) - (1.0 / 1.6 * pos_x) - pos_y);
        unit = CreateObject("InvisibleLightBlueHigh", 253) + 1;
        Delete(unit - 1);
        for (k = 0 ; k < 9 ; k ++)
        {
            pos_x = GetRatioUnitWpX(ptr, 253, 23.0);
            pos_y = GetRatioUnitWpY(ptr, 253, 23.0);
            MoveWaypoint(253, GetObjectX(ptr) - (1.0 / 8.0 * pos_y) - pos_x, GetObjectY(ptr) + (1.0 / 8.0 * pos_x) - pos_y);
            SetOwner(owner, CreateObject("CherubArrow", 253));
            LookAtObject(unit + k, ptr);
            LookWithAngle(unit + k, GetDirection(unit + k) + 128);
            PushObject(unit + k, 27.0, GetObjectX(ptr), GetObjectY(ptr));
        }
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, TripleArrow);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void TeleportInLabyrinth(int unit)
{
    int wp = Random(267, 276);

    if (CurrentHealth(unit))
    {
        Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
        MoveObject(unit, GetWaypointX(wp), GetWaypointY(wp));
        Effect("TELEPORT", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
    }
}

void CastPoisonBall()
{
    float pos_x = UnitRatioX(SELF, OTHER, 20.0), pos_y = UnitRatioY(SELF, OTHER, 20.0);
    int ptr;

    MoveWaypoint(253, GetObjectX(SELF) - pos_x, GetObjectY(SELF) - pos_y);
    ptr = CreateObject("InvisibleLightBlueHigh", 253);
    CreateObject("InvisibleLightBlueHigh", 253);
    Frozen(CreateObject("Maiden", 253), 1);
    Frozen(CreateObject("GreenOrb", 253), 1);
    Raise(ptr, pos_x);
    Raise(ptr + 1, pos_y);
    SetOwner(SELF, ptr + 2);
    ObjectOff(ptr + 2);
    SetCallback(ptr + 2, 9, EnemyPoisoned);
    FrameTimerWithArg(1, ptr, FlyPoisonBall);
}

void FlyPoisonBall(int ptr)
{
    int owner = GetOwner(ptr + 2), count = GetDirection(ptr);

    if (count < 32)
    {
        MoveObject(ptr + 2, GetObjectX(ptr + 2) - GetObjectZ(ptr), GetObjectY(ptr + 2) - GetObjectZ(ptr + 1));
        MoveObject(ptr + 3, GetObjectX(ptr + 2), GetObjectY(ptr + 2));
        LookWithAngle(ptr, count + 1);
        FrameTimerWithArg(1, ptr, FlyPoisonBall);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        if (CurrentHealth(ptr + 2))
            Delete(ptr + 2);
        Delete(ptr + 3);
    }
}

void EnemyPoisoned()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        if (!HasEnchant(OTHER, "ENCHANT_AFRAID") && IsObjectOn(OTHER))
        {
            Damage(OTHER, owner, 30, 5);
            Enchant(OTHER, "ENCHANT_AFRAID", 10.0);
            FrameTimerWithArg(1, GetCaller(), PlayerPoisoned);
        }
    }
    else if (!GetCaller())
        Delete(SELF);
}

void PlayerPoisoned(int unit)
{
    if (CurrentHealth(unit) && HasEnchant(unit, "ENCHANT_AFRAID"))
    {
        MoveWaypoint(253, GetObjectX(unit), GetObjectY(unit));
        DeleteObjectTimer(CreateObject("GreenSmoke", 253), 9);
        Damage(unit, MasterUnit(), 15, 5);
        FrameTimerWithArg(20, unit, PlayerPoisoned);
    }
}

void JandorWeapon()
{
    if (CurrentHealth(SELF))
    {
        if (CurrentHealth(OTHER))
        {
            Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Damage(OTHER, SELF, 35, 16);
        }
        if (!HasEnchant(SELF, "ENCHANT_DETECTING"))
        {
            Enchant(SELF, "ENCHANT_DETECTING", 0.0);
            FrameTimerWithArg(22, GetTrigger(), ResetUnitSight);
        }
    }
}

void LoopDetectPlayers(int unit)
{
    int target, idx;

    if (CurrentHealth(unit))
    {
        if (!HasEnchant(unit, "ENCHANT_VILLAIN"))
        {
            target = GetNearbyPlayer(unit);
            if (target >= 0)
            {
                CreatureFollow(unit, target);
                AggressionLevel(unit, 1.0);
            }
            Enchant(unit, "ENCHANT_VILLAIN", 1.8);
        }
        idx = GetDirection(unit + 2);
        if (GetDirection(unit) / 32 != idx)
        {
            MoveObject(InitStatues(idx), GetWaypointX(idx + 259), GetWaypointY(idx + 259));
            LookWithAngle(unit + 2, GetDirection(unit) / 32);
        }
        MoveObject(InitStatues(GetDirection(unit + 2)), GetObjectX(unit), GetObjectY(unit));
        FrameTimerWithArg(1, unit, LoopDetectPlayers);
    }
}

void RemoveUnit(int unit)
{
    Delete(unit);
}

// void BringYourGuardian(int plr)
// {
//     int user=GetPlayer(plr);
//     if (!HasEnchant(user, "ENCHANT_PROTECT_FROM_MAGIC"))
//     {
//         Enchant(user, "ENCHANT_PROTECT_FROM_MAGIC", 3.0);
//         if (CurrentHealth(Jandors[plr]))
//         {
//             MoveObject(Jandors[plr], GetObjectX(user) + UnitAngleCos(user, 11.0), GetObjectY(user) + UnitAngleSin(user, 11.0));
//             Effect("TELEPORT", GetObjectX(Jandors[plr]), GetObjectY(Jandors[plr]), 0.0, 0.0);
//             Chat(Jandors[plr], "소환수가 로드됨(쿨다운 3초)");
//         }
//     }
// }

void WindBooster(int pUnit)
{
    PushObjectTo(pUnit, UnitAngleCos(pUnit, 85.0), UnitAngleSin(pUnit, 85.0));
    Enchant(pUnit, EnchantList(ENCHANT_RUN), 0.2);
    Effect("RICOCHET", GetObjectX(pUnit), GetObjectY(pUnit), 0.0, 0.0);
}

void EmptyInventory(int unit)
{
    while (GetLastItem(unit))
        Delete(GetLastItem(unit));
}

string FxStatue(int num)
{
    string table[] = {
        "MovableStatue2B", "MovableStatue2A", "MovableStatue2H", "MovableStatue2G",
        "MovableStatue2F", "MovableStatue2E", "MovableStatue2D", "MovableStatue2C" };

    return table[num];
}

int InitStatues(int num)
{
    int k, arr[8];

    if (!arr[0])
    {
        for (k = 0 ; k < 8 ; k ++)
        {
            arr[k] = CreateObject(FxStatue(k), 259 + k);
            SetUnitFlags(arr[k], GetUnitFlags(arr[k]) ^ 0x40);
            Enchant(arr[k], "ENCHANT_FREEZE", 0.0);
        }
        return 0;
    }
    return arr[num];
}

float GetRatioUnitWpX(int unit, int wp, float size)
{
    return (GetObjectX(unit) - GetWaypointX(wp)) * size / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp));
}

float GetRatioUnitWpY(int unit, int wp, float size)
{
    return (GetObjectY(unit) - GetWaypointY(wp)) * size / Distance(GetObjectX(unit), GetObjectY(unit), GetWaypointX(wp), GetWaypointY(wp));
}

void InitRainyDay()
{
    CosTable(-1);
    SinTable(-1);
    FrameTimerWithArg(1, 278, SpawnRainy);
    FrameTimerWithArg(2, 279, SpawnRainy);
    FrameTimerWithArg(3, 280, SpawnRainy);
    FrameTimerWithArg(4, 281, SpawnRainy);
    FrameTimerWithArg(5, 282, SpawnRainy);
    FrameTimerWithArg(6, 283, SpawnRainy);
    FrameTimer(30, EndMission);
    ObjectOff(SELF);
}

void EndMission()
{
    Effect("WHITE_FLASH", GetWaypointX(258), GetWaypointY(258), 0.0, 0.0);
    UniPrintToAll("미션완료!, 오르비스 탑의 꼭대기 층에 도달하셨습니다");
}

void SpawnRainy(int wp)
{
    int ptr = CreateObject("InvisibleLightBlueLow", wp) + 1, k;

    for (k = 0 ; k < 30 ; k += 1)
    {
        ObjectOff(CreateObject("CorpseRightUpperArmE", wp));
    }
    FrameTimerWithArg(1, ptr, RainDrop);
}

void RainDrop(int ptr)
{
    int idx, rnd;
    float x, y;

    if (IsObjectOn(ptr - 1))
    {
        idx = GetDirection(ptr - 1);
        x = GetObjectX(ptr + idx);
        y = GetObjectY(ptr + idx);
        rnd = Random(0, 359);
        MoveObject(ptr + idx, GetObjectX(ptr - 1) + MathSine(rnd + 90, RandomFloat(20.0, 600.0)), GetObjectY(ptr - 1) + MathSine(rnd, RandomFloat(20.0, 600.0)));
        if (!IsVisibleTo(ptr - 1, ptr + idx))
            MoveObject(ptr + idx, x, y);
        Raise(ptr + idx, 280.0);
        LookWithAngle(ptr - 1, (idx + 1) % 30);
        FrameTimerWithArg(1, ptr, RainDrop);
    }
}

float CosTable(int num)
{
    float arr[360];
    int k;
    if (num < 0)
    {
        for (k = 359 ; k >= 0 ; k -= 1)
            arr[k] = MathSine(k + 90, 1.0);
        return ToFloat(0);
    }
    return arr[num];
}

float SinTable(int num)
{
    float arr[360];
    int k;
    if (num < 0)
    {
        for (k = 359 ; k >= 0 ; k -= 1)
            arr[k] = MathSine(k, 1.0);
        return ToFloat(0);
    }
    return arr[num];
}

string ShopItemDescript(int num)
{
    string table[] = {
        "재생의 팔찌[27000G]: 소유하면 체력회복 속도가 증가됩니다(중복적용 가능)",
        "핵폭발 검[21000G]: 적을 치거나 공격을 막을 때 전방에 있는 적에게 피해를 입힙니다",
        "피의 목걸이[22000G]: 이 목걸이를 지니면 항상 흡혈상태가 유지됩니다, 벗게되면 즉시 무효됩니다",
        "잔도 소환[18000G]: 잔도를 소환합니다, 당신을 따라다니며 서로 다른 공간에 있는경우 L 키로 불러올 수 있습니다",
        "혼돈의 해머[32000G]: 해머를 내리칠 때 마다 전방에 에너지파가 시전됩니다, 닿은 적은 일시적으로 혼란상태가 됩니다",
        "대쉬 배우기[18000G]: 조심스럽게 걷기를 시전하면 대쉬가 발동됩니다, 대쉬는 짧은 거리를 순간적으로 이동합니다",
        "창고 개방열쇠[30000G]: 강력한 갑옷이나 무기가 있는 비밀창고를 개방하기 위한 열쇠를 구입합니다, 창고는 총 3곳입니다",
        "망각의 지팡이[36000G]: 망각의 지팡이를 얻습니다, 이때 자신의 체력이 1000 으로 증가됩니다"};

    return table[num];
}

void PutDesc(int idx)
{
    int unit = CreateObject("FoilCandleLit", 303 + idx);
    CreateObject("InvisibleLightBlueHigh", 303 + idx);
    LookWithAngle(unit + 1, idx);
    RegistItemPickupCallback(unit, ReadPage);
}

void DummyFunction()
{
    //
}

void InitShopItems()
{
    int ptr = CreateObject("Maiden", 294);
    Frozen(CreateObject("BraceletofHealth", 294), 1);

    Frozen(ptr, 1);
    LookWithAngle(ptr, 0);
    SetDialog(ptr, "NORMAL", BuyHealthItem, DummyFunction);

    Frozen(CreateObject("Maiden", 295), 1); //+2
    Enchant(CreateObject("GreatSword", 295), "ENCHANT_FREEZE", 0.0);
    LookWithAngle(ptr + 2, 0);
    SetDialog(ptr + 2, "NORMAL", BuyMagicSword, DummyFunction);

    Frozen(CreateObject("Maiden", 296), 1); //+4
    Frozen(CreateObject("AmuletofManipulation", 296), 1);
    LookWithAngle(ptr + 4, 0);
    SetDialog(ptr + 4, "NORMAL", BuyAmuletOfBlood, DummyFunction);

    Frozen(CreateObject("Maiden", 297), 1); //+6
    Frozen(CreateObject("FieldGuide", 297), 1);
    LookWithAngle(ptr + 6, 0);
    SetDialog(ptr + 6, "NORMAL", BuySummonJando, DummyFunction);

    Frozen(CreateObject("Maiden", 298), 1); //+8
    Frozen(CreateObject("WarHammer", 298), 1);
    LookWithAngle(ptr + 8, 0);
    SetDialog(ptr + 8, "NORMAL", BuySummonJando, DummyFunction);

    Frozen(CreateObject("Maiden", 299), 1); //+10
    Frozen(CreateObject("AbilityBook", 299), 1);
    LookWithAngle(ptr + 10, 0);
    SetDialog(ptr + 10, "NORMAL", AwardDash, DummyFunction);

    Frozen(CreateObject("Maiden", 300), 1); //+12
    Frozen(CreateObject("SilverKey", 300), 1);
    LookWithAngle(ptr + 12, 0);
    SetDialog(ptr + 12, "NORMAL", BuySilverkey, DummyFunction);

    Frozen(CreateObject("Maiden", 301), 1); //+14
    Frozen(CreateObject("OblivionOrb", 301), 1);
    SetDialog(ptr + 14, "NORMAL", BuyOblivion, DummyFunction);

    FrameTimer(1, PutShopItemDescrtions);
    FrameTimerWithArg(120, 0, Garage1Puts);
}

void PutShopItemDescrtions()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 292), k;

    // Raise(ptr, ReadPage);
    // DescFunc = ToInt(GetObjectZ(ptr));
    for (k = 0 ; k < 8 ; k ++)
        PutDesc(k);
}

void ReadPage()
{
    int idx = GetDirection(GetTrigger() + 1);

    UniPrint(OTHER,ShopItemDescript(idx));
    Delete(SELF);
    Delete(GetTrigger() + 1);
    FrameTimerWithArg(30, idx, PutDesc);
}

void BuyHealthItem()
{
    int count = GetDirection(SELF), ptr;

    if (count < 5)
    {
        if (GetGold(OTHER) >= 27000)
        {
            MoveWaypoint(254, GetObjectX(SELF), GetObjectY(SELF));
            ptr = CreateObject("BraceletofHealth", 254);
            CreateObject("InvisibleLightBlueHigh", 254);
            Raise(ptr + 1, ToFloat(GetCaller()));
            FrameTimerWithArg(1, ptr, DelayFeed);
            FrameTimerWithArg(1, ptr, IncreaseRiseHp);
            LookWithAngle(SELF, count + 1);
            ChangeGold(OTHER, -27000);
        }
    }
    else
    {
        UniPrint(OTHER, "현재 본 제품은 품절 상태입니다");
        if (IsObjectOn(GetTrigger() + 1))
            Delete(GetTrigger() + 1);
    }
}

void BuySilverkey()
{
    int count = GetDirection(SELF), ptr;

    if (count < 3)
    {
        if (GetGold(OTHER) >= 30000)
        {
            MoveWaypoint(254, GetObjectX(SELF), GetObjectY(SELF));
            ptr = CreateObject("SilverKey", 254);
            CreateObject("InvisibleLightBlueHigh", 254);
            Raise(ptr + 1, ToFloat(GetCaller()));
            Enchant(ptr, "ENCHANT_FREEZE", 0.0);
            FrameTimerWithArg(1, ptr, DelayFeed);
            ChangeGold(OTHER, -30000);
            LookWithAngle(SELF, count + 1);
        }
    }
    else
        Print("매진");
}

void BuyMagicSword()
{
    int count = GetDirection(SELF), ptr;

    if (count < 5)
    {
        if (GetGold(OTHER) >= 21000)
        {
            MoveWaypoint(254, GetObjectX(SELF), GetObjectY(SELF));
            ptr = CreateObject("GreatSword", 254);
            CreateObject("InvisibleLightBlueHigh", 254);
            Raise(ptr + 1, ToFloat(GetCaller()));
            Enchant(ptr, "ENCHANT_FREEZE", 0.0);
            FrameTimerWithArg(1, ptr, DelayFeed);
            FrameTimerWithArg(1, ptr, MagicSwordLoop);
            ChangeGold(OTHER, -21000);
            LookWithAngle(SELF, count + 1);
        }
    }
    else
    {
        Chat(SELF, "이미 품절된 상품입니다");
        if (IsObjectOn(GetTrigger() + 1))
            Delete(GetTrigger() + 1);
    }
}

void BuyAmuletOfBlood()
{
    int count = GetDirection(SELF), ptr;

    if (count < 5)
    {
        if (GetGold(OTHER) >= 22000)
        {
            MoveWaypoint(254, GetObjectX(SELF), GetObjectY(SELF));
            ptr = CreateObject("AmuletofManipulation", 254);
            CreateObject("InvisibleLightBlueHigh", 254);
            Raise(ptr + 1, ToFloat(GetCaller()));
            Enchant(ptr, "ENCHANT_FREEZE", 0.0);
            FrameTimerWithArg(1, ptr, DelayFeed);
            FrameTimerWithArg(1, ptr, BloodAmuletLoop);
            ChangeGold(OTHER, -22000);
            LookWithAngle(SELF, count + 1);
        }
    }
}

void BuySummonJando()
{
    // int ptr, plr = CheckPlayer();

    // if (plr >= 0)
    // {
    //     if (GetGold(OTHER) >= 18000 && !CurrentHealth(Jandors[plr]))
    //     {
    //         MoveWaypoint(254, GetObjectX(OTHER), GetObjectY(OTHER));
    //         ptr = CreateObject("AirshipCaptain", 254);
    //         CreateObject("InvisibleLightBlueHigh", 254);
    //         LookWithAngle(ptr + 1, plr);
    //         SetUnitMaxHealth(ptr, 700);
    //         RetreatLevel(ptr, 0.0);
    //         SetOwner(OTHER, ptr);
    //         SetCallback(ptr, 3, JandorWeapon);
    //         SetCallback(ptr, 5, SummonedUnitDeaths);
    //         FrameTimerWithArg(1, ptr, DelayFollow);
    //         Jandors[plr] = ptr;
    //         ChangeGold(OTHER, -18000);
    //     }
    //     else
    //         Chat(SELF, "금액이 부족하거나 이미 필드에 소환된 잔도가 존재하기 때문에 지금은 구입할 수 없습니다");
    // }
}

void BuyMagicHammer()
{
    int ptr, count = GetDirection(SELF);

    if (count < 5)
    {
        if (GetGold(OTHER) >= 32000)
        {
            MoveWaypoint(254, GetObjectX(SELF), GetObjectY(SELF));
            ptr = CreateObject("WarHammer", 254);
            CreateObject("InvisibleLightBlueHigh", 254);
            Raise(ptr + 1, ToFloat(GetCaller()));
            Enchant(ptr, "ENCHANT_FREEZE", 0.0);
            FrameTimerWithArg(1, ptr, DelayFeed);
            FrameTimerWithArg(1, ptr, MagicHammerLoop);
            ChangeGold(OTHER, -32000);
            LookWithAngle(SELF, count + 1);
        }
    }
    else
        Print("이 제품은 품절입니다");
}

void BuyOblivion()
{
    int ptr;

    if (GetGold(OTHER) >= 36000)
    {
        MoveWaypoint(254, GetObjectX(SELF), GetObjectY(SELF));
        ptr = CreateObject("OblivionOrb", 254);
        CreateObject("InvisibleLightBlueHigh", 254);
        Raise(ptr + 1, ToFloat(GetCaller()));
        Enchant(ptr, "ENCHANT_FREEZE", 0.0);
        FrameTimerWithArg(1, ptr + 1, RiseHealth);
        FrameTimerWithArg(1, ptr, DelayFeed);
        ChangeGold(OTHER, -36000);
    }
}

void RiseHealth(int ptr)
{
    int unit = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(unit))
    {
        EnchantOff(unit, "ENCHANT_INVULNERABLE");
        EnchantOff(unit, "ENCHANT_SHIELD");
        SetUnitHealth(unit, 1000);
    }
}

void AwardDash()
{
    // int plr;

    // if (GetGold(OTHER) >= 18000)
    // {
    //     plr = CheckPlayer();
    //     if (plr >= 0)
    //     {
    //         if (!(Plr_flag[plr] & 1))
    //         {
    //             UniPrint(OTHER, "대쉬 마술을 구입했습니다, 조심스럽게 걷기를 시전하면 발동됩니다");
    //             Plr_flag[plr] = Plr_flag[plr] ^ 1;
    //             MoveWaypoint(253, GetObjectX(OTHER), GetObjectY(OTHER));
    //             Effect("YELLOW_SPARKS", GetWaypointX(253), GetWaypointY(253), 0.0, 0.0);
    //             AudioEvent("SpellPopOffBook", 253);
    //         }
    //         else
    //             UniPrint(OTHER, "이미 대쉬 마술을 배웠습니다");
    //     }
    // }
}

void DelayFeed(int ptr)
{
    int target = ToInt(GetObjectZ(ptr + 1));
    if (CurrentHealth(target))
        Pickup(target, ptr);
    else
        Delete(ptr);
    Delete(ptr + 1);
}

void DelayFollow(int unit)
{
    int owner = GetOwner(unit);

    if (CurrentHealth(unit))
    {
        CreatureFollow(unit, owner);
        AggressionLevel(unit, 1.0);
        GiveCreatureToPlayer(owner, unit);
        UniChatMessage(unit, "소환된 유닛, L 키를 누르면 캐릭터 위치로 유닛을 불러올 수 있습니다", 150);
    }
}

void SummonedUnitDeaths()
{
    // int plr = GetDirection(GetTrigger() + 1);
    // Delete(SELF);
    // Delete(GetTrigger() + 1);
    // Jandors[plr] = 0;
}

void IncreaseRiseHp(int unit)
{
    int owner;
    if (IsObjectOn(unit))
    {
        owner = GetOwner(unit);
        if (CurrentHealth(owner))
        {
            RestoreHealth(owner, 2);
        }
        FrameTimerWithArg(9, unit, IncreaseRiseHp);
    }
}

void MagicSwordLoop(int unit)
{
    int owner;

    if (IsObjectOn(unit))
    {
        if (MaxHealth(unit) - CurrentHealth(unit))
        {
            owner = GetOwner(unit);
            if (CurrentHealth(owner) && !HasEnchant(owner, "ENCHANT_ETHEREAL"))
            {
                SplashFrontArea(owner);
                Enchant(owner, "ENCHANT_ETHEREAL", 0.7);
            }
            RestoreHealth(unit, MaxHealth(unit));
        }
        FrameTimerWithArg(1, unit, MagicSwordLoop);
    }
}

void MagicHammerLoop(int unit)
{
    int owner;

    if (IsObjectOn(unit))
    {
        if (MaxHealth(unit) - CurrentHealth(unit))
        {
            owner = GetOwner(unit);
            if (CurrentHealth(owner) && !HasEnchant(owner, "ENCHANT_ETHEREAL"))
            {
                FrameTimerWithArg(1, owner, CastEnergyPar);
                Enchant(owner, "ENCHANT_ETHEREAL", 3.0);
            }
            RestoreHealth(unit, MaxHealth(unit));
        }
        FrameTimerWithArg(1, unit, MagicHammerLoop);
    }
}

void SplashFrontArea(int unit)
{
    int ptr;

    MoveWaypoint(253, GetObjectX(unit) + UnitAngleCos(unit, 34.0), GetObjectY(unit) + UnitAngleSin(unit, 34.0));
    ptr = CreateObject("Maiden", 253);
    DeleteObjectTimer(CreateObject("InvisibleLightBlueHigh", 253), 6);
    DeleteObjectTimer(CreateObject("Explosion", 253), 9);
    Raise(ptr + 1, ToFloat(unit));
    Frozen(ptr, 1);
    DeleteObjectTimer(ptr, 1);
    SetCallback(ptr, 9, SplashDamage);
}

void SplashDamage()
{
    int owner = ToInt(GetObjectZ(GetTrigger() + 1));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Effect("DAMAGE_POOF", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Damage(OTHER, owner, 140, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.6);
    }
}

void CastEnergyPar(int unit)
{
    int k, ptr;
    float pos_x = UnitAngleCos(unit, 30.0), pos_y = UnitAngleSin(unit, 30.0);
    
    MoveWaypoint(253, GetObjectX(unit) + pos_x, GetObjectY(unit) + pos_y);
    ptr = CreateObject("InvisibleLightBlueHigh", 253) + 1;
    Raise(ptr, ToFloat(unit));
    for (k = 0 ; k < 15 && CheckLimitLine(253) ; k ++)
    {
        AudioEvent("FireballExplode", 253);
        Effect("SPARK_EXPLOSION", GetWaypointX(253), GetWaypointY(253), 0.0, 0.0);
        Frozen(CreateObject("Maiden", 253), 1);
        DeleteObjectTimer(ptr + k, 1);
        SetOwner(ptr - 1, ptr + k);
        SetCallback(ptr + k, 9, EnergyParDamage);
        MoveWaypoint(253, GetWaypointX(253) + pos_x, GetWaypointY(253) + pos_y);
    }
    Delete(ptr - 1);
}

void EnergyParDamage()
{
    int owner = ToInt(GetObjectZ(GetOwner(SELF)));

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 50, 1);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.7);
    }
}

void BloodAmuletLoop(int unit)
{
    int owner, temp;

	if (IsObjectOn(unit))
	{
		temp = GetOwner(unit);
		if (temp != owner)
		{
			if (CurrentHealth(owner))
				EnchantOff(owner, "ENCHANT_VAMPIRISM");
			owner = temp;
			if (CurrentHealth(owner))
				Enchant(owner, "ENCHANT_VAMPIRISM", 0.0);
		}
		FrameTimerWithArg(1, unit, BloodAmuletLoop);
	}
}

// void PlayerSendToStartZone(int plr)
// {
//     int ptr;

//     if (!HasEnchant(player[plr], "ENCHANT_LIGHT"))
//     {
//         Enchant(player[plr], "ENCHANT_LIGHT", 5.0);
//         Chat(player[plr], "시작위치로 귀환중 입니다, 취소하려면 움직이세요");
//         MoveWaypoint(253, GetObjectX(player[plr]), GetObjectY(player[plr]));
//         AudioEvent("HecubahDieFrame194", 253);
//         ptr = CreateObject("InvisibleLightBlueHigh", 253);
//         CreateObject("VortexSource", 253);
//         LookWithAngle(ptr, plr);
//         FrameTimerWithArg(60, ptr, TeleportToHome);
//     }
// }

// void TeleportToHome(int ptr)
// {
//     int plr = GetDirection(ptr);

//     if (CurrentHealth(player[plr]))
//     {
//         if (Distance(GetObjectX(player[plr]), GetObjectY(player[plr]), GetObjectX(ptr), GetObjectY(ptr)) < 12.0)
//         {
//             Effect("COUNTERSPELL_EXPLOSION", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
//             MoveObject(player[plr], GetWaypointX(1), GetWaypointY(1));
//             AudioEvent("HecubahDieFrame98", 1);
//             Effect("COUNTERSPELL_EXPLOSION", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
//             Effect("SMOKE_BLAST", GetObjectX(player[plr]), GetObjectY(player[plr]), 0.0, 0.0);
//         }
//     }
//     Delete(ptr);
//     Delete(ptr + 1);
// }

void HomePortal(int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp), ptr;
    Frozen(CreateObject("MagicMissile", wp), 1);
    ptr = GetMemory(0x750710);
    SetMemory(ptr + 0xc, GetMemory(ptr + 0xc) ^ 0x1);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    ObjectOff(unit);
    SetDialog(unit, "NORMAL", ClickPortal, DummyFunction);
    SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x40);
}

void ClickPortal()
{
    if (!HasEnchant(OTHER, "ENCHANT_LIGHT"))
    {
        Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Enchant(OTHER, "ENCHANT_PROTECT_FROM_POISON", 1.0);
    }
}

int CheckLimitLine(int wp)
{
    float x = GetWaypointX(wp), y = GetWaypointY(wp);
    if (x > 100.0 && y > 100.0 && x < 5710.0 && y < 5710.0)
        return 1;
    return 0;
}

string ArmorTable(int num)
{
    string table[] = {"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", "PlateLeggings", "SteelShield"};

    return table[num];
}

string WeaponTable(int num)
{
    string table[] = {"GreatSword", "WarHammer", "RoundChakram", "OgreAxe", "Longsword", "Sword", "MorningStar", "BattleAxe"};

    return table[num];
}

void Garage1Puts(int idx)
{
    int k;

    if (idx < 6)
    {
        TeleportLocation(315, LocationX(313), LocationY(313));
        for (k = 0 ; k < 4 ; k ++)
        {
            Frozen(GarageArmor(ArmorTable(idx), 315, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_Regeneration4), TRUE);
            TeleportLocationVector(315, - 23.0,  23.0);
        }
        TeleportLocationVector(313,  23.0,  23.0);
        FrameTimerWithArg(1, idx + 1, Garage1Puts);
    }
    else
        Garage2Puts(0);
}

void Garage2Puts(int idx)
{
    int k;

    if (idx < 6)
    {
        TeleportLocation(315, LocationX(312), LocationY(312));
        for (k = 0 ; k < 4 ; k ++)
        {
            Frozen(GarageArmor(ArmorTable(idx), 315, ITEM_PROPERTY_LightningProtect4, ITEM_PROPERTY_Regeneration4), TRUE);
            TeleportLocationVector(315, - 23.0,  23.0);
        }
        TeleportLocationVector(312, 23.0, 23.0);
        FrameTimerWithArg(1, idx + 1, Garage2Puts);
    }
    else
        Garage3Puts(0);
}

void Garage3Puts(int idx)
{
    int k;

    if (idx < 4)
    {
        TeleportLocation(315, LocationX(314), LocationY(314));
        for (k = 0 ; k < 4 ; k ++)
        {
            Frozen(GarageWeapon(WeaponTable(idx), 315, ITEM_PROPERTY_fire4, ITEM_PROPERTY_venom4), TRUE);
            TeleportLocationVector(315, - 23.0,  23.0);
        }
        TeleportLocationVector(314, 23.0, 23.0);
        FrameTimerWithArg(1, idx + 1, Garage3Puts);
    }
}

int GarageWeapon(string weapon, int wp, int fst, int scd)
{
    int item=CreateObjectAt(weapon, LocationX(wp),LocationY(wp));

    SetWeaponPropertiesDirect(item, ITEM_PROPERTY_weaponPower6,ITEM_PROPERTY_Matrial7, fst,scd);
    return item;
}

int GarageArmor(string armor, int wp, int fst, int scd)
{
    int item=CreateObjectAt(armor,LocationX(wp),LocationY(wp));

    SetArmorPropertiesDirect(item,ITEM_PROPERTY_armorQuality6,ITEM_PROPERTY_Matrial7,fst,scd);
    return item;
}

void HarpoonEvent(int owner, int cur)
{
    int ptr;
    float x_vect, y_vect;

    if (CurrentHealth(owner))
    {
        if (HasEnchant(owner, "ENCHANT_CROWN"))
        {
            Delete(cur);
            x_vect = UnitAngleCos(owner, 13.0);
            y_vect = UnitAngleSin(owner, 13.0);
            MoveWaypoint(329, GetObjectX(owner) + x_vect, GetObjectY(owner) + y_vect);
            ptr = CreateObject("InvisibleLightBlueHigh", 329);
            Raise(CreateObject("InvisibleLightBlueHigh", 329), y_vect);
            Frozen(HecubahDeadMotion(329), 1);
            //LookWithAngle(ptr + 2, GetDirection(owner));
            SetOwner(owner, ptr);
            Raise(ptr, x_vect);
            SetCallback(ptr + 2, 9, FlierTouchEvent);
            FrameTimerWithArg(1, ptr, FlyingFlier);
        }
    }
}

int HecubahDeadMotion(int wp)
{
    int unit = CreateObject("Hecubah", wp);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetMemory(GetMemory(GetMemory(0x750710) + 0x2ec) + 0x1e0, 38817);
    return unit;
}

void ShurikenEvent(int owner, int cur)
{
    int mis;

    if (CurrentHealth(owner))
    {
        MoveWaypoint(327, GetObjectX(cur), GetObjectY(cur));
        mis = CreateObject("WeakFireball", 327);
        SetOwner(owner, mis);
        PushObjectTo(mis, UnitRatioX(cur, owner, 25.0), UnitRatioY(cur, owner, 25.0));
    }
}

void ChakramEvent(int owner, int cur)
{
    Enchant(cur, "ENCHANT_REFLECTIVE_SHIELD", 0.0);
    Enchant(cur, "ENCHANT_HASTED", 0.0);
}

void SplashDamageOnRange(int owner, int dam, float range, int wp)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1, k;

    SetOwner(owner, ptr - 1);
    MoveObject(ptr - 1, range, GetObjectY(ptr - 1));
    Raise(ptr - 1, ToFloat(dam));
    for (k = 0 ; k < 8 ; k ++)
    {
        DeleteObjectTimer(CreateObject("WeirdlingBeast", wp), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 32);
        SetOwner(ptr - 1, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplash);
    }
    DeleteObjectTimer(ptr - 1, 2);
}

void UnitVisibleSplash()
{
    int parent;

    if (!HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    {
        parent = GetOwner(SELF);
        if (Distance(GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER)) <= GetObjectX(parent))
        {
            Enchant(OTHER, "ENCHANT_VILLAIN", 0.1);
            Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
        }
    }
}

void FlyingFlier(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(owner) && count < 50)
    {
        if (IsVisibleTo(ptr + 2, ptr))
        {
            MoveObject(ptr + 2, GetObjectX(ptr + 2) + GetObjectZ(ptr), GetObjectY(ptr + 2) + GetObjectZ(ptr + 1));
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            LookWithAngle(ptr, 200);
        }
        FrameTimerWithArg(1, ptr, FlyingFlier);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
    }
}

void FlierTouchEvent()
{
    int owner = GetOwner(GetTrigger() - 2);

    if (IsAttackedBy(OTHER, owner) && CurrentHealth(OTHER))
    {
        MoveWaypoint(329, GetObjectX(SELF), GetObjectY(SELF));
        SplashDamageOnRange(owner, 100, 150.0, 329);
        DeleteObjectTimer(CreateObject("Explosion", 329), 9);
        AudioEvent("PowderBarrelExplode", 329);
        Delete(SELF);
    }
}

void PutHorrendous()
{
    int ptr = CreateObject("Horrendous", 327);

    Damage(ptr, 0, MaxHealth(ptr) + 1, -1);
    SetDialog(ptr, "NORMAL", HorrendousTalk, Nothing);
    FrameTimerWithArg(23, ptr, DelayUnitDisable);
}

void Nothing()
{
    //
}

void HorrendousTalk()
{
    // int plr;
    // if (HasEnchant(OTHER, "ENCHANT_VILLAIN"))
    // {
    //     EnchantOff(OTHER, "ENCHANT_VILLAIN");
    //     if (HasEnchant(OTHER, "ENCHANT_CROWN"))
    //         Print("구입실패_ 이 능력을 이미 배우셨습니다");
    //     else if (GetGold(OTHER) >= 20000)
    //     {
    //         plr = CheckPlayer();
    //         if (plr + 1)
    //         {
    //             PlrSkill[plr] = 1;
    //             Enchant(player[plr], "ENCHANT_CROWN", 0.0);
    //             ChangeGold(OTHER, -20000);
    //             MoveWaypoint(327, GetObjectX(OTHER), GetObjectY(OTHER));
    //             GreenSparkFx(327);
    //             AudioEvent("AwardSpell", 327);
    //             Print("구입에 성공했습니다, 이제 작살을 시전하면 다른 능력이 발사될 것입니다");
    //         }
    //         else
    //             Print("허용되지 않은 플레이어 입니다");
    //     }
    //     else
    //         Print("구입실패_ 골드가 부족합니다, [2만 골드 필요]");
    // }
    // else
    // {
    //     Enchant(OTHER, "ENCHANT_VILLAIN", 0.6);
    //     Print("작살 능력을 강화합니다, 계속하려면 더블클릭 하십시오 [2만 골드 필요]");
    // }
}

void DelayUnitDisable(int unit)
{
    ObjectOff(unit);
    Frozen(unit, 1);
}

void GreenSparkFx(int wp)
{
    int ptr = CreateObject("MonsterGenerator", wp);

    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

void GolemSkill(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(owner) && count < 50)
    {
        if (Distance(GetObjectX(ptr), GetObjectY(ptr), GetObjectX(ptr + 1), GetObjectY(ptr + 1)) > 30.0)
        {
            MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr), GetObjectY(ptr) - GetObjectZ(ptr + 1));
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            MoveWaypoint(329, GetObjectX(ptr), GetObjectY(ptr));
            CastSpellObjectObject("SPELL_TURN_UNDEAD", ptr, ptr);
            SplashDamageOnRange(owner, 130, 100.0, 329);
            LookWithAngle(ptr, 200);
        }
        FrameTimerWithArg(1, ptr, GolemSkill);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void CureAllYourItems()
{
    int cur, res = 0;

    if (CurrentHealth(OTHER))
    {
        Enchant(OTHER, "ENCHANT_FREEZE", 0.5);
        cur = GetLastItem(OTHER);
        while (IsObjectOn(cur))
        {
            if (MaxHealth(cur) - CurrentHealth(cur))
            {
                RestoreHealth(cur, MaxHealth(cur));
                res ++;
            }
            cur = GetPreviousItem(cur);
        }
        char msg[128];
        if (res)
        {
            NoxSprintfString(msg,"내구도 회복처리 완료 %d 개", &res,1);
            UniPrint(OTHER,ReadStringAddressEx(msg));
        }
    }
}

void OnInitializeMap(){
    MusicEvent();
    int lastUnit=MasterUnit();
    MakeCoopTeam();
    doInitializeReward(lastUnit);
    LockDoor(Object("Area2Gate"));

    PlaceWaspNest(120);
    PlaceWaspNest(121);
    PlaceWaspNest(122);
    PlaceWaspNest(123);
    FirstBossBlocks();

    //delay_run
    FrameTimer(1, AfterTriggersExec);
    FrameTimer(60, PutGenerators);
    FrameTimer(10, PutHorrendous);
}

void OnShutdownMap(){
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
}

void RemoveGardenWalls()
{
    int k;

    ObjectOff(SELF);
    for (k = 13 ; k >= 0 ; k --)
        WallOpen(Wall(60 - k, 36 + k));
}

void EntryPointPart15()
{
    int k;

    ObjectOff(SELF);
    if (!k)
    {
        for (k = 6 ; k >= 0 ; k --)
            WallOpen(Wall(18 + k, 68 + k));
    }
}

void EntryPointPart25()
{
    int k;

    ObjectOff(SELF);
    if (!k)
    {
        for (k = 0 ; k < 6 ; k ++)
            WallOpen(Wall(122 - k, 46 + k));
    }
}

void InitializeIndexLoop(int h){
    HashPushback(h, OBJ_ROUND_CHAKRAM_IN_MOTION, ChakramEvent);
    HashPushback(h, OBJ_FAN_CHAKRAM_IN_MOTION, ShurikenEvent);
    HashPushback(h, OBJ_HARPOON_BOLT, HarpoonEvent);
}
