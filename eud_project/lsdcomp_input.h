

#include "observerPatch.h"
#include "libs/keyscan.h"
#include "libs/queueTimer.h"
#include "libs/network.h"

// #define SEND_UP_SHIFT 
#define SEND_KEY_S_SHIFT 1
#define SEND_KEY_D_SHIFT 2
#define SEND_KEY_F_SHIFT 3

void ServerInputLoop(int *pState)
{
    int keyA = KeyboardIOCheckKeyEx(VIRTUAL_KEY_A);
    int keyS = KeyboardIOCheckKeyEx(VIRTUAL_KEY_S)<<SEND_KEY_S_SHIFT;
    int keyD = KeyboardIOCheckKeyEx(VIRTUAL_KEY_D)<<SEND_KEY_D_SHIFT;
    int keyF = KeyboardIOCheckKeyEx(VIRTUAL_KEY_F)<<SEND_KEY_F_SHIFT;
    int prevState, state = keyA|keyS|keyD|keyF;

    if (prevState!=state)
    {
        if (KeyboardIO_IsInputMode())
            state=0;
            
        prevState=state;
        pState[31]=state;
    }
    PushTimerQueue(3, pState, ServerInputLoop);
}

void ClientKeyHandlerRemix(int clientId)
{
    int keyA = KeyboardIOCheckKeyEx(VIRTUAL_KEY_A);
    int keyS = KeyboardIOCheckKeyEx(VIRTUAL_KEY_S)<<SEND_KEY_S_SHIFT;
    int keyD = KeyboardIOCheckKeyEx(VIRTUAL_KEY_D)<<SEND_KEY_D_SHIFT;
    int keyF = KeyboardIOCheckKeyEx(VIRTUAL_KEY_F)<<SEND_KEY_F_SHIFT;
    int prevState, state = keyA|keyS|keyD|keyF;

    if (prevState!=state)
    {
        if (KeyboardIO_IsInputMode())
            state=0;
        prevState=state;

        char testPacket[]={0x3d, 0x00+(clientId*4),0x10,0x75,0x00, 0, 0,0,0};
        int *p = &testPacket[1];

        p[1] = prevState;
        NetClientSendRaw(31, 0, testPacket, sizeof(testPacket));
    }
}

void ClientClassTimerLoop(int clientId)
{
    ClientKeyHandlerRemix(clientId);
    FrameTimerWithArg(1, clientId, ClientClassTimerLoop);
}

void InitialServerPatchMapleOnly() //maple only
{
    int oldProtect;

    WinApiVirtualProtect(0x51cf84, 1024, 0x40, &oldProtect);
    char *p = 0x51cf84;

    p[23]=0x7a;
    SetMemory(0x51d0c8, 0x513f10);
    WinApiVirtualProtect(0x51cf84, 1024, oldProtect, NULLPTR);
    //46 8B 06 8B 4E 04 89 08 83 C6 09 E9 24 8F 00 00

    WinApiVirtualProtect(0x513f10, 256, 0x40, &oldProtect);

    char servcode[]={0x46, 0x8B, 0x06, 0x8B, 0x4E, 0x04, 0x89, 0x08, 0x83, 0xC6, 0x08, 0xE9, 0x24, 0x8F, 0x00, 0x00};

    NoxByteMemCopy(servcode, 0x513f10, sizeof(servcode));
    WinApiVirtualProtect(0x513f10, 256, oldProtect, NULLPTR);

    SetMemory(0x5c31ec, 0x513f30);
    DoObserverPatchRev();
}

