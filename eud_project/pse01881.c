
#include "pse01881_resource.h"
#include "pse01881_mon.h"
#include "pse01881_utils.h"
#include "pse01881_shop.h"

#include "libs\itemproperty.h"
#include "libs\playerupdate.h"
#include "libs\printutil.h"
#include "libs\fixtellstory.h"
#include "libs\coopteam.h"
#include "libs\wallutil.h"
#include "libs\waypoint.h"
#include "libs\cweaponproperty.h"
#include "libs\network.h"
#include "libs\wandpatch.h"
#include "libs/indexloop.h"

#define INVISIBLE_SUBUNIT "ImaginaryCaster"

#define PLAYER_FLAG_DEATH       0x80
#define PLAYER_FLAG_ALLBUFF     2
#define PLAYER_FLAG_WINDBOOST   4

#define MON_GEN_MAX         16

int player[20];

#define SAFEZONE_ID     277
#define DROP_ITEM_FUNCTION              8

#define PUZZLE_GEN_COUNT 9
int g_dropItemFunctions[DROP_ITEM_FUNCTION];
int g_userLastSavedLocation;

int g_blockLeftUp;
int g_blockLeftDown;
int g_blockRightLeft;
int g_blockRightRight;

int g_mixGenPuzzle[PUZZLE_GEN_COUNT];
int g_genPuzzleBeacon[9];
int g_puzzleGens = 0;

int g_ternnelSwitch;

int g_frontBlocks;
int g_backBlocks;

int s_necroUnitCount = 0;
int s_finalDemonsCount = 0;

#define PLAYER_START_LOCATION           12
#define PLAYER_TELEPORT_LOCATION            51  //12


void InitializeDungeonDropItemFunctions()
{
    g_dropItemFunctions[1] = RewardClassDropPotion;
    g_dropItemFunctions[2] = MoneyDrop;
    g_dropItemFunctions[3] = NormalWeaponItemDrop;
    g_dropItemFunctions[4] = NormalArmorItemDrop;
    g_dropItemFunctions[5] = WeaponItemDrop;
    g_dropItemFunctions[6] = ArmorItemDrop;
    g_dropItemFunctions[0] = RewardClassDropHotPotion;
    g_dropItemFunctions[7] = SomeGermDrop;
}

void InitializeDungeonDrawText()
{
    DrawTextOnBottom(0,0, LocationX(284), LocationY(284));
    DrawTextOnBottom(1,0, LocationX(285), LocationY(285));
}

void PlayerClassCommonWhenEntry()
{
    InitializeResources();
    AppendAllDummyStaffsToWeaponList();
}

static void NetworkUtilClientMain()
{
    PlayerClassCommonWhenEntry();
}

void MapInitialize()
{
    MusicEvent();

    InitializeDungeonDropItemFunctions();
    InitializeDungeonDrawText();
    InitFireTraps();
    FireTrapJustDeco();

    FrameTimer(3, MakeCoopTeam);
    FrameTimer(10, PlayerClassOnLoop);
    FrameTimer(30, DelayMapInit);
}

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    // SelfDamageClassMapExit();
    ResetPlayerHandlerWhenExitMap();
    ResetHostileCritter();
}

void PlayerClassMissileWandFix(int plr, int pUnit)
{
    int wand[10];

    if (wand[plr] ^ PlayerGetEquipedWeapon(pUnit))
    {
        if (HasItem(pUnit, wand[plr]))
        {
            if (GetUnitThingID(wand[plr]) == 213)
            {
                Drop(pUnit, wand[plr]);
                SetOwner(pUnit, wand[plr]);
                FrameTimerWithArg(1, wand[plr], DelayGiveToOwner);
            }
        }
        wand[plr] = PlayerGetEquipedWeapon(pUnit);
    }
}

int CheckPlayer()
{
    int i;

    for (i = 9 ; i >= 0 ; i--)
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}

void PlayerAllEnchantment(int plrUnit)
{
    if (CurrentHealth(plrUnit))
    {
        Enchant(plrUnit, EnchantList(ENCHANT_VAMPIRISM), 0.0);
        Enchant(plrUnit, EnchantList(ENCHANT_PROTECT_FROM_FIRE), 0.0);
        Enchant(plrUnit, EnchantList(ENCHANT_PROTECT_FROM_POISON), 0.0);
        Enchant(plrUnit, EnchantList(ENCHANT_PROTECT_FROM_ELECTRICITY), 0.0);
        Enchant(plrUnit, EnchantList(ENCHANT_REFLECTIVE_SHIELD), 0.0);
    }
}

int PlayerClassOnInit(int plr, int plrUnit)
{
    int pResult = plr;

    player[plr] = plrUnit;
    player[plr + 10] = 1;
    ChangeGold(plrUnit, -GetGold(plrUnit));
    SelfDamageClassEntry(plrUnit);
    DiePlayerHandlerEntry(plrUnit);
    if (ValidPlayerCheck(plrUnit))
    {
        if (plrUnit ^ GetHost())
            NetworkUtilClientEntry(plrUnit);
        else
            PlayerClassCommonWhenEntry();
    }
    return pResult;
}

void PlayerClassBufferJoin(int pUnit)
{
    MoveObject(pUnit, LocationX(PLAYER_START_LOCATION), LocationY(PLAYER_START_LOCATION));
}

void PlayerClassOnJoin(int plr, int plrUnit)
{
    if (PlayerClassCheckAllEnchant(plr))
        PlayerAllEnchantment(plrUnit);
    MoveObject(plrUnit, LocationX(288), LocationY(288));
    FrameTimerWithArg(3, plrUnit, PlayerClassBufferJoin);
    PlaySoundAround(plrUnit, 1008);
}

void PlayerClassFailToJoin()
{
    MoveObject(other, LocationX(11), LocationY(11));
}

void RegistNewPlayer()
{
    int i, plr;

    while (true)
    {
        if (CurrentHealth(other))
        {
            plr = CheckPlayer();
            for (i = 9 ; i >= 0 && plr < 0 ; i --)
            {
                if (!MaxHealth(player[i]))
                {
                    plr = PlayerClassOnInit(i, GetCaller());
                    break;
                }
            }
            if (plr >= 0)
            {
                PlayerClassOnJoin(plr, player[plr]);
                break;
            }
        }
        PlayerClassFailToJoin();
        break;
    }
}

void PlayerClassFastJoin()
{
    if (CurrentHealth(other))
    {
        int plr = CheckPlayer();
        if (plr < 0)
            MoveObject(other, LocationX(14), LocationY(14));
        else
        {
            MoveObject(other, LocationX(13), LocationY(13));
            UniPrint(other, "패스트 조인되었습니다");
        }
    }
}

void PlayerClassProc(int plr)
{
    int plrUnit = player[plr];

    if (UnitCheckEnchant(plrUnit, GetLShift(ENCHANT_SNEAK)))
    {
        if (PlayerClassCheckWindboost(plr))
            WindBooster(plrUnit);
    }
    PlayerClassMissileWandFix(plr, plrUnit);
}

int PlayerClassCheckDeathFlag(int plr)
{
    return player[plr + 10] & PLAYER_FLAG_DEATH;
}

void PlayerClassSetDeathFlag(int plr)
{
    player[plr + 10] = player[plr + 10] ^ PLAYER_FLAG_DEATH;
}

void PlayerClassSetAllEnchant(int plr)
{
    player[plr + 10] = player[plr + 10] ^ PLAYER_FLAG_ALLBUFF;
}

int PlayerClassCheckAllEnchant(int plr)
{
    return player[plr + 10] & PLAYER_FLAG_ALLBUFF;
}

void PlayerClassSetWindboost(int plr)
{
    player[plr + 10] ^= PLAYER_FLAG_WINDBOOST;
}

int PlayerClassCheckWindboost(int plr)
{
    return player[plr + 10] & PLAYER_FLAG_WINDBOOST;
}

void PlayerClassOnDeath(int plr)
{
    UniPrintToAll(PlayerIngameNick(player[plr]) + " 님께서 적에게 격추되었습니다");
}

void PlayerClassOnExit(int plr)
{
    player[plr] = 0;
    player[plr + 10] = 0;
}

void PlayerClassOnLoop()
{
    int i;

    for (i = 9 ; i >= 0 ; i -= 1)
    {
        while (true)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    PlayerClassProc(i);
                    break;
                }
                else
                {
                    if (PlayerClassCheckDeathFlag(i))
                        break;
                    else
                    {
                        PlayerClassSetDeathFlag(i);
                        PlayerClassOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
                PlayerClassOnExit(i);
            break;
        }
    }
    FrameTimer(1, PlayerClassOnLoop);
}

string PotionList(int index)
{
    string list[] = {"RedPotion", "CurePoisonPotion", "YellowPotion", "BlackPotion",
        "VampirismPotion", "Mushroom", "PoisonProtectPotion", "ShockProtectPotion",
        "FireProtectPotion", "HastePotion", "ShieldPotion", "InfravisionPotion",
        "InvisibilityPotion", "AmuletofManipulation", "AmuletofManipulation", "AmuletofNature",
        "Fear", "WhitePotion", "BluePotion", "BottleCandle"};

    return list[index];
}

string WeaponList(int index)
{
    string name[] = {
        "GreatSword", "Longsword", "Sword", "MorningStar",
        "OgreAxe", "StaffWooden", "BattleAxe", "FanChakram",
        "RoundChakram", "WarHammer", "OblivionHalberd", "OblivionWierdling",
        "OblivionHeart"
    };
    return name[index];
}

string ArmorList(int index)
{
    string name[] = {
        "OrnateHelm", "Breastplate", "PlateArms", "PlateBoots",
        "PlateLeggings", "MedievalCloak", "ChainCoif", "ChainLeggings",
        "ChainTunic", "SteelHelm", "LeatherArmbands", "LeatherArmor",
        "LeatherArmoredBoots", "LeatherBoots", "LeatherHelm", "LeatherLeggings",
        "MedievalPants", "MedievalShirt"
    };
    return name[index];
}

int RewardClassDropHotPotion(int sUnit)
{
    return CreateObjectAt("RedPotion", GetObjectX(sUnit), GetObjectY(sUnit));
}

void ItemUseClassTeleportAmulet()
{
    if (CurrentHealth(other))
    {
        Delete(self);
        Effect("TELEPORT", GetObjectX(other), GetObjectY(other), 0.0, 0.0);

        MoveObject(other, LocationX(SAFEZONE_ID), LocationY(SAFEZONE_ID));
        PlaySoundAround(other, 6);
        Effect("TELEPORT", GetObjectX(other), GetObjectY(other), 0.0, 0.0);
        Effect("SMOKE_BLAST", GetObjectX(other), GetObjectY(other), 0.0, 0.0);
        UniPrint(other, "안전한 곳으로 공간이동 하였습니다");
    }
}

void ItemUseClassElectricAmulet()
{
    if (CurrentHealth(other))
    {
        if (CurrentHealth(other) ^ MaxHealth(other))
            RestoreHealth(other, 65);
        Enchant(other, EnchantList(27), 15.0);
        Enchant(other, EnchantList(22), 15.0);
        Delete(self);
    }
}

void ItemUseClassHealingPotion()
{
    if (CurrentHealth(other))
    {
        if (!UnitCheckEnchant(other, GetLShift(ENCHANT_CROWN)))
        {
            Enchant(other, EnchantList(ENCHANT_CROWN), 10.0);

            int healing = CreateObjectAtUnit("InvisibleLightBlueLow", other);
            SetOwner(other, healing);
            Enchant(healing, EnchantList(ENCHANT_RUN), 0.0);
            FrameTimerWithArg(1, healing, UnitHealingHandler);

            PlaySoundAround(other, 1004);
            UniPrint(other, "자연의 힘이 잠시동안 당신을 지속적으로 치료해 줍니다");
        }
        else
            UniPrint(other, "당신은 이미 이 능력의 영향권에 있습니다");
        Delete(self);
    }
}

void ItemUseClassCandle()
{
    if (CurrentHealth(other))
    {
        Delete(self);
        CastSpellObjectObject("SPELL_FORCE_OF_NATURE", other, other);
        UniPrint(other, "광화문 1000만 촛불의 힘을 보여주겠다!");
    }
}

int CreateYellowPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 639); //YellowPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateBlackPotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 641); //BlackPotion
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CreateWhitePotion(int restoreAmount, float xProfile, float yProfile)
{
    int unit = CreateObjectAt("RedPotion", xProfile, yProfile);
    int ptr = GetMemory(0x750710);

    SetMemory(ptr + 4, 640); //WhitePotion
    SetMemory(ptr + 12, GetMemory(ptr + 12) ^ 0x20);
    SetMemory(GetMemory(ptr + 0x2e0), restoreAmount);

    return unit;
}

int CheckPotionThingID(int unit)
{
    int thingID = GetUnitThingID(unit), x = unit;

    if (thingID == 639)
        x = CreateYellowPotion(125, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 640)
        x = CreateWhitePotion(100, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 641)
        x = CreateBlackPotion(85, GetObjectX(unit), GetObjectY(unit));
    else if (thingID == 1184)
        SetUnitCallbackOnUseItem(unit, ItemUseClassTeleportAmulet);
    else if (thingID == 239)
        SetUnitCallbackOnUseItem(unit, ItemUseClassElectricAmulet);
    else if (thingID == 1185)
        SetUnitCallbackOnUseItem(unit, ItemUseClassHealingPotion);
    else if (thingID == 1197)
        SetUnitCallbackOnUseItem(unit, ItemUseClassCandle);

    if (x ^ unit) Delete(unit);
    return x;
}

int RewardClassDropPotion(int sUnit)
{
    return CheckPotionThingID(CreateObjectAtUnit(PotionList(Random(0, 19)), sUnit));
}

int MoneyDrop(int sUnit)
{
    int money = CreateObjectAt("Gold", GetObjectX(sUnit), GetObjectY(sUnit));

    SetMemory(GetMemory(GetMemory(0x750710) + 0x2b4), Random(1000, 3000));
    return money;
}

int SomeGermDrop(int sUnit)
{
    string name[] = {"Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Emerald", "Diamond"};

    return CreateObjectAtUnit(name[Random(0, 5)], sUnit);
}

int NormalWeaponItemDrop(int sUnit)
{
    int weapon = CreateObjectAtUnit(WeaponList(Random(0, 7)), sUnit);

    CheckSpecialItem(weapon);
    return weapon;
}

int NormalArmorItemDrop(int sUnit)
{
    return CreateObjectAtUnit(ArmorList(Random(0, 17)), sUnit);
}

int WeaponItemDrop(int sUnit)
{
    int unit = CreateObjectAtUnit(WeaponList(Random(0, 12)), sUnit);

    CheckSpecialItem(unit);
    SetWeaponProperties(unit, Random(0, 5), Random(0, 5), Random(0, 36), Random(0, 36));
    return unit;
}

int ArmorItemDrop(int sUnit)
{
    int unit = CreateObjectAtUnit(ArmorList(Random(0, 17)), sUnit);

    SetArmorProperties(unit, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
    return unit;
}

void DungeonMonsterDeath()
{
    int maxCount = DROP_ITEM_FUNCTION - 1;

    CallFunctionWithArgInt(g_dropItemFunctions[Random(0, maxCount)], self);
    DeleteObjectTimer(self, 150);
}

void CheckSpecialItem(int weapon)
{
    int thingId = GetUnitThingID(weapon);
    int ptr = UnitToPtr(weapon);

    if (thingId >= 222 && thingId <= 225)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId == 1178)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
    else if (thingId == 1168)
        SetMemory(GetMemory(ptr + 0x2e0), 0xffff);
}

void WindBooster(int plrUnit)
{
    EnchantOff(plrUnit, EnchantList(ENCHANT_SNEAK));
    RemoveTreadLightly(plrUnit);
    Enchant(plrUnit, EnchantList(ENCHANT_RUN), 0.3);
    PushObjectTo(plrUnit, UnitAngleCos(plrUnit, 70.0), UnitAngleSin(plrUnit, 70.0));
    Effect("RICOCHET", GetObjectX(plrUnit), GetObjectY(plrUnit), 0.0, 0.0);
}

int PlaceMagicWeapon(int location)
{
    int weapon = CreateObjectAt("OblivionHeart", LocationX(location), LocationY(location));

    SetWeaponProperties(weapon, 5, 5, 8, 3);
    //Todo. 여기에서 마법 세팅을 한다

    return weapon;
}

int SummonMobSwordsman(int sUnit)
{
    int unit = CreateObjectAtUnit("Swordsman", sUnit);

    SetUnitMaxHealth(unit, 325);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    return unit;
}

int SummonMobArcher(int sUnit)
{
    int unit = CreateObjectAtUnit("Archer", sUnit);

    SetUnitMaxHealth(unit, 98);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    return unit;
}

int SummonMobMecaFlying(int sUnit)
{
    int unit = CreateObjectAtUnit("FlyingGolem", sUnit);

    SetUnitMaxHealth(unit, 64);
    return unit;
}

void MobMakerProc(int maker)
{
    int count = GetDirection(maker);

    while (IsObjectOn(maker))
    {
        if (count)
        {
            FrameTimerWithArg(1, maker, MobMakerProc);
            LookWithAngle(maker, --count);
            int mob = CallFunctionWithArgInt(ToInt(GetObjectZ(maker)), maker);

            if (CurrentHealth(mob))
                SetCallback(mob, 5, MobMakerMobDeath);
            break;
        }
        Delete(maker);
        break;
    }
}

int MobMakerCounter()
{
    int counter, nextFunction;

    return &counter;
}

void SetNextFunction(int nextFunction)
{
    int *ptr = MobMakerCounter();

    ptr[1] = nextFunction;
}

void MobMakerDecraseCounter()
{
    int *counterPtr = MobMakerCounter();

    if (--counterPtr[0] == 0)
    {
        CallFunction(counterPtr[1]);
        SetNextFunction(EndAreaDefault);
    }
}

void MobMakerMobDeath()
{
    MobMakerDecraseCounter();
    DungeonMonsterDeath();
}

int PlaceMobMaker(int location, int count, object function)
{
    int maker = CreateObjectAt("ImaginaryCaster", LocationX(location), LocationY(location));
    int *counterPtr = MobMakerCounter();

    LookWithAngle(maker, count);
    Raise(maker, function);
    counterPtr[0] += count;
    return maker;
}

void MobMakerStartSummon(int delay, int maker)
{
    if (IsObjectOn(maker))
    {
        if (!delay)
            delay = 1;
        FrameTimerWithArg(delay, maker, MobMakerProc);
    }
}

void SaveLastLocation(int location)
{
    TeleportLocation(g_userLastSavedLocation, LocationX(location), LocationY(location));
}

void TeleportComplete(int point, int owner)
{
    Effect("SMOKE_BLAST", GetObjectX(point), GetObjectY(point), 0.0, 0.0);
    Effect("TELEPORT", GetObjectX(point), GetObjectY(point), 0.0, 0.0);
    MoveObject(owner, GetObjectX(point), GetObjectY(point));
    PlaySoundAround(owner, 6);
    Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
}

void TeleportProgress(int point)
{
    int owner = GetOwner(point), durate = GetDirection(point);

    while (IsObjectOn(point))
    {
        if (CurrentHealth(owner))
        {
            if (durate)
            {
                if (DistanceUnitToUnit(point + 1, owner) < 23.0)
                {
                    FrameTimerWithArg(1, point, TeleportProgress);
                    LookWithAngle(point, --durate);
                    break;
                }
            }
            else
                TeleportComplete(point, owner);
            EnchantOff(owner, EnchantList(ENCHANT_BURNING));
        }
        Delete(point);
        Delete(++point);
        break;
    }
}

void TeleportStart(int target, int destLocation)
{
    if (CurrentHealth(target))
    {
        if (!UnitCheckEnchant(target, GetLShift(ENCHANT_BURNING)))
        {
            Enchant(target, EnchantList(ENCHANT_BURNING), 4.0);
            int point = CreateObjectAt("ImaginaryCaster", LocationX(destLocation), LocationY(destLocation));
            CreateObjectAt("VortexSource", GetObjectX(target), GetObjectY(target));
            FrameTimerWithArg(1, point, TeleportProgress);
            SetOwner(target, point);
            LookWithAngle(point, 48);
            GreenSparkAt(GetObjectX(target), GetObjectY(target));
            PlaySoundAround(point + 1, 772);    //SoundFX: LongBellsUp
            if (IsPlayerUnit(target))
                UniPrint(target, "공간이동을 준비중입니다. 캐릭터를 움직이면 취소됩니다");
        }
    }
}

void EntryTeleportPortal()
{
    TeleportStart(OTHER, 51);
}

int PlaceLastSavedTeleport(object location)
{
    int telpo = CreateObjectAt("WeirdlingBeast", LocationX(location), LocationY(location));

    Enchant(CreateObjectAtUnit("InvisibleLightBlueLow", telpo), EnchantList(14), 0.0);
    Damage(telpo, 0, MaxHealth(telpo) + 1, -1);
    SetCallback(telpo, 9, EntryTeleportPortal);

    return telpo;
}

static int MonsterMeleeAttackRegistCallback()
{ 
    return MonsterStrikeCallback;
}

void StartMovingPicket(int picketUnit)
{
    if (IsObjectOn(picketUnit))
    {
        Enchant(picketUnit, "ENCHANT_RUN", 0.0);
        Enchant(picketUnit, "ENCHANT_FREEZE", 0.0);
        Move(picketUnit, Random(278, 281));
    }
}

void DelayMapInit()
{
    g_userLastSavedLocation = 51;

    PlaceLastSavedTeleport(50);
    PlaceDefaultItems("RedPotion", 48);
    PlaceDefaultItems("RedPotion", 47);
    PlaceDefaultItems("GreatSword", 46);
    PlaceDefaultItems("RoundChakram", 45);
    ShopClassMagicalWeaponMarketCreate(49);
    ShopClassAllEnchantMarketCreate(67);
    ShopClassWindboostMarketCreate(275);
    ShopClassInvulnerabilityItemShopCreate(157);
    ShopClassDemonsWandItemShopCreate(286);
    ShopClassMissileWandItemShopCreate(287);
    RegistSignMessage(Object("MapSign1"), "안녕하시오! 여행자. 이곳은 당신같은 사람이면 위험한 곳이지..");
    RegistSignMessage(Object("MapSign2"), "-이곳은 이마트 편의점의 입구입니다- 거래를 하려면 문을열고 안으로 오세요");
    RegistSignMessage(Object("MapSign3"), "특수 무기를 판매하고 있어요");
    RegistSignMessage(Object("MapSign4"), "이 워프게이트는 사용자가 마지막으로 탐험한 위치로 이동합니다");
    RegistSignMessage(Object("MapSign5"), "이것은 \"마법의 우물\"입니다. 짧은 시간동안 지속적으로 당신을 치료합니다");
    RegistSignMessage(Object("MapSign6"), " \"마녀의 성\"에 오신걸 환영합니다. 이곳 최심부에 있는 마녀를 쓰러뜨리면 승리합니다 ");
    RegistSignMessage(Object("MapSign7"), "안내문 \"마녀의 체스장\"");
    RegistSignMessage(Object("MapSign8"), "안내문 \"마녀의 실험실\"");
    RegistSignMessage(Object("MapSign9"), "안내문 \"마녀의 버섯이랑 광물 체굴하는 장\"");
    RegistSignMessage(Object("MapSignA"), "안내문 \"마녀의 이벤트 실\"");
    RegistSignMessage(Object("MapSignB"), "안내문 \"마녀의 (고)남편\" 삼가 고인의 명목을 액션빔!");
    RegistSignMessage(Object("MapSignC"), "마녀의 저택 앞마당. 소유주의 땅이므로 부지를 허가 없이 침입 하거나 개발하지 말 것");
    RegistSignMessage(Object("MapSignD"), "저택으로 공급되는 수도관 및 기계실");
    RegistSignMessage(Object("MapSignE"), "그림설명: 이 그림은 도대체 뭘 그린건지 나도 모르겠다");
    RegistSignMessage(Object("MapSignF"), "마녀의 별관 뒷편 위험한 늪");
    RegistSignMessage(Object("MapSignG"), "플레이 해주셔서 감사합니다- 게임 끝나는 지점");
    RegistSignMessage(Object("MapSignH"), "이 벽이 열리게 되면 양 옆에 있는 스위치를 조작하여 전방의 벽을 열 수 있다");
    RegistSignMessage(Object("MapSignI"), "여러분 이제 마지막 시험입니다. 마녀를 죽이십시오");
    RegistSignMessage(Object("MapSignJ"), "여기 놓인 유닛배치를 보고 서쪽에 있는 오벨리스크를 움직여 배치하세요. 이미 완료하셨으면 개무시바람");
    RegistSignMessage(Object("MapSignK"), "동쪽에서 보고온 유닛배치 대로 오벨리스크를 클릭하여 움직여 배치한다");
    RegistSignMessage(Object("MapSignL"), "관광 안내문 \"여기는 마녀가 온천욕을 즐기는 곳이다\"");
    FrameTimerWithArg(1, Object("MapSign6"), StartMovingPicket);

    g_ternnelSwitch = Object("TernnelSwitch");
    ObjectOff(g_ternnelSwitch);
    Enchant(g_ternnelSwitch, EnchantList(0), 0.0);
    FrameTimer(60, InitBlocks);
    FrameTimer(60, InitLastPart);
    FrameTimer(20, InitWeaponContainer);
    // FrameTimer(1, InitializeGenerators);
    FrameTimer(1, InitTeleports);
    InitSavePoint();

    SetHostileCritter();
}

void EntryArea1()
{
    int once;

    ObjectOff(SELF);
    if (!once)
    {
        once = true;
        MobMakerStartSummon(0, PlaceMobMaker(15, 30, SummonMobSwordsman));
        MobMakerStartSummon(0, PlaceMobMaker(16, 30, SummonMobArcher));
        MobMakerStartSummon(0, PlaceMobMaker(17, 30, SummonMobMecaFlying));
        SetNextFunction(EndArea1);
    }
}

void EndAreaDefault()
{ }

void EndArea1()
{
    int i;

    for (i = 18 ; i <= 29 ; i += 1)
        WallUtilOpenWallAtObjectPosition(i);
    FrameTimer(30, EntryArea2);
    TeleportLocation(SAFEZONE_ID, LocationX(276), LocationY(276));
    UniPrintToAll("비밀의 벽이 열립니다");
}

void EntryArea2()
{
    MobMakerStartSummon(0, PlaceMobMaker(30, 60, SummonMobMystic));
    SetNextFunction(EndArea2);
}

int SummonMobMystic(int sUnit)
{
    int unit = CreateObjectAtUnit("Wizard", sUnit);

    SetUnitMaxHealth(unit, 260);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    return unit;
}

void EndArea2()
{
    int i;

    for (i = 0 ; i < 4 ; i += 1)
    {
        WallUtilOpenWallAtObjectPosition(31);
        WallUtilOpenWallAtObjectPosition(35);
        TeleportLocationVector(31, -23.0, 23.0);
        TeleportLocationVector(35, -23.0, 23.0);
    }
    FrameTimer(30, EntryArea3);
    UniPrintToAll("비밀의 벽이 열렸습니다!");
}

int SummonMobBlackSpider(int posUnit)
{
    int unit = CreateObjectAtUnit("BlackWidow", posUnit);

    BlackWidowSubProcess(unit);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 1.0);

    return unit;
}

int SummonMobGoon(int posUnit)
{
    int unit = CreateObjectAtUnit("Goon", posUnit);

    UnitLinkBinScript(unit, GoonBinTable());
    SetUnitVoice(unit, 63);
    SetUnitMaxHealth(unit, 275);

    return unit;
}

void DryadSightHandler()
{ }

void EnableCreatureSight()
{
    EnchantOff(SELF, EnchantList(ENCHANT_BLINDED));
}

int SummonMobDryad(int posUnit)
{
    int dry = CreateObjectAtUnit("WizardGreen", posUnit);

    SetUnitMaxHealth(dry, 375);
    SetUnitStatus(dry, GetUnitStatus(dry) ^ 0x8020);
    SetCallback(dry, 3, DryadSightHandler);
    SetCallback(dry, 13, EnableCreatureSight);
    return dry;
}

int SummonUrchin(int posUnit)
{
    int unit = CreateObjectAtUnit("Urchin", posUnit);

    UnitZeroFleeRange(unit);
    SetUnitMaxHealth(unit, 130);
    return unit;
}

void RemoveSingleWallWithFx(int location, float vectX, float vectY)
{
    WallUtilOpenWallAtObjectPosition(location);
    Effect("JIGGLE", LocationX(location), LocationY(location), 11.0, 0.0);
    Effect("SMOKE_BLAST", LocationX(location), LocationY(location), 11.0, 0.0);
    if (ToInt(vectX) || ToInt(vectY))
        TeleportLocationVector(location, vectX, vectY);
}

void ClearMinesWalls()
{
    int i;

    for (i = 0 ; i < 18 ; i ++)
    {
        if (i < 8)
            RemoveSingleWallWithFx(53, -23.0, 23.0);
        RemoveSingleWallWithFx(54, 23.0, 23.0);
    }
}

void EndArea3()
{
    FrameTimer(10, ClearMinesWalls);
    FrameTimer(20, EntryArea4);
}

void EntryArea3()
{
    MobMakerStartSummon(3, PlaceMobMaker(39, 20, SummonMobBlackSpider));
    MobMakerStartSummon(3, PlaceMobMaker(40, 20, SummonMobGoon));
    MobMakerStartSummon(3, PlaceMobMaker(41, 20, SummonUrchin));
    SetNextFunction(EndArea3);
}

int SummonMobStoneGiant(int posUnit)
{
    int giant = CreateObjectAtUnit("StoneGolem", posUnit);

    SetUnitMaxHealth(giant, 750);
    Enchant(giant, EnchantList(9), 0.0);
    return giant;
}

int SummonMobImp(int posUnit)
{
    int fairy = CreateObjectAtUnit("Imp", posUnit);

    ImpSubProcess(fairy);
    return fairy;
}

int SummonMobShade(int posUnit)
{
    int shade = CreateObjectAtUnit("Shade", posUnit);

    SetUnitMaxHealth(shade, 225);
    return shade;
}

void EndArea4()
{
    WallUtilOpenWallAtObjectPosition(68);
    WallUtilOpenWallAtObjectPosition(69);
    WallUtilOpenWallAtObjectPosition(70);
    // TeleportLocation(PLAYER_TELEPORT_LOCATION, LocationX(39), LocationY(39));
    
    FrameTimer(30, EntryArea5);
}

void EntryArea4()
{
    MobMakerStartSummon(1, PlaceMobMaker(42, 32, SummonMobStoneGiant));
    MobMakerStartSummon(1, PlaceMobMaker(43, 20, SummonMobImp));
    MobMakerStartSummon(1, PlaceMobMaker(44, 32, SummonMobShade));
    SetNextFunction(EndArea4);
}

int SummonMobWasp(int posUnit)
{
    int honey = CreateObjectAtUnit("Wasp", posUnit);

    WaspSubProcess(honey);
    return honey;
}

int SummonMobScorpion(int posUnit)
{
    int mob = CreateObjectAtUnit("Scorpion", posUnit);

    ScorpionSubProcess(mob);
    return mob;
}

int SummonMobHorrendous(int posUnit)
{
    int mob = CreateObjectAtUnit("Horrendous", posUnit);

    HorrendousSubProcess(mob);
    RegistUnitStrikeHook(mob);
    return mob;
}

int SummonBigFish(int posUnit)
{
    int mob = CreateObjectAtUnit("FishBig", posUnit);

    FishBigSubProcess(mob);
    RegistUnitStrikeHook(mob);
    return mob;
}

int SummonLichMaster(int posUnit)
{
    int master = CreateObjectAtUnit("LichLord", posUnit);

    LichLordSubProcess(master);
    return master;
}

void EndArea5()
{
    UnlockDoor(Object("ExitLocker1"));
    UnlockDoor(Object("ExitLocker2"));
    UniPrintToAll("출입문이 열렸습니다");
    FrameTimer(30, EntryArea6);
}

void EntryArea5()
{
    MobMakerStartSummon(1, PlaceMobMaker(75, 6, SummonMobWasp));
    MobMakerStartSummon(1, PlaceMobMaker(76, 6, SummonMobWasp));

    MobMakerStartSummon(3, PlaceMobMaker(72, 15, SummonMobScorpion));
    MobMakerStartSummon(3, PlaceMobMaker(73, 15, SummonMobScorpion));
    MobMakerStartSummon(3, PlaceMobMaker(74, 23, SummonMobHorrendous));
    SetNextFunction(EndArea5);
}

void EndArea6()
{
    WallUtilOpenWallAtObjectPosition(103);
    WallUtilOpenWallAtObjectPosition(104);
    WallUtilOpenWallAtObjectPosition(105);
    FrameTimer(1, EntryArea7);
    FrameTimer(30, InitializeGenerators);
    UniPrintToAll("좌측상단의 비밀벽 하나가 열렸습니다");
}

void EntryArea6()   //error here
{
    //100, 101// (98, 99)
    MobMakerStartSummon(1, PlaceMobMaker(100, 20, SummonMobStrongWizard));
    MobMakerStartSummon(1, PlaceMobMaker(101, 20, SummonBigFish));
    MobMakerStartSummon(10, PlaceMobMaker(98, 20, SummonLichMaster));
    MobMakerStartSummon(11, PlaceMobMaker(99, 20, SummonMobRedWizard));
    // MobMakerStartSummon(1, PlaceMobMaker(101, 20, Summon))
    SetNextFunction(EndArea6);
}

void EndArea7()
{
    EntryArea8();   //here
}

void EntryArea7()
{
    MobMakerStartSummon(1, PlaceMobMaker(106, 20, SummonBigFish));
    MobMakerStartSummon(3, PlaceMobMaker(107, 20, SummonMobPlant));
    MobMakerStartSummon(5, PlaceMobMaker(108, 30, SummonMobGoon));
    MobMakerStartSummon(9, PlaceMobMaker(109, 30, SummonMobLich));

    SetNextFunction(EndArea8);
}

/**
* @brief. strike a weapon handler
* @brief. self - the weapon
* @brief. other - the holder
*/



void PlaceDefaultItems(string itemname, object location)
{
    int i;
    
    for (i = 0 ; i < 8 ; i += 1)
    {
        CreateObjectAt(itemname, LocationX(location), LocationY(location));
        TeleportLocationVector(location, 23.0, 23.0);
    }
}

void ShopClassSetInvulnerabilityItemDesc()
{
    UniPrint(OTHER, "당신이 소유한 모든 아이템을 무적화 시켜드립니다");
    UniPrint(OTHER, "이 작업은 금화 5천이 필요합니다. 계속하려면 \"예\" 를 누르세요");
    TellStoryUnitName("AA", "Con05A.scr:FarmerWifeGreeting", "인벤토리 무적화\n5천골드");
}

void ShopClassSetInvulnerabilityItemTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;
    if (GetGold(OTHER) >= 5000)
    {
        if (SetInvulnerabilityItem(OTHER))
        {
            ChangeGold(OTHER, -5000);
            UniPrint(OTHER, "결제가 완료되었습니다");
        }
        else
            UniPrint(OTHER, "처리할 아이템이 없어서 거래되지 않았습니다");
    }
}

void ShopClassMagicWeaponDesc()
{
    int curIndex = GetDirection(SELF);
    string desc = WeaponContainerDesc(curIndex);

    if (desc != "null")
    {
        UniPrint(OTHER, desc + " 을 구입하시겠어요? 가격은 " + IntToString(WeaponContainerPay(curIndex)) + " 골드 입니다");
        UniPrint(OTHER, "구입하시려면 '예' 를 누르시고, 다른 아이템을 보시려면 '아니오'를 누르세요. 거래를 취소하려면 '떠나기'를 누릅니다");
        TellStoryUnitName("AA", "thing.db:IdentifyDescription", desc);
    }
}

void ShopClassMagicWeaponTrade()
{
    int dlgResult = GetAnswer(SELF), curIndex = GetDirection(SELF);

    if (dlgResult == 1)     //@brief. YES
    {
        int pay = WeaponContainerPay(curIndex);

        if (pay < 0)
            return;
        if (GetGold(OTHER) >= pay)
        {
            ChangeGold(OTHER, -pay);
            object resultFunction = WeaponContainerFunction(curIndex);

            if (resultFunction >= 0)
            {
                TeleportLocation(110, GetObjectX(OTHER), GetObjectY(OTHER));
                CallFunctionWithArgInt(resultFunction, 110);
            }
            // FrameTimerWithArg(1, resultItem, DelayForcePickItemToOwner);
            PlaySoundAround(OTHER, 308);
            UniPrint(OTHER, WeaponContainerDesc(curIndex) + " 거래가 완료되었습니다");
        }
        else
            UniPrint(OTHER, "거래가 취소되었습니다. 잔액이 부족합니다");
    }
    else if (dlgResult == 2)    //@brief. NO
    {
        UniPrint(OTHER, "'아니오'를 누르셨습니다. 다음 판매 품목을 보여드립니다");
        LookWithAngle(SELF, (curIndex + 1) % WeaponContainerCount());
        ShopClassMagicWeaponDesc();
    }
}

void ShopClassAwardWindboostDesc()
{
    UniPrint(OTHER, "짧은 거리를 빠르게 이동할 수 있는 전사의 이동기 윈드 부스트를 배우시겠어요?");
    UniPrint(OTHER, "이것의 가격은 10000 골드이며 계속하려면 \"예\" 를 클릭하세요");
    TellStoryUnitName("AA", "Con02A:NecroTalk01", "윈드부스터");
}

void ShopClassAwardWindboostTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    int plr = CheckPlayer();

    if (plr < 0)
        return;

    if (GetGold(OTHER) >= 10000)
    {
        if (PlayerClassCheckWindboost(plr))
            UniPrint(OTHER, "당신은 이미 이 능력을 지녔다");
        else
        {
            ChangeGold(OTHER, -10000);
            PlayerClassSetWindboost(plr);
            UniPrint(OTHER, "당신은 \"윈드 부스트\" 능력을 가졌습니다");
        }
        
    }
    else
    {
        UniPrint(OTHER, "이런 금화가 모자라는 군요");
    }
    
}

void ShopClassMissileWandDesc()
{
    UniPrint(OTHER, "미사일 완드를 구입하시겠어요? 가격은 3만 골드 입니다");
    TellStoryUnitName("AA", "Con01a:CaptainGreet", "MISSING.c_str");
}

void ShopClassMissileWandTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    if (GetGold(OTHER) >= 30000)
    {
        SummonMissileWand(GetObjectX(OTHER), GetObjectY(OTHER));
        ChangeGold(OTHER, -30000);
        UniPrint(OTHER, "결제가 완료되었습니다 (마법 미사일의 지팡이를 구입하신겁니다) -3만 골드가 차감되었어요");
    }
    else
    {
        UniPrint(OTHER, "잔액부족- 이것은 3만 골드이므로 다시 확인해보세요 ");
    }
    
}

void ShopClassDemonsWandDesc()
{
    UniPrint(OTHER, "용의 숨결 지팡이를 사시겠나요? 가격은 19200 골드 입니다");
    TellStoryUnitName("AA", "Con05:OgreTalk05", "MISSING.c_str");
}

void ShopClassDemonsWandTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    if (GetGold(OTHER) >= 19200)
    {
        SummonDemonsWand(GetObjectX(OTHER), GetObjectY(OTHER));
        ChangeGold(OTHER, -19200);
        UniPrint(OTHER, "결제가 완료되었습니다 (용의 숨결 지팡이를 구입핫");
    }
    else
    {
        UniPrint(OTHER, "잔액부족- 이것은 19200 골드이므로 다시 확인해보세요 ");
    }
    
}

void ShopClassAllEnchantmentDesc()
{
    UniPrint(OTHER, "올 엔첸트 능력을 구입하시겠어요? 가격은 6만 골드 입니다");
    UniPrint(OTHER, "올 엔첸트 능력은 각종 유용한 버프가 당신에게 항상 지속됩니다");
    UniPrint(OTHER, "계속 거래하기를 원하시면 '예' 을 누르세요");
    TellStoryUnitName("AA", "thing.db:IdentifyDescription", "올엔첸6만원");
}

void ShopClassAllEnchantmentTrade()
{
    if (GetAnswer(SELF) ^ 1)
        return;

    while (true)
    {
        if (GetGold(OTHER) >= 60000)
        {
            int plr = CheckPlayer();

            if (plr < 0)
                break;
            if (PlayerClassCheckAllEnchant(plr))
            {
                UniPrint(OTHER, "당신은 이미 이 능력의 소유자 입니다");
                break;
            }
            ChangeGold(OTHER, -60000);
            PlayerClassSetAllEnchant(plr);
            PlayerAllEnchantment(OTHER);
            UniPrint(OTHER, "거래가 완료되었습니다. 올 엔첸트 능력은 이제 당신의 소유입니다");
            break;
        }
        UniPrint(OTHER, "거래가 취소되었습니다. 사유: 잔액이 부족합니다");
        break;
    }
}

int ShopClassDemonsWandItemShopCreate(int location)
{
    int mk = DummyUnitCreateById(OBJ_DEMON, LocationX(location), LocationY(location));

    SetDialog(mk, "YESNO", ShopClassDemonsWandDesc, ShopClassDemonsWandTrade);
    return mk;
}

int ShopClassMissileWandItemShopCreate(int location)
{
    int mk = DummyUnitCreateById(OBJ_WIZARD_RED, LocationX(location), LocationY(location));

    SetDialog(mk, "YESNO", ShopClassMissileWandDesc, ShopClassMissileWandTrade);
    return mk;
}

int ShopClassInvulnerabilityItemShopCreate(int location)
{
    int mk = DummyUnitCreateById(OBJ_SWORDSMAN , LocationX(location), LocationY(location));

    SetDialog(mk, "YESNO", ShopClassSetInvulnerabilityItemDesc, ShopClassSetInvulnerabilityItemTrade);
    return mk;
}

int ShopClassMagicalWeaponMarketCreate(int location)
{
    int keeper = DummyUnitCreateById(OBJ_HORRENDOUS, LocationX(location), LocationY(location));

    CreateObjectAtUnit("ImaginaryCaster", keeper);
    LookWithAngle(keeper, 0);
    SetDialog(keeper, "YESNO", ShopClassMagicWeaponDesc, ShopClassMagicWeaponTrade);

    return keeper;
}

int ShopClassWindboostMarketCreate(int locationId)
{
    int teacher = DummyUnitCreateById(OBJ_WIZARD_WHITE, LocationX(locationId), LocationY(locationId));

    SetDialog(teacher, "YESNO", ShopClassAwardWindboostDesc, ShopClassAwardWindboostTrade);
    return teacher;
}

int ShopClassAllEnchantMarketCreate(int location)
{
    int allEnchantMarket = DummyUnitCreateById(OBJ_WIZARD_GREEN , LocationX(location), LocationY(location));

    SetDialog(allEnchantMarket, "YESNO", ShopClassAllEnchantmentDesc, ShopClassAllEnchantmentTrade);
    return allEnchantMarket;
}

void LightningShotCollide()
{
    if (GetTrigger() && GetCaller())
    {
        Damage(other, 0, 50, 9);
        Enchant(other, EnchantList(25), 2.0);
        Delete(self);
    }
}

int LightningShotSingle(float posX, float posY)
{
    int mis = CreateObjectAt("LightningBolt", posX, posY);
    
    Enchant(mis, "ENCHANT_FREEZE", 0.0);
    SetUnitCallbackOnCollide(mis, LightningShotCollide);
    return mis;
}

int LightningShooter(object baseUnit, float shotVectX, float shotVectY, float xVect, float yVect)
{
    int mis = LightningShotSingle(GetObjectX(baseUnit) + xVect, GetObjectY(baseUnit) + yVect);

    PushObjectTo(mis, shotVectX, shotVectY);
    return mis;
}

void LightningShotGunTrapArea1()
{
    int i, trpBase = CreateObjectAt("ImaginaryCaster", LocationX(52), LocationY(52));

    for (i = 0 ; i < 10 ; i ++)
        LightningShooter(trpBase + i, -32.0, 32.0, 13.0, 13.0);
    Delete(trpBase);
}

void UnitHealingHandler(int healing)
{
    int owner = GetOwner(healing);

    while (IsObjectOn(healing))
    {
        if (CurrentHealth(owner) && UnitCheckEnchant(owner, GetLShift(ENCHANT_CROWN)))
        {
            RestoreHealth(owner, 1);
            if (ToInt(DistanceUnitToUnit(owner, healing)))
                MoveObject(healing, GetObjectX(owner), GetObjectY(owner));
            FrameTimerWithArg(1, healing, UnitHealingHandler);
            break;
        }
        Delete(healing);
        break;
    }
}

void WellRefreshing()
{
    if (!UnitCheckEnchant(other, GetLShift(ENCHANT_CROWN)))
    {
        Enchant(other, EnchantList(ENCHANT_CROWN), 10.0);

        int healing = CreateObjectAtUnit("InvisibleLightBlueLow", other);
        SetOwner(other, healing);
        Enchant(healing, EnchantList(ENCHANT_RUN), 0.0);
        FrameTimerWithArg(1, healing, UnitHealingHandler);

        PlaySoundAround(self, 1004);
        UniPrint(other, "우물의 마법이 잠시동안 당신을 지속적으로 치료해 줍니다");
    }
}

int DisposeBlockGroup(int baseLocation)
{
    int blocks = CreateObjectAt("ImaginaryCaster", LocationX(baseLocation), LocationY(baseLocation));

    CreateObjectAtUnit("ImaginaryCaster", blocks);
    Frozen(CreateObjectAt("SpikeBlock", LocationX(baseLocation), LocationY(baseLocation)), true);
    Frozen(CreateObjectAt("SpikeBlock", LocationX(baseLocation + 1), LocationY(baseLocation + 1)), true);
    Frozen(CreateObjectAt("SpikeBlock", LocationX(baseLocation + 2), LocationY(baseLocation + 2)), true);

    return blocks;
}

void SetBlockParams(int blocks, float vectX, float vectY, int moveDist)
{
    if (IsObjectOn(blocks))
    {
        LookWithAngle(blocks + 1, moveDist);
        Raise(blocks, vectX);
        Raise(blocks + 1, vectY);
    }
}

void InitBlocks()
{
    g_blockLeftUp = DisposeBlockGroup(55);
    g_blockLeftDown = DisposeBlockGroup(58);
    g_blockRightLeft = DisposeBlockGroup(61);
    g_blockRightRight = DisposeBlockGroup(64);

    SetBlockParams(g_blockLeftUp, 2.0, 2.0, 57);
    SetBlockParams(g_blockLeftDown, 2.0, -2.0, 57);
    SetBlockParams(g_blockRightLeft, 2.0, -2.0, 57);
    SetBlockParams(g_blockRightRight, -2.0, -2.0, 57);
}

void BlockReset(int blocks)
{
    ObjectOn(blocks);
}

void BlockMovingCommon(int blocks)
{
    int durate = GetDirection(blocks);
    int obstacle = blocks + 2;

    if (durate)
    {
        float vectX = GetObjectZ(blocks);
        float vectY = GetObjectZ(blocks + 1);

        MoveObjectVector(obstacle, vectX, vectY);
        MoveObjectVector(obstacle + 1, vectX, vectY);
        MoveObjectVector(obstacle + 2, vectX, vectY);
        LookWithAngle(blocks, durate - 1);
        FrameTimerWithArg(1, blocks, BlockMovingCommon);
    }
    else
    {
        Raise(blocks, -GetObjectZ(blocks)); //direction swapping
        Raise(blocks + 1, -GetObjectZ(blocks + 1)); //direction swapping
        // LookWithAngle(blocks, GetDirection(blocks) ^ true);
        ObjectToggle(blocks + 1);
        if (!IsObjectOn(blocks + 1))
        {
            FrameTimerWithArg(30, blocks, BlockMovingCommon);
            LookWithAngle(blocks, GetDirection(blocks + 1));
            PlaySoundAround(obstacle, 882);
            Effect("JIGGLE", GetObjectX(obstacle), GetObjectY(obstacle), 17.0, 0.0);
        }
        else
            FrameTimerWithArg(30, blocks, BlockReset);
    }
}

void StartBlockMoving(int blocks)
{
    if (IsObjectOn(blocks))
    {
        ObjectOff(blocks);  //Lock
        LookWithAngle(blocks, GetDirection(blocks + 1));    //setCounter
        FrameTimerWithArg(1, blocks, BlockMovingCommon);
    }
}

void BlockPressLeft()
{
    int upRow = g_blockLeftUp;
    int downRow = g_blockLeftDown;

    if (IsObjectOn(upRow) && IsObjectOn(downRow))
    {
        PlaySoundAround(SELF, 910);
        StartBlockMoving(upRow);
        StartBlockMoving(downRow);
    }
}

void BlockPressRight()
{
    int leftRow = g_blockRightLeft;
    int rightRow = g_blockRightRight;

    if (IsObjectOn(leftRow) && IsObjectOn(rightRow))
    {
        PlaySoundAround(SELF, 910);
        StartBlockMoving(leftRow);
        StartBlockMoving(rightRow);
    }
}

void HorrendousElectricStrike()
{
    Damage(OTHER, SELF, 7, 9);
    Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    Effect("RICOCHET", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
}

void FishBiteStrike()
{
    LinearOrbMove(CreateObjectAt("CharmOrb", GetObjectX(SELF), GetObjectY(SELF)), UnitRatioX(SELF, OTHER, -1.0), UnitRatioY(SELF, OTHER, -1.0), 3.0, 3);
    Damage(OTHER, SELF, 6, 10);
}

void DrawGeometryRingAt(string orbName, float xProfile, float yProfile, int angleAdder)
{
    int u;
    float speed = 2.3;

    for ( u = 0 ; u < 60 ; u += 1)
        LinearOrbMove(CreateObjectAt(orbName, xProfile, yProfile), MathSine((u * 6) + 90 + angleAdder, -12.0), MathSine((u * 6) + angleAdder, -12.0), speed, 10);
}

int FxBurnningZombie(float xProfile, float yProfile)
{
    int fxU = CreateObjectAt("Zombie", xProfile, yProfile);

    UnitNoCollide(fxU);
    ObjectOff(fxU);
    Damage(fxU, 0, MaxHealth(fxU) + 1, 1);
    return fxU;
}

void HecubahPunchSubCollide()
{
    int owner = GetOwner(SELF);

    while (TRUE)
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 130, 1);
            Enchant(OTHER, "ENCHANT_FREEZE", 1.0);
            Enchant(OTHER, EnchantList(ENCHANT_CHARMING), 0.2);
        }
        else if (!GetCaller())
            1;
        else
            break;
        Delete(SELF);
        break;
    }
}

void HecubahPunchFlying(int subUnit)
{
    while (IsObjectOn(subUnit))
    {
        if (IsVisibleTo(subUnit, subUnit + 1))
        {
            int owner = GetOwner(subUnit), durate = GetDirection(subUnit);
            float xVect = GetObjectZ(subUnit), yVect = GetObjectZ(subUnit + 1);

            if (CurrentHealth(owner) && durate)
            {
                FrameTimerWithArg(1, subUnit, HecubahPunchFlying);
                
                MoveObjectVector(subUnit, xVect, yVect);
                MoveObjectVector(subUnit + 1, xVect, yVect);
                int dmgHelper = DummyUnitCreateById(OBJ_TROLL , GetObjectX(subUnit), GetObjectY(subUnit));

                SetOwner(owner, dmgHelper);
                SetCallback(dmgHelper, 9, HecubahPunchSubCollide);
                DeleteObjectTimer(dmgHelper, 1);
                FxBurnningZombie(GetObjectX(subUnit), GetObjectY(subUnit));
                LookWithAngle(subUnit, -- durate);
                break;
            }
        }
        Delete(subUnit);
        Delete(++ subUnit);
        break;
    }
}

void HecubahPunchSkill()
{
    float xVect = UnitRatioX(OTHER, SELF, 19.0), yVect = UnitRatioY(OTHER, SELF, 19.0);
    int subUnit = CreateObjectAt(INVISIBLE_SUBUNIT, GetObjectX(SELF) + xVect, GetObjectY(SELF) + yVect);

    CreateObjectAt(INVISIBLE_SUBUNIT, GetObjectX(SELF) - xVect, GetObjectY(SELF) - yVect);

    LookWithAngle(subUnit, 28);
    Raise(subUnit, xVect);
    Raise(subUnit + 1, yVect);
    PlaySoundAround(subUnit, 485);
    PlaySoundAround(subUnit + 1, 86);
    SetOwner(SELF, subUnit);
    DrawGeometryRingAt("CharmOrb", GetObjectX(subUnit), GetObjectY(subUnit), 30);
    FrameTimerWithArg(1, subUnit, HecubahPunchFlying);

    UniChatMessage(SELF, "죽어라! 닝겐!", 90);
}

void ShotDeathrayOnDetected()
{
    CastSpellObjectObject("SPELL_DEATH_RAY", SELF, OTHER);
}

void HecubahAutoDeathray()
{
    float xVect = UnitRatioX(OTHER, SELF, 3.0), yVect = UnitRatioY(OTHER, SELF, 3.0);
    int detector = CreateObjectAt("WeirdlingBeast", GetObjectX(SELF) + xVect, GetObjectY(SELF) + yVect);

    UnitNoCollide(detector);
    SetOwner(SELF, detector);
    LookAtObject(detector, OTHER);

    SetCallback(detector, 3, ShotDeathrayOnDetected);
    DrawGeometryRingAt("HealOrb", GetObjectX(SELF), GetObjectY(SELF), 60);
    DeleteObjectTimer(detector, 1);
}

void LichlordLifeTimeManager(int subUnit)
{
    while (IsObjectOn(subUnit))
    {
        int creat = ToInt(GetObjectZ(subUnit));

        if (CurrentHealth(creat))
        {
            int lifetime = GetDirection(subUnit), owner = GetOwner(subUnit);

            if (CurrentHealth(owner))
            {
                if (lifetime)
                {
                    FrameTimerWithArg(3, subUnit, LichlordLifeTimeManager);
                    LookWithAngle(subUnit, --lifetime);
                    break;
                }
                else
                {
                    Damage(creat, 0, CurrentHealth(creat) + 1, 14);
                    DeleteObjectTimer(creat, 60);
                    DeleteObjectTimer(CreateObjectAt("MediumFlame", GetObjectX(creat), GetObjectY(creat)), 90);
                    PlaySoundAround(creat, 8);
                    Effect("SPARK_EXPLOSION", GetObjectX(creat), GetObjectY(creat), 0.0, 0.0);
                }
            }
        }
        Delete(subUnit);
        break;
    }
}

void HecubahSummonCompleted(int subUnit)
{
    int owner = GetOwner(subUnit), unit = CreateObjectAt("LichLord", GetObjectX(subUnit), GetObjectY(subUnit));

    LichLordSubProcess(unit);
    SetUnitScanRange(unit, 400.0);
    SetOwner(owner, unit);
    LichlordLifeTimeManager(subUnit);
    Raise(subUnit, unit);
}

void HecubahSummonLichLord()
{
    float xVect = UnitRatioX(OTHER, SELF, 33.0), yVect = UnitRatioY(OTHER, SELF, 33.0), gap = 0.1;
    int leftSub = CreateObjectAt(INVISIBLE_SUBUNIT, GetObjectX(SELF) + xVect - (yVect * gap), GetObjectY(SELF) + yVect + (xVect * gap));
    int rightSub = CreateObjectAt(INVISIBLE_SUBUNIT, GetObjectX(SELF) + xVect + (yVect * gap), GetObjectY(SELF) + yVect - (xVect * gap));

    LookWithAngle(leftSub, 60);
    LookWithAngle(rightSub, 60);
    SetOwner(SELF, leftSub);
    SetOwner(SELF, rightSub);
    FrameTimerWithArg(1, leftSub, HecubahSummonCompleted);
    FrameTimerWithArg(1, rightSub, HecubahSummonCompleted);

    DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(leftSub), GetObjectY(leftSub)), 12);
    DeleteObjectTimer(CreateObjectAt("MagicSpark", GetObjectX(rightSub), GetObjectY(rightSub)), 12);
    Effect("SMOKE_BLAST", GetObjectX(leftSub), GetObjectY(leftSub), 0.0, 0.0);
    Effect("SMOKE_BLAST", GetObjectX(rightSub), GetObjectY(rightSub), 0.0, 0.0);

    UniChatMessage(SELF, "저놈을 공격하라! 나의 제군들이여", 90);
}

void EmberDemonStrike()
{
    Damage(OTHER, SELF, 30, 1);
    DrawGeometryRingAt("HealOrb", GetObjectX(OTHER), GetObjectY(OTHER), 0);
    PushObjectTo(OTHER, UnitRatioX(OTHER, SELF, 19.0), UnitRatioY(OTHER, SELF, 19.0));
}

void SkullLordStrike()
{
    int subunit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(OTHER) + UnitRatioX(OTHER, SELF, 3.0), GetObjectY(OTHER) + UnitRatioY(OTHER, SELF, 3.0));

    SetOwner(SELF, subunit);
    Damage(OTHER, subunit, 40, 9);
    Effect("LIGHTNING", GetObjectX(OTHER), GetObjectY(OTHER), GetObjectX(OTHER), GetObjectY(OTHER) - 181.0);
    Delete(subunit);
}

void DemonLordStrike()
{
    DeleteObjectTimer(CreateObjectAt("MeteorExplode", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
}

void HecubahStrikeEvent()
{
    if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_PROTECT_FROM_MAGIC)))
    {
        int skills[] = {HecubahPunchSkill, HecubahSummonLichLord, HecubahAutoDeathray};

        CallFunction(skills[Random(0, 2)]);
        Enchant(SELF, EnchantList(ENCHANT_PROTECT_FROM_MAGIC), 7.0);
    }
}

void MonsterStrikeCallback()
{
    int thingId = GetUnitThingID(SELF);

    if (CurrentHealth(OTHER))
    {
        if (thingId == 1386)
            HorrendousElectricStrike();
        else if (thingId == 1329)
            FishBiteStrike();
        else if (thingId == 1337)
            EmberDemonStrike();
        else if (thingId == 1315)
            SkullLordStrike();
        else if (thingId == 1347)
            DemonLordStrike();
        else if (thingId == 1383)
            HecubahStrikeEvent();
    }
}

void UrchinStoneCollide()
{
    int owner = GetOwner(SELF);

    if (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, SELF, 30, 10);
            Effect("RICOCHET", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            Delete(SELF);
        }
    }
}

void SpiderWebMissileCollide()
{
    int owner = GetOwner(SELF);

    if (GetTrigger())
    {
        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, SELF, 35, 5);
            DeleteObjectTimer(CreateObjectAt("GreenSmoke", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
            Delete(SELF);
        }
    }
}

void LightningCollide()
{
    int owner = GetOwner(SELF);

    if (!GetTrigger())
        return;
    if (!owner)
        return;

    if (CurrentHealth(OTHER) && IsAttackedBy(owner, OTHER))
    {
        int damageSub = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) - UnitAngleCos(OTHER, 3.0), GetObjectY(OTHER) - UnitAngleSin(OTHER, 3.0));

        SetOwner(owner, damageSub);
        Damage(OTHER, damageSub, 100, 0);
        Delete(SELF);
    }
}

void UrchinStone(int missile, int owner)
{
    SetUnitCallbackOnCollide(missile, UrchinStoneCollide);
    Enchant(missile, EnchantList(21), 0.0);
}

void SpiderWebMissile(int missile, int owner)
{
    SetUnitCallbackOnCollide(missile, SpiderWebMissileCollide);
}

void HarpoonEvent(int missile, int owner)
{
    Enchant(missile, EnchantList(21), 0.0);
}
void onLightningBoltDetected(int cur, int owner){
    if (!UnitCheckEnchant(cur, GetLShift(ENCHANT_FREEZE)))
            SetUnitCallbackOnCollide(cur, LightningCollide);
}
static void IntroducedIndexLoopHashCondition(int pInstance){
    HashPushback(pInstance, OBJ_SPIDER_SPIT, SpiderWebMissile);
    HashPushback(pInstance, OBJ_THROWING_STONE, UrchinStone);
    HashPushback(pInstance, OBJ_HARPOON_BOLT, HarpoonEvent);
    HashPushback(pInstance, OBJ_LIGHTNING_BOLT, onLightningBoltDetected);
}

void RemoveMinesExitWalls()
{
    ObjectOff(self);

    int i;

    for (i = 0 ; i < 6 ; i += 1)
    {
        WallUtilOpenWallAtObjectPosition(71);
        TeleportLocationVector(71, 23.0, 23.0);
    }
    UniPrint(other, "우측 울타리가 낮아졌습니다");
}

string GeneratorUnitList(int index)
{
    string units[] = {
        "WizardGreen", "Bear", "Imp", "Horrendous",
        "Archer", "Wizard", "Mimic", "UrchinShaman", "Demon"
    };
    return units[index];
}

int CreateGenerator(int index, int location)
{
    string genName = GeneratorUnitList(index % 9) + "Generator";
    int gen = CreateObjectAt(genName, LocationX(location), LocationY(location));

    if (gen)
    {
        SetUnitMass(gen, 25.0);
        ObjectOff(gen);
        Enchant(gen, EnchantList(23), 0.0);
    }
    return gen;
}

string GetGeneratorUnitname(int index)
{
    return GeneratorUnitList(index % 9);
}

void AfterInitPuzzle()
{
    int i;

    for (i = 0 ; i < 9 ; i += 1)
    {
        int index = g_mixGenPuzzle[i];

        DummyUnitCreateByName(GetGeneratorUnitname(index), LocationX(77 + i), LocationY(77 + i));
        g_genPuzzleBeacon[index] = Object("PuzBeacon" + IntToString(i + 1));
    }
}

void GenPuzzleMix()
{
    int i, pic, max = PUZZLE_GEN_COUNT, temp;

    for (i = 0 ; i < max ; i += 1)
    {
        pic = Random(0, max - 1);
        if (i ^ pic)
        {
            temp = g_mixGenPuzzle[i];
            g_mixGenPuzzle[i] = g_mixGenPuzzle[pic];
            g_mixGenPuzzle[pic] = temp;
        }
    }
}

void InitializeGenerators()
{
    int gen = CreateObjectAt("ImaginaryCaster", LocationX(86), LocationY(86)), i;

    for (i = 0 ; i < PUZZLE_GEN_COUNT ; i += 1)
    {
        CreateGenerator(i, i + 86);
        g_mixGenPuzzle[i] = i;
    }
    GenPuzzleMix();
    GenPuzzleMix();
    GenPuzzleMix();

    g_puzzleGens = gen + 1;
    FrameTimer(1, AfterInitPuzzle);
}

int CheckPuzzleSolved()
{
    int i, result = 0;

    for (i = 0 ; i < 9 ; i += 1)
    {
        if (DistanceUnitToUnit(g_puzzleGens + i, g_genPuzzleBeacon[i]) < 46.0)
            result ++;
    }
    return result;
}

void RemoveSouthWalls()
{
    int i;

    for (i = 21 ; i ; i -= 1)
    {
        WallUtilOpenWallAtObjectPosition(95);
        TeleportLocationVector(95, -23.0, 23.0);
    }
}

void PuzzleGenHolding(int i)
{
    ObjectOff(g_genPuzzleBeacon[i]);
    MoveObject(g_puzzleGens + i, GetObjectX(g_genPuzzleBeacon[i]), GetObjectY(g_genPuzzleBeacon[i]));
    Frozen(g_puzzleGens + i, true);
}

void PuzzleSolved() //21, 95
{
    int i;

    for (i = 0 ; i < 9 ; i += 1)
        PuzzleGenHolding(i);
    FrameTimer(15, RemoveSouthWalls);
    Effect("WHITE_FLASH", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
    PlaySoundAround(self, 795);
    UniPrintToAll("모든 비밀이 풀렸습니다. 남쪽 벽이 개방되었습니다");
    ObjectOn(g_ternnelSwitch);
    EnchantOff(g_ternnelSwitch, EnchantList(0));
}

void PressedGenPuzzleBeacon()
{
    if (HasClass(other, "MONSTERGENERATOR"))
    {
        if (CheckPuzzleSolved() ^ PUZZLE_GEN_COUNT)
            Effect("YELLOW_SPARKS", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
        else
            PuzzleSolved();
    }
}

void ClearTernnelWalls()
{
    ObjectOff(self);

    int i;

    for (i = 11 ; i ; i -= 1)
    {
        WallUtilOpenWallAtObjectPosition(96);
        WallUtilOpenWallAtObjectPosition(97);
        TeleportLocationVector(96, -23.0, 23.0);
        TeleportLocationVector(97, -23.0, 23.0);
    }
}

void TestFunction()
{
    ObjectOff(self);
    // FrameTimer(30, EntryArea6);
}

void FrontBlockMovingToRight(int block)
{
    int remain = GetDirection(block);

    if (remain)
    {
        FrameTimerWithArg(1, block, FrontBlockMovingToRight);
        MoveObjectVector(block, 1.0, -1.0);
        LookWithAngle(block, --remain);
    }
}

void FrontBlockMovingToLeft(int block)
{
    int remain = GetDirection(block);

    if (remain)
    {
        FrameTimerWithArg(1, block, FrontBlockMovingToLeft);
        MoveObjectVector(block, -1.0, 1.0);
        LookWithAngle(block, --remain);
    }
}

void RemoveFrontBlockWalls()
{
    int i;

    for (i = 0 ; i < 5 ; i ++)
    {
        WallUtilOpenWallAtObjectPosition(118);
        TeleportLocationVector(118, -23.0, 23.0);
    }
}

void StartFrontBlocks()
{
    int blocks = g_frontBlocks;

    if (!IsObjectOn(blocks))
    {
        ObjectOn(blocks);
        FrameTimerWithArg(1, blocks, FrontBlockMovingToRight);
        FrameTimerWithArg(1, blocks + 1, FrontBlockMovingToRight);
        FrameTimerWithArg(1, blocks + 2, FrontBlockMovingToLeft);
        FrameTimerWithArg(1, blocks + 3, FrontBlockMovingToLeft);
        SecondTimer(3, RemoveFrontBlockWalls);
    }
}

void FrontBlockLever()
{
    int count;

    ObjectOff(self);
    if (++count == 2)
        StartFrontBlocks();
}

int InitSecondBlocks()
{
    int blocks = CreateObjectAt("IronBlock", LocationX(111), LocationY(111));

    Frozen(CreateObjectAt("IronBlock", LocationX(112), LocationY(112)), true);
    Frozen(CreateObjectAt("IronBlock", LocationX(113), LocationY(113)), true);
    Frozen(CreateObjectAt("IronBlock", LocationX(114), LocationY(114)), true);
    Frozen(blocks, true);
    int u;

    for (u = 0 ; u < 4 ; u += 1)
        LookWithAngle(blocks + u, 99);
    
    ObjectOff(blocks);

    return blocks;
}

void BackBlockMovingToRight(int block)
{
    int remain = GetDirection(block);

    if (remain)
    {
        FrameTimerWithArg(1, block, BackBlockMovingToRight);
        MoveObjectVector(block, 2.0, 2.0);
        LookWithAngle(block, --remain);
    }
}

void BackBlockMovingToLeft(int block)
{
    int remain = GetDirection(block);

    if (remain)
    {
        FrameTimerWithArg(1, block, BackBlockMovingToLeft);
        MoveObjectVector(block, -2.0, 2.0);
        LookWithAngle(block, --remain);
    }
}

void StartBackBlocks()
{
    int blocks = g_backBlocks;

    ObjectOff(self);
    if (!IsObjectOn(blocks))
    {
        FrameTimerWithArg(3, blocks, BackBlockMovingToRight);
        FrameTimerWithArg(3, blocks + 1, BackBlockMovingToRight);
        FrameTimerWithArg(3, blocks + 2, BackBlockMovingToRight);
    }
}

void StartBackBlocks2()
{
    int blocks = g_backBlocks + 3;

    ObjectOff(self);
    if (!IsObjectOn(blocks))
    {
        FrameTimerWithArg(3, blocks, BackBlockMovingToLeft);
        FrameTimerWithArg(3, blocks + 1, BackBlockMovingToLeft);
        FrameTimerWithArg(3, blocks + 2, BackBlockMovingToLeft);
    }
}

int InitBackBlocks()
{
    int blocks = CreateObjectAt("SpikeBlock", LocationX(115), LocationY(115));

    CreateObjectAt("SpikeBlock", LocationX(116), LocationY(116));
    CreateObjectAt("SpikeBlock", LocationX(117), LocationY(117));

    ObjectOff(CreateObjectAt("SpikeBlock", LocationX(119), LocationY(119)));
    CreateObjectAt("SpikeBlock", LocationX(120), LocationY(120));
    CreateObjectAt("SpikeBlock", LocationX(121), LocationY(121));
    ObjectOff(blocks);

    int u;
    for (u = 0 ; u < 6 ; u += 1)
        LookWithAngle(blocks + u, 162);

    return blocks;
}

void InitLastPart()
{
    g_frontBlocks = InitSecondBlocks();
    g_backBlocks = InitBackBlocks();
}

void EndArea9()
{
    FrameTimer(1, EntryAreaA);

    UnlockDoor(Object("Area9Gate"));
    UnlockDoor(Object("Area9Gate1"));

    InitPlacingGenerator();
}

void EntryArea9()
{
    //Todo.
    MobMakerStartSummon(1, PlaceMobMaker(122, 20, SummonBeast));
    MobMakerStartSummon(1, PlaceMobMaker(129, 20, SummonBeast));
    MobMakerStartSummon(1, PlaceMobMaker(130, 20, SummonBeast));
    MobMakerStartSummon(1, PlaceMobMaker(123, 20, SummonMobWasp));
    MobMakerStartSummon(1, PlaceMobMaker(124, 20, SummonMobWasp));
    MobMakerStartSummon(1, PlaceMobMaker(131, 20, SummonMobBlackSpider));

    SetNextFunction(EndArea9);
}

void EndArea8()
{
    FrameTimer(1, EntryArea9);

    int u;

    for (u = 0 ; u < 3 ; u += 1)
    {
        UnlockDoor(Object("A6Gate" + IntToString(u + 1)));
        UnlockDoor(Object("A6Gate" + IntToString(u + 1) + "1"));
    }
    UniPrintToAll("출입문의 잠금이 해제되었습니다");
}

void EntryArea8()
{
    MobMakerStartSummon(1, PlaceMobMaker(109, 30, SummonMobStrongWizard));
    MobMakerStartSummon(1, PlaceMobMaker(108, 30, SummonMobPlant));
    MobMakerStartSummon(1, PlaceMobMaker(128, 10, SummonMobLich));

    SetNextFunction(EndArea8);
}

void EndAreaA()
{ }

void EntryAreaA()
{
    SetNextFunction(EndAreaA);

    MobMakerStartSummon(1, PlaceMobMaker(137, 14, SummonMobEmberDemon));
    MobMakerStartSummon(1, PlaceMobMaker(138, 14, SummonMobSkullLord));
    MobMakerStartSummon(1, PlaceMobMaker(139, 8, SummonMobLich));
}

void RemoveArea9EntryWalls()
{
    ObjectOff(self);

    WallUtilOpenWallAtObjectPosition(133);
    WallUtilOpenWallAtObjectPosition(134);
    int u;

    for (u = 0 ; u < 6 ; u += 1)
    {
        WallUtilOpenWallAtObjectPosition(132);
        TeleportLocationVector(132, -23.0, 23.0);
    }
}

void RemoveBackBlockWalls()
{
    int u;

    for (u = 0 ; u < 6 ; u += 1)
    {
        if (u < 4)
        {
            WallUtilOpenWallAtObjectPosition(125);
            TeleportLocationVector(125, -23.0, 23.0);
            if (u < 3)
            {
                WallUtilOpenWallAtObjectPosition(127);
                TeleportLocationVector(127, 23.0, -23.0);
            }
        }
        WallUtilOpenWallAtObjectPosition(126);
        TeleportLocationVector(126, 23.0, 23.0);
    }
}

void BackBlockClearWallTriggered()
{
    int alreadyExec;

    ObjectOff(SELF);
    if (alreadyExec == false)
    {
        RemoveBackBlockWalls();
        alreadyExec = true;
    }
}

int FireTrapReference(int *st, int *trap, int assign)
{
    int *tst, *ttrap;

    if (assign)
    {
        tst = st;
        ttrap = trap;
    }
    else
    {
        if (tst != NULLPTR && ttrap != NULLPTR)
        {
            st[0] = tst;
            trap[0] = ttrap;
            return true;
        }
    }
    return false;
}

void InitFireTraps()
{
    int statues[] = {
        CreateObjectAt("MovableStatueVictory3SW", LocationX(135), LocationY(135)), CreateObjectAt("MovableStatueVictory3NE", LocationX(136), LocationY(136))};
    int traps[] = {CreateObjectAtUnit("Skull1", statues[0]), CreateObjectAtUnit("Skull1", statues[1])};

    UnitNoCollide(statues[0]);
    UnitNoCollide(statues[1]);
    ObjectOff(traps[0]);
    ObjectOff(traps[1]);

    FireTrapReference(&statues, &traps, true);
}

void DisableObject(int unit)
{
    if (IsObjectOn(unit))
        ObjectOff(unit);
}

void FireballStatueFx(int *pack)
{
    int *time = pack[1];

    if (time[0] > 0)
    {
        --time[0];
        FrameTimerWithArg(3, pack, FireballStatueFx);
    }
    else
    {
        int *statue = pack[0];

        EnchantOff(statue[0], EnchantList(ENCHANT_PROTECT_FROM_FIRE));
        EnchantOff(statue[1], EnchantList(ENCHANT_PROTECT_FROM_FIRE));
    }
}

void LaunchFireballFromTrap()
{
    int *statue = NULLPTR, *trap = NULLPTR;
    
    if (!FireTrapReference(&statue, &trap, false))
        return;

    int u, time;

    for (u = 0 ; u < 2 ; ++u)
    {
        ObjectOff(trap[u]);
        LookAtObject(trap[u], OTHER);
        ObjectOn(trap[u]);
        FrameTimerWithArg(1, trap[u], DisableObject);
    }

    int pack[] = {statue, &time};

    if (!time)
    {
        Enchant(statue[0], EnchantList(ENCHANT_PROTECT_FROM_FIRE), 0.0);
        Enchant(statue[1], EnchantList(ENCHANT_PROTECT_FROM_FIRE), 0.0);
        FrameTimerWithArg(1, &pack, FireballStatueFx);
    }
    time = 30;
}

int GeneratorUnitCreate(string unitType, int hpAmount, int subunit)
{
    int mob = CreateObjectAt(unitType, GetObjectX(subunit), GetObjectY(subunit));

    SetUnitMaxHealth(mob, hpAmount);
    Enchant(mob, EnchantList(ENCHANT_ANCHORED), 0.0);
    return mob;
}

void MonsterObeliskOnDestroy()
{
    int subunit = GetTrigger() + 1;

    if (IsObjectOn(subunit))
    {
        int gentype = GetDirection(subunit), u;
        int amount = ObeliskTypeAmount(gentype);
        int hpamount = ObeliskTypeHitPoint(gentype);
        string unitType = ObeliskTypes(gentype);

        for (u = 0 ; u < amount ; u += 1)
            GeneratorUnitCreate(unitType, hpamount, subunit);
        DeleteObjectTimer(CreateObjectAt("Explosion", GetObjectX(subunit), GetObjectY(subunit)), 9);
        Delete(subunit);
    }
    Delete(SELF);
}

string ObeliskTypes(int index)
{
    string types[MON_GEN_MAX] =
    {
        "Horrendous", "Skeleton", "Wizard", "WizardGreen",
        "EmberDemon", "Troll", "Mimic", "Beholder",
        "Swordsman", "EvilCherub", "Bear", "UrchinShaman",
        "StoneGolem", "MechanicalGolem", "Demon", "OgreBrute"
    };
    return types[index];
}

int ObeliskTypeHitPoint(int index)
{
    int hptable[MON_GEN_MAX] =
    {
        600, 225, 275, 225,
        225, 360, 800, 325,
        325, 192, 421, 198,
        1200, 1300, 1080, 295
    };
    return hptable[index];
}

int ObeliskTypeAmount(int index)
{
    int amountTable[MON_GEN_MAX] =
    {
        2, 8, 4, 4,
        4, 4, 2, 4,
        16, 32, 4, 4,
        2, 2, 2, 8
    };
    return amountTable[index];
}

int PlaceMonsterObelisk(int location, int type)
{
    int oblisk = CreateObjectAt(ObeliskTypes(type) + "Generator", LocationX(location), LocationY(location));

    LookWithAngle(CreateObjectAt("AmbBeachBirds", LocationX(location), LocationY(location)), type);
    SetUnitMaxHealth(oblisk, 744);
    ObjectOff(oblisk);
    SetUnitCallbackOnDeath(oblisk, MonsterObeliskOnDestroy);

    return oblisk;
}

void InitPlacingGenerator()
{
    int u;

    for (u = 0 ; u < MON_GEN_MAX ; u += 1)
        PlaceMonsterObelisk(140 + u, u);
}

void OnClearGen()
{
    int count, complete;

    ObjectOff(SELF);
    if ((++count) >= MON_GEN_MAX)
    {
        if (complete)
            return;
        complete = true;
        int u;

        for (u = 0 ; u < 5 ; u += 1)
        {
            WallUtilOpenWallAtObjectPosition(156);
            TeleportLocationVector(156, 23.0, 23.0);
        }
        UniPrintToAll("다음 구간으로 향하는 벽이 열렸습니다");
        FrameTimer(30, SwampLavaMonsters);
    }
}

void SwampLavaMonsters()
{
    SetNextFunction(EndAreaDefault);
    MobMakerStartSummon(0, PlaceMobMaker(158, 16, SummonMobPlant));
    MobMakerStartSummon(2, PlaceMobMaker(159, 20, SummonMobGoon));
    MobMakerStartSummon(0, PlaceMobMaker(160, 16, SummonMobPlant));
    MobMakerStartSummon(2, PlaceMobMaker(161, 8, SummonWoundedWiz));
    MobMakerStartSummon(2, PlaceMobMaker(162, 20, SummonMobGoon));

    MobMakerStartSummon(3, PlaceMobMaker(163, 20, SummonMobEmberDemon));
    MobMakerStartSummon(4, PlaceMobMaker(164, 20, SummonMobFireFairy));
    MobMakerStartSummon(4, PlaceMobMaker(165, 10, SummonMobFireFairy));
    MobMakerStartSummon(3, PlaceMobMaker(166, 20, SummonMobEmberDemon));
    MobMakerStartSummon(3, PlaceMobMaker(167, 20, SummonMobEmberDemon));
}

void TeleportProgress2(int subUnit)
{
    while (IsObjectOn(subUnit))
    {
        int owner = GetOwner(subUnit);

        if (CurrentHealth(owner))
        {
            int count = GetDirection(subUnit), destination = ToInt(GetObjectZ(subUnit));

            if (count)
            {
                if (DistanceUnitToUnit(subUnit, owner) < 23.0)
                {
                    LookWithAngle(subUnit, --count);
                    FrameTimerWithArg(1, subUnit, TeleportProgress2);
                    break;
                }
            }
            else if (IsObjectOn(destination))
            {
                Effect("TELEPORT", GetObjectX(subUnit), GetObjectY(subUnit), 0.0, 0.0);
                Effect("SMOKE_BLAST", GetObjectX(subUnit), GetObjectY(subUnit), 0.0, 0.0);
                MoveObject(owner, GetObjectX(destination), GetObjectY(destination));
                PlaySoundAround(owner, 6);
                Effect("TELEPORT", GetObjectX(owner), GetObjectY(owner), 0.0, 0.0);
            }
            EnchantOff(owner, "ENCHANT_BURNING");
        }
        Delete(subUnit);
        Delete(++subUnit);
        break;
    }
}

void TeleportPortal()
{
    if (CurrentHealth(other) && IsObjectOn(other))
    {
        if (!HasEnchant(other, "ENCHANT_BURNING"))
        {
            Enchant(other, "ENCHANT_BURNING", 4.0);
            int unit = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));

            Raise(unit, GetTrigger() + 1);
            LookWithAngle(unit, 48); //TODO: 1.XX seconds...
            CreateObjectAt("VortexSource", GetObjectX(unit), GetObjectY(unit));
            Effect("YELLOW_SPARKS", GetObjectX(unit), GetObjectY(unit), 0.0, 0.0);
            SetOwner(other, unit);
            TeleportProgress2(unit);
            PlaySoundAround(unit, 772);
            UniPrint(other, "공간이동을 준비 중 입니다. 취소하려면 움직이세요");
        }
    }
}

int TeleportSetup(int srcWp, int dstWp)
{
    int unit = CreateObject("WeirdlingBeast", srcWp);

    SetUnitMaxHealth(CreateObject("InvisibleLightBlueHigh", dstWp) - 1, 10);
    Enchant(CreateObject("InvisibleLightBlueHigh", srcWp), "ENCHANT_ANCHORED", 0.0);

    int teleportFx = CreateObject("TeleportWake", srcWp);

    UnitNoCollide(teleportFx);
    Frozen(teleportFx, TRUE);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    SetCallback(unit, 9, TeleportPortal);

    return unit;
}

void InitTeleports()
{
    TeleportSetup(168, 171);
    TeleportSetup(169, 172);
    TeleportSetup(170, 173);

    TeleportSetup(200, 203);
    TeleportSetup(201, 204);
    TeleportSetup(202, 205);
}

void LichStatueCollide()
{
    Damage(OTHER, SELF, 2, 14);
}

int LichStatueSetting(int objId, int startLocation)
{
    if (IsObjectOn(objId))
    {
        SetUnitCallbackOnCollide(objId, LichStatueCollide);
        Move(objId, startLocation);
    }
    return objId;
}

void StartLichStatueMoving()
{
    LichStatueSetting(Object("lotdst1"), 186);
    LichStatueSetting(Object("lotdst2"), 189);
    LichStatueSetting(Object("lotdst3"), 190);

    LichStatueSetting(Object("lotdst4"), 192);
    LichStatueSetting(Object("lotdst5"), 195);
    LichStatueSetting(Object("lotdst6"), 197);
}

void NecroOnDeath()
{
    if (--s_necroUnitCount == 0)
    {
        int u;

        for (u = 0 ; u < 10 ; u += 1)
        {
            WallUtilOpenWallAtObjectPosition(185);
            TeleportLocationVector(185, -23.0, -23.0);
        }
        FrameTimer(60, StartLichStatueMoving);

        int o;

        for (o = 0 ; o < 20 ; o+=1)
        {
            int lich = CreateObjectAt("LichLord", LocationX(199), LocationY(199));

            LichLordSubProcess(lich);
        }
    }
}

void SurpriseNecros()
{
    int u, baseLocation = 174, centering = CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(184), LocationY(184));
    int max = 10;

    s_necroUnitCount += max;
    for (u = 0 ; u < max ; u += 1)
    {
        int necro = SummonNecromancer(u + baseLocation);

        SetCallback(necro, 5, NecroOnDeath);
        LookAtObject(necro, centering);
        CastSpellObjectObject("SPELL_INVISIBILITY", necro, necro);
        Effect("SMOKE_BLAST", GetObjectX(necro), GetObjectY(necro), 0.0, 0.0);
        UniChatMessage(necro, "으헤헤..", 100);
    }
    Delete(centering);
}

void SurpriseNecrosInPlace()
{
    int checker;

    ObjectOff(SELF);
    if (checker == FALSE)
    {
        checker = TRUE;
        SurpriseNecros();
    }
}

void OpenLotdWalls()
{
    int u;

    for (u = 0 ; u < 8 ; u += 1)
    {
        WallUtilOpenWallAtObjectPosition(198);
        TeleportLocationVector(198, -23.0, -23.0);
    }
}

void LeftRightLotdLeverCheck()
{
    int count;

    if (++count == 2)
    {
        OpenLotdWalls();
        UniPrintToAll("전방에 있던 비밀의 벽이 사라집니다");
    }
}

void LeftLotdLever()
{
    ObjectOff(SELF);
    LeftRightLotdLeverCheck();
}

void RightLotdLever()
{
    ObjectOff(SELF);
    LeftRightLotdLeverCheck();
}

void DemonOnLostEnemy()
{
    Wander(SELF);
    AggressionLevel(SELF, 1.0);
}

void DelayCreatureJunkYardDog(int creat)
{
    if (CurrentHealth(creat))
    {
        Wander(creat);
        AggressionLevel(creat, 1.0);
    }
}

void TeleportAllPlayerToLastPosition(int baselocationId)
{
    int u;

    for (u = 0 ; u < 10 ; u += 1)
    {
        if (CurrentHealth(player[u]))
            MoveObject(player[u], LocationX(baselocationId) + MathSine((u * 36) + 90, 60.0), LocationY(baselocationId) + MathSine(u * 36, 60.0));
    }
}

void VictoryEvent(int soundPosUnit)
{
    PlaySoundAround(soundPosUnit, 914);
    UniPrintToAll("승.리.");
    UniPrintToAll("마침내 보스가 죽었습니다. 이 게임을 플레이 해주셔서 대단히 감사합니다 =)");
    Effect("WHITE_FLASH", GetObjectX(soundPosUnit), GetObjectY(soundPosUnit), 0.0, 0.0);
}

void FinalBossDeath()
{
    MoveObject(SELF, LocationX(211), LocationY(211));
    TeleportAllPlayerToLastPosition(211);
    UniChatMessage(SELF, "내가 죽다니... 보스 죽음 TT", 150);
    FrameTimerWithArg(50, CreateObjectAt("BlueRain", GetObjectX(SELF), GetObjectY(SELF)), VictoryEvent);
    PlaySoundAround(SELF, 591);
}

void SummonFinalBoss()
{
    int respLocation = Random(206, 236);
    int mob = CreateObjectAt("Hecubah", LocationX(respLocation), LocationY(respLocation));

    HecubahSubProcess(mob);
    SetUnitScanRange(mob, 600.0);
    FrameTimerWithArg(1, mob, DelayCreatureJunkYardDog);
    SetCallback(mob, 5, FinalBossDeath);
    SetCallback(mob, 13, DemonOnLostEnemy);
    CastSpellObjectObject("SPELL_TURN_UNDEAD", mob, mob);
}

void FinalDemonOnDeath()
{
    if (--s_finalDemonsCount == 0)
    {
        SummonFinalBoss();
        UniPrintToAll("방금 \"최종 보스\"가 맵에 등장했습니다..!");
    }
    else if (s_finalDemonsCount < 0)
        s_finalDemonsCount = 0;
}

void DemonsJunkYardDog(int units)
{
    int u;

    for (u = 0 ; u < 12 ; u+=1)
    {
        if (CurrentHealth(units + u))
            DelayCreatureJunkYardDog(units + u);
    }
}

void SummonFinalMonsters()
{
    //location_id. 237~248
    int u, locationBase = 237;
    int baseUnit = CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(locationBase), LocationY(locationBase));

    Delete(baseUnit);
    int maxCount = 12;

    for (u = 0 ; u < maxCount ; u+=1)
    {
        int finalMons = CreateObjectAt("Demon", LocationX(locationBase), LocationY(locationBase));

        DemonSubProcess(finalMons);
        SetCallback(finalMons, 13, DemonOnLostEnemy);
        SetCallback(finalMons, 5, FinalDemonOnDeath);
        RegistUnitStrikeHook(finalMons);
    }
    s_finalDemonsCount = maxCount;
    FrameTimerWithArg(1, ++baseUnit, DemonsJunkYardDog);
}

void EntranceFinalZone()
{
    int alreadChecked;

    ObjectOff(SELF);
    if (alreadChecked == FALSE)
    {
        alreadChecked = TRUE;
        UniPrintToAll("최후의 장소에 온 것을 환영한단다. 바로 이곳이 너의 무덤이기도 할 것이다");
    }
}

void SavePositionCollide()
{
    if (MaxHealth(SELF))
    {
        if (IsPlayerUnit(OTHER))
        {
            TeleportLocation(PLAYER_TELEPORT_LOCATION, GetObjectX(SELF), GetObjectY(SELF));
            int fxChange = CreateObjectAt("MagicMissile", GetObjectX(SELF), GetObjectY(SELF));

            Frozen(fxChange, true);
            SetUnitSubclass(fxChange, GetUnitSubclass(fxChange) ^ 1);
            UniPrint(OTHER, "현재 위치가 저장되었습니다");
            PlaySoundAround(SELF, 1005);
            Effect("YELLOW_SPARKS", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
            Delete(SELF);
            Delete(GetTrigger() + 1);
        }
    }
}

void PlacePositionSavePoint(int locationId)
{
    int beacon = DummyUnitCreateById(OBJ_BOMBER, LocationX(locationId), LocationY(locationId));
    int fxUnit = CreateObjectAt("Magic", GetObjectX(beacon), GetObjectY(beacon));

    SetCallback(beacon, 9, SavePositionCollide);
    Frozen(fxUnit, true);
    SetUnitSubclass(fxUnit, GetUnitSubclass(fxUnit) ^ 1);
}

void InitSavePoint()
{
    PlacePositionSavePoint(249);
    PlacePositionSavePoint(250);
    PlacePositionSavePoint(251);
    PlacePositionSavePoint(252);
    PlacePositionSavePoint(253);
    PlacePositionSavePoint(254);
    PlacePositionSavePoint(255);
}

void MonHandlerPolypCollideNothing()
{ }

void MonHandlerPolypDeath()
{ }

void PolypReviveTimer(int polyp)
{
    while (MaxHealth(polyp))
    {
        int remain = GetDirection(polyp);

        if (CurrentHealth(polyp))
        {
            if (remain)
            {
                FrameTimerWithArg(2, polyp, PolypReviveTimer);
                LookWithAngle(polyp, --remain);
                break;
            }
            else
            {
                Effect("LESSER_EXPLOSION", GetObjectX(polyp), GetObjectY(polyp), 0.0, 0.0);
                Effect("DAMAGE_POOF", GetObjectX(polyp), GetObjectY(polyp), 0.0, 0.0);
                SummonNecromancerAtUnit(polyp);
            }
            
        }
        Delete(polyp);
        break;
    }
}

int SpawnNecromancerBabyEgg(int posUnit)
{
    int egg = CreateObjectAt("Polyp", GetObjectX(SELF), GetObjectY(SELF));

    SetUnitMass(egg, 20.0);
    SetUnitMaxHealth(egg, 225);
    LookWithAngle(egg, 225);
    SetUnitCallbackOnCollide(egg, MonHandlerPolypCollideNothing);
    SetUnitCallbackOnDeath(egg, MonHandlerPolypDeath);
    FrameTimerWithArg(2, egg, PolypReviveTimer);

    return egg;
}

void MonHandlerNecromancerOnDeath()
{
    UnitNoCollide(SELF);
    SpawnNecromancerBabyEgg(SELF);
    SpawnNecromancerBabyEgg(SELF);
    DeleteObjectTimer(SELF, 30);
}


void SummonNecromancerAtUnit(int subUnit)
{
    int necro = CreateObjectAt("Necromancer", GetObjectX(subUnit), GetObjectY(subUnit));

    NecromancerSubProcess(necro);
    SetCallback(necro, 5, MonHandlerNecromancerOnDeath);
    SetUnitScanRange(necro, 400.0);
}

void SummonNecro2(int spawnMarks)
{
    while (IsObjectOn(spawnMarks))
    {
        int canEscape = GetDirection(spawnMarks);

        SummonNecromancerAtUnit(spawnMarks);
        Delete(spawnMarks);
        if (canEscape)
            break;
        spawnMarks += 1;
    }
}

void SurpriseNecro2()
{
    int alreadyExec;

    ObjectOff(SELF);
    if (alreadyExec == FALSE)
    {
        alreadyExec = TRUE;

        int spawnMarks = CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(256), LocationY(256));

        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(257), LocationY(257));
        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(258), LocationY(258));
        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(259), LocationY(259));
        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(260), LocationY(260));
        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(261), LocationY(261));
        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(262), LocationY(262));
        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(263), LocationY(263));
        CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(264), LocationY(264));
        LookWithAngle(CreateObjectAt(INVISIBLE_SUBUNIT, LocationX(265), LocationY(265)), 1);
        FrameTimerWithArg(3, spawnMarks, SummonNecro2);

        LibraryDeco();
    }
}

void LibrarySingleWallOpen()
{
    WallUtilOpenWallAtObjectPosition(268);
}

void StartLibraryBlockMoving()
{
    ObjectOff(SELF);

    Move(Object("pbt1"), 266);
    Move(Object("pbt11"), 267);
    FrameTimer(60, LibrarySingleWallOpen);
}

void OpenUpWalls()
{
    int u, baseLocationId = 269;

    for (u = 0 ; u < 5 ; u += 1)
        WallUtilOpenWallAtObjectPosition(baseLocationId + u);
}

void CloseUpWalls()
{
    int u, baseLocationId = 269;

    for (u = 0 ; u < 5 ; u += 1)
        WallUtilCloseWallAtObjectPosition(baseLocationId + u);
}

void LibraryDeco()
{
    int u;

    for (u = 0 ; u < 12 ; u +=1)
    {
        CreateObjectAt("MovableBookcase1", LocationX(274), LocationY(274));
        Effect("LESSER_EXPLOSION", LocationX(274), LocationY(274), 0.0, 0.0);
        TeleportLocationVector(274, -27.0, 27.0);
    }
}

void FireTrapJustDeco()
{
    int deco1 = Object("justFireDec1"), deco2 = Object("justFireDec11");

    ObjectOff(deco1);
    LookWithAngle(deco1, 160);
    ObjectOn(deco1);

    ObjectOff(deco2);
    LookWithAngle(deco2, 160);
    ObjectOn(deco2);
}

void SwampSecretWall()
{
    ObjectOff(SELF);
    WallUtilOpenWallAtObjectPosition(282);
    WallUtilOpenWallAtObjectPosition(283);
}

