
#include "a_utils.h"
#include "libs/mathlab.h"
#include "libs/voiceList.h"
#include "libs/hash.h"

void MagicMissileCollide()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
    {
        Damage(OTHER, owner, 60, 0);
        if (CurrentHealth(OTHER))
            Enchant(OTHER, "ENCHANT_SLOWED", 2.0);
        Effect("LESSER_EXPLOSION", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Delete(SELF);
    }
    else if (!GetCaller())
        Delete(SELF);
}

void CastSingleMagicMissile(int sCaster, int sTarget)
{
    int ptr, misUnit;

    CastSpellObjectLocation("SPELL_MAGIC_MISSILE", sCaster, GetObjectX(sTarget), GetObjectY(sTarget));
    ptr = GetMemory(0x750710);
    if (ptr)
    {
        misUnit = GetMemory(ptr + 0x2c);
        Delete(misUnit);
        Delete(misUnit - 1);
        Delete(misUnit - 2);
        SetUnitCallbackOnCollide(misUnit - 3, MagicMissileCollide);
    }
}

int MobClassDemon(int sUnit)
{
    int mob = CreateObjectAt("Demon", GetObjectX(sUnit), GetObjectY(sUnit));

    DemonSubProcess(mob);
    return mob;
}

int MobClassJandor(int sUnit)
{
    int mob = CreateObjectById(OBJ_AIRSHIP_CAPTAIN, GetObjectX(sUnit), GetObjectY(sUnit));

    AirshipCaptainSubProcess(mob);
    SetUnitVoice(mob,MONSTER_VOICE__Maiden2);
    return mob;
}

int MobClassBeast(int sUnit)
{
    int mob = CreateObjectAt("WeirdlingBeast", GetObjectX(sUnit), GetObjectY(sUnit));

    WeirdlingBeastSubProcess(mob);
    return mob;
}

int GiantLeechBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1851877703; arr[1] = 1701137524; arr[2] = 26723; arr[17] = 270; arr[19] = 91; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1065353216; arr[25] = 1; arr[26] = 1; 
		arr[27] = 5; arr[28] = 1108082688; arr[29] = 40; arr[31] = 11; arr[32] = 13; 
		arr[33] = 21; arr[59] = 5542784; arr[60] = 1358; arr[61] = 46895696; 
	pArr = arr;
	return pArr;
}

void GiantLeechSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1076803666;		ptr[137] = 1076803666;
	int *hpTable = ptr[139];
	hpTable[0] = 270;	hpTable[1] = 270;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = GiantLeechBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int MobClassGiantLeech(int sUnit)
{
    int mob=CreateObjectById(OBJ_GIANT_LEECH,GetObjectX(sUnit),GetObjectY(sUnit));

    GiantLeechSubProcess(mob);
    return mob;
}

int MobClassHecubah(int sUnit)
{
    int mob = CreateObjectAt("Hecubah", GetObjectX(sUnit), GetObjectY(sUnit));

    HecubahSubProcess(mob);
    return mob;
}

int MobClassBomber(int sUnit)
{
    short bombName[] = {OBJ_BOMBER,OBJ_BOMBER_BLUE,OBJ_BOMBER_GREEN,OBJ_BOMBER_YELLOW};
    int mob = CreateObjectById(bombName[Random(0,3)], GetObjectX(sUnit), GetObjectY(sUnit));

    BomberSetMonsterCollide(mob);
    BomberSubProcess(mob);
    return mob;
}

int MobClassSwordsman(int sUnit)
{
    int mob = CreateObjectAt("Swordsman", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 160);
    return mob;
}

int MobClassArcher(int sUnit)
{
    int mob = CreateObjectAt("Archer", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 96);
    return mob;
}

int MobClassFrog(int sUnit)
{
    int mob = CreateObjectAt("GreenFrog", GetObjectX(sUnit), GetObjectY(sUnit));

    GreenFrogSubProcess(mob);
    return mob;
}

int MobClassWolf(int sUnit)
{
    int mob = CreateObjectAt("Wolf", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 135);
    return mob;
}

int MobClassWhiteWolf(int sUnit)
{
    int mob = CreateObjectAt("WhiteWolf", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 160);
    return mob;
}

int MobClassBlackWolf(int sUnit)
{
    int mob = CreateObjectAt("BlackWolf", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 200);
    return mob;
}

int MobClassGoon(int sUnit)
{
    int mob = CreateObjectById(OBJ_GOON, GetObjectX(sUnit), GetObjectY(sUnit));

    GoonSubProcess(mob);
    SetUnitVoice(mob,MONSTER_VOICE_Mimic);
    return mob;
}

int MobClassEmberDemon(int sUnit)
{
    short uName[] = {OBJ_MELEE_DEMON,OBJ_EMBER_DEMON,OBJ_MELEE_DEMON,};
    int mob = CreateObjectById(uName[Random(0, 2)], GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 185);
    return mob;
}

int MobClassFireSprite(int sUnit)
{
    int mob = CreateObjectAt("FireSprite", GetObjectX(sUnit), GetObjectY(sUnit));

    FireSpriteSubProcess(mob);
    return mob;
}

int MobClassHorrendous(int sUnit)
{
    int mob = CreateObjectAt("Horrendous", GetObjectX(sUnit), GetObjectY(sUnit));

    HorrendousSubProcess(mob);
    return mob;
}

int MobClassSkeletonLord(int sUnit)
{
    int mob = CreateObjectAt("SkeletonLord", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 275);
    return mob;
}

int MobClassCrazyGirl(int sUnit)
{
    int mob = ColorMaidenAt(35, 27, 31, GetObjectX(sUnit), GetObjectY(sUnit));

    MaidenSubProcess(mob);
    return mob;
}

int MobClassSpider(int sUnit)
{
    int mob = CreateObjectAt("BlackWidow", GetObjectX(sUnit), GetObjectY(sUnit));

    BlackWidowSubProcess(mob);
    return mob;
}

int MobClassOgreLord(int sUnit)
{
    int mob = CreateObjectAt("OgreWarlord", GetObjectX(sUnit), GetObjectY(sUnit));

    SetUnitMaxHealth(mob, 325);
    return mob;
}

int MobClassNecromancer(int sUnit)
{
    int mob = CreateObjectAt("Necromancer", GetObjectX(sUnit), GetObjectY(sUnit));

    NecromancerSubProcess(mob);
    return mob;
}

int MobClassLichLord(int sUnit)
{
    int mob = CreateObjectAt("LichLord", GetObjectX(sUnit), GetObjectY(sUnit));

    LichLordSubProcess(mob);
    return mob;
}

int MobClassOrbHecubah(int sUnit)
{
    int mob = CreateObjectAt("HecubahWithOrb", GetObjectX(sUnit), GetObjectY(sUnit));

    HecubahWithOrbSubProcess(mob);
    return mob;
}

int MobClassMecaGolem(int sUnit)
{
    int mob = CreateObjectAt("MechanicalGolem", GetObjectX(sUnit), GetObjectY(sUnit));

    MechanicalGolemSubProcess(mob);
    return mob;
}

int MobClassPlant(int sUnit)
{
    int mob = CreateObjectAt("CarnivorousPlant", GetObjectX(sUnit), GetObjectY(sUnit));

    CarnivorousPlantSubProcess(mob);
    return mob;
}

void RedWizardDetectedEnemy()
{
    int cFps = GetMemory(0x84ea04), caster;

    if (ABS(cFps - GetUnit1C(SELF)) > 24)
    {
        SetUnit1C(SELF, cFps);
        caster = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0));

        SetOwner(SELF, caster);
        CastSingleMagicMissile(caster, OTHER);
        Raise(GetTrigger() + 1, GetCaller());
        Enchant(SELF, "ENCHANT_BLINDED", 0.0);
        DeleteObjectTimer(caster, 90);
    }
}

void RedWizardLostEnemy()
{
    int enemy = ToInt(GetObjectZ(GetTrigger() + 1));

    EnchantOff(SELF, "ENCHANT_BLINDED");
    if (CurrentHealth(enemy))
    {
        LookAtObject(SELF, enemy);
    }
}

int MobClassRedWiz(int sUnit)
{
    int mob = CreateObjectAt("WizardRed", GetObjectX(sUnit), GetObjectY(sUnit));

    WizardRedSubProcess(mob);
    SetCallback(mob, 3, RedWizardDetectedEnemy);
    SetCallback(mob, 13, RedWizardLostEnemy);
    return mob;
}

void mobMecaGolem(int g)
{
    SetUnitMaxHealth(g,800);
}
void mobMinion(int g)
{
    SetUnitMaxHealth(g,MaxHealth(g)*3);
    UnitZeroFleeRange(g);
}
void mobMedium(int g)
{
    SetUnitMaxHealth(g,200);
}
void mobHard(int g)
{
    SetUnitMaxHealth(g,350);
}
void mobKillerian(int g)
{
    SetUnitMaxHealth(g,600);
}

void initMonsterSpecHash(int hash)
{
    HashPushback(hash,OBJ_MECHANICAL_GOLEM,mobMecaGolem);
    HashPushback(hash,OBJ_URCHIN,mobMinion);
    HashPushback(hash,OBJ_BAT,mobMinion);
    HashPushback(hash,OBJ_SKELETON_LORD,mobHard);
    HashPushback(hash,OBJ_OGRE_BRUTE,mobMedium);
    HashPushback(hash,OBJ_OGRE_WARLORD,mobHard);
    HashPushback(hash,OBJ_GHOST,mobMedium);
    HashPushback(hash,OBJ_EMBER_DEMON,mobMedium);
    HashPushback(hash,OBJ_SHADE,mobMedium);
    HashPushback(hash,OBJ_SCORPION,mobMedium);
    HashPushback(hash,OBJ_SPITTING_SPIDER,mobMedium);
    HashPushback(hash,OBJ_DEMON,mobKillerian);
}

int MonHash()
{
    int hash;
    if (!hash) {
        HashCreateInstance(&hash);
        initMonsterSpecHash(hash);
    }
    return hash;
}

