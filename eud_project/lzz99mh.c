
#include "lzz99mh_gen.h"
#include "lzz99mh_readable.h"
#include "lzz99mh_player.h"
#include "lzz99mh_item.h"
#include "lzz99mh_initscan.h"
#include "lzz99mh_mob.h"
#include "lzz99mh_weapon.h"
#include "lzz99mh_shop.h"
#include "lzz99mh_popupMessage.h"
#include "lzz99mh_sub.h"
#include "lzz99mh_resource.h"
#include "libs/weapon_effect.h"
#include "libs/shop.h"
#include "libs/coopteam.h"
#include "libs/logging.h"

#define GROUP_ixWallsPart1 0
#define GROUP_ixWallsPart2 1
#define GROUP_ixWallsPart3 2
#define GROUP_ixWallsPart4 3
#define GROUP_mysticWalls 4
#define GROUP_fastSecretWalls 6
#define GROUP_part1FinalWalls 7
#define GROUP_part2LockWalls 8

#define GROUP_surpriseGenWalls1F 9
#define GROUP_surpriseGenWalls1FOut 10
#define GROUP_thirdPartWalls1 11
#define GROUP_gate31Walls 12
#define GROUP_thirdPartWalls2 13

int m_invincibleGen;

int m_mysticWallCount;
int m_genericHash;

static int GenericHash()
{
    return m_genericHash;
}

#define GROUP_horrendousWalls 5

int m_lastUnit;
int m_unitscanHashInstance;
int m_monsterHash;

string *m_weaponNameTable;
int m_weaponNameTableLength;
string *m_armorNameTable;
int m_armorNameTableLength;
string *m_potionNameTable;
int m_potionNameTableLength;
string *m_staffNameTable;
int m_staffNameTableLength;
int *m_itemFunctionTable;
int m_itemFunctionTableLength;

int m_potionHash;
int m_initScanHash;

int *m_pTestGen;
int *m_pWeaponProperty;
int *m_pPropertyExecutor;
int *m_pPropertyFxExec;

#define MAX_PLAYER_COUNT 32
int m_player[MAX_PLAYER_COUNT];
int m_pFlags[MAX_PLAYER_COUNT];
int m_pLastItem[MAX_PLAYER_COUNT];

int m_monSightHash;
int m_dialogCtx;

static int GetDialogCtxPtr() //virtual
{
    return &m_dialogCtx;
}

static int GetDialogCtx() //virtual
{
    return m_dialogCtx;
}

string m_popupMessage[32];

static string GetPopupMessage(int index)
{
    return m_popupMessage[index];
}

int m_userRespawnMark[MAX_PLAYER_COUNT];

static int GetUserRespawnMark(int pIndex) //virtual
{
    return m_userRespawnMark[pIndex];
}

#define INIT_RESPAWN_MARK_LOCATION 189

void initUserRespawnMark()
{
    int r=MAX_PLAYER_COUNT;
    float x=LocationX(INIT_RESPAWN_MARK_LOCATION), y=LocationY(INIT_RESPAWN_MARK_LOCATION);

    while(--r>=0)
    {
        m_userRespawnMark[r]=CreateObjectById(OBJ_TEEPEE_SHELVES_FULL_1 , x+MathSine(r*10 +90, 11.0), y+MathSine(r*10, 11.0));
        UnitNoCollide(m_userRespawnMark[r]);
        CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(m_userRespawnMark[r]), GetObjectY(m_userRespawnMark[r]));
    }
}

string *m_specialWeaponNameArray;
int m_specialWeaponNameArrayCount;
int *m_specialWeaponPays;
int *m_specialWeaponFn;
int *m_specialWeaponMessages;

static int GetSpecialWeaponMessage(int cur) //override
{
    return m_specialWeaponMessages[cur];
}

static int GetSpecialWeaponFunction(int cur) //virtual
{
    return m_specialWeaponFn[cur];
}

void ServerInitSpecialWeaponFunctions()
{
    int fns[]={DispositionArrowRainSword, DispositionWolfSword, DispositionAutoTrackingSword,
    DispositionYellowLightningSword,DispositionTripleArrowHammer, DispositionDeathraySword,};

    m_specialWeaponFn=fns;
}

static string GetSpecialWeaponName(int cur) //override
{
    return m_specialWeaponNameArray[cur];
}
static int GetSpecialWeaponPay(int cur) //virtual
{
    return m_specialWeaponPays[cur];
}

static void SetSpecialWeaponNameArray(string *p, int count) //virtual
{
    m_specialWeaponNameArray=p;
    m_specialWeaponNameArrayCount=count;
}

static void SetSpecialWeaponPayAndMessage(int *pPay, int *pMessages) //virtual
{
    m_specialWeaponPays=pPay;
    m_specialWeaponMessages=pMessages;
}

void DeferredPutRandomItem(int pos)
{
    CreateRandomItemCommon(pos);
}

static void OnGeneratorDestroyed(int gen)
{
    GreenExplosion(GetObjectX(gen), GetObjectY(gen));
    PlaySoundAround(gen, SOUND_MonsterGeneratorDie);
    PushTimerQueue(1, CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,GetObjectX(gen),GetObjectY(gen)), DeferredPutRandomItem );
}

static int MonsterSightHash() //virtual
{
    return m_monSightHash;
}

int m_specialWeaponProperties[10];

static int GetWeaponSpecialProperty(int n) //virtual
{
    return m_specialWeaponProperties[n];
}

int m_exitWarpUnit;

void MapExit()
{
    MusicEvent();
    RemoveCoopTeamMode();
    ResetPlayerHandlerWhenExitMap();
    ResetHostileCritter();
}

void EnableObject(int unit)
{
    ObjectOn(unit);
}

void ArrowSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 9.0), yvect = UnitAngleSin(OTHER, 9.0);
    int posUnit = CreateObjectAt("ImaginaryCaster", GetObjectX(OTHER) + xvect -(yvect*2.0), GetObjectY(OTHER) + yvect +(xvect*2.0));
    int rep = -1, single = 0, dir = GetDirection(OTHER);

    while (++rep<5)
    {
        CreateObjectAtEx("ArcherArrow", GetObjectX(posUnit), GetObjectY(posUnit), &single);
        SetOwner(OTHER, single);
        PushObjectTo(single, xvect*2.0, yvect*2.0);
        LookWithAngle(single, dir);
        MoveObjectVector(posUnit, yvect, -xvect);
    }
}

void PropertyTestPrint(int *ptr)
{
    UniPrintToAll(IntToString(ptr));
}

void InitSpecialProperties()
{
    SpecialWeaponPropertyCreate(&m_pWeaponProperty);
    int *propertyOffset = 0x5BA264;
    int *pFirst = propertyOffset[0];
    m_pWeaponProperty[0] = pFirst[0];
    m_pWeaponProperty[5] = 30;
    SpecialWeaponPropertyExecuteScriptCodeGen(ArrowSwordTriggered, &m_pPropertyExecutor);
    SpecialWeaponPropertyExecuteFXCodeGen(FX_ID_COUNTERSPELL_EXPLOSION, SOUND_FirewalkOn, &m_pPropertyFxExec);
    SpecialWeaponPropertySetFXCode(m_pWeaponProperty, m_pPropertyFxExec);
    SpecialWeaponPropertySetExecuteCode(m_pWeaponProperty, m_pPropertyExecutor);

    FrameTimerWithArg(60, m_pWeaponProperty, PropertyTestPrint);
}

void DeferredInit()
{
    WriteLog("DeferredInit");
    HashCreateInstance(&m_potionHash);
    FillElemPotionHash(m_potionHash);
    HashCreateInstance(&m_monSightHash);
    HashCreateInstance(&m_monsterHash);
    InitializeSummonMobHash(m_monsterHash);
    HashCreateInstance(&m_initScanHash);
    HashPushback(m_initScanHash, OBJ_REWARD_MARKER, CreateRandomItemCommon);
    StartUnitScan(Object("firstscan"), m_lastUnit, m_initScanHash);
    FrameTimer(1, PutGeneratorArea1);
    InitializeShopSystem();
    SetMemory(0x5d5330, 0);
    SetMemory(0x5d5394, 1);
    InitializeReadables();
    m_invincibleGen=CreateObjectById(OBJ_MONSTER_GENERATOR, LocationX(138),LocationY(138));
    ObjectOff(m_invincibleGen);
    Frozen(m_invincibleGen,TRUE);
    InitialSubPart();
    WriteLog("DeferredInit-end");
}

void MapInitialize()
{
    MusicEvent();
    CreateLogFile("lzz99mh-log.txt");
    m_lastUnit=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, 100.0,100.0);
    PushTimerQueue(1, Object("Elv11"), EnableObject);
    PushTimerQueue(16, Object("Elv12"), EnableObject);
    PushTimerQueue(31, Object("Elv13"), EnableObject);
    InitPopupMessage(m_popupMessage); //server works
    InitItemNames();
    InitSpecialProperties();
    initUserRespawnMark();
    InitializeUserSpecialWeapon(m_specialWeaponProperties);
    PushTimerQueue(1, 0, MakeCoopTeam);
    PushTimerQueue(1, 0, DeferredInit);
    HashCreateInstance(&m_genericHash);
    ServerInitSpecialWeaponFunctions();
    WriteLog("end mapInitialize");
    InitializeThreeFloorElevators();
    SetHostileCritter();
}

static int GetPlayerItem(int pIndex)
{
    return m_pLastItem[pIndex];
}

static void SetPlayerItem(int pIndex, int item)
{
    m_pLastItem[pIndex]=item;
}

static int GetPlayer(int pIndex) //override
{
    return m_player[pIndex];
}

static void SetPlayer(int pIndex, int user) //override
{
    m_player[pIndex]=user;
}

static int GetPlayerFlags(int pIndex) //override
{
    return m_pFlags[pIndex];
}

static void SetPlayerFlags(int pIndex, int flags) //override
{
    m_pFlags[pIndex]=flags;
}

static int GetPotionHash() //override
{
    return m_potionHash;
}
static void SetItemNameWeapon(int *p, int count)
{
    m_weaponNameTable=p;
    m_weaponNameTableLength=count;
}
static void SetItemNameArmor(int *p, int count)
{
    m_armorNameTable=p;
    m_armorNameTableLength=count;
}
static void SetItemNamePotion(int *p,int count)
{
    m_potionNameTable=p;
    m_potionNameTableLength=count;
}
static void SetItemNameStaff(int *p,int count)
{
    m_staffNameTable=p;
    m_staffNameTableLength=count;
}
static void SetItemFunctionPtr(int *p,int count) //override
{
    m_itemFunctionTable=p;
    m_itemFunctionTableLength=count;
}
static string GetWeaponName()
{
    return m_weaponNameTable[Random(0, m_weaponNameTableLength-1)];
}
static string GetArmorName()
{
    return m_armorNameTable[Random(0, m_armorNameTableLength-1)];
}
static string GetPotionName()
{
    return m_potionNameTable[Random(0, m_potionNameTableLength-1)];
}
static string GetWandName()
{return m_staffNameTable [Random(0, m_staffNameTableLength-1)];}

static int GetItemCreateFunction()
{
    return m_itemFunctionTable[Random(0,m_itemFunctionTableLength-1)];
}

void InvokeSummonMonsterProcedure(int f, int mob)
{
    Bind(f, &f + 4);
}

static void OnGeneratorSummoned(int gen, int mob)
{
    int fn;

    WriteLog("OnGeneratorSummoned");
    if (HashGet(m_monsterHash, GetUnitThingID(mob), &fn, FALSE))
        InvokeSummonMonsterProcedure(fn, mob);
    else
        SetUnitMaxHealth(mob, GetGeneratorMonsterSpecHP(GetUnitThingID(mob), MaxHealth(mob)*4));
    
    LookAtObject(mob, gen);
    LookWithAngle(mob, GetDirection(mob)+128);
    AggressionLevel(mob, 1.0);
    RetreatLevel(mob, 0.0);
    SetCallback(mob,7,onMonsterHit);
    WriteLog("OnGeneratorSummoned-end");
}

void OpenWallsIxPart1()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_ixWallsPart1);
    PutGeneratorArea1P1();
}

void OpenWallIxPart2()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_ixWallsPart2);
    SpawnTinyBomber(98, 5);
    SpawnTinyBomber(99, 5);
    SpawnTinyBomber(100, 5);
    SpawnTinyBomber(101, 5);
    CreateObjectById(OBJ_SILVER_KEY, LocationX(102),LocationY(102));
}

void OpenWallPart3()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_ixWallsPart3);
    PutGeneratorArea1P2();
}

void SurpriseArea1P3()
{
    ObjectOff(SELF);
    PutGeneratorArea1P3();
}
void OnJandorDeath()
{
    UnlockDoor(Object("jandorGate1"));
    UnlockDoor(Object("jandorGate2"));
    UniChatMessage(SELF, "앗 슈벌...", 90);
    UniPrintToAll("다음구간으로 통하는 출입문의 잠금이 해제되었습니다");
    PushTimerQueue(1, 0, PutGeneratorArea1P5);
}

void OpenWallPart4()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_ixWallsPart4);
    PutGeneratorArea1P4();
    int boss=SpawnBossJandor(23);

    SetCallback(boss,5,OnJandorDeath);
    UniChatMessage(boss, "이곳을 지나가고 싶으면, 당신이 나를 지려밟고 지나가소서...", 150);
}

void BossHorrendousDie()
{
    WallGroupOpen(GROUP_horrendousWalls);
    UniChatMessage(SELF, "이것은 말도안돼~!", 120);
    PutGeneratorArea1P6();
}

void OpenMysticWalls()
{
    ObjectOff(SELF);
    if (m_mysticWallCount<2)
    {
        m_mysticWallCount+=1;
        int mob= CreateMysticSludge(GetObjectX(SELF),GetObjectY(SELF));

        UniChatMessage(mob, "오올옭옭..", 120);
        return;
    }
    int already;

    if (already)
        return;
    already=TRUE;
    WallGroupOpen(GROUP_mysticWalls);
    int boss=CreateBossHorrendous(LocationX(56),LocationY(56));

    LookWithAngle(boss, 92);
    SetCallback(boss,5,BossHorrendousDie);
    UniChatMessage(boss, "뒤졌어, 네는 내가 특별히 제일먼저 죽여줄게..", 150);
}

void OpenFastSecret()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_fastSecretWalls);
}

void PutP6_1Gen()
{
    ObjectOff(SELF);
    PutGeneratorArea1P6_1();
}

void StartArea1P7()
{
    ObjectOff(SELF);
    PutGeneratorArea1P7();
}

void Part1ElevOn()
{
    ObjectOff(SELF);
    ResumeThreeElevator();
    PlaySoundAround(SELF,SOUND_CreatureCageAppears);
    int boss= Part1FinalBoss(LocationX(81),LocationY(81));

    UniChatMessage(boss, "나와싸우자!", 150);
    PutGeneratorArea2P1();
    PutGeneratorArea1P9();
}

void UnlockPart1FinalGate()
{
    ObjectOff(SELF);
    UnlockDoor(Object("p724"));
    UnlockDoor(Object("p723"));
    UnlockDoor(Object("p725"));
    UnlockDoor(Object("p726"));
    UniPrint(OTHER,"출입문의 잠금이 해제되었습니다");
}

void RemovePart1Final()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_part1FinalWalls);
    PutGeneratorArea1P8();
}

void ClientProcLoop()
{
    char *type = _CLIENT_OPTION_TYPE_OFF_;

    if (type[1])
    {
        onPopupMessageChanged(type[1]);
        type[1]=0;
    }
    if (type[0])
    {
        type[1]=type[0];
        type[0]=0;
    }
    FrameTimer(1, ClientProcLoop);
}

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    InitPopupMessage(m_popupMessage);
    InitializeCommonImage();
    FrameTimer(10, ClientProcLoop);
}

void MoveVolume2Block1()
{
    ObjectOff(SELF);
    Move(Object("volume2bl1"), 136);
    Move(Object("volume2bl2"), 137);
}

void OnPart2BossDeath(){
    UniChatMessage(SELF, "윽.. 사망", 90);
    if (CurrentHealth(m_invincibleGen))
    {
        Frozen(m_invincibleGen,FALSE);
        ObjectOn(m_invincibleGen);
        Damage(m_invincibleGen,0,999,DAMAGE_TYPE_PLASMA);
    }
}

void OnBlackBookPickup()
{
    if (CurrentHealth(OTHER))
    {
        Delete(SELF);
        int boss=Part2BossShaman(GetObjectX(OTHER),GetObjectY(OTHER));
        UniChatMessage(boss,"보스등장이요~",120);
        SetCallback(boss,5,OnPart2BossDeath);
    }
}

void EntryArea2()
{
    ObjectOff(SELF);
    int book=CreateObjectById(OBJ_BLACK_BOOK_1, LocationX(137),LocationY(137));

    RegistItemPickupCallback(book,OnBlackBookPickup);
    PutGeneratorArea2P2();
}

void PutGen2_3()
{
    ObjectOff(SELF);
    PutGeneratorArea2P3();
}

void PutGen2_4()
{
    ObjectOff(SELF);
    PutGeneratorArea2P4();
}

void PutGen2_5()
{
    ObjectOff(SELF);
    PutGeneratorArea2P5();
}

void OnRockP2Death()
{
    float x=GetObjectX(SELF),y=GetObjectY(SELF);

    DeleteObjectTimer(CreateObjectById(OBJ_BIG_SMOKE, x,y), 30);
    DeferredPutRandomItem(CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW,x,y));
    PlaySoundAround(SELF, SOUND_HitStoneBreakable);
    Delete(SELF);
}

void PlaceRockSingleP2(short location)
{
    int rock=CreateObjectById(OBJ_ROCK_1, LocationX(location),LocationY(location));

    SetUnitMaxHealth(rock, 255);
    SetUnitMass(rock, 9999.0);
    SetUnitCallbackOnDeath(rock, OnRockP2Death);
}

void PlacingPart2Rocks()
{
    PlaceRockSingleP2(171);
    PlaceRockSingleP2(172);
    PlaceRockSingleP2(174);
    PlaceRockSingleP2(175);
    PlaceRockSingleP2(176);
    PlaceRockSingleP2(177);
    PlaceRockSingleP2(178);
    PlaceRockSingleP2(179);
    PlaceRockSingleP2(181);
    PlaceRockSingleP2(182);
    PlaceRockSingleP2(183);
    PlaceRockSingleP2(184);
    PlaceRockSingleP2(185);
}

void Part2LockEvent()
{
    ObjectOff(SELF);
    PlacingPart2Rocks();
    WallOpen(Wall(225, 81));
    PutGeneratorArea2P6();
    PlaySoundAround(OTHER, SOUND_Lock);
    UniChatMessage(OTHER, "이런! 문이 잠기어 버렸네!. 어쩔수가 없군~ 바쁠수록 돌아서 가라고 누군가 이야기 혔지?", 150);
}

void OpenPart2Lock()
{
    ObjectOff(SELF);
    UniPrint(OTHER,"벽을 열었습니다");
    WallGroupOpen(GROUP_part2LockWalls);
    UnlockDoor(Object("part2LockSingle"));
}

void OpenSurpriseGenWalls1F()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_surpriseGenWalls1F);
    PutGenArea1Surprise1();
}

void OpenSur1Out()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_surpriseGenWalls1FOut);
}

void TestBtn() //RemoveMe
{
    WallGroupOpen(GROUP_gate31Walls);
}

void Unlock3F1()
{
    ObjectOff(SELF);
    UnlockDoor(Object("gate3f1"));
    UnlockDoor(Object("gate3f11"));
    WallOpen(Wall( 21,149));
    WallOpen(Wall(22,150));
    WallOpen(Wall(23,151));
    WallOpen(Wall(24,152));
    WallOpen(Wall(25,153));
    PutGenArea3Part2();
    UniPrint(OTHER, "아래쪽 출입문의 잠금이 해제되었습니다");
}

void OpenThirdWalls1()
{
    ObjectOff(SELF);
    ThirdPartGen();
    WallGroupOpen(GROUP_thirdPartWalls1);
}

void OpenThirdWalls2()
{
    ObjectOff(SELF);
    WallGroupOpen(GROUP_thirdPartWalls2);
}

void onWarpCollide()
{
    if (CurrentHealth(OTHER))
    {
        PlaySoundAround(OTHER, SOUND_PlayerExit);
        Effect("SMOKE_BLAST", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        MoveObject(OTHER, LocationX(313), LocationY(313));
        Effect("TELEPORT", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void onFinalPartDead()
{
    if (ToInt(GetObjectX(m_exitWarpUnit)))
    {
        float x= GetObjectX(m_exitWarpUnit),y=GetObjectY(m_exitWarpUnit);

        Delete(m_exitWarpUnit);
        int warp = CreateObjectById(OBJ_GAUNTLET_WARP_EXIT_B, x,y);

        SetUnitCallbackOnCollide(warp, onWarpCollide);
    }
}

void StartFinal()
{
    ObjectOff(SELF);
    FinalPartGen2();
    int fin = CreateWizardRed(LocationX(311), LocationY(311));

    SetCallback(fin, 5, onFinalPartDead);
    m_exitWarpUnit=CreateObjectById(OBJ_GAUNTLET_WARP_EXIT_B, LocationX(312), LocationY(312));

    CloseWarpExit(m_exitWarpUnit);
}

static void PlaceGenComplex() //virtual
{
    PlaceFinalGenComplex();
    StartFinalWave();
}

static void OnPlayerEntryMap(int pInfo)
{
    int *ptr=GetMemory(pInfo+0x808);
    int pUnit = 0;

    if (ptr)
        pUnit = GetMemory(ptr+0x2c);
    
    // if (pUnit)
    //     ShowQuestIntroOne(pUnit, 237, "NoxWorldMap", "Noxworld.wnd:NotReady");

    if (pInfo==0x653a7c)
    {
        InitializeCommonImage();
        return;
    }
    if (pUnit)
        NetworkUtilClientEntry(pUnit);
}
