
#include "libs/grplib.h"
#include "libs/animFrame.h"
#include "libs/objectIDdefines.h"

 void imageDesc1()
{ }
 void imageDesc2()
{ }

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 114160
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_MECHANICAL_GOLEM;
    int xyInc[]={0,-30};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake4Directions(frameCount-4);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}

void initializeClientStuff(){
    ChangeSpriteItemNameAsString(OBJ_YELLOW_POTION, "황금 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_YELLOW_POTION, "체력을 무려 125 이나 회복시켜준다");
    ChangeSpriteItemNameAsString(OBJ_BLACK_POTION, "검은 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_BLACK_POTION, "체력을 85 만큼 회복시켜줍니다");
    ChangeSpriteItemNameAsString(OBJ_WHITE_POTION, "백색 엘릭서");
    ChangeSpriteItemDescriptionAsString(OBJ_WHITE_POTION, "체력과 마력을 100 만큼 회복시켜줍니다");
    ChangeSpriteItemNameAsString(OBJ_FOIL_CANDLE_LIT, "전기 촛불");
    ChangeSpriteItemDescriptionAsString(OBJ_FOIL_CANDLE_LIT, "쇼크 버프를 제공해 준다");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_MANIPULATION, "대피소 긴급이동 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_MANIPULATION, "이 목걸이를 사용하면, 안전한 장소로 즉시 이동합니다");
    ChangeSpriteItemNameAsString(OBJ_BRACELETOF_HEALTH, "패스트힐링 포션");
    ChangeSpriteItemDescriptionAsString(OBJ_BRACELETOF_HEALTH, "잠시동안 체력회복 속도가 증가합니다");
    ChangeSpriteItemNameAsString(OBJ_CANDLE_1, "매직가드 촛불");
    ChangeSpriteItemDescriptionAsString(OBJ_CANDLE_1, "잠시동안 주변에 보호벽이 생성됩니다");
    ChangeSpriteItemNameAsString(OBJ_AMULET_OF_CLARITY, "신비의 물약");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULET_OF_CLARITY, "주변으로 널리 퍼지는 수리검 소환!");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_COMBAT, "곰덫 교환권");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_COMBAT, "사용 시 곰덫 1개 생성됩니다");
    ChangeSpriteItemNameAsString(OBJ_CORN, "갓구운 바게트빵");
    ChangeSpriteItemDescriptionAsString(OBJ_CORN, "체력을 거의 100퍼센트에 근접하게 채워줌. 멋지지만, 애매한 아이템..ㄱㅡ");
}

void EnableTileTexture(int enabled)
{
    //0x5acd50
    int *p = 0x5acd50;

    if (p[0]==enabled)
        return;
        
    char code[]={0xB8, 0x15, 0xB7, 0x4C, 0x00, 0xFF, 0xD0, 0x31, 0xC0, 0xC3,};

    invokeRawCode(code, NULLPTR);
    
}

void onUpdateTileChanged()
{
    EnableTileTexture(FALSE);
    // SolidTile(628, 6,0x07FA);
    EnableTileTexture(TRUE);
}

void GRPDumpTileSet(){}

void initializeCustomTileset(int imgVector){
    int *pack=GetScrCodeField(GRPDumpTileSet)+4;
    int *off=&pack[1];
    int frames[]={pack+off[0],pack+off[1], pack+off[2],pack+off[3],pack+off[4],};

    AppendImageFrame(imgVector, frames[0], 12195);
    AppendImageFrame(imgVector, frames[1], 12196);
    AppendImageFrame(imgVector, frames[0], 12197);
    AppendImageFrame(imgVector, frames[2], 12198);
    AppendImageFrame(imgVector, frames[0], 12199);
    AppendImageFrame(imgVector, frames[3], 12200);
    AppendImageFrame(imgVector, frames[0], 12201);
    AppendImageFrame(imgVector, frames[4], 12202);
    AppendImageFrame(imgVector, frames[0], 12203);
    onUpdateTileChanged();
}

void InitializeResource(){
    int imgVector = CreateImageVector(1024);
    AppendImageFrame(imgVector, GetScrCodeField(imageDesc1)+4, 132313);
    AppendImageFrame(imgVector, GetScrCodeField(imageDesc2)+4, 132314);
    initializeImageFrameBillyOni(imgVector);
    initializeCustomTileset(imgVector);
    DoImageDataExchange(imgVector);
    initializeClientStuff();
}
