
#include "libs/define.h"

#define MAX_PLAYER_COUNT 32

#define PLAYER_START_LOCATION_AT 12
#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_HARPOON 32
#define PLAYER_FLAG_THREADLIGHTLY 64
#define PLAYER_FLAG_ALL_BUFF 128
#define PLAYER_FLAG_FASTHEALING 256

#define ABILITY_ID_BERSERKER_CHARGE 	1
#define ABILITY_ID_WARCRY 			2
#define ABILITY_ID_HARPOON 			3
#define ABILITY_ID_TREAD_LIGHTLY		4
#define ABILITY_ID_EYE_OF_WOLF		5

void QueryMainKeySecond(int *get, int set){}//virtual
void QueryMainKeyFirst(int *get, int set){}//virtual
void QueryLibraryBookcases1(int *get, int set){}//virtual
void QueryLibraryBookcases2(int *get, int set){}//virtual
void QueryLibraryCount(int *get,int set){}//virtual
void QueryMyBlockFirst(int *get,int set){}//virtual
void QueryMyBlockSecond(int *get,int set){}//virtual
void QueryCampArea(int *get,int set){}//virtual
void QueryArrowTrapThird(int *get,int set){}//virtual
void QueryArrowTrapSecond(int *get,int set){}//virtual
void QueryArrowTrapFirst(int *get,int set){}//virtual
void QueryUnderfootRots(int *get,int set){}//virtual
void QueryFireTrap1(int *get,int set){}//virtual
void QueryLibraryBookEx(int *get,int set){}//virtual
void QueryLibraryArrowTraps(int *get,int set, int n){}//virtual

