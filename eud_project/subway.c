
#include "subway_main.h"

void MapExit(){OnShutdownMap();}
void MapInitialize(){OnInitializeMap();}

int m_player[MAX_PLAYER_COUNT];
static int GetPlayer(int pIndex) //virtual
{ return m_player[pIndex]; }
static void SetPlayer(int pIndex, int user) //virtual
{ m_player[pIndex]=user; }
int m_playerFlags[MAX_PLAYER_COUNT];
static int GetPlayerFlags(int pIndex) //virtual
{return m_playerFlags[pIndex]; }
static void SetPlayerFlags(int pIndex, int flags) //virtual
{ m_playerFlags[pIndex]=flags;}

int m_LUnit[10], m_DLord[21];

static void QueryLUnit(int *get, int set, int n){
    if (get)
    {
        get[0]=m_LUnit[n];
        return;
    }
    m_LUnit[n]=set;
}

static void QueryDLord(int *get,int set, int n){
    if (get){
        get[0]=m_DLord[n];
        return;
    }
    m_DLord[n]=set;
}

int m_AGate[8];

static void QueryAGate(int *get,int set,int n){
    if (get){
        get[0]=m_AGate[n];
        return;
    }
    m_AGate[n]=set;
}
static void CheckDemonDeathCount(int count){DoCheckDemonDeathCount(count);}

int m_creature1[MAX_PLAYER_COUNT];
static void GetCreature1DataPtr(int pIndex,int**pget){//virtual
    if (pget)
        pget[0]=&m_creature1[pIndex];
}//virtual

static void NetworkUtilClientMain(){ InitializeClientOnly(); }
static void InitServerResource(){ 
    int once;
    if (once) return;
    once=TRUE;
    DoInitializeResources();
}//virtual

static int NetworkUtilClientTimerEnabler(){return TRUE;}

static void OnPlayerEntryMap(int pInfo)
{
    ConfirmPlayerEntryMap(pInfo);
}

