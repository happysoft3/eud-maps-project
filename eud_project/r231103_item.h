
#include "libs/define.h"
#include "libs/itemproperty.h"
#include "libs/unitstruct.h"
#include "libs/weaponcapacity.h"
#include "libs/potionex.h"
#include "libs/hash.h"
#include "libs/buff.h"
#include "libs/potionpickup.h"
#include "libs/printutil.h"
#include "libs/sound_define.h"
#include "libs/waypoint.h"
#include "libs/queueTimer.h"
#include "libs/fxeffect.h"
#include "libs/bind.h"
#include "libs/objectIDdefines.h"

#define PLAYER_SAFE_LOCATION 38

int yellowPotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateYellowPotion(GetObjectX(del), GetObjectY(del), 125);
    Delete(del);
    return pot;
}
int blackPotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateBlackPotion(GetObjectX(del), GetObjectY(del), 85);
    Delete(del);
    return pot;
}
int whitePotionHash(int pot)
{
    int del=pot;
    pot = PotionExCreateWhitePotion(GetObjectX(del), GetObjectY(del), 100);
    Delete(del);
    return pot;
}
void onGodPotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_INVULNERABLE), 25.0);
}
int godPotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onGodPotionUse);
    return pot;
}

void onHastePotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_HASTED), 0.0);
}

int hastePotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onHastePotionUse);
    return pot;
}

void onVampPotionUse()
{
    Delete(SELF);
    Enchant(OTHER, EnchantList(ENCHANT_VAMPIRISM), 0.0);
}

int vampPotionHash(int pot)
{
    SetUnitCallbackOnUseItem(pot, onVampPotionUse);
    return pot;
}
void onTeleportAmuletUse()
{
    Delete(SELF);
    Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
    MoveObject(OTHER, LocationX(PLAYER_SAFE_LOCATION),LocationY(PLAYER_SAFE_LOCATION));
    Effect("TELEPORT",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
    PlaySoundAround(OTHER,SOUND_BlindOff);
    UniPrint(OTHER, "공간이동 목걸이를 사용하여, 안전한 장소로 이동을 했습니다");
}

int teleportAmulet(int pot)
{
    SetUnitCallbackOnUseItem(pot,onTeleportAmuletUse);
    SetItemPropertyAllowAllDrop(pot);
    return pot;
}

void onHealingBuff(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int owner= GetOwner(sub);
        if (CurrentHealth(owner))
        {
            int dur=GetDirection(sub);
            if (dur)
            {
                if (ToInt(DistanceUnitToUnit(sub, owner)))
                    MoveObject(sub, GetObjectX(owner), GetObjectY(owner));
                LookWithAngle(sub, dur-1);
                RestoreHealth(owner, 4);
                PushTimerQueue(3, sub, onHealingBuff);
                return;
            }
        }
        Delete(sub);
    }
}

void startHealingBuff(int owner)
{
    int sub=CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(owner), GetObjectY(owner));

    LookWithAngle(sub, 80);
    SetOwner(owner, sub);
    SetUnitEnchantCopy(sub, GetLShift(ENCHANT_RUN));
    PushTimerQueue(1, sub, onHealingBuff);
}

void onHealingPotionUse()
{
    Delete(SELF);
    startHealingBuff(GetCaller());
}

int healingAmulet(int pot)
{
    SetUnitCallbackOnUseItem(pot,onHealingPotionUse);
    return pot;
}

void onFearUse()
{
    Delete(SELF);
    RestoreHealth(OTHER, 999);
    Enchant(OTHER, "ENCHANT_SHOCK", 120.0);
}

int potionFear(int pot)
{
    SetUnitCallbackOnUseItem(pot,onFearUse);
    return pot;
}

void onAmuletofManipulationUse()
{
    Delete(SELF);
    CastSpellObjectObject("SPELL_METEOR_SHOWER",OTHER,OTHER);
    UniPrint(OTHER,"자신 주변에 운석 소나기를 내립니다");
}

int potionAmuletOfManipulation(int pot)
{
    SetUnitCallbackOnUseItem(pot,onAmuletofManipulationUse);
    return pot;
}

int GetPotionHash()
{ return 0;}

void FillElemPotionHash(int *pHash)
{
    HashPushback(pHash, OBJ_YELLOW_POTION, yellowPotionHash);
    HashPushback(pHash, OBJ_WHITE_POTION, whitePotionHash);
    HashPushback(pHash, OBJ_BLACK_POTION, blackPotionHash);
    HashPushback(pHash, OBJ_INVULNERABILITY_POTION, godPotionHash);
    HashPushback(pHash, OBJ_HASTE_POTION, hastePotionHash);
    HashPushback(pHash, OBJ_VAMPIRISM_POTION, vampPotionHash);
    HashPushback(pHash, OBJ_AMULET_OF_TELEPORTATION, teleportAmulet);
    HashPushback(pHash, OBJ_AMULETOF_NATURE, healingAmulet);
    HashPushback(pHash, OBJ_FEAR, potionFear);
    HashPushback(pHash, OBJ_AMULETOF_MANIPULATION, potionAmuletOfManipulation);
}

void SetItemNameWeapon(int *p, int count)
{ }
void SetItemNameArmor(int *p, int count)
{ }
void SetItemNamePotion(int *p,int count)
{ }
void SetItemNameStaff(int *p,int count)
{ }
void SetItemFunctionPtr(int *p,int count)
{ }

void InitItemNames()
{
    int weapons[]={"WarHammer", "GreatSword", "Sword", "Longsword", "StaffWooden", 
    "OgreAxe", "BattleAxe", "FanChakram", "RoundChakram",
            "OblivionHalberd", "OblivionHeart", "OblivionWierdling"};
    int armors[]={"OrnateHelm", "Breastplate", "PlateArms", "PlateBoots", 
    "PlateLeggings", "ChainCoif", "ChainLeggings", "ChainTunic", "SteelShield","WoodenShield",
            "LeatherArmbands", "LeatherArmor", "LeatherArmoredBoots", "LeatherBoots", 
            "LeatherHelm", "LeatherLeggings", "MedievalCloak",
            "MedievalPants", "MedievalShirt", "StreetShirt", "StreetSneakers", "StreetPants"};
    int pots[]={"RedPotion", "RedPotion", "CurePoisonPotion", "VampirismPotion", "RedPotion",
    "ShieldPotion", "InvisibilityPotion", "InvulnerabilityPotion","CurePoisonPotion",
            "ShieldPotion", "HastePotion", "FireProtectPotion", "ShockProtectPotion", 
            "PoisonProtectPotion", "YellowPotion", "BlackPotion", "CurePoisonPotion",
            "CurePoisonPotion", "AmuletOfTeleportation", "AmuletofNature", "Fear","AmuletofManipulation"};
    int wands[]={"DeathRayWand", "LesserFireballWand", "FireStormWand", "ForceWand", 
    "InfinitePainWand", "SulphorousFlareWand", "SulphorousShowerWand"};

    SetItemNameWeapon(weapons,sizeof(weapons));
    SetItemNameArmor(armors,sizeof(armors));
    SetItemNamePotion(pots,sizeof(pots));
    SetItemNameStaff(wands,sizeof(wands));
    int items[]={ CreateHotPotion, CreateHotPotion2, CreateCoinBetter, CreateGerm,
        CreateMagicalStaff, CreateRandomArmor, CreateRandomPotion, CreateRandomWeapon};

    SetItemFunctionPtr(items,sizeof(items));
}

string GetWeaponName()
{}
string GetArmorName()
{}
string GetPotionName()
{}
string GetWandName()
{}
int GetItemCreateFunction() //virtual
{}

void CreateRandomWeapon(float xpos, float ypos, int *pDest)
{
    int weapon = CreateObjectAt(GetWeaponName(), xpos, ypos);

    if (pDest)
        pDest[0] = weapon;

    SetWeaponProperties(weapon, Random(0, 5), Random(0, 5), Random(0, 28), Random(0, 28));
    
    int thingId = GetUnitThingID(weapon);

    if (thingId>=OBJ_OBLIVION_HALBERD&&thingId<=OBJ_OBLIVION_ORB)
    {
        DisableOblivionItemPickupEvent(weapon);
        SetItemPropertyAllowAllDrop(weapon);
    }
    else if (thingId==OBJ_FAN_CHAKRAM||OBJ_QUIVER==thingId)
        SetConsumablesWeaponCapacity(weapon, 255, 255);
}

void CreateRandomArmor(float xpos, float ypos, int *pDest)
{
    int armor = CreateObjectAt(GetArmorName(), xpos, ypos);

    if (pDest)
        pDest[0] = armor;

    SetArmorProperties(armor, Random(0, 5), Random(0, 5), Random(0, 20), Random(0, 20));
}

void CreateCoin(float xpos, float ypos, int *pDest)
{
    int coin = CreateObjectAt("QuestGoldChest", xpos, ypos);

    if (pDest)
        pDest[0] = coin;
    UnitStructSetGoldAmount(coin, Random(500, 2000));
}

void CreateCoinBetter(float xpos, float ypos, int *pDest)
{
    int coin = CreateObjectAt("QuestGoldChest", xpos, ypos);

    if (pDest)
        pDest[0] = coin;
    UnitStructSetGoldAmount(coin, Random(1500, 5000));
}

void CreateGerm(float xpos, float ypos, int *pDest)
{
    string germs[] = {"Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Ruby", "Emerald", "Emerald", "Emerald", "Diamond"};
    int pic = CreateObjectAt(germs[Random(0, sizeof(germs) - 1)], xpos, ypos);

    if (pDest)
        pDest[0] = pic;
}

int potionHashProto(int functionId, int pot)
{
    return Bind(functionId, &functionId+4);
}

void CreateRandomPotion(float xpos, float ypos, int *pDest)
{
    int potion = CreateObjectAt(GetPotionName(), xpos, ypos);

    // CheckPotionThingID(potion);
    int fn;
    if (HashGet(GetPotionHash(), GetUnitThingID(potion), &fn, FALSE))
        potion= potionHashProto(fn, potion);
    PotionPickupRegist(potion);
    if (pDest)
        pDest[0] = potion;
    if (MaxHealth(potion))
        SetUnitMaxHealth(potion, 0);
}

void CreateHotPotion(float xpos, float ypos, int *pDest)
{
    int potion = CreateObjectAt("RedPotion", xpos, ypos);

    PotionPickupRegist(potion);
    if (pDest)
        pDest[0] = potion;
}

void CreateHotPotion2(float x,float y, int*p)
{
    string n[]={"RedPotion","CurePoisonPotion"};
    int pot=CreateObjectAt(n[Random(0,1)], x,y);

    PotionPickupRegist(pot);
    if (p)
        p[0]=pot;
}

void CreateMagicalStaff(float xpos, float ypos, int *pDest)
{
    int s= CreateObjectAt(GetWandName(), xpos, ypos);
    if(pDest)
     pDest[0]=s;
}

void CreateRandomItemProto(int functionId, float xpos, float ypos, int *ptr)
{
    Bind(functionId, &functionId+4);
}

void postItemCommon(int getitem)
{
    if (getitem)
    {
        if (GetUnitFlags(getitem) & UNIT_FLAG_NO_COLLIDE)
            UnitNoCollide(getitem);
        int durability=MaxHealth(getitem);

        if (durability)
        {
            durability*=4;
            if (durability<5000)
                SetUnitMaxHealth(getitem, durability);
        }
    }
}

void CreateRandomItemCommon(int posUnit)
{
    float xpos = GetObjectX(posUnit), ypos = GetObjectY(posUnit);

    Delete(posUnit);

    int getitem = 0;

    CreateRandomItemProto(GetItemCreateFunction(), xpos, ypos, &getitem);
    postItemCommon(getitem);
}

