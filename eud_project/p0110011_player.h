
#include "p0110011_gvar.h"
#include "p0110011_creskill.h"
#include "libs/unitstruct.h"
#include "libs/cmdline.h"
#include "libs/format.h"
#include "libs/buff.h"
#include "libs/playerinfo.h"
#include "libs/waypoint.h"
#include "libs/printutil.h"
#include "libs/network.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"
#include "libs/observer.h"
#include "libs/hash.h"

#define PLAYER_INITIAL_LOCATION 11
#define PLAYER_START_LOCATION 12
#define PLAYER_CONTROL_ROOM 13
#define PLAYER_DEATH_FLAG 0x80000000
#define CREATURE_INIT_SPEED 1.6

#define CREATURE_INIT_HP 40
#define CREATURE_INIT_DAMAGE 10
#define CREATURE_INCREASE_HP 10

#define WINDBOOSTER_DISTANCE 70.0

int GetAvataHash()
{
    int hash;

    if (!hash)
        HashCreateInstance(&hash);
    return hash;
}

int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

int GetAvata(int pIndex)//virtual
{}
void SetAvata(int pIndex, int cre)//virtual
{}

int PlayerClassCheckFlag(int pIndex, int flags)
{
    return GetPlayerFlags(pIndex) & flags;
}

void PlayerClassSetFlag(int pIndex, int flags)
{
    SetPlayerFlags(pIndex, GetPlayerFlags(pIndex)^flags);
}

/////Creatures///

void removeUserCreatureForce(int pIndex)
{
    int cre=GetAvata(pIndex);

    if (!cre) return;

    int nil;
    HashGet(GetAvataHash(), cre, &nil, TRUE);
    if (MaxHealth(cre))
        Delete(cre);
    SetAvata(pIndex,0);
}

int checkCorrectOwner(int owner)
{
    if (MaxHealth(owner))
    {
        int pIndex=GetPlayerIndex(owner);

        if (pIndex<0)
            return FALSE;

        return owner==GetPlayer(pIndex);
    }
    return FALSE;
}

#define REVIVE_DATA_CRE 0
#define REVIVE_DATA_OWNER 1
#define REVIVE_DATA_COUNTDOWN 2
#define REVIVE_DATA_MAX 3

void creatureReviveDurations(int *d)
{
    int owner=d[REVIVE_DATA_OWNER];

    if (checkCorrectOwner(owner))
    {
        if (--d[REVIVE_DATA_COUNTDOWN]>=0)
        {
            PushTimerQueue(1,d,creatureReviveDurations);
            return;
        }
        int pIndex=GetPlayerIndex(owner);

        removeUserCreatureForce(pIndex);
        respawnPlayerCreature(pIndex);
    }
    FreeSmartMemEx(d);
}

void onCreatureDeath()
{
    int owner = 0;

    HashGet(GetAvataHash(), GetTrigger(), &owner, TRUE);
    if (CurrentHealth(owner))
    {
        int *rvdata;

        AllocSmartMemEx(REVIVE_DATA_MAX*4,&rvdata);
        rvdata[REVIVE_DATA_CRE]=GetTrigger();
        rvdata[REVIVE_DATA_COUNTDOWN]=150;
        rvdata[REVIVE_DATA_OWNER]=owner;
        creatureReviveDurations(rvdata);
        UniPrint(owner, "당신의 캐릭터가 적에게 격추되었습니다");
    }
    DeleteObjectTimer(SELF, 300);
}

void onKillerianDeath()
{
    int owner;

    HashGet(GetAvataHash(), GetTrigger(), &owner, FALSE);
    int pIndex=GetPlayerIndex(owner);
    
    onCreatureDeath();
    if (pIndex<0)
        return;
    
    int dm= DummyUnitCreateById(OBJ_GHOST, GetObjectX(SELF),GetObjectY(SELF));
    UnitNoCollide(dm);
    SetAvata(pIndex, dm);
    DeleteObjectTimer(dm, 300);
}

int spawnCharacterDryad(float x,float y)
{
    int cre=CreateObjectById(OBJ_WIZARD_GREEN,x,y);

    WizardGreenSubProcess(cre);
    return cre;
}

int spawnCharacterHorrendous(float x,float y)
{
    int cre=CreateObjectById(OBJ_HORRENDOUS,x,y);

    HorrendousSubProcess(cre);
    Enchant(cre,EnchantList(ENCHANT_RUN),0.0);
    return cre;
}

int WizardBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1635412311; arr[1] = 25714; arr[17] = 100; arr[18] = 75; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 65536; arr[24] = 1067869798; arr[26] = 4; arr[27] = 4; 
		arr[53] = 1128792064; arr[54] = 4; arr[60] = 1327; arr[61] = 46910208; 
	pArr = arr;
	return pArr;
}

void WizardSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1069547520;		ptr[137] = 1069547520;
	int *hpTable = ptr[139];
	hpTable[0] = 100;	hpTable[1] = 100;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = WizardBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int spawnCharacterMystic(float x,float y)
{
    int cre=CreateObjectById(OBJ_WIZARD,x,y);

    WizardSubProcess(cre);
    return cre;
}

int DemonBinTable()
{
	int *pArr;

	if (pArr)	return pArr;
	int arr[62];

	arr[0] = 1869440324; arr[1] = 110; arr[17] = 200; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 65536; arr[24] = 1065353216; arr[26] = 4; arr[27] = 5; arr[28] = 1050253722; 
		arr[53] = 1128792064; arr[54] = 4; arr[58] = 5545472; arr[59] = 5542784; arr[60] = 1347; 
		arr[61] = 46910976; 
	pArr = arr;
	return pArr;
}

void DemonSubProcess(int sUnit)
{
	int *ptr = UnitToPtr(sUnit);

	if (!ptr) return;

	ptr[136] = 1077936128;		ptr[137] = 1077936128;
	int *hpTable = ptr[139];
	hpTable[0] = 200;	hpTable[1] = 200;
	int *uec = ptr[187];
	uec[360] = 65536;		uec[121] = DemonBinTable();
	uec[339] = 0;		uec[334] = 0;		uec[336] = 1065353216;
}

int spawnCharacterKillerian(float x,float y)
{
    int killer=CreateObjectById(OBJ_DEMON,x,y);
    DemonSubProcess(killer);
    return killer;
}

int invokeSpawnCharacter(int fn, float x, float y)
{
    return Bind(fn, &fn+4);
}

int createUserCreature(int user, int pIndex, float x,float y)
{
    int cre=invokeSpawnCharacter(GetCharacterSpawnFn(pIndex),x,y);
    int id = GetUserCharacterId(pIndex);

    if (id==3)
        SetCallback(cre,5,onKillerianDeath);
    else
        SetCallback(cre,5,onCreatureDeath);
    AggressionLevel(cre,0.0);
    ResumeLevel(cre,0.0);
    RetreatLevel(cre,0.0);
    SetUnitScanRange(cre, 1.0);
    SetOwner(user,cre);
    HashPushback(GetAvataHash(), cre, user);
    return cre;
}

void respawnPlayerCreature(int pIndex)
{
    if (MaxHealth(GetAvata(pIndex))) return;
    int resp=GetUserRespawnMark(pIndex),user=GetPlayer(pIndex);
    float x=GetObjectX(resp),y=GetObjectY(resp);
    int cre=createUserCreature(user,pIndex,x,y), lv=GetCreatureLevel(pIndex);
    SetUnitMaxHealth(cre,CREATURE_INIT_HP+(lv*CREATURE_INCREASE_HP));
    SetAvata(pIndex,cre);
    AttachHealthbar(cre);
    GreenSparkAt(x,y);
    PlaySoundAround(cre,SOUND_BlindOff);
}

////End/////

int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(GetPlayer(pIndex)))
        return GetPlayer(pIndex)==pUnit;
    return FALSE;
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

void PlayerClassOnShutdown(int pIndex)
{
    SetPlayer(pIndex,0);
    SetPlayerFlags(pIndex,0);
    ResetUserRespawnMarkPos(pIndex);

    char buff[128];

    NoxSprintfString(buff, "playeronshutdown. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void PlayerClassOnDeath(int pIndex, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

int userAwardCommon(int user, int pIndex, int flag)
{
    if (PlayerClassCheckFlag(pIndex, flag))
    {
        UniPrint(user, "이미 그 기술을 배우셨어요");
        return FALSE;
    }
    PlayerClassSetFlag(pIndex,flag);
    float point[]={GetObjectX(user),GetObjectY(user)};
    GreenSparkAt(point[0],point[1]);
    Effect("WHITE_FLASH",point[0],point[1],0.0,0.0);
    PlaySoundAround(user,SOUND_AwardSpell);
    return FALSE;
}

void onCreatureDefaultAttack(int cre, int pIndex)
{
    //DryadDefaultAttack
    Bind(GetCharacterDefaultAttack(pIndex), &cre);
}

void onFirstSkillCompleted(int pIndex)
{
    int user=GetPlayer(pIndex);

    if (CurrentHealth(user))
    {
        reportSubData(pIndex, SUBDATA_REPORT_SKILL_1, 1);
        UniPrint(user,"첫번째 스킬이 완료되었습니다");
    }
}
void onSecondSkillCompleted(int pIndex)
{
    int user=GetPlayer(pIndex);

    if (CurrentHealth(user))
    {
        reportSubData(pIndex,SUBDATA_REPORT_SKILL_2, 1);
        UniPrint(user,"두번째 스킬이 완료되었습니다");
    }
}
void onLastSkillCompleted(int pIndex)
{
    int user=GetPlayer(pIndex);

    if (CurrentHealth(user))
    {
        reportSubData(pIndex,SUBDATA_REPORT_SKILL_3, 1);
        UniPrint(user,"마지막 스킬이 완료되었습니다");
    }
}

void playerInputHanlder(int pIndex, int cre)
{
    int keyState=GetClientKeyInput(pIndex);
    float xVect=0,yVect=0,speed=GetCreatureSpeed(pIndex);
    int dir=0;

    if (keyState&KEY_L_CTRL_SHIFT)
    {
        onCreatureDefaultAttack(cre, pIndex);
        return;
    }
    if (keyState&KEY_Z_SHIFT)
    {
        if (DoFirstSkill(pIndex,cre, onFirstSkillCompleted))
            reportSubData(pIndex, SUBDATA_REPORT_SKILL_1, 2);
        return;
    }
    if (keyState&KEY_X_SHIFT)
    {
        if (DoSecondSkill(pIndex,cre,onSecondSkillCompleted))
            reportSubData(pIndex, SUBDATA_REPORT_SKILL_2, 2);
        return;
    }
    if (keyState&KEY_C_SHIFT)
    {
        if (DoLastSkill(pIndex,cre,onLastSkillCompleted))
            reportSubData(pIndex, SUBDATA_REPORT_SKILL_3, 2);
        return;
    }
    if (keyState&KEY_UP_SHIFT)
    {
        yVect-=speed;
        dir=192;
    }
    if (keyState&KEY_DOWN_SHIFT)
    {
        yVect+=speed;
        dir=64;
    }
    if (keyState&KEY_LEFT_SHIFT)
    {
        xVect-=speed;
        dir=128;
    }
    if (keyState&KEY_RIGHT_SHIFT)
    {
        xVect+=speed;
        dir=255;
    }
    if (ToInt(xVect) || ToInt(yVect))
    {
        Walk(cre,GetObjectX(cre)+xVect,GetObjectY(cre)+yVect);
        PushObjectTo(cre, xVect, yVect);
        if (GetDirection(cre)!=dir)
            LookWithAngle(cre, dir);
    }
}
#define REPORT_TIME_INTERVAL 5
void reportHpPointForCreature(int pIndex, int cre, int user)
{
    int fps[MAX_PLAYER_COUNT],*cfps=0x84ea04;

    if (ABS(cfps[0]-fps[pIndex])>REPORT_TIME_INTERVAL)
    {
        int prev = GetCreatureHpPrev(pIndex),curHp=CurrentHealth(cre);

        if (prev!=curHp)
        {
            int percent=(curHp*100)/MaxHealth(cre);

            if (++percent>100) percent=100;
            if (pIndex==31)
            {
                char *pb=CLIENT_SUBDATA_OFF;
                pb[0]=percent;
            }
            else
                ClientSetMemory(user, CLIENT_SUBDATA_OFF, percent);
            SetCreatureHpPrev(pIndex,curHp);
        }
        fps[pIndex]=cfps[0];
    }
}

void onAvataProc(int pIndex, int user, int cre)
{
    if (CheckWatchFocus(    user ))
        PlayerLook(user, cre);

    if (!CurrentHealth(cre))
        return;

    playerInputHanlder(pIndex,cre);
    reportHpPointForCreature(pIndex,cre,user);
}

void PlayerClassOnAlive(int pIndex, int user)
{
    int cre=GetAvata(pIndex);

    if (MaxHealth(cre))
        onAvataProc(pIndex, user, cre);
}

void PlayerClassOnLoop(int pIndex)
{
    int user = GetPlayer(pIndex);

    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (GetPlayerFlags(pIndex))
        {
            PlayerClassOnShutdown(pIndex);
            return;
        }
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

void PlayerInitCallbackStorage(int **p)
{
    int cb;
    p[0]=&cb;
}

void testOnlyAllSktelecom(int pIndex)
{
    //test
    awardSecondSkill(pIndex, 40, GetPlayer(pIndex));
    awardLastSkill(pIndex, 40, GetPlayer(pIndex));
    awardLastSkillHorren(pIndex, 40, GetPlayer(pIndex));
}

void initHeroDryad(int pIndex)
{
    SetCharacterSpawnFn(pIndex, spawnCharacterDryad);
    SetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE,CREATURE_INIT_DAMAGE+6);
    SetDefaultStat(pIndex, DEFAULT_STAT_HP,CREATURE_INIT_HP);
    SetCreatureSpeed(pIndex,CREATURE_INIT_SPEED);
    SetCharacterDefaultAttack(pIndex, DryadDefaultAttack);
}
void initHeroHorrendous(int pIndex)
{
    SetCharacterSpawnFn(pIndex, spawnCharacterHorrendous);
    SetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE,CREATURE_INIT_DAMAGE+2);
    SetDefaultStat(pIndex, DEFAULT_STAT_HP,CREATURE_INIT_HP+10);
    SetCreatureSpeed(pIndex,CREATURE_INIT_SPEED+0.3);
    SetCharacterDefaultAttack(pIndex, HorrendousDefaultAttack);
}
void initHeroMystic(int pIndex)
{
    SetCharacterSpawnFn(pIndex, spawnCharacterMystic);
    SetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE,CREATURE_INIT_DAMAGE+1);
    SetDefaultStat(pIndex, DEFAULT_STAT_HP,CREATURE_INIT_HP+2);
    SetCreatureSpeed(pIndex,CREATURE_INIT_SPEED);
    SetCharacterDefaultAttack(pIndex, MysticDefaultAttack);
}

void initHeroKillerian(int pIndex)
{
    SetCharacterSpawnFn(pIndex, spawnCharacterKillerian);
    SetDefaultStat(pIndex, DEFAULT_STAT_DAMAGE,CREATURE_INIT_DAMAGE);
    SetDefaultStat(pIndex, DEFAULT_STAT_HP,CREATURE_INIT_HP+6);
    SetCreatureSpeed(pIndex,CREATURE_INIT_SPEED);
    SetCharacterDefaultAttack(pIndex, KillerianDefaultAttack);
}

void invokeInitHero(int fn, int pIndex)
{
    Bind(fn,&pIndex);
}

void initUserCharacter(int pIndex)
{
    int fns[]={initHeroDryad,initHeroHorrendous,initHeroMystic,initHeroKillerian,};
    invokeInitHero(fns[GetUserCharacterId(pIndex)], pIndex);        
}

void PlayerClassOnInit(int pIndex, int pUnit)
{
    SetPlayer(pIndex, pUnit);
    SetPlayerFlags(pIndex,1);

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        int *pCb;

        PlayerInitCallbackStorage(&pCb);
        if (pCb[0])
            Bind(pCb[0], &pIndex);
        // else
        //     commonServerClientProcedure();
        // PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    }
    // m_clientKeyState[ plr ] = 0;
    SetClientKeyInput(pIndex,0);
    ChangeGold(pUnit, -GetGold(pUnit));
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
    // SelfDamageClassEntry(pUnit);
    // DiePlayerHandlerEntry(pUnit);
    // EmptyAll(pUnit);
    ResetUserRespawnMarkPos(pIndex);
    removeUserCreatureForce(pIndex);
    initUserCharacter(pIndex);

    // char buff[128];

    // NoxSprintfString(buff, "playeroninit. index-%d", &pIndex, 1);
    // NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
    SetCreatureHpPrev(pIndex,0);
    SetCreatureLevel(pIndex,0);
    SetUserSkill1(pIndex,0);
    SetUserSkill2(pIndex,0);
    SetUserSkill3(pIndex,0);
    SetUserCooldown1(pIndex,0);
    SetUserCooldown2(pIndex,0);
    SetUserCooldown3(pIndex,0);
    SetDamageUpgradeLevel(pIndex,0);
    SetHpUpgradeLevel(pIndex,0);
    SetSpeedUpgradeLevel(pIndex,0);
    // PushTimerQueue(10, pIndex, testOnlyAllSktelecom);    //testonly
}

void PlayerClassOnJoin(int user, int pIndex)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
    SetUnitEnchantCopy(user, GetLShift(ENCHANT_ANTI_MAGIC)|GetLShift(ENCHANT_ANCHORED)|GetLShift(ENCHANT_FREEZE)|GetLShift(ENCHANT_INVULNERABLE));
    EmptyAll(user);    
    MoveObject(user, LocationX(PLAYER_CONTROL_ROOM), LocationY(PLAYER_CONTROL_ROOM));
    respawnPlayerCreature(pIndex);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int pIndex)
{
    PlayerClassOnJoin(user, pIndex);
    UniPrint(user, "<<--- !캐릭터 키우기 -미니버전! ----- 제작. 237 -----------------<<<<");
}

void PlayerClassOnEntry(int plrUnit)
{
    if (!CurrentHealth(plrUnit))
        return;

    int pIndex = GetPlayerIndex(plrUnit);
    char initialUser=FALSE;

    if (!MaxHealth(GetPlayer(pIndex)))
        PlayerClassOnInit(pIndex, plrUnit);
    if (pIndex >= 0)
    {
        if (initialUser)
            OnPlayerDeferredJoin(plrUnit, pIndex);
        else
            PlayerClassOnJoin(plrUnit, pIndex);
        return;
    }
    PlayerClassOnEntryFailure(plrUnit);
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC));
        if (checkIsPlayerAlive(GetPlayerIndex(OTHER), GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else
            MoveObject(OTHER, LocationX(PLAYER_INITIAL_LOCATION), LocationY(PLAYER_INITIAL_LOCATION));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

int getNextExp(int lv)
{
    int *p;

    if (!p)
    {
        int expTable[]={
            13, 32,61,98,141,194,
255,328,411,514,
623,754,903,1066,1245,
1438,1649,1882,2139,2416,
2727,3074,3447,3848,4291,
4752,5251,5808,6421,7068,
7751,8478,9247,10058,10915,
11822,12775,13766,14797,15900,
17101,18422,19881,21448,23181,
25004,26935,28934,31047,33606,
            9999999,
        };
        p=expTable;
    }
    if (lv<0) return 0;
    return p[lv];
}

void reportSubData(int pIndex, int n, int byteValue)
{
    char *p=CLIENT_SUBDATA_OFF;
    int user=GetPlayer(pIndex);

    if (pIndex==31)
    {
        p[n]=byteValue;
        return;
    }
    ClientSetMemory(user, &p[n], byteValue);
}

void awardFirstSkill(int pIndex, int lv, int user)
{
    char buff[256], *p=CLIENT_SUBDATA_OFF;
    int id= GetUserCharacterId(pIndex);

    if (id==0) SetUserSkill1(pIndex, UserSkillMegaTrain);
    else if (id==1) return;
    else if (id==2) return;
    else if (id==3) SetUserSkill1(pIndex, UserSkillSummonFireboom);

    NoxSprintfString(buff, "레밸 %d 이 되셨습니다, 지금부터 첫번째 마법을 사용할 수 있게 되었습니다", &lv, 1);
    UniPrint(user, ReadStringAddressEx(buff));
    reportSubData(pIndex, SUBDATA_REPORT_SKILL_1, 1);
}

void awardFirstSkillHorren(int pIndex, int lv, int user)
{
    char buff[256], *p=CLIENT_SUBDATA_OFF;
    int id= GetUserCharacterId(pIndex);

    if (id==0) return;
    else if (id==1) SetUserSkill1(pIndex, UserSkillHarpoon);
    else if (id==2) SetUserSkill1(pIndex, UserSkillRealHarpoon);
    NoxSprintfString(buff, "레밸 %d 이 되셨습니다, 지금부터 첫번째 마법을 사용할 수 있게 되었습니다", &lv, 1);
    UniPrint(user, ReadStringAddressEx(buff));
    reportSubData(pIndex, SUBDATA_REPORT_SKILL_1, 1);
}
void awardSecondSkill(int pIndex, int lv, int user)
{
    char buff[256], *p=CLIENT_SUBDATA_OFF;
    int id= GetUserCharacterId(pIndex);

    if (id==0) SetUserSkill2(pIndex, UserSkillManaExplosion);
    else if (id==1) SetUserSkill2(pIndex, UserSkillMultipleShot);
    else if (id==2) SetUserSkill2(pIndex, UserSkillSummonBlackShadow);
    else if (id==3) SetUserSkill2(pIndex, UseSkillFireRing);

    NoxSprintfString(buff, "레밸 %d 이 되셨습니다, 지금부터 2번째 마법을 사용할 수 있게 되었습니다", &lv, 1);
    UniPrint(user, ReadStringAddressEx(buff));
    reportSubData(pIndex, SUBDATA_REPORT_SKILL_2, 1);
}
void awardLastSkill(int pIndex, int lv, int user)
{
    char buff[256], *p=CLIENT_SUBDATA_OFF;
    int id= GetUserCharacterId(pIndex);

    if (id==0) SetUserSkill3(pIndex, UserSkillAutoDeathray);
    else if (id==1) return;
    else if (id==2) return;
    NoxSprintfString(buff, "레밸 %d 이 되셨습니다, 지금부터 마지막 마법을 사용할 수 있게 되었습니다", &lv, 1);
    UniPrint(user, ReadStringAddressEx(buff));
    reportSubData(pIndex, SUBDATA_REPORT_SKILL_3, 1);
    SetUserSkill3(pIndex, UserSkillAutoDeathray);
}
void awardLastSkillHorren(int pIndex, int lv, int user)
{
    char buff[256], *p=CLIENT_SUBDATA_OFF;
    int id= GetUserCharacterId(pIndex);

    if (id==0) return;
    else if (id==1) SetUserSkill3(pIndex, UserSkillSpreadRing);
    else if (id==2) SetUserSkill3(pIndex, UserSkillBlackHole);
    else if (id==3) SetUserSkill3(pIndex, UseSkillSpitFlame);
    NoxSprintfString(buff, "레밸 %d 이 되셨습니다, 지금부터 마지막 마법을 사용할 수 있게 되었습니다", &lv, 1);
    UniPrint(user, ReadStringAddressEx(buff));
    reportSubData(pIndex, SUBDATA_REPORT_SKILL_3, 1);
}
void termIncreaseSpeed(int pIndex, int lv, int user)
{
    SetCreatureSpeed(pIndex, GetCreatureSpeed(pIndex)+0.2);
}

void levelUpEventProto(int fn, int pIndex, int lv, int user)
{
    Bind(fn, &pIndex);
}

void checkLevelUpEvent(int pIndex, int lv, int user)
{
    short *p;

    if (!p)
    {
        short events[]={
            0,termIncreaseSpeed,0,0,0,
            awardFirstSkill,awardFirstSkillHorren,termIncreaseSpeed,0,0, //10
            0,0,termIncreaseSpeed,0,0,
            awardSecondSkill,0,0,0,0, //20
            0,termIncreaseSpeed,0,0,0,
            awardLastSkillHorren,awardLastSkill,0,0,termIncreaseSpeed, //30
            0,0,0,0,termIncreaseSpeed,
            0,0,0,0,termIncreaseSpeed,
            0,0,0,0,0,
            0,termIncreaseSpeed,0,0,0,
            0,
        };
        p=events;
    }
    if (p[lv])
        levelUpEventProto(p[lv],pIndex, lv, user);
}

int levelUpProcedure(int pIndex, int exp, int lv, int user, int cre)
{
    while (exp >= getNextExp(lv))
    {
        UniPrint(user, "============레밸 업===============");
        SetCreatureLevel(pIndex, ++lv);
        SetUnitMaxHealth(cre,CREATURE_INIT_HP+(lv*CREATURE_INCREASE_HP));
        DeleteObjectTimer( CreateObjectById(OBJ_LEVEL_UP, GetObjectX(cre),GetObjectY(cre)), 30);
        PlaySoundAround(cre,SOUND_LevelUp);
        reportSubData(pIndex,SUBDATA_REPORT_LEVEL,lv);
        checkLevelUpEvent(pIndex, lv, user);
    }
    return getNextExp(lv);
}

void GiveExpToPlayer(int pIndex, int exp)
{
    if (exp<=0) return;
    int lv= GetCreatureLevel(pIndex), setTo=GetCreatureExp(pIndex)+exp;
    int user=GetPlayer(pIndex),cre=GetAvata(pIndex);

    if (!CurrentHealth(user))
        return;
    if (!CurrentHealth(cre))
        return;
    SetCreatureExp(pIndex,setTo);
    int goal = levelUpProcedure(pIndex, setTo, lv, user,cre);

    lv=GetCreatureLevel(pIndex);
    int prev= getNextExp(lv-1);
    reportSubData(pIndex, SUBDATA_REPORT_EXP, ((setTo-prev)*100)/(goal-prev));
}

void doUpgradeDamage()
{
    char buff[192];
    if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(OTHER);

        if (IsPlayerUnit(owner))
        {
            int pIndex=GetPlayerIndex(owner);
            int prevLv=GetDamageUpgradeLevel(pIndex);
            int pay =ComputeDamageUpPay(prevLv);

            if (GetGold(owner) >= pay)
            {
                ChangeGold(owner,-pay);
                UpdatePlayerGold(owner);
                SetDefaultStat(pIndex,DEFAULT_STAT_DAMAGE,GetDefaultStat(pIndex,DEFAULT_STAT_DAMAGE)+2);
                SetDamageUpgradeLevel(pIndex,++prevLv);
                reportSubData(pIndex, SUBDATA_REPORT_DAMAGE_UP, prevLv);
                reportSubData(pIndex,SUBDATA_REPORT_UPDATE_FLAGS,1);
                GreenSparkAt(GetObjectX(OTHER),GetObjectY(OTHER));
                NoxSprintfString(buff, "데미지업그레이드 완료! 현재 당신의 레밸은 %d 입니다", &prevLv,1);
                return;
            }
            NoxSprintfString(buff,"금화가 부족합니다. 가격은 %d 금 입니다", &pay, 1);
            UniPrint(owner, ReadStringAddressEx(buff));
        }
    }
}

#define INCREASE_UP_HP 10
void doUpgradeHp()
{
    char buff[192];
    if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(OTHER);
        if (IsPlayerUnit(owner))
        {
            int pIndex=GetPlayerIndex(owner);
            int lv=GetHpUpgradeLevel(pIndex);
            int pay=ComputeHpUpPay(lv);
            if (GetGold(owner)>=pay)
            {
                ChangeGold(owner,-pay);
                UpdatePlayerGold(owner);
                SetDefaultStat(pIndex,DEFAULT_STAT_HP,GetDefaultStat(pIndex,DEFAULT_STAT_HP)+INCREASE_UP_HP);
                int cre=GetAvata(pIndex);
                SetUnitMaxHealth(cre, MaxHealth(cre)+ INCREASE_UP_HP);
                SetHpUpgradeLevel(pIndex,++lv);
                reportSubData(pIndex, SUBDATA_REPORT_HP_UP, lv);
                reportSubData(pIndex,SUBDATA_REPORT_UPDATE_FLAGS,2);
                Effect("VIOLET_SPARKS",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
                NoxSprintfString(buff, "HP업그레이드 완료! 현재 당신의 레밸은 %d 입니다", &lv,1);
                return;
            }
            NoxSprintfString(buff,"금화가 부족합니다. 가격은 %d 금 입니다", &pay, 1);
            UniPrint(owner, ReadStringAddressEx(buff));
        }
    }
}

#define INCREASE_UP_SPEED 0.1
void doUpgradeSpeed()
{
    char buff[192];
    if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(OTHER);
        if (IsPlayerUnit(owner))
        {
            int pIndex=GetPlayerIndex(owner);
            int lv=GetSpeedUpgradeLevel(pIndex);
            int pay=ComputeSpeedUpPay(lv);
            if (GetGold(owner)>=pay)
            {
                ChangeGold(owner,-pay);
                UpdatePlayerGold(owner);
                SetCreatureSpeed(pIndex,GetCreatureSpeed(pIndex)+INCREASE_UP_SPEED);
                int cre=GetAvata(pIndex);
                SetSpeedUpgradeLevel(pIndex,++lv);
                reportSubData(pIndex, SUBDATA_REPORT_SPEED_UP, lv);
                reportSubData(pIndex,SUBDATA_REPORT_UPDATE_FLAGS,4);
                Effect("YELLOW_SPARKS",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
                NoxSprintfString(buff, "이동속도 업그레이드 완료! 현재 당신의 레밸은 %d 입니다", &lv,1);
                return;
            }
            NoxSprintfString(buff,"금화가 부족합니다. 가격은 %d 금 입니다", &pay, 1);
            UniPrint(owner, ReadStringAddressEx(buff));
        }
    }
}

void doExchangeMoneyToExp()
{
    if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(OTHER);
        if (IsPlayerUnit(owner))
        {
            int pIndex=GetPlayerIndex(owner);
            float point[]={GetObjectX(OTHER),GetObjectY(OTHER)};

            if (GetGold(owner)>=25)
            {
                ChangeGold(owner,-25);
                UpdatePlayerGold(owner);
                GiveExpToPlayer(pIndex,10);
                PlaySummonEffect(point, OBJ_WILL_O_WISP_GENERATOR, 38);
                UniPrint(owner, "금화25을 경험치 10으로 전환했습니다");
                return;
            }
            UniPrint(owner, "금화가 부족합니다, 가격은 25골드");
        }
    }
}

void doExchangeMoneyToExp5X()
{
    if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(OTHER);
        if (IsPlayerUnit(owner))
        {
            int pIndex=GetPlayerIndex(owner);
            float point[]={GetObjectX(OTHER),GetObjectY(OTHER)};

            if (GetGold(owner)>=110)
            {
                ChangeGold(owner,-110);
                UpdatePlayerGold(owner);
                GiveExpToPlayer(pIndex,50);
                PlaySummonEffect(point, OBJ_WILL_O_WISP_GENERATOR, 38);
                UniPrint(owner, "금화110을 경험치 50으로 전환했습니다");
                return;
            }
            UniPrint(owner, "금화가 부족합니다, 가격은 110골드");
        }
    }
}
void doGodmode60Seconds()
{
    if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(OTHER);
        if (IsPlayerUnit(owner))
        {
            int pIndex=GetPlayerIndex(owner);
            float point[]={GetObjectX(OTHER),GetObjectY(OTHER)};

            if (GetGold(owner)>=5)
            {
                ChangeGold(owner,-5);
                UpdatePlayerGold(owner);
                RestoreHealth(OTHER,9999);
                Enchant(OTHER,EnchantList(ENCHANT_INVULNERABLE),60.0);
                Effect("BLUE_SPARKS",point[0],point[1],0.0,0.0);
                UniPrint(owner, "체력 풀 회복+1분동안 무적입니다, 공격 시 풀릴 수도 있음");
                return;
            }
            UniPrint(owner, "금화가 부족합니다, 가격은 5골드");
        }
    }
}

void teleportLastTransmission()
{
    if (CurrentHealth(OTHER))
    {
        int owner=GetUnitTopParent(OTHER);
        if (!IsPlayerUnit(owner))
            return;
        
        int last=GetLastTransmission();

        Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
        MoveObject(OTHER,GetObjectX(last),GetObjectY(last));
        Effect("TELEPORT",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
    }
}

void InitializeUpgradeSystem()
{
    float size[]={13.0,13.0};
    int cb[]={0,0,0,};
    float point1[]={LocationX(89),LocationY(89)};
    cb[0]=doUpgradeDamage;
    CreateInvisibleBeacon(NULLPTR, point1,size,UNIT_CLASS_MONSTER,cb);
    float point2[]={LocationX(90),LocationY(90)};
    cb[0]=doUpgradeHp;
    CreateInvisibleBeacon(NULLPTR, point2,size,UNIT_CLASS_MONSTER,cb);
    float point3[]={LocationX(91),LocationY(91)};
    cb[0]=doExchangeMoneyToExp;
    CreateInvisibleBeacon(NULLPTR, point3,size,UNIT_CLASS_MONSTER,cb);
    float point4[]={LocationX(165),LocationY(165)};
    cb[0]=doUpgradeSpeed;
    CreateInvisibleBeacon(NULLPTR, point4,size,UNIT_CLASS_MONSTER,cb);
    float point5[]={LocationX(166),LocationY(166)};
    cb[0]=teleportLastTransmission;
    CreateInvisibleBeacon(NULLPTR, point5,size,UNIT_CLASS_MONSTER,cb);
    float point6[]={LocationX(472),LocationY(472)};
    cb[0]=doExchangeMoneyToExp5X;
    CreateInvisibleBeacon(NULLPTR, point6,size,UNIT_CLASS_MONSTER,cb);
    float point7[]={LocationX(473),LocationY(473)};
    cb[0]=doGodmode60Seconds;
    CreateInvisibleBeacon(NULLPTR, point7,size,UNIT_CLASS_MONSTER,cb);
}
