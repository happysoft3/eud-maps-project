
#include "noxscript\builtins.h"
#include "libs\typecast.h"
#include "libs\callmethod.h"
#include "libs\mathlab.h"
#include "libs\unitstruct.h"
#include "libs\unitutil.h"
#include "libs\printutil.h"
#include "libs\username.h"
#include "libs\waypoint.h"

#define NULLPTR 0

int player[20];
int Gvar_3[20];
int Gvar_4 = 0; //게임승리 시 값이 1이 됩니다.
int Gvar_5[10]; //플레이어의 주행거리

int CheckPlayerDeathFlag(int plr)
{
    return player[plr + 10] & 0x02;
}

void SetPlayerDeathFlag(int plr)
{
    player[plr + 10] ^=0x02;
}

int PlayerClassOnInit(int plr, int pUnit)
{
    player[plr] = pUnit;
    player[plr + 10] = 1;

    return plr;
}

void Nop(int n)
{ }

void MoveObjectVector(int unit, float xVect, float yVect)
{
    int *ptr = UnitToPtr(unit);

    if (ptr != NULLPTR)
    {
        float *coor = ptr;
        MoveObject(unit, coor[14] + xVect, coor[15] + yVect);
    }
}

void getPlayers()
{
    int i, plr;

    if (CurrentHealth(other))
    {
        plr = CheckPlayer();
        for (i = 0 ; i < 10 && plr < 0 ; Nop(i ++))
        {
            if (!MaxHealth(player[i]))
            {
                plr = PlayerClassOnInit(i, GetCaller());
                break;
            }
        }
        if (plr + 1)
            respawnPlayer(plr);
        else
            playerIsFull(other);
    }
}

void PlayerOnDeath(int plr)
{
    UniPrint(player[plr], "죽으셨습니다");
}

void PlayerOnShutdown(int plr)
{
    if (HasEnchant(player[plr], "ENCHANT_CROWN"))
        EnchantOff(player[plr], "ENCHANT_CROWN");
    CancelTimer(Gvar_3[plr + 10]);
    Frozen(playerMover(plr), 0);
    MoveObject(playerMover(plr), GetWaypointX(13), GetWaypointY(13));
}

void loopPreservePlayers()
{
    int i;

    for (i = 9 ; i >= 0 ; i --)
    {
        while (1)
        {
            if (MaxHealth(player[i]))
            {
                if (GetUnitFlags(player[i]) & 0x40)
                    1;
                else if (CurrentHealth(player[i]))
                {
                    if (!HasEnchant(player[i], "ENCHANT_VILLAIN") && HasEnchant(player[i], "ENCHANT_CROWN"))
                        MoveObject(Gvar_3[i], GetObjectX(player[i]), GetObjectY(player[i]));
                    break;
                }
                else
                {
                    if (!CheckPlayerDeathFlag(i))
                    {
                        SetPlayerDeathFlag(i);
                        PlayerOnDeath(i);
                    }
                    break;
                }
            }
            if (player[i + 10])
            {
                PlayerOnShutdown(i);
                player[i] = 0;
                player[i + 10] = 0;
            }
            break;
        }
    }
    FrameTimer(1, loopPreservePlayers);
}

void pushingPlayer()
{
    int plrscrindex = GetDirection(GetTrigger() + 1);

    if (CurrentHealth(player[plrscrindex]) > 0 && !Gvar_4)
    {
        if (IsCaller(player[plrscrindex]))
        {
            MoveObject(self, GetObjectX(player[plrscrindex]), GetObjectY(player[plrscrindex]));
            if (!HasEnchant(player[plrscrindex], "ENCHANT_ETHEREAL"))
            {
                Enchant(player[plrscrindex], "ENCHANT_ETHEREAL", 1.0);
                Enchant(player[plrscrindex], "ENCHANT_VILLAIN", 2.0);
                Gvar_5[plrscrindex] ++;
            }
        }
        else if (!HasEnchant(player[plrscrindex], "ENCHANT_LIGHT") && !HasClass(other, "TRIGGER"))
        {
            UniChatMessage(self, "충돌해버렸다...!, 주행거리: " + IntToString(Gvar_5[plrscrindex]) + "m", 150);
            Effect("SPARK_EXPLOSION", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
            Effect("EXPLOSION", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
            MoveWaypoint(14, GetObjectX(self), GetObjectY(self));
            AudioEvent("BurnCast", 14);
            AudioEvent("BomberDie", 14);
            EnchantOff(player[plrscrindex], "ENCHANT_INVULNERABLE");
            Damage(player[plrscrindex], 0, 255, 14);
            Frozen(self, 0);
            MoveObject(self, GetWaypointX(13), GetWaypointY(13));
        }
    }
}

void respawnPlayer(int plrscrindex)
{
    if (!HasEnchant(player[plrscrindex], "ENCHANT_CROWN"))
        FrameTimerWithArg(3, player[plrscrindex], PlayerDisplayCountdown);
    if (CheckPlayerDeathFlag(plrscrindex))
        SetPlayerDeathFlag(plrscrindex);
    removeAllInvens(other);
    MoveObject(player[plrscrindex], GetWaypointX(plrscrindex + 3), GetWaypointY(plrscrindex + 3));
    Enchant(player[plrscrindex], "ENCHANT_FREEZE", 5.0);
    Enchant(player[plrscrindex], "ENCHANT_LIGHT", 8.0);
    DeleteObjectTimer(CreateObject("BlueRain", plrscrindex + 3), 7);
    Effect("TELEPORT", GetObjectX(player[plrscrindex]), GetObjectY(player[plrscrindex]), 0.0, 0.0);
    AudioEvent("BlindOff", plrscrindex + 3);
    Enchant(player[plrscrindex], "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(player[plrscrindex], "ENCHANT_ANCHORED", 0.0);
    Gvar_5[plrscrindex] = 0;
    Gvar_3[plrscrindex + 10] = FrameTimerWithArg(125, plrscrindex, attachedSki);
}

void attachedSki(int plrscrindex)
{
    if (CurrentHealth(player[plrscrindex]) && !Gvar_4)
    {
        UniChatMessage(player[plrscrindex], "출발~~!", 120);
        Enchant(player[plrscrindex], "ENCHANT_CROWN", 0.0);
        Enchant(player[plrscrindex], "ENCHANT_ETHEREAL", 2.0);
        Enchant(player[plrscrindex], "ENCHANT_VILLAIN", 4.0);
        Frozen(Gvar_3[plrscrindex], 1);
        MoveObject(Gvar_3[plrscrindex], GetObjectX(player[plrscrindex]), GetObjectY(player[plrscrindex]));
        Effect("VIOLET_SPARKS", GetObjectX(player[plrscrindex]), GetObjectY(player[plrscrindex]), 0.0, 0.0);
        AudioEvent("FumbleEffect", plrscrindex + 3);
        AudioEvent("MetallicBong", plrscrindex + 3);
        AudioEvent("GlyphCast", plrscrindex + 3);
    }
}

void playerIsFull(int pUnit)
{
    UniPrint(pUnit, "이 맵이 수용할 수 있는 플레이어 수의 허용치를 초과했기 때문에 더 이상 입장이 불가능합니다.");
    MoveObject(pUnit, GetWaypointX(12), GetWaypointY(12));
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);
}

void removeAllInvens(int holder)
{
    while (GetLastItem(holder))
        Delete(GetLastItem(holder));
}

int CheckPlayer()
{
    int i;

    for (i = 0 ; i < 10 ; Nop(i ++))
    {
        if (IsCaller(player[i]))
            return i;
    }
    return -1;
}


int playerMover(int plr)
{
    int unit[10];

    if (plr == -1)
    {
        int i;

        for (i = 0 ; i < 10 ; i ++) {
            Gvar_3[i] = CreateObject("maiden", 13);
            SetCallback(Gvar_3[i], 9, pushingPlayer);
            LookWithAngle(CreateObject("sword", 13), i);
            // MoveWaypoint(13, GetWaypointX(13) + 20.0, GetWaypointY(13) - 20.0);
            TeleportLocationVector(13, 20.0, -20.0);
        }
        return unit[0];
    }
    return unit[plr];
}

void placedUnits() {
    int var_0;

    Frozen(CreateObject("Ankh", 16), 1);
    var_0 = CreateObject("maiden", 16);
    Frozen(var_0, 1);
    SetCallback(var_0, 9, touchedVictory);

    MoveWaypoint(18, GetWaypointX(19) - 87.0, GetWaypointY(19));
    Frozen(CreateObject("BarrelLOTD", 18), 1);
    var_0 = CreateObject("maiden", 18);
    LookWithAngle(var_0, 0);
    Frozen(var_0, 1);
    SetCallback(var_0, 9, movingCircle);

    Frozen(CreateObject("BarrelLOTD", 20), 1);
    var_0 = CreateObject("maiden", 20);
    LookWithAngle(var_0, 0);
    Frozen(var_0, 1);
    SetCallback(var_0, 9, movingCircle);

    Frozen(CreateObject("BarrelLOTD", 21), 1);
    var_0 = CreateObject("maiden", 21);
    LookWithAngle(var_0, 0);
    Frozen(var_0, 1);
    SetCallback(var_0, 9, movingCircle);

    Frozen(CreateObject("SmallStoneBlock", 22), 1);
    var_0 = CreateObject("maiden", 22);
    LookWithAngle(var_0, 0);
    Frozen(var_0, 1);
    SetCallback(var_0, 9, patrolMoving);

    Frozen(CreateObject("SmallStoneBlock", 23), 1);
    var_0 = CreateObject("maiden", 23);
    LookWithAngle(var_0, 1);
    Frozen(var_0, 1);
    SetCallback(var_0, 9, patrolMoving);
}

void movingCircle()
{
    int angle = GetDirection(self);

    if (IsCaller(GetTrigger() - 1))
    {
        if (angle < 180)
        {
            // MoveObject(other, GetObjectX(other) + MathSine(angle * 2, 3.2), GetObjectY(other) + MathSine(angle * 2 + 90, 3.2));
            MoveObjectVector(other, MathSine(angle * 2, 3.2), MathSine(angle * 2 + 90, 3.2));
            LookWithAngle(self, ++angle);
        }
        else
            LookWithAngle(self, 0);
        MoveObject(self, GetObjectX(other), GetObjectY(other));
    }
}

void patrolMoving()
{
    int subsub = GetTrigger() - 1;

    if (!GetDirection(subsub)) {
        if (GetObjectX(subsub) + GetObjectY(subsub) <= 6761.0)
            MoveObjectVector(subsub, 2.0, 2.0);
        else
            LookWithAngle(subsub, 1);
    }
    else {
        if (GetObjectX(subsub) + GetObjectY(subsub) >= 5886.0)
            MoveObjectVector(subsub, -2.0, -2.0);
        else
            LookWithAngle(subsub, 0);
    }
    MoveObject(self, GetObjectX(other), GetObjectY(other));
}

void touchedVictory()
{
    int once;

    if (!once && IsPlayerUnit(other))
    {
        int plr = CheckPlayer();
        if (plr >= 0)
        {
            UniPrintToAll("지금, 스키 레이스의 승자가 결정되었습니다.");
            Effect("YELLOW_SPARKS", GetObjectX(self), GetObjectY(self), 0.0, 0.0);
            MoveWaypoint(14, GetObjectX(self), GetObjectY(self));
            AudioEvent("AwardLife", 14);
            once = 1;
            Gvar_4 = 1;
            playWav("StaffOblivionAchieve3");
            resetMovers();
            FrameTimerWithArg(100, plr, showWinnerPlayer);
        }
    }
}

void resetMovers()
{
    int i;

    for (i = 0 ; i < 10 ; i ++)
    {
        Frozen(Gvar_3[i], 0);
        MoveObject(Gvar_3[i], GetWaypointX(13), GetWaypointY(13));
    }
}

void showWinnerPlayer(int plr)
{
    int winner = player[plr];

    MoveObject(Object("playerStartLocation"), GetWaypointX(17), GetWaypointY(17));
    teleportAllPlayers(17);
    Enchant(winner, "ENCHANT_VAMPIRISM", 0.0);
    UniPrintToAll("이번 게임에서는 플레이어" + PlayerIngameNick(winner) + " 님께서 1등을 차지하셨습니다.");
    UniPrintToAll("1위 플레이어의 주행거리는 " + IntToString(Gvar_5[plr]) + "m 입니다.");
    UniPrintToAll("이번 게임의 MVP 는 " + PlayerIngameNick(winner) + " 님이 되셨습니다, 축하합니다.");
    Effect("WHITE_FLASH", LocationX(17), LocationY(17), 0.0, 0.0);
    TeleportLocation(17, GetObjectX(winner), GetObjectY(winner));
    DeleteObjectTimer(CreateObject("blueRain", 17), 600);
    AudioEvent("BigGong", 17);
    AudioEvent("HecubahDieFrame0A", 17);
}

void teleportAllPlayers(int wp) {
    int i;

    for (i = 0 ; i < 10 ; i ++) {
        if (CurrentHealth(player[i]) > 0) {
            MoveObject(player[i], GetWaypointX(wp) + (IntToFloat(i) * 23.0), GetWaypointY(wp) - (IntToFloat(i) * 23.0));
            Enchant(player[i], "ENCHANT_FREEZE", 5.0);
        }
    }
}

void setMapLighting(int min, int max) {
    int i;

    for (i = min ; i <= max ; i ++)
        CreateObject("InvisibleLightBlueHigh", i);
}

void playWav(string arg_0) {
    int i;

    for (i = 0 ; i < 10 ; i ++) {
        if (CurrentHealth(player[i]) > 0) {
            MoveWaypoint(14, GetObjectX(player[i]), GetObjectY(player[i]));
            AudioEvent(arg_0, 14);
        }
    }
}

void StrNum3()
{
	int arr[6], i, count = 0;
	string name = "CharmOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 8921336; arr[1] = 4196361; arr[2] = 8392578; arr[3] = 604012560; arr[4] = 136446528; arr[5] = 124; 
	for (i = 0 ; i < 6 ; i ++)
		count = DrawStrNum3(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrNum3(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 186 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 11 == 10)
            TeleportLocationVector(1, -20.0, 2.0);
		else
            TeleportLocationVector(1, 2.0, 0.0);
		count ++;
	}
	return count;
}

void StrNum2()
{
	int arr[6], i, count = 0;
	string name = "CharmOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 8921336; arr[1] = 4212745; arr[2] = 1075841026; arr[3] = 1074266625; arr[4] = 1049088; arr[5] = 511; 
	for (i = 0 ; i < 6 ; i ++)
		count = DrawStrNum2(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrNum2(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 186 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 11 == 10)
            TeleportLocationVector(1, -20.0, 2.0);
		else
            TeleportLocationVector(1, 2.0, 0.0);
		count ++;
	}
	return count;
}

void StrNum1()
{
	int arr[6], i, count = 0;
	string name = "CharmOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 234946592; arr[1] = 537133184; arr[2] = 1049088; arr[3] = 4196353; arr[4] = 16785412; arr[5] = 16; 
	for (i = 0 ; i < 6 ; i ++)
		count = DrawStrNum1(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrNum1(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 186 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 11 == 10)
            TeleportLocationVector(1, -20.0, 2.0);
		else
            TeleportLocationVector(1, 2.0, 0.0);
		count ++;
	}
	return count;
}

void StrGo()
{
	int arr[16], i, count = 0;
	string name = "HealOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 1041301052; arr[1] = 545539580; arr[2] = 138479680; arr[3] = 289440273; arr[4] = 2236676; arr[5] = 1107838210; arr[6] = 285475592; arr[7] = 403769375; 
	arr[8] = 272778256; arr[9] = 2015395908; arr[10] = 143147273; arr[11] = 34869378; arr[12] = 553918465; arr[13] = 71436368; arr[14] = 278955023; arr[15] = 4360; 
	
	for (i = 0 ; i < 16 ; i ++)
		count = DrawStrGo(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStrGo(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 496 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 48 == 47)
            TeleportLocationVector(1, -94.0, 2.0);
		else
            TeleportLocationVector(1, 2.0, 0.0);
		count ++;
	}
	return count;
}

void RemoveNumbering(int ptr)
{
    int cur = ptr;

    while (IsObjectOn(cur))
    {
        if (GetDirection(cur))
            break;
        Delete(cur);
        cur ++;
    }
    Delete(cur);
}

void ControlNumbering(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), unit;

    while (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner) && count)
        {
            if (!(count % 30))
            {
                MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
                unit = CreateObject("InvisibleLightBlueLow", 1) + 1;
                Delete(unit - 1);
                CallFunction(ToInt(GetObjectZ(ptr + GetDirection(ptr + 1))));
                FrameTimerWithArg(29, unit, RemoveNumbering);
                LookWithAngle(CreateObject("InvisibleLightBlueLow", 1), 1);
                LookWithAngle(ptr + 1, GetDirection(ptr + 1) + 1);
                AudioEvent("TowerShoot", 1);
            }
            LookWithAngle(ptr, --count);
            FrameTimerWithArg(1, ptr, ControlNumbering);
            break;
        }
        Delete(ptr++);
        Delete(ptr++);
        Delete(ptr++);
        Delete(ptr++);
        break;
    }
}

void PlayerDisplayCountdown(int plrUnit)
{
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(plrUnit), GetObjectY(plrUnit) - 72.0);

    Raise(unit, StrNum3);
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), StrNum2);
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), StrNum1);
    Raise(CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)), StrGo);
    SetOwner(plrUnit, unit);
    LookWithAngle(unit, 149);
    FrameTimerWithArg(1, unit, ControlNumbering);
}

int setWhiteScreen(int wp)
{
    float pos_y;
    int subsub = CreateObject("maiden", wp);
 
    MoveObject(subsub, 100.0 + pos_y, 5900.0);
    pos_y += 25.0;
    Enchant(subsub, "ENCHANT_FREEZE", 0.0);
    CreateObject("InvisibleLightBlueHigh", wp);
    RetreatLevel(subsub, 0.9);
    SetCallback(subsub, 8, whiteScreen);
    Damage(subsub, 0, CurrentHealth(subsub) - 1, 16);
 
    return subsub + 1;
}
 
void whiteScreen()
{
    int subsub = GetTrigger() + 1;

    Effect("WHITE_FLASH", GetObjectX(subsub), GetObjectY(subsub), 0.0, 0.0);
    MoveObject(self, GetObjectX(self), GetObjectY(self));
    RestoreHealth(self, 100);
    Damage(self, 0, CurrentHealth(self) - 1, 16);
    if (!IsObjectOn(subsub))
        Delete(self);
}

void whiteOutMessage()
{
    UniPrintToAll("주의! 전방에 화이트아웃 현상이 발생되고 있습니다!!");
}

void StartPicket()
{
	int arr[22], i, count = 0;
	string name = "ManaBombOrb";
	float pos_x = GetWaypointX(1), pos_y = GetWaypointY(1);

	arr[0] = 472900112; arr[1] = 20420; arr[2] = 540804; arr[3] = 4609; arr[4] = 670175265; arr[5] = 536904832; arr[6] = 134449672; arr[7] = 134234400; 
	arr[8] = 35398722; arr[9] = 1157636168; arr[10] = 59279880; arr[11] = 289411089; arr[12] = 539107328; arr[13] = 76056580; arr[14] = 67668223; arr[15] = 19014657; 
	arr[16] = 545399328; arr[17] = 5261824; arr[18] = 135825160; arr[19] = 1049344; arr[20] = 33562626; arr[21] = 248; 
	for (i = 0 ; i < 22 ; i ++)
		count = DrawStartPicket(arr[i], name, count);
	MoveWaypoint(1, pos_x, pos_y);
}

int DrawStartPicket(int arg, string name, int count)
{
	int i;

	for (i = 1 ; i > 0 && count < 682 ; i <<= 1)
	{
		if (i & arg)
			CreateObject(name, 1);
		if (count % 60 == 59)
			MoveWaypoint(1, GetWaypointX(1) - 118.0, GetWaypointY(1) + 2.0);
		else
			MoveWaypoint(1, GetWaypointX(1) + 2.0, GetWaypointY(1));
		count ++;
	}
	return count;
}

void PutStampString()
{
    MoveWaypoint(1, GetWaypointX(122), GetWaypointY(122));
    StartPicket();
}

void MapInitialize()
{
    playerMover(-1);
    placedUnits();
    setMapLighting(24, 121);

    setWhiteScreen(123);
    setWhiteScreen(112);

    //delay_run
    FrameTimer(10, PutStampString);
    //loop_run
    FrameTimer(10, loopPreservePlayers);

    RegistSignMessage(Object("guide1"), "/*\"아이스 스키\"*/ 장애물을 피하여 스키를 타세요! 결국 최종목적지로 가셔야 이기는 게임입니다");
    RegistSignMessage(Object("guide2"), "스키는 가만히 둬도 저절로 움직입니다. 이 상태에서 장애물만 잘좀 피하세요");
    RegistSignMessage(Object("guide3"), "제작자. 녹스게임리마스터. 두갈래 나오는데 사실 본선은 이 모퉁이 돌아서 오른쪽 골목이다");
    RegistSignMessage(Object("guide4"), "[화이트아웃 경보!] 전방에 잦은 화이트아웃 발생지역이 있습니다. 운행에 주의하세요!!");
    RegistSignMessage(Object("guide5"), "조금만 더 힘을 내세요! 거의다 오셨어요! 저기 저 언덕위에 당신을 위한 숙소가 마련되어 있어요");
    RegistSignMessage(Object("guide6"), "!당신의 승리! 당신은 해내셨습니다! 안전한 숙소까지 도착하였습니다");
    RegistSignMessage(Object("guide7"), "사실 뭐 끝까지 가는게 중요하지 미터수가 중요치는 않음! never!!");
}

