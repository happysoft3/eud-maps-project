
#include "q047251_gvar.h"
#include "libs/define.h"
#include "libs/objectIDdefines.h"
#include "libs/unitstruct.h"
#include "libs/winapi.h"
#include "libs/game_flags.h"
#include "libs/recovery.h"

int ColorMaiden(int red, int grn, int blue, int wp)
{
    int unit = CreateObject("Bear2", wp);
    int ptr1 = GetMemory(0x750710), k;

    SetMemory(ptr1 + 4, 1385);
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetUnitVoice(unit, 7);
    UnitLinkBinScript(unit, MaidenBinTable());

    return unit;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
        arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link = arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 90; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link = arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
        arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 38; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link = arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743; arr[19] = 80; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20;
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link = arr;
	}
	return link;
}

int LichLordBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1751345484; arr[1] = 1685221196; arr[17] = 473; arr[19] = 100; arr[21] = 1065353216; 
		arr[23] = 2048; arr[24] = 1065353216; arr[25] = 1; arr[26] = 4; arr[27] = 7; 
		arr[28] = 1108082688; arr[29] = 50; arr[30] = 1092616192; arr[32] = 19; arr[33] = 27; 
		arr[57] = 5548288; arr[59] = 5542784;
		link = arr;
	}
	return link;
}

void LichLordSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1077936128);
		SetMemory(ptr + 0x224, 1077936128);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 2048);
		SetMemory(GetMemory(ptr + 0x22c), 300);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, LichLordBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

int WizardRedBinTable()
{
	int link, arr[62];

	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100; arr[16] = 80000; arr[17] = 300; 
		arr[18] = 100; arr[19] = 50; arr[21] = 1065353216; arr[23] = 36864; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1117782016; arr[29] = 20; arr[32] = 12; arr[33] = 20; 
		arr[34] = 2; arr[35] = 2; arr[36] = 10; arr[54] = 4; arr[59] = 5543904; 
		link = arr;
	}
	return link;
}

void WizardRedSubProcess(int sUnit)
{
	int ptr = UnitToPtr(sUnit);

	if (ptr)
	{
		SetMemory(ptr + 0x220, 1069547520);
		SetMemory(ptr + 0x224, 1069547520);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x5a0, 36864);
		SetMemory(GetMemory(ptr + 0x22c), 300);
		SetMemory(GetMemory(ptr + 0x22c) + 0x4, 300);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
		SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x538, 0);
		SetMemory(GetMemory(ptr + 0x2ec) + 0x540, 1065353216);
	}
}

// float FloatTable(int num)
// {
//     float arr[28], count;
//     int k;

//     if (num < 0)
//     {
//         count = 27.0;
//         for (k = 27 ; k >= 0 ; k --)
//         {
//             arr[k] = count;
//             count -= 1.0;
//         }
//         return ToFloat(0);
//     }
//     return arr[num];
// }

// int NumberData(int num)
// {
//     int data[10] = {110729622, 239354980, 252799126, 110643350, 143194521,
//         126382367, 110719382, 71583903, 110717334, 110684566};

//     return data[num];
// }

// void DisplayNumber(float x, float y, int bytes, string orb)
// {
//     float pos_x = x, pos_y = y;
//     int idx = 0, k;

//     for (k = 1 ; !(k & 0x10000000) ; k <<= 1)
//     {
//         if (bytes & k)
//             CreateObjectAt(orb, pos_x, pos_y);
//         if (idx % 4 == 3)
//         {
//             pos_x = x;
//             pos_y += 2.0;
//         }
//         else
//             pos_x += 2.0;
//         idx ++;
//     }
// }

// int DrawNumber(int num, float x, float y)
// {
//     int ptr = CreateObjectAt("RedPotion", x, y) + 1;

//     Delete(ptr - 1);
//     if (num)
//     {
//         if (num >= 10 && num < 100)
//             DisplayNumber(x, y, NumberData(num / 10), "ManaBombOrb");
//         DisplayNumber(x + 12.0, y, NumberData(num % 10), "ManaBombOrb");
//     }
//     LookWithAngle(CreateObjectAt("InvisibleLightBlueLow", x, y), 1);
//     return ptr;
// }

void controlHealthbarMotion(int bar, int motionId)
{
    int ptr=UnitToPtr(bar);
    
    SetMemory(GetMemory(ptr+0x2ec)+0x1e0, (motionId<<8)|0x10000);
}

int DummyUnitCreateById(short thingId, float xpos, float ypos)
{
    int unit = CreateObjectById(thingId, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);
    return unit;
}

void changeNumber(int n, int value)
{
    controlHealthbarMotion(n,value%10);
}

int createNumber(float x, float y)
{
    int n=DummyUnitCreateById(OBJ_BEAR_2,x,y);

    LookWithAngle(n,128);
    UnitNoCollide(n);
    return n;
}

void DrawTextSprite(float x, float y, int textId){
    int text=DummyUnitCreateById(OBJ_NECROMANCER,x,y);

    controlHealthbarMotion(text,textId);
    UnitNoCollide(text);
}

void BomberSetMonsterCollide(int bombUnit)
{
    int ptr = UnitToPtr(bombUnit);

    if (ptr)
        SetMemory(ptr + 0x2b8, 0x4e83b0);
}

void ModifyCodeGuarded(int target, char *pValue, int length)
{
    int pOldProtect;
    int mask = target & 0xfff000;

    WinApiVirtualProtect(mask, 4096, 0x40, &pOldProtect);
    NoxByteMemCopy(pValue, target, length);
    WinApiVirtualProtect(mask, 4096, pOldProtect, NULLPTR);
}

void blockObserverMode(){
    int *p=0x589cf8;
    p[0]=0x1000;
    SetRecoveryDataType2(p,0x2000);
    char patch[]={0xFF, 0x35, 0xac, 0xac, 0xac, 0x00, 0xE8, 0x6F, 0x66, 0xFC, 0xFF, 0x5B, 0x90};
    int *h=&patch[2];
    h[0]=p;
    ModifyCodeGuarded(0x443f46,patch,sizeof(patch));
}

void initGame()
{
    if (CheckGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR))
        SetGameFlags(GAME_FLAG_RESPAWN_WEAPON_ARMOR);
    SetGameSettingForceRespawn(TRUE);
}

void DrawLavaUnitKills(int value)
{
    int ptr;

    if (value < 100)
    {
        changeNumber(FirstNumber(), value/10);
        changeNumber(SecondNumber(), value%10);
    }
}
