
#include "boss_utils.h"
#include "libs/hash.h"
#include "libs/sound_define.h"
#include "libs/itemproperty.h"
#include "libs/weapon_effect.h"

int getUserWeaponFxHash(){
    int hash;
    if (!hash)HashCreateInstance(&hash);
    return hash;
}

void DeferredPickupUserWeapon(int weapon)
{
    if (IsObjectOn(weapon))
    {
        RegistItemPickupCallback(weapon, OnPickupUserWeapon);
        return;
    }
    int del;

    HashGet(getUserWeaponFxHash(), weapon, &del, TRUE);
}

void OnPickupUserWeapon()
{
    int *pMem;

    FrameTimerWithArg(1, GetTrigger(), DeferredPickupUserWeapon);
    if (HashGet(getUserWeaponFxHash(), GetTrigger(), &pMem, FALSE))
    {
        Delete(pMem[0]);
        Effect("CYAN_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
    }
}

void DeferredDiscardUserWeapon(int weapon)
{
    if (!MaxHealth(weapon))
        return;

    int *pMem;
    float xy[]={GetObjectX(weapon), GetObjectY(weapon)};
    if (HashGet(getUserWeaponFxHash(), weapon, &pMem, FALSE))
    {
        pMem[0]=CreateObjectById(OBJ_BLUE_SUMMONS, xy[0], xy[1]);
        PlaySoundAround(weapon, SOUND_BurnCast);
        Effect("SPARK_EXPLOSION", xy[0], xy[1], 0.0, 0.0);
    }    
}

void OnDiscardUserWeapon()
{
    PushTimerQueue(1, GetTrigger(), DeferredDiscardUserWeapon);
}

void CreateUserWeaponFX(int weapon)
{
    int *pMem;

    AllocSmartMemEx(4, &pMem);
    pMem[0]=CreateObjectById(OBJ_BLUE_SUMMONS, GetObjectX(weapon), GetObjectY(weapon));
    HashPushback(getUserWeaponFxHash(), weapon, pMem);
    RegistItemPickupCallback(weapon, OnPickupUserWeapon);
    SetUnitCallbackOnDiscardBypass(weapon, OnDiscardUserWeapon);
}





////////////////////////////////////////////////


#define SPECIAL_WEAPON_PROPERTY 0
#define SPECIAL_WEAPON_FXCODE 1
#define SPECIAL_WEAPON_EXECCODE 2
#define SPECIAL_WEAPON_MAX 3


////////////////////////////////////////////////


void DeferredDrawYellowLightning(int *pMem)
{
    int count=pMem[1], unit=pMem[0];

    while (--count>=0)
    {
        if (IsObjectOn(unit))
            CastSpellObjectObject("SPELL_LIGHTNING", unit,++unit);
    }
    FreeSmartMemEx(pMem);
}

void DrawYellowLightningFx(float x1, float y1, float x2, float y2, int dur)
{
    float xyVect[2];
    ComputePointRatio(&x2, &x1, &xyVect, 32.0);
    int count = FloatToInt(Distance(x1,y1,x2,y2)/32.0);

    if (count==0)
        return;

    int *mem;
    AllocSmartMemEx(8, &mem);
    int curPoint = CreateObjectAt("InvisibleLightBlueHigh", x1, y1);

    mem[0]=curPoint;
    DeleteObjectTimer(curPoint, dur);
    int createdCount=0, prevPoint;
    while (--count>=0)
	{
        prevPoint=curPoint;
        curPoint= CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(curPoint) + xyVect[0], GetObjectY(curPoint)+xyVect[1]);
        DeleteObjectTimer(curPoint, dur);
		if (!IsVisibleTo(prevPoint, curPoint))
            break;
        createdCount+=1;
	}
    mem[1]=createdCount;
	PushTimerQueue(1, mem, DeferredDrawYellowLightning);
}

void OnThunderLightningCollide()
{
    if (CurrentHealth(OTHER))
    {
        int owner = GetOwner(SELF);

        if (IsAttackedBy(OTHER, owner))
        {
            int hash=GetUnit1C(SELF);

            if (hash)
            {
                if (HashGet(hash,GetCaller(),NULLPTR,FALSE))
                    return;
                HashPushback(hash,GetCaller(),TRUE);
                Damage(OTHER, owner, 200, DAMAGE_TYPE_ELECTRIC);
                Effect("YELLOW_SPARKS", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            }
        }
    }
}

void ThunderLightningSwordTriggered()
{
    float xvect = UnitAngleCos(OTHER, 17.0), yvect = UnitAngleSin(OTHER, 17.0);
    int arr[30], u=0,hash;

    arr[u] = CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(OTHER)+xvect,GetObjectY(OTHER)+yvect);
    HashCreateInstance(&hash);
    while (++u<sizeof(arr))
    {
        arr[u]=DummyUnitCreateById(OBJ_DEMON, GetObjectX(arr[u-1])+xvect, GetObjectY(arr[u-1])+yvect);
        DeleteObjectTimer(arr[u], 1);
        SetOwner(OTHER, arr[u]);
        SetUnitFlags(arr[u],GetUnitFlags(arr[u])^UNIT_FLAG_NO_PUSH_CHARACTERS);
        SetUnit1C(arr[u],hash);
        SetCallback(arr[u], 9, OnThunderLightningCollide);
        if (!IsVisibleTo(arr[0], arr[u]))
        {
            ++u;
            break;
        }
    }
    DrawYellowLightningFx(GetObjectX(arr[0]), GetObjectY(arr[0]), GetObjectX(arr[u-1]), GetObjectY(arr[u-1]), 24);
    PlaySoundAround(OTHER, SOUND_PlasmaSustain);
}
int CreateThunderSword(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(ThunderLightningSwordTriggered, &property[SPECIAL_WEAPON_EXECCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_GREAT_SWORD,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_Speed4, ITEM_PROPERTY_stun1, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}

int SpreadBloodFx(float xProfile, float yProfile)
{
    int fx=CreateObjectById(OBJ_SHADE, xProfile, yProfile);
    UnitNoCollide(fx);
    Damage(fx, 0, 999, DAMAGE_TYPE_FLAME);
    return fx;
}

void StrikeFlameCollide()
{
    while (TRUE)
    {
        if (!GetTrigger())
            break;
        int owner = GetOwner(SELF);

        if (CurrentHealth(OTHER) && IsAttackedBy(OTHER, owner))
        {
            Damage(OTHER, owner, 140, DAMAGE_TYPE_EXPLOSION);
            Enchant(OTHER, "ENCHANT_CHARMING", 0.2);
        }
        break;
    }
    Effect("DAMAGE_POOF", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
    Delete(SELF);
}

#define STRIKE_FLAME_MARK_UNIT 0
#define STRIKE_FLAME_XVECT 1
#define STRIKE_FLAME_YVECT 2
#define STRIKE_FLAME_COUNT 3
#define STRIKE_FLAME_OWNER 4
#define STRIKE_FLAME_MAX 5

void StrikeFlameLoop(int *pArgs)
{
    int owner=pArgs[STRIKE_FLAME_OWNER];
    while (TRUE)
    {
        if (CurrentHealth(owner))
        {
            int *count=&pArgs[ STRIKE_FLAME_COUNT], mark = pArgs[STRIKE_FLAME_MARK_UNIT];

            if (count[0] && IsVisibleTo(mark, mark+1))
            {
                PushTimerQueue(1, pArgs, StrikeFlameLoop);
                float *vxy = &pArgs[STRIKE_FLAME_XVECT];

                MoveObjectVector(mark, vxy[0], vxy[1]);
                MoveObjectVector(mark+1, vxy[0], vxy[1]);
                int dum=DummyUnitCreateById(OBJ_DEMON, GetObjectX(mark), GetObjectY(mark));

                SetCallback(dum, 9, StrikeFlameCollide);
                SetOwner(owner, dum);
                DeleteObjectTimer(dum, 1);
                DeleteObjectTimer( SpreadBloodFx(GetObjectX(mark), GetObjectY(mark)), 12);
                
                --count[0];
                break;
            }
        }
        Delete(pArgs[STRIKE_FLAME_MARK_UNIT]++);
        Delete(pArgs[STRIKE_FLAME_MARK_UNIT]++);
        FreeSmartMemEx(pArgs);
        break;
    }
}

void StrikeFlameTriggered(int caster)
{
    float xvect=UnitAngleCos(caster, 17.0), yvect=UnitAngleSin(caster, 17.0);
    int mark = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) + xvect, GetObjectY(caster) + yvect);
    int visCheck=CreateObjectAt("InvisibleLightBlueLow", GetObjectX(caster) - xvect, GetObjectY(caster) - yvect);

    int *args;

    AllocSmartMemEx(STRIKE_FLAME_MAX*4, &args);
    args[STRIKE_FLAME_MARK_UNIT]=mark;
    args[STRIKE_FLAME_XVECT]=xvect;
    args[STRIKE_FLAME_YVECT]=yvect;
    args[STRIKE_FLAME_COUNT]=30; //count
    args[STRIKE_FLAME_OWNER]=caster;
    PushTimerQueue(1, args, StrikeFlameLoop);
}
void OnBurningZombieSwordTriggered()
{
    StrikeFlameTriggered(GetCaller());
    PlaySoundAround(OTHER, SOUND_MeteorShowerCast);
}

int CreateBurningSword(float x,float y){
    int property[SPECIAL_WEAPON_MAX],sd;
    if (!sd){
    SpecialWeaponPropertyCreate(&property[SPECIAL_WEAPON_PROPERTY]);
    SpecialWeaponPropertyExecuteScriptCodeGen(OnBurningZombieSwordTriggered, &property[SPECIAL_WEAPON_EXECCODE]);
    SpecialWeaponPropertySetExecuteCode(property[SPECIAL_WEAPON_PROPERTY], property[SPECIAL_WEAPON_EXECCODE]);}
    sd=CreateObjectById(OBJ_GREAT_SWORD,x,y);
    SetWeaponPropertiesDirect(sd, ITEM_PROPERTY_weaponPower6, ITEM_PROPERTY_FireProtect4, ITEM_PROPERTY_fire4, 0);
    SpecialWeaponPropertySetWeapon(sd, 3, property[SPECIAL_WEAPON_PROPERTY]);
    CreateUserWeaponFX(sd);
    return sd;
}
