
#include "ak250624_utils.h"
#include "libs/playerupdate.h"
#include "libs/cmdline.h"
#include "libs/bind.h"
#include "libs/printutil.h"
#include "libs/networkRev.h"
#include "libs/spellutil.h"
#include "libs/waypoint.h"
#include "libs/format.h"
#include "libs/buff.h"
#include "libs/fxeffect.h"
#include"libs/queueTimer.h"
#include"libs/observer.h"
#include"libs/reaction.h"

#define PLAYER_INITIAL_LOCATION 11
#define PLAYER_START_LOCATION 12
#define PLAYER_DEATH_FLAG 0x80000000
#define PLAYER_GGOVER_POS 11

void InitServerResource(){}//virtual
int GetPlayer(int pIndex) //virtual
{ }

void SetPlayer(int pIndex, int user) //virtual
{ }

int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

int GetUserCamera(int pIndex){}//virtual
void SetUserCamera(int pIndex, int cam){}//virtual
void removeUserCamera(int pIndex){
    int cam=GetUserCamera(pIndex);
    if (MaxHealth(cam))
    {
        Delete(cam);
        SetUserCamera(pIndex,0);
    }
}
void createUserCamera(int pIndex){
    int cam=GetUserCamera(pIndex);

    if (MaxHealth(cam)) return;
    cam= DummyUnitCreateById(OBJ_FISH_BIG, LocationX(14),LocationY(14));
    UnitNoCollide(cam);
    SetUserCamera(pIndex,cam);
}

int PlayerClassCheckFlag(int pIndex, int flags)
{
    return GetPlayerFlags(pIndex) & flags;
}

void PlayerClassSetFlag(int pIndex, int flags)
{
    SetPlayerFlags(pIndex, GetPlayerFlags(pIndex)^flags);
}

int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(GetPlayer(pIndex)))
        return GetPlayer(pIndex)==pUnit;
    return FALSE;
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

void PlayerClassOnShutdown(int pIndex)
{
    SetPlayer(pIndex,0);
    SetPlayerFlags(pIndex,0);
    removeUserCamera(pIndex);

    char buff[128];

    NoxSprintfString(buff, "playeronshutdown. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
    QueryPlayerLevel(pIndex,0,0);
}

void PlayerClassOnDeath(int pIndex, int user)
{
    char dieMsg[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
}

//@brief. returns - not=zero, else=player id
int IsPlayerPoint(int unit){
    if (MaxHealth(unit)){
        if (GetUnitThingID(unit)==OBJ_BOMBER){
            int ret=GetOwner(unit);
            if (!CurrentHealth(ret)) return 0;
            if (IsPlayerUnit(ret)) return ret;
        }
    }
    return 0;
}

void OnPoint(int point)
{
    int owner=GetOwner(point);

    if (CurrentHealth(owner))
    {
        int sub=DummyUnitCreateById(OBJ_BOMBER, GetObjectX(point),GetObjectY(point));

        SetOwner(owner, sub);
        DeleteObjectTimer(sub, 1);
        SetUnitFlags(sub,GetUnitFlags(sub)^UNIT_FLAG_NO_PUSH_CHARACTERS);
        DeleteObjectTimer(CreateObjectById(OBJ_MAGIC_SPARK, GetObjectX(sub), GetObjectY(sub)), 12);
    }
    Delete(point);
}

#define CLICK_DELAY 24
void ServerTaskClickProcedure(int pIndex, int user)
{
    int input=CheckPlayerInput(user);
    int fps[MAX_PLAYER_COUNT];

    if (input!=6)
        return;

    int *cfps=0x84ea04;
    if (ABS(cfps[0] - fps[pIndex]) < CLICK_DELAY)
        return;

    fps[pIndex]=cfps[0];
        
    int point=CreateObjectById(OBJ_MOONGLOW, GetObjectX(user),GetObjectY(user));

    PushTimerQueue(1,point,OnPoint);
    SetOwner(user,point);
}

void PlayerClassOnAlive(int pIndex, int user)
{
    int cam=GetUserCamera(pIndex);

    if (MaxHealth(cam))
    {
        if (CheckWatchFocus(    user ))
            PlayerLook(user, cam);
        ServerTaskClickProcedure(pIndex, user);
    }
}

void PlayerClassOnLoop(int pIndex)
{
    int user = GetPlayer(pIndex);

    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (GetPlayerFlags(pIndex))
        {
            PlayerClassOnShutdown(pIndex);
            return;
        }
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

void PlayerClassOnInit(int pIndex, int pUnit)
{
    SetPlayer(pIndex, pUnit);
    SetPlayerFlags(pIndex,1);

    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else InitServerResource();
        ShowQuestIntroOne(pUnit, 9999, "WarriorChapterBegin9", "Noxworld.wnd:DismalSwamp");
    }
    ChangeGold(pUnit, -GetGold(pUnit));
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    EmptyAll(pUnit);

    createUserCamera(pIndex);

    char buff[128];

    NoxSprintfString(buff, "playeroninit. index-%d", &pIndex, 1);
    NoxConsolePrint(ReadStringAddressEx(buff), CONSOLE_COLOR_GREEN);
}

void QueryPlayerJoinFunction(int *get, int set){
    int fn;
    if (get){
        get[0]=fn;
        return;
    }
    fn=set;
}

void playerJoinGGOver(int user){
    MoveObject(user,LocationX(PLAYER_GGOVER_POS),LocationY(PLAYER_GGOVER_POS));
    Enchant(user,EnchantList(ENCHANT_FREEZE),0.0);
    PlaySoundAround(user,SOUND_PlayerExit);
}

void playerJoinNormal(int user){
    float x=LocationX(PLAYER_START_LOCATION),y=LocationY(PLAYER_START_LOCATION);
    
    MoveObject(user, x,y);
    Effect("TELEPORT", x,y, 0.0, 0.0);
    PlaySoundAround(user, SOUND_BlindOff);
}

// void playerJoinBoss(int user){
//     short re[]={114,115,116,117,};
//     short pic=re[Random(0,3)];
//     float x=LocationX(pic),y=LocationY(pic);
//     MoveObject(user,x,y);
//     Effect("TELEPORT", x,y, 0.0, 0.0);
//     PlaySoundAround(user, SOUND_BlindOff);
// }
// void PlayerSetAllBuff(int pUnit)
// {
//     Enchant(pUnit, EnchantList(ENCHANT_VAMPIRISM), 0.0);
//     SetUnitEnchantCopy(pUnit, GetLShift(ENCHANT_PROTECT_FROM_FIRE) | GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY) | GetLShift(ENCHANT_PROTECT_FROM_POISON)
//         | GetLShift(ENCHANT_INFRAVISION) | GetLShift(ENCHANT_REFLECTIVE_SHIELD));
// }

void PlayerClassOnJoin(int user, int pIndex)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
    // if (PlayerClassCheckFlag(pIndex, PLAYER_FLAG_ALL_BUFF))
    //     PlayerSetAllBuff(user);
    SetUnitEnchantCopy(user, GetLShift(ENCHANT_ANTI_MAGIC));

    int joinFn;
    QueryPlayerJoinFunction(&joinFn, 0);
    Bind(joinFn, &user);
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(11), LocationY(11));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int pIndex)
{
    PlayerClassOnJoin(user, pIndex);
    UniPrint(user, "<<--- 마우스 피하기 -----------제작. 237------<<<<");
}

void PlayerClassOnEntry(int plrUnit)
{
    if (!CurrentHealth(plrUnit))
        return;

    int pIndex = GetPlayerIndex(plrUnit);
    char initialUser=FALSE;

    if (!MaxHealth(GetPlayer(pIndex)))
        PlayerClassOnInit(pIndex, plrUnit);
    if (pIndex >= 0)
    {
        if (initialUser)
            OnPlayerDeferredJoin(plrUnit, pIndex);
        else
            PlayerClassOnJoin(plrUnit, pIndex);
        return;
    }
    PlayerClassOnEntryFailure(plrUnit);
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC));
        if (checkIsPlayerAlive(GetPlayerIndex(OTHER), GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(PLAYER_INITIAL_LOCATION), LocationY(PLAYER_INITIAL_LOCATION));
    }
}

void PlayerRegist()
{
    PlayerClassOnEntry(GetCaller());
}

void TeleportPlayerAll(float x, float y){
    int r=MAX_PLAYER_COUNT;
    while (--r>=0){
        if (CurrentHealth(GetPlayer(r))){
            MoveObject(GetPlayer(r),x,y);
        }
    }
}

void PlayerSetGGOver(){
    QueryPlayerJoinFunction(NULLPTR,playerJoinGGOver);
    TeleportPlayerAll(LocationX(PLAYER_GGOVER_POS),LocationY(PLAYER_GGOVER_POS));
}

void InitializePlayerSystem(){
    QueryPlayerJoinFunction(NULLPTR,playerJoinNormal);
}
