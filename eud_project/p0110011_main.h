
#include "p0110011_gvar.h"
#include "p0110011_utils.h"
#include "p0110011_input.h"
#include "p0110011_player.h"
#include "p0110011_mob.h"
#include "p0110011_resource.h"
#include "libs/objectIDdefines.h"
#include "libs/coopteam.h"
#include "libs/waypoint.h"
#include "libs/displayInfo.h"
#include "libs/spriteDb.h"

#define INIT_RESPAWN_MARK_LOCATION 12

void initializePlayerRespawnMark()
{
    int r=MAX_PLAYER_COUNT;
    float x=LocationX(INIT_RESPAWN_MARK_LOCATION), y=LocationY(INIT_RESPAWN_MARK_LOCATION);
    int mark;

    while(--r>=0)
    {
        mark=CreateObjectById(OBJ_SPINNING_CROWN, x+MathSine(r*10 +90, 11.0), y+MathSine(r*10, 11.0));
        UnitNoCollide(mark);
        CreateObjectById(OBJ_INVISIBLE_LIGHT_BLUE_LOW, GetObjectX(mark), GetObjectY(mark));
        SetUserRespawnMark(r, mark);
    }
}

void placingTestMob()
{
    int mon=CreateObjectById(OBJ_BEAR, LocationX(14),LocationY(14));

    SetUnitMaxHealth(mon, 360);
}

void clientDisplaySkillInfo(int enabled, short *pMessage, char *buff, int skillId, char hotKey, string skillName)
{
    GetSkillMessage(&pMessage, skillId);
    if (!enabled)
    {
        NoxUtf8ToUnicode(StringUtilGetScriptStringPtr("--"), pMessage);
        return;
    }
    string statusStr[]={"아직 사용할 수 없음", "사용가능", "쿨다운 대기중...",};
    char *status=StringUtilGetScriptStringPtr(statusStr[enabled]);

    int args[]={StringUtilGetScriptStringPtr(skillName), hotKey, status};
    NoxSprintfString(buff, "%s스킬(발동키: %c)- %s", args,sizeof(args));
    NoxUtf8ToUnicode(buff,pMessage);
}

void onClientGameLoop()
{
    short *pMessage;
    char buff[256], *pArg=CLIENT_SUBDATA_OFF;
    int hpPerc=pArg[SUBDATA_REPORT_HP];

    GetCreatureHpInfoMessage(&pMessage);
    NoxSprintfString(buff,"당신의 캐릭터 체력: %d%%",&hpPerc,1);
    NoxUtf8ToUnicode(buff,pMessage);

    GetCreatureLevelExpInfoMessage(&pMessage);
    int args[]={pArg[SUBDATA_REPORT_LEVEL],pArg[SUBDATA_REPORT_EXP],};
    NoxSprintfString(buff, "캐릭터 레밸: %d, 경험치: %d %%", args, 2);
    NoxUtf8ToUnicode(buff,pMessage);

    clientDisplaySkillInfo(pArg[SUBDATA_REPORT_SKILL_1], pMessage, buff, 0, 'Z', "첫번째");
    clientDisplaySkillInfo(pArg[SUBDATA_REPORT_SKILL_2], pMessage, buff, 1, 'X', "두번째");
    clientDisplaySkillInfo(pArg[SUBDATA_REPORT_SKILL_3], pMessage, buff, 2, 'C', "마지막");

    int updateFlags=pArg[SUBDATA_REPORT_UPDATE_FLAGS];

    if (updateFlags&1) UpdateDamageUpgradeText();
    if (updateFlags&2) UpdateHpUpgradeText();
    if (updateFlags&4) UpdateSpeedUpgradeText();
    FrameTimer(3, onClientGameLoop);
}

void commonServerClientProcedure()
{
    short *pMessage;

    GetCreatureHpInfoMessage(&pMessage);
    AddShowInfoData(30, 300, pMessage, 0xFB20);
    GetCreatureLevelExpInfoMessage(&pMessage);
    AddShowInfoData(30, 330, pMessage, 0x07DF);
    GetSkillMessage(&pMessage, 0);
    AddShowInfoData(50, 550, pMessage, 0xffa0);
    GetSkillMessage(&pMessage, 1);
    AddShowInfoData(50, 570, pMessage, 0xffa0);
    GetSkillMessage(&pMessage, 2);
    AddShowInfoData(50, 590, pMessage, 0xffa0);
    FrameTimer(31,onClientGameLoop);
    char *p=CLIENT_SUBDATA_OFF;
    p[SUBDATA_REPORT_EXP]=0;
    p[SUBDATA_REPORT_HP]=0;
    p[SUBDATA_REPORT_LEVEL]=0;
    p[SUBDATA_REPORT_SKILL_1]=0;
    p[SUBDATA_REPORT_SKILL_2]=0;
    p[SUBDATA_REPORT_SKILL_3]=0;
    p[SUBDATA_REPORT_DAMAGE_UP]=0;
    p[SUBDATA_REPORT_HP_UP]=0;
    p[SUBDATA_REPORT_UPDATE_FLAGS]=0;
    p[SUBDATA_REPORT_SPEED_UP]=0;
    InitializeResource();
}

void beaconDeco(int beacon)
{
    int dec=CreateObjectById(OBJ_CORPSE_SKULL_SW,GetObjectX(beacon),GetObjectY(beacon));

    UnitNoCollide(dec);
    SetUnitEnchantCopy(dec,GetLShift(ENCHANT_ANCHORED)|GetLShift(ENCHANT_INFRAVISION)|GetLShift(ENCHANT_PROTECT_FROM_FIRE));
    Frozen(dec,TRUE);
}

void initialPlaceBeacon()
{
    int 
    t=CreateObjectById(OBJ_AMULET_OF_CLARITY,LocationX(89),LocationY(89));
    Frozen(t,TRUE);
    UnitNoCollide(t);
    beaconDeco(t);
    t=CreateObjectById(OBJ_AMULETOF_COMBAT,LocationX(90),LocationY(90));
    Frozen(t,TRUE);
    UnitNoCollide(t);    
    beaconDeco(t);
    t=CreateObjectById(OBJ_AMULETOF_MANIPULATION,LocationX(91),LocationY(91));
    Frozen(t,TRUE);
    UnitNoCollide(t);
    beaconDeco(t);
    t=CreateObjectById(OBJ_AMULETOF_NATURE,LocationX(165),LocationY(165));
    Frozen(t,TRUE);
    UnitNoCollide(t);
    beaconDeco(t);
    t=CreateObjectById(OBJ_AMULET_OF_TELEPORTATION,LocationX(166),LocationY(166));
    Frozen(t,TRUE);
    UnitNoCollide(t);
    beaconDeco(t);
    t=CreateObjectById(OBJ_CANDLE_1_UNLIT,LocationX(472),LocationY(472));
    Frozen(t,TRUE);
    UnitNoCollide(t);
    beaconDeco(t);
    t=CreateObjectById(OBJ_FOIL_CANDLE_UNLIT,LocationX(473),LocationY(473));
    Frozen(t,TRUE);
    UnitNoCollide(t);
    beaconDeco(t);
    InitializeUpgradeSystem();
}

void onHomeCollide()
{
    if (CurrentHealth(OTHER))
    {
        if (IsPlayerUnit(GetOwner(OTHER)))
        {
            Effect("SMOKE_BLAST",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
            MoveObject(OTHER,LocationX(INIT_RESPAWN_MARK_LOCATION),LocationY(INIT_RESPAWN_MARK_LOCATION));
            Effect("TELEPORT",GetObjectX(OTHER),GetObjectY(OTHER),0.0,0.0);
        }
    }
}

void placeGoHome(short location)
{
    int home=CreateObjectById(OBJ_ANKH,LocationX(location),LocationY(location));

    SetUnitCallbackOnCollide(home,onHomeCollide);
}

void initialPlaceHome()
{
    placeGoHome(167);
    placeGoHome(168);
    placeGoHome(169);
    placeGoHome(170);
    placeGoHome(171);
    placeGoHome(221);
    placeGoHome(222);
    placeGoHome(400);
    placeGoHome(401);
    placeGoHome(402);
    placeGoHome(403);
    placeGoHome(404);
    placeGoHome(405);
    placeGoHome(406);
    placeGoHome(407);
    placeGoHome(427);
}

void selectUserCharacter(int user, int id)
{
    int pIndex=GetPlayerIndex(user);

    if (pIndex<0) return;
    SetUserCharacterId(pIndex,id);
    MoveObject(user,LocationX(174),LocationY(174));
}

void selectDryad()
{
    selectUserCharacter(OTHER, 0);
    UniPrint(OTHER,"디바(D.Va)를 선택하셨어요");
}
void selectHorren()
{
    selectUserCharacter(OTHER,1);
    UniPrint(OTHER,"호렌더스를 선택하셨어요");
}
void selectMystic()
{
    selectUserCharacter(OTHER,2);
    UniPrint(OTHER,"미스틱을 선택하셨어요");
}
void selectKillerian()
{
    selectUserCharacter(OTHER,3);
    UniPrint(OTHER,"키러리언을 선택하셨어요");
    UniChatMessage(SELF, "키러리언이 선택됨!", 50);
}

void initialPlaceHeros()
{
    int gwiz=CreateObjectById(OBJ_WIZARD_GREEN,LocationX(172),LocationY(172));

    Frozen(gwiz,TRUE);
    LookWithAngle(gwiz,96);
    SetDialog(gwiz,"NORMAL",selectDryad,Nop);
    int hor=CreateObjectById(OBJ_HORRENDOUS,LocationX(173),LocationY(173));

    Frozen(hor,TRUE);
    LookWithAngle(hor,96);
    SetDialog(hor,"NORMAL",selectHorren,Nop);
    int mys=CreateObjectById(OBJ_WIZARD,LocationX(346),LocationY(346));

    Frozen(mys,TRUE);
    LookWithAngle(mys,96);
    SetDialog(mys,"NORMAL",selectMystic,Nop);
    mys=CreateObjectById(OBJ_DEMON,LocationX(428),LocationY(428));

    Frozen(mys,TRUE);
    LookWithAngle(mys,96);
    SetDialog(mys,"NORMAL",selectKillerian,Nop);
}

void OnInitializeClient(int clientId)
{
    commonServerClientProcedure();
    if (clientId!=31)
        FrameTimerWithArg(30, clientId, ClientClassTimerLoop);
}

void initPlayer(int pIndex)
{
    if (pIndex==31)
    {
        int *pStatus;
        GetClientKeyInputPtr(&pStatus);
        PushTimerQueue(60, pStatus, ServerInputLoop);
        OnInitializeClient(31);
    }
}

void initialReadableSettings()
{
    RegistSignMessage(Object("read1"), "오른쪽 캐릭터를 클릭하여, 플레이 하실 캐릭터를 선택하세요. 앞에있는 비콘을 밟으면 고정된 캐릭터가 선택됩니다");
    RegistSignMessage(Object("read2"), "디바(DVA)- 체력 약함, 민첩 및 공격성 강함");
    RegistSignMessage(Object("read3"), "호렌더스- 체력 강함, 이동속도 느림");
    RegistSignMessage(Object("read4"), "<<----아오오니 헌터---------- 제작. 237-----------<");
    RegistSignMessage(Object("read5"), "미스틱- 체력 중간, 이동속도 빠름");
    RegistSignMessage(Object("read6"), "키러리언- 체력 강함, 이동속도 느림, 불에 저항합니다");
}

void OnInitializeMap()
{
    MakeCoopTeam();
    CreateLogFile("sibal.log");
    initializePlayerRespawnMark();
    MasterUnit();        
    // placingTestMob();
    InitializeMonsters();
    initialPlaceBeacon();
    initialPlaceHome();
    initialPlaceHeros();
    int *cb;
    PlayerInitCallbackStorage(&cb);
    cb[0]=initPlayer;
    initialReadableSettings();
    InitialServerPatchMapleOnly();
}

void OnShutdownMap()
{ RemoveCoopTeamMode(); }

#define ROOM_MAX_COUNT 32
void roomImpl(int roomId, int fn)
{
    char room[ROOM_MAX_COUNT];

    if (room[roomId]) return;
    if (CurrentHealth(OTHER))
    {
        int parent =GetUnitTopParent(OTHER);

        if (!IsPlayerUnit(parent))
            return;
        Bind(fn,NULLPTR);
        MoveLastTransmission(SELF);
        room[roomId]=TRUE;
    }
}

void EntryRoom1()
{
    roomImpl(1, PlaceMobRoom1);
}

void EntryRoom2()
{
    roomImpl(2, PlaceMobRoom2);
}
void EntryRoom3()
{
    roomImpl(3, PlaceMobRoom3);
}
void EntryRoom4(){
    roomImpl(4, PlaceMobRoom4);
}
void EntryRoom5(){
    roomImpl(5, PlaceMobRoom5);
}
void EntryRoom6(){
    roomImpl(6, PlaceMobRoom6);
}
void EntryRoom7(){roomImpl(7, PlaceMobRoom7);}
void EntryRoom8(){roomImpl(8, PlaceMobRoom8);}
void EntryRoom9(){roomImpl(9, PlaceMobRoom9);}
void EntryRoom10(){roomImpl(10, PlaceMobRoomA);}
void EntryRoom11(){roomImpl(11, PlaceMobRoomB);}
void EntryRoom12(){roomImpl(12, PlaceMobRoomC);}
void EntryRoom13(){roomImpl(13, PlaceMobRoomD);}
void EntryRoom14(){roomImpl(14, PlaceMobRoomE);}
void EntryRoom15(){roomImpl(15, PlaceMobRoomF);}
void EntryRoom16(){roomImpl(16, PlaceMobRoomG);}
void EntryRoom17(){roomImpl(17, PlaceMobRoomH);}
void EntryRoom18(){roomImpl(18, PlaceMobRoomI);}

void spreadYouWinMarker(int sub)
{
    if (ToInt(GetObjectX(sub)))
    {
        int count=GetDirection(sub);

        if (--count>=0)
        {
            PushTimerQueue(3,sub,spreadYouWinMarker);
            LookWithAngle(sub,count);
            CreateObjectById(OBJ_BOTTLE_CANDLE_UNLIT,GetObjectX(sub),GetObjectY(sub));
            return;
        }
        Delete(sub);
    }
}

void VictoryEvent()
{
    int sub=CreateObjectById(OBJ_REWARD_MARKER,GetObjectX(OTHER),GetObjectY(OTHER));

    LookWithAngle(sub,30);
    spreadYouWinMarker(sub);
    UniPrintToAll("!!이겼습니다!! YOU WIN !!");
    UniPrintToAll("!!이겼습니다!! YOU WIN !!");
    UniPrintToAll("!!이겼습니다!! YOU WIN !!");
}

void EndRoom(){roomImpl(ROOM_MAX_COUNT-1, VictoryEvent);}

void showTheMap()
{
    UniPrint(OTHER, "<<---아오오니 헌터 - 제작 237-------------------------------------------------------------------------");
    UniPrint(OTHER, "당신의 임무 --- 캐릭터를 육성하여 모든 아오오니를 파괴하고 마지막 목적지로 이동하십시오--------------------");
    PlaySoundAround(OTHER,SOUND_JournalEntryAdd);
}

