
#include "demono_utils.h"
#include "libs/printutil.h"
#include "libs/buff.h"
#include "libs/spellutil.h"

void onWarSkillSplash()
{
    if (GetTrigger()==GetCaller())
        return;

    if (CurrentHealth(OTHER))
    {
        if (IsAttackedBy(OTHER, SELF) && IsVisibleOr(OTHER, SELF))
        {
            Damage(OTHER, SELF, 65, DAMAGE_TYPE_PLASMA);
        }
    }
}

void PlayerClassUseWarcry(int owner)
{
    ManaBombCancelFx(owner);
    SplashDamageAtEx(owner,GetObjectX(owner), GetObjectY(owner), 160.0, onWarSkillSplash);
    UniChatMessage(owner, "전쟁의 함성!!", 150);
}


void FrontOfWarTouched()
{
    int owner = GetOwner(GetTrigger() + 1);

    if (IsAttackedBy(OTHER, owner) && CurrentHealth(OTHER))
    {
        Damage(OTHER, owner, 100, 14);
        Enchant(OTHER, "ENCHANT_CHARMING", 0.8);
    }
}

int FrontOfWarDummyUnit(int target, int owner)
{
    int unit = CreateObjectAt("Bomber", GetObjectX(target), GetObjectY(target));

    SetOwner(owner, CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)));
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    ObjectOff(unit);
    SetCallback(unit, 9, FrontOfWarTouched);
    Frozen(unit, 1);
    DeleteObjectTimer(unit, 1);
    DeleteObjectTimer(unit + 1, 3);

    return unit;
}

void FrontOfWarBack(int ptr)
{
    int owner = GetOwner(ptr), i;

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner))
        {
            for (i = GetDirection(ptr) ; i ; Nop(i --))
            {
                MoveObject(ptr, GetObjectX(ptr) - GetObjectZ(ptr), GetObjectY(ptr) - GetObjectZ(ptr + 1));
                if (IsVisibleTo(ptr, ptr + 1))
                    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
                else
                    break;
                Nop(FrontOfWarDummyUnit(ptr, owner));
            }
            if (Distance(GetObjectX(owner), GetObjectY(owner), GetObjectX(ptr), GetObjectY(ptr)) > 23.0)
                MyGreenLightningFloat(GetObjectX(owner), GetObjectY(owner), GetObjectX(ptr), GetObjectY(ptr));
            MoveObject(owner, GetWaypointX(1), GetWaypointY(1));
            EnchantOff(owner, "ENCHANT_FREEZE");
        }
        Delete(ptr++);
        Delete(ptr++);
    }
}

void FrontOfWar(int ptr)
{
    int owner = GetOwner(ptr), i, count = GetDirection(ptr);

    if (IsObjectOn(ptr))
    {
        if (CurrentHealth(owner))
        {
            for (i = count ; i ; i --)
            {
                MoveObject(ptr, GetObjectX(ptr) + GetObjectZ(ptr), GetObjectY(ptr) + GetObjectZ(ptr + 1));
                if (IsVisibleTo(ptr, ptr + 1))
                    MoveWaypoint(1, GetObjectX(ptr), GetObjectY(ptr));
                else
                    break;
                FrontOfWarDummyUnit(ptr, owner);
            }
            if (Distance(GetObjectX(owner), GetObjectY(owner), GetObjectX(ptr), GetObjectY(ptr)) > 23.0)
                MyGreenLightningFloat(GetObjectX(owner), GetObjectY(owner), GetObjectX(ptr), GetObjectY(ptr));
            LookWithAngle(ptr, count - i);
            MoveObject(owner, GetWaypointX(1), GetWaypointY(1));
            FrameTimerWithArg(25, ptr, FrontOfWarBack);
        }
        else
        {
            Delete(ptr++);
            Delete(ptr++);
        }
    }
}

void PlayerClassUseSkill1(int owner)
{
    EnchantOff(owner, EnchantList(ENCHANT_SNEAK));
    RemoveTreadLightly(owner);
    Enchant(owner, EnchantList(ENCHANT_FREEZE), 0.0);
    int unit = CreateObjectAt("InvisibleLightBlueLow", GetObjectX(owner), GetObjectY(owner));
    SetOwner(owner, CreateObjectAt("InvisibleLightBlueLow", GetObjectX(unit), GetObjectY(unit)) - 1);
    LookWithAngle(unit, 12);
    Raise(unit, UnitAngleCos(owner, 20.0));
    Raise(unit + 1, UnitAngleSin(owner, 20.0));
    FrameTimerWithArg(1, unit, FrontOfWar);
}
