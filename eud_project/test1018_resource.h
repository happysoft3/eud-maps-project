


#include "ak180716_gvar.h"
#include "libs/grplib.h"
#include "libs/animFrame.h"

#define IMAGE_VECTOR_MAX_COUNT 2048

void GRPDump_HansomeSquidwardOutput(){}
#define HANSOME_SQUIDWARD_OUTPUT_BASE_IMAGE_ID 133943
void initializeImageFrameHansomeSquidwardOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_APPRENTICE;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_HansomeSquidwardOutput)+4, thingId, xyinc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1, USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, HANSOME_SQUIDWARD_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(HANSOME_SQUIDWARD_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(HANSOME_SQUIDWARD_OUTPUT_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(HANSOME_SQUIDWARD_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_MaidenGhostOutput(){}
#define MAIDENGHOST_OUTPUT_BASE_IMAGE_ID 114546
void initializeImageFrameMaidenGhost(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GHOST;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_MaidenGhostOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameAssign8Directions(5, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    AnimFrameAssign8Directions(10, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    AnimFrameAssign8Directions(15, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    AnimFrameAssign8Directions(20, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);
    AnimFrameAssign8Directions(25, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_IDLE, 5);
    AnimFrameAssign8Directions(30, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_IDLE, 6);
    AnimFrameAssign8Directions(35, MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_IDLE, 7);

    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_WALK, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_WALK, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_WALK, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_RUN, 5);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_RUN, 6);
    AnimFrameCopy8Directions( MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+56,    IMAGE_SPRITE_MON_ACTION_RUN, 7);
    AnimFrameCopy8Directions(MAIDENGHOST_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}

void GRPDump_BunnyGirlOutput(){}
#define BUNNYGIRL_OUTPUT_BASE_IMAGE_ID 121039
void initializeImageFrameBunnyGirlOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_ALBINO_SPIDER, xyInc[]={0,-15};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_BunnyGirlOutput)+4, thingId, xyInc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0, BUNNYGIRL_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3, BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6, BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy4Directions(     BUNNYGIRL_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_QueenGirlOutput(){}
#define QUEENGIRL_OUTPUT_BASE_IMAGE_ID 120669
void initializeImageFrameQueenGirlOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SMALL_SPIDER,xyInc[]={0,-15};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_QueenGirlOutput)+4, thingId, xyInc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0, QUEENGIRL_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3, QUEENGIRL_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(     QUEENGIRL_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6, QUEENGIRL_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(     QUEENGIRL_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(     QUEENGIRL_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(     QUEENGIRL_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(     QUEENGIRL_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(     QUEENGIRL_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy4Directions(     QUEENGIRL_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_PunchGirlOutput(){}
#define PUNCHGIRL_OUTPUT_BASE_IMAGE_ID 120829
void initializeImageFramePunchGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_ALBINO_SPIDER;
    int xyInc[]={0,-1};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_PunchGirlOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,3,2,2);
    // AnimFrameAssign8Directions(0, PUNCHGIRL_OUTPUT_BASE_IMAGE_ID,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);

    // AnimFrameAssign8Directions(5,  PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameAssign8Directions(10, PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign8Directions(15, PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy8Directions  (    PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy8Directions  (    PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+8,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy8Directions  (    PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy8Directions  (    PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+24,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy8Directions  (    PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+16,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameAssign8Directions(20,PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+32,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    // AnimFrameAssign8Directions(25,PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+40,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    // AnimFrameAssign8Directions(30,PUNCHGIRL_OUTPUT_BASE_IMAGE_ID+48,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
}
void GRPDump_RedmistOutput(){}
#define REDMIST_OUTPUT_BASE_IMAGE_ID 133955
void initializeImageFrameRedmistOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_CONJURER;
    int xyinc[]={0,1};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_RedmistOutput)+4, thingId, xyinc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    AnimFrameMake8Directions(frameCount);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,1,USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,1, USE_DEFAULT_SETTINGS,2);
    ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    AnimFrameAssign8Directions(0, REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    AnimFrameCopy8Directions(REDMIST_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_LeeMBOutput(){}
#define MRLEEMB_OUTPUT_BASE_IMAGE_ID 133967
void initializeImageFrameLeeMBOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WOUNDED_WARRIOR;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_LeeMBOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0, MRLEEMB_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3, MRLEEMB_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(     MRLEEMB_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6, MRLEEMB_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(     MRLEEMB_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(     MRLEEMB_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(     MRLEEMB_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(     MRLEEMB_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(     MRLEEMB_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy4Directions(     MRLEEMB_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_SoldierOutput(){}

#define SOLIDER_IMAGE_START_AT 122915
void initializeImageFrameSoldier(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GOON;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SoldierOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,USE_DEFAULT_SETTINGS,2);

    // AnimFrameAssign8Directions(0,SOLIDER_IMAGE_START_AT,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    // AnimFrameAssign8Directions(5,SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    // AnimFrameAssign8Directions(10,SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    // AnimFrameAssign8Directions(15,SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    // AnimFrameAssign8Directions(20,SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    // AnimFrameAssign8Directions(25,SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_RUN,4);

    // AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+8,IMAGE_SPRITE_MON_ACTION_WALK,0 );
    // AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+16,IMAGE_SPRITE_MON_ACTION_WALK,1);
    // AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    // AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    // AnimFrameCopy8Directions(SOLIDER_IMAGE_START_AT+40,IMAGE_SPRITE_MON_ACTION_WALK,4);
}
void GRPDump_FemaleBeachGirl(){}

#define FEMALE_BEACH_GIRL_IMAGE_ID 129717

void initializeImageFrameBeachGirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_AIRSHIP_CAPTAIN;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_FemaleBeachGirl)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    // ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1);
    // ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4);
    // ChangeMonsterSpriteImageCount(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4);
    // ChangeMonsterSpriteImageCount(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1);
    // AnimFrameAssign8Directions(0, FEMALE_BEACH_GIRL_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign8Directions(5,  FEMALE_BEACH_GIRL_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameAssign8Directions(10, FEMALE_BEACH_GIRL_IMAGE_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign8Directions(15, FEMALE_BEACH_GIRL_IMAGE_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameAssign8Directions(20, FEMALE_BEACH_GIRL_IMAGE_ID+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,0);
    // AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_RUN,1);
    // AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    // AnimFrameCopy8Directions(FEMALE_BEACH_GIRL_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    // AnimFrameAssign8Directions(25,FEMALE_BEACH_GIRL_IMAGE_ID+40,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_SlimeDumpOutput(){}

#define SLIME_DUMP_OUTPUT_IMAGE_START_AT 121568

void initializeImageSlimeOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_GIANT_LEECH;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_SlimeDumpOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,5,2,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,5,1,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,5,1,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0,  SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_IDLE, 1);
    // AnimFrameAssign4Directions(6,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_IDLE, 2);
    // AnimFrameAssign4Directions(9,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_IDLE, 3);
    // AnimFrameAssign4Directions(12,  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_IDLE, 4);

    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_WALK, 4);
    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+4,    IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+8,    IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions( SLIME_DUMP_OUTPUT_IMAGE_START_AT+12,    IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_RUN, 4);
    // AnimFrameCopy4Directions(  SLIME_DUMP_OUTPUT_IMAGE_START_AT+16,    IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDumpRoachOniOutput(){}

#define ROACH_ONI_START_IMAGE_ID 130133

void initializeImageFrameRoachOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_WEIRDLING_BEAST;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRoachOniOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);

    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0, ROACH_ONI_START_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3, ROACH_ONI_START_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6, ROACH_ONI_START_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(     ROACH_ONI_START_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
}

void GRPDump_Sketch(){}

#define SKETCHMAN_IMAGE_ID 122643

void initializeImageFrameSketchMan(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SHADE;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_Sketch)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign8Directions(0,SKETCHMAN_IMAGE_ID,IMAGE_SPRITE_MON_ACTION_IDLE,0);
    // AnimFrameAssign8Directions(5,SKETCHMAN_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_WALK,0    );
    // AnimFrameAssign8Directions(10,SKETCHMAN_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_WALK,1 );
    // AnimFrameAssign8Directions(15,SKETCHMAN_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_WALK,2);
    // AnimFrameAssign8Directions(20,SKETCHMAN_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_WALK,3);
    // AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+8,IMAGE_SPRITE_MON_ACTION_RUN,0    );
    // AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+16,IMAGE_SPRITE_MON_ACTION_RUN,1 );
    // AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+24,IMAGE_SPRITE_MON_ACTION_RUN,2);
    // AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_RUN,3);
    // AnimFrameCopy8Directions(SKETCHMAN_IMAGE_ID+32,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,0);
}

void GRPDump_BillyOniOutput(){}
#define AOONI_BILLY_BASE_IMAGE_ID 114160
void initializeImageFrameBillyOni(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_MECHANICAL_GOLEM;
    int xyInc[]={0,-30};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_BillyOniOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount-4);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    // ChangeMonsterSpriteImageHeader(thingId, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,8,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0,  AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3,  AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6,  AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(AOONI_BILLY_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12,frames[9], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    // AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13,frames[10], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    // AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14,frames[11], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    // AnimFrameAssignAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15,frames[12], IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    // AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+15, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    // AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+14, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    // AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+13, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    // AnimFrameCopyAsSingleImage(AOONI_BILLY_BASE_IMAGE_ID+12, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 7);
}

void GRPDump_GobdungBigOutput(){}

#define LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT 120459

void initializeImageFrameBigGopdung(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BLACK_WIDOW;
    // int xyInc[]={0,-2};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_GobdungBigOutput)+4, thingId, NULLPTR, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,2,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4,2,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign8Directions(0,  LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign8Directions(5,  LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameAssign8Directions(10, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameAssign8Directions(15, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameAssign8Directions(20, LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    // AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy8Directions( LARGE_GOPDUNG_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
}
void GRPDumpRectangle3Output(){}
void initializeImageRectangleThree(int v){
    int *frames, *sizes, thingId=OBJ_CORPSE_SKULL_SW;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpRectangle3Output)+4, thingId, NULLPTR, &frames,&sizes);
    AppendImageFrame(v,frames[1],132568);
    AppendImageFrame(v,frames[2],132524);
    AppendImageFrame(v,frames[0],132557);
}
void GRPDump_TalesweaverWallensteinOutput(){}

#define TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT 118041

void initializeImageFrameTalesweavergirl(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_LICH_LORD;
    int xyInc[]={0,-13};
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_TalesweaverWallensteinOutput)+4, thingId, xyInc, &frames,&sizes);

    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,8,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,8,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,7,2,2);
    // AnimFrameAssign8Directions(0,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT,    IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign8Directions(5,  TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameAssign8Directions(10, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameAssign8Directions(15, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameAssign8Directions(20, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameAssign8Directions(25, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_RUN, 4);
    // AnimFrameAssign8Directions(30, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_RUN, 5);
    // AnimFrameAssign8Directions(35, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_RUN, 6);
    // AnimFrameAssign8Directions(40, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_RUN, 7);
    // AnimFrameAssign8Directions(75, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+72, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
    // AnimFrameAssign8Directions(70, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+80, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 1);
    // AnimFrameAssign8Directions(65, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+88, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 2);
    // AnimFrameAssign8Directions(60, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+96, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 3);
    // AnimFrameAssign8Directions(55, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 4);
    // AnimFrameAssign8Directions(50, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 5);
    // AnimFrameAssign8Directions(45, TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+104, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 6);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+8,  IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+16, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+24, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+32, IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+40, IMAGE_SPRITE_MON_ACTION_WALK, 4);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+48, IMAGE_SPRITE_MON_ACTION_WALK, 5);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+56, IMAGE_SPRITE_MON_ACTION_WALK, 6);
    // AnimFrameCopy8Directions( TALESWEAVER_WALLENSTEIN_OUTPUT_IMAGE_START_AT+64, IMAGE_SPRITE_MON_ACTION_WALK, 7);
}
void GRPDump_LowsRPGOutput(){}
#define LOWSRPG_OUTPUT_IMAGE_START_ID 131415
void initializeImageFrameLowsRPG(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_NECROMANCER;
    int frameCount = UnpackAllFromGrp(GetScrCodeField(GRPDump_LowsRPGOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake8Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,3,3,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,3,3,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_CAST_SPELL,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign8Directions(0,  LOWSRPG_OUTPUT_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameCopy8Directions(      LOWSRPG_OUTPUT_IMAGE_START_ID, IMAGE_SPRITE_MON_ACTION_CAST_SPELL,0);
    // AnimFrameAssign8Directions(5,  LOWSRPG_OUTPUT_IMAGE_START_ID+24, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameAssign8Directions(10, LOWSRPG_OUTPUT_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign8Directions(15, LOWSRPG_OUTPUT_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy8Directions(       LOWSRPG_OUTPUT_IMAGE_START_ID+24, IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy8Directions(       LOWSRPG_OUTPUT_IMAGE_START_ID+8, IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy8Directions(       LOWSRPG_OUTPUT_IMAGE_START_ID+16, IMAGE_SPRITE_MON_ACTION_RUN, 2);
}
void GRPDump_VendingMachine(){}
void initializeVendingMachine(int imgVec){
    int *frames,*sizes;
    UnpackAllFromGrp(GetScrCodeField(GRPDump_VendingMachine)+4,OBJ_URCHIN_SHAMAN,NULLPTR,&frames,&sizes);
    AppendImageFrame(imgVec,frames[0],117757);
    AppendImageFrame(imgVec,frames[1],117769);
}

void initializeRenameItems(){
    ChangeSpriteItemNameAsString(OBJ_AMULET_OF_CLARITY, "패스트힐링 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULET_OF_CLARITY, "잠시동안 체력회복 속도를 대폭 높혀준다");
    ChangeSpriteItemNameAsString(OBJ_AMULETOF_MANIPULATION, "운석소나기 목걸이");
    ChangeSpriteItemDescriptionAsString(OBJ_AMULETOF_MANIPULATION, "이 목걸이를 사용한 지역에 운석 소나기가 내리게 한다");
    ChangeSpriteItemNameAsString(OBJ_FEAR, "전기 팬던트");
    ChangeSpriteItemDescriptionAsString(OBJ_FEAR, "이 팬던트를 사용하면 체력회복 및 쇼크 엔첸트가 부여됩니다");
}
void GRPDump_SupermanOniOutput(){}
#define SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID 113300
void initializeImageFrameSurpermanOniOutput(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SKELETON,xyInc[]={0,-15};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_SupermanOniOutput)+4, thingId, xyInc, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0, SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3, SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(     SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6, SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(     SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(     SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(     SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(     SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(     SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy4Directions(     SURPERMANONI_OUTPUT_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void ImageSpellset(){ }
void ImageSpellsetLeftbase(){ }
void ImageSpellsetRightbase(){ }
void GRPDumpHealthBar(){}

#define HEALTH_BAR_IMAGE_START_AT 113030
void initializeImageHealthBar(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_BEAR_2;
    int xyInc[]={0,35};
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDumpHealthBar)+4, thingId, xyInc, &frames, &sizes);

    AppendImageFrame(imgVector, frames[0],HEALTH_BAR_IMAGE_START_AT);
    AppendImageFrame(imgVector, frames[1],HEALTH_BAR_IMAGE_START_AT+1);
    AppendImageFrame(imgVector, frames[2],HEALTH_BAR_IMAGE_START_AT+2);
    AppendImageFrame(imgVector, frames[3],HEALTH_BAR_IMAGE_START_AT+3);
    AppendImageFrame(imgVector, frames[4],HEALTH_BAR_IMAGE_START_AT+4);
    AppendImageFrame(imgVector, frames[5],HEALTH_BAR_IMAGE_START_AT+5);
    AppendImageFrame(imgVector, frames[6],HEALTH_BAR_IMAGE_START_AT+6);
    AppendImageFrame(imgVector, frames[7],HEALTH_BAR_IMAGE_START_AT+7);
    AppendImageFrame(imgVector, frames[8],HEALTH_BAR_IMAGE_START_AT+8);
    AppendImageFrame(imgVector, frames[9],HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,0,HEALTH_BAR_IMAGE_START_AT);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,1,HEALTH_BAR_IMAGE_START_AT+1);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,2,HEALTH_BAR_IMAGE_START_AT+2);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,3,HEALTH_BAR_IMAGE_START_AT+3);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,4,HEALTH_BAR_IMAGE_START_AT+4);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,5,HEALTH_BAR_IMAGE_START_AT+5);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,6,HEALTH_BAR_IMAGE_START_AT+6);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,7,HEALTH_BAR_IMAGE_START_AT+7);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,8,HEALTH_BAR_IMAGE_START_AT+8);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,9,HEALTH_BAR_IMAGE_START_AT+9);
    ChangeMonsterSpriteImage(thingId,IMAGE_SPRITE_MON_ACTION_SLAIN,IMAGE_SPRITE_DIRECTION_RIGHT,10,HEALTH_BAR_IMAGE_START_AT+9);
}
void GRPDump_XiatallGreenOutput(){}
#define XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID 113461
void initializeImageFrameXiatallGreen(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_SKELETON_LORD;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_XiatallGreenOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0, XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3, XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(     XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6, XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(     XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(     XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(     XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(     XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(     XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy4Directions(     XIATALL_GREEN_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void GRPDump_XiatallCyanOutput(){}
#define XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID 117185
void initializeImageFrameXiatallCyan(int imgVector)
{
    int *frames, *sizes, thingId=OBJ_URCHIN;
    int frameCount= UnpackAllFromGrp(GetScrCodeField(GRPDump_XiatallCyanOutput)+4, thingId, NULLPTR, &frames,&sizes);
    AnimFrameLoaderPutParams(frames,sizes,thingId,imgVector);
    // AnimFrameMake4Directions(frameCount);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_IDLE,1,USE_DEFAULT_SETTINGS,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_WALK,4,4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_RUN,4, 4,2);
    // ChangeMonsterSpriteImageHeader(thingId,IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK,1,USE_DEFAULT_SETTINGS,2);
    // AnimFrameAssign4Directions(0, XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_IDLE, 0);
    // AnimFrameAssign4Directions(3, XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_WALK, 0);
    // AnimFrameCopy4Directions(     XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID, IMAGE_SPRITE_MON_ACTION_WALK, 1);
    // AnimFrameAssign4Directions(6, XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID+8, IMAGE_SPRITE_MON_ACTION_WALK, 2);
    // AnimFrameCopy4Directions(     XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID  , IMAGE_SPRITE_MON_ACTION_WALK, 3);
    // AnimFrameCopy4Directions(     XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID+4 , IMAGE_SPRITE_MON_ACTION_RUN, 0);
    // AnimFrameCopy4Directions(     XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID   , IMAGE_SPRITE_MON_ACTION_RUN, 1);
    // AnimFrameCopy4Directions(     XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID+8 , IMAGE_SPRITE_MON_ACTION_RUN, 2);
    // AnimFrameCopy4Directions(     XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID ,   IMAGE_SPRITE_MON_ACTION_RUN, 3);
    // AnimFrameCopy4Directions(     XIATALL_CYAN_OUTPUT_BASE_IMAGE_ID+4, IMAGE_SPRITE_MON_ACTION_MELEE_ATTACK, 0);
}
void InitializeResource(){
    int vec=CreateImageVector(IMAGE_VECTOR_MAX_COUNT);

    initializeImageFrameHansomeSquidwardOutput(vec);
    initializeImageFrameMaidenGhost(vec);
    initializeImageFrameBunnyGirlOutput(vec);
    initializeImageFrameQueenGirlOutput(vec);
    initializeImageFramePunchGirl(vec);
    initializeImageFrameRedmistOutput(vec);
    initializeImageFrameLeeMBOutput(vec);
    initializeImageFrameSoldier(vec);
    initializeImageFrameBeachGirl(vec);
    initializeImageSlimeOutput(vec);
    initializeImageFrameRoachOni(vec);
    initializeImageFrameSketchMan(vec);
    initializeImageFrameBillyOni(vec);
    initializeImageFrameBigGopdung(vec);
    initializeImageRectangleThree(vec);
    initializeImageFrameTalesweavergirl(vec);
    initializeImageFrameLowsRPG(vec);
    initializeVendingMachine(vec);
    initializeImageFrameSurpermanOniOutput(vec);
    AppendImageFrame(vec, GetScrCodeField(ImageSpellset)+4, 14415);
    AppendImageFrame(vec, GetScrCodeField(ImageSpellsetLeftbase)+4, 14446);
    AppendImageFrame(vec, GetScrCodeField(ImageSpellsetRightbase)+4, 14447);
    initializeImageHealthBar(vec);
    initializeImageFrameXiatallGreen(vec);
    initializeImageFrameXiatallCyan(vec);
    DoImageDataExchange(vec);
    initializeRenameItems();
}

