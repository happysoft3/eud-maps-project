
#include "libs/define.h"
#include "libs/printutil.h"
#include "libs/fxeffect.h"
#include "libs/observer.h"
#include "libs/format.h"
#include "libs/unitstruct.h"
#include "libs/playerinfo.h"
#include "libs/waypoint.h"
#include "libs/buff.h"
#include "libs/network.h"
#include "libs/sound_define.h"
#include "libs/objectIDdefines.h"
#include "libs/playerupdate.h"
#include "libs/logging.h"
#include "libs/queueTimer.h"
#include "libs/bind.h"
#include "libs/hash.h"

#define REAL_THISMAP_MAX_PLAYER 6
#define MAX_PLAYER_COUNT 32

int m_clientNetId;
int m_entryController;
int *m_clientKeyState;
int *m_towerAttackFn;
int m_towerAttackFnCount;
int m_userCount = 0;
int m_userGameCount=0;

#define MAX_INIT_PLAYER_COUNT 32
int m_initPlayer[MAX_INIT_PLAYER_COUNT];
int m_initPlayerCounter;

int m_player[MAX_PLAYER_COUNT];
int m_pFlag[MAX_PLAYER_COUNT];
int m_pLife[MAX_PLAYER_COUNT];
int m_pCamera[MAX_PLAYER_COUNT];
int m_userTowerCount[MAX_PLAYER_COUNT];
int m_pLineIndex[MAX_PLAYER_COUNT];
int m_genericHash;
int m_clickEventHash;

short m_score[3];
short m_time[3];

int PlrCnt, TColor[5], PlrTcount[6], MainProc = 0, MisCnt;
//int player[24], 
int PlrCor[6], PlrPtr, Tower[480], StackPtr[6];
int Pstack[480], LineMob[240], LineCnt[6], GTime, PKills[6];
int m_LineKill;
int PUpgrade[48], Upg_pay[48], MissTime, PlrList[6], Quest[240], PlrRb[6], ListPtr[6], m_KillBonus[6];
float CamSpeed = 30.0;
short m_monSubproc[97];

#define PLAYER_LINE_FLAG 2
#define PLAYER_HELPMODE_FLAG 4
#define PLAYER_DEATH_FLAG 0x80000000

static void invokeInitPlayerCb(int fn, int user)
{
    Bind(fn, &fn+4);
}

static void loadInitPlayer(int fn)
{
    int r=m_initPlayerCounter;

    while (--r>=0)
    {
        if (CurrentHealth(m_initPlayer[r]))
            invokeInitPlayerCb(fn, m_initPlayer[r]);
    }
}

static int findUserIndexByLineId(int lineId)
{
    int r=sizeof(m_player);

    while(--r>=0)
    {
        if (CurrentHealth(m_player[r]))
        {
            if (PlayerClassCheckFlag(r, PLAYER_LINE_FLAG))
            {
                if (m_pLineIndex[r]==lineId)
                    return r;
            }
        }
    }
    return -1;
}

static void initializeScoreTime()
{
    short score[]={200, 400, 700};
    short timeArr[]={180,240,300};
    
    NoxWordMemCopy(score, m_score, sizeof(score));
    NoxWordMemCopy(timeArr, m_time, sizeof(timeArr));
}

int PlayerClassCheckFlag(int plr, int flags)
{
    return m_pFlag[plr] & flags;
}

void PlayerClassSetFlag(int plr, int flags)
{
    m_pFlag[plr] ^= flags;
}

void EmptyAll(int sUnit)
{
    while (GetLastItem(sUnit))
        Delete(GetLastItem(sUnit));
}

static void initTowerAttackFunctions()
{
    int attackFn[]={
        FireBoomWeapon,
        WispWeapon,
        SmallSpiderWeapon,
        CherubStrike,
        ArcherStrike,
        GoonStrkeFunction,
        ImpStrikeFunction,
        WolfStrikeFunction,
        ShortDemonStrikeFunction,
        LichStrikeFunction,
        SpiderStrikeFunction,
        SkeletonPunches,
        SwordsmanAttack,
        OgreAxeMeleeAttackFunction,
        UrchinMissileStrikeFunction,
        WhiteWolfStrikeFunction,
        EmberDemonStrike,
        LichLordStrike,
        ScorpionStrikeEvent,
        ZombieStrikeEvent,
        CaptainWeapon,
        OgreAttackFunction,
        ShamanAttackFunction,
        BlackWolfStrikeFunction,
        RedWizWeapon,
        HovathStrikeFunction,
        MimicStrikeFunction,
        NecromancerSkill,
        HorrendousStrikeFunction,
        OgreWarlordStrikeFunction,
        TrollStrikeFunction,
        BlackBearStrikeFunction,
        DemonSkill,
        SoulTouchSkill,
        PlantSkill,
        MecaGolemHit,
        HecubahSkill,
        DryadSkill,
        StoneGolemStrikeFunction,
        IceBearStrikeFunction,
    };
    m_towerAttackFn=attackFn;
    m_towerAttackFnCount=sizeof(attackFn);
}

void GreenSparkFx(float xpos,float ypos)
{
    int ptr = CreateObjectAt("MonsterGenerator", xpos, ypos);

    PlaySoundAround(ptr, SOUND_AwardSpell);
    Damage(ptr, 0, 10, 100);
    Delete(ptr);
}

int DummyUnitCreateAt(string name, float xpos, float ypos)
{
    int unit = CreateObjectAt(name, xpos, ypos);

    ObjectOff(unit);
    Damage(unit, 0, MaxHealth(unit) + 1, -1);
    Frozen(unit, TRUE);

    return unit;
}

void ChangeColorMaiden(int red, int grn, int blue, int unit)
{
    int ptr1 = UnitToPtr(unit), k, num;

    if (!ptr1) return;
    SetMemory(ptr1 + 4, 1385);  //TODO: Maiden Unit Thing ID
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    // R  G  B  R    G  B  R  G    B  R  G  B    R  G  B  R    G  B 
    // 00 ff 00 00 / ff 00 00 ff / 00 00 ff 00 / 00 ff 00 00 / ff 00 00
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

int WizardRedBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1635412311; arr[1] = 1699898482; arr[2] = 100;
		arr[16] = 80000; arr[17] = 300; arr[18] = 100; arr[19] = 50; 
		arr[21] = 1065353216; arr[23] = 40; arr[24] = 1067869798; 
		arr[26] = 4; arr[27] = 4; 
		arr[53] = 1128792064; arr[54] = 4; 
		link=arr;
	}
	return link;
}

int MaidenBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1684627789; arr[1] = 28261;
		arr[17] = 30; arr[18] = 92; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1065688760; 
		arr[27] = 1; arr[28] = 1106247680; arr[29] = 22; 
		arr[30] = 1101004800; arr[31] = 2; arr[32] = 22; arr[33] = 30;
		arr[58] = 5546320; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int GoonBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1852796743;		
		arr[17] = 85; arr[19] = 15; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1066192077; 
		arr[26] = 4; arr[28] = 1106247680; arr[29] = 25; 
		arr[30] = 1092616192; arr[31] = 4; arr[32] = 20; arr[33] = 28; arr[34] = 2; 
		arr[35] = 3; arr[36] = 20;		
		arr[57] = 5548176; arr[58] = 5546608; arr[59] = 5543680; 
		link=arr;
	}
	return link;
}

int StrongWizardWhiteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1869771859; arr[1] = 1767335790; arr[2] = 1685217658; arr[3] = 1953065047; arr[4] = 101; 
		
		arr[16] = 80000; arr[17] = 200; arr[18] = 55; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 8; arr[24] = 1069547520; 
		arr[37] = 1701996870; arr[38] = 1819042146;
		arr[53] = 1128792064; arr[54] = 4; 
		arr[55] = 20; arr[56] = 30; arr[57] = 5547984;
		link=arr;
	}
	return link;
}

int WeirdlingBeastBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1919509847; arr[1] = 1852402788; arr[2] = 1634026087; arr[3] = 29811;
		arr[17] = 85; arr[18] = 50; arr[19] = 55; 
		arr[21] = 1065353216; arr[23] = 32776; arr[24] = 1068708659; 
		arr[26] = 4; arr[28] = 1082130432; arr[29] = 20; 
		arr[31] = 2; arr[32] = 8; arr[33] = 16;
		arr[57] = 5548112; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

int BlackWidowBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1667329090; arr[1] = 1684625259; arr[2] = 30575;
		arr[17] = 130; arr[18] = 45; arr[19] = 85; 
		arr[21] = 1065353216; arr[23] = 4; arr[24] = 1069547520; 
		arr[26] = 4; arr[27] = 3; arr[28] = 1097859072; arr[29] = 25; 
		arr[31] = 8; arr[32] = 13; arr[33] = 21; arr[34] = 50; 
		arr[35] = 3; arr[36] = 6; arr[37] = 1684631635; arr[38] = 1884516965; arr[39] = 29801; 
		arr[53] = 1128792064;
		arr[55] = 20; arr[56] = 28; arr[59] = 5544896; 
		arr[61] = 45071360; 
		link=arr;
	}
	return link;
}

int FireSpriteBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1701996870; arr[1] = 1769107539; arr[2] = 25972;
		arr[17] = 85; arr[18] = 25; arr[19] = 70; 
		arr[21] = 1065353216; arr[23] = 65544; arr[24] = 1065353216; 
		arr[37] = 1801545047; arr[38] = 1701996870; arr[39] = 1819042146; 
		arr[53] = 1128792064;
		arr[55] = 15; arr[56] = 21; arr[58] = 5545472;
		link=arr;
	}
	return link;
}

int AirshipCaptainBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1936877889; arr[1] = 1131440488; arr[2] = 1635020897; arr[3] = 28265;
		arr[17] = 130; arr[18] = 100; arr[19] = 60; 
		arr[21] = 1065353216; arr[23] = 32768; arr[24] = 1067869798; 
		arr[26] = 4; arr[28] = 1077936128; arr[29] = 20; 
		arr[31] = 8; arr[32] = 12; arr[33] = 20;
		arr[57] = 5547984; arr[58] = 5546320; arr[59] = 5542432; 
		link=arr;
	}
	return link;
}

int WoundedApprenticeBinTable()
{
	int arr[62], link;
	if (!link)
	{
		arr[0] = 1853189975; arr[1] = 1097098596; arr[2] = 1701998704; arr[3] = 1667855470; arr[4] = 101; 
		arr[17] = 50; arr[18] = 10; arr[19] = 100; 
		arr[21] = 1065353216; arr[24] = 1065353216; 
		arr[28] = 1112014848; arr[29] = 40; 
		arr[30] = 1106247680; arr[31] = 2; arr[32] = 12; arr[33] = 20;
		arr[57] = 5548112; arr[59] = 5542784; 
		link=arr;
	}
	return link;
}

void CheckMonsterThing(int unit)
{
    int thingID = GetUnitThingID(unit);
    int key = thingID % 97;

    if (thingID)
        CallFunctionWithArg(m_monSubproc[key], unit);
}

void MonsterGoonProcess(int unit)
{
    //TODO: Index. 1, ThingName= Goon
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, GoonBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(63));       //MimicVoice
        SetUnitMaxHealth(unit, 130);
    }
}

void MonsterStrongWhiteWizProcess(int unit)
{
    //TODO: Index. 2, ThingName= StrongWizardWhite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, StrongWizardWhiteBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 200);
    }
}

void MonsterWeirdlingBeastProcess(int unit)
{
    //TODO: Index. 3, ThingName= WeirdlingBeast
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WeirdlingBeastBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
        //SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(34));       //HorvathVoice
        SetUnitMaxHealth(unit, 150);
    }
}

void MonsterBlackWidowProcess(int unit)
{
    //TODO: Index. 4, ThingName= BlackWidow
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, BlackWidowBinTable());
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e8, VoiceList(19));       //HorvathVoice
        SetUnitMaxHealth(unit, 225);
    }
}

void MonsterBear2Process(int unit)
{
    //TODO: Index. 5, ThingName= Bear2 -> Maiden
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        ChangeColorMaiden(255, 0, 255, unit);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, MaidenBinTable());
        SetUnitMaxHealth(unit, 325);
    }
}

void MonsterFireSpriteProcess(int unit)
{
    //TODO: Index. 6, ThingName= FireSprite
    int ptr = UnitToPtr(unit);

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, FireSpriteBinTable());
        SetUnitMaxHealth(unit, 135);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x10000);
    }
}

void MonsterWizardRedProcess(int unit)
{
    //TODO: Index. 7, ThingName= WizardRed
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WizardRedBinTable());
        SetUnitMaxHealth(unit, 225);
        SetMemory(GetMemory(ptr + 0x2ec) + 0x54c, 0); //Flee Range set to 0
    }
}

void MonsterAirshipCaptainProcess(int unit)
{
    //TODO: Index. 8, ThingName= AirshipCaptain
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, AirshipCaptainBinTable());
        SetUnitMaxHealth(unit, 250);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
    }
}

void MonsterWoundedApprentice(int unit)
{
    //TODO: Index. 9, ThingName= WoundedApprentice
    int ptr = UnitToPtr(unit), uec;

    if (ptr)
    {
        SetMemory(GetMemory(ptr + 0x2ec) + 0x1e4, WoundedApprenticeBinTable());
        SetUnitMaxHealth(unit, 230);
        RetreatLevel(unit, 0.0);
        ResumeLevel(unit, 1.0);
        SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    }
}

static void initMonsterSubclass()
{
    m_monSubproc[OBJ_GOON%97] = MonsterGoonProcess;
    m_monSubproc[OBJ_STRONG_WIZARD_WHITE%97] = MonsterStrongWhiteWizProcess;
    m_monSubproc[OBJ_WEIRDLING_BEAST%97] = MonsterWeirdlingBeastProcess;
    m_monSubproc[OBJ_BLACK_WIDOW%97] = MonsterBlackWidowProcess;
    m_monSubproc[OBJ_BEAR_2%97] = MonsterBear2Process;
    m_monSubproc[OBJ_FIRE_SPRITE%97] = MonsterFireSpriteProcess;
    m_monSubproc[OBJ_WIZARD_RED%97] = MonsterWizardRedProcess;
    m_monSubproc[OBJ_AIRSHIP_CAPTAIN%97] = MonsterAirshipCaptainProcess;
    m_monSubproc[OBJ_WOUNDED_APPRENTICE%97] = MonsterWoundedApprentice;
}

int KeyboardIOCheckKey(int keyId)
{
    int *keyTable = 0x6950b0;
    int *selkey = keyTable+(keyId*8);
    int key = (selkey[0]>>8)&0xff;

    return key==2;
}

#define SEND_KEY_X_SHIFT 1
#define SEND_KEY_C_SHIFT 2
#define SEND_KEY_V_SHIFT 3

static void serverInputLoop()
{
    int keyZ = KeyboardIOCheckKey(44);
    int keyX = KeyboardIOCheckKey(45)<<SEND_KEY_X_SHIFT;
    int keyC = KeyboardIOCheckKey(46)<<SEND_KEY_C_SHIFT;
    int keyV = KeyboardIOCheckKey(47)<<SEND_KEY_V_SHIFT;
    int prevState, state = keyZ|keyX|keyC|keyV;

    if (prevState!=state)
    {
        prevState=state;
        m_clientKeyState[31]=state;
    }
    FrameTimer(3, serverInputLoop);
}

static void ClientKeyHandlerRemix()
{
    int keyZ = KeyboardIOCheckKey(44);
    int keyX = KeyboardIOCheckKey(45)<<SEND_KEY_X_SHIFT;
    int keyC=KeyboardIOCheckKey(46)<<SEND_KEY_C_SHIFT;
    int keyV=KeyboardIOCheckKey(47)<<SEND_KEY_V_SHIFT;
    int prevState, state = keyZ|keyX|keyC|keyV;

    if (prevState!=state)
    {
        prevState=state;

        char testPacket[]={0x3d, 0x00+(m_clientNetId*4),0x10,0x75,0x00, 0, 0,0,0};
        int *p = testPacket + 1;

        p[1] = prevState;
        NetClientSendRaw(31, 0, testPacket, sizeof(testPacket));
    }
}

int KillBonusGold(int num)
{
    int bonus[]={
        200,300,400,500,600,
    };
    return bonus[num];
}

int KillBonusTable(int num)
{
    int arr[10];

    if (!arr[0])
    {
        arr[0] = 250; arr[1] = 500; arr[2] = 750; arr[3] = 1000; arr[4] = 0;
        arr[5] = 200; arr[6] = 300; arr[7] = 400; arr[8] = 500; arr[9] = 0;
        return 0;
    }
    return arr[0];
}

void AddList(int pIndex, int data)
{
    int lineId=m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    int ptr = CreateObject("InvisibleLightBlueHigh", 124), k;

    if (IsObjectOn(PlrList[lineId]))
        SetOwner(PlrList[lineId], ptr);
    PlrList[lineId] = ptr;
    LookWithAngle(PlrList[lineId], data);
    TeleportLocationVector(124,  MathSine(k + 90, 10.0),  MathSine(k, 10.0));
    k ++;
}

int ExportList(int pIndex)
{
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return -1;

    int res, temp;

    if (IsObjectOn(PlrList[lineId]))
    {
        temp = PlrList[lineId];
        res = GetDirection(temp);
        PlrList[lineId] = GetOwner(temp);
        Delete(temp);
        return res;
    }
    else
        PlrList[lineId] = 0;
    return -1;
}

int InitBossTimeCountdown()
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 122) + 1;

    for (k = 0 ; k < 18 ; k ++)
    {
        LookWithAngle(CreateObject("InvisibleLightBlueHigh", 122), k % 6);
        TeleportLocationVector(122, 3.0, 0.0);
    }
    Delete(ptr - 1);
    return ptr;
}

// int IsClickedUnit()
// {
//     int unit;

//     if (!unit)
//     {
//         unit = CreateObject("Hecubah", 1);
//         MoveObject(unit, 100.0, 100.0);
//         Frozen(unit, 1);
//     }
//     return unit;
// }

string UpgradeBooks(int num)
{
    string name[] = {
        "AbilityBook", "ConjurerSpellBook", "CommonSpellBook", "SpellBook", "FieldGuide",
        "BlackBook1", "BookOfOblivion", "Compass"
    };
    return name[num];
}

string UpgradeName(int num)
{
    string name[] = {
        "화염", "전기", "독", "언데드", "인간", "오우거", "흙", "생명",
    };
    return name[num];
}

void UniqueTowerUnitPlace()
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 153), k;
    float x=GetObjectX(ptr), y=GetObjectY(ptr);

    for (k = 0 ; k < 8 ; k ++)
    {
        Frozen(CreateObjectAt(TowerTypes(k + 24), x+ MathSine(k * 45 + 90, 150.0), y + MathSine(k * 45, 150.0)), TRUE);
        CheckMonsterThing(ptr + k + 1);
        LookWithAngle(ptr + k + 1, k + 24);
        SetCallback(ptr + k + 1, 9, SelectLv3Tower);
    }
}

#define SPECIAL_COUNT 9
void SpecialUnitSelectRoomSetting()
{
    float x=LocationX(123),y=LocationY(123);
    int special[SPECIAL_COUNT], k=SPECIAL_COUNT;

    while(--k>=0)
    {
        float point[]={x + MathSine(k * 40 + 90, 150.0), y + MathSine(k * 40, 150.0)};

        if (k < 8)
        {
            special[k]=CreateObjectAt(TowerTypes(k + 16), point[0],point[1]);
            Frozen(special[k], TRUE);
            CheckMonsterThing(special[k]);
            LookWithAngle(special[k], k + 16);
            SetCallback(special[k], 9, SelectLv2Tower);
        }
        else
        {
            special[k]=CreateObjectAt("Maiden", point[0],point[1]);
            Frozen(special[k], TRUE);
            Frozen(CreateObjectAt("TreasureChest", point[0],point[1]), TRUE);
            SetCallback(special[k], 9, ScoreToGold);
        }
    }
    FrameTimer(5, UniqueTowerUnitPlace);
    PushTimerQueue(10, 0, PutCameraMoveButton);
}

void CameraMoveToMobList()
{
    int lineId = GetDirection(OTHER), k;
    int pIndex=findUserIndexByLineId(lineId);

    if (pIndex<0)
        return;

    char buff[1024];

    NoxByteMemset(buff, sizeof(buff), 0);
    StringUtilCopy(StringUtilGetScriptStringPtr("속성 보는법(좌측부터): "), buff);

    char *p, lbuff[128];
    int cam=m_pCamera[pIndex];

    for (k = 0 ; k < 8 ; k ++)
    {
        p=StringUtilGetScriptStringPtr(UpgradeName(k));
        NoxSprintfString(lbuff, "%s/ ", &p, 1);
        StringUtilAppend(buff, lbuff);
    }
    MoveObject(cam, 4407.0, 2064.0);
    UniPrint(m_player[pIndex], "레밸 별 유닛목록으로 화면을 전환합니다");
    UniPrint(m_player[pIndex], ReadStringAddressEx(buff));
}

void CamMoveButtonDecor()
{
    int k = REAL_THISMAP_MAX_PLAYER;

    while (--k>=0)
        Frozen(CreateObject("BlueOrbKeyOfTheLich", 147 + k), TRUE);
}

void PutCameraMoveButton(int ct)
{
    int k = REAL_THISMAP_MAX_PLAYER, ptr = CreateObject("RedPotion", 1) + 1;

    while (--k>=0)
    {
        Damage(CreateObject("Maiden", 147 + k), 0, 999, -1);
        SetCallback(ptr + k, 9, CameraMoveToMobList);
        Frozen(ptr + k, TRUE);
    }
    Delete(ptr - 1);
    FrameTimer(1, CamMoveButtonDecor);
}

void SelectLv2Tower()
{
    int pIndex;

    if (!HashGet(m_genericHash, GetCaller(), &pIndex, FALSE))
        return;

    int user = m_player[pIndex], em;

    if (HasEnchant(user, "ENCHANT_PROTECT_FROM_POISON"))
    {
        EnchantOff(user, "ENCHANT_PROTECT_FROM_POISON");
        em = CheckItem(user, 1);
        if (em)
        {
            Delete(em);
            AddList(pIndex, GetDirection(SELF));
            UniPrint(user, "레어 타워를 구입하셨습니다, 구입하신 타워는 다음에 짓는 타워에서 나오게 됩니다");
        }
        else
            UniPrint(user, "에메랄드가 부족합니다");
    }
    else
    {
        Enchant(user, "ENCHANT_PROTECT_FROM_POISON", 0.8);
        UniPrint(user, "원하는 레어타워를 고르세요, 에메랄드가 필요합니다");
        UniPrint(user, "이 작업을 계속하려면 한번 더 클릭하십시오");
    }
}

void SelectLv3Tower()
{
    int pIndex;

    if (!HashGet(m_genericHash, GetCaller(), &pIndex, FALSE))
        return;
    int user = m_player[pIndex];

    if (HasEnchant(user, "ENCHANT_PROTECT_FROM_POISON"))
    {
        EnchantOff(user, "ENCHANT_PROTECT_FROM_POISON");
        int em = CheckItem(user, 2);
        if (em)
        {
            Delete(em);
            AddList(pIndex, GetDirection(SELF));
            UniPrint(user, "유니크 타워를 구입했습니다, 구입하신 타워는 다음에 짓는 타워에서 나옵니다");
        }
        else
            UniPrint(user, "다이아몬드가 부족합니다");
    }
    else
    {
        Enchant(user, "ENCHANT_PROTECT_FROM_POISON", 0.8);
        UniPrint(user, "원하는 유니크타워를 고르세요, 다이아몬드 1개가 필요합니다");
        UniPrint(user, "이 작업을 계속하려면 한번 더 클릭하십시오");
    }
}

void ScoreToGold()
{
    int pIndex;

    if (!HashGet(m_genericHash, GetCaller(), &pIndex, FALSE))
        return;
    int user = m_player[pIndex];

    if (HasEnchant(user, "ENCHANT_SLOWED"))
    {
        EnchantOff(user, "ENCHANT_SLOWED");
        int em = CheckItem(user, 1);
        if (em)
        {
            Delete(em);
            ChangeGold(OTHER, 400);
            UniPrint(user, "에메랄드를 400 골드로 전환했습니다");
        }
        else
            UniPrint(user, "에메랄드가 없습니다 (에메랄드->골드 전환)");
    }
    else
    {
        UniPrint(user, "에메랄드를 400 골드로 변환합니다, 계속하려면 한번 더 클릭하십시오");
        Enchant(user, "ENCHANT_SLOWED", 0.8);
    }
}

void InitPlayerUpgrades(int pIndex)
{
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    int k, idx = lineId * 8;

    for (k = 0 ; k < 8 ; k ++)
    {
        PUpgrade[idx + k] = 0;
        Upg_pay[idx + k] = 10;
    }
}

void MissionNumbering(int ptr)
{
    int pIndex = GetDirection(ptr + 3);
    int user = m_player[pIndex];

    if (CurrentHealth(user))
    {
        TeleportLocation(140, GetObjectX(ptr), GetObjectY(ptr) + 39.0);
        SetOwner(user, CreateObject("TeleportGlyph1", 140));
        TeleportLocation(140, GetObjectX(ptr + 1), GetObjectY(ptr + 1) + 39.0);
        SetOwner(user, CreateObject("TeleportGlyph2", 140));
        TeleportLocation(140, GetObjectX(ptr + 2), GetObjectY(ptr + 2) + 39.0);
        SetOwner(user, CreateObject("TeleportGlyph3", 140));
    }
}

void PutPersonalMissionButton(int lineId)
{
    int pIndex=findUserIndexByLineId(lineId);

    if (pIndex<0)
    {
        UniPrintToAll("failed");
        return;
    }

    int unit = CreateObject("Ankh", 116 + lineId);
    Frozen(unit, 1);
    TeleportLocationVector(116 + lineId, 138.0, 0.0);
    Frozen(CreateObject("Ankh", 116 + lineId), 1);
    TeleportLocationVector(116 + lineId, 138.0, 0.0);
    Frozen(CreateObject("Ankh", 116 + lineId), 1);
    TeleportLocationVector(116 + lineId, 30.0, 150.0);
    Frozen(CreateObject("Ruby", 116 + lineId), 1);
    LookWithAngle(unit, 0);
    LookWithAngle(unit + 1, 1);
    LookWithAngle(unit + 2, 2);
    LookWithAngle(unit + 3, pIndex);
    Enchant(unit, "ENCHANT_FREEZE", 0.0);
    Enchant(unit, "ENCHANT_AFRAID", 0.0);
    Enchant(unit, "ENCHANT_VAMPIRISM", 0.0);
    PushTimerQueue(3, unit, MissionNumbering);
}

void DoUpgrade()
{
    int pIndex;

    if (!HashGet(m_genericHash, GetCaller(), &pIndex, FALSE))
        return;

    int user = m_player[pIndex];    
    int idx = GetDirection(GetTrigger() + 1);
    char buff[256], *p;
    int pay, lineId=m_pLineIndex[pIndex];

    if (CurrentHealth(user))
    {
        if (HasEnchant(user, "ENCHANT_CROWN"))
        {
            if (PlrRb[lineId] >= Upg_pay[lineId * 8 + idx])
            {
                PlrRb[lineId] -= Upg_pay[lineId * 8 + idx];
                p=StringUtilGetScriptStringPtr(UpgradeName(idx));
                NoxSprintfString(buff, "%s 속성 업그레이드를 했습니다", &p, 1);
                UniPrint(user, ReadStringAddressEx(buff));
                Upg_pay[lineId * 8 + idx] += 10;
                PUpgrade[lineId * 8 + idx] ++;
            }
            else
            {
                pay=Upg_pay[lineId * 8 + idx];
                NoxSprintfString(buff, "루비가 부족합니다, 요구되는 루비 양: %d", &pay, 1);
                UniPrint(user, ReadStringAddressEx(buff));
            }
            EnchantOff(user, "ENCHANT_CROWN");
        }
        else
        {
            int args[]={StringUtilGetScriptStringPtr(UpgradeName(idx)), Upg_pay[lineId * 8 + idx]};
            NoxSprintfString(buff, "%s업그레이드를 진행하려면 한번 더 클릭 하십시오.  요구 루비: %d", args, sizeof(args));
            UniPrint(user, ReadStringAddressEx(buff));
            Enchant(user, "ENCHANT_CROWN", 1.5);
        }
    }
}

void PlaceTowerUpgradeButtons(int lineId)
{
    int k, ptr = CreateObject("InvisibleLightBlueHigh", 110 + lineId) + 1;

    for (k = 0 ; k < 8 ; k ++)
    {
        SetUnitMaxHealth(CreateObject("WeirdlingBeast", 110 + lineId), 10);
        Damage(ptr + (k * 2), 0, 999, 14);
        Frozen(CreateObject(UpgradeBooks(k), 110 + lineId), 1);
        LookWithAngle(ptr + (k * 2) + 1, k);
        SetCallback(ptr + (k * 2), 9, DoUpgrade);
        TeleportLocationVector(110 + lineId, 63.0,0.0);
    }
    PushTimerQueue(30, ptr, UpgradeUnitAlign);
    PushTimerQueue(1, lineId, PutPersonalMissionButton);
    Delete(ptr - 1);
}

void UpgradeUnitAlign(int ptr)
{
    int k;

    for (k = 0 ; k < 8 ; k ++)
        MoveObject(ptr + (k * 2), GetObjectX(ptr + (k * 2) + 1), GetObjectY(ptr + (k * 2) + 1));
}

/*
Color Info
16: Purple	--Rare
17: Blue	--Magic
18: Teal	--Epic
20: Orange	--Unique
22: White	--Normal
*/

void TowerColorSet()
{
    TColor[0] = 22; //Normal
    TColor[1] = 17; //Magic
    TColor[2] = 16; //Rare
    TColor[3] = 20; //Unique
    TColor[4] = 18; //Epic
}

int TowerSellPay(int num)
{
    int pay[5];

    if (!pay[0])
    {
        pay[0] = 50; pay[1] = 100; pay[2] = 200; pay[3] = 400; pay[4] = 600;
        return 0;
    }
    return pay[num];
}

void LadderBoard()
{
    char buff[2048];

    NoxSprintfString(buff, "남은시간: %d 초\n플레이어 골드 보유현황\n", &GTime, 1);
    
    int k = sizeof(m_player), user, lineId;
    char lbuff[192];

    while (--k >=0)
    {
        user=m_player[k];
        if (CurrentHealth(user))
        {
            if (PlayerClassCheckFlag(k, PLAYER_LINE_FLAG))
            {
                lineId=m_pLineIndex[k];
                int args[]={StringUtilGetScriptStringPtr(PlayerIngameNick(user)), GetGold(user), PlrRb[lineId], PKills[lineId]};
                NoxSprintfString(lbuff, "%s\n골드: %d, 루비: %d, 킬: %d\n", args, sizeof(args));
                StringUtilAppend(buff, lbuff);
            }
        }
    }
    if (GTime)
        GTime --;
    UniChatMessage(GetMaster(), ReadStringAddressEx(buff), 60);
    SecondTimer(1, LadderBoard);
}

int GetMaster()
{
    int unit;

    if (!unit)
    {
        unit = CreateObject("Hecubah", 1);
        MoveObject(unit, 5813.0, 514.0);
        Frozen(unit, 1);
    }
    return unit;
}

void PlayerInitStack(int pIndex)
{
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;
        
    int k = 80;

    while (--k>=0)
        PlayerStackPush(lineId, (lineId * 80) + k);
}

int PlayerStackPop(int pIndex)
{
    int lineId=m_pLineIndex[pIndex];

    if (lineId<0)
        return 0;

    StackPtr[lineId] --;
    return Pstack[StackPtr[lineId]];
}

void PlayerStackPush(int pIndex, int value)
{
    int lineId=m_pLineIndex[pIndex];

    if (lineId<0)
        return;
    Pstack[(lineId * 80) + StackPtr[lineId]] = value;
    StackPtr[lineId] ++;
}

void RemovePlayerTowers(int plr)
{
    int k, idx = (plr * 80);

    for (k = 79 ; k >= 0 ; k --)
    {
        TowerRemove(Tower[idx + k]);
    }
}

void InitPlrPtrSetting()
{
    int k;
    PlrPtr = CreateObject("RedPotion", 95) + 1;
    for (k = 5 ; k >= 0 ; k --)
    {
        CreateObject("InvisibleLightBlueHigh", 95);
        CreateObject("InvisibleLightBlueHigh", 95);
        TeleportLocationVector(95, 3.0, 0.0);
    }
    Delete(PlrPtr - 1);
}

void InitUseMapSetting()
{
    int k = REAL_THISMAP_MAX_PLAYER;

    while (--k>=0)
    {
        TeleportLocation(81 + (k * 2), LocationX((k + 2) * 10), LocationY((k + 2) * 10));
        TeleportLocation(82 + (k * 2), LocationX((k + 2) * 10), LocationY((k + 2) * 10));
        PushTimerQueue(k + 1, k, PutMarkOnStadium);
    }
}

static void onClickEventFlag(int pIndex, int trg, int caller)
{
    Raise(trg + 1, ToFloat(caller));
    ObjectOff(trg + 1);
    Effect("VIOLET_SPARKS", GetObjectX(caller), GetObjectY(caller), 0.0, 0.0);
}

static void onClickEventAnkh(int pIndex, int trg, int caller)
{
    Lv1SummonBoss(pIndex, m_score[GetDirection(caller)], m_time[GetDirection(caller)], GetDirection(caller));
}

static void onClickEventRuby(int pIndex, int trg, int caller)
{
    BuyRubyEvent(pIndex);
}

static void invokeClickEvent(int fn, int pIndex, int trg, int caller)
{
    Bind(fn, &fn+4);
}

void ClickCollideEvent() //Handling_Conbine_Event_Function
{
    int pIndex;

    if (HashGet(m_genericHash, GetTrigger(), &pIndex, FALSE))
    {
        if (CurrentHealth(m_player[pIndex]))
        {
            int fn;

            if (HashGet(m_clickEventHash, GetUnitThingID(OTHER), &fn, FALSE))
                invokeClickEvent(fn, pIndex, GetTrigger(), GetCaller());
        }
    }
}

void BuyRubyEvent(int pIndex)
{
    char buff[192];
    int user =m_player[pIndex];
    int lineId=m_pLineIndex[pIndex];

    if (HasEnchant(user, "ENCHANT_CROWN"))
    {
        EnchantOff(user, "ENCHANT_CROWN");
        if (GetGold(user) >= 100)
        {
            ChangeGold(user, -100);
            int rnd = Random(20, 100);
            NoxSprintfString(buff, "루비를 구입했습니다 (+%d 루비)", &rnd, 1);
            UniPrint(user, ReadStringAddressEx(buff));
            PlrRb[lineId] += rnd;
        }
        else
            UniPrint(user, "루비 구입에 실패했습니다, 에러코드 메시지: 금액부족");
    }
    else
    {
        Enchant(user, "ENCHANT_CROWN", 0.8);
        UniPrint(user, "업그레이드에 필요한 루비를 구입합니다, 계속하려면 한번 더 클릭하세요 [100골드 필요]");
    }
}

void Lv1BossTimeCounter(int ptr)
{
    int lineId = GetDirection(ptr);
    int pIndex=findUserIndexByLineId(lineId);

    if (pIndex<0)
        return;
    int user =m_player[pIndex];
    int time = ToInt(GetObjectZ(ptr)), lv = GetDirection(GetOwner(ptr));
    char buff[128];

    if (time && CurrentHealth(user))
    {
        time --;
        Raise(ptr, ToFloat(time));
        if (!time)
        {
            ++lv;
            NoxSprintfString(buff, "%d 번째 개인미션 쿨다운이 완료되었습니다", &lv, 1);
            UniPrint(user, ReadStringAddressEx(buff));
        }
        PushTimerQueue(30, ptr, Lv1BossTimeCounter);
    }
    else
    {
        Delete(GetOwner(ptr));
        Raise(ptr, ToFloat(0));
    }
}

void Lv1SummonBoss(int pIndex, int score, int time, int lv)
{
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    int unit, user=m_player[pIndex];
    char buff[256];

    if (!ToInt(GetObjectZ(MissTime + (6 * lv) + lineId)))
    {
        int args[]={lv+1, time};
        NoxSprintfString(buff, "%d단계 개인미션을 시작합니다, 개인 미션 유닛을 놓치게 되면 목숨을 많이 잃게됩니다 [쿨다운:%d초]", args, sizeof(args));
        UniPrint(user, ReadStringAddressEx(buff));
        SpawnMissionMonster(pIndex, lv, score);
        MisCnt ++;
        unit = CreateObject("InvisibleLightBlueHigh", 1);
        LookWithAngle(unit, lv);
        SetOwner(unit, MissTime + lineId + (6 * lv));
        Raise(MissTime + (6 * lv) + lineId, ToFloat(time));
        SecondTimerWithArg(1, MissTime + (6 * lv) + lineId, Lv1BossTimeCounter);
    }
    else
    {
        int n = ToInt(GetObjectZ(MissTime + (6 * lv) + lineId));
        NoxSprintfString(buff, "쿨다운 %d 초 남았습니다", &n, 1);
        UniPrint(user, ReadStringAddressEx(buff));
    }
}

void MarkProperties(int plr, int wp)
{
    int unit = CreateObject("WeirdlingBeast", wp);

    SetUnitMaxHealth(unit, 10);
    Raise(CreateObject("InvisibleLightBlueLow", wp), ToFloat(1000));
    Damage(unit, 0, MaxHealth(unit) + 1, 14);
    LookWithAngle(unit, plr);
}

void DrawVaildMarker(int arg_0, int wp, int plr)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(wp);
		pos_y = GetWaypointY(wp);
	}
	for (i = 1 ; i > 0 && count < 155 ; i <<= 1)
	{
		if (i & arg_0)
			MarkProperties(plr, wp);
		if (count % 12 == 11)
		{
            TeleportLocationVector(wp + 1, 46.0, 46.0);
            TeleportLocation(wp, LocationX(wp + 1), LocationY(wp + 1));
        }
		else
			TeleportLocationVector(wp, - 46.000000, 46.0);
		count ++;
	}
	if (count >= 155)
	{
		count = 0;
		TeleportLocation(wp, pos_x, pos_y);
	}
}

void PutMarkOnStadium(int plr)
{
    int k = -1, arr[] ={ 1853286240, 1610673614, 4046811, 1941388144, 24694};
    
    while (++k < 5)
    {
        DrawVaildMarker(arr[k], 81 + (plr * 2), plr);
    }
}

void PlayerClassOnShutdown(int pIndex)
{
    PlayerClassDeleteCamera(pIndex);
    RemovePlayerTowers(pIndex);
    m_player[pIndex]=0;
    m_pFlag[pIndex]=0;
    --m_userCount;
    if (m_pLineIndex[pIndex]>=0)
        PKills[m_pLineIndex[pIndex]] = 0;
    m_pLineIndex[pIndex]=-1;
}

void PlayerClassOnDeath(int plr, int user)
{
    WriteLog("playerclassondeath");
    char dieMsg[128], *p =StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(dieMsg, "방금 %s님께서 적에게 격추되었습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(dieMsg));
    WriteLog("playerclassondeath end");
}

// static void onPlayerClickAt(int point)
// {
//     Delete(point);
// }

#define USER_CAM_SPEED 30.0

static void onHandlingPlayerCamera(int pIndex, int user, int cam)
{
    int ix,iy;
    
    GetPlayerMouseXY(user, &ix, &iy);
    float mousePoint[]={IntToFloat(ix), IntToFloat(iy)};
    float camPoint[]={GetObjectX(cam), GetObjectY(cam)};
    int goX = Distance(camPoint[0],0.0,mousePoint[0],0.0)>480.0;
    int goY=Distance(0.0, camPoint[1], 0.0, mousePoint[1])>350.0;

    if (goX|goY)
    {
        float xyVect[2];

        ComputePointRatio(mousePoint, camPoint, xyVect, USER_CAM_SPEED);
        MoveObjectVector(cam, xyVect[0], xyVect[1]);
    }

    if (CheckWatchFocus(user))
    {
        if (PlayerClassCheckFlag(pIndex, PLAYER_LINE_FLAG))
        {
            if (!UnitCheckEnchant(user, GetLShift(ENCHANT_PROTECT_FROM_ELECTRICITY)))
            {
                Enchant(user, EnchantList(ENCHANT_PROTECT_FROM_ELECTRICITY), 0.7);
                int glow=CreateObjectById(OBJ_MOONGLOW, GetObjectX(user), GetObjectY(user));
                PushTimerQueue(1, glow, ClickLocationAt);
                HashPushback(m_genericHash, glow, pIndex);
                SetOwner(user, glow);
            }
        }
        PlayerLook(user, cam);
    }
}

#define KEY_Z_FLAG 1
#define KEY_X_FLAG 2
#define KEY_C_FLAG 4
#define KEY_V_FLAG 8

static void serverTaskUserInputHandler(int pIndex)
{
    int keyState=m_clientKeyState[pIndex];
    int prevState[32];

    if (keyState&KEY_Z_FLAG)
    {
        if (!(prevState[pIndex]&KEY_Z_FLAG))
            BuildRandomTower(pIndex);
    }
    if (keyState&KEY_X_FLAG)
    {
        if (!(prevState[pIndex]&KEY_X_FLAG))
            ScreenMoveToCenter(pIndex);
    }
    if (keyState&KEY_C_FLAG)
    {
        if (!(prevState[pIndex]&KEY_C_FLAG))
            SellYourTower(pIndex);
    }
    if (keyState&KEY_V_FLAG)
    { } //spare
    if (keyState!=prevState[pIndex])
        prevState[pIndex]=keyState;
}

void PlayerClassOnAlive(int pIndex, int user)
{
    onHandlingPlayerCamera(pIndex, user, m_pCamera[pIndex]);
    if (PlayerClassCheckFlag(pIndex, PLAYER_LINE_FLAG))
        serverTaskUserInputHandler(pIndex);
}

void PlayerClassOnLoop(int pIndex)
{
    int user=m_player[pIndex];
    
    while (TRUE)
    {
        if (MaxHealth(user))
        {
            if (GetUnitFlags(user) & UNIT_FLAG_NO_COLLIDE)
                1;
            else if (CurrentHealth(user))
            {
                PlayerClassOnAlive(pIndex, user);
                break;
            }
            else
            {
                if (PlayerClassCheckFlag(pIndex, PLAYER_DEATH_FLAG))
                    break;
                else
                {
                    PlayerClassSetFlag(pIndex, PLAYER_DEATH_FLAG);
                    PlayerClassOnDeath(pIndex, user);
                }
                break;
            }                
        }
        if (m_pFlag[pIndex])
            PlayerClassOnShutdown(pIndex);
        break;
    }
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

void TowerGameInit()
{
    TowerDamage(0);
    LineHpTable(0);
    initializeScoreTime();
    FrameTimer(1, SpecialUnitSelectRoomSetting);
    HashCreateInstance(&m_clickEventHash);
    HashPushback(m_clickEventHash, OBJ_ANKH, onClickEventAnkh);
    HashPushback(m_clickEventHash, OBJ_RUBY, onClickEventRuby);
    HashPushback(m_clickEventHash, OBJ_FLAG, onClickEventFlag);

    
}

void MapInitialize()
{
    GetMaster();
    m_clientKeyState=0x751000;
    CreateLogFile("rtddfc-log.txt");
    m_entryController=playerGoSelectRoom;
    initMonsterSubclass();
    initTowerAttackFunctions();
    HashCreateInstance(&m_genericHash);
    MusicEvent();
    FrameTimer(13, TowerGameInit);
    MissTime = InitBossTimeCountdown();
    MusicEvent();
    TowerSellPay(0);
    GetItemColors(0);
    SetMemory(0x75ae40, 0);
    FrameTimer(150, InitUseMapSetting);
    FrameTimer(18, TowerColorSet);
    FrameTimer(19, InitPlrPtrSetting);
    FrameTimer(50, StrWaitRoom);
    FrameTimer(50, StrGameBegin);
    FrameTimer(100, PutGameStartButton);
    FrameTimer(120, PutDescriptStr);
    SecondTimerWithArg(5, 0, StartGuideMessage);
    FrameTimer(60, serverInputLoop);
}

void EntryMainProc()
{
    KillBonusTable(0);
    FrameTimer(60, LadderBoard);
    SecondTimer(1, InitCurrentWave);
    FrameTimer(45, DisableMapEntry);
    FrameTimer(110, StrRareTowerShop);
    FrameTimer(30, FigureOutTowers);
}

static void initPlayerProc(int user)
{
    PlayerClassOnEntry(user);
}

static void onstartGame()
{
    int count=REAL_THISMAP_MAX_PLAYER;

    while (--count>=0)
        PushTimerQueue(10+count, count, PlaceTowerUpgradeButtons);
}

void PressStartGame()
{
    if (!IsPlayerUnit(OTHER))
        return;

    Delete(SELF);
    MainProc = 1;
    EntryMainProc();
    loadInitPlayer(initPlayerProc);
    char buff[128], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(OTHER));
    m_entryController = PlayerClassOnEntry;
    onstartGame();
    NoxSprintfString(buff, "%s 님께서 게임시작 버튼을 누르셨습니다", &p, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

void PutGameStartButton()
{
    int ptr = DummyUnitCreateAt("Bomber", LocationX( 127), LocationY(127) );

    CreateObjectAt("PlayerWaypoint", GetObjectX(ptr), GetObjectY(ptr));
    SetCallback(ptr, 9, PressStartGame);
}

void DisableMapEntry()
{
    UniPrintToAll("지금부터 플레이어들은 더 이상 맵에 입장할 수 없습니다");
    ObjectOff(Object("MainGameSwitch"));
}

int CheckItem(int unit, int id)
{
    int cur = GetLastItem(unit);

    while (IsObjectOn(cur))
    {
        if (GetDirection(cur) == id)
            return cur;
        cur = GetPreviousItem(cur);
    }
    return 0;
}

void ScreenMoveToCenter(int pIndex)
{
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    int user = m_player[pIndex];

    if (!CurrentHealth(user))
        return;

    MoveObject(m_pCamera[pIndex], LocationX(lineId + 1), LocationY(lineId + 1));
    UniPrint(user, "카메라를 중앙으로 이송했습니다");
}

void SellCheckUnitTouch()
{
    int plr = GetDirection(SELF);

    if (HasClass(OTHER, "FLAG"))
    {
        ObjectOff(GetTrigger() + 1);
        LookWithAngle(GetTrigger() + 1, GetDirection(OTHER)); //GetTowerLv
        Raise(GetTrigger() + 1, ToFloat(GetCaller()));
    }
}

void TowerSellControl(int sub)
{
    int plr = GetDirection(sub);
    int user = GetOwner(sub);

    if (CurrentHealth(user))
    {
        if (!IsObjectOn(sub + 1))
        {
            int tower = ToInt(GetObjectZ(sub + 1));

            PlaySoundAround(sub, SOUND_TreasurePickup);
            TowerRemove(tower);
            ChangeGold(user, TowerSellPay(GetDirection(tower)));
            UniPrint(user, "타워 판매를 성공했습니다");
        }
    }
    Delete(sub);
    Delete(sub + 1);
}

void TowerSellEvent(int point)
{
    int plr = GetDirection(point);
    int user = m_player[plr];

    if (CurrentHealth(user))
    {
        int sub = DummyUnitCreateAt("Demon", GetObjectX(point), GetObjectY(point));
        CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(sub), GetObjectY(sub));
        LookWithAngle(sub, plr);
        SetCallback(sub, 9, SellCheckUnitTouch);
        SetOwner(user, sub);
        PushTimerQueue(1, sub, TowerSellControl);
    }
    Delete(point);
}

void SellYourTower(int plr)
{
    int user = m_player[plr];

    if (!CurrentHealth(user))
        return;

    int point = CreateObjectById(OBJ_MOONGLOW, GetObjectX(user), GetObjectY(user));

    SetOwner(user, point);
    LookWithAngle(point, plr);
    UniPrint(user, "판매할 타워를 선택하십시오");
    PushTimerQueue(1, point, TowerSellEvent);
}

void AnalyzeClickEvent(int ptr)
{
    int pIndex;

    while (HashGet(m_genericHash, ptr, &pIndex, TRUE))
    {
        int lineId = m_pLineIndex[pIndex];

        if (lineId<0)
            break;

        int target = ToInt(GetObjectZ(ptr + 1)), fst;
        int user = m_player[pIndex], lv;
        float x,y;

        if (CurrentHealth(user))
        {
            int con = !IsObjectOn(ptr + 1);

            if (UnitCheckEnchant(user, GetLShift(ENCHANT_AFRAID)) )
            {
                EnchantOff(user, "ENCHANT_AFRAID");
                Delete(ToInt(GetObjectZ(PlrPtr + (lineId * 2) + 1)));
                if (con)
                {
                    fst = ToInt(GetObjectZ(PlrPtr + (lineId * 2)));
                    if (GetDirection(fst + 3) == GetDirection(target + 3) && IsOwnedBy(target + 1, user) && fst != target)
                    {
                        if (GetDirection(target) < 4)
                        {
                            WriteLog("tower combinations");
                            lv = GetDirection(target);
                            WriteLog(IntToString(lv));
                            x=GetObjectX(target);
                            y=GetObjectY(target);
                            TowerRemove(fst);
                            WriteLog("remove first");
                            TowerRemove(target);
                            WriteLog("remove second");
                            SpawnRandomTower(pIndex, x,y, lv + 1, -1);
                            WriteLog("end combine");
                            UniPrint(user, "타워 조합을 성공하였습니다");
                        }
                        else
                            UniPrint(user, "이미 타워 레밸이 최대치입니다");
                    }
                    else
                        UniPrint(user, "조합실패! 첫번째 선택된 유닛과 같은 종류의 유닛을 선택하세요");
                }
            }
            else
            {
                if (con)
                {
                    UniPrint(user, "조합유닛 선택됨, 이 유닛과 조합할 유닛을 선택하세요");
                    Enchant(user, "ENCHANT_AFRAID", 0.0);
                    Raise(PlrPtr + (lineId * 2), ToFloat(target));
                    Raise(PlrPtr + (lineId * 2) + 1, ToFloat(CreateObjectAt("BlueSummons", GetObjectX(target), GetObjectY(target))));
                }
            }
        }
        break;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void ClickLocationAt(int glow)
{
    int pIndex;

    if (HashGet(m_genericHash, glow, &pIndex, TRUE))
    {
        float x=GetObjectX(glow),y=GetObjectY(glow);
        int user = GetOwner(glow);

        if (CurrentHealth(user))
        {
            int unit = DummyUnitCreateAt("Demon", x,y);

            CreateObjectAt("InvisibleLightBlueHigh", x,y);
            SetOwner(user, unit);
            HashPushback(m_genericHash, unit, pIndex);
            LookWithAngle(unit + 1, pIndex);
            SetCallback(unit, 9, ClickCollideEvent);
            DeleteObjectTimer(CreateObjectAt("MagicSpark", x,y), 12);
            PushTimerQueue(1, unit, AnalyzeClickEvent);
        }
    }
	Delete(glow);
}

void AnalyzeBuildArea(int ptr)
{
    int pIndex;

    while (HashGet(m_genericHash, ptr, &pIndex, TRUE))
    {
        if (!GetDirection(ptr))
            break;

        int user = GetOwner(ptr);

        if (CurrentHealth(user))
        {
            // if (!HasEnchant(ptr, "ENCHANT_ETHEREAL") && HasEnchant(ptr, "ENCHANT_DETECTING"))
            // {
                if (GetGold(user) >= 100)
                {
                    SpawnRandomTower(pIndex, GetObjectX(ptr), GetObjectY(ptr), 0, ExportList(pIndex));
                    GreenSparkFx(GetObjectX(ptr), GetObjectY(ptr));
                    ChangeGold(user, -100);
                }
                else
                    UniPrint(user, "골드가 부족합니다 [타워건설: 100골드]");
            // }
        }
        break;
    }
    Delete(ptr);
}

void TargetingDetect()
{
    int pIndex = GetDirection(SELF);
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    if (CurrentHealth(GetOwner(SELF)))
    {
        if (GetUnitClass(OTHER) & UNIT_CLASS_FLAG)
            ClearOwner(SELF);
        else if (ToInt(GetObjectZ(GetCaller() + 1)) == 1000)
        {
            WriteLog(IntToString(GetDirection(OTHER)));
            WriteLog(IntToString(lineId));
            if (GetDirection(OTHER) == lineId)
            {
                MoveObject(SELF, GetObjectX(OTHER), GetObjectY(OTHER));
                LookWithAngle(SELF, 1);
            }
        }
    }
}

void DelayDetectedForBuild(int glow)
{
    if (!ToInt( GetObjectX(glow)) )
        return;

    int pIndex;

    if (!HashGet(m_genericHash, glow, &pIndex, TRUE))
        return;

    int owner = GetOwner(glow);

    if (CurrentHealth(owner))
    {
        int unit = DummyUnitCreateAt("Demon", GetObjectX(glow), GetObjectY(glow));

        HashPushback(m_genericHash, unit, pIndex);
        SetOwner(owner, unit);
        LookWithAngle(unit, 0);
        SetCallback(unit, 9, TargetingDetect);
        PushTimerQueue(1, unit, AnalyzeBuildArea);
    }
    Delete(glow);
}

void BuildRandomTower(int pIndex)
{
    int user=m_player[pIndex];
    int glow = CreateObjectById(OBJ_MOONGLOW, GetObjectX(user), GetObjectY(user));

    SetOwner(user, glow);
    HashPushback(m_genericHash, glow, pIndex);
    PushTimerQueue(1, glow, DelayDetectedForBuild);
}

int SpawnRandomTower(int pIndex, float x, float y, int lv, int sel)
{
    int lineId=m_pLineIndex[pIndex];

    if (lineId<0)
        return 0;

    int stack, pic, user = m_player[pIndex];

    if (sel + 1)
        pic = sel;
    else
        pic = (lv * 8) + Random(0, 7);
    int unit = CreateObjectAt("Flag", x,y);                    //+0:
    SetFlagColor(unit, TColor[lv]);
    LookWithAngle(unit, lv);                                //TowerLevel
    TowerUnitSetting(CreateObjectAt(TowerTypes(pic), x,y)); //+1: MainUnit
    SetOwner(user, unit + 1);
    CreateObjectAt("InvisibleLightBlueLow", x,y);          //+2:
    Raise(unit + 2, ToFloat(TowerDamage(pic)));        //TowerDamage
    LookWithAngle(unit + 2, pic % 8);                        //TowerUpgradeType
    CreateObjectAt("InvisibleLightBlueLow", x,y);          //+3:
    LookWithAngle(unit + 3, pic);                           //tower_index_id
    Raise(unit + 3, ToFloat(pIndex));                    //player_index_number
    Enchant(unit + 1, "ENCHANT_HELD", 0.0);
    Enchant(unit + 1, "ENCHANT_ANCHORED", 0.0);
    stack = PlayerStackPop(pIndex);
    CreateObjectAt("InvisibleLightBlueLow", x,y);          //+4:
    Raise(unit + 4, ToFloat(stack));                         //Created_Unit_index
    AggressionLevel(unit + 1, 1.0);
    RetreatLevel(unit, 0.0);
    SetCallback(unit + 1, 3, m_towerAttackFn[pic]);
    SetCallback(unit + 1, 10, UnitHearEvent);
    LookWithAngle(unit + 1, 192);
    ++m_userTowerCount[pIndex];
    Tower[stack] = unit;
    Quest[(lineId * 40) + pic] ++;
    PushTimerQueue(1, pIndex, CheckQuestList);
    return unit;
}

void TowerUnitSetting(int unit)
{
    CheckMonsterThing(unit);
    SetUnitMaxHealth(unit, 2000);
    SetUnitFlags(unit, GetUnitFlags(unit) | 0x4040);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x40);
    SetUnitMass(unit, 999999.0);
}

void TowerRemove(int unit)
{
    int pIndex = ToInt(GetObjectZ(unit + 3)), stack = ToInt(GetObjectZ(unit + 4));
    int lineId=m_pLineIndex[pIndex];

    if (CurrentHealth(m_player[pIndex]))
        PlayerStackPush(pIndex, stack);
    Quest[(lineId * 40) + GetDirection(unit + 3)] --;
    m_userTowerCount[pIndex] --;
    Delete(unit);
    Delete(unit + 1);
    Delete(unit + 2);
    Delete(unit + 3);
    Delete(unit + 4);
}

string TowerTypes(int num)
{
    string name[] = {
        //0             1           2                   3                   4                5              6               7
        "FireSprite", "WillOWisp", "SmallAlbinoSpider", "EvilCherub",       "Archer",       "Goon",         "Imp",          "Wolf", //Lv. 1
        "MeleeDemon", "Lich",      "Spider",            "SkeletonLord",     "Swordsman",    "GruntAxe",     "Urchin",       "WhiteWolf", //Lv. 2
        "EmberDemon", "LichLord",  "Scorpion",          "VileZombie",       "AirshipCaptain","OgreBrute",   "UrchinShaman", "BlackWolf", //Lv. 3
        "WizardRed",  "WizardWhite", "Mimic",           "Necromancer",      "Horrendous",   "OgreWarlord",  "Troll",        "BlackBear", //Lv. 4
        "Demon",     "Beholder",    "CarnivorousPlant", "MechanicalGolem",  "Hecubah",      "WizardGreen",  "StoneGolem",   "Bear" //Lv. 5
    };
    return name[num];
}

int TowerDamage(int num)
{
    int dam[40];

    if (!dam[0]) //here
    {
        dam[0] = 9;     dam[1] = 10;    dam[2] = 2;     dam[3] = 10;    dam[4] = 5;     dam[5] = 3;     dam[6] = 7;     dam[7] = 15;
        dam[8] = 22;    dam[9] = 20;    dam[10] = 18;   dam[11] = 19;   dam[12] = 15;   dam[13] = 35;   dam[14] = 21;   dam[15] = 19;
        dam[16] = 32;   dam[17] = 19;   dam[18] = 20;   dam[19] = 20;   dam[20] = 25;   dam[21] = 20;   dam[22] = 50;   dam[23] = 30;
        dam[24] = 24;   dam[25] = 25;   dam[26] = 27;   dam[27] = 80;   dam[28] = 25;   dam[29] = 74;   dam[30] = 25;   dam[31] = 30;
        dam[32] = 40;   dam[33] = 48;   dam[34] = 38;   dam[35] = 100;   dam[36] = 60;   dam[37] = 82;   dam[38] = 30;   dam[39] = 37;
        return 0;
    }
    return dam[num];
}

string LineMonster(int num)
{
    string name[] = {
        "GreenFrog", "Goon", "Rat", "Swordsman", "GiantLeech",
        "WeirdlingBeast", "Skeleton", "Bear2", "GruntAxe", "Bear2",

        "BlackWolf", "Wizard", "SmallAlbinoSpider", "EvilCherub", "OgreBrute",
        "MeleeDemon", "UrchinShaman", "FlyingGolem", "WoundedApprentice", "Scorpion",

        "AlbinoSpider", "Bat", "Wasp", "Troll", "StrongWizardWhite",
        "AirshipCaptain", "WhiteWolf", "BlackBear", "Archer", "WizardRed",

        "WillOWisp", "WizardGreen", "EmberDemon", "SmallSpider", "SkeletonLord",
        "Lich", "Necromancer", "Mimic", "TalkingSkull", "Demon",

        "Bear", "WizardWhite", "MechanicalGolem" };
    return name[num];
}

int LineHpTable(int num)
{
    int hp[43];

    if (!hp[0])
    {
        hp[0] = 25; hp[1] = 31 | (1<<16); hp[2] = 48; hp[3] = 61; hp[4] = 113;
        hp[5] = 131; hp[6] = 197; hp[7] = 255 | (2 << 16); hp[8] = 280; hp[9] = 2290;

        hp[10] = 325; hp[11] = 390; hp[12] = 438 | (1 << 16); hp[13] = 470; hp[14] = 533;
        hp[15] = 601; hp[16] = 632; hp[17] = 650 | (1 << 16); hp[18] = 703; hp[19] = 4732;

        hp[20] = 720; hp[21] = 739; hp[22] = 722 | (1 << 16); hp[23] = 790; hp[24] = 824;
        hp[25] = 857; hp[26] = 891; hp[27] = 916; hp[28] = 969; hp[29] = 8000;

        hp[30] = 991; hp[31] = 1013; hp[32] = 1188; hp[33] = 1224 | (1 << 16); hp[34] = 1287;
        hp[35] = 1350; hp[36] = 1376; hp[37] = 1428; hp[38] = 1518; hp[39] = 13190;

        hp[40] = 800; hp[41] = 4500; hp[42] = 7050;
    }
    return hp[num];
}

void VictoryEvent()
{
    char buff[2048], *p;
    char lbuff[128];

    NoxByteMemset(buff, sizeof(buff), 0);
    StringUtilCopy(StringUtilGetScriptStringPtr("이번 게임에서 끝까지 남은분들: "), buff);
    PlayWav(SOUND_LevelUp);

    int k = sizeof(m_player);
    while (--k>=0)
    {
        if (CurrentHealth(m_player[k]))
        {
            if (PlayerClassCheckFlag(k, PLAYER_LINE_FLAG))
            {
                p=StringUtilGetScriptStringPtr(PlayerIngameNick(m_player[k]));
                NoxSprintfString(lbuff, "%s, ", &p, 1);
                StringUtilAppend(buff, lbuff);
                DeleteObjectTimer(CreateObject("BlueRain", m_pLineIndex[k] + 1), 1200);
            }
        }
    }
    StringUtilAppend(buff, StringUtilGetScriptStringPtr(", 모두 축하합니다"));
    UniPrintToAll("축하합니다_! 40개의 모든 웨이브를 다 깨셨습니다 대단합니다!!                --임무성공");
    UniPrintToAll(ReadStringAddressEx(buff));
}

void StartCurrentWave(int lv)
{
    int count, boss = 0;

    if (count>=40)
    {
        count=0;
        return;
    }

    if (lv % 10 == 9)
    {
        m_LineKill = 234;
        boss = 1;
        count = 100;
    }
    int k=sizeof(m_player), ptr;

    while (--k>=0)
    {
        if (PlayerClassCheckFlag(k, PLAYER_LINE_FLAG))
        {
            if (CurrentHealth(m_player[k]))
            {
                ptr = SpawnLineMonster(k, lv);
                if (boss)
                {
                    ObjectOff(ptr + 1);
                    Raise(ptr + 1, ToFloat(200));
                    Enchant(ptr, "ENCHANT_INFRAVISION", 0.0);
                }
            }
            else
                CheckLineKills(0);
        }
    }
    count ++;
    PushTimerQueue(25, lv, StartCurrentWave);
}

void InitCurrentWave()
{
    int lv;
    char buff[128];

    if (lv >= 40)
    {
        FrameTimer(68, VictoryEvent);
        return;
    }
    int k = sizeof(m_player), userCnt=0;
    int lineId;

    // for (k = 5 ; k >= 0 ; k --)
    while (--k>=0)
    {
        if (CurrentHealth(m_player[k]))
        {
            if (PlayerClassCheckFlag(k, PLAYER_LINE_FLAG))
            {
                lineId = m_pLineIndex[k];
                ++userCnt;
                TeleportLocation(lineId + 103, LocationX(lineId + 97), LocationY(lineId + 97));
            }
        }
    }
    if (!userCnt)
    {
        UniPrintToAll("생존한 플레이어가 없어서 더 이상 진행할 수 없습니다");
        return;
    }
    GTime = 40;
    SecondTimerWithArg(GTime, lv, StartCurrentWave);
    lv ++;
    NoxSprintfString(buff, "지금부터 40초 후 %d 웨이브가 시작됩니다", &lv, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

int SpawnMissionMonster(int pIndex, int lv, int score)
{
    int lineId = m_pLineIndex[pIndex];
    int unit = CreateObject(LineMonster(lv + 40), lineId + 7);

    Raise(CreateObject("InvisibleLightBlueHigh", lineId + 103), ToFloat(score));
    Enchant(unit + 1, "ENCHANT_VILLAIN", 0.0);
    LookWithAngle(unit + 1, pIndex);
    SetOwner(GetMaster(), unit);
    SetCallback(unit, 5, MissionUnitDeathEvent);
    SetCallback(unit, 3, LookAtMe);
    SetCallback(unit, 11, EndLineMonsterOverRun);
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    Enchant(unit, "ENCHANT_INFRAVISION", 0.0);
    TeleportLocationVector(lineId + 103, 1.0,0.0);
    PushTimerQueue(1, unit, DelayUnitMove);
    LineMobSetting(unit, lv + 40);
    return unit;
}

void LineMobSetting(int unit, int lv)
{
    int hp = LineHpTable(lv), ptr = UnitToPtr(unit);

    CheckMonsterThing(unit);
    SetUnitMaxHealth(unit, 200);
    SetUnitHealth(unit, hp & 0xffff);
    SetUnitFlags(unit, GetUnitFlags(unit) ^ 0x40);
    SetUnitStatus(unit, GetUnitStatus(unit) ^ 0x8000);
    if ((hp >> 16) & 1)
        Enchant(unit, "ENCHANT_SLOWED", 0.0);
}

int SpawnLineMonster(int pIndex, int lv)
{
    int lineId=m_pLineIndex[pIndex];
    int unit = CreateObject(LineMonster(lv), lineId + 7);

    Raise(CreateObject("InvisibleLightBlueLow", lineId + 103), ToFloat(5));
    LookWithAngle(unit + 1, pIndex);
    SetOwner(GetMaster(), unit);
    SetUnitScanRange(unit, 600.0);
    SetCallback(unit, 5, UnitDeathEvent);
    SetCallback(unit, 3, LookAtMe);
    SetCallback(unit, 11, EndLineMonsterOverRun);
    UnitZeroFleeRange(unit);
    AggressionLevel(unit, 0.0);
    RetreatLevel(unit, 0.0);
    ResumeLevel(unit, 0.0);
    SetUnitMass(unit, 999999.0);
    Enchant(unit, "ENCHANT_ANCHORED", 0.0);
    TeleportLocationVector(lineId + 103,  1.0, 0.0);
    LineMobSetting(unit, lv);
    PushTimerQueue(1, unit, DelayUnitMove);
    return unit;
}

// void RemoveLineMonsters(int plr)
// {
//     int k, idx = (plr * 40);

//     for (k = 39 ; k >= 0 ; k --)
//     {
//         if (CurrentHealth(LineMob[idx + k]))
//         {
//             Delete(LineMob[idx + k]);
//             Delete(LineMob[idx + k] + 1);
//             CheckLineKills(0);
//         }
//     }
// }

void EndLineMonsterOverRun()
{
    int pIndex = GetDirection(GetTrigger() + 1), k = 0;
    int user=m_player[pIndex];

    if (CurrentHealth(user) && m_pLife[pIndex])
    {
        char buff[192], *p;
        int life;

        while (1)
        {
            m_pLife[pIndex] --;
            if (m_pLife[pIndex])
            {
                life=m_pLife[pIndex];
                NoxSprintfString(buff, "라인몹을 하나 놓쳤습니다, 라이프가 하나 감소됩니다 (남은 라이프: %d)", &life, 1);
                UniPrint(user, ReadStringAddressEx(buff));
                if (m_pLife[pIndex] == 5)
                    HelpMode(pIndex);
            }
            else
            {
                m_player[pIndex]=0; // Damage(player[plr], 0, 255, 14); 이거대신 이 코드로 대체//
                UniPrint(user, "게.임.오.버.!!                                         ");
                p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));
                NoxSprintfString(buff, "%s 이 게임오버 되었습니다", &p, 1);
                UniPrintToAll(ReadStringAddressEx(buff));
            }
            if (HasEnchant(SELF, "ENCHANT_INFRAVISION"))
            {
                if (k < 5) 
                {
                    k++;
                    continue;
                }
            }
            break;
        }
    }
    CheckLineKills(GetTrigger() + 1);
    Delete(SELF);
    Delete(GetTrigger() + 1);
}

void HelpMode(int pIndex)
{
    int arr[6];

    if (!CurrentHealth(m_player[pIndex]))
        return;

    int lineId=m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    if (!PlayerClassCheckFlag(pIndex, PLAYER_HELPMODE_FLAG))
    {
        PlayerClassSetFlag(pIndex, PLAYER_HELPMODE_FLAG);
        PlrRb[lineId]+=400;
        UniPrint(m_player[pIndex], "헬프모드 발동!_ 궁지에 몰리다 (루비 +400)");
    }
}

void BossKillBouns(int pIndex)
{
    MoveObject(m_pCamera[pIndex], LocationX(123), LocationY(123));
    Effect("YELLOW_SPARKS", LocationX(123), LocationY(123), 0.0, 0.0);
    PlaySoundAround(m_pCamera[pIndex], SOUND_AwardLife);
    GiveItemToPlayer(m_player[pIndex], "Emerald", 1);
    UniPrint(m_player[pIndex], "보스 킬 보너스! 원하는 레어타워를 고르세요");
}

void KillsEventFunction(int pIndex)
{
    int lineId=m_pLineIndex[pIndex];
    char buff[192];
    int bonusGold=KillBonusGold(m_KillBonus[lineId]);
    int args[]={KillBonusTable(m_KillBonus[lineId]), bonusGold};

    NoxSprintfString(buff, "%d 킬 보너스 이벤트! 보상: %d 골드 획득", args, sizeof(args));
    UniPrint(m_player[pIndex], ReadStringAddressEx(buff));
    ChangeGold(m_player[pIndex], bonusGold);
    m_KillBonus[lineId] ++;
}

void UnitDeathEvent()
{
    int pIndex = GetDirection(GetTrigger() + 1);
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    int user =m_player[pIndex];

    if (CurrentHealth(user))
    {
        if ((++PKills[lineId]) >= KillBonusTable(m_KillBonus[lineId]))
            KillsEventFunction(pIndex);
        if (!IsObjectOn(GetTrigger() + 1))
            BossKillBouns(pIndex);
        ChangeGold(user, ToInt(GetObjectZ(GetTrigger() + 1)));
    }
    CheckLineKills(GetTrigger() + 1);
    DeleteObjectTimer(SELF, 1);
    Delete(GetTrigger() + 1);
}

void MissionUnitDeathEvent()
{
    int pIndex = GetDirection(GetTrigger() + 1);
    int lineIdx=m_pLineIndex[pIndex];

    if (CurrentHealth(m_player[pIndex]))
    {
        PKills[lineIdx] ++;
        ChangeGold(m_player[pIndex], ToInt(GetObjectZ(GetTrigger() + 1)));
    }
    CheckLineKills(GetTrigger() + 1);
    DeleteObjectTimer(SELF, 1);
    Delete(GetTrigger() + 1);
}

void DecreaseMissionUnitCount(int ptr)
{
    if (HasEnchant(ptr, "ENCHANT_VILLAIN"))
        MisCnt --;
    else
        m_LineKill ++;
}

int CheckSurvivePlayer()
{
    int k=sizeof(m_player);
    int count=0;

    while (--k>=0)
    {
        if (CurrentHealth(m_player[k]))
        {
            if (PlayerClassCheckFlag(k,PLAYER_LINE_FLAG))
                count+=1;
        }
    }
    return count;
}

static int computeMaxLineKills()
{
    int surviveUser = CheckSurvivePlayer();

    if (surviveUser<0)
        return 999999;

    return surviveUser*40;
}

void CheckLineKills(int ptr)
{
    DecreaseMissionUnitCount(ptr);
    char buff[128];
    int args[]={CheckSurvivePlayer(), m_LineKill};

    NoxSprintfString(buff, "survive-%d, kills-%d", args,sizeof(args));
    WriteLog(ReadStringAddressEx(buff));
    if (m_LineKill >= computeMaxLineKills() && !MisCnt)
    {
        m_LineKill = 0;
        if (CheckSurvivePlayer())
        {
            UniPrintToAll("웨이브 클리어를 성공 하셨습니다, 잠시 후 계속 이어집니다");
            PlayWav(SOUND_SoulGateTouch);
            SecondTimer(2, InitCurrentWave);
        }
        else
            UniPrintToAll("패배__    생존한 플레이어가 아무도 없습니다, 재도전 하시려면 맵을 다시 구동하세요");
    }
}

void DelayUnitMove(int ptr)
{
    int pIndex = GetDirection(ptr + 1);

    Move(ptr, m_pLineIndex[pIndex] + 13);
}

void SetFlagColor(int flag, int color)
{
    int ptr = UnitToPtr(flag);

    if (!ptr)
        return;

    int k, id = GetMemory(ptr + 0x2c);

    SetMemory(GetMemory(ptr + 0x2b4), GetItemColors(color));
    SetMemory(GetMemory(ptr + 0x2b4) + 4, GetItemColors(color));
    SetMemory(GetMemory(ptr + 0x2b4) + 8, GetItemColors(color));
    SetMemory(GetMemory(ptr + 0x2b4) + 12, GetItemColors(color));
    for (k = 31 ; k >= 0 ; k --)
        SetMemory(ptr + 0x230 + (k * 4), 0x200);
    SetUnitFlags(id, GetUnitFlags(id) ^  UNIT_FLAG_BELOW );
}

int GetItemColors(int num)
{
	int arr[32], k, ptr;

	if (!arr[0])
	{
		ptr = GetMemory(0x00654634);
		for (k = 0 ; k < 32 ; k ++)
		{
			arr[k] = ptr;
			ptr = GetMemory(ptr + 0x88);
		}
		return 0;
	}
	return arr[num];
}

void PlayWav(short soundId)
{
    int k = sizeof(m_player);

    while (--k>=0)
    {
        if (CurrentHealth(m_player[k]))
            PlaySoundAround(m_player[k], soundId);
    }
}

void DelayFireBoomHunt(int ptr)
{
    int enemy = ToInt(GetObjectZ(ptr)), owner = GetOwner(ptr);

    if (CurrentHealth(owner) && CurrentHealth(enemy))
    {
        DeleteObjectTimer(CreateObjectAt("SmallFireBoom", GetObjectX(enemy), GetObjectY(enemy)), 10);
        TowerWeaponForce(enemy, owner - 1, 1, 0);
    }
    Delete(ptr);
}

void FireBoomWeapon()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.8);
            float x_vect = UnitRatioX(OTHER, SELF, 16.0);
            float y_vect = UnitRatioY(OTHER, SELF, 16.0);
            float x= GetObjectX(SELF) + x_vect, y=GetObjectY(SELF) + y_vect;
            int mis = CreateObjectAt("WeakFireball", x,y);
            CreateObjectAt("InvisibleLightBlueLow", x,y);
            SetOwner(SELF, mis);
            SetOwner(SELF, mis + 1);
            Raise(mis + 1, ToFloat(GetCaller()));
            PushObjectTo(mis, (GetObjectX(OTHER) - GetObjectX(SELF)) / 3.0, (GetObjectY(OTHER) - GetObjectY(SELF)) / 3.0);
            DeleteObjectTimer(mis, 5);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            PushTimerQueue(4, mis + 1, DelayFireBoomHunt);
        }
        LookAtObject(SELF, OTHER);
        Enchant(SELF, "ENCHANT_BLINDED", 0.2);
        AggressionLevel(SELF, 1.0);
    }
}

void WispWeapon()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
            float x= GetObjectX(SELF),y= GetObjectY(SELF);
            PlaySoundAround(SELF, SOUND_WillOWispEngage);
            Effect("RICOCHET", x,y, 0.0, 0.0);
            Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
            Effect("RICOCHET", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            TowerWeaponForce(OTHER, GetTrigger() - 1, 1, 9);
        }
        LookAtObject(SELF, OTHER);
        Enchant(SELF, "ENCHANT_BLINDED", 0.2);
        AggressionLevel(SELF, 1.0);
    }
}

void SmallSpiderWeapon()
{
    int plr = ToInt(GetObjectZ(GetTrigger() + 3));

    if (CurrentHealth(OTHER))
    {
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        Effect("DAMAGE_POOF", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        LookAtObject(SELF, OTHER);
        Enchant(SELF, "ENCHANT_BLINDED", 0.2);
        AggressionLevel(SELF, 1.0);
        TowerWeaponForce(OTHER, GetTrigger() - 1, 1, 10);
    }
}

void DelayHitCherubBullet(int ptr)
{
    int target = ToInt(GetObjectZ(ptr)), owner = GetOwner(ptr), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 30)
    {
        if (DistanceUnitToUnit(ptr, target) > 30.0)
        {
            MoveObjectVector(ptr, UnitRatioX(target, ptr, 16.0), UnitRatioY(target, ptr, 16.0));
            int mis = CreateObjectAt("ArcherBolt", GetObjectX(ptr), GetObjectY(ptr));
            SetOwner(owner, mis);
            LookAtObject(mis, target);
            DeleteObjectTimer(mis, 6);
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            TowerWeaponForce(target, owner - 1, 1, 14);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, DelayHitCherubBullet);
        return;
    }
    Delete(ptr);
}

void CherubStrike()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.8);
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0));
            PlaySoundAround(ptr, SOUND_CrossBowShoot);
            SetOwner(SELF, ptr);
            Raise(ptr, ToFloat(GetCaller()));
            PushTimerQueue(1, ptr, DelayHitCherubBullet);
        }
        LookAtObject(SELF, OTHER);
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void ArcherStrike()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.8);
            int unit = CreateObjectAt("Barrel", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 15.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 15.0));
            UnitNoCollide(unit);
            CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(unit), GetObjectY(unit));
            Raise(unit + 1, ToFloat(GetCaller()));
            SetOwner(SELF, unit + 1);
            PushTimerQueue(1, unit, BarrelAttackLoop);
        }
        LookAtObject(SELF, OTHER);
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void GoonStrkeFunction()
{
    // int plr = ToInt(GetObjectZ(GetTrigger() + 3));

    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            DeleteObjectTimer(CreateObjectAt("GreenPuff", GetObjectX(OTHER), GetObjectY(OTHER)), 9);
            TowerWeaponForce(OTHER, GetTrigger() - 1, 1, 5);
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.8);
        }
        if (!HasEnchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC"))
        {
            Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
            PushTimerQueue(22, GetTrigger(), ResetUnitSight);
        }
    }
}

void ImpStrikeFunction()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.7);
            SetOwner(SELF, CreateObjectAt("ImpShot", GetObjectX(OTHER), GetObjectY(OTHER)));
            Effect("COUNTERSPELL_EXPLOSION", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0), 0.0, 0.0);
            TowerWeaponForce(OTHER, GetTrigger() - 1, 2, 14);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.2);
        AggressionLevel(SELF, 1.0);
        LookAtObject(SELF, OTHER);
    }
}

void WolfStormShot(int ptr)
{
    int owner = GetOwner(ptr), count = GetDirection(ptr), target = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 40)
    {
        if (DistanceUnitToUnit(ptr+1, target) > 60.0)
        {
            MoveObjectVector(ptr + 1, - UnitRatioX(ptr + 1, target, 18.0), - UnitRatioY(ptr + 1, target, 18.0));
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            Effect("CYAN_SPARKS", GetObjectX(target), GetObjectY(target), 0.0, 0.0);
            TowerWeaponForce(target, owner - 1, 1, 17);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, WolfStormShot);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void WolfStrikeFunction()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
            float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), y=GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0);
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
            Raise(ptr, ToFloat(GetCaller()));
            CreateObjectAt("BlueOrb", x,y);
            SetOwner(SELF, ptr);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            PushTimerQueue(1, ptr, WolfStormShot);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.3);
        AggressionLevel(SELF, 1.0);
    }
}

void ShortDemonStrikeFunction()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.4);
            PlaySoundAround(OTHER, SOUND_BurnCast);
            Effect("THIN_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            TowerWeaponForce(OTHER, GetTrigger() - 1, 2, 12);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void LichShockFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 40)
    {
        if (DistanceUnitToUnit(ptr, target) > 40.0)
        {
            LookWithAngle(ptr, count + 1);
            MoveObjectVector(ptr, - UnitRatioX(ptr, target, 18.0), - UnitRatioY(ptr, target, 18.0));
        }
        else
        {
            PlaySoundAround(target, SOUND_LightningBolt);
            Effect("RICOCHET", GetObjectX(target), GetObjectY(target), 0.0, 0.0);
            TowerWeaponForce(target, owner - 1, 2, 9);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, LichShockFx);
        return;
    }
    Delete(ptr);
}

void LichStrikeFunction()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.7);
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0));
            Enchant(ptr, "ENCHANT_SHOCK", 0.0);
            SetOwner(SELF, ptr);
            Raise(ptr, ToFloat(GetCaller()));
            PushTimerQueue(1, ptr, LichShockFx);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void PoisonSpiderFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr + 1)), count = GetDirection(ptr + 1);

    if (CurrentHealth(target) && count < 30)
    {
        if (DistanceUnitToUnit(ptr, target) > 60.0)
        {
            LookWithAngle(ptr + 1, count + 1);
            MoveObjectVector(ptr, - UnitRatioX(ptr, target, 18.0), - UnitRatioY(ptr, target, 18.0));
        }
        else
        {
            DeleteObjectTimer(CreateObjectAt("GreenSmoke", GetObjectX(target), GetObjectY(target)), 9);
            TowerWeaponForce(target, owner - 1, 3, 5);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, PoisonSpiderFx);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void SpiderStrikeFunction()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.6);
            float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0),y= GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0);
            int ptr = CreateObjectAt("GreenOrb", x,y);
            CreateObjectAt("InvisibleLightBlueHigh", x,y);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            Raise(ptr + 1, ToFloat(GetCaller()));
            SetOwner(SELF, ptr);
            PushTimerQueue(1, ptr, PoisonSpiderFx);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void SkeletonWindSwordsFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 70)
    {
        if (DistanceUnitToUnit(ptr, target) > 70.0)
        {
            LookWithAngle(ptr, count + 1);
            MoveObjectVector(ptr, - UnitRatioX(ptr, target, 16.0), - UnitRatioY(ptr, target, 16.0));
            PlaySoundAround(ptr, SOUND_OgreBruteMeleeHit);
            MoveObject(ptr + 1, GetObjectX(ptr), GetObjectY(ptr));
        }
        else
        {
            DeleteObjectTimer(CreateObjectAt("ReleasedSoul", GetObjectX(target), GetObjectY(target)), 9);
            TowerWeaponForce(target, owner - 1, 2, 2);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, SkeletonWindSwordsFx);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void SkeletonPunches()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.6);
            float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0),y= GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0);
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
            CreateObjectAt("Fear", x,y);
            Raise(ptr, ToFloat(GetCaller()));
            SetOwner(SELF, ptr);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            PushTimerQueue(1, ptr, SkeletonWindSwordsFx);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void SwordsmanAttack()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.3);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            TowerWeaponForce(OTHER, GetTrigger() - 1, 3, 12);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void OgreAxeMeleeAttackFunction()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 1.2);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            float x=GetObjectX(OTHER),y=GetObjectY(OTHER);
            CreateObjectAt("PiledBarrels3Breaking", x,y);
            SplashDamageAt(GetTrigger(), GetTowerPower(GetTrigger() - 1, 4), x,y, 95.0);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.3);
        AggressionLevel(SELF, 1.0);
    }
}

void FlyingUrchinStone(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 40)
    {
        if (DistanceUnitToUnit(ptr+1, target) > 70.0)
        {
            LookWithAngle(ptr, count + 1);
            MoveObjectVector(ptr + 1, UnitRatioX(target, ptr + 1, 18.0), UnitRatioY(target, ptr + 1, 18.0));
        }
        else
        {
            Effect("DAMAGE_POOF", GetObjectX(target), GetObjectY(target), 0.0, 0.0);
            TowerWeaponForce(target, owner - 1, 2, 2);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, FlyingUrchinStone);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void UrchinMissileStrikeFunction()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.61);
            float x= GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), y=GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0);
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
            CreateObjectAt("Rock5", x,y);
            SetOwner(SELF, ptr);
            Raise(ptr, ToFloat(GetCaller()));
            PushTimerQueue(1, ptr, FlyingUrchinStone);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void WhiteWolfFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 40)
    {
        if (DistanceUnitToUnit(ptr, target) > 70.0)
        {
            LookWithAngle(ptr, count + 1);
            MoveObjectVector(ptr, UnitRatioX(target, ptr, 16.0), UnitRatioY(target, ptr, 16.0));
        }
        else
        {
            TowerWeaponForce(target, owner - 1, 3, 2);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, WhiteWolfFx);
        return;
    }
    Delete(ptr);
}

void WhiteWolfStrikeFunction()
{
    if (CurrentHealth(OTHER))
    {
        LookAtObject(SELF, OTHER);
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.6);
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0));
            Enchant(ptr, "ENCHANT_PROTECT_FROM_ELECTRICITY", 0.0);
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            SetOwner(SELF, ptr);
            Raise(ptr, ToFloat(GetCaller()));
            PushTimerQueue(1, ptr, WhiteWolfFx);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void FirePieceFlying(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 30)
    {
        if (DistanceUnitToUnit(ptr+1, target) > 40.0)
        {
            LookWithAngle(ptr, count + 1);
            MoveObjectVector(ptr+1, - UnitRatioX(ptr, target, 9.0), - UnitRatioY(ptr, target, 9.0));
        }
        else
        {
            float x=GetObjectX(target), y=GetObjectY(target);
            Effect("SPARK_EXPLOSION", x,y, 0.0, 0.0);
            SplashDamageAt(owner, GetTowerPower(owner - 1, 6), x,y,85.0);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, FirePieceFlying);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void EmberDemonStrike()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
            float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0),y= GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0);
            int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
            CreateObjectAt("ProtectionFire", x,y);
            SetOwner(SELF, ptr);
            Raise(ptr, ToFloat(GetCaller()));
            HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
            PushTimerQueue(1, ptr, FirePieceFlying);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void LichLordSplashEvent()
{
    int owner = GetOwner(SELF);

    if (CurrentHealth(owner))
    {
        Effect("DRAIN_MANA", GetObjectX(owner), GetObjectY(owner), GetObjectX(OTHER), GetObjectY(OTHER));
        TowerWeaponForce(OTHER, owner - 1, 3, 2);
    }
}

void LichLordStrike()
{
    if (CurrentHealth(OTHER))
    {
        if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
        {
            Enchant(SELF, "ENCHANT_ETHEREAL", 1.2);
            float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), y=GetObjectY(SELF) - UnitRatioX(SELF, OTHER, 13.0);
            int unit = CreateObjectAt("Wizard", x,y);
            PlaySoundAround(unit, SOUND_DrainManaCast);
            LookAtObject(unit, OTHER);
            SetOwner(SELF, unit);
            SetCallback(unit, 3, LichLordSplashEvent);
            DeleteObjectTimer(unit, 1);
        }
        Enchant(SELF, "ENCHANT_BLINDED", 0.1);
        AggressionLevel(SELF, 1.0);
    }
}

void ScorpionStrikeEvent()
{
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.0);
        float x=GetObjectX(OTHER), y=GetObjectY(OTHER);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        DeleteObjectTimer(CreateObjectAt("GreenSmoke", x,y), 9);
        GreenSparkFx(x,y);
        SplashDamageAt(GetTrigger(), GetTowerPower(GetTrigger() - 1, 6), x,y,60.0);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void ZombieStrikeEvent()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.75);
        float x= GetObjectX(OTHER) - UnitRatioX(OTHER, SELF, 20.0), y=GetObjectY(OTHER) - UnitRatioY(OTHER, SELF, 20.0);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        Enchant(CreateObjectAt("Zombie", x,y), "ENCHANT_FREEZE", 0.0);
        UnitNoCollide(ptr + 1);
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        LookAtObject(ptr + 1, OTHER);
        HitLocation(ptr + 1, GetObjectX(ptr + 1), GetObjectY(ptr + 1));
        PushTimerQueue(22, ptr, ZombieSpreadRing);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void HitTrash(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && count < 30)
    {
        if (DistanceUnitToUnit(ptr+3, ptr) > 30.0)
        {
            LookWithAngle(ptr, count + 1);
            MoveObjectVector(ptr + 3, - GetObjectZ(ptr + 1), - GetObjectZ(ptr + 2));
        }
        else
        {
            DeleteObjectTimer(CreateObjectAt("CrateBreaking1", GetObjectX(ptr), GetObjectY(ptr)), 13);
            LookWithAngle(ptr, 200);
            TowerWeaponForce(target, owner - 1, 4, 2);
        }
        PushTimerQueue(1, ptr, HitTrash);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
    Delete(ptr + 2);
    Delete(ptr + 3);
}

void CaptainWeapon()
{
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        float x_vect = UnitRatioX(SELF, OTHER, 18.0);
        float y_vect = UnitRatioY(SELF, OTHER, 18.0);
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.5);
        float x= GetObjectX(SELF) - x_vect, y= GetObjectY(SELF) - y_vect;
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        MoveObject(ptr, GetObjectX(OTHER), GetObjectY(OTHER));
        Raise(CreateObjectAt("InvisibleLightBlueHigh", x,y), x_vect);
        Raise(CreateObjectAt("InvisibleLightBlueHigh", x,y), y_vect);
        UnitNoCollide(CreateObjectAt("AirshipBasket", x,y));
        Raise(ptr, ToFloat(GetCaller()));
        SetOwner(SELF, ptr);
        PushTimerQueue(1, ptr, HitTrash);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);

}

void OgreWindPar(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 30)
    {
        if (DistanceUnitToUnit(ptr, target) > 36.0)
        {
            MoveObjectVector(ptr, UnitRatioX(target, ptr, 18.0), UnitRatioY(target, ptr, 18.0));
            MoveObject(ptr + 1, GetObjectX(ptr), GetObjectY(ptr));
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            TowerWeaponForce(target, owner - 1, 5, 4);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, OgreWindPar);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void OgreAttackFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.8);
        float x= GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), y= GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        CreateObjectAt("WhirlWind", x,y);
        Raise(ptr, ToFloat(GetCaller()));
        SetOwner(SELF, ptr);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        PushTimerQueue(1, ptr, OgreWindPar);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void ShamanAttackFunction()
{
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        CreatureGuard(SELF, GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER), 450.0);
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.5);
        float x= GetObjectX(OTHER) + UnitAngleCos(OTHER, 46.0), y= GetObjectY(OTHER) + UnitAngleSin(OTHER, 46.0);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        Raise(ptr, ToFloat(GetCaller()));
        SetOwner(SELF, ptr);
        CastSpellObjectLocation("SPELL_METEOR", SELF, x,y);
        PushTimerQueue(17, ptr, FallenMeteor);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void BlackWolfStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Effect("THIN_EXPLOSION", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.7);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        TowerWeaponForce(OTHER, GetTrigger() - 1, 5, 0);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void RedWizWeapon()
{
    Effect("SENTRY_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
    TowerWeaponForce(OTHER, GetTrigger() - 1, 4, 16);
    if (!HasEnchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC"))
    {
        Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
        PushTimerQueue(36, GetTrigger(), ResetUnitSight);
    }
}

void HovathStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        CreatureGuard(SELF, GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER), 500.0);
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.67);
        float x= GetObjectX(OTHER),y= GetObjectY(OTHER);
        PlaySoundAround(OTHER, SOUND_LightningCast);
        DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", x,y), 12);
        DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", x,y), 12);
        DeleteObjectTimer(CreateObjectAt("PlayerWaypoint", x,y), 12);
        TowerWeaponForce(OTHER, GetTrigger() - 1, 8, 9);
        Effect("LIGHTNING", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(SELF), GetObjectY(SELF) - 200.0);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void MimicSpikeFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 40)
    {
        if (DistanceUnitToUnit(ptr, target) > 60.0)
        {
            MoveObjectVector(ptr, - UnitRatioX(ptr, target, 18.0), - UnitRatioY(ptr, target, 18.0));
            Effect("DAMAGE_POOF", GetObjectX(ptr), GetObjectY(ptr), 0.0, 0.0);
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            unit = CreateObjectAt("SpikeBrazier", GetObjectX(target), GetObjectY(target));
            DeleteObjectTimer(unit, 12);
            UnitNoCollide(unit);
            PlaySoundAround(unit, SOUND_HitEarthBreakable);
            TowerWeaponForce(target, owner - 1, 10, 10);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, MimicSpikeFx);
        return;
    }
    Delete(ptr);
}

void MimicStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0));
        Raise(ptr, ToFloat(GetCaller()));
        SetOwner(SELF, ptr);
        PushTimerQueue(1, ptr, MimicSpikeFx);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void NecromancerSkill()
{
    CreatureGuard(SELF, GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER), 500.0);
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Effect("DEATH_RAY", GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER));
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.9);
        PlaySoundAround(SELF, SOUND_DeathRayCast);
        PlaySoundAround(OTHER, SOUND_SentryRayHit);
        TowerWeaponForce(OTHER, GetTrigger() - 1, 25, 16);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.2);
    AggressionLevel(SELF, 1.0);
}

void HorrendousStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.3);
        TowerWeaponForce(OTHER, GetTrigger() - 1, 8, 11);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void OgreHitShurikens(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 30)
    {
        if (DistanceUnitToUnit(target, ptr) > 60.0)
        {
            MoveObjectVector(ptr, UnitRatioX(target, ptr, 17.0), UnitRatioY(target, ptr, 17.0));
            unit = CreateObjectAt("HarpoonBolt", GetObjectX(ptr), GetObjectY(ptr));
            Frozen(unit, 1);
            LookAtObject(unit, target);
            LookWithAngle(ptr, count + 1);
            DeleteObjectTimer(unit, 6);
        }
        else
        {
            GreenExplosion(GetObjectX(ptr), GetObjectY(ptr));            
            SplashDamageAt(owner, GetTowerPower(owner - 1, 22), GetObjectX(ptr), GetObjectY(ptr),150.0);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, OgreHitShurikens);
    }
    else
    {
        Delete(ptr);
    }
}

void OgreWarlordStrikeFunction()
{
    CreatureGuard(SELF, GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER), 530.0);
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0));
        Enchant(ptr, "ENCHANT_SLOWED", 0.0);
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.8);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        PushTimerQueue(1, ptr, OgreHitShurikens);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void FallenStone(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        float x=GetObjectX(ptr), y=GetObjectY(ptr);
        DeleteObjectTimer(CreateObjectAt("Smoke", x,y), 9);
        PlaySoundAround(ptr, SOUND_HitStoneBreakable);
        Effect("JIGGLE", x,y, 20.0, 0.0);
        SplashDamageAt(owner, GetTowerPower(owner - 1, 9),x,y, 130.0);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void TrollStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.3);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(OTHER), GetObjectY(OTHER));
        Raise(CreateObjectAt("Rock3", GetObjectX(ptr), GetObjectY(ptr)), 200.0);
        PushTimerQueue(18, ptr, FallenStone);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void BlackBearStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.5);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0),y= GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        CreateObjectAt("Mushroom", x,y);
        MoveObject(ptr, GetObjectX(OTHER), GetObjectY(OTHER));
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        PushTimerQueue(1, ptr, BearThrowMushroom);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void DemonSkill()
{
    int ptr;

    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
        CreatureGuard(SELF, GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER), 500.0);
        ptr = CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0));
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        PushTimerQueue(1, ptr, FireWayFx);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void SoulTouchSkill()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.83);
        CreatureGuard(SELF, GetObjectX(SELF), GetObjectY(SELF), GetObjectX(OTHER), GetObjectY(OTHER), 500.0);
        Damage(CreateObjectAt("WillOWisp", GetObjectX(OTHER), GetObjectY(OTHER)), 0, 999, 14);
        TowerWeaponForce(OTHER, GetTrigger() - 1, 26, 9);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void GreenOrbFlying(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 40)
    {
        if (DistanceUnitToUnit(ptr, target) < 38.0)
        {
            GreenSparkFx(GetObjectX(target), GetObjectY(target));
            LookWithAngle(ptr, 200);
            TowerWeaponForce(target, owner - 1, 21, 5);
        }
        else
        {
            MoveObjectVector(ptr, - UnitRatioX(ptr, target, 15.0), - UnitRatioX(ptr, target, 15.0));
            LookWithAngle(ptr, count + 1);
        }
        PushTimerQueue(1, ptr, GreenOrbFlying);
        return;
    }
    Delete(ptr);
}

void PlantSkill()
{
    int ptr;
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.6);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), y= GetObjectY(SELF) - UnitRatioX(SELF, OTHER, 13.0);
        ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        Enchant(ptr, "ENCHANT_PROTECT_FROM_POISON", 0.0);
        Enchant(ptr, "ENCHANT_INFRAVISION", 0.0);
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        PushTimerQueue(1, ptr, GreenOrbFlying);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void SummonMecaGolemFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 30)
    {
        if (DistanceUnitToUnit(ptr+1, target) > 36.0)
        {
            MoveObjectVector(ptr + 1,  - UnitRatioX(ptr + 1, target, 18.0), - UnitRatioY(ptr + 1, target, 18.0));
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            CreateObjectAt("BarrelBreakingLOTD", GetObjectX(ptr + 1), GetObjectY(ptr + 1));
            LookWithAngle(ptr, 200);
            TowerWeaponForce(target, owner - 1, 27, 14);
        }
        PushTimerQueue(1, ptr, SummonMecaGolemFx);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void MecaGolemHit()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.2);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0),y= GetObjectY(OTHER) - UnitRatioY(SELF, OTHER, 13.0);
        int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
        Frozen(CreateObjectAt("FlyingGolem", x,y), TRUE);
        LookAtObject(ptr + 1, OTHER);
        SetOwner(SELF, ptr);
        Raise(ptr, ToFloat(GetCaller()));
        PushTimerQueue(1, ptr, SummonMecaGolemFx);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void HecubahSkill()
{
    int ptr;

    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
        MoveWaypoint(120, GetObjectX(OTHER), GetObjectY(OTHER));
        ptr = CreateObject("InvisibleLightBlueHigh", 120);
        Raise(CreateObject("InvisibleLightBlueLow", 120), 100.0);
        UnitNoCollide(CreateObject("GameBall", 120));
        UnitNoCollide(CreateObject("GameBall", 120));
        Raise(ptr, ToFloat(GetCaller()));
        SetOwner(SELF, ptr);
        DeathRingFx(ptr);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void DryadSkill()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        CastDryadStatueMisAttackFunction();
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.9);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void StoneGolemStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        Effect("SMOKE_BLAST", GetObjectX(SELF), GetObjectY(SELF), 0.0, 0.0);
        Effect("DAMAGE_POOF", GetObjectX(OTHER), GetObjectY(OTHER), 0.0, 0.0);
        TowerWeaponForce(OTHER, GetTrigger() - 1, 35, 2);
        Enchant(SELF, "ENCHANT_ETHEREAL", 0.5);
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.3);
    AggressionLevel(SELF, 1.0);
}

void IceBearStrikeFunction()
{
    LookAtObject(SELF, OTHER);
    if (!HasEnchant(SELF, "ENCHANT_ETHEREAL"))
    {
        Enchant(SELF, "ENCHANT_ETHEREAL", 1.1);
        HitLocation(SELF, GetObjectX(SELF), GetObjectY(SELF));
        IceBearHitImp();
    }
    Enchant(SELF, "ENCHANT_BLINDED", 0.1);
    AggressionLevel(SELF, 1.0);
}

void ResetLineSight(int unit)
{
    EnchantOff(unit, "ENCHANT_PROTECT_FROM_MAGIC");
    Enchant(unit, "ENCHANT_BLINDED", 0.08);
}

void ResetUnitSight(int unit)
{
    EnchantOff(unit, "ENCHANT_PROTECT_FROM_MAGIC");
    Enchant(unit, "ENCHANT_BLINDED", 0.08);
    AggressionLevel(unit, 1.0);
}

void BarrelAttackLoop(int ptr)
{
    int target = ToInt(GetObjectZ(ptr + 1)), owner = GetOwner(ptr + 1), count = GetDirection(ptr + 1);

    if (CurrentHealth(owner) && count < 50 && IsObjectOn(ptr) && CurrentHealth(target))
    {
        if (DistanceUnitToUnit(ptr, target) < 60.0)
        {
            float x=GetObjectX(target), y=GetObjectY(target);
            DeleteObjectTimer(CreateObjectAt("Explosion", x,y), 9);
            PlaySoundAround(target, SOUND_PowderBarrelExplode);
            TowerWeaponForce(target, owner - 1, 2, 7);
            SplashDamageAt(owner, GetTowerPower(owner - 1, 1), x,y, 50.0);
            LookWithAngle(ptr + 1, 200);
        }
        else
        {
            MoveObjectVector(ptr, - UnitRatioX(ptr, target, 18.0), - UnitRatioY(ptr, target, 18.0));
            LookWithAngle(ptr + 1, count + 1);
        }
        PushTimerQueue(1, ptr, BarrelAttackLoop);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void UnitHearEvent()
{
    LookAtObject(SELF, OTHER);
}

void LookAtMe()
{
    if (GetUnitClass(GetCaller()-1) & UNIT_CLASS_FLAG)
        LookAtObject(OTHER, SELF);
    if (!UnitCheckEnchant(SELF, GetLShift(ENCHANT_PROTECT_FROM_MAGIC)) )
    {
        Enchant(SELF, "ENCHANT_PROTECT_FROM_MAGIC", 0.0);
        PushTimerQueue(22, GetTrigger(), ResetLineSight);
    }
}

void ZombieSpreadRing(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        MoveWaypoint(95, GetObjectX(target), GetObjectY(target));
        TowerWeaponForce(target, owner - 1, 7, 14);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void TowerWeaponForce(int target, int owner, int apply, int type)
{
    int plr = ToInt(GetObjectZ(owner + 3));
    Damage(target, owner, ToInt(GetObjectZ(owner + 2)) + (PUpgrade[(plr * 8) + GetDirection(owner + 2)] * apply), type);
}

static void printAllFormatMessage(int user, string fmt)
{
    char buff[256], *p=StringUtilGetScriptStringPtr(PlayerIngameNick(user));

    NoxSprintfString(buff, fmt, &p, 1);
    UniPrintToAll(ReadStringAddressEx(buff));
}

void FirstQuest(int pIndex, int user, int lineId)
{
    int k, res = 0;

    if (CurrentHealth(user))
    {
        for (k = 7 ; k + 1 ; k --)
        {
            if (Quest[(lineId * 40) + k])
                res ++;
        }
        if (res == 8)
        {
            printAllFormatMessage(user, "%s 님께서 '편식은 금물' 업적을 완료하셨습니다. [골드 +300]");
            PlayWav(SOUND_AwardSpell);
            ChangeGold(user, 300);
        }
    }
}

void SecondQuest(int pIndex, int user, int lineId)
{
    if (CurrentHealth(user))
    {
        if (Quest[(lineId * 40) + 12] && Quest[(lineId * 40) + 13])
        {
            printAllFormatMessage(user, "%s 님께서 '그 여자 그 남자' 업적을 완료하셨습니다. [골드 + 300]");
            PlayWav(SOUND_AwardSpell);
            ChangeGold(user, 300);
        }
    }
}

void ThirdQuest(int pIndex, int user, int lineId)
{
    int k, res = 0;

    if (CurrentHealth(user))
    {
        for (k = 7 ; k >= 0 ; k --)
        {
            res += Quest[(lineId * 40) + 16 + k];
        }
        if (res >= 7)
        {
            printAllFormatMessage(user, "%s 님께서 '언럭키세븐' 업적을 완료하셨습니다. [골드 +400]");
            PlayWav(SOUND_AwardSpell);
            ChangeGold(user, 400);
        }
    }
}

void PlayerQuest4(int pIndex, int user, int lineId)
{
    int k, res = 0;

    if (CurrentHealth(user))
    {
        for (k = 7 ; k >= 0 ; k --)
            res += Quest[(lineId * 40) + 24 + k];
        if (res >= 6)
        {
            printAllFormatMessage(user, "%s 님께서 '악운도 운이다' 업적을 완료하셨습니다. [골드 +400]");
            PlayWav(SOUND_AwardSpell);
            ChangeGold(user, 400);
        }
    }
}

void PlayerQuest5(int pIndex, int user, int lineId)
{
    if (CurrentHealth(user))
    {
        if (Quest[(lineId * 40) + 27] >= 3)
        {
            printAllFormatMessage(user, "%s 님께서 '네크로멘서의 저주' 업적을 완료하셨습니다. [골드 +400, 루비 +200]");
            PlayWav(SOUND_AwardSpell);
            PlrRb[lineId] += 200;
            ChangeGold(user, 400);
        }
    }
}

void PlayerQuest6(int pIndex, int user, int lineId)
{
    int k, res = 0;

    if (CurrentHealth(user))
    {
        for (k = 7 ; k >= 0 ; k --)
        {
            if (Quest[(lineId * 40) + 24 + k])
                res ++;
        }
        if (res >= 7)
        {
            printAllFormatMessage(user, "%s 님께서 '이상한 사람의 취미생활' 업적을 완료하셨습니다. (에메랄드 +3)");
            GiveItemToPlayer(user, "Emerald", 1);
            GiveItemToPlayer(user, "Emerald", 1);
            GiveItemToPlayer(user, "Emerald", 1);
        }
    }
}

void PlayerQuest7(int pIndex, int user, int lineId)
{
    int k;

    if (CurrentHealth(user))
    {
        if (Quest[(lineId * 40) + 36] && Quest[(lineId * 40) + 37] >= 2)
        {
            printAllFormatMessage(user, "%s 님께서 '탄핵이 가결되었습니다' 업적을 완료하셨습니다. (루비 +555)");
            PlrRb[lineId] += 555;
        }
    }
}

void PlayerQuest8(int pIndex, int user, int lineId)
{
    int k, res;

    if (CurrentHealth(user))
    {
        for (k = 7 ; k >= 0 ; k --)
        {
            if (Quest[lineId * 40 + k] && Quest[lineId * 40 + 8 + k] && Quest[lineId * 40 + 16 + k] && Quest[lineId * 40 + 24 + k])
                res ++;
        }
        if (res >= 2)
        {
            printAllFormatMessage(user, "%s 님께서 '녹스 포에버' 업적을 완료하셨습니다. (다이아몬드 +2)");
            GiveItemToPlayer(user, "Diamond", 2);
            GiveItemToPlayer(user, "Diamond", 2);
        }
        else
            res = 0;
    }
}

void PlayerQuest9(int pIndex, int user, int lineId)
{
    if (CurrentHealth(user))
    {
        if (Quest[lineId * 40 + 28] >= 3)
        {
            printAllFormatMessage(user, "%s 님께서 '호렌더스의 영광을 위하여' 업적을 완료하셨습니다. (골드, 루비 +300)");
            ChangeGold(user, 300);
            PlrRb[lineId] += 300;
        }
    }
}

void PlayerQuest10(int pIndex, int user, int lineId)
{
    if (CurrentHealth(user))
    {
        if (Quest[lineId * 40 + 9] >= 2 && Quest[lineId * 40 + 14] >= 2 && Quest[lineId * 40 + 17] >= 2 && Quest[lineId * 40 + 22] >= 2)
        {
            printAllFormatMessage(user, "%s 님께서 'District 9' 업적을 완료하셨습니다. (다이아몬드 +1)");
        }
    }
}

void AddQuestList(int pIndex, int data)
{
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;
        
    int location = lineId+129;
    int ptr = CreateObject("InvisibleLightBlueHigh", location);
    CreateObject("InvisibleLightBlueHigh", location);
    if (ListPtr[lineId])
    {
        SetOwner(ListPtr[lineId], ptr);        //ptr->prev = head
        SetOwner(ptr, ListPtr[lineId] + 1);    //head->next = ptr
        ListPtr[lineId] = ptr;
    }
    else
        ListPtr[lineId] = ptr;
    LookWithAngle(ptr, pIndex);
    Raise(ptr, ToFloat(data));
    TeleportLocationVector(location, 1.0, 0.0);
}

void RemoveQuestList(int ptr)
{
    int pIndex = GetDirection(ptr);
    int lineId=m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    int left = GetOwner(ptr), right = GetOwner(ptr + 1);

    if (ListPtr[lineId] == ptr)
        ListPtr[lineId] = left;
    else if (right)
    {
        SetOwner(left, right); //right->prev = left
        SetOwner(right, left + 1);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

static int invokeConfirmQuest(int fn, int pIndex, int user, int lineId)
{
    return Bind(fn, &fn+4);
}

void CheckQuestList(int pIndex)
{
    int lineId = m_pLineIndex[pIndex];

    if (lineId<0)
        return;

    int cur = ListPtr[lineId], prev;

    while (cur)
    {
        prev = GetOwner(cur);
        if (ToInt(GetObjectX(cur)))
        {
            if (invokeConfirmQuest(ToInt(GetObjectZ(cur)), pIndex, m_player[pIndex], lineId))
                RemoveQuestList(cur);
        }
        cur = prev; //cur->prev
    }
}

void RegistQuestList(int pIndex)
{
    AddQuestList(pIndex, FirstQuest);
    AddQuestList(pIndex, SecondQuest);
    AddQuestList(pIndex, ThirdQuest);
    AddQuestList(pIndex, PlayerQuest4);
    AddQuestList(pIndex, PlayerQuest5);
    AddQuestList(pIndex, PlayerQuest6);
    AddQuestList(pIndex, PlayerQuest7);
    AddQuestList(pIndex, PlayerQuest8);
    AddQuestList(pIndex, PlayerQuest9);
    AddQuestList(pIndex, PlayerQuest10);
}

void FallenMeteor(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        float x=GetObjectX(ptr), y=GetObjectY(ptr);
        DeleteObjectTimer(CreateObjectAt("MeteorExplode", x,y), 9);
        PlaySoundAround(ptr, SOUND_MeteorHit);
        SplashDamageAt(owner, GetTowerPower(owner - 1, 12),x,y, 150.0);
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void StrWaitRoom()
{
	string name = "DrainManaOrb";
    int i = -1, arr[]={
	    271049310, 75515972, 269027460, 18944321, 606226468, 72385664, 559119, 2115043617, 34612259, 1090815040,
	    412585968, 301990416, 4178432, 
    };
	while (++i < 13)
		drawStrWaitRoom(arr[i], name);
}

void drawStrWaitRoom(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(126);
		pos_y = GetWaypointY(126);
	}
	for (i = 1 ; i > 0 && count < 403 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 126);
		if (count % 36 == 35)
			MoveWaypoint(126, GetWaypointX(126) - 70.000000, GetWaypointY(126) + 2.000000);
		else
			MoveWaypoint(126, GetWaypointX(126) + 2.000000, GetWaypointY(126));
		count ++;
	}
	if (count >= 403)
	{
		count = 0;
		MoveWaypoint(126, pos_x, pos_y);
	}
}

void StrGameBegin()
{
	string name = "CharmOrb";
    int i = -1, arr[]={
	    270779038, 171990980, 135299361, 75780610, 1208500356, 34612258, 252428741, 19009665, 1157890130, 21236232,
	    141312, 285149728, 675413522, 4752392, 270614792, 2097794, 134283018, 256,
    };
	while(++i < 18)
		drawStrGameBegin(arr[i], name);
}

void drawStrGameBegin(int arg_0, string name)
{
	int count, i;
	float pos_x, pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(127);
		pos_y = GetWaypointY(127);
	}
	for (i = 1 ; i > 0 && count < 558 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 127);
		if (count % 49 == 48)
			MoveWaypoint(127, GetWaypointX(127) - 96.000000, GetWaypointY(127) + 2.000000);
		else
			MoveWaypoint(127, GetWaypointX(127) + 2.000000, GetWaypointY(127));
		count ++;
	}
	if (count >= 558)
	{
		count = 0;
		MoveWaypoint(127, pos_x, pos_y);
	}
}

void BearThrowMushroom(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 50)
    {
        if (DistanceUnitToUnit(ptr, ptr+1) > 38.0)
        {
            MoveObjectVector(ptr + 1, UnitRatioX(ptr, ptr + 1, 18.0), UnitRatioY(ptr, ptr + 1, 18.0));
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            LookWithAngle(ptr, 200);
            DeleteObjectTimer(CreateObjectAt("ProtectionEnchantments", GetObjectX(ptr), GetObjectY(ptr)), 9);
            TowerWeaponForce(target, owner - 1, 11, 4);
        }
        PushTimerQueue(1, ptr, BearThrowMushroom);
        return;
    }
    Delete(ptr);
    Delete(ptr + 1);
}

void FireWayFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr), unit;

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 40)
    {
        if (DistanceUnitToUnit(target, ptr) > 38.0)
        {
            MoveObjectVector(ptr,  - UnitRatioX(ptr, target, 18.0),  - UnitRatioY(ptr, target, 18.0));
            unit = CreateObjectAt("FireGrateFlame", GetObjectX(ptr), GetObjectY(ptr));
            SetOwner(owner, unit);
            DeleteObjectTimer(unit, 12);
            PlaySoundAround(unit, SOUND_FireballCast);
            LookWithAngle(ptr, count + 1);
        }
        else
        {
            DeleteObjectTimer(CreateObjectAt("Barrel", GetObjectX(ptr), GetObjectY(ptr)), 12);
            SetOwner(owner, CreateObjectAt("Fireball", GetObjectX(ptr), GetObjectY(ptr)));
            SplashDamageAt(owner, GetTowerPower(owner - 1, 15), GetObjectX(ptr), GetObjectY(ptr), 175.0);
            LookWithAngle(ptr, 200);
        }
        PushTimerQueue(1, ptr, FireWayFx);
        return;
    }
    Delete(ptr);
}

void RemoveMecaDummy(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    TowerWeaponForce(target, owner - 1, 10, 2);
    Delete(ptr);
    Delete(ToInt(GetObjectZ(ptr + 1)));
}

void DeathRingFx(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), dt = GetDirection(ptr + 1) * 8;
    float size = GetObjectZ(ptr + 1);

    if (CurrentHealth(target) && ToInt(size))
    {
        MoveObject(ptr + 2, GetObjectX(ptr) + MathSine(dt + 90, size), GetObjectY(ptr) + MathSine(dt, size));
        MoveObject(ptr + 3, GetObjectX(ptr) + MathSine(dt + 270, size), GetObjectY(ptr) + MathSine(dt + 180, size));
        size = size - 2.0;
        Raise(ptr + 1, size);
        LookWithAngle(ptr + 1, GetDirection(ptr + 1) + 1);
        if (!ToInt(size))
        {
            float x= GetObjectX(ptr), y=GetObjectY(ptr);
            Effect("TELEPORT", x,y, 0.0, 0.0);
            SplashDamageAt(owner, GetTowerPower(owner - 1, 13),x,y, 100.0);
        }
        PushTimerQueue(1, ptr, DeathRingFx);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
        Delete(ptr + 2);
        Delete(ptr + 3);
    }
}

string DryadStatue(int idx)
{
    string st[] = { "StatueVictory4E", "StatueVictory4SE", "StatueVictory4S", "StatueVictory4SW",
    "StatueVictory4W", "StatueVictory4NW", "StatueVictory4N", "StatueVictory4NE" };

    return st[idx];
}

void TrackingStatueMissile(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr)), count = GetDirection(ptr);

    if (CurrentHealth(owner) && CurrentHealth(target) && count < 30)
    {
        if (DistanceUnitToUnit(target, ptr+1) < 50.0)
        {
            LookWithAngle(ptr, 200);
            TowerWeaponForce(target, owner - 1, 17, 11);
        }
        else
        {
            MoveObjectVector(ptr + 1, UnitRatioX(target, ptr + 1, 18.0), UnitRatioY(target, ptr + 1, 18.0));
            LookWithAngle(ptr, count + 1);
        }
        PushTimerQueue(1, ptr, TrackingStatueMissile);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void CastDryadStatueMisAttackFunction()
{
    float x=GetObjectX(SELF) + UnitRatioX(OTHER, SELF, 13.0), y=GetObjectY(SELF) + UnitRatioY(OTHER, SELF, 13.0);
    int sub = CreateObjectAt("InvisibleLightBlueHigh", x,y);

    Enchant(CreateObjectAt(DryadStatue(GetDirection(SELF) / 32), x,y), "ENCHANT_FREEZE", 0.0);
    SetOwner(SELF, sub);
    Raise(sub, ToFloat(GetCaller()));
    PushTimerQueue(1, sub, TrackingStatueMissile);
}

void ThrowImpByIceBear(int ptr)
{
    int owner = GetOwner(ptr), target = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner) && CurrentHealth(target))
    {
        if (DistanceUnitToUnit(owner, target) < DistanceUnitToUnit(ptr+1, target))
        {
            TowerWeaponForce(target, owner - 1, 32, 2);
            target = 0;
        }
        else
        {
            MoveObjectVector(ptr + 1, - UnitRatioX(ptr, target, 20.0), - UnitRatioY(ptr, target, 20.0));
        }
        PushTimerQueue(1, ptr, ThrowImpByIceBear);
    }
    else
    {
        Delete(ptr);
        Delete(ptr + 1);
    }
}

void IceBearHitImp()
{
    float x= GetObjectX(SELF) - UnitRatioX(SELF, OTHER, 13.0), y= GetObjectY(SELF) - UnitRatioY(SELF, OTHER, 13.0);
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", x,y);
    Frozen(CreateObjectAt("Imp", x,y), TRUE);
    LookAtObject(ptr + 1, OTHER);
    SetOwner(SELF, ptr);
    Raise(ptr, ToFloat(GetCaller()));
    PushTimerQueue(1, ptr, ThrowImpByIceBear);
}

void StrRareTowerShop()
{
	string name = "ManaBombOrb";
	int i = -1, arr[]={
        2116272798, 343951108, 144968185, 304152850, 37780416, 1078232146, 311429392, 554991904, 38019106, 2085097336,
        167309451, 2097436, 606241316, 1077943300, 545817087, 1895895569, 536870959, 1149521988, 268976256, 336660496,
        1344283202, 1091041287, 2022640376, 33622039, 41951330, 1090781248, 134283008,
    };
	while(++i < 27)
		drawStrRareTowerShop(arr[i], name);
}

void drawStrRareTowerShop(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(128);
		pos_y = GetWaypointY(128);
	}
	for (i = 1 ; i > 0 && count < 837 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 128);
		if (count % 76 == 75)
			MoveWaypoint(128, GetWaypointX(128) - 150.000000, GetWaypointY(128) + 2.000000);
		else
			MoveWaypoint(128, GetWaypointX(128) + 2.000000, GetWaypointY(128));
		count ++;
	}
	if (count >= 837)
	{
		count = 0;
		MoveWaypoint(128, pos_x, pos_y);
	}
}

void FigureTowers(int args)
{
    int wp = args & 0xffff, lv = (args >> 16) & 0xffff, k;
    int ptr = CreateObject("InvisibleLightBlueHigh", wp) + 1;
    float pos_y = LocationY(wp);
    int flag;

    for (k = 0 ; k < 8 ; k ++)
    {
        FigureUnitSetting(CreateObject(TowerTypes(lv * 8 + k), wp));
        flag=CreateObjectById(OBJ_FLAG, LocationX(wp), LocationY(wp));
        UnitNoCollide(flag);
        SetFlagColor(flag, TColor[lv]);
        TeleportLocationVector(wp, 56.0, 0.0);
    }
    Delete(ptr - 1);
}

void FigureUnitSetting(int unit)
{
    CheckMonsterThing(unit);
    SetOwner(GetMaster(), unit);
    Frozen(unit, 1);
    UnitNoCollide(unit);
    LookWithAngle(unit, 64);
}

void FigureOutTowers()
{
    PushTimerQueue(1, 135, FigureTowers);
    PushTimerQueue(2, 136 | (1 << 16), FigureTowers);
    PushTimerQueue(2, 137 | (2 << 16), FigureTowers);
    PushTimerQueue(2, 138 | (3 << 16), FigureTowers);
    PushTimerQueue(2, 139 | (4 << 16), FigureTowers);
}

void UnitVisibleSplashA()
{
    int parent = GetOwner(SELF);
    int spIdx = ToInt(GetObjectZ(parent + 1));

    if (CurrentHealth(GetOwner(parent)))
    {
        if (GetUnit1C(OTHER) ^ spIdx)
        {
            if (DistanceUnitToUnit(SELF, OTHER) <= GetObjectX(parent))
            {
                Damage(OTHER, GetOwner(parent), ToInt(GetObjectZ(parent)), 14);
                SetUnit1C(OTHER, spIdx);
            }
        }
    }
}

void SplashDamageAt(int owner, int dam, float x, float y, float range)
{
    int ptr = CreateObjectAt("InvisibleLightBlueHigh", range, y) + 2, SplashIdx;

    SplashIdx ++;
    Raise(CreateObjectAt("InvisibleLightBlueHigh", GetObjectX(ptr), GetObjectY(ptr)), SplashIdx);
    SetOwner(owner, ptr - 2);
    Raise(ptr - 2, ToFloat(dam));

    int k = -1;
    while (++k < 4)
    {
        DeleteObjectTimer(CreateObjectAt("WeirdlingBeast", x, y), 1);
        UnitNoCollide(ptr + k);
        LookWithAngle(ptr + k, k * 64);
        SetOwner(ptr - 2, ptr + k);
        SetCallback(ptr + k, 3, UnitVisibleSplashA);
    }
    DeleteObjectTimer(ptr - 1, 2);
    DeleteObjectTimer(ptr - 2, 2);
}

int GetTowerPower(int owner, int apply)
{
    return ToInt(GetObjectZ(owner + 2)) + (PUpgrade[(ToInt(GetObjectZ(owner + 3)) * 8) + GetDirection(owner + 2)] * apply);
}

void SetUnitHealth(int unit, int amount)
{
    Damage(unit, 0, CurrentHealth(unit) - 1, -1);
    Pickup(unit, CreateObject("RottenMeat", 1));
    Damage(unit, 0, CurrentHealth(unit) - amount, -1);
}

void StrPersonalMission()
{
	string name = "ManaBombOrb";
    int arr[]={
        2116272734, 1151353348, 1678313616, 287380097, 620892196, 1346913412, 151557128, 1746813217, 1111630787, 1074040912,
        545294600, 303104018, 69485056, 1074284608, 528486544, 1629487137, 1069678655,
    };
    int i = -1;

	while (++i < 17)
		drawStrPersonalMission(arr[i], name);
}

void drawStrPersonalMission(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(140);
		pos_y = GetWaypointY(140);
	}
	for (i = 1 ; i > 0 && count < 527 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 140);
		if (count % 48 == 47)
			MoveWaypoint(140, GetWaypointX(140) - 94.000000, GetWaypointY(140) + 2.000000);
		else
			MoveWaypoint(140, GetWaypointX(140) + 2.000000, GetWaypointY(140));
		count ++;
	}
	if (count >= 527)
	{
		count = 0;
		MoveWaypoint(140, pos_x, pos_y);
	}
}

void PutDescriptStr()
{
    int count;

    if (count < 6)
    {
        MoveWaypoint(140, GetWaypointX(141 + count), GetWaypointY(141 + count));
        StrPersonalMission();
        count ++;
        FrameTimer(3, PutDescriptStr);
    }
    else
        StrUniqueTower();
}

string GameMessage(int num)
{
    string name[] = {
        "녹스 랜덤타워 디펜스                                  -제작: HappySoftLtd",
        "게임이 시작되면 대기실에 있는 모든 플레이어들이 각자의 라인으로 이동됩니다, 게임 시작이 시작된 후 부터는 플레이어의 입장이 제한됩니다",
        "라인 몬스터가 목적지에 도달하게 되면 라이프가 감소됩니다, 기본 라이프는 30개가 제공되며 라이프가 없을 시 게임오버 처리됩니다",
        "키 조작 방법 안내: K키(가리키기)= 해당 위치에 랜덤타워 건설, L키(웃기)=라인 중앙으로 화면이동, J키=타워 판매",
        "화면 이동방법: 마우스를 화면 끝에 갖다대면 마우스 방향으로 화면이 이동됩니다, 참고하셔서 착오 없으시기 바랍니다",
        "같은 종류의 타워 두개를 클릭하면 다음 레밸의 타워로 조합됩니다, 이때 조합된 타워 역시 랜덤으로 결정됩니다",
        "개인미션: 1, 2, 3단계의 개인미션이 존재합니다, 각 미션은 고유의 쿨다운이 존재하며 미션을 성공하면 보상을, 실패하면 라이프가 소모됩니다",
        "업적 시스템: 특정 타워를 모으거나 킬수를 채워야 하는 업적 시스템이 존재합니다, 업적을 달성하면 라인 클리어에 좀더 수월할 수 있습니다",
        "루비는 타워의 데미지 업그레이드를 위해 필요한 자원입니다, 100 골드를 사용하여 루비로 교환할 수 있습니다, 교환되는 루비의 양은 랜덤입니다",
        "타워마다 속성과 등급이 있습니다, 같은 속성의 타워는 동일한 업그레이드에 영향을 받습니다. 타워 등급 마다 깃발 색상이 다릅니다",
    };
    return name[num];
}

void StartGuideMessage(int num)
{
    if (num < 10)
    {
        UniPrintToAll(GameMessage(num));
        SecondTimerWithArg(5, num + 1, StartGuideMessage);
    }
    else
        UniPrintToAll("맵에 관한 버그 및 개선사항 문의는 blog.daum.net/ky10613 으로 방문하십시오                                   ");
}

void DelayGiveItem(int ptr)
{
    int owner = ToInt(GetObjectZ(ptr));

    if (CurrentHealth(owner))
        Pickup(owner, ptr + 1);
    else
        Delete(ptr + 1);
    Delete(ptr);
}

void GiveItemToPlayer(int user, string item, int id)
{
    int ptr = CreateObject("InvisibleLightBlueHigh", 154);

    LookWithAngle(CreateObject(item, 154), id);
    Raise(ptr, ToFloat(user));
    PushTimerQueue(1, ptr, DelayGiveItem);
}

void StrUniqueTower()
{
	string name = "ManaBombOrb";
    int arr[]={
        2082480632, 1191508999, 158893056, 1052930, 608305666, 75776512, 526465, 304152833, 1138638080, 1074005056,
        152076416, 1092686144, 1069679135, 74469312, 9472288, 268501248, 33611808, 4965640, 134250624, 25104400,
        2113929730, 33570895, 8652808, 69198080, 8225, 7996420, 1107820545, 67049456, 1075905278, 553910272,
        2048, 1611677952, 131135
    };
    int count = -1;
	while(++count < 33)
		drawStrUniqueTower(arr[count], name);
}

void drawStrUniqueTower(int arg_0, string name)
{
	int count;
	int i;
	float pos_x;
	float pos_y;

	if (!count)
	{
		pos_x = GetWaypointX(155);
		pos_y = GetWaypointY(155);
	}
	for (i = 1 ; i > 0 && count < 1023 ; i <<= 1)
	{
		if (i & arg_0)
			CreateObject(name, 155);
		if (count % 92 == 91)
			MoveWaypoint(155, GetWaypointX(155) - 182.000000, GetWaypointY(155) + 2.000000);
		else
			MoveWaypoint(155, GetWaypointX(155) + 2.000000, GetWaypointY(155));
		count ++;
	}
	if (count >= 1023)
	{
		count = 0;
		MoveWaypoint(155, pos_x, pos_y);
	}
}

void SetColorMaiden(int ptr1, int red, int grn, int blue)
{
    int k;

    SetMemory(ptr1 + 4, 1385);
    SetMemory(ptr1 + 0x224, ToInt(1.6));
    for (k = 0 ; k < 32 ; k ++)
        SetMemory(ptr1 + 0x230 + (k * 4), 0x400);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x178, 0xa0);
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 4, grn | (blue << 8) | (red << 16) | (grn << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 8, blue | (red << 8) | (grn << 16) | (blue << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 12, red | (grn << 8) | (blue << 16) | (red << 24));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x81c + 16, grn | (blue << 8));
    SetMemory(GetMemory(ptr1 + 0x2ec) + 0x1e8, VoiceList(7));
}

void PlayerClassOnJoin(int user, int plr)
{
    if (!CurrentHealth(user))
        return;
    
    if (PlayerClassCheckFlag(plr, PLAYER_DEATH_FLAG))
        PlayerClassSetFlag(plr, PLAYER_DEATH_FLAG);
    
    Enchant(user, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(user, "ENCHANT_ANCHORED", 0.0);
    Enchant(user, "ENCHANT_PROTECT_FROM_ELECTRICITY", 3.0);
    MoveObject(user, LocationX(157), LocationY(157));
    EnchantOff(user,"ENCHANT_ANTI_MAGIC");
}

void PlayerClassOnEntryFailure(int pUnit)
{
    MoveObject(pUnit, LocationX(12), LocationY(12));

    Enchant(pUnit, "ENCHANT_FREEZE", 0.0);
    Enchant(pUnit, "ENCHANT_ANTI_MAGIC", 0.0);
    Enchant(pUnit, "ENCHANT_ANCHORED", 0.0);

    UniPrintToAll("현재 버전에서는 이 맵을 구동할 수 없습니다");
}

void OnPlayerDeferredJoin(int user, int plr)
{
    PlayerClassOnJoin(user, plr);
}

static void thisMapInformationPrintMessage(int user)
{
    UniPrint(user, "랜덤 타워 디펜스- 녹스버전");
}

static int createPlayerCamera(float x, float y, int user)
{
    int cam=DummyUnitCreateAt("Bomber", x,y);

    // GiveCreatureToPlayer(user, cam);
    return cam;
}

void PlayerClassDeleteCamera(int plr)
{
    int cam=m_pCamera[plr];

    if (MaxHealth(cam))
    {
        Delete(cam);
        m_pCamera[plr]=0;
    }
}

#define CAMERA_INIT_POS 159
void PlayerClassSetCamera(int pIndex)
{
    PlayerClassDeleteCamera(pIndex);
    m_pCamera[pIndex] = createPlayerCamera(LocationX(CAMERA_INIT_POS), LocationY(CAMERA_INIT_POS), m_player[pIndex]);
}

static void commonServerClientProcedure()
{
    // int index=0;
    // m_pImgVector=CreateImageVector(32);

    // InitializeImageHandlerProcedure(m_pImgVector);
    // AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatUp)+4, 117769, index++);
    // AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatDown)+4, 117828, index++);
    // AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatRight)+4, 117804, index++);
    // AddImageFromResource(m_pImgVector, GetScrCodeField(imageCatLeft)+4, 117792, index++);
    // AddImageFromResource(m_pImgVector, GetScrCodeField(imageBrick)+4, 125167, index++);
    // AddImageFromResource(m_pImgVector, GetScrCodeField(imageCongrate) + 4, 132315, index++);
    // AddImageFromResource(m_pImgVector, GetScrCodeField(KeySettingImage) + 4, 131930, index++);

    // ExtractMapBgm("..\\buzzer.mp3", MapBgmData);
}

void ClientClassTimerLoop()
{
    ClientKeyHandlerRemix();
    FrameTimer(1, ClientClassTimerLoop);
}

static int NetworkUtilClientTimerEnabler()
{
    return TRUE;
}

static void NetworkUtilClientMain()
{
    m_clientNetId = GetMemory(0x979720);
    FrameTimer(30, ClientClassTimerLoop);
    commonServerClientProcedure();
}

static void playerClassOnCommonInit(int pIndex, int pUnit)
{
    m_player[pIndex]=pUnit;
    m_pFlag[pIndex]=1;
    m_pLife[pIndex]=30;
    m_userTowerCount[pIndex]=0;
    m_pLineIndex[pIndex]=-1;
    ++m_userCount;
    PlayerClassSetCamera(pIndex);
    if (ValidPlayerCheck(pUnit))
    {
        if (GetHost() ^ pUnit)
            NetworkUtilClientEntry(pUnit);
        else
            commonServerClientProcedure();
        // PushTimerQueue(60, pUnit, NetPlayCustomBgm);
    }
    EmptyAll(pUnit);
    SelfDamageClassEntry(pUnit);
    DiePlayerHandlerEntry(pUnit);
    ChangeGold(pUnit, -GetGold(pUnit));
    ChangeGold(pUnit, 400);
    m_clientKeyState[ GetPlayerIndex(pUnit) ] = 0;
    PushTimerQueue(1, pIndex, PlayerClassOnLoop);
}

static void playerClassOnLineInit(int pIndex, int pUnit, int lineIndex)
{
    m_pLineIndex[pIndex]=lineIndex;
    short moveTo=lineIndex+1;
    MoveObject(m_pCamera[pIndex], LocationX(moveTo), LocationY(moveTo));
    InitPlayerUpgrades(pIndex);
    PlayerInitStack(pIndex);
    RegistQuestList(pIndex);
    if (!PlayerClassCheckFlag(pIndex, PLAYER_LINE_FLAG))
        PlayerClassSetFlag(pIndex, PLAYER_LINE_FLAG);
}

static int playerClassCheckLimitUserCount()
{
    if (m_userGameCount<REAL_THISMAP_MAX_PLAYER)
        return m_userGameCount++;
    return -1;
}

int PlayerClassOnInit(int pIndex, int pUnit, char *userInit)
{
    WriteLog("PlayerClassOnInit");
    playerClassOnCommonInit(pIndex, pUnit);
    int lineIdx=playerClassCheckLimitUserCount();
    if (lineIdx>=0)
        playerClassOnLineInit(pIndex, pUnit, lineIdx);

    return pIndex;
}

void PlayerClassOnEntry(int plrUnit)
{
    if (!CurrentHealth(plrUnit))
        return;

    int pIndex = GetPlayerIndex(plrUnit);
    char initialUser=FALSE;

    if (pIndex<0)
        return;

    if (!MaxHealth(m_player[pIndex]))
        PlayerClassOnInit(pIndex, plrUnit, &initialUser);
    
    if (initialUser)
        OnPlayerDeferredJoin(plrUnit, pIndex);
    else
        PlayerClassOnJoin(plrUnit, pIndex);
    // PlayerClassOnEntryFailure(plrUnit); //where place it
}

static void entryInvoke(int fn, int user)
{
    Bind(fn, &fn+4);
}

static void playerGoSelectRoom(int user)
{
    if (m_initPlayerCounter<MAX_INIT_PLAYER_COUNT)
    {
        m_initPlayer[m_initPlayerCounter++]=user;
        MoveObject(user, LocationX(158), LocationY(158));
        PlaySoundAround(user, SOUND_SmallGong);
        UniPrint(user, "언제든지 게임시작 버튼을 눌러보세요. 단, 게임 시작 후 들어오는 유저는 구경만 해야해요!");
        return;
    }
    MoveObject(user, LocationX(156), LocationY(156));
}

void PlayerRegist()
{
    entryInvoke(m_entryController, GetCaller());
}

static int checkIsPlayerAlive(int pIndex, int pUnit)
{
    if (MaxHealth(m_player[pIndex]))
    {
        return m_player[pIndex]==pUnit;
    }
    return FALSE;
}

void PlayerFastJoin()
{
    if (CurrentHealth(OTHER))
    {
        SetUnitEnchantCopy(OTHER, GetLShift(ENCHANT_ANTI_MAGIC) | GetLShift(ENCHANT_ANCHORED));
        if (checkIsPlayerAlive(GetPlayerIndex(OTHER), GetCaller()))
        {
            PlayerClassOnEntry(GetCaller());
            UniPrint(OTHER, "패스트 조인되었습니다");
        }
        else        
            MoveObject(OTHER, LocationX(156), LocationY(156));
    }
}

