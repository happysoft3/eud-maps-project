
#define MAX_PLAYER_COUNT 32

int GetPlayer(int pIndex) //virtual
{ }

void SetPlayer(int pIndex, int user) //virtual
{ }

int GetPlayerFlags(int pIndex) //virtual
{ }

void SetPlayerFlags(int pIndex, int flags) //virtual
{ }

#define PLAYER_FLAG_WINDBOOST 0x4
#define PLAYER_FLAG_BERSERKER_CHARGE 0x8
#define PLAYER_FLAG_WARCRY 16
#define PLAYER_FLAG_GOD_MODE 32

int PlayerClassCheckFlag(int pIndex, int flags)
{
    return GetPlayerFlags(pIndex) & flags;
}

void PlayerClassSetFlag(int pIndex, int flags)
{
    SetPlayerFlags(pIndex, GetPlayerFlags(pIndex)^flags);
}

int GetMasterUnit() //virtual
{ }

void TryAttackMonsterToPlayer(int mon, int user) //virtual
{ }

int GetDummyDamageHandler() //virtual
{ }

int GetMobAttackFunction(int n) //virtual
{ }

int GetMobHP(int n) //virtual
{ }

int GetMobType() //virtual
{ }

#define _CLIENT_OPTION_TYPE_OFF_ 0x753B10

int GetDialogCtx() //virtual
{ }

void SetDialogCtx(int ctx) //virtual
{ }

string GetPopupMessage(int index) //virtual
{ }

int GenericHash() //virtual
{}

